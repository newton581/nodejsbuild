	.file	"test_util.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB8029:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8029:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.text
	.p2align 4
	.type	_ZZN21UtilTest_SPrintF_Test8TestBodyEvENUlvE_4_FUNEv, @function
_ZZN21UtilTest_SPrintF_Test8TestBodyEvENUlvE_4_FUNEv:
.LFB8232:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8232:
	.size	_ZZN21UtilTest_SPrintF_Test8TestBodyEvENUlvE_4_FUNEv, .-_ZZN21UtilTest_SPrintF_Test8TestBodyEvENUlvE_4_FUNEv
	.section	.text._ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED2Ev:
.LFB11219:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11219:
	.size	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED1Ev,_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED2Ev:
.LFB11227:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11227:
	.size	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED1Ev,_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED2Ev:
.LFB11235:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11235:
	.size	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED1Ev,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED2Ev:
.LFB11243:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11243:
	.size	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED1Ev,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED2Ev:
.LFB11251:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11251:
	.size	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED1Ev,_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED2Ev:
.LFB11259:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11259:
	.size	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED1Ev,_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED2Ev:
.LFB11267:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11267:
	.size	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED1Ev,_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED2Ev:
.LFB11275:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11275:
	.size	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED1Ev,_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED2Ev:
.LFB11283:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11283:
	.size	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED1Ev,_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED2Ev:
.LFB11291:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11291:
	.size	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED1Ev,_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED0Ev:
.LFB11293:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11293:
	.size	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED0Ev:
.LFB11285:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11285:
	.size	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED0Ev:
.LFB11277:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11277:
	.size	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED0Ev:
.LFB11269:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11269:
	.size	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED0Ev:
.LFB11261:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11261:
	.size	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED0Ev:
.LFB11253:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11253:
	.size	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED0Ev:
.LFB11245:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11245:
	.size	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED0Ev:
.LFB11237:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11237:
	.size	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED0Ev:
.LFB11229:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11229:
	.size	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED0Ev:
.LFB11221:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11221:
	.size	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED0Ev
	.section	.text._ZN21UtilTest_SPrintF_TestD2Ev,"axG",@progbits,_ZN21UtilTest_SPrintF_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN21UtilTest_SPrintF_TestD2Ev
	.type	_ZN21UtilTest_SPrintF_TestD2Ev, @function
_ZN21UtilTest_SPrintF_TestD2Ev:
.LFB11215:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV21UtilTest_SPrintF_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11215:
	.size	_ZN21UtilTest_SPrintF_TestD2Ev, .-_ZN21UtilTest_SPrintF_TestD2Ev
	.weak	_ZN21UtilTest_SPrintF_TestD1Ev
	.set	_ZN21UtilTest_SPrintF_TestD1Ev,_ZN21UtilTest_SPrintF_TestD2Ev
	.section	.text._ZN21UtilTest_SPrintF_TestD0Ev,"axG",@progbits,_ZN21UtilTest_SPrintF_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN21UtilTest_SPrintF_TestD0Ev
	.type	_ZN21UtilTest_SPrintF_TestD0Ev, @function
_ZN21UtilTest_SPrintF_TestD0Ev:
.LFB11217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV21UtilTest_SPrintF_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11217:
	.size	_ZN21UtilTest_SPrintF_TestD0Ev, .-_ZN21UtilTest_SPrintF_TestD0Ev
	.section	.text._ZN30UtilTest_MaybeStackBuffer_TestD2Ev,"axG",@progbits,_ZN30UtilTest_MaybeStackBuffer_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN30UtilTest_MaybeStackBuffer_TestD2Ev
	.type	_ZN30UtilTest_MaybeStackBuffer_TestD2Ev, @function
_ZN30UtilTest_MaybeStackBuffer_TestD2Ev:
.LFB11223:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV30UtilTest_MaybeStackBuffer_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11223:
	.size	_ZN30UtilTest_MaybeStackBuffer_TestD2Ev, .-_ZN30UtilTest_MaybeStackBuffer_TestD2Ev
	.weak	_ZN30UtilTest_MaybeStackBuffer_TestD1Ev
	.set	_ZN30UtilTest_MaybeStackBuffer_TestD1Ev,_ZN30UtilTest_MaybeStackBuffer_TestD2Ev
	.section	.text._ZN30UtilTest_MaybeStackBuffer_TestD0Ev,"axG",@progbits,_ZN30UtilTest_MaybeStackBuffer_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN30UtilTest_MaybeStackBuffer_TestD0Ev
	.type	_ZN30UtilTest_MaybeStackBuffer_TestD0Ev, @function
_ZN30UtilTest_MaybeStackBuffer_TestD0Ev:
.LFB11225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV30UtilTest_MaybeStackBuffer_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11225:
	.size	_ZN30UtilTest_MaybeStackBuffer_TestD0Ev, .-_ZN30UtilTest_MaybeStackBuffer_TestD0Ev
	.section	.text._ZN29UtilTest_UncheckedCalloc_TestD2Ev,"axG",@progbits,_ZN29UtilTest_UncheckedCalloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN29UtilTest_UncheckedCalloc_TestD2Ev
	.type	_ZN29UtilTest_UncheckedCalloc_TestD2Ev, @function
_ZN29UtilTest_UncheckedCalloc_TestD2Ev:
.LFB11231:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV29UtilTest_UncheckedCalloc_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11231:
	.size	_ZN29UtilTest_UncheckedCalloc_TestD2Ev, .-_ZN29UtilTest_UncheckedCalloc_TestD2Ev
	.weak	_ZN29UtilTest_UncheckedCalloc_TestD1Ev
	.set	_ZN29UtilTest_UncheckedCalloc_TestD1Ev,_ZN29UtilTest_UncheckedCalloc_TestD2Ev
	.section	.text._ZN29UtilTest_UncheckedCalloc_TestD0Ev,"axG",@progbits,_ZN29UtilTest_UncheckedCalloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN29UtilTest_UncheckedCalloc_TestD0Ev
	.type	_ZN29UtilTest_UncheckedCalloc_TestD0Ev, @function
_ZN29UtilTest_UncheckedCalloc_TestD0Ev:
.LFB11233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV29UtilTest_UncheckedCalloc_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11233:
	.size	_ZN29UtilTest_UncheckedCalloc_TestD0Ev, .-_ZN29UtilTest_UncheckedCalloc_TestD0Ev
	.section	.text._ZN29UtilTest_UncheckedMalloc_TestD2Ev,"axG",@progbits,_ZN29UtilTest_UncheckedMalloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN29UtilTest_UncheckedMalloc_TestD2Ev
	.type	_ZN29UtilTest_UncheckedMalloc_TestD2Ev, @function
_ZN29UtilTest_UncheckedMalloc_TestD2Ev:
.LFB11239:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV29UtilTest_UncheckedMalloc_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11239:
	.size	_ZN29UtilTest_UncheckedMalloc_TestD2Ev, .-_ZN29UtilTest_UncheckedMalloc_TestD2Ev
	.weak	_ZN29UtilTest_UncheckedMalloc_TestD1Ev
	.set	_ZN29UtilTest_UncheckedMalloc_TestD1Ev,_ZN29UtilTest_UncheckedMalloc_TestD2Ev
	.section	.text._ZN29UtilTest_UncheckedMalloc_TestD0Ev,"axG",@progbits,_ZN29UtilTest_UncheckedMalloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN29UtilTest_UncheckedMalloc_TestD0Ev
	.type	_ZN29UtilTest_UncheckedMalloc_TestD0Ev, @function
_ZN29UtilTest_UncheckedMalloc_TestD0Ev:
.LFB11241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV29UtilTest_UncheckedMalloc_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11241:
	.size	_ZN29UtilTest_UncheckedMalloc_TestD0Ev, .-_ZN29UtilTest_UncheckedMalloc_TestD0Ev
	.section	.text._ZN20UtilTest_Calloc_TestD2Ev,"axG",@progbits,_ZN20UtilTest_Calloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN20UtilTest_Calloc_TestD2Ev
	.type	_ZN20UtilTest_Calloc_TestD2Ev, @function
_ZN20UtilTest_Calloc_TestD2Ev:
.LFB11247:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV20UtilTest_Calloc_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11247:
	.size	_ZN20UtilTest_Calloc_TestD2Ev, .-_ZN20UtilTest_Calloc_TestD2Ev
	.weak	_ZN20UtilTest_Calloc_TestD1Ev
	.set	_ZN20UtilTest_Calloc_TestD1Ev,_ZN20UtilTest_Calloc_TestD2Ev
	.section	.text._ZN20UtilTest_Calloc_TestD0Ev,"axG",@progbits,_ZN20UtilTest_Calloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN20UtilTest_Calloc_TestD0Ev
	.type	_ZN20UtilTest_Calloc_TestD0Ev, @function
_ZN20UtilTest_Calloc_TestD0Ev:
.LFB11249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV20UtilTest_Calloc_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11249:
	.size	_ZN20UtilTest_Calloc_TestD0Ev, .-_ZN20UtilTest_Calloc_TestD0Ev
	.section	.text._ZN20UtilTest_Malloc_TestD2Ev,"axG",@progbits,_ZN20UtilTest_Malloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN20UtilTest_Malloc_TestD2Ev
	.type	_ZN20UtilTest_Malloc_TestD2Ev, @function
_ZN20UtilTest_Malloc_TestD2Ev:
.LFB11255:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV20UtilTest_Malloc_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11255:
	.size	_ZN20UtilTest_Malloc_TestD2Ev, .-_ZN20UtilTest_Malloc_TestD2Ev
	.weak	_ZN20UtilTest_Malloc_TestD1Ev
	.set	_ZN20UtilTest_Malloc_TestD1Ev,_ZN20UtilTest_Malloc_TestD2Ev
	.section	.text._ZN20UtilTest_Malloc_TestD0Ev,"axG",@progbits,_ZN20UtilTest_Malloc_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN20UtilTest_Malloc_TestD0Ev
	.type	_ZN20UtilTest_Malloc_TestD0Ev, @function
_ZN20UtilTest_Malloc_TestD0Ev:
.LFB11257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV20UtilTest_Malloc_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11257:
	.size	_ZN20UtilTest_Malloc_TestD0Ev, .-_ZN20UtilTest_Malloc_TestD0Ev
	.section	.text._ZN21UtilTest_ToLower_TestD2Ev,"axG",@progbits,_ZN21UtilTest_ToLower_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN21UtilTest_ToLower_TestD2Ev
	.type	_ZN21UtilTest_ToLower_TestD2Ev, @function
_ZN21UtilTest_ToLower_TestD2Ev:
.LFB11263:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV21UtilTest_ToLower_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11263:
	.size	_ZN21UtilTest_ToLower_TestD2Ev, .-_ZN21UtilTest_ToLower_TestD2Ev
	.weak	_ZN21UtilTest_ToLower_TestD1Ev
	.set	_ZN21UtilTest_ToLower_TestD1Ev,_ZN21UtilTest_ToLower_TestD2Ev
	.section	.text._ZN21UtilTest_ToLower_TestD0Ev,"axG",@progbits,_ZN21UtilTest_ToLower_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN21UtilTest_ToLower_TestD0Ev
	.type	_ZN21UtilTest_ToLower_TestD0Ev, @function
_ZN21UtilTest_ToLower_TestD0Ev:
.LFB11265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV21UtilTest_ToLower_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11265:
	.size	_ZN21UtilTest_ToLower_TestD0Ev, .-_ZN21UtilTest_ToLower_TestD0Ev
	.section	.text._ZN32UtilTest_StringEqualNoCaseN_TestD2Ev,"axG",@progbits,_ZN32UtilTest_StringEqualNoCaseN_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32UtilTest_StringEqualNoCaseN_TestD2Ev
	.type	_ZN32UtilTest_StringEqualNoCaseN_TestD2Ev, @function
_ZN32UtilTest_StringEqualNoCaseN_TestD2Ev:
.LFB11271:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV32UtilTest_StringEqualNoCaseN_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11271:
	.size	_ZN32UtilTest_StringEqualNoCaseN_TestD2Ev, .-_ZN32UtilTest_StringEqualNoCaseN_TestD2Ev
	.weak	_ZN32UtilTest_StringEqualNoCaseN_TestD1Ev
	.set	_ZN32UtilTest_StringEqualNoCaseN_TestD1Ev,_ZN32UtilTest_StringEqualNoCaseN_TestD2Ev
	.section	.text._ZN32UtilTest_StringEqualNoCaseN_TestD0Ev,"axG",@progbits,_ZN32UtilTest_StringEqualNoCaseN_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32UtilTest_StringEqualNoCaseN_TestD0Ev
	.type	_ZN32UtilTest_StringEqualNoCaseN_TestD0Ev, @function
_ZN32UtilTest_StringEqualNoCaseN_TestD0Ev:
.LFB11273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV32UtilTest_StringEqualNoCaseN_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11273:
	.size	_ZN32UtilTest_StringEqualNoCaseN_TestD0Ev, .-_ZN32UtilTest_StringEqualNoCaseN_TestD0Ev
	.section	.text._ZN31UtilTest_StringEqualNoCase_TestD2Ev,"axG",@progbits,_ZN31UtilTest_StringEqualNoCase_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31UtilTest_StringEqualNoCase_TestD2Ev
	.type	_ZN31UtilTest_StringEqualNoCase_TestD2Ev, @function
_ZN31UtilTest_StringEqualNoCase_TestD2Ev:
.LFB11279:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV31UtilTest_StringEqualNoCase_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11279:
	.size	_ZN31UtilTest_StringEqualNoCase_TestD2Ev, .-_ZN31UtilTest_StringEqualNoCase_TestD2Ev
	.weak	_ZN31UtilTest_StringEqualNoCase_TestD1Ev
	.set	_ZN31UtilTest_StringEqualNoCase_TestD1Ev,_ZN31UtilTest_StringEqualNoCase_TestD2Ev
	.section	.text._ZN31UtilTest_StringEqualNoCase_TestD0Ev,"axG",@progbits,_ZN31UtilTest_StringEqualNoCase_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31UtilTest_StringEqualNoCase_TestD0Ev
	.type	_ZN31UtilTest_StringEqualNoCase_TestD0Ev, @function
_ZN31UtilTest_StringEqualNoCase_TestD0Ev:
.LFB11281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV31UtilTest_StringEqualNoCase_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11281:
	.size	_ZN31UtilTest_StringEqualNoCase_TestD0Ev, .-_ZN31UtilTest_StringEqualNoCase_TestD0Ev
	.section	.text._ZN22UtilTest_ListHead_TestD2Ev,"axG",@progbits,_ZN22UtilTest_ListHead_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22UtilTest_ListHead_TestD2Ev
	.type	_ZN22UtilTest_ListHead_TestD2Ev, @function
_ZN22UtilTest_ListHead_TestD2Ev:
.LFB11287:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV22UtilTest_ListHead_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11287:
	.size	_ZN22UtilTest_ListHead_TestD2Ev, .-_ZN22UtilTest_ListHead_TestD2Ev
	.weak	_ZN22UtilTest_ListHead_TestD1Ev
	.set	_ZN22UtilTest_ListHead_TestD1Ev,_ZN22UtilTest_ListHead_TestD2Ev
	.section	.text._ZN22UtilTest_ListHead_TestD0Ev,"axG",@progbits,_ZN22UtilTest_ListHead_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22UtilTest_ListHead_TestD0Ev
	.type	_ZN22UtilTest_ListHead_TestD0Ev, @function
_ZN22UtilTest_ListHead_TestD0Ev:
.LFB11289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV22UtilTest_ListHead_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11289:
	.size	_ZN22UtilTest_ListHead_TestD0Ev, .-_ZN22UtilTest_ListHead_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv:
.LFB11368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV22UtilTest_ListHead_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11368:
	.size	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv:
.LFB11367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV31UtilTest_StringEqualNoCase_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11367:
	.size	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv:
.LFB11366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV32UtilTest_StringEqualNoCaseN_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11366:
	.size	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv:
.LFB11365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV21UtilTest_ToLower_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11365:
	.size	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv:
.LFB11364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV20UtilTest_Malloc_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11364:
	.size	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv:
.LFB11363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV20UtilTest_Calloc_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11363:
	.size	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv:
.LFB11362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV29UtilTest_UncheckedMalloc_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11362:
	.size	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv:
.LFB11361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV29UtilTest_UncheckedCalloc_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11361:
	.size	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv:
.LFB11360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV30UtilTest_MaybeStackBuffer_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11360:
	.size	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv:
.LFB11359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV21UtilTest_SPrintF_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11359:
	.size	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0:
.LFB11479:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11479:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0:
.LFB11480:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11480:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	.text
	.align 2
	.p2align 4
	.type	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0, @function
_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0:
.LFB11515:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L82
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%r12), %rax
	subq	$8, %rsp
	movq	(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	ret
	.cfi_endproc
.LFE11515:
	.size	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0, .-_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	.align 2
	.p2align 4
	.globl	_ZN32UtilTest_StringEqualNoCaseN_Test8TestBodyEv
	.type	_ZN32UtilTest_StringEqualNoCaseN_Test8TestBodyEv, @function
_ZN32UtilTest_StringEqualNoCaseN_Test8TestBodyEv:
.LFB8180:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8180:
	.size	_ZN32UtilTest_StringEqualNoCaseN_Test8TestBodyEv, .-_ZN32UtilTest_StringEqualNoCaseN_Test8TestBodyEv
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0:
.LFB11644:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L90
	testq	%rsi, %rsi
	je	.L106
.L90:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L107
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L93
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L94:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L108
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L94
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L92:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L94
.L106:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11644:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB11518:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	orq	$-1, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L110
	movq	%rsi, %rdi
	call	strlen@PLT
	leaq	(%r12,%rax), %rdx
.L110:
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	.cfi_endproc
.LFE11518:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata._ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"0123456789abcdef"
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB11532:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC5(%rip), %rax
.L116:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L116
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L117
	call	__stack_chk_fail@PLT
.L117:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11532:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB11536:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC5(%rip), %rax
.L121:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L121
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L122
	call	__stack_chk_fail@PLT
.L122:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11536:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.rodata.str1.1
.LC6:
	.string	"equal"
.LC7:
	.string	"EQUAL"
.LC8:
	.string	"equals"
.LC9:
	.string	"true"
.LC10:
	.string	"false"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"StringEqualNoCase(\"equal\", \"EQUAL\")"
	.section	.rodata.str1.1
.LC12:
	.string	"../test/cctest/test_util.cc"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"StringEqualNoCase(\"equal\", \"equals\")"
	.align 8
.LC14:
	.string	"StringEqualNoCase(\"equals\", \"equal\")"
	.text
	.align 2
	.p2align 4
	.globl	_ZN31UtilTest_StringEqualNoCase_Test8TestBodyEv
	.type	_ZN31UtilTest_StringEqualNoCase_Test8TestBodyEv, @function
_ZN31UtilTest_StringEqualNoCase_Test8TestBodyEv:
.LFB8173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L126:
	addq	$1, %rax
	cmpb	$0, (%rax)
	jne	.L126
	leaq	.LC7(%rip), %rcx
	movl	$101, %eax
	leaq	.LC6(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L127:
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L130
	leal	-65(%rax), %r8d
	leal	32(%rax), %edi
	addq	$1, %rsi
	cmpb	$26, %r8b
	leal	-65(%rdx), %r8d
	cmovb	%edi, %eax
	addq	$1, %rcx
	leal	32(%rdx), %edi
	cmpb	$26, %r8b
	cmovb	%edi, %edx
	cmpb	%al, %dl
	jne	.L130
	movzbl	(%rsi), %eax
	testb	%al, %al
	jne	.L127
	movzbl	(%rcx), %eax
	movq	$0, -72(%rbp)
	testb	%al, %al
	sete	-80(%rbp)
	jne	.L128
.L129:
	leaq	.LC7(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L137:
	addq	$1, %rax
	cmpb	$0, (%rax)
	jne	.L137
	leaq	.LC8(%rip), %rcx
	movl	$101, %eax
	leaq	.LC6(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L138:
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L155
	leal	-65(%rax), %r8d
	leal	32(%rax), %edi
	addq	$1, %rsi
	cmpb	$26, %r8b
	leal	-65(%rdx), %r8d
	cmovb	%edi, %eax
	addq	$1, %rcx
	leal	32(%rdx), %edi
	cmpb	$26, %r8b
	cmovb	%edi, %edx
	cmpb	%al, %dl
	jne	.L155
	movzbl	(%rsi), %eax
	testb	%al, %al
	jne	.L138
	movzbl	(%rcx), %eax
	movq	$0, -72(%rbp)
	testb	%al, %al
	setne	-80(%rbp)
	jne	.L155
	leaq	-88(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-64(%rbp), %r8
	movl	$67, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*8(%rax)
.L145:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L155
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L155:
	leaq	.LC6(%rip), %rcx
	movl	$101, %eax
	leaq	.LC8(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L146:
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L125
	leal	-65(%rax), %r8d
	leal	32(%rax), %edi
	addq	$1, %rsi
	cmpb	$26, %r8b
	leal	-65(%rdx), %r8d
	cmovb	%edi, %eax
	addq	$1, %rcx
	leal	32(%rdx), %edi
	cmpb	$26, %r8b
	cmovb	%edi, %edx
	cmpb	%al, %dl
	jne	.L125
	movzbl	(%rsi), %eax
	testb	%al, %al
	jne	.L146
	movzbl	(%rcx), %eax
	movq	$0, -72(%rbp)
	testb	%al, %al
	setne	-80(%rbp)
	jne	.L125
	leaq	-88(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-64(%rbp), %r8
	movl	$68, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L153
	movq	(%rdi), %rax
	call	*8(%rax)
.L153:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L125:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movb	$0, -80(%rbp)
	movq	$0, -72(%rbp)
.L128:
	leaq	-88(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-64(%rbp), %r8
	movl	$65, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L135
	movq	(%rdi), %rax
	call	*8(%rax)
.L135:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L129
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L129
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8173:
	.size	_ZN31UtilTest_StringEqualNoCase_Test8TestBodyEv, .-_ZN31UtilTest_StringEqualNoCase_Test8TestBodyEv
	.section	.rodata.str1.1
.LC15:
	.string	"NULL"
	.text
	.p2align 4
	.type	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_:
.LFB9693:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	cmpq	%rax, (%rcx)
	je	.L212
	movq	.LC16(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC17(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L213
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L197:
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L198
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L199
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L200:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-560(%rbp), %xmm3
	movq	-536(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L214
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L203:
	movq	-384(%rbp), %rax
	leaq	-496(%rbp), %r14
	movq	$0, -504(%rbp)
	leaq	-512(%rbp), %r15
	movq	%r14, -512(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L204
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L205
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L206:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L193
	call	_ZdlPv@PLT
.L193:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L215
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L212:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L205:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L199:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$4, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L206
.L215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9693:
	.size	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	.section	.rodata.str1.1
.LC19:
	.string	"buf[i]"
.LC20:
	.string	"static_cast<T>(i)"
	.text
	.p2align 4
	.type	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0, @function
_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0:
.LFB11651:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	(%rdx), %eax
	cmpw	%ax, (%rsi)
	je	.L231
	movq	.LC16(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%rdx, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -600(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC17(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %rdx
	movq	%r14, %rdi
	movzwl	(%rdx), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -576(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L219
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L232
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L221:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -592(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-600(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-568(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movzwl	(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L223
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L224
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L225:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-592(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	-544(%rbp), %r8
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-480(%rbp), %rdi
	cmpq	-576(%rbp), %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	movq	-520(%rbp), %rax
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L231:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L224:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L219:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L221
.L233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11651:
	.size	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0, .-_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L234
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L238:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L236
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L238
.L234:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L238
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4103:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L247
	movq	16(%rdi), %r10
.L241:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L242
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L248
	movq	16(%r9), %r10
.L243:
	cmpq	%r10, %rax
	jbe	.L250
.L242:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L244:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L251
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L246:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L251:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$15, %r10d
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$15, %r10d
	jmp	.L243
	.cfi_endproc
.LFE8630:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB6032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L253
	leaq	16(%r13), %rax
	movq	%r12, %rdi
	movq	%rax, 0(%r13)
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	(%r12,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
.L252:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$96, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	cmpb	$37, 1(%rax)
	jne	.L261
	leaq	-80(%rbp), %r8
	leaq	2(%rax), %rsi
	movq	%rax, -128(%rbp)
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	leaq	-112(%rbp), %r15
	leaq	-96(%rbp), %r14
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-128(%rbp), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r14, -112(%rbp)
	leaq	1(%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-120(%rbp), %r8
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L252
	call	_ZdlPv@PLT
	jmp	.L252
.L261:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6032:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.section	.text._ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,"axG",@progbits,_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,comdat
	.p2align 4
	.weak	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.type	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, @function
_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_:
.LFB8967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	leaq	-432(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -504(%rbp)
	movq	.LC16(%rip), %xmm1
	movhps	.LC17(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -496(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-24(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rax, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r14, %rdi
	leaq	-368(%rbp), %r14
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r15, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movdqa	-496(%rbp), %xmm1
	movq	-24(%rax), %rdx
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp,%rdx)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-504(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L272
	leaq	-464(%rbp), %rcx
	leaq	-480(%rbp), %r9
	movq	%rsi, %rdi
	movq	%rsi, -504(%rbp)
	movq	%rcx, -480(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%r9, -520(%rbp)
	call	strlen@PLT
	movq	-504(%rbp), %rsi
	movq	-520(%rbp), %r9
	leaq	(%rsi,%rax), %rdx
	movq	%r9, %rdi
	movq	%r9, -504(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-504(%rbp), %r9
	movq	%r15, %rsi
	movq	%r9, %rdi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	movq	-480(%rbp), %rdi
	movq	-512(%rbp), %rcx
	cmpq	%rcx, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	leaq	16(%r12), %rax
	movb	$0, 16(%r12)
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	testq	%rax, %rax
	je	.L266
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L273
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L268:
	movq	.LC16(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-496(%rbp), %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L274
	addq	$488, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L272:
	movl	$4, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L266:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L268
.L274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8967:
	.size	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, .-_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_:
.LFB9148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dl, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L279
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L277:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L277
	.cfi_endproc
.LFE9148:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_
	.section	.rodata.str1.1
.LC21:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN21UtilTest_ToLower_Test8TestBodyEv
	.type	_ZN21UtilTest_ToLower_Test8TestBodyEv, @function
_ZN21UtilTest_ToLower_Test8TestBodyEv:
.LFB8187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L319
.L281:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L284
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L284:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L320
.L285:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L288
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L288:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L321
.L289:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L280
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L280:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L282
	movq	(%rax), %r8
.L282:
	leaq	-80(%rbp), %r14
	movl	$88, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L281
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L321:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L290
	movq	(%rax), %r8
.L290:
	leaq	-80(%rbp), %r12
	movl	$90, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L289
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L286
	movq	(%rax), %r8
.L286:
	leaq	-80(%rbp), %r14
	movl	$89, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L285
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L285
.L322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8187:
	.size	_ZN21UtilTest_ToLower_Test8TestBodyEv, .-_ZN21UtilTest_ToLower_Test8TestBodyEv
	.section	.text._ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm,"axG",@progbits,_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm
	.type	_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm, @function
_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm:
.LFB9704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L336
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpq	8(%rdi), %rsi
	jbe	.L326
	leaq	24(%rdi), %r15
	movl	$1, %r14d
	cmpq	%r15, %r13
	je	.L337
.L327:
	testq	%r12, %r12
	je	.L338
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L339
.L329:
	movq	%rax, 16(%rbx)
	movq	%r12, 8(%rbx)
	testb	%r14b, %r14b
	jne	.L326
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L340
.L326:
	movq	%r12, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	_ZZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L338:
	movq	%r13, %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L339:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L329
	leaq	_ZZN4node7ReallocIhEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9704:
	.size	_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm, .-_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm
	.section	.text._ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm,"axG",@progbits,_ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm
	.type	_ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm, @function
_ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm:
.LFB9723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L362
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpq	8(%rdi), %rsi
	jbe	.L344
	leaq	24(%rdi), %rax
	movl	$1, %r14d
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r13
	je	.L363
.L345:
	leaq	(%r12,%r12), %r15
	testq	%r12, %r12
	js	.L364
	testq	%r15, %r15
	je	.L365
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L366
.L349:
	movq	%rdi, 16(%rbx)
	movq	%r12, 8(%rbx)
	testb	%r14b, %r14b
	jne	.L344
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L344
	movq	-56(%rbp), %rsi
	addq	%rdx, %rdx
	call	memcpy@PLT
.L344:
	movq	%r12, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	leaq	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L363:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%r13, %rdi
	call	free@PLT
	movl	$1, %eax
	xorl	%edi, %edi
.L348:
	testq	%r12, %r12
	je	.L349
	testb	%al, %al
	je	.L349
	leaq	_ZZN4node7ReallocItEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L364:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L366:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	sete	%al
	jmp	.L348
	.cfi_endproc
.LFE9723:
	.size	_ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm, .-_ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm
	.section	.text._ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB9725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	cmpq	(%r8), %rax
	je	.L382
	movq	.LC16(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC17(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r14, %rdi
	movq	(%r8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L370
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L383
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L372:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movl	(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L374
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L375
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L376:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L377
	call	_ZdlPv@PLT
.L377:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L378
	call	_ZdlPv@PLT
.L378:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L384
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L382:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L375:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L372
.L384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9725:
	.size	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB9728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	cmpq	%rax, (%rcx)
	je	.L400
	movq	.LC16(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC17(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r14, %rdi
	movq	(%r8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L388
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L401
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L390:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L391
	call	_ZdlPv@PLT
.L391:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L392
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L393
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L394:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L395
	call	_ZdlPv@PLT
.L395:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L385
	call	_ZdlPv@PLT
.L385:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L402
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L400:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L393:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L390
.L402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9728:
	.size	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L404
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L403:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L413
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L406
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L407
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L408:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L403
	call	_ZdlPv@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L407:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L408
.L413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9732:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L415
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L414:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L424
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L417
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L418
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L419:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L420
	call	_ZdlPv@PLT
.L420:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L414
	call	_ZdlPv@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L418:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L419
.L424:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9733:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L426
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L425:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L428
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L429
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L430:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L425
	call	_ZdlPv@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L429:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L428:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L430
.L435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9735:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L437
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L439
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L440
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L441:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L436
	call	_ZdlPv@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L440:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L441
.L446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9736:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L448
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L447:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L450
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L451
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L452:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L453
	call	_ZdlPv@PLT
.L453:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L447
	call	_ZdlPv@PLT
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L451:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L450:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L452
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9738:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L459
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L458:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L468
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L461
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L462
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L463:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L458
	call	_ZdlPv@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L462:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L461:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L463
.L468:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9739:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB9741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -552(%rbp)
	movsd	(%rcx), %xmm0
	movq	%rdx, -560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	ucomisd	(%r8), %xmm0
	jp	.L470
	jne	.L470
	call	_ZN7testing16AssertionSuccessEv@PLT
.L469:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	addq	$568, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	.LC16(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %r15
	movq	%r8, -568(%rbp)
	movq	%r13, %rdi
	movq	%r15, -600(%rbp)
	movhps	.LC17(%rip), %xmm1
	movaps	%xmm1, -544(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-432(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r15, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-544(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -608(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-568(%rbp), %r8
	movq	%r15, %rdi
	movsd	(%r8), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -576(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -568(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L473
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L474
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L475:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -592(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	-600(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	addq	%r15, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-544(%rbp), %xmm3
	movq	-520(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	movq	-608(%rbp), %rsi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movsd	(%r14), %xmm0
	movq	%r15, %rdi
	leaq	-496(%rbp), %r14
	leaq	-512(%rbp), %r15
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L477
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L478
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L479:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-592(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L480
	call	_ZdlPv@PLT
.L480:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	-568(%rbp), %r8
	movq	-560(%rbp), %rdx
	movq	-552(%rbp), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L481
	call	_ZdlPv@PLT
.L481:
	movq	-480(%rbp), %rdi
	cmpq	-576(%rbp), %rdi
	je	.L469
	call	_ZdlPv@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L478:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L474:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L479
.L486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9741:
	.size	_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L488
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L487:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L497
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L490
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L491
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L492:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L493
	call	_ZdlPv@PLT
.L493:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L487
	call	_ZdlPv@PLT
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L491:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L492
.L497:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9746:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L499
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L498:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L508
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L501
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L502
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L503:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L498
	call	_ZdlPv@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L502:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L501:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L503
.L508:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9749:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_:
.LFB9751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$520, %rsp
	movq	%rsi, -536(%rbp)
	movq	%r8, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L510
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L509:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L519
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movq	%rbx, -520(%rbp)
	leaq	-480(%rbp), %rbx
	leaq	-520(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-448(%rbp), %r15
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -552(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdx
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -544(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rdx, %xmm1
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm2
	movaps	%xmm0, -416(%rbp)
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-552(%rbp), %r10
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r13, %rdi
	leaq	-512(%rbp), %r13
	movq	%r8, %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -544(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L512
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L513
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L514:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-536(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L509
	call	_ZdlPv@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L513:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L514
.L519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9751:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_:
.LFB9754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$552, %rsp
	movq	%rdx, -552(%rbp)
	movq	8(%rcx), %rdx
	movq	%rsi, -528(%rbp)
	movq	%rcx, -520(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	8(%r8), %rdx
	je	.L537
.L521:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	leaq	-320(%rbp), %r15
	punpcklqdq	%xmm2, %xmm1
	movq	%r15, %rdi
	leaq	-448(%rbp), %r13
	movaps	%xmm1, -544(%rbp)
	leaq	-432(%rbp), %r14
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r9
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%r9), %rax
	movq	%r9, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%r9), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	pxor	%xmm0, %xmm0
	movdqa	-544(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -584(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -576(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	-480(%rbp), %rbx
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-464(%rbp), %rax
	movq	$0, -472(%rbp)
	movq	%rax, -560(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L524
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L525
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L526:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r9
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r9), %rax
	movq	%r9, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%r9), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movdqa	-544(%rbp), %xmm3
	movq	-568(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	movq	-576(%rbp), %rsi
	movq	%rax, -424(%rbp)
	movq	-584(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-520(%rbp), %rdi
	movq	%r14, %rsi
	leaq	-496(%rbp), %r14
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L527
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L528
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L529:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	-552(%rbp), %rdx
	movq	-528(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	movq	-480(%rbp), %rdi
	cmpq	-560(%rbp), %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L538
	addq	$552, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L522
	movq	(%r8), %rsi
	movq	(%rcx), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L521
.L522:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L528:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L525:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L524:
	leaq	-352(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L527:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L529
.L538:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9754:
	.size	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_
	.section	.rodata._ZN7testing15AssertionResultlsIPKcEERS0_RKT_.str1.1,"aMS",@progbits,1
.LC22:
	.string	"(null)"
.LC23:
	.string	"basic_string::append"
	.section	.text._ZN7testing15AssertionResultlsIPKcEERS0_RKT_,"axG",@progbits,_ZN7testing15AssertionResultlsIPKcEERS0_RKT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	.type	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_, @function
_ZN7testing15AssertionResultlsIPKcEERS0_RKT_:
.LFB10140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-88(%rbp), %r14
	movq	%rdi, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing7MessageC1Ev@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L554
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L555
.L542:
	movq	%r14, %rsi
	leaq	-80(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L556
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	movq	(%rdi), %rax
	call	*8(%rax)
.L545:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L557
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	movl	$6, %edx
	leaq	.LC22(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	jne	.L542
.L555:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdi
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L542
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L542
.L556:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L557:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10140:
	.size	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_, .-_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	.section	.rodata._ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_.str1.1,"aMS",@progbits,1
.LC24:
	.string	"Expected: ("
.LC25:
	.string	") "
.LC26:
	.string	" ("
.LC27:
	.string	"), actual: "
.LC28:
	.string	"(nullptr)"
.LC29:
	.string	" vs "
	.section	.text._ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_,"axG",@progbits,_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	.type	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_, @function
_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_:
.LFB9695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-728(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-720(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$840, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -792(%rbp)
	movq	%rdx, -800(%rbp)
	movq	%r8, -840(%rbp)
	movq	%r9, -808(%rbp)
	movq	%rdi, -816(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing16AssertionFailureEv@PLT
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-728(%rbp), %rax
	movl	$11, %edx
	leaq	.LC24(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L635
.L559:
	movq	%r14, %rsi
	leaq	-640(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-640(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-640(%rbp), %rdi
	leaq	-624(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movq	-728(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L562
	movq	(%rdi), %rax
	call	*8(%rax)
.L562:
	leaq	-792(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-736(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-736(%rbp), %rax
	movl	$2, %edx
	leaq	.LC25(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L636
.L563:
	movq	%r14, %rsi
	leaq	-608(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-608(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L565
	call	_ZdlPv@PLT
.L565:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L566
	movq	(%rdi), %rax
	call	*8(%rax)
.L566:
	leaq	-808(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-744(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-744(%rbp), %rax
	movl	$2, %edx
	leaq	.LC26(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L637
.L567:
	movq	%r14, %rsi
	leaq	-576(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-576(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L568
	call	_ZdlPv@PLT
.L568:
	movq	-744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L569
	movq	(%rdi), %rax
	call	*8(%rax)
.L569:
	leaq	-800(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-752(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-752(%rbp), %rax
	movl	$11, %edx
	leaq	.LC27(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L638
.L570:
	movq	%r14, %rsi
	leaq	-544(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-544(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L571
	call	_ZdlPv@PLT
.L571:
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L572
	movq	(%rdi), %rax
	call	*8(%rax)
.L572:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	leaq	-320(%rbp), %r15
	punpcklqdq	%xmm2, %xmm1
	movq	%r15, %rdi
	leaq	-448(%rbp), %r13
	movaps	%xmm1, -832(%rbp)
	leaq	-432(%rbp), %rbx
	leaq	-424(%rbp), %r14
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%rbx, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-832(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -864(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -872(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$9, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	leaq	-704(%rbp), %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	leaq	-760(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -848(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-760(%rbp), %rax
	movq	-696(%rbp), %rdx
	movq	-704(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r8
	movq	-848(%rbp), %r9
	testq	%r8, %r8
	je	.L639
.L573:
	movq	%r9, %rsi
	leaq	-512(%rbp), %rdi
	movq	%r8, -856(%rbp)
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-512(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -848(%rbp)
	call	strlen@PLT
	movq	-856(%rbp), %r8
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r8), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	-848(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	-760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L575
	movq	(%rdi), %rax
	call	*8(%rax)
.L575:
	leaq	-768(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -848(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-768(%rbp), %rax
	movl	$4, %edx
	leaq	.LC29(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r8
	movq	-848(%rbp), %r9
	testq	%r8, %r8
	je	.L640
.L576:
	movq	%r9, %rsi
	leaq	-480(%rbp), %rdi
	movq	%r8, -848(%rbp)
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-480(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -856(%rbp)
	call	strlen@PLT
	movq	-848(%rbp), %r8
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r8), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	-856(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L577
	call	_ZdlPv@PLT
.L577:
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L578
	movq	(%rdi), %rax
	call	*8(%rax)
.L578:
	movq	-840(%rbp), %rax
	movq	%r15, %rdi
	movq	(%rax), %r8
	movq	%r8, -840(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%rbx, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-832(%rbp), %xmm3
	movq	-864(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	movq	-872(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-840(%rbp), %r8
	testq	%r8, %r8
	je	.L641
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L580:
	movq	%r14, %rsi
	leaq	-672(%rbp), %rdi
	leaq	-776(%rbp), %r15
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-776(%rbp), %rax
	movq	-664(%rbp), %rdx
	movq	-672(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L642
.L581:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-448(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r14), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-448(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L582
	call	_ZdlPv@PLT
.L582:
	movq	-776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L583
	movq	(%rdi), %rax
	call	*8(%rax)
.L583:
	movq	-816(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN7testing15AssertionResultC1ERKS0_@PLT
	movq	-672(%rbp), %rdi
	leaq	-656(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L584
	call	_ZdlPv@PLT
.L584:
	movq	-704(%rbp), %rdi
	leaq	-688(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L558
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L587
	call	_ZdlPv@PLT
.L587:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L558:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L643
	movq	-816(%rbp), %rax
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L635:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-712(%rbp), %rdi
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -712(%rbp)
	testq	%rdi, %rdi
	je	.L559
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-712(%rbp), %r12
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L563
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L567
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L638:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movb	$0, 16(%r13)
	movq	8(%r12), %rdi
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movq	%r13, 8(%r12)
	testq	%rdi, %rdi
	je	.L570
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r13
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L639:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-848(%rbp), %r9
	movq	%rax, %r8
	leaq	16(%rax), %rax
	movb	$0, 16(%r8)
	movq	8(%r12), %rdi
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	movq	$0, 8(%r8)
	movq	%r8, 8(%r12)
	je	.L573
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r8
	movq	-848(%rbp), %r9
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L640:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-848(%rbp), %r9
	movq	%rax, %r8
	leaq	16(%rax), %rax
	movb	$0, 16(%r8)
	movq	8(%r12), %rdi
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	movq	$0, 8(%r8)
	movq	%r8, 8(%r12)
	je	.L576
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r8
	movq	-848(%rbp), %r9
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L642:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	16(%rax), %rax
	movb	$0, 16(%r14)
	movq	8(%r12), %rdi
	movq	%rax, (%r14)
	movq	$0, 8(%r14)
	movq	%r14, 8(%r12)
	testq	%rdi, %rdi
	je	.L581
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r14
	jmp	.L581
.L564:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L643:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9695:
	.size	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_, .-_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	.section	.rodata.str1.1
.LC30:
	.string	"!="
.LC31:
	.string	"pointer"
.LC32:
	.string	"nullptr"
	.text
	.align 2
	.p2align 4
	.globl	_ZN20UtilTest_Malloc_Test8TestBodyEv
	.type	_ZN20UtilTest_Malloc_Test8TestBodyEv, @function
_ZN20UtilTest_Malloc_Test8TestBodyEv:
.LFB8194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	malloc@PLT
	testq	%rax, %rax
	je	.L710
	movq	%rax, -88(%rbp)
	movq	$0, -72(%rbp)
.L672:
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L711
.L648:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L651:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L712
.L652:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L713
.L653:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L656
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L656:
	movq	%r12, %rdi
	call	free@PLT
	movl	$1, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L714
	movq	%rax, -88(%rbp)
	movq	$0, -72(%rbp)
.L671:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L659:
	cmpb	$0, -64(%rbp)
	je	.L715
.L660:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L663
.L719:
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L663:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L716
.L664:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L717
.L666:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L669
.L720:
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L669:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L718
	addq	$64, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L661
	movq	(%rax), %r8
.L661:
	leaq	-80(%rbp), %r12
	movl	$104, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L660
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L719
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L717:
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L667
	movq	(%rax), %r8
.L667:
	leaq	-80(%rbp), %r13
	movl	$105, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L666
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L720
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L710:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L672
	leaq	-64(%rbp), %r13
	leaq	-72(%rbp), %rcx
	leaq	.LC30(%rip), %r9
	leaq	-88(%rbp), %r8
	movq	%r13, %rdi
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	cmpb	$0, -64(%rbp)
	jne	.L648
.L711:
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L649
	movq	(%rax), %r8
.L649:
	leaq	-80(%rbp), %r12
	movl	$102, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L648
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L654
	movq	(%rax), %r8
.L654:
	leaq	-80(%rbp), %r15
	movl	$103, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L653
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L714:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L671
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r13, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L712:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L652
.L665:
	leaq	_ZZN4node6MallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L716:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L665
	jmp	.L664
.L718:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8194:
	.size	_ZN20UtilTest_Malloc_Test8TestBodyEv, .-_ZN20UtilTest_Malloc_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN20UtilTest_Calloc_Test8TestBodyEv
	.type	_ZN20UtilTest_Calloc_Test8TestBodyEv, @function
_ZN20UtilTest_Calloc_Test8TestBodyEv:
.LFB8201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	calloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L722
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L723:
	cmpb	$0, -64(%rbp)
	je	.L782
.L724:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L727
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L727:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L739
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L783
.L729:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L732
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L732:
	movq	%r12, %rdi
	call	free@PLT
	movl	$1, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L733
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L734:
	cmpb	$0, -64(%rbp)
	je	.L784
.L735:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L738
.L787:
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L738:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L739
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -64(%rbp)
	je	.L785
.L740:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L743
.L788:
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L743:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L786
	addq	$64, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L736
	movq	(%rax), %r8
.L736:
	leaq	-80(%rbp), %r12
	movl	$112, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L735
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L787
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L785:
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L741
	movq	(%rax), %r8
.L741:
	leaq	-80(%rbp), %r13
	movl	$113, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L740
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L788
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L782:
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L725
	movq	(%rax), %r8
.L725:
	leaq	-80(%rbp), %r12
	movl	$110, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L724
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r13, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L783:
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L730
	movq	(%rax), %r8
.L730:
	leaq	-80(%rbp), %r15
	movl	$111, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L729
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L733:
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r13, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L739:
	leaq	_ZZN4node6CallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L786:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8201:
	.size	_ZN20UtilTest_Calloc_Test8TestBodyEv, .-_ZN20UtilTest_Calloc_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN29UtilTest_UncheckedMalloc_Test8TestBodyEv
	.type	_ZN29UtilTest_UncheckedMalloc_Test8TestBodyEv, @function
_ZN29UtilTest_UncheckedMalloc_Test8TestBodyEv:
.LFB8208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	malloc@PLT
	testq	%rax, %rax
	je	.L857
	movq	%rax, -88(%rbp)
	movq	$0, -72(%rbp)
.L821:
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L792:
	cmpb	$0, -64(%rbp)
	je	.L858
.L793:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L796
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L796:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L859
	movq	%rax, -88(%rbp)
	movq	$0, -72(%rbp)
.L820:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L799:
	cmpb	$0, -64(%rbp)
	je	.L860
.L800:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L803
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L803:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L861
	movq	%rax, -88(%rbp)
	movq	$0, -72(%rbp)
.L819:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L806:
	cmpb	$0, -64(%rbp)
	je	.L862
.L807:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L810
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L810:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L863
	movq	%rax, -88(%rbp)
	movq	$0, -72(%rbp)
.L818:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L813:
	cmpb	$0, -64(%rbp)
	je	.L864
.L814:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L817
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L817:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L865
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L858:
	.cfi_restore_state
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L794
	movq	(%rax), %r8
.L794:
	leaq	-80(%rbp), %r14
	movl	$118, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L793
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L864:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L815
	movq	(%rax), %r8
.L815:
	leaq	-80(%rbp), %r12
	movl	$121, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L814
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L808
	movq	(%rax), %r8
.L808:
	leaq	-80(%rbp), %r14
	movl	$120, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L807
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L860:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L801
	movq	(%rax), %r8
.L801:
	leaq	-80(%rbp), %r14
	movl	$119, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L800
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L859:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L820
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L857:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L821
	leaq	-64(%rbp), %r12
	leaq	-72(%rbp), %rcx
	leaq	.LC30(%rip), %r9
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L863:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L818
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L861:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	jne	.L819
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L806
.L865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8208:
	.size	_ZN29UtilTest_UncheckedMalloc_Test8TestBodyEv, .-_ZN29UtilTest_UncheckedMalloc_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN29UtilTest_UncheckedCalloc_Test8TestBodyEv
	.type	_ZN29UtilTest_UncheckedCalloc_Test8TestBodyEv, @function
_ZN29UtilTest_UncheckedCalloc_Test8TestBodyEv:
.LFB8215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	calloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L867
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L868:
	cmpb	$0, -64(%rbp)
	je	.L925
.L869:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L872
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L872:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L873
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L874:
	cmpb	$0, -64(%rbp)
	je	.L926
.L875:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L878
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L878:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L879
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L880:
	cmpb	$0, -64(%rbp)
	je	.L927
.L881:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L884
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L884:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$1, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	$0, -72(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L885
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L886:
	cmpb	$0, -64(%rbp)
	je	.L928
.L887:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L890
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L890:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L929
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	.cfi_restore_state
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L870
	movq	(%rax), %r8
.L870:
	leaq	-80(%rbp), %r14
	movl	$126, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L869
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L928:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L888
	movq	(%rax), %r8
.L888:
	leaq	-80(%rbp), %r12
	movl	$129, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L887
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L927:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L882
	movq	(%rax), %r8
.L882:
	leaq	-80(%rbp), %r14
	movl	$128, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L881
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L926:
	leaq	-72(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L876
	movq	(%rax), %r8
.L876:
	leaq	-80(%rbp), %r14
	movl	$127, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L875
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L873:
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L867:
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L885:
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L879:
	leaq	-72(%rbp), %rcx
	leaq	-88(%rbp), %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	.LC31(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIDnPcEENS_15AssertionResultEPKcS5_RKT_RKT0_S5_
	jmp	.L880
.L929:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8215:
	.size	_ZN29UtilTest_UncheckedCalloc_Test8TestBodyEv, .-_ZN29UtilTest_UncheckedCalloc_Test8TestBodyEv
	.section	.text._ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_,"axG",@progbits,_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	.type	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_, @function
_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_:
.LFB9727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-728(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-720(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$840, %rsp
	movq	%rsi, -792(%rbp)
	movq	%rdx, -800(%rbp)
	movq	%r8, -848(%rbp)
	movq	%r9, -808(%rbp)
	movq	%rdi, -840(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing16AssertionFailureEv@PLT
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-728(%rbp), %rax
	movl	$11, %edx
	leaq	.LC24(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L1005
.L931:
	movq	%r14, %rsi
	leaq	-640(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-640(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L936
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-640(%rbp), %rdi
	leaq	-624(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L933
	call	_ZdlPv@PLT
.L933:
	movq	-728(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L934
	movq	(%rdi), %rax
	call	*8(%rax)
.L934:
	leaq	-792(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-736(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-736(%rbp), %rax
	movl	$2, %edx
	leaq	.LC25(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1006
.L935:
	movq	%r14, %rsi
	leaq	-608(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-608(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L936
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L937
	call	_ZdlPv@PLT
.L937:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L938
	movq	(%rdi), %rax
	call	*8(%rax)
.L938:
	leaq	-808(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-744(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-744(%rbp), %rax
	movl	$2, %edx
	leaq	.LC26(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1007
.L939:
	movq	%r14, %rsi
	leaq	-576(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-576(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L936
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L940
	call	_ZdlPv@PLT
.L940:
	movq	-744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L941
	movq	(%rdi), %rax
	call	*8(%rax)
.L941:
	leaq	-800(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-752(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-752(%rbp), %rax
	movl	$11, %edx
	leaq	.LC27(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1008
.L942:
	movq	%r14, %rsi
	leaq	-544(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-544(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L936
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L943
	call	_ZdlPv@PLT
.L943:
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L944
	movq	(%rdi), %rax
	call	*8(%rax)
.L944:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	leaq	-320(%rbp), %r15
	punpcklqdq	%xmm2, %xmm1
	movq	%r15, %rdi
	leaq	-448(%rbp), %r13
	movaps	%xmm1, -832(%rbp)
	leaq	-424(%rbp), %r14
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -72(%rbp)
	movw	%dx, -96(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rdx), %rcx
	movq	%rax, -432(%rbp)
	leaq	-432(%rbp), %rax
	movq	%rax, -816(%rbp)
	addq	%rax, %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rcx, %rdi
	movq	%rax, (%rcx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-832(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -864(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -872(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%rbx), %rsi
	movq	-816(%rbp), %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r14, %rsi
	leaq	-704(%rbp), %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	leaq	-760(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -856(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-760(%rbp), %rax
	movq	-696(%rbp), %rdx
	movq	-704(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1009
.L945:
	movq	%r8, %rsi
	leaq	-512(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-512(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -856(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L936
	movq	-856(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L946
	call	_ZdlPv@PLT
.L946:
	movq	-760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L947
	movq	(%rdi), %rax
	call	*8(%rax)
.L947:
	leaq	-768(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -856(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-768(%rbp), %rax
	movl	$4, %edx
	leaq	.LC29(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1010
.L948:
	movq	%r8, %rsi
	leaq	-480(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-480(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -856(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L936
	movq	-856(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L950
	movq	(%rdi), %rax
	call	*8(%rax)
.L950:
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-816(%rbp), %rbx
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%rbx, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-832(%rbp), %xmm3
	movq	-864(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	movq	-872(%rbp), %rax
	leaq	-776(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-848(%rbp), %rax
	movq	%rbx, %rdi
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r14, %rsi
	leaq	-672(%rbp), %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-776(%rbp), %rax
	movq	-664(%rbp), %rdx
	movq	-672(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1011
.L951:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-448(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r14), %rax
	cmpq	%rax, %rdx
	ja	.L936
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-448(%rbp), %rdi
	cmpq	-816(%rbp), %rdi
	je	.L952
	call	_ZdlPv@PLT
.L952:
	movq	-776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L953
	movq	(%rdi), %rax
	call	*8(%rax)
.L953:
	movq	-840(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN7testing15AssertionResultC1ERKS0_@PLT
	movq	-672(%rbp), %rdi
	leaq	-656(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movq	-704(%rbp), %rdi
	leaq	-688(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L955
	call	_ZdlPv@PLT
.L955:
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L930
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L957
	call	_ZdlPv@PLT
.L957:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L930:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1012
	movq	-840(%rbp), %rax
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-712(%rbp), %rdi
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -712(%rbp)
	testq	%rdi, %rdi
	je	.L931
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-712(%rbp), %r12
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L1006:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L935
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L1007:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L939
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movb	$0, 16(%r13)
	movq	8(%r12), %rdi
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movq	%r13, 8(%r12)
	testq	%rdi, %rdi
	je	.L942
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r13
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-856(%rbp), %r8
	movq	%rax, %rbx
	leaq	16(%rax), %rax
	movb	$0, 16(%rbx)
	movq	8(%r12), %rdi
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	movq	$0, 8(%rbx)
	movq	%rbx, 8(%r12)
	je	.L945
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L1010:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-856(%rbp), %r8
	movq	%rax, %rbx
	leaq	16(%rax), %rax
	movb	$0, 16(%rbx)
	movq	8(%r12), %rdi
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	movq	$0, 8(%rbx)
	movq	%rbx, 8(%r12)
	je	.L948
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1011:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	16(%rax), %rax
	movb	$0, 16(%r14)
	movq	8(%r12), %rdi
	movq	%rax, (%r14)
	movq	$0, 8(%r14)
	movq	%r14, 8(%r12)
	testq	%rdi, %rdi
	je	.L951
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r14
	jmp	.L951
.L936:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1012:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9727:
	.size	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_, .-_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	.section	.text._ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_,"axG",@progbits,_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	.type	_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_, @function
_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_:
.LFB9743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-728(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-720(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$840, %rsp
	movq	%rsi, -792(%rbp)
	movq	%rdx, -800(%rbp)
	movq	%r8, -848(%rbp)
	movq	%r9, -808(%rbp)
	movq	%rdi, -840(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing16AssertionFailureEv@PLT
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-728(%rbp), %rax
	movl	$11, %edx
	leaq	.LC24(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L1088
.L1014:
	movq	%r14, %rsi
	leaq	-640(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-640(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1019
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-640(%rbp), %rdi
	leaq	-624(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1016
	call	_ZdlPv@PLT
.L1016:
	movq	-728(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1017
	movq	(%rdi), %rax
	call	*8(%rax)
.L1017:
	leaq	-792(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-736(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-736(%rbp), %rax
	movl	$2, %edx
	leaq	.LC25(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1089
.L1018:
	movq	%r14, %rsi
	leaq	-608(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-608(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1019
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1020
	call	_ZdlPv@PLT
.L1020:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1021
	movq	(%rdi), %rax
	call	*8(%rax)
.L1021:
	leaq	-808(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-744(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-744(%rbp), %rax
	movl	$2, %edx
	leaq	.LC26(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1090
.L1022:
	movq	%r14, %rsi
	leaq	-576(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-576(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1019
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movq	-744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1024
	movq	(%rdi), %rax
	call	*8(%rax)
.L1024:
	leaq	-800(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-752(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-752(%rbp), %rax
	movl	$11, %edx
	leaq	.LC27(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1091
.L1025:
	movq	%r14, %rsi
	leaq	-544(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-544(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L1019
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1026
	call	_ZdlPv@PLT
.L1026:
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1027
	movq	(%rdi), %rax
	call	*8(%rax)
.L1027:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	leaq	-320(%rbp), %r15
	punpcklqdq	%xmm2, %xmm1
	movq	%r15, %rdi
	leaq	-448(%rbp), %r13
	movaps	%xmm1, -832(%rbp)
	leaq	-424(%rbp), %r14
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -72(%rbp)
	movw	%dx, -96(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rdx), %rcx
	movq	%rax, -432(%rbp)
	leaq	-432(%rbp), %rax
	movq	%rax, -816(%rbp)
	addq	%rax, %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rcx, %rdi
	movq	%rax, (%rcx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-832(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -864(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -872(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%rbx), %rsi
	movq	-816(%rbp), %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r14, %rsi
	leaq	-704(%rbp), %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	leaq	-760(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -856(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-760(%rbp), %rax
	movq	-696(%rbp), %rdx
	movq	-704(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1092
.L1028:
	movq	%r8, %rsi
	leaq	-512(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-512(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -856(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L1019
	movq	-856(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movq	-760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1030
	movq	(%rdi), %rax
	call	*8(%rax)
.L1030:
	leaq	-768(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -856(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-768(%rbp), %rax
	movl	$4, %edx
	leaq	.LC29(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1093
.L1031:
	movq	%r8, %rsi
	leaq	-480(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-480(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -856(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L1019
	movq	-856(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1032
	call	_ZdlPv@PLT
.L1032:
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1033
	movq	(%rdi), %rax
	call	*8(%rax)
.L1033:
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-816(%rbp), %rbx
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%rbx, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-832(%rbp), %xmm3
	movq	-864(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	movq	-872(%rbp), %rax
	leaq	-776(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-848(%rbp), %rax
	movq	%rbx, %rdi
	movl	(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r14, %rsi
	leaq	-672(%rbp), %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-776(%rbp), %rax
	movq	-664(%rbp), %rdx
	movq	-672(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1094
.L1034:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-448(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r14), %rax
	cmpq	%rax, %rdx
	ja	.L1019
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-448(%rbp), %rdi
	cmpq	-816(%rbp), %rdi
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movq	-776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1036
	movq	(%rdi), %rax
	call	*8(%rax)
.L1036:
	movq	-840(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN7testing15AssertionResultC1ERKS0_@PLT
	movq	-672(%rbp), %rdi
	leaq	-656(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1037
	call	_ZdlPv@PLT
.L1037:
	movq	-704(%rbp), %rdi
	leaq	-688(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1038
	call	_ZdlPv@PLT
.L1038:
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L1013
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1040
	call	_ZdlPv@PLT
.L1040:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1013:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1095
	movq	-840(%rbp), %rax
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-712(%rbp), %rdi
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -712(%rbp)
	testq	%rdi, %rdi
	je	.L1014
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-712(%rbp), %r12
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1089:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L1018
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1090:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L1022
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1091:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movb	$0, 16(%r13)
	movq	8(%r12), %rdi
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movq	%r13, 8(%r12)
	testq	%rdi, %rdi
	je	.L1025
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r13
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1092:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-856(%rbp), %r8
	movq	%rax, %rbx
	leaq	16(%rax), %rax
	movb	$0, 16(%rbx)
	movq	8(%r12), %rdi
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	movq	$0, 8(%rbx)
	movq	%rbx, 8(%r12)
	je	.L1028
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1093:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-856(%rbp), %r8
	movq	%rax, %rbx
	leaq	16(%rax), %rax
	movb	$0, 16(%rbx)
	movq	8(%r12), %rdi
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	movq	$0, 8(%rbx)
	movq	%rbx, 8(%r12)
	je	.L1031
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %rbx
	movq	-856(%rbp), %r8
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1094:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	16(%rax), %rax
	movb	$0, 16(%r14)
	movq	8(%r12), %rdi
	movq	%rax, (%r14)
	movq	$0, 8(%r14)
	movq	%r14, 8(%r12)
	testq	%rdi, %rdi
	je	.L1034
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r14
	jmp	.L1034
.L1019:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1095:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9743:
	.size	_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_, .-_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	.section	.rodata.str1.1
.LC33:
	.string	"it"
.LC34:
	.string	"list.end()"
	.text
	.p2align 4
	.type	_ZN7testing8internal18CmpHelperOpFailureIN4node8ListHeadIZN22UtilTest_ListHead_Test8TestBodyEvE4ItemXadL_ZZNS4_8TestBodyEvENS5_5node_EEEE8IteratorES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_SA_.constprop.0, @function
_ZN7testing8internal18CmpHelperOpFailureIN4node8ListHeadIZN22UtilTest_ListHead_Test8TestBodyEvE4ItemXadL_ZZNS4_8TestBodyEvENS5_5node_EEEE8IteratorES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_SA_.constprop.0:
.LFB11645:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-728(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-720(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$824, %rsp
	movq	%rdx, -840(%rbp)
	movq	%rdi, -816(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rax
	movq	%rax, -784(%rbp)
	leaq	.LC33(%rip), %rax
	movq	%rax, -792(%rbp)
	leaq	.LC34(%rip), %rax
	movq	%rax, -800(%rbp)
	call	_ZN7testing16AssertionFailureEv@PLT
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-728(%rbp), %rax
	movl	$11, %edx
	leaq	.LC24(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L1171
.L1097:
	movq	%r14, %rsi
	leaq	-640(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-640(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1102
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-640(%rbp), %rdi
	leaq	-624(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1099
	call	_ZdlPv@PLT
.L1099:
	movq	-728(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1100
	movq	(%rdi), %rax
	call	*8(%rax)
.L1100:
	leaq	-800(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-736(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-736(%rbp), %rax
	movl	$2, %edx
	leaq	.LC25(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1172
.L1101:
	movq	%r14, %rsi
	leaq	-608(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-608(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1102
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1103
	call	_ZdlPv@PLT
.L1103:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1104
	movq	(%rdi), %rax
	call	*8(%rax)
.L1104:
	leaq	-784(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-744(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-744(%rbp), %rax
	movl	$2, %edx
	leaq	.LC26(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1173
.L1105:
	movq	%r14, %rsi
	leaq	-576(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-576(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1102
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1106
	call	_ZdlPv@PLT
.L1106:
	movq	-744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1107
	movq	(%rdi), %rax
	call	*8(%rax)
.L1107:
	leaq	-792(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-752(%rbp), %r14
	call	_ZN7testing15AssertionResultlsIPKcEERS0_RKT_
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-752(%rbp), %rax
	movl	$11, %edx
	leaq	.LC27(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1174
.L1108:
	movq	%r14, %rsi
	leaq	-544(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-544(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L1102
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1109
	call	_ZdlPv@PLT
.L1109:
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1110
	movq	(%rdi), %rax
	call	*8(%rax)
.L1110:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, %xmm2
	movq	%rdx, %xmm1
	leaq	-320(%rbp), %r15
	punpcklqdq	%xmm2, %xmm1
	movq	%r15, %rdi
	leaq	-448(%rbp), %r13
	movaps	%xmm1, -832(%rbp)
	leaq	-424(%rbp), %r14
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rcx), %rdi
	movq	%rax, -432(%rbp)
	leaq	-432(%rbp), %rax
	movq	%rax, -808(%rbp)
	addq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-832(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -856(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -864(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-808(%rbp), %rdx
	movq	%rbx, %rdi
	movl	$8, %esi
	call	_ZN7testing9internal220PrintBytesInObjectToEPKhmPSo@PLT
	movq	%r14, %rsi
	leaq	-672(%rbp), %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	leaq	-760(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -848(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-760(%rbp), %rax
	movq	-664(%rbp), %rdx
	movq	-672(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rbx
	movq	-848(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1175
.L1111:
	movq	%r8, %rsi
	leaq	-512(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-512(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -848(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L1102
	movq	-848(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1112
	call	_ZdlPv@PLT
.L1112:
	movq	-760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1113
	movq	(%rdi), %rax
	call	*8(%rax)
.L1113:
	leaq	-768(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -848(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-768(%rbp), %rax
	movl	$4, %edx
	leaq	.LC29(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %rbx
	movq	-848(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1176
.L1114:
	movq	%r8, %rsi
	leaq	-480(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-480(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -848(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L1102
	movq	-848(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1115
	call	_ZdlPv@PLT
.L1115:
	movq	-768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1116
	movq	(%rdi), %rax
	call	*8(%rax)
.L1116:
	movq	%r15, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-808(%rbp), %rbx
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%rbx, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-832(%rbp), %xmm3
	movq	-856(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	movq	-864(%rbp), %rax
	leaq	-776(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-840(%rbp), %rdi
	movq	%rbx, %rdx
	movl	$8, %esi
	call	_ZN7testing9internal220PrintBytesInObjectToEPKhmPSo@PLT
	movq	%r14, %rsi
	leaq	-704(%rbp), %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-776(%rbp), %rax
	movq	-696(%rbp), %rdx
	movq	-704(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1177
.L1117:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-448(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r14), %rax
	cmpq	%rax, %rdx
	ja	.L1102
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-448(%rbp), %rdi
	cmpq	-808(%rbp), %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	movq	-776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1119
	movq	(%rdi), %rax
	call	*8(%rax)
.L1119:
	movq	-816(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN7testing15AssertionResultC1ERKS0_@PLT
	movq	-704(%rbp), %rdi
	leaq	-688(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1120
	call	_ZdlPv@PLT
.L1120:
	movq	-672(%rbp), %rdi
	leaq	-656(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1121
	call	_ZdlPv@PLT
.L1121:
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L1096
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1123
	call	_ZdlPv@PLT
.L1123:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1096:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1178
	movq	-816(%rbp), %rax
	addq	$824, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1171:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-712(%rbp), %rdi
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -712(%rbp)
	testq	%rdi, %rdi
	je	.L1097
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-712(%rbp), %r12
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1172:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L1101
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1173:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movb	$0, 16(%r12)
	movq	8(%r13), %rdi
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movq	%r12, 8(%r13)
	testq	%rdi, %rdi
	je	.L1105
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r13), %r12
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1174:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movb	$0, 16(%r13)
	movq	8(%r12), %rdi
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movq	%r13, 8(%r12)
	testq	%rdi, %rdi
	je	.L1108
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r13
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1175:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-848(%rbp), %r8
	movq	%rax, %rbx
	leaq	16(%rax), %rax
	movb	$0, 16(%rbx)
	movq	8(%r12), %rdi
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	movq	$0, 8(%rbx)
	movq	%rbx, 8(%r12)
	je	.L1111
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %rbx
	movq	-848(%rbp), %r8
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1176:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-848(%rbp), %r8
	movq	%rax, %rbx
	leaq	16(%rax), %rax
	movb	$0, 16(%rbx)
	movq	8(%r12), %rdi
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	movq	$0, 8(%rbx)
	movq	%rbx, 8(%r12)
	je	.L1114
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %rbx
	movq	-848(%rbp), %r8
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1177:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	16(%rax), %rax
	movb	$0, 16(%r14)
	movq	8(%r12), %rdi
	movq	%rax, (%r14)
	movq	$0, 8(%r14)
	movq	%r14, 8(%r12)
	testq	%rdi, %rdi
	je	.L1117
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	8(%r12), %r14
	jmp	.L1117
.L1102:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11645:
	.size	_ZN7testing8internal18CmpHelperOpFailureIN4node8ListHeadIZN22UtilTest_ListHead_Test8TestBodyEvE4ItemXadL_ZZNS4_8TestBodyEvENS5_5node_EEEE8IteratorES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_SA_.constprop.0, .-_ZN7testing8internal18CmpHelperOpFailureIN4node8ListHeadIZN22UtilTest_ListHead_Test8TestBodyEvE4ItemXadL_ZZNS4_8TestBodyEvENS5_5node_EEEE8IteratorES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_SA_.constprop.0
	.section	.rodata.str1.1
.LC35:
	.string	"*it"
.LC36:
	.string	"&one"
.LC37:
	.string	"it != list.end()"
.LC38:
	.string	"&two"
.LC39:
	.string	"list.PopFront()"
.LC40:
	.string	"one.node_.IsEmpty()"
.LC41:
	.string	"list.IsEmpty()"
.LC42:
	.string	"two.node_.IsEmpty()"
.LC43:
	.string	"list.begin() != list.end()"
	.text
	.align 2
	.p2align 4
	.globl	_ZN22UtilTest_ListHead_Test8TestBodyEv
	.type	_ZN22UtilTest_ListHead_Test8TestBodyEv, @function
_ZN22UtilTest_ListHead_Test8TestBodyEv:
.LFB8157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	movq	%r14, %rdi
	.cfi_offset 13, -40
	leaq	-168(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-160(%rbp), %rbx
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %xmm0
	movq	%rax, -200(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -112(%rbp)
	je	.L1405
.L1180:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1183
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1183:
	movq	-200(%rbp), %rax
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC35(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	movq	%rax, -128(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L1406
.L1184:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1187
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1187:
	movq	-136(%rbp), %rax
	movq	$0, -104(%rbp)
	cmpq	%rbx, %rax
	sete	-112(%rbp)
	jne	.L1407
.L1189:
	movq	-160(%rbp), %rax
	movq	%r15, -160(%rbp)
	movq	%r15, 8(%rax)
	movq	%rax, -128(%rbp)
	movq	-152(%rbp), %rax
	movq	%rbx, -120(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rbx, -168(%rbp)
	cmpq	%rbx, %rax
	je	.L1193
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L1194:
	cmpb	$0, -112(%rbp)
	leaq	-176(%rbp), %r12
	je	.L1408
.L1195:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1198
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1198:
	movq	-184(%rbp), %rax
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	leaq	.LC35(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	movq	%rax, -168(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -176(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L1409
.L1199:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1202
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1202:
	movq	-184(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -184(%rbp)
	cmpq	%rbx, %rax
	je	.L1203
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L1204:
	cmpb	$0, -112(%rbp)
	je	.L1410
.L1205:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1208
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1208:
	movq	-184(%rbp), %rax
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	leaq	.LC35(%rip), %rdx
	leaq	.LC38(%rip), %rsi
	movq	%r15, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L1411
.L1209:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1212
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1212:
	movq	-184(%rbp), %rax
	movq	8(%rax), %rax
	movq	$0, -104(%rbp)
	cmpq	%rbx, %rax
	movq	%rax, -184(%rbp)
	sete	-112(%rbp)
	jne	.L1412
.L1214:
	cmpq	%rbx, -160(%rbp)
	je	.L1277
	movq	-152(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 8(%rcx)
	movq	%rcx, (%rdx)
	movups	%xmm0, (%rax)
.L1218:
	movq	%rax, -168(%rbp)
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	-200(%rbp), %rax
	leaq	.LC39(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L1413
.L1219:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1222
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1222:
	movq	-144(%rbp), %rax
	movq	-200(%rbp), %rsi
	movq	$0, -104(%rbp)
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L1414
.L1224:
	movq	-160(%rbp), %rax
	movq	$0, -104(%rbp)
	cmpq	%rbx, %rax
	setne	-112(%rbp)
	je	.L1415
.L1229:
	movq	-152(%rbp), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -184(%rbp)
	cmpq	%rbx, %rax
	je	.L1233
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L1234:
	cmpb	$0, -112(%rbp)
	je	.L1416
.L1235:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1238
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1238:
	movq	-184(%rbp), %rax
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	leaq	.LC35(%rip), %rdx
	leaq	.LC38(%rip), %rsi
	movq	%r15, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L1417
.L1239:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1242
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1242:
	movq	-184(%rbp), %rax
	movq	8(%rax), %rax
	movq	$0, -104(%rbp)
	cmpq	%rbx, %rax
	movq	%rax, -184(%rbp)
	sete	-112(%rbp)
	jne	.L1418
.L1244:
	cmpq	%rbx, -160(%rbp)
	je	.L1281
	movq	-152(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 8(%rcx)
	movq	%rcx, (%rdx)
	movups	%xmm0, (%rax)
.L1248:
	movq	%r13, %r8
	movq	%r12, %rcx
	leaq	.LC39(%rip), %rdx
	movq	%r14, %rdi
	leaq	.LC38(%rip), %rsi
	movq	%rax, -168(%rbp)
	movq	%r15, -176(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPZN22UtilTest_ListHead_Test8TestBodyEvE4ItemS4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L1419
.L1249:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1252
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1252:
	movq	-128(%rbp), %rax
	movq	$0, -104(%rbp)
	cmpq	%r15, %rax
	sete	-112(%rbp)
	jne	.L1420
.L1254:
	movq	-160(%rbp), %rax
	movq	$0, -104(%rbp)
	cmpq	%rbx, %rax
	sete	-112(%rbp)
	jne	.L1421
.L1259:
	movq	-152(%rbp), %rax
	movq	$0, -104(%rbp)
	cmpq	%rbx, %rax
	sete	-112(%rbp)
	jne	.L1422
.L1264:
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	cmpq	%rbx, -160(%rbp)
	je	.L1268
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	-152(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rcx, (%rdx)
	movq	%rax, (%rax)
	movq	%rax, 8(%rax)
	cmpq	%rbx, -160(%rbp)
	jne	.L1269
.L1268:
	movq	-152(%rbp), %rax
	movq	%rbx, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1423
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1405:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1181
	movq	(%rax), %r8
.L1181:
	leaq	-168(%rbp), %r13
	movl	$22, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1180
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC43(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$57, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1265
	call	_ZdlPv@PLT
.L1265:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1266
	movq	(%rdi), %rax
	call	*8(%rax)
.L1266:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1264
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC41(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1260
	call	_ZdlPv@PLT
.L1260:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1261
	movq	(%rdi), %rax
	call	*8(%rax)
.L1261:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1259
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC42(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1255
	call	_ZdlPv@PLT
.L1255:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1256
	movq	(%rdi), %rax
	call	*8(%rax)
.L1256:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1254
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1250
	movq	(%rax), %r8
.L1250:
	movl	$54, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1249
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$51, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1245
	call	_ZdlPv@PLT
.L1245:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1246
	movq	(%rdi), %rax
	call	*8(%rax)
.L1246:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1244
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1240
	movq	(%rax), %r8
.L1240:
	movl	$49, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1239
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1236
	movq	(%rax), %r8
.L1236:
	movl	$48, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1235
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC41(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$44, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1230
	call	_ZdlPv@PLT
.L1230:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1231
	movq	(%rdi), %rax
	call	*8(%rax)
.L1231:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1229
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$43, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1225
	call	_ZdlPv@PLT
.L1225:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1226
	movq	(%rdi), %rax
	call	*8(%rax)
.L1226:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1224
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1220
	movq	(%rax), %r8
.L1220:
	movl	$42, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1219
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$39, %ecx
	movq	%r12, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1215
	call	_ZdlPv@PLT
.L1215:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1216
	movq	(%rdi), %rax
	call	*8(%rax)
.L1216:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1214
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1210
	movq	(%rax), %r8
.L1210:
	movl	$37, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1209
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1206
	movq	(%rax), %r8
.L1206:
	movl	$36, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1205
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1200
	movq	(%rax), %r8
.L1200:
	movl	$34, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1199
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1196
	movq	(%rax), %r8
.L1196:
	leaq	-176(%rbp), %r12
	movl	$33, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1195
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$25, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1190
	call	_ZdlPv@PLT
.L1190:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1191
	movq	(%rdi), %rax
	call	*8(%rax)
.L1191:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1189
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1185
	movq	(%rax), %r8
.L1185:
	movl	$23, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1184
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1203:
	leaq	-184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperOpFailureIN4node8ListHeadIZN22UtilTest_ListHead_Test8TestBodyEvE4ItemXadL_ZZNS4_8TestBodyEvENS5_5node_EEEE8IteratorES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_SA_.constprop.0
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	-184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperOpFailureIN4node8ListHeadIZN22UtilTest_ListHead_Test8TestBodyEvE4ItemXadL_ZZNS4_8TestBodyEvENS5_5node_EEEE8IteratorES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_SA_.constprop.0
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1233:
	leaq	-184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperOpFailureIN4node8ListHeadIZN22UtilTest_ListHead_Test8TestBodyEvE4ItemXadL_ZZNS4_8TestBodyEvENS5_5node_EEEE8IteratorES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_SA_.constprop.0
	jmp	.L1234
.L1281:
	xorl	%eax, %eax
	jmp	.L1248
.L1277:
	xorl	%eax, %eax
	jmp	.L1218
.L1423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8157:
	.size	_ZN22UtilTest_ListHead_Test8TestBodyEv, .-_ZN22UtilTest_ListHead_Test8TestBodyEv
	.section	.text._ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB10155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%r8), %eax
	cmpl	%eax, (%rcx)
	je	.L1439
	movq	.LC16(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC17(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r14, %rsi
	movzbl	(%r8), %edi
	call	_ZN7testing8internal7PrintToEhPSo@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L1427
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1440
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1429:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L1430
	call	_ZdlPv@PLT
.L1430:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movl	(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L1431
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1432
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1433:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L1434
	call	_ZdlPv@PLT
.L1434:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1435
	call	_ZdlPv@PLT
.L1435:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L1424
	call	_ZdlPv@PLT
.L1424:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1441
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1440:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1439:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1432:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1431:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1427:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1429
.L1441:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10155:
	.size	_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.text._ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB10161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	(%r8), %eax
	cmpl	%eax, (%rcx)
	je	.L1457
	movq	.LC16(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC17(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r14, %rdi
	movzwl	(%r8), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L1445
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1458
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1447:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L1448
	call	_ZdlPv@PLT
.L1448:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movl	(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L1449
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1450
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1451:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L1452
	call	_ZdlPv@PLT
.L1452:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1453
	call	_ZdlPv@PLT
.L1453:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L1442
	call	_ZdlPv@PLT
.L1442:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1459
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1458:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1457:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1450:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1449:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1445:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1447
.L1459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10161:
	.size	_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.text._ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB10166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-432(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -600(%rbp)
	movq	.LC16(%rip), %xmm1
	movq	%r8, -560(%rbp)
	movq	%rdi, -552(%rbp)
	movhps	.LC17(%rip), %xmm1
	movq	%r12, %rdi
	movq	%rsi, -584(%rbp)
	movq	%rdx, -592(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -544(%rbp)
	movq	%r15, -616(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-544(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-560(%rbp), %r8
	movq	%r14, %rsi
	movzbl	(%r8), %edi
	call	_ZN7testing8internal7PrintToEhPSo@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -560(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L1461
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1473
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1463:
	movq	.LC16(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC18(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -576(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L1464
	call	_ZdlPv@PLT
.L1464:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-544(%rbp), %xmm3
	movq	-520(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-600(%rbp), %rax
	movq	%r14, %rsi
	leaq	-496(%rbp), %r14
	movzbl	(%rax), %edi
	call	_ZN7testing8internal7PrintToEhPSo@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L1465
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1466
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1467:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-576(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L1468
	call	_ZdlPv@PLT
.L1468:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rsi, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-552(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-560(%rbp), %r8
	movq	-592(%rbp), %rdx
	movq	-584(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1469
	call	_ZdlPv@PLT
.L1469:
	movq	-480(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L1460
	call	_ZdlPv@PLT
.L1460:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1474
	movq	-552(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1473:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1466:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1465:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1461:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1463
.L1474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10166:
	.size	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.rodata.str1.1
.LC45:
	.string	"buf.length()"
.LC46:
	.string	"0U"
.LC47:
	.string	">"
.LC48:
	.string	"buf.capacity()"
.LC49:
	.string	"buf.capacity() - 1"
.LC50:
	.string	"buf[buf.length() - 1]"
.LC51:
	.string	"0"
.LC52:
	.string	"buf.IsAllocated()"
.LC53:
	.string	"buf[old_length]"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"static_cast<size_t>(old_capacity * 1.5)"
	.text
	.p2align 4
	.type	_Z21MaybeStackBufferBasicIhEvv, @function
_Z21MaybeStackBufferBasicIhEvv:
.LFB9091:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC45(%rip), %rdx
	leaq	.LC46(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1152(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1168(%rbp), %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1160(%rbp), %r12
	movq	%r13, %rcx
	pushq	%rbx
	movq	%r12, %r8
	subq	$1144, %rsp
	.cfi_offset 3, -56
	movdqa	.LC44(%rip), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1080(%rbp), %rax
	movb	$0, -1080(%rbp)
	movq	%rax, -1176(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1160(%rbp)
	movl	$0, -1168(%rbp)
	movaps	%xmm2, -1104(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1868
.L1476:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1479
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1479:
	movq	-1088(%rbp), %rax
	cmpq	-1176(%rbp), %rax
	je	.L1482
	testq	%rax, %rax
	je	.L1482
	movq	%r12, %rdi
	movb	$0, -1152(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1136(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1136(%rbp), %r8
	movl	$142, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1644
	call	_ZdlPv@PLT
.L1644:
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1483
	movq	(%rdi), %rax
	call	*8(%rax)
.L1483:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1482
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1482:
	movq	-1104(%rbp), %rdx
	movq	-1096(%rbp), %rax
	movq	%rdx, -1160(%rbp)
	movq	%rax, -1168(%rbp)
	cmpq	%rax, %rdx
	jnb	.L1484
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	je	.L1869
.L1486:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1489
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1489:
	movq	-1096(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -1104(%rbp)
	movq	%rax, -1160(%rbp)
	movq	%rax, -1168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1870
.L1490:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1493
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1493:
	movq	-1088(%rbp), %rax
	testq	%rax, %rax
	je	.L1496
	cmpq	-1176(%rbp), %rax
	je	.L1496
	movq	%r12, %rdi
	movb	$0, -1152(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1136(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1136(%rbp), %r8
	movl	$148, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1642
	call	_ZdlPv@PLT
.L1642:
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1497
	movq	(%rdi), %rax
	call	*8(%rax)
.L1497:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1496
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1496:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	je	.L1871
.L1498:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1501
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1501:
	cmpq	$0, -1104(%rbp)
	je	.L1527
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	je	.L1872
.L1503:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1506
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1506:
	xorl	%eax, %eax
	cmpq	$0, -1104(%rbp)
	je	.L1511
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	-1088(%rbp), %rdx
	movb	%al, (%rdx,%rax)
	movq	-1104(%rbp), %rdx
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L1507
	testq	%rdx, %rdx
	je	.L1511
	xorl	%r15d, %r15d
	leaq	.LC21(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	-1088(%rbp), %r8
	movb	%r15b, -1160(%rbp)
	addq	%r15, %r8
	cmpb	%r15b, (%r8)
	je	.L1873
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1874
.L1515:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1518
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %r15
	cmpq	%r15, -1104(%rbp)
	ja	.L1510
.L1511:
	movq	-1096(%rbp), %rax
	leaq	-1(%rax), %rdx
	cmpq	%rax, %rdx
	ja	.L1508
	movq	%rdx, -1104(%rbp)
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-1088(%rbp), %rdx
	leaq	.LC49(%rip), %rsi
	movb	$0, -1(%rdx,%rax)
	movq	-1104(%rbp), %rax
	leaq	.LC45(%rip), %rdx
	movq	%rax, -1160(%rbp)
	movq	-1096(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -1168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1875
.L1520:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1523
.L1879:
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1523:
	xorl	%r15d, %r15d
	cmpq	$0, -1104(%rbp)
	leaq	.LC21(%rip), %rbx
	je	.L1536
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	-1088(%rbp), %r8
	movb	%r15b, -1160(%rbp)
	addq	%r15, %r8
	cmpb	%r15b, (%r8)
	je	.L1876
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1877
.L1531:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1534
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %r15
	cmpq	%r15, -1104(%rbp)
	ja	.L1524
.L1536:
	movq	-1096(%rbp), %r8
	movq	%r8, -1104(%rbp)
	addq	$-1, %r8
	jc	.L1878
.L1527:
	leaq	_ZZN4node16MaybeStackBufferIhLm1024EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1518:
	addq	$1, %r15
	cmpq	-1104(%rbp), %r15
	jb	.L1510
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1534:
	addq	$1, %r15
	cmpq	-1104(%rbp), %r15
	jb	.L1524
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	jne	.L1515
.L1874:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rdx
	movq	%rbx, %r8
	testq	%rdx, %rdx
	je	.L1516
	movq	(%rdx), %r8
.L1516:
	movl	$158, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1515
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	jne	.L1531
.L1877:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rdx
	movq	%rbx, %r8
	testq	%rdx, %rdx
	je	.L1532
	movq	(%rdx), %r8
.L1532:
	movl	$164, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1531
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1484:
	leaq	.LC47(%rip), %r9
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	cmpb	$0, -1152(%rbp)
	jne	.L1486
.L1869:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1487
	movq	(%rax), %r8
.L1487:
	movl	$143, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1486
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1875:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1521
	movq	(%rax), %r8
.L1521:
	movl	$162, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1520
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1879
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1868:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1477
	movq	(%rax), %r8
.L1477:
	movl	$141, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1476
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1491
	movq	(%rax), %r8
.L1491:
	movl	$147, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1490
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1499
	movq	(%rax), %r8
.L1499:
	movl	$151, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1498
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1504
	movq	(%rax), %r8
.L1504:
	movl	$152, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1503
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1508:
	leaq	_ZZN4node16MaybeStackBufferIhLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1878:
	addq	-1088(%rbp), %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	$0, -1160(%rbp)
	leaq	.LC50(%rip), %rdx
	leaq	.LC51(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1880
	.p2align 4,,10
	.p2align 3
.L1537:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1540
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1540:
	movq	-1104(%rbp), %rax
	leaq	-1(%rax), %rbx
	movq	-1096(%rbp), %rax
	leaq	(%rax,%rax), %rsi
	leaq	-1104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1184(%rbp)
	call	_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-1104(%rbp), %rax
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -1160(%rbp)
	movq	-1096(%rbp), %rax
	movq	%rax, -1168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1881
.L1541:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1544
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1544:
	movq	-1088(%rbp), %rax
	testq	%rax, %rax
	je	.L1675
	cmpq	-1176(%rbp), %rax
	je	.L1675
.L1545:
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	je	.L1557
	.p2align 4,,10
	.p2align 3
.L1549:
	cmpq	%r15, -1104(%rbp)
	jbe	.L1527
	movq	-1088(%rbp), %r8
	movb	%r15b, -1160(%rbp)
	addq	%r15, %r8
	cmpb	%r15b, (%r8)
	je	.L1882
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1883
.L1553:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1556
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %r15
	cmpq	%r15, %rbx
	jne	.L1549
.L1557:
	cmpq	-1104(%rbp), %rbx
	jnb	.L1527
	addq	-1088(%rbp), %rbx
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	$0, -1160(%rbp)
	movq	%rbx, %r8
	leaq	.LC53(%rip), %rdx
	leaq	.LC51(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQIihEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1884
.L1559:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1562
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1562:
	xorl	%eax, %eax
	cmpq	$0, -1104(%rbp)
	je	.L1565
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	-1088(%rbp), %rdx
	movb	%al, (%rdx,%rax)
	addq	$1, %rax
	cmpq	-1104(%rbp), %rax
	jb	.L1563
.L1565:
	cmpq	$9, -1096(%rbp)
	jbe	.L1508
	movq	$10, -1104(%rbp)
	xorl	%r15d, %r15d
	leaq	.LC21(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	-1088(%rbp), %r8
	movb	%r15b, -1160(%rbp)
	addq	%r15, %r8
	cmpb	%r15b, (%r8)
	je	.L1885
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1886
.L1568:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1571
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %r15
	cmpq	%r15, -1104(%rbp)
	ja	.L1574
.L1573:
	movq	-1096(%rbp), %rbx
	movq	%rbx, -1104(%rbp)
	testq	%rbx, %rbx
	je	.L1664
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	-1088(%rbp), %r8
	movb	%r15b, -1160(%rbp)
	addq	%r15, %r8
	cmpb	%r15b, (%r8)
	je	.L1887
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1888
.L1578:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1581
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-1104(%rbp), %rbx
	addq	$1, %r15
	cmpq	%r15, %rbx
	ja	.L1584
.L1583:
	movq	-1096(%rbp), %rdx
	testq	%rdx, %rdx
	js	.L1889
.L1865:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L1586:
	mulsd	.LC54(%rip), %xmm0
	movsd	.LC55(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L1587
	cvttsd2siq	%xmm0, %r15
.L1588:
	movq	-1184(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-1104(%rbp), %rax
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -1160(%rbp)
	movq	-1096(%rbp), %rax
	movq	%rax, -1168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1890
.L1589:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1592
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1592:
	movq	-1104(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC56(%rip), %rsi
	movq	%r15, -1168(%rbp)
	movq	%rax, -1160(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1891
.L1593:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1596
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1596:
	movq	-1088(%rbp), %rax
	testq	%rax, %rax
	je	.L1676
	cmpq	-1176(%rbp), %rax
	je	.L1676
.L1597:
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	je	.L1612
	.p2align 4,,10
	.p2align 3
.L1601:
	cmpq	-1104(%rbp), %r15
	jnb	.L1527
	movq	-1088(%rbp), %r8
	movb	%r15b, -1160(%rbp)
	addq	%r15, %r8
	cmpb	(%r8), %r15b
	je	.L1892
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1893
.L1608:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1611
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %r15
	cmpq	%r15, %rbx
	jne	.L1601
.L1612:
	xorl	%eax, %eax
	cmpq	$0, -1104(%rbp)
	je	.L1615
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	-1088(%rbp), %rdx
	movb	%al, (%rdx,%rax)
	movq	-1104(%rbp), %rdx
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L1602
	testq	%rdx, %rdx
	je	.L1615
	xorl	%ebx, %ebx
	leaq	.LC21(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	-1088(%rbp), %r8
	movb	%bl, -1160(%rbp)
	addq	%rbx, %r8
	cmpb	%bl, (%r8)
	je	.L1894
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1895
.L1619:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1622
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	cmpq	%rbx, -1104(%rbp)
	ja	.L1614
.L1615:
	movq	-1088(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1605
	cmpq	-1176(%rbp), %rbx
	je	.L1605
	movq	-1176(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movdqa	.LC44(%rip), %xmm3
	leaq	.LC45(%rip), %rdx
	leaq	.LC46(%rip), %rsi
	movq	$0, -1160(%rbp)
	movq	%rax, -1088(%rbp)
	movl	$0, -1168(%rbp)
	movaps	%xmm3, -1104(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -1152(%rbp)
	je	.L1896
.L1624:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1627
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1627:
	movq	-1088(%rbp), %rax
	cmpq	-1176(%rbp), %rax
	je	.L1630
	testq	%rax, %rax
	je	.L1630
	movq	%r12, %rdi
	movb	$0, -1152(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1136(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1136(%rbp), %r8
	movl	$208, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1640
	call	_ZdlPv@PLT
.L1640:
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1631
	movq	(%rdi), %rax
	call	*8(%rax)
.L1631:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1630
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1630:
	movq	-1104(%rbp), %rdx
	movq	-1096(%rbp), %rax
	movq	%rdx, -1160(%rbp)
	movq	%rax, -1168(%rbp)
	cmpq	%rax, %rdx
	jnb	.L1632
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L1633:
	cmpb	$0, -1152(%rbp)
	je	.L1897
.L1634:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1637
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1637:
	movq	%rbx, %rdi
	call	free@PLT
	movq	-1088(%rbp), %rdi
	cmpq	-1176(%rbp), %rdi
	je	.L1475
	testq	%rdi, %rdi
	je	.L1475
	call	free@PLT
.L1475:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1898
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1556:
	.cfi_restore_state
	addq	$1, %r15
	cmpq	%r15, %rbx
	jne	.L1549
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	jne	.L1553
.L1883:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rdx
	leaq	.LC21(%rip), %r8
	testq	%rdx, %rdx
	je	.L1554
	movq	(%rdx), %r8
.L1554:
	movl	$175, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1553
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	-1104(%rbp), %rbx
	addq	$1, %r15
	cmpq	%rbx, %r15
	jb	.L1584
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1571:
	addq	$1, %r15
	cmpq	-1104(%rbp), %r15
	jb	.L1574
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1887:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	jne	.L1578
.L1888:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rdx
	leaq	.LC21(%rip), %r8
	testq	%rdx, %rdx
	je	.L1579
	movq	(%rdx), %r8
.L1579:
	movl	$186, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1578
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1885:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	jne	.L1568
.L1886:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rdx
	movq	%rbx, %r8
	testq	%rdx, %rdx
	je	.L1569
	movq	(%rdx), %r8
.L1569:
	movl	$183, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1568
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1611:
	addq	$1, %r15
	cmpq	%rbx, %r15
	jne	.L1601
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	jne	.L1608
.L1893:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rdx
	leaq	.LC21(%rip), %r8
	testq	%rdx, %rdx
	je	.L1609
	movq	(%rdx), %r8
.L1609:
	movl	$196, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1608
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1675:
	movq	%r12, %rdi
	movb	$0, -1152(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1136(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1136(%rbp), %r8
	movl	$173, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1547
	call	_ZdlPv@PLT
.L1547:
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1548
	movq	(%rdi), %rax
	call	*8(%rax)
.L1548:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1545
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1622:
	addq	$1, %rbx
	cmpq	-1104(%rbp), %rbx
	jb	.L1614
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1881:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1542
	movq	(%rax), %r8
.L1542:
	movl	$172, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1541
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1541
.L1880:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1538
	movq	(%rax), %r8
.L1538:
	movl	$166, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1537
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1894:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -1152(%rbp)
	jne	.L1619
.L1895:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rdx
	movq	%r15, %r8
	testq	%rdx, %rdx
	je	.L1620
	movq	(%rdx), %r8
.L1620:
	movl	$202, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1619
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	%r12, %rdi
	movb	$0, -1152(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1136(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1136(%rbp), %r8
	movl	$194, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1599
	call	_ZdlPv@PLT
.L1599:
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1600
	movq	(%rdi), %rax
	call	*8(%rax)
.L1600:
	movq	-1144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1597
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1587:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %r15
	btcq	$63, %r15
	jmp	.L1588
.L1889:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1586
.L1884:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1560
	movq	(%rax), %r8
.L1560:
	movl	$176, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1559
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1559
.L1891:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1594
	movq	(%rax), %r8
.L1594:
	movl	$193, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1593
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1593
.L1890:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1590
	movq	(%rax), %r8
.L1590:
	movl	$192, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1589
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1589
.L1605:
	leaq	_ZZN4node16MaybeStackBufferIhLm1024EE7ReleaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1632:
	leaq	.LC47(%rip), %r9
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	jmp	.L1633
.L1896:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1625
	movq	(%rax), %r8
.L1625:
	movl	$207, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1624
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1624
.L1897:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1144(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1635
	movq	(%rax), %r8
.L1635:
	movl	$209, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1634
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1634
.L1664:
	xorl	%edx, %edx
	jmp	.L1865
.L1898:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9091:
	.size	_Z21MaybeStackBufferBasicIhEvv, .-_Z21MaybeStackBufferBasicIhEvv
	.section	.rodata.str1.1
.LC62:
	.string	"100U"
.LC63:
	.string	"bigbuf.length()"
.LC64:
	.string	"10000U"
.LC65:
	.string	"static_cast<unsigned char>(i)"
.LC66:
	.string	"bigbuf.IsAllocated()"
.LC67:
	.string	"bigbuf.capacity()"
.LC68:
	.string	"bigbuf[i]"
	.text
	.align 2
	.p2align 4
	.globl	_ZN30UtilTest_MaybeStackBuffer_Test8TestBodyEv
	.type	_ZN30UtilTest_MaybeStackBuffer_Test8TestBodyEv, @function
_ZN30UtilTest_MaybeStackBuffer_Test8TestBodyEv:
.LFB8223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3232(%rbp), %r14
	leaq	-2104(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3248(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3240(%rbp), %r12
	pushq	%rbx
	subq	$3240, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Z21MaybeStackBufferBasicIhEvv
	xorl	%esi, %esi
	movq	%r12, %r8
	movq	%r13, %rcx
	movdqa	.LC44(%rip), %xmm4
	movw	%si, -2104(%rbp)
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC46(%rip), %rsi
	movq	%r15, -2112(%rbp)
	movq	$0, -3240(%rbp)
	movl	$0, -3248(%rbp)
	movaps	%xmm4, -2128(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2483
.L1900:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1903
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1903:
	movq	-2112(%rbp), %rax
	cmpq	%r15, %rax
	je	.L1906
	testq	%rax, %rax
	je	.L1906
	movq	%r12, %rdi
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-3184(%rbp), %r10
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	movq	%r10, %rdi
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-3184(%rbp), %r8
	movl	$142, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3184(%rbp), %rdi
	leaq	-3168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2141
	call	_ZdlPv@PLT
.L2141:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1907
	movq	(%rdi), %rax
	call	*8(%rax)
.L1907:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1906
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1906:
	movq	-2128(%rbp), %rdx
	movq	-2120(%rbp), %rax
	movq	%rdx, -3240(%rbp)
	movq	%rax, -3248(%rbp)
	cmpq	%rax, %rdx
	jnb	.L1908
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -3232(%rbp)
	je	.L2484
.L1910:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1913
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1913:
	movq	-2120(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -2128(%rbp)
	movq	%rax, -3240(%rbp)
	movq	%rax, -3248(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2485
.L1914:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1917
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1917:
	movq	-2112(%rbp), %rax
	testq	%rax, %rax
	je	.L1920
	cmpq	%r15, %rax
	je	.L1920
	movq	%r12, %rdi
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-3184(%rbp), %r10
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	movq	%r10, %rdi
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-3184(%rbp), %r8
	movl	$148, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3184(%rbp), %rdi
	leaq	-3168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2139
	call	_ZdlPv@PLT
.L2139:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1921
	movq	(%rdi), %rax
	call	*8(%rax)
.L1921:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1920
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1920:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -3232(%rbp)
	je	.L2486
.L1922:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1925
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1925:
	cmpq	$0, -2128(%rbp)
	je	.L1949
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -3232(%rbp)
	je	.L2487
.L1927:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1930
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1930:
	movq	-2128(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1931
	leaq	-1(%rcx), %rdx
	movq	-2112(%rbp), %rax
	cmpq	$6, %rdx
	jbe	.L2148
	movq	%rcx, %rsi
	movdqa	.LC57(%rip), %xmm3
	movdqa	.LC58(%rip), %xmm7
	movq	%rax, %rdx
	shrq	$3, %rsi
	movdqa	.LC59(%rip), %xmm6
	movdqa	.LC60(%rip), %xmm5
	salq	$4, %rsi
	movdqa	.LC61(%rip), %xmm4
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L1933:
	movdqa	%xmm3, %xmm1
	addq	$16, %rdx
	paddq	%xmm7, %xmm3
	movdqa	%xmm1, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm6, %xmm0
	shufps	$136, %xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm4, %xmm1
	paddq	%xmm5, %xmm2
	shufps	$136, %xmm1, %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rsi
	jne	.L1933
	movq	%rcx, %rdx
	andq	$-8, %rdx
	testb	$7, %cl
	je	.L1934
.L1932:
	leaq	1(%rdx), %rsi
	movw	%dx, (%rax,%rdx,2)
	cmpq	%rsi, %rcx
	jbe	.L1934
	movw	%si, (%rax,%rsi,2)
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1934
	movw	%si, (%rax,%rsi,2)
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1934
	movw	%si, (%rax,%rsi,2)
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1934
	movw	%si, (%rax,%rsi,2)
	leaq	5(%rdx), %rsi
	cmpq	%rsi, %rcx
	jbe	.L1934
	addq	$6, %rdx
	movw	%si, (%rax,%rsi,2)
	cmpq	%rdx, %rcx
	jbe	.L1934
	movw	%dx, (%rax,%rdx,2)
.L1934:
	xorl	%ebx, %ebx
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L2489:
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	cmpq	%rbx, -2128(%rbp)
	jbe	.L1931
.L1939:
	movq	-2112(%rbp), %rax
.L1940:
	leaq	(%rax,%rbx,2), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -3240(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	cmpb	$0, -3232(%rbp)
	je	.L2488
.L1935:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2489
	addq	$1, %rbx
	cmpq	%rbx, -2128(%rbp)
	ja	.L1939
.L1931:
	movq	-2120(%rbp), %rdx
	leaq	-1(%rdx), %rax
	cmpq	%rax, %rdx
	jb	.L1989
	movq	-2112(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	%rax, -2128(%rbp)
	leaq	.LC49(%rip), %rsi
	movw	%cx, (%rdx,%rax,2)
	movq	%r13, %rcx
	leaq	.LC45(%rip), %rdx
	movq	%rax, -3240(%rbp)
	movq	%rax, -3248(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2490
.L1942:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1945
.L2493:
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1945:
	xorl	%ebx, %ebx
	cmpq	$0, -2128(%rbp)
	je	.L1956
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	-2112(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -3240(%rbp)
	leaq	(%rax,%rbx,2), %rdx
	call	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	cmpb	$0, -3232(%rbp)
	je	.L2491
.L1951:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1954
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	cmpq	%rbx, -2128(%rbp)
	ja	.L1946
.L1956:
	movq	-2120(%rbp), %rax
	movq	%rax, -2128(%rbp)
	addq	$-1, %rax
	jc	.L2492
.L1949:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1954:
	addq	$1, %rbx
	cmpq	%rbx, -2128(%rbp)
	ja	.L1946
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1936
	movq	(%rax), %r8
.L1936:
	movl	$158, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1935
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1952
	movq	(%rax), %r8
.L1952:
	movl	$164, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1951
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1908:
	leaq	.LC47(%rip), %r9
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	cmpb	$0, -3232(%rbp)
	jne	.L1910
.L2484:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1911
	movq	(%rax), %r8
.L1911:
	movl	$143, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1910
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1943
	movq	(%rax), %r8
.L1943:
	movl	$162, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1942
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2493
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L2483:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1901
	movq	(%rax), %r8
.L1901:
	movl	$141, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1900
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1915
	movq	(%rax), %r8
.L1915:
	movl	$147, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1914
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L2486:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1923
	movq	(%rax), %r8
.L1923:
	movl	$151, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1922
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1928
	movq	(%rax), %r8
.L1928:
	movl	$152, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1927
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1989:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2148:
	xorl	%edx, %edx
	jmp	.L1932
.L2492:
	movq	-2112(%rbp), %rdx
	movq	%r12, %rcx
	leaq	.LC51(%rip), %rsi
	movq	%r14, %rdi
	movl	$0, -3240(%rbp)
	leaq	(%rdx,%rax,2), %r8
	leaq	.LC50(%rip), %rdx
	call	_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2494
	.p2align 4,,10
	.p2align 3
.L1957:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1960
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1960:
	movq	-2128(%rbp), %rax
	movq	-2120(%rbp), %rsi
	movq	-2112(%rbp), %rdi
	subq	$1, %rax
	leaq	(%rsi,%rsi), %rbx
	movq	%rax, -3256(%rbp)
	testq	%rdi, %rdi
	je	.L2495
	cmpq	%rbx, %rsi
	jnb	.L1962
	movb	$1, -3264(%rbp)
	cmpq	%r15, %rdi
	je	.L2496
.L1963:
	salq	$2, %rsi
	testq	%rbx, %rbx
	js	.L2497
	testq	%rsi, %rsi
	je	.L2498
	movq	%rsi, -3280(%rbp)
	movq	%rdi, -3272(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2499
.L1967:
	cmpb	$0, -3264(%rbp)
	movq	%rax, -2112(%rbp)
	movq	%rbx, %rsi
	movq	%rbx, -2120(%rbp)
	jne	.L1962
	movq	-2128(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L2500
	.p2align 4,,10
	.p2align 3
.L1962:
	movq	%rsi, -3248(%rbp)
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rbx, -2128(%rbp)
	movq	%rbx, -3240(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2501
.L1968:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1971
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1971:
	movq	-2112(%rbp), %rax
	cmpq	%r15, %rax
	je	.L2186
	testq	%rax, %rax
	je	.L2186
.L1972:
	xorl	%ebx, %ebx
	cmpq	$0, -3256(%rbp)
	je	.L1982
	.p2align 4,,10
	.p2align 3
.L1976:
	cmpq	%rbx, -2128(%rbp)
	jbe	.L1949
	movq	-2112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -3240(%rbp)
	leaq	(%rdx,%rbx,2), %rdx
	call	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	cmpb	$0, -3232(%rbp)
	je	.L2502
.L1978:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1981
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	cmpq	%rbx, -3256(%rbp)
	jne	.L1976
.L1982:
	movq	-3256(%rbp), %rax
	cmpq	-2128(%rbp), %rax
	jnb	.L1949
	movq	%rax, %rcx
	movq	-2112(%rbp), %rax
	leaq	.LC53(%rip), %rdx
	movq	%r14, %rdi
	leaq	.LC51(%rip), %rsi
	movl	$0, -3240(%rbp)
	leaq	(%rax,%rcx,2), %r8
	movq	%r12, %rcx
	call	_ZN7testing8internal11CmpHelperEQIitEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2503
.L1984:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1987
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1987:
	movq	-2128(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1994
	leaq	-1(%rcx), %rax
	movq	-2112(%rbp), %rsi
	cmpq	$6, %rax
	jbe	.L2161
	movq	%rcx, %rdx
	movdqa	.LC57(%rip), %xmm3
	movdqa	.LC58(%rip), %xmm7
	movq	%rsi, %rax
	shrq	$3, %rdx
	movdqa	.LC59(%rip), %xmm6
	movdqa	.LC60(%rip), %xmm5
	salq	$4, %rdx
	movdqa	.LC61(%rip), %xmm4
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L1992:
	movdqa	%xmm3, %xmm1
	addq	$16, %rax
	paddq	%xmm7, %xmm3
	movdqa	%xmm1, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm6, %xmm0
	shufps	$136, %xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm4, %xmm1
	paddq	%xmm5, %xmm2
	shufps	$136, %xmm1, %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L1992
	movq	%rcx, %rax
	andq	$-8, %rax
	testb	$7, %cl
	je	.L1994
.L1991:
	leaq	1(%rax), %rdx
	movw	%ax, (%rsi,%rax,2)
	cmpq	%rdx, %rcx
	jbe	.L1994
	movw	%dx, (%rsi,%rdx,2)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1994
	movw	%dx, (%rsi,%rdx,2)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1994
	movw	%dx, (%rsi,%rdx,2)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1994
	movw	%dx, (%rsi,%rdx,2)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L1994
	addq	$6, %rax
	movw	%dx, (%rsi,%rdx,2)
	cmpq	%rax, %rcx
	jbe	.L1994
	movw	%ax, (%rsi,%rax,2)
.L1994:
	cmpq	$9, -2120(%rbp)
	jbe	.L1989
	movq	$10, -2128(%rbp)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2002:
	movq	-2112(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -3240(%rbp)
	leaq	(%rax,%rbx,2), %rdx
	call	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	cmpb	$0, -3232(%rbp)
	je	.L2504
.L1996:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1999
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	cmpq	%rbx, -2128(%rbp)
	ja	.L2002
.L2001:
	movq	-2120(%rbp), %rax
	movq	%rax, -3256(%rbp)
	movq	%rax, -2128(%rbp)
	testq	%rax, %rax
	je	.L2163
	xorl	%ebx, %ebx
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2007
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2007:
	movq	-2128(%rbp), %rax
	addq	$1, %rbx
	movq	%rax, -3256(%rbp)
	cmpq	%rbx, %rax
	jbe	.L2505
.L2010:
	movq	-2112(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -3240(%rbp)
	leaq	(%rax,%rbx,2), %rdx
	call	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	cmpb	$0, -3232(%rbp)
	jne	.L2004
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2005
	movq	(%rax), %r8
.L2005:
	movl	$186, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2004
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L1981:
	addq	$1, %rbx
	cmpq	%rbx, -3256(%rbp)
	jne	.L1976
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rdx
	leaq	.LC21(%rip), %r8
	testq	%rdx, %rdx
	je	.L1979
	movq	(%rdx), %r8
.L1979:
	movl	$175, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1978
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L1999:
	addq	$1, %rbx
	cmpq	%rbx, -2128(%rbp)
	ja	.L2002
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1997
	movq	(%rax), %r8
.L1997:
	movl	$183, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1996
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2186:
	movq	%r12, %rdi
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-3184(%rbp), %r10
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	movq	%r10, %rdi
	leaq	.LC10(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-3184(%rbp), %r8
	movl	$173, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3184(%rbp), %rdi
	leaq	-3168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1974
	call	_ZdlPv@PLT
.L1974:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1975
	movq	(%rdi), %rax
	call	*8(%rax)
.L1975:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1972
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L1972
.L2494:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1958
	movq	(%rax), %r8
.L1958:
	movl	$166, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1957
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L2501:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1969
	movq	(%rax), %r8
.L1969:
	movl	$172, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1968
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	-2120(%rbp), %rdx
	testq	%rdx, %rdx
	js	.L2506
.L2478:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L2012:
	mulsd	.LC54(%rip), %xmm0
	movsd	.LC55(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L2013
	cvttsd2siq	%xmm0, %rbx
.L2014:
	leaq	-2128(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -3264(%rbp)
	call	_ZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEm
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-2128(%rbp), %rax
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -3240(%rbp)
	movq	-2120(%rbp), %rax
	movq	%rax, -3248(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2507
.L2015:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2018
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2018:
	movq	-2128(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC56(%rip), %rsi
	movq	%rbx, -3248(%rbp)
	movq	%rax, -3240(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2508
.L2019:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2022
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2022:
	movq	-2112(%rbp), %rax
	cmpq	%r15, %rax
	je	.L2187
	testq	%rax, %rax
	je	.L2187
.L2023:
	xorl	%ebx, %ebx
	cmpq	$0, -3256(%rbp)
	je	.L2034
	.p2align 4,,10
	.p2align 3
.L2027:
	cmpq	%rbx, -2128(%rbp)
	jbe	.L1949
	movq	-2112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -3240(%rbp)
	leaq	(%rdx,%rbx,2), %rdx
	call	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	cmpb	$0, -3232(%rbp)
	je	.L2509
.L2030:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2033
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	cmpq	%rbx, -3256(%rbp)
	jne	.L2027
.L2034:
	movq	-2128(%rbp), %rcx
	movq	-2112(%rbp), %r11
	testq	%rcx, %rcx
	je	.L2029
	leaq	-1(%rcx), %rax
	cmpq	$6, %rax
	jbe	.L2169
	movq	%rcx, %rdx
	movdqa	.LC57(%rip), %xmm3
	movdqa	.LC58(%rip), %xmm7
	movq	%r11, %rax
	shrq	$3, %rdx
	movdqa	.LC59(%rip), %xmm6
	movdqa	.LC60(%rip), %xmm5
	salq	$4, %rdx
	movdqa	.LC61(%rip), %xmm4
	addq	%r11, %rdx
	.p2align 4,,10
	.p2align 3
.L2037:
	movdqa	%xmm3, %xmm1
	addq	$16, %rax
	paddq	%xmm7, %xmm3
	movdqa	%xmm1, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm6, %xmm0
	shufps	$136, %xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm4, %xmm1
	paddq	%xmm5, %xmm2
	shufps	$136, %xmm1, %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L2037
	movq	%rcx, %rax
	andq	$-8, %rax
	testb	$7, %cl
	je	.L2038
.L2036:
	leaq	1(%rax), %rdx
	movw	%ax, (%r11,%rax,2)
	cmpq	%rcx, %rdx
	jnb	.L2038
	movw	%dx, (%r11,%rdx,2)
	leaq	2(%rax), %rdx
	cmpq	%rcx, %rdx
	jnb	.L2038
	movw	%dx, (%r11,%rdx,2)
	leaq	3(%rax), %rdx
	cmpq	%rcx, %rdx
	jnb	.L2038
	movw	%dx, (%r11,%rdx,2)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2038
	movw	%dx, (%r11,%rdx,2)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2038
	addq	$6, %rax
	movw	%dx, (%r11,%rdx,2)
	cmpq	%rax, %rcx
	jbe	.L2038
	movw	%ax, (%r11,%rax,2)
.L2038:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2045:
	leaq	(%r11,%rbx,2), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -3240(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIttEENS_15AssertionResultEPKcS4_RKT_RKT0_.constprop.0
	cmpb	$0, -3232(%rbp)
	je	.L2510
.L2039:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2042
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	movq	-2112(%rbp), %r11
	cmpq	-2128(%rbp), %rbx
	jb	.L2045
.L2029:
	testq	%r11, %r11
	je	.L2188
	cmpq	%r15, %r11
	je	.L2188
	movq	%r12, %r8
	movq	%r13, %rcx
	leaq	.LC45(%rip), %rdx
	movq	%r14, %rdi
	movdqa	.LC44(%rip), %xmm5
	leaq	.LC46(%rip), %rsi
	movq	%r11, -3256(%rbp)
	movq	%r15, -2112(%rbp)
	movq	$0, -3240(%rbp)
	movl	$0, -3248(%rbp)
	movaps	%xmm5, -2128(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	movq	-3256(%rbp), %r11
	je	.L2511
.L2048:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2051
	movq	%r11, -3256(%rbp)
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-3256(%rbp), %r11
.L2051:
	movq	-2112(%rbp), %rax
	leaq	-3184(%rbp), %r10
	cmpq	%r15, %rax
	je	.L2054
	testq	%rax, %rax
	je	.L2054
	movq	%r12, %rdi
	movq	%r11, -3272(%rbp)
	movq	%r10, -3256(%rbp)
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3256(%rbp), %r10
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	movq	%r10, %rdi
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-3184(%rbp), %r8
	movl	$208, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3184(%rbp), %rdi
	leaq	-3168(%rbp), %rax
	movq	-3256(%rbp), %r10
	movq	-3272(%rbp), %r11
	cmpq	%rax, %rdi
	je	.L2137
	movq	%r10, -3272(%rbp)
	movq	%r11, -3256(%rbp)
	call	_ZdlPv@PLT
	movq	-3272(%rbp), %r10
	movq	-3256(%rbp), %r11
.L2137:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2055
	movq	(%rdi), %rax
	movq	%r10, -3272(%rbp)
	movq	%r11, -3256(%rbp)
	call	*8(%rax)
	movq	-3272(%rbp), %r10
	movq	-3256(%rbp), %r11
.L2055:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2054
	movq	%r10, -3272(%rbp)
	movq	%r11, -3256(%rbp)
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-3272(%rbp), %r10
	movq	-3256(%rbp), %r11
.L2054:
	movq	-2128(%rbp), %rdx
	movq	-2120(%rbp), %rax
	movq	%r10, -3272(%rbp)
	movq	%r11, -3256(%rbp)
	movq	%rdx, -3240(%rbp)
	movq	%rax, -3248(%rbp)
	cmpq	%rax, %rdx
	jnb	.L2056
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	movq	-3256(%rbp), %r11
	movq	-3272(%rbp), %r10
.L2057:
	cmpb	$0, -3232(%rbp)
	je	.L2512
.L2058:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2061
	movq	%r10, -3272(%rbp)
	movq	%r11, -3256(%rbp)
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	movq	-3272(%rbp), %r10
	movq	-3256(%rbp), %r11
.L2061:
	movq	%r11, %rdi
	movq	%r10, -3256(%rbp)
	call	free@PLT
	movq	-2112(%rbp), %rdi
	movq	-3256(%rbp), %r10
	testq	%rdi, %rdi
	je	.L2062
	cmpq	%r15, %rdi
	je	.L2062
	call	free@PLT
	movq	-3256(%rbp), %r10
.L2062:
	movdqa	.LC44(%rip), %xmm6
	leaq	-3160(%rbp), %rax
	movq	%r10, %rdi
	movl	$100, %esi
	movq	%rax, -3256(%rbp)
	movq	%rax, -3168(%rbp)
	movaps	%xmm6, -3184(%rbp)
	movb	$0, -3160(%rbp)
	call	_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-3184(%rbp), %rax
	leaq	.LC45(%rip), %rdx
	leaq	.LC62(%rip), %rsi
	movl	$100, -3248(%rbp)
	movq	%rax, -3240(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2513
.L2063:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2066
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2066:
	movq	-3168(%rbp), %rax
	cmpq	-3256(%rbp), %rax
	je	.L2069
	testq	%rax, %rax
	je	.L2069
	movq	%r12, %rdi
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3264(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-2128(%rbp), %r8
	movl	$223, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2128(%rbp), %rdi
	leaq	-2112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2135
	call	_ZdlPv@PLT
.L2135:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2070
	movq	(%rdi), %rax
	call	*8(%rax)
.L2070:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2069
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2069:
	movq	-3184(%rbp), %rdx
	movq	-3176(%rbp), %rax
	movq	%rdx, -3240(%rbp)
	movq	%rax, -3248(%rbp)
	cmpq	%rax, %rdx
	jnb	.L2071
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L2072:
	cmpb	$0, -3232(%rbp)
	je	.L2514
.L2073:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2076
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2076:
	movq	-3176(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -3184(%rbp)
	movq	%rax, -3240(%rbp)
	movq	%rax, -3248(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2515
.L2077:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2080
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2080:
	movq	-3168(%rbp), %rax
	cmpq	-3256(%rbp), %rax
	je	.L2083
	testq	%rax, %rax
	je	.L2083
	movq	%r12, %rdi
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3264(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC9(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-2128(%rbp), %r8
	movl	$227, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2128(%rbp), %rdi
	leaq	-2112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2133
	call	_ZdlPv@PLT
.L2133:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2084
	movq	(%rdi), %rax
	call	*8(%rax)
.L2084:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2083
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2083:
	xorl	%eax, %eax
	cmpq	$0, -3184(%rbp)
	je	.L2089
	.p2align 4,,10
	.p2align 3
.L2085:
	movq	-3168(%rbp), %rdx
	movb	%al, (%rdx,%rax)
	movq	-3184(%rbp), %rdx
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L2085
	testq	%rdx, %rdx
	je	.L2089
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	-3168(%rbp), %r8
	movb	%bl, -3240(%rbp)
	addq	%rbx, %r8
	cmpb	(%r8), %bl
	je	.L2516
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC65(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2517
.L2093:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2096
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	addq	$1, %rbx
	cmpq	-3184(%rbp), %rbx
	jb	.L2088
.L2089:
	movdqa	.LC44(%rip), %xmm7
	movq	-3264(%rbp), %rdi
	movl	$10000, %esi
	movq	%r15, -2112(%rbp)
	movb	$0, -2104(%rbp)
	movaps	%xmm7, -2128(%rbp)
	call	_ZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEm
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-2128(%rbp), %rax
	leaq	.LC63(%rip), %rdx
	leaq	.LC64(%rip), %rsi
	movl	$10000, -3248(%rbp)
	movq	%rax, -3240(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2518
.L2086:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2100
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2100:
	movq	-2112(%rbp), %rax
	cmpq	%r15, %rax
	je	.L2189
	testq	%rax, %rax
	je	.L2189
.L2101:
	movq	-2120(%rbp), %rax
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC67(%rip), %rdx
	leaq	.LC63(%rip), %rsi
	movq	%rax, -3240(%rbp)
	movq	-2128(%rbp), %rax
	movq	%rax, -3248(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2519
.L2105:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2108
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2108:
	xorl	%eax, %eax
	cmpq	$0, -2128(%rbp)
	je	.L2114
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	-2112(%rbp), %rdx
	movb	%al, (%rdx,%rax)
	movq	-2128(%rbp), %rdx
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L2109
	testq	%rdx, %rdx
	je	.L2114
	xorl	%ebx, %ebx
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2116:
	movq	%r12, %rcx
	leaq	.LC68(%rip), %rdx
	leaq	.LC65(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIhhEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2520
.L2118:
	movq	-3224(%rbp), %r8
	testq	%r8, %r8
	je	.L2121
	movq	(%r8), %rdi
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rdi
	je	.L2122
	movq	%r8, -3264(%rbp)
	call	_ZdlPv@PLT
	movq	-3264(%rbp), %r8
.L2122:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2121:
	addq	$1, %rbx
	cmpq	-2128(%rbp), %rbx
	jnb	.L2114
.L2113:
	movq	-2112(%rbp), %r8
	movb	%bl, -3240(%rbp)
	addq	%rbx, %r8
	cmpb	%bl, (%r8)
	jne	.L2116
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -3232(%rbp)
	jne	.L2118
.L2520:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rdx
	leaq	.LC21(%rip), %r8
	testq	%rdx, %rdx
	je	.L2119
	movq	(%rdx), %r8
.L2119:
	movl	$240, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2118
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2033:
	addq	$1, %rbx
	cmpq	%rbx, -3256(%rbp)
	jne	.L2027
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2509:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rdx
	leaq	.LC21(%rip), %r8
	testq	%rdx, %rdx
	je	.L2031
	movq	(%rdx), %r8
.L2031:
	movl	$196, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2030
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	-2112(%rbp), %r11
	addq	$1, %rbx
	cmpq	%rbx, -2128(%rbp)
	ja	.L2045
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2096:
	addq	$1, %rbx
	cmpq	-3184(%rbp), %rbx
	jb	.L2088
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2040
	movq	(%rax), %r8
.L2040:
	movl	$202, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2039
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -3232(%rbp)
	jne	.L2093
.L2517:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rdx
	leaq	.LC21(%rip), %r8
	testq	%rdx, %rdx
	je	.L2094
	movq	(%rdx), %r8
.L2094:
	movl	$231, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2093
	movq	(%rdi), %rdx
	call	*8(%rdx)
	jmp	.L2093
.L2187:
	movq	%r12, %rdi
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-3184(%rbp), %r10
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	movq	%r10, %rdi
	leaq	.LC10(%rip), %rcx
	leaq	.LC52(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-3184(%rbp), %r8
	movl	$194, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3184(%rbp), %rdi
	leaq	-3168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2025
	call	_ZdlPv@PLT
.L2025:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2026
	movq	(%rdi), %rax
	call	*8(%rax)
.L2026:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2023
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L2023
.L2013:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rbx
	btcq	$63, %rbx
	jmp	.L2014
.L2506:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L2012
.L2495:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2496:
	movb	$0, -3264(%rbp)
	xorl	%edi, %edi
	jmp	.L1963
.L2503:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L1985
	movq	(%rax), %r8
.L1985:
	movl	$176, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1984
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1984
.L2507:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2016
	movq	(%rax), %r8
.L2016:
	movl	$192, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2015
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2015
.L2508:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2020
	movq	(%rax), %r8
.L2020:
	movl	$193, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2019
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2019
.L2114:
	movq	-2112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2112
	testq	%rdi, %rdi
	je	.L2112
	call	free@PLT
.L2112:
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2123
	cmpq	-3256(%rbp), %rdi
	je	.L2123
	call	free@PLT
.L2123:
	pxor	%xmm0, %xmm0
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC46(%rip), %rsi
	movb	$0, -2104(%rbp)
	movq	$0, -2112(%rbp)
	movq	$0, -3240(%rbp)
	movl	$0, -3248(%rbp)
	movaps	%xmm0, -2128(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2521
.L2124:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2127
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L2127:
	movq	%r12, %r8
	movq	%r13, %rcx
	leaq	.LC48(%rip), %rdx
	movq	%r14, %rdi
	leaq	.LC46(%rip), %rsi
	movq	$0, -3240(%rbp)
	movl	$0, -3248(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIjmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -3232(%rbp)
	je	.L2522
.L2128:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1899
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L1899:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2523
	addq	$3240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2498:
	.cfi_restore_state
	call	free@PLT
.L1966:
	leaq	_ZZN4node7ReallocItEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2499:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-3280(%rbp), %rsi
	movq	-3272(%rbp), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L1967
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L2500:
	movq	%r15, %rsi
	addq	%rdx, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	-2120(%rbp), %rsi
	jmp	.L1962
.L2188:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EE7ReleaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2497:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2189:
	movq	%r12, %rdi
	movb	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-3216(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC9(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC66(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-3216(%rbp), %r8
	movl	$235, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3216(%rbp), %rdi
	leaq	-3200(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2103
	call	_ZdlPv@PLT
.L2103:
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2104
	movq	(%rdi), %rax
	call	*8(%rax)
.L2104:
	movq	-3224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2101
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
	jmp	.L2101
.L2071:
	leaq	.LC47(%rip), %r9
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	jmp	.L2072
.L2056:
	leaq	.LC47(%rip), %r9
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	leaq	.LC45(%rip), %rdx
	leaq	.LC48(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureImmEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
	movq	-3272(%rbp), %r10
	movq	-3256(%rbp), %r11
	jmp	.L2057
.L2511:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	movq	-3256(%rbp), %r11
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2049
	movq	(%rax), %r8
.L2049:
	movl	$207, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r11, -3256(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	movq	-3256(%rbp), %r11
	testq	%rdi, %rdi
	je	.L2048
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-3256(%rbp), %r11
	jmp	.L2048
.L2518:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2098
	movq	(%rax), %r8
.L2098:
	movl	$234, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2086
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2086
.L2514:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2074
	movq	(%rax), %r8
.L2074:
	movl	$224, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2073
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2073
.L2513:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2064
	movq	(%rax), %r8
.L2064:
	movl	$222, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2063
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2063
.L2512:
	movq	%r12, %rdi
	movq	%r10, -3272(%rbp)
	movq	%r11, -3256(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	movq	-3256(%rbp), %r11
	leaq	.LC21(%rip), %r8
	movq	-3272(%rbp), %r10
	testq	%rax, %rax
	je	.L2059
	movq	(%rax), %r8
.L2059:
	movl	$209, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r10, -3272(%rbp)
	movq	%r11, -3256(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	movq	-3256(%rbp), %r11
	movq	-3272(%rbp), %r10
	testq	%rdi, %rdi
	je	.L2058
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-3272(%rbp), %r10
	movq	-3256(%rbp), %r11
	jmp	.L2058
.L2163:
	xorl	%edx, %edx
	jmp	.L2478
.L2515:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2078
	movq	(%rax), %r8
.L2078:
	movl	$226, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2077
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2077
.L2519:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2106
	movq	(%rax), %r8
.L2106:
	movl	$236, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2105
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2105
.L2161:
	xorl	%eax, %eax
	jmp	.L1991
.L2522:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2129
	movq	(%rax), %r8
.L2129:
	movl	$250, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2128
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2128
.L2521:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-3224(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L2125
	movq	(%rax), %r8
.L2125:
	movl	$249, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-3240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2124
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2124
.L2169:
	xorl	%eax, %eax
	jmp	.L2036
.L2523:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8223:
	.size	_ZN30UtilTest_MaybeStackBuffer_Test8TestBodyEv, .-_ZN30UtilTest_MaybeStackBuffer_Test8TestBodyEv
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB10173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L2528
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L2526:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2528:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L2526
	.cfi_endproc
.LFE10173:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB10278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L2530
	testq	%rsi, %rsi
	je	.L2546
.L2530:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L2547
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L2533
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L2534:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2548
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2533:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L2534
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2547:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L2532:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L2534
.L2546:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10278:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB10167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	movq	8(%rsi), %rdx
	movq	%rax, (%rdi)
	movq	(%rsi), %r8
	addq	%r8, %rdx
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	8(%r12), %rsi
	movsbl	%bl, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10167:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.rodata._ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC69:
	.string	"lz"
	.section	.text.unlikely._ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L2552
	leaq	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2552:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	movq	%r12, %rsi
	leaq	-144(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-168(%rbp), %r9
.L2553:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	jne	.L2553
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r14
	jg	.L2554
	cmpb	$99, %dl
	jg	.L2555
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -184(%rbp)
	je	.L2556
	cmpb	$88, %dl
	je	.L2557
	jmp	.L2554
.L2555:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2554
	leaq	.L2559(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2559:
	.long	.L2560-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2560-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2562-.L2559
	.long	.L2561-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2560-.L2559
	.long	.L2554-.L2559
	.long	.L2560-.L2559
	.long	.L2554-.L2559
	.long	.L2554-.L2559
	.long	.L2558-.L2559
	.section	.text.unlikely._ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2556:
	movq	-168(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -192(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L2591
	jmp	.L2589
.L2554:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-128(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2589
.L2591:
	call	_ZdlPv@PLT
	jmp	.L2589
.L2560:
	cmpb	$0, (%rbx)
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rax
	cmove	%rax, %rsi
	jmp	.L2587
.L2562:
	movb	(%rbx), %al
	movb	$0, -57(%rbp)
	addl	$48, %eax
	jmp	.L2585
.L2558:
	movzbl	(%rbx), %eax
	leaq	.LC5(%rip), %rdx
	movb	$0, -57(%rbp)
	movb	(%rdx,%rax), %al
.L2585:
	movb	%al, -58(%rbp)
	leaq	-58(%rbp), %rsi
.L2587:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2582
	jmp	.L2570
.L2557:
	movzbl	(%rbx), %eax
	movq	%r10, %rdi
	leaq	-58(%rbp), %rsi
	movb	$0, -57(%rbp)
	leaq	.LC5(%rip), %rdx
	movq	%r10, -192(%rbp)
	movb	(%rdx,%rax), %al
	movb	%al, -58(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2573
	call	_ZdlPv@PLT
.L2573:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2570
.L2582:
	call	_ZdlPv@PLT
	jmp	.L2570
.L2561:
	leaq	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2570:
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2589:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2565
	call	_ZdlPv@PLT
.L2565:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2551
	call	_ZdlPv@PLT
.L2551:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2577
	call	__stack_chk_fail@PLT
.L2577:
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9731:
	.size	_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2593
	call	__stack_chk_fail@PLT
.L2593:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9118:
	.size	_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.rodata._ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC70:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L2596
	leaq	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2596:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r9
.L2597:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L2597
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L2598
	cmpb	$99, %dl
	jg	.L2599
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2600
	cmpb	$88, %dl
	je	.L2601
	jmp	.L2598
.L2599:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2598
	leaq	.L2603(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2603:
	.long	.L2602-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2602-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2602-.L2603
	.long	.L2605-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2602-.L2603
	.long	.L2598-.L2603
	.long	.L2602-.L2603
	.long	.L2598-.L2603
	.long	.L2598-.L2603
	.long	.L2602-.L2603
	.section	.text.unlikely._ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2600:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2640
	jmp	.L2638
.L2598:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2638
.L2640:
	call	_ZdlPv@PLT
	jmp	.L2638
.L2602:
	cmpq	$0, (%rbx)
	leaq	.LC10(%rip), %rax
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2633
	jmp	.L2614
.L2601:
	cmpq	$0, (%rbx)
	leaq	.LC10(%rip), %rax
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	leaq	.LC9(%rip), %rsi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2620
	call	_ZdlPv@PLT
.L2620:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2614
.L2633:
	call	_ZdlPv@PLT
	jmp	.L2614
.L2605:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC70(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L2622
	leaq	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2622:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L2614:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2638:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2609
	call	_ZdlPv@PLT
.L2609:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2595
	call	_ZdlPv@PLT
.L2595:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2625
	call	__stack_chk_fail@PLT
.L2625:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9742:
	.size	_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2642
	call	__stack_chk_fail@PLT
.L2642:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9131:
	.size	_ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L2645
	leaq	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2645:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r9
.L2646:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L2646
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L2647
	cmpb	$99, %dl
	jg	.L2648
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2649
	cmpb	$88, %dl
	je	.L2650
	jmp	.L2647
.L2648:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2647
	leaq	.L2652(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2652:
	.long	.L2651-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2651-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2651-.L2652
	.long	.L2654-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2651-.L2652
	.long	.L2647-.L2652
	.long	.L2651-.L2652
	.long	.L2647-.L2652
	.long	.L2647-.L2652
	.long	.L2651-.L2652
	.section	.text.unlikely._ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2649:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2689
	jmp	.L2687
.L2647:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2687
.L2689:
	call	_ZdlPv@PLT
	jmp	.L2687
.L2651:
	cmpq	$0, (%rbx)
	leaq	.LC10(%rip), %rax
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2682
	jmp	.L2663
.L2650:
	cmpq	$0, (%rbx)
	leaq	.LC10(%rip), %rax
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	leaq	.LC9(%rip), %rsi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2669
	call	_ZdlPv@PLT
.L2669:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2663
.L2682:
	call	_ZdlPv@PLT
	jmp	.L2663
.L2654:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC70(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L2671
	leaq	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2671:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L2663:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2687:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2658
	call	_ZdlPv@PLT
.L2658:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2644
	call	_ZdlPv@PLT
.L2644:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2674
	call	__stack_chk_fail@PLT
.L2674:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9744:
	.size	_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2691
	call	__stack_chk_fail@PLT
.L2691:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9134:
	.size	_ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L2694
	leaq	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2694:
	movq	%rax, %r8
	leaq	-160(%rbp), %r13
	movq	%r12, %rsi
	leaq	-144(%rbp), %rax
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	movq	%rax, -176(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
.L2695:
	movq	%r8, %rbx
	movq	%r12, %rdi
	leaq	1(%r8), %r8
	movsbl	1(%rbx), %esi
	movq	%r8, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r8
	testq	%rax, %rax
	jne	.L2695
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r14
	jg	.L2696
	cmpb	$99, %dl
	jg	.L2697
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r9
	movq	%rax, -184(%rbp)
	je	.L2698
	cmpb	$88, %dl
	je	.L2699
	jmp	.L2696
.L2697:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2696
	leaq	.L2701(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2701:
	.long	.L2700-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2700-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2700-.L2701
	.long	.L2703-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2700-.L2701
	.long	.L2696-.L2701
	.long	.L2700-.L2701
	.long	.L2696-.L2701
	.long	.L2696-.L2701
	.long	.L2700-.L2701
	.section	.text.unlikely._ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2698:
	movq	-168(%rbp), %rdx
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r9, -192(%rbp)
	call	_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r9
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r9
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r9, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L2729
	jmp	.L2727
.L2696:
	movq	-168(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2727
.L2729:
	call	_ZdlPv@PLT
	jmp	.L2727
.L2700:
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2722
	jmp	.L2711
.L2699:
	movq	%r9, %rdi
	leaq	.LC22(%rip), %rsi
	movq	%r9, -168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-168(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2714
	call	_ZdlPv@PLT
.L2714:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2711
.L2722:
	call	_ZdlPv@PLT
	jmp	.L2711
.L2703:
	leaq	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2711:
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2727:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2707
	call	_ZdlPv@PLT
.L2707:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2693
	call	_ZdlPv@PLT
.L2693:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2718
	call	__stack_chk_fail@PLT
.L2718:
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9748:
	.size	_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2731
	call	__stack_chk_fail@PLT
.L2731:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9141:
	.size	_ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.rodata.str1.1
.LC71:
	.string	"meow"
	.section	.text.unlikely,"ax",@progbits
	.type	_ZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9752:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L2734
	leaq	_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2734:
	movq	%rax, %r8
	leaq	-160(%rbp), %r13
	movq	%r12, %rsi
	leaq	-144(%rbp), %rax
	movq	%r8, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	movq	%rax, -176(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
.L2735:
	movq	%r8, %rbx
	movq	%r12, %rdi
	leaq	1(%r8), %r8
	movsbl	1(%rbx), %esi
	movq	%r8, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r8
	testq	%rax, %rax
	jne	.L2735
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r14
	jg	.L2736
	cmpb	$99, %dl
	jg	.L2737
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r9
	movq	%rax, -184(%rbp)
	je	.L2738
	cmpb	$88, %dl
	je	.L2739
	jmp	.L2736
.L2737:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2736
	leaq	.L2741(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2741:
	.long	.L2740-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2740-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2740-.L2741
	.long	.L2743-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2740-.L2741
	.long	.L2736-.L2741
	.long	.L2740-.L2741
	.long	.L2736-.L2741
	.long	.L2736-.L2741
	.long	.L2740-.L2741
	.section	.text.unlikely
.L2738:
	movq	-168(%rbp), %rdx
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r9, -192(%rbp)
	call	_ZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r9
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r9
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r9, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L2769
	jmp	.L2767
.L2736:
	movq	-168(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2767
.L2769:
	call	_ZdlPv@PLT
	jmp	.L2767
.L2740:
	leaq	.LC71(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2762
	jmp	.L2751
.L2739:
	movq	%r9, %rdi
	leaq	.LC71(%rip), %rsi
	movq	%r9, -168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-168(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2754
	call	_ZdlPv@PLT
.L2754:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2751
.L2762:
	call	_ZdlPv@PLT
	jmp	.L2751
.L2743:
	leaq	_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2751:
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2767:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2747
	call	_ZdlPv@PLT
.L2747:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2733
	call	_ZdlPv@PLT
.L2733:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2758
	call	__stack_chk_fail@PLT
.L2758:
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9752:
	.size	_ZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.rodata.str1.1
.LC72:
	.string	"%s"
	.section	.text.unlikely
	.type	_ZN4node7SPrintFIJZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_.constprop.0, @function
_ZN4node7SPrintFIJZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_.constprop.0:
.LFB11653:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	leaq	.LC72(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2771
	call	__stack_chk_fail@PLT
.L2771:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11653:
	.size	_ZN4node7SPrintFIJZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_.constprop.0, .-_ZN4node7SPrintFIJZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_.constprop.0
	.section	.text.unlikely._ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_:
.LFB10202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L2774
	leaq	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2774:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %r9
.L2775:
	movq	%r9, %rbx
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -192(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -184(%rbp)
	movb	%sil, -176(%rbp)
	call	strchr@PLT
	movb	-176(%rbp), %dl
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	-192(%rbp), %r8
	jne	.L2775
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r14
	jg	.L2776
	cmpb	$99, %dl
	jg	.L2777
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -176(%rbp)
	je	.L2778
	cmpb	$88, %dl
	je	.L2779
	jmp	.L2776
.L2777:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2776
	leaq	.L2781(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_,comdat
	.align 4
	.align 4
.L2781:
	.long	.L2780-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2780-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2780-.L2781
	.long	.L2783-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2780-.L2781
	.long	.L2776-.L2781
	.long	.L2780-.L2781
	.long	.L2776-.L2781
	.long	.L2776-.L2781
	.long	.L2780-.L2781
	.section	.text.unlikely._ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_,comdat
.L2778:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	call	_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_
	movq	-184(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	jne	.L2809
	jmp	.L2807
.L2776:
	movq	%r8, %rdx
	movq	%r9, %rsi
	leaq	-128(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2807
.L2809:
	call	_ZdlPv@PLT
	jmp	.L2807
.L2780:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2802
	jmp	.L2791
.L2779:
	movq	%r8, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-184(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2794
	call	_ZdlPv@PLT
.L2794:
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2791
.L2802:
	call	_ZdlPv@PLT
	jmp	.L2791
.L2783:
	leaq	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2791:
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2807:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2787
	call	_ZdlPv@PLT
.L2787:
	movq	-160(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L2773
	call	_ZdlPv@PLT
.L2773:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2798
	call	__stack_chk_fail@PLT
.L2798:
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10202:
	.size	_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_, .-_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB9902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L2811
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2811:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r9
.L2812:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L2812
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L2813
	cmpb	$99, %dl
	jg	.L2814
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2815
	cmpb	$88, %dl
	je	.L2816
	jmp	.L2813
.L2814:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2813
	leaq	.L2818(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L2818:
	.long	.L2817-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2817-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2817-.L2818
	.long	.L2820-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2817-.L2818
	.long	.L2813-.L2818
	.long	.L2817-.L2818
	.long	.L2813-.L2818
	.long	.L2813-.L2818
	.long	.L2817-.L2818
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L2815:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2852
	jmp	.L2849
.L2813:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2849
.L2852:
	call	_ZdlPv@PLT
	jmp	.L2849
.L2817:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L2832
	leaq	.LC22(%rip), %rsi
.L2832:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L2844
	jmp	.L2829
.L2816:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L2834
	leaq	.LC22(%rip), %rsi
.L2834:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2835
	call	_ZdlPv@PLT
.L2835:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2829
.L2844:
	call	_ZdlPv@PLT
	jmp	.L2829
.L2820:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC70(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L2837
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2837:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L2829:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2849:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2824
	call	_ZdlPv@PLT
.L2824:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2810
	call	_ZdlPv@PLT
.L2810:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2840
	call	__stack_chk_fail@PLT
.L2840:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9902:
	.size	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text._ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.type	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, @function
_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_:
.LFB10200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-64(%rbp), %rdi
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	subq	$48, %rsp
	movq	(%rsi), %r8
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -64(%rbp)
	addq	%r8, %rdx
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2853
	call	_ZdlPv@PLT
.L2853:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2857
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2857:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10200:
	.size	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, .-_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.section	.text._ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.type	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, @function
_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_:
.LFB10201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-64(%rbp), %rdi
	.cfi_offset 3, -32
	leaq	-48(%rbp), %rbx
	subq	$48, %rsp
	movq	(%rsi), %r8
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -64(%rbp)
	addq	%r8, %rdx
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2858
	call	_ZdlPv@PLT
.L2858:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2862
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2862:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10201:
	.size	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, .-_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_:
.LFB9750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L2864
	leaq	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2864:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC69(%rip), %rbx
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %r9
.L2865:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	-200(%rbp), %r8
	jne	.L2865
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L2866
	cmpb	$99, %dl
	jg	.L2867
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r14
	movq	%rax, -184(%rbp)
	je	.L2868
	cmpb	$88, %dl
	je	.L2869
	jmp	.L2866
.L2867:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2866
	leaq	.L2871(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2871:
	.long	.L2872-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2872-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2874-.L2871
	.long	.L2873-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2872-.L2871
	.long	.L2866-.L2871
	.long	.L2872-.L2871
	.long	.L2866-.L2871
	.long	.L2866-.L2871
	.long	.L2870-.L2871
	.section	.text.unlikely._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
.L2868:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L2900
	jmp	.L2898
.L2866:
	movq	%r8, %rdx
	movq	%r9, %rsi
	leaq	-128(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2898
.L2900:
	call	_ZdlPv@PLT
	jmp	.L2898
.L2872:
	movq	(%r8), %rsi
	movq	8(%r8), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	jmp	.L2895
.L2874:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	jmp	.L2895
.L2870:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
.L2895:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L2892
	jmp	.L2881
.L2869:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2884
	call	_ZdlPv@PLT
.L2884:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2881
.L2892:
	call	_ZdlPv@PLT
	jmp	.L2881
.L2873:
	leaq	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2881:
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2898:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2877
	call	_ZdlPv@PLT
.L2877:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2863
	call	_ZdlPv@PLT
.L2863:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2888
	call	__stack_chk_fail@PLT
.L2888:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9750:
	.size	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	.type	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_, @function
_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_:
.LFB9144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2902
	call	__stack_chk_fail@PLT
.L2902:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9144:
	.size	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_, .-_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB8308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$208, %rsp
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L2905
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L2905:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L2907
.L2914:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L2914
.L2907:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L2915
.L2908:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r13
	movl	$32, -224(%rbp)
	andq	$-16, %r13
	movq	%rax, -216(%rbp)
	leaq	-192(%rbp), %rax
	leaq	-224(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	*%r10
	leaq	16(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, (%r12)
	movslq	%eax, %rdx
	addq	%r13, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2916
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2915:
	.cfi_restore_state
	orq	$0, -8(%rsp,%rcx)
	jmp	.L2908
.L2916:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8308:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.rodata._ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC73:
	.string	"%lld"
	.section	.text.unlikely._ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2918
	leaq	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2918:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC69(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2919:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2919
	cmpb	$120, %dl
	jg	.L2920
	cmpb	$99, %dl
	jg	.L2921
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L2922
	cmpb	$88, %dl
	je	.L2923
	jmp	.L2920
.L2921:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2920
	leaq	.L2925(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2925:
	.long	.L2926-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2926-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2928-.L2925
	.long	.L2927-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2926-.L2925
	.long	.L2920-.L2925
	.long	.L2926-.L2925
	.long	.L2920-.L2925
	.long	.L2920-.L2925
	.long	.L2924-.L2925
	.section	.text.unlikely._ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2922:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2929
	call	_ZdlPv@PLT
.L2929:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2949
	jmp	.L2931
.L2920:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2954
	call	_ZdlPv@PLT
	jmp	.L2954
.L2926:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC73(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2951
.L2928:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2936:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2936
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2951
.L2924:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2951:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2948
	jmp	.L2935
.L2923:
	movq	(%r8), %rsi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4ExLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2939
	call	_ZdlPv@PLT
.L2939:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2935
.L2948:
	call	_ZdlPv@PLT
	jmp	.L2935
.L2927:
	leaq	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2935:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2954:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2931
.L2949:
	call	_ZdlPv@PLT
.L2931:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2917
	call	_ZdlPv@PLT
.L2917:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2943
	call	__stack_chk_fail@PLT
.L2943:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9734:
	.size	_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2956
	call	__stack_chk_fail@PLT
.L2956:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9121:
	.size	_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.rodata._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC74:
	.string	"%d"
	.section	.text.unlikely._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2959
	leaq	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2959:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC69(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2960:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2960
	cmpb	$120, %dl
	jg	.L2961
	cmpb	$99, %dl
	jg	.L2962
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L2963
	cmpb	$88, %dl
	je	.L2964
	jmp	.L2961
.L2962:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2961
	leaq	.L2966(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2966:
	.long	.L2967-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2967-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2969-.L2966
	.long	.L2968-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2967-.L2966
	.long	.L2961-.L2966
	.long	.L2967-.L2966
	.long	.L2961-.L2966
	.long	.L2961-.L2966
	.long	.L2965-.L2966
	.section	.text.unlikely._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2963:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2970
	call	_ZdlPv@PLT
.L2970:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2990
	jmp	.L2972
.L2961:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2995
	call	_ZdlPv@PLT
	jmp	.L2995
.L2967:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC74(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2992
.L2969:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2977:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2977
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2992
.L2965:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2992:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2989
	jmp	.L2976
.L2964:
	movl	(%r8), %esi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2980
	call	_ZdlPv@PLT
.L2980:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2976
.L2989:
	call	_ZdlPv@PLT
	jmp	.L2976
.L2968:
	leaq	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2976:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2995:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2972
.L2990:
	call	_ZdlPv@PLT
.L2972:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2958
	call	_ZdlPv@PLT
.L2958:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2984
	call	__stack_chk_fail@PLT
.L2984:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9737:
	.size	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2997
	call	__stack_chk_fail@PLT
.L2997:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9125:
	.size	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.rodata._ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC75:
	.string	"%f"
	.section	.text.unlikely._ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L3000
	leaq	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3000:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	movq	%r12, %rsi
	leaq	-144(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	leaq	.LC69(%rip), %r12
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-168(%rbp), %r9
.L3001:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	jne	.L3001
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r14
	jg	.L3002
	cmpb	$99, %dl
	jg	.L3003
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -184(%rbp)
	je	.L3004
	cmpb	$88, %dl
	je	.L3005
	jmp	.L3002
.L3003:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3002
	leaq	.L3007(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3007:
	.long	.L3006-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3006-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3006-.L3007
	.long	.L3009-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3006-.L3007
	.long	.L3002-.L3007
	.long	.L3006-.L3007
	.long	.L3002-.L3007
	.long	.L3002-.L3007
	.long	.L3006-.L3007
	.section	.text.unlikely._ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3004:
	movq	-168(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -192(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L3035
	jmp	.L3033
.L3002:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-128(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3033
.L3035:
	call	_ZdlPv@PLT
	jmp	.L3033
.L3006:
	movsd	(%rbx), %xmm0
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	movb	$1, %al
	leaq	.LC75(%rip), %rcx
	movl	$328, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L3028
	jmp	.L3017
.L3005:
	movsd	(%rbx), %xmm0
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%r10, %rdi
	movb	$1, %al
	leaq	.LC75(%rip), %rcx
	movl	$328, %edx
	movq	%r10, -192(%rbp)
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	movq	-192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3020
	call	_ZdlPv@PLT
.L3020:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3017
.L3028:
	call	_ZdlPv@PLT
	jmp	.L3017
.L3009:
	leaq	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3017:
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3033:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3013
	call	_ZdlPv@PLT
.L3013:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2999
	call	_ZdlPv@PLT
.L2999:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3024
	call	__stack_chk_fail@PLT
.L3024:
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9740:
	.size	_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3037
	call	__stack_chk_fail@PLT
.L3037:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9128:
	.size	_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_:
.LFB9745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L3040
	leaq	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3040:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC69(%rip), %rbx
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %r9
.L3041:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	-200(%rbp), %r8
	jne	.L3041
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L3042
	cmpb	$99, %dl
	jg	.L3043
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -184(%rbp)
	je	.L3044
	cmpb	$88, %dl
	je	.L3045
	jmp	.L3042
.L3043:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3042
	leaq	.L3047(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_,comdat
	.align 4
	.align 4
.L3047:
	.long	.L3048-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3048-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3050-.L3047
	.long	.L3049-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3048-.L3047
	.long	.L3042-.L3047
	.long	.L3048-.L3047
	.long	.L3042-.L3047
	.long	.L3042-.L3047
	.long	.L3046-.L3047
	.section	.text.unlikely._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_,comdat
.L3044:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -192(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_
	movq	-192(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L3076
	jmp	.L3074
.L3042:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_
	leaq	-128(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3074
.L3076:
	call	_ZdlPv@PLT
	jmp	.L3074
.L3048:
	movq	(%r8), %rsi
	movq	8(%r8), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	jmp	.L3071
.L3050:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	jmp	.L3071
.L3046:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
.L3071:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L3068
	jmp	.L3057
.L3045:
	movq	%r8, %rsi
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	movq	-192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3060
	call	_ZdlPv@PLT
.L3060:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3057
.L3068:
	call	_ZdlPv@PLT
	jmp	.L3057
.L3049:
	leaq	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3057:
	movq	-168(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3074:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3053
	call	_ZdlPv@PLT
.L3053:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L3039
	call	_ZdlPv@PLT
.L3039:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3064
	call	__stack_chk_fail@PLT
.L3064:
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9745:
	.size	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_, .-_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_
	.type	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_, @function
_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_:
.LFB9136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3078
	call	__stack_chk_fail@PLT
.L3078:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9136:
	.size	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_, .-_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_:
.LFB9747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L3081
	leaq	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3081:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC69(%rip), %rbx
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %r9
.L3082:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	-200(%rbp), %r8
	jne	.L3082
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L3083
	cmpb	$99, %dl
	jg	.L3084
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -184(%rbp)
	je	.L3085
	cmpb	$88, %dl
	je	.L3086
	jmp	.L3083
.L3084:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3083
	leaq	.L3088(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_,comdat
	.align 4
	.align 4
.L3088:
	.long	.L3089-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3089-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3091-.L3088
	.long	.L3090-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3089-.L3088
	.long	.L3083-.L3088
	.long	.L3089-.L3088
	.long	.L3083-.L3088
	.long	.L3083-.L3088
	.long	.L3087-.L3088
	.section	.text.unlikely._ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_,comdat
.L3085:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -192(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_
	movq	-192(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L3117
	jmp	.L3115
.L3083:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_
	leaq	-128(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3115
.L3117:
	call	_ZdlPv@PLT
	jmp	.L3115
.L3089:
	movq	(%r8), %rsi
	movq	8(%r8), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	jmp	.L3112
.L3091:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	jmp	.L3112
.L3087:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
.L3112:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L3109
	jmp	.L3098
.L3086:
	movq	%r8, %rsi
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	movq	-192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3101
	call	_ZdlPv@PLT
.L3101:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3098
.L3109:
	call	_ZdlPv@PLT
	jmp	.L3098
.L3090:
	leaq	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3098:
	movq	-168(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3115:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3094
	call	_ZdlPv@PLT
.L3094:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L3080
	call	_ZdlPv@PLT
.L3080:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3105
	call	__stack_chk_fail@PLT
.L3105:
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9747:
	.size	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_, .-_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_
	.type	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_, @function
_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_:
.LFB9140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3119
	call	__stack_chk_fail@PLT
.L3119:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9140:
	.size	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_, .-_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_
	.section	.rodata.str1.1
.LC76:
	.string	"\"false\""
.LC77:
	.string	"SPrintF(\"%s\", false)"
.LC78:
	.string	"\"true\""
.LC79:
	.string	"SPrintF(\"%s\", true)"
.LC80:
	.string	"SPrintF(\"%d\", true)"
.LC81:
	.string	"%u"
.LC82:
	.string	"SPrintF(\"%u\", true)"
.LC83:
	.string	"10000000000"
.LC84:
	.string	"\"10000000000\""
.LC85:
	.string	"SPrintF(\"%d\", 10000000000LL)"
.LC86:
	.string	"-10000000000"
.LC87:
	.string	"\"-10000000000\""
.LC88:
	.string	"SPrintF(\"%d\", -10000000000LL)"
.LC89:
	.string	"SPrintF(\"%u\", 10000000000LL)"
.LC90:
	.string	"SPrintF(\"%u\", -10000000000LL)"
.LC91:
	.string	"%i"
.LC92:
	.string	"10"
.LC93:
	.string	"\"10\""
.LC94:
	.string	"SPrintF(\"%i\", 10)"
.LC95:
	.string	"SPrintF(\"%d\", 10)"
.LC96:
	.string	"%x"
.LC97:
	.string	"f"
.LC98:
	.string	"\"f\""
.LC99:
	.string	"SPrintF(\"%x\", 15)"
.LC100:
	.string	"SPrintF(\"%x\", 16)"
.LC101:
	.string	"%X"
.LC102:
	.string	"F"
.LC103:
	.string	"\"F\""
.LC104:
	.string	"SPrintF(\"%X\", 15)"
.LC105:
	.string	"SPrintF(\"%X\", 16)"
.LC106:
	.string	"%o"
.LC107:
	.string	"7"
.LC108:
	.string	"\"7\""
.LC109:
	.string	"SPrintF(\"%o\", 7)"
.LC110:
	.string	"SPrintF(\"%o\", 8)"
.LC112:
	.string	"0.5"
	.section	.rodata.str1.8
	.align 8
.LC113:
	.string	"atof(SPrintF(\"%s\", 0.5).c_str())"
	.section	.rodata.str1.1
.LC115:
	.string	"-0.5"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"atof(SPrintF(\"%s\", -0.5).c_str())"
	.section	.rodata.str1.1
.LC117:
	.string	">="
.LC118:
	.string	"4u"
.LC119:
	.string	"SPrintF(\"%p\", fn).size()"
.LC120:
	.string	"SPrintF(\"%p\", p).size()"
.LC121:
	.string	"foo"
.LC122:
	.string	"bar"
.LC123:
	.string	"%s %s"
.LC124:
	.string	"foo bar"
.LC125:
	.string	"\"foo bar\""
.LC126:
	.string	"SPrintF(\"%s %s\", foo, \"bar\")"
.LC127:
	.string	"SPrintF(\"%s %s\", foo, bar)"
.LC128:
	.string	"\"(null)\""
.LC129:
	.string	"SPrintF(\"%s\", nullptr)"
.LC130:
	.string	"[%% %s %%]"
.LC131:
	.string	"[% foo %]"
.LC132:
	.string	"\"[% foo %]\""
.LC133:
	.string	"SPrintF(\"[%% %s %%]\", foo)"
.LC134:
	.string	"\"meow\""
.LC135:
	.string	"SPrintF(\"%s\", HasToString{})"
.LC136:
	.string	"a"
.LC137:
	.string	"with_zero"
.LC138:
	.string	"SPrintF(\"%s\", with_zero)"
	.section	.text.unlikely
	.align 2
	.globl	_ZN21UtilTest_SPrintF_Test8TestBodyEv
	.type	_ZN21UtilTest_SPrintF_Test8TestBodyEv, @function
_ZN21UtilTest_SPrintF_Test8TestBodyEv:
.LFB8230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC72(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	leaq	-208(%rbp), %r15
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-216(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -216(%rbp)
	call	_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC10(%rip), %r8
	leaq	.LC76(%rip), %rdx
	leaq	.LC77(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA6_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3122
	call	_ZdlPv@PLT
.L3122:
	cmpb	$0, -208(%rbp)
	leaq	-224(%rbp), %r13
	jne	.L3123
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3124
	movq	(%rax), %r8
.L3124:
	movl	$261, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3123
	movq	(%rdi), %rax
	call	*8(%rax)
.L3123:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3126
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3126:
	movq	%r12, %rdx
	leaq	.LC72(%rip), %rsi
	movq	%r14, %rdi
	movb	$1, -216(%rbp)
	call	_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC9(%rip), %r8
	leaq	.LC78(%rip), %rdx
	leaq	.LC79(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3127
	call	_ZdlPv@PLT
.L3127:
	cmpb	$0, -208(%rbp)
	jne	.L3128
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3129
	movq	(%rax), %r8
.L3129:
	movl	$262, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3128
	movq	(%rdi), %rax
	call	*8(%rax)
.L3128:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3131
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3131:
	movq	%r12, %rdx
	leaq	.LC74(%rip), %rsi
	movq	%r14, %rdi
	movb	$1, -216(%rbp)
	call	_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC9(%rip), %r8
	leaq	.LC78(%rip), %rdx
	leaq	.LC80(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3132
	call	_ZdlPv@PLT
.L3132:
	cmpb	$0, -208(%rbp)
	jne	.L3133
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3134
	movq	(%rax), %r8
.L3134:
	movl	$263, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3133
	movq	(%rdi), %rax
	call	*8(%rax)
.L3133:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3136
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3136:
	movq	%r12, %rdx
	leaq	.LC81(%rip), %rsi
	movq	%r14, %rdi
	movb	$1, -216(%rbp)
	call	_ZN4node7SPrintFIJbEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC9(%rip), %r8
	leaq	.LC78(%rip), %rdx
	leaq	.LC82(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3137
	call	_ZdlPv@PLT
.L3137:
	cmpb	$0, -208(%rbp)
	jne	.L3138
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3139
	movq	(%rax), %r8
.L3139:
	movl	$264, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3138
	movq	(%rdi), %rax
	call	*8(%rax)
.L3138:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3141
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3141:
	movl	$9765625, %eax
	movq	%r12, %rdx
	leaq	.LC74(%rip), %rsi
	movq	%r14, %rdi
	salq	$10, %rax
	movq	%rax, -216(%rbp)
	call	_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC83(%rip), %r8
	leaq	.LC84(%rip), %rdx
	leaq	.LC85(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3142
	call	_ZdlPv@PLT
.L3142:
	cmpb	$0, -208(%rbp)
	jne	.L3143
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3144
	movq	(%rax), %r8
.L3144:
	movl	$265, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3143
	movq	(%rdi), %rax
	call	*8(%rax)
.L3143:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3146
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3146:
	movabsq	$-10000000000, %rax
	movq	%r12, %rdx
	leaq	.LC74(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC86(%rip), %r8
	leaq	.LC87(%rip), %rdx
	leaq	.LC88(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3147
	call	_ZdlPv@PLT
.L3147:
	cmpb	$0, -208(%rbp)
	jne	.L3148
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3149
	movq	(%rax), %r8
.L3149:
	movl	$266, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3148
	movq	(%rdi), %rax
	call	*8(%rax)
.L3148:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3151
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3151:
	movl	$9765625, %eax
	movq	%r12, %rdx
	leaq	.LC81(%rip), %rsi
	movq	%r14, %rdi
	salq	$10, %rax
	movq	%rax, -216(%rbp)
	call	_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC83(%rip), %r8
	leaq	.LC84(%rip), %rdx
	leaq	.LC89(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA12_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3152
	call	_ZdlPv@PLT
.L3152:
	cmpb	$0, -208(%rbp)
	jne	.L3153
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3154
	movq	(%rax), %r8
.L3154:
	movl	$267, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3153
	movq	(%rdi), %rax
	call	*8(%rax)
.L3153:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3156
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3156:
	movabsq	$-10000000000, %rax
	movq	%r12, %rdx
	leaq	.LC81(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN4node7SPrintFIJxEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC86(%rip), %r8
	leaq	.LC87(%rip), %rdx
	leaq	.LC90(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA13_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3157
	call	_ZdlPv@PLT
.L3157:
	cmpb	$0, -208(%rbp)
	jne	.L3158
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3159
	movq	(%rax), %r8
.L3159:
	movl	$268, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3158
	movq	(%rdi), %rax
	call	*8(%rax)
.L3158:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3161
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3161:
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	.LC91(%rip), %rsi
	movl	$10, -216(%rbp)
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC92(%rip), %r8
	leaq	.LC93(%rip), %rdx
	leaq	.LC94(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3162
	call	_ZdlPv@PLT
.L3162:
	cmpb	$0, -208(%rbp)
	jne	.L3163
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3164
	movq	(%rax), %r8
.L3164:
	movl	$269, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3163
	movq	(%rdi), %rax
	call	*8(%rax)
.L3163:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3166
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3166:
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	.LC74(%rip), %rsi
	movl	$10, -216(%rbp)
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC92(%rip), %r8
	leaq	.LC93(%rip), %rdx
	leaq	.LC95(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3167
	call	_ZdlPv@PLT
.L3167:
	cmpb	$0, -208(%rbp)
	jne	.L3168
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3169
	movq	(%rax), %r8
.L3169:
	movl	$270, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3168
	movq	(%rdi), %rax
	call	*8(%rax)
.L3168:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3171
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3171:
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	.LC96(%rip), %rsi
	movl	$15, -216(%rbp)
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r14, %rcx
	leaq	.LC97(%rip), %r8
	leaq	.LC98(%rip), %rdx
	leaq	.LC99(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3172
	call	_ZdlPv@PLT
.L3172:
	cmpb	$0, -208(%rbp)
	leaq	-216(%rbp), %r12
	leaq	-224(%rbp), %rbx
	jne	.L3173
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3174
	movq	(%rax), %r8
.L3174:
	movl	$271, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3173
	movq	(%rdi), %rax
	call	*8(%rax)
.L3173:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3176
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3176:
	leaq	-96(%rbp), %r13
	movq	%r12, %rdx
	leaq	-80(%rbp), %r15
	movl	$16, -216(%rbp)
	leaq	.LC96(%rip), %rsi
	movq	%r13, %rdi
	leaq	-208(%rbp), %r14
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r14, %rdi
	movq	%r13, %rcx
	leaq	.LC92(%rip), %r8
	leaq	.LC93(%rip), %rdx
	leaq	.LC100(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3177
	call	_ZdlPv@PLT
.L3177:
	cmpb	$0, -208(%rbp)
	jne	.L3178
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3179
	movq	(%rax), %r8
.L3179:
	movl	$272, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3178
	movq	(%rdi), %rax
	call	*8(%rax)
.L3178:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3181
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3181:
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	.LC101(%rip), %rsi
	movl	$15, -216(%rbp)
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r14, %rdi
	movq	%r13, %rcx
	leaq	.LC102(%rip), %r8
	leaq	.LC103(%rip), %rdx
	leaq	.LC104(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3182
	call	_ZdlPv@PLT
.L3182:
	cmpb	$0, -208(%rbp)
	jne	.L3183
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3184
	movq	(%rax), %r8
.L3184:
	movl	$273, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3183
	movq	(%rdi), %rax
	call	*8(%rax)
.L3183:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3186
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3186:
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	.LC101(%rip), %rsi
	movl	$16, -216(%rbp)
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r14, %rdi
	movq	%r13, %rcx
	leaq	.LC92(%rip), %r8
	leaq	.LC93(%rip), %rdx
	leaq	.LC105(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3187
	call	_ZdlPv@PLT
.L3187:
	cmpb	$0, -208(%rbp)
	jne	.L3188
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3189
	movq	(%rax), %r8
.L3189:
	movl	$274, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3188
	movq	(%rdi), %rax
	call	*8(%rax)
.L3188:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3191
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3191:
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	.LC106(%rip), %rsi
	movl	$7, -216(%rbp)
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r14, %rdi
	movq	%r13, %rcx
	leaq	.LC107(%rip), %r8
	leaq	.LC108(%rip), %rdx
	leaq	.LC109(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA2_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3192
	call	_ZdlPv@PLT
.L3192:
	cmpb	$0, -208(%rbp)
	jne	.L3193
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3194
	movq	(%rax), %r8
.L3194:
	movl	$275, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3193
	movq	(%rdi), %rax
	call	*8(%rax)
.L3193:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3196
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3196:
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	.LC106(%rip), %rsi
	movl	$8, -216(%rbp)
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r14, %rdi
	movq	%r13, %rcx
	leaq	.LC92(%rip), %r8
	leaq	.LC93(%rip), %rdx
	leaq	.LC110(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA3_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3197
	call	_ZdlPv@PLT
.L3197:
	cmpb	$0, -208(%rbp)
	jne	.L3198
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3199
	movq	(%rax), %r8
.L3199:
	movl	$276, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3198
	movq	(%rdi), %rax
	call	*8(%rax)
.L3198:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3201
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3201:
	leaq	-232(%rbp), %rax
	movsd	.LC111(%rip), %xmm0
	leaq	.LC72(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rax, -264(%rbp)
	movsd	%xmm0, -216(%rbp)
	movsd	%xmm0, -232(%rbp)
	call	_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	-96(%rbp), %rdi
	xorl	%esi, %esi
	call	strtod@PLT
	movq	%r14, %rdi
	movq	%r12, %r8
	movq	%rbx, %rcx
	leaq	.LC112(%rip), %rdx
	leaq	.LC113(%rip), %rsi
	movsd	%xmm0, -224(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3202
	call	_ZdlPv@PLT
.L3202:
	cmpb	$0, -208(%rbp)
	jne	.L3203
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3204
	movq	(%rax), %r8
.L3204:
	movl	$278, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3203
	movq	(%rdi), %rax
	call	*8(%rax)
.L3203:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3206
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3206:
	movsd	.LC114(%rip), %xmm0
	movq	-264(%rbp), %rdx
	leaq	.LC72(%rip), %rsi
	movq	%r13, %rdi
	movsd	%xmm0, -216(%rbp)
	movsd	%xmm0, -232(%rbp)
	call	_ZN4node7SPrintFIJdEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	-96(%rbp), %rdi
	xorl	%esi, %esi
	call	strtod@PLT
	movq	%r14, %rdi
	movq	%r12, %r8
	movq	%rbx, %rcx
	leaq	.LC115(%rip), %rdx
	leaq	.LC116(%rip), %rsi
	movsd	%xmm0, -224(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIddEENS_15AssertionResultEPKcS4_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3207
	call	_ZdlPv@PLT
.L3207:
	cmpb	$0, -208(%rbp)
	jne	.L3208
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3209
	movq	(%rax), %r8
.L3209:
	movl	$279, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3208
	movq	(%rdi), %rax
	call	*8(%rax)
.L3208:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3211
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3211:
	leaq	_ZZN21UtilTest_SPrintF_Test8TestBodyEvENUlvE_4_FUNEv(%rip), %rax
	leaq	-248(%rbp), %rdx
	movq	%r13, %rdi
	movl	$4, -224(%rbp)
	leaq	.LC70(%rip), %rsi
	movq	%rax, -248(%rbp)
	movq	%rdx, -240(%rbp)
	call	_ZN4node7SPrintFIJRPFvvEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	-88(%rbp), %rax
	movl	-224(%rbp), %edx
	movq	%rax, -216(%rbp)
	cmpq	%rdx, %rax
	jb	.L3212
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L3213
.L3212:
	leaq	.LC117(%rip), %r9
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	leaq	.LC118(%rip), %rdx
	leaq	.LC119(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
.L3213:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3214
	call	_ZdlPv@PLT
.L3214:
	cmpb	$0, -208(%rbp)
	jne	.L3215
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3216
	movq	(%rax), %r8
.L3216:
	movl	$283, %ecx
	leaq	.LC12(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3215
	movq	(%rdi), %rax
	call	*8(%rax)
.L3215:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3218
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3218:
	leaq	-240(%rbp), %rdx
	movq	%r13, %rdi
	leaq	.LC70(%rip), %rsi
	movl	$4, -224(%rbp)
	call	_ZN4node7SPrintFIJRPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	-88(%rbp), %rax
	movl	-224(%rbp), %edx
	movq	%rax, -216(%rbp)
	cmpq	%rdx, %rax
	jb	.L3219
	movq	%r14, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L3220
.L3219:
	leaq	.LC117(%rip), %r9
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r14, %rdi
	leaq	.LC118(%rip), %rdx
	leaq	.LC120(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureImjEENS_15AssertionResultEPKcS4_RKT_RKT0_S4_
.L3220:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3221
	call	_ZdlPv@PLT
.L3221:
	cmpb	$0, -208(%rbp)
	leaq	-216(%rbp), %rbx
	jne	.L3222
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3223
	movq	(%rax), %r8
.L3223:
	leaq	-224(%rbp), %r12
	movl	$284, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3222
	movq	(%rdi), %rax
	call	*8(%rax)
.L3222:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3225
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3225:
	leaq	-192(%rbp), %r12
	leaq	.LC121(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	leaq	.LC122(%rip), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	.LC123(%rip), %rsi
	movq	%rcx, -232(%rbp)
	call	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA4_KcEEES6_PS9_DpOT_
	movq	%r14, %rdi
	movq	%r13, %rcx
	leaq	.LC124(%rip), %r8
	leaq	.LC125(%rip), %rdx
	leaq	.LC126(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3226
	call	_ZdlPv@PLT
.L3226:
	cmpb	$0, -208(%rbp)
	jne	.L3227
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3228
	movq	(%rax), %r8
.L3228:
	leaq	-224(%rbp), %r13
	movl	$288, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3227
	movq	(%rdi), %rax
	call	*8(%rax)
.L3227:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3230
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3230:
	movq	-264(%rbp), %rcx
	leaq	-96(%rbp), %r13
	movq	%r12, %rdx
	leaq	.LC123(%rip), %rsi
	movq	%r13, %rdi
	leaq	-208(%rbp), %r15
	leaq	-80(%rbp), %r14
	call	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERPKcEEES6_SA_DpOT_
	movq	%r15, %rdi
	movq	%r13, %rcx
	leaq	.LC124(%rip), %r8
	leaq	.LC125(%rip), %rdx
	leaq	.LC127(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA8_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3231
	call	_ZdlPv@PLT
.L3231:
	cmpb	$0, -208(%rbp)
	jne	.L3232
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3233
	movq	(%rax), %r8
.L3233:
	leaq	-224(%rbp), %rdi
	movl	$289, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%rdi, -264(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-264(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-264(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3232
	movq	(%rdi), %rax
	call	*8(%rax)
.L3232:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3235
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3235:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	leaq	.LC72(%rip), %rsi
	movq	$0, -216(%rbp)
	call	_ZN4node7SPrintFIJDnEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r15, %rdi
	movq	%r13, %rcx
	leaq	.LC22(%rip), %r8
	leaq	.LC128(%rip), %rdx
	leaq	.LC129(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA7_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3236
	call	_ZdlPv@PLT
.L3236:
	cmpb	$0, -208(%rbp)
	jne	.L3237
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3238
	movq	(%rax), %r8
.L3238:
	leaq	-224(%rbp), %rdi
	movl	$290, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%rdi, -264(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-264(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-264(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3237
	movq	(%rdi), %rax
	call	*8(%rax)
.L3237:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3240
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3240:
	movq	%r12, %rdx
	leaq	.LC130(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	movq	%r15, %rdi
	movq	%r13, %rcx
	leaq	.LC131(%rip), %r8
	leaq	.LC132(%rip), %rdx
	leaq	.LC133(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA10_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3241
	call	_ZdlPv@PLT
.L3241:
	cmpb	$0, -208(%rbp)
	jne	.L3242
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3243
	movq	(%rax), %r8
.L3243:
	leaq	-224(%rbp), %r12
	movl	$292, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3242
	movq	(%rdi), %rax
	call	*8(%rax)
.L3242:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3245
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3245:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN4node7SPrintFIJZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rcx
	leaq	.LC71(%rip), %r8
	leaq	.LC134(%rip), %rdx
	leaq	.LC135(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEA5_cEENS_15AssertionResultEPKcSB_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3246
	call	_ZdlPv@PLT
.L3246:
	cmpb	$0, -208(%rbp)
	jne	.L3247
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3248
	movq	(%rax), %r8
.L3248:
	leaq	-224(%rbp), %r12
	movl	$299, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3247
	movq	(%rdi), %rax
	call	*8(%rax)
.L3247:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3250
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3250:
	leaq	-128(%rbp), %r12
	leaq	.LC136(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_
	leaq	-160(%rbp), %r12
	movl	$98, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S5_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3251
	call	_ZdlPv@PLT
.L3251:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3252
	call	_ZdlPv@PLT
.L3252:
	movq	%r12, %rdx
	leaq	.LC72(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node7SPrintFIJRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	movq	%r15, %rdi
	movq	%r12, %r8
	movq	%r13, %rcx
	leaq	.LC137(%rip), %rdx
	leaq	.LC138(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_EENS_15AssertionResultEPKcSA_RKT_RKT0_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3253
	call	_ZdlPv@PLT
.L3253:
	cmpb	$0, -208(%rbp)
	jne	.L3254
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rax
	leaq	.LC21(%rip), %r8
	testq	%rax, %rax
	je	.L3255
	movq	(%rax), %r8
.L3255:
	leaq	-224(%rbp), %r12
	movl	$302, %ecx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3254
	movq	(%rdi), %rax
	call	*8(%rax)
.L3254:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3257
	call	_ZNKSt14default_deleteINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclEPS5_.isra.0
.L3257:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3258
	call	_ZdlPv@PLT
.L3258:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3121
	call	_ZdlPv@PLT
.L3121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3260
	call	__stack_chk_fail@PLT
.L3260:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8230:
	.size	_ZN21UtilTest_SPrintF_Test8TestBodyEv, .-_ZN21UtilTest_SPrintF_Test8TestBodyEv
	.section	.rodata.str1.1
.LC139:
	.string	"ListHead"
.LC140:
	.string	"UtilTest"
.LC141:
	.string	"StringEqualNoCase"
.LC142:
	.string	"StringEqualNoCaseN"
.LC143:
	.string	"ToLower"
.LC144:
	.string	"Malloc"
.LC145:
	.string	"Calloc"
.LC146:
	.string	"UncheckedMalloc"
.LC147:
	.string	"UncheckedCalloc"
.LC148:
	.string	"MaybeStackBuffer"
.LC149:
	.string	"SPrintF"
	.section	.text.startup
	.p2align 4
	.type	_Z41__static_initialization_and_destruction_0ii.constprop.0, @function
_Z41__static_initialization_and_destruction_0ii.constprop.0:
.LFB11654:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3487
.L3445:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3488
.L3446:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	-128(%rbp), %r14
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r13
	leaq	-80(%rbp), %r12
	movq	%rax, %r15
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC140(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	movq	%r15, %r9
	movq	%r13, %r8
	leaq	.LC139(%rip), %rsi
	pushq	%rbx
	pushq	$0
	pushq	$0
	movl	$6, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN22UtilTest_ListHead_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3447
	call	_ZdlPv@PLT
.L3447:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L3448
	call	_ZdlPv@PLT
.L3448:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3489
.L3449:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3490
.L3450:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r11
	leaq	.LC140(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC141(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$60, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN31UtilTest_StringEqualNoCase_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3451
	call	_ZdlPv@PLT
.L3451:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3452
	call	_ZdlPv@PLT
.L3452:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3491
.L3453:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3492
.L3454:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r10
	leaq	.LC140(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC142(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$71, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN32UtilTest_StringEqualNoCaseN_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3455
	call	_ZdlPv@PLT
.L3455:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3456
	call	_ZdlPv@PLT
.L3456:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3493
.L3457:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3494
.L3458:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC140(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r9
	movq	-136(%rbp), %r9
	movq	%r13, %r8
	leaq	.LC143(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$86, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN21UtilTest_ToLower_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3459
	call	_ZdlPv@PLT
.L3459:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3460
	call	_ZdlPv@PLT
.L3460:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3495
.L3461:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3496
.L3462:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r8
	leaq	.LC140(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC144(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$100, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN20UtilTest_Malloc_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3463
	call	_ZdlPv@PLT
.L3463:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3464
	call	_ZdlPv@PLT
.L3464:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3497
.L3465:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3498
.L3466:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rdi
	movq	%r13, %r8
	leaq	.LC140(%rip), %rdi
	leaq	.LC145(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$108, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN20UtilTest_Calloc_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3467
	call	_ZdlPv@PLT
.L3467:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3468
	call	_ZdlPv@PLT
.L3468:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3499
.L3469:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3500
.L3470:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rsi
	leaq	.LC140(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC146(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$116, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN29UtilTest_UncheckedMalloc_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3471
	call	_ZdlPv@PLT
.L3471:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3472
	call	_ZdlPv@PLT
.L3472:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3501
.L3473:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3502
.L3474:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	leaq	.LC140(%rip), %rdi
	xorl	%edx, %edx
	pushq	%rcx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	leaq	.LC147(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$124, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN29UtilTest_UncheckedCalloc_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3475
	call	_ZdlPv@PLT
.L3475:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3476
	call	_ZdlPv@PLT
.L3476:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3503
.L3477:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3504
.L3478:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	leaq	.LC140(%rip), %rdi
	xorl	%ecx, %ecx
	pushq	%rdx
	movq	%r13, %r8
	xorl	%edx, %edx
	leaq	.LC148(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$213, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN30UtilTest_MaybeStackBuffer_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3479
	call	_ZdlPv@PLT
.L3479:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3480
	call	_ZdlPv@PLT
.L3480:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3505
.L3481:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L3506
.L3482:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	.LC140(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC149(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$256, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN21UtilTest_SPrintF_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L3483
	call	_ZdlPv@PLT
.L3483:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3444
	call	_ZdlPv@PLT
.L3444:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3507
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3490:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3450
.L3489:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3449
.L3488:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3446
.L3500:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3470
.L3499:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3469
.L3502:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3474
.L3501:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3473
.L3504:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3478
.L3503:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3477
.L3506:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3482
.L3505:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3481
.L3492:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3454
.L3491:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3453
.L3494:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3458
.L3493:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3457
.L3496:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3462
.L3495:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3461
.L3498:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L3466
.L3497:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3465
.L3487:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L3445
.L3507:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11654:
	.size	_Z41__static_initialization_and_destruction_0ii.constprop.0, .-_Z41__static_initialization_and_destruction_0ii.constprop.0
	.p2align 4
	.type	_GLOBAL__sub_I__ZN22UtilTest_ListHead_Test10test_info_E, @function
_GLOBAL__sub_I__ZN22UtilTest_ListHead_Test10test_info_E:
.LFB11447:
	.cfi_startproc
	endbr64
	jmp	_Z41__static_initialization_and_destruction_0ii.constprop.0
	.cfi_endproc
.LFE11447:
	.size	_GLOBAL__sub_I__ZN22UtilTest_ListHead_Test10test_info_E, .-_GLOBAL__sub_I__ZN22UtilTest_ListHead_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN22UtilTest_ListHead_Test10test_info_E
	.weak	_ZTVN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22UtilTest_ListHead_TestE10CreateTestEv
	.weak	_ZTV22UtilTest_ListHead_Test
	.section	.data.rel.ro._ZTV22UtilTest_ListHead_Test,"awG",@progbits,_ZTV22UtilTest_ListHead_Test,comdat
	.align 8
	.type	_ZTV22UtilTest_ListHead_Test, @object
	.size	_ZTV22UtilTest_ListHead_Test, 64
_ZTV22UtilTest_ListHead_Test:
	.quad	0
	.quad	0
	.quad	_ZN22UtilTest_ListHead_TestD1Ev
	.quad	_ZN22UtilTest_ListHead_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN22UtilTest_ListHead_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31UtilTest_StringEqualNoCase_TestE10CreateTestEv
	.weak	_ZTV31UtilTest_StringEqualNoCase_Test
	.section	.data.rel.ro._ZTV31UtilTest_StringEqualNoCase_Test,"awG",@progbits,_ZTV31UtilTest_StringEqualNoCase_Test,comdat
	.align 8
	.type	_ZTV31UtilTest_StringEqualNoCase_Test, @object
	.size	_ZTV31UtilTest_StringEqualNoCase_Test, 64
_ZTV31UtilTest_StringEqualNoCase_Test:
	.quad	0
	.quad	0
	.quad	_ZN31UtilTest_StringEqualNoCase_TestD1Ev
	.quad	_ZN31UtilTest_StringEqualNoCase_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN31UtilTest_StringEqualNoCase_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32UtilTest_StringEqualNoCaseN_TestE10CreateTestEv
	.weak	_ZTV32UtilTest_StringEqualNoCaseN_Test
	.section	.data.rel.ro._ZTV32UtilTest_StringEqualNoCaseN_Test,"awG",@progbits,_ZTV32UtilTest_StringEqualNoCaseN_Test,comdat
	.align 8
	.type	_ZTV32UtilTest_StringEqualNoCaseN_Test, @object
	.size	_ZTV32UtilTest_StringEqualNoCaseN_Test, 64
_ZTV32UtilTest_StringEqualNoCaseN_Test:
	.quad	0
	.quad	0
	.quad	_ZN32UtilTest_StringEqualNoCaseN_TestD1Ev
	.quad	_ZN32UtilTest_StringEqualNoCaseN_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN32UtilTest_StringEqualNoCaseN_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI21UtilTest_ToLower_TestE10CreateTestEv
	.weak	_ZTV21UtilTest_ToLower_Test
	.section	.data.rel.ro._ZTV21UtilTest_ToLower_Test,"awG",@progbits,_ZTV21UtilTest_ToLower_Test,comdat
	.align 8
	.type	_ZTV21UtilTest_ToLower_Test, @object
	.size	_ZTV21UtilTest_ToLower_Test, 64
_ZTV21UtilTest_ToLower_Test:
	.quad	0
	.quad	0
	.quad	_ZN21UtilTest_ToLower_TestD1Ev
	.quad	_ZN21UtilTest_ToLower_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN21UtilTest_ToLower_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI20UtilTest_Malloc_TestE10CreateTestEv
	.weak	_ZTV20UtilTest_Malloc_Test
	.section	.data.rel.ro._ZTV20UtilTest_Malloc_Test,"awG",@progbits,_ZTV20UtilTest_Malloc_Test,comdat
	.align 8
	.type	_ZTV20UtilTest_Malloc_Test, @object
	.size	_ZTV20UtilTest_Malloc_Test, 64
_ZTV20UtilTest_Malloc_Test:
	.quad	0
	.quad	0
	.quad	_ZN20UtilTest_Malloc_TestD1Ev
	.quad	_ZN20UtilTest_Malloc_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN20UtilTest_Malloc_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI20UtilTest_Calloc_TestE10CreateTestEv
	.weak	_ZTV20UtilTest_Calloc_Test
	.section	.data.rel.ro._ZTV20UtilTest_Calloc_Test,"awG",@progbits,_ZTV20UtilTest_Calloc_Test,comdat
	.align 8
	.type	_ZTV20UtilTest_Calloc_Test, @object
	.size	_ZTV20UtilTest_Calloc_Test, 64
_ZTV20UtilTest_Calloc_Test:
	.quad	0
	.quad	0
	.quad	_ZN20UtilTest_Calloc_TestD1Ev
	.quad	_ZN20UtilTest_Calloc_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN20UtilTest_Calloc_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedMalloc_TestE10CreateTestEv
	.weak	_ZTV29UtilTest_UncheckedMalloc_Test
	.section	.data.rel.ro._ZTV29UtilTest_UncheckedMalloc_Test,"awG",@progbits,_ZTV29UtilTest_UncheckedMalloc_Test,comdat
	.align 8
	.type	_ZTV29UtilTest_UncheckedMalloc_Test, @object
	.size	_ZTV29UtilTest_UncheckedMalloc_Test, 64
_ZTV29UtilTest_UncheckedMalloc_Test:
	.quad	0
	.quad	0
	.quad	_ZN29UtilTest_UncheckedMalloc_TestD1Ev
	.quad	_ZN29UtilTest_UncheckedMalloc_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN29UtilTest_UncheckedMalloc_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI29UtilTest_UncheckedCalloc_TestE10CreateTestEv
	.weak	_ZTV29UtilTest_UncheckedCalloc_Test
	.section	.data.rel.ro._ZTV29UtilTest_UncheckedCalloc_Test,"awG",@progbits,_ZTV29UtilTest_UncheckedCalloc_Test,comdat
	.align 8
	.type	_ZTV29UtilTest_UncheckedCalloc_Test, @object
	.size	_ZTV29UtilTest_UncheckedCalloc_Test, 64
_ZTV29UtilTest_UncheckedCalloc_Test:
	.quad	0
	.quad	0
	.quad	_ZN29UtilTest_UncheckedCalloc_TestD1Ev
	.quad	_ZN29UtilTest_UncheckedCalloc_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN29UtilTest_UncheckedCalloc_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI30UtilTest_MaybeStackBuffer_TestE10CreateTestEv
	.weak	_ZTV30UtilTest_MaybeStackBuffer_Test
	.section	.data.rel.ro._ZTV30UtilTest_MaybeStackBuffer_Test,"awG",@progbits,_ZTV30UtilTest_MaybeStackBuffer_Test,comdat
	.align 8
	.type	_ZTV30UtilTest_MaybeStackBuffer_Test, @object
	.size	_ZTV30UtilTest_MaybeStackBuffer_Test, 64
_ZTV30UtilTest_MaybeStackBuffer_Test:
	.quad	0
	.quad	0
	.quad	_ZN30UtilTest_MaybeStackBuffer_TestD1Ev
	.quad	_ZN30UtilTest_MaybeStackBuffer_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN30UtilTest_MaybeStackBuffer_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI21UtilTest_SPrintF_TestE10CreateTestEv
	.weak	_ZTV21UtilTest_SPrintF_Test
	.section	.data.rel.ro._ZTV21UtilTest_SPrintF_Test,"awG",@progbits,_ZTV21UtilTest_SPrintF_Test,comdat
	.align 8
	.type	_ZTV21UtilTest_SPrintF_Test, @object
	.size	_ZTV21UtilTest_SPrintF_Test, 64
_ZTV21UtilTest_SPrintF_Test:
	.quad	0
	.quad	0
	.quad	_ZN21UtilTest_SPrintF_TestD1Ev
	.quad	_ZN21UtilTest_SPrintF_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN21UtilTest_SPrintF_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC150:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC152:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char (&)[4]; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.weak	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args
	.section	.rodata.str1.1
.LC153:
	.string	"../src/debug_utils-inl.h:76"
.LC154:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRA4_KcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS1_OT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC152
	.weak	_ZZN4node7ReallocItEEPT_S2_mE4args
	.section	.rodata.str1.1
.LC155:
	.string	"../src/util-inl.h:374"
.LC156:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"T* node::Realloc(T*, size_t) [with T = short unsigned int; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocItEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocItEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocItEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocItEEPT_S2_mE4args, 24
_ZZN4node7ReallocItEEPT_S2_mE4args:
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.weak	_ZZN4node7ReallocIhEEPT_S2_mE4args
	.section	.rodata.str1.8
	.align 8
.LC158:
	.string	"T* node::Realloc(T*, size_t) [with T = unsigned char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIhEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIhEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIhEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIhEEPT_S2_mE4args, 24
_ZZN4node7ReallocIhEEPT_S2_mE4args:
	.quad	.LC155
	.quad	.LC156
	.quad	.LC158
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC159:
	.string	"../src/debug_utils-inl.h:113"
.LC160:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC161
	.section	.rodata.str1.8
	.align 8
.LC162:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = UtilTest_SPrintF_Test::TestBody()::HasToString; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @object
	.size	_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC162
	.align 16
	.type	_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @object
	.size	_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIZN21UtilTest_SPrintF_Test8TestBodyEvE11HasToStringJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC162
	.weak	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const std::__cxx11::basic_string<char>&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC163
	.weak	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC163
	.weak	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = std::nullptr_t; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC164
	.weak	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIDnJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC164
	.weak	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC165:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const std::__cxx11::basic_string<char>&; Args = {const char*&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC165
	.weak	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRPKcEEES6_SA_OT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC165
	.weak	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC166:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const std::__cxx11::basic_string<char>&; Args = {const char (&)[4]}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC166
	.weak	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJRA4_KcEEES6_PS9_OT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC166
	.weak	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = void*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC167
	.weak	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC167
	.weak	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC168:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = void (*&)(); Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC168
	.weak	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPFvvEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC168
	.weak	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = double; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC169
	.weak	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIdJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC169
	.weak	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC170:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = int; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC170
	.weak	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC170
	.weak	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC171:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long long int; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC171
	.weak	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIxJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC171
	.weak	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = bool; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC172
	.weak	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIbJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC172
	.weak	_ZZN4node16MaybeStackBufferItLm1024EE7ReleaseEvE4args
	.section	.rodata.str1.1
.LC173:
	.string	"../src/util.h:428"
.LC174:
	.string	"IsAllocated()"
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::Release() [with T = short unsigned int; long unsigned int kStackStorageSize = 1024]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EE7ReleaseEvE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EE7ReleaseEvE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EE7ReleaseEvE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EE7ReleaseEvE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EE7ReleaseEvE4args:
	.quad	.LC173
	.quad	.LC174
	.quad	.LC175
	.weak	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.1
.LC176:
	.string	"../src/util.h:376"
.LC177:
	.string	"!IsInvalidated()"
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args:
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.weak	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args
	.section	.rodata.str1.1
.LC179:
	.string	"../src/util.h:352"
.LC180:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC181:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args:
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.weak	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC182:
	.string	"../src/util.h:391"
.LC183:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args:
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.weak	_ZZN4node16MaybeStackBufferIhLm1024EE7ReleaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC185:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::Release() [with T = unsigned char; long unsigned int kStackStorageSize = 1024]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIhLm1024EE7ReleaseEvE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIhLm1024EE7ReleaseEvE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIhLm1024EE7ReleaseEvE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIhLm1024EE7ReleaseEvE4args, 24
_ZZN4node16MaybeStackBufferIhLm1024EE7ReleaseEvE4args:
	.quad	.LC173
	.quad	.LC174
	.quad	.LC185
	.weak	_ZZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.8
	.align 8
.LC186:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = unsigned char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIhLm1024EE25AllocateSufficientStorageEmE4args:
	.quad	.LC176
	.quad	.LC177
	.quad	.LC186
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC187:
	.string	"../src/util-inl.h:325"
.LC188:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC189:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC187
	.quad	.LC188
	.quad	.LC189
	.weak	_ZZN4node16MaybeStackBufferIhLm1024EEixEmE4args
	.section	.rodata.str1.8
	.align 8
.LC190:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = unsigned char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIhLm1024EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIhLm1024EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIhLm1024EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIhLm1024EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIhLm1024EEixEmE4args:
	.quad	.LC179
	.quad	.LC180
	.quad	.LC190
	.weak	_ZZN4node16MaybeStackBufferIhLm1024EE9SetLengthEmE4args
	.section	.rodata.str1.8
	.align 8
.LC191:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = unsigned char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIhLm1024EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIhLm1024EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIhLm1024EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIhLm1024EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIhLm1024EE9SetLengthEmE4args:
	.quad	.LC182
	.quad	.LC183
	.quad	.LC191
	.weak	_ZZN4node6CallocIcEEPT_mE4args
	.section	.rodata.str1.1
.LC192:
	.string	"../src/util-inl.h:388"
	.section	.rodata.str1.8
	.align 8
.LC193:
	.string	"T* node::Calloc(size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6CallocIcEEPT_mE4args,"awG",@progbits,_ZZN4node6CallocIcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6CallocIcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6CallocIcEEPT_mE4args, 24
_ZZN4node6CallocIcEEPT_mE4args:
	.quad	.LC192
	.quad	.LC156
	.quad	.LC193
	.weak	_ZZN4node6MallocIcEEPT_mE4args
	.section	.rodata.str1.1
.LC194:
	.string	"../src/util-inl.h:381"
	.section	.rodata.str1.8
	.align 8
.LC195:
	.string	"T* node::Malloc(size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIcEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIcEEPT_mE4args, 24
_ZZN4node6MallocIcEEPT_mE4args:
	.quad	.LC194
	.quad	.LC156
	.quad	.LC195
	.globl	_ZN21UtilTest_SPrintF_Test10test_info_E
	.bss
	.align 8
	.type	_ZN21UtilTest_SPrintF_Test10test_info_E, @object
	.size	_ZN21UtilTest_SPrintF_Test10test_info_E, 8
_ZN21UtilTest_SPrintF_Test10test_info_E:
	.zero	8
	.globl	_ZN30UtilTest_MaybeStackBuffer_Test10test_info_E
	.align 8
	.type	_ZN30UtilTest_MaybeStackBuffer_Test10test_info_E, @object
	.size	_ZN30UtilTest_MaybeStackBuffer_Test10test_info_E, 8
_ZN30UtilTest_MaybeStackBuffer_Test10test_info_E:
	.zero	8
	.globl	_ZN29UtilTest_UncheckedCalloc_Test10test_info_E
	.align 8
	.type	_ZN29UtilTest_UncheckedCalloc_Test10test_info_E, @object
	.size	_ZN29UtilTest_UncheckedCalloc_Test10test_info_E, 8
_ZN29UtilTest_UncheckedCalloc_Test10test_info_E:
	.zero	8
	.globl	_ZN29UtilTest_UncheckedMalloc_Test10test_info_E
	.align 8
	.type	_ZN29UtilTest_UncheckedMalloc_Test10test_info_E, @object
	.size	_ZN29UtilTest_UncheckedMalloc_Test10test_info_E, 8
_ZN29UtilTest_UncheckedMalloc_Test10test_info_E:
	.zero	8
	.globl	_ZN20UtilTest_Calloc_Test10test_info_E
	.align 8
	.type	_ZN20UtilTest_Calloc_Test10test_info_E, @object
	.size	_ZN20UtilTest_Calloc_Test10test_info_E, 8
_ZN20UtilTest_Calloc_Test10test_info_E:
	.zero	8
	.globl	_ZN20UtilTest_Malloc_Test10test_info_E
	.align 8
	.type	_ZN20UtilTest_Malloc_Test10test_info_E, @object
	.size	_ZN20UtilTest_Malloc_Test10test_info_E, 8
_ZN20UtilTest_Malloc_Test10test_info_E:
	.zero	8
	.globl	_ZN21UtilTest_ToLower_Test10test_info_E
	.align 8
	.type	_ZN21UtilTest_ToLower_Test10test_info_E, @object
	.size	_ZN21UtilTest_ToLower_Test10test_info_E, 8
_ZN21UtilTest_ToLower_Test10test_info_E:
	.zero	8
	.globl	_ZN32UtilTest_StringEqualNoCaseN_Test10test_info_E
	.align 8
	.type	_ZN32UtilTest_StringEqualNoCaseN_Test10test_info_E, @object
	.size	_ZN32UtilTest_StringEqualNoCaseN_Test10test_info_E, 8
_ZN32UtilTest_StringEqualNoCaseN_Test10test_info_E:
	.zero	8
	.globl	_ZN31UtilTest_StringEqualNoCase_Test10test_info_E
	.align 8
	.type	_ZN31UtilTest_StringEqualNoCase_Test10test_info_E, @object
	.size	_ZN31UtilTest_StringEqualNoCase_Test10test_info_E, 8
_ZN31UtilTest_StringEqualNoCase_Test10test_info_E:
	.zero	8
	.globl	_ZN22UtilTest_ListHead_Test10test_info_E
	.align 8
	.type	_ZN22UtilTest_ListHead_Test10test_info_E, @object
	.size	_ZN22UtilTest_ListHead_Test10test_info_E, 8
_ZN22UtilTest_ListHead_Test10test_info_E:
	.zero	8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC196:
	.string	"../src/debug_utils-inl.h:67"
.LC197:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC198:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC196
	.quad	.LC197
	.quad	.LC198
	.section	.data.rel.ro,"aw"
	.align 8
.LC16:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC17:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC18:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC44:
	.quad	0
	.quad	1024
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC54:
	.long	0
	.long	1073217536
	.align 8
.LC55:
	.long	0
	.long	1138753536
	.section	.rodata.cst16
	.align 16
.LC57:
	.quad	0
	.quad	1
	.align 16
.LC58:
	.quad	8
	.quad	8
	.align 16
.LC59:
	.quad	2
	.quad	2
	.align 16
.LC60:
	.quad	4
	.quad	4
	.align 16
.LC61:
	.quad	6
	.quad	6
	.section	.rodata.cst8
	.align 8
.LC111:
	.long	0
	.long	1071644672
	.align 8
.LC114:
	.long	0
	.long	-1075838976
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
