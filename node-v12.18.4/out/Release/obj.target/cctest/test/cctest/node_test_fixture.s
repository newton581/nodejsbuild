	.file	"node_test_fixture.cc"
	.text
	.section	.text._ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED2Ev
	.type	_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED2Ev, @function
_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED2Ev:
.LFB9417:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE9417:
	.size	_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED2Ev, .-_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED2Ev
	.weak	_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED1Ev
	.set	_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED1Ev,_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED2Ev
	.section	.text._ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED2Ev
	.type	_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED2Ev, @function
_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED2Ev:
.LFB9398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L4
	movq	%r12, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1312, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9398:
	.size	_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED2Ev, .-_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED2Ev
	.weak	_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED1Ev
	.set	_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED1Ev,_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED2Ev
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB9983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L9
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L10
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L36
	.p2align 4,,10
	.p2align 3
.L9:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L15
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L21
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L37
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L15
.L16:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L23
.L37:
	lock subl	$1, 8(%r13)
	jne	.L23
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L23
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L19
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L15
.L21:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L19
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L19
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L10:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L9
.L36:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L13
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L14:
	cmpl	$1, %eax
	jne	.L9
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L14
	.cfi_endproc
.LFE9983:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.text._ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED2Ev
	.type	_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED2Ev, @function
_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED2Ev:
.LFB9425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L38
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L40
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L42
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L43
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L44:
	cmpl	$1, %eax
	jne	.L42
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L46
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L47:
	cmpl	$1, %eax
	je	.L68
	.p2align 4,,10
	.p2align 3
.L42:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L48
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L49
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L48
.L49:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L56
	lock subl	$1, 8(%r13)
	jne	.L56
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L56
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L49
	.p2align 4,,10
	.p2align 3
.L48:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L52
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L48
.L54:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L52
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L52
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L38:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L47
	.cfi_endproc
.LFE9425:
	.size	_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED2Ev, .-_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED2Ev
	.weak	_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED1Ev
	.set	_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED1Ev,_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED2Ev
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB9981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L71
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L72
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L98
	.p2align 4,,10
	.p2align 3
.L71:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L77
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L83
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L99
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L77
.L78:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L85
.L99:
	lock subl	$1, 8(%r13)
	jne	.L85
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L85
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L81
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L77
.L83:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L81
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L81
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L72:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L71
.L98:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L75
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L76:
	cmpl	$1, %eax
	jne	.L71
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L76
	.cfi_endproc
.LFE9981:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN15NodeTestFixture9allocatorE, @function
_GLOBAL__sub_I__ZN15NodeTestFixture9allocatorE:
.LFB11484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN15NodeTestFixture9allocatorE(%rip), %rsi
	movq	$0, _ZN15NodeTestFixture9allocatorE(%rip)
	leaq	_ZNSt10unique_ptrIN4node20ArrayBufferAllocatorEPFvPS1_EED1Ev(%rip), %rdi
	movq	$0, 8+_ZN15NodeTestFixture9allocatorE(%rip)
	call	__cxa_atexit@PLT
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN15NodeTestFixture8platformE(%rip), %rsi
	leaq	_ZNSt10unique_ptrIN4node12NodePlatformESt14default_deleteIS1_EED1Ev(%rip), %rdi
	call	__cxa_atexit@PLT
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN15NodeTestFixture13tracing_agentE(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZNSt10unique_ptrIN4node7tracing5AgentESt14default_deleteIS2_EED1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11484:
	.size	_GLOBAL__sub_I__ZN15NodeTestFixture9allocatorE, .-_GLOBAL__sub_I__ZN15NodeTestFixture9allocatorE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN15NodeTestFixture9allocatorE
	.globl	_ZN15NodeTestFixture16node_initializedE
	.bss
	.type	_ZN15NodeTestFixture16node_initializedE, @object
	.size	_ZN15NodeTestFixture16node_initializedE, 1
_ZN15NodeTestFixture16node_initializedE:
	.zero	1
	.globl	_ZN15NodeTestFixture13tracing_agentE
	.align 8
	.type	_ZN15NodeTestFixture13tracing_agentE, @object
	.size	_ZN15NodeTestFixture13tracing_agentE, 8
_ZN15NodeTestFixture13tracing_agentE:
	.zero	8
	.globl	_ZN15NodeTestFixture8platformE
	.align 8
	.type	_ZN15NodeTestFixture8platformE, @object
	.size	_ZN15NodeTestFixture8platformE, 8
_ZN15NodeTestFixture8platformE:
	.zero	8
	.globl	_ZN15NodeTestFixture12current_loopE
	.align 32
	.type	_ZN15NodeTestFixture12current_loopE, @object
	.size	_ZN15NodeTestFixture12current_loopE, 848
_ZN15NodeTestFixture12current_loopE:
	.zero	848
	.globl	_ZN15NodeTestFixture9allocatorE
	.align 16
	.type	_ZN15NodeTestFixture9allocatorE, @object
	.size	_ZN15NodeTestFixture9allocatorE, 16
_ZN15NodeTestFixture9allocatorE:
	.zero	16
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
