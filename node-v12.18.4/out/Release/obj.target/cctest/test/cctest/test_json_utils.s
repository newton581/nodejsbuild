	.file	"test_json_utils.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB4748:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4748:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED2Ev:
.LFB6172:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6172:
	.size	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED1Ev,_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED0Ev:
.LFB6174:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6174:
	.size	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED0Ev
	.section	.text._ZN34JSONUtilsTest_EscapeJsonChars_TestD2Ev,"axG",@progbits,_ZN34JSONUtilsTest_EscapeJsonChars_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN34JSONUtilsTest_EscapeJsonChars_TestD2Ev
	.type	_ZN34JSONUtilsTest_EscapeJsonChars_TestD2Ev, @function
_ZN34JSONUtilsTest_EscapeJsonChars_TestD2Ev:
.LFB6168:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV34JSONUtilsTest_EscapeJsonChars_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE6168:
	.size	_ZN34JSONUtilsTest_EscapeJsonChars_TestD2Ev, .-_ZN34JSONUtilsTest_EscapeJsonChars_TestD2Ev
	.weak	_ZN34JSONUtilsTest_EscapeJsonChars_TestD1Ev
	.set	_ZN34JSONUtilsTest_EscapeJsonChars_TestD1Ev,_ZN34JSONUtilsTest_EscapeJsonChars_TestD2Ev
	.section	.text._ZN34JSONUtilsTest_EscapeJsonChars_TestD0Ev,"axG",@progbits,_ZN34JSONUtilsTest_EscapeJsonChars_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN34JSONUtilsTest_EscapeJsonChars_TestD0Ev
	.type	_ZN34JSONUtilsTest_EscapeJsonChars_TestD0Ev, @function
_ZN34JSONUtilsTest_EscapeJsonChars_TestD0Ev:
.LFB6170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV34JSONUtilsTest_EscapeJsonChars_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6170:
	.size	_ZN34JSONUtilsTest_EscapeJsonChars_TestD0Ev, .-_ZN34JSONUtilsTest_EscapeJsonChars_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv:
.LFB6218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV34JSONUtilsTest_EscapeJsonChars_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6218:
	.size	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv
	.section	.rodata._ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NULL"
	.section	.text._ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,"axG",@progbits,_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,comdat
	.p2align 4
	.weak	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.type	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, @function
_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_:
.LFB5215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$504, %rsp
	movq	.LC1(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	.LC2(%rip), %xmm1
	movaps	%xmm1, -512(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r15), %rax
	movq	$0, -104(%rbp)
	movq	%r15, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%r15), %rdi
	addq	%r14, %rdi
	leaq	-368(%rbp), %r14
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rdx), %rdi
	movq	%rax, -432(%rbp)
	leaq	-432(%rbp), %rax
	movq	%rax, -520(%rbp)
	addq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-512(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -512(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%rbx), %r9
	testq	%r9, %r9
	je	.L25
	leaq	-480(%rbp), %rax
	movq	%r9, %rdi
	leaq	-464(%rbp), %rbx
	movq	%r9, -536(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rbx, -480(%rbp)
	call	strlen@PLT
	movq	-536(%rbp), %r9
	cmpq	$15, %rax
	movq	%rax, -488(%rbp)
	movq	%rax, %r8
	ja	.L26
	cmpq	$1, %rax
	jne	.L15
	movzbl	(%r9), %edx
	movb	%dl, -464(%rbp)
	movq	%rbx, %rdx
.L16:
	movq	%rax, -472(%rbp)
	movq	-528(%rbp), %rdi
	movb	$0, (%rdx,%rax)
	movq	-520(%rbp), %rsi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	leaq	16(%r12), %rax
	movb	$0, 16(%r12)
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	testq	%rax, %rax
	je	.L18
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L19
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L20:
	movq	.LC1(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC3(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%r15), %rax
	movq	%r15, -448(%rbp)
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$504, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	-528(%rbp), %rdi
	leaq	-488(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -544(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-536(%rbp), %r9
	movq	-544(%rbp), %r8
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -464(%rbp)
.L14:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %rax
	movq	-480(%rbp), %rdx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-520(%rbp), %rdi
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L15:
	testq	%rax, %rax
	jne	.L28
	movq	%rbx, %rdx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L20
.L27:
	call	__stack_chk_fail@PLT
.L28:
	movq	%rbx, %rdi
	jmp	.L14
	.cfi_endproc
.LFE5215:
	.size	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, .-_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.section	.text._ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_,"axG",@progbits,_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_,comdat
	.p2align 4
	.weak	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	.type	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_, @function
_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_:
.LFB6104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-448(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -480(%rbp)
	movq	.LC1(%rip), %xmm1
	movhps	.LC2(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r13, %rdi
	leaq	-368(%rbp), %r13
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r10
	xorl	%esi, %esi
	movq	%r10, %rdi
	movq	%r10, -472(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-464(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	80(%r15), %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	movq	%r15, -448(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-472(%rbp), %r10
	movq	-480(%rbp), %r8
	movq	%r10, %rsi
	movq	%r8, %rdi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	16(%r14), %rax
	movb	$0, 16(%r14)
	movq	%rax, (%r14)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r14)
	testq	%rax, %rax
	je	.L30
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L36
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L32:
	movq	.LC1(%rip), %xmm0
	leaq	104+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, -448(%rbp)
	movq	%rax, -320(%rbp)
	movq	-352(%rbp), %rdi
	movhps	.LC3(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$440, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L32
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6104:
	.size	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_, .-_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	""
.LC5:
	.string	"abc"
.LC6:
	.string	"EscapeJsonChars(\"abc\")"
.LC7:
	.string	"\"abc\""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../test/cctest/test_json_utils.cc"
	.section	.rodata.str1.1
.LC9:
	.string	"abc\\n"
.LC10:
	.string	"EscapeJsonChars(\"abc\\n\")"
.LC11:
	.string	"\"abc\\\\n\""
.LC12:
	.string	"abc\\nabc"
.LC13:
	.string	"EscapeJsonChars(\"abc\\nabc\")"
.LC14:
	.string	"\"abc\\\\nabc\""
.LC15:
	.string	"abc\\\\"
.LC16:
	.string	"EscapeJsonChars(\"abc\\\\\")"
.LC17:
	.string	"\"abc\\\\\\\\\""
.LC18:
	.string	"abc\\\""
.LC19:
	.string	"EscapeJsonChars(\"abc\\\"\")"
.LC20:
	.string	"\"abc\\\\\\\"\""
.LC21:
	.string	"EscapeJsonChars(input)"
.LC22:
	.string	"expected[i]"
.LC23:
	.string	"basic_string::append"
.LC24:
	.string	"a"
.LC25:
	.string	"EscapeJsonChars(\"a\" + input)"
.LC26:
	.string	"\"a\" + expected[i]"
	.text
	.align 2
	.p2align 4
	.globl	_ZN34JSONUtilsTest_EscapeJsonChars_Test8TestBodyEv
	.type	_ZN34JSONUtilsTest_EscapeJsonChars_Test8TestBodyEv, @function
_ZN34JSONUtilsTest_EscapeJsonChars_Test8TestBodyEv:
.LFB4876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1184(%rbp), %r14
	pushq	%r13
	movq	%r14, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$1368, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1168(%rbp), %rax
	movb	$99, -1166(%rbp)
	movq	%rax, -1328(%rbp)
	movq	%rax, -1184(%rbp)
	movl	$25185, %eax
	movw	%ax, -1168(%rbp)
	leaq	-1152(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	$3, -1176(%rbp)
	movb	$0, -1165(%rbp)
	movq	%rax, -1368(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L39
	leaq	-1296(%rbp), %rax
	leaq	-1072(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rax, -1336(%rbp)
	call	_ZN7testing16AssertionSuccessEv@PLT
	leaq	-1088(%rbp), %rax
	movq	%rax, -1400(%rbp)
.L40:
	movq	-1152(%rbp), %rdi
	leaq	-1136(%rbp), %rax
	movq	%rax, -1376(%rbp)
	cmpq	%rax, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-1184(%rbp), %rdi
	cmpq	-1328(%rbp), %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	cmpb	$0, -1296(%rbp)
	je	.L199
.L45:
	movq	-1288(%rbp), %r12
	testq	%r12, %r12
	je	.L48
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L48:
	movq	-1368(%rbp), %r15
	movq	%r14, %rsi
	movq	-1328(%rbp), %rax
	movl	$174285409, -1168(%rbp)
	movq	$4, -1176(%rbp)
	movq	%r15, %rdi
	movq	%rax, -1184(%rbp)
	movb	$0, -1164(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L50
	movq	-1336(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L51:
	movq	-1152(%rbp), %rdi
	cmpq	-1376(%rbp), %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	-1184(%rbp), %rdi
	cmpq	-1328(%rbp), %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	cmpb	$0, -1296(%rbp)
	je	.L200
.L56:
	movq	-1288(%rbp), %r12
	testq	%r12, %r12
	je	.L59
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L59:
	movq	-1328(%rbp), %rax
	movq	%r14, %rsi
	movq	-1368(%rbp), %r15
	movl	$174285409, -1168(%rbp)
	movb	$99, -1162(%rbp)
	movq	%rax, -1184(%rbp)
	movq	%r15, %rdi
	movl	$25185, %eax
	movw	%ax, -1164(%rbp)
	movq	$7, -1176(%rbp)
	movb	$0, -1161(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L61
	movq	-1336(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L62:
	movq	-1152(%rbp), %rdi
	cmpq	-1376(%rbp), %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	-1184(%rbp), %rdi
	cmpq	-1328(%rbp), %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	cmpb	$0, -1296(%rbp)
	je	.L201
.L67:
	movq	-1288(%rbp), %r12
	testq	%r12, %r12
	je	.L70
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L70:
	movq	-1368(%rbp), %r15
	movq	%r14, %rsi
	movq	-1328(%rbp), %rax
	movl	$1550017121, -1168(%rbp)
	movq	$4, -1176(%rbp)
	movq	%r15, %rdi
	movq	%rax, -1184(%rbp)
	movb	$0, -1164(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L72
	movq	-1336(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L73:
	movq	-1152(%rbp), %rdi
	cmpq	-1376(%rbp), %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-1184(%rbp), %rdi
	cmpq	-1328(%rbp), %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	cmpb	$0, -1296(%rbp)
	je	.L202
.L78:
	movq	-1288(%rbp), %r12
	testq	%r12, %r12
	je	.L81
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L81:
	movq	-1368(%rbp), %r15
	movq	%r14, %rsi
	movq	-1328(%rbp), %rax
	movl	$576938593, -1168(%rbp)
	movq	$4, -1176(%rbp)
	movq	%r15, %rdi
	movq	%rax, -1184(%rbp)
	movb	$0, -1164(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L83
	movq	-1336(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L84:
	movq	-1152(%rbp), %rdi
	cmpq	-1376(%rbp), %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	-1184(%rbp), %rdi
	cmpq	-1328(%rbp), %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	cmpb	$0, -1296(%rbp)
	je	.L203
.L89:
	movq	-1288(%rbp), %r12
	testq	%r12, %r12
	je	.L92
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L92:
	movl	$12336, %eax
	movl	$12592, %edx
	movl	$12848, %ecx
	movl	$13104, %esi
	movw	%ax, -1068(%rbp)
	leaq	-1040(%rbp), %rax
	movl	$13360, %edi
	movl	$13616, %r8d
	movq	%rax, -1056(%rbp)
	leaq	-1008(%rbp), %rax
	movl	$13872, %r9d
	movl	$14128, %r10d
	movq	%rax, -1024(%rbp)
	leaq	-976(%rbp), %rax
	movl	$25180, %r11d
	movl	$28252, %r12d
	movq	%rax, -992(%rbp)
	leaq	-944(%rbp), %rax
	movl	$30300, %r13d
	movl	$26204, %r15d
	movq	%rax, -960(%rbp)
	leaq	-912(%rbp), %rax
	movq	%rax, -928(%rbp)
	leaq	-880(%rbp), %rax
	movq	%rax, -896(%rbp)
	leaq	-848(%rbp), %rax
	movq	%rbx, -1088(%rbp)
	movl	$29788, %ebx
	movw	%dx, -1036(%rbp)
	movl	$12849, %edx
	movw	%cx, -1004(%rbp)
	movl	$13105, %ecx
	movw	%si, -972(%rbp)
	movl	$13361, %esi
	movw	%di, -940(%rbp)
	movl	$13617, %edi
	movw	%r8w, -908(%rbp)
	movl	$13873, %r8d
	movl	$808482140, -1072(%rbp)
	movq	$6, -1080(%rbp)
	movb	$0, -1066(%rbp)
	movl	$808482140, -1040(%rbp)
	movq	$6, -1048(%rbp)
	movb	$0, -1034(%rbp)
	movl	$808482140, -1008(%rbp)
	movq	$6, -1016(%rbp)
	movb	$0, -1002(%rbp)
	movl	$808482140, -976(%rbp)
	movq	$6, -984(%rbp)
	movb	$0, -970(%rbp)
	movl	$808482140, -944(%rbp)
	movq	$6, -952(%rbp)
	movb	$0, -938(%rbp)
	movl	$808482140, -912(%rbp)
	movq	$6, -920(%rbp)
	movb	$0, -906(%rbp)
	movl	$808482140, -880(%rbp)
	movw	%r9w, -876(%rbp)
	movl	$14129, %r9d
	movq	%rax, -864(%rbp)
	leaq	-816(%rbp), %rax
	movq	%rax, -832(%rbp)
	leaq	-784(%rbp), %rax
	movq	%rax, -800(%rbp)
	leaq	-752(%rbp), %rax
	movq	%rax, -768(%rbp)
	leaq	-720(%rbp), %rax
	movq	%rax, -736(%rbp)
	leaq	-688(%rbp), %rax
	movq	%rax, -704(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rax, -672(%rbp)
	movl	$29276, %eax
	movw	%ax, -656(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rax, -640(%rbp)
	movl	$25904, %eax
	movw	%r10w, -844(%rbp)
	movl	$14385, %r10d
	movw	%r11w, -816(%rbp)
	movl	$14641, %r11d
	movw	%bx, -784(%rbp)
	movl	$24881, %ebx
	movw	%r12w, -752(%rbp)
	movl	$25137, %r12d
	movw	%r13w, -720(%rbp)
	movl	$25393, %r13d
	movw	%r15w, -688(%rbp)
	movl	$25649, %r15d
	movq	$6, -888(%rbp)
	movb	$0, -874(%rbp)
	movl	$808482140, -848(%rbp)
	movq	$6, -856(%rbp)
	movb	$0, -842(%rbp)
	movq	$2, -824(%rbp)
	movb	$0, -814(%rbp)
	movq	$2, -792(%rbp)
	movb	$0, -782(%rbp)
	movq	$2, -760(%rbp)
	movb	$0, -750(%rbp)
	movq	$2, -728(%rbp)
	movb	$0, -718(%rbp)
	movq	$2, -696(%rbp)
	movb	$0, -686(%rbp)
	movq	$2, -664(%rbp)
	movb	$0, -654(%rbp)
	movl	$808482140, -624(%rbp)
	movw	%ax, -620(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -608(%rbp)
	movl	$26160, %eax
	movw	%ax, -588(%rbp)
	leaq	-560(%rbp), %rax
	movq	%rax, -576(%rbp)
	movl	$12337, %eax
	movw	%ax, -556(%rbp)
	leaq	-528(%rbp), %rax
	movq	%rax, -544(%rbp)
	movl	$12593, %eax
	movw	%ax, -524(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, -512(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -480(%rbp)
	leaq	-432(%rbp), %rax
	movq	%rax, -448(%rbp)
	leaq	-400(%rbp), %rax
	movq	$6, -632(%rbp)
	movb	$0, -618(%rbp)
	movl	$808482140, -592(%rbp)
	movq	$6, -600(%rbp)
	movb	$0, -586(%rbp)
	movl	$808482140, -560(%rbp)
	movq	$6, -568(%rbp)
	movb	$0, -554(%rbp)
	movl	$808482140, -528(%rbp)
	movq	$6, -536(%rbp)
	movb	$0, -522(%rbp)
	movl	$808482140, -496(%rbp)
	movw	%dx, -492(%rbp)
	movq	$6, -504(%rbp)
	movb	$0, -490(%rbp)
	movl	$808482140, -464(%rbp)
	movw	%cx, -460(%rbp)
	movq	$6, -472(%rbp)
	movb	$0, -458(%rbp)
	movl	$808482140, -432(%rbp)
	movw	%si, -428(%rbp)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
	movq	%rax, -416(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, -384(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -352(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -320(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -288(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -256(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	leaq	-176(%rbp), %rax
	movw	%bx, -236(%rbp)
	movl	$808482140, -400(%rbp)
	movw	%di, -396(%rbp)
	movq	$6, -408(%rbp)
	movb	$0, -394(%rbp)
	movl	$808482140, -368(%rbp)
	movw	%r8w, -364(%rbp)
	movq	$6, -376(%rbp)
	movb	$0, -362(%rbp)
	movl	$808482140, -336(%rbp)
	movw	%r9w, -332(%rbp)
	movq	$6, -344(%rbp)
	movb	$0, -330(%rbp)
	movl	$808482140, -304(%rbp)
	movw	%r10w, -300(%rbp)
	movq	$6, -312(%rbp)
	movb	$0, -298(%rbp)
	movl	$808482140, -272(%rbp)
	movw	%r11w, -268(%rbp)
	movq	$6, -280(%rbp)
	movb	$0, -266(%rbp)
	movl	$808482140, -240(%rbp)
	movq	$6, -248(%rbp)
	movb	$0, -234(%rbp)
	movl	$808482140, -208(%rbp)
	movw	%r12w, -204(%rbp)
	movq	-1400(%rbp), %rbx
	movq	%rax, -192(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -160(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -128(%rbp)
	movl	$25905, %eax
	movw	%ax, -108(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$26161, %eax
	movw	%ax, -76(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -1384(%rbp)
	leaq	-1264(%rbp), %rax
	movq	%rax, -1344(%rbp)
	leaq	-1200(%rbp), %rax
	movq	%rax, -1352(%rbp)
	leaq	-1120(%rbp), %rax
	movq	%rax, -1360(%rbp)
	leaq	-1104(%rbp), %rax
	movw	%r13w, -172(%rbp)
	leaq	-1216(%rbp), %r13
	movq	$6, -216(%rbp)
	movb	$0, -202(%rbp)
	movl	$808482140, -176(%rbp)
	movq	$6, -184(%rbp)
	movb	$0, -170(%rbp)
	movl	$808482140, -144(%rbp)
	movw	%r15w, -140(%rbp)
	movq	$6, -152(%rbp)
	movb	$0, -138(%rbp)
	movl	$808482140, -112(%rbp)
	movq	$6, -120(%rbp)
	movb	$0, -106(%rbp)
	movl	$808482140, -80(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
	movl	$0, -1316(%rbp)
	movq	%rax, -1392(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L94:
	movq	-1360(%rbp), %r12
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	movq	-1368(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	xorl	%r9d, %r9d
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	-1336(%rbp), %rdi
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1376(%rbp), %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	-1120(%rbp), %rdi
	cmpq	-1392(%rbp), %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	-1184(%rbp), %rdi
	cmpq	-1328(%rbp), %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	cmpb	$0, -1296(%rbp)
	je	.L204
.L100:
	movq	-1288(%rbp), %r12
	testq	%r12, %r12
	je	.L103
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L104
	call	_ZdlPv@PLT
.L104:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L103:
	movq	-1352(%rbp), %rax
	movq	%r13, %rdi
	movq	$0, -1208(%rbp)
	movb	$0, -1200(%rbp)
	movq	%rax, -1216(%rbp)
	movq	-1272(%rbp), %rax
	leaq	1(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -1208(%rbp)
	je	.L106
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1272(%rbp), %rdx
	movq	-1280(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1248(%rbp), %r12
	leaq	-1232(%rbp), %r15
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	%r15, -1248(%rbp)
	movq	$0, -1240(%rbp)
	leaq	1(%rax), %rsi
	movb	$0, -1232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -1240(%rbp)
	je	.L106
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1240(%rbp), %rdx
	cmpq	-1176(%rbp), %rdx
	je	.L205
.L107:
	movq	-1360(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	movq	%r12, %rsi
	movq	-1368(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	movq	-1336(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	-1360(%rbp), %r8
	leaq	.LC25(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1376(%rbp), %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	-1120(%rbp), %rdi
	cmpq	-1392(%rbp), %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	-1248(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L112
	call	_ZdlPv@PLT
.L112:
	movq	-1184(%rbp), %rdi
	cmpq	-1328(%rbp), %rdi
	je	.L113
	call	_ZdlPv@PLT
.L113:
	movq	-1216(%rbp), %rdi
	cmpq	-1352(%rbp), %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	cmpb	$0, -1296(%rbp)
	je	.L206
.L115:
	movq	-1288(%rbp), %r12
	testq	%r12, %r12
	je	.L118
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L118:
	movq	-1280(%rbp), %rdi
	cmpq	-1344(%rbp), %rdi
	je	.L120
	call	_ZdlPv@PLT
.L120:
	addl	$1, -1316(%rbp)
	movl	-1316(%rbp), %eax
	addq	$32, %rbx
	cmpl	$32, %eax
	je	.L207
.L123:
	movq	-1344(%rbp), %rax
	movq	%r14, %rdi
	movq	-1384(%rbp), %rsi
	movq	$1, -1272(%rbp)
	movb	$0, -1263(%rbp)
	movq	%rax, -1280(%rbp)
	movzbl	-1316(%rbp), %eax
	movb	%al, -1264(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	8(%rbx), %rdx
	cmpq	-1176(%rbp), %rdx
	jne	.L94
	testq	%rdx, %rdx
	je	.L95
	movq	-1184(%rbp), %rsi
	movq	(%rbx), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L94
.L95:
	movq	-1336(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L205:
	testq	%rdx, %rdx
	je	.L108
	movq	-1184(%rbp), %rsi
	movq	-1248(%rbp), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L107
.L108:
	movq	-1336(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	-1304(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1288(%rbp), %rax
	leaq	.LC4(%rip), %r8
	testq	%rax, %rax
	je	.L101
	movq	(%rax), %r8
.L101:
	leaq	-1312(%rbp), %r15
	movl	$23, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	-1304(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1288(%rbp), %rax
	leaq	.LC4(%rip), %r8
	testq	%rax, %rax
	je	.L116
	movq	(%rax), %r8
.L116:
	leaq	-1312(%rbp), %r15
	movl	$24, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L115
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L207:
	movq	-1400(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L208:
	call	_ZdlPv@PLT
	leaq	-32(%rbx), %rax
	cmpq	%rbx, %r12
	je	.L38
.L126:
	movq	%rax, %rbx
.L127:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	jne	.L208
	leaq	-32(%rbx), %rax
	cmpq	%rbx, %r12
	jne	.L126
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L209
	addq	$1368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	-1400(%rbp), %r13
	movq	-1368(%rbp), %rsi
	leaq	-1120(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	movq	-1336(%rbp), %r15
	leaq	.LC18(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -1296(%rbp)
	movq	%r15, %rsi
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movq	%r13, %r8
	movq	%r12, %rcx
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-1120(%rbp), %rdi
	leaq	-1104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L84
	call	_ZdlPv@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-1400(%rbp), %r13
	movq	-1368(%rbp), %rsi
	leaq	-1120(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	movq	-1336(%rbp), %r15
	leaq	.LC15(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -1296(%rbp)
	movq	%r15, %rsi
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movq	%r13, %r8
	movq	%r12, %rcx
	leaq	.LC16(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-1120(%rbp), %rdi
	leaq	-1104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L73
	call	_ZdlPv@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L61:
	movq	-1400(%rbp), %r13
	movq	-1368(%rbp), %rsi
	leaq	-1120(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	movq	-1336(%rbp), %r15
	leaq	.LC12(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -1296(%rbp)
	movq	%r15, %rsi
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movq	%r13, %r8
	movq	%r12, %rcx
	leaq	.LC13(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-1120(%rbp), %rdi
	leaq	-1104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L62
	call	_ZdlPv@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-1400(%rbp), %r13
	movq	-1368(%rbp), %rsi
	leaq	-1120(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	movq	-1336(%rbp), %r15
	leaq	.LC9(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -1296(%rbp)
	movq	%r15, %rsi
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movq	%r13, %r8
	movq	%r12, %rcx
	leaq	.LC10(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-1120(%rbp), %rdi
	leaq	-1104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L51
	call	_ZdlPv@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	-1088(%rbp), %rbx
	movq	-1368(%rbp), %rsi
	leaq	-1120(%rbp), %r12
	movq	%rbx, %rdi
	movq	%rbx, -1400(%rbp)
	call	_ZN7testing13PrintToStringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT_
	leaq	.LC5(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, -1296(%rbp)
	leaq	-1296(%rbp), %rax
	movq	%rax, %r15
	movq	%rax, %rsi
	movq	%rax, -1336(%rbp)
	call	_ZN7testing13PrintToStringIPKcEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r12, %rcx
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-1120(%rbp), %rdi
	leaq	-1104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	-1088(%rbp), %rdi
	leaq	-1072(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L40
	call	_ZdlPv@PLT
	jmp	.L40
.L199:
	leaq	-1304(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1288(%rbp), %rax
	leaq	.LC4(%rip), %r8
	testq	%rax, %rax
	je	.L46
	movq	(%rax), %r8
.L46:
	leaq	-1312(%rbp), %r13
	movl	$7, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L45
.L201:
	leaq	-1304(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1288(%rbp), %rax
	leaq	.LC4(%rip), %r8
	testq	%rax, %rax
	je	.L68
	movq	(%rax), %r8
.L68:
	leaq	-1312(%rbp), %r13
	movl	$9, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L67
.L200:
	leaq	-1304(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1288(%rbp), %rax
	leaq	.LC4(%rip), %r8
	testq	%rax, %rax
	je	.L57
	movq	(%rax), %r8
.L57:
	leaq	-1312(%rbp), %r13
	movl	$8, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L56
.L203:
	leaq	-1304(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1288(%rbp), %rax
	leaq	.LC4(%rip), %r8
	testq	%rax, %rax
	je	.L90
	movq	(%rax), %r8
.L90:
	leaq	-1312(%rbp), %r13
	movl	$11, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L89
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L89
.L202:
	leaq	-1304(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1288(%rbp), %rax
	leaq	.LC4(%rip), %r8
	testq	%rax, %rax
	je	.L79
	movq	(%rax), %r8
.L79:
	leaq	-1312(%rbp), %r13
	movl	$10, %ecx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L78
.L106:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4876:
	.size	_ZN34JSONUtilsTest_EscapeJsonChars_Test8TestBodyEv, .-_ZN34JSONUtilsTest_EscapeJsonChars_Test8TestBodyEv
	.section	.rodata.str1.1
.LC27:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC29:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.align 8
.LC30:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.align 8
.LC31:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata.str1.1
.LC32:
	.string	"EscapeJsonChars"
.LC33:
	.string	"JSONUtilsTest"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E, @function
_GLOBAL__sub_I__ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E:
.LFB6297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-136(%rbp), %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L232
.L211:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L233
.L212:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	-112(%rbp), %r13
	xorl	%edx, %edx
	leaq	-128(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r13, -128(%rbp)
	movq	%rax, %r14
	leaq	-80(%rbp), %r15
	movq	$33, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %rdx
	movdqa	.LC34(%rip), %xmm0
	movq	%rax, -128(%rbp)
	movq	%rdx, -112(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC35(%rip), %xmm0
	movq	-128(%rbp), %rdx
	movb	$99, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %r9
	movq	%r15, -96(%rbp)
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L213
	testq	%r10, %r10
	je	.L234
.L213:
	movq	%r9, -136(%rbp)
	cmpq	$15, %r9
	ja	.L235
	cmpq	$1, %r9
	jne	.L216
	movzbl	(%r10), %eax
	leaq	-96(%rbp), %r8
	movb	%al, -80(%rbp)
.L217:
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%r14, %r9
	xorl	%ecx, %ecx
	subq	$8, %rsp
	leaq	.LC33(%rip), %rdi
	leaq	.LC32(%rip), %rsi
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	xorl	%edx, %edx
	pushq	%rbx
	pushq	$0
	pushq	$0
	movl	$5, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E(%rip)
	cmpq	%r15, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L236
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L216:
	.cfi_restore_state
	leaq	-96(%rbp), %r8
	testq	%r9, %r9
	je	.L217
	movq	%r15, %rdi
	jmp	.L215
.L233:
	movl	$6946, %ecx
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	.LC27(%rip), %rdx
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC28(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC30(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	jmp	.L212
.L235:
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -168(%rbp)
	movq	%r8, %rdi
	movq	%r10, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r10
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %r9
	movq	%rax, -80(%rbp)
.L215:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r8, -152(%rbp)
	call	memcpy@PLT
	movq	-152(%rbp), %r8
	jmp	.L217
.L232:
	movl	$6959, %ecx
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	.LC27(%rip), %rdx
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC28(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC29(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	jmp	.L211
.L234:
	leaq	.LC31(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6297:
	.size	_GLOBAL__sub_I__ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E, .-_GLOBAL__sub_I__ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E
	.weak	_ZTVN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI34JSONUtilsTest_EscapeJsonChars_TestE10CreateTestEv
	.weak	_ZTV34JSONUtilsTest_EscapeJsonChars_Test
	.section	.data.rel.ro._ZTV34JSONUtilsTest_EscapeJsonChars_Test,"awG",@progbits,_ZTV34JSONUtilsTest_EscapeJsonChars_Test,comdat
	.align 8
	.type	_ZTV34JSONUtilsTest_EscapeJsonChars_Test, @object
	.size	_ZTV34JSONUtilsTest_EscapeJsonChars_Test, 64
_ZTV34JSONUtilsTest_EscapeJsonChars_Test:
	.quad	0
	.quad	0
	.quad	_ZN34JSONUtilsTest_EscapeJsonChars_TestD1Ev
	.quad	_ZN34JSONUtilsTest_EscapeJsonChars_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN34JSONUtilsTest_EscapeJsonChars_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.globl	_ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E
	.bss
	.align 8
	.type	_ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E, @object
	.size	_ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E, 8
_ZN34JSONUtilsTest_EscapeJsonChars_Test10test_info_E:
	.zero	8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.data.rel.ro,"aw"
	.align 8
.LC1:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC2:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC3:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC34:
	.quad	3419484896659189294
	.quad	8372038271277228899
	.align 16
.LC35:
	.quad	7957705967292150629
	.quad	7146776568146457951
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
