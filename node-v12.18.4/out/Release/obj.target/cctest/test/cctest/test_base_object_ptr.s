	.file	"test_base_object_ptr.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB4725:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4725:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB8143:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE8143:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE,"axG",@progbits,_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE
	.type	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE, @function
_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE:
.LFB8557:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8557:
	.size	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE, .-_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE
	.section	.text._ZNK15DummyBaseObject8SelfSizeEv,"axG",@progbits,_ZNK15DummyBaseObject8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15DummyBaseObject8SelfSizeEv
	.type	_ZNK15DummyBaseObject8SelfSizeEv, @function
_ZNK15DummyBaseObject8SelfSizeEv:
.LFB8559:
	.cfi_startproc
	endbr64
	movl	$32, %eax
	ret
	.cfi_endproc
.LFE8559:
	.size	_ZNK15DummyBaseObject8SelfSizeEv, .-_ZNK15DummyBaseObject8SelfSizeEv
	.text
	.align 2
	.p2align 4
	.type	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr10MemoryInfoEPN4node13MemoryTrackerE, @function
_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr10MemoryInfoEPN4node13MemoryTrackerE:
.LFB8634:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8634:
	.size	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr10MemoryInfoEPN4node13MemoryTrackerE, .-_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr10MemoryInfoEPN4node13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr8SelfSizeEv, @function
_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr8SelfSizeEv:
.LFB8636:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE8636:
	.size	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr8SelfSizeEv, .-_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr8SelfSizeEv
	.section	.text._ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED2Ev:
.LFB11590:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11590:
	.size	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED1Ev,_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED2Ev:
.LFB11598:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11598:
	.size	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED1Ev,_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED2Ev:
.LFB11606:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11606:
	.size	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED1Ev,_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED2Ev:
.LFB11614:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11614:
	.size	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED1Ev,_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED2Ev:
.LFB11622:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11622:
	.size	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED1Ev,_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED2Ev:
.LFB11630:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11630:
	.size	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED1Ev,_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED2Ev
	.section	.text._ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,"axG",@progbits,_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.type	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, @function
_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_:
.LFB8147:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %rdi
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE8147:
	.size	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, .-_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.section	.text._ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED0Ev:
.LFB11632:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11632:
	.size	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED0Ev:
.LFB11624:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11624:
	.size	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED0Ev:
.LFB11616:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11616:
	.size	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED0Ev:
.LFB11608:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11608:
	.size	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED0Ev:
.LFB11600:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11600:
	.size	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED0Ev:
.LFB11592:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11592:
	.size	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED0Ev
	.section	.text._ZN15NodeTestFixture8TearDownEv,"axG",@progbits,_ZN15NodeTestFixture8TearDownEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture8TearDownEv
	.type	_ZN15NodeTestFixture8TearDownEv, @function
_ZN15NodeTestFixture8TearDownEv:
.LFB8542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8542:
	.size	_ZN15NodeTestFixture8TearDownEv, .-_ZN15NodeTestFixture8TearDownEv
	.section	.text._ZN15NodeTestFixture5SetUpEv,"axG",@progbits,_ZN15NodeTestFixture5SetUpEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture5SetUpEv
	.type	_ZN15NodeTestFixture5SetUpEv, @function
_ZN15NodeTestFixture5SetUpEv:
.LFB8539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node26CreateArrayBufferAllocatorEv@PLT
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %r8
	movq	%rax, %rdi
	movq	%rax, 8+_ZN15NodeTestFixture9allocatorE(%rip)
	testq	%r8, %r8
	je	.L24
	movq	%r8, %rdi
	call	*_ZN15NodeTestFixture9allocatorE(%rip)
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %rdi
.L24:
	movq	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE@GOTPCREL(%rip), %rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	movq	%rax, _ZN15NodeTestFixture9allocatorE(%rip)
	call	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L31
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate5EnterEv@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture5SetUpEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8539:
	.size	_ZN15NodeTestFixture5SetUpEv, .-_ZN15NodeTestFixture5SetUpEv
	.section	.text._ZN36BaseObjectPtrTest_NestedClasses_TestD2Ev,"axG",@progbits,_ZN36BaseObjectPtrTest_NestedClasses_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN36BaseObjectPtrTest_NestedClasses_TestD2Ev
	.type	_ZN36BaseObjectPtrTest_NestedClasses_TestD2Ev, @function
_ZN36BaseObjectPtrTest_NestedClasses_TestD2Ev:
.LFB11582:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11582:
	.size	_ZN36BaseObjectPtrTest_NestedClasses_TestD2Ev, .-_ZN36BaseObjectPtrTest_NestedClasses_TestD2Ev
	.weak	_ZN36BaseObjectPtrTest_NestedClasses_TestD1Ev
	.set	_ZN36BaseObjectPtrTest_NestedClasses_TestD1Ev,_ZN36BaseObjectPtrTest_NestedClasses_TestD2Ev
	.section	.text._ZN36BaseObjectPtrTest_NestedClasses_TestD0Ev,"axG",@progbits,_ZN36BaseObjectPtrTest_NestedClasses_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN36BaseObjectPtrTest_NestedClasses_TestD0Ev
	.type	_ZN36BaseObjectPtrTest_NestedClasses_TestD0Ev, @function
_ZN36BaseObjectPtrTest_NestedClasses_TestD0Ev:
.LFB11584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11584:
	.size	_ZN36BaseObjectPtrTest_NestedClasses_TestD0Ev, .-_ZN36BaseObjectPtrTest_NestedClasses_TestD0Ev
	.section	.text._ZN31BaseObjectPtrTest_Moveable_TestD2Ev,"axG",@progbits,_ZN31BaseObjectPtrTest_Moveable_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31BaseObjectPtrTest_Moveable_TestD2Ev
	.type	_ZN31BaseObjectPtrTest_Moveable_TestD2Ev, @function
_ZN31BaseObjectPtrTest_Moveable_TestD2Ev:
.LFB11594:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11594:
	.size	_ZN31BaseObjectPtrTest_Moveable_TestD2Ev, .-_ZN31BaseObjectPtrTest_Moveable_TestD2Ev
	.weak	_ZN31BaseObjectPtrTest_Moveable_TestD1Ev
	.set	_ZN31BaseObjectPtrTest_Moveable_TestD1Ev,_ZN31BaseObjectPtrTest_Moveable_TestD2Ev
	.section	.text._ZN31BaseObjectPtrTest_Moveable_TestD0Ev,"axG",@progbits,_ZN31BaseObjectPtrTest_Moveable_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31BaseObjectPtrTest_Moveable_TestD0Ev
	.type	_ZN31BaseObjectPtrTest_Moveable_TestD0Ev, @function
_ZN31BaseObjectPtrTest_Moveable_TestD0Ev:
.LFB11596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11596:
	.size	_ZN31BaseObjectPtrTest_Moveable_TestD0Ev, .-_ZN31BaseObjectPtrTest_Moveable_TestD0Ev
	.section	.text._ZN29BaseObjectPtrTest_GCWeak_TestD2Ev,"axG",@progbits,_ZN29BaseObjectPtrTest_GCWeak_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN29BaseObjectPtrTest_GCWeak_TestD2Ev
	.type	_ZN29BaseObjectPtrTest_GCWeak_TestD2Ev, @function
_ZN29BaseObjectPtrTest_GCWeak_TestD2Ev:
.LFB11602:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11602:
	.size	_ZN29BaseObjectPtrTest_GCWeak_TestD2Ev, .-_ZN29BaseObjectPtrTest_GCWeak_TestD2Ev
	.weak	_ZN29BaseObjectPtrTest_GCWeak_TestD1Ev
	.set	_ZN29BaseObjectPtrTest_GCWeak_TestD1Ev,_ZN29BaseObjectPtrTest_GCWeak_TestD2Ev
	.section	.text._ZN29BaseObjectPtrTest_GCWeak_TestD0Ev,"axG",@progbits,_ZN29BaseObjectPtrTest_GCWeak_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN29BaseObjectPtrTest_GCWeak_TestD0Ev
	.type	_ZN29BaseObjectPtrTest_GCWeak_TestD0Ev, @function
_ZN29BaseObjectPtrTest_GCWeak_TestD0Ev:
.LFB11604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11604:
	.size	_ZN29BaseObjectPtrTest_GCWeak_TestD0Ev, .-_ZN29BaseObjectPtrTest_GCWeak_TestD0Ev
	.section	.text._ZN33BaseObjectPtrTest_Undetached_TestD2Ev,"axG",@progbits,_ZN33BaseObjectPtrTest_Undetached_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33BaseObjectPtrTest_Undetached_TestD2Ev
	.type	_ZN33BaseObjectPtrTest_Undetached_TestD2Ev, @function
_ZN33BaseObjectPtrTest_Undetached_TestD2Ev:
.LFB11610:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11610:
	.size	_ZN33BaseObjectPtrTest_Undetached_TestD2Ev, .-_ZN33BaseObjectPtrTest_Undetached_TestD2Ev
	.weak	_ZN33BaseObjectPtrTest_Undetached_TestD1Ev
	.set	_ZN33BaseObjectPtrTest_Undetached_TestD1Ev,_ZN33BaseObjectPtrTest_Undetached_TestD2Ev
	.section	.text._ZN33BaseObjectPtrTest_Undetached_TestD0Ev,"axG",@progbits,_ZN33BaseObjectPtrTest_Undetached_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33BaseObjectPtrTest_Undetached_TestD0Ev
	.type	_ZN33BaseObjectPtrTest_Undetached_TestD0Ev, @function
_ZN33BaseObjectPtrTest_Undetached_TestD0Ev:
.LFB11612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11612:
	.size	_ZN33BaseObjectPtrTest_Undetached_TestD0Ev, .-_ZN33BaseObjectPtrTest_Undetached_TestD0Ev
	.section	.text._ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD2Ev,"axG",@progbits,_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD2Ev
	.type	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD2Ev, @function
_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD2Ev:
.LFB11618:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11618:
	.size	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD2Ev, .-_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD2Ev
	.weak	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD1Ev
	.set	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD1Ev,_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD2Ev
	.section	.text._ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD0Ev,"axG",@progbits,_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD0Ev
	.type	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD0Ev, @function
_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD0Ev:
.LFB11620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11620:
	.size	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD0Ev, .-_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD0Ev
	.section	.text._ZN37BaseObjectPtrTest_ScopedDetached_TestD2Ev,"axG",@progbits,_ZN37BaseObjectPtrTest_ScopedDetached_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN37BaseObjectPtrTest_ScopedDetached_TestD2Ev
	.type	_ZN37BaseObjectPtrTest_ScopedDetached_TestD2Ev, @function
_ZN37BaseObjectPtrTest_ScopedDetached_TestD2Ev:
.LFB11626:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11626:
	.size	_ZN37BaseObjectPtrTest_ScopedDetached_TestD2Ev, .-_ZN37BaseObjectPtrTest_ScopedDetached_TestD2Ev
	.weak	_ZN37BaseObjectPtrTest_ScopedDetached_TestD1Ev
	.set	_ZN37BaseObjectPtrTest_ScopedDetached_TestD1Ev,_ZN37BaseObjectPtrTest_ScopedDetached_TestD2Ev
	.section	.text._ZN37BaseObjectPtrTest_ScopedDetached_TestD0Ev,"axG",@progbits,_ZN37BaseObjectPtrTest_ScopedDetached_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN37BaseObjectPtrTest_ScopedDetached_TestD0Ev
	.type	_ZN37BaseObjectPtrTest_ScopedDetached_TestD0Ev, @function
_ZN37BaseObjectPtrTest_ScopedDetached_TestD0Ev:
.LFB11628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11628:
	.size	_ZN37BaseObjectPtrTest_ScopedDetached_TestD0Ev, .-_ZN37BaseObjectPtrTest_ScopedDetached_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv:
.LFB11715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV37BaseObjectPtrTest_ScopedDetached_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11715:
	.size	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv:
.LFB11714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV45BaseObjectPtrTest_ScopedDetachedWithWeak_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11714:
	.size	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv:
.LFB11713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV33BaseObjectPtrTest_Undetached_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11713:
	.size	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv:
.LFB11712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV29BaseObjectPtrTest_GCWeak_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11712:
	.size	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv:
.LFB11711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV31BaseObjectPtrTest_Moveable_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11711:
	.size	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv:
.LFB11710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV36BaseObjectPtrTest_NestedClasses_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11710:
	.size	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv
	.section	.text._ZN15NodeTestFixture16TearDownTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture16TearDownTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture16TearDownTestCaseEv
	.type	_ZN15NodeTestFixture16TearDownTestCaseEv, @function
_ZN15NodeTestFixture16TearDownTestCaseEv:
.LFB8538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L64:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L67
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_close@PLT
	testl	%eax, %eax
	jne	.L68
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8538:
	.size	_ZN15NodeTestFixture16TearDownTestCaseEv, .-_ZN15NodeTestFixture16TearDownTestCaseEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0:
.LFB11838:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11838:
	.size	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0:
.LFB11839:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11839:
	.size	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB11880:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L88
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L89
	cmpq	$1, %rax
	jne	.L81
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L82:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L82
	jmp	.L80
.L89:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L80:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L82
.L88:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11880:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB10136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L93
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L94
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L120
	.p2align 4,,10
	.p2align 3
.L93:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L99
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L105
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L121
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L99
.L100:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L107
.L121:
	lock subl	$1, 8(%r13)
	jne	.L107
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L107
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L100
	.p2align 4,,10
	.p2align 3
.L99:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L103
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L99
.L105:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L103
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L103
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L94:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L93
.L120:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L97
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L98:
	cmpl	$1, %eax
	jne	.L93
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L98
	.cfi_endproc
.LFE10136:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata._ZN15NodeTestFixture13SetUpTestCaseEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"NODE_OPTIONS"
.LC6:
	.string	"cctest"
	.section	.text._ZN15NodeTestFixture13SetUpTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture13SetUpTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture13SetUpTestCaseEv
	.type	_ZN15NodeTestFixture13SetUpTestCaseEv, @function
_ZN15NodeTestFixture13SetUpTestCaseEv:
.LFB8533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN15NodeTestFixture16node_initializedE(%rip)
	je	.L164
.L123:
	movl	$1312, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing5AgentC1Ev@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r13
	movq	%r12, _ZN15NodeTestFixture13tracing_agentE(%rip)
	testq	%r13, %r13
	je	.L124
	movq	%r13, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r12
.L124:
	movq	%r12, %rdi
	call	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %rax
	movq	976(%rax), %r12
	testq	%r12, %r12
	je	.L165
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L166
	movl	$128, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r12
	movq	%r13, _ZN15NodeTestFixture8platformE(%rip)
	testq	%r12, %r12
	je	.L127
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L128
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L130
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L131
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L132:
	cmpl	$1, %eax
	jne	.L130
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L134
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L135:
	cmpl	$1, %eax
	jne	.L130
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L130:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L149
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L139
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L149
.L139:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L147
	lock subl	$1, 8(%r13)
	jne	.L147
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L147
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L139
	.p2align 4,,10
	.p2align 3
.L149:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
.L127:
	movq	%r13, %rdi
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L142
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L149
.L145:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L142
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L142
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	.LC5(%rip), %rdi
	call	uv_os_unsetenv@PLT
	leaq	.LC6(%rip), %rax
	leaq	-48(%rbp), %rcx
	movl	$1, -64(%rbp)
	leaq	-60(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movb	$1, _ZN15NodeTestFixture16node_initializedE(%rip)
	call	_ZN4node4InitEPiPPKcS0_PS3_@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r12, %rdi
	call	*%rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L131:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L135
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8533:
	.size	_ZN15NodeTestFixture13SetUpTestCaseEv, .-_ZN15NodeTestFixture13SetUpTestCaseEv
	.text
	.align 2
	.p2align 4
	.type	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr14MemoryInfoNameEv, @function
_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr14MemoryInfoNameEv:
.LFB8635:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1951426676, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7590663667208315471, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$114, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE8635:
	.size	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr14MemoryInfoNameEv, .-_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr14MemoryInfoNameEv
	.section	.text._ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev
	.type	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev, @function
_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev:
.LFB8558:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1784827749, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8313999476397012292, %rcx
	movq	%rdx, (%rdi)
	movl	$25445, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$116, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE8558:
	.size	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev, .-_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev
	.section	.rodata.str1.1
.LC7:
	.string	"(nullptr)"
.LC8:
	.string	"NULL"
.LC9:
	.string	"nullptr"
	.text
	.p2align 4
	.type	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0, @function
_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0:
.LFB11977:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	cmpq	$0, (%rdx)
	je	.L187
	movq	.LC10(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, -608(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC11(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r14, %rdi
	movl	$9, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -600(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L173
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L188
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L175:
	movq	.LC10(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC12(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -592(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-608(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-576(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L189
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L178:
	movq	-384(%rbp), %rax
	leaq	-496(%rbp), %r14
	movq	$0, -504(%rbp)
	leaq	-512(%rbp), %r15
	movq	%r14, -512(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L179
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L180
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L181:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-592(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	-544(%rbp), %r8
	movq	-568(%rbp), %rsi
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	-480(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	movq	-520(%rbp), %rax
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L187:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L180:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L175
.L190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11977:
	.size	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0, .-_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB8141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L192
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L192:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L202
.L193:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L194
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L193
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8141:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB8129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L204
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L205:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L204
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L204
	movq	%r9, %rdi
.L207:
	cmpq	%rsi, %rbx
	jne	.L205
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L205
	cmpq	16(%rdi), %rbx
	jne	.L205
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L239
	testq	%rsi, %rsi
	je	.L209
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L209
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L209:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L204:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L240
.L212:
	cmpq	$0, 8(%rbx)
	je	.L203
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L216
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L241
.L216:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L203
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L203:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L221
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L209
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L208:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L243
.L210:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L241:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r10, %rax
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L240:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L244
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L212
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rsi, 2600(%r12)
	jmp	.L210
.L244:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8129:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN15DummyBaseObjectD2Ev,"axG",@progbits,_ZN15DummyBaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN15DummyBaseObjectD2Ev
	.type	_ZN15DummyBaseObjectD2Ev, @function
_ZN15DummyBaseObjectD2Ev:
.LFB11634:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE11634:
	.size	_ZN15DummyBaseObjectD2Ev, .-_ZN15DummyBaseObjectD2Ev
	.weak	_ZN15DummyBaseObjectD1Ev
	.set	_ZN15DummyBaseObjectD1Ev,_ZN15DummyBaseObjectD2Ev
	.section	.text._ZN15DummyBaseObjectD0Ev,"axG",@progbits,_ZN15DummyBaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN15DummyBaseObjectD0Ev
	.type	_ZN15DummyBaseObjectD0Ev, @function
_ZN15DummyBaseObjectD0Ev:
.LFB11636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11636:
	.size	_ZN15DummyBaseObjectD0Ev, .-_ZN15DummyBaseObjectD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD2Ev, @function
_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD2Ev:
.LFB11586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvE13ObjectWithPtr(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L250
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L258
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L259
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L250
	cmpb	$0, 9(%rdx)
	je	.L254
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L250:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L257
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L258
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L259
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L257
	cmpb	$0, 9(%rdx)
	je	.L261
	movq	(%rdi), %rax
	call	*8(%rax)
.L257:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	cmpb	$0, 8(%rdx)
	je	.L250
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L250
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L261:
	cmpb	$0, 8(%rdx)
	je	.L257
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L257
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11586:
	.size	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD2Ev, .-_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD2Ev
	.set	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD1Ev,_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD2Ev
	.align 2
	.p2align 4
	.type	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD0Ev, @function
_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD0Ev:
.LFB11588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvE13ObjectWithPtr(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L272
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L280
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L281
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L272
	cmpb	$0, 9(%rdx)
	je	.L276
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L272:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L279
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L280
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L281
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L279
	cmpb	$0, 9(%rdx)
	je	.L283
	movq	(%rdi), %rax
	call	*8(%rax)
.L279:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	cmpb	$0, 8(%rdx)
	je	.L272
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L272
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L283:
	cmpb	$0, 8(%rdx)
	je	.L279
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L279
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11588:
	.size	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD0Ev, .-_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD0Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB8131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8131:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB10134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L296
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L297
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L323
	.p2align 4,,10
	.p2align 3
.L296:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L302
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L308
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L324
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L302
.L303:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L310
.L324:
	lock subl	$1, 8(%r13)
	jne	.L310
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L310
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L303
	.p2align 4,,10
	.p2align 3
.L302:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L306
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L302
.L308:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L306
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L306
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L297:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L296
.L323:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L300
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L301:
	cmpl	$1, %eax
	jne	.L296
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L300:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L301
	.cfi_endproc
.LFE10134:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text._ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_:
.LFB10151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L342
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L343
.L328:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L332
.L325:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L333
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L329:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L328
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L334
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L330:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L332:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L325
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%edx, %edx
	jmp	.L330
	.cfi_endproc
.LFE10151:
	.size	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_
	.section	.text._ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_:
.LFB10158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	cmpq	%rax, (%rcx)
	je	.L363
	movq	.LC10(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC11(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L364
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L348:
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L349
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L350
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L351:
	movq	.LC10(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC12(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-560(%rbp), %xmm3
	movq	-536(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L365
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L354:
	movq	-384(%rbp), %rax
	leaq	-496(%rbp), %r14
	movq	$0, -504(%rbp)
	leaq	-512(%rbp), %r15
	movq	%r14, -512(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L355
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L356
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L357:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L358
	call	_ZdlPv@PLT
.L358:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L366
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L356:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L350:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L365:
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L349:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L357
.L366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10158:
	.size	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_
	.section	.text._ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC2EPS1_:
.LFB10163:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	testq	%rsi, %rsi
	je	.L376
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L379
	addl	$1, 4(%rax)
	movq	%rax, (%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$24, %edi
	movq	%rsi, -24(%rbp)
	call	_Znwm@PLT
	movq	-24(%rbp), %rsi
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rsi), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L371
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L370:
	movq	%rax, 24(%rsi)
	movb	%dl, 8(%rax)
	addl	$1, 4(%rax)
	movq	%rsi, 16(%rax)
	movq	%rax, (%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L370
	.cfi_endproc
.LFE10163:
	.size	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC1EPS1_,_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC2EPS1_
	.section	.text._ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB10627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-432(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -600(%rbp)
	movq	.LC10(%rip), %xmm1
	movq	%r8, -560(%rbp)
	movq	%rdi, -552(%rbp)
	movhps	.LC11(%rip), %xmm1
	movq	%r12, %rdi
	movq	%rsi, -584(%rbp)
	movq	%rdx, -592(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -544(%rbp)
	movq	%r15, -616(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-544(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-560(%rbp), %r8
	movq	%r14, %rdi
	movl	(%r8), %esi
	call	_ZNSolsEi@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -560(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L381
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L393
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L383:
	movq	.LC10(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC12(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -576(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L384
	call	_ZdlPv@PLT
.L384:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-544(%rbp), %xmm3
	movq	-520(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-600(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L385
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L386
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L387:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-576(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L388
	call	_ZdlPv@PLT
.L388:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rsi, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-552(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-560(%rbp), %r8
	movq	-592(%rbp), %rdx
	movq	-584(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L389
	call	_ZdlPv@PLT
.L389:
	movq	-480(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L380
	call	_ZdlPv@PLT
.L380:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	movq	-552(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L386:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L383
.L394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10627:
	.size	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.rodata.str1.1
.LC13:
	.string	""
.LC14:
	.string	"0"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"static_cast<Environment*>(arg)->base_object_count()"
	.align 8
.LC16:
	.string	"../test/cctest/test_base_object_ptr.cc"
	.text
	.p2align 4
	.type	_ZZN33BaseObjectPtrTest_Undetached_Test8TestBodyEvENUlPvE_4_FUNES0_, @function
_ZZN33BaseObjectPtrTest_Undetached_Test8TestBodyEvENUlPvE_4_FUNES0_:
.LFB8606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	2656(%rdi), %rax
	movl	$0, -64(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L413
	leaq	-56(%rbp), %rcx
	leaq	-64(%rbp), %r8
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -48(%rbp)
	je	.L414
.L398:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L395
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L395:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L415
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -48(%rbp)
	jne	.L398
.L414:
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-40(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L399
	movq	(%rax), %r8
.L399:
	leaq	-56(%rbp), %r12
	movl	$83, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L398
.L415:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8606:
	.size	_ZZN33BaseObjectPtrTest_Undetached_Test8TestBodyEvENUlPvE_4_FUNES0_, .-_ZZN33BaseObjectPtrTest_Undetached_Test8TestBodyEvENUlPvE_4_FUNES0_
	.p2align 4
	.type	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENUlPvE_4_FUNES0_, @function
_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENUlPvE_4_FUNES0_:
.LFB8638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	2656(%rdi), %rax
	movl	$0, -64(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L434
	leaq	-56(%rbp), %rcx
	leaq	-64(%rbp), %r8
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -48(%rbp)
	je	.L435
.L419:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L416
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L423
	call	_ZdlPv@PLT
.L423:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L416:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L436
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -48(%rbp)
	jne	.L419
.L435:
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-40(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L420
	movq	(%rax), %r8
.L420:
	leaq	-56(%rbp), %r12
	movl	$167, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L419
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L419
.L436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8638:
	.size	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENUlPvE_4_FUNES0_, .-_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENUlPvE_4_FUNES0_
	.section	.rodata.str1.1
.LC17:
	.string	"node"
.LC18:
	.string	"-p"
.LC19:
	.string	"process.version"
.LC20:
	.string	"%s"
.LC21:
	.string	"1"
.LC22:
	.string	"env->base_object_count()"
	.text
	.align 2
	.p2align 4
	.globl	_ZN33BaseObjectPtrTest_Undetached_Test8TestBodyEv
	.type	_ZN33BaseObjectPtrTest_Undetached_Test8TestBodyEv, @function
_ZN33BaseObjectPtrTest_Undetached_Test8TestBodyEv:
.LFB8604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC20(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC17(%rip), %rcx
	leaq	.LC18(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	.LC19(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r12
	call	malloc@PLT
	leal	22(%r12), %edi
	movl	$5, %r12d
	movslq	%edi, %rdi
	movq	%rax, %r13
	call	malloc@PLT
	movq	%rbx, -200(%rbp)
	leaq	-80(%rbp), %r10
	movl	%r12d, %ebx
	movq	%rax, 0(%r13)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L438:
	movq	(%r10,%r15), %rcx
	movq	%r10, -224(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -216(%rbp)
	call	strlen@PLT
	movslq	%ebx, %r8
	movq	-216(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r12d
	movq	%r8, %rdi
	xorl	%eax, %eax
	addq	0(%r13), %rdi
	movslq	%r12d, %rsi
	movq	%r8, -208(%rbp)
	addl	%r12d, %ebx
	call	snprintf@PLT
	movq	-208(%rbp), %r8
	addq	0(%r13), %r8
	movq	%r8, 0(%r13,%r15)
	addq	$8, %r15
	movq	-224(%rbp), %r10
	cmpq	$24, %r15
	jne	.L438
	movq	-144(%rbp), %r12
	xorl	%esi, %esi
	movq	-200(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L510
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L511
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r13, %r9
	movl	$3, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L512
	movq	16(%rbx), %rdi
	movq	%rax, %rdx
	leaq	_ZZN33BaseObjectPtrTest_Undetached_Test8TestBodyEvENUlPvE_4_FUNES0_(%rip), %rsi
	call	_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L513
.L442:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L514
	movl	$32, %edi
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, %r14
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%r14)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm2
	movq	$0, 24(%r14)
	movq	%rbx, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r14)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L515
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -208(%rbp)
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	-208(%rbp), %rdx
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	%r14, 16(%rax)
	movq	%rax, %rbx
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	2592(%r12), %rsi
	movq	$0, (%rax)
	movq	%r14, %rax
	divq	%rsi
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r8
	testq	%r9, %r9
	je	.L444
	movq	(%r9), %rax
	movq	32(%rax), %rcx
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L444
	movq	32(%rdi), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L444
	movq	%rdi, %rax
.L447:
	cmpq	%r14, %rcx
	jne	.L445
	cmpq	%r11, 8(%rax)
	jne	.L445
	cmpq	%rcx, 16(%rax)
	jne	.L445
	cmpq	$0, (%r9)
	je	.L444
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L444:
	movq	2608(%r12), %rdx
	movl	$1, %ecx
	leaq	2616(%r12), %rdi
	movq	%r8, -208(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rcx
	testb	%al, %al
	jne	.L448
	movq	2584(%r12), %r9
	movq	-208(%rbp), %r8
	movq	%r14, 32(%rbx)
	addq	%r9, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L458
.L523:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r8), %rax
	movq	%rbx, (%rax)
.L459:
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	leaq	-184(%rbp), %rdi
	movq	%r14, %rsi
	addq	$1, 2608(%r12)
	addq	$1, 2656(%r12)
	movq	%rax, (%r14)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	2656(%r12), %rax
	movl	$1, -176(%rbp)
	leaq	-160(%rbp), %rdi
	movq	%rax, -168(%rbp)
	cmpq	$1, %rax
	je	.L516
	leaq	-168(%rbp), %rcx
	leaq	-176(%rbp), %r8
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L517
.L463:
	movq	-152(%rbp), %r14
	testq	%r14, %r14
	je	.L466
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L466:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L469
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L518
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L519
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L469
	cmpb	$0, 9(%rdx)
	je	.L473
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%r12, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	0(%r13), %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	movq	-232(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L463
.L517:
	leaq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L464
	movq	(%rax), %r8
.L464:
	leaq	-176(%rbp), %r14
	movl	$87, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L463
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L448:
	cmpq	$1, %rdx
	je	.L521
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L522
	leaq	0(,%rdx,8), %rdx
	movq	%rcx, -216(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -208(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-216(%rbp), %rcx
	movq	%rax, %r9
	leaq	2632(%r12), %rax
	movq	%rax, -208(%rbp)
.L451:
	movq	2600(%r12), %rdi
	movq	$0, 2600(%r12)
	testq	%rdi, %rdi
	je	.L453
	xorl	%r10d, %r10d
	leaq	2600(%r12), %r11
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L455:
	movq	(%r8), %rdx
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
.L456:
	testq	%rdi, %rdi
	je	.L453
.L454:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%rcx
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L455
	movq	2600(%r12), %r8
	movq	%r8, (%rsi)
	movq	%rsi, 2600(%r12)
	movq	%r11, (%rax)
	cmpq	$0, (%rsi)
	je	.L479
	movq	%rsi, (%r9,%r10,8)
	movq	%rdx, %r10
	testq	%rdi, %rdi
	jne	.L454
	.p2align 4,,10
	.p2align 3
.L453:
	movq	2584(%r12), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L457
	movq	%rcx, -216(%rbp)
	movq	%r9, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rcx
	movq	-208(%rbp), %r9
.L457:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%rcx, 2592(%r12)
	divq	%rcx
	movq	%r9, 2584(%r12)
	movq	%r14, 32(%rbx)
	leaq	0(,%rdx,8), %r8
	addq	%r9, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L523
.L458:
	movq	2600(%r12), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 2600(%r12)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L460
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%rbx, (%r9,%rdx,8)
.L460:
	leaq	2600(%r12), %rax
	movq	%rax, (%r8)
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L510:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%rdx, %r10
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%rdi, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L514:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L515:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L473:
	cmpb	$0, 8(%rdx)
	je	.L469
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L469
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L519:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L521:
	leaq	2632(%r12), %r9
	movq	$0, 2632(%r12)
	movq	%r9, -208(%rbp)
	jmp	.L451
.L520:
	call	__stack_chk_fail@PLT
.L522:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8604:
	.size	_ZN33BaseObjectPtrTest_Undetached_Test8TestBodyEv, .-_ZN33BaseObjectPtrTest_Undetached_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN37BaseObjectPtrTest_ScopedDetached_Test8TestBodyEv
	.type	_ZN37BaseObjectPtrTest_ScopedDetached_Test8TestBodyEv, @function
_ZN37BaseObjectPtrTest_ScopedDetached_Test8TestBodyEv:
.LFB8590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC20(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$5, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	.LC17(%rip), %rbx
	subq	$200, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC18(%rip), %rax
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %rbx
	movq	%rax, %xmm1
	leaq	.LC19(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r12
	call	malloc@PLT
	leal	22(%r12), %edi
	movl	$8, %r12d
	movslq	%edi, %rdi
	movq	%rax, %r13
	call	malloc@PLT
	movq	%rbx, -200(%rbp)
	movq	%r13, %rbx
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	movq	%rax, 0(%r13)
	movl	%r14d, %r13d
	.p2align 4,,10
	.p2align 3
.L525:
	movq	-200(%rbp), %rax
	movslq	%r13d, %r14
	movq	(%rax,%r12), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -216(%rbp)
	call	strlen@PLT
	movq	(%rbx), %rdi
	movq	-216(%rbp), %rcx
	movq	%r15, %rdx
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	movslq	%r8d, %rsi
	addq	%r14, %rdi
	movl	%r8d, -208(%rbp)
	call	snprintf@PLT
	movl	-208(%rbp), %r8d
	addq	(%rbx), %r14
	movq	%r14, (%rbx,%r12)
	addq	$8, %r12
	addl	%r8d, %r13d
	cmpq	$24, %r12
	jne	.L525
	movq	-144(%rbp), %r12
	xorl	%esi, %esi
	movq	%rbx, %r13
	movq	%r12, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L633
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L634
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movl	$3, %r8d
	movq	%rbx, %rcx
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L635
	movq	2656(%rax), %rax
	movl	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	leaq	-160(%rbp), %rax
	movq	%rax, -208(%rbp)
	je	.L636
	leaq	-168(%rbp), %rcx
	leaq	-176(%rbp), %r8
	movq	%rax, %rdi
	leaq	.LC14(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L637
.L531:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L534
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L534:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rsi
	popq	%r8
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L638
.L536:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L639
	movl	$32, %edi
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%rbx, %rsi
	movq	%rax, %r15
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm2
	movq	$0, 24(%r15)
	movq	%rbx, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r15)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L640
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -216(%rbp)
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	-216(%rbp), %rdx
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r9
	movq	%r15, 16(%rax)
	movq	%rax, %rbx
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r9, 8(%rax)
	movq	2592(%r12), %rsi
	movq	$0, (%rax)
	movq	%r15, %rax
	divq	%rsi
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %r10
	movq	%rdx, %r11
	leaq	0(,%rdx,8), %r8
	testq	%r10, %r10
	je	.L538
	movq	(%r10), %rax
	movq	32(%rax), %rcx
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L539:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L538
	movq	32(%rdi), %rcx
	movq	%rax, %r10
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r11
	jne	.L538
	movq	%rdi, %rax
.L541:
	cmpq	%rcx, %r15
	jne	.L539
	cmpq	%r9, 8(%rax)
	jne	.L539
	cmpq	%r15, 16(%rax)
	jne	.L539
	cmpq	$0, (%r10)
	je	.L538
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L636:
	movq	%rax, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L531
.L637:
	leaq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L532
	movq	(%rax), %r8
.L532:
	leaq	-176(%rbp), %r15
	movl	$50, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L531
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L538:
	movq	2608(%r12), %rdx
	movl	$1, %ecx
	leaq	2616(%r12), %rdi
	movq	%r8, -216(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rcx
	testb	%al, %al
	jne	.L542
	movq	2584(%r12), %r10
	movq	-216(%rbp), %r8
	movq	%r15, 32(%rbx)
	leaq	(%r10,%r8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L552
.L652:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%rcx), %rax
	movq	%rbx, (%rax)
.L553:
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	leaq	-184(%rbp), %rdi
	movq	%r15, %rsi
	addq	$1, 2608(%r12)
	addq	$1, 2656(%r12)
	movq	%rax, (%r15)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-184(%rbp), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L641
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L581
	movb	$1, 9(%rax)
	movq	2656(%r12), %rax
	movl	$1, -176(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	$1, %rax
	je	.L642
	movq	-208(%rbp), %rdi
	leaq	-168(%rbp), %rcx
	leaq	-176(%rbp), %r8
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L643
.L559:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L562
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L562:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L565
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L644
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L645
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L565
	cmpb	$0, 9(%rdx)
	jne	.L646
	cmpb	$0, 8(%rdx)
	je	.L565
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L565
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L565:
	movq	2656(%r12), %rax
	movl	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L647
	movq	-208(%rbp), %rdi
	leaq	-168(%rbp), %rcx
	leaq	-176(%rbp), %r8
	leaq	.LC14(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L648
.L573:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L576
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L577
	call	_ZdlPv@PLT
.L577:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L576:
	movq	%r12, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	0(%r13), %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	movq	-224(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L649
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	movq	-208(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L559
.L643:
	leaq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L560
	movq	(%rax), %r8
.L560:
	leaq	-176(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L559
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L542:
	cmpq	$1, %rdx
	je	.L650
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L651
	leaq	0(,%rdx,8), %rdx
	movq	%rcx, -232(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -216(%rbp)
	call	_Znwm@PLT
	movq	-216(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-232(%rbp), %rcx
	movq	%rax, %r10
	leaq	2632(%r12), %rax
	movq	%rax, -216(%rbp)
.L545:
	movq	2600(%r12), %rdi
	movq	$0, 2600(%r12)
	testq	%rdi, %rdi
	je	.L547
	xorl	%r8d, %r8d
	leaq	2600(%r12), %r9
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L549:
	movq	(%r11), %rdx
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
.L550:
	testq	%rdi, %rdi
	je	.L547
.L548:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%rcx
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L549
	movq	2600(%r12), %r11
	movq	%r11, (%rsi)
	movq	%rsi, 2600(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rsi)
	je	.L585
	movq	%rsi, (%r10,%r8,8)
	movq	%rdx, %r8
	testq	%rdi, %rdi
	jne	.L548
	.p2align 4,,10
	.p2align 3
.L547:
	movq	2584(%r12), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L551
	movq	%rcx, -232(%rbp)
	movq	%r10, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rcx
	movq	-216(%rbp), %r10
.L551:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%rcx, 2592(%r12)
	divq	%rcx
	movq	%r10, 2584(%r12)
	movq	%r15, 32(%rbx)
	leaq	0(,%rdx,8), %r8
	leaq	(%r10,%r8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L652
.L552:
	movq	2600(%r12), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 2600(%r12)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L554
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%rbx, (%r10,%rdx,8)
.L554:
	leaq	2600(%r12), %rax
	movq	%rax, (%rcx)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L647:
	movq	-208(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L573
.L648:
	leaq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L574
	movq	(%rax), %r8
.L574:
	leaq	-176(%rbp), %r15
	movl	$55, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L573
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L641:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	movw	%cx, 8(%rax)
	movq	8(%rbx), %rcx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L586
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L556:
	movb	%cl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
.L581:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L634:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%rdx, %r8
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L639:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%rax, -216(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-216(%rbp), %rdi
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L640:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L646:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L644:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L586:
	xorl	%ecx, %ecx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	2632(%r12), %r10
	movq	$0, 2632(%r12)
	movq	%r10, -216(%rbp)
	jmp	.L545
.L649:
	call	__stack_chk_fail@PLT
.L651:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8590:
	.size	_ZN37BaseObjectPtrTest_ScopedDetached_Test8TestBodyEv, .-_ZN37BaseObjectPtrTest_ScopedDetached_Test8TestBodyEv
	.section	.rodata.str1.1
.LC23:
	.string	"weak_ptr.get()"
	.text
	.align 2
	.p2align 4
	.globl	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test8TestBodyEv
	.type	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test8TestBodyEv, @function
_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test8TestBodyEv:
.LFB8597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.LC20(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	$5, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC17(%rip), %rcx
	leaq	.LC18(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	.LC19(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$8, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r13
	call	malloc@PLT
	movq	%r13, %r15
	leaq	-80(%rbp), %r9
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	movq	%rax, 0(%r13)
	movl	%r12d, %r13d
	.p2align 4,,10
	.p2align 3
.L654:
	movq	(%r9,%rbx), %rcx
	movq	%r9, -216(%rbp)
	movslq	%r13d, %r12
	movq	%rcx, %rdi
	movq	%rcx, -208(%rbp)
	call	strlen@PLT
	movq	(%r15), %rdi
	movq	-208(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	addq	%r12, %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -200(%rbp)
	call	snprintf@PLT
	addq	(%r15), %r12
	movl	-200(%rbp), %r8d
	movq	%r12, (%r15,%rbx)
	addq	$8, %rbx
	movq	-216(%rbp), %r9
	addl	%r8d, %r13d
	cmpq	$24, %rbx
	jne	.L654
	movq	-144(%rbp), %r12
	xorl	%esi, %esi
	movq	%r15, %r13
	movq	%r12, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L787
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L788
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r15, %r9
	movl	$3, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L789
	movq	2656(%rax), %rax
	movq	$0, -192(%rbp)
	movl	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	leaq	-160(%rbp), %rax
	movq	%rax, -208(%rbp)
	je	.L790
	leaq	-168(%rbp), %rcx
	leaq	-176(%rbp), %r8
	movq	%rax, %rdi
	leaq	.LC14(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L791
.L660:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L663
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L663:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rsi
	popq	%r8
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L792
.L665:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L793
	movl	$32, %edi
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm2
	movq	$0, 24(%rbx)
	movq	%r15, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L794
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -216(%rbp)
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	-216(%rbp), %rdx
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	%rbx, 16(%rax)
	movq	%rax, %r15
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	2592(%r12), %rsi
	movq	$0, (%rax)
	movq	%rbx, %rax
	divq	%rsi
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r8
	testq	%r9, %r9
	je	.L667
	movq	(%r9), %rax
	movq	32(%rax), %rcx
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L668:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L667
	movq	32(%rdi), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L667
	movq	%rdi, %rax
.L670:
	cmpq	%rcx, %rbx
	jne	.L668
	cmpq	%r11, 8(%rax)
	jne	.L668
	cmpq	%rbx, 16(%rax)
	jne	.L668
	cmpq	$0, (%r9)
	je	.L667
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L790:
	movq	%rax, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L660
.L791:
	leaq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L661
	movq	(%rax), %r8
.L661:
	leaq	-176(%rbp), %r15
	movl	$66, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L660
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L667:
	movq	2608(%r12), %rdx
	movl	$1, %ecx
	leaq	2616(%r12), %rdi
	movq	%r8, -216(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rcx
	testb	%al, %al
	jne	.L671
	movq	2584(%r12), %r9
	movq	-216(%rbp), %r8
	movq	%rbx, 32(%r15)
	leaq	(%r9,%r8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L681
.L809:
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%rcx), %rax
	movq	%r15, (%rax)
.L682:
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%rbx, %rsi
	leaq	-184(%rbp), %rdi
	addq	$1, 2608(%r12)
	addq	$1, 2656(%r12)
	movq	%rax, (%rbx)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-184(%rbp), %rsi
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L795
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L721
	movq	-192(%rbp), %rdi
	movb	$1, 9(%rax)
	testq	%rdi, %rdi
	je	.L686
	movq	16(%rdi), %rdx
	cmpq	%rsi, %rdx
	je	.L687
	subl	$1, 4(%rdi)
	jne	.L686
	testq	%rdx, %rdx
	je	.L796
.L686:
	leaq	-192(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC1EPS1_
.L687:
	movq	2656(%r12), %rax
	movl	$1, -176(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	$1, %rax
	je	.L797
	movq	-208(%rbp), %rdi
	leaq	-168(%rbp), %rcx
	leaq	-176(%rbp), %r8
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L798
.L690:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L693
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L694
	call	_ZdlPv@PLT
.L694:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L693:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L696
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L799
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L800
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L696
	cmpb	$0, 9(%rdx)
	jne	.L801
	cmpb	$0, 8(%rdx)
	je	.L696
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L696
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L696:
	movq	-192(%rbp), %rax
	testq	%rax, %rax
	je	.L702
	movq	16(%rax), %rax
.L702:
	movq	-208(%rbp), %rdi
	leaq	-168(%rbp), %rbx
	leaq	.LC23(%rip), %rsi
	movq	%rax, -168(%rbp)
	movq	%rbx, %rdx
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -160(%rbp)
	je	.L802
.L703:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L706
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L707
	call	_ZdlPv@PLT
.L707:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L706:
	movq	2656(%r12), %rax
	movl	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L803
	movq	-208(%rbp), %rdi
	leaq	-176(%rbp), %r8
	movq	%rbx, %rcx
	leaq	.LC14(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L804
.L710:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L713
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L713:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L716
	subl	$1, 4(%rdi)
	je	.L805
.L716:
	movq	%r12, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	0(%r13), %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	movq	-224(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L806
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore_state
	cmpq	$0, 16(%rdi)
	jne	.L716
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L797:
	movq	-208(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L690
.L798:
	leaq	-168(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L691
	movq	(%rax), %r8
.L691:
	leaq	-176(%rbp), %r15
	movl	$70, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L690
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L671:
	cmpq	$1, %rdx
	je	.L807
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L808
	leaq	0(,%rdx,8), %rdx
	movq	%rcx, -232(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -216(%rbp)
	call	_Znwm@PLT
	movq	-216(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-232(%rbp), %rcx
	movq	%rax, %r9
	leaq	2632(%r12), %rax
	movq	%rax, -216(%rbp)
.L674:
	movq	2600(%r12), %rdi
	movq	$0, 2600(%r12)
	testq	%rdi, %rdi
	je	.L676
	xorl	%r11d, %r11d
	leaq	2600(%r12), %r8
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L678:
	movq	(%r10), %rdx
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
.L679:
	testq	%rdi, %rdi
	je	.L676
.L677:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%rcx
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L678
	movq	2600(%r12), %r10
	movq	%r10, (%rsi)
	movq	%rsi, 2600(%r12)
	movq	%r8, (%rax)
	cmpq	$0, (%rsi)
	je	.L725
	movq	%rsi, (%r9,%r11,8)
	movq	%rdx, %r11
	testq	%rdi, %rdi
	jne	.L677
	.p2align 4,,10
	.p2align 3
.L676:
	movq	2584(%r12), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L680
	movq	%rcx, -232(%rbp)
	movq	%r9, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rcx
	movq	-216(%rbp), %r9
.L680:
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	%rcx, 2592(%r12)
	divq	%rcx
	movq	%r9, 2584(%r12)
	movq	%rbx, 32(%r15)
	leaq	0(,%rdx,8), %r8
	leaq	(%r9,%r8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L809
.L681:
	movq	2600(%r12), %rax
	movq	%rax, (%r15)
	movq	%r15, 2600(%r12)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L683
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r15, (%r9,%rdx,8)
.L683:
	leaq	2600(%r12), %rax
	movq	%rax, (%rcx)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L803:
	movq	-208(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L710
.L804:
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L711
	movq	(%rax), %r8
.L711:
	leaq	-176(%rbp), %r15
	movl	$73, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L710
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L802:
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L704
	movq	(%rax), %r8
.L704:
	leaq	-176(%rbp), %r15
	movl	$72, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L703
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L787:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L795:
	movl	$24, %edi
	movq	%rsi, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movq	8(%rsi), %rdx
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L726
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L685:
	movb	%dl, 8(%rax)
	movq	%rsi, 16(%rax)
	movq	%rax, 24(%rsi)
.L721:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L788:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L725:
	movq	%rdx, %r11
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L793:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L792:
	movq	%rax, -216(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-216(%rbp), %rdi
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L801:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L799:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L796:
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-184(%rbp), %rsi
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L800:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L726:
	xorl	%edx, %edx
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L807:
	leaq	2632(%r12), %r9
	movq	$0, 2632(%r12)
	movq	%r9, -216(%rbp)
	jmp	.L674
.L806:
	call	__stack_chk_fail@PLT
.L808:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8597:
	.size	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test8TestBodyEv, .-_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test8TestBodyEv
	.section	.rodata.str1.1
.LC24:
	.string	"ptr.get()"
.LC25:
	.string	"ptr2.get()"
.LC26:
	.string	"weak_ptr2.get()"
	.text
	.align 2
	.p2align 4
	.globl	_ZN31BaseObjectPtrTest_Moveable_Test8TestBodyEv
	.type	_ZN31BaseObjectPtrTest_Moveable_Test8TestBodyEv, @function
_ZN31BaseObjectPtrTest_Moveable_Test8TestBodyEv:
.LFB8621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.LC20(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$5, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC17(%rip), %rcx
	leaq	.LC18(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	.LC19(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$8, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r15
	movq	%rax, -200(%rbp)
	call	malloc@PLT
	leaq	-80(%rbp), %r8
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	movq	%rax, (%r15)
	movq	%r8, %r15
	.p2align 4,,10
	.p2align 3
.L811:
	movq	(%r15,%rbx), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -216(%rbp)
	call	strlen@PLT
	movslq	%r12d, %r9
	movq	-216(%rbp), %rcx
	movq	%r13, %rdx
	leal	1(%rax), %r14d
	movq	-200(%rbp), %rax
	movq	%r9, %rdi
	movq	%r9, -208(%rbp)
	movslq	%r14d, %rsi
	addl	%r14d, %r12d
	addq	(%rax), %rdi
	xorl	%eax, %eax
	call	snprintf@PLT
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %r9
	addq	(%rax), %r9
	movq	%r9, (%rax,%rbx)
	addq	$8, %rbx
	cmpq	$24, %rbx
	jne	.L811
	movq	-144(%rbp), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -208(%rbp)
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1043
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L1044
	movq	-200(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	movl	$3, %r8d
	movl	$1, %edx
	movq	%rcx, %r9
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1045
	subq	$8, %rsp
	movq	2680(%rax), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rax), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rsi
	popq	%r8
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1046
.L815:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1047
	movl	$32, %edi
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%r14, %rsi
	movq	%rax, %r13
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm2
	movq	$0, 24(%r13)
	movq	%r14, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r13)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1048
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rbx
	movl	$40, %edi
	leaq	1(%rbx), %rax
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r10
	xorl	%edx, %edx
	movq	%rbx, 24(%rax)
	movq	%rax, %r14
	movq	%r10, 8(%rax)
	movq	%r13, 16(%rax)
	movq	2592(%r12), %rsi
	movq	$0, (%rax)
	movq	%r13, %rax
	divq	%rsi
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %rbx
	testq	%r8, %r8
	je	.L817
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L818:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L817
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L817
	movq	%rdi, %rax
.L820:
	cmpq	%rcx, %r13
	jne	.L818
	cmpq	%r10, 8(%rax)
	jne	.L818
	cmpq	%r13, 16(%rax)
	jne	.L818
	cmpq	$0, (%r8)
	je	.L817
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L817:
	movq	2608(%r12), %rdx
	leaq	2616(%r12), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r15
	testb	%al, %al
	jne	.L821
	movq	2584(%r12), %rcx
	movq	%r13, 32(%r14)
	addq	%rcx, %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L831
.L1070:
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%r14, (%rax)
.L832:
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	leaq	-192(%rbp), %rdi
	movq	%r13, %rsi
	addq	$1, 2608(%r12)
	addq	$1, 2656(%r12)
	movq	%rax, 0(%r13)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-192(%rbp), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L1049
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L920
	movb	$1, 9(%rax)
	movq	2656(%r12), %rax
	leaq	-160(%rbp), %r15
	movl	$1, -176(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	$1, %rax
	je	.L1050
	leaq	-176(%rbp), %r13
	leaq	-168(%rbp), %r14
	movq	%r15, %rdi
	movq	%r13, %r8
	movq	%r14, %rcx
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1051
.L838:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L841
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L842
	call	_ZdlPv@PLT
.L842:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L841:
	movq	-192(%rbp), %rsi
	leaq	-184(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC1EPS1_
	movq	-192(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L843
	movq	16(%rax), %rax
.L843:
	movq	%r14, %r8
	movq	%r13, %rcx
	leaq	.LC24(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1052
.L844:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L847
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L848
	call	_ZdlPv@PLT
.L848:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L847:
	movq	-192(%rbp), %rax
	movq	$0, -192(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rax, -168(%rbp)
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L849
	movq	16(%rax), %rax
.L849:
	movq	%r14, %r8
	movq	%r13, %rcx
	leaq	.LC25(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1053
.L850:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L853
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L854
	call	_ZdlPv@PLT
.L854:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L853:
	movq	-192(%rbp), %rax
	movq	%r14, %rdx
	leaq	.LC24(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -160(%rbp)
	je	.L1054
.L855:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L858
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L858:
	movq	-184(%rbp), %rbx
	movq	-232(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rbx, %rbx
	je	.L929
	movq	16(%rbx), %rax
.L860:
	movq	%r14, %r8
	movq	%r13, %rcx
	leaq	.LC25(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC26(%rip), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1055
.L861:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L864
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L865
	movq	%r8, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %r8
.L865:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L864:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L866
	movq	16(%rax), %rax
.L866:
	movq	%r14, %rdx
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -160(%rbp)
	je	.L1056
.L867:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L870
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L871
	movq	%r8, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %r8
.L871:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L870:
	movq	2656(%r12), %rax
	movl	$1, -176(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	$1, %rax
	je	.L1057
	movq	%r13, %r8
	movq	%r14, %rcx
	leaq	.LC21(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1058
.L874:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L877
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L878
	movq	%r8, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %r8
.L878:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L877:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-232(%rbp), %rax
	testq	%rax, %rax
	je	.L880
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L904
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L905
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L880
	cmpb	$0, 9(%rdx)
	jne	.L1059
	cmpb	$0, 8(%rdx)
	je	.L880
	movq	-232(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L880
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L880:
	movq	-160(%rbp), %rax
	movq	%rax, -232(%rbp)
	testq	%rbx, %rbx
	je	.L933
	movq	16(%rbx), %rax
.L886:
	movq	%r14, %rdx
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -160(%rbp)
	je	.L1060
.L887:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L890
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L891
	movq	%r8, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %r8
.L891:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L890:
	movq	2656(%r12), %rax
	movl	$0, -176(%rbp)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L1061
	movq	%r13, %r8
	movq	%r14, %rcx
	leaq	.LC14(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1062
.L894:
	movq	-152(%rbp), %r13
	testq	%r13, %r13
	je	.L897
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L897:
	testq	%rbx, %rbx
	je	.L900
	subl	$1, 4(%rbx)
	jne	.L900
	cmpq	$0, 16(%rbx)
	je	.L1063
.L900:
	movq	-232(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L903
	movq	24(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L904
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L905
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L903
	cmpb	$0, 9(%rdx)
	jne	.L1064
	cmpb	$0, 8(%rdx)
	je	.L903
	movq	8(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L903
	movq	%rcx, %rsi
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	xorl	%ecx, %ecx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L903:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L910
	subl	$1, 4(%rdi)
	jne	.L910
	cmpq	$0, 16(%rdi)
	je	.L1065
.L910:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L913
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L904
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L905
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L913
	cmpb	$0, 9(%rdx)
	jne	.L1066
	cmpb	$0, 8(%rdx)
	je	.L913
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L913
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r12, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-216(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	-208(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-200(%rbp), %rbx
	movq	(%rbx), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	movq	-224(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1067
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L904:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	%r15, %rdi
	leaq	-176(%rbp), %r13
	leaq	-168(%rbp), %r14
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L838
.L1051:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L839
	movq	(%rax), %r8
.L839:
	movl	$129, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L838
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	%r15, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L874
.L1058:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L875
	movq	(%rax), %r8
.L875:
	movl	$140, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L874
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L845
	movq	(%rax), %r8
.L845:
	movl	$131, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L844
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L868
	movq	(%rax), %r8
.L868:
	movl	$139, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L867
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L856
	movq	(%rax), %r8
.L856:
	movl	$135, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L855
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L851
	movq	(%rax), %r8
.L851:
	movl	$134, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L850
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L862
	movq	(%rax), %r8
.L862:
	movl	$138, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L861
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L821:
	cmpq	$1, %rdx
	je	.L1068
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1069
	leaq	0(,%rdx,8), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%r12), %r11
	movq	%rax, %rcx
.L824:
	movq	2600(%r12), %rdi
	movq	$0, 2600(%r12)
	testq	%rdi, %rdi
	je	.L826
	xorl	%r9d, %r9d
	leaq	2600(%r12), %r10
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L828:
	movq	(%r8), %rdx
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
.L829:
	testq	%rdi, %rdi
	je	.L826
.L827:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%r15
	leaq	(%rcx,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L828
	movq	2600(%r12), %r8
	movq	%r8, (%rsi)
	movq	%rsi, 2600(%r12)
	movq	%r10, (%rax)
	cmpq	$0, (%rsi)
	je	.L923
	movq	%rsi, (%rcx,%r9,8)
	movq	%rdx, %r9
	testq	%rdi, %rdi
	jne	.L827
	.p2align 4,,10
	.p2align 3
.L826:
	movq	2584(%r12), %rdi
	cmpq	%r11, %rdi
	je	.L830
	movq	%rcx, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rcx
.L830:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r15, 2592(%r12)
	divq	%r15
	movq	%rcx, 2584(%r12)
	movq	%r13, 32(%r14)
	leaq	0(,%rdx,8), %rbx
	addq	%rcx, %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L1070
.L831:
	movq	2600(%r12), %rax
	movq	%rax, (%r14)
	movq	%r14, 2600(%r12)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L833
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r14, (%rcx,%rdx,8)
.L833:
	leaq	2600(%r12), %rax
	movq	%rax, (%rbx)
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	%r15, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L894
.L1062:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L895
	movq	(%rax), %r8
.L895:
	movl	$145, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L894
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L888
	movq	(%rax), %r8
.L888:
	movl	$144, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L887
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L1043:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1049:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L924
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L835:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
.L920:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1044:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L923:
	movq	%rdx, %r9
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L1045:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	%rax, -232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %rdi
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L1047:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1048:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	-232(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	(%rcx), %rax
	movq	%rcx, %rdi
	call	*8(%rax)
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L1063:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L1065:
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L905:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L929:
	xorl	%eax, %eax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L933:
	xorl	%eax, %eax
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L924:
	xorl	%edx, %edx
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L1068:
	leaq	2632(%r12), %rcx
	movq	$0, 2632(%r12)
	movq	%rcx, %r11
	jmp	.L824
.L1067:
	call	__stack_chk_fail@PLT
.L1069:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8621:
	.size	_ZN31BaseObjectPtrTest_Moveable_Test8TestBodyEv, .-_ZN31BaseObjectPtrTest_Moveable_Test8TestBodyEv
	.section	.text._ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC2EPS1_:
.LFB10640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L1088
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L1089
.L1074:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L1078
.L1071:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1089:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1079
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1075:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L1074
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1080
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1076:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L1078:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1071
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L1088:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1079:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1080:
	xorl	%edx, %edx
	jmp	.L1076
	.cfi_endproc
.LFE10640:
	.size	_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC2EPS1_
	.section	.rodata.str1.1
.LC27:
	.string	"3"
	.text
	.align 2
	.p2align 4
	.globl	_ZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEv
	.type	_ZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEv, @function
_ZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEv:
.LFB8630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	.LC20(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	.LC17(%rip), %rbx
	subq	$200, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC18(%rip), %rax
	movq	%rbx, %xmm0
	movq	%rax, %xmm1
	leaq	.LC19(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r14
	call	malloc@PLT
	movq	%r13, -184(%rbp)
	leaq	-80(%rbp), %r10
	movl	%ebx, %r13d
	movq	%rax, (%r14)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	(%r10,%r15), %rcx
	movq	%r10, -208(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -200(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r8
	movq	-200(%rbp), %rcx
	movq	%r12, %rdx
	leal	1(%rax), %ebx
	movq	%r8, %rdi
	xorl	%eax, %eax
	addq	(%r14), %rdi
	movslq	%ebx, %rsi
	movq	%r8, -192(%rbp)
	addl	%ebx, %r13d
	call	snprintf@PLT
	movq	-192(%rbp), %r8
	addq	(%r14), %r8
	movq	%r8, (%r14,%r15)
	addq	$8, %r15
	movq	-208(%rbp), %r10
	cmpq	$24, %r15
	jne	.L1091
	movq	-144(%rbp), %r12
	xorl	%esi, %esi
	movq	-184(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1289
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L1290
	movq	-112(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r14, %r9
	movl	$3, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1291
	movq	16(%r13), %rdi
	movq	%rax, %rdx
	leaq	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENUlPvE_4_FUNES0_(%rip), %rsi
	call	_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1292
.L1095:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1293
	movl	$48, %edi
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, %r13
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm2
	movq	$0, 24(%r13)
	movq	%r15, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r13)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1182
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	-192(%rbp), %rdx
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	movq	%rdx, 24(%r15)
	xorl	%edx, %edx
	movq	%rax, 8(%r15)
	movq	%r13, %rax
	movq	%r13, 16(%r15)
	movq	2592(%r12), %rsi
	divq	%rsi
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r8
	testq	%r9, %r9
	je	.L1097
	movq	(%r9), %rax
	movq	32(%rax), %rcx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1097
	movq	32(%rdi), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L1097
	movq	%rdi, %rax
.L1100:
	cmpq	%rcx, %r13
	jne	.L1098
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rcx
	cmpq	%rcx, 8(%rax)
	jne	.L1098
	cmpq	%r13, 16(%rax)
	jne	.L1098
	cmpq	$0, (%r9)
	je	.L1097
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1097:
	leaq	2616(%r12), %rax
	movq	2608(%r12), %rdx
	movl	$1, %ecx
	movq	%r8, -192(%rbp)
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r9
	testb	%al, %al
	jne	.L1101
	movq	2584(%r12), %r10
	movq	-192(%rbp), %r8
	movq	%r13, 32(%r15)
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L1111
.L1305:
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%r8), %rax
	movq	%r15, (%rax)
.L1112:
	subq	$8, %rsp
	pxor	%xmm0, %xmm0
	movl	$1, %r9d
	xorl	%ecx, %ecx
	addq	$1, 2608(%r12)
	leaq	16+_ZTVZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvE13ObjectWithPtr(%rip), %rax
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	addq	$1, 2656(%r12)
	movq	352(%r12), %rdi
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	movups	%xmm0, 32(%r13)
	movq	%rax, 0(%r13)
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1294
.L1114:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L1288
	movl	$32, %edi
	movq	%rax, -192(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %r8
	movq	352(%r12), %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%r15)
	movq	%r8, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-192(%rbp), %r8
	movq	%r12, %xmm3
	movq	$0, 24(%r15)
	movq	%rax, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movq	%r8, %rdi
	movups	%xmm0, 8(%r15)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-192(%rbp), %r8
	testl	%eax, %eax
	jle	.L1182
	movq	%r8, %rdi
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	-192(%rbp), %rdx
	movq	$0, (%rax)
	movq	%rax, %r8
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	movq	%rdx, 24(%r8)
	xorl	%edx, %edx
	movq	%rax, 8(%r8)
	movq	%r15, %rax
	movq	%r15, 16(%r8)
	movq	2592(%r12), %r10
	divq	%r10
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r11
	leaq	0(,%rdx,8), %r9
	testq	%rdi, %rdi
	je	.L1116
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1116
	movq	32(%rsi), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r10
	cmpq	%rdx, %r11
	jne	.L1116
	movq	%rsi, %rax
.L1119:
	cmpq	%rcx, %r15
	jne	.L1117
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rcx
	cmpq	%rcx, 8(%rax)
	jne	.L1117
	cmpq	%r15, 16(%rax)
	jne	.L1117
	cmpq	$0, (%rdi)
	je	.L1116
.L1181:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	2608(%r12), %rdx
	movq	-200(%rbp), %rdi
	movl	$1, %ecx
	movq	%r10, %rsi
	movq	%r8, -192(%rbp)
	movq	%r9, -208(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-192(%rbp), %r8
	testb	%al, %al
	movq	%rdx, %rcx
	jne	.L1120
	movq	2584(%r12), %r10
	movq	-208(%rbp), %r9
.L1121:
	addq	%r10, %r9
	movq	%r15, 32(%r8)
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L1130
	movq	(%rax), %rax
	movq	%rax, (%r8)
	movq	(%r9), %rax
	movq	%r8, (%rax)
.L1131:
	addq	$1, 2608(%r12)
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%r15, %rsi
	addq	$1, 2656(%r12)
	movq	%rax, (%r15)
	leaq	-168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-168(%rbp), %r15
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L1295
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L1188
	movq	32(%r13), %rdi
	movb	$1, 9(%rax)
	cmpq	%r15, %rdi
	je	.L1135
	testq	%rdi, %rdi
	je	.L1136
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1142
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1143
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L1296
.L1136:
	movq	%r15, %rsi
	leaq	32(%r13), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC1EPS1_
	movq	-168(%rbp), %r15
	testq	%r15, %r15
	jne	.L1135
.L1141:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1297
.L1147:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L1288
	movl	$32, %edi
	movq	%rax, -208(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %r8
	movq	352(%r12), %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%r15)
	movq	%r8, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-208(%rbp), %r8
	movq	%r12, %xmm4
	movq	$0, 24(%r15)
	movq	%rax, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movq	%r8, %rdi
	movups	%xmm0, 8(%r15)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-208(%rbp), %r8
	testl	%eax, %eax
	jle	.L1182
	movq	%r8, %rdi
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -208(%rbp)
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	-208(%rbp), %rdx
	movq	$0, (%rax)
	movq	%rax, %r8
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	movq	%rdx, 24(%r8)
	xorl	%edx, %edx
	movq	%rax, 8(%r8)
	movq	%r15, %rax
	movq	%r15, 16(%r8)
	movq	2592(%r12), %r9
	divq	%r9
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r11
	testq	%rdi, %rdi
	je	.L1149
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1149
	movq	32(%rsi), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L1149
	movq	%rsi, %rax
.L1152:
	cmpq	%r15, %rcx
	jne	.L1150
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rdx
	cmpq	%rdx, 8(%rax)
	jne	.L1150
	cmpq	%rcx, 16(%rax)
	jne	.L1150
	cmpq	$0, (%rdi)
	jne	.L1181
.L1149:
	movq	2608(%r12), %rdx
	movq	-200(%rbp), %rdi
	movl	$1, %ecx
	movq	%r9, %rsi
	movq	%r8, -208(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-208(%rbp), %r8
	testb	%al, %al
	movq	%rdx, %r10
	jne	.L1153
	movq	2584(%r12), %r9
	movq	-224(%rbp), %r11
.L1154:
	addq	%r9, %r11
	movq	%r15, 32(%r8)
	movq	(%r11), %rax
	testq	%rax, %rax
	je	.L1162
	movq	(%rax), %rax
	movq	%rax, (%r8)
	movq	(%r11), %rax
	movq	%r8, (%rax)
.L1163:
	addq	$1, 2608(%r12)
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%r15, %rsi
	addq	$1, 2656(%r12)
	leaq	-160(%rbp), %r9
	movq	%rax, (%r15)
	movq	%r9, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-160(%rbp), %rsi
	movq	40(%r13), %rdi
	movq	-200(%rbp), %r9
	cmpq	%rdi, %rsi
	je	.L1165
	testq	%rdi, %rdi
	je	.L1166
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1142
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1143
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L1298
.L1166:
	leaq	40(%r13), %rdi
	movq	%r9, -200(%rbp)
	call	_ZN4node17BaseObjectPtrImplINS_10BaseObjectELb0EEC1EPS1_
	movq	-160(%rbp), %rdi
	movq	-200(%rbp), %r9
.L1165:
	testq	%rdi, %rdi
	je	.L1170
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1142
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1143
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L1299
.L1170:
	movq	2656(%r12), %rax
	movl	$3, -176(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	$3, %rax
	je	.L1300
	movq	-192(%rbp), %rcx
	leaq	-176(%rbp), %r8
	movq	%r9, %rdi
	leaq	.LC27(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
.L1175:
	cmpb	$0, -160(%rbp)
	je	.L1301
.L1176:
	movq	-152(%rbp), %r13
	testq	%r13, %r13
	je	.L1179
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1180
	call	_ZdlPv@PLT
.L1180:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1179:
	movq	%r12, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-184(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r14), %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	free@PLT
	movq	-216(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1302
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1120:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L1303
	movabsq	$1152921504606846975, %rax
	movq	%r8, -232(%rbp)
	cmpq	%rax, %rdx
	ja	.L1124
	leaq	0(,%rdx,8), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -192(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-232(%rbp), %r8
	movq	-224(%rbp), %rcx
	movq	%rax, %r10
	leaq	2632(%r12), %rax
	movq	%rax, -208(%rbp)
.L1123:
	movq	2600(%r12), %rdi
	movq	$0, 2600(%r12)
	testq	%rdi, %rdi
	je	.L1125
	leaq	2600(%r12), %rax
	xorl	%r11d, %r11d
	movq	%rax, -192(%rbp)
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	(%r9), %rax
	movq	%rsi, (%rax)
.L1128:
	testq	%rdi, %rdi
	je	.L1125
.L1126:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%rcx
	leaq	(%r10,%rdx,8), %r9
	movq	(%r9), %rax
	testq	%rax, %rax
	jne	.L1127
	movq	2600(%r12), %rax
	movq	%rax, (%rsi)
	movq	-192(%rbp), %rax
	movq	%rsi, 2600(%r12)
	movq	%rax, (%r9)
	cmpq	$0, (%rsi)
	je	.L1192
	movq	%rsi, (%r10,%r11,8)
	movq	%rdx, %r11
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	24(%r15), %rdx
	testq	%rdx, %rdx
	je	.L1142
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1143
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1141
	cmpb	$0, 9(%rdx)
	je	.L1145
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1182:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1101:
	cmpq	$1, %rdx
	je	.L1304
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1124
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -208(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -192(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-208(%rbp), %r9
	movq	%rax, %r10
	leaq	2632(%r12), %rax
	movq	%rax, -192(%rbp)
.L1104:
	movq	2600(%r12), %rsi
	movq	$0, 2600(%r12)
	testq	%rsi, %rsi
	je	.L1106
	xorl	%r8d, %r8d
	leaq	2600(%r12), %r11
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1109:
	testq	%rsi, %rsi
	je	.L1106
.L1107:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r9
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L1108
	movq	2600(%r12), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%r12)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L1191
	movq	%rcx, (%r10,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L1107
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	2584(%r12), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1110
	movq	%r9, -208(%rbp)
	movq	%r10, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %r9
	movq	-192(%rbp), %r10
.L1110:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r9, 2592(%r12)
	divq	%r9
	movq	%r10, 2584(%r12)
	movq	%r13, 32(%r15)
	leaq	0(,%rdx,8), %r8
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L1305
.L1111:
	movq	2600(%r12), %rax
	movq	%rax, (%r15)
	movq	%r15, 2600(%r12)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L1113
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r15, (%r10,%rdx,8)
.L1113:
	leaq	2600(%r12), %rax
	movq	%rax, (%r8)
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1289:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1290:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	2584(%r12), %rdi
	cmpq	%rdi, -208(%rbp)
	je	.L1129
	movq	%rcx, -224(%rbp)
	movq	%r10, -208(%rbp)
	movq	%r8, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-224(%rbp), %rcx
	movq	-208(%rbp), %r10
	movq	-192(%rbp), %r8
.L1129:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%rcx, 2592(%r12)
	divq	%rcx
	movq	%r10, 2584(%r12)
	leaq	0(,%rdx,8), %r9
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	%rdx, %r8
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1291:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	2600(%r12), %rax
	movq	%rax, (%r8)
	movq	%r8, 2600(%r12)
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L1132
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r8, (%r10,%rdx,8)
.L1132:
	leaq	2600(%r12), %rax
	movq	%rax, (%r9)
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1288:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	%rax, -192(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-192(%rbp), %rdi
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1293:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$48, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1295:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r15), %rdx
	xorl	%edi, %edi
	movq	$0, (%rax)
	movw	%di, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1193
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1134:
	movb	%dl, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%rax, 24(%r15)
.L1188:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	%rdx, %r11
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	%rax, -192(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-192(%rbp), %rdi
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1296:
	cmpb	$0, 9(%rdx)
	je	.L1139
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-168(%rbp), %r15
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1299:
	cmpb	$0, 9(%rdx)
	je	.L1172
	movq	(%rdi), %rax
	movq	%r9, -200(%rbp)
	call	*8(%rax)
	movq	-200(%rbp), %r9
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1298:
	cmpb	$0, 9(%rdx)
	je	.L1167
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-160(%rbp), %rsi
	movq	-200(%rbp), %r9
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1142:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1300:
	movq	%r9, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	-192(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1177
	movq	(%rax), %r8
.L1177:
	leaq	-176(%rbp), %r13
	movl	$175, %ecx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-192(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1176
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1153:
	cmpq	$1, %rdx
	je	.L1306
	movabsq	$1152921504606846975, %rax
	movq	%r8, -232(%rbp)
	cmpq	%rax, %rdx
	ja	.L1124
	leaq	0(,%rdx,8), %rdx
	movq	%r10, -224(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-232(%rbp), %r8
	movq	-224(%rbp), %r10
	movq	%rax, %r9
	leaq	2632(%r12), %rax
	movq	%rax, -208(%rbp)
.L1156:
	movq	2600(%r12), %rsi
	movq	$0, 2600(%r12)
	testq	%rsi, %rsi
	je	.L1157
	leaq	2600(%r12), %rax
	xorl	%r11d, %r11d
	movq	%rax, -200(%rbp)
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L1160:
	testq	%rsi, %rsi
	je	.L1157
.L1158:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r10
	leaq	(%r9,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L1159
	movq	2600(%r12), %rax
	movq	%rax, (%rcx)
	movq	-200(%rbp), %rax
	movq	%rcx, 2600(%r12)
	movq	%rax, (%rdi)
	cmpq	$0, (%rcx)
	je	.L1194
	movq	%rcx, (%r9,%r11,8)
	movq	%rdx, %r11
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	2584(%r12), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L1161
	movq	%r10, -224(%rbp)
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-224(%rbp), %r10
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %r8
.L1161:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r10, 2592(%r12)
	divq	%r10
	movq	%r9, 2584(%r12)
	leaq	0(,%rdx,8), %r11
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1143:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	2600(%r12), %rax
	movq	%rax, (%r8)
	movq	%r8, 2600(%r12)
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L1164
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r8, (%r9,%rdx,8)
.L1164:
	leaq	2600(%r12), %rax
	movq	%rax, (%r11)
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	%rdx, %r11
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1145:
	cmpb	$0, 8(%rdx)
	je	.L1141
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1141
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r15, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1139:
	cmpb	$0, 8(%rdx)
	je	.L1136
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1136
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-168(%rbp), %r15
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdi
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1172:
	cmpb	$0, 8(%rdx)
	je	.L1170
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1170
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-200(%rbp), %r9
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1167:
	cmpb	$0, 8(%rdx)
	je	.L1166
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1166
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-160(%rbp), %rsi
	movq	-200(%rbp), %r9
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1193:
	xorl	%edx, %edx
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1304:
	leaq	2632(%r12), %r10
	movq	$0, 2632(%r12)
	movq	%r10, -192(%rbp)
	jmp	.L1104
.L1303:
	leaq	2632(%r12), %r10
	movq	$0, 2632(%r12)
	movq	%r10, -208(%rbp)
	jmp	.L1123
.L1306:
	leaq	2632(%r12), %r9
	movq	$0, 2632(%r12)
	movq	%r9, -208(%rbp)
	jmp	.L1156
.L1124:
	call	_ZSt17__throw_bad_allocv@PLT
.L1302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8630:
	.size	_ZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEv, .-_ZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEv
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB10679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1308
	testq	%rsi, %rsi
	je	.L1324
.L1308:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L1325
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L1311
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L1312:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1326
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1311:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1312
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1310:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1312
.L1324:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1326:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10679:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.1
.LC28:
	.string	"ScopedDetached"
.LC29:
	.string	"BaseObjectPtrTest"
.LC30:
	.string	"ScopedDetachedWithWeak"
.LC31:
	.string	"Undetached"
.LC32:
	.string	"GCWeak"
.LC33:
	.string	"Moveable"
.LC34:
	.string	"NestedClasses"
	.section	.text.startup
	.p2align 4
	.type	_Z41__static_initialization_and_destruction_0ii.constprop.0, @function
_Z41__static_initialization_and_destruction_0ii.constprop.0:
.LFB11978:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1354
.L1328:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1355
.L1329:
	leaq	-128(%rbp), %r15
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	leaq	_ZN15NodeTestFixture16TearDownTestCaseEv(%rip), %r14
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC29(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r8
	leaq	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC28(%rip), %rsi
	pushq	%r13
	leaq	_ZN15NodeTestFixture13SetUpTestCaseEv(%rip), %r13
	pushq	%r14
	pushq	%r13
	movl	$44, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1330
	call	_ZdlPv@PLT
.L1330:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L1331
	call	_ZdlPv@PLT
.L1331:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1356
.L1332:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1357
.L1333:
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rdi
	movq	%r12, %r8
	leaq	.LC29(%rip), %rdi
	leaq	.LC30(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$58, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1334
	call	_ZdlPv@PLT
.L1334:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1335
	call	_ZdlPv@PLT
.L1335:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1358
.L1336:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1359
.L1337:
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC29(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rsi
	leaq	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC31(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$76, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN33BaseObjectPtrTest_Undetached_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1338
	call	_ZdlPv@PLT
.L1338:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1339
	call	_ZdlPv@PLT
.L1339:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1360
.L1340:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1361
.L1341:
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC29(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E(%rip), %r9
	xorl	%edx, %edx
	pushq	%rcx
	movq	%r12, %r8
	xorl	%ecx, %ecx
	leaq	.LC32(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$90, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN29BaseObjectPtrTest_GCWeak_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1342
	call	_ZdlPv@PLT
.L1342:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1343
	call	_ZdlPv@PLT
.L1343:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1362
.L1344:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1363
.L1345:
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC29(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	pushq	%rdx
	movq	%r12, %r8
	xorl	%edx, %edx
	leaq	.LC33(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$122, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN31BaseObjectPtrTest_Moveable_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1346
	call	_ZdlPv@PLT
.L1346:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1347
	call	_ZdlPv@PLT
.L1347:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1364
.L1348:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1365
.L1349:
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC29(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC34(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$148, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN36BaseObjectPtrTest_NestedClasses_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1350
	call	_ZdlPv@PLT
.L1350:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1327
	call	_ZdlPv@PLT
.L1327:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1366
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1357:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1333
.L1356:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1332
.L1355:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1329
.L1359:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1337
.L1358:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1336
.L1361:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1341
.L1360:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1340
.L1363:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1345
.L1362:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1344
.L1365:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1349
.L1364:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1348
.L1354:
	call	_ZN7testing8internal16SuiteApiResolverI17BaseObjectPtrTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1328
.L1366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11978:
	.size	_Z41__static_initialization_and_destruction_0ii.constprop.0, .-_Z41__static_initialization_and_destruction_0ii.constprop.0
	.section	.text._ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,"axG",@progbits,_ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,comdat
	.p2align 4
	.weak	_ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.type	_ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, @function
_ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_:
.LFB11240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -480(%rbp)
	movq	.LC10(%rip), %xmm1
	movhps	.LC11(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%eax, %eax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rbx), %rdx
	movq	%r15, -320(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rax, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r14, %rdi
	leaq	-368(%rbp), %r14
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -472(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movdqa	-464(%rbp), %xmm1
	movq	-24(%rax), %rdx
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp,%rdx)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-480(%rbp), %rax
	movq	-472(%rbp), %r8
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1376
	movq	%r8, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L1369:
	leaq	16(%r12), %rax
	movb	$0, 16(%r12)
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	testq	%rax, %rax
	je	.L1370
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1377
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1372:
	movq	.LC10(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC12(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L1373
	call	_ZdlPv@PLT
.L1373:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	%r15, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1378
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1376:
	movl	$4, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1370:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1372
.L1378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11240:
	.size	_ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, .-_ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.section	.rodata._ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_.str1.1,"aMS",@progbits,1
.LC35:
	.string	"Expected: ("
.LC36:
	.string	"basic_string::append"
.LC37:
	.string	"(null)"
.LC38:
	.string	") "
.LC39:
	.string	" ("
.LC40:
	.string	"), actual: "
.LC41:
	.string	" vs "
	.section	.text._ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_,"axG",@progbits,_ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_
	.type	_ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_, @function
_ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_:
.LFB10161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$904, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -904(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -912(%rbp)
	call	_ZN7testing16AssertionFailureEv@PLT
	leaq	-824(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -928(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-824(%rbp), %rax
	movl	$11, %edx
	leaq	.LC35(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %rbx
	movq	-928(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1501
.L1380:
	movq	%r8, %rsi
	leaq	-736(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-736(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -928(%rbp)
	call	strlen@PLT
	movq	-928(%rbp), %rsi
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-736(%rbp), %rdi
	leaq	-720(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1383
	call	_ZdlPv@PLT
.L1383:
	movq	-824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1384
	movq	(%rdi), %rax
	call	*8(%rax)
.L1384:
	leaq	-832(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	testq	%r12, %r12
	je	.L1502
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	-832(%rbp), %rax
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	je	.L1503
.L1387:
	movq	%rbx, %rsi
	leaq	-704(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-704(%rbp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-704(%rbp), %rdi
	leaq	-688(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1390
	call	_ZdlPv@PLT
.L1390:
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1391
	movq	(%rdi), %rax
	call	*8(%rax)
.L1391:
	leaq	-840(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-840(%rbp), %rax
	movl	$2, %edx
	leaq	.LC38(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	je	.L1504
.L1392:
	movq	%rbx, %rsi
	leaq	-672(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-672(%rbp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-672(%rbp), %rdi
	leaq	-656(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1394
	call	_ZdlPv@PLT
.L1394:
	movq	-840(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1395
	movq	(%rdi), %rax
	call	*8(%rax)
.L1395:
	leaq	-848(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	testq	%r13, %r13
	je	.L1505
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	-848(%rbp), %rax
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r13
	testq	%r13, %r13
	je	.L1506
.L1398:
	movq	%r12, %rsi
	leaq	-640(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-640(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-640(%rbp), %rdi
	leaq	-624(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1400
	call	_ZdlPv@PLT
.L1400:
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1401
	movq	(%rdi), %rax
	call	*8(%rax)
.L1401:
	leaq	-856(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-856(%rbp), %rax
	movl	$2, %edx
	leaq	.LC39(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	je	.L1507
.L1402:
	movq	%r13, %rsi
	leaq	-608(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-608(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1404
	call	_ZdlPv@PLT
.L1404:
	movq	-856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1405
	movq	(%rdi), %rax
	call	*8(%rax)
.L1405:
	leaq	-864(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	testq	%r14, %r14
	je	.L1508
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	-864(%rbp), %rax
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r13
	testq	%r13, %r13
	je	.L1509
.L1408:
	movq	%r12, %rsi
	leaq	-576(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-576(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1410
	call	_ZdlPv@PLT
.L1410:
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1411
	movq	(%rdi), %rax
	call	*8(%rax)
.L1411:
	leaq	-872(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-872(%rbp), %rax
	movl	$11, %edx
	leaq	.LC40(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	je	.L1510
.L1412:
	movq	%r13, %rsi
	leaq	-544(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-544(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1414
	call	_ZdlPv@PLT
.L1414:
	movq	-872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1415
	movq	(%rdi), %rax
	call	*8(%rax)
.L1415:
	movq	%r15, %rsi
	leaq	-800(%rbp), %rdi
	leaq	-880(%rbp), %r13
	call	_ZN7testing13PrintToStringIP15DummyBaseObjectEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-880(%rbp), %rax
	movq	-792(%rbp), %rdx
	movq	-800(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	je	.L1511
.L1416:
	movq	%r13, %rsi
	leaq	-512(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-512(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1418
	call	_ZdlPv@PLT
.L1418:
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1419
	movq	(%rdi), %rax
	call	*8(%rax)
.L1419:
	leaq	-888(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-888(%rbp), %rax
	movl	$4, %edx
	leaq	.LC41(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	je	.L1512
.L1420:
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-480(%rbp), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1422
	call	_ZdlPv@PLT
.L1422:
	movq	-888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1423
	movq	(%rdi), %rax
	call	*8(%rax)
.L1423:
	movq	.LC10(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %r14
	movq	%r13, %rdi
	leaq	-432(%rbp), %r12
	leaq	-368(%rbp), %r15
	movhps	.LC11(%rip), %xmm1
	movaps	%xmm1, -928(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r14, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r12, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-928(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -928(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r12, %rdi
	movl	$9, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-752(%rbp), %rax
	movq	$0, -760(%rbp)
	leaq	-768(%rbp), %rdi
	movq	%rax, -936(%rbp)
	movq	%rax, -768(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -752(%rbp)
	testq	%rax, %rax
	je	.L1424
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1513
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1426:
	movq	.LC10(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC12(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-928(%rbp), %rdi
	je	.L1427
	call	_ZdlPv@PLT
.L1427:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	leaq	-896(%rbp), %r15
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-896(%rbp), %rax
	movq	-760(%rbp), %rdx
	movq	-768(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r13
	testq	%r13, %r13
	je	.L1514
.L1428:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing7Message9GetStringB5cxx11Ev@PLT
	movq	-448(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L1389
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-448(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1430
	call	_ZdlPv@PLT
.L1430:
	movq	-896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1431
	movq	(%rdi), %rax
	call	*8(%rax)
.L1431:
	movq	-904(%rbp), %rdi
	movq	-912(%rbp), %rsi
	call	_ZN7testing15AssertionResultC1ERKS0_@PLT
	movq	-768(%rbp), %rdi
	cmpq	-936(%rbp), %rdi
	je	.L1432
	call	_ZdlPv@PLT
.L1432:
	movq	-800(%rbp), %rdi
	leaq	-784(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1433
	call	_ZdlPv@PLT
.L1433:
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	je	.L1379
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1435
	call	_ZdlPv@PLT
.L1435:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1379:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1515
	movq	-904(%rbp), %rax
	addq	$904, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	-832(%rbp), %rax
	movl	$6, %edx
	leaq	.LC37(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r12
	testq	%r12, %r12
	jne	.L1387
.L1503:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %r8
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -808(%rbp)
	testq	%r8, %r8
	je	.L1387
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1388
	movq	%r8, -928(%rbp)
	call	_ZdlPv@PLT
	movq	-928(%rbp), %r8
.L1388:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r12
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	-848(%rbp), %rax
	movl	$6, %edx
	leaq	.LC37(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r13
	testq	%r13, %r13
	jne	.L1398
.L1506:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %rbx
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movb	$0, 16(%r13)
	movq	%r13, -808(%rbp)
	testq	%rbx, %rbx
	je	.L1398
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1399
	call	_ZdlPv@PLT
.L1399:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r13
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	-864(%rbp), %rax
	movl	$6, %edx
	leaq	.LC37(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-808(%rbp), %r13
	testq	%r13, %r13
	jne	.L1408
.L1509:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %r14
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movb	$0, 16(%r13)
	movq	%r13, -808(%rbp)
	testq	%r14, %r14
	je	.L1408
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1409
	call	_ZdlPv@PLT
.L1409:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r13
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1501:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %r9
	movq	-928(%rbp), %r8
	movq	%rax, %rbx
	leaq	16(%rax), %rax
	testq	%r9, %r9
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	movq	%rbx, -808(%rbp)
	je	.L1380
	movq	(%r9), %rdi
	leaq	16(%r9), %rax
	cmpq	%rax, %rdi
	je	.L1381
	movq	%r8, -936(%rbp)
	movq	%r9, -928(%rbp)
	call	_ZdlPv@PLT
	movq	-936(%rbp), %r8
	movq	-928(%rbp), %r9
.L1381:
	movl	$32, %esi
	movq	%r9, %rdi
	movq	%r8, -928(%rbp)
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %rbx
	movq	-928(%rbp), %r8
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1504:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %r8
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -808(%rbp)
	testq	%r8, %r8
	je	.L1392
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1393
	movq	%r8, -928(%rbp)
	call	_ZdlPv@PLT
	movq	-928(%rbp), %r8
.L1393:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r12
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1507:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %rbx
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -808(%rbp)
	testq	%rbx, %rbx
	je	.L1402
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1403
	call	_ZdlPv@PLT
.L1403:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r12
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1510:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %r14
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -808(%rbp)
	testq	%r14, %r14
	je	.L1412
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1413
	call	_ZdlPv@PLT
.L1413:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r12
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1511:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %r14
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -808(%rbp)
	testq	%r14, %r14
	je	.L1416
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1417
	call	_ZdlPv@PLT
.L1417:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r12
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1424:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1514:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %rbx
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movq	%rax, 0(%r13)
	movq	$0, 8(%r13)
	movb	$0, 16(%r13)
	movq	%r13, -808(%rbp)
	testq	%rbx, %rbx
	je	.L1428
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1429
	call	_ZdlPv@PLT
.L1429:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r13
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1512:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	-808(%rbp), %r14
	movq	%rax, %r12
	leaq	16(%rax), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%r12, -808(%rbp)
	testq	%r14, %r14
	je	.L1420
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1421
	call	_ZdlPv@PLT
.L1421:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	-808(%rbp), %r12
	jmp	.L1420
.L1389:
	leaq	.LC36(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10161:
	.size	_ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_, .-_ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_
	.section	.rodata._ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_.str1.1,"aMS",@progbits,1
.LC42:
	.string	"true"
.LC43:
	.string	"false"
	.section	.text._ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,"axG",@progbits,_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,comdat
	.p2align 4
	.weak	_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.type	_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, @function
_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_:
.LFB11376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-448(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -480(%rbp)
	movq	.LC10(%rip), %xmm1
	movhps	.LC11(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-24(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rax, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r13, %rdi
	leaq	-368(%rbp), %r13
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -472(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movdqa	-464(%rbp), %xmm1
	movq	-24(%rax), %rdx
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp,%rdx)
	leaq	80(%r15), %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	movq	%r15, -448(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-480(%rbp), %rax
	movq	-472(%rbp), %r8
	leaq	.LC42(%rip), %rsi
	cmpb	$1, (%rax)
	movq	%r8, %rdi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	cmpb	$0, (%rax)
	leaq	.LC43(%rip), %rax
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	16(%r14), %rax
	movb	$0, 16(%r14)
	movq	%rax, (%r14)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r14)
	testq	%rax, %rax
	je	.L1525
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1527
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1519:
	movq	.LC10(%rip), %xmm0
	leaq	104+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, -448(%rbp)
	movq	%rax, -320(%rbp)
	movq	-352(%rbp), %rdi
	movhps	.LC12(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L1520
	call	_ZdlPv@PLT
.L1520:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1528
	addq	$440, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1525:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1519
.L1528:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11376:
	.size	_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, .-_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"weak_ptr->persistent().IsWeak()"
	.section	.rodata.str1.1
.LC45:
	.string	"!="
.LC46:
	.string	"--expose-gc"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB47:
	.text
.LHOTB47:
	.align 2
	.p2align 4
	.globl	_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv
	.type	_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv, @function
_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv:
.LFB8614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-224(%rbp), %rax
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$8, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	.LC20(%rip), %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -304(%rbp)
	movq	16(%rdi), %rsi
	movq	%rax, %rdi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -320(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC17(%rip), %rdx
	leaq	.LC18(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC19(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r13
	call	malloc@PLT
	movq	%r13, %r15
	movq	%rax, 0(%r13)
	movl	%ebx, %r13d
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, -280(%rbp)
	.p2align 4,,10
	.p2align 3
.L1530:
	movq	-280(%rbp), %rax
	movslq	%r13d, %rbx
	movq	(%rax,%r14), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -296(%rbp)
	call	strlen@PLT
	movq	(%r15), %rdi
	movq	-296(%rbp), %rcx
	movq	%r12, %rdx
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	movslq	%r8d, %rsi
	addq	%rbx, %rdi
	movl	%r8d, -288(%rbp)
	call	snprintf@PLT
	movl	-288(%rbp), %r8d
	addq	(%r15), %rbx
	movq	%rbx, (%r15,%r14)
	addq	$8, %r14
	addl	%r8d, %r13d
	cmpq	$24, %r14
	jne	.L1530
	movq	-224(%rbp), %r12
	xorl	%esi, %esi
	movq	%r15, %r13
	movq	%r12, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -296(%rbp)
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L1747
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L1748
	movq	-192(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r15, %r9
	movq	%r15, %rcx
	movl	$3, %r8d
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1749
	movq	-304(%rbp), %rax
	movq	$0, -272(%rbp)
	movq	16(%rax), %rsi
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1750
.L1534:
	movq	3280(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1751
	movl	$32, %edi
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, %r14
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%r14)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm2
	movq	$0, 24(%r14)
	movq	%r15, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r14)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1752
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rbx
	movl	$40, %edi
	leaq	1(%rbx), %rax
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r10
	xorl	%edx, %edx
	movq	%rbx, 24(%rax)
	movq	%rax, %r15
	movq	%r10, 8(%rax)
	movq	%r14, 16(%rax)
	movq	2592(%r12), %rsi
	movq	$0, (%rax)
	movq	%r14, %rax
	divq	%rsi
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %rbx
	testq	%r8, %r8
	je	.L1536
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1537:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1536
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1536
	movq	%rdi, %rax
.L1539:
	cmpq	%rcx, %r14
	jne	.L1537
	cmpq	%r10, 8(%rax)
	jne	.L1537
	cmpq	16(%rax), %r14
	jne	.L1537
	cmpq	$0, (%r8)
	je	.L1536
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1536:
	movq	2608(%r12), %rdx
	movl	$1, %ecx
	leaq	2616(%r12), %rdi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rcx
	testb	%al, %al
	jne	.L1540
	movq	2584(%r12), %r8
	movq	%r14, 32(%r15)
	addq	%r8, %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L1550
.L1775:
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%rbx), %rax
	movq	%r15, (%rax)
.L1551:
	addq	$1, 2608(%r12)
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%r14, %rsi
	addq	$1, 2656(%r12)
	leaq	-264(%rbp), %rdi
	movq	%rax, (%r14)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-272(%rbp), %rdi
	movq	-264(%rbp), %r8
	testq	%rdi, %rdi
	je	.L1553
	movq	16(%rdi), %rsi
	cmpq	%rsi, %r8
	je	.L1554
	subl	$1, 4(%rdi)
	je	.L1753
.L1555:
	movq	%r8, %rsi
	leaq	-272(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb1EEC1EPS1_
	movq	-264(%rbp), %rsi
.L1554:
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L1558
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1557
.L1558:
	movq	8(%rsi), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L1557:
	movq	2656(%r12), %rax
	leaq	-240(%rbp), %r15
	movl	$1, -256(%rbp)
	movq	%rax, -248(%rbp)
	cmpq	$1, %rax
	je	.L1754
	leaq	-256(%rbp), %rbx
	leaq	-248(%rbp), %r14
	movq	%r15, %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -240(%rbp)
	je	.L1755
.L1561:
	movq	-232(%rbp), %r8
	testq	%r8, %r8
	je	.L1564
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1565
	movq	%r8, -328(%rbp)
	call	_ZdlPv@PLT
	movq	-328(%rbp), %r8
.L1565:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1564:
	movq	-264(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L1566
	movq	16(%rax), %rax
.L1566:
	movq	%r14, %r8
	movq	%rbx, %rcx
	leaq	.LC24(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, -256(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectS3_EENS_15AssertionResultEPKcS6_RKT_RKT0_
	cmpb	$0, -240(%rbp)
	je	.L1756
.L1567:
	movq	-232(%rbp), %r8
	testq	%r8, %r8
	je	.L1570
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1571
	movq	%r8, -328(%rbp)
	call	_ZdlPv@PLT
	movq	-328(%rbp), %r8
.L1571:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1570:
	movq	-272(%rbp), %rax
	movb	$0, -248(%rbp)
	testq	%rax, %rax
	je	.L1613
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1757
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	sete	-256(%rbp)
	jne	.L1574
	movq	-280(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-128(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -328(%rbp)
	call	_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	-280(%rbp), %r8
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movq	-328(%rbp), %rcx
	leaq	.LC43(%rip), %rdx
	leaq	.LC44(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1577
	call	_ZdlPv@PLT
.L1577:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1576
	call	_ZdlPv@PLT
.L1576:
	cmpb	$0, -240(%rbp)
	je	.L1758
.L1579:
	movq	-232(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1582
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1583
	call	_ZdlPv@PLT
.L1583:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1582:
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1585
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1593
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1594
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1585
	cmpb	$0, 9(%rdx)
	jne	.L1759
	cmpb	$0, 8(%rdx)
	je	.L1585
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1585
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	-240(%rbp), %rdi
	movq	%rdi, -264(%rbp)
	testq	%rdi, %rdi
	je	.L1592
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1593
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1594
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1592
	cmpb	$0, 9(%rdx)
	jne	.L1760
	cmpb	$0, 8(%rdx)
	je	.L1592
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1592
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	-288(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	2656(%r12), %rax
	movl	$1, -248(%rbp)
	movq	%rax, -240(%rbp)
	cmpq	$1, %rax
	je	.L1761
	movq	-288(%rbp), %rdi
	movq	%r14, %r8
	movq	%r15, %rcx
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1762
.L1600:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1603
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1604
	call	_ZdlPv@PLT
.L1604:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1603:
	movq	-272(%rbp), %rax
	movq	$0, -240(%rbp)
	testq	%rax, %rax
	je	.L1763
	movq	16(%rax), %rax
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L1606
	movq	-288(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L1607:
	cmpb	$0, -160(%rbp)
	je	.L1764
.L1608:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1611
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1612
	call	_ZdlPv@PLT
.L1612:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1611:
	movq	-272(%rbp), %rax
	movb	$1, -240(%rbp)
	testq	%rax, %rax
	je	.L1613
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1765
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	sete	-248(%rbp)
	je	.L1766
.L1615:
	movq	-280(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-128(%rbp), %rbx
	call	_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN7testing13PrintToStringIbEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	-288(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	movq	-280(%rbp), %r8
	leaq	.LC42(%rip), %rdx
	leaq	.LC44(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1617
	call	_ZdlPv@PLT
.L1617:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1616
	call	_ZdlPv@PLT
.L1616:
	cmpb	$0, -160(%rbp)
	je	.L1767
.L1619:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1622
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1623
	call	_ZdlPv@PLT
.L1623:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1622:
	leaq	.LC46(%rip), %rdi
	call	_ZN2v82V818SetFlagsFromStringEPKc@PLT
	movq	-304(%rbp), %rax
	xorl	%esi, %esi
	movq	16(%rax), %rdi
	call	_ZN2v87Isolate34RequestGarbageCollectionForTestingENS0_21GarbageCollectionTypeE@PLT
	movq	2656(%r12), %rax
	movl	$0, -248(%rbp)
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L1768
	movq	-288(%rbp), %rdi
	movq	%r14, %r8
	movq	%r15, %rcx
	leaq	.LC14(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIliEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L1769
.L1626:
	movq	-152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1629
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1630
	call	_ZdlPv@PLT
.L1630:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1629:
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L1631
	movq	16(%rax), %rax
.L1631:
	movq	-288(%rbp), %rdi
	movq	%r15, %rdx
	leaq	.LC23(%rip), %rsi
	movq	%rax, -240(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -160(%rbp)
	je	.L1770
.L1632:
	movq	-152(%rbp), %r14
	testq	%r14, %r14
	je	.L1635
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1636
	call	_ZdlPv@PLT
.L1636:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1635:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1638
	subl	$1, 4(%rdi)
	je	.L1771
.L1638:
	movq	%r12, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-312(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	-296(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	0(%r13), %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	movq	-320(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1772
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1753:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L1555
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-264(%rbp), %r8
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	-288(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L1619
.L1767:
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1620
	movq	(%rax), %r8
.L1620:
	movl	$113, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1619
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1771:
	cmpq	$0, 16(%rdi)
	jne	.L1638
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1757:
	movb	$0, -256(%rbp)
.L1574:
	movq	%r15, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -240(%rbp)
	jne	.L1579
.L1758:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1580
	movq	(%rax), %r8
.L1580:
	movl	$106, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1579
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	%r15, %rdi
	leaq	-256(%rbp), %rbx
	leaq	-248(%rbp), %r14
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -240(%rbp)
	jne	.L1561
.L1755:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1562
	movq	(%rax), %r8
.L1562:
	movl	$104, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1561
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1568
	movq	(%rax), %r8
.L1568:
	movl	$105, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1567
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1540:
	cmpq	$1, %rdx
	je	.L1773
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1774
	leaq	0(,%rdx,8), %rbx
	movq	%rdx, -328(%rbp)
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	leaq	2632(%r12), %rbx
	movq	%rax, %rdi
	call	memset@PLT
	movq	-328(%rbp), %rcx
	movq	%rax, %r8
.L1543:
	movq	2600(%r12), %rdi
	movq	$0, 2600(%r12)
	testq	%rdi, %rdi
	je	.L1545
	xorl	%r10d, %r10d
	leaq	2600(%r12), %r11
	jmp	.L1546
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	(%r9), %rdx
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
.L1548:
	testq	%rdi, %rdi
	je	.L1545
.L1546:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%rcx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r9
	testq	%r9, %r9
	jne	.L1547
	movq	2600(%r12), %r9
	movq	%r9, (%rsi)
	movq	%rsi, 2600(%r12)
	movq	%r11, (%rax)
	cmpq	$0, (%rsi)
	je	.L1644
	movq	%rsi, (%r8,%r10,8)
	movq	%rdx, %r10
	testq	%rdi, %rdi
	jne	.L1546
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	2584(%r12), %rdi
	cmpq	%rbx, %rdi
	je	.L1549
	movq	%rcx, -336(%rbp)
	movq	%r8, -328(%rbp)
	call	_ZdlPv@PLT
	movq	-336(%rbp), %rcx
	movq	-328(%rbp), %r8
.L1549:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%rcx, 2592(%r12)
	divq	%rcx
	movq	%r8, 2584(%r12)
	movq	%r14, 32(%r15)
	leaq	0(,%rdx,8), %rbx
	addq	%r8, %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L1775
.L1550:
	movq	2600(%r12), %rax
	movq	%rax, (%r15)
	movq	%r15, 2600(%r12)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L1552
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r15, (%r8,%rdx,8)
.L1552:
	leaq	2600(%r12), %rax
	movq	%rax, (%rbx)
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	-288(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L1626
.L1769:
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1627
	movq	(%rax), %r8
.L1627:
	movl	$118, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1626
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1761:
	movq	-288(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -160(%rbp)
	jne	.L1600
.L1762:
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1601
	movq	(%rax), %r8
.L1601:
	movl	$111, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1600
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1633
	movq	(%rax), %r8
.L1633:
	movl	$119, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1632
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	leaq	.LC13(%rip), %r8
	testq	%rax, %rax
	je	.L1609
	movq	(%rax), %r8
.L1609:
	movl	$112, %ecx
	leaq	.LC16(%rip), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1608
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	$0, -248(%rbp)
.L1606:
	movq	-288(%rbp), %rdi
	leaq	.LC45(%rip), %r9
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	.LC9(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperOpFailureIP15DummyBaseObjectDnEENS_15AssertionResultEPKcS6_RKT_RKT0_S6_
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1747:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1748:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	%rdx, %r10
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1749:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1751:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	%rax, -328(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-328(%rbp), %rdi
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1752:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1765:
	movb	$0, -248(%rbp)
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1760:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1594:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1593:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1553:
	testq	%r8, %r8
	jne	.L1555
	xorl	%esi, %esi
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1773:
	leaq	2632(%r12), %r8
	movq	$0, 2632(%r12)
	movq	%r8, %rbx
	jmp	.L1543
.L1772:
	call	__stack_chk_fail@PLT
.L1774:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv.cold, @function
_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv.cold:
.LFSB8614:
.L1613:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE8614:
	.text
	.size	_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv, .-_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv
	.section	.text.unlikely
	.size	_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv.cold, .-_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv.cold
.LCOLDE47:
	.text
.LHOTE47:
	.section	.text.startup
	.p2align 4
	.type	_GLOBAL__sub_I__ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E, @function
_GLOBAL__sub_I__ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E:
.LFB11794:
	.cfi_startproc
	endbr64
	jmp	_Z41__static_initialization_and_destruction_0ii.constprop.0
	.cfi_endproc
.LFE11794:
	.size	_GLOBAL__sub_I__ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E, .-_GLOBAL__sub_I__ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E
	.weak	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E
	.section	.bss._ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E,"awG",@nobits,_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E,comdat
	.type	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E, @gnu_unique_object
	.size	_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E, 1
_ZN7testing8internal12TypeIdHelperI17BaseObjectPtrTestE6dummy_E:
	.zero	1
	.weak	_ZTV15NodeTestFixture
	.section	.data.rel.ro._ZTV15NodeTestFixture,"awG",@progbits,_ZTV15NodeTestFixture,comdat
	.align 8
	.type	_ZTV15NodeTestFixture, @object
	.size	_ZTV15NodeTestFixture, 64
_ZTV15NodeTestFixture:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTV15DummyBaseObject
	.section	.data.rel.ro._ZTV15DummyBaseObject,"awG",@progbits,_ZTV15DummyBaseObject,comdat
	.align 8
	.type	_ZTV15DummyBaseObject, @object
	.size	_ZTV15DummyBaseObject, 88
_ZTV15DummyBaseObject:
	.quad	0
	.quad	0
	.quad	_ZN15DummyBaseObjectD1Ev
	.quad	_ZN15DummyBaseObjectD0Ev
	.quad	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE
	.quad	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK15DummyBaseObject8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI37BaseObjectPtrTest_ScopedDetached_TestE10CreateTestEv
	.weak	_ZTV37BaseObjectPtrTest_ScopedDetached_Test
	.section	.data.rel.ro.local._ZTV37BaseObjectPtrTest_ScopedDetached_Test,"awG",@progbits,_ZTV37BaseObjectPtrTest_ScopedDetached_Test,comdat
	.align 8
	.type	_ZTV37BaseObjectPtrTest_ScopedDetached_Test, @object
	.size	_ZTV37BaseObjectPtrTest_ScopedDetached_Test, 64
_ZTV37BaseObjectPtrTest_ScopedDetached_Test:
	.quad	0
	.quad	0
	.quad	_ZN37BaseObjectPtrTest_ScopedDetached_TestD1Ev
	.quad	_ZN37BaseObjectPtrTest_ScopedDetached_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN37BaseObjectPtrTest_ScopedDetached_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI45BaseObjectPtrTest_ScopedDetachedWithWeak_TestE10CreateTestEv
	.weak	_ZTV45BaseObjectPtrTest_ScopedDetachedWithWeak_Test
	.section	.data.rel.ro.local._ZTV45BaseObjectPtrTest_ScopedDetachedWithWeak_Test,"awG",@progbits,_ZTV45BaseObjectPtrTest_ScopedDetachedWithWeak_Test,comdat
	.align 8
	.type	_ZTV45BaseObjectPtrTest_ScopedDetachedWithWeak_Test, @object
	.size	_ZTV45BaseObjectPtrTest_ScopedDetachedWithWeak_Test, 64
_ZTV45BaseObjectPtrTest_ScopedDetachedWithWeak_Test:
	.quad	0
	.quad	0
	.quad	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD1Ev
	.quad	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33BaseObjectPtrTest_Undetached_TestE10CreateTestEv
	.weak	_ZTV33BaseObjectPtrTest_Undetached_Test
	.section	.data.rel.ro.local._ZTV33BaseObjectPtrTest_Undetached_Test,"awG",@progbits,_ZTV33BaseObjectPtrTest_Undetached_Test,comdat
	.align 8
	.type	_ZTV33BaseObjectPtrTest_Undetached_Test, @object
	.size	_ZTV33BaseObjectPtrTest_Undetached_Test, 64
_ZTV33BaseObjectPtrTest_Undetached_Test:
	.quad	0
	.quad	0
	.quad	_ZN33BaseObjectPtrTest_Undetached_TestD1Ev
	.quad	_ZN33BaseObjectPtrTest_Undetached_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN33BaseObjectPtrTest_Undetached_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI29BaseObjectPtrTest_GCWeak_TestE10CreateTestEv
	.weak	_ZTV29BaseObjectPtrTest_GCWeak_Test
	.section	.data.rel.ro.local._ZTV29BaseObjectPtrTest_GCWeak_Test,"awG",@progbits,_ZTV29BaseObjectPtrTest_GCWeak_Test,comdat
	.align 8
	.type	_ZTV29BaseObjectPtrTest_GCWeak_Test, @object
	.size	_ZTV29BaseObjectPtrTest_GCWeak_Test, 64
_ZTV29BaseObjectPtrTest_GCWeak_Test:
	.quad	0
	.quad	0
	.quad	_ZN29BaseObjectPtrTest_GCWeak_TestD1Ev
	.quad	_ZN29BaseObjectPtrTest_GCWeak_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN29BaseObjectPtrTest_GCWeak_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31BaseObjectPtrTest_Moveable_TestE10CreateTestEv
	.weak	_ZTV31BaseObjectPtrTest_Moveable_Test
	.section	.data.rel.ro.local._ZTV31BaseObjectPtrTest_Moveable_Test,"awG",@progbits,_ZTV31BaseObjectPtrTest_Moveable_Test,comdat
	.align 8
	.type	_ZTV31BaseObjectPtrTest_Moveable_Test, @object
	.size	_ZTV31BaseObjectPtrTest_Moveable_Test, 64
_ZTV31BaseObjectPtrTest_Moveable_Test:
	.quad	0
	.quad	0
	.quad	_ZN31BaseObjectPtrTest_Moveable_TestD1Ev
	.quad	_ZN31BaseObjectPtrTest_Moveable_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN31BaseObjectPtrTest_Moveable_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI36BaseObjectPtrTest_NestedClasses_TestE10CreateTestEv
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvE13ObjectWithPtr, @object
	.size	_ZTVZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvE13ObjectWithPtr, 88
_ZTVZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvE13ObjectWithPtr:
	.quad	0
	.quad	0
	.quad	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD1Ev
	.quad	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvEN13ObjectWithPtrD0Ev
	.quad	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr10MemoryInfoEPN4node13MemoryTrackerE
	.quad	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr14MemoryInfoNameEv
	.quad	_ZZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEvENK13ObjectWithPtr8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.weak	_ZTV36BaseObjectPtrTest_NestedClasses_Test
	.section	.data.rel.ro.local._ZTV36BaseObjectPtrTest_NestedClasses_Test,"awG",@progbits,_ZTV36BaseObjectPtrTest_NestedClasses_Test,comdat
	.align 8
	.type	_ZTV36BaseObjectPtrTest_NestedClasses_Test, @object
	.size	_ZTV36BaseObjectPtrTest_NestedClasses_Test, 64
_ZTV36BaseObjectPtrTest_NestedClasses_Test:
	.quad	0
	.quad	0
	.quad	_ZN36BaseObjectPtrTest_NestedClasses_TestD1Ev
	.quad	_ZN36BaseObjectPtrTest_NestedClasses_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN36BaseObjectPtrTest_NestedClasses_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.globl	_ZN36BaseObjectPtrTest_NestedClasses_Test10test_info_E
	.bss
	.align 8
	.type	_ZN36BaseObjectPtrTest_NestedClasses_Test10test_info_E, @object
	.size	_ZN36BaseObjectPtrTest_NestedClasses_Test10test_info_E, 8
_ZN36BaseObjectPtrTest_NestedClasses_Test10test_info_E:
	.zero	8
	.globl	_ZN31BaseObjectPtrTest_Moveable_Test10test_info_E
	.align 8
	.type	_ZN31BaseObjectPtrTest_Moveable_Test10test_info_E, @object
	.size	_ZN31BaseObjectPtrTest_Moveable_Test10test_info_E, 8
_ZN31BaseObjectPtrTest_Moveable_Test10test_info_E:
	.zero	8
	.globl	_ZN29BaseObjectPtrTest_GCWeak_Test10test_info_E
	.align 8
	.type	_ZN29BaseObjectPtrTest_GCWeak_Test10test_info_E, @object
	.size	_ZN29BaseObjectPtrTest_GCWeak_Test10test_info_E, 8
_ZN29BaseObjectPtrTest_GCWeak_Test10test_info_E:
	.zero	8
	.globl	_ZN33BaseObjectPtrTest_Undetached_Test10test_info_E
	.align 8
	.type	_ZN33BaseObjectPtrTest_Undetached_Test10test_info_E, @object
	.size	_ZN33BaseObjectPtrTest_Undetached_Test10test_info_E, 8
_ZN33BaseObjectPtrTest_Undetached_Test10test_info_E:
	.zero	8
	.globl	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test10test_info_E
	.align 8
	.type	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test10test_info_E, @object
	.size	_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test10test_info_E, 8
_ZN45BaseObjectPtrTest_ScopedDetachedWithWeak_Test10test_info_E:
	.zero	8
	.globl	_ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E
	.align 8
	.type	_ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E, @object
	.size	_ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E, 8
_ZN37BaseObjectPtrTest_ScopedDetached_Test10test_info_E:
	.zero	8
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"../test/cctest/node_test_fixture.h:140"
	.section	.rodata.str1.1
.LC49:
	.string	"(nullptr) != (environment_)"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"EnvironmentTestFixture::Env::Env(const v8::HandleScope&, const Argv&)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"../test/cctest/node_test_fixture.h:135"
	.section	.rodata.str1.1
.LC52:
	.string	"(nullptr) != (isolate_data_)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC50
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"../test/cctest/node_test_fixture.h:129"
	.section	.rodata.str1.1
.LC54:
	.string	"!context_.IsEmpty()"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC50
	.weak	_ZZN15NodeTestFixture5SetUpEvE4args
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"../test/cctest/node_test_fixture.h:108"
	.section	.rodata.str1.1
.LC56:
	.string	"(isolate_) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"virtual void NodeTestFixture::SetUp()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture5SetUpEvE4args,"awG",@progbits,_ZZN15NodeTestFixture5SetUpEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture5SetUpEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture5SetUpEvE4args, 24
_ZZN15NodeTestFixture5SetUpEvE4args:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.weak	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"../test/cctest/node_test_fixture.h:101"
	.align 8
.LC59:
	.string	"(0) == (uv_loop_close(&current_loop))"
	.align 8
.LC60:
	.string	"static void NodeTestFixture::TearDownTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture16TearDownTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture16TearDownTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, 24
_ZZN15NodeTestFixture16TearDownTestCaseEvE4args:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.weak	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../test/cctest/node_test_fixture.h:87"
	.align 8
.LC62:
	.string	"(0) == (uv_loop_init(&current_loop))"
	.align 8
.LC63:
	.string	"static void NodeTestFixture::SetUpTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture13SetUpTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture13SetUpTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, 24
_ZZN15NodeTestFixture13SetUpTestCaseEvE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC64:
	.string	"../src/tracing/agent.h:91"
.LC65:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC67:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC69:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC70:
	.string	"../src/base_object-inl.h:195"
.LC71:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC69
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC72:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC74:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC75:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC77:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC78:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC80:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC81:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC83:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC84:
	.string	"../src/base_object-inl.h:44"
.LC85:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC84
	.quad	.LC85
	.quad	.LC83
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC86:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC88:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro
	.align 8
.LC10:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC11:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC12:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
