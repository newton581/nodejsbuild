	.file	"test_sockaddr.cc"
	.text
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4971:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4971:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4972:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4972:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6406:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6406:
	.size	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node13SocketAddress8SelfSizeEv,"axG",@progbits,_ZNK4node13SocketAddress8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13SocketAddress8SelfSizeEv
	.type	_ZNK4node13SocketAddress8SelfSizeEv, @function
_ZNK4node13SocketAddress8SelfSizeEv:
.LFB6408:
	.cfi_startproc
	endbr64
	movl	$136, %eax
	ret
	.cfi_endproc
.LFE6408:
	.size	_ZNK4node13SocketAddress8SelfSizeEv, .-_ZNK4node13SocketAddress8SelfSizeEv
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB7485:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7485:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED2Ev:
.LFB9758:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9758:
	.size	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED1Ev,_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED2Ev:
.LFB9766:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9766:
	.size	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED1Ev,_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED2Ev
	.section	.text._ZN4node13SocketAddressD2Ev,"axG",@progbits,_ZN4node13SocketAddressD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13SocketAddressD2Ev
	.type	_ZN4node13SocketAddressD2Ev, @function
_ZN4node13SocketAddressD2Ev:
.LFB9786:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9786:
	.size	_ZN4node13SocketAddressD2Ev, .-_ZN4node13SocketAddressD2Ev
	.weak	_ZN4node13SocketAddressD1Ev
	.set	_ZN4node13SocketAddressD1Ev,_ZN4node13SocketAddressD2Ev
	.section	.text._ZN4node13SocketAddressD0Ev,"axG",@progbits,_ZN4node13SocketAddressD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13SocketAddressD0Ev
	.type	_ZN4node13SocketAddressD0Ev, @function
_ZN4node13SocketAddressD0Ev:
.LFB9788:
	.cfi_startproc
	endbr64
	movl	$136, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9788:
	.size	_ZN4node13SocketAddressD0Ev, .-_ZN4node13SocketAddressD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED0Ev:
.LFB9768:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9768:
	.size	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED0Ev:
.LFB9760:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9760:
	.size	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED0Ev
	.section	.text._ZN36SocketAddress_SocketAddressIPv6_TestD2Ev,"axG",@progbits,_ZN36SocketAddress_SocketAddressIPv6_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN36SocketAddress_SocketAddressIPv6_TestD2Ev
	.type	_ZN36SocketAddress_SocketAddressIPv6_TestD2Ev, @function
_ZN36SocketAddress_SocketAddressIPv6_TestD2Ev:
.LFB9754:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV36SocketAddress_SocketAddressIPv6_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE9754:
	.size	_ZN36SocketAddress_SocketAddressIPv6_TestD2Ev, .-_ZN36SocketAddress_SocketAddressIPv6_TestD2Ev
	.weak	_ZN36SocketAddress_SocketAddressIPv6_TestD1Ev
	.set	_ZN36SocketAddress_SocketAddressIPv6_TestD1Ev,_ZN36SocketAddress_SocketAddressIPv6_TestD2Ev
	.section	.text._ZN36SocketAddress_SocketAddressIPv6_TestD0Ev,"axG",@progbits,_ZN36SocketAddress_SocketAddressIPv6_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN36SocketAddress_SocketAddressIPv6_TestD0Ev
	.type	_ZN36SocketAddress_SocketAddressIPv6_TestD0Ev, @function
_ZN36SocketAddress_SocketAddressIPv6_TestD0Ev:
.LFB9756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV36SocketAddress_SocketAddressIPv6_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9756:
	.size	_ZN36SocketAddress_SocketAddressIPv6_TestD0Ev, .-_ZN36SocketAddress_SocketAddressIPv6_TestD0Ev
	.section	.text._ZN32SocketAddress_SocketAddress_TestD2Ev,"axG",@progbits,_ZN32SocketAddress_SocketAddress_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32SocketAddress_SocketAddress_TestD2Ev
	.type	_ZN32SocketAddress_SocketAddress_TestD2Ev, @function
_ZN32SocketAddress_SocketAddress_TestD2Ev:
.LFB9762:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV32SocketAddress_SocketAddress_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE9762:
	.size	_ZN32SocketAddress_SocketAddress_TestD2Ev, .-_ZN32SocketAddress_SocketAddress_TestD2Ev
	.weak	_ZN32SocketAddress_SocketAddress_TestD1Ev
	.set	_ZN32SocketAddress_SocketAddress_TestD1Ev,_ZN32SocketAddress_SocketAddress_TestD2Ev
	.section	.text._ZN32SocketAddress_SocketAddress_TestD0Ev,"axG",@progbits,_ZN32SocketAddress_SocketAddress_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32SocketAddress_SocketAddress_TestD0Ev
	.type	_ZN32SocketAddress_SocketAddress_TestD0Ev, @function
_ZN32SocketAddress_SocketAddress_TestD0Ev:
.LFB9764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV32SocketAddress_SocketAddress_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9764:
	.size	_ZN32SocketAddress_SocketAddress_TestD0Ev, .-_ZN32SocketAddress_SocketAddress_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv:
.LFB9847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV32SocketAddress_SocketAddress_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9847:
	.size	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv:
.LFB9846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV36SocketAddress_SocketAddressIPv6_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9846:
	.size	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0:
.LFB9942:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9942:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0:
.LFB9943:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9943:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB9960:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L41
	cmpq	$1, %rax
	jne	.L34
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L35:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L35
	jmp	.L33
.L41:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L33:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L35
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9960:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev:
.LFB6407:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1936028260, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7224183256221183827, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$115, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE6407:
	.size	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev, .-_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev
	.section	.rodata.str1.1
.LC4:
	.string	"::1"
	.text
	.align 2
	.p2align 4
	.globl	_ZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEv
	.type	_ZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEv, @function
_ZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEv:
.LFB7642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$443, %edx
	movl	$10, %edi
	leaq	.LC4(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	-368(%rbp), %rbx
	movq	%rbx, %rcx
	subq	$384, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage@PLT
	leaq	16+_ZTVN4node13SocketAddressE(%rip), %rax
	cmpw	$2, -368(%rbp)
	movl	$28, %edx
	movq	%rax, -240(%rbp)
	movl	$16, %eax
	leaq	-232(%rbp), %rcx
	cmovne	%rdx, %rax
	movq	-8(%rbx,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subl	$1, %eax
	xorl	%edx, %edx
	andl	$-8, %eax
.L46:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rbx,%rsi), %rdi
	movq	%rdi, (%rcx,%rsi)
	cmpl	%eax, %edx
	jb	.L46
	movzwl	-232(%rbp), %eax
	cmpw	$2, %ax
	je	.L84
	cmpw	$10, %ax
	jne	.L85
	leaq	-96(%rbp), %r12
	movl	$46, %ecx
	leaq	-224(%rbp), %rsi
	movl	$10, %edi
	movq	%r12, %rdx
	leaq	-384(%rbp), %r13
	movq	%r12, %rbx
	call	uv_inet_ntop@PLT
	movq	%r13, -400(%rbp)
.L50:
	movl	(%rbx), %edx
	addq	$4, %rbx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L50
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rbx), %rdx
	cmove	%rdx, %rbx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rbx
	subq	%r12, %rbx
	movq	%rbx, -408(%rbp)
	cmpq	$15, %rbx
	ja	.L86
	cmpq	$1, %rbx
	jne	.L54
	movzbl	-96(%rbp), %eax
	leaq	-400(%rbp), %r14
	movb	%al, -384(%rbp)
	movq	%r13, %rax
.L55:
	movq	%rbx, -392(%rbp)
	movq	%r14, %rdi
	leaq	.LC4(%rip), %rsi
	movb	$0, (%rax,%rbx)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-400(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	%r13, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	testl	%ebx, %ebx
	jne	.L87
	movzwl	-232(%rbp), %edx
	movl	%edx, %eax
	andl	$-9, %eax
	cmpw	$2, %ax
	jne	.L88
	movzwl	-230(%rbp), %eax
	rolw	$8, %ax
	cmpw	$2, %dx
	je	.L89
	cmpw	$443, %ax
	jne	.L66
	cmpw	$10, %dx
	jne	.L67
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$384, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%r13, %rax
	leaq	-400(%rbp), %r14
	testq	%rbx, %rbx
	je	.L55
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	-400(%rbp), %r14
	xorl	%edx, %edx
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-408(%rbp), %rdx
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
.L53:
	movl	%ebx, %edx
	cmpl	$8, %ebx
	jb	.L91
	movq	-96(%rbp), %rdx
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r12,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rsi, %rax
	leal	(%rbx,%rax), %edx
	subq	%rax, %r12
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L57
	andl	$-8, %edx
	xorl	%eax, %eax
.L60:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%r12,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L60
.L57:
	movq	-408(%rbp), %rbx
	movq	-400(%rbp), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L89:
	cmpw	$443, %ax
	jne	.L66
.L67:
	leaq	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	andl	$4, %ebx
	jne	.L92
	testl	%edx, %edx
	je	.L57
	movzbl	(%r12), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L57
	movzwl	-2(%r12,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	_ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	movl	(%r12), %ecx
	movl	%ecx, (%rax)
	movl	-4(%r12,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L57
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7642:
	.size	_ZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEv, .-_ZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEv
	.section	.text._ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.type	_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, @function
_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_:
.LFB8833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%r15), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	call	_ZNK4node13SocketAddress4HashclERKS0_@PLT
	movq	8(%r12), %r10
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r10
	leaq	0(,%rdx,8), %rax
	movq	%rax, -72(%rbp)
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L94
	movq	(%rax), %rbx
	movq	%rdx, %r11
	movq	152(%rbx), %rsi
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L94
	movq	152(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r10
	cmpq	%rdx, %r11
	jne	.L94
.L98:
	cmpq	%rsi, %r13
	jne	.L95
	movzwl	8(%r15), %eax
	cmpw	16(%rbx), %ax
	jne	.L95
	cmpw	$2, %ax
	movl	$28, %edx
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	movl	$16, %eax
	movq	%r11, -64(%rbp)
	cmove	%rax, %rdx
	movq	%r10, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r11
	testl	%eax, %eax
	jne	.L95
.L137:
	addq	$40, %rsp
	leaq	144(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$160, %edi
	call	_Znwm@PLT
	cmpw	$2, 8(%r15)
	movl	$28, %edx
	movq	%r14, %r9
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node13SocketAddressE(%rip), %rax
	movq	%rax, 8(%rbx)
	leaq	24(%rbx), %rsi
	movl	$16, %eax
	movq	8(%r15), %rcx
	cmove	%rax, %rdx
	leaq	16(%rbx), %rax
	andq	$-8, %rsi
	subq	%rsi, %rax
	movq	%rcx, 16(%rbx)
	subq	%rax, %r9
	movq	-8(%r14,%rdx), %rcx
	addl	%edx, %eax
	andl	$-8, %eax
	movq	%rcx, 8(%rbx,%rdx)
	cmpl	$8, %eax
	jb	.L100
	andl	$-8, %eax
	xorl	%edx, %edx
.L101:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%r9,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L101
.L100:
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, 144(%rbx)
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L103
	movq	(%r12), %r15
.L104:
	movq	%r13, 152(%rbx)
	movq	-72(%rbp), %r13
	addq	%r15, %r13
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L113
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	0(%r13), %rax
	movq	%rbx, (%rax)
.L114:
	addq	$1, 24(%r12)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L103:
	cmpq	$1, %rdx
	je	.L138
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L139
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%r12), %r10
.L106:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L108
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L111:
	testq	%rsi, %rsi
	je	.L108
.L109:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	152(%rcx), %rax
	divq	%r14
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L110
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L118
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L109
	.p2align 4,,10
	.p2align 3
.L108:
	movq	(%r12), %rdi
	cmpq	%rdi, %r10
	je	.L112
	call	_ZdlPv@PLT
.L112:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	divq	%r14
	movq	%r15, (%r12)
	leaq	0(,%rdx,8), %rax
	movq	%rax, -72(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L113:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L115
	movq	152(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r15,%rdx,8)
.L115:
	leaq	16(%r12), %rax
	movq	%rax, 0(%r13)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rdx, %rdi
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L138:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r15
	movq	%r15, %r10
	jmp	.L106
.L139:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8833:
	.size	_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_, .-_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	.section	.rodata.str1.1
.LC5:
	.string	"123.123.123.123"
.LC6:
	.string	"localhost"
.LC7:
	.string	"1.1.1.1"
	.text
	.align 2
	.p2align 4
	.globl	_ZN32SocketAddress_SocketAddress_Test8TestBodyEv
	.type	_ZN32SocketAddress_SocketAddress_Test8TestBodyEv, @function
_ZN32SocketAddress_SocketAddress_Test8TestBodyEv:
.LFB7613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rsi
	movl	$2, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-256(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdx
	subq	$760, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	inet_pton@PLT
	cmpl	$1, %eax
	jne	.L241
.L141:
	movq	%r12, %rdx
	leaq	.LC6(%rip), %rsi
	movl	$2, %edi
	call	inet_pton@PLT
	cmpl	$1, %eax
	jne	.L242
.L142:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r12, %rdx
	leaq	.LC6(%rip), %rsi
	movl	$10, %edi
	call	inet_pton@PLT
	cmpl	$1, %eax
	je	.L142
	leaq	-656(%rbp), %r13
	movl	$443, %edx
	leaq	.LC5(%rip), %rsi
	movl	$2, %edi
	movq	%r13, %rcx
	leaq	-528(%rbp), %rbx
	leaq	-392(%rbp), %r14
	call	_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage@PLT
	movl	$80, %edx
	movq	%rbx, %rcx
	leaq	.LC7(%rip), %rsi
	movl	$2, %edi
	call	_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage@PLT
	movl	$28, %edx
	cmpw	$2, -656(%rbp)
	movl	$16, %eax
	cmovne	%rdx, %rax
	leaq	16+_ZTVN4node13SocketAddressE(%rip), %rsi
	movq	%rsi, -400(%rbp)
	movq	-8(%r13,%rax), %rdx
	movq	%rdx, -8(%r14,%rax)
	subl	$1, %eax
	xorl	%edx, %edx
	andl	$-8, %eax
.L145:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	0(%r13,%rcx), %rdi
	movq	%rdi, (%r14,%rcx)
	cmpl	%eax, %edx
	jb	.L145
	cmpw	$2, -528(%rbp)
	movl	$28, %edx
	movl	$16, %eax
	movq	%rsi, -256(%rbp)
	cmovne	%rdx, %rax
	leaq	-248(%rbp), %rsi
	movq	%rsi, -776(%rbp)
	movq	-8(%rbx,%rax), %rdx
	movq	%rdx, -256(%rbp,%rax)
	subl	$1, %eax
	xorl	%edx, %edx
	andl	$-8, %eax
.L148:
	movl	%edx, %ecx
	movq	-776(%rbp), %rdi
	addl	$8, %edx
	movq	(%rbx,%rcx), %rsi
	movq	%rsi, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L148
	cmpw	$2, -392(%rbp)
	je	.L150
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	-388(%rbp), %rax
	leaq	-112(%rbp), %rbx
	movl	$46, %ecx
	movl	$2, %edi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	leaq	-672(%rbp), %r13
	movq	%rax, -784(%rbp)
	call	uv_inet_ntop@PLT
	movq	%r13, -688(%rbp)
	movq	%rbx, %rcx
.L151:
	movl	(%rcx), %edx
	addq	$4, %rcx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L151
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rcx), %rdx
	cmove	%rdx, %rcx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rcx
	subq	%rbx, %rcx
	movq	%rcx, -752(%rbp)
	cmpq	$15, %rcx
	ja	.L243
	cmpq	$1, %rcx
	jne	.L155
	movzbl	-112(%rbp), %eax
	leaq	-688(%rbp), %r15
	movb	%al, -672(%rbp)
	movq	%r13, %rax
.L156:
	movq	%rcx, -680(%rbp)
	movq	%r15, %rdi
	leaq	.LC5(%rip), %rsi
	movb	$0, (%rax,%rcx)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-688(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L163
	movl	%eax, -792(%rbp)
	call	_ZdlPv@PLT
	movl	-792(%rbp), %eax
.L163:
	testl	%eax, %eax
	jne	.L244
	movzwl	-392(%rbp), %edx
	movl	%edx, %eax
	andl	$-9, %eax
	cmpw	$2, %ax
	jne	.L189
	movzwl	-390(%rbp), %eax
	rolw	$8, %ax
	cmpw	$2, %dx
	je	.L245
	cmpw	$443, %ax
	jne	.L167
	cmpw	$10, %dx
	jne	.L170
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_5(%rip), %rdi
	movl	$12345, -388(%rbp)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%r12, %rdx
	leaq	.LC5(%rip), %rsi
	movl	$10, %edi
	call	inet_pton@PLT
	cmpl	$1, %eax
	je	.L141
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r13, %rax
	leaq	-688(%rbp), %r15
	testq	%rcx, %rcx
	je	.L156
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	-688(%rbp), %r15
	xorl	%edx, %edx
	leaq	-752(%rbp), %rsi
	movq	%rcx, -792(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-752(%rbp), %rdx
	movq	-792(%rbp), %rcx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
.L154:
	movl	%ecx, %edx
	cmpl	$8, %ecx
	jnb	.L157
	andl	$4, %ecx
	jne	.L246
	testl	%edx, %edx
	je	.L158
	movzbl	(%rbx), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L158
	movzwl	-2(%rbx,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	.p2align 4,,10
	.p2align 3
.L158:
	movq	-752(%rbp), %rcx
	movq	-688(%rbp), %rax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L245:
	cmpw	$443, %ax
	jne	.L167
	cmpw	$2, -248(%rbp)
	movl	$16, %edx
	je	.L196
.L171:
	leaq	-753(%rbp), %r8
	leaq	-400(%rbp), %r14
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%r8, -800(%rbp)
	call	_ZNK4node13SocketAddress4HashclERKS0_@PLT
	leaq	-752(%rbp), %rsi
	movq	%rsi, -776(%rbp)
	movq	%rsi, %rdi
	movq	%r14, %rsi
	movq	%rax, -792(%rbp)
	call	_ZNK4node13SocketAddress4HashclERKS0_@PLT
	cmpq	%rax, -792(%rbp)
	movq	-800(%rbp), %r8
	jne	.L247
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZNK4node13SocketAddress4HashclERKS0_@PLT
	movq	-776(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -792(%rbp)
	call	_ZNK4node13SocketAddress4HashclERKS0_@PLT
	cmpq	%rax, -792(%rbp)
	je	.L248
	movq	-528(%rbp), %rax
	movq	-520(%rbp), %rdx
	movq	%rax, -392(%rbp)
	movq	%rdx, -384(%rbp)
	cmpw	$2, %ax
	je	.L174
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__10_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$46, %ecx
	movq	%rbx, %rdx
	movl	$2, %edi
	movq	%rbx, %r12
	movq	-784(%rbp), %rsi
	call	uv_inet_ntop@PLT
	movq	%r13, -688(%rbp)
.L175:
	movl	(%r12), %edx
	addq	$4, %r12
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L175
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r12), %rdx
	cmove	%rdx, %r12
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %r12
	subq	%rbx, %r12
	movq	%r12, -752(%rbp)
	cmpq	$15, %r12
	ja	.L249
	cmpq	$1, %r12
	jne	.L179
	movzbl	-112(%rbp), %eax
	movb	%al, -672(%rbp)
	movq	%r13, %rax
.L180:
	movq	%r12, -680(%rbp)
	movq	%r15, %rdi
	leaq	.LC7(%rip), %rsi
	movb	$0, (%rax,%r12)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-688(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	%r13, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	testl	%ebx, %ebx
	jne	.L250
	movzwl	-392(%rbp), %eax
	andl	$-9, %eax
	cmpw	$2, %ax
	jne	.L189
	cmpw	$20480, -390(%rbp)
	jne	.L251
	movq	-776(%rbp), %rbx
	movq	%r14, %rsi
	leaq	-704(%rbp), %r12
	movq	$1, -744(%rbp)
	movq	%r12, -752(%rbp)
	movq	$0, -736(%rbp)
	movq	%rbx, %rdi
	movq	$0, -728(%rbp)
	movl	$0x3f800000, -720(%rbp)
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	call	_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	%r14, %rsi
	movq	%rbx, %rdi
	addq	$1, (%rax)
	call	_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	movq	%r14, %rsi
	movq	%rbx, %rdi
	addq	$1, (%rax)
	call	_ZNSt8__detail9_Map_baseIN4node13SocketAddressESt4pairIKS2_mESaIS5_ENS_10_Select1stESt8equal_toIS2_ENS2_4HashENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS4_
	cmpq	$2, (%rax)
	jne	.L252
	movq	-736(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L195
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L192
.L195:
	movq	-744(%rbp), %rax
	movq	-752(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-752(%rbp), %rdi
	movq	$0, -728(%rbp)
	movq	$0, -736(%rbp)
	cmpq	%r12, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L253
	addq	$760, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	cmpw	%dx, -248(%rbp)
	jne	.L171
	movl	$28, %edx
.L196:
	movq	-776(%rbp), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L171
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	movq	-112(%rbp), %rdx
	leaq	8(%rax), %rdi
	movq	%rbx, %r8
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ecx, %edx
	movq	-8(%rbx,%rdx), %rsi
	movq	%rsi, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rcx,%rax), %edx
	subq	%rax, %r8
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L158
	andl	$-8, %edx
	xorl	%eax, %eax
.L161:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%r8,%rcx), %rsi
	movq	%rsi, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L161
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	_ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r13, %rax
	testq	%r12, %r12
	je	.L180
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L249:
	movq	-776(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-752(%rbp), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
.L178:
	movl	%r12d, %edx
	cmpl	$8, %r12d
	jnb	.L181
	andl	$4, %r12d
	jne	.L254
	testl	%edx, %edx
	je	.L182
	movzbl	(%rbx), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	jne	.L255
.L182:
	movq	-752(%rbp), %r12
	movq	-688(%rbp), %rax
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-112(%rbp), %rdx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%r12d, %edx
	movq	-8(%rbx,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%r12,%rax), %edx
	subq	%rax, %rbx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L182
	andl	$-8, %edx
	xorl	%eax, %eax
.L185:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rbx,%rcx), %rsi
	movq	%rsi, (%rdi,%rcx)
	cmpl	%edx, %eax
	jb	.L185
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_8(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_9(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__12_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__13_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__14_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L246:
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	movl	-4(%rbx,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L158
.L254:
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	movl	-4(%rbx,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L182
.L255:
	movzwl	-2(%rbx,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L182
.L253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7613:
	.size	_ZN32SocketAddress_SocketAddress_Test8TestBodyEv, .-_ZN32SocketAddress_SocketAddress_Test8TestBodyEv
	.section	.rodata._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB9225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L257
	testq	%rsi, %rsi
	je	.L273
.L257:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L274
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L260
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L261:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L261
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L259:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L261
.L273:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9225:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"../test/cctest/test_sockaddr.cc"
	.section	.rodata.str1.1
.LC11:
	.string	"SocketAddress"
.LC12:
	.string	"SocketAddressIPv6"
	.section	.text.startup
	.p2align 4
	.type	_GLOBAL__sub_I__ZN32SocketAddress_SocketAddress_Test10test_info_E, @function
_GLOBAL__sub_I__ZN32SocketAddress_SocketAddress_Test10test_info_E:
.LFB9926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L287
.L277:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L288
.L278:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	-128(%rbp), %r14
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	movq	%rax, %r15
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	subq	$8, %rsp
	movq	%r15, %r9
	movq	%r12, %r8
	pushq	%r13
	leaq	.LC11(%rip), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	movq	%rsi, %rdi
	pushq	$0
	movl	$6, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN32SocketAddress_SocketAddress_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L289
.L281:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L290
.L282:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	subq	$8, %rsp
	movq	%r12, %r8
	xorl	%ecx, %ecx
	pushq	%r13
	movq	-136(%rbp), %r9
	leaq	.LC11(%rip), %rdi
	xorl	%edx, %edx
	pushq	$0
	leaq	.LC12(%rip), %rsi
	pushq	$0
	movl	$44, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN36SocketAddress_SocketAddressIPv6_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L287:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L277
.L290:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L282
.L289:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L281
.L288:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L278
.L291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9926:
	.size	_GLOBAL__sub_I__ZN32SocketAddress_SocketAddress_Test10test_info_E, .-_GLOBAL__sub_I__ZN32SocketAddress_SocketAddress_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN32SocketAddress_SocketAddress_Test10test_info_E
	.weak	_ZTVN4node13SocketAddressE
	.section	.data.rel.ro.local._ZTVN4node13SocketAddressE,"awG",@progbits,_ZTVN4node13SocketAddressE,comdat
	.align 8
	.type	_ZTVN4node13SocketAddressE, @object
	.size	_ZTVN4node13SocketAddressE, 72
_ZTVN4node13SocketAddressE:
	.quad	0
	.quad	0
	.quad	_ZN4node13SocketAddressD1Ev
	.quad	_ZN4node13SocketAddressD0Ev
	.quad	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node13SocketAddress8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32SocketAddress_SocketAddress_TestE10CreateTestEv
	.weak	_ZTV32SocketAddress_SocketAddress_Test
	.section	.data.rel.ro._ZTV32SocketAddress_SocketAddress_Test,"awG",@progbits,_ZTV32SocketAddress_SocketAddress_Test,comdat
	.align 8
	.type	_ZTV32SocketAddress_SocketAddress_Test, @object
	.size	_ZTV32SocketAddress_SocketAddress_Test, 64
_ZTV32SocketAddress_SocketAddress_Test:
	.quad	0
	.quad	0
	.quad	_ZN32SocketAddress_SocketAddress_TestD1Ev
	.quad	_ZN32SocketAddress_SocketAddress_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN32SocketAddress_SocketAddress_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI36SocketAddress_SocketAddressIPv6_TestE10CreateTestEv
	.weak	_ZTV36SocketAddress_SocketAddressIPv6_Test
	.section	.data.rel.ro._ZTV36SocketAddress_SocketAddressIPv6_Test,"awG",@progbits,_ZTV36SocketAddress_SocketAddressIPv6_Test,comdat
	.align 8
	.type	_ZTV36SocketAddress_SocketAddressIPv6_Test, @object
	.size	_ZTV36SocketAddress_SocketAddressIPv6_Test, 64
_ZTV36SocketAddress_SocketAddressIPv6_Test:
	.quad	0
	.quad	0
	.quad	_ZN36SocketAddress_SocketAddressIPv6_TestD1Ev
	.quad	_ZN36SocketAddress_SocketAddressIPv6_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"../test/cctest/test_sockaddr.cc:56"
	.align 8
.LC14:
	.string	"(addr.flow_label()) == (12345)"
	.align 8
.LC15:
	.string	"virtual void SocketAddress_SocketAddressIPv6_Test::TestBody()"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_3, @object
	.size	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_3, 24
_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_3:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"../test/cctest/test_sockaddr.cc:53"
	.section	.rodata.str1.1
.LC17:
	.string	"(addr.port()) == (443)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_2, @object
	.size	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_2, 24
_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_2:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC15
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"../test/cctest/test_sockaddr.cc:52"
	.section	.rodata.str1.1
.LC19:
	.string	"(addr.address()) == (\"::1\")"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_1, @object
	.size	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_1, 24
_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_1:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC15
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"../test/cctest/test_sockaddr.cc:51"
	.section	.rodata.str1.1
.LC21:
	.string	"(addr.family()) == (10)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_0, @object
	.size	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_0, 24
_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args_0:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC15
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"../test/cctest/test_sockaddr.cc:50"
	.align 8
.LC23:
	.string	"(addr.length()) == (sizeof(sockaddr_in6))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args, @object
	.size	_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args, 24
_ZZN36SocketAddress_SocketAddressIPv6_Test8TestBodyEvE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC15
	.globl	_ZN36SocketAddress_SocketAddressIPv6_Test10test_info_E
	.bss
	.align 8
	.type	_ZN36SocketAddress_SocketAddressIPv6_Test10test_info_E, @object
	.size	_ZN36SocketAddress_SocketAddressIPv6_Test10test_info_E, 8
_ZN36SocketAddress_SocketAddressIPv6_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"../test/cctest/test_sockaddr.cc:41"
	.section	.rodata.str1.1
.LC25:
	.string	"(map[addr]) == (2)"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"virtual void SocketAddress_SocketAddress_Test::TestBody()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__14_, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__14_, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__14_:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"../test/cctest/test_sockaddr.cc:36"
	.section	.rodata.str1.1
.LC28:
	.string	"(addr.port()) == (80)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__13_, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__13_, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__13_:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"../test/cctest/test_sockaddr.cc:35"
	.align 8
.LC30:
	.string	"(addr.address()) == (\"1.1.1.1\")"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__12_, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__12_, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__12_:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"../test/cctest/test_sockaddr.cc:33"
	.align 8
.LC32:
	.string	"(addr.length()) == (sizeof(sockaddr_in))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__10_, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__10_, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args__10_:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"../test/cctest/test_sockaddr.cc:30"
	.align 8
.LC34:
	.string	"(SocketAddress::Hash()(addr)) != (SocketAddress::Hash()(addr2))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_9, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_9, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_9:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"../test/cctest/test_sockaddr.cc:29"
	.align 8
.LC36:
	.string	"(SocketAddress::Hash()(addr)) == (SocketAddress::Hash()(addr))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_8, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_8, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_8:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"../test/cctest/test_sockaddr.cc:26"
	.section	.rodata.str1.1
.LC38:
	.string	"(addr) != (addr2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_6, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_6, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_6:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"../test/cctest/test_sockaddr.cc:24"
	.section	.rodata.str1.1
.LC40:
	.string	"(addr.flow_label()) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_5, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_5, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_5:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"../test/cctest/test_sockaddr.cc:21"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_4, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_4, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_4:
	.quad	.LC41
	.quad	.LC17
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"../test/cctest/test_sockaddr.cc:20"
	.align 8
.LC43:
	.string	"(addr.address()) == (\"123.123.123.123\")"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_3, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_3, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_3:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"../test/cctest/test_sockaddr.cc:18"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_1, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_1, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_1:
	.quad	.LC44
	.quad	.LC32
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"../test/cctest/test_sockaddr.cc:8"
	.align 8
.LC46:
	.string	"!SocketAddress::is_numeric_host(\"localhost\")"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_0, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_0, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args_0:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC26
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"../test/cctest/test_sockaddr.cc:7"
	.align 8
.LC48:
	.string	"SocketAddress::is_numeric_host(\"123.123.123.123\")"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args, @object
	.size	_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args, 24
_ZZN32SocketAddress_SocketAddress_Test8TestBodyEvE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC26
	.globl	_ZN32SocketAddress_SocketAddress_Test10test_info_E
	.bss
	.align 8
	.type	_ZN32SocketAddress_SocketAddress_Test10test_info_E, @object
	.size	_ZN32SocketAddress_SocketAddress_Test10test_info_E, 8
_ZN32SocketAddress_SocketAddress_Test10test_info_E:
	.zero	8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args
	.section	.rodata.str1.1
.LC49:
	.string	"../src/node_sockaddr-inl.h:36"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"addr->sa_family == 2 || addr->sa_family == 10"
	.align 8
.LC51:
	.string	"static int node::SocketAddress::GetPort(const sockaddr*)"
	.section	.data.rel.ro.local._ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args,"awG",@progbits,_ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args,comdat
	.align 16
	.type	_ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args, @gnu_unique_object
	.size	_ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args, 24
_ZZN4node13SocketAddress7GetPortEPK8sockaddrE4args:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
