	.file	"test_traced_value.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB5991:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5991:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED2Ev:
.LFB7725:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7725:
	.size	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED1Ev,_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED2Ev:
.LFB7733:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7733:
	.size	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED1Ev,_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED2Ev:
.LFB7741:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7741:
	.size	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED2Ev:
.LFB7749:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7749:
	.size	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED1Ev,_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED0Ev:
.LFB7751:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7751:
	.size	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED0Ev:
.LFB7743:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7743:
	.size	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED0Ev:
.LFB7735:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7735:
	.size	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED0Ev:
.LFB7727:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7727:
	.size	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED0Ev
	.section	.text._ZN4node7tracing11TracedValueD0Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD0Ev
	.type	_ZN4node7tracing11TracedValueD0Ev, @function
_ZN4node7tracing11TracedValueD0Ev:
.LFB7093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7093:
	.size	_ZN4node7tracing11TracedValueD0Ev, .-_ZN4node7tracing11TracedValueD0Ev
	.section	.text._ZN30TracedValue_EscapingArray_TestD2Ev,"axG",@progbits,_ZN30TracedValue_EscapingArray_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN30TracedValue_EscapingArray_TestD2Ev
	.type	_ZN30TracedValue_EscapingArray_TestD2Ev, @function
_ZN30TracedValue_EscapingArray_TestD2Ev:
.LFB7721:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV30TracedValue_EscapingArray_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE7721:
	.size	_ZN30TracedValue_EscapingArray_TestD2Ev, .-_ZN30TracedValue_EscapingArray_TestD2Ev
	.weak	_ZN30TracedValue_EscapingArray_TestD1Ev
	.set	_ZN30TracedValue_EscapingArray_TestD1Ev,_ZN30TracedValue_EscapingArray_TestD2Ev
	.section	.text._ZN30TracedValue_EscapingArray_TestD0Ev,"axG",@progbits,_ZN30TracedValue_EscapingArray_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN30TracedValue_EscapingArray_TestD0Ev
	.type	_ZN30TracedValue_EscapingArray_TestD0Ev, @function
_ZN30TracedValue_EscapingArray_TestD0Ev:
.LFB7723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV30TracedValue_EscapingArray_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7723:
	.size	_ZN30TracedValue_EscapingArray_TestD0Ev, .-_ZN30TracedValue_EscapingArray_TestD0Ev
	.section	.text._ZN31TracedValue_EscapingObject_TestD2Ev,"axG",@progbits,_ZN31TracedValue_EscapingObject_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31TracedValue_EscapingObject_TestD2Ev
	.type	_ZN31TracedValue_EscapingObject_TestD2Ev, @function
_ZN31TracedValue_EscapingObject_TestD2Ev:
.LFB7729:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV31TracedValue_EscapingObject_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE7729:
	.size	_ZN31TracedValue_EscapingObject_TestD2Ev, .-_ZN31TracedValue_EscapingObject_TestD2Ev
	.weak	_ZN31TracedValue_EscapingObject_TestD1Ev
	.set	_ZN31TracedValue_EscapingObject_TestD1Ev,_ZN31TracedValue_EscapingObject_TestD2Ev
	.section	.text._ZN31TracedValue_EscapingObject_TestD0Ev,"axG",@progbits,_ZN31TracedValue_EscapingObject_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31TracedValue_EscapingObject_TestD0Ev
	.type	_ZN31TracedValue_EscapingObject_TestD0Ev, @function
_ZN31TracedValue_EscapingObject_TestD0Ev:
.LFB7731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV31TracedValue_EscapingObject_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7731:
	.size	_ZN31TracedValue_EscapingObject_TestD0Ev, .-_ZN31TracedValue_EscapingObject_TestD0Ev
	.section	.text._ZN22TracedValue_Array_TestD2Ev,"axG",@progbits,_ZN22TracedValue_Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22TracedValue_Array_TestD2Ev
	.type	_ZN22TracedValue_Array_TestD2Ev, @function
_ZN22TracedValue_Array_TestD2Ev:
.LFB7737:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV22TracedValue_Array_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE7737:
	.size	_ZN22TracedValue_Array_TestD2Ev, .-_ZN22TracedValue_Array_TestD2Ev
	.weak	_ZN22TracedValue_Array_TestD1Ev
	.set	_ZN22TracedValue_Array_TestD1Ev,_ZN22TracedValue_Array_TestD2Ev
	.section	.text._ZN22TracedValue_Array_TestD0Ev,"axG",@progbits,_ZN22TracedValue_Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22TracedValue_Array_TestD0Ev
	.type	_ZN22TracedValue_Array_TestD0Ev, @function
_ZN22TracedValue_Array_TestD0Ev:
.LFB7739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV22TracedValue_Array_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7739:
	.size	_ZN22TracedValue_Array_TestD0Ev, .-_ZN22TracedValue_Array_TestD0Ev
	.section	.text._ZN23TracedValue_Object_TestD2Ev,"axG",@progbits,_ZN23TracedValue_Object_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN23TracedValue_Object_TestD2Ev
	.type	_ZN23TracedValue_Object_TestD2Ev, @function
_ZN23TracedValue_Object_TestD2Ev:
.LFB7745:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV23TracedValue_Object_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE7745:
	.size	_ZN23TracedValue_Object_TestD2Ev, .-_ZN23TracedValue_Object_TestD2Ev
	.weak	_ZN23TracedValue_Object_TestD1Ev
	.set	_ZN23TracedValue_Object_TestD1Ev,_ZN23TracedValue_Object_TestD2Ev
	.section	.text._ZN23TracedValue_Object_TestD0Ev,"axG",@progbits,_ZN23TracedValue_Object_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN23TracedValue_Object_TestD0Ev
	.type	_ZN23TracedValue_Object_TestD0Ev, @function
_ZN23TracedValue_Object_TestD0Ev:
.LFB7747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV23TracedValue_Object_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7747:
	.size	_ZN23TracedValue_Object_TestD0Ev, .-_ZN23TracedValue_Object_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv:
.LFB7806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV23TracedValue_Object_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7806:
	.size	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv:
.LFB7805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV22TracedValue_Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7805:
	.size	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv:
.LFB7804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV31TracedValue_EscapingObject_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7804:
	.size	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv:
.LFB7803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV30TracedValue_EscapingArray_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7803:
	.size	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0:
.LFB7901:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7901:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0:
.LFB7902:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7902:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB7920:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L53
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L54
	cmpq	$1, %rax
	jne	.L46
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L47:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L47
	jmp	.L45
.L54:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L45:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L47
.L53:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7920:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZN4node7tracing11TracedValueD2Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD2Ev
	.type	_ZN4node7tracing11TracedValueD2Ev, @function
_ZN4node7tracing11TracedValueD2Ev:
.LFB7091:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L56
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	ret
	.cfi_endproc
.LFE7091:
	.size	_ZN4node7tracing11TracedValueD2Ev, .-_ZN4node7tracing11TracedValueD2Ev
	.weak	_ZN4node7tracing11TracedValueD1Ev
	.set	_ZN4node7tracing11TracedValueD1Ev,_ZN4node7tracing11TracedValueD2Ev
	.section	.rodata._ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"NULL"
	.section	.text._ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_
	.type	_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_, @function
_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_:
.LFB7362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-424(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -592(%rbp)
	movq	.LC6(%rip), %xmm1
	movq	%r8, -584(%rbp)
	movq	%rdi, -560(%rbp)
	movhps	.LC7(%rip), %xmm1
	movq	%r12, %rdi
	movq	%rsi, -616(%rbp)
	movq	%rdx, -624(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -576(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	leaq	-432(%rbp), %rax
	xorl	%esi, %esi
	movq	%rax, -552(%rbp)
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-576(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -536(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %r8
	movq	-552(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	leaq	-512(%rbp), %rdi
	movq	$0, -504(%rbp)
	movq	%rax, -632(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -584(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L59
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L79
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L61:
	movq	.LC6(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC8(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-592(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rdx), %r9
	movq	%r9, -592(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %r15
	movq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-552(%rbp), %rax
	addq	-24(%r13), %rax
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, -432(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movdqa	-576(%rbp), %xmm3
	movq	-536(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-544(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-592(%rbp), %r9
	testq	%r9, %r9
	je	.L80
	movq	%r9, %rdi
	leaq	-464(%rbp), %r14
	movq	%r9, -576(%rbp)
	leaq	-480(%rbp), %r15
	movq	%r14, -480(%rbp)
	call	strlen@PLT
	movq	-576(%rbp), %r9
	cmpq	$15, %rax
	movq	%rax, -520(%rbp)
	movq	%rax, %r8
	ja	.L81
	cmpq	$1, %rax
	jne	.L67
	movzbl	(%r9), %edx
	movb	%dl, -464(%rbp)
	movq	%r14, %rdx
.L68:
	movq	%rax, -472(%rbp)
	movq	-552(%rbp), %rsi
	movq	%r15, %rdi
	movb	$0, (%rdx,%rax)
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	movq	-480(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	-384(%rbp), %rax
	movq	%r14, -480(%rbp)
	movq	$0, -472(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L70
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L71
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L72:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-560(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-584(%rbp), %r8
	movq	-624(%rbp), %rdx
	movq	-616(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-480(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	-512(%rbp), %rdi
	cmpq	-632(%rbp), %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	movq	-560(%rbp), %rax
	addq	$600, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L71:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-552(%rbp), %rdi
	movl	$4, %edx
	leaq	.LC5(%rip), %rsi
	leaq	-480(%rbp), %r15
	leaq	-464(%rbp), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	testq	%rax, %rax
	jne	.L83
	movq	%r14, %rdx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r15, %rdi
	leaq	-520(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -592(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-576(%rbp), %r9
	movq	-592(%rbp), %r8
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %rax
	movq	%rax, -464(%rbp)
.L66:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-520(%rbp), %rax
	movq	-480(%rbp), %rdx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L61
.L82:
	call	__stack_chk_fail@PLT
.L83:
	movq	%r14, %rdi
	jmp	.L66
	.cfi_endproc
.LFE7362:
	.size	_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_, .-_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_
	.section	.rodata.str1.1
.LC9:
	.string	""
.LC10:
	.string	"b"
.LC11:
	.string	"a"
.LC13:
	.string	"c"
.LC15:
	.string	"d"
.LC17:
	.string	"e"
.LC19:
	.string	"f"
.LC21:
	.string	"g"
.LC22:
	.string	"h"
.LC23:
	.string	"i"
.LC24:
	.string	"j"
.LC25:
	.string	"k"
.LC26:
	.string	"m"
.LC27:
	.string	"l"
.LC28:
	.string	"string"
.LC29:
	.string	"check"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"../test/cctest/test_traced_value.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN23TracedValue_Object_Test8TestBodyEv
	.type	_ZN23TracedValue_Object_Test8TestBodyEv, @function
_ZN23TracedValue_Object_Test8TestBodyEv:
.LFB6119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-120(%rbp), %rdi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	-120(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	-120(%rbp), %rdi
	movsd	.LC12(%rip), %xmm0
	leaq	.LC13(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetDoubleEPKcd@PLT
	movq	-120(%rbp), %rdi
	movsd	.LC14(%rip), %xmm0
	leaq	.LC15(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetDoubleEPKcd@PLT
	movq	-120(%rbp), %rdi
	movsd	.LC16(%rip), %xmm0
	leaq	.LC17(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetDoubleEPKcd@PLT
	movq	-120(%rbp), %rdi
	movsd	.LC18(%rip), %xmm0
	leaq	.LC19(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetDoubleEPKcd@PLT
	movq	-120(%rbp), %rdi
	movsd	.LC20(%rip), %xmm0
	leaq	.LC21(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetDoubleEPKcd@PLT
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetBooleanEPKcb@PLT
	movq	-120(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetBooleanEPKcb@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZN4node7tracing11TracedValue7SetNullEPKc@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC25(%rip), %rsi
	call	_ZN4node7tracing11TracedValue15BeginDictionaryEPKc@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC26(%rip), %rdx
	leaq	.LC27(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue13EndDictionaryEv@PLT
	movq	-120(%rbp), %rdi
	movb	$0, -64(%rbp)
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	(%rdi), %rax
	movq	$0, -72(%rbp)
	call	*16(%rax)
	movq	_ZZN23TracedValue_Object_Test8TestBodyEvE5check(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	leaq	-96(%rbp), %rdi
	testl	%eax, %eax
	jne	.L85
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -96(%rbp)
	je	.L109
.L87:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L90
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L90:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L92
	call	_ZdlPv@PLT
.L92:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L84
	movq	(%r12), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L94
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L84:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	%r12, %r8
	leaq	_ZZN23TracedValue_Object_Test8TestBodyEvE5check(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_
	cmpb	$0, -96(%rbp)
	jne	.L87
.L109:
	leaq	-104(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-88(%rbp), %rax
	leaq	.LC9(%rip), %r8
	testq	%rax, %rax
	je	.L88
	movq	(%rax), %r8
.L88:
	leaq	-112(%rbp), %r12
	movl	$35, %ecx
	movl	$1, %esi
	leaq	.LC30(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L87
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L84
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6119:
	.size	_ZN23TracedValue_Object_Test8TestBodyEv, .-_ZN23TracedValue_Object_Test8TestBodyEv
	.section	.rodata.str1.1
.LC31:
	.string	"1\342\202\25423\"\001\b\f\n\r\t\\"
	.text
	.align 2
	.p2align 4
	.globl	_ZN30TracedValue_EscapingArray_Test8TestBodyEv
	.type	_ZN30TracedValue_EscapingArray_Test8TestBodyEv, @function
_ZN30TracedValue_EscapingArray_Test8TestBodyEv:
.LFB6142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-120(%rbp), %rdi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7tracing11TracedValue11CreateArrayEv@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC31(%rip), %rsi
	call	_ZN4node7tracing11TracedValue12AppendStringEPKc@PLT
	movq	-120(%rbp), %rdi
	movb	$0, -64(%rbp)
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	(%rdi), %rax
	movq	$0, -72(%rbp)
	call	*16(%rax)
	movq	_ZZN30TracedValue_EscapingArray_Test8TestBodyEvE5check(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	leaq	-96(%rbp), %rdi
	testl	%eax, %eax
	jne	.L112
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -96(%rbp)
	je	.L136
.L114:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L117
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L117:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L111
	movq	(%r12), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L121
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L122
	call	_ZdlPv@PLT
.L122:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L111:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%r12, %r8
	leaq	_ZZN30TracedValue_EscapingArray_Test8TestBodyEvE5check(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_
	cmpb	$0, -96(%rbp)
	jne	.L114
.L136:
	leaq	-104(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-88(%rbp), %rax
	leaq	.LC9(%rip), %r8
	testq	%rax, %rax
	je	.L115
	movq	(%rax), %r8
.L115:
	leaq	-112(%rbp), %r12
	movl	$95, %ecx
	movl	$1, %esi
	leaq	.LC30(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L111
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6142:
	.size	_ZN30TracedValue_EscapingArray_Test8TestBodyEv, .-_ZN30TracedValue_EscapingArray_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN31TracedValue_EscapingObject_Test8TestBodyEv
	.type	_ZN31TracedValue_EscapingObject_Test8TestBodyEv, @function
_ZN31TracedValue_EscapingObject_Test8TestBodyEv:
.LFB6135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-120(%rbp), %rdi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC31(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	-120(%rbp), %rdi
	movb	$0, -64(%rbp)
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	(%rdi), %rax
	movq	$0, -72(%rbp)
	call	*16(%rax)
	movq	_ZZN31TracedValue_EscapingObject_Test8TestBodyEvE5check(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	leaq	-96(%rbp), %rdi
	testl	%eax, %eax
	jne	.L139
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -96(%rbp)
	je	.L163
.L141:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L144
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L144:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L138
	movq	(%r12), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L148
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L138:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	%r12, %r8
	leaq	_ZZN31TracedValue_EscapingObject_Test8TestBodyEvE5check(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_
	cmpb	$0, -96(%rbp)
	jne	.L141
.L163:
	leaq	-104(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-88(%rbp), %rax
	leaq	.LC9(%rip), %r8
	testq	%rax, %rax
	je	.L142
	movq	(%rax), %r8
.L142:
	leaq	-112(%rbp), %r12
	movl	$83, %ecx
	movl	$1, %esi
	leaq	.LC30(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L138
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6135:
	.size	_ZN31TracedValue_EscapingObject_Test8TestBodyEv, .-_ZN31TracedValue_EscapingObject_Test8TestBodyEv
	.section	.rodata.str1.1
.LC32:
	.string	"foo"
	.text
	.align 2
	.p2align 4
	.globl	_ZN22TracedValue_Array_Test8TestBodyEv
	.type	_ZN22TracedValue_Array_Test8TestBodyEv, @function
_ZN22TracedValue_Array_Test8TestBodyEv:
.LFB6128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-120(%rbp), %rdi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7tracing11TracedValue11CreateArrayEv@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZN4node7tracing11TracedValue12AppendStringEPKc@PLT
	movq	-120(%rbp), %rdi
	movl	$1, %esi
	call	_ZN4node7tracing11TracedValue13AppendIntegerEi@PLT
	movsd	.LC12(%rip), %xmm0
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue12AppendDoubleEd@PLT
	movsd	.LC14(%rip), %xmm0
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue12AppendDoubleEd@PLT
	movsd	.LC16(%rip), %xmm0
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue12AppendDoubleEd@PLT
	movsd	.LC18(%rip), %xmm0
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue12AppendDoubleEd@PLT
	movsd	.LC20(%rip), %xmm0
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue12AppendDoubleEd@PLT
	movq	-120(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN4node7tracing11TracedValue13AppendBooleanEb@PLT
	movq	-120(%rbp), %rdi
	movl	$1, %esi
	call	_ZN4node7tracing11TracedValue13AppendBooleanEb@PLT
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue10AppendNullEv@PLT
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue15BeginDictionaryEv@PLT
	movq	-120(%rbp), %rdi
	leaq	.LC32(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10BeginArrayEPKc@PLT
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue8EndArrayEv@PLT
	movq	-120(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue13EndDictionaryEv@PLT
	movq	-120(%rbp), %rdi
	movb	$0, -64(%rbp)
	movq	%r12, %rsi
	movq	%rbx, -80(%rbp)
	movq	(%rdi), %rax
	movq	$0, -72(%rbp)
	call	*16(%rax)
	movq	_ZZN22TracedValue_Array_Test8TestBodyEvE5check(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	leaq	-96(%rbp), %rdi
	testl	%eax, %eax
	jne	.L166
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -96(%rbp)
	je	.L190
.L168:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L171
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L171:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L173
	call	_ZdlPv@PLT
.L173:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L165
	movq	(%r12), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L175
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L165:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	%r12, %r8
	leaq	_ZZN22TracedValue_Array_Test8TestBodyEvE5check(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEENS_15AssertionResultES3_S3_RKT_RKT0_
	cmpb	$0, -96(%rbp)
	jne	.L168
.L190:
	leaq	-104(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-88(%rbp), %rax
	leaq	.LC9(%rip), %r8
	testq	%rax, %rax
	je	.L169
	movq	(%rax), %r8
.L169:
	leaq	-112(%rbp), %r12
	movl	$62, %ecx
	movl	$1, %esi
	leaq	.LC30(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L168
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L165
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6128:
	.size	_ZN22TracedValue_Array_Test8TestBodyEv, .-_ZN22TracedValue_Array_Test8TestBodyEv
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB7413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L193
	testq	%rsi, %rsi
	je	.L209
.L193:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L210
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L196
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L197:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L197
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L195:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L197
.L209:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7413:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.1
.LC33:
	.string	"Object"
.LC34:
	.string	"TracedValue"
.LC35:
	.string	"Array"
.LC36:
	.string	"EscapingObject"
.LC37:
	.string	"EscapingArray"
	.section	.text.startup
	.p2align 4
	.type	_Z41__static_initialization_and_destruction_0ii.constprop.0, @function
_Z41__static_initialization_and_destruction_0ii.constprop.0:
.LFB7925:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI23TracedValue_Object_TestEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L231
.L213:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L232
.L214:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	-128(%rbp), %r14
	leaq	.LC30(%rip), %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r13
	leaq	-80(%rbp), %r12
	movq	%rax, %r15
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC34(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rsi
	movq	%r15, %r9
	movq	%r13, %r8
	leaq	.LC33(%rip), %rsi
	pushq	%rbx
	pushq	$0
	pushq	$0
	movl	$11, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN23TracedValue_Object_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI22TracedValue_Array_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L233
.L217:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L234
.L218:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC30(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	leaq	.LC34(%rip), %rdi
	xorl	%edx, %edx
	pushq	%rcx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	leaq	.LC35(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$38, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN22TracedValue_Array_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L235
.L221:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L236
.L222:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC30(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	leaq	.LC34(%rip), %rdi
	xorl	%ecx, %ecx
	pushq	%rdx
	movq	%r13, %r8
	xorl	%edx, %edx
	leaq	.LC36(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$74, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN31TracedValue_EscapingObject_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L237
.L225:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L238
.L226:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	.LC30(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-136(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	.LC34(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC37(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$86, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN30TracedValue_EscapingArray_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L234:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L218
.L233:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L217
.L232:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L214
.L236:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L222
.L235:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L221
.L238:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L226
.L237:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L225
.L231:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L213
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7925:
	.size	_Z41__static_initialization_and_destruction_0ii.constprop.0, .-_Z41__static_initialization_and_destruction_0ii.constprop.0
	.p2align 4
	.type	_GLOBAL__sub_I__ZN23TracedValue_Object_Test10test_info_E, @function
_GLOBAL__sub_I__ZN23TracedValue_Object_Test10test_info_E:
.LFB7885:
	.cfi_startproc
	endbr64
	jmp	_Z41__static_initialization_and_destruction_0ii.constprop.0
	.cfi_endproc
.LFE7885:
	.size	_GLOBAL__sub_I__ZN23TracedValue_Object_Test10test_info_E, .-_GLOBAL__sub_I__ZN23TracedValue_Object_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN23TracedValue_Object_Test10test_info_E
	.weak	_ZTVN7testing8internal15TestFactoryImplI23TracedValue_Object_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI23TracedValue_Object_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI23TracedValue_Object_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI23TracedValue_Object_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI23TracedValue_Object_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI23TracedValue_Object_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI23TracedValue_Object_TestE10CreateTestEv
	.weak	_ZTV23TracedValue_Object_Test
	.section	.data.rel.ro._ZTV23TracedValue_Object_Test,"awG",@progbits,_ZTV23TracedValue_Object_Test,comdat
	.align 8
	.type	_ZTV23TracedValue_Object_Test, @object
	.size	_ZTV23TracedValue_Object_Test, 64
_ZTV23TracedValue_Object_Test:
	.quad	0
	.quad	0
	.quad	_ZN23TracedValue_Object_TestD1Ev
	.quad	_ZN23TracedValue_Object_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN23TracedValue_Object_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI22TracedValue_Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI22TracedValue_Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI22TracedValue_Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI22TracedValue_Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI22TracedValue_Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI22TracedValue_Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22TracedValue_Array_TestE10CreateTestEv
	.weak	_ZTV22TracedValue_Array_Test
	.section	.data.rel.ro._ZTV22TracedValue_Array_Test,"awG",@progbits,_ZTV22TracedValue_Array_Test,comdat
	.align 8
	.type	_ZTV22TracedValue_Array_Test, @object
	.size	_ZTV22TracedValue_Array_Test, 64
_ZTV22TracedValue_Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN22TracedValue_Array_TestD1Ev
	.quad	_ZN22TracedValue_Array_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN22TracedValue_Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31TracedValue_EscapingObject_TestE10CreateTestEv
	.weak	_ZTV31TracedValue_EscapingObject_Test
	.section	.data.rel.ro._ZTV31TracedValue_EscapingObject_Test,"awG",@progbits,_ZTV31TracedValue_EscapingObject_Test,comdat
	.align 8
	.type	_ZTV31TracedValue_EscapingObject_Test, @object
	.size	_ZTV31TracedValue_EscapingObject_Test, 64
_ZTV31TracedValue_EscapingObject_Test:
	.quad	0
	.quad	0
	.quad	_ZN31TracedValue_EscapingObject_TestD1Ev
	.quad	_ZN31TracedValue_EscapingObject_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN31TracedValue_EscapingObject_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI30TracedValue_EscapingArray_TestE10CreateTestEv
	.weak	_ZTV30TracedValue_EscapingArray_Test
	.section	.data.rel.ro._ZTV30TracedValue_EscapingArray_Test,"awG",@progbits,_ZTV30TracedValue_EscapingArray_Test,comdat
	.align 8
	.type	_ZTV30TracedValue_EscapingArray_Test, @object
	.size	_ZTV30TracedValue_EscapingArray_Test, 64
_ZTV30TracedValue_EscapingArray_Test:
	.quad	0
	.quad	0
	.quad	_ZN30TracedValue_EscapingArray_TestD1Ev
	.quad	_ZN30TracedValue_EscapingArray_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN30TracedValue_EscapingArray_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"[\"1\\u20AC23\\\"\\u0001\\b\\f\\n\\r\\t\\\\\"]"
	.section	.data.rel.local,"aw"
	.align 8
	.type	_ZZN30TracedValue_EscapingArray_Test8TestBodyEvE5check, @object
	.size	_ZZN30TracedValue_EscapingArray_Test8TestBodyEvE5check, 8
_ZZN30TracedValue_EscapingArray_Test8TestBodyEvE5check:
	.quad	.LC38
	.globl	_ZN30TracedValue_EscapingArray_Test10test_info_E
	.bss
	.align 8
	.type	_ZN30TracedValue_EscapingArray_Test10test_info_E, @object
	.size	_ZN30TracedValue_EscapingArray_Test10test_info_E, 8
_ZN30TracedValue_EscapingArray_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"{\"a\":\"1\\u20AC23\\\"\\u0001\\b\\f\\n\\r\\t\\\\\"}"
	.section	.data.rel.local
	.align 8
	.type	_ZZN31TracedValue_EscapingObject_Test8TestBodyEvE5check, @object
	.size	_ZZN31TracedValue_EscapingObject_Test8TestBodyEvE5check, 8
_ZZN31TracedValue_EscapingObject_Test8TestBodyEvE5check:
	.quad	.LC39
	.globl	_ZN31TracedValue_EscapingObject_Test10test_info_E
	.bss
	.align 8
	.type	_ZN31TracedValue_EscapingObject_Test10test_info_E, @object
	.size	_ZN31TracedValue_EscapingObject_Test10test_info_E, 8
_ZN31TracedValue_EscapingObject_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"[\"a\",1,1.234,\"NaN\",\"Infinity\",\"-Infinity\",1.23e+07,false,true,null,{\"foo\":[]}]"
	.section	.data.rel.local
	.align 8
	.type	_ZZN22TracedValue_Array_Test8TestBodyEvE5check, @object
	.size	_ZZN22TracedValue_Array_Test8TestBodyEvE5check, 8
_ZZN22TracedValue_Array_Test8TestBodyEvE5check:
	.quad	.LC40
	.globl	_ZN22TracedValue_Array_Test10test_info_E
	.bss
	.align 8
	.type	_ZN22TracedValue_Array_Test10test_info_E, @object
	.size	_ZN22TracedValue_Array_Test10test_info_E, 8
_ZN22TracedValue_Array_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"{\"a\":\"b\",\"b\":1,\"c\":1.234,\"d\":\"NaN\",\"e\":\"Infinity\",\"f\":\"-Infinity\",\"g\":1.23e+07,\"h\":false,\"i\":true,\"j\":null,\"k\":{\"l\":\"m\"}}"
	.section	.data.rel.local
	.align 8
	.type	_ZZN23TracedValue_Object_Test8TestBodyEvE5check, @object
	.size	_ZZN23TracedValue_Object_Test8TestBodyEvE5check, 8
_ZZN23TracedValue_Object_Test8TestBodyEvE5check:
	.quad	.LC41
	.globl	_ZN23TracedValue_Object_Test10test_info_E
	.bss
	.align 8
	.type	_ZN23TracedValue_Object_Test10test_info_E, @object
	.size	_ZN23TracedValue_Object_Test10test_info_E, 8
_ZN23TracedValue_Object_Test10test_info_E:
	.zero	8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.data.rel.ro,"aw"
	.align 8
.LC6:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC7:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC8:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC12:
	.long	3367254360
	.long	1072938614
	.align 8
.LC14:
	.long	0
	.long	2146959360
	.align 8
.LC16:
	.long	0
	.long	2146435072
	.align 8
.LC18:
	.long	0
	.long	-1048576
	.align 8
.LC20:
	.long	0
	.long	1097299420
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
