	.file	"test_environment.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB7442:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7442:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.text
	.p2align 4
	.type	_ZL17at_exit_callback2Pv, @function
_ZL17at_exit_callback2Pv:
.LFB8583:
	.cfi_startproc
	endbr64
	movb	$1, _ZL11called_cb_2(%rip)
	ret
	.cfi_endproc
.LFE8583:
	.size	_ZL17at_exit_callback2Pv, .-_ZL17at_exit_callback2Pv
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED2Ev:
.LFB11231:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE11231:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED2Ev:
.LFB11281:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L6:
	ret
	.cfi_endproc
.LFE11281:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED2Ev:
.LFB11796:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11796:
	.size	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED1Ev,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED2Ev:
.LFB11804:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11804:
	.size	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED1Ev,_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED2Ev:
.LFB11812:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11812:
	.size	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED1Ev,_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED2Ev:
.LFB11820:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11820:
	.size	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED1Ev,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED2Ev:
.LFB11828:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11828:
	.size	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED1Ev,_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED2Ev:
.LFB11836:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11836:
	.size	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED1Ev,_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED2Ev:
.LFB11844:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11844:
	.size	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED1Ev,_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED2Ev:
.LFB11852:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11852:
	.size	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED1Ev,_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED2Ev:
.LFB11860:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11860:
	.size	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED1Ev,_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED2Ev:
.LFB11868:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11868:
	.size	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED1Ev,_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED2Ev
	.text
	.p2align 4
	.type	_ZL17at_exit_callback1Pv, @function
_ZL17at_exit_callback1Pv:
.LFB8582:
	.cfi_startproc
	endbr64
	movb	$1, _ZL11called_cb_1(%rip)
	movq	%rdi, %rsi
	testq	%rdi, %rdi
	je	.L18
	leaq	_ZL8cb_1_arg(%rip), %rdi
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	ret
	.cfi_endproc
.LFE8582:
	.size	_ZL17at_exit_callback1Pv, .-_ZL17at_exit_callback1Pv
	.p2align 4
	.type	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENUlPcPvE_4_FUNES0_S1_, @function
_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENUlPcPvE_4_FUNES0_S1_:
.LFB8595:
	.cfi_startproc
	endbr64
	leaq	_ZL5hello(%rip), %rax
	cmpq	%rdi, %rax
	jne	.L25
	addl	$1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENKUlPcPvE_clES0_S1_E4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8595:
	.size	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENUlPcPvE_4_FUNES0_S1_, .-_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENUlPcPvE_4_FUNES0_S1_
	.section	.text._ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED0Ev:
.LFB11870:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11870:
	.size	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED0Ev:
.LFB11862:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11862:
	.size	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED0Ev:
.LFB11854:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11854:
	.size	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED0Ev:
.LFB11846:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11846:
	.size	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED0Ev:
.LFB11838:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11838:
	.size	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED0Ev:
.LFB11830:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11830:
	.size	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED0Ev:
.LFB11822:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11822:
	.size	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED0Ev:
.LFB11814:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11814:
	.size	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED0Ev:
.LFB11806:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11806:
	.size	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED0Ev:
.LFB11798:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11798:
	.size	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED0Ev:
.LFB11233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*8(%rax)
.L37:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11233:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED0Ev:
.LFB11283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L43
	movq	(%rdi), %rax
	call	*8(%rax)
.L43:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11283:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED0Ev
	.section	.text._ZN15NodeTestFixture8TearDownEv,"axG",@progbits,_ZN15NodeTestFixture8TearDownEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture8TearDownEv
	.type	_ZN15NodeTestFixture8TearDownEv, @function
_ZN15NodeTestFixture8TearDownEv:
.LFB8492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8492:
	.size	_ZN15NodeTestFixture8TearDownEv, .-_ZN15NodeTestFixture8TearDownEv
	.section	.text._ZN15NodeTestFixture5SetUpEv,"axG",@progbits,_ZN15NodeTestFixture5SetUpEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture5SetUpEv
	.type	_ZN15NodeTestFixture5SetUpEv, @function
_ZN15NodeTestFixture5SetUpEv:
.LFB8489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node26CreateArrayBufferAllocatorEv@PLT
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %r8
	movq	%rax, %rdi
	movq	%rax, 8+_ZN15NodeTestFixture9allocatorE(%rip)
	testq	%r8, %r8
	je	.L51
	movq	%r8, %rdi
	call	*_ZN15NodeTestFixture9allocatorE(%rip)
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %rdi
.L51:
	movq	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE@GOTPCREL(%rip), %rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	movq	%rax, _ZN15NodeTestFixture9allocatorE(%rip)
	call	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L58
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate5EnterEv@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture5SetUpEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8489:
	.size	_ZN15NodeTestFixture5SetUpEv, .-_ZN15NodeTestFixture5SetUpEv
	.section	.text._ZN40EnvironmentTest_SetImmediateCleanup_TestD2Ev,"axG",@progbits,_ZN40EnvironmentTest_SetImmediateCleanup_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN40EnvironmentTest_SetImmediateCleanup_TestD2Ev
	.type	_ZN40EnvironmentTest_SetImmediateCleanup_TestD2Ev, @function
_ZN40EnvironmentTest_SetImmediateCleanup_TestD2Ev:
.LFB11792:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11792:
	.size	_ZN40EnvironmentTest_SetImmediateCleanup_TestD2Ev, .-_ZN40EnvironmentTest_SetImmediateCleanup_TestD2Ev
	.weak	_ZN40EnvironmentTest_SetImmediateCleanup_TestD1Ev
	.set	_ZN40EnvironmentTest_SetImmediateCleanup_TestD1Ev,_ZN40EnvironmentTest_SetImmediateCleanup_TestD2Ev
	.section	.text._ZN40EnvironmentTest_SetImmediateCleanup_TestD0Ev,"axG",@progbits,_ZN40EnvironmentTest_SetImmediateCleanup_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN40EnvironmentTest_SetImmediateCleanup_TestD0Ev
	.type	_ZN40EnvironmentTest_SetImmediateCleanup_TestD0Ev, @function
_ZN40EnvironmentTest_SetImmediateCleanup_TestD0Ev:
.LFB11794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11794:
	.size	_ZN40EnvironmentTest_SetImmediateCleanup_TestD0Ev, .-_ZN40EnvironmentTest_SetImmediateCleanup_TestD0Ev
	.section	.text._ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD2Ev,"axG",@progbits,_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD2Ev
	.type	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD2Ev, @function
_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD2Ev:
.LFB11800:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11800:
	.size	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD2Ev, .-_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD2Ev
	.weak	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD1Ev
	.set	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD1Ev,_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD2Ev
	.section	.text._ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD0Ev,"axG",@progbits,_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD0Ev
	.type	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD0Ev, @function
_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD0Ev:
.LFB11802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11802:
	.size	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD0Ev, .-_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD0Ev
	.section	.text._ZN37EnvironmentTest_NonNodeJSContext_TestD2Ev,"axG",@progbits,_ZN37EnvironmentTest_NonNodeJSContext_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN37EnvironmentTest_NonNodeJSContext_TestD2Ev
	.type	_ZN37EnvironmentTest_NonNodeJSContext_TestD2Ev, @function
_ZN37EnvironmentTest_NonNodeJSContext_TestD2Ev:
.LFB11808:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11808:
	.size	_ZN37EnvironmentTest_NonNodeJSContext_TestD2Ev, .-_ZN37EnvironmentTest_NonNodeJSContext_TestD2Ev
	.weak	_ZN37EnvironmentTest_NonNodeJSContext_TestD1Ev
	.set	_ZN37EnvironmentTest_NonNodeJSContext_TestD1Ev,_ZN37EnvironmentTest_NonNodeJSContext_TestD2Ev
	.section	.text._ZN37EnvironmentTest_NonNodeJSContext_TestD0Ev,"axG",@progbits,_ZN37EnvironmentTest_NonNodeJSContext_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN37EnvironmentTest_NonNodeJSContext_TestD0Ev
	.type	_ZN37EnvironmentTest_NonNodeJSContext_TestD0Ev, @function
_ZN37EnvironmentTest_NonNodeJSContext_TestD0Ev:
.LFB11810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11810:
	.size	_ZN37EnvironmentTest_NonNodeJSContext_TestD0Ev, .-_ZN37EnvironmentTest_NonNodeJSContext_TestD0Ev
	.section	.text._ZN40EnvironmentTest_NoEnvironmentSanity_TestD2Ev,"axG",@progbits,_ZN40EnvironmentTest_NoEnvironmentSanity_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD2Ev
	.type	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD2Ev, @function
_ZN40EnvironmentTest_NoEnvironmentSanity_TestD2Ev:
.LFB11816:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11816:
	.size	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD2Ev, .-_ZN40EnvironmentTest_NoEnvironmentSanity_TestD2Ev
	.weak	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD1Ev
	.set	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD1Ev,_ZN40EnvironmentTest_NoEnvironmentSanity_TestD2Ev
	.section	.text._ZN40EnvironmentTest_NoEnvironmentSanity_TestD0Ev,"axG",@progbits,_ZN40EnvironmentTest_NoEnvironmentSanity_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD0Ev
	.type	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD0Ev, @function
_ZN40EnvironmentTest_NoEnvironmentSanity_TestD0Ev:
.LFB11818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11818:
	.size	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD0Ev, .-_ZN40EnvironmentTest_NoEnvironmentSanity_TestD0Ev
	.section	.text._ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD2Ev,"axG",@progbits,_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD2Ev
	.type	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD2Ev, @function
_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD2Ev:
.LFB11824:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11824:
	.size	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD2Ev, .-_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD2Ev
	.weak	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD1Ev
	.set	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD1Ev,_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD2Ev
	.section	.text._ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD0Ev,"axG",@progbits,_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD0Ev
	.type	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD0Ev, @function
_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD0Ev:
.LFB11826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11826:
	.size	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD0Ev, .-_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD0Ev
	.section	.text._ZN33EnvironmentTest_AtExitRunsJS_TestD2Ev,"axG",@progbits,_ZN33EnvironmentTest_AtExitRunsJS_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33EnvironmentTest_AtExitRunsJS_TestD2Ev
	.type	_ZN33EnvironmentTest_AtExitRunsJS_TestD2Ev, @function
_ZN33EnvironmentTest_AtExitRunsJS_TestD2Ev:
.LFB11832:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11832:
	.size	_ZN33EnvironmentTest_AtExitRunsJS_TestD2Ev, .-_ZN33EnvironmentTest_AtExitRunsJS_TestD2Ev
	.weak	_ZN33EnvironmentTest_AtExitRunsJS_TestD1Ev
	.set	_ZN33EnvironmentTest_AtExitRunsJS_TestD1Ev,_ZN33EnvironmentTest_AtExitRunsJS_TestD2Ev
	.section	.text._ZN33EnvironmentTest_AtExitRunsJS_TestD0Ev,"axG",@progbits,_ZN33EnvironmentTest_AtExitRunsJS_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33EnvironmentTest_AtExitRunsJS_TestD0Ev
	.type	_ZN33EnvironmentTest_AtExitRunsJS_TestD0Ev, @function
_ZN33EnvironmentTest_AtExitRunsJS_TestD0Ev:
.LFB11834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11834:
	.size	_ZN33EnvironmentTest_AtExitRunsJS_TestD0Ev, .-_ZN33EnvironmentTest_AtExitRunsJS_TestD0Ev
	.section	.text._ZN39EnvironmentTest_AtExitWithArgument_TestD2Ev,"axG",@progbits,_ZN39EnvironmentTest_AtExitWithArgument_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39EnvironmentTest_AtExitWithArgument_TestD2Ev
	.type	_ZN39EnvironmentTest_AtExitWithArgument_TestD2Ev, @function
_ZN39EnvironmentTest_AtExitWithArgument_TestD2Ev:
.LFB11840:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11840:
	.size	_ZN39EnvironmentTest_AtExitWithArgument_TestD2Ev, .-_ZN39EnvironmentTest_AtExitWithArgument_TestD2Ev
	.weak	_ZN39EnvironmentTest_AtExitWithArgument_TestD1Ev
	.set	_ZN39EnvironmentTest_AtExitWithArgument_TestD1Ev,_ZN39EnvironmentTest_AtExitWithArgument_TestD2Ev
	.section	.text._ZN39EnvironmentTest_AtExitWithArgument_TestD0Ev,"axG",@progbits,_ZN39EnvironmentTest_AtExitWithArgument_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39EnvironmentTest_AtExitWithArgument_TestD0Ev
	.type	_ZN39EnvironmentTest_AtExitWithArgument_TestD0Ev, @function
_ZN39EnvironmentTest_AtExitWithArgument_TestD0Ev:
.LFB11842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11842:
	.size	_ZN39EnvironmentTest_AtExitWithArgument_TestD0Ev, .-_ZN39EnvironmentTest_AtExitWithArgument_TestD0Ev
	.section	.text._ZN32EnvironmentTest_AtExitOrder_TestD2Ev,"axG",@progbits,_ZN32EnvironmentTest_AtExitOrder_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32EnvironmentTest_AtExitOrder_TestD2Ev
	.type	_ZN32EnvironmentTest_AtExitOrder_TestD2Ev, @function
_ZN32EnvironmentTest_AtExitOrder_TestD2Ev:
.LFB11848:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11848:
	.size	_ZN32EnvironmentTest_AtExitOrder_TestD2Ev, .-_ZN32EnvironmentTest_AtExitOrder_TestD2Ev
	.weak	_ZN32EnvironmentTest_AtExitOrder_TestD1Ev
	.set	_ZN32EnvironmentTest_AtExitOrder_TestD1Ev,_ZN32EnvironmentTest_AtExitOrder_TestD2Ev
	.section	.text._ZN32EnvironmentTest_AtExitOrder_TestD0Ev,"axG",@progbits,_ZN32EnvironmentTest_AtExitOrder_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32EnvironmentTest_AtExitOrder_TestD0Ev
	.type	_ZN32EnvironmentTest_AtExitOrder_TestD0Ev, @function
_ZN32EnvironmentTest_AtExitOrder_TestD0Ev:
.LFB11850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11850:
	.size	_ZN32EnvironmentTest_AtExitOrder_TestD0Ev, .-_ZN32EnvironmentTest_AtExitOrder_TestD0Ev
	.section	.text._ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD2Ev,"axG",@progbits,_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD2Ev
	.type	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD2Ev, @function
_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD2Ev:
.LFB11856:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11856:
	.size	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD2Ev, .-_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD2Ev
	.weak	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD1Ev
	.set	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD1Ev,_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD2Ev
	.section	.text._ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD0Ev,"axG",@progbits,_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD0Ev
	.type	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD0Ev, @function
_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD0Ev:
.LFB11858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11858:
	.size	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD0Ev, .-_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD0Ev
	.section	.text._ZN42EnvironmentTest_AtExitWithEnvironment_TestD2Ev,"axG",@progbits,_ZN42EnvironmentTest_AtExitWithEnvironment_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD2Ev
	.type	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD2Ev, @function
_ZN42EnvironmentTest_AtExitWithEnvironment_TestD2Ev:
.LFB11864:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11864:
	.size	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD2Ev, .-_ZN42EnvironmentTest_AtExitWithEnvironment_TestD2Ev
	.weak	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD1Ev
	.set	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD1Ev,_ZN42EnvironmentTest_AtExitWithEnvironment_TestD2Ev
	.section	.text._ZN42EnvironmentTest_AtExitWithEnvironment_TestD0Ev,"axG",@progbits,_ZN42EnvironmentTest_AtExitWithEnvironment_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD0Ev
	.type	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD0Ev, @function
_ZN42EnvironmentTest_AtExitWithEnvironment_TestD0Ev:
.LFB11866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11866:
	.size	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD0Ev, .-_ZN42EnvironmentTest_AtExitWithEnvironment_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv:
.LFB11956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV42EnvironmentTest_AtExitWithEnvironment_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11956:
	.size	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv:
.LFB11955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV45EnvironmentTest_AtExitWithoutEnvironment_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11955:
	.size	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv:
.LFB11954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV32EnvironmentTest_AtExitOrder_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11954:
	.size	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv:
.LFB11953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV39EnvironmentTest_AtExitWithArgument_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11953:
	.size	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv:
.LFB11952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV33EnvironmentTest_AtExitRunsJS_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11952:
	.size	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv:
.LFB11951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11951:
	.size	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv:
.LFB11950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV40EnvironmentTest_NoEnvironmentSanity_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11950:
	.size	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv:
.LFB11949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV37EnvironmentTest_NonNodeJSContext_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11949:
	.size	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv:
.LFB11948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11948:
	.size	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv:
.LFB11947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV40EnvironmentTest_SetImmediateCleanup_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11947:
	.size	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv
	.section	.text._ZN15NodeTestFixture16TearDownTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture16TearDownTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture16TearDownTestCaseEv
	.type	_ZN15NodeTestFixture16TearDownTestCaseEv, @function
_ZN15NodeTestFixture16TearDownTestCaseEv:
.LFB8488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L111:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L114
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_close@PLT
	testl	%eax, %eax
	jne	.L115
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8488:
	.size	_ZN15NodeTestFixture16TearDownTestCaseEv, .-_ZN15NodeTestFixture16TearDownTestCaseEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0:
.LFB12076:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L119
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12076:
	.size	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0:
.LFB12077:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L123:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12077:
	.size	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	.section	.rodata.str1.1
.LC4:
	.string	"void at_exit_js(void*)"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"../test/cctest/test_environment.cc"
	.section	.rodata.str1.1
.LC6:
	.string	"!obj.IsEmpty()"
.LC7:
	.string	"obj->IsObject()"
	.text
	.p2align 4
	.type	_ZL10at_exit_jsPv, @function
_ZL10at_exit_jsPv:
.LFB8586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	testq	%rax, %rax
	je	.L129
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L130
	movq	%r13, %rdi
	movb	$1, _ZL17called_at_exit_js(%rip)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L129:
	.cfi_restore_state
	leaq	.LC4(%rip), %rcx
	movl	$186, %edx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	__assert_fail@PLT
.L131:
	call	__stack_chk_fail@PLT
.L130:
	leaq	.LC4(%rip), %rcx
	movl	$187, %edx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE8586:
	.size	_ZL10at_exit_jsPv, .-_ZL10at_exit_jsPv
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB12110:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L143
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L144
	cmpq	$1, %rax
	jne	.L136
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L137:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L136:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L137
	jmp	.L135
.L144:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L135:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L137
.L143:
	leaq	.LC8(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12110:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZN15EnvironmentTest8TearDownEv,"axG",@progbits,_ZN15EnvironmentTest8TearDownEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15EnvironmentTest8TearDownEv
	.type	_ZN15EnvironmentTest8TearDownEv, @function
_ZN15EnvironmentTest8TearDownEv:
.LFB8501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	$0, 16(%rbx)
	movb	$0, _ZL11called_cb_1(%rip)
	movb	$0, _ZL11called_cb_2(%rip)
	movb	$0, _ZL19called_cb_ordered_1(%rip)
	movb	$0, _ZL19called_cb_ordered_2(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8501:
	.size	_ZN15EnvironmentTest8TearDownEv, .-_ZN15EnvironmentTest8TearDownEv
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB10057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L150
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L151
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L177
	.p2align 4,,10
	.p2align 3
.L150:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L156
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L162
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L178
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L156
.L157:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L164
.L178:
	lock subl	$1, 8(%r13)
	jne	.L164
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L164
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L157
	.p2align 4,,10
	.p2align 3
.L156:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L160
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L156
.L162:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L160
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L160
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L151:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L150
.L177:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L154
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L155:
	cmpl	$1, %eax
	jne	.L150
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L154:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L155
	.cfi_endproc
.LFE10057:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata._ZN15NodeTestFixture13SetUpTestCaseEv.str1.1,"aMS",@progbits,1
.LC9:
	.string	"NODE_OPTIONS"
.LC10:
	.string	"cctest"
	.section	.text._ZN15NodeTestFixture13SetUpTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture13SetUpTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture13SetUpTestCaseEv
	.type	_ZN15NodeTestFixture13SetUpTestCaseEv, @function
_ZN15NodeTestFixture13SetUpTestCaseEv:
.LFB8483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN15NodeTestFixture16node_initializedE(%rip)
	je	.L221
.L180:
	movl	$1312, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing5AgentC1Ev@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r13
	movq	%r12, _ZN15NodeTestFixture13tracing_agentE(%rip)
	testq	%r13, %r13
	je	.L181
	movq	%r13, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r12
.L181:
	movq	%r12, %rdi
	call	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %rax
	movq	976(%rax), %r12
	testq	%r12, %r12
	je	.L222
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L223
	movl	$128, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r12
	movq	%r13, _ZN15NodeTestFixture8platformE(%rip)
	testq	%r12, %r12
	je	.L184
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L185
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L187
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L188
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L189:
	cmpl	$1, %eax
	jne	.L187
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L191
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L192:
	cmpl	$1, %eax
	jne	.L187
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L187:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L206
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L196
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L206
.L196:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L204
	lock subl	$1, 8(%r13)
	jne	.L204
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L204
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L196
	.p2align 4,,10
	.p2align 3
.L206:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
.L184:
	movq	%r13, %rdi
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L199
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L206
.L202:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L199
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L199
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	.LC9(%rip), %rdi
	call	uv_os_unsetenv@PLT
	leaq	.LC10(%rip), %rax
	leaq	-48(%rbp), %rcx
	movl	$1, -64(%rbp)
	leaq	-60(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movb	$1, _ZN15NodeTestFixture16node_initializedE(%rip)
	call	_ZN4node4InitEPiPPKcS0_PS3_@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r12, %rdi
	call	*%rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L188:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L191:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L192
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8483:
	.size	_ZN15NodeTestFixture13SetUpTestCaseEv, .-_ZN15NodeTestFixture13SetUpTestCaseEv
	.section	.rodata.str1.1
.LC11:
	.string	"(nullptr)"
.LC12:
	.string	"NULL"
.LC13:
	.string	"nullptr"
	.text
	.p2align 4
	.type	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0, @function
_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0:
.LFB12169:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-432(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -552(%rbp)
	movq	.LC14(%rip), %xmm1
	movq	%r12, %rdi
	movq	%rsi, -584(%rbp)
	movq	%rdx, -592(%rbp)
	movhps	.LC15(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -544(%rbp)
	movq	%r15, -600(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-544(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r14, %rdi
	movl	$9, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-496(%rbp), %rax
	leaq	-512(%rbp), %rdi
	movq	$0, -504(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -560(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L226
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L240
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L228:
	movq	.LC14(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC16(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -576(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	-600(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-544(%rbp), %xmm3
	movq	-520(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-592(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L241
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L231:
	movq	-384(%rbp), %rax
	leaq	-464(%rbp), %r14
	movq	$0, -472(%rbp)
	leaq	-480(%rbp), %r15
	movq	%r14, -480(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L232
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L233
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L234:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-576(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-552(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	-560(%rbp), %r8
	movq	-584(%rbp), %rsi
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-480(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	-512(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	movq	-552(%rbp), %rax
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L233:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$4, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L228
.L242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12169:
	.size	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0, .-_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
	.section	.rodata.str1.1
.LC17:
	.string	"true"
.LC18:
	.string	"false"
.LC19:
	.string	"called_cb_ordered_2"
	.text
	.p2align 4
	.type	_ZL25at_exit_callback_ordered1Pv, @function
_ZL25at_exit_callback_ordered1Pv:
.LFB8584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZL19called_cb_ordered_2(%rip), %eax
	movq	$0, -72(%rbp)
	movb	%al, -80(%rbp)
	testb	%al, %al
	je	.L258
.L245:
	movb	$1, _ZL19called_cb_ordered_1(%rip)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	leaq	-88(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC19(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-64(%rbp), %r8
	movl	$173, %ecx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L247
	movq	(%rdi), %rax
	call	*8(%rax)
.L247:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L245
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L245
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8584:
	.size	_ZL25at_exit_callback_ordered1Pv, .-_ZL25at_exit_callback_ordered1Pv
	.section	.rodata.str1.1
.LC20:
	.string	"called_cb_ordered_1"
	.text
	.p2align 4
	.type	_ZL25at_exit_callback_ordered2Pv, @function
_ZL25at_exit_callback_ordered2Pv:
.LFB8585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZL19called_cb_ordered_1(%rip), %eax
	movq	$0, -72(%rbp)
	xorl	$1, %eax
	movb	%al, -80(%rbp)
	testb	%al, %al
	je	.L275
.L262:
	movb	$1, _ZL19called_cb_ordered_2(%rip)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	leaq	-88(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	leaq	.LC18(%rip), %r8
	leaq	.LC17(%rip), %rcx
	leaq	.LC20(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-64(%rbp), %r8
	movl	$178, %ecx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L263
	call	_ZdlPv@PLT
.L263:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L264
	movq	(%rdi), %rax
	call	*8(%rax)
.L264:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L262
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L262
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8585:
	.size	_ZL25at_exit_callback_ordered2Pv, .-_ZL25at_exit_callback_ordered2Pv
	.section	.rodata.str1.1
.LC21:
	.string	"node"
.LC22:
	.string	"-p"
.LC23:
	.string	"process.version"
.LC24:
	.string	"%s"
	.text
	.align 2
	.p2align 4
	.globl	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEv
	.type	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEv, @function
_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEv:
.LFB8593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	leaq	.LC24(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	16(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movl	$8, %r8d
	leaq	-80(%rbp), %r11
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L278:
	movq	(%r11,%r8), %rcx
	movq	%r8, -200(%rbp)
	movq	%r11, -192(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -184(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r10
	movq	-184(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r9d
	movq	%r10, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r9d, %rsi
	movl	%r9d, -172(%rbp)
	movq	%r10, -168(%rbp)
	call	snprintf@PLT
	movq	-200(%rbp), %r8
	movq	-168(%rbp), %r10
	movl	-172(%rbp), %r9d
	addq	(%r12), %r10
	movq	%r10, (%r12,%r8)
	addq	$8, %r8
	movq	-192(%rbp), %r11
	addl	%r9d, %r13d
	cmpq	$24, %r8
	jne	.L278
	movq	-144(%rbp), %r14
	xorl	%esi, %esi
	movl	$0, -148(%rbp)
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L290
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L291
	movq	-112(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L292
	movq	16(%rbx), %rdi
	leaq	-148(%rbp), %r8
	leaq	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENUlPcPvE_4_FUNES0_S1_(%rip), %rcx
	movl	$6, %edx
	leaq	_ZL5hello(%rip), %rsi
	call	_ZN4node6Buffer3NewEPN2v87IsolateEPcmPFvS4_PvES5_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L293
.L282:
	movq	%rbx, %rdi
	call	_ZNK2v85Value12IsUint8ArrayEv@PLT
	testb	%al, %al
	je	.L294
	movq	%rbx, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	$6, %rax
	jne	.L295
	movq	-168(%rbp), %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	cmpl	$1, -148(%rbp)
	jne	.L296
	movq	%rbx, %rdi
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	testq	%rax, %rax
	jne	.L297
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	leaq	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L292:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	leaq	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8593:
	.size	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEv, .-_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEv
	.section	.rodata.str1.1
.LC25:
	.string	"called_cb_1"
	.text
	.align 2
	.p2align 4
	.globl	_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test8TestBodyEv
	.type	_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test8TestBodyEv, @function
_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test8TestBodyEv:
.LFB8539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC24(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-160(%rbp), %rbx
	subq	$184, %rsp
	movq	16(%rdi), %rsi
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L300:
	movq	-200(%rbp), %rax
	movq	(%rax,%r15), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -224(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r9
	movq	-224(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -212(%rbp)
	movq	%r9, -208(%rbp)
	call	snprintf@PLT
	movq	-208(%rbp), %r9
	addq	(%r12), %r9
	movl	-212(%rbp), %r8d
	movq	%r9, (%r12,%r15)
	addq	$8, %r15
	addl	%r8d, %r13d
	cmpq	$24, %r15
	jne	.L300
	movq	-160(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L319
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L320
	movq	-128(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L321
	xorl	%esi, %esi
	leaq	_ZL17at_exit_callback1Pv(%rip), %rdi
	call	_ZN4node6AtExitEPFvPvES0_@PLT
	movq	%r15, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movzbl	_ZL11called_cb_1(%rip), %eax
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L322
.L305:
	movq	%r15, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	leaq	-184(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -208(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC25(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-192(%rbp), %rdi
	movl	$71, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -200(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L307
	movq	(%rdi), %rax
	call	*8(%rax)
.L307:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L305
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L309
	movq	%r8, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r8
.L309:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L321:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8539:
	.size	_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test8TestBodyEv, .-_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN42EnvironmentTest_AtExitWithEnvironment_Test8TestBodyEv
	.type	_ZN42EnvironmentTest_AtExitWithEnvironment_Test8TestBodyEv, @function
_ZN42EnvironmentTest_AtExitWithEnvironment_Test8TestBodyEv:
.LFB8532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	leaq	.LC24(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$8, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L325:
	movq	(%r15,%r13), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -216(%rbp)
	call	strlen@PLT
	movslq	%ebx, %r10
	movq	-216(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r9d
	movq	%r10, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r9d, %rsi
	movl	%r9d, -204(%rbp)
	movq	%r10, -200(%rbp)
	call	snprintf@PLT
	movq	-200(%rbp), %r10
	addq	(%r12), %r10
	movl	-204(%rbp), %r9d
	movq	%r10, (%r12,%r13)
	addq	$8, %r13
	addl	%r9d, %ebx
	cmpq	$24, %r13
	jne	.L325
	movq	-160(%rbp), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L344
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L345
	movq	-128(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L346
	movq	%rax, %rdi
	xorl	%edx, %edx
	leaq	_ZL17at_exit_callback1Pv(%rip), %rsi
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movq	%r13, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movzbl	_ZL11called_cb_1(%rip), %eax
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L347
.L330:
	movq	%r13, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%rbx, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-224(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	leaq	-184(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -200(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	%r15, %rdi
	leaq	-176(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC25(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	leaq	-192(%rbp), %r15
	movq	-96(%rbp), %r8
	movl	$61, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-200(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L332
	movq	(%rdi), %rax
	call	*8(%rax)
.L332:
	movq	-168(%rbp), %r15
	testq	%r15, %r15
	je	.L330
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8532:
	.size	_ZN42EnvironmentTest_AtExitWithEnvironment_Test8TestBodyEv, .-_ZN42EnvironmentTest_AtExitWithEnvironment_Test8TestBodyEv
	.section	.rodata.str1.1
.LC26:
	.string	""
.LC27:
	.string	"cb_1_arg"
.LC28:
	.string	"arg"
	.text
	.align 2
	.p2align 4
	.globl	_ZN39EnvironmentTest_AtExitWithArgument_Test8TestBodyEv
	.type	_ZN39EnvironmentTest_AtExitWithArgument_Test8TestBodyEv, @function
_ZN39EnvironmentTest_AtExitWithArgument_Test8TestBodyEv:
.LFB8553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movl	$8, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC24(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm3
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, -432(%rbp)
	movaps	%xmm0, -448(%rbp)
	movq	-440(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%r15, %r9
	movl	%ebx, %r15d
	movq	%r12, %rbx
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L350:
	movq	(%r9,%r14), %rcx
	movq	%r9, -664(%rbp)
	movslq	%r15d, %r12
	movq	%rcx, %rdi
	movq	%rcx, -656(%rbp)
	call	strlen@PLT
	movq	(%rbx), %rdi
	movq	-656(%rbp), %rcx
	movq	%r13, %rdx
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	movslq	%r8d, %rsi
	addq	%r12, %rdi
	movl	%r8d, -648(%rbp)
	call	snprintf@PLT
	movq	(%rbx), %rax
	movl	-648(%rbp), %r8d
	movq	-664(%rbp), %r9
	addq	%r12, %rax
	addl	%r8d, %r15d
	movq	%rax, (%rbx,%r14)
	addq	$8, %r14
	cmpq	$24, %r14
	jne	.L350
	movq	-608(%rbp), %r14
	xorl	%esi, %esi
	movq	%r9, %r15
	movq	%rbx, %r12
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -576(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L387
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -648(%rbp)
	testq	%rax, %rax
	je	.L388
	movq	-576(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%rbx, %rcx
	movl	$3, %r8d
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L389
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdx
	movq	%r14, %rdi
	movb	$115, -520(%rbp)
	movq	%rax, -664(%rbp)
	leaq	_ZL17at_exit_callback1Pv(%rip), %rsi
	movq	%rax, -544(%rbp)
	movabsq	$7454127125020110707, %rax
	movq	%rdx, -696(%rbp)
	movq	%rax, -528(%rbp)
	movq	$9, -536(%rbp)
	movb	$0, -519(%rbp)
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movq	%r14, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movq	-536(%rbp), %rdx
	cmpq	8+_ZL8cb_1_arg(%rip), %rdx
	je	.L390
.L354:
	movq	.LC14(%rip), %xmm1
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -656(%rbp)
	movhps	.LC15(%rip), %xmm1
	movaps	%xmm1, -720(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movw	%dx, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rcx
	addq	%r15, %rcx
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %rax
	xorl	%esi, %esi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -704(%rbp)
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rcx
	addq	%rax, %rcx
	movq	%rdx, (%rcx)
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-720(%rbp), %xmm1
	movq	-24(%rdx), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -448(%rbp)
	addq	$80, %rdx
	movq	%rdx, -320(%rbp)
	leaq	-368(%rbp), %rdx
	movq	%rdx, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-656(%rbp), %rdi
	movl	$24, -360(%rbp)
	movq	%rdx, -424(%rbp)
	leaq	-336(%rbp), %rdx
	movq	%rdx, -688(%rbp)
	movq	%rdx, -352(%rbp)
	leaq	-424(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rdx, -760(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-704(%rbp), %rsi
	leaq	_ZL8cb_1_arg(%rip), %rdi
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -736(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -728(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L357
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L391
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L359:
	movq	.LC14(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC16(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -752(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-688(%rbp), %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	-680(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-656(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-656(%rbp), %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %r15
	movq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-704(%rbp), %r15
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	addq	%r15, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-720(%rbp), %xmm4
	movq	-680(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm4, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-760(%rbp), %rsi
	movq	-656(%rbp), %rdi
	movq	%rax, -424(%rbp)
	movq	-688(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-696(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-512(%rbp), %r15
	call	_ZN7testing8internal13PrintStringToERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSo@PLT
	leaq	-496(%rbp), %rax
	movq	$0, -504(%rbp)
	movq	%rax, -696(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L361
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L362
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L363:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-752(%rbp), %xmm5
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm5, -432(%rbp)
	cmpq	-688(%rbp), %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	-680(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-656(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-728(%rbp), %r8
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	leaq	-624(%rbp), %rdi
	leaq	.LC27(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	-480(%rbp), %rdi
	cmpq	-736(%rbp), %rdi
	je	.L356
	call	_ZdlPv@PLT
	cmpb	$0, -624(%rbp)
	jne	.L367
.L392:
	leaq	-632(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-616(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L368
	movq	(%rax), %r8
.L368:
	leaq	-640(%rbp), %r15
	movl	$95, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L367
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L390:
	testq	%rdx, %rdx
	je	.L355
	movq	_ZL8cb_1_arg(%rip), %rsi
	movq	-544(%rbp), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L354
.L355:
	leaq	-624(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L356:
	cmpb	$0, -624(%rbp)
	je	.L392
.L367:
	movq	-616(%rbp), %r15
	testq	%r15, %r15
	je	.L370
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L371
	call	_ZdlPv@PLT
.L371:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L370:
	movq	-544(%rbp), %rdi
	cmpq	-664(%rbp), %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	%r14, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-648(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-672(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L393
	addq	$728, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L362:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L387:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L389:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L361:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L363
.L393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8553:
	.size	_ZN39EnvironmentTest_AtExitWithArgument_Test8TestBodyEv, .-_ZN39EnvironmentTest_AtExitWithArgument_Test8TestBodyEv
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"node::Environment::GetCurrent(context)"
	.align 8
.LC30:
	.string	"node::GetCurrentEnvironment(context)"
	.align 8
.LC31:
	.string	"node::Environment::GetCurrent(isolate_)"
	.text
	.align 2
	.p2align 4
	.globl	_ZN40EnvironmentTest_NoEnvironmentSanity_Test8TestBodyEv
	.type	_ZN40EnvironmentTest_NoEnvironmentSanity_Test8TestBodyEv, @function
_ZN40EnvironmentTest_NoEnvironmentSanity_Test8TestBodyEv:
.LFB8574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	16(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L397
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L397
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L397
	movq	271(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L396
	leaq	-80(%rbp), %r13
	leaq	-120(%rbp), %rdx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L399:
	cmpb	$0, -80(%rbp)
	je	.L503
.L400:
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L403
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L403:
	movq	%r12, %rdi
	call	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L504
	leaq	-120(%rbp), %rdx
	leaq	.LC30(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L406:
	cmpb	$0, -80(%rbp)
	je	.L505
.L407:
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L410
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L410:
	movq	16(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L501
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L415
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L415
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L415
	movq	271(%rax), %r14
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r14, -120(%rbp)
	testq	%r14, %r14
	je	.L413
	leaq	-120(%rbp), %rdx
	leaq	.LC31(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -80(%rbp)
	je	.L506
.L418:
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L421
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L421:
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	testq	%r12, %r12
	je	.L425
	movq	%r12, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L425
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L425
	movq	271(%rax), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L424
	leaq	-120(%rbp), %rdx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L427:
	cmpb	$0, -80(%rbp)
	je	.L507
.L428:
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L431
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L431:
	movq	%r12, %rdi
	call	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L508
	leaq	-120(%rbp), %rdx
	leaq	.LC30(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L434:
	cmpb	$0, -80(%rbp)
	je	.L509
.L435:
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L438
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L438:
	movq	16(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L502
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L443
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L443
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L443
	movq	271(%rax), %rbx
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%rbx, -120(%rbp)
	testq	%rbx, %rbx
	je	.L441
	leaq	-120(%rbp), %rdx
	leaq	.LC31(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -80(%rbp)
	je	.L510
.L446:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L449
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L449:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L502:
	movq	$0, -120(%rbp)
.L441:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -80(%rbp)
	jne	.L446
.L510:
	leaq	-120(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-72(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L447
	movq	(%rax), %r8
.L447:
	leaq	-128(%rbp), %r13
	movl	$135, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L446
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L501:
	movq	$0, -120(%rbp)
.L413:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -80(%rbp)
	jne	.L418
.L506:
	leaq	-120(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-72(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L419
	movq	(%rax), %r8
.L419:
	leaq	-128(%rbp), %rdi
	movl	$130, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L418
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L505:
	leaq	-120(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-72(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L408
	movq	(%rax), %r8
.L408:
	leaq	-128(%rbp), %rdi
	movl	$129, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L407
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	-120(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-72(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L436
	movq	(%rax), %r8
.L436:
	leaq	-128(%rbp), %rdi
	movl	$134, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	-120(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-72(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L429
	movq	(%rax), %r8
.L429:
	leaq	-128(%rbp), %rdi
	movl	$133, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L428
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L503:
	leaq	-120(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-72(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L401
	movq	(%rax), %r8
.L401:
	leaq	-128(%rbp), %rdi
	movl	$128, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L400
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L397:
	movq	$0, -120(%rbp)
.L396:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L508:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L425:
	movq	$0, -120(%rbp)
.L424:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L406
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8574:
	.size	_ZN40EnvironmentTest_NoEnvironmentSanity_Test8TestBodyEv, .-_ZN40EnvironmentTest_NoEnvironmentSanity_Test8TestBodyEv
	.section	.rodata.str1.1
.LC32:
	.string	"1"
.LC33:
	.string	"called"
.LC34:
	.string	"0"
.LC35:
	.string	"called_unref"
	.text
	.align 2
	.p2align 4
	.globl	_ZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEv
	.type	_ZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEv, @function
_ZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEv:
.LFB8603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$8, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC24(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$664, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-576(%rbp), %rax
	movl	$0, -592(%rbp)
	movq	%rax, %rdi
	movq	%rax, -656(%rbp)
	movl	$0, -588(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm5
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, -432(%rbp)
	movaps	%xmm0, -448(%rbp)
	movq	-440(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%r12, %r15
	movq	%rax, (%r12)
	movl	%ebx, %r12d
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-448(%rbp), %rax
	movq	%rax, -600(%rbp)
	.p2align 4,,10
	.p2align 3
.L513:
	movq	-600(%rbp), %rax
	movslq	%r12d, %rbx
	movq	(%rax,%r14), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -616(%rbp)
	call	strlen@PLT
	movq	(%r15), %rdi
	movq	-616(%rbp), %rcx
	movq	%r13, %rdx
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	movslq	%r8d, %rsi
	addq	%rbx, %rdi
	movl	%r8d, -608(%rbp)
	call	snprintf@PLT
	movl	-608(%rbp), %r8d
	addq	(%r15), %rbx
	movq	%rbx, (%r15,%r14)
	addq	$8, %r14
	addl	%r8d, %r12d
	cmpq	$24, %r14
	jne	.L513
	movq	-576(%rbp), %r13
	xorl	%esi, %esi
	movq	%r15, %r12
	movq	$0, -544(%rbp)
	movq	%r13, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -544(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L587
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -536(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L588
	movq	-544(%rbp), %rsi
	movq	%r15, %r9
	movl	$3, %r8d
	movq	%r15, %rcx
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, -528(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L589
	movl	$40, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_EE(%rip), %rdx
	leaq	-544(%rbp), %rcx
	movq	%rdx, (%rax)
	leaq	-592(%rbp), %rdx
	movq	%rdx, 32(%rax)
	movq	2480(%rbx), %rdx
	movb	$1, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, -640(%rbp)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L517
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L519
.L585:
	movq	(%rdi), %rax
	call	*8(%rax)
.L519:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L590
.L521:
	addl	$1, %eax
	movl	$40, %edi
	movq	-528(%rbp), %rbx
	movl	%eax, 4(%rdx)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_EE(%rip), %rdx
	movq	-640(%rbp), %rcx
	movq	%rdx, (%rax)
	leaq	-588(%rbp), %rdx
	movq	%rdx, 32(%rax)
	movq	2480(%rbx), %rdx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L522
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L524
.L586:
	movq	(%rdi), %rax
	call	*8(%rax)
.L524:
	movq	-528(%rbp), %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-536(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	-544(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-656(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	cmpl	$1, -592(%rbp)
	je	.L591
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-320(%rbp), %r12
	movq	.LC14(%rip), %xmm1
	movq	%rax, -608(%rbp)
	movq	%r12, %rdi
	leaq	-432(%rbp), %r14
	leaq	-368(%rbp), %r13
	leaq	-424(%rbp), %r15
	movhps	-608(%rbp), %xmm1
	movaps	%xmm1, -688(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edi, %edi
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%di, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movq	-600(%rbp), %rdi
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	%rax, -616(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movdqa	-688(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -624(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -632(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -608(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZNSolsEi@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -672(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -664(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L528
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L592
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L530:
	movq	-624(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC14(%rip), %xmm2
	movq	%rax, -448(%rbp)
	movq	-632(%rbp), %rax
	movhps	-648(%rbp), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -704(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-608(%rbp), %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	-616(%rbp), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movq	-600(%rbp), %rdi
	movups	%xmm0, -72(%rbp)
	movw	%si, -96(%rbp)
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-688(%rbp), %xmm6
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-624(%rbp), %rax
	movaps	%xmm6, -432(%rbp)
	movq	%rax, -448(%rbp)
	movq	-632(%rbp), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-648(%rbp), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	$0, -336(%rbp)
	leaq	-512(%rbp), %r15
	movq	%rax, -424(%rbp)
	movq	-608(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	-592(%rbp), %esi
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	call	_ZNSolsEi@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L532
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L533
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L534:
	movq	-624(%rbp), %rax
	movdqa	-704(%rbp), %xmm7
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-632(%rbp), %rax
	movaps	%xmm7, -432(%rbp)
	movq	%rax, -320(%rbp)
	cmpq	-608(%rbp), %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-640(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	-664(%rbp), %r8
	leaq	.LC32(%rip), %rdx
	leaq	.LC33(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	-480(%rbp), %rdi
	cmpq	-672(%rbp), %rdi
	je	.L527
	call	_ZdlPv@PLT
	cmpb	$0, -544(%rbp)
	jne	.L538
.L593:
	movq	-656(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-536(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L539
	movq	(%rax), %r8
.L539:
	leaq	-584(%rbp), %r12
	movl	$241, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L538
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L591:
	movq	-640(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L527:
	cmpb	$0, -544(%rbp)
	je	.L593
.L538:
	movq	-536(%rbp), %r12
	testq	%r12, %r12
	je	.L541
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L541:
	movl	-588(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L594
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-320(%rbp), %r12
	movq	.LC14(%rip), %xmm3
	movq	%rax, -608(%rbp)
	movq	%r12, %rdi
	leaq	-432(%rbp), %r14
	leaq	-368(%rbp), %r13
	leaq	-424(%rbp), %r15
	movhps	-608(%rbp), %xmm3
	movaps	%xmm3, -688(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	-600(%rbp), %rdi
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	%rax, -616(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-688(%rbp), %xmm3
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -624(%rbp)
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -632(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -608(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZNSolsEi@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -672(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -664(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L545
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L595
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L547:
	movq	-624(%rbp), %rax
	movq	-352(%rbp), %rdi
	movq	.LC14(%rip), %xmm4
	movq	%rax, -448(%rbp)
	movq	-632(%rbp), %rax
	movhps	-648(%rbp), %xmm4
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -704(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-608(%rbp), %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	-616(%rbp), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movq	-600(%rbp), %rdi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movdqa	-688(%rbp), %xmm6
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-624(%rbp), %rax
	movaps	%xmm6, -432(%rbp)
	movq	%rax, -448(%rbp)
	movq	-632(%rbp), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-648(%rbp), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	$0, -336(%rbp)
	leaq	-512(%rbp), %r15
	movq	%rax, -424(%rbp)
	movq	-608(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	-588(%rbp), %esi
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	call	_ZNSolsEi@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L549
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L550
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L551:
	movq	-624(%rbp), %rax
	movdqa	-704(%rbp), %xmm7
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-632(%rbp), %rax
	movaps	%xmm7, -432(%rbp)
	movq	%rax, -320(%rbp)
	cmpq	-608(%rbp), %rdi
	je	.L552
	call	_ZdlPv@PLT
.L552:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-640(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	-664(%rbp), %r8
	leaq	.LC34(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L553
	call	_ZdlPv@PLT
.L553:
	movq	-480(%rbp), %rdi
	cmpq	-672(%rbp), %rdi
	je	.L544
	call	_ZdlPv@PLT
	cmpb	$0, -544(%rbp)
	jne	.L555
.L596:
	movq	-656(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-536(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L556
	movq	(%rax), %r8
.L556:
	leaq	-584(%rbp), %r12
	movl	$242, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L555
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L590:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L594:
	movq	-640(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
.L544:
	cmpb	$0, -544(%rbp)
	je	.L596
.L555:
	movq	-536(%rbp), %r12
	testq	%r12, %r12
	je	.L512
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L512:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L597
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L586
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L517:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L585
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L595:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L592:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L550:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L533:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L549:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L532:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L589:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8603:
	.size	_ZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEv, .-_ZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEv
	.section	.rodata.str1.1
.LC36:
	.string	"called_at_exit_js"
	.text
	.align 2
	.p2align 4
	.globl	_ZN33EnvironmentTest_AtExitRunsJS_Test8TestBodyEv
	.type	_ZN33EnvironmentTest_AtExitRunsJS_Test8TestBodyEv, @function
_ZN33EnvironmentTest_AtExitRunsJS_Test8TestBodyEv:
.LFB8560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC24(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L599:
	movq	-200(%rbp), %rax
	movq	(%rax,%r15), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -224(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r9
	movq	-224(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -216(%rbp)
	movq	%r9, -208(%rbp)
	call	snprintf@PLT
	movq	-208(%rbp), %r9
	addq	(%r12), %r9
	movl	-216(%rbp), %r8d
	movq	%r9, (%r12,%r15)
	addq	$8, %r15
	addl	%r8d, %r13d
	cmpq	$24, %r15
	jne	.L599
	movq	-160(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L630
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L631
	movq	-128(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L632
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	leaq	_ZL10at_exit_jsPv(%rip), %rsi
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movzbl	_ZL17called_at_exit_js(%rip), %eax
	movq	$0, -168(%rbp)
	xorl	$1, %eax
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L633
.L604:
	movq	%r14, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movzbl	_ZL17called_at_exit_js(%rip), %eax
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L634
.L610:
	movq	%r14, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r15, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-232(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L635
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	leaq	-184(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -208(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC36(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-192(%rbp), %rdi
	movl	$106, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -200(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L612
	movq	(%rdi), %rax
	call	*8(%rax)
.L612:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L610
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L614
	movq	%r8, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r8
.L614:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	-184(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -216(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	leaq	.LC18(%rip), %r8
	leaq	.LC17(%rip), %rcx
	leaq	.LC36(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-192(%rbp), %rdi
	movl	$104, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -208(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-216(%rbp), %r9
	movq	-208(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-208(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L605
	call	_ZdlPv@PLT
.L605:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L606
	movq	(%rdi), %rax
	call	*8(%rax)
.L606:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L604
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L608
	movq	%r8, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %r8
.L608:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L630:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L631:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8560:
	.size	_ZN33EnvironmentTest_AtExitRunsJS_Test8TestBodyEv, .-_ZN33EnvironmentTest_AtExitRunsJS_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN32EnvironmentTest_AtExitOrder_Test8TestBodyEv
	.type	_ZN32EnvironmentTest_AtExitOrder_Test8TestBodyEv, @function
_ZN32EnvironmentTest_AtExitOrder_Test8TestBodyEv:
.LFB8546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC24(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-160(%rbp), %rbx
	subq	$184, %rsp
	movq	16(%rdi), %rsi
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L637:
	movq	-200(%rbp), %rax
	movq	(%rax,%r15), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -224(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r9
	movq	-224(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -216(%rbp)
	movq	%r9, -208(%rbp)
	call	snprintf@PLT
	movq	-208(%rbp), %r9
	addq	(%r12), %r9
	movl	-216(%rbp), %r8d
	movq	%r9, (%r12,%r15)
	addq	$8, %r15
	addl	%r8d, %r13d
	cmpq	$24, %r15
	jne	.L637
	movq	-160(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L668
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L669
	movq	-128(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L670
	movq	%rax, %rdi
	xorl	%edx, %edx
	leaq	_ZL25at_exit_callback_ordered1Pv(%rip), %rsi
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	xorl	%edx, %edx
	leaq	_ZL25at_exit_callback_ordered2Pv(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movq	%r14, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movzbl	_ZL19called_cb_ordered_1(%rip), %eax
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L671
.L642:
	movzbl	_ZL19called_cb_ordered_2(%rip), %eax
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L672
.L648:
	movq	%r14, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r15, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L673
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	leaq	-184(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -208(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC19(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-192(%rbp), %rdi
	movl	$84, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -200(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	movq	(%rdi), %rax
	call	*8(%rax)
.L650:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L648
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L652
	movq	%r8, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r8
.L652:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L671:
	leaq	-184(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -216(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-200(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC20(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-192(%rbp), %rdi
	movl	$83, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -208(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-216(%rbp), %r9
	movq	-208(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-208(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L644
	movq	(%rdi), %rax
	call	*8(%rax)
.L644:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L642
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L646
	movq	%r8, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %r8
.L646:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L668:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L673:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8546:
	.size	_ZN32EnvironmentTest_AtExitOrder_Test8TestBodyEv, .-_ZN32EnvironmentTest_AtExitOrder_Test8TestBodyEv
	.section	.rodata.str1.1
.LC37:
	.string	"called_cb_2"
	.text
	.align 2
	.p2align 4
	.globl	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test8TestBodyEv
	.type	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test8TestBodyEv, @function
_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test8TestBodyEv:
.LFB8567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$8, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.LC24(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rdx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%r12, %r15
	movq	%rax, (%r12)
	movl	%ebx, %r12d
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-232(%rbp), %rax
	movslq	%r12d, %rbx
	movq	(%rax,%r14), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -248(%rbp)
	call	strlen@PLT
	movq	(%r15), %rdi
	movq	-248(%rbp), %rcx
	movq	%r13, %rdx
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	movslq	%r8d, %rsi
	addq	%rbx, %rdi
	movl	%r8d, -240(%rbp)
	call	snprintf@PLT
	movl	-240(%rbp), %r8d
	addq	(%r15), %rbx
	movq	%rbx, (%r15,%r14)
	addq	$8, %r14
	addl	%r8d, %r12d
	cmpq	$24, %r14
	jne	.L675
	movq	-192(%rbp), %r14
	xorl	%esi, %esi
	movq	%r15, %r12
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -160(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L679
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L680
	movq	-160(%rbp), %rsi
	movq	%r15, %r9
	movq	%r15, %rcx
	movq	%rax, %rdi
	movl	$3, %r8d
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L681
	movq	-192(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -240(%rbp)
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L679
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	-240(%rbp), %r8
	xorl	%ecx, %ecx
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L680
	movq	-128(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L681
	xorl	%edx, %edx
	leaq	_ZL17at_exit_callback1Pv(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movq	-240(%rbp), %rdi
	xorl	%edx, %edx
	leaq	_ZL17at_exit_callback2Pv(%rip), %rsi
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movq	%r15, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movzbl	_ZL11called_cb_1(%rip), %eax
	movq	$0, -200(%rbp)
	movb	%al, -208(%rbp)
	testb	%al, %al
	je	.L730
.L683:
	movzbl	_ZL11called_cb_2(%rip), %eax
	movq	$0, -200(%rbp)
	xorl	$1, %eax
	movb	%al, -208(%rbp)
	testb	%al, %al
	je	.L731
.L689:
	movq	-240(%rbp), %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movzbl	_ZL11called_cb_2(%rip), %eax
	movq	$0, -200(%rbp)
	movb	%al, -208(%rbp)
	testb	%al, %al
	je	.L732
.L695:
	movq	-240(%rbp), %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-248(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L733
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	leaq	-216(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rdi
	leaq	-208(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-224(%rbp), %rdi
	movl	$122, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -232(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-264(%rbp), %r9
	movq	-232(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-232(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L696
	call	_ZdlPv@PLT
.L696:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L697
	movq	(%rdi), %rax
	call	*8(%rax)
.L697:
	movq	-200(%rbp), %r8
	testq	%r8, %r8
	je	.L695
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L699
	movq	%r8, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %r8
.L699:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L730:
	leaq	-216(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -272(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rdi
	leaq	-208(%rbp), %rsi
	leaq	.LC17(%rip), %r8
	leaq	.LC18(%rip), %rcx
	leaq	.LC25(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-224(%rbp), %rdi
	movl	$118, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -264(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-272(%rbp), %r9
	movq	-264(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-264(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L685
	movq	(%rdi), %rax
	call	*8(%rax)
.L685:
	movq	-200(%rbp), %r8
	testq	%r8, %r8
	je	.L683
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L687
	movq	%r8, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r8
.L687:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	-216(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -272(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rdi
	leaq	-208(%rbp), %rsi
	leaq	.LC18(%rip), %r8
	leaq	.LC17(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-224(%rbp), %rdi
	movl	$119, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -264(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-272(%rbp), %r9
	movq	-264(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-264(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L690
	call	_ZdlPv@PLT
.L690:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L691
	movq	(%rdi), %rax
	call	*8(%rax)
.L691:
	movq	-200(%rbp), %r8
	testq	%r8, %r8
	je	.L689
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L693
	movq	%r8, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r8
.L693:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L681:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L733:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8567:
	.size	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test8TestBodyEv, .-_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test8TestBodyEv
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB10055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L736
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L737
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L763
	.p2align 4,,10
	.p2align 3
.L736:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L742
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L748
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L764
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L742
.L743:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L750
.L764:
	lock subl	$1, 8(%r13)
	jne	.L750
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L750
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L743
	.p2align 4,,10
	.p2align 3
.L742:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L747:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L746
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L742
.L748:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L746
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L746
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L737:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L736
.L763:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L740
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L741:
	cmpl	$1, %eax
	jne	.L736
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L740:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L741
	.cfi_endproc
.LFE10055:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text._ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_:
.LFB10097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	cmpq	%rax, (%rcx)
	je	.L784
	movq	.LC14(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC15(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L785
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L769:
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L770
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L771
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L772:
	movq	.LC14(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC16(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L773
	call	_ZdlPv@PLT
.L773:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-560(%rbp), %xmm3
	movq	-536(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L786
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.L775:
	movq	-384(%rbp), %rax
	leaq	-496(%rbp), %r14
	movq	$0, -504(%rbp)
	leaq	-512(%rbp), %r15
	movq	%r14, -512(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L776
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L777
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L778:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L779
	call	_ZdlPv@PLT
.L779:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L780
	call	_ZdlPv@PLT
.L780:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L765
	call	_ZdlPv@PLT
.L765:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L787
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	movl	$4, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L784:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L777:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L771:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L786:
	movl	$4, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L770:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L776:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L778
.L787:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10097:
	.size	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	.section	.rodata.str1.1
.LC38:
	.string	"env"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"node::Environment::GetCurrent(env->context())"
	.align 8
.LC40:
	.string	"node::GetCurrentEnvironment(env->context())"
	.text
	.align 2
	.p2align 4
	.globl	_ZN37EnvironmentTest_NonNodeJSContext_Test8TestBodyEv
	.type	_ZN37EnvironmentTest_NonNodeJSContext_Test8TestBodyEv, @function
_ZN37EnvironmentTest_NonNodeJSContext_Test8TestBodyEv:
.LFB8581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-176(%rbp), %rax
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$8, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	.LC24(%rip), %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -216(%rbp)
	movq	16(%rdi), %rsi
	movq	%rax, %rdi
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%rax, -248(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC21(%rip), %rcx
	leaq	.LC22(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	.LC23(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r14
	call	malloc@PLT
	movq	%r14, %r15
	leaq	-80(%rbp), %r9
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	movq	%rax, (%r14)
	movl	%ebx, %r14d
	.p2align 4,,10
	.p2align 3
.L789:
	movq	(%r9,%r13), %rcx
	movq	%r9, -240(%rbp)
	movslq	%r14d, %rbx
	movq	%rcx, %rdi
	movq	%rcx, -232(%rbp)
	call	strlen@PLT
	movq	(%r15), %rdi
	movq	-232(%rbp), %rcx
	movq	%r12, %rdx
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	addq	%rbx, %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -224(%rbp)
	call	snprintf@PLT
	addq	(%r15), %rbx
	movl	-224(%rbp), %r8d
	movq	%rbx, (%r15,%r13)
	addq	$8, %r13
	movq	-240(%rbp), %r9
	addl	%r8d, %r14d
	cmpq	$24, %r13
	jne	.L789
	movq	-176(%rbp), %r12
	xorl	%esi, %esi
	movq	%r15, %r14
	movq	%r12, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L961
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L962
	movq	-144(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r15, %r9
	movq	%r15, %rcx
	movl	$3, %r8d
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, -240(%rbp)
	testq	%rax, %rax
	je	.L963
	leaq	-112(%rbp), %r12
	leaq	-184(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -112(%rbp)
	je	.L964
.L793:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L796
.L977:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L796:
	movq	-240(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-216(%rbp), %rax
	movq	16(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L876
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L801
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L801
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L801
	movq	271(%rax), %rbx
.L800:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L798:
	movq	%rbx, -184(%rbp)
	movq	%r13, %rcx
	leaq	-200(%rbp), %rbx
	leaq	.LC38(%rip), %rdx
	movq	%rbx, %r8
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L965
.L802:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L805
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L806
	call	_ZdlPv@PLT
.L806:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L805:
	movq	-200(%rbp), %rax
	movq	3280(%rax), %r15
	testq	%r15, %r15
	je	.L809
	movq	%r15, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L809
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L809
	movq	271(%rax), %rax
.L808:
	movq	%rbx, %r8
	movq	%r13, %rcx
	leaq	.LC38(%rip), %rdx
	movq	%r12, %rdi
	leaq	.LC39(%rip), %rsi
	movq	%rax, -184(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L966
.L810:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L813
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L814
	call	_ZdlPv@PLT
.L814:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L813:
	movq	-200(%rbp), %rax
	movq	3280(%rax), %rdi
	call	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE@PLT
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	leaq	.LC38(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	movq	%rax, -184(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L967
.L815:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L818
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L819
	call	_ZdlPv@PLT
.L819:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L818:
	movq	-216(%rbp), %rax
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	16(%rax), %rdi
	pushq	$0
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L822
	movq	%r15, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L822
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L822
	movq	271(%rax), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L821
	movq	%r13, %rdx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L824:
	cmpb	$0, -112(%rbp)
	je	.L968
.L825:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L828
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L829
	movq	%r8, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %r8
.L829:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L828:
	movq	%r15, %rdi
	call	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L969
	movq	%r13, %rdx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L831:
	cmpb	$0, -112(%rbp)
	je	.L970
.L832:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L835
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L836
	movq	%r8, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %r8
.L836:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L835:
	movq	-216(%rbp), %rax
	movq	16(%rax), %r8
	movq	%r8, %rdi
	movq	%r8, -256(%rbp)
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L882
	movq	-256(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-256(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	testq	%rax, %rax
	je	.L840
	movq	%rax, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L840
	movq	-256(%rbp), %rdx
	movq	(%rdx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L840
	movq	271(%rax), %rax
.L839:
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-256(%rbp), %rax
.L837:
	movq	%rbx, %r8
	movq	%r13, %rcx
	leaq	.LC38(%rip), %rdx
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, -184(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L971
.L841:
	movq	-104(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L844
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L845
	call	_ZdlPv@PLT
.L845:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L844:
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	testq	%r15, %r15
	je	.L848
	movq	%r15, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L848
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L848
	movq	271(%rax), %rax
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L847
	movq	%r13, %rdx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L850:
	cmpb	$0, -112(%rbp)
	je	.L972
.L851:
	movq	-104(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L854
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L855
	call	_ZdlPv@PLT
.L855:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L854:
	movq	%r15, %rdi
	call	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L973
	movq	%r13, %rdx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
.L857:
	cmpb	$0, -112(%rbp)
	je	.L974
.L858:
	movq	-104(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L861
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L862
	call	_ZdlPv@PLT
.L862:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L861:
	movq	-216(%rbp), %rax
	movq	16(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L960
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%rbx, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L866
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L866
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L866
	movq	271(%rax), %rbx
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%rbx, -184(%rbp)
	testq	%rbx, %rbx
	je	.L864
	movq	%r13, %rdx
	leaq	.LC31(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN7testing8internal18CmpHelperEQFailureIPN4node11EnvironmentEDnEENS_15AssertionResultEPKcS7_RKT_RKT0_.isra.0.constprop.0
	cmpb	$0, -112(%rbp)
	je	.L975
.L869:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L872
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L872:
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-240(%rbp), %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-232(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	-224(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r14), %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	free@PLT
	movq	-248(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L976
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L964:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L794
	movq	(%rax), %r8
.L794:
	leaq	-192(%rbp), %r15
	movl	$143, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L793
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	jne	.L977
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L960:
	movq	$0, -184(%rbp)
.L864:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -112(%rbp)
	jne	.L869
.L975:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L870
	movq	(%rax), %r8
.L870:
	leaq	-192(%rbp), %r12
	movl	$158, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L869
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L967:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L816
	movq	(%rax), %r8
.L816:
	leaq	-192(%rbp), %r15
	movl	$148, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L815
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L966:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L811
	movq	(%rax), %r8
.L811:
	leaq	-192(%rbp), %r15
	movl	$147, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L810
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L965:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L803
	movq	(%rax), %r8
.L803:
	leaq	-192(%rbp), %r15
	movl	$146, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L802
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L974:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L859
	movq	(%rax), %r8
.L859:
	leaq	-192(%rbp), %rbx
	movl	$157, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L858
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L972:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L852
	movq	(%rax), %r8
.L852:
	leaq	-192(%rbp), %rbx
	movl	$156, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L851
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L971:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L842
	movq	(%rax), %r8
.L842:
	leaq	-192(%rbp), %rbx
	movl	$153, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L841
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L970:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L833
	movq	(%rax), %r8
.L833:
	leaq	-192(%rbp), %rdi
	movl	$152, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rdi, -256(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-256(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-256(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L832
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L968:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L826
	movq	(%rax), %r8
.L826:
	leaq	-192(%rbp), %rdi
	movl	$151, %ecx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdx
	movq	%rdi, -256(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-256(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-256(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L825
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L969:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L822:
	movq	$0, -184(%rbp)
.L821:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L848:
	movq	$0, -184(%rbp)
.L847:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L801:
	xorl	%ebx, %ebx
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L809:
	xorl	%eax, %eax
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L840:
	xorl	%eax, %eax
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L961:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L962:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L963:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L876:
	xorl	%ebx, %ebx
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L882:
	xorl	%eax, %eax
	jmp	.L837
.L976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8581:
	.size	_ZN37EnvironmentTest_NonNodeJSContext_Test8TestBodyEv, .-_ZN37EnvironmentTest_NonNodeJSContext_Test8TestBodyEv
	.section	.rodata.str1.1
.LC41:
	.string	"*env"
.LC42:
	.string	"env_arg"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_E4CallES2_:
.LFB11932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC41(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-72(%rbp), %r12
	leaq	-88(%rbp), %rcx
	pushq	%rbx
	movq	%r12, %r8
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%rsi, -88(%rbp)
	leaq	-64(%rbp), %rdi
	leaq	.LC42(%rip), %rsi
	movq	16(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -64(%rbp)
	je	.L994
.L979:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L982
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L983
	call	_ZdlPv@PLT
.L983:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L982:
	movq	32(%rbx), %rax
	addl	$1, (%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L995
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L994:
	.cfi_restore_state
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L980
	movq	(%rax), %r8
.L980:
	movl	$232, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L979
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L979
.L995:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11932:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_E4CallES2_
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_E4CallES2_:
.LFB11930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC41(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-72(%rbp), %r12
	leaq	-88(%rbp), %rcx
	pushq	%rbx
	movq	%r12, %r8
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%rsi, -88(%rbp)
	leaq	-64(%rbp), %rdi
	leaq	.LC42(%rip), %rsi
	movq	16(%rax), %rax
	movq	%rax, -72(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIPN4node11EnvironmentES4_EENS_15AssertionResultEPKcS7_RKT_RKT0_
	cmpb	$0, -64(%rbp)
	je	.L1012
.L997:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L1000
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1001
	call	_ZdlPv@PLT
.L1001:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1000:
	movq	32(%rbx), %rax
	addl	$1, (%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1013
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1012:
	.cfi_restore_state
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC26(%rip), %r8
	testq	%rax, %rax
	je	.L998
	movq	(%rax), %r8
.L998:
	movl	$236, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L997
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L997
.L1013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11930:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_E4CallES2_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB10627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1015
	testq	%rsi, %rsi
	je	.L1031
.L1015:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L1032
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L1018
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L1019:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1033
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1019
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1017:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1019
.L1031:
	leaq	.LC8(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1033:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10627:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.1
.LC43:
	.string	"AtExitWithEnvironment"
.LC44:
	.string	"EnvironmentTest"
.LC45:
	.string	"AtExitWithoutEnvironment"
.LC46:
	.string	"AtExitOrder"
.LC47:
	.string	"AtExitWithArgument"
.LC48:
	.string	"AtExitRunsJS"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"MultipleEnvironmentsPerIsolate"
	.section	.rodata.str1.1
.LC50:
	.string	"NoEnvironmentSanity"
.LC51:
	.string	"NonNodeJSContext"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"BufferWithFreeCallbackIsDetached"
	.section	.rodata.str1.1
.LC53:
	.string	"SetImmediateCleanup"
	.section	.text.startup
	.p2align 4
	.type	_Z41__static_initialization_and_destruction_0ii.constprop.0, @function
_Z41__static_initialization_and_destruction_0ii.constprop.0:
.LFB12175:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	leaq	16+_ZL8cb_1_arg(%rip), %rax
	movq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	-16(%rax), %rsi
	movq	%rax, _ZL8cb_1_arg(%rip)
	movq	$0, 8+_ZL8cb_1_arg(%rip)
	movb	$0, 16+_ZL8cb_1_arg(%rip)
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1077
.L1035:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1078
.L1036:
	leaq	-128(%rbp), %r15
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	pushq	%r14
	leaq	_ZN15NodeTestFixture16TearDownTestCaseEv(%rip), %r14
	xorl	%ecx, %ecx
	pushq	%r13
	leaq	_ZN15NodeTestFixture13SetUpTestCaseEv(%rip), %r13
	movq	%r12, %r8
	xorl	%edx, %edx
	pushq	%r14
	leaq	.LC44(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	pushq	%r13
	leaq	.LC43(%rip), %rsi
	movl	$54, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1037
	call	_ZdlPv@PLT
.L1037:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L1038
	call	_ZdlPv@PLT
.L1038:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1079
.L1039:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1080
.L1040:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r11
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC45(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$64, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN45EnvironmentTest_AtExitWithoutEnvironment_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1041
	call	_ZdlPv@PLT
.L1041:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1042
	call	_ZdlPv@PLT
.L1042:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1081
.L1043:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1082
.L1044:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r10
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC46(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$74, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN32EnvironmentTest_AtExitOrder_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1045
	call	_ZdlPv@PLT
.L1045:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1046
	call	_ZdlPv@PLT
.L1046:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1083
.L1047:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1084
.L1048:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r9
	movq	%r12, %r8
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	leaq	.LC47(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$87, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN39EnvironmentTest_AtExitWithArgument_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1049
	call	_ZdlPv@PLT
.L1049:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1085
.L1051:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1086
.L1052:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r8
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC48(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$98, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN33EnvironmentTest_AtExitRunsJS_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1054
	call	_ZdlPv@PLT
.L1054:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1087
.L1055:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1088
.L1056:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rdi
	movq	%r12, %r8
	leaq	.LC44(%rip), %rdi
	leaq	.LC49(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$109, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1058
	call	_ZdlPv@PLT
.L1058:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1089
.L1059:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1090
.L1060:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rsi
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC50(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$125, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN40EnvironmentTest_NoEnvironmentSanity_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1091
.L1063:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1092
.L1064:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	xorl	%edx, %edx
	pushq	%rcx
	movq	%r12, %r8
	xorl	%ecx, %ecx
	leaq	.LC51(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$138, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN37EnvironmentTest_NonNodeJSContext_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1066
	call	_ZdlPv@PLT
.L1066:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1093
.L1067:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1094
.L1068:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	pushq	%rdx
	movq	%r12, %r8
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$193, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1070
	call	_ZdlPv@PLT
.L1070:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1095
.L1071:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L1096
.L1072:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC44(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC53(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$222, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN40EnvironmentTest_SetImmediateCleanup_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L1073
	call	_ZdlPv@PLT
.L1073:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1097
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1080:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1040
.L1079:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1039
.L1078:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1036
.L1090:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1060
.L1089:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1059
.L1092:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1064
.L1091:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1063
.L1094:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1068
.L1093:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1067
.L1096:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1072
.L1095:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1071
.L1082:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1044
.L1081:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1043
.L1084:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1048
.L1083:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1047
.L1086:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1052
.L1085:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1051
.L1088:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L1056
.L1087:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1055
.L1077:
	call	_ZN7testing8internal16SuiteApiResolverI15EnvironmentTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L1035
.L1097:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12175:
	.size	_Z41__static_initialization_and_destruction_0ii.constprop.0, .-_Z41__static_initialization_and_destruction_0ii.constprop.0
	.p2align 4
	.type	_GLOBAL__sub_I__ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E, @function
_GLOBAL__sub_I__ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E:
.LFB12035:
	.cfi_startproc
	endbr64
	jmp	_Z41__static_initialization_and_destruction_0ii.constprop.0
	.cfi_endproc
.LFE12035:
	.size	_GLOBAL__sub_I__ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E, .-_GLOBAL__sub_I__ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E
	.weak	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E
	.section	.bss._ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E,"awG",@nobits,_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E,comdat
	.type	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E, @gnu_unique_object
	.size	_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E, 1
_ZN7testing8internal12TypeIdHelperI15EnvironmentTestE6dummy_E:
	.zero	1
	.weak	_ZTV15NodeTestFixture
	.section	.data.rel.ro._ZTV15NodeTestFixture,"awG",@progbits,_ZTV15NodeTestFixture,comdat
	.align 8
	.type	_ZTV15NodeTestFixture, @object
	.size	_ZTV15NodeTestFixture, 64
_ZTV15NodeTestFixture:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI42EnvironmentTest_AtExitWithEnvironment_TestE10CreateTestEv
	.weak	_ZTV42EnvironmentTest_AtExitWithEnvironment_Test
	.section	.data.rel.ro.local._ZTV42EnvironmentTest_AtExitWithEnvironment_Test,"awG",@progbits,_ZTV42EnvironmentTest_AtExitWithEnvironment_Test,comdat
	.align 8
	.type	_ZTV42EnvironmentTest_AtExitWithEnvironment_Test, @object
	.size	_ZTV42EnvironmentTest_AtExitWithEnvironment_Test, 64
_ZTV42EnvironmentTest_AtExitWithEnvironment_Test:
	.quad	0
	.quad	0
	.quad	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD1Ev
	.quad	_ZN42EnvironmentTest_AtExitWithEnvironment_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN42EnvironmentTest_AtExitWithEnvironment_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI45EnvironmentTest_AtExitWithoutEnvironment_TestE10CreateTestEv
	.weak	_ZTV45EnvironmentTest_AtExitWithoutEnvironment_Test
	.section	.data.rel.ro.local._ZTV45EnvironmentTest_AtExitWithoutEnvironment_Test,"awG",@progbits,_ZTV45EnvironmentTest_AtExitWithoutEnvironment_Test,comdat
	.align 8
	.type	_ZTV45EnvironmentTest_AtExitWithoutEnvironment_Test, @object
	.size	_ZTV45EnvironmentTest_AtExitWithoutEnvironment_Test, 64
_ZTV45EnvironmentTest_AtExitWithoutEnvironment_Test:
	.quad	0
	.quad	0
	.quad	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD1Ev
	.quad	_ZN45EnvironmentTest_AtExitWithoutEnvironment_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32EnvironmentTest_AtExitOrder_TestE10CreateTestEv
	.weak	_ZTV32EnvironmentTest_AtExitOrder_Test
	.section	.data.rel.ro.local._ZTV32EnvironmentTest_AtExitOrder_Test,"awG",@progbits,_ZTV32EnvironmentTest_AtExitOrder_Test,comdat
	.align 8
	.type	_ZTV32EnvironmentTest_AtExitOrder_Test, @object
	.size	_ZTV32EnvironmentTest_AtExitOrder_Test, 64
_ZTV32EnvironmentTest_AtExitOrder_Test:
	.quad	0
	.quad	0
	.quad	_ZN32EnvironmentTest_AtExitOrder_TestD1Ev
	.quad	_ZN32EnvironmentTest_AtExitOrder_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN32EnvironmentTest_AtExitOrder_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39EnvironmentTest_AtExitWithArgument_TestE10CreateTestEv
	.weak	_ZTV39EnvironmentTest_AtExitWithArgument_Test
	.section	.data.rel.ro.local._ZTV39EnvironmentTest_AtExitWithArgument_Test,"awG",@progbits,_ZTV39EnvironmentTest_AtExitWithArgument_Test,comdat
	.align 8
	.type	_ZTV39EnvironmentTest_AtExitWithArgument_Test, @object
	.size	_ZTV39EnvironmentTest_AtExitWithArgument_Test, 64
_ZTV39EnvironmentTest_AtExitWithArgument_Test:
	.quad	0
	.quad	0
	.quad	_ZN39EnvironmentTest_AtExitWithArgument_TestD1Ev
	.quad	_ZN39EnvironmentTest_AtExitWithArgument_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN39EnvironmentTest_AtExitWithArgument_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33EnvironmentTest_AtExitRunsJS_TestE10CreateTestEv
	.weak	_ZTV33EnvironmentTest_AtExitRunsJS_Test
	.section	.data.rel.ro.local._ZTV33EnvironmentTest_AtExitRunsJS_Test,"awG",@progbits,_ZTV33EnvironmentTest_AtExitRunsJS_Test,comdat
	.align 8
	.type	_ZTV33EnvironmentTest_AtExitRunsJS_Test, @object
	.size	_ZTV33EnvironmentTest_AtExitRunsJS_Test, 64
_ZTV33EnvironmentTest_AtExitRunsJS_Test:
	.quad	0
	.quad	0
	.quad	_ZN33EnvironmentTest_AtExitRunsJS_TestD1Ev
	.quad	_ZN33EnvironmentTest_AtExitRunsJS_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN33EnvironmentTest_AtExitRunsJS_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestE10CreateTestEv
	.weak	_ZTV51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test
	.section	.data.rel.ro.local._ZTV51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test,"awG",@progbits,_ZTV51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test,comdat
	.align 8
	.type	_ZTV51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test, @object
	.size	_ZTV51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test, 64
_ZTV51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test:
	.quad	0
	.quad	0
	.quad	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD1Ev
	.quad	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_NoEnvironmentSanity_TestE10CreateTestEv
	.weak	_ZTV40EnvironmentTest_NoEnvironmentSanity_Test
	.section	.data.rel.ro.local._ZTV40EnvironmentTest_NoEnvironmentSanity_Test,"awG",@progbits,_ZTV40EnvironmentTest_NoEnvironmentSanity_Test,comdat
	.align 8
	.type	_ZTV40EnvironmentTest_NoEnvironmentSanity_Test, @object
	.size	_ZTV40EnvironmentTest_NoEnvironmentSanity_Test, 64
_ZTV40EnvironmentTest_NoEnvironmentSanity_Test:
	.quad	0
	.quad	0
	.quad	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD1Ev
	.quad	_ZN40EnvironmentTest_NoEnvironmentSanity_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN40EnvironmentTest_NoEnvironmentSanity_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI37EnvironmentTest_NonNodeJSContext_TestE10CreateTestEv
	.weak	_ZTV37EnvironmentTest_NonNodeJSContext_Test
	.section	.data.rel.ro.local._ZTV37EnvironmentTest_NonNodeJSContext_Test,"awG",@progbits,_ZTV37EnvironmentTest_NonNodeJSContext_Test,comdat
	.align 8
	.type	_ZTV37EnvironmentTest_NonNodeJSContext_Test, @object
	.size	_ZTV37EnvironmentTest_NonNodeJSContext_Test, 64
_ZTV37EnvironmentTest_NonNodeJSContext_Test:
	.quad	0
	.quad	0
	.quad	_ZN37EnvironmentTest_NonNodeJSContext_TestD1Ev
	.quad	_ZN37EnvironmentTest_NonNodeJSContext_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN37EnvironmentTest_NonNodeJSContext_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestE10CreateTestEv
	.weak	_ZTV53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test
	.section	.data.rel.ro.local._ZTV53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test,"awG",@progbits,_ZTV53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test,comdat
	.align 8
	.type	_ZTV53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test, @object
	.size	_ZTV53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test, 64
_ZTV53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test:
	.quad	0
	.quad	0
	.quad	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD1Ev
	.quad	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI40EnvironmentTest_SetImmediateCleanup_TestE10CreateTestEv
	.weak	_ZTV40EnvironmentTest_SetImmediateCleanup_Test
	.section	.data.rel.ro.local._ZTV40EnvironmentTest_SetImmediateCleanup_Test,"awG",@progbits,_ZTV40EnvironmentTest_SetImmediateCleanup_Test,comdat
	.align 8
	.type	_ZTV40EnvironmentTest_SetImmediateCleanup_Test, @object
	.size	_ZTV40EnvironmentTest_SetImmediateCleanup_Test, 64
_ZTV40EnvironmentTest_SetImmediateCleanup_Test:
	.quad	0
	.quad	0
	.quad	_ZN40EnvironmentTest_SetImmediateCleanup_TestD1Ev
	.quad	_ZN40EnvironmentTest_SetImmediateCleanup_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15EnvironmentTest8TearDownEv
	.quad	_ZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E_E4CallES2_
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN40EnvironmentTest_SetImmediateCleanup_Test8TestBodyEvEUlS2_E0_E4CallES2_
	.globl	_ZN40EnvironmentTest_SetImmediateCleanup_Test10test_info_E
	.bss
	.align 8
	.type	_ZN40EnvironmentTest_SetImmediateCleanup_Test10test_info_E, @object
	.size	_ZN40EnvironmentTest_SetImmediateCleanup_Test10test_info_E, 8
_ZN40EnvironmentTest_SetImmediateCleanup_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"../test/cctest/test_environment.cc:219"
	.section	.rodata.str1.1
.LC55:
	.string	"(ab->ByteLength()) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"virtual void EnvironmentTest_BufferWithFreeCallbackIsDetached_Test::TestBody()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_2, @object
	.size	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_2, 24
_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_2:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"../test/cctest/test_environment.cc:218"
	.section	.rodata.str1.1
.LC58:
	.string	"(callback_calls) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_1, @object
	.size	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_1, 24
_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_1:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC56
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"../test/cctest/test_environment.cc:215"
	.align 8
.LC60:
	.string	"(ab->ByteLength()) == (sizeof(hello))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_0, @object
	.size	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_0, 24
_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args_0:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC56
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../test/cctest/test_environment.cc:213"
	.section	.rodata.str1.1
.LC62:
	.string	"buf_obj->IsUint8Array()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args, @object
	.size	_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args, 24
_ZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC56
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"../test/cctest/test_environment.cc:209"
	.section	.rodata.str1.1
.LC64:
	.string	"(data) == (hello)"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"EnvironmentTest_BufferWithFreeCallbackIsDetached_Test::TestBody()::<lambda(char*, void*)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENKUlPcPvE_clES0_S1_E4args, @object
	.size	_ZZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENKUlPcPvE_clES0_S1_E4args, 24
_ZZZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test8TestBodyEvENKUlPcPvE_clES0_S1_E4args:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.globl	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test10test_info_E
	.bss
	.align 8
	.type	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test10test_info_E, @object
	.size	_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test10test_info_E, 8
_ZN53EnvironmentTest_BufferWithFreeCallbackIsDetached_Test10test_info_E:
	.zero	8
	.data
	.type	_ZL5hello, @object
	.size	_ZL5hello, 6
_ZL5hello:
	.string	"hello"
	.globl	_ZN37EnvironmentTest_NonNodeJSContext_Test10test_info_E
	.bss
	.align 8
	.type	_ZN37EnvironmentTest_NonNodeJSContext_Test10test_info_E, @object
	.size	_ZN37EnvironmentTest_NonNodeJSContext_Test10test_info_E, 8
_ZN37EnvironmentTest_NonNodeJSContext_Test10test_info_E:
	.zero	8
	.globl	_ZN40EnvironmentTest_NoEnvironmentSanity_Test10test_info_E
	.align 8
	.type	_ZN40EnvironmentTest_NoEnvironmentSanity_Test10test_info_E, @object
	.size	_ZN40EnvironmentTest_NoEnvironmentSanity_Test10test_info_E, 8
_ZN40EnvironmentTest_NoEnvironmentSanity_Test10test_info_E:
	.zero	8
	.globl	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test10test_info_E
	.align 8
	.type	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test10test_info_E, @object
	.size	_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test10test_info_E, 8
_ZN51EnvironmentTest_MultipleEnvironmentsPerIsolate_Test10test_info_E:
	.zero	8
	.globl	_ZN33EnvironmentTest_AtExitRunsJS_Test10test_info_E
	.align 8
	.type	_ZN33EnvironmentTest_AtExitRunsJS_Test10test_info_E, @object
	.size	_ZN33EnvironmentTest_AtExitRunsJS_Test10test_info_E, 8
_ZN33EnvironmentTest_AtExitRunsJS_Test10test_info_E:
	.zero	8
	.globl	_ZN39EnvironmentTest_AtExitWithArgument_Test10test_info_E
	.align 8
	.type	_ZN39EnvironmentTest_AtExitWithArgument_Test10test_info_E, @object
	.size	_ZN39EnvironmentTest_AtExitWithArgument_Test10test_info_E, 8
_ZN39EnvironmentTest_AtExitWithArgument_Test10test_info_E:
	.zero	8
	.globl	_ZN32EnvironmentTest_AtExitOrder_Test10test_info_E
	.align 8
	.type	_ZN32EnvironmentTest_AtExitOrder_Test10test_info_E, @object
	.size	_ZN32EnvironmentTest_AtExitOrder_Test10test_info_E, 8
_ZN32EnvironmentTest_AtExitOrder_Test10test_info_E:
	.zero	8
	.globl	_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test10test_info_E
	.align 8
	.type	_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test10test_info_E, @object
	.size	_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test10test_info_E, 8
_ZN45EnvironmentTest_AtExitWithoutEnvironment_Test10test_info_E:
	.zero	8
	.globl	_ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E
	.align 8
	.type	_ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E, @object
	.size	_ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E, 8
_ZN42EnvironmentTest_AtExitWithEnvironment_Test10test_info_E:
	.zero	8
	.local	_ZL8cb_1_arg
	.comm	_ZL8cb_1_arg,32,32
	.local	_ZL17called_at_exit_js
	.comm	_ZL17called_at_exit_js,1,1
	.local	_ZL19called_cb_ordered_2
	.comm	_ZL19called_cb_ordered_2,1,1
	.local	_ZL19called_cb_ordered_1
	.comm	_ZL19called_cb_ordered_1,1,1
	.local	_ZL11called_cb_2
	.comm	_ZL11called_cb_2,1,1
	.local	_ZL11called_cb_1
	.comm	_ZL11called_cb_1,1,1
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"../test/cctest/node_test_fixture.h:140"
	.section	.rodata.str1.1
.LC67:
	.string	"(nullptr) != (environment_)"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"EnvironmentTestFixture::Env::Env(const v8::HandleScope&, const Argv&)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1:
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"../test/cctest/node_test_fixture.h:135"
	.section	.rodata.str1.1
.LC70:
	.string	"(nullptr) != (isolate_data_)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0:
	.quad	.LC69
	.quad	.LC70
	.quad	.LC68
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"../test/cctest/node_test_fixture.h:129"
	.section	.rodata.str1.1
.LC72:
	.string	"!context_.IsEmpty()"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args:
	.quad	.LC71
	.quad	.LC72
	.quad	.LC68
	.weak	_ZZN15NodeTestFixture5SetUpEvE4args
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"../test/cctest/node_test_fixture.h:108"
	.section	.rodata.str1.1
.LC74:
	.string	"(isolate_) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"virtual void NodeTestFixture::SetUp()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture5SetUpEvE4args,"awG",@progbits,_ZZN15NodeTestFixture5SetUpEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture5SetUpEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture5SetUpEvE4args, 24
_ZZN15NodeTestFixture5SetUpEvE4args:
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.weak	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"../test/cctest/node_test_fixture.h:101"
	.align 8
.LC77:
	.string	"(0) == (uv_loop_close(&current_loop))"
	.align 8
.LC78:
	.string	"static void NodeTestFixture::TearDownTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture16TearDownTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture16TearDownTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, 24
_ZZN15NodeTestFixture16TearDownTestCaseEvE4args:
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.weak	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"../test/cctest/node_test_fixture.h:87"
	.align 8
.LC80:
	.string	"(0) == (uv_loop_init(&current_loop))"
	.align 8
.LC81:
	.string	"static void NodeTestFixture::SetUpTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture13SetUpTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture13SetUpTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, 24
_ZZN15NodeTestFixture13SetUpTestCaseEvE4args:
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC82:
	.string	"../src/tracing/agent.h:91"
.LC83:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC14:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC15:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC16:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
