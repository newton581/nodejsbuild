	.file	"test_node_postmortem_metadata.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB4725:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4725:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB6984:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6984:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB8518:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE8518:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE,"axG",@progbits,_ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE
	.type	_ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE, @function
_ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE:
.LFB8578:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8578:
	.size	_ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE, .-_ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE
	.section	.text._ZNK14TestHandleWrap8SelfSizeEv,"axG",@progbits,_ZNK14TestHandleWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK14TestHandleWrap8SelfSizeEv
	.type	_ZNK14TestHandleWrap8SelfSizeEv, @function
_ZNK14TestHandleWrap8SelfSizeEv:
.LFB8580:
	.cfi_startproc
	endbr64
	movl	$88, %eax
	ret
	.cfi_endproc
.LFE8580:
	.size	_ZNK14TestHandleWrap8SelfSizeEv, .-_ZNK14TestHandleWrap8SelfSizeEv
	.section	.text._ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE,"axG",@progbits,_ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE
	.type	_ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE, @function
_ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE:
.LFB8588:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8588:
	.size	_ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE, .-_ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE
	.section	.text._ZNK11TestReqWrap8SelfSizeEv,"axG",@progbits,_ZNK11TestReqWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK11TestReqWrap8SelfSizeEv
	.type	_ZNK11TestReqWrap8SelfSizeEv, @function
_ZNK11TestReqWrap8SelfSizeEv:
.LFB8590:
	.cfi_startproc
	endbr64
	movl	$152, %eax
	ret
	.cfi_endproc
.LFE8590:
	.size	_ZNK11TestReqWrap8SelfSizeEv, .-_ZNK11TestReqWrap8SelfSizeEv
	.section	.text._ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE,"axG",@progbits,_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE
	.type	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE, @function
_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE:
.LFB8635:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8635:
	.size	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE, .-_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE
	.section	.text._ZNK15DummyBaseObject8SelfSizeEv,"axG",@progbits,_ZNK15DummyBaseObject8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15DummyBaseObject8SelfSizeEv
	.type	_ZNK15DummyBaseObject8SelfSizeEv, @function
_ZNK15DummyBaseObject8SelfSizeEv:
.LFB8637:
	.cfi_startproc
	endbr64
	movl	$32, %eax
	ret
	.cfi_endproc
.LFE8637:
	.size	_ZNK15DummyBaseObject8SelfSizeEv, .-_ZNK15DummyBaseObject8SelfSizeEv
	.section	.text._ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED2Ev:
.LFB11601:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11601:
	.size	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED1Ev,_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED2Ev:
.LFB11609:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11609:
	.size	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED1Ev,_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED2Ev:
.LFB11617:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11617:
	.size	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED1Ev,_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED2Ev:
.LFB11625:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11625:
	.size	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED1Ev,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED2Ev:
.LFB11633:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11633:
	.size	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED1Ev,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED2Ev:
.LFB11645:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11645:
	.size	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED1Ev,_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED2Ev:
.LFB11653:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11653:
	.size	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED1Ev,_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED2Ev
	.section	.text._ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv:
.LFB11745:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE11745:
	.size	_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.section	.text._ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED0Ev:
.LFB11655:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11655:
	.size	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED0Ev:
.LFB11647:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11647:
	.size	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED0Ev:
.LFB11635:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11635:
	.size	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED0Ev:
.LFB11627:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11627:
	.size	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED0Ev:
.LFB11619:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11619:
	.size	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED0Ev:
.LFB11611:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11611:
	.size	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED0Ev:
.LFB11603:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11603:
	.size	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED0Ev
	.section	.text._ZN14TestHandleWrapD2Ev,"axG",@progbits,_ZN14TestHandleWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN14TestHandleWrapD2Ev
	.type	_ZN14TestHandleWrapD2Ev, @function
_ZN14TestHandleWrapD2Ev:
.LFB11661:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE11661:
	.size	_ZN14TestHandleWrapD2Ev, .-_ZN14TestHandleWrapD2Ev
	.weak	_ZN14TestHandleWrapD1Ev
	.set	_ZN14TestHandleWrapD1Ev,_ZN14TestHandleWrapD2Ev
	.section	.text._ZN14TestHandleWrapD0Ev,"axG",@progbits,_ZN14TestHandleWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN14TestHandleWrapD0Ev
	.type	_ZN14TestHandleWrapD0Ev, @function
_ZN14TestHandleWrapD0Ev:
.LFB11663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdx
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11663:
	.size	_ZN14TestHandleWrapD0Ev, .-_ZN14TestHandleWrapD0Ev
	.section	.text._ZN4node7ReqWrapI8uv_req_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI8uv_req_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI8uv_req_sE6CancelEv
	.type	_ZN4node7ReqWrapI8uv_req_sE6CancelEv, @function
_ZN4node7ReqWrapI8uv_req_sE6CancelEv:
.LFB11744:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L31
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE11744:
	.size	_ZN4node7ReqWrapI8uv_req_sE6CancelEv, .-_ZN4node7ReqWrapI8uv_req_sE6CancelEv
	.section	.text._ZN15NodeTestFixture8TearDownEv,"axG",@progbits,_ZN15NodeTestFixture8TearDownEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture8TearDownEv
	.type	_ZN15NodeTestFixture8TearDownEv, @function
_ZN15NodeTestFixture8TearDownEv:
.LFB8491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8491:
	.size	_ZN15NodeTestFixture8TearDownEv, .-_ZN15NodeTestFixture8TearDownEv
	.section	.text._ZN15NodeTestFixture5SetUpEv,"axG",@progbits,_ZN15NodeTestFixture5SetUpEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture5SetUpEv
	.type	_ZN15NodeTestFixture5SetUpEv, @function
_ZN15NodeTestFixture5SetUpEv:
.LFB8488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node26CreateArrayBufferAllocatorEv@PLT
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %r8
	movq	%rax, %rdi
	movq	%rax, 8+_ZN15NodeTestFixture9allocatorE(%rip)
	testq	%r8, %r8
	je	.L35
	movq	%r8, %rdi
	call	*_ZN15NodeTestFixture9allocatorE(%rip)
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %rdi
.L35:
	movq	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE@GOTPCREL(%rip), %rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	movq	%rax, _ZN15NodeTestFixture9allocatorE(%rip)
	call	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L42
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate5EnterEv@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture5SetUpEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8488:
	.size	_ZN15NodeTestFixture5SetUpEv, .-_ZN15NodeTestFixture5SetUpEv
	.section	.text._ZN33DebugSymbolsTest_ReqWrapList_TestD2Ev,"axG",@progbits,_ZN33DebugSymbolsTest_ReqWrapList_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33DebugSymbolsTest_ReqWrapList_TestD2Ev
	.type	_ZN33DebugSymbolsTest_ReqWrapList_TestD2Ev, @function
_ZN33DebugSymbolsTest_ReqWrapList_TestD2Ev:
.LFB11597:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11597:
	.size	_ZN33DebugSymbolsTest_ReqWrapList_TestD2Ev, .-_ZN33DebugSymbolsTest_ReqWrapList_TestD2Ev
	.weak	_ZN33DebugSymbolsTest_ReqWrapList_TestD1Ev
	.set	_ZN33DebugSymbolsTest_ReqWrapList_TestD1Ev,_ZN33DebugSymbolsTest_ReqWrapList_TestD2Ev
	.section	.text._ZN33DebugSymbolsTest_ReqWrapList_TestD0Ev,"axG",@progbits,_ZN33DebugSymbolsTest_ReqWrapList_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33DebugSymbolsTest_ReqWrapList_TestD0Ev
	.type	_ZN33DebugSymbolsTest_ReqWrapList_TestD0Ev, @function
_ZN33DebugSymbolsTest_ReqWrapList_TestD0Ev:
.LFB11599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11599:
	.size	_ZN33DebugSymbolsTest_ReqWrapList_TestD0Ev, .-_ZN33DebugSymbolsTest_ReqWrapList_TestD0Ev
	.section	.text._ZN36DebugSymbolsTest_HandleWrapList_TestD2Ev,"axG",@progbits,_ZN36DebugSymbolsTest_HandleWrapList_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN36DebugSymbolsTest_HandleWrapList_TestD2Ev
	.type	_ZN36DebugSymbolsTest_HandleWrapList_TestD2Ev, @function
_ZN36DebugSymbolsTest_HandleWrapList_TestD2Ev:
.LFB11605:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11605:
	.size	_ZN36DebugSymbolsTest_HandleWrapList_TestD2Ev, .-_ZN36DebugSymbolsTest_HandleWrapList_TestD2Ev
	.weak	_ZN36DebugSymbolsTest_HandleWrapList_TestD1Ev
	.set	_ZN36DebugSymbolsTest_HandleWrapList_TestD1Ev,_ZN36DebugSymbolsTest_HandleWrapList_TestD2Ev
	.section	.text._ZN36DebugSymbolsTest_HandleWrapList_TestD0Ev,"axG",@progbits,_ZN36DebugSymbolsTest_HandleWrapList_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN36DebugSymbolsTest_HandleWrapList_TestD0Ev
	.type	_ZN36DebugSymbolsTest_HandleWrapList_TestD0Ev, @function
_ZN36DebugSymbolsTest_HandleWrapList_TestD0Ev:
.LFB11607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11607:
	.size	_ZN36DebugSymbolsTest_HandleWrapList_TestD0Ev, .-_ZN36DebugSymbolsTest_HandleWrapList_TestD0Ev
	.section	.text._ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD2Ev,"axG",@progbits,_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD2Ev
	.type	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD2Ev, @function
_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD2Ev:
.LFB11613:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11613:
	.size	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD2Ev, .-_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD2Ev
	.weak	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD1Ev
	.set	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD1Ev,_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD2Ev
	.section	.text._ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD0Ev,"axG",@progbits,_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD0Ev
	.type	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD0Ev, @function
_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD0Ev:
.LFB11615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11615:
	.size	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD0Ev, .-_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD0Ev
	.section	.text._ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD2Ev,"axG",@progbits,_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD2Ev
	.type	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD2Ev, @function
_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD2Ev:
.LFB11621:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11621:
	.size	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD2Ev, .-_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD2Ev
	.weak	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD1Ev
	.set	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD1Ev,_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD2Ev
	.section	.text._ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD0Ev,"axG",@progbits,_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD0Ev
	.type	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD0Ev, @function
_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD0Ev:
.LFB11623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11623:
	.size	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD0Ev, .-_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD0Ev
	.section	.text._ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD2Ev,"axG",@progbits,_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD2Ev
	.type	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD2Ev, @function
_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD2Ev:
.LFB11629:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11629:
	.size	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD2Ev, .-_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD2Ev
	.weak	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD1Ev
	.set	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD1Ev,_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD2Ev
	.section	.text._ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD0Ev,"axG",@progbits,_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD0Ev
	.type	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD0Ev, @function
_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD0Ev:
.LFB11631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11631:
	.size	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD0Ev, .-_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD0Ev
	.section	.text._ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD2Ev,"axG",@progbits,_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD2Ev
	.type	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD2Ev, @function
_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD2Ev:
.LFB11641:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11641:
	.size	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD2Ev, .-_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD2Ev
	.weak	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD1Ev
	.set	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD1Ev,_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD2Ev
	.section	.text._ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD0Ev,"axG",@progbits,_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD0Ev
	.type	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD0Ev, @function
_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD0Ev:
.LFB11643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11643:
	.size	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD0Ev, .-_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD0Ev
	.section	.text._ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD2Ev,"axG",@progbits,_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD2Ev
	.type	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD2Ev, @function
_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD2Ev:
.LFB11649:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11649:
	.size	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD2Ev, .-_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD2Ev
	.weak	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD1Ev
	.set	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD1Ev,_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD2Ev
	.section	.text._ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD0Ev,"axG",@progbits,_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD0Ev
	.type	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD0Ev, @function
_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD0Ev:
.LFB11651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11651:
	.size	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD0Ev, .-_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv:
.LFB11743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11743:
	.size	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv:
.LFB11742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV46DebugSymbolsTest_ExternalStringDataOffset_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11742:
	.size	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv:
.LFB11741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV48DebugSymbolsTest_BaseObjectPersistentHandle_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11741:
	.size	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv:
.LFB11740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11740:
	.size	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv:
.LFB11739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV45DebugSymbolsTest_EnvironmentReqWrapQueue_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11739:
	.size	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv:
.LFB11738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV36DebugSymbolsTest_HandleWrapList_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11738:
	.size	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv:
.LFB11737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV33DebugSymbolsTest_ReqWrapList_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11737:
	.size	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv
	.section	.text._ZN15NodeTestFixture16TearDownTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture16TearDownTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture16TearDownTestCaseEv
	.type	_ZN15NodeTestFixture16TearDownTestCaseEv, @function
_ZN15NodeTestFixture16TearDownTestCaseEv:
.LFB8487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L80:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L83
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_close@PLT
	testl	%eax, %eax
	jne	.L84
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8487:
	.size	_ZN15NodeTestFixture16TearDownTestCaseEv, .-_ZN15NodeTestFixture16TearDownTestCaseEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0:
.LFB11865:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11865:
	.size	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0:
.LFB11866:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L92:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11866:
	.size	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	.section	.text._ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv:
.LFB11974:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE11974:
	.size	_ZThn56_N4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB11903:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L105
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L106
	cmpq	$1, %rax
	jne	.L98
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L99:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L98:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L99
	jmp	.L97
.L106:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L97:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L99
.L105:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11903:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZN4node7ReqWrapI8uv_req_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI8uv_req_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI8uv_req_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI8uv_req_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI8uv_req_sE6CancelEv:
.LFB11976:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L110
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE11976:
	.size	_ZThn56_N4node7ReqWrapI8uv_req_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI8uv_req_sE6CancelEv
	.section	.text._ZN11TestReqWrapD0Ev,"axG",@progbits,_ZN11TestReqWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N11TestReqWrapD0Ev
	.type	_ZThn56_N11TestReqWrapD0Ev, @function
_ZThn56_N11TestReqWrapD0Ev:
.LFB11978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI8uv_req_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L114
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$152, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11978:
	.size	_ZThn56_N11TestReqWrapD0Ev, .-_ZThn56_N11TestReqWrapD0Ev
	.section	.text._ZN11TestReqWrapD2Ev,"axG",@progbits,_ZN11TestReqWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N11TestReqWrapD1Ev
	.type	_ZThn56_N11TestReqWrapD1Ev, @function
_ZThn56_N11TestReqWrapD1Ev:
.LFB11975:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI8uv_req_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L120
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11975:
	.size	_ZThn56_N11TestReqWrapD1Ev, .-_ZThn56_N11TestReqWrapD1Ev
	.align 2
	.p2align 4
	.weak	_ZN11TestReqWrapD2Ev
	.type	_ZN11TestReqWrapD2Ev, @function
_ZN11TestReqWrapD2Ev:
.LFB11657:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI8uv_req_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L126
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11657:
	.size	_ZN11TestReqWrapD2Ev, .-_ZN11TestReqWrapD2Ev
	.weak	_ZN11TestReqWrapD1Ev
	.set	_ZN11TestReqWrapD1Ev,_ZN11TestReqWrapD2Ev
	.section	.text._ZN11TestReqWrapD0Ev,"axG",@progbits,_ZN11TestReqWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN11TestReqWrapD0Ev
	.type	_ZN11TestReqWrapD0Ev, @function
_ZN11TestReqWrapD0Ev:
.LFB11659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI8uv_req_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L130
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$152, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11659:
	.size	_ZN11TestReqWrapD0Ev, .-_ZN11TestReqWrapD0Ev
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB10172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L133
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L134
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L160
	.p2align 4,,10
	.p2align 3
.L133:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L139
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L145
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L161
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L139
.L140:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L147
.L161:
	lock subl	$1, 8(%r13)
	jne	.L147
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L147
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L140
	.p2align 4,,10
	.p2align 3
.L139:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L143
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L139
.L145:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L143
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L143
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L134:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L133
.L160:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L137
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L138:
	cmpl	$1, %eax
	jne	.L133
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L138
	.cfi_endproc
.LFE10172:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata._ZN15NodeTestFixture13SetUpTestCaseEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"NODE_OPTIONS"
.LC6:
	.string	"cctest"
	.section	.text._ZN15NodeTestFixture13SetUpTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture13SetUpTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture13SetUpTestCaseEv
	.type	_ZN15NodeTestFixture13SetUpTestCaseEv, @function
_ZN15NodeTestFixture13SetUpTestCaseEv:
.LFB8482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN15NodeTestFixture16node_initializedE(%rip)
	je	.L204
.L163:
	movl	$1312, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing5AgentC1Ev@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r13
	movq	%r12, _ZN15NodeTestFixture13tracing_agentE(%rip)
	testq	%r13, %r13
	je	.L164
	movq	%r13, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r12
.L164:
	movq	%r12, %rdi
	call	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %rax
	movq	976(%rax), %r12
	testq	%r12, %r12
	je	.L205
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L206
	movl	$128, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r12
	movq	%r13, _ZN15NodeTestFixture8platformE(%rip)
	testq	%r12, %r12
	je	.L167
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L168
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L170
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L171
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L172:
	cmpl	$1, %eax
	jne	.L170
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L174
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L175:
	cmpl	$1, %eax
	jne	.L170
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L170:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L189
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L179
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L189
.L179:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L187
	lock subl	$1, 8(%r13)
	jne	.L187
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L187
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L179
	.p2align 4,,10
	.p2align 3
.L189:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
.L167:
	movq	%r13, %rdi
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L207
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L182
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L189
.L185:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L182
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L182
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	.LC5(%rip), %rdi
	call	uv_os_unsetenv@PLT
	leaq	.LC6(%rip), %rax
	leaq	-48(%rbp), %rcx
	movl	$1, -64(%rbp)
	leaq	-60(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movb	$1, _ZN15NodeTestFixture16node_initializedE(%rip)
	call	_ZN4node4InitEPiPPKcS0_PS3_@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r12, %rdi
	call	*%rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L171:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L174:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L175
.L207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8482:
	.size	_ZN15NodeTestFixture13SetUpTestCaseEv, .-_ZN15NodeTestFixture13SetUpTestCaseEv
	.section	.text._ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev
	.type	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev, @function
_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev:
.LFB8636:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1784827749, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8313999476397012292, %rcx
	movq	%rdx, (%rdi)
	movl	$25445, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$116, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE8636:
	.size	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev, .-_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev:
.LFB8579:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 30(%rdi)
	movq	%rdi, %rax
	movabsq	$7236828615050224980, %rcx
	movq	%rdx, (%rdi)
	movl	$28769, %edx
	movq	%rcx, 16(%rdi)
	movl	$1918330220, 24(%rdi)
	movw	%dx, 28(%rdi)
	movq	$14, 8(%rdi)
	ret
	.cfi_endproc
.LFE8579:
	.size	_ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev:
.LFB8589:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6300928758483477844, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE8589:
	.size	_ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev
	.section	.rodata.str1.1
.LC7:
	.string	""
.LC8:
	.string	"kEnvironmentIndex"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"nodedbg_const_ContextEmbedderIndex__kEnvironment__int"
	.align 8
.LC10:
	.string	"../test/cctest/test_node_postmortem_metadata.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test8TestBodyEv
	.type	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test8TestBodyEv, @function
_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test8TestBodyEv:
.LFB8624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$32, nodedbg_const_ContextEmbedderIndex__kEnvironment__int(%rip)
	je	.L239
	movq	.LC11(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, -592(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC12(%rip), %xmm1
	movaps	%xmm1, -576(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r12), %rdi
	movq	%r12, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-576(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -560(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -552(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r14, %rdi
	movl	$32, %esi
	call	_ZNSolsEi@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -616(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -584(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L214
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L240
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L216:
	movq	.LC11(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC13(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-552(%rbp), %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	-560(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r12), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	-592(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r12), %rax
	movq	%r12, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-576(%rbp), %xmm3
	movq	-560(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	movq	-552(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	nodedbg_const_ContextEmbedderIndex__kEnvironment__int(%rip), %esi
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	call	_ZNSolsEi@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L218
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L219
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L220:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-552(%rbp), %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-560(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r12), %rax
	movq	%r12, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-584(%rbp), %r8
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	leaq	-528(%rbp), %rdi
	leaq	.LC8(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	-480(%rbp), %rdi
	cmpq	-616(%rbp), %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	cmpb	$0, -528(%rbp)
	je	.L241
.L224:
	movq	-520(%rbp), %r12
	testq	%r12, %r12
	je	.L211
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L228
	call	_ZdlPv@PLT
.L228:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	-528(%rbp), %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -528(%rbp)
	jne	.L224
.L241:
	leaq	-536(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-520(%rbp), %rax
	leaq	.LC7(%rip), %r8
	testq	%rax, %rax
	je	.L225
	movq	(%rax), %r8
.L225:
	leaq	-544(%rbp), %r12
	movl	$67, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L219:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L214:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L216
.L242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8624:
	.size	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test8TestBodyEv, .-_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test8TestBodyEv
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB8516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L244:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L253
.L245:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L245
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8516:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB8504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L255
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L255
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L255
	movq	%r9, %rdi
.L258:
	cmpq	%rsi, %rbx
	jne	.L256
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L256
	cmpq	16(%rdi), %rbx
	jne	.L256
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L290
	testq	%rsi, %rsi
	je	.L260
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L260
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L260:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L255:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L291
.L263:
	cmpq	$0, 8(%rbx)
	je	.L254
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L267
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L292
.L267:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L254
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L254:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L293
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L272
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L260
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L259:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L294
.L261:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L292:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r10, %rax
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L291:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L295
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L263
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%rsi, 2600(%r12)
	jmp	.L261
.L295:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8504:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN15DummyBaseObjectD2Ev,"axG",@progbits,_ZN15DummyBaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN15DummyBaseObjectD2Ev
	.type	_ZN15DummyBaseObjectD2Ev, @function
_ZN15DummyBaseObjectD2Ev:
.LFB11637:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE11637:
	.size	_ZN15DummyBaseObjectD2Ev, .-_ZN15DummyBaseObjectD2Ev
	.weak	_ZN15DummyBaseObjectD1Ev
	.set	_ZN15DummyBaseObjectD1Ev,_ZN15DummyBaseObjectD2Ev
	.section	.text._ZN15DummyBaseObjectD0Ev,"axG",@progbits,_ZN15DummyBaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN15DummyBaseObjectD0Ev
	.type	_ZN15DummyBaseObjectD0Ev, @function
_ZN15DummyBaseObjectD0Ev:
.LFB11639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11639:
	.size	_ZN15DummyBaseObjectD0Ev, .-_ZN15DummyBaseObjectD0Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB8506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8506:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB10170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L303
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L304
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L330
	.p2align 4,,10
	.p2align 3
.L303:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L309
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L315
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L331
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L309
.L310:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L317
.L331:
	lock subl	$1, 8(%r13)
	jne	.L317
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L317
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L310
	.p2align 4,,10
	.p2align 3
.L309:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L313
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L309
.L315:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L313
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L313
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L304:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L303
.L330:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L307
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L308:
	cmpl	$1, %eax
	jne	.L303
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L307:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L308
	.cfi_endproc
.LFE10170:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text._ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB10202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	cmpq	%rax, (%rcx)
	je	.L347
	movq	.LC11(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC12(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r14, %rdi
	movq	(%r8), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L335
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L348
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L337:
	movq	.LC11(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC13(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L339
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L340
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L341:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L343
	call	_ZdlPv@PLT
.L343:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L349
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L347:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L340:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L339:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L337
.L349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10202:
	.size	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.rodata.str1.1
.LC14:
	.string	"sizeof(void*)"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"nodedbg_offset_ExternalString__data__uintptr_t"
	.text
	.align 2
	.p2align 4
	.globl	_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test8TestBodyEv
	.type	_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test8TestBodyEv, @function
_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test8TestBodyEv:
.LFB8631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	nodedbg_offset_ExternalString__data__uintptr_t(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r12
	leaq	-48(%rbp), %rdi
	movq	%r12, %r8
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$8, -56(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -48(%rbp)
	je	.L366
.L351:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L350
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L355
	call	_ZdlPv@PLT
.L355:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L350:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-40(%rbp), %rax
	leaq	.LC7(%rip), %r8
	testq	%rax, %rax
	je	.L352
	movq	(%rax), %r8
.L352:
	leaq	-64(%rbp), %r13
	movl	$72, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L351
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L351
.L367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8631:
	.size	_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test8TestBodyEv, .-_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test8TestBodyEv
	.section	.rodata.str1.1
.LC16:
	.string	"node"
.LC17:
	.string	"-p"
.LC18:
	.string	"process.version"
.LC19:
	.string	"%s"
.LC20:
	.string	"calculated"
.LC21:
	.string	"expected"
	.text
	.align 2
	.p2align 4
	.globl	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test8TestBodyEv
	.type	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test8TestBodyEv, @function
_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test8TestBodyEv:
.LFB8651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC19(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$5, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC16(%rip), %rdx
	leaq	.LC17(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC18(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$8, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	leaq	-80(%rbp), %r10
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L369:
	movq	(%r10,%rbx), %rcx
	movq	%r10, -224(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -216(%rbp)
	call	strlen@PLT
	movslq	%r14d, %r9
	movq	-216(%rbp), %rcx
	movq	%r15, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	snprintf@PLT
	movq	-200(%rbp), %r9
	addq	(%r12), %r9
	movl	-208(%rbp), %r8d
	movq	%r9, (%r12,%rbx)
	addq	$8, %rbx
	movq	-224(%rbp), %r10
	addl	%r8d, %r14d
	cmpq	$24, %rbx
	jne	.L369
	movq	-144(%rbp), %r15
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L389
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L390
	movq	-112(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L391
	leaq	2096(%rax), %rax
	leaq	-160(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	nodedbg_offset_Environment__handle_wrap_queue___Environment_HandleWrapQueue(%rip), %rax
	leaq	-192(%rbp), %rcx
	leaq	-184(%rbp), %r8
	leaq	.LC20(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	addq	%r15, %rax
	movq	%rax, -184(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L392
.L373:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L376
.L394:
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L377
	movq	%r8, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r8
.L377:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L376:
	movq	%r15, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%rbx, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L393
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	leaq	-168(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-200(%rbp), %r9
	leaq	.LC7(%rip), %r8
	testq	%rax, %rax
	je	.L374
	movq	(%rax), %r8
.L374:
	leaq	-176(%rbp), %rdi
	movl	$114, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%r9, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L373
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	jne	.L394
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L389:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L391:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8651:
	.size	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test8TestBodyEv, .-_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test8TestBodyEv
	.type	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test8TestBodyEv, @function
_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test8TestBodyEv:
.LFB8658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC19(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$5, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC16(%rip), %rdx
	leaq	.LC17(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC18(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$8, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	leaq	-80(%rbp), %r10
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L396:
	movq	(%r10,%rbx), %rcx
	movq	%r10, -224(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -216(%rbp)
	call	strlen@PLT
	movslq	%r14d, %r9
	movq	-216(%rbp), %rcx
	movq	%r15, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	snprintf@PLT
	movq	-200(%rbp), %r9
	addq	(%r12), %r9
	movl	-208(%rbp), %r8d
	movq	%r9, (%r12,%rbx)
	addq	$8, %rbx
	movq	-224(%rbp), %r10
	addl	%r8d, %r14d
	cmpq	$24, %rbx
	jne	.L396
	movq	-144(%rbp), %r15
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L416
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L417
	movq	-112(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L418
	leaq	2112(%rax), %rax
	leaq	-160(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	nodedbg_offset_Environment__req_wrap_queue___Environment_ReqWrapQueue(%rip), %rax
	leaq	-192(%rbp), %rcx
	leaq	-184(%rbp), %r8
	leaq	.LC20(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	addq	%r15, %rax
	movq	%rax, -184(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -160(%rbp)
	je	.L419
.L400:
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	je	.L403
.L421:
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L404
	movq	%r8, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r8
.L404:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L403:
	movq	%r15, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%rbx, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L420
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	leaq	-168(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -200(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-200(%rbp), %r9
	leaq	.LC7(%rip), %r8
	testq	%rax, %rax
	je	.L401
	movq	(%rax), %r8
.L401:
	leaq	-176(%rbp), %rdi
	movl	$125, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%r9, -208(%rbp)
	movq	%rdi, -200(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-200(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L400
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-152(%rbp), %r8
	testq	%r8, %r8
	jne	.L421
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L420:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8658:
	.size	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test8TestBodyEv, .-_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN36DebugSymbolsTest_HandleWrapList_Test8TestBodyEv
	.type	_ZN36DebugSymbolsTest_HandleWrapList_Test8TestBodyEv, @function
_ZN36DebugSymbolsTest_HandleWrapList_Test8TestBodyEv:
.LFB8665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC19(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$520, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-464(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC16(%rip), %rdx
	leaq	.LC17(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC18(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	movq	-296(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-304(%rbp), %rax
	movq	%rax, -520(%rbp)
	.p2align 4,,10
	.p2align 3
.L423:
	movq	-520(%rbp), %rax
	movq	(%rax,%r15), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -544(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r9
	movq	-544(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -536(%rbp)
	movq	%r9, -528(%rbp)
	call	snprintf@PLT
	movq	-528(%rbp), %r9
	addq	(%r12), %r9
	movl	-536(%rbp), %r8d
	movq	%r9, (%r12,%r15)
	addq	$8, %r15
	addl	%r8d, %r13d
	cmpq	$24, %r15
	jne	.L423
	movq	-464(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -432(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L449
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L450
	movq	-432(%rbp), %rsi
	movq	%r12, %r9
	movq	%r12, %rcx
	movq	%rax, %rdi
	movl	$3, %r8d
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L451
	subq	$8, %rsp
	movq	nodedbg_offset_ListNode_HandleWrap__prev___uintptr_t(%rip), %rax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	addq	nodedbg_offset_Environment_HandleWrapQueue__head___ListNode_HandleWrap(%rip), %rax
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	2096(%r14,%rax), %rax
	pushq	$0
	movl	$1, %r9d
	movq	%rax, -528(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L452
.L427:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L453
.L428:
	movq	-520(%rbp), %rcx
	leaq	-400(%rbp), %rbx
	movl	$32, %r8d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTV14TestHandleWrap(%rip), %rax
	leaq	-480(%rbp), %rdi
	movq	%rax, -400(%rbp)
	movq	-528(%rbp), %rax
	leaq	-512(%rbp), %rcx
	leaq	-504(%rbp), %r8
	addq	nodedbg_offset_ListNode_HandleWrap__next___uintptr_t(%rip), %rax
	leaq	.LC20(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	movq	(%rax), %rax
	subq	nodedbg_offset_HandleWrap__handle_wrap_queue___ListNode_HandleWrap(%rip), %rax
	movq	%rbx, -512(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -480(%rbp)
	je	.L454
.L429:
	movq	-472(%rbp), %r8
	testq	%r8, %r8
	je	.L432
.L456:
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L433
	movq	%r8, -520(%rbp)
	call	_ZdlPv@PLT
	movq	-520(%rbp), %r8
.L433:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L432:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L434
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -392(%rbp)
.L434:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-344(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, -400(%rbp)
	movq	-336(%rbp), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r15, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-552(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	leaq	-488(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -520(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-472(%rbp), %rax
	movq	-520(%rbp), %r9
	leaq	.LC7(%rip), %r8
	testq	%rax, %rax
	je	.L430
	movq	(%rax), %r8
.L430:
	leaq	-496(%rbp), %rdi
	movl	$156, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%r9, -528(%rbp)
	movq	%rdi, -520(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-528(%rbp), %r9
	movq	-520(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-520(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L429
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-472(%rbp), %r8
	testq	%r8, %r8
	jne	.L456
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L450:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L451:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%rax, -536(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-536(%rbp), %rdx
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%rdi, -536(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-536(%rbp), %rdi
	jmp	.L427
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8665:
	.size	_ZN36DebugSymbolsTest_HandleWrapList_Test8TestBodyEv, .-_ZN36DebugSymbolsTest_HandleWrapList_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN33DebugSymbolsTest_ReqWrapList_Test8TestBodyEv
	.type	_ZN33DebugSymbolsTest_ReqWrapList_Test8TestBodyEv, @function
_ZN33DebugSymbolsTest_ReqWrapList_Test8TestBodyEv:
.LFB8672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	leaq	-272(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -352(%rbp)
	movq	16(%rdi), %rsi
	movq	%rax, %rdi
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%rax, -360(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC16(%rip), %rdx
	leaq	.LC17(%rip), %rax
	movq	%rax, %xmm2
	movq	%rdx, %xmm0
	leaq	.LC18(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-200(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	leal	22(%rbx), %edi
	movl	$5, %ebx
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-208(%rbp), %rax
	movq	%rax, -344(%rbp)
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-344(%rbp), %rax
	movslq	%ebx, %r14
	movq	(%rax,%r15), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -336(%rbp)
	call	strlen@PLT
	movq	(%r12), %rdi
	movq	-336(%rbp), %rcx
	leaq	.LC19(%rip), %rdx
	leal	1(%rax), %r13d
	xorl	%eax, %eax
	movslq	%r13d, %rsi
	addq	%r14, %rdi
	addl	%r13d, %ebx
	call	snprintf@PLT
	movq	(%r12), %r9
	addq	%r14, %r9
	movq	%r9, (%r12,%r15)
	addq	$8, %r15
	cmpq	$24, %r15
	jne	.L458
	movq	-272(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -240(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L485
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L486
	movq	-240(%rbp), %rsi
	movq	%r12, %r9
	movq	%r12, %rcx
	movq	%rax, %rdi
	movl	$3, %r8d
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L487
	leaq	2112(%rax), %rbx
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	nodedbg_offset_ListNode_ReqWrap__prev___uintptr_t(%rip), %rax
	movl	$1, %r9d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	addq	%rbx, %rax
	addq	nodedbg_offset_Environment_ReqWrapQueue__head___ListNode_ReqWrapQueue(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-352(%rbp), %rax
	movq	16(%rax), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	-336(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L488
.L462:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L489
.L463:
	leaq	-144(%rbp), %rax
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rsi
	movsd	.LC22(%rip), %xmm0
	movq	-344(%rbp), %rdi
	movq	%rax, %xmm3
	movq	%rsi, %xmm1
	movl	$7, %ecx
	movq	%r14, %rsi
	punpcklqdq	%xmm3, %xmm1
	movq	%rax, -352(%rbp)
	movaps	%xmm1, -336(%rbp)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	movdqa	-336(%rbp), %xmm1
	movq	-352(%rbp), %rax
	movups	%xmm1, -152(%rbp)
	cmpb	$0, 1928(%r14)
	movq	%rax, -136(%rbp)
	je	.L490
	movq	2112(%r14), %rdx
	movq	%rax, 8(%rdx)
	movq	%rdx, -144(%rbp)
	movq	%rbx, -136(%rbp)
	movq	%rax, 2112(%r14)
	leaq	16+_ZTV11TestReqWrap(%rip), %rax
	movq	%rax, -208(%rbp)
	addq	$112, %rax
	movq	$0, -120(%rbp)
	movq	%rax, -152(%rbp)
	movq	2120(%r14), %rax
	movq	$0, -128(%rbp)
	cmpq	%rbx, %rax
	je	.L465
	.p2align 4,,10
	.p2align 3
.L466:
	movq	8(%rax), %rax
	cmpq	%rax, %rbx
	jne	.L466
.L465:
	movq	-344(%rbp), %rdx
	movq	-368(%rbp), %rax
	leaq	-288(%rbp), %rdi
	leaq	-320(%rbp), %rcx
	addq	nodedbg_offset_ListNode_ReqWrap__next___uintptr_t(%rip), %rax
	leaq	-312(%rbp), %r8
	leaq	.LC21(%rip), %rsi
	movq	(%rax), %rax
	movq	%rdx, -320(%rbp)
	leaq	.LC20(%rip), %rdx
	subq	nodedbg_offset_ReqWrap__req_wrap_queue___ListNode_ReqWrapQueue(%rip), %rax
	movq	%rax, -312(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -288(%rbp)
	je	.L491
.L467:
	movq	-280(%rbp), %r8
	testq	%r8, %r8
	je	.L470
.L494:
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L471
	movq	%r8, -336(%rbp)
	call	_ZdlPv@PLT
	movq	-336(%rbp), %r8
.L471:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L470:
	movq	-344(%rbp), %rax
	movq	%rax, -120(%rbp)
	leaq	16+_ZTVN4node7ReqWrapI8uv_req_sEE(%rip), %rax
	movq	%rax, -208(%rbp)
	addq	$112, %rax
	cmpq	$0, -200(%rbp)
	movq	%rax, -152(%rbp)
	je	.L492
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	movq	-344(%rbp), %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-360(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	leaq	-296(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -336(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-280(%rbp), %rax
	movq	-336(%rbp), %r9
	leaq	.LC7(%rip), %r8
	testq	%rax, %rax
	je	.L468
	movq	(%rax), %r8
.L468:
	leaq	-304(%rbp), %rdi
	movl	$193, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%r9, -352(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-352(%rbp), %r9
	movq	-336(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-336(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L467
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-280(%rbp), %r8
	testq	%r8, %r8
	jne	.L494
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L486:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L487:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L488:
	movq	%rdi, -336(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-336(%rbp), %rdi
	jmp	.L462
.L489:
	movq	%rax, -336(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-336(%rbp), %rdx
	jmp	.L463
.L490:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L492:
	leaq	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8672:
	.size	_ZN33DebugSymbolsTest_ReqWrapList_Test8TestBodyEv, .-_ZN33DebugSymbolsTest_ReqWrapList_Test8TestBodyEv
	.section	.text._ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB10586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movdqu	0(%r13), %xmm0
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	$0, (%rax)
	movups	%xmm0, 8(%rax)
	movq	16(%r13), %rax
	movq	16(%r12), %r13
	movq	%rax, 24(%r12)
	movq	%r13, %rax
	divq	%rsi
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	leaq	0(,%rdx,8), %r15
	testq	%rdi, %rdi
	je	.L496
	movq	(%rdi), %rax
	movq	%rdx, %r8
	movq	32(%rax), %rcx
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L497:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L496
	movq	32(%r9), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L496
	movq	%r9, %rax
.L499:
	cmpq	%rcx, %r13
	jne	.L497
	movq	8(%rax), %rcx
	cmpq	%rcx, 8(%r12)
	jne	.L497
	cmpq	16(%rax), %r13
	jne	.L497
	movq	(%rdi), %r14
	testq	%r14, %r14
	je	.L496
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r14, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L501
	movq	(%rbx), %r8
	movq	%r13, 32(%r12)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L511
.L535:
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%r15), %rax
	movq	%r12, (%rax)
.L512:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L533
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L534
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L504:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L506
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L508:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L509:
	testq	%rsi, %rsi
	je	.L506
.L507:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L508
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L515
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L507
	.p2align 4,,10
	.p2align 3
.L506:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L510
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L510:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%rbx)
	divq	%r14
	movq	%r8, (%rbx)
	movq	%r13, 32(%r12)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L535
.L511:
	movq	16(%rbx), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L513
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r12, (%r8,%rdx,8)
.L513:
	leaq	16(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L515:
	movq	%rdx, %rdi
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L533:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L504
.L534:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10586:
	.size	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_:
.LFB10672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L553
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L554
.L539:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L543
.L536:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L544
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L540:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L539
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L545
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L541:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L543:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L536
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L545:
	xorl	%edx, %edx
	jmp	.L541
	.cfi_endproc
.LFE10672:
	.size	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC2EPS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test8TestBodyEv
	.type	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test8TestBodyEv, @function
_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test8TestBodyEv:
.LFB8644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC19(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC16(%rip), %rcx
	leaq	.LC17(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	.LC18(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	leaq	-80(%rbp), %r10
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L556:
	movq	(%r10,%r15), %rcx
	movq	%r10, -256(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -248(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r9
	movq	-248(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	snprintf@PLT
	movq	-232(%rbp), %r9
	addq	(%r12), %r9
	movl	-240(%rbp), %r8d
	movq	%r9, (%r12,%r15)
	addq	$8, %r15
	movq	-256(%rbp), %r10
	addl	%r8d, %r13d
	cmpq	$24, %r15
	jne	.L556
	movq	-176(%rbp), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -144(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L595
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L596
	movq	-144(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r12, %r9
	movq	%r12, %rcx
	movl	$3, %r8d
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L597
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v814ObjectTemplate3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movl	$1, %esi
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L598
	movl	$32, %edi
	call	_Znwm@PLT
	movq	352(%r13), %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r13, %xmm2
	movq	$0, 24(%rbx)
	movq	%r15, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L599
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r13), %rax
	leaq	-112(%rbp), %r9
	leaq	2584(%r13), %rdi
	movq	%r9, %rsi
	movq	%r9, -240(%rbp)
	leaq	1(%rax), %rdx
	movq	%rdx, 2640(%r13)
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rdx
	movq	%rbx, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-240(%rbp), %r9
	testb	%dl, %dl
	je	.L600
	addq	$1, 2656(%r13)
	leaq	16+_ZTV15DummyBaseObject(%rip), %rax
	movq	%rbx, %rsi
	movq	%rax, (%rbx)
	leaq	-216(%rbp), %rdi
	movq	%r9, -240(%rbp)
	call	_ZN4node17BaseObjectPtrImplI15DummyBaseObjectLb0EEC1EPS1_
	movq	-216(%rbp), %rbx
	movq	-240(%rbp), %r9
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L601
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L578
	movb	$1, 9(%rax)
	leaq	-208(%rbp), %rcx
	leaq	8(%rbx), %rax
	movq	%r9, %rdi
	addq	nodedbg_offset_BaseObject__persistent_handle___v8_Persistent_v8_Object(%rip), %rbx
	leaq	-200(%rbp), %r8
	leaq	.LC20(%rip), %rdx
	movq	%rax, -208(%rbp)
	leaq	.LC21(%rip), %rsi
	movq	%rbx, -200(%rbp)
	call	_ZN7testing8internal11CmpHelperEQImmEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -112(%rbp)
	je	.L602
.L564:
	movq	-104(%rbp), %r15
	testq	%r15, %r15
	je	.L567
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L568
	call	_ZdlPv@PLT
.L568:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L567:
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L570
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L603
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L604
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L570
	cmpb	$0, 9(%rdx)
	jne	.L605
	cmpb	$0, 8(%rdx)
	je	.L570
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L570
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%r13, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-232(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-264(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L606
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L581
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L563:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
.L578:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	leaq	-184(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-104(%rbp), %rax
	leaq	.LC7(%rip), %r8
	testq	%rax, %rax
	je	.L565
	movq	(%rax), %r8
.L565:
	leaq	-192(%rbp), %r15
	movl	$102, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L564
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L595:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L596:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L597:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L598:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L599:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L605:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L603:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	xorl	%edx, %edx
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L604:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L606:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8644:
	.size	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test8TestBodyEv, .-_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test8TestBodyEv
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB10715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L608
	testq	%rsi, %rsi
	je	.L624
.L608:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L625
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L611
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L612:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L626
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L612
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L610:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L612
.L624:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L626:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10715:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"ContextEmbedderEnvironmentIndex"
	.section	.rodata.str1.1
.LC24:
	.string	"DebugSymbolsTest"
.LC25:
	.string	"ExternalStringDataOffset"
.LC26:
	.string	"BaseObjectPersistentHandle"
.LC27:
	.string	"EnvironmentHandleWrapQueue"
.LC28:
	.string	"EnvironmentReqWrapQueue"
.LC29:
	.string	"HandleWrapList"
.LC30:
	.string	"ReqWrapList"
	.section	.text.startup
	.p2align 4
	.type	_Z41__static_initialization_and_destruction_0ii.constprop.0, @function
_Z41__static_initialization_and_destruction_0ii.constprop.0:
.LFB11982:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L658
.L628:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L659
.L629:
	leaq	-128(%rbp), %r15
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	leaq	_ZN15NodeTestFixture16TearDownTestCaseEv(%rip), %r14
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC24(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r9
	movq	%r12, %r8
	leaq	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E(%rip), %r9
	leaq	.LC23(%rip), %rsi
	pushq	%r13
	leaq	_ZN15NodeTestFixture13SetUpTestCaseEv(%rip), %r13
	pushq	%r14
	pushq	%r13
	movl	$65, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L630
	call	_ZdlPv@PLT
.L630:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L631
	call	_ZdlPv@PLT
.L631:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L660
.L632:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L661
.L633:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC24(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r8
	leaq	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC25(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$71, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN46DebugSymbolsTest_ExternalStringDataOffset_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L634
	call	_ZdlPv@PLT
.L634:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L635
	call	_ZdlPv@PLT
.L635:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L662
.L636:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L663
.L637:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rdi
	movq	%r12, %r8
	leaq	.LC24(%rip), %rdi
	leaq	.LC26(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$86, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L638
	call	_ZdlPv@PLT
.L638:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L639
	call	_ZdlPv@PLT
.L639:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L664
.L640:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L665
.L641:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC24(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rsi
	leaq	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC27(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$106, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L666
.L644:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L667
.L645:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC24(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E(%rip), %r9
	xorl	%edx, %edx
	pushq	%rcx
	movq	%r12, %r8
	xorl	%ecx, %ecx
	leaq	.LC28(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$117, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L646
	call	_ZdlPv@PLT
.L646:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L647
	call	_ZdlPv@PLT
.L647:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L668
.L648:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L669
.L649:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC24(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	pushq	%rdx
	movq	%r12, %r8
	xorl	%edx, %edx
	leaq	.LC29(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$128, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN36DebugSymbolsTest_HandleWrapList_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L670
.L652:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L671
.L653:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC24(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC30(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$161, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN33DebugSymbolsTest_ReqWrapList_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L672
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L659:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L629
.L661:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L633
.L660:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L632
.L663:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L637
.L662:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L636
.L669:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L649
.L668:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L648
.L671:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L653
.L670:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L652
.L665:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L641
.L664:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L640
.L667:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L645
.L666:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L644
.L658:
	call	_ZN7testing8internal16SuiteApiResolverI16DebugSymbolsTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L628
.L672:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11982:
	.size	_Z41__static_initialization_and_destruction_0ii.constprop.0, .-_Z41__static_initialization_and_destruction_0ii.constprop.0
	.p2align 4
	.type	_GLOBAL__sub_I__ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E, @function
_GLOBAL__sub_I__ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E:
.LFB11824:
	.cfi_startproc
	endbr64
	jmp	_Z41__static_initialization_and_destruction_0ii.constprop.0
	.cfi_endproc
.LFE11824:
	.size	_GLOBAL__sub_I__ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E, .-_GLOBAL__sub_I__ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E
	.weak	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E
	.section	.bss._ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E,"awG",@nobits,_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E,comdat
	.type	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E, @gnu_unique_object
	.size	_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E, 1
_ZN7testing8internal12TypeIdHelperI16DebugSymbolsTestE6dummy_E:
	.zero	1
	.weak	_ZTVN4node11ReqWrapBaseE
	.section	.data.rel.ro._ZTVN4node11ReqWrapBaseE,"awG",@progbits,_ZTVN4node11ReqWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11ReqWrapBaseE, @object
	.size	_ZTVN4node11ReqWrapBaseE, 48
_ZTVN4node11ReqWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTV15NodeTestFixture
	.section	.data.rel.ro._ZTV15NodeTestFixture,"awG",@progbits,_ZTV15NodeTestFixture,comdat
	.align 8
	.type	_ZTV15NodeTestFixture, @object
	.size	_ZTV15NodeTestFixture, 64
_ZTV15NodeTestFixture:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTV14TestHandleWrap
	.section	.data.rel.ro._ZTV14TestHandleWrap,"awG",@progbits,_ZTV14TestHandleWrap,comdat
	.align 8
	.type	_ZTV14TestHandleWrap, @object
	.size	_ZTV14TestHandleWrap, 112
_ZTV14TestHandleWrap:
	.quad	0
	.quad	0
	.quad	_ZN14TestHandleWrapD1Ev
	.quad	_ZN14TestHandleWrapD0Ev
	.quad	_ZNK14TestHandleWrap10MemoryInfoEPN4node13MemoryTrackerE
	.quad	_ZNK14TestHandleWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK14TestHandleWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.weak	_ZTVN4node7ReqWrapI8uv_req_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI8uv_req_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI8uv_req_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI8uv_req_sEE, @object
	.size	_ZTVN4node7ReqWrapI8uv_req_sEE, 160
_ZTVN4node7ReqWrapI8uv_req_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI8uv_req_sE6CancelEv
	.quad	_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI8uv_req_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.weak	_ZTV11TestReqWrap
	.section	.data.rel.ro._ZTV11TestReqWrap,"awG",@progbits,_ZTV11TestReqWrap,comdat
	.align 8
	.type	_ZTV11TestReqWrap, @object
	.size	_ZTV11TestReqWrap, 160
_ZTV11TestReqWrap:
	.quad	0
	.quad	0
	.quad	_ZN11TestReqWrapD1Ev
	.quad	_ZN11TestReqWrapD0Ev
	.quad	_ZNK11TestReqWrap10MemoryInfoEPN4node13MemoryTrackerE
	.quad	_ZNK11TestReqWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK11TestReqWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI8uv_req_sE6CancelEv
	.quad	_ZN4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N11TestReqWrapD1Ev
	.quad	_ZThn56_N11TestReqWrapD0Ev
	.quad	_ZThn56_N4node7ReqWrapI8uv_req_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI8uv_req_sE12GetAsyncWrapEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestE10CreateTestEv
	.weak	_ZTV53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test
	.section	.data.rel.ro.local._ZTV53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test,"awG",@progbits,_ZTV53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test,comdat
	.align 8
	.type	_ZTV53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test, @object
	.size	_ZTV53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test, 64
_ZTV53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test:
	.quad	0
	.quad	0
	.quad	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD1Ev
	.quad	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI46DebugSymbolsTest_ExternalStringDataOffset_TestE10CreateTestEv
	.weak	_ZTV46DebugSymbolsTest_ExternalStringDataOffset_Test
	.section	.data.rel.ro.local._ZTV46DebugSymbolsTest_ExternalStringDataOffset_Test,"awG",@progbits,_ZTV46DebugSymbolsTest_ExternalStringDataOffset_Test,comdat
	.align 8
	.type	_ZTV46DebugSymbolsTest_ExternalStringDataOffset_Test, @object
	.size	_ZTV46DebugSymbolsTest_ExternalStringDataOffset_Test, 64
_ZTV46DebugSymbolsTest_ExternalStringDataOffset_Test:
	.quad	0
	.quad	0
	.quad	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD1Ev
	.quad	_ZN46DebugSymbolsTest_ExternalStringDataOffset_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTV15DummyBaseObject
	.section	.data.rel.ro._ZTV15DummyBaseObject,"awG",@progbits,_ZTV15DummyBaseObject,comdat
	.align 8
	.type	_ZTV15DummyBaseObject, @object
	.size	_ZTV15DummyBaseObject, 88
_ZTV15DummyBaseObject:
	.quad	0
	.quad	0
	.quad	_ZN15DummyBaseObjectD1Ev
	.quad	_ZN15DummyBaseObjectD0Ev
	.quad	_ZNK15DummyBaseObject10MemoryInfoEPN4node13MemoryTrackerE
	.quad	_ZNK15DummyBaseObject14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK15DummyBaseObject8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_BaseObjectPersistentHandle_TestE10CreateTestEv
	.weak	_ZTV48DebugSymbolsTest_BaseObjectPersistentHandle_Test
	.section	.data.rel.ro.local._ZTV48DebugSymbolsTest_BaseObjectPersistentHandle_Test,"awG",@progbits,_ZTV48DebugSymbolsTest_BaseObjectPersistentHandle_Test,comdat
	.align 8
	.type	_ZTV48DebugSymbolsTest_BaseObjectPersistentHandle_Test, @object
	.size	_ZTV48DebugSymbolsTest_BaseObjectPersistentHandle_Test, 64
_ZTV48DebugSymbolsTest_BaseObjectPersistentHandle_Test:
	.quad	0
	.quad	0
	.quad	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD1Ev
	.quad	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestE10CreateTestEv
	.weak	_ZTV48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test
	.section	.data.rel.ro.local._ZTV48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test,"awG",@progbits,_ZTV48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test,comdat
	.align 8
	.type	_ZTV48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test, @object
	.size	_ZTV48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test, 64
_ZTV48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test:
	.quad	0
	.quad	0
	.quad	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD1Ev
	.quad	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI45DebugSymbolsTest_EnvironmentReqWrapQueue_TestE10CreateTestEv
	.weak	_ZTV45DebugSymbolsTest_EnvironmentReqWrapQueue_Test
	.section	.data.rel.ro.local._ZTV45DebugSymbolsTest_EnvironmentReqWrapQueue_Test,"awG",@progbits,_ZTV45DebugSymbolsTest_EnvironmentReqWrapQueue_Test,comdat
	.align 8
	.type	_ZTV45DebugSymbolsTest_EnvironmentReqWrapQueue_Test, @object
	.size	_ZTV45DebugSymbolsTest_EnvironmentReqWrapQueue_Test, 64
_ZTV45DebugSymbolsTest_EnvironmentReqWrapQueue_Test:
	.quad	0
	.quad	0
	.quad	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD1Ev
	.quad	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI36DebugSymbolsTest_HandleWrapList_TestE10CreateTestEv
	.weak	_ZTV36DebugSymbolsTest_HandleWrapList_Test
	.section	.data.rel.ro.local._ZTV36DebugSymbolsTest_HandleWrapList_Test,"awG",@progbits,_ZTV36DebugSymbolsTest_HandleWrapList_Test,comdat
	.align 8
	.type	_ZTV36DebugSymbolsTest_HandleWrapList_Test, @object
	.size	_ZTV36DebugSymbolsTest_HandleWrapList_Test, 64
_ZTV36DebugSymbolsTest_HandleWrapList_Test:
	.quad	0
	.quad	0
	.quad	_ZN36DebugSymbolsTest_HandleWrapList_TestD1Ev
	.quad	_ZN36DebugSymbolsTest_HandleWrapList_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN36DebugSymbolsTest_HandleWrapList_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33DebugSymbolsTest_ReqWrapList_TestE10CreateTestEv
	.weak	_ZTV33DebugSymbolsTest_ReqWrapList_Test
	.section	.data.rel.ro.local._ZTV33DebugSymbolsTest_ReqWrapList_Test,"awG",@progbits,_ZTV33DebugSymbolsTest_ReqWrapList_Test,comdat
	.align 8
	.type	_ZTV33DebugSymbolsTest_ReqWrapList_Test, @object
	.size	_ZTV33DebugSymbolsTest_ReqWrapList_Test, 64
_ZTV33DebugSymbolsTest_ReqWrapList_Test:
	.quad	0
	.quad	0
	.quad	_ZN33DebugSymbolsTest_ReqWrapList_TestD1Ev
	.quad	_ZN33DebugSymbolsTest_ReqWrapList_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN33DebugSymbolsTest_ReqWrapList_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args
	.section	.rodata.str1.1
.LC31:
	.string	"../src/req_wrap-inl.h:28"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"(false) == (persistent().IsEmpty())"
	.align 8
.LC33:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_req_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI8uv_req_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI8uv_req_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI8uv_req_sED4EvE4args, 24
_ZZN4node7ReqWrapI8uv_req_sED4EvE4args:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.globl	_ZN33DebugSymbolsTest_ReqWrapList_Test10test_info_E
	.bss
	.align 8
	.type	_ZN33DebugSymbolsTest_ReqWrapList_Test10test_info_E, @object
	.size	_ZN33DebugSymbolsTest_ReqWrapList_Test10test_info_E, 8
_ZN33DebugSymbolsTest_ReqWrapList_Test10test_info_E:
	.zero	8
	.globl	_ZN36DebugSymbolsTest_HandleWrapList_Test10test_info_E
	.align 8
	.type	_ZN36DebugSymbolsTest_HandleWrapList_Test10test_info_E, @object
	.size	_ZN36DebugSymbolsTest_HandleWrapList_Test10test_info_E, 8
_ZN36DebugSymbolsTest_HandleWrapList_Test10test_info_E:
	.zero	8
	.globl	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test10test_info_E
	.align 8
	.type	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test10test_info_E, @object
	.size	_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test10test_info_E, 8
_ZN45DebugSymbolsTest_EnvironmentReqWrapQueue_Test10test_info_E:
	.zero	8
	.globl	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test10test_info_E
	.align 8
	.type	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test10test_info_E, @object
	.size	_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test10test_info_E, 8
_ZN48DebugSymbolsTest_EnvironmentHandleWrapQueue_Test10test_info_E:
	.zero	8
	.globl	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test10test_info_E
	.align 8
	.type	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test10test_info_E, @object
	.size	_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test10test_info_E, 8
_ZN48DebugSymbolsTest_BaseObjectPersistentHandle_Test10test_info_E:
	.zero	8
	.globl	_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test10test_info_E
	.align 8
	.type	_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test10test_info_E, @object
	.size	_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test10test_info_E, 8
_ZN46DebugSymbolsTest_ExternalStringDataOffset_Test10test_info_E:
	.zero	8
	.globl	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E
	.align 8
	.type	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E, @object
	.size	_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E, 8
_ZN53DebugSymbolsTest_ContextEmbedderEnvironmentIndex_Test10test_info_E:
	.zero	8
	.weak	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC34:
	.string	"../src/req_wrap-inl.h:13"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC36:
	.string	"node::ReqWrapBase::ReqWrapBase(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, 24
_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC37:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC39:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC40:
	.string	"../src/base_object-inl.h:195"
.LC41:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC39
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC42:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC44:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC45:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC47:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC48:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC50:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC51:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC53:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC54:
	.string	"../src/base_object-inl.h:44"
.LC55:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC53
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"../test/cctest/node_test_fixture.h:140"
	.section	.rodata.str1.1
.LC57:
	.string	"(nullptr) != (environment_)"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"EnvironmentTestFixture::Env::Env(const v8::HandleScope&, const Argv&)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"../test/cctest/node_test_fixture.h:135"
	.section	.rodata.str1.1
.LC60:
	.string	"(nullptr) != (isolate_data_)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC58
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../test/cctest/node_test_fixture.h:129"
	.section	.rodata.str1.1
.LC62:
	.string	"!context_.IsEmpty()"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC58
	.weak	_ZZN15NodeTestFixture5SetUpEvE4args
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"../test/cctest/node_test_fixture.h:108"
	.section	.rodata.str1.1
.LC64:
	.string	"(isolate_) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"virtual void NodeTestFixture::SetUp()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture5SetUpEvE4args,"awG",@progbits,_ZZN15NodeTestFixture5SetUpEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture5SetUpEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture5SetUpEvE4args, 24
_ZZN15NodeTestFixture5SetUpEvE4args:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.weak	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"../test/cctest/node_test_fixture.h:101"
	.align 8
.LC67:
	.string	"(0) == (uv_loop_close(&current_loop))"
	.align 8
.LC68:
	.string	"static void NodeTestFixture::TearDownTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture16TearDownTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture16TearDownTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, 24
_ZZN15NodeTestFixture16TearDownTestCaseEvE4args:
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.weak	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"../test/cctest/node_test_fixture.h:87"
	.align 8
.LC70:
	.string	"(0) == (uv_loop_init(&current_loop))"
	.align 8
.LC71:
	.string	"static void NodeTestFixture::SetUpTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture13SetUpTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture13SetUpTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, 24
_ZZN15NodeTestFixture13SetUpTestCaseEvE4args:
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC72:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC74:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC75:
	.string	"../src/tracing/agent.h:91"
.LC76:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC11:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC12:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC13:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC22:
	.long	0
	.long	-1074790400
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
