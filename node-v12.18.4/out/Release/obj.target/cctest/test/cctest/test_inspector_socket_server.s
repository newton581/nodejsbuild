	.file	"test_inspector_socket_server.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB6154:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6154:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s, @function
_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s:
.LFB6279:
	.cfi_startproc
	endbr64
	movb	$1, -8(%rdi)
	ret
	.cfi_endproc
.LFE6279:
	.size	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s, .-_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s
	.p2align 4
	.type	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s, @function
_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s:
.LFB6281:
	.cfi_startproc
	endbr64
	movb	$1, -7(%rdi)
	ret
	.cfi_endproc
.LFE6281:
	.size	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s, .-_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegate12AssignServerEPN4node9inspector21InspectorSocketServerE, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegate12AssignServerEPN4node9inspector21InspectorSocketServerE:
.LFB6344:
	.cfi_startproc
	endbr64
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE6344:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegate12AssignServerEPN4node9inspector21InspectorSocketServerE, .-_ZN12_GLOBAL__N_124TestSocketServerDelegate12AssignServerEPN4node9inspector21InspectorSocketServerE
	.section	.text._ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED2Ev:
.LFB8591:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8591:
	.size	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED1Ev,_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED2Ev:
.LFB8599:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8599:
	.size	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED1Ev,_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED2Ev:
.LFB8607:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8607:
	.size	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED1Ev,_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED2Ev:
.LFB8615:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8615:
	.size	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED1Ev,_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED2Ev:
.LFB8623:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8623:
	.size	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED1Ev,_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED2Ev:
.LFB8631:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8631:
	.size	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED1Ev,_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED2Ev:
.LFB8639:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8639:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED1Ev,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED2Ev:
.LFB8647:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8647:
	.size	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED1Ev,_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED2Ev:
.LFB8655:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8655:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED1Ev,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED2Ev:
.LFB8663:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8663:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED1Ev,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED0Ev:
.LFB8665:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8665:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED0Ev:
.LFB8657:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8657:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED0Ev:
.LFB8649:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8649:
	.size	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED0Ev:
.LFB8641:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8641:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED0Ev:
.LFB8633:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8633:
	.size	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED0Ev:
.LFB8625:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8625:
	.size	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED0Ev:
.LFB8617:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8617:
	.size	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED0Ev:
.LFB8609:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8609:
	.size	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED0Ev:
.LFB8601:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8601:
	.size	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED0Ev:
.LFB8593:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8593:
	.size	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEi, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEi:
.LFB6347:
	.cfi_startproc
	endbr64
	cmpl	%esi, 48(%rdi)
	jne	.L31
	movq	8(%rdi), %rax
	addl	$1, 4(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE6347:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEi, .-_ZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEi
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t, @function
_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t:
.LFB6303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znam@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	popq	%rbx
	movq	%rax, (%r12)
	movq	%rdx, 8(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6303:
	.size	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t, .-_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t
	.section	.text._ZN42InspectorSocketServerTest_BindsToIpV6_TestD2Ev,"axG",@progbits,_ZN42InspectorSocketServerTest_BindsToIpV6_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD2Ev
	.type	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD2Ev, @function
_ZN42InspectorSocketServerTest_BindsToIpV6_TestD2Ev:
.LFB8587:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8587:
	.size	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD2Ev, .-_ZN42InspectorSocketServerTest_BindsToIpV6_TestD2Ev
	.weak	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD1Ev
	.set	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD1Ev,_ZN42InspectorSocketServerTest_BindsToIpV6_TestD2Ev
	.section	.text._ZN42InspectorSocketServerTest_BindsToIpV6_TestD0Ev,"axG",@progbits,_ZN42InspectorSocketServerTest_BindsToIpV6_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD0Ev
	.type	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD0Ev, @function
_ZN42InspectorSocketServerTest_BindsToIpV6_TestD0Ev:
.LFB8589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8589:
	.size	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD0Ev, .-_ZN42InspectorSocketServerTest_BindsToIpV6_TestD0Ev
	.section	.text._ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD2Ev,"axG",@progbits,_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD2Ev
	.type	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD2Ev, @function
_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD2Ev:
.LFB8595:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8595:
	.size	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD2Ev, .-_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD2Ev
	.weak	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD1Ev
	.set	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD1Ev,_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD2Ev
	.section	.text._ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD0Ev,"axG",@progbits,_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD0Ev
	.type	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD0Ev, @function
_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD0Ev:
.LFB8597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8597:
	.size	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD0Ev, .-_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD0Ev
	.section	.text._ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD2Ev,"axG",@progbits,_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD2Ev
	.type	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD2Ev, @function
_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD2Ev:
.LFB8603:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8603:
	.size	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD2Ev, .-_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD2Ev
	.weak	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD1Ev
	.set	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD1Ev,_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD2Ev
	.section	.text._ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD0Ev,"axG",@progbits,_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD0Ev
	.type	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD0Ev, @function
_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD0Ev:
.LFB8605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8605:
	.size	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD0Ev, .-_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD0Ev
	.section	.text._ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD2Ev,"axG",@progbits,_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD2Ev
	.type	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD2Ev, @function
_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD2Ev:
.LFB8611:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8611:
	.size	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD2Ev, .-_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD2Ev
	.weak	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD1Ev
	.set	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD1Ev,_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD2Ev
	.section	.text._ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD0Ev,"axG",@progbits,_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD0Ev
	.type	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD0Ev, @function
_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD0Ev:
.LFB8613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8613:
	.size	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD0Ev, .-_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD0Ev
	.section	.text._ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD2Ev,"axG",@progbits,_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD2Ev
	.type	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD2Ev, @function
_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD2Ev:
.LFB8619:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8619:
	.size	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD2Ev, .-_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD2Ev
	.weak	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD1Ev
	.set	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD1Ev,_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD2Ev
	.section	.text._ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD0Ev,"axG",@progbits,_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD0Ev
	.type	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD0Ev, @function
_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD0Ev:
.LFB8621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8621:
	.size	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD0Ev, .-_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD0Ev
	.section	.text._ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD2Ev,"axG",@progbits,_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD2Ev
	.type	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD2Ev, @function
_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD2Ev:
.LFB8627:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8627:
	.size	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD2Ev, .-_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD2Ev
	.weak	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD1Ev
	.set	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD1Ev,_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD2Ev
	.section	.text._ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD0Ev,"axG",@progbits,_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD0Ev
	.type	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD0Ev, @function
_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD0Ev:
.LFB8629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8629:
	.size	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD0Ev, .-_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD0Ev
	.section	.text._ZN48InspectorSocketServerTest_ServerCannotStart_TestD2Ev,"axG",@progbits,_ZN48InspectorSocketServerTest_ServerCannotStart_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD2Ev
	.type	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD2Ev, @function
_ZN48InspectorSocketServerTest_ServerCannotStart_TestD2Ev:
.LFB8635:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8635:
	.size	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD2Ev, .-_ZN48InspectorSocketServerTest_ServerCannotStart_TestD2Ev
	.weak	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD1Ev
	.set	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD1Ev,_ZN48InspectorSocketServerTest_ServerCannotStart_TestD2Ev
	.section	.text._ZN48InspectorSocketServerTest_ServerCannotStart_TestD0Ev,"axG",@progbits,_ZN48InspectorSocketServerTest_ServerCannotStart_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD0Ev
	.type	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD0Ev, @function
_ZN48InspectorSocketServerTest_ServerCannotStart_TestD0Ev:
.LFB8637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8637:
	.size	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD0Ev, .-_ZN48InspectorSocketServerTest_ServerCannotStart_TestD0Ev
	.section	.text._ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD2Ev,"axG",@progbits,_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD2Ev
	.type	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD2Ev, @function
_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD2Ev:
.LFB8643:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8643:
	.size	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD2Ev, .-_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD2Ev
	.weak	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD1Ev
	.set	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD1Ev,_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD2Ev
	.section	.text._ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD0Ev,"axG",@progbits,_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD0Ev
	.type	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD0Ev, @function
_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD0Ev:
.LFB8645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8645:
	.size	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD0Ev, .-_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD0Ev
	.section	.text._ZN48InspectorSocketServerTest_ServerDoesNothing_TestD2Ev,"axG",@progbits,_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD2Ev
	.type	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD2Ev, @function
_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD2Ev:
.LFB8651:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8651:
	.size	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD2Ev, .-_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD2Ev
	.weak	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD1Ev
	.set	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD1Ev,_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD2Ev
	.section	.text._ZN48InspectorSocketServerTest_ServerDoesNothing_TestD0Ev,"axG",@progbits,_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD0Ev
	.type	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD0Ev, @function
_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD0Ev:
.LFB8653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8653:
	.size	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD0Ev, .-_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD0Ev
	.section	.text._ZN48InspectorSocketServerTest_InspectorSessions_TestD2Ev,"axG",@progbits,_ZN48InspectorSocketServerTest_InspectorSessions_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48InspectorSocketServerTest_InspectorSessions_TestD2Ev
	.type	_ZN48InspectorSocketServerTest_InspectorSessions_TestD2Ev, @function
_ZN48InspectorSocketServerTest_InspectorSessions_TestD2Ev:
.LFB8659:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE8659:
	.size	_ZN48InspectorSocketServerTest_InspectorSessions_TestD2Ev, .-_ZN48InspectorSocketServerTest_InspectorSessions_TestD2Ev
	.weak	_ZN48InspectorSocketServerTest_InspectorSessions_TestD1Ev
	.set	_ZN48InspectorSocketServerTest_InspectorSessions_TestD1Ev,_ZN48InspectorSocketServerTest_InspectorSessions_TestD2Ev
	.section	.text._ZN48InspectorSocketServerTest_InspectorSessions_TestD0Ev,"axG",@progbits,_ZN48InspectorSocketServerTest_InspectorSessions_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN48InspectorSocketServerTest_InspectorSessions_TestD0Ev
	.type	_ZN48InspectorSocketServerTest_InspectorSessions_TestD0Ev, @function
_ZN48InspectorSocketServerTest_InspectorSessions_TestD0Ev:
.LFB8661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8661:
	.size	_ZN48InspectorSocketServerTest_InspectorSessions_TestD0Ev, .-_ZN48InspectorSocketServerTest_InspectorSessions_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv:
.LFB8736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV48InspectorSocketServerTest_InspectorSessions_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8736:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv:
.LFB8735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV48InspectorSocketServerTest_ServerDoesNothing_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8735:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv:
.LFB8734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV51InspectorSocketServerTest_ServerWithoutTargets_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8734:
	.size	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv:
.LFB8733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV48InspectorSocketServerTest_ServerCannotStart_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8733:
	.size	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv:
.LFB8732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8732:
	.size	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv:
.LFB8731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV59InspectorSocketServerTest_ClosingConnectionReportsDone_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8731:
	.size	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv:
.LFB8730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV55InspectorSocketServerTest_ClosingSocketReportsDone_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8730:
	.size	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv:
.LFB8729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV60InspectorSocketServerTest_TerminatingSessionReportsDone_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8729:
	.size	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv:
.LFB8728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV54InspectorSocketServerTest_FailsToBindToNodejsHost_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8728:
	.size	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv:
.LFB8727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV42InspectorSocketServerTest_BindsToIpV6_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8727:
	.size	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0:
.LFB8842:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8842:
	.size	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0:
.LFB8843:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8843:
	.size	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.1
.LC4:
	.string	"vector::_M_range_insert"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	%esi, 48(%rdi)
	jne	.L118
	movq	8(%rdi), %rbx
	movq	8(%rdx), %r12
	movq	(%rdx), %r9
	movq	32(%rbx), %r13
	testq	%r12, %r12
	je	.L92
	movq	40(%rbx), %rax
	subq	%r13, %rax
	cmpq	%rax, %r12
	ja	.L95
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
	addq	%r12, 32(%rbx)
.L92:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	24(%rbx), %r15
	movq	%r13, %rdx
	movabsq	$9223372036854775807, %rcx
	movq	%rcx, %rax
	subq	%r15, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L119
	cmpq	%rdx, %r12
	movq	%rdx, %rax
	cmovnb	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, %r14
	jc	.L107
	testq	%rax, %rax
	js	.L107
	jne	.L99
	movq	$0, -56(%rbp)
	xorl	%r14d, %r14d
.L105:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rdx, %rdx
	jne	.L120
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	32(%rbx), %rdx
	subq	%r13, %rdx
	jne	.L121
.L103:
	testq	%r15, %r15
	jne	.L102
.L104:
	movq	-56(%rbp), %xmm0
	movq	%r14, 40(%rbx)
	movhps	-64(%rbp), %xmm0
	movups	%xmm0, 24(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	%rcx, %r14
.L99:
	movq	%r14, %rdi
	movq	%r9, -64(%rbp)
	call	_Znwm@PLT
	movq	24(%rbx), %r15
	movq	%r13, %rdx
	movq	-64(%rbp), %r9
	movq	%rax, -56(%rbp)
	addq	%rax, %r14
	subq	%r15, %rdx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-56(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r11, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r11
	movq	%r12, %rdx
	movq	%r9, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	32(%rbx), %rdx
	movq	-64(%rbp), %rax
	subq	%r13, %rdx
	leaq	(%rax,%rdx), %r12
	jne	.L101
	movq	%r12, -64(%rbp)
.L102:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	_ZZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-64(%rbp), %rax
	leaq	(%rax,%rdx), %r12
.L101:
	movq	-64(%rbp), %rdi
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%r12, -64(%rbp)
	jmp	.L103
.L119:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6346:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0:
.LFB8947:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L123
	testq	%rsi, %rsi
	je	.L139
.L123:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L140
	movq	(%rbx), %rdx
	movq	%rdx, %rdi
	cmpq	$1, %r12
	jne	.L126
	movzbl	0(%r13), %eax
	movb	%al, (%rdx)
	movq	(%rbx), %rdx
.L127:
	movq	-48(%rbp), %rax
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L127
	jmp	.L125
.L140:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L125:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	(%rbx), %rdx
	jmp	.L127
.L139:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8947:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegateD2Ev, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegateD2Ev:
.LFB6341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_124TestSocketServerDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %r13
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	movb	$1, 8(%rax)
	cmpq	%r12, %r13
	je	.L143
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L147
.L145:
	movq	16(%rbx), %r12
.L143:
	testq	%r12, %r12
	je	.L142
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L147
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6341:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegateD2Ev, .-_ZN12_GLOBAL__N_124TestSocketServerDelegateD2Ev
	.set	_ZN12_GLOBAL__N_124TestSocketServerDelegateD1Ev,_ZN12_GLOBAL__N_124TestSocketServerDelegateD2Ev
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t, @function
_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t:
.LFB6309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-16(%rdi), %rbx
	subq	$56, %rsp
	movq	(%rdx), %r13
	cmpq	$-4095, %rsi
	jne	.L151
	movb	$1, 1(%rbx)
.L152:
	testq	%r13, %r13
	je	.L150
.L183:
	addq	$56, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L152
	movq	568(%rbx), %r14
	movq	576(%rbx), %rax
	movq	%rdx, %r15
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L154
	testq	%rsi, %rsi
	jne	.L182
.L155:
	addq	%r14, %r12
	movq	%r12, 568(%rbx)
	testq	%r13, %r13
	jne	.L183
.L150:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	560(%rbx), %r9
	movq	%r14, %rdx
	movabsq	$9223372036854775807, %rsi
	movq	%rsi, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L184
	cmpq	%rdx, %r12
	movq	%rdx, %rax
	cmovnb	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, %rcx
	jc	.L168
	testq	%rax, %rax
	js	.L168
	jne	.L159
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%rsi, %rcx
.L159:
	movq	%rcx, %rdi
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	560(%rbx), %r9
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %rcx
	subq	%r9, %rdx
	movq	%rcx, -64(%rbp)
.L166:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L185
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	memcpy@PLT
	movq	568(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r13
	jne	.L161
.L163:
	testq	%r9, %r9
	jne	.L162
.L164:
	movq	-56(%rbp), %xmm0
	movq	%r13, %xmm1
	movq	-64(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 576(%rbx)
	movups	%xmm0, 560(%rbx)
	movq	(%r15), %r13
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r11, -88(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	568(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r13
	jne	.L161
.L162:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%rsi, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	memmove@PLT
	movq	568(%rbx), %r14
	movq	(%r15), %r13
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	jmp	.L163
.L184:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6309:
	.size	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t, .-_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegateD0Ev, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegateD0Ev:
.LFB6343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_124TestSocketServerDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %rbx
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	movb	$1, 8(%rax)
	cmpq	%r12, %rbx
	je	.L187
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L188
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L191
.L189:
	movq	16(%r13), %r12
.L187:
	testq	%r12, %r12
	je	.L192
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L192:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$56, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L191
	jmp	.L189
	.cfi_endproc
.LFE6343:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegateD0Ev, .-_ZN12_GLOBAL__N_124TestSocketServerDelegateD0Ev
	.section	.rodata.str1.1
.LC6:
	.string	"basic_string::append"
.LC7:
	.string	"GET /"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	" HTTP/1.1\r\nHost: localhost:9229\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Key: aaa==\r\nSec-WebSocket-Version: 13\r\n\r\n"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6361:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$4611686018427387903, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	leaq	5(%rax), %rsi
	movb	$0, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	%r15, %rax
	subq	-88(%rbp), %rax
	cmpq	$4, %rax
	jbe	.L199
	movl	$5, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	subq	-88(%rbp), %r15
	cmpq	$128, %r15
	jbe	.L199
	movl	$129, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L205
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L201:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r13, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L201
.L199:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6361:
	.size	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC9:
	.string	"file://"
.LC10:
	.string	"/script.js"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$4611686018427387903, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	leaq	7(%rax), %rsi
	movb	$0, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	%r15, %rax
	subq	-88(%rbp), %rax
	cmpq	$6, %rax
	jbe	.L209
	movl	$7, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	subq	-88(%rbp), %r15
	cmpq	$9, %r15
	jbe	.L209
	movl	$10, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L215
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L211:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r13, %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L211
.L209:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6350:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB8902:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L227
	cmpq	$1, %rax
	jne	.L220
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L221:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L220:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L221
	jmp	.L219
.L227:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L219:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L221
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8902:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"../test/cctest/test_inspector_socket_server.cc"
	.section	.rodata.str1.1
.LC12:
	.string	"InspectorSessions"
.LC13:
	.string	"InspectorSocketServerTest"
.LC14:
	.string	"ServerDoesNothing"
.LC15:
	.string	"ServerWithoutTargets"
.LC16:
	.string	"ServerCannotStart"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"StoppingServerDoesNotKillConnections"
	.section	.rodata.str1.1
.LC18:
	.string	"ClosingConnectionReportsDone"
.LC19:
	.string	"ClosingSocketReportsDone"
.LC20:
	.string	"TerminatingSessionReportsDone"
.LC21:
	.string	"FailsToBindToNodejsHost"
.LC22:
	.string	"BindsToIpV6"
	.section	.text.startup
	.p2align 4
	.type	_Z41__static_initialization_and_destruction_0ii.constprop.0, @function
_Z41__static_initialization_and_destruction_0ii.constprop.0:
.LFB8951:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L272
.L230:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L273
.L231:
	leaq	-128(%rbp), %r14
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r13
	leaq	-80(%rbp), %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	pushq	%r15
	leaq	.LC13(%rip), %rdi
	xorl	%ecx, %ecx
	pushq	%rbx
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	movq	%r13, %r8
	xorl	%edx, %edx
	pushq	$0
	leaq	.LC12(%rip), %rsi
	pushq	$0
	movl	$388, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L274
.L234:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L275
.L235:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r11
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	movq	%r13, %r8
	leaq	.LC14(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$462, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN48InspectorSocketServerTest_ServerDoesNothing_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L276
.L238:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L277
.L239:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r10
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	movq	%r13, %r8
	leaq	.LC15(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$472, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN51InspectorSocketServerTest_ServerWithoutTargets_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L278
.L242:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L279
.L243:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r9
	movq	%r13, %r8
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	leaq	.LC16(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$490, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN48InspectorSocketServerTest_ServerCannotStart_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L280
.L246:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L281
.L247:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r8
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	movq	%r13, %r8
	leaq	.LC17(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$502, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L282
.L250:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L283
.L251:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rdi
	movq	%r13, %r8
	leaq	.LC13(%rip), %rdi
	leaq	.LC18(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$515, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L284
.L254:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L285
.L255:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rsi
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	movq	%r13, %r8
	leaq	.LC19(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$528, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L286
.L258:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L287
.L259:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	xorl	%edx, %edx
	pushq	%rcx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	leaq	.LC20(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$541, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L261
	call	_ZdlPv@PLT
.L261:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L288
.L262:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L289
.L263:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	pushq	%rdx
	movq	%r13, %r8
	xorl	%edx, %edx
	leaq	.LC21(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$557, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestEE(%rip), %rax
	movq	%rax, (%r15)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L290
.L266:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L291
.L267:
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	.LC13(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E(%rip), %r9
	movq	%r13, %r8
	leaq	.LC22(%rip), %rsi
	pushq	%r15
	pushq	$0
	pushq	$0
	movl	$580, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN42InspectorSocketServerTest_BindsToIpV6_Test10test_info_E(%rip)
	cmpq	%r12, %rdi
	je	.L268
	call	_ZdlPv@PLT
.L268:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L275:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L235
.L274:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L234
.L273:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L231
.L285:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L255
.L284:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L254
.L287:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L259
.L286:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L258
.L289:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L263
.L288:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L262
.L291:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L267
.L290:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L266
.L277:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L239
.L276:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L238
.L279:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L243
.L278:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L242
.L281:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L247
.L280:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L246
.L283:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L251
.L282:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L250
.L272:
	call	_ZN7testing8internal16SuiteApiResolverIN12_GLOBAL__N_125InspectorSocketServerTestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L230
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8951:
	.size	_Z41__static_initialization_and_destruction_0ii.constprop.0, .-_Z41__static_initialization_and_destruction_0ii.constprop.0
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetIdsEv, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetIdsEv:
.LFB6348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %r12
	subq	16(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	movq	$0, 16(%rdi)
	sarq	$5, %rax
	movups	%xmm0, (%rdi)
	je	.L316
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L317
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%rax, %rbx
.L295:
	movq	%rbx, %xmm0
	addq	%rbx, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 16(%r15)
	movups	%xmm0, (%r15)
	movq	24(%r14), %r13
	movq	16(%r14), %rcx
	cmpq	%rcx, %r13
	je	.L297
	leaq	-64(%rbp), %rax
	movq	%rcx, %r14
	movq	%rax, -80(%rbp)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L320:
	movzbl	(%r8), %eax
	movb	%al, 16(%rbx)
.L302:
	addq	$32, %r14
	movq	%r12, 8(%rbx)
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r14, %r13
	je	.L297
.L303:
	leaq	16(%rbx), %rdi
	movq	8(%r14), %r12
	movq	%rdi, (%rbx)
	movq	(%r14), %r8
	movq	%r8, %rax
	addq	%r12, %rax
	je	.L298
	testq	%r8, %r8
	je	.L318
.L298:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L319
	cmpq	$1, %r12
	je	.L320
	testq	%r12, %r12
	je	.L302
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
.L300:
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%rbx, 8(%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L295
.L318:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L321:
	call	__stack_chk_fail@PLT
.L317:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6348:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetIdsEv, .-_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetIdsEv
	.section	.rodata.str1.1
.LC23:
	.string	"false"
.LC24:
	.string	"true"
.LC25:
	.string	"wrapper->closed_"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s, @function
_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s:
.LFB6304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	-16(%rdi), %eax
	movq	$0, -72(%rbp)
	xorl	$1, %eax
	movb	%al, -80(%rbp)
	testb	%al, %al
	je	.L337
	movb	$1, -16(%rdi)
.L322:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	leaq	-88(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC25(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-64(%rbp), %r8
	movl	$191, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L324
	call	_ZdlPv@PLT
.L324:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L325
	movq	(%rdi), %rax
	call	*8(%rax)
.L325:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L322
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L327
	call	_ZdlPv@PLT
.L327:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L322
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6304:
	.size	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s, .-_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s
	.section	.rodata.str1.1
.LC26:
	.string	"wrapper->sending_"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si, @function
_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si:
.LFB6310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L355
	movzbl	194(%rdi), %eax
	subq	$360, %rdi
	movq	$0, -72(%rbp)
	movb	%al, -80(%rbp)
	testb	%al, %al
	je	.L356
	movb	$0, 554(%rdi)
.L339:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L357
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	leaq	-88(%rbp), %r13
	leaq	-96(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC26(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-64(%rbp), %r8
	movl	$226, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L343
	movq	(%rdi), %rax
	call	*8(%rax)
.L343:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L339
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_siE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L357:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6310:
	.size	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si, .-_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si
	.section	.rodata.str1.1
.LC27:
	.string	" Target Title"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	8(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rdx), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L359
	testq	%r14, %r14
	je	.L376
.L359:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L377
	cmpq	$1, %r13
	jne	.L362
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L363:
	movabsq	$4611686018427387903, %rax
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	subq	8(%r12), %rax
	cmpq	$12, %rax
	jbe	.L378
	movl	$13, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L379
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L363
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L361:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L363
.L376:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L378:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L379:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6349:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_124TestSocketServerDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC28:
	.string	"(!connected_)"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEib, @function
_ZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEib:
.LFB6296:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	xorl	%ecx, %ecx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%dx, 552(%rdi)
	movq	560(%rdi), %rax
	movw	%cx, (%rdi)
	cmpq	%rax, 568(%rdi)
	je	.L381
	movq	%rax, 568(%rdi)
.L381:
	movq	8(%rbx), %rdi
	leaq	16(%rbx), %r14
	movq	%r14, %rsi
	call	uv_tcp_init@PLT
	testb	%r15b, %r15b
	movq	(%r12), %rdi
	movl	%r13d, %esi
	leaq	-288(%rbp), %r15
	movq	%r15, %rdx
	je	.L382
	call	uv_ip6_addr@PLT
	testl	%eax, %eax
	jne	.L409
.L384:
	leaq	264(%rbx), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L410
	leaq	-216(%rbp), %r13
	xorl	%eax, %eax
	leaq	_ZL4loop(%rip), %rdi
	movq	%r13, %rsi
	movw	%ax, -224(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, 552(%rbx)
	jne	.L386
	leaq	_ZL4loop(%rip), %r12
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L411:
	movl	$1, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	cmpb	$0, 552(%rbx)
	jne	.L386
.L388:
	cmpb	$0, -224(%rbp)
	je	.L411
	leaq	-312(%rbp), %r14
	movb	$0, -304(%rbp)
	leaq	-320(%rbp), %r12
	movq	$0, -296(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-256(%rbp), %rdi
	leaq	-304(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-256(%rbp), %r8
	movl	$115, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L398
	call	_ZdlPv@PLT
.L398:
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L389
	movq	(%rdi), %rax
	call	*8(%rax)
.L389:
	movq	-296(%rbp), %r12
	testq	%r12, %r12
	je	.L390
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L391
	call	_ZdlPv@PLT
.L391:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L390:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -223(%rbp)
	jne	.L380
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -223(%rbp)
	je	.L393
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L386:
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -223(%rbp)
	jne	.L396
	.p2align 4,,10
	.p2align 3
.L395:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -223(%rbp)
	je	.L395
.L396:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r14, %rdi
	call	uv_read_start@PLT
.L380:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	je	.L384
.L409:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6296:
	.size	_ZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEib, .-_ZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEib
	.section	.rodata.str1.1
.LC29:
	.string	"sending_"
.LC30:
	.string	"(sending_)"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$256, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	554(%rdi), %eax
	movq	$0, -264(%rbp)
	xorl	$1, %eax
	movb	%al, -272(%rbp)
	testb	%al, %al
	je	.L451
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	leaq	-256(%rbp), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movl	$1, %ecx
	movq	%rax, -256(%rbp)
	movq	8(%rsi), %rax
	leaq	16(%rdi), %rsi
	leaq	360(%rdi), %rdi
	movb	$1, 194(%rdi)
	movq	%rax, -248(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L452
	leaq	-200(%rbp), %r13
	xorl	%eax, %eax
	leaq	_ZL4loop(%rip), %rdi
	movq	%r13, %rsi
	movw	%ax, -208(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, 554(%rbx)
	je	.L421
	leaq	_ZL4loop(%rip), %r12
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$1, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	cmpb	$0, 554(%rbx)
	je	.L421
.L423:
	cmpb	$0, -208(%rbp)
	je	.L453
	leaq	-280(%rbp), %r14
	movb	$0, -272(%rbp)
	leaq	-288(%rbp), %r12
	movq	$0, -264(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-272(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$179, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L424
	movq	(%rdi), %rax
	call	*8(%rax)
.L424:
	movq	-264(%rbp), %r12
	testq	%r12, %r12
	je	.L425
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L425:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L413
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L428
	.p2align 4,,10
	.p2align 3
.L413:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L413
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L430:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L430
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L451:
	leaq	-280(%rbp), %r13
	leaq	-288(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-208(%rbp), %rdi
	leaq	-272(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-208(%rbp), %r8
	movl	$171, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L415
	call	_ZdlPv@PLT
.L415:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L416
	movq	(%rdi), %rax
	call	*8(%rax)
.L416:
	movq	-264(%rbp), %r12
	testq	%r12, %r12
	je	.L413
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L452:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L454:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6302:
	.size	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC31:
	.string	""
.LC32:
	.string	"expects.c_str()"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"std::string(buffer_.data(), expects.length()).c_str()"
	.align 8
.LC34:
	.string	"(buffer_.size() < expects.length())"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_112ServerHolder6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_112ServerHolder6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6329:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	_ZL4loop(%rip), %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movw	%ax, -224(%rbp)
	call	uv_timer_init@PLT
	movl	$5000, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r14, %rdi
	call	uv_unref@PLT
	movq	8(%r12), %rdx
	movq	32(%rbx), %rax
	subq	24(%rbx), %rax
	cmpq	%rdx, %rax
	jnb	.L456
	leaq	_ZL4loop(%rip), %r13
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L515:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	movq	8(%r12), %rdx
	movq	32(%rbx), %rax
	subq	24(%rbx), %rax
	cmpq	%rdx, %rax
	jnb	.L456
.L458:
	cmpb	$0, -224(%rbp)
	je	.L515
	leaq	-280(%rbp), %r13
	cmpq	%rax, %rdx
	movq	$0, -264(%rbp)
	leaq	-288(%rbp), %r12
	movq	%r13, %rdi
	setbe	-272(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-256(%rbp), %rdi
	leaq	-272(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC34(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-256(%rbp), %r8
	movl	$286, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L459
	movq	(%rdi), %rax
	call	*8(%rax)
.L459:
	movq	-264(%rbp), %r12
	testq	%r12, %r12
	je	.L460
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L460:
	movq	%r14, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r14, %rdi
	call	uv_close@PLT
	cmpb	$0, -223(%rbp)
	jne	.L455
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -223(%rbp)
	je	.L462
.L455:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L516
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	movq	%r14, %rdi
	leaq	_ZL4loop(%rip), %r13
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r14, %rdi
	call	uv_close@PLT
	cmpb	$0, -223(%rbp)
	jne	.L485
	.p2align 4,,10
	.p2align 3
.L465:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -223(%rbp)
	je	.L465
.L485:
	movq	24(%rbx), %r15
	movq	8(%r12), %r13
	leaq	-208(%rbp), %r14
	movq	%r14, -224(%rbp)
	movq	(%r12), %r8
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L466
	testq	%r15, %r15
	je	.L517
.L466:
	movq	%r13, -272(%rbp)
	cmpq	$15, %r13
	ja	.L518
	cmpq	$1, %r13
	jne	.L469
	movzbl	(%r15), %eax
	leaq	-272(%rbp), %r9
	movb	%al, -208(%rbp)
	movq	%r14, %rax
.L470:
	movq	%r13, -216(%rbp)
	movq	%r9, %rdi
	leaq	.LC32(%rip), %rdx
	leaq	.LC33(%rip), %rsi
	movb	$0, (%rax,%r13)
	movq	-224(%rbp), %rcx
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	movq	-224(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	cmpb	$0, -272(%rbp)
	je	.L519
	movq	-264(%rbp), %r13
	testq	%r13, %r13
	je	.L477
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L477:
	movq	8(%r12), %rax
	movq	24(%rbx), %rcx
	addq	%rcx, %rax
	movq	%rax, %r12
	cmpq	%rax, %rcx
	je	.L455
	movq	32(%rbx), %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	%rax, %r12
	je	.L481
	movq	%rcx, %rdi
	movq	%r12, %rsi
	call	memmove@PLT
	movq	%rax, %rcx
	movq	32(%rbx), %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
.L481:
	addq	%rdx, %rcx
	cmpq	%rcx, %rax
	je	.L455
	movq	%rcx, 32(%rbx)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	-272(%rbp), %r9
	leaq	-224(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -304(%rbp)
	movq	%r9, %rsi
	movq	%r9, -296(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-296(%rbp), %r9
	movq	-304(%rbp), %r8
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-272(%rbp), %rax
	movq	%rax, -208(%rbp)
.L468:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r9, -304(%rbp)
	movq	%r8, -296(%rbp)
	call	memcpy@PLT
	movq	-272(%rbp), %r13
	movq	-224(%rbp), %rax
	movq	-296(%rbp), %r8
	movq	-304(%rbp), %r9
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L469:
	testq	%r13, %r13
	jne	.L520
	movq	%r14, %rax
	leaq	-272(%rbp), %r9
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L519:
	leaq	-280(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-264(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L473
	movq	(%rax), %r8
.L473:
	leaq	-288(%rbp), %r12
	movl	$287, %ecx
	movl	$2, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L474
	movq	(%rdi), %rax
	call	*8(%rax)
.L474:
	movq	-264(%rbp), %r12
	testq	%r12, %r12
	je	.L455
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L455
.L520:
	movq	%r14, %rdi
	leaq	-272(%rbp), %r9
	jmp	.L468
.L516:
	call	__stack_chk_fail@PLT
.L517:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE6329:
	.size	_ZN12_GLOBAL__N_112ServerHolder6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_112ServerHolder6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"std::string(contents_.data(), expects.length()).c_str()"
	.align 8
.LC36:
	.string	"(contents_.size() < expects.length())"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6299:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	_ZL4loop(%rip), %rdi
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movw	%ax, -224(%rbp)
	call	uv_timer_init@PLT
	movl	$5000, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r14, %rdi
	call	uv_unref@PLT
	movq	8(%r12), %rdx
	movq	568(%rbx), %rax
	subq	560(%rbx), %rax
	cmpq	%rdx, %rax
	jnb	.L522
	leaq	_ZL4loop(%rip), %r13
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L581:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	movq	8(%r12), %rdx
	movq	568(%rbx), %rax
	subq	560(%rbx), %rax
	cmpq	%rdx, %rax
	jnb	.L522
.L524:
	cmpb	$0, -224(%rbp)
	je	.L581
	leaq	-280(%rbp), %r13
	cmpq	%rax, %rdx
	movq	$0, -264(%rbp)
	leaq	-288(%rbp), %r12
	movq	%r13, %rdi
	setbe	-272(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-256(%rbp), %rdi
	leaq	-272(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC36(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-256(%rbp), %r8
	movl	$145, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-256(%rbp), %rdi
	leaq	-240(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L525
	movq	(%rdi), %rax
	call	*8(%rax)
.L525:
	movq	-264(%rbp), %r12
	testq	%r12, %r12
	je	.L526
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L527
	call	_ZdlPv@PLT
.L527:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L526:
	movq	%r14, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r14, %rdi
	call	uv_close@PLT
	cmpb	$0, -223(%rbp)
	jne	.L521
	.p2align 4,,10
	.p2align 3
.L528:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -223(%rbp)
	je	.L528
.L521:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L582
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	movq	%r14, %rdi
	leaq	_ZL4loop(%rip), %r13
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r14, %rdi
	call	uv_close@PLT
	cmpb	$0, -223(%rbp)
	jne	.L551
	.p2align 4,,10
	.p2align 3
.L531:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -223(%rbp)
	je	.L531
.L551:
	movq	560(%rbx), %r15
	movq	8(%r12), %r13
	leaq	-208(%rbp), %r14
	movq	%r14, -224(%rbp)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L532
	testq	%r15, %r15
	je	.L583
.L532:
	movq	%r13, -272(%rbp)
	cmpq	$15, %r13
	ja	.L584
	cmpq	$1, %r13
	jne	.L535
	movzbl	(%r15), %eax
	leaq	-272(%rbp), %r9
	movb	%al, -208(%rbp)
	movq	%r14, %rax
.L536:
	movq	%r13, -216(%rbp)
	movq	%r9, %rdi
	leaq	.LC35(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	movb	$0, (%rax,%r13)
	movq	-224(%rbp), %r8
	movq	(%r12), %rcx
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	movq	-224(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	cmpb	$0, -272(%rbp)
	je	.L585
	movq	-264(%rbp), %r13
	testq	%r13, %r13
	je	.L543
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L543:
	movq	8(%r12), %rax
	movq	560(%rbx), %rcx
	addq	%rcx, %rax
	movq	%rax, %r12
	cmpq	%rax, %rcx
	je	.L521
	movq	568(%rbx), %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	%rax, %r12
	je	.L547
	movq	%rcx, %rdi
	movq	%r12, %rsi
	call	memmove@PLT
	movq	%rax, %rcx
	movq	568(%rbx), %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
.L547:
	addq	%rdx, %rcx
	cmpq	%rcx, %rax
	je	.L521
	movq	%rcx, 568(%rbx)
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L584:
	leaq	-272(%rbp), %r9
	leaq	-224(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r9, -296(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-296(%rbp), %r9
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-272(%rbp), %rax
	movq	%rax, -208(%rbp)
.L534:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r9, -296(%rbp)
	call	memcpy@PLT
	movq	-272(%rbp), %r13
	movq	-224(%rbp), %rax
	movq	-296(%rbp), %r9
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L535:
	testq	%r13, %r13
	jne	.L586
	movq	%r14, %rax
	leaq	-272(%rbp), %r9
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	-280(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-264(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L539
	movq	(%rax), %r8
.L539:
	leaq	-288(%rbp), %r12
	movl	$146, %ecx
	movl	$2, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L540
	movq	(%rdi), %rax
	call	*8(%rax)
.L540:
	movq	-264(%rbp), %r12
	testq	%r12, %r12
	je	.L521
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L521
.L586:
	movq	%r14, %rdi
	leaq	-272(%rbp), %r9
	jmp	.L534
.L582:
	call	__stack_chk_fail@PLT
.L583:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE6299:
	.size	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC37:
	.string	"(!closed_)"
.LC38:
	.string	"(!eof_)"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper9ExpectEOFEv, @function
_ZN12_GLOBAL__N_113SocketWrapper9ExpectEOFEv:
.LFB6300:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-200(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	_ZL4loop(%rip), %rdi
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%dx, -208(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, 1(%rbx)
	jne	.L588
	leaq	_ZL4loop(%rip), %r12
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L629:
	movl	$1, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	cmpb	$0, 1(%rbx)
	jne	.L588
.L590:
	cmpb	$0, -208(%rbp)
	je	.L629
	leaq	-264(%rbp), %r14
	movb	$0, -256(%rbp)
	leaq	-272(%rbp), %r12
	movq	$0, -248(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-256(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC38(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$152, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L591
	movq	(%rdi), %rax
	call	*8(%rax)
.L591:
	movq	-248(%rbp), %r12
	testq	%r12, %r12
	je	.L592
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L593
	call	_ZdlPv@PLT
.L593:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L592:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L587
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L595
.L587:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L630
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %r12
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L611
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$2, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L597
.L611:
	leaq	16(%rbx), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
	xorl	%eax, %eax
	movq	%r13, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -208(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, (%rbx)
	jne	.L598
	leaq	_ZL4loop(%rip), %r12
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$1, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	cmpb	$0, (%rbx)
	jne	.L598
.L600:
	cmpb	$0, -208(%rbp)
	je	.L631
	leaq	-264(%rbp), %r14
	movb	$0, -256(%rbp)
	leaq	-272(%rbp), %r12
	movq	$0, -248(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-256(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$141, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L608
	call	_ZdlPv@PLT
.L608:
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L601
	movq	(%rdi), %rax
	call	*8(%rax)
.L601:
	movq	-248(%rbp), %r12
	testq	%r12, %r12
	je	.L602
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L603
	call	_ZdlPv@PLT
.L603:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L602:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L587
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L605:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L605
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L587
	.p2align 4,,10
	.p2align 3
.L606:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L606
	jmp	.L587
.L630:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6300:
	.size	_ZN12_GLOBAL__N_113SocketWrapper9ExpectEOFEv, .-_ZN12_GLOBAL__N_113SocketWrapper9ExpectEOFEv
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0, @function
_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0:
.LFB8948:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rdx, -128(%rbp)
	movl	%ecx, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	movb	$0, 8(%rdi)
	movaps	%xmm0, 16(%rdi)
	movaps	%xmm0, 32(%rdi)
	testb	%sil, %sil
	jne	.L694
	movl	$56, %edi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	call	_Znwm@PLT
	movq	$0, -120(%rbp)
	movq	%rax, %r15
	leaq	16+_ZTVN12_GLOBAL__N_124TestSocketServerDelegateE(%rip), %rax
	movq	%rax, -168(%rbp)
	movq	%rax, (%r15)
	movq	%rbx, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	$0, 32(%r15)
.L663:
	movq	%r13, 24(%r15)
	movl	$257, %eax
	movl	$152, %edi
	movl	$0, 48(%r15)
	movw	%ax, -106(%rbp)
	movq	%r15, -104(%rbp)
	call	_Znwm@PLT
	subq	$8, %rsp
	movq	-128(%rbp), %rcx
	movl	-132(%rbp), %r8d
	pushq	$0
	leaq	_ZL4loop(%rip), %rdx
	leaq	-104(%rbp), %rsi
	movq	%rax, %rdi
	leaq	-106(%rbp), %r9
	movq	%rax, %r13
	call	_ZN4node9inspector21InspectorSocketServerC1ESt10unique_ptrINS0_20SocketServerDelegateESt14default_deleteIS3_EEP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiRKNS_17InspectPublishUidEP8_IO_FILE@PLT
	movq	-104(%rbp), %r8
	popq	%rdx
	popq	%rcx
	testq	%r8, %r8
	je	.L648
	movq	(%r8), %rax
	leaq	_ZN12_GLOBAL__N_124TestSocketServerDelegateD0Ev(%rip), %rdx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L649
	movq	-168(%rbp), %rax
	movq	16(%r8), %r15
	movq	%rax, (%r8)
	movq	8(%r8), %rax
	movb	$1, 8(%rax)
	movq	24(%r8), %rax
	cmpq	%r15, %rax
	je	.L650
	movq	%rbx, -128(%rbp)
	movq	%rax, %r14
	movq	%r8, %rbx
	.p2align 4,,10
	.p2align 3
.L654:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L651
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r14, %r15
	jne	.L654
	movq	%rbx, %r8
	movq	-128(%rbp), %rbx
	movq	16(%r8), %r15
.L650:
	testq	%r15, %r15
	je	.L655
	movq	%r15, %rdi
	movq	%r8, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r8
.L655:
	movl	$56, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L648:
	movq	16(%rbx), %r15
	movq	%r13, 16(%rbx)
	testq	%r15, %r15
	je	.L656
	movq	%r15, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L656:
	movq	%r12, %rbx
	cmpq	%r12, -120(%rbp)
	je	.L661
	.p2align 4,,10
	.p2align 3
.L657:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L660
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L657
.L661:
	testq	%r12, %r12
	je	.L632
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L632:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L695
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	addq	$32, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L657
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L651:
	addq	$32, %r15
	cmpq	%r15, %r14
	jne	.L654
	movq	%rbx, %r8
	movq	-128(%rbp), %rbx
	movq	16(%r8), %r15
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L694:
	movl	$25959, %esi
	movl	$32, %edi
	leaq	-80(%rbp), %r15
	movabsq	$8241996531539468653, %rax
	movq	%r15, -96(%rbp)
	movq	%rax, -80(%rbp)
	movw	%si, -72(%rbp)
	movb	$116, -70(%rbp)
	movq	$11, -88(%rbp)
	movb	$0, -69(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %r14
	movq	-88(%rbp), %r13
	leaq	16(%rax), %rdi
	movq	%rax, %r12
	movq	%rdi, (%rax)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L666
	testq	%r14, %r14
	je	.L634
.L666:
	movq	%r13, -104(%rbp)
	cmpq	$15, %r13
	ja	.L696
	cmpq	$1, %r13
	je	.L638
	testq	%r13, %r13
	jne	.L637
.L639:
	movq	%r13, 8(%r12)
	leaq	32(%r12), %rax
	movb	$0, (%rdi,%r13)
	movq	-96(%rbp), %rdi
	movq	%rax, -120(%rbp)
	cmpq	%r15, %rdi
	je	.L641
	call	_ZdlPv@PLT
.L641:
	movl	$56, %edi
	movq	%r12, %r14
	call	_Znwm@PLT
	movl	$32, %edi
	movq	%rax, %r15
	leaq	16+_ZTVN12_GLOBAL__N_124TestSocketServerDelegateE(%rip), %rax
	movq	%rax, (%r15)
	movq	%rbx, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	$0, 32(%r15)
	movq	%rax, -168(%rbp)
	call	_Znwm@PLT
	movq	%r15, -152(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %r13
	leaq	32(%rax), %rax
	movq	%r12, -160(%rbp)
	movq	%rax, 32(%r15)
	punpcklqdq	%xmm0, %xmm0
	leaq	-104(%rbp), %rax
	movq	%rax, -144(%rbp)
	movups	%xmm0, 16(%r15)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L699:
	movzbl	(%r15), %eax
	movb	%al, 16(%r13)
.L646:
	movq	%r12, 8(%r13)
	addq	$32, %r14
	addq	$32, %r13
	movb	$0, (%rdi,%r12)
	cmpq	%r14, -120(%rbp)
	je	.L697
.L647:
	movq	(%r14), %r15
	movq	8(%r14), %r12
	leaq	16(%r13), %rdi
	movq	%rdi, 0(%r13)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L667
	testq	%r15, %r15
	je	.L634
.L667:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L698
	cmpq	$1, %r12
	je	.L699
	testq	%r12, %r12
	je	.L646
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L698:
	movq	-144(%rbp), %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%r13)
.L644:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	0(%r13), %rdi
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L697:
	movq	-152(%rbp), %r15
	movq	-160(%rbp), %r12
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L638:
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r12, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%r12)
.L637:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L639
.L634:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L695:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8948:
	.size	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0, .-_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	.section	.rodata.str1.1
.LC39:
	.string	"server->Start()"
.LC40:
	.string	"(uv_loop_alive(&loop))"
	.text
	.align 2
	.p2align 4
	.globl	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test8TestBodyEv
	.type	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test8TestBodyEv, @function
_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test8TestBodyEv:
.LFB6434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$26482, %edx
	movl	$80, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-304(%rbp), %rdi
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-208(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-192(%rbp), %rbx
	subq	$304, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movw	%dx, -184(%rbp)
	movq	%r12, %rdx
	movabsq	$8011467687943499630, %rax
	movq	%rbx, -208(%rbp)
	movq	%rax, -192(%rbp)
	movq	$10, -200(%rbp)
	movb	$0, -182(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L701
	call	_ZdlPv@PLT
.L701:
	movq	-288(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -312(%rbp)
	xorl	$1, %eax
	movb	%al, -320(%rbp)
	testb	%al, %al
	je	.L756
	leaq	-200(%rbp), %r12
	xorl	%eax, %eax
	leaq	_ZL4loop(%rip), %rdi
	movq	%r12, %rsi
	movw	%ax, -208(%rbp)
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L757:
	cmpb	$0, -208(%rbp)
	jne	.L708
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L709:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L757
.L708:
	leaq	_ZL4loop(%rip), %rdi
	call	uv_loop_alive@PLT
	movq	$0, -312(%rbp)
	testl	%eax, %eax
	sete	-320(%rbp)
	jne	.L758
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L707
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L718:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L718
.L707:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L722
	call	_ZdlPv@PLT
.L722:
	movq	-288(%rbp), %r12
	testq	%r12, %r12
	je	.L700
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L700:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L759
	addq	$304, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	.cfi_restore_state
	leaq	-328(%rbp), %r14
	leaq	-336(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-320(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$560, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L711
	call	_ZdlPv@PLT
.L711:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L712
	movq	(%rdi), %rax
	call	*8(%rax)
.L712:
	movq	-312(%rbp), %r13
	testq	%r13, %r13
	je	.L713
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L713:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L707
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L716:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L716
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	-328(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	%r12, %rdi
	leaq	-320(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$559, %ecx
	movq	-208(%rbp), %r8
	leaq	-336(%rbp), %r12
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L703
	call	_ZdlPv@PLT
.L703:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L704
	movq	(%rdi), %rax
	call	*8(%rax)
.L704:
	movq	-312(%rbp), %r12
	testq	%r12, %r12
	je	.L707
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L706
	call	_ZdlPv@PLT
.L706:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L707
.L759:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6434:
	.size	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test8TestBodyEv, .-_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test8TestBodyEv
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"HTTP/1.0 200 OK\r\nContent-Type: application/json; charset=UTF-8\r\nCache-Control: no-cache\r\nContent-Length: "
	.section	.rodata.str1.1
.LC42:
	.string	"\r\n\r\n"
.LC43:
	.string	"\n\n"
.LC44:
	.string	"GET "
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	" HTTP/1.1\r\nHost: localhost:9229\r\n\r\n"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_1L15TestHttpRequestEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_, @function
_ZN12_GLOBAL__N_1L15TestHttpRequestEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_:
.LFB6357:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	_ZL4loop(%rip), %r14
	leaq	-1248(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	xorl	%edi, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1352, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%di, -640(%rbp)
	leaq	-624(%rbp), %rdi
	movq	%r14, -632(%rbp)
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	movb	$49, -1192(%rbp)
	rep stosq
	leaq	-1200(%rbp), %rax
	movq	%r14, %rdi
	movb	$0, -87(%rbp)
	movq	%rax, -1344(%rbp)
	movq	%rax, -1216(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -1200(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -1191(%rbp)
	movq	$9, -1208(%rbp)
	movq	%rax, -1336(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	uv_tcp_init@PLT
	movq	-1216(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L858
	movq	-1336(%rbp), %rsi
	leaq	-376(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L859
	xorl	%esi, %esi
	leaq	-1016(%rbp), %r12
	movq	%r14, %rdi
	movw	%si, -1024(%rbp)
	movq	%r12, %rsi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -88(%rbp)
	je	.L765
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L860:
	movl	$1, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -88(%rbp)
	jne	.L763
.L765:
	cmpb	$0, -1024(%rbp)
	je	.L860
	leaq	-1328(%rbp), %r9
	leaq	-1184(%rbp), %r15
	movb	$0, -1312(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1360(%rbp)
	leaq	-1320(%rbp), %r14
	movq	$0, -1304(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1312(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1184(%rbp), %r8
	movl	$115, %ecx
	movq	%r14, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1360(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	leaq	-1168(%rbp), %r14
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1184(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	-1328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L766
	movq	(%rdi), %rax
	call	*8(%rax)
.L766:
	movq	-1304(%rbp), %r14
	testq	%r14, %r14
	je	.L767
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L768
	call	_ZdlPv@PLT
.L768:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L767:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %r14
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -1023(%rbp)
	jne	.L770
	.p2align 4,,10
	.p2align 3
.L769:
	movl	$2, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -1023(%rbp)
	je	.L769
	movq	-1216(%rbp), %rdi
	cmpq	-1344(%rbp), %rdi
	je	.L773
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L773:
	movq	.LC46(%rip), %xmm1
	leaq	-912(%rbp), %r15
	leaq	-1024(%rbp), %r14
	movq	%r15, %rdi
	movq	%r15, -1376(%rbp)
	movhps	.LC47(%rip), %xmm1
	movaps	%xmm1, -1360(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, -912(%rbp)
	pxor	%xmm0, %xmm0
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%cx, -688(%rbp)
	movups	%xmm0, -680(%rbp)
	movups	%xmm0, -664(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -1024(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -696(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1360(%rbp), %xmm1
	movq	%rax, -912(%rbp)
	leaq	-960(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -1024(%rbp)
	movaps	%xmm0, -1008(%rbp)
	movaps	%xmm0, -992(%rbp)
	movaps	%xmm0, -976(%rbp)
	movq	%rax, -1360(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -1016(%rbp)
	leaq	-928(%rbp), %rax
	movq	%rax, -1368(%rbp)
	movq	%rax, -944(%rbp)
	movl	$16, -952(%rbp)
	movq	$0, -936(%rbp)
	movb	$0, -928(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$105, %edx
	movq	%r14, %rdi
	leaq	.LC41(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rax
	movq	%r14, %rdi
	leaq	2(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r14, %rdi
	movl	$4, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	leaq	-1280(%rbp), %r14
	movabsq	$4611686018427387903, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC43(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1264(%rbp), %rax
	movq	%r14, %rdi
	movq	$0, -1272(%rbp)
	movq	%rax, -1384(%rbp)
	movq	%rax, -1280(%rbp)
	movq	8(%r13), %rax
	movb	$0, -1264(%rbp)
	leaq	4(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	%rbx, %rax
	subq	-1272(%rbp), %rax
	cmpq	$3, %rax
	jbe	.L775
	movl	$4, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	subq	-1272(%rbp), %rbx
	cmpq	$34, %rbx
	jbe	.L775
	movl	$35, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-1232(%rbp), %rcx
	movq	%rcx, -1392(%rbp)
	leaq	16(%rax), %rdx
	movq	%rcx, -1248(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L861
	movq	%rcx, -1248(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1232(%rbp)
.L777:
	movq	8(%rax), %rcx
	movq	%rcx, -1240(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movzbl	-86(%rbp), %eax
	movq	$0, -1304(%rbp)
	xorl	$1, %eax
	movb	%al, -1312(%rbp)
	testb	%al, %al
	je	.L862
	movq	-1248(%rbp), %rax
	movq	-1336(%rbp), %rsi
	leaq	-1296(%rbp), %rdx
	leaq	-280(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movl	$1, %ecx
	movb	$1, -86(%rbp)
	movq	%rax, -1296(%rbp)
	movq	-1240(%rbp), %rax
	movq	%rax, -1288(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L863
	leaq	-1176(%rbp), %r13
	xorl	%edx, %edx
	leaq	_ZL4loop(%rip), %rdi
	movq	%r13, %rsi
	movw	%dx, -1184(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, -86(%rbp)
	je	.L785
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L864:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -86(%rbp)
	je	.L785
.L787:
	cmpb	$0, -1184(%rbp)
	je	.L864
	leaq	-1328(%rbp), %r15
	movb	$0, -1312(%rbp)
	leaq	-1320(%rbp), %r14
	movq	$0, -1304(%rbp)
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1216(%rbp), %rdi
	leaq	-1312(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1216(%rbp), %r8
	movl	$179, %ecx
	movq	%r14, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1216(%rbp), %rdi
	cmpq	-1344(%rbp), %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	-1328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L788
	movq	(%rdi), %rax
	call	*8(%rax)
.L788:
	movq	-1304(%rbp), %r14
	testq	%r14, %r14
	je	.L789
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L790
	call	_ZdlPv@PLT
.L790:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L789:
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -1183(%rbp)
	jne	.L857
	.p2align 4,,10
	.p2align 3
.L791:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1183(%rbp)
	je	.L791
.L857:
	leaq	-1184(%rbp), %r15
	leaq	-1168(%rbp), %r14
.L794:
	movq	-1248(%rbp), %rdi
	cmpq	-1392(%rbp), %rdi
	je	.L796
	call	_ZdlPv@PLT
.L796:
	movq	-1280(%rbp), %rdi
	cmpq	-1384(%rbp), %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	-976(%rbp), %rax
	movq	%r14, -1184(%rbp)
	movq	$0, -1176(%rbp)
	movb	$0, -1168(%rbp)
	testq	%rax, %rax
	je	.L798
	movq	-992(%rbp), %r8
	movq	-984(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L799
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L800:
	leaq	-640(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-1184(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L801
	call	_ZdlPv@PLT
.L801:
	movq	.LC46(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-944(%rbp), %rdi
	movq	%rax, -912(%rbp)
	movhps	.LC48(%rip), %xmm0
	movaps	%xmm0, -1024(%rbp)
	cmpq	-1368(%rbp), %rdi
	je	.L802
	call	_ZdlPv@PLT
.L802:
	movq	-1360(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1016(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1376(%rbp), %rdi
	movq	%rax, -1024(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1024(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -912(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1336(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -1024(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -640(%rbp)
	jne	.L803
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L865:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -640(%rbp)
	jne	.L803
.L805:
	cmpb	$0, -1024(%rbp)
	je	.L865
	leaq	-1320(%rbp), %rbx
	movb	$0, -1312(%rbp)
	leaq	-1328(%rbp), %r13
	movq	$0, -1304(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1312(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1184(%rbp), %r8
	movl	$141, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1184(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L815
	call	_ZdlPv@PLT
.L815:
	movq	-1320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L806
	movq	(%rdi), %rax
	call	*8(%rax)
.L806:
	movq	-1304(%rbp), %r13
	testq	%r13, %r13
	je	.L807
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L807:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -1023(%rbp)
	jne	.L812
	.p2align 4,,10
	.p2align 3
.L809:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1023(%rbp)
	je	.L809
.L812:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L760
	call	_ZdlPv@PLT
.L760:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L866
	addq	$1352, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %r14
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -1023(%rbp)
	jne	.L816
	.p2align 4,,10
	.p2align 3
.L772:
	movl	$2, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -1023(%rbp)
	je	.L772
.L816:
	movq	-1336(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	call	uv_read_start@PLT
.L770:
	movq	-1216(%rbp), %rdi
	cmpq	-1344(%rbp), %rdi
	je	.L773
	call	_ZdlPv@PLT
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L785:
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -1183(%rbp)
	jne	.L857
	.p2align 4,,10
	.p2align 3
.L793:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1183(%rbp)
	je	.L793
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -1023(%rbp)
	jne	.L812
	.p2align 4,,10
	.p2align 3
.L813:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1023(%rbp)
	je	.L813
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L799:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	-1328(%rbp), %r14
	leaq	-1184(%rbp), %r15
	movq	%r14, %rdi
	leaq	-1320(%rbp), %r13
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1312(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1184(%rbp), %r8
	movl	$171, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-1168(%rbp), %r14
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1184(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L779
	call	_ZdlPv@PLT
.L779:
	movq	-1328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L780
	movq	(%rdi), %rax
	call	*8(%rax)
.L780:
	movq	-1304(%rbp), %r13
	testq	%r13, %r13
	je	.L794
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L782
	call	_ZdlPv@PLT
.L782:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L861:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -1232(%rbp)
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L863:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L798:
	leaq	-944(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L858:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L859:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L775:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L866:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6357:
	.size	_ZN12_GLOBAL__N_1L15TestHttpRequestEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_, .-_ZN12_GLOBAL__N_1L15TestHttpRequestEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_, @function
_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_:
.LFB6301:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-432(%rbp), %r14
	leaq	-368(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$744, %rsp
	movq	%rdx, -760(%rbp)
	movq	.LC46(%rip), %xmm1
	movhps	.LC47(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -752(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%dx, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-752(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -768(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$105, %edx
	movq	%r14, %rdi
	leaq	.LC41(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-760(%rbp), %rcx
	movq	%r14, %rdi
	movq	8(%rcx), %rax
	movq	%rcx, -752(%rbp)
	leaq	2(%rax), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%r14, %rdi
	movl	$4, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-752(%rbp), %rcx
	movq	%r14, %rdi
	leaq	-688(%rbp), %r14
	movq	8(%rcx), %rdx
	movq	(%rcx), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC43(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-672(%rbp), %rax
	movq	%r14, %rdi
	movq	$0, -680(%rbp)
	movq	%rax, -752(%rbp)
	movq	%rax, -688(%rbp)
	movq	8(%rbx), %rax
	movb	$0, -672(%rbp)
	leaq	4(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rcx
	movq	%rcx, %rax
	subq	-680(%rbp), %rax
	cmpq	$3, %rax
	jbe	.L869
	movl	$4, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movabsq	$4611686018427387903, %rcx
	subq	-680(%rbp), %rcx
	cmpq	$34, %rcx
	jbe	.L869
	movl	$35, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-640(%rbp), %rbx
	movq	%rbx, -656(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	movq	%rbx, -760(%rbp)
	cmpq	%rdx, %rcx
	je	.L922
	movq	%rcx, -656(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -640(%rbp)
.L871:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -648(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movzbl	554(%r12), %eax
	movq	$0, -712(%rbp)
	xorl	$1, %eax
	movb	%al, -720(%rbp)
	testb	%al, %al
	je	.L923
	movq	-656(%rbp), %rax
	leaq	16(%r12), %rsi
	movb	$1, 554(%r12)
	leaq	-704(%rbp), %rdx
	leaq	360(%r12), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movl	$1, %ecx
	movq	%rax, -704(%rbp)
	movq	-648(%rbp), %rax
	movq	%rax, -696(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L924
	leaq	-584(%rbp), %r14
	xorl	%eax, %eax
	leaq	_ZL4loop(%rip), %rdi
	movq	%r14, %rsi
	movw	%ax, -592(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r14, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r14, %rdi
	call	uv_unref@PLT
	cmpb	$0, 554(%r12)
	je	.L879
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L925:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, 554(%r12)
	je	.L879
.L881:
	cmpb	$0, -592(%rbp)
	je	.L925
	leaq	-736(%rbp), %r9
	movb	$0, -720(%rbp)
	leaq	-728(%rbp), %rbx
	movq	%r9, %rdi
	movq	%r9, -776(%rbp)
	movq	$0, -712(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-624(%rbp), %rdi
	leaq	-720(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-624(%rbp), %r8
	movl	$179, %ecx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-776(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-624(%rbp), %rdi
	leaq	-608(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L882
	movq	(%rdi), %rax
	call	*8(%rax)
.L882:
	movq	-712(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L883
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L883:
	movq	%r14, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r14, %rdi
	call	uv_close@PLT
	cmpb	$0, -591(%rbp)
	jne	.L921
	.p2align 4,,10
	.p2align 3
.L885:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -591(%rbp)
	je	.L885
.L921:
	leaq	-592(%rbp), %r14
	leaq	-576(%rbp), %rbx
.L888:
	movq	-656(%rbp), %rdi
	cmpq	-760(%rbp), %rdi
	je	.L890
	call	_ZdlPv@PLT
.L890:
	movq	-688(%rbp), %rdi
	cmpq	-752(%rbp), %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	-384(%rbp), %rax
	movq	%rbx, -592(%rbp)
	movq	$0, -584(%rbp)
	movb	$0, -576(%rbp)
	testq	%rax, %rax
	je	.L892
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L893
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L894:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-592(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L895
	call	_ZdlPv@PLT
.L895:
	movq	.LC46(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC48(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-768(%rbp), %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L926
	addq	$744, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	movq	%r14, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r14, %rdi
	call	uv_close@PLT
	cmpb	$0, -591(%rbp)
	jne	.L921
	.p2align 4,,10
	.p2align 3
.L887:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -591(%rbp)
	je	.L887
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L893:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L923:
	leaq	-736(%rbp), %r9
	leaq	-592(%rbp), %r14
	movq	%r9, %rdi
	movq	%r9, -776(%rbp)
	leaq	-728(%rbp), %rbx
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-720(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-592(%rbp), %r8
	movl	$171, %ecx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-776(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	leaq	-576(%rbp), %rbx
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-592(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L874
	movq	(%rdi), %rax
	call	*8(%rax)
.L874:
	movq	-712(%rbp), %r8
	testq	%r8, %r8
	je	.L888
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L876
	movq	%r8, -776(%rbp)
	call	_ZdlPv@PLT
	movq	-776(%rbp), %r8
.L876:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L922:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -640(%rbp)
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L892:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L924:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L869:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L926:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6301:
	.size	_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_, .-_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	.section	.rodata.str1.1
.LC49:
	.string	"server.delegate_done"
	.text
	.align 2
	.p2align 4
	.globl	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test8TestBodyEv
	.type	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test8TestBodyEv, @function
_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test8TestBodyEv:
.LFB6406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-640(%rbp), %r14
	leaq	-960(%rbp), %rdi
	pushq	%r13
	movq	%r14, %rdx
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-624(%rbp), %r12
	pushq	%rbx
	subq	$968, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -640(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -624(%rbp)
	movb	$49, -616(%rbp)
	movq	$9, -632(%rbp)
	movb	$0, -615(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-640(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L928
	call	_ZdlPv@PLT
.L928:
	movq	-944(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -968(%rbp)
	movb	%al, -976(%rbp)
	testb	%al, %al
	je	.L1031
	xorl	%r9d, %r9d
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rax
	movw	%r9w, -640(%rbp)
	movq	%rax, -632(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	-944(%rbp), %rdi
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movb	$49, -840(%rbp)
	movl	%eax, %r13d
	leaq	-848(%rbp), %rax
	movq	$9, -856(%rbp)
	movq	%rax, -1000(%rbp)
	movq	%rax, -864(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -848(%rbp)
	movq	-80(%rbp), %rax
	movb	$0, -839(%rbp)
	movw	%r10w, -88(%rbp)
	movw	%r11w, -640(%rbp)
	cmpq	-72(%rbp), %rax
	je	.L935
	movq	%rax, -72(%rbp)
.L935:
	movq	-632(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-896(%rbp), %r15
	call	uv_tcp_init@PLT
	movq	-864(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r13d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L1032
	leaq	-376(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L1033
	leaq	-792(%rbp), %r13
	xorl	%r8d, %r8d
	leaq	_ZL4loop(%rip), %rdi
	movq	%r13, %rsi
	movw	%r8w, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, -88(%rbp)
	jne	.L938
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1034:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -88(%rbp)
	jne	.L938
.L940:
	cmpb	$0, -800(%rbp)
	je	.L1034
	leaq	-992(%rbp), %r9
	leaq	-832(%rbp), %r15
	movb	$0, -976(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1008(%rbp)
	leaq	-984(%rbp), %rbx
	movq	$0, -968(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-976(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$115, %ecx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1008(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	leaq	-816(%rbp), %rbx
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L979
	call	_ZdlPv@PLT
.L979:
	movq	-992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L941
	movq	(%rdi), %rax
	call	*8(%rax)
.L941:
	movq	-968(%rbp), %r8
	testq	%r8, %r8
	je	.L942
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L943
	movq	%r8, -1008(%rbp)
	call	_ZdlPv@PLT
	movq	-1008(%rbp), %r8
.L943:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L942:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L946
	.p2align 4,,10
	.p2align 3
.L945:
	movl	$2, %esi
	leaq	_ZL4loop(%rip), %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L945
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L977
	.p2align 4,,10
	.p2align 3
.L947:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L947
.L977:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r12, %rdi
	call	uv_read_start@PLT
	leaq	-832(%rbp), %r15
	leaq	-816(%rbp), %rbx
.L946:
	movq	-864(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L948
	call	_ZdlPv@PLT
.L948:
	leaq	-784(%rbp), %rax
	movl	$8283, %esi
	movl	$29811, %edi
	movb	$93, -782(%rbp)
	movq	%rax, -1000(%rbp)
	leaq	-800(%rbp), %rdx
	movq	%rax, -800(%rbp)
	movabsq	$7596498822829926959, %rax
	movw	%si, -784(%rbp)
	movq	%r15, %rsi
	movw	%di, -808(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -1008(%rbp)
	movq	$3, -792(%rbp)
	movb	$0, -781(%rbp)
	movq	%rbx, -832(%rbp)
	movq	%rax, -816(%rbp)
	movq	$10, -824(%rbp)
	movb	$0, -806(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	-800(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L950
	call	_ZdlPv@PLT
.L950:
	movq	-944(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movl	$8283, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	-1000(%rbp), %rax
	movw	%dx, -784(%rbp)
	movl	$29811, %ecx
	movq	-1008(%rbp), %rdx
	movb	$93, -782(%rbp)
	movq	%rax, -800(%rbp)
	movabsq	$7596498822829926959, %rax
	movq	$3, -792(%rbp)
	movb	$0, -781(%rbp)
	movq	%rbx, -832(%rbp)
	movq	%rax, -816(%rbp)
	movw	%cx, -808(%rbp)
	movq	$10, -824(%rbp)
	movb	$0, -806(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L951
	call	_ZdlPv@PLT
.L951:
	movq	-800(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L952
	call	_ZdlPv@PLT
.L952:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	xorl	%eax, %eax
	movq	%r13, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, -640(%rbp)
	jne	.L953
	leaq	_ZL4loop(%rip), %r12
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1035:
	movl	$1, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	cmpb	$0, -640(%rbp)
	jne	.L953
.L955:
	cmpb	$0, -800(%rbp)
	je	.L1035
	leaq	-984(%rbp), %r14
	movb	$0, -976(%rbp)
	leaq	-992(%rbp), %r12
	movq	$0, -968(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-976(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$141, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L976
	call	_ZdlPv@PLT
.L976:
	movq	-984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L956
	movq	(%rdi), %rax
	call	*8(%rax)
.L956:
	movq	-968(%rbp), %r12
	testq	%r12, %r12
	je	.L957
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L958
	call	_ZdlPv@PLT
.L958:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L957:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L959
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L960:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L960
.L959:
	xorl	%esi, %esi
	leaq	_ZL4loop(%rip), %rdi
	call	uv_run@PLT
	movzbl	-952(%rbp), %eax
	movq	$0, -968(%rbp)
	movb	%al, -976(%rbp)
	testb	%al, %al
	je	.L1036
.L961:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L934
	call	_ZdlPv@PLT
.L934:
	movq	-936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L973
	call	_ZdlPv@PLT
.L973:
	movq	-944(%rbp), %r12
	testq	%r12, %r12
	je	.L927
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L927:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1037
	addq	$968, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L953:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L959
	.p2align 4,,10
	.p2align 3
.L963:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L963
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1031:
	leaq	-984(%rbp), %r15
	leaq	-992(%rbp), %r13
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-976(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-640(%rbp), %r8
	movl	$504, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-640(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L930
	call	_ZdlPv@PLT
.L930:
	movq	-984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L931
	movq	(%rdi), %rax
	call	*8(%rax)
.L931:
	movq	-968(%rbp), %r12
	testq	%r12, %r12
	je	.L934
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L933
	call	_ZdlPv@PLT
.L933:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L1036:
	leaq	-984(%rbp), %r13
	leaq	-992(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1008(%rbp), %rdi
	leaq	-976(%rbp), %rsi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC49(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-800(%rbp), %r8
	movl	$512, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L964
	call	_ZdlPv@PLT
.L964:
	movq	-984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L965
	movq	(%rdi), %rax
	call	*8(%rax)
.L965:
	movq	-968(%rbp), %r12
	testq	%r12, %r12
	je	.L961
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L967
	call	_ZdlPv@PLT
.L967:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L1032:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1033:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1037:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6406:
	.size	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test8TestBodyEv, .-_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test8TestBodyEv
	.type	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test8TestBodyEv, @function
_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test8TestBodyEv:
.LFB6413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-640(%rbp), %r14
	leaq	-960(%rbp), %rdi
	pushq	%r13
	movq	%r14, %rdx
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-624(%rbp), %r12
	pushq	%rbx
	subq	$968, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -640(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -624(%rbp)
	movb	$49, -616(%rbp)
	movq	$9, -632(%rbp)
	movb	$0, -615(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-640(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1039
	call	_ZdlPv@PLT
.L1039:
	movq	-944(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -968(%rbp)
	movb	%al, -976(%rbp)
	testb	%al, %al
	je	.L1142
	xorl	%r9d, %r9d
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rax
	movw	%r9w, -640(%rbp)
	movq	%rax, -632(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	-944(%rbp), %rdi
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movb	$49, -840(%rbp)
	movl	%eax, %r13d
	leaq	-848(%rbp), %rax
	movq	$9, -856(%rbp)
	movq	%rax, -1000(%rbp)
	movq	%rax, -864(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -848(%rbp)
	movq	-80(%rbp), %rax
	movb	$0, -839(%rbp)
	movw	%r10w, -88(%rbp)
	movw	%r11w, -640(%rbp)
	cmpq	-72(%rbp), %rax
	je	.L1046
	movq	%rax, -72(%rbp)
.L1046:
	movq	-632(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-896(%rbp), %r15
	call	uv_tcp_init@PLT
	movq	-864(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r13d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L1143
	leaq	-376(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L1144
	leaq	-792(%rbp), %r13
	xorl	%r8d, %r8d
	leaq	_ZL4loop(%rip), %rdi
	movq	%r13, %rsi
	movw	%r8w, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1049
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1049
.L1051:
	cmpb	$0, -800(%rbp)
	je	.L1145
	leaq	-992(%rbp), %r9
	leaq	-832(%rbp), %r15
	movb	$0, -976(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1008(%rbp)
	leaq	-984(%rbp), %rbx
	movq	$0, -968(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-976(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$115, %ecx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1008(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	leaq	-816(%rbp), %rbx
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1090
	call	_ZdlPv@PLT
.L1090:
	movq	-992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1052
	movq	(%rdi), %rax
	call	*8(%rax)
.L1052:
	movq	-968(%rbp), %r8
	testq	%r8, %r8
	je	.L1053
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1054
	movq	%r8, -1008(%rbp)
	call	_ZdlPv@PLT
	movq	-1008(%rbp), %r8
.L1054:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1053:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1057
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	$2, %esi
	leaq	_ZL4loop(%rip), %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1056
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1088
	.p2align 4,,10
	.p2align 3
.L1058:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1058
.L1088:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r12, %rdi
	call	uv_read_start@PLT
	leaq	-832(%rbp), %r15
	leaq	-816(%rbp), %rbx
.L1057:
	movq	-864(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L1059
	call	_ZdlPv@PLT
.L1059:
	leaq	-784(%rbp), %rax
	movl	$8283, %esi
	movl	$29811, %edi
	movb	$93, -782(%rbp)
	movq	%rax, -1000(%rbp)
	leaq	-800(%rbp), %rdx
	movq	%rax, -800(%rbp)
	movabsq	$7596498822829926959, %rax
	movw	%si, -784(%rbp)
	movq	%r15, %rsi
	movw	%di, -808(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -1008(%rbp)
	movq	$3, -792(%rbp)
	movb	$0, -781(%rbp)
	movq	%rbx, -832(%rbp)
	movq	%rax, -816(%rbp)
	movq	$10, -824(%rbp)
	movb	$0, -806(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	-800(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	-944(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movl	$8283, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	-1000(%rbp), %rax
	movw	%dx, -784(%rbp)
	movl	$29811, %ecx
	movq	-1008(%rbp), %rdx
	movb	$93, -782(%rbp)
	movq	%rax, -800(%rbp)
	movabsq	$7596498822829926959, %rax
	movq	$3, -792(%rbp)
	movb	$0, -781(%rbp)
	movq	%rbx, -832(%rbp)
	movq	%rax, -816(%rbp)
	movw	%cx, -808(%rbp)
	movq	$10, -824(%rbp)
	movb	$0, -806(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper15TestHttpRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	movq	-800(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	xorl	%eax, %eax
	movq	%r13, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1064
	leaq	_ZL4loop(%rip), %r12
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1146:
	movl	$1, %esi
	movq	%r12, %rdi
	call	uv_run@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1064
.L1066:
	cmpb	$0, -800(%rbp)
	je	.L1146
	leaq	-984(%rbp), %r14
	movb	$0, -976(%rbp)
	leaq	-992(%rbp), %r12
	movq	$0, -968(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-976(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$141, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1087
	call	_ZdlPv@PLT
.L1087:
	movq	-984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1067
	movq	(%rdi), %rax
	call	*8(%rax)
.L1067:
	movq	-968(%rbp), %r12
	testq	%r12, %r12
	je	.L1068
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1068:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1070
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1071:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1071
.L1070:
	xorl	%esi, %esi
	leaq	_ZL4loop(%rip), %rdi
	call	uv_run@PLT
	movzbl	-952(%rbp), %eax
	movq	$0, -968(%rbp)
	movb	%al, -976(%rbp)
	testb	%al, %al
	je	.L1147
.L1072:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1045
	call	_ZdlPv@PLT
.L1045:
	movq	-936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1084
	call	_ZdlPv@PLT
.L1084:
	movq	-944(%rbp), %r12
	testq	%r12, %r12
	je	.L1038
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1038:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1148
	addq	$968, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1064:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1070
	.p2align 4,,10
	.p2align 3
.L1074:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1074
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1142:
	leaq	-984(%rbp), %r15
	leaq	-992(%rbp), %r13
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-976(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-640(%rbp), %r8
	movl	$517, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-640(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1041
	call	_ZdlPv@PLT
.L1041:
	movq	-984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1042
	movq	(%rdi), %rax
	call	*8(%rax)
.L1042:
	movq	-968(%rbp), %r12
	testq	%r12, %r12
	je	.L1045
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1147:
	leaq	-984(%rbp), %r13
	leaq	-992(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1008(%rbp), %rdi
	leaq	-976(%rbp), %rsi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC49(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-800(%rbp), %r8
	movl	$525, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	cmpq	-1000(%rbp), %rdi
	je	.L1075
	call	_ZdlPv@PLT
.L1075:
	movq	-984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1076
	movq	(%rdi), %rax
	call	*8(%rax)
.L1076:
	movq	-968(%rbp), %r12
	testq	%r12, %r12
	je	.L1072
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1078
	call	_ZdlPv@PLT
.L1078:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1143:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1144:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6413:
	.size	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test8TestBodyEv, .-_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test8TestBodyEv
	.section	.rodata.str1.1
.LC50:
	.string	"(!server.done())"
	.text
	.align 2
	.p2align 4
	.globl	_ZN48InspectorSocketServerTest_ServerDoesNothing_Test8TestBodyEv
	.type	_ZN48InspectorSocketServerTest_ServerDoesNothing_Test8TestBodyEv, @function
_ZN48InspectorSocketServerTest_ServerDoesNothing_Test8TestBodyEv:
.LFB6385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-208(%rbp), %r14
	leaq	-304(%rbp), %rdi
	pushq	%r12
	movq	%r14, %rdx
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	-192(%rbp), %rbx
	subq	$304, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -208(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -192(%rbp)
	movb	$49, -184(%rbp)
	movq	$9, -200(%rbp)
	movb	$0, -183(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1150
	call	_ZdlPv@PLT
.L1150:
	movq	-288(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -312(%rbp)
	movb	%al, -320(%rbp)
	testb	%al, %al
	je	.L1251
	movq	-288(%rbp), %rdi
	leaq	-200(%rbp), %r12
	leaq	_ZL4loop(%rip), %r13
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movq	-288(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer20TerminateConnectionsEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%dx, -208(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1157:
	cmpb	$0, -208(%rbp)
	jne	.L1252
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
.L1162:
	movq	-288(%rbp), %rax
	movq	64(%rax), %rcx
	movq	56(%rax), %rdx
	cmpq	%rdx, %rcx
	jne	.L1157
	cmpq	$0, 120(%rax)
	jne	.L1157
.L1158:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %r13
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1163
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1164
.L1163:
	movzbl	-296(%rbp), %eax
	movq	$0, -312(%rbp)
	movb	%al, -320(%rbp)
	testb	%al, %al
	je	.L1253
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -208(%rbp)
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1254:
	cmpb	$0, -208(%rbp)
	jne	.L1176
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L1180:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L1254
.L1176:
	leaq	_ZL4loop(%rip), %rdi
	call	uv_loop_alive@PLT
	movq	$0, -312(%rbp)
	testl	%eax, %eax
	sete	-320(%rbp)
	jne	.L1255
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1156
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1188:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1188
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1192
	call	_ZdlPv@PLT
.L1192:
	movq	-288(%rbp), %r12
	testq	%r12, %r12
	je	.L1149
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1149:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1256
	addq	$304, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1252:
	.cfi_restore_state
	cmpq	%rdx, %rcx
	je	.L1160
.L1237:
	leaq	-328(%rbp), %r14
	movb	$0, -320(%rbp)
	leaq	-336(%rbp), %r13
	movq	$0, -312(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-320(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC50(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$467, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1165
	call	_ZdlPv@PLT
.L1165:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1166
	movq	(%rdi), %rax
	call	*8(%rax)
.L1166:
	movq	-312(%rbp), %r13
	testq	%r13, %r13
	je	.L1167
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1168
	call	_ZdlPv@PLT
.L1168:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1167:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1156
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1170:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1170
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1251:
	leaq	-328(%rbp), %r13
	leaq	-336(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC24(%rip), %r8
	movq	%r14, %rdi
	leaq	.LC23(%rip), %rcx
	leaq	-320(%rbp), %rsi
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-208(%rbp), %r8
	movl	$464, %ecx
.L1250:
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1172
	call	_ZdlPv@PLT
.L1172:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1173
	movq	(%rdi), %rax
	call	*8(%rax)
.L1173:
	movq	-312(%rbp), %r12
	testq	%r12, %r12
	je	.L1156
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1175
	call	_ZdlPv@PLT
.L1175:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1255:
	leaq	-328(%rbp), %r14
	leaq	-336(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-320(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$469, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1181
	call	_ZdlPv@PLT
.L1181:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1182
	movq	(%rdi), %rax
	call	*8(%rax)
.L1182:
	movq	-312(%rbp), %r13
	testq	%r13, %r13
	je	.L1183
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1184
	call	_ZdlPv@PLT
.L1184:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1183:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1156
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1186:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1186
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1253:
	leaq	-328(%rbp), %r13
	leaq	-336(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC24(%rip), %r8
	movq	%r14, %rdi
	leaq	.LC23(%rip), %rcx
	leaq	-320(%rbp), %rsi
	leaq	.LC49(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-208(%rbp), %r8
	movl	$468, %ecx
	jmp	.L1250
.L1160:
	cmpq	$0, 120(%rax)
	jne	.L1237
	jmp	.L1158
.L1256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6385:
	.size	_ZN48InspectorSocketServerTest_ServerDoesNothing_Test8TestBodyEv, .-_ZN48InspectorSocketServerTest_ServerDoesNothing_Test8TestBodyEv
	.section	.rodata.str1.1
.LC51:
	.string	"(!server.delegate_done)"
	.text
	.align 2
	.p2align 4
	.globl	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test8TestBodyEv
	.type	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test8TestBodyEv, @function
_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test8TestBodyEv:
.LFB6420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-640(%rbp), %r14
	leaq	-976(%rbp), %rdi
	pushq	%r13
	movq	%r14, %rdx
	.cfi_offset 13, -40
	leaq	-624(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$1000, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -640(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -624(%rbp)
	movb	$49, -616(%rbp)
	movq	$9, -632(%rbp)
	movb	$0, -615(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1258
	call	_ZdlPv@PLT
.L1258:
	movq	-960(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -984(%rbp)
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1418
	xorl	%r8d, %r8d
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rax
	movw	%r8w, -640(%rbp)
	movq	%rax, -632(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	-960(%rbp), %rdi
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movb	$49, -840(%rbp)
	movl	%eax, %r12d
	leaq	-848(%rbp), %rax
	movq	$9, -856(%rbp)
	movq	%rax, -1016(%rbp)
	movq	%rax, -864(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -848(%rbp)
	movq	-80(%rbp), %rax
	movb	$0, -839(%rbp)
	movw	%r9w, -88(%rbp)
	movw	%r10w, -640(%rbp)
	cmpq	-72(%rbp), %rax
	je	.L1265
	movq	%rax, -72(%rbp)
.L1265:
	movq	-632(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-896(%rbp), %r15
	call	uv_tcp_init@PLT
	movq	-864(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L1419
	leaq	-376(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L1420
	xorl	%edi, %edi
	leaq	-792(%rbp), %r12
	movw	%di, -800(%rbp)
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1268
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1421:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1268
.L1270:
	cmpb	$0, -800(%rbp)
	je	.L1421
	leaq	-1008(%rbp), %r9
	leaq	-992(%rbp), %rbx
	movb	$0, -992(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1024(%rbp)
	movq	$0, -984(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	%rbx, %rsi
	leaq	-832(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$115, %ecx
	movq	-832(%rbp), %r8
	leaq	-1000(%rbp), %rbx
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1024(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1338
	call	_ZdlPv@PLT
.L1338:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1271
	movq	(%rdi), %rax
	call	*8(%rax)
.L1271:
	movq	-984(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1272
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1273
	call	_ZdlPv@PLT
.L1273:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1272:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1276
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1275:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1275
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1336
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1277
.L1336:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r13, %rdi
	call	uv_read_start@PLT
.L1276:
	movq	-864(%rbp), %rdi
	cmpq	-1016(%rbp), %rdi
	je	.L1278
	call	_ZdlPv@PLT
.L1278:
	leaq	-880(%rbp), %rax
	movl	$25959, %esi
	leaq	-864(%rbp), %rdi
	movb	$116, -870(%rbp)
	movq	%rax, -1032(%rbp)
	movq	%rax, -896(%rbp)
	movabsq	$8241996531539468653, %rax
	movw	%si, -872(%rbp)
	movq	%r15, %rsi
	movq	%rax, -880(%rbp)
	movq	$11, -888(%rbp)
	movb	$0, -869(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movzbl	-86(%rbp), %eax
	movq	$0, -984(%rbp)
	xorl	$1, %eax
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1422
	movq	-864(%rbp), %rax
	movl	$1, %ecx
	movq	%r13, %rsi
	leaq	-912(%rbp), %rdx
	leaq	-280(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movb	$1, -86(%rbp)
	movq	%rax, -912(%rbp)
	movq	-856(%rbp), %rax
	movq	%rax, -904(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L1423
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%cx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -86(%rbp)
	je	.L1286
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1424:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -86(%rbp)
	je	.L1286
.L1288:
	cmpb	$0, -800(%rbp)
	je	.L1424
	leaq	-1008(%rbp), %r9
	leaq	-992(%rbp), %rbx
	movb	$0, -992(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1024(%rbp)
	leaq	-1000(%rbp), %r15
	movq	$0, -984(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$179, %ecx
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1024(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1340
	call	_ZdlPv@PLT
.L1340:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1289
	movq	(%rdi), %rax
	call	*8(%rax)
.L1289:
	movq	-984(%rbp), %r15
	testq	%r15, %r15
	je	.L1290
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1291
	call	_ZdlPv@PLT
.L1291:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1290:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1410
	leaq	_ZL4loop(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L1293:
	movl	$2, %esi
	movq	%r15, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1293
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1418:
	leaq	-1000(%rbp), %r15
	leaq	-1008(%rbp), %r12
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-992(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-640(%rbp), %r8
	movl	$530, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1260
	call	_ZdlPv@PLT
.L1260:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1261
	movq	(%rdi), %rax
	call	*8(%rax)
.L1261:
	movq	-984(%rbp), %r12
	testq	%r12, %r12
	je	.L1264
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1263
	call	_ZdlPv@PLT
.L1263:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1264:
	movq	-952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1329
	call	_ZdlPv@PLT
.L1329:
	movq	-960(%rbp), %r12
	testq	%r12, %r12
	je	.L1257
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1257:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1425
	addq	$1000, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1286:
	.cfi_restore_state
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1411
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1295:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1295
.L1411:
	leaq	-992(%rbp), %rbx
.L1410:
	leaq	-784(%rbp), %rax
	leaq	-800(%rbp), %r15
	movq	%rax, -1024(%rbp)
.L1296:
	movq	-864(%rbp), %rdi
	cmpq	-1016(%rbp), %rdi
	je	.L1297
	call	_ZdlPv@PLT
.L1297:
	movq	-896(%rbp), %rdi
	cmpq	-1032(%rbp), %rdi
	je	.L1298
	call	_ZdlPv@PLT
.L1298:
	movq	-1024(%rbp), %rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	$129, -992(%rbp)
	movq	%rax, -800(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-992(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movdqa	.LC52(%rip), %xmm0
	movq	%rax, -800(%rbp)
	movq	%rdx, -784(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC53(%rip), %xmm0
	movb	$10, 128(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC54(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC55(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	.LC56(%rip), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	.LC57(%rip), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	.LC58(%rip), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	.LC59(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movq	-992(%rbp), %rax
	movq	-800(%rbp), %rdx
	movq	%rax, -792(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-800(%rbp), %rdi
	cmpq	-1024(%rbp), %rdi
	je	.L1299
	call	_ZdlPv@PLT
.L1299:
	movq	-960(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movzbl	-968(%rbp), %eax
	movq	$0, -984(%rbp)
	xorl	$1, %eax
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1426
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%dx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1306
	leaq	_ZL4loop(%rip), %r13
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1427:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1306
.L1308:
	cmpb	$0, -800(%rbp)
	je	.L1427
	leaq	-1000(%rbp), %r14
	movb	$0, -992(%rbp)
	leaq	-1008(%rbp), %r13
	movq	$0, -984(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$141, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1335
	call	_ZdlPv@PLT
.L1335:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1309
	movq	(%rdi), %rax
	call	*8(%rax)
.L1309:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1310
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1311
	call	_ZdlPv@PLT
.L1311:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1310:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1312
	leaq	_ZL4loop(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L1313:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1313
.L1312:
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -800(%rbp)
	leaq	_ZL4loop(%rip), %r13
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -968(%rbp)
	je	.L1315
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1428:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -968(%rbp)
	jne	.L1314
.L1315:
	cmpb	$0, -800(%rbp)
	je	.L1428
	leaq	-1000(%rbp), %r14
	movb	$0, -992(%rbp)
	leaq	-1008(%rbp), %r13
	movq	$0, -984(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC51(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$538, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1332
	call	_ZdlPv@PLT
.L1332:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1318
	movq	(%rdi), %rax
	call	*8(%rax)
.L1318:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1319
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1320
	call	_ZdlPv@PLT
.L1320:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1319:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1305
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1322:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1322
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1264
	call	_ZdlPv@PLT
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1306:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %r13
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1312
	.p2align 4,,10
	.p2align 3
.L1316:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1316
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1305
	.p2align 4,,10
	.p2align 3
.L1323:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1323
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1422:
	leaq	-1008(%rbp), %r9
	leaq	-800(%rbp), %r15
	movq	%r9, %rdi
	leaq	-992(%rbp), %rbx
	movq	%r9, -1040(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$171, %ecx
	movq	-800(%rbp), %r8
	leaq	-1000(%rbp), %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rdi, -1024(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1040(%rbp), %r9
	movq	-1024(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-1024(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	leaq	-784(%rbp), %rax
	movq	%rax, -1024(%rbp)
	cmpq	%rax, %rdi
	je	.L1280
	call	_ZdlPv@PLT
.L1280:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1281
	movq	(%rdi), %rax
	call	*8(%rax)
.L1281:
	movq	-984(%rbp), %r8
	testq	%r8, %r8
	je	.L1296
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1283
	movq	%r8, -1040(%rbp)
	call	_ZdlPv@PLT
	movq	-1040(%rbp), %r8
.L1283:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1426:
	leaq	-1000(%rbp), %r13
	leaq	-1008(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	.LC49(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-800(%rbp), %r8
	movl	$536, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	cmpq	-1024(%rbp), %rdi
	je	.L1301
	call	_ZdlPv@PLT
.L1301:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1302
	movq	(%rdi), %rax
	call	*8(%rax)
.L1302:
	movq	-984(%rbp), %r12
	testq	%r12, %r12
	je	.L1305
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1304
	call	_ZdlPv@PLT
.L1304:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1419:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1420:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1423:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1425:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6420:
	.size	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test8TestBodyEv, .-_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test8TestBodyEv
	.type	_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test8TestBodyEv, @function
_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test8TestBodyEv:
.LFB6392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-640(%rbp), %r14
	leaq	-976(%rbp), %rdi
	pushq	%r13
	movq	%r14, %rdx
	.cfi_offset 13, -40
	leaq	-624(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$1016, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -640(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -624(%rbp)
	movb	$49, -616(%rbp)
	movq	$9, -632(%rbp)
	movb	$0, -615(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1430
	call	_ZdlPv@PLT
.L1430:
	movq	-960(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -984(%rbp)
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1631
	movq	-960(%rbp), %rdi
	leaq	-800(%rbp), %r15
	movabsq	$7596498822829926959, %rax
	leaq	-784(%rbp), %rbx
	movq	%rax, -784(%rbp)
	movl	$8283, %r12d
	movl	$29811, %eax
	movq	%r13, -640(%rbp)
	movw	%r12w, -624(%rbp)
	movb	$93, -622(%rbp)
	movq	$3, -632(%rbp)
	movb	$0, -621(%rbp)
	movq	%r15, -1016(%rbp)
	movq	%rbx, -1024(%rbp)
	movq	%rbx, -800(%rbp)
	movw	%ax, -776(%rbp)
	movq	$10, -792(%rbp)
	movb	$0, -774(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	%eax, %edi
	call	_ZN12_GLOBAL__N_1L15TestHttpRequestEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_
	movq	-800(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1437
	call	_ZdlPv@PLT
.L1437:
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1438
	call	_ZdlPv@PLT
.L1438:
	movl	$8283, %ebx
	movq	-960(%rbp), %rdi
	movq	%r13, -640(%rbp)
	movw	%bx, -624(%rbp)
	movq	-1024(%rbp), %rbx
	movb	$93, -622(%rbp)
	movq	$3, -632(%rbp)
	movb	$0, -621(%rbp)
	movq	%rbx, -800(%rbp)
	movl	$1869834799, -784(%rbp)
	movb	$110, -780(%rbp)
	movq	$5, -792(%rbp)
	movb	$0, -779(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	movq	-1016(%rbp), %rsi
	movq	%r14, %rdx
	movl	%eax, %edi
	call	_ZN12_GLOBAL__N_1L15TestHttpRequestEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES7_
	movq	-800(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1439
	call	_ZdlPv@PLT
.L1439:
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1440
	call	_ZdlPv@PLT
.L1440:
	xorl	%r9d, %r9d
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rax
	movw	%r9w, -640(%rbp)
	movq	%rax, -632(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	-960(%rbp), %rdi
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movb	$49, -840(%rbp)
	movl	%eax, %r12d
	leaq	-848(%rbp), %rax
	movq	$9, -856(%rbp)
	movq	%rax, -1032(%rbp)
	movq	%rax, -864(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -848(%rbp)
	movq	-80(%rbp), %rax
	movb	$0, -839(%rbp)
	movw	%r10w, -88(%rbp)
	movw	%r11w, -640(%rbp)
	cmpq	-72(%rbp), %rax
	je	.L1441
	movq	%rax, -72(%rbp)
.L1441:
	movq	-632(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-896(%rbp), %r15
	call	uv_tcp_init@PLT
	movq	-864(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L1632
	leaq	-376(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L1633
	leaq	-792(%rbp), %r12
	xorl	%r8d, %r8d
	leaq	_ZL4loop(%rip), %rdi
	movq	%r12, %rsi
	movw	%r8w, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1444
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1634:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1444
.L1446:
	cmpb	$0, -800(%rbp)
	je	.L1634
	leaq	-1008(%rbp), %r9
	movb	$0, -992(%rbp)
	leaq	-1000(%rbp), %rbx
	movq	%r9, %rdi
	movq	%r9, -1040(%rbp)
	movq	$0, -984(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-992(%rbp), %rax
	leaq	-832(%rbp), %rdi
	movq	%rax, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$115, %ecx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1040(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1537
	call	_ZdlPv@PLT
.L1537:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1447
	movq	(%rdi), %rax
	call	*8(%rax)
.L1447:
	movq	-984(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1448
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1449
	call	_ZdlPv@PLT
.L1449:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1448:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1452
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1451:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1451
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1535
	.p2align 4,,10
	.p2align 3
.L1453:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1453
.L1535:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r13, %rdi
	call	uv_read_start@PLT
.L1452:
	movq	-864(%rbp), %rdi
	cmpq	-1032(%rbp), %rdi
	je	.L1454
	call	_ZdlPv@PLT
.L1454:
	leaq	-880(%rbp), %rax
	movq	%r15, %rsi
	leaq	-864(%rbp), %rdi
	movl	$1763734629, -872(%rbp)
	movq	%rax, -1048(%rbp)
	movq	%rax, -896(%rbp)
	movabsq	$7454127484640521825, %rax
	movq	%rax, -880(%rbp)
	movb	$100, -868(%rbp)
	movq	$13, -888(%rbp)
	movb	$0, -867(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movzbl	-86(%rbp), %eax
	movq	$0, -984(%rbp)
	xorl	$1, %eax
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1635
	movq	-864(%rbp), %rax
	movl	$1, %ecx
	movq	%r13, %rsi
	leaq	-912(%rbp), %rdx
	leaq	-280(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movb	$1, -86(%rbp)
	movq	%rax, -912(%rbp)
	movq	-856(%rbp), %rax
	movq	%rax, -904(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L1636
	xorl	%edi, %edi
	movq	%r12, %rsi
	movw	%di, -800(%rbp)
	leaq	_ZL4loop(%rip), %rdi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -86(%rbp)
	je	.L1462
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1637:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -86(%rbp)
	je	.L1462
.L1464:
	cmpb	$0, -800(%rbp)
	je	.L1637
	leaq	-1008(%rbp), %rbx
	movb	$0, -992(%rbp)
	leaq	-1000(%rbp), %r15
	movq	$0, -984(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-992(%rbp), %rax
	leaq	-832(%rbp), %rdi
	movq	%rax, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	movq	%rax, -1040(%rbp)
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$179, %ecx
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1539
	call	_ZdlPv@PLT
.L1539:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1465
	movq	(%rdi), %rax
	call	*8(%rax)
.L1465:
	movq	-984(%rbp), %r15
	testq	%r15, %r15
	je	.L1466
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1467
	call	_ZdlPv@PLT
.L1467:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1466:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1472
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1469:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1469
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1631:
	leaq	-1000(%rbp), %r15
	leaq	-1008(%rbp), %r12
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-992(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-640(%rbp), %r8
	movl	$474, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1432
	call	_ZdlPv@PLT
.L1432:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1433
	movq	(%rdi), %rax
	call	*8(%rax)
.L1433:
	movq	-984(%rbp), %r12
	testq	%r12, %r12
	je	.L1436
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1435
	call	_ZdlPv@PLT
.L1435:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1436:
	movq	-952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1528
	call	_ZdlPv@PLT
.L1528:
	movq	-960(%rbp), %r12
	testq	%r12, %r12
	je	.L1429
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1429:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1638
	addq	$1016, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore_state
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1623
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1471:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1471
.L1623:
	leaq	-992(%rbp), %rax
	movq	%rax, -1040(%rbp)
.L1472:
	movq	-864(%rbp), %rdi
	cmpq	-1032(%rbp), %rdi
	je	.L1473
	call	_ZdlPv@PLT
.L1473:
	movq	-896(%rbp), %rdi
	cmpq	-1048(%rbp), %rdi
	je	.L1474
	call	_ZdlPv@PLT
.L1474:
	movq	-1016(%rbp), %r15
	movq	-1024(%rbp), %rbx
	xorl	%edx, %edx
	movq	$24, -992(%rbp)
	movq	-1040(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rbx, -800(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-992(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movdqa	.LC60(%rip), %xmm0
	movq	%rax, -800(%rbp)
	movabsq	$8391162085809410592, %rcx
	movq	%rdx, -784(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-992(%rbp), %rax
	movq	-800(%rbp), %rdx
	movq	%rax, -792(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-800(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1475
	call	_ZdlPv@PLT
.L1475:
	xorl	%esi, %esi
	leaq	_ZL4loop(%rip), %rdi
	movw	%si, -800(%rbp)
	movq	%r12, %rsi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -639(%rbp)
	jne	.L1476
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1639:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	jne	.L1476
.L1478:
	cmpb	$0, -800(%rbp)
	je	.L1639
	leaq	-1008(%rbp), %r14
	movb	$0, -992(%rbp)
	leaq	-1000(%rbp), %r13
	movq	$0, -984(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1040(%rbp), %rsi
	leaq	-832(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC38(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$152, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1534
	call	_ZdlPv@PLT
.L1534:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1479
	movq	(%rdi), %rax
	call	*8(%rax)
.L1479:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1480
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1481
	call	_ZdlPv@PLT
.L1481:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1480:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1484
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1483:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1483
.L1484:
	movq	-960(%rbp), %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movq	-960(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer20TerminateConnectionsEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%dx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1495:
	cmpb	$0, -800(%rbp)
	jne	.L1640
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L1500:
	movq	-960(%rbp), %rax
	movq	64(%rax), %rcx
	movq	56(%rax), %rdx
	cmpq	%rdx, %rcx
	jne	.L1495
	cmpq	$0, 120(%rax)
	jne	.L1495
.L1496:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1501
	.p2align 4,,10
	.p2align 3
.L1502:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1502
.L1501:
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -800(%rbp)
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1641:
	cmpb	$0, -800(%rbp)
	jne	.L1510
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L1514:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L1641
.L1510:
	leaq	_ZL4loop(%rip), %rdi
	call	uv_loop_alive@PLT
	movq	$0, -984(%rbp)
	testl	%eax, %eax
	sete	-992(%rbp)
	jne	.L1642
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1509
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1522:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1522
.L1509:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1436
	call	_ZdlPv@PLT
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1532
	.p2align 4,,10
	.p2align 3
.L1485:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1485
.L1532:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%cx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1486
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1643:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1486
.L1488:
	cmpb	$0, -800(%rbp)
	je	.L1643
	leaq	-1000(%rbp), %r14
	movb	$0, -992(%rbp)
	leaq	-1008(%rbp), %r13
	movq	$0, -984(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1040(%rbp), %rsi
	leaq	-832(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$141, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1531
	call	_ZdlPv@PLT
.L1531:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1489
	movq	(%rdi), %rax
	call	*8(%rax)
.L1489:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1490
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1491
	call	_ZdlPv@PLT
.L1491:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1490:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1484
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1493:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1493
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1484
	.p2align 4,,10
	.p2align 3
.L1494:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1494
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1640:
	cmpq	%rdx, %rcx
	je	.L1498
.L1624:
	leaq	-1000(%rbp), %r14
	movb	$0, -992(%rbp)
	leaq	-1008(%rbp), %r13
	movq	$0, -984(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1040(%rbp), %rsi
	leaq	-832(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC50(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$486, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1503
	call	_ZdlPv@PLT
.L1503:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1504
	movq	(%rdi), %rax
	call	*8(%rax)
.L1504:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1505
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1506
	call	_ZdlPv@PLT
.L1506:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1505:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1509
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1508
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1635:
	leaq	-1008(%rbp), %rbx
	leaq	-1000(%rbp), %r15
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-992(%rbp), %rax
	movq	-1016(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	movq	%rax, %rsi
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	movq	%rax, -1040(%rbp)
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-800(%rbp), %r8
	movl	$171, %ecx
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	cmpq	-1024(%rbp), %rdi
	je	.L1456
	call	_ZdlPv@PLT
.L1456:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1457
	movq	(%rdi), %rax
	call	*8(%rax)
.L1457:
	movq	-984(%rbp), %r15
	testq	%r15, %r15
	je	.L1472
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1459
	call	_ZdlPv@PLT
.L1459:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1642:
	leaq	-1000(%rbp), %r14
	leaq	-1008(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-1040(%rbp), %rsi
	leaq	-832(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$487, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1515
	call	_ZdlPv@PLT
.L1515:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1516
	movq	(%rdi), %rax
	call	*8(%rax)
.L1516:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1517
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1518
	call	_ZdlPv@PLT
.L1518:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1517:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1509
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1520:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1520
	jmp	.L1509
.L1498:
	cmpq	$0, 120(%rax)
	jne	.L1624
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1632:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1633:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1636:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6392:
	.size	_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test8TestBodyEv, .-_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test8TestBodyEv
	.section	.rodata.str1.1
.LC61:
	.string	"server1->Start()"
.LC62:
	.string	"server2->Start()"
.LC63:
	.string	"(!server1.done())"
.LC64:
	.string	"server1.delegate_done"
	.text
	.align 2
	.p2align 4
	.globl	_ZN48InspectorSocketServerTest_ServerCannotStart_Test8TestBodyEv
	.type	_ZN48InspectorSocketServerTest_ServerCannotStart_Test8TestBodyEv, @function
_ZN48InspectorSocketServerTest_ServerCannotStart_Test8TestBodyEv:
.LFB6399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-368(%rbp), %rdi
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-208(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdx
	.cfi_offset 3, -48
	leaq	-192(%rbp), %rbx
	subq	$368, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -208(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -192(%rbp)
	movb	$49, -184(%rbp)
	movq	$9, -200(%rbp)
	movb	$0, -183(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1645
	call	_ZdlPv@PLT
.L1645:
	movq	-352(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -296(%rbp)
	movb	%al, -304(%rbp)
	testb	%al, %al
	je	.L1779
	movq	-352(%rbp), %rdi
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	leaq	-304(%rbp), %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	%eax, %ecx
	movabsq	$3328210909095473713, %rax
	movq	%rbx, -208(%rbp)
	movq	%rax, -192(%rbp)
	movb	$49, -184(%rbp)
	movq	$9, -200(%rbp)
	movb	$0, -183(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1652
	call	_ZdlPv@PLT
.L1652:
	movq	-288(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -376(%rbp)
	xorl	$1, %eax
	movb	%al, -384(%rbp)
	testb	%al, %al
	je	.L1780
	movq	-352(%rbp), %rdi
	leaq	-200(%rbp), %r13
	leaq	_ZL4loop(%rip), %r14
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movq	-352(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer20TerminateConnectionsEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%dx, -208(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1659:
	cmpb	$0, -208(%rbp)
	jne	.L1781
	movl	$1, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
.L1664:
	movq	-352(%rbp), %rax
	movq	64(%rax), %rcx
	movq	56(%rax), %rdx
	cmpq	%rdx, %rcx
	jne	.L1659
	cmpq	$0, 120(%rax)
	jne	.L1659
.L1660:
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %r14
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1665
	.p2align 4,,10
	.p2align 3
.L1666:
	movl	$2, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1666
.L1665:
	movzbl	-360(%rbp), %eax
	movq	$0, -376(%rbp)
	movb	%al, -384(%rbp)
	testb	%al, %al
	je	.L1782
	xorl	%eax, %eax
	movq	%r13, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -208(%rbp)
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r13, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1783:
	cmpb	$0, -208(%rbp)
	jne	.L1678
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L1682:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L1783
.L1678:
	leaq	_ZL4loop(%rip), %rdi
	call	uv_loop_alive@PLT
	movq	$0, -376(%rbp)
	testl	%eax, %eax
	sete	-384(%rbp)
	jne	.L1784
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1658
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1690:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1690
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	-280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1696
	call	_ZdlPv@PLT
.L1696:
	movq	-288(%rbp), %r12
	testq	%r12, %r12
	je	.L1651
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1651:
	movq	-344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1698
	call	_ZdlPv@PLT
.L1698:
	movq	-352(%rbp), %r12
	testq	%r12, %r12
	je	.L1644
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1644:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1785
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1780:
	.cfi_restore_state
	leaq	-392(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%r12, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	-384(%rbp), %rsi
	leaq	.LC62(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-208(%rbp), %r8
	movl	$494, %ecx
	leaq	-400(%rbp), %r12
.L1777:
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1674
	call	_ZdlPv@PLT
.L1674:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1675
	movq	(%rdi), %rax
	call	*8(%rax)
.L1675:
	movq	-376(%rbp), %r12
	testq	%r12, %r12
	je	.L1658
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1677
	call	_ZdlPv@PLT
.L1677:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1779:
	leaq	-384(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	%r12, %rdi
	leaq	-304(%rbp), %rsi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC61(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$492, %ecx
	movq	-208(%rbp), %r8
	leaq	-392(%rbp), %r12
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1647
	call	_ZdlPv@PLT
.L1647:
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1648
	movq	(%rdi), %rax
	call	*8(%rax)
.L1648:
	movq	-296(%rbp), %r12
	testq	%r12, %r12
	je	.L1651
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1650
	call	_ZdlPv@PLT
.L1650:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1781:
	cmpq	%rdx, %rcx
	je	.L1662
.L1761:
	leaq	-392(%rbp), %r14
	movb	$0, -384(%rbp)
	leaq	-400(%rbp), %r12
	movq	$0, -376(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-384(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC63(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$497, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1667
	call	_ZdlPv@PLT
.L1667:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1668
	movq	(%rdi), %rax
	call	*8(%rax)
.L1668:
	movq	-376(%rbp), %r12
	testq	%r12, %r12
	je	.L1669
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1670
	call	_ZdlPv@PLT
.L1670:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1669:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1658
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1672:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1672
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1782:
	leaq	-392(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC24(%rip), %r8
	movq	%r12, %rdi
	leaq	.LC23(%rip), %rcx
	leaq	-384(%rbp), %rsi
	leaq	.LC64(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	leaq	-400(%rbp), %r12
	movl	$498, %ecx
	movq	-208(%rbp), %r8
	jmp	.L1777
.L1662:
	cmpq	$0, 120(%rax)
	jne	.L1761
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1784:
	leaq	-392(%rbp), %r14
	leaq	-400(%rbp), %r12
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-240(%rbp), %rdi
	leaq	-384(%rbp), %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-240(%rbp), %r8
	movl	$499, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1683
	call	_ZdlPv@PLT
.L1683:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1684
	movq	(%rdi), %rax
	call	*8(%rax)
.L1684:
	movq	-376(%rbp), %r12
	testq	%r12, %r12
	je	.L1685
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1686
	call	_ZdlPv@PLT
.L1686:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1685:
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	cmpb	$0, -207(%rbp)
	jne	.L1658
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1688:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -207(%rbp)
	je	.L1688
	jmp	.L1658
.L1785:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6399:
	.size	_ZN48InspectorSocketServerTest_ServerCannotStart_Test8TestBodyEv, .-_ZN48InspectorSocketServerTest_ServerCannotStart_Test8TestBodyEv
	.section	.rodata.str1.1
.LC65:
	.string	"No IPv6 network detected\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN42InspectorSocketServerTest_BindsToIpV6_Test8TestBodyEv
	.type	_ZN42InspectorSocketServerTest_BindsToIpV6_Test8TestBodyEv, @function
_ZN42InspectorSocketServerTest_BindsToIpV6_Test8TestBodyEv:
.LFB6442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-992(%rbp), %r14
	pushq	%r13
	movq	%r14, %rsi
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-976(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$1000, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -976(%rbp)
	movl	$0, -992(%rbp)
	call	uv_interface_addresses@PLT
	testl	%eax, %eax
	jne	.L1788
	movl	-992(%rbp), %esi
	movq	-976(%rbp), %rdi
	testl	%esi, %esi
	jle	.L1789
	leal	-1(%rsi), %edx
	leaq	20(%rdi), %rax
	xorl	%ebx, %ebx
	leaq	(%rdx,%rdx,4), %rdx
	salq	$4, %rdx
	leaq	100(%rdi,%rdx), %rcx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L1792:
	cmpw	$10, (%rax)
	cmove	%edx, %ebx
	addq	$80, %rax
	cmpq	%rcx, %rax
	jne	.L1792
	call	uv_free_interface_addresses@PLT
	testb	%bl, %bl
	je	.L1788
	movq	%r12, %rdi
	movl	$14906, %ebx
	xorl	%ecx, %ecx
	movl	$1, %esi
	leaq	-640(%rbp), %r15
	leaq	-624(%rbp), %r13
	movw	%bx, -624(%rbp)
	movq	%r15, %rdx
	movq	%r13, -640(%rbp)
	movq	$2, -632(%rbp)
	movb	$0, -622(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1795
	call	_ZdlPv@PLT
.L1795:
	movq	-960(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -984(%rbp)
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1956
	xorl	%r8d, %r8d
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	_ZL4loop(%rip), %rax
	movw	%r8w, -640(%rbp)
	movq	%rax, -632(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	-960(%rbp), %rdi
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	movl	$14906, %r9d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movl	%eax, %r12d
	movw	%r10w, -88(%rbp)
	leaq	-848(%rbp), %rax
	movq	%rax, -1024(%rbp)
	movq	%rax, -864(%rbp)
	movq	-80(%rbp), %rax
	movw	%r9w, -848(%rbp)
	movb	$49, -846(%rbp)
	movq	$3, -856(%rbp)
	movb	$0, -845(%rbp)
	movw	%r11w, -640(%rbp)
	cmpq	-72(%rbp), %rax
	je	.L1802
	movq	%rax, -72(%rbp)
.L1802:
	movq	-632(%rbp), %rdi
	movq	%r13, %rsi
	call	uv_tcp_init@PLT
	leaq	-896(%rbp), %rax
	movq	-864(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, %rdx
	movq	%rax, -1016(%rbp)
	call	uv_ip6_addr@PLT
	testl	%eax, %eax
	jne	.L1957
	movq	-1016(%rbp), %rdx
	leaq	-376(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r13, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L1958
	xorl	%edi, %edi
	leaq	-792(%rbp), %r12
	movw	%di, -800(%rbp)
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1805
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1959:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1805
.L1807:
	cmpb	$0, -800(%rbp)
	je	.L1959
	leaq	-1008(%rbp), %r9
	movb	$0, -992(%rbp)
	leaq	-1000(%rbp), %rbx
	movq	%r9, %rdi
	movq	%r9, -1032(%rbp)
	movq	$0, -984(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$115, %ecx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1032(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1873
	call	_ZdlPv@PLT
.L1873:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1808
	movq	(%rdi), %rax
	call	*8(%rax)
.L1808:
	movq	-984(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1809
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1810
	call	_ZdlPv@PLT
.L1810:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1809:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1812
	.p2align 4,,10
	.p2align 3
.L1811:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1811
	movq	-864(%rbp), %rdi
	cmpq	-1024(%rbp), %rdi
	je	.L1815
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1815:
	movl	$25959, %esi
	leaq	-880(%rbp), %rax
	leaq	-864(%rbp), %rdi
	movb	$116, -870(%rbp)
	movw	%si, -872(%rbp)
	movq	-1016(%rbp), %rsi
	movq	%rax, -1032(%rbp)
	movq	%rax, -896(%rbp)
	movabsq	$8241996531539468653, %rax
	movq	%rax, -880(%rbp)
	movq	$11, -888(%rbp)
	movb	$0, -869(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movzbl	-86(%rbp), %eax
	movq	$0, -984(%rbp)
	xorl	$1, %eax
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1960
	movq	-864(%rbp), %rax
	movl	$1, %ecx
	movq	%r13, %rsi
	leaq	-912(%rbp), %rdx
	leaq	-280(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movb	$1, -86(%rbp)
	movq	%rax, -912(%rbp)
	movq	-856(%rbp), %rax
	movq	%rax, -904(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L1961
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%cx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -86(%rbp)
	je	.L1823
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1962:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -86(%rbp)
	je	.L1823
.L1825:
	cmpb	$0, -800(%rbp)
	je	.L1962
	leaq	-1008(%rbp), %r9
	movb	$0, -992(%rbp)
	leaq	-1000(%rbp), %rbx
	movq	%r9, %rdi
	movq	%r9, -1016(%rbp)
	movq	$0, -984(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$179, %ecx
	movq	%rbx, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1016(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1875
	call	_ZdlPv@PLT
.L1875:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1826
	movq	(%rdi), %rax
	call	*8(%rax)
.L1826:
	movq	-984(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1827
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1828
	call	_ZdlPv@PLT
.L1828:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1827:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1949
	.p2align 4,,10
	.p2align 3
.L1829:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1829
.L1949:
	leaq	-784(%rbp), %rax
	leaq	-800(%rbp), %rbx
	movq	%rax, -1016(%rbp)
.L1832:
	movq	-864(%rbp), %rdi
	cmpq	-1024(%rbp), %rdi
	je	.L1834
	call	_ZdlPv@PLT
.L1834:
	movq	-896(%rbp), %rdi
	cmpq	-1032(%rbp), %rdi
	je	.L1835
	call	_ZdlPv@PLT
.L1835:
	movq	-1016(%rbp), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	$129, -992(%rbp)
	movq	%rax, -800(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-992(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movdqa	.LC52(%rip), %xmm0
	movq	%rax, -800(%rbp)
	movq	%rdx, -784(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC53(%rip), %xmm0
	movb	$10, 128(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC54(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC55(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	.LC56(%rip), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	.LC57(%rip), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	.LC58(%rip), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	.LC59(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movq	-992(%rbp), %rax
	movq	-800(%rbp), %rdx
	movq	%rax, -792(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-800(%rbp), %rdi
	cmpq	-1016(%rbp), %rdi
	je	.L1836
	call	_ZdlPv@PLT
.L1836:
	movq	-960(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movzbl	-968(%rbp), %eax
	movq	$0, -984(%rbp)
	xorl	$1, %eax
	movb	%al, -992(%rbp)
	testb	%al, %al
	je	.L1963
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%dx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1843
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1964:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -640(%rbp)
	jne	.L1843
.L1845:
	cmpb	$0, -800(%rbp)
	je	.L1964
	leaq	-1000(%rbp), %r15
	movb	$0, -992(%rbp)
	leaq	-1008(%rbp), %r13
	movq	$0, -984(%rbp)
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$141, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1870
	call	_ZdlPv@PLT
.L1870:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1846
	movq	(%rdi), %rax
	call	*8(%rax)
.L1846:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1847
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1848
	call	_ZdlPv@PLT
.L1848:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1847:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1852
	.p2align 4,,10
	.p2align 3
.L1849:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1849
.L1852:
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -800(%rbp)
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -968(%rbp)
	je	.L1851
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1965:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -968(%rbp)
	jne	.L1850
.L1851:
	cmpb	$0, -800(%rbp)
	je	.L1965
	leaq	-1000(%rbp), %r15
	movb	$0, -992(%rbp)
	leaq	-1008(%rbp), %r13
	movq	$0, -984(%rbp)
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC51(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$594, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1867
	call	_ZdlPv@PLT
.L1867:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1855
	movq	(%rdi), %rax
	call	*8(%rax)
.L1855:
	movq	-984(%rbp), %r13
	testq	%r13, %r13
	je	.L1856
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1857
	call	_ZdlPv@PLT
.L1857:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1856:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1842
	.p2align 4,,10
	.p2align 3
.L1858:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1858
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1801
	call	_ZdlPv@PLT
.L1801:
	movq	-952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1865
	call	_ZdlPv@PLT
.L1865:
	movq	-960(%rbp), %r12
	testq	%r12, %r12
	je	.L1786
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1786:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1966
	addq	$1000, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1956:
	.cfi_restore_state
	leaq	-1000(%rbp), %rbx
	leaq	-1008(%rbp), %r12
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC24(%rip), %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-640(%rbp), %r8
	movl	$586, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1797
	call	_ZdlPv@PLT
.L1797:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1798
	movq	(%rdi), %rax
	call	*8(%rax)
.L1798:
	movq	-984(%rbp), %r12
	testq	%r12, %r12
	je	.L1801
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1800
	call	_ZdlPv@PLT
.L1800:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1789:
	call	uv_free_interface_addresses@PLT
	.p2align 4,,10
	.p2align 3
.L1788:
	movq	stderr(%rip), %rcx
	movl	$25, %edx
	movl	$1, %esi
	leaq	.LC65(%rip), %rdi
	call	fwrite@PLT
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1805:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1871
	.p2align 4,,10
	.p2align 3
.L1814:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1814
.L1871:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r13, %rdi
	call	uv_read_start@PLT
.L1812:
	movq	-864(%rbp), %rdi
	cmpq	-1024(%rbp), %rdi
	je	.L1815
	call	_ZdlPv@PLT
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1949
	.p2align 4,,10
	.p2align 3
.L1831:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1831
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1842
	.p2align 4,,10
	.p2align 3
.L1860:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1860
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1843:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1852
	.p2align 4,,10
	.p2align 3
.L1853:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1853
	jmp	.L1852
	.p2align 4,,10
	.p2align 3
.L1960:
	leaq	-1008(%rbp), %r9
	leaq	-800(%rbp), %rbx
	movq	%r9, %rdi
	movq	%r9, -1040(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$171, %ecx
	movq	-800(%rbp), %r8
	leaq	-1000(%rbp), %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rdi, -1016(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1040(%rbp), %r9
	movq	-1016(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-1016(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	leaq	-784(%rbp), %rax
	movq	%rax, -1016(%rbp)
	cmpq	%rax, %rdi
	je	.L1817
	call	_ZdlPv@PLT
.L1817:
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1818
	movq	(%rdi), %rax
	call	*8(%rax)
.L1818:
	movq	-984(%rbp), %r8
	testq	%r8, %r8
	je	.L1832
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1820
	movq	%r8, -1040(%rbp)
	call	_ZdlPv@PLT
	movq	-1040(%rbp), %r8
.L1820:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1963:
	leaq	-1000(%rbp), %r13
	leaq	-1008(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	.LC49(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-800(%rbp), %r8
	movl	$592, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	cmpq	-1016(%rbp), %rdi
	je	.L1838
	call	_ZdlPv@PLT
.L1838:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1839
	movq	(%rdi), %rax
	call	*8(%rax)
.L1839:
	movq	-984(%rbp), %r12
	testq	%r12, %r12
	je	.L1842
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1841
	call	_ZdlPv@PLT
.L1841:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1957:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1958:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1961:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1966:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6442:
	.size	_ZN42InspectorSocketServerTest_BindsToIpV6_Test8TestBodyEv, .-_ZN42InspectorSocketServerTest_BindsToIpV6_Test8TestBodyEv
	.align 2
	.p2align 4
	.globl	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test8TestBodyEv
	.type	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test8TestBodyEv, @function
_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test8TestBodyEv:
.LFB6427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-640(%rbp), %r14
	leaq	-1024(%rbp), %rdi
	pushq	%r13
	movq	%r14, %rdx
	.cfi_offset 13, -40
	leaq	-624(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$1048, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -640(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -624(%rbp)
	movb	$49, -616(%rbp)
	movq	$9, -632(%rbp)
	movb	$0, -615(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1968
	call	_ZdlPv@PLT
.L1968:
	movq	-1008(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -1032(%rbp)
	movb	%al, -1040(%rbp)
	testb	%al, %al
	je	.L2186
	leaq	_ZL4loop(%rip), %rax
	xorl	%ebx, %ebx
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	movq	%rax, -632(%rbp)
	movl	$31, %ecx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movw	%bx, -640(%rbp)
	rep stosq
	movq	-1008(%rbp), %rdi
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	movb	$49, -840(%rbp)
	movl	%eax, %r12d
	leaq	-848(%rbp), %rax
	movw	%r15w, -88(%rbp)
	movq	%rax, -1072(%rbp)
	movq	%rax, -864(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -848(%rbp)
	xorl	%eax, %eax
	movw	%ax, -640(%rbp)
	movq	-80(%rbp), %rax
	movq	$9, -856(%rbp)
	movb	$0, -839(%rbp)
	cmpq	-72(%rbp), %rax
	je	.L1975
	movq	%rax, -72(%rbp)
.L1975:
	movq	-632(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-896(%rbp), %r15
	call	uv_tcp_init@PLT
	movq	-864(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L2187
	leaq	-376(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L2188
	leaq	-792(%rbp), %r12
	xorl	%r11d, %r11d
	leaq	_ZL4loop(%rip), %rdi
	movq	%r12, %rsi
	movw	%r11w, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1978
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1980
	.p2align 4,,10
	.p2align 3
.L2189:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -88(%rbp)
	jne	.L1978
.L1980:
	cmpb	$0, -800(%rbp)
	je	.L2189
	leaq	-1056(%rbp), %r9
	leaq	-1040(%rbp), %rbx
	movb	$0, -1040(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1064(%rbp)
	movq	$0, -1032(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	movq	%rbx, %rsi
	leaq	-832(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$115, %ecx
	movq	-832(%rbp), %r8
	leaq	-1048(%rbp), %rbx
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1064(%rbp), %r9
	movq	%rbx, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2081
	call	_ZdlPv@PLT
.L2081:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1981
	movq	(%rdi), %rax
	call	*8(%rax)
.L1981:
	movq	-1032(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1982
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1983
	call	_ZdlPv@PLT
.L1983:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1982:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L1986
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L1985:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1985
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L1978:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2079
	.p2align 4,,10
	.p2align 3
.L1987:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L1987
.L2079:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r13, %rdi
	call	uv_read_start@PLT
.L1986:
	movq	-864(%rbp), %rdi
	cmpq	-1072(%rbp), %rdi
	je	.L1988
	call	_ZdlPv@PLT
.L1988:
	leaq	-912(%rbp), %rax
	movl	$25959, %r10d
	movq	%r15, %rdi
	movb	$116, -902(%rbp)
	movq	%rax, -1080(%rbp)
	leaq	-928(%rbp), %rsi
	movq	%rax, -928(%rbp)
	movabsq	$8241996531539468653, %rax
	movq	%rax, -912(%rbp)
	movw	%r10w, -904(%rbp)
	movq	$11, -920(%rbp)
	movb	$0, -901(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movzbl	-86(%rbp), %eax
	movq	$0, -1032(%rbp)
	xorl	$1, %eax
	movb	%al, -1040(%rbp)
	testb	%al, %al
	je	.L2190
	movq	-896(%rbp), %rax
	movl	$1, %ecx
	movq	%r13, %rsi
	leaq	-960(%rbp), %rdx
	leaq	-280(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movb	$1, -86(%rbp)
	movq	%rax, -960(%rbp)
	movq	-888(%rbp), %rax
	movq	%rax, -952(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L2023
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%r9w, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -86(%rbp)
	je	.L1996
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L2191:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -86(%rbp)
	je	.L1996
.L1998:
	cmpb	$0, -800(%rbp)
	je	.L2191
	leaq	-1056(%rbp), %r9
	leaq	-1040(%rbp), %rbx
	movb	$0, -1040(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1064(%rbp)
	leaq	-1048(%rbp), %r15
	movq	$0, -1032(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$179, %ecx
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1064(%rbp), %r9
	movq	%r15, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2085
	call	_ZdlPv@PLT
.L2085:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1999
	movq	(%rdi), %rax
	call	*8(%rax)
.L1999:
	movq	-1032(%rbp), %r15
	testq	%r15, %r15
	je	.L2000
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2001
	call	_ZdlPv@PLT
.L2001:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2000:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2178
	leaq	_ZL4loop(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L2003:
	movl	$2, %esi
	movq	%r15, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2003
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2186:
	leaq	-1048(%rbp), %r15
	leaq	-1056(%rbp), %r12
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-1040(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-640(%rbp), %r8
	movl	$543, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-640(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1970
	call	_ZdlPv@PLT
.L1970:
	movq	-1048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1971
	movq	(%rdi), %rax
	call	*8(%rax)
.L1971:
	movq	-1032(%rbp), %r12
	testq	%r12, %r12
	je	.L1974
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1973
	call	_ZdlPv@PLT
.L1973:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1974:
	movq	-1000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2069
	call	_ZdlPv@PLT
.L2069:
	movq	-1008(%rbp), %r12
	testq	%r12, %r12
	je	.L1967
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1967:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2192
	addq	$1048, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1996:
	.cfi_restore_state
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2179
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2005:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2005
.L2179:
	leaq	-1040(%rbp), %rbx
.L2178:
	leaq	-784(%rbp), %rax
	leaq	-800(%rbp), %r15
	movq	%rax, -1064(%rbp)
.L2006:
	movq	-896(%rbp), %rdi
	leaq	-880(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2007
	call	_ZdlPv@PLT
.L2007:
	movq	-928(%rbp), %rdi
	cmpq	-1080(%rbp), %rdi
	je	.L2008
	call	_ZdlPv@PLT
.L2008:
	movq	-1064(%rbp), %rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	$129, -1040(%rbp)
	movq	%rax, -800(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1040(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movdqa	.LC52(%rip), %xmm0
	movq	%rax, -800(%rbp)
	movq	%rdx, -784(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC53(%rip), %xmm0
	movb	$10, 128(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC54(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC55(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	.LC56(%rip), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	.LC57(%rip), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	.LC58(%rip), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	.LC59(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movq	-1040(%rbp), %rax
	movq	-800(%rbp), %rdx
	movq	%rax, -792(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-800(%rbp), %rdi
	cmpq	-1064(%rbp), %rdi
	je	.L2009
	call	_ZdlPv@PLT
.L2009:
	movq	-1008(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movzbl	-1016(%rbp), %eax
	movq	$0, -1032(%rbp)
	xorl	$1, %eax
	movb	%al, -1040(%rbp)
	testb	%al, %al
	je	.L2193
	movq	-1008(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer20TerminateConnectionsEv@PLT
	movq	-1064(%rbp), %rax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	$136, %r8d
	movq	$1, -792(%rbp)
	movq	%rax, -800(%rbp)
	movw	%r8w, -784(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-800(%rbp), %rdi
	cmpq	-1064(%rbp), %rdi
	je	.L2016
	call	_ZdlPv@PLT
.L2016:
	movq	-1072(%rbp), %rax
	movl	$-1506, %edi
	movl	$237863048, -848(%rbp)
	movw	%di, -844(%rbp)
	movq	%rax, -864(%rbp)
	movzbl	-86(%rbp), %eax
	movq	$6, -856(%rbp)
	xorl	$1, %eax
	movb	$0, -842(%rbp)
	movb	%al, -1040(%rbp)
	movq	$0, -1032(%rbp)
	testb	%al, %al
	je	.L2194
	movq	-1072(%rbp), %rax
	movl	$1, %ecx
	movq	%r13, %rsi
	leaq	-944(%rbp), %rdx
	leaq	-280(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_si(%rip), %r8
	movb	$1, -86(%rbp)
	movq	%rax, -944(%rbp)
	movq	$6, -936(%rbp)
	call	uv_write@PLT
	testl	%eax, %eax
	jne	.L2023
	xorl	%esi, %esi
	leaq	_ZL4loop(%rip), %rdi
	movw	%si, -800(%rbp)
	movq	%r12, %rsi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -86(%rbp)
	je	.L2024
	leaq	_ZL4loop(%rip), %r14
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2195:
	movl	$1, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -86(%rbp)
	je	.L2024
.L2026:
	cmpb	$0, -800(%rbp)
	je	.L2195
	leaq	-1056(%rbp), %r15
	movb	$0, -1040(%rbp)
	leaq	-1048(%rbp), %r14
	movq	$0, -1032(%rbp)
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC30(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$179, %ecx
	movq	%r14, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2083
	call	_ZdlPv@PLT
.L2083:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2027
	movq	(%rdi), %rax
	call	*8(%rax)
.L2027:
	movq	-1032(%rbp), %r14
	testq	%r14, %r14
	je	.L2028
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2029
	call	_ZdlPv@PLT
.L2029:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2028:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2034
	leaq	_ZL4loop(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L2031:
	movl	$2, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2031
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	-864(%rbp), %rdi
	cmpq	-1072(%rbp), %rdi
	je	.L2035
	call	_ZdlPv@PLT
.L2035:
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%cx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2036
	leaq	_ZL4loop(%rip), %r14
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L2196:
	movl	$1, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2036
.L2038:
	cmpb	$0, -800(%rbp)
	je	.L2196
	leaq	-1056(%rbp), %r14
	movb	$0, -1040(%rbp)
	leaq	-1048(%rbp), %r13
	movq	$0, -1032(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC38(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$152, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2078
	call	_ZdlPv@PLT
.L2078:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2039
	movq	(%rdi), %rax
	call	*8(%rax)
.L2039:
	movq	-1032(%rbp), %r13
	testq	%r13, %r13
	je	.L2040
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2041
	call	_ZdlPv@PLT
.L2041:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2040:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2044
	leaq	_ZL4loop(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L2043:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2043
.L2044:
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -1016(%rbp)
	jne	.L2055
	leaq	_ZL4loop(%rip), %r13
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2197:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -1016(%rbp)
	jne	.L2055
.L2057:
	cmpb	$0, -800(%rbp)
	je	.L2197
	leaq	-1048(%rbp), %r14
	movb	$0, -1040(%rbp)
	leaq	-1056(%rbp), %r13
	movq	$0, -1032(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC51(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$554, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2073
	call	_ZdlPv@PLT
.L2073:
	movq	-1048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2058
	movq	(%rdi), %rax
	call	*8(%rax)
.L2058:
	movq	-1032(%rbp), %r13
	testq	%r13, %r13
	je	.L2059
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2060
	call	_ZdlPv@PLT
.L2060:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2059:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2015
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2062:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2062
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1974
	call	_ZdlPv@PLT
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L2023:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2055:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2015
	.p2align 4,,10
	.p2align 3
.L2063:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2063
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %r14
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2076
	.p2align 4,,10
	.p2align 3
.L2045:
	movl	$2, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2045
.L2076:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r13, %rdi
	call	uv_close@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%dx, -800(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -640(%rbp)
	jne	.L2046
	leaq	_ZL4loop(%rip), %r13
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2198:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -640(%rbp)
	jne	.L2046
.L2048:
	cmpb	$0, -800(%rbp)
	je	.L2198
	leaq	-1048(%rbp), %r14
	movb	$0, -1040(%rbp)
	leaq	-1056(%rbp), %r13
	movq	$0, -1032(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-832(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-832(%rbp), %r8
	movl	$141, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-832(%rbp), %rdi
	leaq	-816(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2075
	call	_ZdlPv@PLT
.L2075:
	movq	-1048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2049
	movq	(%rdi), %rax
	call	*8(%rax)
.L2049:
	movq	-1032(%rbp), %r13
	testq	%r13, %r13
	je	.L2050
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2051
	call	_ZdlPv@PLT
.L2051:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2050:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2044
	leaq	_ZL4loop(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L2053:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2053
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2190:
	leaq	-1056(%rbp), %r9
	leaq	-800(%rbp), %r15
	movq	%r9, %rdi
	leaq	-1040(%rbp), %rbx
	movq	%r9, -1088(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$171, %ecx
	movq	-800(%rbp), %r8
	leaq	-1048(%rbp), %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rdi, -1064(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1088(%rbp), %r9
	movq	-1064(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-1064(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	leaq	-784(%rbp), %rax
	movq	%rax, -1064(%rbp)
	cmpq	%rax, %rdi
	je	.L1990
	call	_ZdlPv@PLT
.L1990:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1991
	movq	(%rdi), %rax
	call	*8(%rax)
.L1991:
	movq	-1032(%rbp), %r8
	testq	%r8, %r8
	je	.L2006
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1993
	movq	%r8, -1088(%rbp)
	call	_ZdlPv@PLT
	movq	-1088(%rbp), %r8
.L1993:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2193:
	leaq	-1048(%rbp), %r13
	leaq	-1056(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	.LC49(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-800(%rbp), %r8
	movl	$549, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	cmpq	-1064(%rbp), %rdi
	je	.L2011
	call	_ZdlPv@PLT
.L2011:
	movq	-1048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2012
	movq	(%rdi), %rax
	call	*8(%rax)
.L2012:
	movq	-1032(%rbp), %r12
	testq	%r12, %r12
	je	.L2015
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2014
	call	_ZdlPv@PLT
.L2014:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2046:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %r13
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2044
	.p2align 4,,10
	.p2align 3
.L2054:
	movl	$2, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2054
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -799(%rbp)
	jne	.L2034
	leaq	_ZL4loop(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L2033:
	movl	$2, %esi
	movq	%r14, %rdi
	call	uv_run@PLT
	cmpb	$0, -799(%rbp)
	je	.L2033
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2194:
	leaq	-1056(%rbp), %r9
	leaq	-1048(%rbp), %r14
	movq	%r9, %rdi
	movq	%r9, -1080(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	.LC23(%rip), %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	.LC24(%rip), %rcx
	leaq	.LC29(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-800(%rbp), %r8
	movl	$171, %ecx
	movq	%r14, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-1080(%rbp), %r9
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-800(%rbp), %rdi
	cmpq	-1064(%rbp), %rdi
	je	.L2018
	call	_ZdlPv@PLT
.L2018:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2019
	movq	(%rdi), %rax
	call	*8(%rax)
.L2019:
	movq	-1032(%rbp), %r14
	testq	%r14, %r14
	je	.L2034
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2021
	call	_ZdlPv@PLT
.L2021:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2187:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2188:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6427:
	.size	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test8TestBodyEv, .-_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test8TestBodyEv
	.p2align 4
	.globl	_Z16has_ipv6_addressv
	.type	_Z16has_ipv6_addressv, @function
_Z16has_ipv6_addressv:
.LFB6435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	leaq	-36(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	movl	$0, -36(%rbp)
	call	uv_interface_addresses@PLT
	testl	%eax, %eax
	jne	.L2199
	movl	-36(%rbp), %esi
	movq	-32(%rbp), %rdi
	testl	%esi, %esi
	jle	.L2201
	leal	-1(%rsi), %edx
	leaq	20(%rdi), %rax
	leaq	(%rdx,%rdx,4), %rdx
	salq	$4, %rdx
	leaq	100(%rdi,%rdx), %rcx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L2203:
	cmpw	$10, (%rax)
	cmove	%edx, %r12d
	addq	$80, %rax
	cmpq	%rcx, %rax
	jne	.L2203
.L2201:
	call	uv_free_interface_addresses@PLT
.L2199:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2210
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2210:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6435:
	.size	_Z16has_ipv6_addressv, .-_Z16has_ipv6_addressv
	.section	.text._ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB7577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	cmpl	%eax, (%rcx)
	je	.L2226
	movq	.LC66(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC47(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r14, %rdi
	movl	(%r8), %esi
	call	_ZNSolsEi@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L2214
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L2227
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2216:
	movq	.LC66(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC48(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L2217
	call	_ZdlPv@PLT
.L2217:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movl	(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L2218
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L2219
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2220:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L2221
	call	_ZdlPv@PLT
.L2221:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2222
	call	_ZdlPv@PLT
.L2222:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L2211
	call	_ZdlPv@PLT
.L2211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2228
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2227:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2226:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L2211
	.p2align 4,,10
	.p2align 3
.L2219:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2218:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2214:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2216
.L2228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7577:
	.size	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.rodata.str1.1
.LC67:
	.string	"server.connected"
.LC68:
	.string	"1"
.LC69:
	.string	"server.disconnected"
.LC70:
	.string	"2"
.LC71:
	.string	"(3 != server.disconnected)"
.LC72:
	.string	"(3 != server.connected)"
.LC73:
	.string	"(server.disconnected < 2)"
	.text
	.align 2
	.p2align 4
	.globl	_ZN48InspectorSocketServerTest_InspectorSessions_Test8TestBodyEv
	.type	_ZN48InspectorSocketServerTest_InspectorSessions_Test8TestBodyEv, @function
_ZN48InspectorSocketServerTest_InspectorSessions_Test8TestBodyEv:
.LFB6378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-640(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-624(%rbp), %rbx
	subq	$2856, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -2840(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -624(%rbp)
	leaq	-2800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rbx, -640(%rbp)
	movb	$49, -616(%rbp)
	movq	$9, -632(%rbp)
	movb	$0, -615(%rbp)
	movq	%rax, -2888(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolderC2EbP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiP8_IO_FILE.constprop.0
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2230
	call	_ZdlPv@PLT
.L2230:
	movq	-2784(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	movq	$0, -2808(%rbp)
	movb	%al, -2816(%rbp)
	testb	%al, %al
	je	.L2629
	xorl	%ecx, %ecx
	leaq	_ZL4loop(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$0, -1864(%rbp)
	leaq	-2400(%rbp), %r14
	movw	%cx, -2416(%rbp)
	movl	$31, %ecx
	leaq	-2624(%rbp), %r15
	movq	%rax, -2408(%rbp)
	movq	%r14, %rdi
	xorl	%eax, %eax
	rep stosq
	movq	-2784(%rbp), %rdi
	movb	$0, -1862(%rbp)
	movq	$0, -1840(%rbp)
	movaps	%xmm0, -1856(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r15, -2640(%rbp)
	movl	%eax, %r12d
	movb	$49, -2616(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -2624(%rbp)
	movq	-1856(%rbp), %rax
	movq	$9, -2632(%rbp)
	movb	$0, -2615(%rbp)
	movw	%si, -1864(%rbp)
	movw	%di, -2416(%rbp)
	cmpq	-1848(%rbp), %rax
	je	.L2237
	movq	%rax, -1848(%rbp)
.L2237:
	movq	-2408(%rbp), %rdi
	movq	%r14, %rsi
	leaq	-2736(%rbp), %rbx
	call	uv_tcp_init@PLT
	movq	-2640(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%r12d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L2281
	leaq	-2152(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L2282
	leaq	-632(%rbp), %r12
	xorl	%edx, %edx
	leaq	_ZL4loop(%rip), %rdi
	movq	%r12, %rsi
	movw	%dx, -640(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -1864(%rbp)
	jne	.L2240
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2630:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1864(%rbp)
	jne	.L2240
.L2242:
	cmpb	$0, -640(%rbp)
	je	.L2630
	leaq	-2832(%rbp), %rbx
	movb	$0, -2816(%rbp)
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-2816(%rbp), %rax
	leaq	-1232(%rbp), %rdi
	movq	%rax, %rsi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	movq	%rdi, -2880(%rbp)
	leaq	.LC28(%rip), %rdx
	movq	%rax, -2856(%rbp)
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$115, %ecx
	leaq	-2824(%rbp), %rax
	movq	-1232(%rbp), %r8
	movq	%rax, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rax, -2848(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	-2848(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1232(%rbp), %rdi
	leaq	-1216(%rbp), %rax
	movq	%rax, -2864(%rbp)
	cmpq	%rax, %rdi
	je	.L2449
	call	_ZdlPv@PLT
.L2449:
	movq	-2832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2243
	movq	(%rdi), %rax
	call	*8(%rax)
.L2243:
	movq	-2808(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2244
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2245
	call	_ZdlPv@PLT
.L2245:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2244:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2248
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2247:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2247
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2240:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2447
	.p2align 4,,10
	.p2align 3
.L2249:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2249
.L2447:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r14, %rdi
	call	uv_read_start@PLT
	leaq	-2824(%rbp), %rax
	movq	%rax, -2848(%rbp)
	leaq	-2816(%rbp), %rax
	movq	%rax, -2856(%rbp)
	leaq	-1232(%rbp), %rax
	movq	%rax, -2880(%rbp)
	leaq	-1216(%rbp), %rax
	movq	%rax, -2864(%rbp)
.L2248:
	movq	-2640(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2250
	call	_ZdlPv@PLT
.L2250:
	movq	-2864(%rbp), %rax
	movq	-2880(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-2416(%rbp), %r15
	movb	$116, -1206(%rbp)
	movq	%rax, -1232(%rbp)
	movabsq	$8241996531539468653, %rax
	movq	%rax, -1216(%rbp)
	movl	$25959, %eax
	movw	%ax, -1208(%rbp)
	movq	$11, -1224(%rbp)
	movb	$0, -1205(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	-2840(%rbp), %rdi
	je	.L2251
	call	_ZdlPv@PLT
.L2251:
	movq	-1232(%rbp), %rdi
	cmpq	-2864(%rbp), %rdi
	je	.L2252
	call	_ZdlPv@PLT
.L2252:
	movq	-2840(%rbp), %rbx
	movq	-2856(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	$129, -2816(%rbp)
	movq	%rbx, -640(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2816(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	.LC52(%rip), %xmm0
	movq	%rax, -640(%rbp)
	movq	%rdx, -624(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC53(%rip), %xmm0
	movb	$10, 128(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC54(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC55(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	.LC56(%rip), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	.LC57(%rip), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	.LC58(%rip), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	.LC59(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movq	-2816(%rbp), %rax
	movq	-640(%rbp), %rdx
	movq	%rax, -632(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2253
	call	_ZdlPv@PLT
.L2253:
	movq	-2888(%rbp), %r8
	movq	-2848(%rbp), %rcx
	leaq	.LC67(%rip), %rdx
	leaq	.LC68(%rip), %rsi
	movq	-2856(%rbp), %rdi
	movl	$1, -2824(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -2816(%rbp)
	je	.L2631
.L2254:
	movq	-2808(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2257
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2258
	call	_ZdlPv@PLT
.L2258:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2257:
	movq	-2840(%rbp), %rbx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movb	$0, -614(%rbp)
	movabsq	$-1130912139003853695, %rax
	movq	$10, -632(%rbp)
	movq	%rax, -624(%rbp)
	movl	$1365, %eax
	movq	%rbx, -640(%rbp)
	movw	%ax, -616(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2259
	call	_ZdlPv@PLT
.L2259:
	movq	-2840(%rbp), %rbx
	movq	%r13, %rsi
	movq	-2888(%rbp), %rdi
	movl	$875770417, -624(%rbp)
	movq	$4, -632(%rbp)
	movq	%rbx, -640(%rbp)
	movb	$0, -620(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolder6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2260
	call	_ZdlPv@PLT
.L2260:
	movq	-2840(%rbp), %rbx
	movq	-2784(%rbp), %rdi
	movq	%r13, %rdx
	movl	$943142453, -624(%rbp)
	movl	-2752(%rbp), %esi
	movb	$0, -620(%rbp)
	movq	%rbx, -640(%rbp)
	movq	$4, -632(%rbp)
	call	_ZN4node9inspector21InspectorSocketServer4SendEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2261
	call	_ZdlPv@PLT
.L2261:
	movq	-2840(%rbp), %rbx
	movl	$14391, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$909444225, -624(%rbp)
	movq	%rbx, -640(%rbp)
	movw	%ax, -620(%rbp)
	movq	$6, -632(%rbp)
	movb	$0, -618(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2262
	call	_ZdlPv@PLT
.L2262:
	movq	-2840(%rbp), %rbx
	movl	$-1506, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$237863048, -624(%rbp)
	movq	%rbx, -640(%rbp)
	movw	%ax, -620(%rbp)
	movq	$6, -632(%rbp)
	movb	$0, -618(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2263
	call	_ZdlPv@PLT
.L2263:
	movq	-2840(%rbp), %rbx
	movl	$136, %eax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	$1, -632(%rbp)
	movq	%rbx, -640(%rbp)
	movw	%ax, -624(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2264
	call	_ZdlPv@PLT
.L2264:
	leaq	-2796(%rbp), %rax
	movq	-2848(%rbp), %rcx
	movq	-2856(%rbp), %rdi
	leaq	.LC69(%rip), %rdx
	movq	%rax, %r8
	leaq	.LC68(%rip), %rsi
	movl	$1, -2824(%rbp)
	movq	%rax, -2896(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -2816(%rbp)
	je	.L2632
.L2265:
	movq	-2808(%rbp), %r15
	testq	%r15, %r15
	je	.L2268
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2269
	call	_ZdlPv@PLT
.L2269:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2268:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r14, %rdi
	call	uv_close@PLT
	xorl	%r11d, %r11d
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%r11w, -640(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -2416(%rbp)
	jne	.L2270
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2633:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -2416(%rbp)
	jne	.L2270
.L2272:
	cmpb	$0, -640(%rbp)
	je	.L2633
	movq	-2848(%rbp), %rbx
	movb	$0, -2816(%rbp)
	leaq	-2832(%rbp), %r14
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	-2880(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1232(%rbp), %r8
	movl	$141, %ecx
	movq	%r14, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1232(%rbp), %rdi
	cmpq	-2864(%rbp), %rdi
	je	.L2446
	call	_ZdlPv@PLT
.L2446:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2273
	movq	(%rdi), %rax
	call	*8(%rax)
.L2273:
	movq	-2808(%rbp), %r14
	testq	%r14, %r14
	je	.L2274
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2275
	call	_ZdlPv@PLT
.L2275:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2274:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2276
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2277:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2277
.L2276:
	leaq	_ZL4loop(%rip), %rax
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	leaq	-1808(%rbp), %r15
	movq	%rax, -1816(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rdi
	movw	%r8w, -1824(%rbp)
	rep stosq
	movq	-2784(%rbp), %rdi
	movb	$0, -1272(%rbp)
	movb	$0, -1270(%rbp)
	movq	$0, -1248(%rbp)
	movaps	%xmm0, -1264(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	movb	$49, -2552(%rbp)
	movl	%eax, %r14d
	leaq	-2560(%rbp), %rax
	movq	$9, -2568(%rbp)
	movq	%rax, -2872(%rbp)
	movq	%rax, -2576(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -2560(%rbp)
	movq	-1264(%rbp), %rax
	movb	$0, -2551(%rbp)
	movw	%r9w, -1272(%rbp)
	movw	%r10w, -1824(%rbp)
	cmpq	-1256(%rbp), %rax
	je	.L2279
	movq	%rax, -1256(%rbp)
.L2279:
	movq	-1816(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-2704(%rbp), %rbx
	call	uv_tcp_init@PLT
	movq	-2576(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%r14d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L2281
	leaq	-1560(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L2282
	xorl	%edi, %edi
	movq	%r12, %rsi
	movw	%di, -640(%rbp)
	leaq	_ZL4loop(%rip), %rdi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -1272(%rbp)
	jne	.L2283
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2634:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1272(%rbp)
	jne	.L2283
.L2285:
	cmpb	$0, -640(%rbp)
	je	.L2634
	leaq	-2832(%rbp), %r14
	movb	$0, -2816(%rbp)
	movq	$0, -2808(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	-2880(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$115, %ecx
	movq	-2848(%rbp), %rbx
	movq	-1232(%rbp), %r8
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1232(%rbp), %rdi
	cmpq	-2864(%rbp), %rdi
	je	.L2443
	call	_ZdlPv@PLT
.L2443:
	movq	-2832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2286
	movq	(%rdi), %rax
	call	*8(%rax)
.L2286:
	movq	-2808(%rbp), %r14
	testq	%r14, %r14
	je	.L2287
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2288
	call	_ZdlPv@PLT
.L2288:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2287:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2291
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2290:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2290
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2270:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2276
	.p2align 4,,10
	.p2align 3
.L2280:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2280
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2629:
	leaq	-2824(%rbp), %r14
	leaq	-2832(%rbp), %r12
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-2816(%rbp), %rsi
	movq	%r13, %rdi
	leaq	.LC24(%rip), %r8
	leaq	.LC23(%rip), %rcx
	leaq	.LC39(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-640(%rbp), %r8
	movl	$390, %ecx
	movq	%r12, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-640(%rbp), %rdi
	cmpq	-2840(%rbp), %rdi
	je	.L2232
	call	_ZdlPv@PLT
.L2232:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2233
	movq	(%rdi), %rax
	call	*8(%rax)
.L2233:
	movq	-2808(%rbp), %r12
	testq	%r12, %r12
	je	.L2236
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2235
	call	_ZdlPv@PLT
.L2235:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2236:
	movq	-2776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2421
	call	_ZdlPv@PLT
.L2421:
	movq	-2784(%rbp), %r12
	testq	%r12, %r12
	je	.L2229
	movq	%r12, %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movl	$152, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2635
	addq	$2856, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2283:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2444
	.p2align 4,,10
	.p2align 3
.L2292:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2292
.L2444:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r15, %rdi
	call	uv_read_start@PLT
.L2291:
	movq	-2576(%rbp), %rdi
	cmpq	-2872(%rbp), %rdi
	je	.L2293
	call	_ZdlPv@PLT
.L2293:
	movq	-2864(%rbp), %rax
	movq	-2880(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1824(%rbp), %r14
	movl	$1952802674, -1208(%rbp)
	movq	%rax, -1232(%rbp)
	movabsq	$7022342668473036642, %rax
	movq	%rax, -1216(%rbp)
	movq	$12, -1224(%rbp)
	movb	$0, -1204(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	-2840(%rbp), %rdi
	je	.L2294
	call	_ZdlPv@PLT
.L2294:
	movq	-1232(%rbp), %rdi
	cmpq	-2864(%rbp), %rdi
	je	.L2295
	call	_ZdlPv@PLT
.L2295:
	movq	-2840(%rbp), %rbx
	movq	-2856(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	$24, -2816(%rbp)
	movq	%rbx, -640(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2816(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	.LC60(%rip), %xmm0
	movq	%rax, -640(%rbp)
	movabsq	$8391162085809410592, %rcx
	movq	%rdx, -624(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-2816(%rbp), %rax
	movq	-640(%rbp), %rdx
	movq	%rax, -632(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2296
	call	_ZdlPv@PLT
.L2296:
	xorl	%esi, %esi
	leaq	_ZL4loop(%rip), %rdi
	movw	%si, -640(%rbp)
	movq	%r12, %rsi
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -1823(%rbp)
	jne	.L2297
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2636:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1823(%rbp)
	jne	.L2297
.L2299:
	cmpb	$0, -640(%rbp)
	je	.L2636
	leaq	-2832(%rbp), %r14
	movb	$0, -2816(%rbp)
	movq	$0, -2808(%rbp)
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	-2880(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC38(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$152, %ecx
	movq	-2848(%rbp), %rbx
	movq	-1232(%rbp), %r8
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1232(%rbp), %rdi
	cmpq	-2864(%rbp), %rdi
	je	.L2441
	call	_ZdlPv@PLT
.L2441:
	movq	-2832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2300
	movq	(%rdi), %rax
	call	*8(%rax)
.L2300:
	movq	-2808(%rbp), %r14
	testq	%r14, %r14
	je	.L2301
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2302
	call	_ZdlPv@PLT
.L2302:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2301:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2305
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2304:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2304
.L2305:
	movq	-2888(%rbp), %r8
	movq	-2848(%rbp), %rcx
	leaq	.LC67(%rip), %rdx
	leaq	.LC68(%rip), %rsi
	movq	-2856(%rbp), %rdi
	movl	$1, -2824(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -2816(%rbp)
	je	.L2637
.L2316:
	movq	-2808(%rbp), %r14
	testq	%r14, %r14
	je	.L2319
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2320
	call	_ZdlPv@PLT
.L2320:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2319:
	movq	-2896(%rbp), %r8
	movq	-2848(%rbp), %rcx
	leaq	.LC69(%rip), %rdx
	leaq	.LC68(%rip), %rsi
	movq	-2856(%rbp), %rdi
	movl	$1, -2824(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -2816(%rbp)
	je	.L2638
.L2321:
	movq	-2808(%rbp), %r14
	testq	%r14, %r14
	je	.L2324
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2325
	call	_ZdlPv@PLT
.L2325:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2324:
	xorl	%eax, %eax
	movq	-2864(%rbp), %rdi
	movl	$31, %ecx
	pxor	%xmm0, %xmm0
	movw	%ax, -1232(%rbp)
	leaq	_ZL4loop(%rip), %rax
	movq	%rax, -1224(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	-2784(%rbp), %rdi
	movb	$0, -680(%rbp)
	movb	$0, -678(%rbp)
	movq	$0, -656(%rbp)
	movaps	%xmm0, -672(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	xorl	%edx, %edx
	movb	$49, -2584(%rbp)
	movl	%eax, %r14d
	leaq	-2592(%rbp), %rax
	movq	$9, -2600(%rbp)
	movq	%rax, -2896(%rbp)
	movq	%rax, -2608(%rbp)
	movabsq	$3328210909095473713, %rax
	movq	%rax, -2592(%rbp)
	xorl	%eax, %eax
	movw	%ax, -680(%rbp)
	movq	-672(%rbp), %rax
	movb	$0, -2583(%rbp)
	movw	%dx, -1232(%rbp)
	cmpq	-664(%rbp), %rax
	je	.L2326
	movq	%rax, -664(%rbp)
.L2326:
	movq	-2864(%rbp), %rbx
	movq	-1224(%rbp), %rdi
	leaq	-2672(%rbp), %r15
	movq	%rbx, %rsi
	call	uv_tcp_init@PLT
	movq	-2608(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r14d, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L2281
	leaq	-968(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si(%rip), %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	uv_tcp_connect@PLT
	testl	%eax, %eax
	jne	.L2282
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -640(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -680(%rbp)
	jne	.L2327
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2639:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -680(%rbp)
	jne	.L2327
.L2329:
	cmpb	$0, -640(%rbp)
	je	.L2639
	leaq	-2832(%rbp), %r15
	movb	$0, -2816(%rbp)
	leaq	-2576(%rbp), %r14
	movq	$0, -2808(%rbp)
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movl	$115, %ecx
	movq	-2848(%rbp), %rbx
	movq	-2576(%rbp), %r8
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2576(%rbp), %rdi
	cmpq	-2872(%rbp), %rdi
	je	.L2436
	call	_ZdlPv@PLT
.L2436:
	movq	-2832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2330
	movq	(%rdi), %rax
	call	*8(%rax)
.L2330:
	movq	-2808(%rbp), %r15
	testq	%r15, %r15
	je	.L2331
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2332
	call	_ZdlPv@PLT
.L2332:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2331:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2335
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2334:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2334
.L2335:
	movq	-2608(%rbp), %rdi
	cmpq	-2896(%rbp), %rdi
	je	.L2337
	call	_ZdlPv@PLT
.L2337:
	movq	-2872(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	$116, -2550(%rbp)
	movq	$11, -2568(%rbp)
	movq	%rax, -2576(%rbp)
	movabsq	$8241996531539468653, %rax
	movq	%rax, -2560(%rbp)
	movl	$25959, %eax
	movw	%ax, -2552(%rbp)
	movb	$0, -2549(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2880(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	-2840(%rbp), %rdi
	je	.L2338
	call	_ZdlPv@PLT
.L2338:
	movq	-2576(%rbp), %rdi
	cmpq	-2872(%rbp), %rdi
	je	.L2339
	call	_ZdlPv@PLT
.L2339:
	movq	-2840(%rbp), %rbx
	movq	-2856(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	$129, -2816(%rbp)
	movq	%rbx, -640(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2816(%rbp), %rdx
	movdqa	.LC52(%rip), %xmm0
	movq	%r13, %rsi
	movq	%rax, -640(%rbp)
	movq	-2880(%rbp), %rdi
	movq	%rdx, -624(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC53(%rip), %xmm0
	movb	$10, 128(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC54(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC55(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	.LC56(%rip), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	.LC57(%rip), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	.LC58(%rip), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	.LC59(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movq	-2816(%rbp), %rax
	movq	-640(%rbp), %rdx
	movq	%rax, -632(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2340
	call	_ZdlPv@PLT
.L2340:
	movq	-2888(%rbp), %r8
	movq	-2848(%rbp), %rcx
	leaq	.LC67(%rip), %rdx
	leaq	.LC70(%rip), %rsi
	movq	-2856(%rbp), %rdi
	movl	$2, -2824(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -2816(%rbp)
	je	.L2640
.L2341:
	movq	-2808(%rbp), %r15
	testq	%r15, %r15
	je	.L2344
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2345
	call	_ZdlPv@PLT
.L2345:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2344:
	movq	-2840(%rbp), %rbx
	movq	-2784(%rbp), %rdi
	movq	%r13, %rdx
	movl	$943142453, -624(%rbp)
	movl	-2752(%rbp), %esi
	movb	$0, -620(%rbp)
	movq	%rbx, -640(%rbp)
	movq	$4, -632(%rbp)
	call	_ZN4node9inspector21InspectorSocketServer4SendEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2346
	call	_ZdlPv@PLT
.L2346:
	movq	-2840(%rbp), %rbx
	movq	-2880(%rbp), %rdi
	movl	$14391, %eax
	movq	%r13, %rsi
	movl	$909444225, -624(%rbp)
	movq	%rbx, -640(%rbp)
	movw	%ax, -620(%rbp)
	movq	$6, -632(%rbp)
	movb	$0, -618(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-640(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2347
	call	_ZdlPv@PLT
.L2347:
	movq	-2864(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	xorl	%r15d, %r15d
	call	uv_close@PLT
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%r15w, -640(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -1232(%rbp)
	jne	.L2348
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2641:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1232(%rbp)
	jne	.L2348
.L2350:
	cmpb	$0, -640(%rbp)
	je	.L2641
	movq	-2848(%rbp), %rbx
	movb	$0, -2816(%rbp)
	leaq	-2832(%rbp), %r15
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-2576(%rbp), %r8
	movl	$141, %ecx
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2576(%rbp), %rdi
	cmpq	-2872(%rbp), %rdi
	je	.L2433
	call	_ZdlPv@PLT
.L2433:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2351
	movq	(%rdi), %rax
	call	*8(%rax)
.L2351:
	movq	-2808(%rbp), %r15
	testq	%r15, %r15
	je	.L2352
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2353
	call	_ZdlPv@PLT
.L2353:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2352:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2354
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2355:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2355
.L2354:
	xorl	%ebx, %ebx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%bx, -640(%rbp)
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpl	$1, -2796(%rbp)
	jle	.L2357
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2642:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpl	$1, -2796(%rbp)
	jg	.L2356
.L2357:
	cmpb	$0, -640(%rbp)
	je	.L2642
	movq	-2848(%rbp), %rbx
	movb	$0, -2816(%rbp)
	leaq	-2832(%rbp), %r13
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	%r14, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC73(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-2576(%rbp), %r8
	movl	$434, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2576(%rbp), %rdi
	cmpq	-2872(%rbp), %rdi
	je	.L2430
	call	_ZdlPv@PLT
.L2430:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2360
	movq	(%rdi), %rax
	call	*8(%rax)
.L2360:
	movq	-2808(%rbp), %r13
	testq	%r13, %r13
	je	.L2361
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2362
	call	_ZdlPv@PLT
.L2362:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2361:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2365
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2364:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2364
.L2365:
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2418
	call	_ZdlPv@PLT
.L2418:
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2419
	call	_ZdlPv@PLT
.L2419:
	movq	-1856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2236
	call	_ZdlPv@PLT
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2439
	.p2align 4,,10
	.p2align 3
.L2306:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2306
.L2439:
	leaq	_ZN12_GLOBAL__N_113SocketWrapper14ClosedCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r15, %rdi
	call	uv_close@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%cx, -640(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpb	$0, -1824(%rbp)
	jne	.L2307
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2643:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -1824(%rbp)
	jne	.L2307
.L2309:
	cmpb	$0, -640(%rbp)
	je	.L2643
	movq	-2848(%rbp), %rbx
	movb	$0, -2816(%rbp)
	leaq	-2832(%rbp), %r14
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	-2880(%rbp), %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC37(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-1232(%rbp), %r8
	movl	$141, %ecx
	movq	%r14, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-1232(%rbp), %rdi
	cmpq	-2864(%rbp), %rdi
	je	.L2438
	call	_ZdlPv@PLT
.L2438:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2310
	movq	(%rdi), %rax
	call	*8(%rax)
.L2310:
	movq	-2808(%rbp), %r14
	testq	%r14, %r14
	je	.L2311
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L2312
	call	_ZdlPv@PLT
.L2312:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2311:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2305
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2314:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2314
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2431
	.p2align 4,,10
	.p2align 3
.L2366:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2366
.L2431:
	movq	-2840(%rbp), %rdi
	leaq	_ZL4loop(%rip), %rax
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	movq	%rax, -632(%rbp)
	movl	$31, %ecx
	xorl	%eax, %eax
	movw	%r11w, -640(%rbp)
	rep stosq
	movq	-2784(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movb	$0, -88(%rbp)
	movb	$0, -86(%rbp)
	movq	$0, -64(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	-2872(%rbp), %rbx
	movl	%eax, %edx
	movabsq	$3328210909095473713, %rax
	movb	$49, -2552(%rbp)
	movq	%rax, -2560(%rbp)
	movq	%rbx, -2576(%rbp)
	movq	$9, -2568(%rbp)
	movb	$0, -2551(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEib
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2367
	call	_ZdlPv@PLT
.L2367:
	movq	-2896(%rbp), %rax
	leaq	-2608(%rbp), %r15
	movl	$25959, %r10d
	movq	%r14, %rdi
	movq	%r15, %rsi
	movw	%r10w, -2584(%rbp)
	movq	%rax, -2608(%rbp)
	movabsq	$8241996531539468653, %rax
	movq	%rax, -2592(%rbp)
	movb	$116, -2582(%rbp)
	movq	$11, -2600(%rbp)
	movb	$0, -2581(%rbp)
	call	_ZN12_GLOBAL__N_1L18WsHandshakeRequestERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2576(%rbp), %rdi
	cmpq	-2872(%rbp), %rdi
	je	.L2368
	call	_ZdlPv@PLT
.L2368:
	movq	-2608(%rbp), %rdi
	cmpq	-2896(%rbp), %rdi
	je	.L2369
	call	_ZdlPv@PLT
.L2369:
	movq	-2872(%rbp), %rbx
	movq	-2856(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	$129, -2816(%rbp)
	movq	%rbx, -2576(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2816(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	.LC52(%rip), %xmm0
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC53(%rip), %xmm0
	movb	$10, 128(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC54(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC55(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	.LC56(%rip), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	.LC57(%rip), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	.LC58(%rip), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	.LC59(%rip), %xmm0
	movups	%xmm0, 112(%rax)
	movq	-2816(%rbp), %rax
	movq	-2576(%rbp), %rdx
	movq	%rax, -2568(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2370
	call	_ZdlPv@PLT
.L2370:
	leaq	-2568(%rbp), %r12
	xorl	%r9d, %r9d
	leaq	_ZL4loop(%rip), %rdi
	movq	%r12, %rsi
	movw	%r9w, -2576(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpl	$3, -2800(%rbp)
	je	.L2371
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2644:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpl	$3, -2800(%rbp)
	je	.L2371
.L2373:
	cmpb	$0, -2576(%rbp)
	je	.L2644
	movq	-2848(%rbp), %rbx
	movb	$0, -2816(%rbp)
	leaq	-2832(%rbp), %r13
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC72(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-2608(%rbp), %r8
	movl	$442, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2608(%rbp), %rdi
	cmpq	-2896(%rbp), %rdi
	je	.L2427
	call	_ZdlPv@PLT
.L2427:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2374
	movq	(%rdi), %rax
	call	*8(%rax)
.L2374:
	movq	-2808(%rbp), %r13
	testq	%r13, %r13
	je	.L2375
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2376
	call	_ZdlPv@PLT
.L2376:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2375:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -2575(%rbp)
	jne	.L2379
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2378:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -2575(%rbp)
	je	.L2378
.L2379:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2365
.L2628:
	call	_ZdlPv@PLT
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2348:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2354
	.p2align 4,,10
	.p2align 3
.L2358:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2358
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2434
	.p2align 4,,10
	.p2align 3
.L2336:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2336
.L2434:
	movq	-2864(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_113SocketWrapper12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN12_GLOBAL__N_113SocketWrapper13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	leaq	-2576(%rbp), %r14
	call	uv_read_start@PLT
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -639(%rbp)
	jne	.L2305
	.p2align 4,,10
	.p2align 3
.L2315:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -639(%rbp)
	je	.L2315
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2631:
	movq	-2848(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2808(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2255
	movq	(%rax), %r8
.L2255:
	leaq	-2832(%rbp), %rbx
	movl	$398, %ecx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-2848(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2254
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2632:
	movq	-2848(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2808(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2266
	movq	(%rax), %r8
.L2266:
	leaq	-2832(%rbp), %r15
	movl	$409, %ecx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-2848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2265
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2281:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2371:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -2575(%rbp)
	jne	.L2428
	.p2align 4,,10
	.p2align 3
.L2380:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -2575(%rbp)
	je	.L2380
.L2428:
	movq	-2872(%rbp), %rbx
	movq	-2784(%rbp), %rdi
	movq	%r14, %rdx
	movl	$943142453, -2560(%rbp)
	movl	-2752(%rbp), %esi
	movb	$0, -2556(%rbp)
	movq	%rbx, -2576(%rbp)
	movq	$4, -2568(%rbp)
	call	_ZN4node9inspector21InspectorSocketServer4SendEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2381
	call	_ZdlPv@PLT
.L2381:
	movq	-2872(%rbp), %rbx
	movl	$14391, %r8d
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$909444225, -2560(%rbp)
	movq	%rbx, -2576(%rbp)
	movw	%r8w, -2556(%rbp)
	movq	$6, -2568(%rbp)
	movb	$0, -2554(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2382
	call	_ZdlPv@PLT
.L2382:
	movq	-2872(%rbp), %rbx
	movl	$1365, %edi
	movq	%r14, %rsi
	movabsq	$-1130912139003853695, %rax
	movw	%di, -2552(%rbp)
	movq	%r13, %rdi
	movq	%rbx, -2576(%rbp)
	movq	%rax, -2560(%rbp)
	movq	$10, -2568(%rbp)
	movb	$0, -2550(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2383
	call	_ZdlPv@PLT
.L2383:
	movq	-2872(%rbp), %rbx
	movq	%r14, %rsi
	movq	-2888(%rbp), %rdi
	movl	$875770417, -2560(%rbp)
	movq	$4, -2568(%rbp)
	movq	%rbx, -2576(%rbp)
	movb	$0, -2556(%rbp)
	call	_ZN12_GLOBAL__N_112ServerHolder6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2384
	call	_ZdlPv@PLT
.L2384:
	movq	-2784(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	movq	-2784(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer20TerminateConnectionsEv@PLT
	movq	-2872(%rbp), %rbx
	movq	%r13, %rdi
	movl	$-1506, %esi
	movw	%si, -2556(%rbp)
	movq	%r14, %rsi
	movq	%rbx, -2576(%rbp)
	movl	$237863048, -2560(%rbp)
	movq	$6, -2568(%rbp)
	movb	$0, -2554(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2385
	call	_ZdlPv@PLT
.L2385:
	movq	-2872(%rbp), %rbx
	movl	$136, %ecx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	$1, -2568(%rbp)
	movq	%rbx, -2576(%rbp)
	movw	%cx, -2560(%rbp)
	call	_ZN12_GLOBAL__N_113SocketWrapper6ExpectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-2576(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2386
	call	_ZdlPv@PLT
.L2386:
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%dx, -2576(%rbp)
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	cmpl	$3, -2796(%rbp)
	je	.L2387
	leaq	_ZL4loop(%rip), %rbx
	jmp	.L2389
	.p2align 4,,10
	.p2align 3
.L2645:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpl	$3, -2796(%rbp)
	je	.L2387
.L2389:
	cmpb	$0, -2576(%rbp)
	je	.L2645
	movq	-2848(%rbp), %rbx
	movb	$0, -2816(%rbp)
	leaq	-2832(%rbp), %r13
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC71(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-2608(%rbp), %r8
	movl	$457, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2608(%rbp), %rdi
	cmpq	-2896(%rbp), %rdi
	je	.L2425
	call	_ZdlPv@PLT
.L2425:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2390
	movq	(%rdi), %rax
	call	*8(%rax)
.L2390:
	movq	-2808(%rbp), %r13
	testq	%r13, %r13
	je	.L2391
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2392
	call	_ZdlPv@PLT
.L2392:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2391:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -2575(%rbp)
	jne	.L2379
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2394:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -2575(%rbp)
	je	.L2394
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2282:
	leaq	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2638:
	movq	-2848(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2808(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2322
	movq	(%rax), %r8
.L2322:
	leaq	-2832(%rbp), %r14
	movl	$420, %ecx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-2848(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2321
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	-2848(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2808(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2317
	movq	(%rax), %r8
.L2317:
	leaq	-2832(%rbp), %r14
	movl	$419, %ecx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-2848(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2316
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2640:
	movq	-2848(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2808(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2342
	movq	(%rax), %r8
.L2342:
	leaq	-2832(%rbp), %r15
	movl	$428, %ecx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-2848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2341
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2387:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -2575(%rbp)
	jne	.L2423
	.p2align 4,,10
	.p2align 3
.L2395:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -2575(%rbp)
	je	.L2395
.L2423:
	xorl	%eax, %eax
	movq	%r12, %rsi
	leaq	_ZL4loop(%rip), %rdi
	movw	%ax, -2576(%rbp)
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_init@PLT
	xorl	%ecx, %ecx
	movl	$5000, %edx
	movq	%r12, %rdi
	leaq	_ZN12_GLOBAL__N_17Timeout8set_flagEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2396:
	cmpb	$0, -2576(%rbp)
	jne	.L2646
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L2401:
	movq	-2784(%rbp), %rax
	movq	64(%rax), %rcx
	movq	56(%rax), %rdx
	cmpq	%rdx, %rcx
	jne	.L2396
	cmpq	$0, 120(%rax)
	jne	.L2396
.L2397:
	movq	%r12, %rdi
	leaq	_ZL4loop(%rip), %rbx
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -2575(%rbp)
	jne	.L2402
	.p2align 4,,10
	.p2align 3
.L2403:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -2575(%rbp)
	je	.L2403
.L2402:
	movq	%r13, %rdi
	call	_ZN12_GLOBAL__N_113SocketWrapper9ExpectEOFEv
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2628
	jmp	.L2365
.L2646:
	cmpq	%rdx, %rcx
	je	.L2399
.L2617:
	movq	-2848(%rbp), %rbx
	movb	$0, -2816(%rbp)
	leaq	-2832(%rbp), %r13
	movq	$0, -2808(%rbp)
	movq	%rbx, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-2856(%rbp), %rsi
	movq	%r15, %rdi
	leaq	.LC23(%rip), %r8
	leaq	.LC24(%rip), %rcx
	leaq	.LC50(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-2608(%rbp), %r8
	movl	$458, %ecx
	movq	%r13, %rdi
	leaq	.LC11(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-2608(%rbp), %rdi
	cmpq	-2896(%rbp), %rdi
	je	.L2404
	call	_ZdlPv@PLT
.L2404:
	movq	-2824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2405
	movq	(%rdi), %rax
	call	*8(%rax)
.L2405:
	movq	-2808(%rbp), %r13
	testq	%r13, %r13
	je	.L2406
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2407
	call	_ZdlPv@PLT
.L2407:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2406:
	movq	%r12, %rdi
	call	uv_timer_stop@PLT
	leaq	_ZN12_GLOBAL__N_17Timeout9mark_doneEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	cmpb	$0, -2575(%rbp)
	jne	.L2379
	leaq	_ZL4loop(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L2409:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
	cmpb	$0, -2575(%rbp)
	je	.L2409
	jmp	.L2379
.L2399:
	cmpq	$0, 120(%rax)
	jne	.L2617
	jmp	.L2397
.L2635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6378:
	.size	_ZN48InspectorSocketServerTest_InspectorSessions_Test8TestBodyEv, .-_ZN48InspectorSocketServerTest_InspectorSessions_Test8TestBodyEv
	.section	.rodata.str1.1
.LC74:
	.string	"status"
.LC75:
	.string	"0"
.LC76:
	.string	"Unable to connect: "
.LC77:
	.string	"(null)"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si, @function
_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si:
.LFB6306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC74(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-72(%rbp), %r12
	leaq	-84(%rbp), %r8
	pushq	%rbx
	movq	%r12, %rcx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$72, %rsp
	movl	%esi, -84(%rbp)
	leaq	.LC75(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -64(%rbp)
	je	.L2665
.L2648:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L2653
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2654
	call	_ZdlPv@PLT
.L2654:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2653:
	movb	$1, 288(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2666
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2665:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-72(%rbp), %rax
	movl	$19, %edx
	leaq	.LC76(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-84(%rbp), %edi
	call	uv_strerror@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2667
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L2650:
	movq	-56(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2651
	movq	(%rax), %r8
.L2651:
	leaq	-80(%rbp), %r13
	movl	$196, %ecx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2648
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2667:
	movq	-72(%rbp), %rax
	movl	$6, %edx
	leaq	.LC77(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2650
.L2666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6306:
	.size	_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si, .-_ZN12_GLOBAL__N_113SocketWrapper10Connected_EP12uv_connect_si
	.section	.rodata.str1.1
.LC78:
	.string	"err"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv, @function
_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv:
.LFB6283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL4loop(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_loop_close@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	jne	.L2688
.L2669:
	leaq	-56(%rbp), %r12
	leaq	-48(%rbp), %rdi
	movl	$0, -56(%rbp)
	leaq	-68(%rbp), %r8
	movq	%r12, %rcx
	leaq	.LC78(%rip), %rdx
	leaq	.LC75(%rip), %rsi
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -48(%rbp)
	je	.L2689
.L2670:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L2668
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2674
	call	_ZdlPv@PLT
.L2674:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2668:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2690
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2689:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-40(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2671
	movq	(%rax), %r8
.L2671:
	leaq	-64(%rbp), %r13
	movl	$85, %ecx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2670
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2670
	.p2align 4,,10
	.p2align 3
.L2688:
	movq	stderr(%rip), %rsi
	leaq	_ZL4loop(%rip), %rdi
	call	uv_print_all_handles@PLT
	jmp	.L2669
.L2690:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6283:
	.size	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv, .-_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.section	.rodata.str1.1
.LC79:
	.string	"uv_loop_init(&loop)"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv, @function
_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv:
.LFB6282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL4loop(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-56(%rbp), %r13
	leaq	-64(%rbp), %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_loop_init@PLT
	leaq	-48(%rbp), %rdi
	movq	%r13, %r8
	movq	%r12, %rcx
	leaq	.LC79(%rip), %rdx
	leaq	.LC75(%rip), %rsi
	movl	%eax, -56(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -48(%rbp)
	je	.L2707
.L2692:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L2691
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2696
	call	_ZdlPv@PLT
.L2696:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2691:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2708
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2707:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-40(%rbp), %rax
	leaq	.LC31(%rip), %r8
	testq	%rax, %rax
	je	.L2693
	movq	(%rax), %r8
.L2693:
	movl	$77, %ecx
	leaq	.LC11(%rip), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2692
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2692
.L2708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6282:
	.size	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv, .-_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag:
.LFB7972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	%rdi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$7, %r13
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	sarq	$5, %rax
	subq	$8, %rsp
	testq	%r13, %r13
	jle	.L2710
	salq	$7, %r13
	movq	8(%rdx), %r12
	addq	%rdi, %r13
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2711:
	cmpq	40(%rbx), %r12
	je	.L2755
.L2714:
	cmpq	72(%rbx), %r12
	je	.L2756
.L2716:
	cmpq	104(%rbx), %r12
	je	.L2757
.L2718:
	subq	$-128, %rbx
	cmpq	%rbx, %r13
	je	.L2758
.L2720:
	cmpq	%r12, 8(%rbx)
	jne	.L2711
	testq	%r12, %r12
	je	.L2731
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2711
.L2731:
	movq	%rbx, %rax
.L2713:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2755:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L2715
	movq	32(%rbx), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2714
.L2715:
	addq	$8, %rsp
	leaq	32(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2756:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L2717
	movq	64(%rbx), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2716
.L2717:
	addq	$8, %rsp
	leaq	64(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2757:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L2719
	movq	96(%rbx), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2718
.L2719:
	addq	$8, %rsp
	leaq	96(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2758:
	.cfi_restore_state
	movq	%r15, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
.L2710:
	cmpq	$2, %rax
	je	.L2721
	cmpq	$3, %rax
	je	.L2722
	cmpq	$1, %rax
	je	.L2759
.L2724:
	movq	%r15, %rax
	jmp	.L2713
.L2722:
	movq	8(%r14), %r12
	cmpq	%r12, 8(%rbx)
	je	.L2760
.L2727:
	addq	$32, %rbx
	jmp	.L2725
.L2759:
	movq	8(%r14), %r12
.L2726:
	cmpq	%r12, 8(%rbx)
	jne	.L2724
	testq	%r12, %r12
	je	.L2731
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2724
	jmp	.L2731
.L2721:
	movq	8(%r14), %r12
.L2725:
	cmpq	%r12, 8(%rbx)
	je	.L2761
.L2729:
	addq	$32, %rbx
	jmp	.L2726
.L2760:
	testq	%r12, %r12
	je	.L2731
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2727
	jmp	.L2731
.L2761:
	testq	%r12, %r12
	je	.L2731
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2729
	jmp	.L2731
	.cfi_endproc
.LFE7972:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	%esi, 48(%rdi)
	movq	24(%rdi), %rsi
	movq	16(%rdi), %rdi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIS8_EEET_SH_SH_T0_St26random_access_iterator_tag
	cmpq	%rax, 24(%rbx)
	je	.L2766
	movq	8(%rbx), %rax
	movl	48(%rbx), %ecx
	movq	24(%rax), %rdx
	cmpq	32(%rax), %rdx
	je	.L2764
	movq	%rdx, 32(%rax)
.L2764:
	addl	$1, (%rax)
	movl	%ecx, 48(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2766:
	.cfi_restore_state
	leaq	_ZZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE6345:
	.size	_ZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.startup
	.p2align 4
	.type	_GLOBAL__sub_I__ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E, @function
_GLOBAL__sub_I__ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E:
.LFB8815:
	.cfi_startproc
	endbr64
	jmp	_Z41__static_initialization_and_destruction_0ii.constprop.0
	.cfi_endproc
.LFE8815:
	.size	_GLOBAL__sub_I__ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E, .-_GLOBAL__sub_I__ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E
	.local	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E
	.comm	_ZN7testing8internal12TypeIdHelperIN12_GLOBAL__N_125InspectorSocketServerTestEE6dummy_E,1,1
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE, @object
	.size	_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE, 64
_ZTVN12_GLOBAL__N_125InspectorSocketServerTestE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN12_GLOBAL__N_124TestSocketServerDelegateE, @object
	.size	_ZTVN12_GLOBAL__N_124TestSocketServerDelegateE, 88
_ZTVN12_GLOBAL__N_124TestSocketServerDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegate12AssignServerEPN4node9inspector21InspectorSocketServerE
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEi
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetIdsEv
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegateD1Ev
	.quad	_ZN12_GLOBAL__N_124TestSocketServerDelegateD0Ev
	.weak	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_InspectorSessions_TestE10CreateTestEv
	.weak	_ZTV48InspectorSocketServerTest_InspectorSessions_Test
	.section	.data.rel.ro.local._ZTV48InspectorSocketServerTest_InspectorSessions_Test,"awG",@progbits,_ZTV48InspectorSocketServerTest_InspectorSessions_Test,comdat
	.align 8
	.type	_ZTV48InspectorSocketServerTest_InspectorSessions_Test, @object
	.size	_ZTV48InspectorSocketServerTest_InspectorSessions_Test, 64
_ZTV48InspectorSocketServerTest_InspectorSessions_Test:
	.quad	0
	.quad	0
	.quad	_ZN48InspectorSocketServerTest_InspectorSessions_TestD1Ev
	.quad	_ZN48InspectorSocketServerTest_InspectorSessions_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN48InspectorSocketServerTest_InspectorSessions_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerDoesNothing_TestE10CreateTestEv
	.weak	_ZTV48InspectorSocketServerTest_ServerDoesNothing_Test
	.section	.data.rel.ro.local._ZTV48InspectorSocketServerTest_ServerDoesNothing_Test,"awG",@progbits,_ZTV48InspectorSocketServerTest_ServerDoesNothing_Test,comdat
	.align 8
	.type	_ZTV48InspectorSocketServerTest_ServerDoesNothing_Test, @object
	.size	_ZTV48InspectorSocketServerTest_ServerDoesNothing_Test, 64
_ZTV48InspectorSocketServerTest_ServerDoesNothing_Test:
	.quad	0
	.quad	0
	.quad	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD1Ev
	.quad	_ZN48InspectorSocketServerTest_ServerDoesNothing_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN48InspectorSocketServerTest_ServerDoesNothing_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI51InspectorSocketServerTest_ServerWithoutTargets_TestE10CreateTestEv
	.weak	_ZTV51InspectorSocketServerTest_ServerWithoutTargets_Test
	.section	.data.rel.ro.local._ZTV51InspectorSocketServerTest_ServerWithoutTargets_Test,"awG",@progbits,_ZTV51InspectorSocketServerTest_ServerWithoutTargets_Test,comdat
	.align 8
	.type	_ZTV51InspectorSocketServerTest_ServerWithoutTargets_Test, @object
	.size	_ZTV51InspectorSocketServerTest_ServerWithoutTargets_Test, 64
_ZTV51InspectorSocketServerTest_ServerWithoutTargets_Test:
	.quad	0
	.quad	0
	.quad	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD1Ev
	.quad	_ZN51InspectorSocketServerTest_ServerWithoutTargets_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI48InspectorSocketServerTest_ServerCannotStart_TestE10CreateTestEv
	.weak	_ZTV48InspectorSocketServerTest_ServerCannotStart_Test
	.section	.data.rel.ro.local._ZTV48InspectorSocketServerTest_ServerCannotStart_Test,"awG",@progbits,_ZTV48InspectorSocketServerTest_ServerCannotStart_Test,comdat
	.align 8
	.type	_ZTV48InspectorSocketServerTest_ServerCannotStart_Test, @object
	.size	_ZTV48InspectorSocketServerTest_ServerCannotStart_Test, 64
_ZTV48InspectorSocketServerTest_ServerCannotStart_Test:
	.quad	0
	.quad	0
	.quad	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD1Ev
	.quad	_ZN48InspectorSocketServerTest_ServerCannotStart_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN48InspectorSocketServerTest_ServerCannotStart_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestE10CreateTestEv
	.weak	_ZTV67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test
	.section	.data.rel.ro.local._ZTV67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test,"awG",@progbits,_ZTV67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test,comdat
	.align 8
	.type	_ZTV67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test, @object
	.size	_ZTV67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test, 64
_ZTV67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test:
	.quad	0
	.quad	0
	.quad	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD1Ev
	.quad	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI59InspectorSocketServerTest_ClosingConnectionReportsDone_TestE10CreateTestEv
	.weak	_ZTV59InspectorSocketServerTest_ClosingConnectionReportsDone_Test
	.section	.data.rel.ro.local._ZTV59InspectorSocketServerTest_ClosingConnectionReportsDone_Test,"awG",@progbits,_ZTV59InspectorSocketServerTest_ClosingConnectionReportsDone_Test,comdat
	.align 8
	.type	_ZTV59InspectorSocketServerTest_ClosingConnectionReportsDone_Test, @object
	.size	_ZTV59InspectorSocketServerTest_ClosingConnectionReportsDone_Test, 64
_ZTV59InspectorSocketServerTest_ClosingConnectionReportsDone_Test:
	.quad	0
	.quad	0
	.quad	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD1Ev
	.quad	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI55InspectorSocketServerTest_ClosingSocketReportsDone_TestE10CreateTestEv
	.weak	_ZTV55InspectorSocketServerTest_ClosingSocketReportsDone_Test
	.section	.data.rel.ro.local._ZTV55InspectorSocketServerTest_ClosingSocketReportsDone_Test,"awG",@progbits,_ZTV55InspectorSocketServerTest_ClosingSocketReportsDone_Test,comdat
	.align 8
	.type	_ZTV55InspectorSocketServerTest_ClosingSocketReportsDone_Test, @object
	.size	_ZTV55InspectorSocketServerTest_ClosingSocketReportsDone_Test, 64
_ZTV55InspectorSocketServerTest_ClosingSocketReportsDone_Test:
	.quad	0
	.quad	0
	.quad	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD1Ev
	.quad	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI60InspectorSocketServerTest_TerminatingSessionReportsDone_TestE10CreateTestEv
	.weak	_ZTV60InspectorSocketServerTest_TerminatingSessionReportsDone_Test
	.section	.data.rel.ro.local._ZTV60InspectorSocketServerTest_TerminatingSessionReportsDone_Test,"awG",@progbits,_ZTV60InspectorSocketServerTest_TerminatingSessionReportsDone_Test,comdat
	.align 8
	.type	_ZTV60InspectorSocketServerTest_TerminatingSessionReportsDone_Test, @object
	.size	_ZTV60InspectorSocketServerTest_TerminatingSessionReportsDone_Test, 64
_ZTV60InspectorSocketServerTest_TerminatingSessionReportsDone_Test:
	.quad	0
	.quad	0
	.quad	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD1Ev
	.quad	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI54InspectorSocketServerTest_FailsToBindToNodejsHost_TestE10CreateTestEv
	.weak	_ZTV54InspectorSocketServerTest_FailsToBindToNodejsHost_Test
	.section	.data.rel.ro.local._ZTV54InspectorSocketServerTest_FailsToBindToNodejsHost_Test,"awG",@progbits,_ZTV54InspectorSocketServerTest_FailsToBindToNodejsHost_Test,comdat
	.align 8
	.type	_ZTV54InspectorSocketServerTest_FailsToBindToNodejsHost_Test, @object
	.size	_ZTV54InspectorSocketServerTest_FailsToBindToNodejsHost_Test, 64
_ZTV54InspectorSocketServerTest_FailsToBindToNodejsHost_Test:
	.quad	0
	.quad	0
	.quad	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD1Ev
	.quad	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI42InspectorSocketServerTest_BindsToIpV6_TestE10CreateTestEv
	.weak	_ZTV42InspectorSocketServerTest_BindsToIpV6_Test
	.section	.data.rel.ro.local._ZTV42InspectorSocketServerTest_BindsToIpV6_Test,"awG",@progbits,_ZTV42InspectorSocketServerTest_BindsToIpV6_Test,comdat
	.align 8
	.type	_ZTV42InspectorSocketServerTest_BindsToIpV6_Test, @object
	.size	_ZTV42InspectorSocketServerTest_BindsToIpV6_Test, 64
_ZTV42InspectorSocketServerTest_BindsToIpV6_Test:
	.quad	0
	.quad	0
	.quad	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD1Ev
	.quad	_ZN42InspectorSocketServerTest_BindsToIpV6_TestD0Ev
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest5SetUpEv
	.quad	_ZN12_GLOBAL__N_125InspectorSocketServerTest8TearDownEv
	.quad	_ZN42InspectorSocketServerTest_BindsToIpV6_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.globl	_ZN42InspectorSocketServerTest_BindsToIpV6_Test10test_info_E
	.bss
	.align 8
	.type	_ZN42InspectorSocketServerTest_BindsToIpV6_Test10test_info_E, @object
	.size	_ZN42InspectorSocketServerTest_BindsToIpV6_Test10test_info_E, 8
_ZN42InspectorSocketServerTest_BindsToIpV6_Test10test_info_E:
	.zero	8
	.globl	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test10test_info_E
	.align 8
	.type	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test10test_info_E, @object
	.size	_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test10test_info_E, 8
_ZN54InspectorSocketServerTest_FailsToBindToNodejsHost_Test10test_info_E:
	.zero	8
	.globl	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test10test_info_E
	.align 8
	.type	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test10test_info_E, @object
	.size	_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test10test_info_E, 8
_ZN60InspectorSocketServerTest_TerminatingSessionReportsDone_Test10test_info_E:
	.zero	8
	.globl	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test10test_info_E
	.align 8
	.type	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test10test_info_E, @object
	.size	_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test10test_info_E, 8
_ZN55InspectorSocketServerTest_ClosingSocketReportsDone_Test10test_info_E:
	.zero	8
	.globl	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test10test_info_E
	.align 8
	.type	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test10test_info_E, @object
	.size	_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test10test_info_E, 8
_ZN59InspectorSocketServerTest_ClosingConnectionReportsDone_Test10test_info_E:
	.zero	8
	.globl	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test10test_info_E
	.align 8
	.type	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test10test_info_E, @object
	.size	_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test10test_info_E, 8
_ZN67InspectorSocketServerTest_StoppingServerDoesNotKillConnections_Test10test_info_E:
	.zero	8
	.globl	_ZN48InspectorSocketServerTest_ServerCannotStart_Test10test_info_E
	.align 8
	.type	_ZN48InspectorSocketServerTest_ServerCannotStart_Test10test_info_E, @object
	.size	_ZN48InspectorSocketServerTest_ServerCannotStart_Test10test_info_E, 8
_ZN48InspectorSocketServerTest_ServerCannotStart_Test10test_info_E:
	.zero	8
	.globl	_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test10test_info_E
	.align 8
	.type	_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test10test_info_E, @object
	.size	_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test10test_info_E, 8
_ZN51InspectorSocketServerTest_ServerWithoutTargets_Test10test_info_E:
	.zero	8
	.globl	_ZN48InspectorSocketServerTest_ServerDoesNothing_Test10test_info_E
	.align 8
	.type	_ZN48InspectorSocketServerTest_ServerDoesNothing_Test10test_info_E, @object
	.size	_ZN48InspectorSocketServerTest_ServerDoesNothing_Test10test_info_E, 8
_ZN48InspectorSocketServerTest_ServerDoesNothing_Test10test_info_E:
	.zero	8
	.globl	_ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E
	.align 8
	.type	_ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E, @object
	.size	_ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E, 8
_ZN48InspectorSocketServerTest_InspectorSessions_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"../test/cctest/test_inspector_socket_server.cc:332"
	.section	.rodata.str1.1
.LC81:
	.string	"(session_id_) == (session_id)"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"virtual void {anonymous}::TestSocketServerDelegate::EndSession(int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEiE4args, @object
	.size	_ZZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEiE4args, 24
_ZZN12_GLOBAL__N_124TestSocketServerDelegate10EndSessionEiE4args:
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"../test/cctest/test_inspector_socket_server.cc:327"
	.align 8
.LC84:
	.string	"virtual void {anonymous}::TestSocketServerDelegate::MessageReceived(int, const string&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZN12_GLOBAL__N_124TestSocketServerDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC83
	.quad	.LC81
	.quad	.LC84
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"../test/cctest/test_inspector_socket_server.cc:321"
	.align 8
.LC86:
	.string	"(targets_.end()) != (std::find(targets_.begin(), targets_.end(), target_id))"
	.align 8
.LC87:
	.string	"virtual void {anonymous}::TestSocketServerDelegate::StartSession(int, const string&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZN12_GLOBAL__N_124TestSocketServerDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"../test/cctest/test_inspector_socket_server.cc:223"
	.section	.rodata.str1.1
.LC89:
	.string	"(0) == (err)"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"static void {anonymous}::SocketWrapper::WriteDone_(uv_write_t*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_siE4args, @object
	.size	_ZZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_siE4args, 24
_ZZN12_GLOBAL__N_113SocketWrapper10WriteDone_EP10uv_write_siE4args:
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"../test/cctest/test_inspector_socket_server.cc:178"
	.section	.rodata.str1.1
.LC92:
	.string	"(err) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"void {anonymous}::SocketWrapper::Write(const string&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZN12_GLOBAL__N_113SocketWrapper5WriteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"../test/cctest/test_inspector_socket_server.cc:114"
	.align 8
.LC95:
	.string	"void {anonymous}::SocketWrapper::Connect(const string&, int, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0, @object
	.size	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0, 24
_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args_0:
	.quad	.LC94
	.quad	.LC89
	.quad	.LC95
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"../test/cctest/test_inspector_socket_server.cc:112"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args, @object
	.size	_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args, 24
_ZZN12_GLOBAL__N_113SocketWrapper7ConnectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEibE4args:
	.quad	.LC96
	.quad	.LC89
	.quad	.LC95
	.local	_ZL4loop
	.comm	_ZL4loop,848,32
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.data.rel.ro
	.align 8
.LC46:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC47:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC48:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC52:
	.quad	3543824036068086856
	.quad	7599634293940302112
	.align 16
.LC53:
	.quad	5773728446201488244
	.quad	8317145084708810610
	.align 16
.LC54:
	.quad	7233188265125546509
	.quad	8030870748461546085
	.align 16
.LC55:
	.quad	8017262814553271139
	.quad	7957695015191670382
	.align 16
.LC56:
	.quad	7233188265125552186
	.quad	6281786342087724389
	.align 16
.LC57:
	.quad	8387227955861086821
	.quad	4212115132259713325
	.align 16
.LC58:
	.quad	5706421604862673952
	.quad	8019313819913702485
	.align 16
.LC59:
	.quad	3983813568291621679
	.quad	939578029636800632
	.align 16
.LC60:
	.quad	3471766442030158920
	.quad	7233135182548579360
	.section	.data.rel.ro
	.align 8
.LC66:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
