	.file	"test_base64.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB5736:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5736:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED2Ev:
.LFB7311:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7311:
	.size	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED1Ev,_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED2Ev:
.LFB7319:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7319:
	.size	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED1Ev,_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED0Ev:
.LFB7321:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7321:
	.size	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED0Ev:
.LFB7313:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7313:
	.size	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED0Ev
	.section	.text._ZN22Base64Test_Decode_TestD2Ev,"axG",@progbits,_ZN22Base64Test_Decode_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22Base64Test_Decode_TestD2Ev
	.type	_ZN22Base64Test_Decode_TestD2Ev, @function
_ZN22Base64Test_Decode_TestD2Ev:
.LFB7307:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV22Base64Test_Decode_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE7307:
	.size	_ZN22Base64Test_Decode_TestD2Ev, .-_ZN22Base64Test_Decode_TestD2Ev
	.weak	_ZN22Base64Test_Decode_TestD1Ev
	.set	_ZN22Base64Test_Decode_TestD1Ev,_ZN22Base64Test_Decode_TestD2Ev
	.section	.text._ZN22Base64Test_Decode_TestD0Ev,"axG",@progbits,_ZN22Base64Test_Decode_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22Base64Test_Decode_TestD0Ev
	.type	_ZN22Base64Test_Decode_TestD0Ev, @function
_ZN22Base64Test_Decode_TestD0Ev:
.LFB7309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV22Base64Test_Decode_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7309:
	.size	_ZN22Base64Test_Decode_TestD0Ev, .-_ZN22Base64Test_Decode_TestD0Ev
	.section	.text._ZN22Base64Test_Encode_TestD2Ev,"axG",@progbits,_ZN22Base64Test_Encode_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22Base64Test_Encode_TestD2Ev
	.type	_ZN22Base64Test_Encode_TestD2Ev, @function
_ZN22Base64Test_Encode_TestD2Ev:
.LFB7315:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV22Base64Test_Encode_Test(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE7315:
	.size	_ZN22Base64Test_Encode_TestD2Ev, .-_ZN22Base64Test_Encode_TestD2Ev
	.weak	_ZN22Base64Test_Encode_TestD1Ev
	.set	_ZN22Base64Test_Encode_TestD1Ev,_ZN22Base64Test_Encode_TestD2Ev
	.section	.text._ZN22Base64Test_Encode_TestD0Ev,"axG",@progbits,_ZN22Base64Test_Encode_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN22Base64Test_Encode_TestD0Ev
	.type	_ZN22Base64Test_Encode_TestD0Ev, @function
_ZN22Base64Test_Encode_TestD0Ev:
.LFB7317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV22Base64Test_Encode_Test(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7317:
	.size	_ZN22Base64Test_Encode_TestD0Ev, .-_ZN22Base64Test_Encode_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv:
.LFB7374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV22Base64Test_Encode_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7374:
	.size	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv:
.LFB7373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV22Base64Test_Decode_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7373:
	.size	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0:
.LFB7465:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7465:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0:
.LFB7466:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L24:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7466:
	.size	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0.constprop.0:
.LFB7486:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$29, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC4(%rip), %xmm0
	movabsq	$7310293695288603493, %rcx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rcx, 16(%rax)
	movl	$1663972406, 24(%rax)
	movb	$99, 28(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7486:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0.constprop.0
	.section	.rodata.str1.1
.LC5:
	.string	""
.LC6:
	.string	"YQ=="
.LC7:
	.string	"buffer"
.LC8:
	.string	"base64_string"
.LC9:
	.string	"../test/cctest/test_base64.cc"
.LC10:
	.string	"YWI="
.LC11:
	.string	"YWJj"
.LC12:
	.string	"YWJjZA=="
.LC13:
	.string	"YWJjZGU="
.LC14:
	.string	"YWJjZGVm"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.ascii	"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed"
	.ascii	" do eiusmod tempor incididunt ut labore et dolore magna aliq"
	.ascii	"ua. Ut enim ad minim veniam, quis nostrud exercitation ullam"
	.ascii	"co labori"
	.string	"s nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
	.align 8
.LC16:
	.ascii	"TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBp"
	.ascii	"c2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQg"
	.ascii	"dXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEuIFV0IGVuaW0gYWQg"
	.ascii	"bWluaW0gdmVuaWFtLCBxdWlzIG5vc3RydWQgZXhlcmNpdGF0aW9uIHVsbGFt"
	.ascii	"Y28gbGFib3JpcyBuaXNpIHV0IGFsaXF1aXAgZXggZWEgY29tbW9kbyBjb25z"
	.ascii	"ZXF1YXQuIER1aXMgYXV0ZSBpcnVyZSBkb2xvciBp"
	.string	"biByZXByZWhlbmRlcml0IGluIHZvbHVwdGF0ZSB2ZWxpdCBlc3NlIGNpbGx1bSBkb2xvcmUgZXUgZnVnaWF0IG51bGxhIHBhcmlhdHVyLiBFeGNlcHRldXIgc2ludCBvY2NhZWNhdCBjdXBpZGF0YXQgbm9uIHByb2lkZW50LCBzdW50IGluIGN1bHBhIHF1aSBvZmZpY2lhIGRlc2VydW50IG1vbGxpdCBhbmltIGlkIGVzdCBsYWJvcnVtLg=="
	.text
	.align 2
	.p2align 4
	.globl	_ZN22Base64Test_Encode_Test8TestBodyEv
	.type	_ZN22Base64Test_Encode_Test8TestBodyEv, @function
_ZN22Base64Test_Encode_Test8TestBodyEv:
.LFB5864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znam@PLT
	leaq	.LC6(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	movl	$1027428697, (%rax)
	movq	%rax, %r8
	movq	%rax, %r13
	leaq	.LC8(%rip), %rsi
	movb	$0, 4(%rax)
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L125
.L30:
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	je	.L33
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L33:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movl	$5, %edi
	call	_Znam@PLT
	leaq	.LC10(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	movl	$1028216665, (%rax)
	movq	%rax, %r8
	movq	%rax, %r13
	leaq	.LC8(%rip), %rsi
	movb	$0, 4(%rax)
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L126
.L35:
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	je	.L38
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L38:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movl	$5, %edi
	call	_Znam@PLT
	leaq	.LC11(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	movl	$1783256921, (%rax)
	movq	%rax, %r8
	movq	%rax, %r13
	leaq	.LC8(%rip), %rsi
	movb	$0, 4(%rax)
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L127
.L40:
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	je	.L43
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L43:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movl	$9, %edi
	call	_Znam@PLT
	leaq	.LC12(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	leaq	.LC8(%rip), %rsi
	movabsq	$4412755066479073113, %rax
	movq	%rax, 0(%r13)
	movq	%r13, %r8
	movb	$0, 8(%r13)
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L128
.L45:
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	je	.L48
.L134:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L48:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movl	$9, %edi
	call	_Znam@PLT
	leaq	.LC13(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	leaq	.LC8(%rip), %rsi
	movabsq	$4419517062989895513, %rax
	movq	%rax, 0(%r13)
	movq	%r13, %r8
	movb	$0, 8(%r13)
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L129
.L50:
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	je	.L53
.L135:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L53:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movl	$9, %edi
	call	_Znam@PLT
	leaq	.LC14(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	leaq	.LC8(%rip), %rsi
	movabsq	$7878563051787147097, %rax
	movq	%rax, 0(%r13)
	movq	%r13, %r8
	movb	$0, 8(%r13)
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L130
.L55:
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	je	.L58
.L136:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L58:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movl	$597, %edi
	call	_Znam@PLT
	movl	$84, %r10d
	leaq	_ZZN4nodeL13base64_encodeEPKcmPcmE5table(%rip), %r8
	movq	%rax, %r13
	movb	$0, 596(%rax)
	leaq	.LC15(%rip), %rax
	movq	%r13, %rsi
	leaq	444(%rax), %r9
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L131:
	movsbl	(%rax), %edx
	shrl	$2, %edx
	andl	$63, %edx
	movzbl	(%r8,%rdx), %r10d
.L61:
	movzbl	1(%rax), %edx
	movsbl	(%rax), %ecx
	movb	%r10b, (%rsi)
	addq	$3, %rax
	movzbl	-1(%rax), %edi
	addq	$4, %rsi
	movl	%edx, %r10d
	sall	$4, %ecx
	shrl	$4, %r10d
	andl	$48, %ecx
	sall	$2, %edx
	orl	%r10d, %ecx
	andl	$60, %edx
	movzbl	(%r8,%rcx), %ecx
	movb	%cl, -3(%rsi)
	movl	%edi, %ecx
	andl	$63, %edi
	shrl	$6, %ecx
	andl	$3, %ecx
	orl	%ecx, %edx
	movzbl	(%r8,%rdx), %edx
	movb	%dl, -2(%rsi)
	movzbl	(%r8,%rdi), %edx
	movb	%dl, -1(%rsi)
	cmpq	%r9, %rax
	jne	.L131
	movq	%r13, %r8
	leaq	.LC16(%rip), %rcx
	movq	%r12, %rdi
	movl	$1027434316, 592(%r13)
	leaq	.LC7(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L132
.L62:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L65
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L65:
	movq	%r13, %rdi
	call	_ZdaPv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L133
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L46
	movq	(%rax), %r8
.L46:
	leaq	-72(%rbp), %r15
	movl	$17, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	jne	.L134
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L31
	movq	(%rax), %r8
.L31:
	leaq	-72(%rbp), %r15
	movl	$17, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L36
	movq	(%rax), %r8
.L36:
	leaq	-72(%rbp), %r15
	movl	$17, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L41
	movq	(%rax), %r8
.L41:
	leaq	-72(%rbp), %r15
	movl	$17, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L51
	movq	(%rax), %r8
.L51:
	leaq	-72(%rbp), %r15
	movl	$17, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	jne	.L135
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L56
	movq	(%rax), %r8
.L56:
	leaq	-72(%rbp), %r15
	movl	$17, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	jne	.L136
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L63
	movq	(%rax), %r8
.L63:
	leaq	-72(%rbp), %r12
	movl	$17, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L62
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5864:
	.size	_ZN22Base64Test_Encode_Test8TestBodyEv, .-_ZN22Base64Test_Encode_Test8TestBodyEv
	.section	.text._ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_,"axG",@progbits,_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_,comdat
	.p2align 4
	.weak	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
	.type	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_, @function
_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_:
.LFB6970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%r8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	_ZN4node14unbase64_tableE(%rip), %rbx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L152:
	cmpq	%rcx, %rax
	jnb	.L142
	cmpb	$61, %r10b
	je	.L142
.L139:
	movzbl	(%rdx,%rax), %r11d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r11, %r10
	movzbl	(%rbx,%r11), %r11d
	cmpb	$63, %r11b
	ja	.L152
	cmpq	%rcx, %rax
	jnb	.L142
	cmpq	%rsi, (%r9)
	jb	.L144
	.p2align 4,,10
	.p2align 3
.L142:
	xorl	%eax, %eax
.L137:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	cmpq	%rcx, %rax
	jnb	.L142
	cmpb	$61, %r10b
	je	.L142
.L144:
	movzbl	(%rdx,%rax), %r12d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r12, %r10
	movzbl	(%rbx,%r12), %r12d
	cmpb	$63, %r12b
	ja	.L153
	movq	(%r9), %rax
	sall	$2, %r11d
	leaq	1(%rax), %r10
	movq	%r10, (%r9)
	movl	%r12d, %r10d
	sarl	$4, %r10d
	orl	%r10d, %r11d
	movb	%r11b, (%rdi,%rax)
	movq	(%r8), %rax
	cmpq	%rcx, %rax
	jnb	.L142
	cmpq	%rsi, (%r9)
	jb	.L146
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L154:
	cmpb	$61, %r10b
	je	.L142
	cmpq	%rcx, %rax
	jnb	.L142
.L146:
	movzbl	(%rdx,%rax), %r11d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r11, %r10
	movzbl	(%rbx,%r11), %r11d
	cmpb	$63, %r11b
	ja	.L154
	movq	(%r9), %r10
	sall	$4, %r12d
	leaq	1(%r10), %rax
	movq	%rax, (%r9)
	movl	%r11d, %eax
	sarl	$2, %eax
	orl	%r12d, %eax
	movb	%al, (%rdi,%r10)
	movq	(%r8), %rax
	cmpq	%rcx, %rax
	jnb	.L142
	cmpq	%rsi, (%r9)
	jb	.L148
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L155:
	cmpb	$61, %r12b
	je	.L142
	cmpq	%rcx, %rax
	jnb	.L142
.L148:
	movzbl	(%rdx,%rax), %r10d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r10, %r12
	movzbl	(%rbx,%r10), %r10d
	cmpb	$63, %r10b
	ja	.L155
	movq	(%r9), %rax
	sall	$6, %r11d
	orl	%r11d, %r10d
	leaq	1(%rax), %rdx
	movq	%rdx, (%r9)
	movb	%r10b, (%rdi,%rax)
	cmpq	%rcx, (%r8)
	jnb	.L142
	cmpq	%rsi, (%r9)
	setb	%al
	jmp	.L137
	.cfi_endproc
.LFE6970:
	.size	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_, .-_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
	.section	.text._ZN4node18base64_decode_fastIcEEmPcmPKT_mm,"axG",@progbits,_ZN4node18base64_decode_fastIcEEmPcmPKT_mm,comdat
	.p2align 4
	.weak	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	.type	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm, @function
_ZN4node18base64_decode_fastIcEEmPcmPKT_mm:
.LFB6755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZN4node14unbase64_tableE(%rip), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rsi
	movq	$0, -72(%rbp)
	cmovbe	%rsi, %r8
	movq	$0, -64(%rbp)
	movq	%r8, %rax
	xorl	%r8d, %r8d
	mulq	%rdx
	movq	%rdx, %r14
	andq	$-2, %rdx
	shrq	%r14
	addq	%rdx, %r14
	xorl	%edx, %edx
	andq	$-4, %rcx
	jne	.L158
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L159:
	movl	%eax, %edi
	movl	%eax, %r9d
	addq	$4, %rdx
	shrl	$22, %edi
	shrl	$20, %r9d
	movq	%rdx, -72(%rbp)
	andl	$3, %r9d
	andl	$-4, %edi
	orl	%r9d, %edi
	movl	%eax, %r9d
	movb	%dil, 0(%r13,%r8)
	movl	%eax, %edi
	shrl	$10, %r9d
	shrl	$12, %edi
	andl	$15, %r9d
	andl	$-16, %edi
	orl	%r9d, %edi
	movb	%dil, 1(%r13,%r8)
	movl	%eax, %edi
	andl	$63, %eax
	shrl	$2, %edi
	andl	$-64, %edi
	orl	%edi, %eax
	movb	%al, 2(%r13,%r8)
	addq	$3, %r8
	movq	%r8, -64(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L157
.L158:
	cmpq	%r14, %r8
	jnb	.L157
	movzbl	(%r12,%rdx), %eax
	movzbl	3(%r12,%rdx), %edi
	movsbl	(%rbx,%rax), %eax
	movsbl	(%rbx,%rdi), %edi
	sall	$24, %eax
	orl	%edi, %eax
	movzbl	1(%r12,%rdx), %edi
	movsbl	(%rbx,%rdi), %edi
	sall	$16, %edi
	orl	%edi, %eax
	movzbl	2(%r12,%rdx), %edi
	movsbl	(%rbx,%rdi), %edi
	sall	$8, %edi
	orl	%edi, %eax
	testl	$-2139062144, %eax
	je	.L159
	leaq	-64(%rbp), %r9
	leaq	-72(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
	movq	-88(%rbp), %rsi
	testb	%al, %al
	je	.L167
	movq	-72(%rbp), %rdx
	movq	%r15, %rcx
	movq	-64(%rbp), %r8
	subq	%rdx, %rcx
	andq	$-4, %rcx
	addq	%rdx, %rcx
	cmpq	%rdx, %rcx
	ja	.L158
	.p2align 4,,10
	.p2align 3
.L157:
	cmpq	%rdx, %r15
	ja	.L168
.L156:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	cmpq	%r8, %rsi
	jbe	.L156
	leaq	-64(%rbp), %r9
	leaq	-72(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
.L167:
	movq	-64(%rbp), %r8
	jmp	.L156
.L169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6755:
	.size	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm, .-_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	.section	.rodata.str1.1
.LC17:
	.string	"YQ"
.LC18:
	.string	"a"
.LC19:
	.string	"string"
.LC20:
	.string	"Y Q"
.LC21:
	.string	"Y Q "
.LC22:
	.string	" Y Q"
.LC23:
	.string	"Y Q=="
.LC24:
	.string	"YQ =="
.LC25:
	.string	"YQ == junk"
.LC26:
	.string	"YWI"
.LC27:
	.string	"ab"
.LC28:
	.string	"abc"
.LC29:
	.string	"YWJjZA"
.LC30:
	.string	"abcd"
.LC31:
	.string	"YW Jj ZA =="
.LC32:
	.string	"abcde"
.LC33:
	.string	"abcdef"
.LC34:
	.string	"Y WJjZGVm"
.LC35:
	.string	"YW JjZGVm"
.LC36:
	.string	"YWJ jZGVm"
.LC37:
	.string	"YWJj ZGVm"
.LC38:
	.string	"Y W J j Z G V m"
.LC39:
	.string	"Y   W\n JjZ \nG Vm"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.ascii	"TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBp"
	.ascii	"c2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQg"
	.ascii	"dXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEuIFV0IGVuaW0gYWQg"
	.ascii	"bWluaW0gdmVuaWFtLCBxdWlzIG5vc3RydWQgZXhlcmNpdGF0aW9uIHVsbGFt"
	.ascii	"Y28gbGFib3JpcyBuaXNpIHV0IGFsaXF1aXAgZXggZWEgY29tbW9kbyBjb25z"
	.ascii	"ZXF1YXQuIER1aXMgYXV0ZSBpcnVyZSBkb2xvci"
	.string	"BpbiByZXByZWhlbmRlcml0IGluIHZvbHVwdGF0ZSB2ZWxpdCBlc3NlIGNpbGx1bSBkb2xvcmUgZXUgZnVnaWF0IG51bGxhIHBhcmlhdHVyLiBFeGNlcHRldXIgc2ludCBvY2NhZWNhdCBjdXBpZGF0YXQgbm9uIHByb2lkZW50LCBzdW50IGluIGN1bHBhIHF1aSBvZmZpY2lhIGRlc2VydW50IG1vbGxpdCBhbmltIGlkIGVzdCBsYWJvcnVtLg"
	.align 8
.LC41:
	.ascii	"TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBp"
	.ascii	"c2Npbmcg\nZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bn"
	.ascii	"QgdXQgbGFib3JlIGV0\nIGRvbG9yZSBtYWduYSBhbGlxdWEuIFV0IGVuaW0g"
	.ascii	"YWQgbWluaW0gdmVuaWFtLCBxdWlz\nIG5vc3RydWQgZXhlcmNpdGF0aW9uIH"
	.ascii	"VsbGFtY28gbGFib3JpcyBuaXNpIHV0IGFsaXF1\naXAgZXggZWEgY29tbW9k"
	.ascii	"byBjb25zZXF1YXQuIER1aXMgYXV0ZSBpcnVyZSBkb2xvciBp\nbiB"
	.string	"yZXByZWhlbmRlcml0IGluIHZvbHVwdGF0ZSB2ZWxpdCBlc3NlIGNpbGx1bSBkb2xv\ncmUgZXUgZnVnaWF0IG51bGxhIHBhcmlhdHVyLiBFeGNlcHRldXIgc2ludCBvY2NhZWNh\ndCBjdXBpZGF0YXQgbm9uIHByb2lkZW50LCBzdW50IGluIGN1bHBhIHF1aSBvZmZpY2lh\nIGRlc2VydW50IG1vbGxpdCBhbmltIGlkIGVzdCBsYWJvcnVtLg=="
	.align 8
.LC42:
	.ascii	"TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBp"
	.ascii	"c2Npbmcg\nZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bn"
	.ascii	"QgdXQgbGFib3JlIGV0\nIGRvbG9yZSBtYWduYSBhbGlxdWEuIFV0IGVuaW0g"
	.ascii	"YWQgbWluaW0gdmVuaWFtLCBxdWlz\nIG5vc3RydWQgZXhlcmNpdGF0aW9uIH"
	.ascii	"VsbGFtY28gbGFib3JpcyBuaXNpIHV0IGFsaXF1\naXAgZXggZWEgY29tbW9k"
	.ascii	"byBjb25zZXF1YXQuIER1aXMgYXV0ZSBpcnVyZSBkb2xvciBp\nb"
	.string	"iByZXByZWhlbmRlcml0IGluIHZvbHVwdGF0ZSB2ZWxpdCBlc3NlIGNpbGx1bSBkb2xv\ncmUgZXUgZnVnaWF0IG51bGxhIHBhcmlhdHVyLiBFeGNlcHRldXIgc2ludCBvY2NhZWNh\ndCBjdXBpZGF0YXQgbm9uIHByb2lkZW50LCBzdW50IGluIGN1bHBhIHF1aSBvZmZpY2lh\nIGRlc2VydW50IG1vbGxpdCBhbmltIGlkIGVzdCBsYWJvcnVtLg"
	.text
	.align 2
	.p2align 4
	.globl	_ZN22Base64Test_Decode_Test8TestBodyEv
	.type	_ZN22Base64Test_Decode_Test8TestBodyEv, @function
_ZN22Base64Test_Decode_Test8TestBodyEv:
.LFB5874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znam@PLT
	movl	$1, %r8d
	movl	$2, %ecx
	leaq	.LC17(%rip), %rdx
	movb	$0, 1(%rax)
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L498
.L171:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L174
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L174:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$2, %edi
	call	_Znam@PLT
	movl	$2, %r8d
	movl	$3, %ecx
	leaq	.LC20(%rip), %rdx
	movb	$0, 1(%rax)
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L499
.L176:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L179
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L179:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$2, %edi
	call	_Znam@PLT
	movl	$3, %r8d
	movl	$4, %ecx
	leaq	.LC21(%rip), %rdx
	movb	$0, 1(%rax)
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L500
.L181:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L184
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L185
	call	_ZdlPv@PLT
.L185:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L184:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$2, %edi
	call	_Znam@PLT
	movl	$3, %r8d
	movl	$4, %ecx
	leaq	.LC22(%rip), %rdx
	movb	$0, 1(%rax)
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L501
.L186:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L189
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L190
	call	_ZdlPv@PLT
.L190:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L189:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$2, %edi
	call	_Znam@PLT
	movl	$2, %r8d
	movl	$5, %ecx
	leaq	.LC23(%rip), %rdx
	movb	$0, 1(%rax)
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L502
.L191:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L194
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L194:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$2, %edi
	call	_Znam@PLT
	movl	$2, %r8d
	movl	$5, %ecx
	leaq	.LC24(%rip), %rdx
	movb	$0, 1(%rax)
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L503
.L196:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L199
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L199:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$2, %edi
	call	_Znam@PLT
	movl	$7, %r8d
	movl	$10, %ecx
	leaq	.LC25(%rip), %rdx
	movb	$0, 1(%rax)
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L504
.L201:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L204
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L204:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$3, %edi
	call	_Znam@PLT
	movl	$2, %r8d
	movl	$3, %ecx
	leaq	.LC26(%rip), %rdx
	movb	$0, 2(%rax)
	movq	%rax, %r14
	movl	$2, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC27(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L505
.L206:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L209
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L209:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$3, %edi
	call	_Znam@PLT
	movl	$2, %r8d
	movl	$4, %ecx
	leaq	.LC10(%rip), %rdx
	movb	$0, 2(%rax)
	movq	%rax, %r14
	movl	$2, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC27(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L506
.L211:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L214
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L214:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$4, %edi
	call	_Znam@PLT
	movl	$3, %r8d
	movl	$4, %ecx
	leaq	.LC11(%rip), %rdx
	movb	$0, 3(%rax)
	movq	%rax, %r14
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC28(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L507
.L216:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L219
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L219:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$5, %edi
	call	_Znam@PLT
	movl	$4, %r8d
	movl	$6, %ecx
	leaq	.LC29(%rip), %rdx
	movb	$0, 4(%rax)
	movq	%rax, %r14
	movl	$4, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L508
.L221:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L224
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L224:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$5, %edi
	call	_Znam@PLT
	movl	$4, %r8d
	movl	$8, %ecx
	leaq	.LC12(%rip), %rdx
	movb	$0, 4(%rax)
	movq	%rax, %r14
	movl	$4, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L509
.L226:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L229
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L229:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$5, %edi
	call	_Znam@PLT
	movl	$7, %r8d
	movl	$11, %ecx
	leaq	.LC31(%rip), %rdx
	movb	$0, 4(%rax)
	movq	%rax, %r14
	movl	$4, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC30(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L510
.L231:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L234
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L234:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$6, %edi
	call	_Znam@PLT
	movl	$5, %r8d
	movl	$8, %ecx
	leaq	.LC13(%rip), %rdx
	movb	$0, 5(%rax)
	movq	%rax, %r14
	movl	$5, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC32(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L511
.L236:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L239
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L239:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$7, %edi
	call	_Znam@PLT
	movl	$6, %r8d
	movl	$8, %ecx
	leaq	.LC14(%rip), %rdx
	movb	$0, 6(%rax)
	movq	%rax, %r14
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L512
.L241:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L244
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L244:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$7, %edi
	call	_Znam@PLT
	movl	$7, %r8d
	movl	$9, %ecx
	leaq	.LC34(%rip), %rdx
	movb	$0, 6(%rax)
	movq	%rax, %r14
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L513
.L246:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L249
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L250
	call	_ZdlPv@PLT
.L250:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L249:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$7, %edi
	call	_Znam@PLT
	movl	$7, %r8d
	movl	$9, %ecx
	leaq	.LC35(%rip), %rdx
	movb	$0, 6(%rax)
	movq	%rax, %r14
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L514
.L251:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L254
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L254:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$7, %edi
	call	_Znam@PLT
	movl	$7, %r8d
	movl	$9, %ecx
	leaq	.LC36(%rip), %rdx
	movb	$0, 6(%rax)
	movq	%rax, %r14
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L515
.L256:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L259
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L259:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$7, %edi
	call	_Znam@PLT
	movl	$7, %r8d
	movl	$9, %ecx
	leaq	.LC37(%rip), %rdx
	movb	$0, 6(%rax)
	movq	%rax, %r14
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L516
.L261:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L264
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L264:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$7, %edi
	call	_Znam@PLT
	movl	$11, %r8d
	movl	$15, %ecx
	leaq	.LC38(%rip), %rdx
	movb	$0, 6(%rax)
	movq	%rax, %r14
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L517
.L266:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L269
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L270
	call	_ZdlPv@PLT
.L270:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L269:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$7, %edi
	call	_Znam@PLT
	movl	$12, %r8d
	movl	$16, %ecx
	leaq	.LC39(%rip), %rdx
	movb	$0, 6(%rax)
	movq	%rax, %r14
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC33(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L518
.L271:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L274
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L274:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$446, %edi
	call	_Znam@PLT
	movl	$445, %r8d
	movl	$596, %ecx
	leaq	.LC16(%rip), %rdx
	movb	$0, 445(%rax)
	movq	%rax, %r14
	movl	$445, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L519
.L276:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L279
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L279:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$446, %edi
	call	_Znam@PLT
	movl	$445, %r8d
	movl	$594, %ecx
	leaq	.LC40(%rip), %rdx
	movb	$0, 445(%rax)
	movq	%rax, %r14
	movl	$445, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L520
.L281:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L284
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L285
	call	_ZdlPv@PLT
.L285:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L284:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$446, %edi
	call	_Znam@PLT
	movl	$451, %r8d
	movl	$604, %ecx
	leaq	.LC41(%rip), %rdx
	movb	$0, 445(%rax)
	movq	%rax, %r14
	movl	$445, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L521
.L286:
	movq	-56(%rbp), %r13
	testq	%r13, %r13
	je	.L289
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L289:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movl	$446, %edi
	call	_Znam@PLT
	movl	$451, %r8d
	movl	$602, %ecx
	leaq	.LC42(%rip), %rdx
	movb	$0, 445(%rax)
	movq	%rax, %r14
	movl	$445, %esi
	movq	%rax, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%r14, %r8
	movq	%r12, %rdi
	leaq	.LC15(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN7testing8internal14CmpHelperSTREQEPKcS2_S2_S2_@PLT
	cmpb	$0, -64(%rbp)
	je	.L522
.L291:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L294
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L294:
	movq	%r14, %rdi
	call	_ZdaPv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L523
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L192
	movq	(%rax), %r8
.L192:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L501:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L187
	movq	(%rax), %r8
.L187:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L186
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L500:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L182
	movq	(%rax), %r8
.L182:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L499:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L177
	movq	(%rax), %r8
.L177:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L498:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L172
	movq	(%rax), %r8
.L172:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L506:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L212
	movq	(%rax), %r8
.L212:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L211
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L505:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L207
	movq	(%rax), %r8
.L207:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L206
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L202
	movq	(%rax), %r8
.L202:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L503:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L197
	movq	(%rax), %r8
.L197:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L510:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L232
	movq	(%rax), %r8
.L232:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L231
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L227
	movq	(%rax), %r8
.L227:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L226
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L508:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L222
	movq	(%rax), %r8
.L222:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L217
	movq	(%rax), %r8
.L217:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L216
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L514:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L252
	movq	(%rax), %r8
.L252:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L251
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L247
	movq	(%rax), %r8
.L247:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L246
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L242
	movq	(%rax), %r8
.L242:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L237
	movq	(%rax), %r8
.L237:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L272
	movq	(%rax), %r8
.L272:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L271
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L517:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L267
	movq	(%rax), %r8
.L267:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L516:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L262
	movq	(%rax), %r8
.L262:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L261
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L515:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L257
	movq	(%rax), %r8
.L257:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L256
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L522:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L292
	movq	(%rax), %r8
.L292:
	leaq	-72(%rbp), %r12
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L521:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L287
	movq	(%rax), %r8
.L287:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L286
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L520:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L282
	movq	(%rax), %r8
.L282:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L281
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L519:
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-56(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L277
	movq	(%rax), %r8
.L277:
	leaq	-72(%rbp), %r15
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L276
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L276
.L523:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5874:
	.size	_ZN22Base64Test_Decode_Test8TestBodyEv, .-_ZN22Base64Test_Decode_Test8TestBodyEv
	.section	.rodata._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB7016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L525
	testq	%rsi, %rsi
	je	.L541
.L525:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L542
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L528
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L529:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L543
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L529
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L527:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L529
.L541:
	leaq	.LC43(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L543:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7016:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.1
.LC44:
	.string	"Encode"
.LC45:
	.string	"Base64Test"
.LC46:
	.string	"Decode"
	.section	.text.startup
	.p2align 4
	.type	_GLOBAL__sub_I__ZN22Base64Test_Encode_Test10test_info_E, @function
_GLOBAL__sub_I__ZN22Base64Test_Encode_Test10test_info_E:
.LFB7453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L555
.L545:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L556
.L546:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	leaq	-128(%rbp), %r14
	leaq	-96(%rbp), %r12
	movq	%r14, %rdi
	movq	%rax, %r15
	leaq	-80(%rbp), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0.constprop.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	subq	$8, %rsp
	movq	%r15, %r9
	movq	%r12, %r8
	pushq	%r13
	leaq	.LC45(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	leaq	.LC44(%rip), %rsi
	pushq	$0
	movl	$11, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN22Base64Test_Encode_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L557
.L549:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L558
.L550:
	call	_ZN7testing8internal13GetTestTypeIdEv@PLT
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0.constprop.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	subq	$8, %rsp
	movq	%r12, %r8
	xorl	%ecx, %ecx
	pushq	%r13
	movq	-136(%rbp), %r9
	leaq	.LC45(%rip), %rdi
	xorl	%edx, %edx
	pushq	$0
	leaq	.LC46(%rip), %rsi
	pushq	$0
	movl	$47, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN22Base64Test_Decode_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L555:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L545
.L558:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L550
.L557:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L549
.L556:
	call	_ZN7testing8internal16SuiteApiResolverINS_4TestEE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L546
.L559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7453:
	.size	_GLOBAL__sub_I__ZN22Base64Test_Encode_Test10test_info_E, .-_GLOBAL__sub_I__ZN22Base64Test_Encode_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN22Base64Test_Encode_Test10test_info_E
	.weak	_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22Base64Test_Encode_TestE10CreateTestEv
	.weak	_ZTV22Base64Test_Encode_Test
	.section	.data.rel.ro._ZTV22Base64Test_Encode_Test,"awG",@progbits,_ZTV22Base64Test_Encode_Test,comdat
	.align 8
	.type	_ZTV22Base64Test_Encode_Test, @object
	.size	_ZTV22Base64Test_Encode_Test, 64
_ZTV22Base64Test_Encode_Test:
	.quad	0
	.quad	0
	.quad	_ZN22Base64Test_Encode_TestD1Ev
	.quad	_ZN22Base64Test_Encode_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN22Base64Test_Encode_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI22Base64Test_Decode_TestE10CreateTestEv
	.weak	_ZTV22Base64Test_Decode_Test
	.section	.data.rel.ro._ZTV22Base64Test_Decode_Test,"awG",@progbits,_ZTV22Base64Test_Decode_Test,comdat
	.align 8
	.type	_ZTV22Base64Test_Decode_Test, @object
	.size	_ZTV22Base64Test_Decode_Test, 64
_ZTV22Base64Test_Decode_Test:
	.quad	0
	.quad	0
	.quad	_ZN22Base64Test_Decode_TestD1Ev
	.quad	_ZN22Base64Test_Decode_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN22Base64Test_Decode_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.globl	_ZN22Base64Test_Decode_Test10test_info_E
	.bss
	.align 8
	.type	_ZN22Base64Test_Decode_Test10test_info_E, @object
	.size	_ZN22Base64Test_Decode_Test10test_info_E, 8
_ZN22Base64Test_Decode_Test10test_info_E:
	.zero	8
	.globl	_ZN22Base64Test_Encode_Test10test_info_E
	.align 8
	.type	_ZN22Base64Test_Encode_Test10test_info_E, @object
	.size	_ZN22Base64Test_Encode_Test10test_info_E, 8
_ZN22Base64Test_Encode_Test10test_info_E:
	.zero	8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata
	.align 32
	.type	_ZZN4nodeL13base64_encodeEPKcmPcmE5table, @object
	.size	_ZZN4nodeL13base64_encodeEPKcmPcmE5table, 65
_ZZN4nodeL13base64_encodeEPKcmPcmE5table:
	.string	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.quad	3419484896659189294
	.quad	8372038271277228899
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
