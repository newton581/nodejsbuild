	.file	"test_per_process.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB5808:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5808:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.text
	.align 2
	.p2align 4
	.type	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED2Ev, @function
_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED2Ev:
.LFB11526:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11526:
	.size	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED2Ev, .-_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED2Ev
	.set	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED1Ev,_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED2Ev
	.align 2
	.p2align 4
	.type	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED0Ev, @function
_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED0Ev:
.LFB11528:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11528:
	.size	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED0Ev, .-_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED0Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD2Ev, @function
_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD2Ev:
.LFB11522:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV14PerProcessTest(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11522:
	.size	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD2Ev, .-_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD2Ev
	.set	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD1Ev,_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD0Ev, @function
_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD0Ev:
.LFB11524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV14PerProcessTest(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11524:
	.size	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD0Ev, .-_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD0Ev
	.align 2
	.p2align 4
	.type	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEE10CreateTestEv:
.LFB11602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTVN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestE(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11602:
	.size	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEE10CreateTestEv
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E:
.LFB10048:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L25
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L14:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L12
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L10
.L13:
	movq	%rbx, %r12
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L13
.L10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE10048:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.section	.rodata._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_:
.LFB10910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -72(%rbp)
	movl	$88, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	32(%rbx), %r9
	movq	40(%rbx), %r8
	leaq	48(%rax), %rdi
	movq	%rax, %r12
	movq	%rdi, 32(%rax)
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L29
	testq	%r9, %r9
	je	.L36
.L29:
	movq	%r8, -64(%rbp)
	cmpq	$15, %r8
	ja	.L74
	cmpq	$1, %r8
	jne	.L32
	movzbl	(%r9), %eax
	movb	%al, 48(%r12)
.L33:
	movq	%r8, 40(%r12)
	movb	$0, (%rdi,%r8)
	movq	80(%rbx), %rax
	movdqu	64(%rbx), %xmm1
	movq	24(%rbx), %rsi
	movq	$0, 16(%r12)
	movq	%rax, 80(%r12)
	movl	(%rbx), %eax
	movq	$0, 24(%r12)
	movl	%eax, (%r12)
	movq	%r15, 8(%r12)
	movups	%xmm1, 64(%r12)
	testq	%rsi, %rsi
	je	.L34
	movq	-72(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r12, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r12)
.L34:
	movq	16(%rbx), %r15
	leaq	-64(%rbp), %rax
	movq	%r12, %rbx
	movq	%rax, -96(%rbp)
	testq	%r15, %r15
	je	.L28
.L35:
	movl	$88, %edi
	call	_Znwm@PLT
	movq	40(%r15), %r10
	leaq	48(%rax), %rdi
	movq	%rax, %r13
	movq	%rdi, 32(%rax)
	movq	32(%r15), %r11
	movq	%r11, %rax
	addq	%r10, %rax
	je	.L47
	testq	%r11, %r11
	je	.L36
.L47:
	movq	%r10, -64(%rbp)
	cmpq	$15, %r10
	ja	.L75
	cmpq	$1, %r10
	jne	.L40
	movzbl	(%r11), %eax
	movb	%al, 48(%r13)
.L41:
	movq	%r10, 40(%r13)
	movb	$0, (%rdi,%r10)
	movq	80(%r15), %rax
	movdqu	64(%r15), %xmm0
	movq	%rax, 80(%r13)
	movl	(%r15), %eax
	movups	%xmm0, 64(%r13)
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movl	%eax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	%rbx, 8(%r13)
	movq	24(%r15), %rsi
	testq	%rsi, %rsi
	je	.L42
	movq	-72(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L28
.L44:
	movq	%r13, %rbx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L32:
	testq	%r8, %r8
	je	.L33
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L42:
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L44
.L28:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L41
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-96(%rbp), %rsi
	leaq	32(%r13), %rdi
	xorl	%edx, %edx
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r11
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r13)
.L39:
	movq	%r10, %rdx
	movq	%r11, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r10
	movq	32(%r13), %rdi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	32(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L31:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	32(%r12), %rdi
	jmp	.L33
.L76:
	call	__stack_chk_fail@PLT
.L36:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE10910:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"NativeModuleLoader::source_ should have some 16bit items"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"true"
.LC3:
	.string	"false"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"std::any_of(sources.cbegin(), sources.cend(), [](auto p){ return !p.second.is_one_byte(); })"
	.align 8
.LC5:
	.string	"../test/cctest/test_per_process.cc"
	.align 8
.LC6:
	.string	"NativeModuleLoader::source_ should have some 8bit items"
	.align 8
.LC7:
	.string	"std::any_of(sources.cbegin(), sources.cend(), [](auto p){ return p.second.is_one_byte(); })"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_Test8TestBodyEv, @function
_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_Test8TestBodyEv:
.LFB8567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-240(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-216(%rbp), %rbx
	subq	$248, %rsp
	movq	120+_ZN4node13native_module18NativeModuleLoader9instance_E(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -200(%rbp)
	movl	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -184(%rbp)
	testq	%rsi, %rsi
	je	.L78
	leaq	-240(%rbp), %r13
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%r14, -240(%rbp)
	movq	%r13, %rcx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE7_M_copyINSG_11_Alloc_nodeEEEPSt13_Rb_tree_nodeISA_EPKSK_PSt18_Rb_tree_node_baseRT_
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rdx, %r15
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L79
	movq	%r15, -200(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L80
	movq	144+_ZN4node13native_module18NativeModuleLoader9instance_E(%rip), %rdx
	movq	%rcx, -192(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rdx, -184(%rbp)
	cmpq	%rbx, %r15
	je	.L78
	leaq	-176(%rbp), %rax
	leaq	-160(%rbp), %r12
	movq	%rax, -280(%rbp)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L178:
	movzbl	(%r10), %eax
	movb	%al, -160(%rbp)
	movq	%r12, %rax
.L85:
	movq	%r9, -168(%rbp)
	movb	$0, (%rax,%r9)
	movdqu	64(%r15), %xmm0
	movq	80(%r15), %rdx
	movq	-176(%rbp), %rdi
	movq	64(%r15), %rax
	movaps	%xmm0, -144(%rbp)
	movq	%rdx, -128(%rbp)
	cmpq	%r12, %rdi
	je	.L86
	movq	%rax, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %rax
.L86:
	testq	%rax, %rax
	jne	.L87
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rbx, %rax
	je	.L78
.L88:
	movq	32(%r15), %r10
	movq	40(%r15), %r9
	movq	%r12, -176(%rbp)
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L81
	testq	%r10, %r10
	je	.L93
.L81:
	movq	%r9, -240(%rbp)
	cmpq	$15, %r9
	ja	.L177
	cmpq	$1, %r9
	je	.L178
	testq	%r9, %r9
	jne	.L179
	movq	%r12, %rax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L177:
	movq	-280(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r9, -272(%rbp)
	movq	%r10, -264(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-264(%rbp), %r10
	movq	-272(%rbp), %r9
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-240(%rbp), %rax
	movq	%rax, -160(%rbp)
.L83:
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-240(%rbp), %r9
	movq	-176(%rbp), %rax
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	-248(%rbp), %r15
	movb	$0, -240(%rbp)
	leaq	-256(%rbp), %r12
	movq	$0, -232(%rbp)
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-248(%rbp), %rax
	movl	$55, %edx
	leaq	.LC6(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	leaq	.LC2(%rip), %r8
	leaq	.LC3(%rip), %rcx
	leaq	.LC7(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-112(%rbp), %r8
	movl	$23, %ecx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rdx
	movl	$2, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L114
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	movq	(%rdi), %rax
	call	*8(%rax)
.L103:
	movq	-232(%rbp), %r12
	testq	%r12, %r12
	je	.L92
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L92:
	movq	-208(%rbp), %r12
	testq	%r12, %r12
	je	.L77
.L112:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L110
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L77
.L111:
	movq	%rbx, %r12
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L111
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	-200(%rbp), %r15
	leaq	-112(%rbp), %rax
	leaq	-96(%rbp), %r12
	movq	%rax, -280(%rbp)
	cmpq	%rbx, %r15
	jne	.L101
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L95:
	cmpq	$1, %r9
	jne	.L97
	movzbl	(%r10), %eax
	movb	%al, -96(%rbp)
	movq	%r12, %rax
.L98:
	movq	%r9, -104(%rbp)
	movb	$0, (%rax,%r9)
	movdqu	64(%r15), %xmm1
	movq	-112(%rbp), %rdi
	movaps	%xmm1, -80(%rbp)
	movq	80(%r15), %rax
	movq	%rax, -64(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%r12, %rdi
	je	.L99
	movq	%rax, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %rax
.L99:
	testq	%rax, %rax
	je	.L100
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rbx, %rax
	je	.L102
.L101:
	movq	%r12, -112(%rbp)
	movq	32(%r15), %r10
	movq	40(%r15), %r9
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L120
	testq	%r10, %r10
	je	.L93
.L120:
	movq	%r9, -240(%rbp)
	cmpq	$15, %r9
	jbe	.L95
	movq	-280(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r9, -272(%rbp)
	movq	%r10, -264(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-264(%rbp), %r10
	movq	-272(%rbp), %r9
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-240(%rbp), %rax
	movq	%rax, -96(%rbp)
.L96:
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-240(%rbp), %r9
	movq	-112(%rbp), %rax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L97:
	testq	%r9, %r9
	jne	.L181
	movq	%r12, %rax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-208(%rbp), %r12
	testq	%r12, %r12
	je	.L77
.L109:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L106
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L77
.L108:
	movq	%rbx, %r12
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L108
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	-248(%rbp), %r15
	movb	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	%r15, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-248(%rbp), %rax
	movl	$56, %edx
	leaq	.LC1(%rip), %rsi
	leaq	16(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	leaq	-112(%rbp), %rdi
	leaq	.LC2(%rip), %r8
	leaq	.LC3(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	leaq	-256(%rbp), %r13
	movq	-112(%rbp), %r8
	movl	$28, %ecx
	leaq	.LC5(%rip), %rdx
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r13, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L114
	call	_ZdlPv@PLT
	jmp	.L114
.L93:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L180:
	call	__stack_chk_fail@PLT
.L179:
	movq	%r12, %rdi
	jmp	.L83
.L181:
	movq	%r12, %rdi
	jmp	.L96
	.cfi_endproc
.LFE8567:
	.size	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_Test8TestBodyEv, .-_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_Test8TestBodyEv
	.section	.rodata.str1.1
.LC8:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC10:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.align 8
.LC11:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.rodata.str1.1
.LC12:
	.string	"EmbeddedSources"
.LC13:
	.string	"PerProcessTest"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_test_per_process.cc, @function
_GLOBAL__sub_I_test_per_process.cc:
.LFB11681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-136(%rbp), %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L204
.L183:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L205
.L184:
	xorl	%edx, %edx
	leaq	-128(%rbp), %rdi
	leaq	-112(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, -128(%rbp)
	leaq	-80(%rbp), %r15
	movq	$34, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %rdx
	movdqa	.LC14(%rip), %xmm0
	movq	%rax, -128(%rbp)
	movq	%rdx, -112(%rbp)
	movl	$25443, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC15(%rip), %xmm0
	movw	%dx, 32(%rax)
	movq	-128(%rbp), %rdx
	movups	%xmm0, 16(%rax)
	movq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r14
	movq	%r15, -96(%rbp)
	movq	%r9, %rax
	addq	%r14, %rax
	je	.L185
	testq	%r9, %r9
	je	.L206
.L185:
	movq	%r14, -136(%rbp)
	cmpq	$15, %r14
	ja	.L207
	cmpq	$1, %r14
	jne	.L188
	movzbl	(%r9), %eax
	leaq	-96(%rbp), %r8
	movb	%al, -80(%rbp)
.L189:
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	xorl	%ecx, %ecx
	subq	$8, %rsp
	leaq	.LC13(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI14PerProcessTestE6dummy_E(%rip), %r9
	movq	%rax, -88(%rbp)
	leaq	.LC12(%rip), %rsi
	movb	$0, (%rdx,%rax)
	xorl	%edx, %edx
	pushq	%rbx
	pushq	$0
	pushq	$0
	movl	$21, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	cmpq	%r15, %rdi
	je	.L190
	call	_ZdlPv@PLT
.L190:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L188:
	.cfi_restore_state
	leaq	-96(%rbp), %r8
	testq	%r14, %r14
	je	.L189
	movq	%r15, %rdi
	jmp	.L187
.L205:
	movl	$6946, %ecx
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	.LC8(%rip), %rdx
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC9(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC11(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	jmp	.L184
.L207:
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -160(%rbp)
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L187:
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%r8, -152(%rbp)
	call	memcpy@PLT
	movq	-152(%rbp), %r8
	jmp	.L189
.L204:
	movl	$6959, %ecx
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	.LC8(%rip), %rdx
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC9(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC10(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	jmp	.L183
.L206:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11681:
	.size	_GLOBAL__sub_I_test_per_process.cc, .-_GLOBAL__sub_I_test_per_process.cc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_test_per_process.cc
	.weak	_ZN7testing8internal12TypeIdHelperI14PerProcessTestE6dummy_E
	.section	.bss._ZN7testing8internal12TypeIdHelperI14PerProcessTestE6dummy_E,"awG",@nobits,_ZN7testing8internal12TypeIdHelperI14PerProcessTestE6dummy_E,comdat
	.type	_ZN7testing8internal12TypeIdHelperI14PerProcessTestE6dummy_E, @gnu_unique_object
	.size	_ZN7testing8internal12TypeIdHelperI14PerProcessTestE6dummy_E, 1
_ZN7testing8internal12TypeIdHelperI14PerProcessTestE6dummy_E:
	.zero	1
	.weak	_ZTV14PerProcessTest
	.section	.data.rel.ro._ZTV14PerProcessTest,"awG",@progbits,_ZTV14PerProcessTest,comdat
	.align 8
	.type	_ZTV14PerProcessTest, @object
	.size	_ZTV14PerProcessTest, 64
_ZTV14PerProcessTest:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEEE, 40
_ZTVN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplIN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestEE10CreateTestEv
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestE, @object
	.size	_ZTVN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestE, 64
_ZTVN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestE:
	.quad	0
	.quad	0
	.quad	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD1Ev
	.quad	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_TestD0Ev
	.quad	_ZN7testing4Test5SetUpEv
	.quad	_ZN7testing4Test8TearDownEv
	.quad	_ZN12_GLOBAL__N_135PerProcessTest_EmbeddedSources_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC14:
	.quad	3419484896659189294
	.quad	8372038271277228899
	.align 16
.LC15:
	.quad	6877671114260378469
	.quad	3347145827363549808
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
