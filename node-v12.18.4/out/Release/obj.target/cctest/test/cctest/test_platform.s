	.file	"test_platform.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB7441:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7441:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN13RepostingTaskD2Ev,"axG",@progbits,_ZN13RepostingTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN13RepostingTaskD2Ev
	.type	_ZN13RepostingTaskD2Ev, @function
_ZN13RepostingTaskD2Ev:
.LFB10034:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10034:
	.size	_ZN13RepostingTaskD2Ev, .-_ZN13RepostingTaskD2Ev
	.weak	_ZN13RepostingTaskD1Ev
	.set	_ZN13RepostingTaskD1Ev,_ZN13RepostingTaskD2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED2Ev:
.LFB11507:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11507:
	.size	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED1Ev,_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED0Ev:
.LFB11509:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11509:
	.size	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED0Ev
	.section	.text._ZN13RepostingTaskD0Ev,"axG",@progbits,_ZN13RepostingTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN13RepostingTaskD0Ev
	.type	_ZN13RepostingTaskD0Ev, @function
_ZN13RepostingTaskD0Ev:
.LFB10036:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10036:
	.size	_ZN13RepostingTaskD0Ev, .-_ZN13RepostingTaskD0Ev
	.section	.text._ZN15NodeTestFixture8TearDownEv,"axG",@progbits,_ZN15NodeTestFixture8TearDownEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture8TearDownEv
	.type	_ZN15NodeTestFixture8TearDownEv, @function
_ZN15NodeTestFixture8TearDownEv:
.LFB8491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8491:
	.size	_ZN15NodeTestFixture8TearDownEv, .-_ZN15NodeTestFixture8TearDownEv
	.section	.text._ZN15NodeTestFixture5SetUpEv,"axG",@progbits,_ZN15NodeTestFixture5SetUpEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture5SetUpEv
	.type	_ZN15NodeTestFixture5SetUpEv, @function
_ZN15NodeTestFixture5SetUpEv:
.LFB8488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node26CreateArrayBufferAllocatorEv@PLT
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %r8
	movq	%rax, %rdi
	movq	%rax, 8+_ZN15NodeTestFixture9allocatorE(%rip)
	testq	%r8, %r8
	je	.L10
	movq	%r8, %rdi
	call	*_ZN15NodeTestFixture9allocatorE(%rip)
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %rdi
.L10:
	movq	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE@GOTPCREL(%rip), %rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	movq	%rax, _ZN15NodeTestFixture9allocatorE(%rip)
	call	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L17
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate5EnterEv@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture5SetUpEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8488:
	.size	_ZN15NodeTestFixture5SetUpEv, .-_ZN15NodeTestFixture5SetUpEv
	.section	.text._ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD2Ev,"axG",@progbits,_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD2Ev
	.type	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD2Ev, @function
_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD2Ev:
.LFB11503:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11503:
	.size	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD2Ev, .-_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD2Ev
	.weak	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD1Ev
	.set	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD1Ev,_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD2Ev
	.section	.text._ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD0Ev,"axG",@progbits,_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD0Ev
	.type	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD0Ev, @function
_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD0Ev:
.LFB11505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11505:
	.size	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD0Ev, .-_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv:
.LFB11583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11583:
	.size	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv
	.section	.text._ZN15NodeTestFixture16TearDownTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture16TearDownTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture16TearDownTestCaseEv
	.type	_ZN15NodeTestFixture16TearDownTestCaseEv, @function
_ZN15NodeTestFixture16TearDownTestCaseEv:
.LFB8487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L25:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L28
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_close@PLT
	testl	%eax, %eax
	jne	.L29
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8487:
	.size	_ZN15NodeTestFixture16TearDownTestCaseEv, .-_ZN15NodeTestFixture16TearDownTestCaseEv
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB10003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L32
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L33
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L59
	.p2align 4,,10
	.p2align 3
.L32:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L38
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L44
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L38
.L39:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L46
.L60:
	lock subl	$1, 8(%r13)
	jne	.L46
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L46
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L42
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L38
.L44:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L42
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L42
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L33:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L32
.L59:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L36
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L37:
	cmpl	$1, %eax
	jne	.L32
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L36:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L37
	.cfi_endproc
.LFE10003:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata._ZN15NodeTestFixture13SetUpTestCaseEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NODE_OPTIONS"
.LC1:
	.string	"cctest"
	.section	.text._ZN15NodeTestFixture13SetUpTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture13SetUpTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture13SetUpTestCaseEv
	.type	_ZN15NodeTestFixture13SetUpTestCaseEv, @function
_ZN15NodeTestFixture13SetUpTestCaseEv:
.LFB8482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN15NodeTestFixture16node_initializedE(%rip)
	je	.L103
.L62:
	movl	$1312, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing5AgentC1Ev@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r13
	movq	%r12, _ZN15NodeTestFixture13tracing_agentE(%rip)
	testq	%r13, %r13
	je	.L63
	movq	%r13, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r12
.L63:
	movq	%r12, %rdi
	call	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %rax
	movq	976(%rax), %r12
	testq	%r12, %r12
	je	.L104
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L105
	movl	$128, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r12
	movq	%r13, _ZN15NodeTestFixture8platformE(%rip)
	testq	%r12, %r12
	je	.L66
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L67
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L69
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L70
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L71:
	cmpl	$1, %eax
	jne	.L69
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L73
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L74:
	cmpl	$1, %eax
	jne	.L69
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L69:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L88
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L78
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L88
.L78:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L86
	lock subl	$1, 8(%r13)
	jne	.L86
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L86
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L88:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
.L66:
	movq	%r13, %rdi
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L81
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L88
.L84:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L81
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L81
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	.LC0(%rip), %rdi
	call	uv_os_unsetenv@PLT
	leaq	.LC1(%rip), %rax
	leaq	-48(%rbp), %rcx
	movl	$1, -64(%rbp)
	leaq	-60(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movb	$1, _ZN15NodeTestFixture16node_initializedE(%rip)
	call	_ZN4node4InitEPiPPKcS0_PS3_@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r12, %rdi
	call	*%rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L70:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L74
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8482:
	.size	_ZN15NodeTestFixture13SetUpTestCaseEv, .-_ZN15NodeTestFixture13SetUpTestCaseEv
	.section	.text._ZN13RepostingTask3RunEv,"axG",@progbits,_ZN13RepostingTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN13RepostingTask3RunEv
	.type	_ZN13RepostingTask3RunEv, @function
_ZN13RepostingTask3RunEv:
.LFB8510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	addl	$1, (%rax)
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L107
	movq	32(%rdi), %rsi
	subl	$1, %eax
	movq	%rdi, %rbx
	movl	%eax, 8(%rdi)
	movq	24(%rbx), %rdx
	leaq	-80(%rbp), %rdi
	movq	(%rsi), %rax
	call	*48(%rax)
	movq	-80(%rbp), %r14
	movq	16(%rbx), %r12
	movl	$40, %edi
	movl	8(%rbx), %r13d
	movq	(%r14), %rax
	movq	(%rax), %r15
	movq	24(%rbx), %rax
	movq	32(%rbx), %rbx
	movq	%rax, -104(%rbp)
	call	_Znwm@PLT
	movq	%r12, %xmm0
	movq	%r14, %rdi
	leaq	-88(%rbp), %rsi
	leaq	16+_ZTV13RepostingTask(%rip), %rcx
	movhps	-104(%rbp), %xmm0
	movl	%r13d, 8(%rax)
	movq	%rcx, (%rax)
	movq	%rbx, 32(%rax)
	movq	%rax, -88(%rbp)
	movups	%xmm0, 16(%rax)
	call	*%r15
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	movq	(%rdi), %rax
	call	*8(%rax)
.L109:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L107
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L112
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L113:
	cmpl	$1, %eax
	jne	.L107
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L115
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L116:
	cmpl	$1, %eax
	je	.L122
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L115:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L116
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8510:
	.size	_ZN13RepostingTask3RunEv, .-_ZN13RepostingTask3RunEv
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB10001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L126
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L127
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L153
	.p2align 4,,10
	.p2align 3
.L126:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L132
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L138
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L154
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L132
.L133:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L140
.L154:
	lock subl	$1, 8(%r13)
	jne	.L140
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L140
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L133
	.p2align 4,,10
	.p2align 3
.L132:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L136
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L132
.L138:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L136
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L136
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L127:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L126
.L153:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L130
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L131:
	cmpl	$1, %eax
	jne	.L126
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L131
	.cfi_endproc
.LFE10001:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text._ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.type	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_, @function
_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_:
.LFB10069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -520(%rbp)
	movq	%rsi, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	cmpl	%eax, (%rcx)
	je	.L170
	movq	.LC2(%rip), %xmm1
	leaq	-320(%rbp), %r12
	leaq	-448(%rbp), %r15
	movq	%r8, -544(%rbp)
	movq	%r12, %rdi
	movq	%r15, -616(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC3(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %r8
	movq	%r14, %rdi
	movl	(%r8), %esi
	call	_ZNSolsEi@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -544(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L158
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L171
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L160:
	movq	.LC2(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC4(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -608(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rsi, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movdqa	-560(%rbp), %xmm3
	movq	-528(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-536(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movl	(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L162
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L163
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L164:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-608(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-536(%rbp), %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movq	-528(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-544(%rbp), %r8
	movq	-576(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	-480(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	movq	-520(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L170:
	call	_ZN7testing16AssertionSuccessEv@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L163:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L160
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10069:
	.size	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_, .-_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	""
.LC6:
	.string	"node"
.LC7:
	.string	"-p"
.LC8:
	.string	"process.version"
.LC9:
	.string	"%s"
.LC10:
	.string	"true"
.LC11:
	.string	"false"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"platform->FlushForegroundTasks(isolate_)"
	.align 8
.LC13:
	.string	"../test/cctest/test_platform.cc"
	.section	.rodata.str1.1
.LC14:
	.string	"run_count"
.LC15:
	.string	"1"
.LC16:
	.string	"2"
.LC17:
	.string	"3"
	.text
	.align 2
	.p2align 4
	.globl	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test8TestBodyEv
	.type	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test8TestBodyEv, @function
_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test8TestBodyEv:
.LFB8549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC9(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	16(%rbx), %rsi
	leaq	-160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC8(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-88(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rbx, -240(%rbp)
	movl	%r13d, %ebx
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	leaq	-96(%rbp), %rax
	movq	%rax, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-232(%rbp), %rax
	movq	(%rax,%r15), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -256(%rbp)
	call	strlen@PLT
	movslq	%ebx, %r8
	movq	-256(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r13d
	movq	%r8, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r13d, %rsi
	movq	%r8, -248(%rbp)
	addl	%r13d, %ebx
	call	snprintf@PLT
	movq	-248(%rbp), %r8
	addq	(%r12), %r8
	movq	%r8, (%r12,%r15)
	addq	$8, %r15
	cmpq	$24, %r15
	jne	.L174
	movq	-160(%rbp), %r13
	xorl	%esi, %esi
	movq	-240(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -240(%rbp)
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L279
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, -248(%rbp)
	testq	%rax, %rax
	je	.L280
	movq	-128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L281
	movq	_ZN15NodeTestFixture8platformE(%rip), %rsi
	movq	16(%rbx), %rdx
	leaq	-192(%rbp), %rdi
	movl	$0, -212(%rbp)
	leaq	-212(%rbp), %r15
	movq	(%rsi), %rax
	call	*48(%rax)
	movq	-192(%rbp), %r13
	movq	16(%rbx), %rcx
	movl	$40, %edi
	movq	_ZN15NodeTestFixture8platformE(%rip), %r14
	movq	0(%r13), %rax
	movq	%rcx, -280(%rbp)
	movq	(%rax), %rdx
	movq	%rdx, -288(%rbp)
	call	_Znwm@PLT
	movq	-280(%rbp), %rcx
	leaq	16+_ZTV13RepostingTask(%rip), %rdx
	movq	%r13, %rdi
	movq	%r14, 32(%rax)
	leaq	-176(%rbp), %r14
	movq	%rdx, (%rax)
	movq	%r14, %rsi
	movq	-288(%rbp), %rdx
	movl	$2, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%rcx, 24(%rax)
	movq	%rax, -176(%rbp)
	call	*%rdx
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L178
	movq	(%rdi), %rax
	call	*8(%rax)
.L178:
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	leaq	-200(%rbp), %r13
	movq	(%rdi), %rax
	call	*168(%rax)
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L179
.L180:
	movq	%r15, %r8
	movq	%r13, %rcx
	leaq	.LC14(%rip), %rdx
	movq	%r14, %rdi
	leaq	.LC15(%rip), %rsi
	movl	$1, -200(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -176(%rbp)
	je	.L282
.L185:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L188
.L293:
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L189
	movq	%r8, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r8
.L189:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L188:
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*168(%rax)
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L283
.L191:
	movq	%r15, %r8
	movq	%r13, %rcx
	leaq	.LC14(%rip), %rdx
	movq	%r14, %rdi
	leaq	.LC16(%rip), %rsi
	movl	$2, -200(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -176(%rbp)
	je	.L284
.L196:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L199
.L292:
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L200
	movq	%r8, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r8
.L200:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L199:
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*168(%rax)
	movq	$0, -168(%rbp)
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L285
.L202:
	movq	%r15, %r8
	movq	%r13, %rcx
	leaq	.LC14(%rip), %rdx
	movq	%r14, %rdi
	leaq	.LC17(%rip), %rsi
	movl	$3, -200(%rbp)
	call	_ZN7testing8internal11CmpHelperEQIiiEENS_15AssertionResultEPKcS4_RKT_RKT0_
	cmpb	$0, -176(%rbp)
	je	.L286
.L207:
	movq	-168(%rbp), %r15
	testq	%r15, %r15
	je	.L210
.L291:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L210:
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*168(%rax)
	movq	$0, -168(%rbp)
	xorl	$1, %eax
	movb	%al, -176(%rbp)
	testb	%al, %al
	je	.L287
.L213:
	movq	-184(%rbp), %r13
	testq	%r13, %r13
	je	.L219
.L290:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L220
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L221:
	cmpl	$1, %eax
	je	.L288
.L219:
	movq	-256(%rbp), %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-248(%rbp), %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	-240(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-272(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-264(%rbp), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC11(%rip), %r8
	leaq	.LC10(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	leaq	-208(%rbp), %r14
	movq	-96(%rbp), %r8
	movl	$58, %ecx
	leaq	.LC13(%rip), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r14, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L215
	movq	(%rdi), %rax
	call	*8(%rax)
.L215:
	movq	-168(%rbp), %r13
	testq	%r13, %r13
	je	.L213
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	%r13, %rdi
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	-184(%rbp), %r13
	testq	%r13, %r13
	jne	.L290
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-168(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L208
	movq	(%rax), %r8
.L208:
	leaq	-208(%rbp), %r15
	movl	$57, %ecx
	movl	$1, %esi
	leaq	.LC13(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r15, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L207
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-168(%rbp), %r15
	testq	%r15, %r15
	jne	.L291
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-168(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L197
	movq	(%rax), %r8
.L197:
	leaq	-208(%rbp), %rdi
	movl	$55, %ecx
	movl	$1, %esi
	leaq	.LC13(%rip), %rdx
	movq	%rdi, -280(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-280(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-280(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	jne	.L292
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC11(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-208(%rbp), %rdi
	movl	$56, %ecx
	leaq	.LC13(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -280(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-280(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-280(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L204
	movq	(%rdi), %rax
	call	*8(%rax)
.L204:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L202
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L206
	movq	%r8, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r8
.L206:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-168(%rbp), %rax
	leaq	.LC5(%rip), %r8
	testq	%rax, %rax
	je	.L186
	movq	(%rax), %r8
.L186:
	leaq	-208(%rbp), %rdi
	movl	$53, %ecx
	movl	$1, %esi
	leaq	.LC13(%rip), %rdx
	movq	%rdi, -280(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-280(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-280(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	jne	.L293
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC11(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-208(%rbp), %rdi
	movl	$54, %ecx
	leaq	.LC13(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -280(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-280(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-280(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L193
	movq	(%rdi), %rax
	call	*8(%rax)
.L193:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L191
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L195
	movq	%r8, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r8
.L195:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-232(%rbp), %rdi
	movq	%r14, %rsi
	leaq	.LC10(%rip), %r8
	leaq	.LC11(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-208(%rbp), %rdi
	movl	$52, %ecx
	leaq	.LC13(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -280(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-280(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-280(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L182
	movq	(%rdi), %rax
	call	*8(%rax)
.L182:
	movq	-168(%rbp), %r8
	testq	%r8, %r8
	je	.L180
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L184
	movq	%r8, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r8
.L184:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L288:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L223
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L224:
	cmpl	$1, %eax
	jne	.L219
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L220:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L223:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L224
.L289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8549:
	.size	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test8TestBodyEv, .-_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test8TestBodyEv
	.section	.rodata.str1.1
.LC18:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC20:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.align 8
.LC21:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.align 8
.LC22:
	.string	"basic_string::_M_construct null not valid"
	.align 8
.LC23:
	.string	"SkipNewTasksInFlushForegroundTasks"
	.section	.rodata.str1.1
.LC24:
	.string	"PlatformTest"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E, @function
_GLOBAL__sub_I__ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E:
.LFB11662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-136(%rbp), %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestEE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L316
.L295:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L317
.L296:
	xorl	%edx, %edx
	leaq	-128(%rbp), %rdi
	leaq	-112(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, -128(%rbp)
	leaq	-80(%rbp), %r15
	movq	$31, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %rdx
	movdqa	.LC25(%rip), %xmm0
	movabsq	$8386103211034178405, %rcx
	movq	%rax, -128(%rbp)
	movq	%rdx, -112(%rbp)
	movl	$25390, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 28(%rax)
	movq	-128(%rbp), %rdx
	movq	%rcx, 16(%rax)
	movl	$1836216166, 24(%rax)
	movb	$99, 30(%rax)
	movq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r14
	movq	%r15, -96(%rbp)
	movq	%r9, %rax
	addq	%r14, %rax
	je	.L297
	testq	%r9, %r9
	je	.L318
.L297:
	movq	%r14, -136(%rbp)
	cmpq	$15, %r14
	ja	.L319
	cmpq	$1, %r14
	jne	.L300
	movzbl	(%r9), %eax
	leaq	-96(%rbp), %r8
	movb	%al, -80(%rbp)
.L301:
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	xorl	%ecx, %ecx
	subq	$8, %rsp
	leaq	.LC24(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI12PlatformTestE6dummy_E(%rip), %r9
	movq	%rax, -88(%rbp)
	leaq	.LC23(%rip), %rsi
	movb	$0, (%rdx,%rax)
	leaq	_ZN15NodeTestFixture16TearDownTestCaseEv(%rip), %rax
	xorl	%edx, %edx
	pushq	%rbx
	pushq	%rax
	leaq	_ZN15NodeTestFixture13SetUpTestCaseEv(%rip), %rax
	pushq	%rax
	movl	$42, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E(%rip)
	cmpq	%r15, %rdi
	je	.L302
	call	_ZdlPv@PLT
.L302:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L300:
	.cfi_restore_state
	leaq	-96(%rbp), %r8
	testq	%r14, %r14
	je	.L301
	movq	%r15, %rdi
	jmp	.L299
.L317:
	movl	$6946, %ecx
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	.LC18(%rip), %rdx
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC19(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC21(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	jmp	.L296
.L319:
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -160(%rbp)
	movq	%r8, %rdi
	movq	%r8, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r9
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L299:
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%r8, -152(%rbp)
	call	memcpy@PLT
	movq	-152(%rbp), %r8
	jmp	.L301
.L316:
	movl	$6959, %ecx
	movq	%r12, %rdi
	movl	$3, %esi
	leaq	.LC18(%rip), %rdx
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC19(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC20(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	jmp	.L295
.L318:
	leaq	.LC22(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11662:
	.size	_GLOBAL__sub_I__ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E, .-_GLOBAL__sub_I__ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E
	.weak	_ZN7testing8internal12TypeIdHelperI12PlatformTestE6dummy_E
	.section	.bss._ZN7testing8internal12TypeIdHelperI12PlatformTestE6dummy_E,"awG",@nobits,_ZN7testing8internal12TypeIdHelperI12PlatformTestE6dummy_E,comdat
	.type	_ZN7testing8internal12TypeIdHelperI12PlatformTestE6dummy_E, @gnu_unique_object
	.size	_ZN7testing8internal12TypeIdHelperI12PlatformTestE6dummy_E, 1
_ZN7testing8internal12TypeIdHelperI12PlatformTestE6dummy_E:
	.zero	1
	.weak	_ZTV15NodeTestFixture
	.section	.data.rel.ro._ZTV15NodeTestFixture,"awG",@progbits,_ZTV15NodeTestFixture,comdat
	.align 8
	.type	_ZTV15NodeTestFixture, @object
	.size	_ZTV15NodeTestFixture, 64
_ZTV15NodeTestFixture:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTV13RepostingTask
	.section	.data.rel.ro.local._ZTV13RepostingTask,"awG",@progbits,_ZTV13RepostingTask,comdat
	.align 8
	.type	_ZTV13RepostingTask, @object
	.size	_ZTV13RepostingTask, 40
_ZTV13RepostingTask:
	.quad	0
	.quad	0
	.quad	_ZN13RepostingTaskD1Ev
	.quad	_ZN13RepostingTaskD0Ev
	.quad	_ZN13RepostingTask3RunEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestE10CreateTestEv
	.weak	_ZTV52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test
	.section	.data.rel.ro.local._ZTV52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test,"awG",@progbits,_ZTV52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test,comdat
	.align 8
	.type	_ZTV52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test, @object
	.size	_ZTV52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test, 64
_ZTV52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test:
	.quad	0
	.quad	0
	.quad	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD1Ev
	.quad	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.globl	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E
	.bss
	.align 8
	.type	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E, @object
	.size	_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E, 8
_ZN52PlatformTest_SkipNewTasksInFlushForegroundTasks_Test10test_info_E:
	.zero	8
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"../test/cctest/node_test_fixture.h:140"
	.section	.rodata.str1.1
.LC27:
	.string	"(nullptr) != (environment_)"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"EnvironmentTestFixture::Env::Env(const v8::HandleScope&, const Argv&)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"../test/cctest/node_test_fixture.h:135"
	.section	.rodata.str1.1
.LC30:
	.string	"(nullptr) != (isolate_data_)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC28
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"../test/cctest/node_test_fixture.h:129"
	.section	.rodata.str1.1
.LC32:
	.string	"!context_.IsEmpty()"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC28
	.weak	_ZZN15NodeTestFixture5SetUpEvE4args
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"../test/cctest/node_test_fixture.h:108"
	.section	.rodata.str1.1
.LC34:
	.string	"(isolate_) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"virtual void NodeTestFixture::SetUp()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture5SetUpEvE4args,"awG",@progbits,_ZZN15NodeTestFixture5SetUpEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture5SetUpEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture5SetUpEvE4args, 24
_ZZN15NodeTestFixture5SetUpEvE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.weak	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"../test/cctest/node_test_fixture.h:101"
	.align 8
.LC37:
	.string	"(0) == (uv_loop_close(&current_loop))"
	.align 8
.LC38:
	.string	"static void NodeTestFixture::TearDownTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture16TearDownTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture16TearDownTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, 24
_ZZN15NodeTestFixture16TearDownTestCaseEvE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.weak	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"../test/cctest/node_test_fixture.h:87"
	.align 8
.LC40:
	.string	"(0) == (uv_loop_init(&current_loop))"
	.align 8
.LC41:
	.string	"static void NodeTestFixture::SetUpTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture13SetUpTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture13SetUpTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, 24
_ZZN15NodeTestFixture13SetUpTestCaseEvE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC42:
	.string	"../src/tracing/agent.h:91"
.LC43:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC3:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC4:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC25:
	.quad	3419484896659189294
	.quad	8372038271277228899
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
