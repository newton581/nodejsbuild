	.file	"test_aliased_buffer.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB6036:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6036:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED2Ev:
.LFB12360:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12360:
	.size	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED1Ev,_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED2Ev:
.LFB12368:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12368:
	.size	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED1Ev,_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED2Ev:
.LFB12376:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12376:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED1Ev,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED2Ev:
.LFB12384:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12384:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED1Ev,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED2Ev:
.LFB12392:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12392:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED1Ev,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED2Ev:
.LFB12400:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12400:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED1Ev,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED2Ev:
.LFB12408:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12408:
	.size	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED2Ev:
.LFB12416:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12416:
	.size	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED2Ev:
.LFB12424:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12424:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED2Ev:
.LFB12432:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12432:
	.size	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED2Ev:
.LFB12440:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12440:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED2Ev:
.LFB12448:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12448:
	.size	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED2Ev:
.LFB12456:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12456:
	.size	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED2Ev:
.LFB12464:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12464:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED1Ev,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED0Ev:
.LFB12466:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12466:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED0Ev:
.LFB12458:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12458:
	.size	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED0Ev:
.LFB12450:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12450:
	.size	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED0Ev:
.LFB12442:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12442:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED0Ev:
.LFB12434:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12434:
	.size	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED0Ev:
.LFB12426:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12426:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED0Ev:
.LFB12418:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12418:
	.size	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED0Ev:
.LFB12410:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12410:
	.size	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED0Ev:
.LFB12402:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12402:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED0Ev:
.LFB12394:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12394:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED0Ev:
.LFB12386:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12386:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED0Ev:
.LFB12378:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12378:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED0Ev:
.LFB12370:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12370:
	.size	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED0Ev:
.LFB12362:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12362:
	.size	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED0Ev
	.section	.text._ZN15NodeTestFixture8TearDownEv,"axG",@progbits,_ZN15NodeTestFixture8TearDownEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture8TearDownEv
	.type	_ZN15NodeTestFixture8TearDownEv, @function
_ZN15NodeTestFixture8TearDownEv:
.LFB8491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8491:
	.size	_ZN15NodeTestFixture8TearDownEv, .-_ZN15NodeTestFixture8TearDownEv
	.section	.text._ZN15NodeTestFixture5SetUpEv,"axG",@progbits,_ZN15NodeTestFixture5SetUpEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture5SetUpEv
	.type	_ZN15NodeTestFixture5SetUpEv, @function
_ZN15NodeTestFixture5SetUpEv:
.LFB8488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node26CreateArrayBufferAllocatorEv@PLT
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %r8
	movq	%rax, %rdi
	movq	%rax, 8+_ZN15NodeTestFixture9allocatorE(%rip)
	testq	%r8, %r8
	je	.L34
	movq	%r8, %rdi
	call	*_ZN15NodeTestFixture9allocatorE(%rip)
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %rdi
.L34:
	movq	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE@GOTPCREL(%rip), %rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	movq	%rax, _ZN15NodeTestFixture9allocatorE(%rip)
	call	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L41
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate5EnterEv@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture5SetUpEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8488:
	.size	_ZN15NodeTestFixture5SetUpEv, .-_ZN15NodeTestFixture5SetUpEv
	.section	.text._ZN42AliasBufferTest_OperatorOverloadsRefs_TestD2Ev,"axG",@progbits,_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD2Ev
	.type	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD2Ev, @function
_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD2Ev:
.LFB12356:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12356:
	.size	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD2Ev, .-_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD2Ev
	.weak	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD1Ev
	.set	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD1Ev,_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD2Ev
	.section	.text._ZN42AliasBufferTest_OperatorOverloadsRefs_TestD0Ev,"axG",@progbits,_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD0Ev
	.type	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD0Ev, @function
_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD0Ev:
.LFB12358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12358:
	.size	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD0Ev, .-_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD0Ev
	.section	.text._ZN38AliasBufferTest_OperatorOverloads_TestD2Ev,"axG",@progbits,_ZN38AliasBufferTest_OperatorOverloads_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN38AliasBufferTest_OperatorOverloads_TestD2Ev
	.type	_ZN38AliasBufferTest_OperatorOverloads_TestD2Ev, @function
_ZN38AliasBufferTest_OperatorOverloads_TestD2Ev:
.LFB12364:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12364:
	.size	_ZN38AliasBufferTest_OperatorOverloads_TestD2Ev, .-_ZN38AliasBufferTest_OperatorOverloads_TestD2Ev
	.weak	_ZN38AliasBufferTest_OperatorOverloads_TestD1Ev
	.set	_ZN38AliasBufferTest_OperatorOverloads_TestD1Ev,_ZN38AliasBufferTest_OperatorOverloads_TestD2Ev
	.section	.text._ZN38AliasBufferTest_OperatorOverloads_TestD0Ev,"axG",@progbits,_ZN38AliasBufferTest_OperatorOverloads_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN38AliasBufferTest_OperatorOverloads_TestD0Ev
	.type	_ZN38AliasBufferTest_OperatorOverloads_TestD0Ev, @function
_ZN38AliasBufferTest_OperatorOverloads_TestD0Ev:
.LFB12366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12366:
	.size	_ZN38AliasBufferTest_OperatorOverloads_TestD0Ev, .-_ZN38AliasBufferTest_OperatorOverloads_TestD0Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer4_TestD2Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer4_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD2Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD2Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer4_TestD2Ev:
.LFB12372:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12372:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD2Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer4_TestD2Ev
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD1Ev
	.set	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD1Ev,_ZN39AliasBufferTest_SharedArrayBuffer4_TestD2Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer4_TestD0Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer4_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD0Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD0Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer4_TestD0Ev:
.LFB12374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12374:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD0Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer4_TestD0Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer3_TestD2Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer3_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD2Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD2Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer3_TestD2Ev:
.LFB12380:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12380:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD2Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer3_TestD2Ev
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD1Ev
	.set	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD1Ev,_ZN39AliasBufferTest_SharedArrayBuffer3_TestD2Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer3_TestD0Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer3_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD0Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD0Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer3_TestD0Ev:
.LFB12382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12382:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD0Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer3_TestD0Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer2_TestD2Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer2_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD2Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD2Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer2_TestD2Ev:
.LFB12388:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12388:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD2Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer2_TestD2Ev
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD1Ev
	.set	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD1Ev,_ZN39AliasBufferTest_SharedArrayBuffer2_TestD2Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer2_TestD0Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer2_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD0Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD0Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer2_TestD0Ev:
.LFB12390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12390:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD0Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer2_TestD0Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer1_TestD2Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer1_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD2Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD2Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer1_TestD2Ev:
.LFB12396:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12396:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD2Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer1_TestD2Ev
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD1Ev
	.set	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD1Ev,_ZN39AliasBufferTest_SharedArrayBuffer1_TestD2Ev
	.section	.text._ZN39AliasBufferTest_SharedArrayBuffer1_TestD0Ev,"axG",@progbits,_ZN39AliasBufferTest_SharedArrayBuffer1_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD0Ev
	.type	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD0Ev, @function
_ZN39AliasBufferTest_SharedArrayBuffer1_TestD0Ev:
.LFB12398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12398:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD0Ev, .-_ZN39AliasBufferTest_SharedArrayBuffer1_TestD0Ev
	.section	.text._ZN33AliasBufferTest_Float64Array_TestD2Ev,"axG",@progbits,_ZN33AliasBufferTest_Float64Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33AliasBufferTest_Float64Array_TestD2Ev
	.type	_ZN33AliasBufferTest_Float64Array_TestD2Ev, @function
_ZN33AliasBufferTest_Float64Array_TestD2Ev:
.LFB12404:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12404:
	.size	_ZN33AliasBufferTest_Float64Array_TestD2Ev, .-_ZN33AliasBufferTest_Float64Array_TestD2Ev
	.weak	_ZN33AliasBufferTest_Float64Array_TestD1Ev
	.set	_ZN33AliasBufferTest_Float64Array_TestD1Ev,_ZN33AliasBufferTest_Float64Array_TestD2Ev
	.section	.text._ZN33AliasBufferTest_Float64Array_TestD0Ev,"axG",@progbits,_ZN33AliasBufferTest_Float64Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33AliasBufferTest_Float64Array_TestD0Ev
	.type	_ZN33AliasBufferTest_Float64Array_TestD0Ev, @function
_ZN33AliasBufferTest_Float64Array_TestD0Ev:
.LFB12406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12406:
	.size	_ZN33AliasBufferTest_Float64Array_TestD0Ev, .-_ZN33AliasBufferTest_Float64Array_TestD0Ev
	.section	.text._ZN33AliasBufferTest_Float32Array_TestD2Ev,"axG",@progbits,_ZN33AliasBufferTest_Float32Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33AliasBufferTest_Float32Array_TestD2Ev
	.type	_ZN33AliasBufferTest_Float32Array_TestD2Ev, @function
_ZN33AliasBufferTest_Float32Array_TestD2Ev:
.LFB12412:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12412:
	.size	_ZN33AliasBufferTest_Float32Array_TestD2Ev, .-_ZN33AliasBufferTest_Float32Array_TestD2Ev
	.weak	_ZN33AliasBufferTest_Float32Array_TestD1Ev
	.set	_ZN33AliasBufferTest_Float32Array_TestD1Ev,_ZN33AliasBufferTest_Float32Array_TestD2Ev
	.section	.text._ZN33AliasBufferTest_Float32Array_TestD0Ev,"axG",@progbits,_ZN33AliasBufferTest_Float32Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33AliasBufferTest_Float32Array_TestD0Ev
	.type	_ZN33AliasBufferTest_Float32Array_TestD0Ev, @function
_ZN33AliasBufferTest_Float32Array_TestD0Ev:
.LFB12414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12414:
	.size	_ZN33AliasBufferTest_Float32Array_TestD0Ev, .-_ZN33AliasBufferTest_Float32Array_TestD0Ev
	.section	.text._ZN31AliasBufferTest_Int32Array_TestD2Ev,"axG",@progbits,_ZN31AliasBufferTest_Int32Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31AliasBufferTest_Int32Array_TestD2Ev
	.type	_ZN31AliasBufferTest_Int32Array_TestD2Ev, @function
_ZN31AliasBufferTest_Int32Array_TestD2Ev:
.LFB12420:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12420:
	.size	_ZN31AliasBufferTest_Int32Array_TestD2Ev, .-_ZN31AliasBufferTest_Int32Array_TestD2Ev
	.weak	_ZN31AliasBufferTest_Int32Array_TestD1Ev
	.set	_ZN31AliasBufferTest_Int32Array_TestD1Ev,_ZN31AliasBufferTest_Int32Array_TestD2Ev
	.section	.text._ZN31AliasBufferTest_Int32Array_TestD0Ev,"axG",@progbits,_ZN31AliasBufferTest_Int32Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31AliasBufferTest_Int32Array_TestD0Ev
	.type	_ZN31AliasBufferTest_Int32Array_TestD0Ev, @function
_ZN31AliasBufferTest_Int32Array_TestD0Ev:
.LFB12422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12422:
	.size	_ZN31AliasBufferTest_Int32Array_TestD0Ev, .-_ZN31AliasBufferTest_Int32Array_TestD0Ev
	.section	.text._ZN32AliasBufferTest_Uint32Array_TestD2Ev,"axG",@progbits,_ZN32AliasBufferTest_Uint32Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32AliasBufferTest_Uint32Array_TestD2Ev
	.type	_ZN32AliasBufferTest_Uint32Array_TestD2Ev, @function
_ZN32AliasBufferTest_Uint32Array_TestD2Ev:
.LFB12428:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12428:
	.size	_ZN32AliasBufferTest_Uint32Array_TestD2Ev, .-_ZN32AliasBufferTest_Uint32Array_TestD2Ev
	.weak	_ZN32AliasBufferTest_Uint32Array_TestD1Ev
	.set	_ZN32AliasBufferTest_Uint32Array_TestD1Ev,_ZN32AliasBufferTest_Uint32Array_TestD2Ev
	.section	.text._ZN32AliasBufferTest_Uint32Array_TestD0Ev,"axG",@progbits,_ZN32AliasBufferTest_Uint32Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32AliasBufferTest_Uint32Array_TestD0Ev
	.type	_ZN32AliasBufferTest_Uint32Array_TestD0Ev, @function
_ZN32AliasBufferTest_Uint32Array_TestD0Ev:
.LFB12430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12430:
	.size	_ZN32AliasBufferTest_Uint32Array_TestD0Ev, .-_ZN32AliasBufferTest_Uint32Array_TestD0Ev
	.section	.text._ZN31AliasBufferTest_Int16Array_TestD2Ev,"axG",@progbits,_ZN31AliasBufferTest_Int16Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31AliasBufferTest_Int16Array_TestD2Ev
	.type	_ZN31AliasBufferTest_Int16Array_TestD2Ev, @function
_ZN31AliasBufferTest_Int16Array_TestD2Ev:
.LFB12436:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12436:
	.size	_ZN31AliasBufferTest_Int16Array_TestD2Ev, .-_ZN31AliasBufferTest_Int16Array_TestD2Ev
	.weak	_ZN31AliasBufferTest_Int16Array_TestD1Ev
	.set	_ZN31AliasBufferTest_Int16Array_TestD1Ev,_ZN31AliasBufferTest_Int16Array_TestD2Ev
	.section	.text._ZN31AliasBufferTest_Int16Array_TestD0Ev,"axG",@progbits,_ZN31AliasBufferTest_Int16Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31AliasBufferTest_Int16Array_TestD0Ev
	.type	_ZN31AliasBufferTest_Int16Array_TestD0Ev, @function
_ZN31AliasBufferTest_Int16Array_TestD0Ev:
.LFB12438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12438:
	.size	_ZN31AliasBufferTest_Int16Array_TestD0Ev, .-_ZN31AliasBufferTest_Int16Array_TestD0Ev
	.section	.text._ZN32AliasBufferTest_Uint16Array_TestD2Ev,"axG",@progbits,_ZN32AliasBufferTest_Uint16Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32AliasBufferTest_Uint16Array_TestD2Ev
	.type	_ZN32AliasBufferTest_Uint16Array_TestD2Ev, @function
_ZN32AliasBufferTest_Uint16Array_TestD2Ev:
.LFB12444:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12444:
	.size	_ZN32AliasBufferTest_Uint16Array_TestD2Ev, .-_ZN32AliasBufferTest_Uint16Array_TestD2Ev
	.weak	_ZN32AliasBufferTest_Uint16Array_TestD1Ev
	.set	_ZN32AliasBufferTest_Uint16Array_TestD1Ev,_ZN32AliasBufferTest_Uint16Array_TestD2Ev
	.section	.text._ZN32AliasBufferTest_Uint16Array_TestD0Ev,"axG",@progbits,_ZN32AliasBufferTest_Uint16Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN32AliasBufferTest_Uint16Array_TestD0Ev
	.type	_ZN32AliasBufferTest_Uint16Array_TestD0Ev, @function
_ZN32AliasBufferTest_Uint16Array_TestD0Ev:
.LFB12446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12446:
	.size	_ZN32AliasBufferTest_Uint16Array_TestD0Ev, .-_ZN32AliasBufferTest_Uint16Array_TestD0Ev
	.section	.text._ZN30AliasBufferTest_Int8Array_TestD2Ev,"axG",@progbits,_ZN30AliasBufferTest_Int8Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN30AliasBufferTest_Int8Array_TestD2Ev
	.type	_ZN30AliasBufferTest_Int8Array_TestD2Ev, @function
_ZN30AliasBufferTest_Int8Array_TestD2Ev:
.LFB12452:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12452:
	.size	_ZN30AliasBufferTest_Int8Array_TestD2Ev, .-_ZN30AliasBufferTest_Int8Array_TestD2Ev
	.weak	_ZN30AliasBufferTest_Int8Array_TestD1Ev
	.set	_ZN30AliasBufferTest_Int8Array_TestD1Ev,_ZN30AliasBufferTest_Int8Array_TestD2Ev
	.section	.text._ZN30AliasBufferTest_Int8Array_TestD0Ev,"axG",@progbits,_ZN30AliasBufferTest_Int8Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN30AliasBufferTest_Int8Array_TestD0Ev
	.type	_ZN30AliasBufferTest_Int8Array_TestD0Ev, @function
_ZN30AliasBufferTest_Int8Array_TestD0Ev:
.LFB12454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12454:
	.size	_ZN30AliasBufferTest_Int8Array_TestD0Ev, .-_ZN30AliasBufferTest_Int8Array_TestD0Ev
	.section	.text._ZN31AliasBufferTest_Uint8Array_TestD2Ev,"axG",@progbits,_ZN31AliasBufferTest_Uint8Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31AliasBufferTest_Uint8Array_TestD2Ev
	.type	_ZN31AliasBufferTest_Uint8Array_TestD2Ev, @function
_ZN31AliasBufferTest_Uint8Array_TestD2Ev:
.LFB12460:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE12460:
	.size	_ZN31AliasBufferTest_Uint8Array_TestD2Ev, .-_ZN31AliasBufferTest_Uint8Array_TestD2Ev
	.weak	_ZN31AliasBufferTest_Uint8Array_TestD1Ev
	.set	_ZN31AliasBufferTest_Uint8Array_TestD1Ev,_ZN31AliasBufferTest_Uint8Array_TestD2Ev
	.section	.text._ZN31AliasBufferTest_Uint8Array_TestD0Ev,"axG",@progbits,_ZN31AliasBufferTest_Uint8Array_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN31AliasBufferTest_Uint8Array_TestD0Ev
	.type	_ZN31AliasBufferTest_Uint8Array_TestD0Ev, @function
_ZN31AliasBufferTest_Uint8Array_TestD0Ev:
.LFB12462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12462:
	.size	_ZN31AliasBufferTest_Uint8Array_TestD0Ev, .-_ZN31AliasBufferTest_Uint8Array_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv:
.LFB12553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV31AliasBufferTest_Uint8Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12553:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv:
.LFB12552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV30AliasBufferTest_Int8Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12552:
	.size	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv:
.LFB12551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV32AliasBufferTest_Uint16Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12551:
	.size	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv:
.LFB12550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV31AliasBufferTest_Int16Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12550:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv:
.LFB12549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV32AliasBufferTest_Uint32Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12549:
	.size	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv:
.LFB12548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV31AliasBufferTest_Int32Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12548:
	.size	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv:
.LFB12547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV33AliasBufferTest_Float32Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12547:
	.size	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv:
.LFB12546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV33AliasBufferTest_Float64Array_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12546:
	.size	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv:
.LFB12545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV39AliasBufferTest_SharedArrayBuffer1_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12545:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv:
.LFB12544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV39AliasBufferTest_SharedArrayBuffer2_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12544:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv:
.LFB12543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV39AliasBufferTest_SharedArrayBuffer3_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12543:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv:
.LFB12542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV39AliasBufferTest_SharedArrayBuffer4_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12542:
	.size	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv:
.LFB12541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV38AliasBufferTest_OperatorOverloads_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12541:
	.size	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv:
.LFB12540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV42AliasBufferTest_OperatorOverloadsRefs_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12540:
	.size	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv
	.section	.text._ZN15NodeTestFixture16TearDownTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture16TearDownTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture16TearDownTestCaseEv
	.type	_ZN15NodeTestFixture16TearDownTestCaseEv, @function
_ZN15NodeTestFixture16TearDownTestCaseEv:
.LFB8487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L114:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L117
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_close@PLT
	testl	%eax, %eax
	jne	.L118
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8487:
	.size	_ZN15NodeTestFixture16TearDownTestCaseEv, .-_ZN15NodeTestFixture16TearDownTestCaseEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC2:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0:
.LFB12658:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC2(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12658:
	.size	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0:
.LFB12659:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC0(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC3(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12659:
	.size	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB12754:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L138
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L139
	cmpq	$1, %rax
	jne	.L131
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L132:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L140
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L131:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L132
	jmp	.L130
.L139:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L130:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L132
.L138:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12754:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB10086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L143
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L144
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L170
	.p2align 4,,10
	.p2align 3
.L143:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L149
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L155
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L171
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L149
.L150:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L157
.L171:
	lock subl	$1, 8(%r13)
	jne	.L157
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L157
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L150
	.p2align 4,,10
	.p2align 3
.L149:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L153
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L149
.L155:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L153
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L153
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L144:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L143
.L170:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L147
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L148:
	cmpl	$1, %eax
	jne	.L143
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L147:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L148
	.cfi_endproc
.LFE10086:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata._ZN15NodeTestFixture13SetUpTestCaseEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"NODE_OPTIONS"
.LC6:
	.string	"cctest"
	.section	.text._ZN15NodeTestFixture13SetUpTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture13SetUpTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture13SetUpTestCaseEv
	.type	_ZN15NodeTestFixture13SetUpTestCaseEv, @function
_ZN15NodeTestFixture13SetUpTestCaseEv:
.LFB8482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN15NodeTestFixture16node_initializedE(%rip)
	je	.L214
.L173:
	movl	$1312, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing5AgentC1Ev@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r13
	movq	%r12, _ZN15NodeTestFixture13tracing_agentE(%rip)
	testq	%r13, %r13
	je	.L174
	movq	%r13, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r12
.L174:
	movq	%r12, %rdi
	call	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %rax
	movq	976(%rax), %r12
	testq	%r12, %r12
	je	.L215
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L216
	movl	$128, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r12
	movq	%r13, _ZN15NodeTestFixture8platformE(%rip)
	testq	%r12, %r12
	je	.L177
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L178
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L180
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L181
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L182:
	cmpl	$1, %eax
	jne	.L180
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L184
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L185:
	cmpl	$1, %eax
	jne	.L180
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L180:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L199
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L189
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L199
.L189:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L197
	lock subl	$1, 8(%r13)
	jne	.L197
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L197
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L189
	.p2align 4,,10
	.p2align 3
.L199:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
.L177:
	movq	%r13, %rdi
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L192
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L199
.L195:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L192
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L192
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L214:
	leaq	.LC5(%rip), %rdi
	call	uv_os_unsetenv@PLT
	leaq	.LC6(%rip), %rax
	leaq	-48(%rbp), %rcx
	movl	$1, -64(%rbp)
	leaq	-60(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movb	$1, _ZN15NodeTestFixture16node_initializedE(%rip)
	call	_ZN4node4InitEPiPPKcS0_PS3_@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r12, %rdi
	call	*%rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L181:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L185
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8482:
	.size	_ZN15NodeTestFixture13SetUpTestCaseEv, .-_ZN15NodeTestFixture13SetUpTestCaseEv
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB10084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L220
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L221
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L247
	.p2align 4,,10
	.p2align 3
.L220:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L226
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L232
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L248
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L226
.L227:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L234
.L248:
	lock subl	$1, 8(%r13)
	jne	.L234
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L234
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L227
	.p2align 4,,10
	.p2align 3
.L226:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L230
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L226
.L232:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L230
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L230
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L221:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L220
.L247:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L224
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L225:
	cmpl	$1, %eax
	jne	.L220
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L224:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L225
	.cfi_endproc
.LFE10084:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.rodata._Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"true"
.LC8:
	.string	"false"
.LC9:
	.string	"v1 == oracle[i]"
	.section	.rodata._Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"../test/cctest/test_aliased_buffer.cc"
	.section	.rodata._Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE.str1.1
.LC11:
	.string	"v2 == oracle[i]"
	.section	.rodata._Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE.str1.8
	.align 8
.LC12:
	.string	"aliasedBuffer->GetJSArray()->Length() == oracle.size()"
	.align 8
.LC13:
	.string	"aliasedBuffer->GetJSArray()->ByteLength() == (oracle.size() * sizeof(NativeT))"
	.section	.rodata._Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE.str1.1
.LC14:
	.string	"v.IsEmpty() == false"
.LC15:
	.string	"v2->IsNumber()"
.LC16:
	.string	"actualValue == oracle[i]"
	.section	.text._Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	8(%rcx), %rdx
	jne	.L250
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L253:
	movzbl	(%rdx,%r12), %ecx
	movq	$0, -104(%rbp)
	cmpb	%r13b, %cl
	sete	-112(%rbp)
	jne	.L351
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	cmpq	%r12, %rax
	jbe	.L263
.L250:
	movq	24(%r14), %rax
	movzbl	(%rdx,%r12), %ecx
	movzbl	(%rax,%r12), %r13d
	movq	$0, -104(%rbp)
	cmpb	%r13b, %cl
	sete	-112(%rbp)
	je	.L253
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	movl	$50, %ecx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L254
	call	_ZdlPv@PLT
.L254:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L255
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L255:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L347
	movq	(%r8), %rdi
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rdi
	je	.L257
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L257:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L347:
	movq	(%rbx), %rdx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-136(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L259
	call	_ZdlPv@PLT
.L259:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L260
	movq	(%rdi), %rax
	call	*8(%rax)
.L260:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L348
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L262
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L262:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L348:
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	cmpq	%r12, %rax
	ja	.L250
.L263:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L251
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L251:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L352
.L265:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L270
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L270:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L353
.L271:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L276
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L354
.L284:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L355
.L289:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	cvttsd2sil	%xmm0, %eax
	movzbl	(%rdx,%r13), %ecx
	movq	$0, -104(%rbp)
	cmpb	%al, %cl
	sete	-112(%rbp)
	jne	.L356
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	cmpq	%rax, %r13
	jnb	.L249
.L276:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L277
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L277:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L296
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L280
	movq	(%rdi), %rax
	call	*8(%rax)
.L280:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L297
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L357
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L297:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L356:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L292
	movq	(%rdi), %rax
	call	*8(%rax)
.L292:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L350
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L350:
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	cmpq	%rax, %r13
	jb	.L276
.L249:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L285
	call	_ZdlPv@PLT
.L285:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L286
	movq	(%rdi), %rax
	call	*8(%rax)
.L286:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L284
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L288
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L288:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L352:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	movq	(%rdi), %rax
	call	*8(%rax)
.L267:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L265
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L353:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	movq	(%rdi), %rax
	call	*8(%rax)
.L273:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L349
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L349:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L271
.L357:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L297
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10113:
	.size	_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE:
.LFB9495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$100, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L421
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L363
.L362:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L363:
	testq	%rax, %rax
	je	.L361
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L361:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$100, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	leaq	100(%rax), %rsi
	movq	%rax, %rdx
	andq	$-8, %rdi
	movq	%rax, -192(%rbp)
	subq	%rdi, %rcx
	movq	%rsi, -176(%rbp)
	addl	$100, %ecx
	movq	$0, (%rax)
	movq	$0, 92(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, -184(%rbp)
	movl	$100, %edi
	.p2align 4,,10
	.p2align 3
.L367:
	movl	%edi, %ecx
	subl	%eax, %ecx
	movb	%cl, (%rdx,%rax)
	movq	-184(%rbp), %rcx
	addq	$1, %rax
	movq	-192(%rbp), %rdx
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rax, %rsi
	ja	.L367
	cmpq	%rcx, %rdx
	je	.L369
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L364:
	movzbl	(%rdx,%rax), %ecx
	movq	-136(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-192(%rbp), %rdx
	addq	$1, %rax
	movq	-184(%rbp), %rcx
	subq	%rdx, %rcx
	cmpq	%rax, %rcx
	ja	.L364
.L369:
	leaq	-160(%rbp), %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, -184(%rbp)
	je	.L366
	.p2align 4,,10
	.p2align 3
.L365:
	movzbl	(%rcx,%rax), %ecx
	movq	-136(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-192(%rbp), %rcx
	addq	$1, %rax
	movq	-184(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L365
.L366:
	movq	-136(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	$0, -80(%rbp)
	movdqu	-152(%rbp), %xmm1
	movq	%rax, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	%rdi, -112(%rbp)
	movups	%xmm1, -104(%rbp)
	testq	%rax, %rax
	je	.L370
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L422
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L374
.L373:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L374:
	testq	%rax, %rax
	je	.L370
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L370:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L377
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L377:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIhN2v810Uint8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L376
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L376:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L423
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L362
	jmp	.L361
.L422:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L373
	jmp	.L370
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9495:
	.size	_Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN31AliasBufferTest_Uint8Array_Test8TestBodyEv
	.type	_ZN31AliasBufferTest_Uint8Array_Test8TestBodyEv, @function
_ZN31AliasBufferTest_Uint8Array_Test8TestBodyEv:
.LFB8529:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestIhN2v810Uint8ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8529:
	.size	_ZN31AliasBufferTest_Uint8Array_Test8TestBodyEv, .-_ZN31AliasBufferTest_Uint8Array_Test8TestBodyEv
	.section	.text._Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	8(%rcx), %rdx
	jne	.L426
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L429:
	movzbl	(%rdx,%r12), %ecx
	movq	$0, -104(%rbp)
	cmpb	%r13b, %cl
	sete	-112(%rbp)
	jne	.L527
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	cmpq	%r12, %rax
	jbe	.L439
.L426:
	movq	24(%r14), %rax
	movzbl	(%rdx,%r12), %ecx
	movzbl	(%rax,%r12), %r13d
	movq	$0, -104(%rbp)
	cmpb	%r13b, %cl
	sete	-112(%rbp)
	je	.L429
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	movl	$50, %ecx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L431
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L431:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L523
	movq	(%r8), %rdi
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rdi
	je	.L433
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L433:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L523:
	movq	(%rbx), %rdx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-136(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L435
	call	_ZdlPv@PLT
.L435:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L436
	movq	(%rdi), %rax
	call	*8(%rax)
.L436:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L524
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L438
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L438:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L524:
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	cmpq	%r12, %rax
	ja	.L426
.L439:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L427
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L427:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L528
.L441:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L446
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L446:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L529
.L447:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L452
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L530
.L460:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L531
.L465:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	cvttsd2sil	%xmm0, %eax
	movzbl	(%rdx,%r13), %ecx
	movq	$0, -104(%rbp)
	cmpb	%al, %cl
	sete	-112(%rbp)
	jne	.L532
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	cmpq	%rax, %r13
	jnb	.L425
.L452:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L453
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L453:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L472
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	movq	(%rdi), %rax
	call	*8(%rax)
.L456:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L473
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L533
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L473:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L468
	movq	(%rdi), %rax
	call	*8(%rax)
.L468:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L526
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L526:
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	cmpq	%rax, %r13
	jb	.L452
.L425:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L534
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	movq	(%rdi), %rax
	call	*8(%rax)
.L462:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L460
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L464
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L464:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L531:
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L443
	movq	(%rdi), %rax
	call	*8(%rax)
.L443:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L441
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L529:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L448
	call	_ZdlPv@PLT
.L448:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L449
	movq	(%rdi), %rax
	call	*8(%rax)
.L449:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L525
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L525:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L447
.L533:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L473
.L534:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10138:
	.size	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE:
.LFB9496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$100, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L597
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L539
.L538:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L539:
	testq	%rax, %rax
	je	.L537
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L537:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$100, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	leaq	100(%rax), %rsi
	movq	%rax, %rdx
	andq	$-8, %rdi
	movq	%rax, -192(%rbp)
	subq	%rdi, %rcx
	movq	%rsi, -176(%rbp)
	addl	$100, %ecx
	movq	$0, (%rax)
	movq	$0, 92(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, -184(%rbp)
	movl	$100, %edi
	.p2align 4,,10
	.p2align 3
.L543:
	movl	%edi, %ecx
	subl	%eax, %ecx
	movb	%cl, (%rdx,%rax)
	movq	-184(%rbp), %rcx
	addq	$1, %rax
	movq	-192(%rbp), %rdx
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rax, %rsi
	ja	.L543
	cmpq	%rcx, %rdx
	je	.L545
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L540:
	movzbl	(%rdx,%rax), %ecx
	movq	-136(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-192(%rbp), %rdx
	addq	$1, %rax
	movq	-184(%rbp), %rcx
	subq	%rdx, %rcx
	cmpq	%rax, %rcx
	ja	.L540
.L545:
	leaq	-160(%rbp), %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, -184(%rbp)
	je	.L542
	.p2align 4,,10
	.p2align 3
.L541:
	movzbl	(%rcx,%rax), %ecx
	movq	-136(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-192(%rbp), %rcx
	addq	$1, %rax
	movq	-184(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L541
.L542:
	movq	-136(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	$0, -80(%rbp)
	movdqu	-152(%rbp), %xmm1
	movq	%rax, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	%rdi, -112(%rbp)
	movups	%xmm1, -104(%rbp)
	testq	%rax, %rax
	je	.L546
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L598
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L550
.L549:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L550:
	testq	%rax, %rax
	je	.L546
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L546:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L553
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L553:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L552
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L552:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L599
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L538
	jmp	.L537
.L598:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L549
	jmp	.L546
.L599:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9496:
	.size	_Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN30AliasBufferTest_Int8Array_Test8TestBodyEv
	.type	_ZN30AliasBufferTest_Int8Array_Test8TestBodyEv, @function
_ZN30AliasBufferTest_Int8Array_Test8TestBodyEv:
.LFB8536:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestIaN2v89Int8ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8536:
	.size	_ZN30AliasBufferTest_Int8Array_Test8TestBodyEv, .-_ZN30AliasBufferTest_Int8Array_Test8TestBodyEv
	.section	.text._Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rdx, 8(%rcx)
	jne	.L602
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L605:
	leaq	(%r12,%r12), %rax
	movzwl	(%rdx,%rax), %ecx
	movq	$0, -104(%rbp)
	cmpw	%r13w, %cx
	sete	-112(%rbp)
	jne	.L703
.L610:
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	%rax
	cmpq	%r12, %rax
	jbe	.L615
.L602:
	leaq	(%r12,%r12), %rax
	movzwl	(%rdx,%r12,2), %ecx
	movq	%rax, -144(%rbp)
	movq	24(%r14), %rax
	movzwl	(%rax,%r12,2), %r13d
	movq	$0, -104(%rbp)
	cmpw	%r13w, %cx
	sete	-112(%rbp)
	je	.L605
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	movl	$50, %ecx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L606
	call	_ZdlPv@PLT
.L606:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L607
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L607:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L699
	movq	(%r8), %rdi
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rdi
	je	.L609
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L609:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L699:
	movq	(%rbx), %rdx
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L703:
	movq	-136(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L612
	movq	(%rdi), %rax
	call	*8(%rax)
.L612:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L700
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L614
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L614:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L700:
	movq	(%rbx), %rdx
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L615:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L603
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L603:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	sarq	%rdx
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L704
.L617:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L622
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L622:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L705
.L623:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L628
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L648:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L706
.L636:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L707
.L641:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	cvttsd2sil	%xmm0, %eax
	movzwl	(%rdx,%r13,2), %ecx
	movq	$0, -104(%rbp)
	cmpw	%ax, %cx
	sete	-112(%rbp)
	jne	.L708
.L642:
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	%rax
	cmpq	%rax, %r13
	jnb	.L601
.L628:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L629
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L629:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L648
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L631
	call	_ZdlPv@PLT
.L631:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L632
	movq	(%rdi), %rax
	call	*8(%rax)
.L632:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L649
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L709
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L649:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L708:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L644
	movq	(%rdi), %rax
	call	*8(%rax)
.L644:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L702
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L646
	call	_ZdlPv@PLT
.L646:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L702:
	movq	(%rbx), %rdx
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L706:
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L638
	movq	(%rdi), %rax
	call	*8(%rax)
.L638:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L636
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L640
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L640:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L601:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L710
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L618
	call	_ZdlPv@PLT
.L618:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L619
	movq	(%rdi), %rax
	call	*8(%rax)
.L619:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L617
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L621
	call	_ZdlPv@PLT
.L621:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L705:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L625
	movq	(%rdi), %rax
	call	*8(%rax)
.L625:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L701
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L701:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L623
.L709:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L649
.L710:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10163:
	.size	_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE:
.LFB9500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$200, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v811Uint16Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%r8
	popq	%r9
	testq	%rax, %rax
	je	.L785
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L715
.L714:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L715:
	testq	%rax, %rax
	je	.L713
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L713:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$200, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	movdqa	.LC22(%rip), %xmm3
	movdqa	.LC18(%rip), %xmm7
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	andq	$-8, %rdi
	leaq	200(%rax), %rsi
	movdqa	.LC19(%rip), %xmm6
	movdqa	.LC20(%rip), %xmm5
	subq	%rdi, %rcx
	movq	%rsi, -176(%rbp)
	movdqa	.LC21(%rip), %xmm4
	addl	$200, %ecx
	movq	$0, (%rax)
	movq	$0, 192(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, -184(%rbp)
	movq	%rdx, %rax
	leaq	192(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L718:
	movdqa	%xmm3, %xmm1
	addq	$16, %rax
	paddq	%xmm7, %xmm3
	movdqa	%xmm1, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm6, %xmm0
	shufps	$136, %xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm4, %xmm1
	paddq	%xmm5, %xmm2
	shufps	$136, %xmm1, %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L718
	movl	$3, %ecx
	movl	$4, %eax
	movl	$2, %esi
	movl	$1, %edi
	movw	%cx, 194(%rdx)
	movq	-136(%rbp), %rcx
	movw	%ax, 192(%rdx)
	leaq	15(%rcx), %rax
	movw	%si, 196(%rdx)
	subq	%rdx, %rax
	movw	%di, 198(%rdx)
	cmpq	$30, %rax
	movl	$0, %eax
	jbe	.L720
	.p2align 4,,10
	.p2align 3
.L721:
	movdqu	(%rdx,%rax), %xmm4
	movups	%xmm4, (%rcx,%rax)
	addq	$16, %rax
	cmpq	$192, %rax
	jne	.L721
	movzwl	192(%rdx), %eax
	movw	%ax, 192(%rcx)
	movzwl	194(%rdx), %eax
	movw	%ax, 194(%rcx)
	movzwl	196(%rdx), %eax
	movw	%ax, 196(%rcx)
	movzwl	198(%rdx), %eax
	movw	%ax, 198(%rcx)
.L722:
	leaq	-160(%rbp), %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rdx
	subq	%rcx, %rdx
	sarq	%rdx
	je	.L786
	movq	-136(%rbp), %rsi
	leaq	15(%rsi), %rax
	subq	%rcx, %rax
	cmpq	$30, %rax
	jbe	.L739
	leaq	-1(%rdx), %rax
	cmpq	$6, %rax
	jbe	.L739
	movq	%rdx, %rdi
	xorl	%eax, %eax
	shrq	$3, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L724:
	movdqu	(%rcx,%rax), %xmm5
	movups	%xmm5, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L724
	movq	%rdx, %rax
	andq	$-8, %rax
	testb	$7, %dl
	je	.L726
	movzwl	(%rcx,%rax,2), %edi
	movw	%di, (%rsi,%rax,2)
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L726
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L726
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	3(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L726
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L726
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	5(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L726
	movzwl	(%rcx,%rdi,2), %r8d
	addq	$6, %rax
	movw	%r8w, (%rsi,%rdi,2)
	cmpq	%rax, %rdx
	jbe	.L726
	movzwl	(%rcx,%rax,2), %edx
	movw	%dx, (%rsi,%rax,2)
.L726:
	movq	-160(%rbp), %rdi
	movdqu	-152(%rbp), %xmm6
	movq	%rsi, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rdi, -112(%rbp)
	movups	%xmm6, -104(%rbp)
	testq	%rax, %rax
	je	.L729
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L787
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L732
.L731:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L732:
	testq	%rax, %rax
	je	.L729
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L729:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L735
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L735:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateItN2v811Uint16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L733
	call	_ZdlPv@PLT
.L733:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L734
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L734:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L788
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_restore_state
	movzwl	(%rdx,%rax,2), %esi
	movw	%si, (%rcx,%rax,2)
	addq	$1, %rax
	cmpq	$100, %rax
	jne	.L720
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L785:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L714
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L739:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L723:
	movzwl	(%rcx,%rax,2), %edi
	movw	%di, (%rsi,%rax,2)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L723
	jmp	.L726
.L786:
	movq	-136(%rbp), %rsi
	jmp	.L726
.L787:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L731
	jmp	.L729
.L788:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9500:
	.size	_Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN32AliasBufferTest_Uint16Array_Test8TestBodyEv
	.type	_ZN32AliasBufferTest_Uint16Array_Test8TestBodyEv, @function
_ZN32AliasBufferTest_Uint16Array_Test8TestBodyEv:
.LFB8543:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestItN2v811Uint16ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8543:
	.size	_ZN32AliasBufferTest_Uint16Array_Test8TestBodyEv, .-_ZN32AliasBufferTest_Uint16Array_Test8TestBodyEv
	.section	.text._Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rdx, 8(%rcx)
	jne	.L791
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	(%r12,%r12), %rax
	movzwl	(%rdx,%rax), %ecx
	movq	$0, -104(%rbp)
	cmpw	%r13w, %cx
	sete	-112(%rbp)
	jne	.L892
.L799:
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	%rax
	cmpq	%r12, %rax
	jbe	.L804
.L791:
	leaq	(%r12,%r12), %rax
	movzwl	(%rdx,%r12,2), %ecx
	movq	%rax, -144(%rbp)
	movq	24(%r14), %rax
	movzwl	(%rax,%r12,2), %r13d
	movq	$0, -104(%rbp)
	cmpw	%r13w, %cx
	sete	-112(%rbp)
	je	.L794
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	movl	$50, %ecx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L795
	call	_ZdlPv@PLT
.L795:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L796
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L796:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L888
	movq	(%r8), %rdi
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rdi
	je	.L798
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L798:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L888:
	movq	(%rbx), %rdx
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L892:
	movq	-136(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L800
	call	_ZdlPv@PLT
.L800:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L801
	movq	(%rdi), %rax
	call	*8(%rax)
.L801:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L889
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L803
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L803:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L889:
	movq	(%rbx), %rdx
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L804:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L792
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L792:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	sarq	%rdx
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L893
.L806:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L811
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L811:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L894
.L812:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L817
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L837:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L895
.L825:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L896
.L830:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	cvttsd2sil	%xmm0, %eax
	movzwl	(%rdx,%r13,2), %ecx
	movq	$0, -104(%rbp)
	cmpw	%ax, %cx
	sete	-112(%rbp)
	jne	.L897
.L831:
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	%rax
	cmpq	%rax, %r13
	jnb	.L790
.L817:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L818
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L818:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L837
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L821
	movq	(%rdi), %rax
	call	*8(%rax)
.L821:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L838
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L898
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L838:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L897:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L832
	call	_ZdlPv@PLT
.L832:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L833
	movq	(%rdi), %rax
	call	*8(%rax)
.L833:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L891
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L891:
	movq	(%rbx), %rdx
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L895:
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L826
	call	_ZdlPv@PLT
.L826:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L827
	movq	(%rdi), %rax
	call	*8(%rax)
.L827:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L825
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L829
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L829:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L790:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L899
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L896:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L893:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L807
	call	_ZdlPv@PLT
.L807:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L808
	movq	(%rdi), %rax
	call	*8(%rax)
.L808:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L806
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L810
	call	_ZdlPv@PLT
.L810:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L894:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L813
	call	_ZdlPv@PLT
.L813:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L814
	movq	(%rdi), %rax
	call	*8(%rax)
.L814:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L890
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L816
	call	_ZdlPv@PLT
.L816:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L890:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L812
.L898:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L838
.L899:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10188:
	.size	_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE:
.LFB9504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$200, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v810Int16Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%r8
	popq	%r9
	testq	%rax, %rax
	je	.L974
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L904
.L903:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L904:
	testq	%rax, %rax
	je	.L902
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L902:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$200, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	movdqa	.LC22(%rip), %xmm3
	movdqa	.LC18(%rip), %xmm7
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	andq	$-8, %rdi
	leaq	200(%rax), %rsi
	movdqa	.LC19(%rip), %xmm6
	movdqa	.LC20(%rip), %xmm5
	subq	%rdi, %rcx
	movq	%rsi, -176(%rbp)
	movdqa	.LC21(%rip), %xmm4
	addl	$200, %ecx
	movq	$0, (%rax)
	movq	$0, 192(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, -184(%rbp)
	movq	%rdx, %rax
	leaq	192(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L907:
	movdqa	%xmm3, %xmm1
	addq	$16, %rax
	paddq	%xmm7, %xmm3
	movdqa	%xmm1, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm6, %xmm0
	shufps	$136, %xmm0, %xmm2
	movdqa	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	paddq	%xmm4, %xmm1
	paddq	%xmm5, %xmm2
	shufps	$136, %xmm1, %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L907
	movl	$3, %ecx
	movl	$4, %eax
	movl	$2, %esi
	movl	$1, %edi
	movw	%cx, 194(%rdx)
	movq	-136(%rbp), %rcx
	movw	%ax, 192(%rdx)
	leaq	15(%rcx), %rax
	movw	%si, 196(%rdx)
	subq	%rdx, %rax
	movw	%di, 198(%rdx)
	cmpq	$30, %rax
	movl	$0, %eax
	jbe	.L909
	.p2align 4,,10
	.p2align 3
.L910:
	movdqu	(%rdx,%rax), %xmm4
	movups	%xmm4, (%rcx,%rax)
	addq	$16, %rax
	cmpq	$192, %rax
	jne	.L910
	movzwl	192(%rdx), %eax
	movw	%ax, 192(%rcx)
	movzwl	194(%rdx), %eax
	movw	%ax, 194(%rcx)
	movzwl	196(%rdx), %eax
	movw	%ax, 196(%rcx)
	movzwl	198(%rdx), %eax
	movw	%ax, 198(%rcx)
.L911:
	leaq	-160(%rbp), %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rdx
	subq	%rcx, %rdx
	sarq	%rdx
	je	.L975
	movq	-136(%rbp), %rsi
	leaq	15(%rsi), %rax
	subq	%rcx, %rax
	cmpq	$30, %rax
	jbe	.L928
	leaq	-1(%rdx), %rax
	cmpq	$6, %rax
	jbe	.L928
	movq	%rdx, %rdi
	xorl	%eax, %eax
	shrq	$3, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L913:
	movdqu	(%rcx,%rax), %xmm5
	movups	%xmm5, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L913
	movq	%rdx, %rax
	andq	$-8, %rax
	testb	$7, %dl
	je	.L915
	movzwl	(%rcx,%rax,2), %edi
	movw	%di, (%rsi,%rax,2)
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L915
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L915
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	3(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L915
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L915
	movzwl	(%rcx,%rdi,2), %r8d
	movw	%r8w, (%rsi,%rdi,2)
	leaq	5(%rax), %rdi
	cmpq	%rdi, %rdx
	jbe	.L915
	movzwl	(%rcx,%rdi,2), %r8d
	addq	$6, %rax
	movw	%r8w, (%rsi,%rdi,2)
	cmpq	%rax, %rdx
	jbe	.L915
	movzwl	(%rcx,%rax,2), %edx
	movw	%dx, (%rsi,%rax,2)
.L915:
	movq	-160(%rbp), %rdi
	movdqu	-152(%rbp), %xmm6
	movq	%rsi, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rdi, -112(%rbp)
	movups	%xmm6, -104(%rbp)
	testq	%rax, %rax
	je	.L918
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L976
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L921
.L920:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L921:
	testq	%rax, %rax
	je	.L918
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L918:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L924
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L924:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIsN2v810Int16ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L922
	call	_ZdlPv@PLT
.L922:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L923
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L923:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L977
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	movzwl	(%rdx,%rax,2), %esi
	movw	%si, (%rcx,%rax,2)
	addq	$1, %rax
	cmpq	$100, %rax
	jne	.L909
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L974:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L903
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L928:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L912:
	movzwl	(%rcx,%rax,2), %edi
	movw	%di, (%rsi,%rax,2)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L912
	jmp	.L915
.L975:
	movq	-136(%rbp), %rsi
	jmp	.L915
.L976:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L920
	jmp	.L918
.L977:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9504:
	.size	_Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN31AliasBufferTest_Int16Array_Test8TestBodyEv
	.type	_ZN31AliasBufferTest_Int16Array_Test8TestBodyEv, @function
_ZN31AliasBufferTest_Int16Array_Test8TestBodyEv:
.LFB8550:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestIsN2v810Int16ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8550:
	.size	_ZN31AliasBufferTest_Int16Array_Test8TestBodyEv, .-_ZN31AliasBufferTest_Int16Array_Test8TestBodyEv
	.section	.text._Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rdx, 8(%rcx)
	jne	.L980
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L983:
	leaq	0(,%r12,4), %rax
	movl	(%rdx,%rax), %ecx
	movq	$0, -104(%rbp)
	cmpl	%r13d, %ecx
	sete	-112(%rbp)
	jne	.L1081
.L988:
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%r12, %rax
	jbe	.L993
.L980:
	leaq	0(,%r12,4), %rax
	movl	(%rdx,%r12,4), %ecx
	movq	%rax, -144(%rbp)
	movq	24(%r14), %rax
	movl	(%rax,%r12,4), %r13d
	movq	$0, -104(%rbp)
	cmpl	%r13d, %ecx
	sete	-112(%rbp)
	je	.L983
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	movl	$50, %ecx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L984
	call	_ZdlPv@PLT
.L984:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L985
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L985:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1077
	movq	(%r8), %rdi
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rdi
	je	.L987
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L987:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1077:
	movq	(%rbx), %rdx
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	-136(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L989
	call	_ZdlPv@PLT
.L989:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L990
	movq	(%rdi), %rax
	call	*8(%rax)
.L990:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1078
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L992
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L992:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1078:
	movq	(%rbx), %rdx
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L993:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L981
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L981:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L1082
.L995:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1000
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1000:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L1083
.L1001:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L1006
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L1084
.L1014:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1085
.L1019:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	cvttsd2siq	%xmm0, %rax
	movl	(%rdx,%r13,4), %ecx
	movq	$0, -104(%rbp)
	cmpl	%eax, %ecx
	sete	-112(%rbp)
	jne	.L1086
.L1020:
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%rax, %r13
	jnb	.L979
.L1006:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1007
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1007:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L1026
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1009
	call	_ZdlPv@PLT
.L1009:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1010
	movq	(%rdi), %rax
	call	*8(%rax)
.L1010:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1027
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1087
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1027:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1021
	call	_ZdlPv@PLT
.L1021:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1022
	movq	(%rdi), %rax
	call	*8(%rax)
.L1022:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1080
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1024
	call	_ZdlPv@PLT
.L1024:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1080:
	movq	(%rbx), %rdx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1016
	movq	(%rdi), %rax
	call	*8(%rax)
.L1016:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1014
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1018
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L1018:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L979:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1088
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1085:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1082:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L996
	call	_ZdlPv@PLT
.L996:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L997
	movq	(%rdi), %rax
	call	*8(%rax)
.L997:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L995
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L999
	call	_ZdlPv@PLT
.L999:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1083:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1002
	call	_ZdlPv@PLT
.L1002:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1003
	movq	(%rdi), %rax
	call	*8(%rax)
.L1003:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1079
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1005
	call	_ZdlPv@PLT
.L1005:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1079:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L1001
.L1087:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1027
.L1088:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10207:
	.size	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE:
.LFB9508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$400, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L1163
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1093
.L1092:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L1093:
	testq	%rax, %rax
	je	.L1091
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1091:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$400, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	movdqa	.LC22(%rip), %xmm1
	movdqa	.LC20(%rip), %xmm4
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	andq	$-8, %rdi
	leaq	400(%rax), %rsi
	movdqa	.LC19(%rip), %xmm3
	subq	%rdi, %rcx
	movq	%rsi, -176(%rbp)
	addl	$400, %ecx
	movq	$0, (%rax)
	movq	$0, 392(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, -184(%rbp)
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L1096:
	movdqa	%xmm1, %xmm0
	addq	$16, %rax
	paddq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm3, %xmm2
	shufps	$136, %xmm2, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L1096
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	movl	$0, %eax
	jbe	.L1097
	.p2align 4,,10
	.p2align 3
.L1098:
	movdqu	(%rdx,%rax), %xmm5
	movups	%xmm5, (%rcx,%rax)
	addq	$16, %rax
	cmpq	$400, %rax
	jne	.L1098
.L1099:
	leaq	-160(%rbp), %rax
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	movq	-184(%rbp), %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	je	.L1164
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L1116
	leaq	-1(%rdx), %rax
	cmpq	$3, %rax
	jbe	.L1116
	movq	%rdx, %rsi
	xorl	%eax, %eax
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1101:
	movdqu	(%rdi,%rax), %xmm6
	movups	%xmm6, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1101
	movq	%rdx, %rax
	andq	$-4, %rax
	testb	$3, %dl
	je	.L1103
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L1103
	movl	(%rdi,%rsi,4), %r8d
	addq	$2, %rax
	movl	%r8d, (%rcx,%rsi,4)
	cmpq	%rax, %rdx
	jbe	.L1103
	movl	(%rdi,%rax,4), %edx
	movl	%edx, (%rcx,%rax,4)
.L1103:
	movq	-160(%rbp), %rdi
	movdqu	-152(%rbp), %xmm7
	movq	%rcx, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rdi, -112(%rbp)
	movups	%xmm7, -104(%rbp)
	testq	%rax, %rax
	je	.L1106
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1165
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L1109
.L1108:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L1109:
	testq	%rax, %rax
	je	.L1106
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1106:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1112
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1112:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1110
	call	_ZdlPv@PLT
.L1110:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1111
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1111:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1166
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1097:
	.cfi_restore_state
	movl	(%rdx,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	$100, %rax
	je	.L1099
	movl	(%rdx,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	$100, %rax
	jne	.L1097
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1092
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1116:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1100:
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1100
	jmp	.L1103
.L1164:
	movq	-136(%rbp), %rcx
	jmp	.L1103
.L1165:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1108
	jmp	.L1106
.L1166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9508:
	.size	_Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN32AliasBufferTest_Uint32Array_Test8TestBodyEv
	.type	_ZN32AliasBufferTest_Uint32Array_Test8TestBodyEv, @function
_ZN32AliasBufferTest_Uint32Array_Test8TestBodyEv:
.LFB8557:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestIjN2v811Uint32ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8557:
	.size	_ZN32AliasBufferTest_Uint32Array_Test8TestBodyEv, .-_ZN32AliasBufferTest_Uint32Array_Test8TestBodyEv
	.section	.text._Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-120(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rdx, 8(%rcx)
	jne	.L1169
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1172:
	leaq	0(,%r12,4), %rax
	movl	(%rdx,%rax), %ecx
	movq	$0, -104(%rbp)
	cmpl	%r13d, %ecx
	sete	-112(%rbp)
	jne	.L1270
.L1177:
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%r12, %rax
	jbe	.L1182
.L1169:
	leaq	0(,%r12,4), %rax
	movl	(%rdx,%r12,4), %ecx
	movq	%rax, -144(%rbp)
	movq	24(%r14), %rax
	movl	(%rax,%r12,4), %r13d
	movq	$0, -104(%rbp)
	cmpl	%r13d, %ecx
	sete	-112(%rbp)
	je	.L1172
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	leaq	.LC10(%rip), %rdx
	movl	$50, %ecx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1174
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L1174:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1266
	movq	(%r8), %rdi
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rdi
	je	.L1176
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L1176:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1266:
	movq	(%rbx), %rdx
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	-136(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1178
	call	_ZdlPv@PLT
.L1178:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1179
	movq	(%rdi), %rax
	call	*8(%rax)
.L1179:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1267
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1181
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L1181:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1267:
	movq	(%rbx), %rdx
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1170
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1170:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L1271
.L1184:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1189
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1189:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L1272
.L1190:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L1195
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L1273
.L1203:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1274
.L1208:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	cvttsd2sil	%xmm0, %eax
	movl	(%rdx,%r13,4), %ecx
	movq	$0, -104(%rbp)
	cmpl	%eax, %ecx
	sete	-112(%rbp)
	jne	.L1275
.L1209:
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%rax, %r13
	jnb	.L1168
.L1195:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1196
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1196:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L1215
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1198
	call	_ZdlPv@PLT
.L1198:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1199
	movq	(%rdi), %rax
	call	*8(%rax)
.L1199:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1216
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1276
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1216:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1210
	call	_ZdlPv@PLT
.L1210:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1211
	movq	(%rdi), %rax
	call	*8(%rax)
.L1211:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1269
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1213
	call	_ZdlPv@PLT
.L1213:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1269:
	movq	(%rbx), %rdx
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1204
	call	_ZdlPv@PLT
.L1204:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1205
	movq	(%rdi), %rax
	call	*8(%rax)
.L1205:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1203
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1207
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L1207:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1277
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1274:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1185
	call	_ZdlPv@PLT
.L1185:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1186
	movq	(%rdi), %rax
	call	*8(%rax)
.L1186:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1184
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1188
	call	_ZdlPv@PLT
.L1188:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1272:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1191
	call	_ZdlPv@PLT
.L1191:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1192
	movq	(%rdi), %rax
	call	*8(%rax)
.L1192:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1268
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1194
	call	_ZdlPv@PLT
.L1194:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1268:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L1190
.L1276:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1216
.L1277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10232:
	.size	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE:
.LFB9509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$400, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v810Int32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L1352
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1282
.L1281:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L1282:
	testq	%rax, %rax
	je	.L1280
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1280:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$400, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	movdqa	.LC22(%rip), %xmm1
	movdqa	.LC20(%rip), %xmm4
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	andq	$-8, %rdi
	leaq	400(%rax), %rsi
	movdqa	.LC19(%rip), %xmm3
	subq	%rdi, %rcx
	movq	%rsi, -176(%rbp)
	addl	$400, %ecx
	movq	$0, (%rax)
	movq	$0, 392(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, -184(%rbp)
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L1285:
	movdqa	%xmm1, %xmm0
	addq	$16, %rax
	paddq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm3, %xmm2
	shufps	$136, %xmm2, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L1285
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	movl	$0, %eax
	jbe	.L1286
	.p2align 4,,10
	.p2align 3
.L1287:
	movdqu	(%rdx,%rax), %xmm5
	movups	%xmm5, (%rcx,%rax)
	addq	$16, %rax
	cmpq	$400, %rax
	jne	.L1287
.L1288:
	leaq	-160(%rbp), %rax
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	movq	-184(%rbp), %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	je	.L1353
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L1305
	leaq	-1(%rdx), %rax
	cmpq	$3, %rax
	jbe	.L1305
	movq	%rdx, %rsi
	xorl	%eax, %eax
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1290:
	movdqu	(%rdi,%rax), %xmm6
	movups	%xmm6, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1290
	movq	%rdx, %rax
	andq	$-4, %rax
	testb	$3, %dl
	je	.L1292
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L1292
	movl	(%rdi,%rsi,4), %r8d
	addq	$2, %rax
	movl	%r8d, (%rcx,%rsi,4)
	cmpq	%rax, %rdx
	jbe	.L1292
	movl	(%rdi,%rax,4), %edx
	movl	%edx, (%rcx,%rax,4)
.L1292:
	movq	-160(%rbp), %rdi
	movdqu	-152(%rbp), %xmm7
	movq	%rcx, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rdi, -112(%rbp)
	movups	%xmm7, -104(%rbp)
	testq	%rax, %rax
	je	.L1295
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1354
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L1298
.L1297:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L1298:
	testq	%rax, %rax
	je	.L1295
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1295:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1301
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1301:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1299
	call	_ZdlPv@PLT
.L1299:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1300
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1300:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1355
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1286:
	.cfi_restore_state
	movl	(%rdx,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	$100, %rax
	je	.L1288
	movl	(%rdx,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	$100, %rax
	jne	.L1286
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1281
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1305:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1289:
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1289
	jmp	.L1292
.L1353:
	movq	-136(%rbp), %rcx
	jmp	.L1292
.L1354:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1297
	jmp	.L1295
.L1355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9509:
	.size	_Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN31AliasBufferTest_Int32Array_Test8TestBodyEv
	.type	_ZN31AliasBufferTest_Int32Array_Test8TestBodyEv, @function
_ZN31AliasBufferTest_Int32Array_Test8TestBodyEv:
.LFB8564:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestIiN2v810Int32ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8564:
	.size	_ZN31AliasBufferTest_Int32Array_Test8TestBodyEv, .-_ZN31AliasBufferTest_Int32Array_Test8TestBodyEv
	.section	.rodata._Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm,"axG",@progbits,_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm,comdat
	.p2align 4
	.weak	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm
	.type	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm, @function
_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm:
.LFB9521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$408, %rsp
	movq	%rdx, -416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	leaq	-384(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v87Context5EnterEv@PLT
	leaq	(%r12,%r13), %rdx
	leaq	0(,%rbx,4), %rax
	movq	%r14, -256(%rbp)
	movq	%rdx, -400(%rbp)
	addq	%rax, %rdx
	movq	%rax, -432(%rbp)
	popq	%rax
	movq	%rdx, -248(%rbp)
	popq	%rcx
	movq	$0, -240(%rbp)
	movq	$0, -224(%rbp)
	testq	%rdx, %rdx
	je	.L1551
	leaq	-160(%rbp), %rax
	movq	%r14, %rsi
	movq	%rdx, -392(%rbp)
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-392(%rbp), %rdx
	movq	-256(%rbp), %rdi
	movq	%rdx, %rsi
	movq	%rdx, -424(%rbp)
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rax, %r13
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, -392(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movq	-240(%rbp), %rsi
	movq	%r13, %rdi
	movq	-424(%rbp), %rdx
	movq	%rax, -232(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1552
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-224(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1362
.L1361:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-112(%rbp), %rax
	movq	$0, -224(%rbp)
.L1362:
	testq	%rax, %rax
	je	.L1360
	movq	-392(%rbp), %rdi
	leaq	-224(%rbp), %rsi
	movq	%rax, -224(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1360:
	movq	-408(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-392(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r14, -208(%rbp)
	movq	%r12, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1429
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1429:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r12
	ja	.L1368
	movq	-232(%rbp), %rax
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1553
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-176(%rbp), %rdi
	movq	%rax, -160(%rbp)
	testq	%rdi, %rdi
	je	.L1367
.L1366:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-160(%rbp), %rax
	movq	$0, -176(%rbp)
.L1367:
	testq	%rax, %rax
	je	.L1365
	movq	-408(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1365:
	movq	-392(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	-288(%rbp), %rax
	movq	%r14, %rsi
	movq	-416(%rbp), %xmm0
	movq	%r12, %xmm7
	movq	%rax, %rdi
	movq	%r14, -160(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movq	$0, -128(%rbp)
	movq	%rax, -424(%rbp)
	movups	%xmm0, -152(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1427
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1427:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-416(%rbp), %rdx
	subq	%r12, %rax
	cmpq	%rax, %rdx
	ja	.L1368
	movq	-232(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addq	%r12, %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1554
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1372
.L1371:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L1372:
	testq	%rax, %rax
	je	.L1370
	movq	-392(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1370:
	movq	-424(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	movq	%r14, %rsi
	movhps	-400(%rbp), %xmm0
	movq	%r14, -112(%rbp)
	movq	$0, -80(%rbp)
	movups	%xmm0, -104(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1425
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1425:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	-400(%rbp), %r13
	movq	%rax, %rdi
	andl	$3, %r13d
	jne	.L1555
	movabsq	$4611686018427387903, %rax
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L1556
	movq	%rdi, -448(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	subq	-400(%rbp), %rax
	cmpq	%rax, -432(%rbp)
	movq	-448(%rbp), %rdi
	ja	.L1557
	movq	-400(%rbp), %rsi
	movq	-232(%rbp), %rax
	movq	%rbx, %rdx
	addq	%rsi, %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v810Int32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1558
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -320(%rbp)
	testq	%rdi, %rdi
	je	.L1379
.L1378:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-320(%rbp), %rax
.L1379:
	testq	%rax, %rax
	je	.L1377
	leaq	-80(%rbp), %rsi
	leaq	-320(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1377:
	movq	-424(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r12, %r12
	js	.L1381
	pxor	%xmm0, %xmm0
	movl	$0, %ecx
	movq	$0, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	je	.L1380
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r12), %rcx
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%rcx, -400(%rbp)
	call	memset@PLT
	movq	-400(%rbp), %rcx
.L1380:
	movq	-416(%rbp), %rax
	movq	%rcx, -344(%rbp)
	testq	%rax, %rax
	js	.L1381
	pxor	%xmm0, %xmm0
	movl	$0, %r12d
	movq	$0, -304(%rbp)
	movaps	%xmm0, -320(%rbp)
	je	.L1382
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r12), %r12
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	movq	%r12, -304(%rbp)
	call	memset@PLT
.L1382:
	movabsq	$2305843009213693951, %rax
	movq	%r12, -312(%rbp)
	cmpq	%rax, %rbx
	ja	.L1381
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movq	$0, -272(%rbp)
	movaps	%xmm0, -288(%rbp)
	testq	%rbx, %rbx
	je	.L1383
	movq	-432(%rbp), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%rbx), %r12
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	movq	%r12, -272(%rbp)
	call	memset@PLT
.L1383:
	movq	-344(%rbp), %rsi
	movq	-352(%rbp), %rdx
	movq	%r12, -280(%rbp)
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	je	.L1384
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1385:
	movl	%edi, %ecx
	subl	%eax, %ecx
	movb	%cl, (%rdx,%rax)
	movq	-344(%rbp), %rsi
	addq	$1, %rax
	movq	-352(%rbp), %rdx
	movq	%rsi, %rcx
	subq	%rdx, %rcx
	cmpq	%rax, %rcx
	ja	.L1385
.L1384:
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %rax
	movq	%rcx, %rdi
	subq	%rax, %rdi
	je	.L1386
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1387:
	movl	%edi, %ecx
	subl	%edx, %ecx
	movb	%cl, (%rax,%rdx)
	movq	-312(%rbp), %rcx
	addq	$1, %rdx
	movq	-320(%rbp), %rax
	movq	%rcx, %rsi
	subq	%rax, %rsi
	cmpq	%rdx, %rsi
	ja	.L1387
	movq	-344(%rbp), %rsi
	movq	-352(%rbp), %rdx
.L1386:
	movq	-288(%rbp), %r12
	movq	-280(%rbp), %rbx
	subq	%r12, %rbx
	sarq	$2, %rbx
	je	.L1394
	leaq	-1(%rbx), %rdi
	cmpq	$2, %rdi
	jbe	.L1436
	movq	%rbx, %r9
	movq	%rdi, %xmm7
	movq	%rbx, %xmm1
	movdqa	.LC20(%rip), %xmm4
	shrq	$2, %r9
	movdqa	.LC19(%rip), %xmm3
	punpcklqdq	%xmm7, %xmm1
	movq	%r12, %rdi
	salq	$4, %r9
	addq	%r12, %r9
	.p2align 4,,10
	.p2align 3
.L1392:
	movdqa	%xmm1, %xmm0
	addq	$16, %rdi
	paddq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm3, %xmm2
	shufps	$136, %xmm2, %xmm0
	movups	%xmm0, -16(%rdi)
	cmpq	%r9, %rdi
	jne	.L1392
	movq	%rbx, %rdi
	movq	%rbx, %r9
	andq	$-4, %rdi
	andl	$3, %r9d
	testb	$3, %bl
	je	.L1394
.L1391:
	leaq	1(%rdi), %r10
	movl	%r9d, (%r12,%rdi,4)
	leaq	-1(%r9), %r11
	cmpq	%r10, %rbx
	jbe	.L1394
	addq	$2, %rdi
	movl	%r11d, (%r12,%r10,4)
	subq	$2, %r9
	cmpq	%rdi, %rbx
	jbe	.L1394
	movl	%r9d, (%r12,%rdi,4)
.L1394:
	xorl	%edi, %edi
	cmpq	%rdx, %rsi
	je	.L1390
	.p2align 4,,10
	.p2align 3
.L1389:
	movzbl	(%rdx,%rdi), %edx
	movq	-184(%rbp), %rax
	movb	%dl, (%rax,%rdi)
	movq	-352(%rbp), %rdx
	addq	$1, %rdi
	movq	-344(%rbp), %rax
	subq	%rdx, %rax
	cmpq	%rdi, %rax
	ja	.L1389
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %rax
.L1390:
	xorl	%edx, %edx
	cmpq	%rax, %rcx
	je	.L1397
	.p2align 4,,10
	.p2align 3
.L1396:
	movzbl	(%rax,%rdx), %ecx
	movq	-136(%rbp), %rax
	movb	%cl, (%rax,%rdx)
	movq	-320(%rbp), %rax
	addq	$1, %rdx
	movq	-312(%rbp), %rcx
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	ja	.L1396
.L1397:
	movq	-288(%rbp), %rdi
	movq	-280(%rbp), %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	je	.L1404
	movq	-88(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L1439
	leaq	-1(%rdx), %rax
	cmpq	$3, %rax
	jbe	.L1439
	movq	%rdx, %rsi
	xorl	%eax, %eax
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1402:
	movdqu	(%rdi,%rax), %xmm5
	movups	%xmm5, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1402
	movq	%rdx, %rax
	andq	$-4, %rax
	testb	$3, %dl
	je	.L1404
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	leaq	1(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L1404
	movl	(%rdi,%rsi,4), %r9d
	addq	$2, %rax
	movl	%r9d, (%rcx,%rsi,4)
	cmpq	%rax, %rdx
	jbe	.L1404
	movl	(%rdi,%rax,4), %edx
	movl	%edx, (%rcx,%rax,4)
.L1404:
	leaq	-208(%rbp), %r8
	leaq	-352(%rbp), %r12
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r8, -400(%rbp)
	leaq	-320(%rbp), %rbx
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	-408(%rbp), %rdx
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-424(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	-392(%rbp), %rdx
	call	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-352(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, -344(%rbp)
	movq	-400(%rbp), %r8
	je	.L1400
	.p2align 4,,10
	.p2align 3
.L1399:
	movzbl	(%rcx,%rax), %ecx
	movq	-184(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-352(%rbp), %rcx
	addq	$1, %rax
	movq	-344(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L1399
.L1400:
	movq	-320(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	-312(%rbp), %rcx
	je	.L1408
	.p2align 4,,10
	.p2align 3
.L1407:
	movzbl	(%rcx,%rax), %ecx
	movq	-136(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-320(%rbp), %rcx
	addq	$1, %rax
	movq	-312(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L1407
.L1408:
	movq	-288(%rbp), %rcx
	movq	-280(%rbp), %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	je	.L1415
	movq	-88(%rbp), %rsi
	leaq	15(%rsi), %rdx
	subq	%rcx, %rdx
	cmpq	$30, %rdx
	jbe	.L1493
	leaq	-1(%rax), %rdx
	cmpq	$3, %rdx
	jbe	.L1493
	movq	%rax, %rdx
	shrq	$2, %rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1413:
	movdqu	(%rcx,%r13), %xmm6
	movups	%xmm6, (%rsi,%r13)
	addq	$16, %r13
	cmpq	%r13, %rdx
	jne	.L1413
	movq	%rax, %rdx
	andq	$-4, %rdx
	testb	$3, %al
	je	.L1415
	movl	(%rcx,%rdx,4), %edi
	movl	%edi, (%rsi,%rdx,4)
	leaq	1(%rdx), %rdi
	cmpq	%rax, %rdi
	jnb	.L1415
	movl	(%rcx,%rdi,4), %r9d
	addq	$2, %rdx
	movl	%r9d, (%rsi,%rdi,4)
	cmpq	%rdx, %rax
	jbe	.L1415
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, (%rsi,%rdx,4)
.L1415:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	-408(%rbp), %rdx
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-424(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	-392(%rbp), %rdx
	call	_Z15ReadAndValidateIiN2v810Int32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1411
	call	_ZdlPv@PLT
.L1411:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1418
	call	_ZdlPv@PLT
.L1418:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1419
	call	_ZdlPv@PLT
.L1419:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1420
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1420:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1421
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1421:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1422
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1422:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1423
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1423:
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-440(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1559
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1493:
	.cfi_restore_state
	movl	(%rcx,%r13,4), %edx
	movl	%edx, (%rsi,%r13,4)
	addq	$1, %r13
	cmpq	%r13, %rax
	jne	.L1493
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1368:
	leaq	_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	-224(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1361
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	-176(%rbp), %rdi
	movq	$0, -160(%rbp)
	testq	%rdi, %rdi
	jne	.L1366
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1371
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1551:
	leaq	_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1439:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1401:
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1401
	jmp	.L1404
.L1558:
	movq	$0, -320(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1378
	jmp	.L1377
.L1555:
	leaq	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1556:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1557:
	leaq	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1436:
	movq	%rbx, %r9
	xorl	%edi, %edi
	jmp	.L1391
.L1381:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9521:
	.size	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm, .-_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm
	.text
	.align 2
	.p2align 4
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer4_Test8TestBodyEv
	.type	_ZN39AliasBufferTest_SharedArrayBuffer4_Test8TestBodyEv, @function
_ZN39AliasBufferTest_SharedArrayBuffer4_Test8TestBodyEv:
.LFB8606:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movl	$1, %ecx
	movl	$3, %edx
	movl	$1, %esi
	jmp	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_iNS0_10Int32ArrayEEvPNS0_7IsolateEmmm
	.cfi_endproc
.LFE8606:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer4_Test8TestBodyEv, .-_ZN39AliasBufferTest_SharedArrayBuffer4_Test8TestBodyEv
	.section	.text._Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, 8(%rcx)
	jne	.L1562
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1668:
	jne	.L1618
.L1565:
	leaq	0(,%r12,4), %rax
	movss	(%rdx,%rax), %xmm1
	movq	$0, -104(%rbp)
	ucomiss	%xmm0, %xmm1
	setnp	%al
	cmovne	%r13d, %eax
	movb	%al, -112(%rbp)
	jp	.L1619
	jne	.L1619
.L1571:
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%r12, %rax
	jbe	.L1577
.L1562:
	leaq	0(,%r12,4), %rax
	movss	(%rdx,%r12,4), %xmm1
	movq	%rax, -136(%rbp)
	movq	24(%r14), %rax
	movss	(%rax,%r12,4), %xmm0
	movq	$0, -104(%rbp)
	ucomiss	%xmm0, %xmm1
	setnp	%al
	cmovne	%r13d, %eax
	movb	%al, -112(%rbp)
	jnp	.L1668
.L1618:
	leaq	-120(%rbp), %r9
	movss	%xmm0, -148(%rbp)
	movq	%r9, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$50, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movss	-148(%rbp), %xmm0
	cmpq	%rax, %rdi
	je	.L1567
	movss	%xmm0, -136(%rbp)
	call	_ZdlPv@PLT
	movss	-136(%rbp), %xmm0
.L1567:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1568
	movq	(%rdi), %rax
	movss	%xmm0, -136(%rbp)
	call	*8(%rax)
	movss	-136(%rbp), %xmm0
.L1568:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1669
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1570
	movq	%r8, -144(%rbp)
	movss	%xmm0, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movss	-136(%rbp), %xmm0
.L1570:
	movl	$32, %esi
	movq	%r8, %rdi
	movss	%xmm0, -136(%rbp)
	call	_ZdlPvm@PLT
	movq	(%rbx), %rdx
	movss	-136(%rbp), %xmm0
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1619:
	leaq	-120(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1573
	call	_ZdlPv@PLT
.L1573:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1574
	movq	(%rdi), %rax
	call	*8(%rax)
.L1574:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1665
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1576
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L1576:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1665:
	movq	(%rbx), %rdx
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1563
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1563:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L1670
.L1579:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1584
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1584:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L1671
.L1585:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L1590
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L1672
.L1598:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1673
.L1603:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	movl	$0, %ecx
	cvtsd2ss	%xmm0, %xmm0
	movss	(%rdx,%r13,4), %xmm1
	movq	$0, -104(%rbp)
	ucomiss	%xmm0, %xmm1
	setnp	%al
	cmovne	%ecx, %eax
	movb	%al, -112(%rbp)
	jp	.L1620
	jne	.L1620
.L1604:
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%rax, %r13
	jnb	.L1561
.L1590:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1591
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1591:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L1612
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1593
	call	_ZdlPv@PLT
.L1593:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1594
	movq	(%rdi), %rax
	call	*8(%rax)
.L1594:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1611
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1674
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1611:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1606
	call	_ZdlPv@PLT
.L1606:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1607
	movq	(%rdi), %rax
	call	*8(%rax)
.L1607:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1667
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1609
	call	_ZdlPv@PLT
.L1609:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1667:
	movq	(%rbx), %rdx
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1599
	call	_ZdlPv@PLT
.L1599:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1600
	movq	(%rdi), %rax
	call	*8(%rax)
.L1600:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1598
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1602
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L1602:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1675
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1673:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1670:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1580
	call	_ZdlPv@PLT
.L1580:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1581
	movq	(%rdi), %rax
	call	*8(%rax)
.L1581:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1579
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1583
	call	_ZdlPv@PLT
.L1583:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1671:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1586
	call	_ZdlPv@PLT
.L1586:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1587
	movq	(%rdi), %rax
	call	*8(%rax)
.L1587:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1666
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1589
	call	_ZdlPv@PLT
.L1589:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1666:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L1585
.L1675:
	call	__stack_chk_fail@PLT
.L1674:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1611
.L1669:
	movq	(%rbx), %rdx
	jmp	.L1565
	.cfi_endproc
.LFE10257:
	.size	_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE:
.LFB9513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$400, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v812Float32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L1749
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1680
.L1679:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L1680:
	testq	%rax, %rax
	je	.L1678
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1678:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$400, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %rsi
	movq	%rax, -192(%rbp)
	andq	$-8, %rdi
	leaq	400(%rax), %rdx
	subq	%rdi, %rcx
	movq	%rdx, -176(%rbp)
	addl	$400, %ecx
	movq	$0, (%rax)
	movq	$0, 392(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, -184(%rbp)
	movq	%rsi, %rcx
	movl	$100, %eax
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1751:
	pxor	%xmm0, %xmm0
	addq	$4, %rcx
	cvtsi2ssq	%rax, %xmm0
	movss	%xmm0, -4(%rcx)
	subq	$1, %rax
	je	.L1750
.L1685:
	testq	%rax, %rax
	jns	.L1751
	movq	%rax, %rdx
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	addq	$4, %rcx
	shrq	%rdx
	andl	$1, %edi
	orq	%rdi, %rdx
	cvtsi2ssq	%rdx, %xmm0
	addss	%xmm0, %xmm0
	movss	%xmm0, -4(%rcx)
	subq	$1, %rax
	jne	.L1685
.L1750:
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rdx
	subq	%rsi, %rdx
	cmpq	$30, %rdx
	movl	$0, %edx
	jbe	.L1686
	.p2align 4,,10
	.p2align 3
.L1687:
	movups	(%rsi,%rdx), %xmm1
	movups	%xmm1, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	$400, %rdx
	jne	.L1687
.L1688:
	movq	%rax, -256(%rbp)
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	leaq	-160(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	movq	-184(%rbp), %rdx
	movq	-256(%rbp), %rax
	subq	%rdi, %rdx
	sarq	$2, %rdx
	je	.L1752
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rsi
	subq	%rdi, %rsi
	cmpq	$30, %rsi
	jbe	.L1727
	leaq	-1(%rdx), %rsi
	cmpq	$3, %rsi
	jbe	.L1727
	movq	%rdx, %rsi
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1690:
	movups	(%rdi,%rax), %xmm2
	movups	%xmm2, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1690
	movq	%rdx, %rax
	andq	$-4, %rax
	testb	$3, %dl
	je	.L1692
	movss	(%rdi,%rax,4), %xmm0
	leaq	1(%rax), %rsi
	movss	%xmm0, (%rcx,%rax,4)
	cmpq	%rsi, %rdx
	jbe	.L1692
	movss	(%rdi,%rsi,4), %xmm0
	addq	$2, %rax
	movss	%xmm0, (%rcx,%rsi,4)
	cmpq	%rax, %rdx
	jbe	.L1692
	movss	(%rdi,%rax,4), %xmm0
	movss	%xmm0, (%rcx,%rax,4)
.L1692:
	movq	-160(%rbp), %rdi
	movdqu	-152(%rbp), %xmm3
	movq	%rcx, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rdi, -112(%rbp)
	movups	%xmm3, -104(%rbp)
	testq	%rax, %rax
	je	.L1695
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1753
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L1698
.L1697:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L1698:
	testq	%rax, %rax
	je	.L1695
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1695:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1701
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1701:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIfN2v812Float32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1699
	call	_ZdlPv@PLT
.L1699:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1700
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1700:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1754
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1686:
	.cfi_restore_state
	movss	(%rsi,%rdx,4), %xmm0
	movss	%xmm0, (%rcx,%rdx,4)
	addq	$1, %rdx
	cmpq	$100, %rdx
	jne	.L1686
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1727:
	movss	(%rdi,%rax,4), %xmm0
	movss	%xmm0, (%rcx,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1727
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1679
	jmp	.L1678
.L1752:
	movq	-136(%rbp), %rcx
	jmp	.L1692
.L1753:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1697
	jmp	.L1695
.L1754:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9513:
	.size	_Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN33AliasBufferTest_Float32Array_Test8TestBodyEv
	.type	_ZN33AliasBufferTest_Float32Array_Test8TestBodyEv, @function
_ZN33AliasBufferTest_Float32Array_Test8TestBodyEv:
.LFB8571:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestIfN2v812Float32ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8571:
	.size	_ZN33AliasBufferTest_Float32Array_Test8TestBodyEv, .-_ZN33AliasBufferTest_Float32Array_Test8TestBodyEv
	.section	.text._Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,"axG",@progbits,_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE,comdat
	.p2align 4
	.weak	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.type	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, @function
_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE:
.LFB10276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	(%rcx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, 8(%rcx)
	jne	.L1757
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L1863:
	jne	.L1813
.L1760:
	leaq	0(,%r12,8), %rax
	movsd	(%rdx,%rax), %xmm1
	movq	$0, -104(%rbp)
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%r13d, %eax
	movb	%al, -112(%rbp)
	jp	.L1814
	jne	.L1814
.L1766:
	movq	8(%rbx), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	jbe	.L1772
.L1757:
	leaq	0(,%r12,8), %rax
	movsd	(%rdx,%r12,8), %xmm1
	movq	%rax, -136(%rbp)
	movq	24(%r14), %rax
	movsd	(%rax,%r12,8), %xmm0
	movq	$0, -104(%rbp)
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%r13d, %eax
	movb	%al, -112(%rbp)
	jnp	.L1863
.L1813:
	leaq	-120(%rbp), %r9
	movsd	%xmm0, -152(%rbp)
	movq	%r9, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC9(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$50, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movsd	-152(%rbp), %xmm0
	cmpq	%rax, %rdi
	je	.L1762
	movsd	%xmm0, -136(%rbp)
	call	_ZdlPv@PLT
	movsd	-136(%rbp), %xmm0
.L1762:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1763
	movq	(%rdi), %rax
	movsd	%xmm0, -136(%rbp)
	call	*8(%rax)
	movsd	-136(%rbp), %xmm0
.L1763:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1864
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1765
	movq	%r8, -144(%rbp)
	movsd	%xmm0, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movsd	-136(%rbp), %xmm0
.L1765:
	movl	$32, %esi
	movq	%r8, %rdi
	movsd	%xmm0, -136(%rbp)
	call	_ZdlPvm@PLT
	movq	(%rbx), %rdx
	movsd	-136(%rbp), %xmm0
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1814:
	leaq	-120(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC11(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$51, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -136(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1768
	call	_ZdlPv@PLT
.L1768:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1769
	movq	(%rdi), %rax
	call	*8(%rax)
.L1769:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1860
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1771
	movq	%r8, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L1771:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1860:
	movq	(%rbx), %rdx
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1772:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1758
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1758:
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	8(%rbx), %rdx
	subq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	sarq	$3, %rdx
	cmpq	%rdx, %rax
	sete	-112(%rbp)
	jne	.L1865
.L1774:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1779
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1779:
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	sete	-112(%rbp)
	jne	.L1866
.L1780:
	leaq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -136(%rbp)
	cmpq	%rdx, %rcx
	jne	.L1785
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1807:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	movq	$0, -104(%rbp)
	movb	%al, -112(%rbp)
	testb	%al, %al
	je	.L1867
.L1793:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1868
.L1798:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdx
	movl	$0, %ecx
	movsd	(%rdx,%r13,8), %xmm1
	movq	$0, -104(%rbp)
	ucomisd	%xmm0, %xmm1
	setnp	%al
	cmovne	%ecx, %eax
	movb	%al, -112(%rbp)
	jp	.L1815
	jne	.L1815
.L1799:
	movq	8(%rbx), %rax
	addq	$1, %r13
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jnb	.L1756
.L1785:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1786
	movq	(%rdi), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1786:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	$0, -104(%rbp)
	testq	%rax, %rax
	movq	%rax, %r12
	setne	-112(%rbp)
	jne	.L1807
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC14(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$67, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1788
	call	_ZdlPv@PLT
.L1788:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1789
	movq	(%rdi), %rax
	call	*8(%rax)
.L1789:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1806
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1869
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1806:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1815:
	movq	-136(%rbp), %rdi
	leaq	-128(%rbp), %r12
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC16(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$73, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1801
	call	_ZdlPv@PLT
.L1801:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1802
	movq	(%rdi), %rax
	call	*8(%rax)
.L1802:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1862
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1804
	call	_ZdlPv@PLT
.L1804:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1862:
	movq	(%rbx), %rdx
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	-136(%rbp), %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	leaq	-128(%rbp), %rdi
	movl	$69, %ecx
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	movq	%rdi, -144(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-144(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1794
	call	_ZdlPv@PLT
.L1794:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1795
	movq	(%rdi), %rax
	call	*8(%rax)
.L1795:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L1793
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L1797
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L1797:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1870
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1868:
	.cfi_restore_state
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1865:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC12(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$55, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1775
	call	_ZdlPv@PLT
.L1775:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1776
	movq	(%rdi), %rax
	call	*8(%rax)
.L1776:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1774
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1778
	call	_ZdlPv@PLT
.L1778:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L1866:
	leaq	-120(%rbp), %r13
	leaq	-128(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rcx
	leaq	.LC13(%rip), %rdx
	call	_ZN7testing8internal30GetBoolAssertionFailureMessageB5cxx11ERKNS_15AssertionResultEPKcS5_S5_@PLT
	movq	-96(%rbp), %r8
	movl	$56, %ecx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rdx
	movl	$1, %esi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1781
	call	_ZdlPv@PLT
.L1781:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1782
	movq	(%rdi), %rax
	call	*8(%rax)
.L1782:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1861
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1784
	call	_ZdlPv@PLT
.L1784:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1861:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L1780
.L1870:
	call	__stack_chk_fail@PLT
.L1869:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1806
.L1864:
	movq	(%rbx), %rdx
	jmp	.L1760
	.cfi_endproc
.LFE10276:
	.size	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE, .-_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	.section	.text._Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE,"axG",@progbits,_Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE
	.type	_Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE, @function
_Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE:
.LFB9517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC17(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movq	$0, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %rdi
	movl	$800, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-248(%rbp), %r8
	movq	-112(%rbp), %rax
	movl	$100, %edx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L1944
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1875
.L1874:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L1875:
	testq	%rax, %rax
	je	.L1873
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1873:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$800, %edi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%rax, %rsi
	movq	%rax, -192(%rbp)
	andq	$-8, %rdi
	leaq	800(%rax), %rdx
	subq	%rdi, %rcx
	movq	%rdx, -176(%rbp)
	addl	$800, %ecx
	movq	$0, (%rax)
	movq	$0, 792(%rax)
	shrl	$3, %ecx
	xorl	%eax, %eax
	rep stosq
	movq	%rdx, -184(%rbp)
	movq	%rsi, %rcx
	movl	$100, %eax
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1946:
	pxor	%xmm0, %xmm0
	addq	$8, %rcx
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, -8(%rcx)
	subq	$1, %rax
	je	.L1945
.L1880:
	testq	%rax, %rax
	jns	.L1946
	movq	%rax, %rdx
	movq	%rax, %rdi
	pxor	%xmm0, %xmm0
	addq	$8, %rcx
	shrq	%rdx
	andl	$1, %edi
	orq	%rdi, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -8(%rcx)
	subq	$1, %rax
	jne	.L1880
.L1945:
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rdx
	subq	%rsi, %rdx
	cmpq	$30, %rdx
	movl	$0, %edx
	jbe	.L1881
	.p2align 4,,10
	.p2align 3
.L1882:
	movupd	(%rsi,%rdx), %xmm1
	movups	%xmm1, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	$800, %rdx
	jne	.L1882
.L1883:
	movq	%rax, -256(%rbp)
	movq	%r13, %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	leaq	-160(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rsi
	movq	-256(%rbp), %rax
	subq	%rdx, %rsi
	sarq	$3, %rsi
	je	.L1947
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rdi
	subq	%rdx, %rdi
	cmpq	$30, %rdi
	jbe	.L1922
	leaq	-1(%rsi), %rdi
	cmpq	$3, %rdi
	jbe	.L1922
	movq	%rsi, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1885:
	movupd	(%rdx,%rax), %xmm2
	movups	%xmm2, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L1885
	movq	%rsi, %rax
	andq	$-2, %rax
	andl	$1, %esi
	je	.L1887
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
.L1887:
	movq	-160(%rbp), %rdi
	movdqu	-152(%rbp), %xmm3
	movq	%rcx, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rdi, -112(%rbp)
	movups	%xmm3, -104(%rbp)
	testq	%rax, %rax
	je	.L1890
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-160(%rbp), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1948
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -232(%rbp)
	testq	%rdi, %rdi
	je	.L1893
.L1892:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-232(%rbp), %rax
.L1893:
	testq	%rax, %rax
	je	.L1890
	leaq	-80(%rbp), %rsi
	leaq	-232(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1890:
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1896
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1896:
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1894
	call	_ZdlPv@PLT
.L1894:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1895
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1895:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1949
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1881:
	.cfi_restore_state
	movsd	(%rsi,%rdx,8), %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	addq	$1, %rdx
	cmpq	$100, %rdx
	jne	.L1881
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1922:
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L1922
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1874
	jmp	.L1873
.L1947:
	movq	-136(%rbp), %rcx
	jmp	.L1887
.L1948:
	movq	$0, -232(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1892
	jmp	.L1890
.L1949:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9517:
	.size	_Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE, .-_Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN33AliasBufferTest_Float64Array_Test8TestBodyEv
	.type	_ZN33AliasBufferTest_Float64Array_Test8TestBodyEv, @function
_ZN33AliasBufferTest_Float64Array_Test8TestBodyEv:
.LFB8578:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_Z13ReadWriteTestIdN2v812Float64ArrayEEvPNS0_7IsolateE
	.cfi_endproc
.LFE8578:
	.size	_ZN33AliasBufferTest_Float64Array_Test8TestBodyEv, .-_ZN33AliasBufferTest_Float64Array_Test8TestBodyEv
	.section	.text._Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm,"axG",@progbits,_Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm,comdat
	.p2align 4
	.weak	_Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm
	.type	_Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm, @function
_Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm:
.LFB9518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	0(,%rbx,4), %r13
	subq	$424, %rsp
	movq	%rdx, -400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	leaq	-384(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	leaq	0(,%r15,8), %rax
	movq	%r12, -256(%rbp)
	movq	%rax, -440(%rbp)
	addq	%r13, %rax
	leaq	(%rax,%r14), %rdx
	movq	%rax, -432(%rbp)
	popq	%rax
	movq	%rdx, -248(%rbp)
	popq	%rcx
	movq	$0, -240(%rbp)
	movq	$0, -224(%rbp)
	testq	%rdx, %rdx
	je	.L2162
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -392(%rbp)
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-392(%rbp), %rdx
	movq	-256(%rbp), %rdi
	movq	%rdx, %rsi
	movq	%rdx, -424(%rbp)
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rax, %r15
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rax, -392(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movq	-240(%rbp), %rsi
	movq	%r15, %rdi
	movq	-424(%rbp), %rdx
	movq	%rax, -232(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2163
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-224(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1956
.L1955:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-112(%rbp), %rax
	movq	$0, -224(%rbp)
.L1956:
	testq	%rax, %rax
	je	.L1954
	movq	-392(%rbp), %rdi
	leaq	-224(%rbp), %rsi
	movq	%rax, -224(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1954:
	movq	-416(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-392(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r12, -208(%rbp)
	movq	%rbx, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2033
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2033:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %r15
	movabsq	$4611686018427387903, %rax
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L1964
	movq	%r15, %rdi
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r13
	ja	.L2164
	movq	-232(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2165
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-176(%rbp), %rdi
	movq	%rax, -160(%rbp)
	testq	%rdi, %rdi
	je	.L1962
.L1961:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-160(%rbp), %rax
	movq	$0, -176(%rbp)
.L1962:
	testq	%rax, %rax
	je	.L1960
	movq	-416(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1960:
	movq	-392(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	-288(%rbp), %rax
	movq	%r12, %rsi
	movq	-400(%rbp), %xmm0
	movq	%r13, %xmm4
	movq	%rax, %rdi
	movq	%r12, -160(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movq	$0, -128(%rbp)
	movq	%rax, -424(%rbp)
	movups	%xmm0, -152(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2031
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2031:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%r13, %r15
	movq	%rax, %rdi
	andl	$7, %r15d
	jne	.L2166
	movq	-400(%rbp), %rcx
	movabsq	$2305843009213693951, %rax
	andq	%rcx, %rax
	cmpq	%rax, %rcx
	jne	.L1964
	movq	%rdi, -456(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-456(%rbp), %rdi
	subq	%r13, %rax
	cmpq	%rax, -440(%rbp)
	ja	.L2167
	movq	-232(%rbp), %rax
	movq	-400(%rbp), %rdx
	movq	%r13, %rsi
	addq	%r13, %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2168
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1969
.L1968:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L1969:
	testq	%rax, %rax
	je	.L1967
	movq	-392(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1967:
	movq	-424(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-424(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r12, -112(%rbp)
	movq	%r14, %xmm0
	movq	$0, -80(%rbp)
	movhps	-432(%rbp), %xmm0
	movups	%xmm0, -104(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2029
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2029:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	subq	-432(%rbp), %rax
	movq	-456(%rbp), %rdi
	cmpq	%rax, %r14
	ja	.L2169
	movq	-432(%rbp), %rsi
	movq	-232(%rbp), %rax
	movq	%r14, %rdx
	addq	%rsi, %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2170
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -320(%rbp)
	testq	%rdi, %rdi
	je	.L1974
.L1973:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-320(%rbp), %rax
.L1974:
	testq	%rax, %rax
	je	.L1972
	leaq	-80(%rbp), %rsi
	leaq	-320(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1972:
	movq	-424(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rbx
	ja	.L1976
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	$0, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	testq	%rbx, %rbx
	je	.L1975
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r13), %rcx
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%rcx, -432(%rbp)
	call	memset@PLT
	movq	-432(%rbp), %rcx
.L1975:
	movq	-400(%rbp), %rbx
	movq	%rcx, -344(%rbp)
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	ja	.L1976
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movq	$0, -304(%rbp)
	movaps	%xmm0, -320(%rbp)
	testq	%rbx, %rbx
	je	.L1977
	movq	-440(%rbp), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%rbx), %r13
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	movq	%r13, -304(%rbp)
	call	memset@PLT
.L1977:
	movq	%r13, -312(%rbp)
	testq	%r14, %r14
	js	.L1976
	movq	$0, -272(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -288(%rbp)
	je	.L2038
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r14), %rbx
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	movq	%rbx, -272(%rbp)
	call	memset@PLT
	movq	-312(%rbp), %r13
	movq	%rax, %rcx
.L1978:
	movq	-352(%rbp), %r9
	movq	-344(%rbp), %rax
	movq	%rbx, -280(%rbp)
	subq	%r9, %rax
	sarq	$2, %rax
	movq	%rax, %r11
	je	.L1985
	leaq	-1(%rax), %rdx
	cmpq	$2, %rdx
	jbe	.L2039
	movq	%rax, %rsi
	movq	%rdx, %xmm5
	movq	%rax, %xmm1
	movdqa	.LC20(%rip), %xmm4
	shrq	$2, %rsi
	movdqa	.LC19(%rip), %xmm3
	punpcklqdq	%xmm5, %xmm1
	movq	%r9, %rdx
	salq	$4, %rsi
	addq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L1983:
	movdqa	%xmm1, %xmm0
	addq	$16, %rdx
	paddq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddq	%xmm3, %xmm2
	shufps	$136, %xmm2, %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rsi, %rdx
	jne	.L1983
	movq	%rax, %rdx
	movq	%rax, %rsi
	andq	$-4, %rdx
	andl	$3, %esi
	testb	$3, %al
	je	.L1985
.L1982:
	leaq	1(%rdx), %rdi
	movl	%esi, (%r9,%rdx,4)
	leaq	-1(%rsi), %r10
	cmpq	%rax, %rdi
	jnb	.L1985
	addq	$2, %rdx
	movl	%r10d, (%r9,%rdi,4)
	subq	$2, %rsi
	cmpq	%rdx, %rax
	jbe	.L1985
	movl	%esi, (%r9,%rdx,4)
.L1985:
	movq	-320(%rbp), %rdi
	subq	%rdi, %r13
	movq	%rdi, %rsi
	sarq	$3, %r13
	movq	%r13, %rax
	jne	.L1991
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2171:
	pxor	%xmm0, %xmm0
	addq	$8, %rsi
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, -8(%rsi)
	subq	$1, %rax
	je	.L1981
.L1991:
	testq	%rax, %rax
	jns	.L2171
	movq	%rax, %rdx
	movq	%rax, %r8
	pxor	%xmm0, %xmm0
	addq	$8, %rsi
	shrq	%rdx
	andl	$1, %r8d
	orq	%r8, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -8(%rsi)
	subq	$1, %rax
	jne	.L1991
.L1981:
	xorl	%eax, %eax
	cmpq	%rbx, %rcx
	je	.L1988
	.p2align 4,,10
	.p2align 3
.L1987:
	movl	%r14d, %edx
	subl	%eax, %edx
	movb	%dl, (%rcx,%rax)
	movq	-280(%rbp), %rbx
	addq	$1, %rax
	movq	-288(%rbp), %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L1987
	movq	-352(%rbp), %r9
	movq	-320(%rbp), %rdi
	movq	-312(%rbp), %r13
	movq	-344(%rbp), %r11
	subq	%rdi, %r13
	subq	%r9, %r11
	sarq	$3, %r13
	sarq	$2, %r11
.L1988:
	testq	%r11, %r11
	je	.L1993
	movq	-184(%rbp), %rdx
	leaq	15(%rdx), %rax
	subq	%r9, %rax
	cmpq	$30, %rax
	jbe	.L2041
	leaq	-1(%r11), %rax
	cmpq	$3, %rax
	jbe	.L2041
	movq	%r11, %rsi
	xorl	%eax, %eax
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1997:
	movdqu	(%r9,%rax), %xmm5
	movups	%xmm5, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1997
	movq	%r11, %rax
	andq	$-4, %rax
	testb	$3, %r11b
	je	.L1993
	movl	(%r9,%rax,4), %esi
	movl	%esi, (%rdx,%rax,4)
	leaq	1(%rax), %rsi
	cmpq	%r11, %rsi
	jnb	.L1993
	movl	(%r9,%rsi,4), %r10d
	addq	$2, %rax
	movl	%r10d, (%rdx,%rsi,4)
	cmpq	%r11, %rax
	jnb	.L1993
	movl	(%r9,%rax,4), %esi
	movl	%esi, (%rdx,%rax,4)
.L1993:
	testq	%r13, %r13
	je	.L1995
	movq	-136(%rbp), %rdx
	leaq	15(%rdx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L2043
	leaq	-1(%r13), %rax
	cmpq	$3, %rax
	jbe	.L2043
	movq	%r13, %rsi
	xorl	%eax, %eax
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2004:
	movupd	(%rdi,%rax), %xmm6
	movups	%xmm6, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L2004
	movq	%r13, %rax
	andq	$-2, %rax
	andl	$1, %r13d
	je	.L1995
	movsd	(%rdi,%rax,8), %xmm0
	movsd	%xmm0, (%rdx,%rax,8)
.L1995:
	xorl	%eax, %eax
	cmpq	%rcx, %rbx
	je	.L2007
	.p2align 4,,10
	.p2align 3
.L2000:
	movzbl	(%rcx,%rax), %ecx
	movq	-88(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-288(%rbp), %rcx
	addq	$1, %rax
	movq	-280(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L2000
.L2007:
	movq	-408(%rbp), %rsi
	leaq	-352(%rbp), %rbx
	movq	%r12, %rdi
	leaq	-208(%rbp), %r13
	movq	%rbx, %rcx
	movq	%r13, %rdx
	leaq	-320(%rbp), %r14
	call	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-416(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	-408(%rbp), %rsi
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-392(%rbp), %rdx
	movq	-424(%rbp), %rcx
	movq	%r12, %rdi
	movq	-408(%rbp), %rsi
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rax
	subq	%rdx, %rax
	sarq	$2, %rax
	je	.L2002
	movq	-184(%rbp), %rcx
	leaq	15(%rcx), %rsi
	subq	%rdx, %rsi
	cmpq	$30, %rsi
	jbe	.L2044
	leaq	-1(%rax), %rsi
	cmpq	$3, %rsi
	jbe	.L2044
	movq	%rax, %rdi
	xorl	%esi, %esi
	shrq	$2, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2011:
	movdqu	(%rdx,%rsi), %xmm7
	movups	%xmm7, (%rcx,%rsi)
	addq	$16, %rsi
	cmpq	%rsi, %rdi
	jne	.L2011
	movq	%rax, %rsi
	andq	$-4, %rsi
	testb	$3, %al
	je	.L2002
	movl	(%rdx,%rsi,4), %edi
	movl	%edi, (%rcx,%rsi,4)
	leaq	1(%rsi), %rdi
	cmpq	%rdi, %rax
	jbe	.L2002
	movl	(%rdx,%rdi,4), %r9d
	addq	$2, %rsi
	movl	%r9d, (%rcx,%rdi,4)
	cmpq	%rsi, %rax
	jbe	.L2002
	movl	(%rdx,%rsi,4), %eax
	movl	%eax, (%rcx,%rsi,4)
.L2002:
	movq	-320(%rbp), %rdx
	movq	-312(%rbp), %rsi
	subq	%rdx, %rsi
	sarq	$3, %rsi
	je	.L2009
	movq	-136(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2045
	leaq	-1(%rsi), %rax
	cmpq	$3, %rax
	jbe	.L2045
	movq	%rsi, %rdi
	xorl	%eax, %eax
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2018:
	movupd	(%rdx,%rax), %xmm3
	movups	%xmm3, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L2018
	movq	%rsi, %rax
	andq	$-2, %rax
	andl	$1, %esi
	je	.L2009
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
.L2009:
	movq	-288(%rbp), %rdx
	cmpq	-280(%rbp), %rdx
	je	.L2021
	.p2align 4,,10
	.p2align 3
.L2014:
	movzbl	(%rdx,%r15), %edx
	movq	-88(%rbp), %rax
	movb	%dl, (%rax,%r15)
	movq	-288(%rbp), %rdx
	addq	$1, %r15
	movq	-280(%rbp), %rax
	subq	%rdx, %rax
	cmpq	%r15, %rax
	ja	.L2014
.L2021:
	movq	%rbx, %rcx
	movq	-408(%rbp), %rbx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_Z15ReadAndValidateIjN2v811Uint32ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-416(%rbp), %rdx
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-424(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	-392(%rbp), %rdx
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2016
	call	_ZdlPv@PLT
.L2016:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2022
	call	_ZdlPv@PLT
.L2022:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2023
	call	_ZdlPv@PLT
.L2023:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2024
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2024:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2025
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2025:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2026
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2026:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2027
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2027:
	movq	-408(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-448(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2172
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1964:
	.cfi_restore_state
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	-224(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1955
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L2038:
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	-176(%rbp), %rdi
	movq	$0, -160(%rbp)
	testq	%rdi, %rdi
	jne	.L1961
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L2162:
	leaq	_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2044:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L2010:
	movl	(%rdx,%rsi,4), %edi
	movl	%edi, (%rcx,%rsi,4)
	addq	$1, %rsi
	cmpq	%rax, %rsi
	jne	.L2010
	jmp	.L2002
.L2043:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2003:
	movsd	(%rdi,%rax,8), %xmm0
	movsd	%xmm0, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L2003
	jmp	.L1995
.L2041:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1996:
	movl	(%r9,%rax,4), %esi
	movl	%esi, (%rdx,%rax,4)
	addq	$1, %rax
	cmpq	%r11, %rax
	jne	.L1996
	jmp	.L1993
.L2045:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2017:
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L2017
	jmp	.L2009
.L2168:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L1968
	jmp	.L1967
.L2164:
	leaq	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2170:
	movq	$0, -320(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1973
	jmp	.L1972
.L2166:
	leaq	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2167:
	leaq	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2169:
	leaq	_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2039:
	movq	%rax, %rsi
	xorl	%edx, %edx
	jmp	.L1982
.L1976:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9518:
	.size	_Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm, .-_Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm
	.text
	.align 2
	.p2align 4
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer1_Test8TestBodyEv
	.type	_ZN39AliasBufferTest_SharedArrayBuffer1_Test8TestBodyEv, @function
_ZN39AliasBufferTest_SharedArrayBuffer1_Test8TestBodyEv:
.LFB8585:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movl	$8, %ecx
	movl	$80, %edx
	movl	$100, %esi
	jmp	_Z16SharedBufferTestIjN2v811Uint32ArrayEdNS0_12Float64ArrayEaNS0_9Int8ArrayEEvPNS0_7IsolateEmmm
	.cfi_endproc
.LFE8585:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer1_Test8TestBodyEv, .-_ZN39AliasBufferTest_SharedArrayBuffer1_Test8TestBodyEv
	.section	.text._Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm,"axG",@progbits,_Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm,comdat
	.p2align 4
	.weak	_Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm
	.type	_Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm, @function
_Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm:
.LFB9519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	0(,%rbx,8), %r14
	subq	$424, %rsp
	movq	%rcx, -392(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%r12, %rdi
	call	_ZN2v87Isolate5EnterEv@PLT
	leaq	-384(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v87Context5EnterEv@PLT
	movq	-392(%rbp), %rax
	leaq	(%r14,%r13), %rdx
	movq	%r12, -256(%rbp)
	movq	%rdx, -416(%rbp)
	movq	$0, -240(%rbp)
	salq	$3, %rax
	addq	%rax, %rdx
	movq	%rax, -440(%rbp)
	popq	%rax
	movq	%rdx, -248(%rbp)
	popq	%rcx
	movq	$0, -224(%rbp)
	testq	%rdx, %rdx
	je	.L2388
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -400(%rbp)
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-400(%rbp), %rdx
	movq	-256(%rbp), %rdi
	movq	%rdx, %rsi
	movq	%rdx, -432(%rbp)
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rdi, -400(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-408(%rbp), %r8
	movq	-112(%rbp), %rax
	movq	-240(%rbp), %rsi
	movq	-432(%rbp), %rdx
	movq	%r8, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2389
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-224(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2179
.L2178:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-112(%rbp), %rax
	movq	$0, -224(%rbp)
.L2179:
	testq	%rax, %rax
	je	.L2177
	movq	-400(%rbp), %rdi
	leaq	-224(%rbp), %rsi
	movq	%rax, -224(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2177:
	movq	-424(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	-288(%rbp), %rax
	movq	%r12, %rsi
	movq	%r12, -208(%rbp)
	movq	%rax, %rdi
	movq	%rbx, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -176(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2257
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2257:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movabsq	$2305843009213693951, %rax
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L2192
	movq	%rdi, -432(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-432(%rbp), %rdi
	cmpq	%rax, %r14
	ja	.L2193
	movq	-232(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rax, -184(%rbp)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2390
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-176(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2185
.L2184:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-112(%rbp), %rax
	movq	$0, -176(%rbp)
.L2185:
	testq	%rax, %rax
	je	.L2183
	movq	-400(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2183:
	movq	-408(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-400(%rbp), %rdi
	movq	%r14, %xmm5
	movq	%r12, %rsi
	movq	%r13, %xmm0
	movq	%r12, -160(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movq	$0, -128(%rbp)
	movups	%xmm0, -152(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2255
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2255:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-432(%rbp), %rdi
	subq	%r14, %rax
	cmpq	%rax, %r13
	ja	.L2391
	movq	-232(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdx
	addq	%r14, %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2392
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -288(%rbp)
	testq	%rdi, %rdi
	je	.L2190
.L2189:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-288(%rbp), %rax
.L2190:
	testq	%rax, %rax
	je	.L2188
	movq	-408(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2188:
	movq	-400(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	-320(%rbp), %rax
	movq	%r12, %rsi
	movq	-392(%rbp), %xmm0
	movq	%rax, %rdi
	movq	%r12, -112(%rbp)
	movhps	-416(%rbp), %xmm0
	movq	$0, -80(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -104(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2253
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2253:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	-416(%rbp), %r8
	movq	%rax, %rdi
	andl	$7, %r8d
	jne	.L2393
	movq	-392(%rbp), %rcx
	movq	%r8, -456(%rbp)
	movabsq	$2305843009213693951, %rax
	andq	%rcx, %rax
	cmpq	%rax, %rcx
	jne	.L2192
	movq	%rdi, -464(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-416(%rbp), %rsi
	subq	%rsi, %rax
	cmpq	%rax, -440(%rbp)
	ja	.L2193
	movq	-232(%rbp), %rax
	movq	-392(%rbp), %rdx
	movq	-464(%rbp), %rdi
	addq	%rsi, %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-456(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L2394
	movq	%r12, %rdi
	movq	%r8, -416(%rbp)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	-416(%rbp), %r8
	movq	%rax, -288(%rbp)
	testq	%rdi, %rdi
	je	.L2197
.L2196:
	movq	%r8, -416(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-288(%rbp), %rax
	movq	-416(%rbp), %r8
.L2197:
	testq	%rax, %rax
	je	.L2195
	movq	-408(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	movq	%r8, -416(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
	movq	-416(%rbp), %r8
.L2195:
	movq	-432(%rbp), %rdi
	movq	%r8, -416(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-416(%rbp), %r8
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	ja	.L2199
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	$0, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	testq	%rbx, %rbx
	je	.L2198
	movq	%r14, %rdi
	movq	%r8, -456(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r14), %rcx
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%rcx, -416(%rbp)
	call	memset@PLT
	movq	-456(%rbp), %r8
	movq	-416(%rbp), %rcx
.L2198:
	movq	%rcx, -344(%rbp)
	testq	%r13, %r13
	js	.L2199
	pxor	%xmm0, %xmm0
	movl	$0, %ebx
	movq	$0, -304(%rbp)
	movaps	%xmm0, -320(%rbp)
	je	.L2200
	movq	%r13, %rdi
	movq	%r8, -416(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r13), %rbx
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	movq	%rbx, -304(%rbp)
	call	memset@PLT
	movq	-416(%rbp), %r8
.L2200:
	movq	-392(%rbp), %rdi
	movq	%rbx, -312(%rbp)
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	ja	.L2199
	movq	$0, -272(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -288(%rbp)
	testq	%rdi, %rdi
	je	.L2262
	movq	-440(%rbp), %rbx
	movq	%r8, -392(%rbp)
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%rbx), %r13
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	movq	%r13, -272(%rbp)
	call	memset@PLT
	movq	-312(%rbp), %rbx
	movq	-392(%rbp), %r8
	movq	%rax, %rdi
.L2201:
	movq	-352(%rbp), %r11
	movq	-344(%rbp), %r10
	movq	%r13, -280(%rbp)
	subq	%r11, %r10
	movq	%r11, %rcx
	sarq	$3, %r10
	movq	%r10, %rax
	jne	.L2207
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2395:
	pxor	%xmm0, %xmm0
	addq	$8, %rcx
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, -8(%rcx)
	subq	$1, %rax
	je	.L2208
.L2207:
	testq	%rax, %rax
	jns	.L2395
	movq	%rax, %rdx
	movq	%rax, %rsi
	pxor	%xmm0, %xmm0
	addq	$8, %rcx
	shrq	%rdx
	andl	$1, %esi
	orq	%rsi, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -8(%rcx)
	subq	$1, %rax
	jne	.L2207
.L2208:
	movq	-320(%rbp), %rax
	movq	%rbx, %rcx
	xorl	%edx, %edx
	subq	%rax, %rcx
	je	.L2204
	.p2align 4,,10
	.p2align 3
.L2203:
	movl	%ecx, %esi
	subl	%edx, %esi
	movb	%sil, (%rax,%rdx)
	movq	-312(%rbp), %rbx
	addq	$1, %rdx
	movq	-320(%rbp), %rax
	movq	%rbx, %rsi
	subq	%rax, %rsi
	cmpq	%rdx, %rsi
	ja	.L2203
	movq	-352(%rbp), %r11
	movq	-344(%rbp), %r10
	movq	-288(%rbp), %rdi
	movq	-280(%rbp), %r13
	subq	%r11, %r10
	sarq	$3, %r10
.L2204:
	movq	%r13, %r9
	movq	%rdi, %rsi
	subq	%rdi, %r9
	sarq	$3, %r9
	movq	%r9, %rdx
	jne	.L2214
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2396:
	pxor	%xmm0, %xmm0
	addq	$8, %rsi
	cvtsi2sdq	%rdx, %xmm0
	movsd	%xmm0, -8(%rsi)
	subq	$1, %rdx
	je	.L2215
.L2214:
	testq	%rdx, %rdx
	jns	.L2396
	movq	%rdx, %rcx
	movq	%rdx, %r13
	pxor	%xmm0, %xmm0
	addq	$8, %rsi
	shrq	%rcx
	andl	$1, %r13d
	orq	%r13, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -8(%rsi)
	subq	$1, %rdx
	jne	.L2214
.L2215:
	testq	%r10, %r10
	je	.L2211
	movq	-184(%rbp), %rcx
	leaq	15(%rcx), %rdx
	subq	%r11, %rdx
	cmpq	$30, %rdx
	jbe	.L2265
	leaq	-1(%r10), %rdx
	cmpq	$3, %rdx
	jbe	.L2265
	movq	%r10, %rsi
	xorl	%edx, %edx
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2219:
	movupd	(%r11,%rdx), %xmm1
	movups	%xmm1, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L2219
	movq	%r10, %rdx
	andq	$-2, %rdx
	andl	$1, %r10d
	je	.L2211
	movsd	(%r11,%rdx,8), %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
.L2211:
	xorl	%edx, %edx
	cmpq	%rbx, %rax
	je	.L2217
	.p2align 4,,10
	.p2align 3
.L2216:
	movzbl	(%rax,%rdx), %ecx
	movq	-136(%rbp), %rax
	movb	%cl, (%rax,%rdx)
	movq	-320(%rbp), %rax
	addq	$1, %rdx
	movq	-312(%rbp), %rcx
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	ja	.L2216
	movq	-288(%rbp), %rdi
	movq	-280(%rbp), %r9
	subq	%rdi, %r9
	sarq	$3, %r9
.L2217:
	testq	%r9, %r9
	je	.L2228
	movq	-88(%rbp), %rdx
	leaq	15(%rdx), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	jbe	.L2266
	leaq	-1(%r9), %rax
	cmpq	$3, %rax
	jbe	.L2266
	movq	%r9, %rcx
	xorl	%eax, %eax
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2226:
	movupd	(%rdi,%rax), %xmm2
	movups	%xmm2, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L2226
	movq	%r9, %rax
	andq	$-2, %rax
	andl	$1, %r9d
	je	.L2228
	movsd	(%rdi,%rax,8), %xmm0
	movsd	%xmm0, (%rdx,%rax,8)
.L2228:
	leaq	-352(%rbp), %r13
	leaq	-208(%rbp), %r14
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r8, -392(%rbp)
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-432(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-424(%rbp), %rdx
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-400(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-408(%rbp), %rcx
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	-392(%rbp), %r8
	subq	%rdx, %rsi
	sarq	$3, %rsi
	je	.L2224
	movq	-184(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2268
	leaq	-1(%rsi), %rax
	cmpq	$3, %rax
	jbe	.L2268
	movq	%rsi, %rdi
	xorl	%eax, %eax
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2234:
	movupd	(%rdx,%rax), %xmm3
	movups	%xmm3, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L2234
	movq	%rsi, %rax
	andq	$-2, %rax
	andl	$1, %esi
	je	.L2224
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
.L2224:
	movq	-320(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, -312(%rbp)
	je	.L2232
	.p2align 4,,10
	.p2align 3
.L2231:
	movzbl	(%rcx,%rax), %ecx
	movq	-136(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-320(%rbp), %rcx
	addq	$1, %rax
	movq	-312(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L2231
.L2232:
	movq	-288(%rbp), %rax
	movq	-280(%rbp), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	je	.L2243
	movq	-88(%rbp), %rdx
	leaq	15(%rdx), %rsi
	subq	%rax, %rsi
	cmpq	$30, %rsi
	jbe	.L2325
	leaq	-1(%rcx), %rsi
	cmpq	$3, %rsi
	jbe	.L2325
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2241:
	movupd	(%rax,%r8), %xmm4
	movups	%xmm4, (%rdx,%r8)
	addq	$16, %r8
	cmpq	%r8, %rsi
	jne	.L2241
	movq	%rcx, %rsi
	andq	$-2, %rsi
	andl	$1, %ecx
	je	.L2243
	movsd	(%rax,%rsi,8), %xmm0
	movsd	%xmm0, (%rdx,%rsi,8)
.L2243:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-432(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-424(%rbp), %rdx
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-408(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	-400(%rbp), %rdx
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2239
	call	_ZdlPv@PLT
.L2239:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2246
	call	_ZdlPv@PLT
.L2246:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2247
	call	_ZdlPv@PLT
.L2247:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2248
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2248:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2249
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2249:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2250
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2250:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2251
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2251:
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-448(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2397
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2325:
	.cfi_restore_state
	movsd	(%rax,%r8,8), %xmm0
	movsd	%xmm0, (%rdx,%r8,8)
	addq	$1, %r8
	cmpq	%r8, %rcx
	jne	.L2325
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	-224(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L2178
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2192:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2262:
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2193:
	leaq	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2390:
	movq	-176(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L2184
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2388:
	leaq	_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2392:
	movq	$0, -288(%rbp)
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2189
	jmp	.L2188
.L2268:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2233:
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L2233
	jmp	.L2224
.L2266:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2225:
	movsd	(%rdi,%rax,8), %xmm0
	movsd	%xmm0, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L2225
	jmp	.L2228
.L2265:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2218:
	movsd	(%r11,%rdx,8), %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	addq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L2218
	jmp	.L2211
.L2394:
	movq	$0, -288(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2196
	jmp	.L2195
.L2391:
	leaq	_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2393:
	leaq	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2199:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9519:
	.size	_Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm, .-_Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm
	.text
	.align 2
	.p2align 4
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer2_Test8TestBodyEv
	.type	_ZN39AliasBufferTest_SharedArrayBuffer2_Test8TestBodyEv, @function
_ZN39AliasBufferTest_SharedArrayBuffer2_Test8TestBodyEv:
.LFB8592:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movl	$8, %ecx
	movl	$8, %edx
	movl	$100, %esi
	jmp	_Z16SharedBufferTestIdN2v812Float64ArrayEaNS0_9Int8ArrayEdS1_EvPNS0_7IsolateEmmm
	.cfi_endproc
.LFE8592:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer2_Test8TestBodyEv, .-_ZN39AliasBufferTest_SharedArrayBuffer2_Test8TestBodyEv
	.section	.text._Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm,"axG",@progbits,_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm,comdat
	.p2align 4
	.weak	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm
	.type	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm, @function
_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm:
.LFB9520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$408, %rsp
	movq	%rcx, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate5EnterEv@PLT
	leaq	-384(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, -392(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	leaq	0(,%rbx,8), %rax
	leaq	0(%r13,%r14), %rdx
	movq	%r12, -256(%rbp)
	movq	%rdx, -400(%rbp)
	addq	%rax, %rdx
	movq	%rax, -432(%rbp)
	popq	%rax
	movq	%rdx, -248(%rbp)
	popq	%rcx
	movq	$0, -240(%rbp)
	movq	$0, -224(%rbp)
	testq	%rdx, %rdx
	je	.L2593
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%rdx, -424(%rbp)
	leaq	-112(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-424(%rbp), %rdx
	movq	-256(%rbp), %rdi
	movq	%rdx, %rsi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movq	-240(%rbp), %rsi
	movq	%rbx, %rdi
	movq	-424(%rbp), %rdx
	movq	%rax, -232(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2594
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-224(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2404
.L2403:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-112(%rbp), %rax
	movq	$0, -224(%rbp)
.L2404:
	testq	%rax, %rax
	je	.L2402
	leaq	-224(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2402:
	movq	-416(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r12, -208(%rbp)
	movq	%r13, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -176(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2470
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2470:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r13
	ja	.L2410
	movq	-232(%rbp), %rax
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2595
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-176(%rbp), %rdi
	movq	%rax, -160(%rbp)
	testq	%rdi, %rdi
	je	.L2409
.L2408:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-160(%rbp), %rax
	movq	$0, -176(%rbp)
.L2409:
	testq	%rax, %rax
	je	.L2407
	movq	-416(%rbp), %rdi
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2407:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	-288(%rbp), %rax
	movq	%r13, %xmm3
	movq	%r12, %rsi
	movq	%r14, %xmm0
	movq	%rax, %rdi
	movq	%r12, -160(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movq	$0, -128(%rbp)
	movq	%rax, -424(%rbp)
	movups	%xmm0, -152(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2468
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2468:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	subq	%r13, %rax
	cmpq	%rax, %r14
	ja	.L2410
	movq	-232(%rbp), %rax
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	addq	%r13, %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2596
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2414
.L2413:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
.L2414:
	testq	%rax, %rax
	je	.L2412
	leaq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2412:
	movq	-424(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r12, -112(%rbp)
	movq	-408(%rbp), %xmm0
	movq	$0, -80(%rbp)
	movhps	-400(%rbp), %xmm0
	movups	%xmm0, -104(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2466
	movq	(%rdi), %rsi
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2466:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	-400(%rbp), %rbx
	movq	%rax, %rdi
	andl	$7, %ebx
	jne	.L2597
	movq	-408(%rbp), %rsi
	movabsq	$2305843009213693951, %rax
	andq	%rsi, %rax
	cmpq	%rax, %rsi
	jne	.L2598
	movq	%rdi, -448(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	subq	-400(%rbp), %rax
	cmpq	%rax, -432(%rbp)
	movq	-448(%rbp), %rdi
	ja	.L2599
	movq	-400(%rbp), %rsi
	movq	-232(%rbp), %rax
	movq	-408(%rbp), %rdx
	addq	%rsi, %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2600
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, -320(%rbp)
	testq	%rdi, %rdi
	je	.L2421
.L2420:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, -80(%rbp)
	movq	-320(%rbp), %rax
.L2421:
	testq	%rax, %rax
	je	.L2419
	leaq	-80(%rbp), %rsi
	leaq	-320(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2419:
	movq	-424(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r13, %r13
	js	.L2423
	pxor	%xmm0, %xmm0
	movl	$0, %ecx
	movq	$0, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	je	.L2422
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r13), %rcx
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%rcx, -400(%rbp)
	call	memset@PLT
	movq	-400(%rbp), %rcx
.L2422:
	movq	%rcx, -344(%rbp)
	testq	%r14, %r14
	js	.L2423
	pxor	%xmm0, %xmm0
	movl	$0, %r13d
	movq	$0, -304(%rbp)
	movaps	%xmm0, -320(%rbp)
	je	.L2424
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r14), %r13
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	movq	%r13, -304(%rbp)
	call	memset@PLT
.L2424:
	movq	-408(%rbp), %rsi
	movq	%r13, -312(%rbp)
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L2423
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movq	$0, -272(%rbp)
	movaps	%xmm0, -288(%rbp)
	testq	%rsi, %rsi
	je	.L2425
	movq	-432(%rbp), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r14), %r13
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	movq	%r13, -272(%rbp)
	call	memset@PLT
.L2425:
	movq	-344(%rbp), %rdi
	movq	-352(%rbp), %rdx
	movq	%r13, -280(%rbp)
	movq	%rdi, %rsi
	subq	%rdx, %rsi
	je	.L2426
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2427:
	movl	%esi, %ecx
	subl	%eax, %ecx
	movb	%cl, (%rdx,%rax)
	movq	-344(%rbp), %rdi
	addq	$1, %rax
	movq	-352(%rbp), %rdx
	movq	%rdi, %rcx
	subq	%rdx, %rcx
	cmpq	%rax, %rcx
	ja	.L2427
.L2426:
	movq	-312(%rbp), %rsi
	movq	-320(%rbp), %rax
	movq	%rsi, %r9
	subq	%rax, %r9
	je	.L2428
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2429:
	movl	%r9d, %ecx
	subl	%edx, %ecx
	movb	%cl, (%rax,%rdx)
	movq	-312(%rbp), %rsi
	addq	$1, %rdx
	movq	-320(%rbp), %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	ja	.L2429
	movq	-344(%rbp), %rdi
	movq	-352(%rbp), %rdx
.L2428:
	movq	-288(%rbp), %r10
	movq	-280(%rbp), %rcx
	subq	%r10, %rcx
	sarq	$3, %rcx
	jne	.L2435
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2601:
	pxor	%xmm0, %xmm0
	addq	$8, %r10
	cvtsi2sdq	%rcx, %xmm0
	movsd	%xmm0, -8(%r10)
	subq	$1, %rcx
	je	.L2436
.L2435:
	testq	%rcx, %rcx
	jns	.L2601
	movq	%rcx, %r8
	movq	%rcx, %r9
	pxor	%xmm0, %xmm0
	addq	$8, %r10
	shrq	%r8
	andl	$1, %r9d
	orq	%r9, %r8
	cvtsi2sdq	%r8, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -8(%r10)
	subq	$1, %rcx
	jne	.L2435
.L2436:
	xorl	%ecx, %ecx
	cmpq	%rdx, %rdi
	je	.L2432
	.p2align 4,,10
	.p2align 3
.L2431:
	movzbl	(%rdx,%rcx), %edx
	movq	-184(%rbp), %rax
	movb	%dl, (%rax,%rcx)
	movq	-352(%rbp), %rdx
	addq	$1, %rcx
	movq	-344(%rbp), %rax
	subq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L2431
	movq	-312(%rbp), %rsi
	movq	-320(%rbp), %rax
.L2432:
	xorl	%edx, %edx
	cmpq	%rax, %rsi
	je	.L2438
	.p2align 4,,10
	.p2align 3
.L2437:
	movzbl	(%rax,%rdx), %ecx
	movq	-136(%rbp), %rax
	movb	%cl, (%rax,%rdx)
	movq	-320(%rbp), %rax
	addq	$1, %rdx
	movq	-312(%rbp), %rcx
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	ja	.L2437
.L2438:
	movq	-288(%rbp), %rdx
	movq	-280(%rbp), %rsi
	subq	%rdx, %rsi
	sarq	$3, %rsi
	je	.L2445
	movq	-88(%rbp), %rcx
	leaq	15(%rcx), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L2479
	leaq	-1(%rsi), %rax
	cmpq	$3, %rax
	jbe	.L2479
	movq	%rsi, %rdi
	xorl	%eax, %eax
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L2443:
	movupd	(%rdx,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L2443
	movq	%rsi, %rax
	andq	$-2, %rax
	andl	$1, %esi
	je	.L2445
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
.L2445:
	leaq	-208(%rbp), %r8
	movq	-392(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-352(%rbp), %r13
	movq	%r8, %rdx
	movq	%r13, %rcx
	movq	%r8, -400(%rbp)
	leaq	-320(%rbp), %r14
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-416(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	-392(%rbp), %rsi
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-424(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	-392(%rbp), %rsi
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-352(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, -344(%rbp)
	movq	-400(%rbp), %r8
	je	.L2441
	.p2align 4,,10
	.p2align 3
.L2440:
	movzbl	(%rcx,%rax), %ecx
	movq	-184(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-352(%rbp), %rcx
	addq	$1, %rax
	movq	-344(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L2440
.L2441:
	movq	-320(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	-312(%rbp), %rcx
	je	.L2449
	.p2align 4,,10
	.p2align 3
.L2448:
	movzbl	(%rcx,%rax), %ecx
	movq	-136(%rbp), %rdx
	movb	%cl, (%rdx,%rax)
	movq	-320(%rbp), %rcx
	addq	$1, %rax
	movq	-312(%rbp), %rdx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	ja	.L2448
.L2449:
	movq	-288(%rbp), %rax
	movq	-280(%rbp), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	je	.L2456
	movq	-88(%rbp), %rdx
	leaq	15(%rdx), %rsi
	subq	%rax, %rsi
	cmpq	$30, %rsi
	jbe	.L2533
	leaq	-1(%rcx), %rsi
	cmpq	$3, %rsi
	jbe	.L2533
	movq	%rcx, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2454:
	movupd	(%rax,%rbx), %xmm2
	movups	%xmm2, (%rdx,%rbx)
	addq	$16, %rbx
	cmpq	%rbx, %rsi
	jne	.L2454
	movq	%rcx, %rsi
	andq	$-2, %rsi
	andl	$1, %ecx
	je	.L2456
	movsd	(%rax,%rsi,8), %xmm0
	movsd	%xmm0, (%rdx,%rsi,8)
.L2456:
	movq	-392(%rbp), %rbx
	movq	%r8, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-416(%rbp), %rdx
	call	_Z15ReadAndValidateIaN2v89Int8ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-424(%rbp), %rcx
	call	_Z15ReadAndValidateIdN2v812Float64ArrayEEvPNS0_7IsolateENS0_5LocalINS0_7ContextEEEPN4node17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS9_E5valueEvE4typeEEERKSt6vectorIS9_SaIS9_EE
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2452
	call	_ZdlPv@PLT
.L2452:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2459
	call	_ZdlPv@PLT
.L2459:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2460
	call	_ZdlPv@PLT
.L2460:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2461
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2461:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2462
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2462:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2463
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2463:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2464
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2464:
	movq	-392(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-440(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2602
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2533:
	.cfi_restore_state
	movsd	(%rax,%rbx,8), %xmm0
	movsd	%xmm0, (%rdx,%rbx,8)
	addq	$1, %rbx
	cmpq	%rbx, %rcx
	jne	.L2533
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2410:
	leaq	_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2594:
	movq	-224(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L2403
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2595:
	movq	-176(%rbp), %rdi
	movq	$0, -160(%rbp)
	testq	%rdi, %rdi
	jne	.L2408
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2596:
	movq	-128(%rbp), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L2413
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2593:
	leaq	_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2479:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2442:
	movsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L2442
	jmp	.L2445
.L2600:
	movq	$0, -320(%rbp)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2420
	jmp	.L2419
.L2597:
	leaq	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2598:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2599:
	leaq	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2423:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2602:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9520:
	.size	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm, .-_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm
	.text
	.align 2
	.p2align 4
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer3_Test8TestBodyEv
	.type	_ZN39AliasBufferTest_SharedArrayBuffer3_Test8TestBodyEv, @function
_ZN39AliasBufferTest_SharedArrayBuffer3_Test8TestBodyEv:
.LFB8599:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movl	$8, %ecx
	movl	$7, %edx
	movl	$1, %esi
	jmp	_Z16SharedBufferTestIaN2v89Int8ArrayEaS1_dNS0_12Float64ArrayEEvPNS0_7IsolateEmmm
	.cfi_endproc
.LFE8599:
	.size	_ZN39AliasBufferTest_SharedArrayBuffer3_Test8TestBodyEv, .-_ZN39AliasBufferTest_SharedArrayBuffer3_Test8TestBodyEv
	.section	.text._ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_,"axG",@progbits,_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_,comdat
	.p2align 4
	.weak	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	.type	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_, @function
_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_:
.LFB11027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-432(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -600(%rbp)
	movq	.LC24(%rip), %xmm1
	movq	%r8, -560(%rbp)
	movq	%rdi, -552(%rbp)
	movhps	.LC25(%rip), %xmm1
	movq	%r12, %rdi
	movq	%rsi, -584(%rbp)
	movq	%rdx, -592(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -544(%rbp)
	movq	%r15, -616(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-424(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%r13), %rdi
	movq	%r13, -432(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-544(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -520(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-560(%rbp), %r8
	movq	%r14, %rdi
	movq	(%r8), %rax
	movq	8(%r8), %rdx
	movq	24(%rax), %rax
	movl	(%rax,%rdx,4), %esi
	call	_ZNSo9_M_insertIxEERSoT_@PLT
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movq	%rax, -608(%rbp)
	movq	%rax, -480(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -560(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L2605
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L2617
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2607:
	movq	.LC24(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC26(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -576(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L2608
	call	_ZdlPv@PLT
.L2608:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rdx, -448(%rbp,%rax)
	movq	-616(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	xorl	%esi, %esi
	addq	%r14, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-544(%rbp), %xmm3
	movq	-520(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm3, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -424(%rbp)
	movq	-528(%rbp), %rax
	leaq	-512(%rbp), %r15
	movl	$24, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-600(%rbp), %rax
	movq	%r14, %rdi
	leaq	-496(%rbp), %r14
	movl	(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rax, %rax
	je	.L2609
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L2610
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2611:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-576(%rbp), %xmm4
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	movaps	%xmm4, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L2612
	call	_ZdlPv@PLT
.L2612:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-24(%r13), %rax
	movq	%r13, -432(%rbp)
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-552(%rbp), %rdi
	xorl	%r9d, %r9d
	movq	-560(%rbp), %r8
	movq	-592(%rbp), %rdx
	movq	-584(%rbp), %rsi
	movq	%r15, %rcx
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2613
	call	_ZdlPv@PLT
.L2613:
	movq	-480(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L2604
	call	_ZdlPv@PLT
.L2604:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2618
	movq	-552(%rbp), %rax
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2617:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L2610:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2609:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2611
	.p2align 4,,10
	.p2align 3
.L2605:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2607
.L2618:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11027:
	.size	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_, .-_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB11074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L2620
	testq	%rsi, %rsi
	je	.L2636
.L2620:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L2637
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L2623
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L2624:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2638
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2623:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L2624
	jmp	.L2622
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L2622:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L2624
.L2636:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11074:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.1
.LC27:
	.string	"Uint8Array"
.LC28:
	.string	"AliasBufferTest"
.LC29:
	.string	"Int8Array"
.LC30:
	.string	"Uint16Array"
.LC31:
	.string	"Int16Array"
.LC32:
	.string	"Uint32Array"
.LC33:
	.string	"Int32Array"
.LC34:
	.string	"Float32Array"
.LC35:
	.string	"Float64Array"
.LC36:
	.string	"SharedArrayBuffer1"
.LC37:
	.string	"SharedArrayBuffer2"
.LC38:
	.string	"SharedArrayBuffer3"
.LC39:
	.string	"SharedArrayBuffer4"
.LC40:
	.string	"OperatorOverloads"
.LC41:
	.string	"OperatorOverloadsRefs"
	.section	.text.startup
	.p2align 4
	.type	_Z41__static_initialization_and_destruction_0ii.constprop.0, @function
_Z41__static_initialization_and_destruction_0ii.constprop.0:
.LFB12945:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2698
.L2640:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2699
.L2641:
	leaq	-128(%rbp), %r15
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	leaq	_ZN15NodeTestFixture16TearDownTestCaseEv(%rip), %r14
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC27(%rip), %rsi
	pushq	%r13
	leaq	_ZN15NodeTestFixture13SetUpTestCaseEv(%rip), %r13
	pushq	%r14
	pushq	%r13
	movl	$151, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN31AliasBufferTest_Uint8Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2642
	call	_ZdlPv@PLT
.L2642:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L2643
	call	_ZdlPv@PLT
.L2643:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2700
.L2644:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2701
.L2645:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC29(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$155, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN30AliasBufferTest_Int8Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2646
	call	_ZdlPv@PLT
.L2646:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2647
	call	_ZdlPv@PLT
.L2647:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2702
.L2648:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2703
.L2649:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC30(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$159, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN32AliasBufferTest_Uint16Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2650
	call	_ZdlPv@PLT
.L2650:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2651
	call	_ZdlPv@PLT
.L2651:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2704
.L2652:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2705
.L2653:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC31(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$163, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN31AliasBufferTest_Int16Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2654
	call	_ZdlPv@PLT
.L2654:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2655
	call	_ZdlPv@PLT
.L2655:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2706
.L2656:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2707
.L2657:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC32(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$167, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN32AliasBufferTest_Uint32Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2658
	call	_ZdlPv@PLT
.L2658:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2659
	call	_ZdlPv@PLT
.L2659:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2708
.L2660:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2709
.L2661:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r11
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC33(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$171, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN31AliasBufferTest_Int32Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2662
	call	_ZdlPv@PLT
.L2662:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2663
	call	_ZdlPv@PLT
.L2663:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2710
.L2664:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2711
.L2665:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r10
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC34(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$175, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN33AliasBufferTest_Float32Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2666
	call	_ZdlPv@PLT
.L2666:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2667
	call	_ZdlPv@PLT
.L2667:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2712
.L2668:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2713
.L2669:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r9
	movq	%r12, %r8
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	leaq	.LC35(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$179, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN33AliasBufferTest_Float64Array_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2670
	call	_ZdlPv@PLT
.L2670:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2671
	call	_ZdlPv@PLT
.L2671:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2714
.L2672:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2715
.L2673:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%r8
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC36(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$183, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN39AliasBufferTest_SharedArrayBuffer1_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2674
	call	_ZdlPv@PLT
.L2674:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2675
	call	_ZdlPv@PLT
.L2675:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2716
.L2676:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2717
.L2677:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rdi
	movq	%r12, %r8
	leaq	.LC28(%rip), %rdi
	leaq	.LC37(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$190, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN39AliasBufferTest_SharedArrayBuffer2_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2678
	call	_ZdlPv@PLT
.L2678:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2679
	call	_ZdlPv@PLT
.L2679:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2718
.L2680:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2719
.L2681:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rsi
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC38(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$197, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN39AliasBufferTest_SharedArrayBuffer3_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2682
	call	_ZdlPv@PLT
.L2682:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2683
	call	_ZdlPv@PLT
.L2683:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2720
.L2684:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2721
.L2685:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	xorl	%edx, %edx
	pushq	%rcx
	movq	%r12, %r8
	xorl	%ecx, %ecx
	leaq	.LC39(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$204, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN39AliasBufferTest_SharedArrayBuffer4_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2686
	call	_ZdlPv@PLT
.L2686:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2687
	call	_ZdlPv@PLT
.L2687:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2722
.L2688:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2723
.L2689:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	xorl	%ecx, %ecx
	pushq	%rdx
	movq	%r12, %r8
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$211, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN38AliasBufferTest_OperatorOverloads_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2690
	call	_ZdlPv@PLT
.L2690:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2691
	call	_ZdlPv@PLT
.L2691:
	movl	$8, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestEE(%rip), %rcx
	movl	$1, %edi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2724
.L2692:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L2725
.L2693:
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	leaq	.LC28(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	leaq	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E(%rip), %r9
	movq	%r12, %r8
	leaq	.LC41(%rip), %rsi
	pushq	-144(%rbp)
	pushq	%r14
	pushq	%r13
	movl	$225, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN42AliasBufferTest_OperatorOverloadsRefs_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L2694
	call	_ZdlPv@PLT
.L2694:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L2639
	call	_ZdlPv@PLT
.L2639:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2726
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2701:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2645
.L2700:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2644
.L2699:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2641
.L2703:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2649
.L2702:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2648
.L2705:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2653
.L2704:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2652
.L2707:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2657
.L2706:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2656
.L2709:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2661
.L2708:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2660
.L2719:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2681
.L2718:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2680
.L2721:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2685
.L2720:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2684
.L2723:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2689
.L2722:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2688
.L2725:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2693
.L2724:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2692
.L2711:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2665
.L2710:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2664
.L2713:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2669
.L2712:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2668
.L2715:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2673
.L2714:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2672
.L2717:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L2677
.L2716:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2676
.L2698:
	call	_ZN7testing8internal16SuiteApiResolverI15AliasBufferTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L2640
.L2726:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12945:
	.size	_Z41__static_initialization_and_destruction_0ii.constprop.0, .-_Z41__static_initialization_and_destruction_0ii.constprop.0
	.section	.text._ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,"axG",@progbits,_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_,comdat
	.p2align 4
	.weak	_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.type	_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, @function
_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_:
.LFB12073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-448(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -480(%rbp)
	movq	.LC24(%rip), %xmm1
	movhps	.LC25(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-24(%rbx), %rdx
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rax, -448(%rbp,%rdx)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r13, %rdi
	leaq	-368(%rbp), %r13
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	-432(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rdi
	movq	%r8, -472(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movdqa	-464(%rbp), %xmm1
	movq	-24(%rax), %rdx
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp,%rdx)
	leaq	80(%r15), %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	movq	%r15, -448(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-480(%rbp), %rax
	movq	-472(%rbp), %r8
	movl	(%rax), %esi
	movq	%r8, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	leaq	16(%r14), %rax
	movb	$0, 16(%r14)
	movq	%rax, (%r14)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r14)
	testq	%rax, %rax
	je	.L2728
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L2734
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2730:
	movq	.LC24(%rip), %xmm0
	leaq	104+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, -448(%rbp)
	movq	%rax, -320(%rbp)
	movq	-352(%rbp), %rdi
	movhps	.LC26(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L2731
	call	_ZdlPv@PLT
.L2731:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2735
	addq	$440, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2734:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2728:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2730
.L2735:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12073:
	.size	_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_, .-_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	.section	.rodata.str1.1
.LC42:
	.string	""
.LC44:
	.string	"ab[0] = 1"
.LC45:
	.string	"static_cast<uint32_t>(1)"
.LC46:
	.string	"ab[0] += 3"
.LC47:
	.string	"static_cast<uint32_t>(4)"
.LC48:
	.string	"ab[0] -= 2"
.LC49:
	.string	"static_cast<uint32_t>(2)"
.LC50:
	.string	"-ab[0]"
.LC51:
	.string	"static_cast<uint32_t>(-2)"
	.text
	.align 2
	.p2align 4
	.globl	_ZN38AliasBufferTest_OperatorOverloads_Test8TestBodyEv
	.type	_ZN38AliasBufferTest_OperatorOverloads_Test8TestBodyEv, @function
_ZN38AliasBufferTest_OperatorOverloads_Test8TestBodyEv:
.LFB8613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-272(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-304(%rbp), %rbx
	subq	$296, %rsp
	movq	16(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	movq	16(%r12), %rdi
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %r9
	movdqa	.LC43(%rip), %xmm0
	movq	%r13, %rdi
	movq	$0, -208(%rbp)
	leaq	-192(%rbp), %r12
	movq	%r9, %rsi
	movq	%r9, -240(%rbp)
	movq	%r9, -336(%rbp)
	movups	%xmm0, -232(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-240(%rbp), %rdi
	movl	$40, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -328(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-328(%rbp), %r8
	movl	$10, %edx
	movq	-192(%rbp), %rax
	movq	-224(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	movq	-336(%rbp), %r9
	testq	%rax, %rax
	popq	%rcx
	je	.L2821
	movq	%r9, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-208(%rbp), %rdi
	movq	%rax, -192(%rbp)
	testq	%rdi, %rdi
	je	.L2740
.L2739:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-192(%rbp), %rax
	movq	$0, -208(%rbp)
.L2740:
	testq	%rax, %rax
	je	.L2738
	leaq	-208(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2738:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	-240(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -328(%rbp)
	movq	-216(%rbp), %rax
	movl	$1, (%rax)
	movl	$1, -312(%rbp)
	cmpl	$1, (%rax)
	je	.L2822
	leaq	-312(%rbp), %rcx
	movq	%r12, %r8
	movq	%r13, %rdi
	leaq	.LC44(%rip), %rdx
	leaq	.LC45(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	cmpb	$0, -272(%rbp)
	je	.L2823
.L2742:
	movq	-264(%rbp), %r8
	testq	%r8, %r8
	je	.L2745
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L2746
	movq	%r8, -336(%rbp)
	call	_ZdlPv@PLT
	movq	-336(%rbp), %r8
.L2746:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2745:
	movq	-328(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-216(%rbp), %rax
	addl	$3, (%rax)
	movl	$4, -312(%rbp)
	cmpl	$4, (%rax)
	je	.L2824
	leaq	-312(%rbp), %rcx
	movq	%r12, %r8
	movq	%r13, %rdi
	leaq	.LC46(%rip), %rdx
	leaq	.LC47(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	cmpb	$0, -272(%rbp)
	je	.L2825
.L2749:
	movq	-264(%rbp), %r8
	testq	%r8, %r8
	je	.L2752
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L2753
	movq	%r8, -336(%rbp)
	call	_ZdlPv@PLT
	movq	-336(%rbp), %r8
.L2753:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2752:
	movq	-328(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-216(%rbp), %rax
	subl	$2, (%rax)
	movl	$2, -312(%rbp)
	cmpl	$2, (%rax)
	je	.L2826
	leaq	-312(%rbp), %rcx
	movq	%r12, %r8
	movq	%r13, %rdi
	leaq	.LC48(%rip), %rdx
	leaq	.LC49(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	cmpb	$0, -272(%rbp)
	je	.L2827
.L2756:
	movq	-264(%rbp), %r8
	testq	%r8, %r8
	je	.L2759
	movq	(%r8), %rdi
	leaq	16(%r8), %rax
	cmpq	%rax, %rdi
	je	.L2760
	movq	%r8, -328(%rbp)
	call	_ZdlPv@PLT
	movq	-328(%rbp), %r8
.L2760:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2759:
	movq	-216(%rbp), %rax
	movl	(%rax), %eax
	movl	$-2, -312(%rbp)
	negl	%eax
	movl	%eax, -272(%rbp)
	cmpl	$-2, %eax
	je	.L2828
	leaq	-96(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%r8, -336(%rbp)
	call	_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-128(%rbp), %rcx
	leaq	-312(%rbp), %rsi
	movq	%rcx, %rdi
	movq	%rcx, -328(%rbp)
	call	_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	-336(%rbp), %r8
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	-328(%rbp), %rcx
	leaq	.LC50(%rip), %rdx
	leaq	.LC51(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2763
	call	_ZdlPv@PLT
.L2763:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2762
	call	_ZdlPv@PLT
.L2762:
	cmpb	$0, -192(%rbp)
	je	.L2829
.L2765:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L2768
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2769
	call	_ZdlPv@PLT
.L2769:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2768:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2770
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2770:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2830
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2822:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -272(%rbp)
	jne	.L2742
.L2823:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-264(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2743
	movq	(%rax), %r8
.L2743:
	leaq	-312(%rbp), %rdi
	movl	$219, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%rdi, -336(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-336(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-336(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2742
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2828:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -192(%rbp)
	jne	.L2765
.L2829:
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-184(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2766
	movq	(%rax), %r8
.L2766:
	leaq	-312(%rbp), %r12
	movl	$222, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2765
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2826:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -272(%rbp)
	jne	.L2756
.L2827:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-264(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2757
	movq	(%rax), %r8
.L2757:
	leaq	-312(%rbp), %rdi
	movl	$221, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%rdi, -328(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-328(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-328(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2756
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2824:
	movq	%r13, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -272(%rbp)
	jne	.L2749
.L2825:
	movq	%r12, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-264(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2750
	movq	(%rax), %r8
.L2750:
	leaq	-312(%rbp), %rdi
	movl	$220, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%rdi, -336(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-336(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-336(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2749
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2821:
	movq	-208(%rbp), %rdi
	movq	$0, -192(%rbp)
	testq	%rdi, %rdi
	jne	.L2739
	jmp	.L2738
.L2830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8613:
	.size	_ZN38AliasBufferTest_OperatorOverloads_Test8TestBodyEv, .-_ZN38AliasBufferTest_OperatorOverloads_Test8TestBodyEv
	.section	.rodata.str1.1
.LC53:
	.string	"ref = ref_value"
.LC54:
	.string	"ref += ref_value"
.LC55:
	.string	"ref -= ref_value"
.LC56:
	.string	"-ref"
	.text
	.align 2
	.p2align 4
	.globl	_ZN42AliasBufferTest_OperatorOverloadsRefs_Test8TestBodyEv
	.type	_ZN42AliasBufferTest_OperatorOverloadsRefs_Test8TestBodyEv, @function
_ZN42AliasBufferTest_OperatorOverloadsRefs_Test8TestBodyEv:
.LFB8620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-304(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	16(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	16(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%rbx), %r13
	movdqa	.LC52(%rip), %xmm0
	leaq	-272(%rbp), %rbx
	movq	$0, -208(%rbp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movups	%xmm0, -232(%rbp)
	movq	%r13, -240(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-240(%rbp), %rdi
	movl	$8, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -328(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-328(%rbp), %r8
	movl	$2, %edx
	movq	-192(%rbp), %rax
	movq	-224(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L2916
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-208(%rbp), %rdi
	movq	%rax, -192(%rbp)
	testq	%rdi, %rdi
	je	.L2835
.L2834:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-192(%rbp), %rax
	movq	$0, -208(%rbp)
.L2835:
	testq	%rax, %rax
	je	.L2833
	leaq	-208(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2833:
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	-240(%rbp), %rax
	movabsq	$8589934594, %rcx
	movq	$0, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	-216(%rbp), %rax
	movq	%rcx, (%rax)
	movl	$2, -312(%rbp)
	cmpl	$2, (%rax)
	je	.L2917
	leaq	-312(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	.LC53(%rip), %rdx
	leaq	.LC49(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	cmpb	$0, -192(%rbp)
	je	.L2918
.L2837:
	movq	-184(%rbp), %r13
	testq	%r13, %r13
	je	.L2840
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2841
	call	_ZdlPv@PLT
.L2841:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2840:
	movq	-272(%rbp), %rax
	movq	-264(%rbp), %rdx
	movq	24(%rax), %rax
	leaq	(%rax,%rdx,4), %rax
	movq	-216(%rbp), %rdx
	movl	4(%rdx), %edx
	addl	%edx, (%rax)
	movl	$4, -312(%rbp)
	cmpl	$4, (%rax)
	je	.L2919
	leaq	-312(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	.LC54(%rip), %rdx
	leaq	.LC47(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	cmpb	$0, -192(%rbp)
	je	.L2920
.L2844:
	movq	-184(%rbp), %r13
	testq	%r13, %r13
	je	.L2847
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2848
	call	_ZdlPv@PLT
.L2848:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2847:
	movq	-272(%rbp), %rax
	movq	-264(%rbp), %rdx
	movq	24(%rax), %rax
	leaq	(%rax,%rdx,4), %rax
	movq	-216(%rbp), %rdx
	movl	4(%rdx), %edx
	subl	%edx, (%rax)
	movl	$2, -312(%rbp)
	cmpl	$2, (%rax)
	je	.L2921
	leaq	-312(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	.LC55(%rip), %rdx
	leaq	.LC49(%rip), %rsi
	call	_ZN7testing8internal18CmpHelperEQFailureIjN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvE9ReferenceEEENS_15AssertionResultEPKcSA_RKT_RKT0_
	cmpb	$0, -192(%rbp)
	je	.L2922
.L2851:
	movq	-184(%rbp), %r13
	testq	%r13, %r13
	je	.L2854
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2855
	call	_ZdlPv@PLT
.L2855:
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2854:
	movq	-272(%rbp), %rax
	movq	-264(%rbp), %rdx
	movq	24(%rax), %rax
	movl	(%rax,%rdx,4), %eax
	movl	$-2, -320(%rbp)
	negl	%eax
	movl	%eax, -312(%rbp)
	cmpl	$-2, %eax
	je	.L2923
	leaq	-96(%rbp), %rbx
	leaq	-312(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r13
	call	_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	leaq	-320(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN7testing13PrintToStringIjEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT_
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	leaq	.LC56(%rip), %rdx
	leaq	.LC51(%rip), %rsi
	call	_ZN7testing8internal9EqFailureEPKcS2_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_b@PLT
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2858
	call	_ZdlPv@PLT
.L2858:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2857
	call	_ZdlPv@PLT
.L2857:
	cmpb	$0, -192(%rbp)
	je	.L2924
.L2860:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L2863
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2864
	call	_ZdlPv@PLT
.L2864:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2863:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2865
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2865:
	movq	-336(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2925
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2917:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -192(%rbp)
	jne	.L2837
.L2918:
	leaq	-312(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-184(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2838
	movq	(%rax), %r8
.L2838:
	leaq	-320(%rbp), %rdi
	movl	$235, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%rdi, -328(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-328(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-328(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2837
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2837
	.p2align 4,,10
	.p2align 3
.L2923:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -192(%rbp)
	jne	.L2860
.L2924:
	leaq	-312(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-184(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2861
	movq	(%rax), %r8
.L2861:
	leaq	-320(%rbp), %r12
	movl	$238, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2860
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2860
	.p2align 4,,10
	.p2align 3
.L2921:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -192(%rbp)
	jne	.L2851
.L2922:
	leaq	-312(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-184(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2852
	movq	(%rax), %r8
.L2852:
	leaq	-320(%rbp), %rbx
	movl	$237, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	%rbx, %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2851
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L2919:
	movq	%r12, %rdi
	call	_ZN7testing16AssertionSuccessEv@PLT
	cmpb	$0, -192(%rbp)
	jne	.L2844
.L2920:
	leaq	-312(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN7testing7MessageC1Ev@PLT
	movq	-184(%rbp), %rax
	leaq	.LC42(%rip), %r8
	testq	%rax, %rax
	je	.L2845
	movq	(%rax), %r8
.L2845:
	leaq	-320(%rbp), %rdi
	movl	$236, %ecx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	movq	%rdi, -328(%rbp)
	call	_ZN7testing8internal12AssertHelperC1ENS_14TestPartResult4TypeEPKciS5_@PLT
	movq	-328(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK7testing8internal12AssertHelperaSERKNS_7MessageE@PLT
	movq	-328(%rbp), %rdi
	call	_ZN7testing8internal12AssertHelperD1Ev@PLT
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2844
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2916:
	movq	-208(%rbp), %rdi
	movq	$0, -192(%rbp)
	testq	%rdi, %rdi
	jne	.L2834
	jmp	.L2833
.L2925:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8620:
	.size	_ZN42AliasBufferTest_OperatorOverloadsRefs_Test8TestBodyEv, .-_ZN42AliasBufferTest_OperatorOverloadsRefs_Test8TestBodyEv
	.section	.text.startup
	.p2align 4
	.type	_GLOBAL__sub_I__ZN31AliasBufferTest_Uint8Array_Test10test_info_E, @function
_GLOBAL__sub_I__ZN31AliasBufferTest_Uint8Array_Test10test_info_E:
.LFB12632:
	.cfi_startproc
	endbr64
	jmp	_Z41__static_initialization_and_destruction_0ii.constprop.0
	.cfi_endproc
.LFE12632:
	.size	_GLOBAL__sub_I__ZN31AliasBufferTest_Uint8Array_Test10test_info_E, .-_GLOBAL__sub_I__ZN31AliasBufferTest_Uint8Array_Test10test_info_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN31AliasBufferTest_Uint8Array_Test10test_info_E
	.weak	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E
	.section	.bss._ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E,"awG",@nobits,_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E,comdat
	.type	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E, @gnu_unique_object
	.size	_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E, 1
_ZN7testing8internal12TypeIdHelperI15AliasBufferTestE6dummy_E:
	.zero	1
	.weak	_ZTV15NodeTestFixture
	.section	.data.rel.ro._ZTV15NodeTestFixture,"awG",@progbits,_ZTV15NodeTestFixture,comdat
	.align 8
	.type	_ZTV15NodeTestFixture, @object
	.size	_ZTV15NodeTestFixture, 64
_ZTV15NodeTestFixture:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Uint8Array_TestE10CreateTestEv
	.weak	_ZTV31AliasBufferTest_Uint8Array_Test
	.section	.data.rel.ro.local._ZTV31AliasBufferTest_Uint8Array_Test,"awG",@progbits,_ZTV31AliasBufferTest_Uint8Array_Test,comdat
	.align 8
	.type	_ZTV31AliasBufferTest_Uint8Array_Test, @object
	.size	_ZTV31AliasBufferTest_Uint8Array_Test, 64
_ZTV31AliasBufferTest_Uint8Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN31AliasBufferTest_Uint8Array_TestD1Ev
	.quad	_ZN31AliasBufferTest_Uint8Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN31AliasBufferTest_Uint8Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI30AliasBufferTest_Int8Array_TestE10CreateTestEv
	.weak	_ZTV30AliasBufferTest_Int8Array_Test
	.section	.data.rel.ro.local._ZTV30AliasBufferTest_Int8Array_Test,"awG",@progbits,_ZTV30AliasBufferTest_Int8Array_Test,comdat
	.align 8
	.type	_ZTV30AliasBufferTest_Int8Array_Test, @object
	.size	_ZTV30AliasBufferTest_Int8Array_Test, 64
_ZTV30AliasBufferTest_Int8Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN30AliasBufferTest_Int8Array_TestD1Ev
	.quad	_ZN30AliasBufferTest_Int8Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN30AliasBufferTest_Int8Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint16Array_TestE10CreateTestEv
	.weak	_ZTV32AliasBufferTest_Uint16Array_Test
	.section	.data.rel.ro.local._ZTV32AliasBufferTest_Uint16Array_Test,"awG",@progbits,_ZTV32AliasBufferTest_Uint16Array_Test,comdat
	.align 8
	.type	_ZTV32AliasBufferTest_Uint16Array_Test, @object
	.size	_ZTV32AliasBufferTest_Uint16Array_Test, 64
_ZTV32AliasBufferTest_Uint16Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN32AliasBufferTest_Uint16Array_TestD1Ev
	.quad	_ZN32AliasBufferTest_Uint16Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN32AliasBufferTest_Uint16Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int16Array_TestE10CreateTestEv
	.weak	_ZTV31AliasBufferTest_Int16Array_Test
	.section	.data.rel.ro.local._ZTV31AliasBufferTest_Int16Array_Test,"awG",@progbits,_ZTV31AliasBufferTest_Int16Array_Test,comdat
	.align 8
	.type	_ZTV31AliasBufferTest_Int16Array_Test, @object
	.size	_ZTV31AliasBufferTest_Int16Array_Test, 64
_ZTV31AliasBufferTest_Int16Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN31AliasBufferTest_Int16Array_TestD1Ev
	.quad	_ZN31AliasBufferTest_Int16Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN31AliasBufferTest_Int16Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI32AliasBufferTest_Uint32Array_TestE10CreateTestEv
	.weak	_ZTV32AliasBufferTest_Uint32Array_Test
	.section	.data.rel.ro.local._ZTV32AliasBufferTest_Uint32Array_Test,"awG",@progbits,_ZTV32AliasBufferTest_Uint32Array_Test,comdat
	.align 8
	.type	_ZTV32AliasBufferTest_Uint32Array_Test, @object
	.size	_ZTV32AliasBufferTest_Uint32Array_Test, 64
_ZTV32AliasBufferTest_Uint32Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN32AliasBufferTest_Uint32Array_TestD1Ev
	.quad	_ZN32AliasBufferTest_Uint32Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN32AliasBufferTest_Uint32Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI31AliasBufferTest_Int32Array_TestE10CreateTestEv
	.weak	_ZTV31AliasBufferTest_Int32Array_Test
	.section	.data.rel.ro.local._ZTV31AliasBufferTest_Int32Array_Test,"awG",@progbits,_ZTV31AliasBufferTest_Int32Array_Test,comdat
	.align 8
	.type	_ZTV31AliasBufferTest_Int32Array_Test, @object
	.size	_ZTV31AliasBufferTest_Int32Array_Test, 64
_ZTV31AliasBufferTest_Int32Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN31AliasBufferTest_Int32Array_TestD1Ev
	.quad	_ZN31AliasBufferTest_Int32Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN31AliasBufferTest_Int32Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float32Array_TestE10CreateTestEv
	.weak	_ZTV33AliasBufferTest_Float32Array_Test
	.section	.data.rel.ro.local._ZTV33AliasBufferTest_Float32Array_Test,"awG",@progbits,_ZTV33AliasBufferTest_Float32Array_Test,comdat
	.align 8
	.type	_ZTV33AliasBufferTest_Float32Array_Test, @object
	.size	_ZTV33AliasBufferTest_Float32Array_Test, 64
_ZTV33AliasBufferTest_Float32Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN33AliasBufferTest_Float32Array_TestD1Ev
	.quad	_ZN33AliasBufferTest_Float32Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN33AliasBufferTest_Float32Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33AliasBufferTest_Float64Array_TestE10CreateTestEv
	.weak	_ZTV33AliasBufferTest_Float64Array_Test
	.section	.data.rel.ro.local._ZTV33AliasBufferTest_Float64Array_Test,"awG",@progbits,_ZTV33AliasBufferTest_Float64Array_Test,comdat
	.align 8
	.type	_ZTV33AliasBufferTest_Float64Array_Test, @object
	.size	_ZTV33AliasBufferTest_Float64Array_Test, 64
_ZTV33AliasBufferTest_Float64Array_Test:
	.quad	0
	.quad	0
	.quad	_ZN33AliasBufferTest_Float64Array_TestD1Ev
	.quad	_ZN33AliasBufferTest_Float64Array_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN33AliasBufferTest_Float64Array_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer1_TestE10CreateTestEv
	.weak	_ZTV39AliasBufferTest_SharedArrayBuffer1_Test
	.section	.data.rel.ro.local._ZTV39AliasBufferTest_SharedArrayBuffer1_Test,"awG",@progbits,_ZTV39AliasBufferTest_SharedArrayBuffer1_Test,comdat
	.align 8
	.type	_ZTV39AliasBufferTest_SharedArrayBuffer1_Test, @object
	.size	_ZTV39AliasBufferTest_SharedArrayBuffer1_Test, 64
_ZTV39AliasBufferTest_SharedArrayBuffer1_Test:
	.quad	0
	.quad	0
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD1Ev
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer1_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer1_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer2_TestE10CreateTestEv
	.weak	_ZTV39AliasBufferTest_SharedArrayBuffer2_Test
	.section	.data.rel.ro.local._ZTV39AliasBufferTest_SharedArrayBuffer2_Test,"awG",@progbits,_ZTV39AliasBufferTest_SharedArrayBuffer2_Test,comdat
	.align 8
	.type	_ZTV39AliasBufferTest_SharedArrayBuffer2_Test, @object
	.size	_ZTV39AliasBufferTest_SharedArrayBuffer2_Test, 64
_ZTV39AliasBufferTest_SharedArrayBuffer2_Test:
	.quad	0
	.quad	0
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD1Ev
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer2_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer2_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer3_TestE10CreateTestEv
	.weak	_ZTV39AliasBufferTest_SharedArrayBuffer3_Test
	.section	.data.rel.ro.local._ZTV39AliasBufferTest_SharedArrayBuffer3_Test,"awG",@progbits,_ZTV39AliasBufferTest_SharedArrayBuffer3_Test,comdat
	.align 8
	.type	_ZTV39AliasBufferTest_SharedArrayBuffer3_Test, @object
	.size	_ZTV39AliasBufferTest_SharedArrayBuffer3_Test, 64
_ZTV39AliasBufferTest_SharedArrayBuffer3_Test:
	.quad	0
	.quad	0
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD1Ev
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer3_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer3_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI39AliasBufferTest_SharedArrayBuffer4_TestE10CreateTestEv
	.weak	_ZTV39AliasBufferTest_SharedArrayBuffer4_Test
	.section	.data.rel.ro.local._ZTV39AliasBufferTest_SharedArrayBuffer4_Test,"awG",@progbits,_ZTV39AliasBufferTest_SharedArrayBuffer4_Test,comdat
	.align 8
	.type	_ZTV39AliasBufferTest_SharedArrayBuffer4_Test, @object
	.size	_ZTV39AliasBufferTest_SharedArrayBuffer4_Test, 64
_ZTV39AliasBufferTest_SharedArrayBuffer4_Test:
	.quad	0
	.quad	0
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD1Ev
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer4_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN39AliasBufferTest_SharedArrayBuffer4_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI38AliasBufferTest_OperatorOverloads_TestE10CreateTestEv
	.weak	_ZTV38AliasBufferTest_OperatorOverloads_Test
	.section	.data.rel.ro.local._ZTV38AliasBufferTest_OperatorOverloads_Test,"awG",@progbits,_ZTV38AliasBufferTest_OperatorOverloads_Test,comdat
	.align 8
	.type	_ZTV38AliasBufferTest_OperatorOverloads_Test, @object
	.size	_ZTV38AliasBufferTest_OperatorOverloads_Test, 64
_ZTV38AliasBufferTest_OperatorOverloads_Test:
	.quad	0
	.quad	0
	.quad	_ZN38AliasBufferTest_OperatorOverloads_TestD1Ev
	.quad	_ZN38AliasBufferTest_OperatorOverloads_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN38AliasBufferTest_OperatorOverloads_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI42AliasBufferTest_OperatorOverloadsRefs_TestE10CreateTestEv
	.weak	_ZTV42AliasBufferTest_OperatorOverloadsRefs_Test
	.section	.data.rel.ro.local._ZTV42AliasBufferTest_OperatorOverloadsRefs_Test,"awG",@progbits,_ZTV42AliasBufferTest_OperatorOverloadsRefs_Test,comdat
	.align 8
	.type	_ZTV42AliasBufferTest_OperatorOverloadsRefs_Test, @object
	.size	_ZTV42AliasBufferTest_OperatorOverloadsRefs_Test, 64
_ZTV42AliasBufferTest_OperatorOverloadsRefs_Test:
	.quad	0
	.quad	0
	.quad	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD1Ev
	.quad	_ZN42AliasBufferTest_OperatorOverloadsRefs_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN42AliasBufferTest_OperatorOverloadsRefs_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0
	.section	.rodata.str1.1
.LC57:
	.string	"../src/aliased_buffer.h:74"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"(MultiplyWithOverflowCheck(sizeof(NativeT), count)) <= (ab->ByteLength() - byte_offset)"
	.align 8
.LC59:
	.ascii	"node::AliasedBufferBase<"
	.string	"NativeT, V8T, <template-parameter-1-3> >::AliasedBufferBase(v8::Isolate*, size_t, size_t, const node::AliasedBufferBase<unsigned char, v8::Uint8Array>&) [with NativeT = int; V8T = v8::Int32Array; <template-parameter-1-3> = void; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,"awG",@progbits,_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, 24
_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.weak	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args
	.section	.rodata.str1.1
.LC60:
	.string	"../src/aliased_buffer.h:72"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"(byte_offset & (sizeof(NativeT) - 1)) == (0)"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args,"awG",@progbits,_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args, 24
_ZZN4node17AliasedBufferBaseIiN2v810Int32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC59
	.weak	_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0
	.section	.rodata.str1.8
	.align 8
.LC62:
	.ascii	"node::AliasedBufferBase<NativeT"
	.string	", V8T, <template-parameter-1-3> >::AliasedBufferBase(v8::Isolate*, size_t, size_t, const node::AliasedBufferBase<unsigned char, v8::Uint8Array>&) [with NativeT = signed char; V8T = v8::Int8Array; <template-parameter-1-3> = void; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,"awG",@progbits,_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, 24
_ZZN4node17AliasedBufferBaseIaN2v89Int8ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC62
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC63:
	.string	"../src/util-inl.h:325"
.LC64:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.weak	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0
	.section	.rodata.str1.8
	.align 8
.LC66:
	.ascii	"node::AliasedBufferBase<NativeT, V"
	.string	"8T, <template-parameter-1-3> >::AliasedBufferBase(v8::Isolate*, size_t, size_t, const node::AliasedBufferBase<unsigned char, v8::Uint8Array>&) [with NativeT = unsigned int; V8T = v8::Uint32Array; <template-parameter-1-3> = void; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,"awG",@progbits,_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, 24
_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC66
	.weak	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0
	.section	.rodata.str1.8
	.align 8
.LC67:
	.ascii	"node::AliasedBufferBase<Nativ"
	.string	"eT, V8T, <template-parameter-1-3> >::AliasedBufferBase(v8::Isolate*, size_t, size_t, const node::AliasedBufferBase<unsigned char, v8::Uint8Array>&) [with NativeT = double; V8T = v8::Float64Array; <template-parameter-1-3> = void; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,"awG",@progbits,_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, 24
_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC67
	.weak	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args,"awG",@progbits,_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args, 24
_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC67
	.weak	_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args
	.section	.rodata.str1.1
.LC68:
	.string	"../src/aliased_buffer.h:37"
.LC69:
	.string	"(count) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"node::AliasedBufferBase<NativeT, V8T, <template-parameter-1-3> >::AliasedBufferBase(v8::Isolate*, size_t) [with NativeT = unsigned char; V8T = v8::Uint8Array; <template-parameter-1-3> = void; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args,"awG",@progbits,_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args, 24
_ZZN4node17AliasedBufferBaseIhN2v810Uint8ArrayEvEC4EPNS1_7IsolateEmE4args:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.globl	_ZN42AliasBufferTest_OperatorOverloadsRefs_Test10test_info_E
	.bss
	.align 8
	.type	_ZN42AliasBufferTest_OperatorOverloadsRefs_Test10test_info_E, @object
	.size	_ZN42AliasBufferTest_OperatorOverloadsRefs_Test10test_info_E, 8
_ZN42AliasBufferTest_OperatorOverloadsRefs_Test10test_info_E:
	.zero	8
	.globl	_ZN38AliasBufferTest_OperatorOverloads_Test10test_info_E
	.align 8
	.type	_ZN38AliasBufferTest_OperatorOverloads_Test10test_info_E, @object
	.size	_ZN38AliasBufferTest_OperatorOverloads_Test10test_info_E, 8
_ZN38AliasBufferTest_OperatorOverloads_Test10test_info_E:
	.zero	8
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer4_Test10test_info_E
	.align 8
	.type	_ZN39AliasBufferTest_SharedArrayBuffer4_Test10test_info_E, @object
	.size	_ZN39AliasBufferTest_SharedArrayBuffer4_Test10test_info_E, 8
_ZN39AliasBufferTest_SharedArrayBuffer4_Test10test_info_E:
	.zero	8
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer3_Test10test_info_E
	.align 8
	.type	_ZN39AliasBufferTest_SharedArrayBuffer3_Test10test_info_E, @object
	.size	_ZN39AliasBufferTest_SharedArrayBuffer3_Test10test_info_E, 8
_ZN39AliasBufferTest_SharedArrayBuffer3_Test10test_info_E:
	.zero	8
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer2_Test10test_info_E
	.align 8
	.type	_ZN39AliasBufferTest_SharedArrayBuffer2_Test10test_info_E, @object
	.size	_ZN39AliasBufferTest_SharedArrayBuffer2_Test10test_info_E, 8
_ZN39AliasBufferTest_SharedArrayBuffer2_Test10test_info_E:
	.zero	8
	.globl	_ZN39AliasBufferTest_SharedArrayBuffer1_Test10test_info_E
	.align 8
	.type	_ZN39AliasBufferTest_SharedArrayBuffer1_Test10test_info_E, @object
	.size	_ZN39AliasBufferTest_SharedArrayBuffer1_Test10test_info_E, 8
_ZN39AliasBufferTest_SharedArrayBuffer1_Test10test_info_E:
	.zero	8
	.globl	_ZN33AliasBufferTest_Float64Array_Test10test_info_E
	.align 8
	.type	_ZN33AliasBufferTest_Float64Array_Test10test_info_E, @object
	.size	_ZN33AliasBufferTest_Float64Array_Test10test_info_E, 8
_ZN33AliasBufferTest_Float64Array_Test10test_info_E:
	.zero	8
	.globl	_ZN33AliasBufferTest_Float32Array_Test10test_info_E
	.align 8
	.type	_ZN33AliasBufferTest_Float32Array_Test10test_info_E, @object
	.size	_ZN33AliasBufferTest_Float32Array_Test10test_info_E, 8
_ZN33AliasBufferTest_Float32Array_Test10test_info_E:
	.zero	8
	.globl	_ZN31AliasBufferTest_Int32Array_Test10test_info_E
	.align 8
	.type	_ZN31AliasBufferTest_Int32Array_Test10test_info_E, @object
	.size	_ZN31AliasBufferTest_Int32Array_Test10test_info_E, 8
_ZN31AliasBufferTest_Int32Array_Test10test_info_E:
	.zero	8
	.globl	_ZN32AliasBufferTest_Uint32Array_Test10test_info_E
	.align 8
	.type	_ZN32AliasBufferTest_Uint32Array_Test10test_info_E, @object
	.size	_ZN32AliasBufferTest_Uint32Array_Test10test_info_E, 8
_ZN32AliasBufferTest_Uint32Array_Test10test_info_E:
	.zero	8
	.globl	_ZN31AliasBufferTest_Int16Array_Test10test_info_E
	.align 8
	.type	_ZN31AliasBufferTest_Int16Array_Test10test_info_E, @object
	.size	_ZN31AliasBufferTest_Int16Array_Test10test_info_E, 8
_ZN31AliasBufferTest_Int16Array_Test10test_info_E:
	.zero	8
	.globl	_ZN32AliasBufferTest_Uint16Array_Test10test_info_E
	.align 8
	.type	_ZN32AliasBufferTest_Uint16Array_Test10test_info_E, @object
	.size	_ZN32AliasBufferTest_Uint16Array_Test10test_info_E, 8
_ZN32AliasBufferTest_Uint16Array_Test10test_info_E:
	.zero	8
	.globl	_ZN30AliasBufferTest_Int8Array_Test10test_info_E
	.align 8
	.type	_ZN30AliasBufferTest_Int8Array_Test10test_info_E, @object
	.size	_ZN30AliasBufferTest_Int8Array_Test10test_info_E, 8
_ZN30AliasBufferTest_Int8Array_Test10test_info_E:
	.zero	8
	.globl	_ZN31AliasBufferTest_Uint8Array_Test10test_info_E
	.align 8
	.type	_ZN31AliasBufferTest_Uint8Array_Test10test_info_E, @object
	.size	_ZN31AliasBufferTest_Uint8Array_Test10test_info_E, 8
_ZN31AliasBufferTest_Uint8Array_Test10test_info_E:
	.zero	8
	.weak	_ZZN15NodeTestFixture5SetUpEvE4args
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"../test/cctest/node_test_fixture.h:108"
	.section	.rodata.str1.1
.LC72:
	.string	"(isolate_) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"virtual void NodeTestFixture::SetUp()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture5SetUpEvE4args,"awG",@progbits,_ZZN15NodeTestFixture5SetUpEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture5SetUpEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture5SetUpEvE4args, 24
_ZZN15NodeTestFixture5SetUpEvE4args:
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.weak	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"../test/cctest/node_test_fixture.h:101"
	.align 8
.LC75:
	.string	"(0) == (uv_loop_close(&current_loop))"
	.align 8
.LC76:
	.string	"static void NodeTestFixture::TearDownTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture16TearDownTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture16TearDownTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, 24
_ZZN15NodeTestFixture16TearDownTestCaseEvE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.weak	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"../test/cctest/node_test_fixture.h:87"
	.align 8
.LC78:
	.string	"(0) == (uv_loop_init(&current_loop))"
	.align 8
.LC79:
	.string	"static void NodeTestFixture::SetUpTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture13SetUpTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture13SetUpTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, 24
_ZZN15NodeTestFixture13SetUpTestCaseEvE4args:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC80:
	.string	"../src/tracing/agent.h:91"
.LC81:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC17:
	.quad	100
	.quad	0
	.align 16
.LC18:
	.quad	-8
	.quad	-8
	.align 16
.LC19:
	.quad	-2
	.quad	-2
	.align 16
.LC20:
	.quad	-4
	.quad	-4
	.align 16
.LC21:
	.quad	-6
	.quad	-6
	.align 16
.LC22:
	.quad	100
	.quad	99
	.section	.data.rel.ro,"aw"
	.align 8
.LC24:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC25:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC26:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16
	.align 16
.LC43:
	.quad	10
	.quad	0
	.align 16
.LC52:
	.quad	2
	.quad	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
