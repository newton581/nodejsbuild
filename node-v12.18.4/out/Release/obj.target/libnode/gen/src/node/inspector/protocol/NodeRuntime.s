	.file	"NodeRuntime.cpp"
	.text
	.section	.text._ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev:
.LFB4828:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movb	$0, 24(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4828:
	.size	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.section	.text._ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4829:
	.cfi_startproc
	endbr64
	movdqu	40(%rsi), %xmm1
	movq	56(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 56(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 40(%rsi)
	ret
	.cfi_endproc
.LFE4829:
	.size	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN4node9inspector8protocol23InternalRawNotificationD2Ev:
.LFB4825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4825:
	.size	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev, .-_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev,_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD2Ev
	.type	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD2Ev, @function
_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD2Ev:
.LFB4925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L19
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L45:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L15
.L18:
	movq	%rbx, %r13
.L19:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L45
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L18
.L15:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L24
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L46:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L21
.L23:
	movq	%rbx, %r13
.L24:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L46
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L23
.L21:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE4925:
	.size	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD2Ev, .-_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD2Ev
	.weak	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD1Ev
	.set	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD1Ev,_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD2Ev
	.section	.text._ZN4node9inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN4node9inspector8protocol23InternalRawNotificationD0Ev:
.LFB4827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4827:
	.size	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev, .-_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.section	.text._ZN4node9inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD0Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD0Ev, @function
_ZN4node9inspector8protocol16InternalResponseD0Ev:
.LFB4818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	(%rdi), %rax
	call	*24(%rax)
.L55:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4818:
	.size	_ZN4node9inspector8protocol16InternalResponseD0Ev, .-_ZN4node9inspector8protocol16InternalResponseD0Ev
	.section	.text._ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev
	.type	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev, @function
_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev:
.LFB4927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L66
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L92:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L62
.L65:
	movq	%rbx, %r13
.L66:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L92
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L65
.L62:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L71
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L93:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L68
.L70:
	movq	%rbx, %r13
.L71:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L93
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L70
.L68:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4927:
	.size	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev, .-_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	80(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	72(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L101
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	56(%rbx), %rcx
.L97:
	cmpq	%rcx, %r13
	je	.L104
.L96:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L101
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L97
.L101:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L96
	testq	%rdx, %rdx
	je	.L100
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L96
.L100:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4929:
	.size	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"enabled"
.LC1:
	.string	"boolean value expected"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl30notifyWhenWaitingForDisconnectEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl30notifyWhenWaitingForDisconnectEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl30notifyWhenWaitingForDisconnectEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB4932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	%rcx, -152(%rbp)
	movq	(%r8), %rdi
	movl	$29549, %ecx
	movq	%rdx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -112(%rbp)
	movl	$1634886000, -96(%rbp)
	movw	%cx, -92(%rbp)
	movq	$6, -104(%rbp)
	movb	$0, -90(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L107
	cmpl	$6, 8(%rax)
	jne	.L107
	cmpq	%r12, %rdi
	je	.L141
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
.L127:
	movl	$25964, %edx
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%r12, -112(%rbp)
	movl	$1650552421, -96(%rbp)
	movw	%dx, -92(%rbp)
	movb	$100, -90(%rbp)
	movq	$7, -104(%rbp)
	movb	$0, -89(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r12, %rdi
	je	.L112
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %r8
.L112:
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-136(%rbp), %r8
	movb	$0, -120(%rbp)
	testq	%r8, %r8
	je	.L113
	movq	(%r8), %rax
	leaq	-120(%rbp), %rsi
	movq	%r8, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L115
.L113:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L115:
	movzbl	-120(%rbp), %eax
	movq	%rbx, %rdi
	movb	%al, -136(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L142
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movzbl	-136(%rbp), %edx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	cmpl	$2, -112(%rbp)
	je	.L143
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L125
	movq	%r14, %rdx
	movl	%r15d, %esi
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE@PLT
.L125:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L105
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-136(%rbp), %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L107:
	cmpq	%r12, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movb	$0, -120(%rbp)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rdi
	movq	%r12, -112(%rbp)
	call	strlen@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L145
	cmpq	$1, %rax
	jne	.L120
	movzbl	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %edx
	movb	%dl, -96(%rbp)
	movq	%r12, %rdx
.L121:
	movq	%rax, -104(%rbp)
	movq	%r13, %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movb	$0, (%rdx,%rax)
	movl	%r15d, %esi
	movl	$-32602, %edx
	call	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L105
	call	_ZdlPv@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L120:
	testq	%rax, %rax
	jne	.L146
	movq	%r12, %rdx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L143:
	movq	8(%r13), %rdi
	movq	-152(%rbp), %rcx
	movl	%r15d, %esi
	movq	-144(%rbp), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r14, %rdi
	leaq	-120(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
.L119:
	movq	%r8, %rdx
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	jmp	.L121
.L144:
	call	__stack_chk_fail@PLT
.L146:
	movq	%r12, %rdi
	jmp	.L119
	.cfi_endproc
.LFE4932:
	.size	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl30notifyWhenWaitingForDisconnectEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl30notifyWhenWaitingForDisconnectEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.text._ZN4node9inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD2Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD2Ev, @function
_ZN4node9inspector8protocol16InternalResponseD2Ev:
.LFB4816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	(%rdi), %rax
	call	*24(%rax)
.L148:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L147
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4816:
	.size	_ZN4node9inspector8protocol16InternalResponseD2Ev, .-_ZN4node9inspector8protocol16InternalResponseD2Ev
	.weak	_ZN4node9inspector8protocol16InternalResponseD1Ev
	.set	_ZN4node9inspector8protocol16InternalResponseD1Ev,_ZN4node9inspector8protocol16InternalResponseD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime8Frontend20waitingForDisconnectEv
	.type	_ZN4node9inspector8protocol11NodeRuntime8Frontend20waitingForDisconnectEv, @function
_ZN4node9inspector8protocol11NodeRuntime8Frontend20waitingForDisconnectEv:
.LFB4873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L154
	movq	(%r12), %rax
	leaq	-96(%rbp), %r14
	leaq	-104(%rbp), %r13
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-80(%rbp), %rbx
	movq	$0, -120(%rbp)
	movq	24(%rax), %r15
	movq	%rbx, -96(%rbp)
	movq	$32, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC2(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC3(%rip), %xmm0
	movq	-96(%rbp), %rdx
	movups	%xmm0, 16(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-120(%rbp), %rdx
	call	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L156
	movq	(%rdi), %rax
	call	*24(%rax)
.L156:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L157
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L158
	movq	48(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L159
	movq	(%rdi), %rax
	call	*24(%rax)
.L159:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L160
	call	_ZdlPv@PLT
.L160:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L157:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L154
	movq	(%rdi), %rax
	call	*24(%rax)
.L154:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L157
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4873:
	.size	_ZN4node9inspector8protocol11NodeRuntime8Frontend20waitingForDisconnectEv, .-_ZN4node9inspector8protocol11NodeRuntime8Frontend20waitingForDisconnectEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime8Frontend5flushEv
	.type	_ZN4node9inspector8protocol11NodeRuntime8Frontend5flushEv, @function
_ZN4node9inspector8protocol11NodeRuntime8Frontend5flushEv:
.LFB4876:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE4876:
	.size	_ZN4node9inspector8protocol11NodeRuntime8Frontend5flushEv, .-_ZN4node9inspector8protocol11NodeRuntime8Frontend5flushEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L196
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	leaq	-96(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rdx, (%rsi)
	movq	%rcx, -96(%rbp)
	movq	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L184
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L186:
	movq	%rdi, -72(%rbp)
	movl	$64, %edi
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movb	$0, -96(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L197
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L188:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movq	$0, 56(%rax)
	leaq	-120(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movups	%xmm0, 40(%rax)
	call	*%r14
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L189
	movq	(%rdi), %rax
	call	*24(%rax)
.L189:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movb	$0, 16(%rsi)
	movq	$0, 8(%rsi)
	leaq	-96(%rbp), %rbx
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
.L184:
	movdqa	-96(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L197:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L188
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4877:
	.size	_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB4878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$64, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movb	$0, 24(%rax)
	leaq	-64(%rbp), %rsi
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rcx
	movq	%rdx, 8(%rax)
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movq	%r15, 56(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 40(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	movq	(%rdi), %rax
	call	*24(%rax)
.L199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L206:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4878:
	.size	_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol11NodeRuntime8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE
	.type	_ZN4node9inspector8protocol11NodeRuntime10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE, @function
_ZN4node9inspector8protocol11NodeRuntime10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE:
.LFB4933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r14, %rsi
	leaq	-104(%rbp), %r14
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE@PLT
	leaq	16+_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE(%rip), %rax
	xorl	%edx, %edx
	movss	.LC4(%rip), %xmm0
	movq	%rax, (%rbx)
	leaq	120(%rbx), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	movq	%rax, 72(%rbx)
	leaq	176(%rbx), %rax
	movq	%r13, 184(%rbx)
	leaq	-80(%rbp), %r13
	movq	$1, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	movq	%rax, 128(%rbx)
	movq	$1, 136(%rbx)
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 176(%rbx)
	movss	%xmm0, 104(%rbx)
	movss	%xmm0, 160(%rbx)
	movq	%r13, -96(%rbp)
	movq	$42, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movdqa	.LC5(%rip), %xmm0
	movabsq	$7308900669957826884, %rcx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$29795, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC6(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movw	%dx, 40(%rax)
	movups	%xmm0, 16(%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-88(%rbp), %rsi
	movl	$3339675911, %edx
	movq	-96(%rbp), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	80(%rbx), %r9
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%r9
	leaq	0(,%rdx,8), %rax
	movq	%rax, -120(%rbp)
	movq	72(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L208
	movq	(%rax), %rcx
	movq	-88(%rbp), %r11
	movq	%rdx, %r10
	movq	-96(%rbp), %rdi
	movq	56(%rcx), %rsi
	testq	%r11, %r11
	je	.L209
.L212:
	cmpq	%rsi, %r8
	je	.L304
.L210:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L208
	movq	56(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	je	.L212
.L208:
	movl	$64, %edi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %r9
	leaq	24(%rax), %rax
	movq	%rax, 8(%r9)
	movq	-96(%rbp), %rax
	cmpq	%r13, %rax
	je	.L305
	movq	%rax, 8(%r9)
	movq	-80(%rbp), %rax
	movq	%rax, 24(%r9)
.L215:
	movq	-88(%rbp), %rax
	movq	96(%rbx), %rdx
	leaq	104(%rbx), %rdi
	movq	%r13, -96(%rbp)
	movq	80(%rbx), %rsi
	movl	$1, %ecx
	movb	$0, -80(%rbp)
	movq	%rax, 16(%r9)
	movq	$0, 40(%r9)
	movq	$0, 48(%r9)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	$0, -88(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %r8
	testb	%al, %al
	movq	%rdx, %rcx
	jne	.L216
	movq	72(%rbx), %r10
.L217:
	movq	-120(%rbp), %rcx
	movq	%r8, 56(%r9)
	addq	%r10, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L226
	movq	(%rax), %rax
	movq	%rax, (%r9)
	movq	(%rcx), %rax
	movq	%r9, (%rax)
.L227:
	addq	$1, 96(%rbx)
	leaq	40(%r9), %rcx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L209:
	cmpq	%rsi, %r8
	je	.L306
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L208
	movq	56(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L208
	cmpq	%rsi, %r8
	jne	.L213
.L306:
	cmpq	$0, 16(%rcx)
	jne	.L213
.L211:
	addq	$40, %rcx
.L246:
	leaq	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl30notifyWhenWaitingForDisconnectEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rax
	movq	$0, 8(%rcx)
	movq	%rax, (%rcx)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	leaq	128(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	movabsq	$8389772152586661710, %rax
	movq	%rbx, -104(%rbp)
	movq	%rax, -80(%rbp)
	movl	$28009, %eax
	movq	%r13, -96(%rbp)
	movw	%ax, -72(%rbp)
	movb	$101, -70(%rbp)
	movq	$11, -88(%rbp)
	movb	$0, -69(%rbp)
	call	_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L207
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L232
	movq	144(%r12), %r13
	leaq	16+_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L233
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L307:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L238
.L239:
	movq	%rbx, %r13
.L233:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L307
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L239
.L238:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L240
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L308:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L244
.L245:
	movq	%rbx, %r13
.L240:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L308
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L245
.L244:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L207:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	cmpq	%r11, 16(%rcx)
	jne	.L210
	movq	8(%rcx), %rsi
	movq	%r11, %rdx
	movq	%r10, -176(%rbp)
	movq	%r9, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%rdi, -136(%rbp)
	call	memcmp@PLT
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %r11
	testl	%eax, %eax
	movq	-152(%rbp), %rcx
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r10
	je	.L211
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L216:
	cmpq	$1, %rdx
	je	.L310
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L311
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -152(%rbp)
	movq	%rdx, %rdi
	movq	%r8, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rdx, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rcx
	movq	%rax, %r10
.L219:
	movq	88(%rbx), %rdi
	movq	$0, 88(%rbx)
	testq	%rdi, %rdi
	je	.L221
	xorl	%edx, %edx
	leaq	88(%rbx), %rax
	movq	%r8, -136(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rdx, %r8
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L223:
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	movq	(%r11), %rax
	movq	%rsi, (%rax)
.L224:
	testq	%rdi, %rdi
	je	.L297
.L222:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	56(%rsi), %rax
	divq	%rcx
	leaq	(%r10,%rdx,8), %r11
	movq	(%r11), %rax
	testq	%rax, %rax
	jne	.L223
	movq	88(%rbx), %rax
	movq	%rax, (%rsi)
	movq	-120(%rbp), %rax
	movq	%rsi, 88(%rbx)
	movq	%rax, (%r11)
	cmpq	$0, (%rsi)
	je	.L248
	movq	%rsi, (%r10,%r8,8)
	movq	%rdx, %r8
	testq	%rdi, %rdi
	jne	.L222
.L297:
	movq	-136(%rbp), %r8
.L221:
	movq	72(%rbx), %rdi
	cmpq	%rdi, -128(%rbp)
	je	.L225
	movq	%rcx, -144(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r8
.L225:
	movq	%r8, %rax
	xorl	%edx, %edx
	movq	%rcx, 80(%rbx)
	divq	%rcx
	movq	%r10, 72(%rbx)
	leaq	0(,%rdx,8), %rax
	movq	%rax, -120(%rbp)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L305:
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 24(%r9)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L226:
	movq	88(%rbx), %rax
	movq	%r9, 88(%rbx)
	movq	%rax, (%r9)
	testq	%rax, %rax
	je	.L228
	movq	56(%rax), %rax
	xorl	%edx, %edx
	divq	80(%rbx)
	movq	%r9, (%r10,%rdx,8)
.L228:
	leaq	88(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%rdx, %r8
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L310:
	movq	$0, 120(%rbx)
	movq	-128(%rbp), %r10
	jmp	.L219
.L309:
	call	__stack_chk_fail@PLT
.L311:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4933:
	.size	_ZN4node9inspector8protocol11NodeRuntime10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE, .-_ZN4node9inspector8protocol11NodeRuntime10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_:
.LFB6032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L313
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	56(%rbx), %rcx
.L316:
	cmpq	%rcx, %r13
	je	.L333
.L314:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L313
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L316
.L313:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L314
	testq	%rdx, %rdx
	je	.L315
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L314
.L315:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6032:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.type	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE, @function
_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE:
.LFB4930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movq	%rcx, -136(%rbp)
	movl	%esi, -140(%rbp)
	movq	%rdx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeRuntime14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	movq	40(%r14), %rax
	addq	48(%r14), %rbx
	movq	-136(%rbp), %rcx
	movl	-140(%rbp), %r10d
	movq	%rbx, %rdi
	testb	$1, %al
	je	.L335
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
.L335:
	movq	0(%r13), %rdx
	movq	%r15, %r9
	leaq	-120(%rbp), %r8
	movl	%r10d, %esi
	movq	$0, 0(%r13)
	movq	%rdx, -120(%rbp)
	movq	%r12, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L336
	movq	(%rdi), %rax
	call	*24(%rax)
.L336:
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L345
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L345:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4930:
	.size	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE, .-_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.weak	_ZTVN4node9inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN4node9inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN4node9inspector8protocol23InternalRawNotificationE, 48
_ZTVN4node9inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE,"awG",@progbits,_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE, @object
	.size	_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE, 48
_ZTVN4node9inspector8protocol11NodeRuntime14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD1Ev
	.quad	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImplD0Ev
	.quad	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector8protocol11NodeRuntime14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.globl	_ZN4node9inspector8protocol11NodeRuntime8Metainfo7versionE
	.section	.rodata
	.type	_ZN4node9inspector8protocol11NodeRuntime8Metainfo7versionE, @object
	.size	_ZN4node9inspector8protocol11NodeRuntime8Metainfo7versionE, 4
_ZN4node9inspector8protocol11NodeRuntime8Metainfo7versionE:
	.string	"1.0"
	.globl	_ZN4node9inspector8protocol11NodeRuntime8Metainfo13commandPrefixE
	.align 8
	.type	_ZN4node9inspector8protocol11NodeRuntime8Metainfo13commandPrefixE, @object
	.size	_ZN4node9inspector8protocol11NodeRuntime8Metainfo13commandPrefixE, 13
_ZN4node9inspector8protocol11NodeRuntime8Metainfo13commandPrefixE:
	.string	"NodeRuntime."
	.globl	_ZN4node9inspector8protocol11NodeRuntime8Metainfo10domainNameE
	.align 8
	.type	_ZN4node9inspector8protocol11NodeRuntime8Metainfo10domainNameE, @object
	.size	_ZN4node9inspector8protocol11NodeRuntime8Metainfo10domainNameE, 12
_ZN4node9inspector8protocol11NodeRuntime8Metainfo10domainNameE:
	.string	"NodeRuntime"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	8389772152586661710
	.quad	8388342945461661033
	.align 16
.LC3:
	.quad	7585313494646877801
	.quad	8386658456067597171
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC4:
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC5:
	.quad	8389772152586661710
	.quad	7598820990296354153
	.align 16
.LC6:
	.quad	7014196326474152294
	.quad	8245886852786648169
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
