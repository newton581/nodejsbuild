	.file	"Protocol.cpp"
	.text
	.section	.text._ZN4node9inspector8protocol5ValueD2Ev,"axG",@progbits,_ZN4node9inspector8protocol5ValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol5ValueD2Ev
	.type	_ZN4node9inspector8protocol5ValueD2Ev, @function
_ZN4node9inspector8protocol5ValueD2Ev:
.LFB4617:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4617:
	.size	_ZN4node9inspector8protocol5ValueD2Ev, .-_ZN4node9inspector8protocol5ValueD2Ev
	.weak	_ZN4node9inspector8protocol5ValueD1Ev
	.set	_ZN4node9inspector8protocol5ValueD1Ev,_ZN4node9inspector8protocol5ValueD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.type	_ZNK4node9inspector8protocol5Value9asBooleanEPb, @function
_ZNK4node9inspector8protocol5Value9asBooleanEPb:
.LFB5348:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5348:
	.size	_ZNK4node9inspector8protocol5Value9asBooleanEPb, .-_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE
	.set	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE,_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.set	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK4node9inspector8protocol5Value9asIntegerEPi
	.set	_ZNK4node9inspector8protocol5Value9asIntegerEPi,_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.globl	_ZNK4node9inspector8protocol5Value8asDoubleEPd
	.set	_ZNK4node9inspector8protocol5Value8asDoubleEPd,_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol5Value17serializeToBinaryEv, @function
_ZN4node9inspector8protocol5Value17serializeToBinaryEv:
.LFB5358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	(%rsi), %rax
	movups	%xmm0, (%r12)
	movq	%r12, %rsi
	movq	$0, 16(%r12)
	call	*80(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5358:
	.size	_ZN4node9inspector8protocol5Value17serializeToBinaryEv, .-_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol16FundamentalValue9asBooleanEPb
	.type	_ZNK4node9inspector8protocol16FundamentalValue9asBooleanEPb, @function
_ZNK4node9inspector8protocol16FundamentalValue9asBooleanEPb:
.LFB5359:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, 8(%rdi)
	jne	.L6
	movzbl	16(%rdi), %eax
	movb	%al, (%rsi)
	movl	$1, %eax
.L6:
	ret
	.cfi_endproc
.LFE5359:
	.size	_ZNK4node9inspector8protocol16FundamentalValue9asBooleanEPb, .-_ZNK4node9inspector8protocol16FundamentalValue9asBooleanEPb
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol16FundamentalValue8asDoubleEPd
	.type	_ZNK4node9inspector8protocol16FundamentalValue8asDoubleEPd, @function
_ZNK4node9inspector8protocol16FundamentalValue8asDoubleEPd:
.LFB5360:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	cmpl	$3, %eax
	je	.L13
	xorl	%r8d, %r8d
	cmpl	$2, %eax
	je	.L14
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movsd	16(%rdi), %xmm0
	movl	$1, %r8d
	movl	%r8d, %eax
	movsd	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	pxor	%xmm0, %xmm0
	movl	$1, %r8d
	cvtsi2sdl	16(%rdi), %xmm0
	movl	%r8d, %eax
	movsd	%xmm0, (%rsi)
	ret
	.cfi_endproc
.LFE5360:
	.size	_ZNK4node9inspector8protocol16FundamentalValue8asDoubleEPd, .-_ZNK4node9inspector8protocol16FundamentalValue8asDoubleEPd
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol16FundamentalValue9asIntegerEPi
	.type	_ZNK4node9inspector8protocol16FundamentalValue9asIntegerEPi, @function
_ZNK4node9inspector8protocol16FundamentalValue9asIntegerEPi:
.LFB5361:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$2, 8(%rdi)
	jne	.L15
	movl	16(%rdi), %eax
	movl	%eax, (%rsi)
	movl	$1, %eax
.L15:
	ret
	.cfi_endproc
.LFE5361:
	.size	_ZNK4node9inspector8protocol16FundamentalValue9asIntegerEPi, .-_ZNK4node9inspector8protocol16FundamentalValue9asIntegerEPi
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11BinaryValue8asBinaryEPNS1_6BinaryE
	.type	_ZNK4node9inspector8protocol11BinaryValue8asBinaryEPNS1_6BinaryE, @function
_ZNK4node9inspector8protocol11BinaryValue8asBinaryEPNS1_6BinaryE:
.LFB5370:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5370:
	.size	_ZNK4node9inspector8protocol11BinaryValue8asBinaryEPNS1_6BinaryE, .-_ZNK4node9inspector8protocol11BinaryValue8asBinaryEPNS1_6BinaryE
	.section	.text._ZN4node9inspector8protocol16FundamentalValueD2Ev,"axG",@progbits,_ZN4node9inspector8protocol16FundamentalValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16FundamentalValueD2Ev
	.type	_ZN4node9inspector8protocol16FundamentalValueD2Ev, @function
_ZN4node9inspector8protocol16FundamentalValueD2Ev:
.LFB7291:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7291:
	.size	_ZN4node9inspector8protocol16FundamentalValueD2Ev, .-_ZN4node9inspector8protocol16FundamentalValueD2Ev
	.weak	_ZN4node9inspector8protocol16FundamentalValueD1Ev
	.set	_ZN4node9inspector8protocol16FundamentalValueD1Ev,_ZN4node9inspector8protocol16FundamentalValueD2Ev
	.section	.text._ZN4node9inspector8protocol11BinaryValueD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11BinaryValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11BinaryValueD2Ev
	.type	_ZN4node9inspector8protocol11BinaryValueD2Ev, @function
_ZN4node9inspector8protocol11BinaryValueD2Ev:
.LFB7328:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7328:
	.size	_ZN4node9inspector8protocol11BinaryValueD2Ev, .-_ZN4node9inspector8protocol11BinaryValueD2Ev
	.weak	_ZN4node9inspector8protocol11BinaryValueD1Ev
	.set	_ZN4node9inspector8protocol11BinaryValueD1Ev,_ZN4node9inspector8protocol11BinaryValueD2Ev
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S6_PS6_, @function
_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S6_PS6_:
.LFB8617:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	movzwl	(%rdi), %eax
	testl	$65408, %eax
	jne	.L24
	cmpw	$32, %ax
	jbe	.L45
.L24:
	cmpw	$47, %ax
	jne	.L22
	cmpq	%rdi, %rsi
	je	.L22
	leaq	2(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L22
	movzwl	2(%rdi), %eax
	cmpw	$47, %ax
	je	.L46
	cmpw	$42, %ax
	je	.L47
.L22:
	movq	%rdi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leal	-9(%rax), %ecx
	cmpw	$4, %cx
	jbe	.L34
	cmpw	$32, %ax
	jne	.L22
.L34:
	addq	$2, %rdi
.L26:
	cmpq	%rdi, %rsi
	ja	.L23
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$4, %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movzwl	(%rdi), %eax
	addq	$2, %rdi
	cmpw	$10, %ax
	je	.L26
	cmpw	$13, %ax
	je	.L26
.L44:
	cmpq	%rdi, %rsi
	ja	.L48
	movq	%rsi, %rdi
	movq	%rdi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	4(%rdi), %rcx
	cmpq	%rcx, %rsi
	ja	.L29
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L50:
	cmpw	$47, (%rax)
	je	.L49
.L30:
	movq	%rax, %rcx
.L29:
	leaq	2(%rcx), %rax
	movzwl	-2(%rax), %r8d
	cmpq	%rax, %rsi
	jbe	.L22
	cmpw	$42, %r8w
	jne	.L30
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	4(%rcx), %rdi
	jmp	.L26
	.cfi_endproc
.LFE8617:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S6_PS6_, .-_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S6_PS6_
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S6_PS6_, @function
_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S6_PS6_:
.LFB8620:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movzbl	(%rdi), %ecx
	movl	%ecx, %eax
	testb	$-128, %al
	jne	.L54
	cmpw	$32, %cx
	jbe	.L74
.L54:
	cmpb	$47, %al
	jne	.L52
	cmpq	%rdi, %rsi
	je	.L52
	leaq	1(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L52
	movzbl	1(%rdi), %eax
	cmpb	$47, %al
	je	.L75
	cmpb	$42, %al
	je	.L76
.L52:
	movq	%rdi, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	leal	-9(%rcx), %eax
	cmpw	$4, %ax
	jbe	.L64
	cmpw	$32, %cx
	jne	.L52
.L64:
	addq	$1, %rdi
.L56:
	cmpq	%rdi, %rsi
	ja	.L53
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$2, %rdi
	cmpq	%rdi, %rsi
	ja	.L58
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L77:
	cmpb	$13, %al
	je	.L56
	cmpq	%rdi, %rsi
	je	.L52
.L58:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	cmpb	$10, %al
	jne	.L77
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	2(%rdi), %rcx
	cmpq	%rcx, %rsi
	ja	.L59
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L79:
	cmpb	$47, (%rax)
	je	.L78
.L60:
	movq	%rax, %rcx
.L59:
	leaq	1(%rcx), %rax
	movzbl	-1(%rax), %r8d
	cmpq	%rax, %rsi
	je	.L52
	cmpb	$42, %r8b
	jne	.L60
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	2(%rcx), %rdi
	jmp	.L56
.L62:
	movq	%rsi, %rdi
	jmp	.L52
	.cfi_endproc
.LFE8620:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S6_PS6_, .-_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S6_PS6_
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE:
.LFB9759:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L80
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	8(%rdi), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
.L80:
	ret
	.cfi_endproc
.LFE9759:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE:
.LFB9771:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L82
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	8(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L82
	movq	%rdx, 8(%rax)
.L82:
	ret
	.cfi_endproc
.LFE9771:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE
	.section	.text._ZN4node9inspector8protocol11StringValueD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11StringValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11StringValueD2Ev
	.type	_ZN4node9inspector8protocol11StringValueD2Ev, @function
_ZN4node9inspector8protocol11StringValueD2Ev:
.LFB7308:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	addq	$32, %rdi
	movq	%rax, -32(%rdi)
	cmpq	%rdi, %r8
	je	.L86
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	ret
	.cfi_endproc
.LFE7308:
	.size	_ZN4node9inspector8protocol11StringValueD2Ev, .-_ZN4node9inspector8protocol11StringValueD2Ev
	.weak	_ZN4node9inspector8protocol11StringValueD1Ev
	.set	_ZN4node9inspector8protocol11StringValueD1Ev,_ZN4node9inspector8protocol11StringValueD2Ev
	.section	.text._ZN4node9inspector8protocol15SerializedValueD2Ev,"axG",@progbits,_ZN4node9inspector8protocol15SerializedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol15SerializedValueD2Ev
	.type	_ZN4node9inspector8protocol15SerializedValueD2Ev, @function
_ZN4node9inspector8protocol15SerializedValueD2Ev:
.LFB7345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	_ZdlPv@PLT
.L89:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L88
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7345:
	.size	_ZN4node9inspector8protocol15SerializedValueD2Ev, .-_ZN4node9inspector8protocol15SerializedValueD2Ev
	.weak	_ZN4node9inspector8protocol15SerializedValueD1Ev
	.set	_ZN4node9inspector8protocol15SerializedValueD1Ev,_ZN4node9inspector8protocol15SerializedValueD2Ev
	.section	.text._ZN4node9inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD2Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD2Ev, @function
_ZN4node9inspector8protocol16InternalResponseD2Ev:
.LFB4816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L96
	movq	(%rdi), %rax
	call	*24(%rax)
.L96:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L95
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4816:
	.size	_ZN4node9inspector8protocol16InternalResponseD2Ev, .-_ZN4node9inspector8protocol16InternalResponseD2Ev
	.weak	_ZN4node9inspector8protocol16InternalResponseD1Ev
	.set	_ZN4node9inspector8protocol16InternalResponseD1Ev,_ZN4node9inspector8protocol16InternalResponseD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev:
.LFB5546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L102
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5546:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev:
.LFB9713:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L106
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	ret
	.cfi_endproc
.LFE9713:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev
	.set	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED1Ev,_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB9709:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L108
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	ret
	.cfi_endproc
.LFE9709:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.set	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11StringValue8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol11StringValue8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol11StringValue8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	16(%r8), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5365:
	.size	_ZNK4node9inspector8protocol11StringValue8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol11StringValue8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol5Value5cloneEv
	.type	_ZNK4node9inspector8protocol5Value5cloneEv, @function
_ZNK4node9inspector8protocol5Value5cloneEv:
.LFB5355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol5ValueE(%rip), %rdx
	movq	%rdx, (%rax)
	movl	$0, 8(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5355:
	.size	_ZNK4node9inspector8protocol5Value5cloneEv, .-_ZNK4node9inspector8protocol5Value5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol16FundamentalValue5cloneEv
	.type	_ZNK4node9inspector8protocol16FundamentalValue5cloneEv, @function
_ZNK4node9inspector8protocol16FundamentalValue5cloneEv:
.LFB5364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	8(%rsi), %eax
	cmpl	$2, %eax
	je	.L115
	cmpl	$3, %eax
	je	.L116
	cmpl	$1, %eax
	je	.L122
	movq	$0, (%rdi)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	16(%rsi), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$2, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, (%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movzbl	16(%rsi), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rdx
	movl	$1, 8(%rax)
	movq	%rdx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, (%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movsd	16(%rsi), %xmm0
	movl	$24, %edi
	movsd	%xmm0, -24(%rbp)
	call	_Znwm@PLT
	movsd	-24(%rbp), %xmm0
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rdx
	movl	$3, 8(%rax)
	movq	%rdx, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, (%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5364:
	.size	_ZNK4node9inspector8protocol16FundamentalValue5cloneEv, .-_ZNK4node9inspector8protocol16FundamentalValue5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11BinaryValue5cloneEv
	.type	_ZNK4node9inspector8protocol11BinaryValue5cloneEv, @function
_ZNK4node9inspector8protocol11BinaryValue5cloneEv:
.LFB5373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$16, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol11BinaryValueE(%rip), %rdx
	movl	$5, 8(%rax)
	movq	%rdx, (%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5373:
	.size	_ZNK4node9inspector8protocol11BinaryValue5cloneEv, .-_ZNK4node9inspector8protocol11BinaryValue5cloneEv
	.section	.text._ZN4node9inspector8protocol5ValueD0Ev,"axG",@progbits,_ZN4node9inspector8protocol5ValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol5ValueD0Ev
	.type	_ZN4node9inspector8protocol5ValueD0Ev, @function
_ZN4node9inspector8protocol5ValueD0Ev:
.LFB4619:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4619:
	.size	_ZN4node9inspector8protocol5ValueD0Ev, .-_ZN4node9inspector8protocol5ValueD0Ev
	.section	.text._ZN4node9inspector8protocol16FundamentalValueD0Ev,"axG",@progbits,_ZN4node9inspector8protocol16FundamentalValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16FundamentalValueD0Ev
	.type	_ZN4node9inspector8protocol16FundamentalValueD0Ev, @function
_ZN4node9inspector8protocol16FundamentalValueD0Ev:
.LFB7293:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7293:
	.size	_ZN4node9inspector8protocol16FundamentalValueD0Ev, .-_ZN4node9inspector8protocol16FundamentalValueD0Ev
	.section	.text._ZN4node9inspector8protocol11BinaryValueD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11BinaryValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11BinaryValueD0Ev
	.type	_ZN4node9inspector8protocol11BinaryValueD0Ev, @function
_ZN4node9inspector8protocol11BinaryValueD0Ev:
.LFB7330:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7330:
	.size	_ZN4node9inspector8protocol11BinaryValueD0Ev, .-_ZN4node9inspector8protocol11BinaryValueD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev:
.LFB9707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	104(%r12), %rax
	movq	72(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L131
	movq	32(%r12), %rdi
.L130:
	call	_ZdlPv@PLT
.L129:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9707:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev
	.section	.text._ZN4node9inspector8protocol11StringValueD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11StringValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11StringValueD0Ev
	.type	_ZN4node9inspector8protocol11StringValueD0Ev, @function
_ZN4node9inspector8protocol11StringValueD0Ev:
.LFB7310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L138
	call	_ZdlPv@PLT
.L138:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7310:
	.size	_ZN4node9inspector8protocol11StringValueD0Ev, .-_ZN4node9inspector8protocol11StringValueD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol9ListValueD0Ev
	.type	_ZN4node9inspector8protocol9ListValueD0Ev, @function
_ZN4node9inspector8protocol9ListValueD0Ev:
.LFB5444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %rbx
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %rbx
	je	.L141
	.p2align 4,,10
	.p2align 3
.L145:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L145
.L143:
	movq	16(%r13), %r12
.L141:
	testq	%r12, %r12
	je	.L146
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L146:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L145
	jmp	.L143
	.cfi_endproc
.LFE5444:
	.size	_ZN4node9inspector8protocol9ListValueD0Ev, .-_ZN4node9inspector8protocol9ListValueD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev:
.LFB9715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9715:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB9711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L158
	call	_ZdlPv@PLT
.L158:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9711:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol9ListValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol9ListValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol9ListValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	$91, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZNSo3putEc@PLT
	movq	16(%r12), %rbx
	movq	24(%r12), %r12
	cmpq	%r12, %rbx
	je	.L164
.L165:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
	cmpq	%rbx, %r12
	je	.L164
	movl	$44, %esi
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L164:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$93, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSo3putEc@PLT
	.cfi_endproc
.LFE5445:
	.size	_ZNK4node9inspector8protocol9ListValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol9ListValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"null"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol5Value9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol5Value9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol5Value9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5353:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	jmp	_ZNSo5writeEPKcl@PLT
	.cfi_endproc
.LFE5353:
	.size	_ZNK4node9inspector8protocol5Value9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol5Value9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15SerializedValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol15SerializedValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol15SerializedValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5374:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rsi
	movq	%r8, %rdi
	jmp	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.cfi_endproc
.LFE5374:
	.size	_ZNK4node9inspector8protocol15SerializedValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol15SerializedValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11BinaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol11BinaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol11BinaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node9inspector8protocol6Binary8toBase64B5cxx11EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5371:
	.size	_ZNK4node9inspector8protocol11BinaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol11BinaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11StringValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol11StringValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol11StringValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5366:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	16(%r8), %rsi
	jmp	_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE@PLT
	.cfi_endproc
.LFE5366:
	.size	_ZNK4node9inspector8protocol11StringValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol11StringValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"static int node::inspector::protocol::json::{anonymous}::JsonParser<Char>::HexToInt(Char) [with Char = unsigned char]"
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/src/node/inspector/protocol/Protocol.cpp"
	.section	.rodata.str1.1
.LC3:
	.string	"false"
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0:
.LFB10008:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rcx
	movl	$3568, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10008:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"static int node::inspector::protocol::json::{anonymous}::JsonParser<Char>::HexToInt(Char) [with Char = short unsigned int]"
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0:
.LFB10009:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rcx
	movl	$3568, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10009:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK4node9inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK4node9inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node9inspector8protocol6Binary4sizeEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5372:
	.size	_ZNK4node9inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK4node9inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleError(node::inspector::protocol::Status) [with C = std::__cxx11::basic_string<char>]"
	.section	.rodata.str1.1
.LC6:
	.string	"!error.ok()"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE.part.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE.part.0:
.LFB10035:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rcx
	movl	$3198, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10035:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE.part.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE.part.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE:
.LFB9735:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L186
	movq	24(%rdi), %rax
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	16(%rdi), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	ret
.L186:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE.part.0
	.cfi_endproc
.LFE9735:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh:
.LFB7798:
	.cfi_startproc
	cmpb	$0, 8(%rdi)
	jne	.L190
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	subq	(%rbx), %rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7798:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt:
.LFB7800:
	.cfi_startproc
	cmpb	$0, 8(%rdi)
	jne	.L196
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	subq	(%rbx), %rdx
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
	movb	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7800:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"int32_t node::inspector::protocol::cbor::CBORTokenizer::GetInt32() const"
	.align 8
.LC8:
	.string	"token_tag_ == CBORTokenTag::INT32"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev.part.0, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev.part.0:
.LFB10038:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC7(%rip), %rcx
	movl	$2396, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10038:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev.part.0, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev.part.0
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"T node::inspector::protocol::cbor::{anonymous}::ReadBytesMostSignificantByteFirst(node::inspector::protocol::span<unsigned char>) [with T = unsigned int]"
	.section	.rodata.str1.1
.LC10:
	.string	"in.size() >= sizeof(T)"
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS1_4spanIhEE.part.0, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS1_4spanIhEE.part.0:
.LFB10046:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %rcx
	movl	$1901, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10046:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS1_4spanIhEE.part.0, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS1_4spanIhEE.part.0
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"T node::inspector::protocol::cbor::{anonymous}::ReadBytesMostSignificantByteFirst(node::inspector::protocol::span<unsigned char>) [with T = long unsigned int]"
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS1_4spanIhEE.part.0, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS1_4spanIhEE.part.0:
.LFB10047:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC11(%rip), %rcx
	movl	$1901, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10047:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS1_4spanIhEE.part.0, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS1_4spanIhEE.part.0
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"double node::inspector::protocol::cbor::CBORTokenizer::GetDouble() const"
	.align 8
.LC13:
	.string	"token_tag_ == CBORTokenTag::DOUBLE"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv.part.0, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv.part.0:
.LFB10058:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC12(%rip), %rcx
	movl	$2405, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10058:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv.part.0, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"void node::inspector::protocol::cbor::CBORTokenizer::EnterEnvelope()"
	.align 8
.LC15:
	.string	"token_tag_ == CBORTokenTag::ENVELOPE"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv.part.0, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv.part.0:
.LFB10059:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC14(%rip), %rcx
	movl	$2380, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10059:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv.part.0, .-_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"node::inspector::protocol::span<unsigned char> node::inspector::protocol::cbor::CBORTokenizer::GetString8() const"
	.align 8
.LC17:
	.string	"token_tag_ == CBORTokenTag::STRING8"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev.part.0, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev.part.0:
.LFB10061:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC16(%rip), %rcx
	movl	$2416, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10061:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev.part.0, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev.part.0
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"bool node::inspector::protocol::cbor::{anonymous}::ParseUTF8String(node::inspector::protocol::cbor::CBORTokenizer*, node::inspector::protocol::StreamingParserHandler*)"
	.align 8
.LC19:
	.string	"tokenizer->TokenTag() == CBORTokenTag::STRING8"
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_115ParseUTF8StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE.part.0, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_115ParseUTF8StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE.part.0:
.LFB10062:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC18(%rip), %rcx
	movl	$2670, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10062:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_115ParseUTF8StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE.part.0, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_115ParseUTF8StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE.part.0
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"node::inspector::protocol::span<unsigned char> node::inspector::protocol::cbor::CBORTokenizer::GetString16WireRep() const"
	.align 8
.LC21:
	.string	"token_tag_ == CBORTokenTag::STRING16"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv.part.0, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv.part.0:
.LFB10063:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC20(%rip), %rcx
	movl	$2422, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10063:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv.part.0, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"node::inspector::protocol::span<unsigned char> node::inspector::protocol::cbor::CBORTokenizer::GetBinary() const"
	.align 8
.LC23:
	.string	"token_tag_ == CBORTokenTag::BINARY"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv.part.0, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv.part.0:
.LFB10064:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC22(%rip), %rcx
	movl	$2428, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10064:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv.part.0, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv.part.0
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcherD0Ev
	.type	_ZN4node9inspector8protocol14UberDispatcherD0Ev, @function
_ZN4node9inspector8protocol14UberDispatcherD0Ev:
.LFB5613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol14UberDispatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L222
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L252:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L218
.L221:
	movq	%rbx, %r13
.L222:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L219
	movq	(%rdi), %rax
	call	*8(%rax)
.L219:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L252
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L221
.L218:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	32(%r12), %r13
	testq	%r13, %r13
	jne	.L228
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L253:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L224
.L227:
	movq	%rbx, %r13
.L228:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L253
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L227
.L224:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5613:
	.size	_ZN4node9inspector8protocol14UberDispatcherD0Ev, .-_ZN4node9inspector8protocol14UberDispatcherD0Ev
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleError(node::inspector::protocol::Status) [with C = std::vector<unsigned char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE.part.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE.part.0:
.LFB10455:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC24(%rip), %rcx
	movl	$3198, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE10455:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE.part.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE.part.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE:
.LFB9747:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L262
	movq	24(%rdi), %rax
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	16(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L256
	movq	%rdx, 8(%rax)
.L256:
	ret
.L262:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE.part.0
	.cfi_endproc
.LFE9747:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE
	.align 2
	.p2align 4
	.type	_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0, @function
_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0:
.LFB10527:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	$8, 8(%rdi)
	movl	$64, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, (%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 40(%rbx)
	movq	%rax, %xmm0
	leaq	512(%rax), %rdx
	movq	%rax, (%r12)
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 72(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10527:
	.size	_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0, .-_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"cannot create std::deque larger than max_size()"
	.text
	.align 2
	.p2align 4
	.type	_ZNSt5dequeIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE12emplace_backIJNS4_9ContainerEEEEvDpOT_, @function
_ZNSt5dequeIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE12emplace_backIJNS4_9ContainerEEEEvDpOT_:
.LFB8718:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %rcx
	movq	48(%rdi), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L266
	movl	(%rsi), %edx
	addq	$8, %rax
	movl	$0, -4(%rax)
	movl	%edx, -8(%rax)
	movq	%rax, 48(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	subq	56(%rdi), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	32(%rdi), %rax
	subq	16(%rdi), %rax
	movabsq	$1152921504606846975, %rdi
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L276
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L277
.L269:
	movl	$512, %edi
	call	_Znwm@PLT
	movl	(%r12), %edx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movl	%edx, (%rax)
	movq	72(%rbx), %rdx
	movl	$0, 4(%rax)
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L278
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rdi, %r14
	ja	.L279
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	40(%rbx), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r15
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L274
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L274:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L272:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L278:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L271
	cmpq	%r14, %rsi
	je	.L272
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L271:
	cmpq	%r14, %rsi
	je	.L272
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L272
.L276:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L279:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8718:
	.size	_ZNSt5dequeIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE12emplace_backIJNS4_9ContainerEEEEvDpOT_, .-_ZNSt5dequeIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE12emplace_backIJNS4_9ContainerEEEEvDpOT_
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0:
.LFB10512:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L281
	testq	%rsi, %rsi
	je	.L297
.L281:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L298
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L284
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L285:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L285
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L283:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L285
.L297:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10512:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB10513:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$208, %rsp
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L301
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L301:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rdx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rdx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L303
.L310:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L310
.L303:
	andl	$4095, %edx
	subq	%rdx, %rsp
	testq	%rdx, %rdx
	jne	.L311
.L304:
	leaq	15(%rsp), %r13
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movl	$1, %edx
	andq	$-16, %r13
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	$-1, %rcx
	movq	%r13, %rdi
	movl	$32, -224(%rbp)
	movl	$48, -220(%rbp)
	movq	%rax, -208(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, (%r12)
	movslq	%eax, %rdx
	addq	%r13, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	orq	$0, -8(%rsp,%rdx)
	jmp	.L304
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10513:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.1
.LC27:
	.string	"true"
.LC30:
	.string	"%d"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol16FundamentalValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol16FundamentalValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol16FundamentalValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L329
	cmpl	$3, %eax
	je	.L330
	cmpl	$2, %eax
	je	.L331
.L313:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L332
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movsd	16(%rdi), %xmm0
	movsd	.LC29(%rip), %xmm2
	movapd	%xmm0, %xmm1
	andpd	.LC28(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L333
	leaq	-64(%rbp), %rdi
	call	_ZN4node9inspector8protocol10StringUtil10fromDoubleB5cxx11Ed@PLT
.L328:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L313
	call	_ZdlPv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L329:
	cmpb	$0, 16(%rdi)
	je	.L315
	movl	$4, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$5, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$4, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L331:
	movl	16(%rdi), %r8d
	leaq	-64(%rbp), %r9
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC30(%rip), %rcx
	movq	%r9, %rdi
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L328
.L332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5362:
	.size	_ZNK4node9inspector8protocol16FundamentalValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol16FundamentalValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValueD0Ev
	.type	_ZN4node9inspector8protocol15DictionaryValueD0Ev, @function
_ZN4node9inspector8protocol15DictionaryValueD0Ev:
.LFB5383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	80(%rdi), %rbx
	movq	72(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L335
	.p2align 4,,10
	.p2align 3
.L339:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L336
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L339
.L337:
	movq	72(%r12), %r13
.L335:
	testq	%r13, %r13
	je	.L340
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L340:
	movq	32(%r12), %r13
	testq	%r13, %r13
	jne	.L341
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L346
.L347:
	movq	%rbx, %r13
.L341:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L344
	movq	(%rdi), %rax
	call	*24(%rax)
.L344:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L363
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L347
.L346:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L339
	jmp	.L337
	.cfi_endproc
.LFE5383:
	.size	_ZN4node9inspector8protocol15DictionaryValueD0Ev, .-_ZN4node9inspector8protocol15DictionaryValueD0Ev
	.section	.text._ZN4node9inspector8protocol15SerializedValueD0Ev,"axG",@progbits,_ZN4node9inspector8protocol15SerializedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol15SerializedValueD0Ev
	.type	_ZN4node9inspector8protocol15SerializedValueD0Ev, @function
_ZN4node9inspector8protocol15SerializedValueD0Ev:
.LFB7347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7347:
	.size	_ZN4node9inspector8protocol15SerializedValueD0Ev, .-_ZN4node9inspector8protocol15SerializedValueD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev:
.LFB5548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5548:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev
	.section	.text._ZN4node9inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD0Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD0Ev, @function
_ZN4node9inspector8protocol16InternalResponseD0Ev:
.LFB4818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L376
	movq	(%rdi), %rax
	call	*24(%rax)
.L376:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L377
	call	_ZdlPv@PLT
.L377:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4818:
	.size	_ZN4node9inspector8protocol16InternalResponseD0Ev, .-_ZN4node9inspector8protocol16InternalResponseD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcherD2Ev
	.type	_ZN4node9inspector8protocol14UberDispatcherD2Ev, @function
_ZN4node9inspector8protocol14UberDispatcherD2Ev:
.LFB5611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol14UberDispatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L387
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L417:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L383
.L386:
	movq	%r13, %r12
.L387:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L384
	movq	(%rdi), %rax
	call	*8(%rax)
.L384:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L417
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L386
.L383:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%rbx), %rdi
	leaq	120(%rbx), %rax
	movq	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	cmpq	%rax, %rdi
	je	.L388
	call	_ZdlPv@PLT
.L388:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	jne	.L393
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L418:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L389
.L392:
	movq	%r13, %r12
.L393:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	je	.L390
	call	_ZdlPv@PLT
.L390:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L418
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L392
.L389:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	addq	$64, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L382
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5611:
	.size	_ZN4node9inspector8protocol14UberDispatcherD2Ev, .-_ZN4node9inspector8protocol14UberDispatcherD2Ev
	.globl	_ZN4node9inspector8protocol14UberDispatcherD1Ev
	.set	_ZN4node9inspector8protocol14UberDispatcherD1Ev,_ZN4node9inspector8protocol14UberDispatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol9ListValueD2Ev
	.type	_ZN4node9inspector8protocol9ListValueD2Ev, @function
_ZN4node9inspector8protocol9ListValueD2Ev:
.LFB5442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %r13
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L420
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L421
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %r13
	jne	.L424
.L422:
	movq	16(%rbx), %r12
.L420:
	testq	%r12, %r12
	je	.L419
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L424
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L419:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5442:
	.size	_ZN4node9inspector8protocol9ListValueD2Ev, .-_ZN4node9inspector8protocol9ListValueD2Ev
	.globl	_ZN4node9inspector8protocol9ListValueD1Ev
	.set	_ZN4node9inspector8protocol9ListValueD1Ev,_ZN4node9inspector8protocol9ListValueD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15SerializedValue5cloneEv
	.type	_ZNK4node9inspector8protocol15SerializedValue5cloneEv, @function
_ZNK4node9inspector8protocol15SerializedValue5cloneEv:
.LFB5376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$72, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_Znwm@PLT
	movq	16(%r12), %rsi
	movq	24(%r12), %rdx
	movl	$8, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol15SerializedValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	addq	%rsi, %rdx
	movq	%rax, 16(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	pxor	%xmm0, %xmm0
	movq	56(%r12), %rax
	movq	$0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	subq	48(%r12), %rax
	je	.L428
	js	.L434
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_Znwm@PLT
	movq	48(%r12), %rsi
	movq	56(%r12), %r12
	movq	%rax, %xmm0
	movq	%rax, %rcx
	leaq	(%rax,%r14), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	subq	%rsi, %r12
	jne	.L435
.L430:
	addq	%r12, %rcx
	movq	%rbx, 0(%r13)
	movq	%r13, %rax
	movq	%rcx, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L428:
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	jmp	.L430
.L434:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5376:
	.size	_ZNK4node9inspector8protocol15SerializedValue5cloneEv, .-_ZNK4node9inspector8protocol15SerializedValue5cloneEv
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_, @function
_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_:
.LFB8613:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S6_PS6_
	movq	(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L516
	movzwl	(%rax), %ecx
	leal	-34(%rcx), %edx
	cmpw	$91, %dx
	ja	.L516
	leaq	.L439(%rip), %rdi
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L439:
	.long	.L449-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L448-.L439
	.long	.L447-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L447-.L439
	.long	.L446-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L445-.L439
	.long	.L516-.L439
	.long	.L444-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L483-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L484-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L485-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L516-.L439
	.long	.L440-.L439
	.long	.L516-.L439
	.long	.L438-.L439
	.text
	.p2align 4,,10
	.p2align 3
.L547:
	movzwl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$2, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L442
.L450:
	testb	%dl, %dl
	je	.L451
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$11, %r8d
.L436:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	cmpw	$45, %cx
	je	.L539
.L456:
	cmpq	%rax, %rsi
	jbe	.L516
	xorl	%edi, %edi
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L540:
	addq	$2, %rax
	addl	$1, %edi
	cmpq	%rax, %rsi
	jbe	.L458
.L459:
	movzwl	(%rax), %edx
	subl	$48, %edx
	cmpw	$9, %dx
	jbe	.L540
	movl	$11, %r8d
	testl	%edi, %edi
	je	.L436
.L458:
	cmpl	$1, %edi
	je	.L517
	cmpw	$48, %cx
	je	.L516
.L517:
	cmpq	%rax, %rsi
	je	.L537
	movzwl	(%rax), %edx
	cmpw	$46, %dx
	je	.L541
.L462:
	andl	$-33, %edx
	cmpw	$69, %dx
	jne	.L467
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L516
	movzwl	2(%rax), %ecx
	subl	$43, %ecx
	testw	$-3, %cx
	jne	.L468
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L516
.L468:
	cmpq	%rdx, %rsi
	jbe	.L516
	xorl	%ecx, %ecx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L542:
	addq	$2, %rdx
	addl	$1, %ecx
	cmpq	%rdx, %rsi
	jbe	.L498
.L470:
	movzwl	(%rdx), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L542
	movl	$11, %r8d
	testl	%ecx, %ecx
	je	.L436
.L498:
	movq	%rdx, %rax
.L467:
	movq	%rax, (%r9)
	movl	$5, %r8d
	jmp	.L436
.L446:
	addq	$2, %rax
	movl	$10, %r8d
	movq	%rax, (%r9)
	jmp	.L436
.L445:
	addq	$2, %rax
	movl	$2, %r8d
	movq	%rax, (%r9)
	jmp	.L436
.L444:
	addq	$2, %rax
	movl	$3, %r8d
	movq	%rax, (%r9)
	jmp	.L436
.L440:
	addq	$2, %rax
	xorl	%r8d, %r8d
	movq	%rax, (%r9)
	jmp	.L436
.L438:
	addq	$2, %rax
	movl	$1, %r8d
	movq	%rax, (%r9)
	jmp	.L436
.L449:
	addq	$2, %rax
	cmpq	%rsi, %rax
	jnb	.L516
	movzwl	(%rax), %edx
	leaq	.L475(%rip), %rdi
	leaq	2(%rax), %rcx
	cmpw	$92, %dx
	je	.L543
	.p2align 4,,10
	.p2align 3
.L473:
	cmpw	$34, %dx
	je	.L544
.L515:
	movq	%rcx, %rax
	cmpq	%rcx, %rsi
	jbe	.L516
	movzwl	(%rax), %edx
	leaq	2(%rax), %rcx
	cmpw	$92, %dx
	jne	.L473
.L543:
	cmpq	%rcx, %rsi
	je	.L516
	movzwl	2(%rax), %edx
	leaq	4(%rax), %rcx
	subl	$34, %edx
	cmpw	$86, %dx
	ja	.L516
	movzwl	%dx, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L475:
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L516-.L475
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L515-.L475
	.long	.L476-.L475
	.long	.L515-.L475
	.long	.L516-.L475
	.long	.L474-.L475
	.text
.L448:
	addq	$2, %rax
	movl	$9, %r8d
	movq	%rax, (%r9)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L539:
	leaq	2(%rax), %rdx
	movl	$11, %r8d
	cmpq	%rdx, %rsi
	je	.L436
	movzwl	2(%rax), %ecx
	movq	%rdx, %rax
	jmp	.L456
.L483:
	movl	$102, %edx
	leaq	.LC3(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L443:
	cmpq	%rax, %rsi
	jbe	.L454
	testb	%dl, %dl
	jne	.L545
.L455:
	movq	%rax, (%r9)
	movl	$7, %r8d
	jmp	.L436
.L485:
	movl	$116, %edx
	leaq	.LC27(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L441:
	cmpq	%rax, %rsi
	jbe	.L452
	testb	%dl, %dl
	jne	.L546
.L453:
	movq	%rax, (%r9)
	movl	$6, %r8d
	jmp	.L436
.L484:
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L442:
	cmpq	%rax, %rsi
	jbe	.L450
	testb	%dl, %dl
	jne	.L547
.L451:
	movq	%rax, (%r9)
	movl	$8, %r8d
	jmp	.L436
.L474:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$2, %rdx
	jle	.L516
	movzwl	4(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L477
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L516
.L477:
	leaq	8(%rax), %rcx
	movzwl	6(%rax), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L515
.L538:
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	jbe	.L515
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L476:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$6, %rdx
	jle	.L516
	movzwl	4(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L478
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L516
.L478:
	movzwl	6(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L479
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L516
.L479:
	movzwl	8(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L480
	andl	$-33, %edx
	subl	$65, %edx
	cmpw	$5, %dx
	ja	.L516
.L480:
	leaq	12(%rax), %rcx
	movzwl	10(%rax), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L515
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L545:
	movzwl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$2, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L443
.L454:
	testb	%dl, %dl
	jne	.L516
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L546:
	movzwl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$2, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L441
.L452:
	testb	%dl, %dl
	jne	.L516
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%rcx, (%r9)
	movl	$4, %r8d
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L541:
	addq	$2, %rax
	cmpq	%rax, %rsi
	je	.L516
	jbe	.L516
	xorl	%ecx, %ecx
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L548:
	addq	$2, %rax
	addl	$1, %ecx
	cmpq	%rax, %rsi
	jbe	.L464
.L465:
	movzwl	(%rax), %edi
	leal	-48(%rdi), %edx
	cmpw	$9, %dx
	jbe	.L548
	movl	$11, %r8d
	testl	%ecx, %ecx
	je	.L436
.L464:
	cmpq	%rax, %rsi
	je	.L537
	movzwl	(%rax), %edx
	jmp	.L462
.L537:
	movq	%rsi, (%r9)
	movl	$5, %r8d
	jmp	.L436
	.cfi_endproc
.LFE8613:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_, .-_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_, @function
_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_:
.LFB8618:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S6_PS6_
	movq	(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L631
	movzbl	(%rax), %ecx
	leal	-34(%rcx), %edx
	cmpb	$91, %dl
	ja	.L631
	leaq	.L552(%rip), %rdi
	movzbl	%dl, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L552:
	.long	.L562-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L561-.L552
	.long	.L560-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L560-.L552
	.long	.L559-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L558-.L552
	.long	.L631-.L552
	.long	.L557-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L598-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L599-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L600-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L631-.L552
	.long	.L553-.L552
	.long	.L631-.L552
	.long	.L551-.L552
	.text
	.p2align 4,,10
	.p2align 3
.L667:
	movzbl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$1, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L555
.L563:
	testb	%dl, %dl
	je	.L564
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$11, %r8d
.L549:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	cmpb	$45, %cl
	je	.L657
.L569:
	cmpq	%rax, %rsi
	jbe	.L631
	xorl	%edi, %edi
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L659:
	addq	$1, %rax
	addl	$1, %edi
	cmpq	%rax, %rsi
	je	.L658
.L572:
	movzbl	(%rax), %edx
	subl	$48, %edx
	cmpb	$9, %dl
	jbe	.L659
	movl	$11, %r8d
	testl	%edi, %edi
	je	.L549
	cmpl	$1, %edi
	je	.L633
	cmpb	$48, %cl
	je	.L549
.L633:
	cmpq	%rax, %rsi
	je	.L577
	movzbl	(%rax), %edx
	cmpb	$46, %dl
	je	.L660
.L575:
	andl	$-33, %edx
	cmpb	$69, %dl
	jne	.L580
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L631
	movzbl	1(%rax), %ecx
	subl	$43, %ecx
	andl	$253, %ecx
	jne	.L581
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L631
.L581:
	cmpq	%rdx, %rsi
	jbe	.L631
	xorl	%ecx, %ecx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L662:
	addq	$1, %rdx
	addl	$1, %ecx
	cmpq	%rdx, %rsi
	je	.L661
.L583:
	movzbl	(%rdx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L662
	movl	$11, %r8d
	movq	%rdx, %rax
	testl	%ecx, %ecx
	je	.L549
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%rax, (%r9)
	movl	$5, %r8d
	jmp	.L549
.L559:
	addq	$1, %rax
	movl	$10, %r8d
	movq	%rax, (%r9)
	jmp	.L549
.L558:
	addq	$1, %rax
	movl	$2, %r8d
	movq	%rax, (%r9)
	jmp	.L549
.L557:
	addq	$1, %rax
	movl	$3, %r8d
	movq	%rax, (%r9)
	jmp	.L549
.L553:
	addq	$1, %rax
	xorl	%r8d, %r8d
	movq	%rax, (%r9)
	jmp	.L549
.L551:
	addq	$1, %rax
	movl	$1, %r8d
	movq	%rax, (%r9)
	jmp	.L549
.L562:
	addq	$1, %rax
	cmpq	%rsi, %rax
	jnb	.L631
	movzbl	(%rax), %edx
	leaq	.L588(%rip), %rdi
	leaq	1(%rax), %rcx
	cmpb	$92, %dl
	je	.L663
	.p2align 4,,10
	.p2align 3
.L586:
	cmpb	$34, %dl
	je	.L664
.L630:
	movq	%rcx, %rax
	cmpq	%rcx, %rsi
	jbe	.L631
	movzbl	(%rax), %edx
	leaq	1(%rax), %rcx
	cmpb	$92, %dl
	jne	.L586
.L663:
	cmpq	%rcx, %rsi
	je	.L631
	movzbl	1(%rax), %edx
	leaq	2(%rax), %rcx
	subl	$34, %edx
	cmpb	$86, %dl
	ja	.L631
	movzbl	%dl, %edx
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L588:
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L631-.L588
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L630-.L588
	.long	.L589-.L588
	.long	.L630-.L588
	.long	.L631-.L588
	.long	.L587-.L588
	.text
.L561:
	addq	$1, %rax
	movl	$9, %r8d
	movq	%rax, (%r9)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L657:
	leaq	1(%rax), %rdx
	movl	$11, %r8d
	cmpq	%rdx, %rsi
	je	.L549
	movzbl	1(%rax), %ecx
	movq	%rdx, %rax
	jmp	.L569
.L598:
	movl	$102, %edx
	leaq	.LC3(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L556:
	cmpq	%rax, %rsi
	jbe	.L567
	testb	%dl, %dl
	jne	.L665
.L568:
	movq	%rax, (%r9)
	movl	$7, %r8d
	jmp	.L549
.L600:
	movl	$116, %edx
	leaq	.LC27(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L554:
	cmpq	%rax, %rsi
	jbe	.L565
	testb	%dl, %dl
	jne	.L666
.L566:
	movq	%rax, (%r9)
	movl	$6, %r8d
	jmp	.L549
.L599:
	movl	$110, %edx
	leaq	.LC0(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L555:
	cmpq	%rax, %rsi
	jbe	.L563
	testb	%dl, %dl
	jne	.L667
.L564:
	movq	%rax, (%r9)
	movl	$8, %r8d
	jmp	.L549
.L587:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$1, %rdx
	jle	.L631
	movzbl	2(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L590
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L631
.L590:
	leaq	4(%rax), %rcx
	movzbl	3(%rax), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L630
.L656:
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	jbe	.L630
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L589:
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	cmpq	$3, %rdx
	jle	.L631
	movzbl	2(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L591
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L631
.L591:
	movzbl	3(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L592
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L631
.L592:
	movzbl	4(%rax), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L593
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L631
.L593:
	leaq	6(%rax), %rcx
	movzbl	5(%rax), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L630
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L665:
	movzbl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$1, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L556
.L567:
	testb	%dl, %dl
	jne	.L631
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L666:
	movzbl	(%rax), %r8d
	addq	$1, %rcx
	movsbl	%dl, %edi
	addq	$1, %rax
	movzbl	(%rcx), %edx
	cmpl	%edi, %r8d
	je	.L554
.L565:
	testb	%dl, %dl
	jne	.L631
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%rcx, (%r9)
	movl	$4, %r8d
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L658:
	cmpl	$1, %edi
	je	.L577
	movl	$11, %r8d
	cmpb	$48, %cl
	je	.L549
.L577:
	movq	%rsi, (%r9)
	movl	$5, %r8d
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L660:
	addq	$1, %rax
	cmpq	%rax, %rsi
	je	.L631
	jbe	.L631
	xorl	%ecx, %ecx
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L668:
	addq	$1, %rax
	addl	$1, %ecx
	cmpq	%rax, %rsi
	je	.L577
.L578:
	movzbl	(%rax), %edx
	leal	-48(%rdx), %edi
	cmpb	$9, %dil
	jbe	.L668
	movl	$11, %r8d
	testl	%ecx, %ecx
	je	.L549
	cmpq	%rax, %rsi
	jne	.L575
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L661:
	movq	%rsi, %rax
	jmp	.L580
	.cfi_endproc
.LFE8618:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_, .-_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValueD2Ev
	.type	_ZN4node9inspector8protocol15DictionaryValueD2Ev, @function
_ZN4node9inspector8protocol15DictionaryValueD2Ev:
.LFB5381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r13
	movq	72(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L670
	.p2align 4,,10
	.p2align 3
.L674:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L671
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L674
.L672:
	movq	72(%rbx), %r12
.L670:
	testq	%r12, %r12
	je	.L675
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L675:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	jne	.L676
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L698:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L681
.L682:
	movq	%r13, %r12
.L676:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L679
	movq	(%rdi), %rax
	call	*24(%rax)
.L679:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L698
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L682
.L681:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	addq	$64, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L699
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L674
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L699:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5381:
	.size	_ZN4node9inspector8protocol15DictionaryValueD2Ev, .-_ZN4node9inspector8protocol15DictionaryValueD2Ev
	.globl	_ZN4node9inspector8protocol15DictionaryValueD1Ev
	.set	_ZN4node9inspector8protocol15DictionaryValueD1Ev,_ZN4node9inspector8protocol15DictionaryValueD2Ev
	.p2align 4
	.type	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelENS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0, @function
_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelENS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0:
.LFB10232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$88, %edi
	movq	24(%rax), %r15
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	8(%r13), %rdx
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	addq	%rsi, %rdx
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rbx)
	movl	%r14d, 8(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	64(%rbx), %rax
	movq	%r12, %rdi
	movb	$0, 64(%rbx)
	movq	%rax, 48(%rbx)
	leaq	-64(%rbp), %rsi
	movq	$0, 56(%rbx)
	movl	$0, 80(%rbx)
	movb	$0, 84(%rbx)
	movq	%rbx, -64(%rbp)
	call	*%r15
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L700
	movq	(%rdi), %rax
	call	*24(%rax)
.L700:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L707
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L707:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10232:
	.size	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelENS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0, .-_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelENS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11StringValue5cloneEv
	.type	_ZNK4node9inspector8protocol11StringValue5cloneEv, @function
_ZNK4node9inspector8protocol11StringValue5cloneEv:
.LFB5369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movq	16(%r13), %rsi
	movq	24(%r13), %rdx
	movl	$4, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	addq	%rsi, %rdx
	movq	%rax, 16(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	%rbx, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5369:
	.size	_ZNK4node9inspector8protocol11StringValue5cloneEv, .-_ZNK4node9inspector8protocol11StringValue5cloneEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0:
.LFB10456:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	call	strlen@PLT
	movq	8(%r12), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %r8
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	.cfi_endproc
.LFE10456:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB9701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L712
	movq	104(%rbx), %rax
	movq	72(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L714
	.p2align 4,,10
	.p2align 3
.L715:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L715
	movq	32(%rbx), %rdi
.L714:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9701:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.set	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev:
.LFB9705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L718
	movq	104(%rbx), %rax
	movq	72(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L720
	.p2align 4,,10
	.p2align 3
.L721:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L721
	movq	32(%rbx), %rdi
.L720:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L718:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9705:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev
	.set	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED1Ev,_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB9703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L725
	movq	104(%r12), %rax
	movq	72(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L726
	.p2align 4,,10
	.p2align 3
.L727:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L727
	movq	32(%r12), %rdi
.L726:
	call	_ZdlPv@PLT
.L725:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$112, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9703:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_:
.LFB8719:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L734
	.p2align 4,,10
	.p2align 3
.L735:
	movzbl	(%rdi), %eax
	cmpb	$32, %al
	je	.L736
	leal	-9(%rax), %r8d
	cmpb	$4, %r8b
	jbe	.L736
	cmpb	$47, %al
	jne	.L734
	cmpq	%rdi, %rsi
	je	.L742
	leaq	1(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L740
	movzbl	1(%rdi), %eax
	cmpb	$47, %al
	je	.L861
	cmpb	$42, %al
	je	.L862
.L740:
	movq	%rdi, (%rdx)
.L791:
	movzbl	(%rdi), %edx
	leal	-34(%rdx), %eax
	cmpb	$91, %al
	ja	.L829
	leaq	.L749(%rip), %r8
	movzbl	%al, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L749:
	.long	.L759-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L758-.L749
	.long	.L757-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L757-.L749
	.long	.L756-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L755-.L749
	.long	.L829-.L749
	.long	.L754-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L796-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L797-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L798-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L829-.L749
	.long	.L750-.L749
	.long	.L829-.L749
	.long	.L748-.L749
	.text
	.p2align 4,,10
	.p2align 3
.L872:
	movzbl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$1, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L752
.L760:
	testb	%al, %al
	je	.L761
.L829:
	movl	$11, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	addq	$1, %rdi
.L738:
	cmpq	%rdi, %rsi
	ja	.L735
.L734:
	movq	%rdi, (%rdx)
	cmpq	%rdi, %rsi
	jne	.L791
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L861:
	addq	$2, %rdi
	cmpq	%rdi, %rsi
	jbe	.L742
	.p2align 4,,10
	.p2align 3
.L743:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	cmpb	$10, %al
	je	.L738
	cmpb	$13, %al
	je	.L738
	cmpq	%rdi, %rsi
	jne	.L743
	.p2align 4,,10
	.p2align 3
.L742:
	movq	%rsi, (%rdx)
.L858:
	movl	$12, %eax
.L733:
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	2(%rdi), %r8
	cmpq	%r8, %rsi
	ja	.L744
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L745:
	movq	%rax, %r8
.L744:
	leaq	1(%r8), %rax
	movzbl	-1(%rax), %r9d
	cmpq	%rax, %rsi
	je	.L740
	cmpb	$42, %r9b
	jne	.L745
	cmpb	$47, (%rax)
	jne	.L745
	leaq	2(%r8), %rdi
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L757:
	cmpb	$45, %dl
	je	.L863
.L766:
	cmpq	%rdi, %rsi
	jbe	.L829
	xorl	%r8d, %r8d
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L865:
	addq	$1, %rdi
	addl	$1, %r8d
	cmpq	%rdi, %rsi
	je	.L864
.L769:
	movzbl	(%rdi), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L865
	movl	$11, %eax
	testl	%r8d, %r8d
	je	.L733
	cmpb	$48, %dl
	jne	.L831
	cmpl	$1, %r8d
	jne	.L733
.L831:
	cmpq	%rdi, %rsi
	je	.L774
	movzbl	(%rdi), %edx
	cmpb	$46, %dl
	je	.L866
.L772:
	andl	$-33, %edx
	cmpb	$69, %dl
	jne	.L777
	leaq	1(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L829
	movzbl	1(%rdi), %eax
	subl	$43, %eax
	testb	$-3, %al
	jne	.L778
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L829
.L778:
	cmpq	%rdx, %rsi
	jbe	.L829
	xorl	%edi, %edi
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L868:
	addq	$1, %rdx
	addl	$1, %edi
	cmpq	%rdx, %rsi
	je	.L867
.L780:
	movzbl	(%rdx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L868
	movl	$11, %eax
	testl	%edi, %edi
	je	.L733
	movq	%rdx, %rdi
.L777:
	movq	%rdi, (%rcx)
	movl	$5, %eax
	ret
.L754:
	addq	$1, %rdi
	movl	$3, %eax
	movq	%rdi, (%rcx)
	ret
.L756:
	addq	$1, %rdi
	movl	$10, %eax
	movq	%rdi, (%rcx)
	ret
.L759:
	addq	$1, %rdi
	cmpq	%rsi, %rdi
	jnb	.L829
	leaq	.L785(%rip), %r8
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L783:
	cmpb	$34, %al
	je	.L869
.L828:
	movq	%rdx, %rdi
.L782:
	cmpq	%rdi, %rsi
	jbe	.L829
.L781:
	movzbl	(%rdi), %eax
	leaq	1(%rdi), %rdx
	cmpb	$92, %al
	jne	.L783
	cmpq	%rdx, %rsi
	je	.L829
	movzbl	1(%rdi), %eax
	leaq	2(%rdi), %rdx
	subl	$34, %eax
	cmpb	$86, %al
	ja	.L829
	movzbl	%al, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L785:
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L829-.L785
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L828-.L785
	.long	.L786-.L785
	.long	.L828-.L785
	.long	.L829-.L785
	.long	.L784-.L785
	.text
.L758:
	addq	$1, %rdi
	movl	$9, %eax
	movq	%rdi, (%rcx)
	ret
.L748:
	addq	$1, %rdi
	movl	$1, %eax
	movq	%rdi, (%rcx)
	ret
.L750:
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, (%rcx)
	ret
.L755:
	addq	$1, %rdi
	movl	$2, %eax
	movq	%rdi, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L863:
	leaq	1(%rdi), %r8
	movl	$11, %eax
	cmpq	%r8, %rsi
	je	.L733
	movzbl	1(%rdi), %edx
	movq	%r8, %rdi
	jmp	.L766
.L796:
	movl	$102, %eax
	leaq	.LC3(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L753:
	cmpq	%rdi, %rsi
	jbe	.L764
	testb	%al, %al
	jne	.L870
.L765:
	movq	%rdi, (%rcx)
	movl	$7, %eax
	ret
.L798:
	movl	$116, %eax
	leaq	.LC27(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L751:
	cmpq	%rdi, %rsi
	jbe	.L762
	testb	%al, %al
	jne	.L871
.L763:
	movq	%rdi, (%rcx)
	movl	$6, %eax
	ret
.L797:
	movl	$110, %eax
	leaq	.LC0(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L752:
	cmpq	%rdi, %rsi
	jbe	.L760
	testb	%al, %al
	jne	.L872
.L761:
	movq	%rdi, (%rcx)
	movl	$8, %eax
	ret
.L784:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$1, %rax
	jle	.L829
	movzbl	2(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L787
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L829
.L787:
	movzbl	3(%rdi), %eax
	leaq	4(%rdi), %r9
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L857
.L860:
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L829
.L857:
	movq	%r9, %rdi
	jmp	.L782
.L786:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$3, %rax
	jle	.L829
	movzbl	2(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L788
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L829
.L788:
	movzbl	3(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L789
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L829
.L789:
	movzbl	4(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L790
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L829
.L790:
	movzbl	5(%rdi), %eax
	leaq	6(%rdi), %r9
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L857
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L870:
	movzbl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$1, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L753
.L764:
	testb	%al, %al
	jne	.L829
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L871:
	movzbl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$1, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L751
.L762:
	testb	%al, %al
	jne	.L829
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L869:
	movq	%rdx, (%rcx)
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	cmpl	$1, %r8d
	je	.L774
	movl	$11, %eax
	cmpb	$48, %dl
	je	.L733
.L774:
	movq	%rsi, (%rcx)
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	je	.L829
	jbe	.L829
	xorl	%r8d, %r8d
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L873:
	addq	$1, %rdi
	addl	$1, %r8d
	cmpq	%rdi, %rsi
	je	.L774
.L775:
	movzbl	(%rdi), %edx
	leal	-48(%rdx), %eax
	cmpb	$9, %al
	jbe	.L873
	movl	$11, %eax
	testl	%r8d, %r8d
	je	.L733
	cmpq	%rdi, %rsi
	jne	.L772
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L867:
	movq	%rsi, %rdi
	jmp	.L777
	.cfi_endproc
.LFE8719:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_:
.LFB8723:
	.cfi_startproc
	cmpq	%rsi, %rdi
	jnb	.L875
	.p2align 4,,10
	.p2align 3
.L876:
	movzwl	(%rdi), %eax
	cmpw	$32, %ax
	je	.L877
	leal	-9(%rax), %r8d
	cmpw	$4, %r8w
	jbe	.L877
	cmpw	$47, %ax
	jne	.L875
	cmpq	%rdi, %rsi
	je	.L883
	leaq	2(%rdi), %rax
	cmpq	%rax, %rsi
	jbe	.L881
	movzwl	2(%rdi), %eax
	cmpw	$47, %ax
	je	.L996
	cmpw	$42, %ax
	je	.L997
.L881:
	movq	%rdi, (%rdx)
.L966:
	movl	$11, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	addq	$2, %rdi
.L879:
	cmpq	%rdi, %rsi
	ja	.L876
.L875:
	movq	%rdi, (%rdx)
	cmpq	%rdi, %rsi
	je	.L992
	movzwl	(%rdi), %r8d
	leal	-34(%r8), %eax
	cmpw	$91, %ax
	ja	.L966
	leaq	.L891(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L891:
	.long	.L901-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L900-.L891
	.long	.L899-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L899-.L891
	.long	.L898-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L897-.L891
	.long	.L966-.L891
	.long	.L896-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L934-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L935-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L936-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L966-.L891
	.long	.L892-.L891
	.long	.L966-.L891
	.long	.L890-.L891
	.text
	.p2align 4,,10
	.p2align 3
.L996:
	addq	$4, %rdi
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L998:
	movzwl	(%rdi), %eax
	addq	$2, %rdi
	cmpw	$10, %ax
	je	.L879
	cmpw	$13, %ax
	je	.L879
.L990:
	cmpq	%rdi, %rsi
	ja	.L998
.L883:
	movq	%rsi, (%rdx)
.L992:
	movl	$12, %eax
.L874:
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	leaq	4(%rdi), %r8
	cmpq	%r8, %rsi
	ja	.L885
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L886:
	movq	%rax, %r8
.L885:
	leaq	2(%r8), %rax
	movzwl	-2(%rax), %r9d
	cmpq	%rax, %rsi
	jbe	.L881
	cmpw	$42, %r9w
	jne	.L886
	cmpw	$47, (%rax)
	jne	.L886
	leaq	4(%r8), %rdi
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L899:
	cmpw	$45, %r8w
	je	.L999
.L908:
	cmpq	%rdi, %rsi
	jbe	.L966
	xorl	%edx, %edx
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L1000:
	addq	$2, %rdi
	addl	$1, %edx
	cmpq	%rdi, %rsi
	jbe	.L910
.L911:
	movzwl	(%rdi), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L1000
	movl	$11, %eax
	testl	%edx, %edx
	je	.L874
.L910:
	cmpw	$48, %r8w
	jne	.L967
	cmpl	$1, %edx
	jne	.L966
.L967:
	cmpq	%rdi, %rsi
	je	.L993
	movzwl	(%rdi), %eax
	cmpw	$46, %ax
	je	.L1001
.L914:
	andl	$-33, %eax
	cmpw	$69, %ax
	jne	.L919
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L966
	movzwl	2(%rdi), %eax
	subl	$43, %eax
	testw	$-3, %ax
	jne	.L920
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L966
.L920:
	cmpq	%rdx, %rsi
	jbe	.L966
	xorl	%edi, %edi
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1002:
	addq	$2, %rdx
	addl	$1, %edi
	cmpq	%rdx, %rsi
	jbe	.L949
.L922:
	movzwl	(%rdx), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L1002
	movl	$11, %eax
	testl	%edi, %edi
	je	.L874
.L949:
	movq	%rdx, %rdi
.L919:
	movq	%rdi, (%rcx)
	movl	$5, %eax
	ret
.L900:
	addq	$2, %rdi
	movl	$9, %eax
	movq	%rdi, (%rcx)
	ret
.L901:
	addq	$2, %rdi
	cmpq	%rsi, %rdi
	jnb	.L966
	leaq	.L927(%rip), %r8
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L925:
	cmpw	$34, %ax
	je	.L1003
.L965:
	movq	%rdx, %rdi
.L924:
	cmpq	%rdi, %rsi
	jbe	.L966
.L923:
	movzwl	(%rdi), %eax
	leaq	2(%rdi), %rdx
	cmpw	$92, %ax
	jne	.L925
	cmpq	%rdx, %rsi
	je	.L966
	movzwl	2(%rdi), %eax
	leaq	4(%rdi), %rdx
	subl	$34, %eax
	cmpw	$86, %ax
	ja	.L966
	movzwl	%ax, %eax
	movslq	(%r8,%rax,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L927:
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L966-.L927
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L965-.L927
	.long	.L928-.L927
	.long	.L965-.L927
	.long	.L966-.L927
	.long	.L926-.L927
	.text
.L890:
	addq	$2, %rdi
	movl	$1, %eax
	movq	%rdi, (%rcx)
	ret
.L892:
	addq	$2, %rdi
	xorl	%eax, %eax
	movq	%rdi, (%rcx)
	ret
.L896:
	addq	$2, %rdi
	movl	$3, %eax
	movq	%rdi, (%rcx)
	ret
.L897:
	addq	$2, %rdi
	movl	$2, %eax
	movq	%rdi, (%rcx)
	ret
.L898:
	addq	$2, %rdi
	movl	$10, %eax
	movq	%rdi, (%rcx)
	ret
.L934:
	movl	$102, %eax
	leaq	.LC3(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L895:
	cmpq	%rdi, %rsi
	jbe	.L906
	testb	%al, %al
	jne	.L1004
.L907:
	movq	%rdi, (%rcx)
	movl	$7, %eax
	ret
.L936:
	movl	$116, %eax
	leaq	.LC27(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L893:
	cmpq	%rdi, %rsi
	jbe	.L904
	testb	%al, %al
	jne	.L1005
.L905:
	movq	%rdi, (%rcx)
	movl	$6, %eax
	ret
.L935:
	movl	$110, %eax
	leaq	.LC0(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L894:
	cmpq	%rdi, %rsi
	jbe	.L902
	testb	%al, %al
	jne	.L1006
.L903:
	movq	%rdi, (%rcx)
	movl	$8, %eax
	ret
.L926:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$2, %rax
	jle	.L966
	movzwl	4(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L929
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L966
.L929:
	movzwl	6(%rdi), %eax
	leaq	8(%rdi), %r9
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L991
.L995:
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L966
.L991:
	movq	%r9, %rdi
	jmp	.L924
.L928:
	movq	%rsi, %rax
	subq	%rdx, %rax
	cmpq	$6, %rax
	jle	.L966
	movzwl	4(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L930
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L966
.L930:
	movzwl	6(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L931
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L966
.L931:
	movzwl	8(%rdi), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L932
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$5, %ax
	ja	.L966
.L932:
	movzwl	10(%rdi), %eax
	leaq	12(%rdi), %r9
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L991
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1005:
	movzwl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$2, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L893
.L904:
	testb	%al, %al
	jne	.L966
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L1006:
	movzwl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$2, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L894
.L902:
	testb	%al, %al
	jne	.L966
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1004:
	movzwl	(%rdi), %r9d
	addq	$1, %rdx
	movsbl	%al, %r8d
	addq	$2, %rdi
	movzbl	(%rdx), %eax
	cmpl	%r8d, %r9d
	je	.L895
.L906:
	testb	%al, %al
	jne	.L966
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	%rdx, (%rcx)
	movl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	leaq	2(%rdi), %rdx
	movl	$11, %eax
	cmpq	%rdx, %rsi
	je	.L874
	movzwl	2(%rdi), %r8d
	movq	%rdx, %rdi
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L1001:
	addq	$2, %rdi
	cmpq	%rdi, %rsi
	je	.L966
	jbe	.L966
	xorl	%edx, %edx
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L1007:
	addq	$2, %rdi
	addl	$1, %edx
	cmpq	%rdi, %rsi
	jbe	.L916
.L917:
	movzwl	(%rdi), %eax
	subl	$48, %eax
	cmpw	$9, %ax
	jbe	.L1007
	movl	$11, %eax
	testl	%edx, %edx
	je	.L874
.L916:
	cmpq	%rdi, %rsi
	je	.L993
	movzwl	(%rdi), %eax
	jmp	.L914
.L993:
	movq	%rsi, (%rcx)
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE8723:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	.section	.rodata.str1.1
.LC31:
	.string	"vector::_M_range_insert"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK4node9inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK4node9inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %r12
	movq	48(%rdi), %r13
	movq	8(%rsi), %r14
	cmpq	%r13, %r12
	je	.L1008
	movq	16(%rsi), %rax
	subq	%r13, %r12
	movq	%rsi, %rbx
	subq	%r14, %rax
	cmpq	%rax, %r12
	ja	.L1010
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	addq	%r12, 8(%rbx)
.L1008:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rsi), %r9
	movq	%r14, %rdx
	movq	%rcx, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L1030
	cmpq	%r12, %rdx
	movq	%r12, %rax
	cmovnb	%rdx, %rax
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L1022
	testq	%rax, %rax
	js	.L1022
	jne	.L1014
	movq	$0, -56(%rbp)
	xorl	%r15d, %r15d
.L1020:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L1031
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L1016
.L1018:
	testq	%r9, %r9
	jne	.L1017
.L1019:
	movq	-56(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1022:
	.cfi_restore_state
	movq	%rcx, %r15
.L1014:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %r15
	subq	%r9, %rdx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-64(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L1016
.L1017:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	jmp	.L1018
.L1030:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5375:
	.size	_ZNK4node9inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK4node9inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvtPT_, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvtPT_:
.LFB9778:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$48, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$87, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	16(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movzwl	%di, %r12d
	pushq	%rbx
	movl	%r12d, %eax
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	sarl	$12, %eax
	subq	$24, %rsp
	cmpl	$40959, %r12d
	cmovbe	%edx, %r15d
	movq	(%rsi), %rdx
	addl	%eax, %r15d
	movq	8(%rsi), %rax
	leaq	1(%rax), %r9
	cmpq	%r14, %rdx
	je	.L1046
	movq	16(%rsi), %rcx
.L1034:
	cmpq	%r9, %rcx
	jb	.L1054
.L1035:
	movb	%r15b, (%rdx,%rax)
	movl	%r12d, %r15d
	movq	(%rbx), %rdx
	sarl	$8, %r15d
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rdx,%rax)
	movl	%r15d, %eax
	movl	$48, %edx
	movl	$87, %r15d
	andl	$15, %eax
	cmpl	$9, %eax
	cmovle	%edx, %r15d
	movq	(%rbx), %rdx
	addl	%eax, %r15d
	movq	8(%rbx), %rax
	leaq	1(%rax), %r9
	cmpq	%rdx, %r14
	je	.L1048
	movq	16(%rbx), %rcx
.L1037:
	cmpq	%rcx, %r9
	ja	.L1055
.L1038:
	movb	%r15b, (%rdx,%rax)
	sarl	$4, %r12d
	movq	(%rbx), %rdx
	andl	$15, %r12d
	movq	%r9, 8(%rbx)
	cmpl	$9, %r12d
	movb	$0, 1(%rdx,%rax)
	movl	$87, %eax
	movl	$48, %edx
	cmovle	%edx, %eax
	movq	8(%rbx), %r15
	addl	%eax, %r12d
	movq	(%rbx), %rax
	leaq	1(%r15), %r9
	cmpq	%rax, %r14
	je	.L1050
	movq	16(%rbx), %rdx
.L1040:
	cmpq	%rdx, %r9
	ja	.L1056
.L1041:
	movb	%r12b, (%rax,%r15)
	movq	(%rbx), %rax
	movl	$48, %r12d
	movl	$87, %edx
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rax,%r15)
	movl	%r13d, %eax
	andl	$15, %r13d
	andl	$15, %eax
	cmpl	$10, %r13d
	movq	8(%rbx), %r13
	cmovge	%edx, %r12d
	leaq	1(%r13), %r15
	addl	%eax, %r12d
	movq	(%rbx), %rax
	cmpq	%r14, %rax
	je	.L1052
	movq	16(%rbx), %rdx
.L1043:
	cmpq	%rdx, %r15
	ja	.L1057
.L1044:
	movb	%r12b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1054:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1057:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1055:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1050:
	movl	$15, %edx
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1052:
	movl	$15, %edx
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1046:
	movl	$15, %ecx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1048:
	movl	$15, %ecx
	jmp	.L1037
	.cfi_endproc
.LFE9778:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvtPT_, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvtPT_
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0:
.LFB10491:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	call	strlen@PLT
	testq	%rax, %rax
	je	.L1058
	movq	8(%r14), %r13
	movq	%rax, %r12
	movq	%rax, %r11
	movq	16(%r14), %rax
	subq	%r13, %rax
	cmpq	%rax, %r12
	ja	.L1060
	leaq	15(%rbx), %rax
	subq	%r13, %rax
	cmpq	$30, %rax
	jbe	.L1076
	leaq	-1(%r12), %rax
	cmpq	$14, %rax
	jbe	.L1076
	movq	%r12, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
.L1062:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1062
	movq	%r12, %rdx
	movq	%r12, %rax
	andq	$-16, %rdx
	andl	$15, %eax
	addq	%rdx, %rbx
	addq	%rdx, %r13
	cmpq	%rdx, %r12
	je	.L1064
	movzbl	(%rbx), %edx
	movb	%dl, 0(%r13)
	cmpq	$1, %rax
	je	.L1064
	movzbl	1(%rbx), %edx
	movb	%dl, 1(%r13)
	cmpq	$2, %rax
	je	.L1064
	movzbl	2(%rbx), %edx
	movb	%dl, 2(%r13)
	cmpq	$3, %rax
	je	.L1064
	movzbl	3(%rbx), %edx
	movb	%dl, 3(%r13)
	cmpq	$4, %rax
	je	.L1064
	movzbl	4(%rbx), %edx
	movb	%dl, 4(%r13)
	cmpq	$5, %rax
	je	.L1064
	movzbl	5(%rbx), %edx
	movb	%dl, 5(%r13)
	cmpq	$6, %rax
	je	.L1064
	movzbl	6(%rbx), %edx
	movb	%dl, 6(%r13)
	cmpq	$7, %rax
	je	.L1064
	movzbl	7(%rbx), %edx
	movb	%dl, 7(%r13)
	cmpq	$8, %rax
	je	.L1064
	movzbl	8(%rbx), %edx
	movb	%dl, 8(%r13)
	cmpq	$9, %rax
	je	.L1064
	movzbl	9(%rbx), %edx
	movb	%dl, 9(%r13)
	cmpq	$10, %rax
	je	.L1064
	movzbl	10(%rbx), %edx
	movb	%dl, 10(%r13)
	cmpq	$11, %rax
	je	.L1064
	movzbl	11(%rbx), %edx
	movb	%dl, 11(%r13)
	cmpq	$12, %rax
	je	.L1064
	movzbl	12(%rbx), %edx
	movb	%dl, 12(%r13)
	cmpq	$13, %rax
	je	.L1064
	movzbl	13(%rbx), %edx
	movb	%dl, 13(%r13)
	cmpq	$14, %rax
	je	.L1064
	movzbl	14(%rbx), %eax
	movb	%al, 14(%r13)
	.p2align 4,,10
	.p2align 3
.L1064:
	addq	%r12, 8(%r14)
.L1058:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1060:
	.cfi_restore_state
	movabsq	$9223372036854775807, %r15
	movq	(%r14), %r8
	movq	%r13, %rdx
	movq	%r15, %rax
	subq	%r8, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L1186
	cmpq	%rdx, %r12
	movq	%rdx, %rax
	cmovnb	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, %r10
	jc	.L1068
	testq	%rax, %rax
	js	.L1068
	jne	.L1079
	xorl	%r15d, %r15d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
.L1075:
	testq	%rdx, %rdx
	jne	.L1187
.L1069:
	leaq	(%rcx,%rdx), %rax
	cmpq	$15, %r12
	jle	.L1080
	movq	%r12, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L1071:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L1071
	movq	%r12, %rsi
	movq	%r12, %r11
	andq	$-16, %rsi
	andl	$15, %r11d
	addq	%rsi, %rbx
	leaq	(%rax,%rsi), %rdx
	cmpq	%rsi, %r12
	je	.L1072
.L1070:
	movzbl	(%rbx), %esi
	movb	%sil, (%rdx)
	cmpq	$1, %r11
	je	.L1072
	movzbl	1(%rbx), %esi
	movb	%sil, 1(%rdx)
	cmpq	$2, %r11
	je	.L1072
	movzbl	2(%rbx), %esi
	movb	%sil, 2(%rdx)
	cmpq	$3, %r11
	je	.L1072
	movzbl	3(%rbx), %esi
	movb	%sil, 3(%rdx)
	cmpq	$4, %r11
	je	.L1072
	movzbl	4(%rbx), %esi
	movb	%sil, 4(%rdx)
	cmpq	$5, %r11
	je	.L1072
	movzbl	5(%rbx), %esi
	movb	%sil, 5(%rdx)
	cmpq	$6, %r11
	je	.L1072
	movzbl	6(%rbx), %esi
	movb	%sil, 6(%rdx)
	cmpq	$7, %r11
	je	.L1072
	movzbl	7(%rbx), %esi
	movb	%sil, 7(%rdx)
	cmpq	$8, %r11
	je	.L1072
	movzbl	8(%rbx), %esi
	movb	%sil, 8(%rdx)
	cmpq	$9, %r11
	je	.L1072
	movzbl	9(%rbx), %esi
	movb	%sil, 9(%rdx)
	cmpq	$10, %r11
	je	.L1072
	movzbl	10(%rbx), %esi
	movb	%sil, 10(%rdx)
	cmpq	$11, %r11
	je	.L1072
	movzbl	11(%rbx), %esi
	movb	%sil, 11(%rdx)
	cmpq	$12, %r11
	je	.L1072
	movzbl	12(%rbx), %esi
	movb	%sil, 12(%rdx)
	cmpq	$13, %r11
	je	.L1072
	movzbl	13(%rbx), %esi
	movb	%sil, 13(%rdx)
	cmpq	$14, %r11
	je	.L1072
	movzbl	14(%rbx), %esi
	movb	%sil, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L1072:
	addq	%rax, %r12
	testq	%r9, %r9
	jne	.L1188
.L1073:
	addq	%r10, %r12
	testq	%r8, %r8
	je	.L1074
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L1074:
	movq	%r12, %xmm3
	movq	%rcx, %xmm0
	movq	%r15, 16(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1079:
	.cfi_restore_state
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	%r15, %rdi
	movq	%r11, -56(%rbp)
	call	_Znwm@PLT
	movq	8(%r14), %r9
	movq	(%r14), %r8
	movq	%r13, %rdx
	movq	-56(%rbp), %r11
	movq	%rax, %rcx
	addq	%rax, %r15
	subq	%r13, %r9
	subq	%r8, %rdx
	movq	%r9, %r10
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1076:
	xorl	%eax, %eax
.L1061:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, 0(%r13,%rax)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L1061
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %rdx
	movq	%rax, %rcx
	movq	-56(%rbp), %r8
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r10
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	%rax, %rdx
	jmp	.L1070
.L1186:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10491:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB10222:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L1200
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L1201
	cmpq	$1, %rax
	jne	.L1193
	movzbl	0(%r13), %edx
	movb	%dl, 16(%rbx)
.L1194:
	movq	%rax, 8(%rbx)
	movb	$0, (%r14,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1202
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L1194
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1201:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1192:
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %rax
	movq	(%rbx), %r14
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1200:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10222:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb:
.LFB9757:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1211
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testb	%sil, %sil
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	setne	%r13b
	pushq	%r12
	subl	$12, %r13d
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r14
	cmpq	%rdx, %rax
	je	.L1209
	movq	16(%rbx), %rdx
.L1206:
	cmpq	%rdx, %r14
	ja	.L1214
.L1207:
	movb	%r13b, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r14, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1214:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1211:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$15, %edx
	jmp	.L1206
	.cfi_endproc
.LFE9757:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0, @function
_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0:
.LFB10520:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$472, %rsp
	movq	%rdx, -488(%rbp)
	movq	.LC32(%rip), %xmm1
	movq	%rdi, -496(%rbp)
	movq	%r13, %rdi
	movhps	.LC33(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -480(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r12, %rdi
	movq	%r15, -320(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-480(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-496(%rbp), %rax
	cmpq	%rbx, %rax
	jnb	.L1216
	movq	%rbx, -480(%rbp)
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L1217:
	movsbl	(%rax), %esi
	leaq	1(%rax), %r12
	cmpb	$92, %sil
	jne	.L1228
	cmpq	%r12, -480(%rbp)
	je	.L1257
	movsbl	1(%rax), %esi
	leaq	2(%rax), %r12
	cmpb	$120, %sil
	je	.L1257
	cmpb	$34, %sil
	je	.L1228
	leal	-47(%rsi), %edx
	cmpb	$71, %dl
	ja	.L1257
	leaq	.L1223(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L1223:
	.long	.L1228-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1228-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1229-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1258-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1227-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1257-.L1223
	.long	.L1226-.L1223
	.long	.L1257-.L1223
	.long	.L1225-.L1223
	.long	.L1224-.L1223
	.long	.L1222-.L1223
	.text
.L1258:
	movl	$12, %esi
.L1228:
	movq	%rbx, %rdi
	call	_ZNSo3putEc@PLT
	movq	%r12, %rax
	cmpq	%rax, -480(%rbp)
	ja	.L1217
.L1216:
	movq	-384(%rbp), %rax
	leaq	-448(%rbp), %rbx
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%rbx, -464(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L1242
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1243
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1244:
	movq	-488(%rbp), %rax
	movq	-456(%rbp), %rdx
	movq	(%rax), %rdi
	movq	-464(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1272
	movq	-488(%rbp), %rcx
	movq	-448(%rbp), %rsi
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	je	.L1273
	movq	-488(%rbp), %r9
	movq	%rdx, %xmm0
	movq	%rsi, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	16(%r9), %rcx
	movq	%rax, (%r9)
	movups	%xmm0, 8(%r9)
	testq	%rdi, %rdi
	je	.L1250
	movq	%rdi, -464(%rbp)
	movq	%rcx, -448(%rbp)
.L1248:
	movq	$0, -456(%rbp)
	movb	$0, (%rdi)
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1251
	call	_ZdlPv@PLT
.L1251:
	movl	$1, %r12d
	jmp	.L1220
.L1229:
	movl	$8, %esi
	jmp	.L1228
.L1257:
	xorl	%r12d, %r12d
.L1220:
	movq	.LC32(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC34(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-504(%rbp), %rdi
	je	.L1252
	call	_ZdlPv@PLT
.L1252:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	%r15, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1274
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1222:
	.cfi_restore_state
	movl	$11, %esi
	jmp	.L1228
.L1224:
	movzbl	2(%rax), %esi
	leal	-48(%rsi), %edi
	movl	%esi, %edx
	cmpb	$9, %dil
	jbe	.L1271
	leal	-65(%rsi), %edi
	cmpb	$5, %dil
	jbe	.L1275
	movzbl	3(%rax), %edi
	subl	$97, %edx
	subl	$87, %esi
	sall	$12, %esi
	cmpb	$6, %dl
	movl	$0, %edx
	leal	-48(%rdi), %r8d
	cmovnb	%edx, %esi
	movl	%edi, %edx
	cmpb	$9, %r8b
	jbe	.L1276
.L1233:
	leal	-65(%rdi), %r8d
	cmpb	$5, %r8b
	jbe	.L1277
	subl	$97, %edx
	cmpb	$5, %dl
	jbe	.L1278
.L1234:
	movzbl	4(%rax), %edi
	leal	-48(%rdi), %r8d
	movl	%edi, %edx
	cmpb	$9, %r8b
	jbe	.L1279
.L1236:
	leal	-65(%rdi), %r8d
	cmpb	$5, %r8b
	jbe	.L1280
	subl	$97, %edx
	cmpb	$5, %dl
	jbe	.L1281
.L1237:
	movzbl	5(%rax), %edi
	leal	-48(%rdi), %r8d
	movl	%edi, %edx
	cmpb	$9, %r8b
	jbe	.L1282
.L1239:
	leal	-65(%rdi), %r8d
	cmpb	$5, %r8b
	jbe	.L1283
	subl	$97, %edx
	leal	-87(%rsi,%rdi), %edi
	cmpb	$6, %dl
	cmovb	%edi, %esi
.L1240:
	leaq	6(%rax), %r12
	movsbl	%sil, %esi
	jmp	.L1228
.L1225:
	movl	$9, %esi
	jmp	.L1228
.L1226:
	movl	$13, %esi
	jmp	.L1228
.L1227:
	movl	$10, %esi
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1275:
	subl	$55, %esi
.L1271:
	movzbl	3(%rax), %edi
	sall	$12, %esi
	leal	-48(%rdi), %r8d
	movl	%edi, %edx
	cmpb	$9, %r8b
	ja	.L1233
.L1276:
	subl	$48, %edi
	sall	$8, %edi
	addl	%edi, %esi
	movzbl	4(%rax), %edi
	leal	-48(%rdi), %r8d
	movl	%edi, %edx
	cmpb	$9, %r8b
	ja	.L1236
.L1279:
	subl	$48, %edi
	sall	$4, %edi
	addl	%edi, %esi
	movzbl	5(%rax), %edi
	leal	-48(%rdi), %r8d
	movl	%edi, %edx
	cmpb	$9, %r8b
	ja	.L1239
.L1282:
	leal	-48(%rsi,%rdi), %esi
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1243:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1281:
	subl	$87, %edi
	sall	$4, %edi
	addl	%edi, %esi
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1278:
	subl	$87, %edi
	sall	$8, %edi
	addl	%edi, %esi
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1272:
	testq	%rdx, %rdx
	je	.L1246
	cmpq	$1, %rdx
	je	.L1284
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %rax
	movq	-456(%rbp), %rdx
	movq	(%rax), %rdi
.L1246:
	movq	-488(%rbp), %rax
	movq	%rdx, 8(%rax)
	movb	$0, (%rdi,%rdx)
	movq	-464(%rbp), %rdi
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1283:
	leal	-55(%rsi,%rdi), %esi
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1277:
	subl	$55, %edi
	sall	$8, %edi
	addl	%edi, %esi
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1280:
	subl	$55, %edi
	sall	$4, %edi
	addl	%edi, %esi
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	-488(%rbp), %rcx
	movq	%rdx, %xmm0
	movq	%rsi, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, (%rcx)
	movups	%xmm0, 8(%rcx)
.L1250:
	movq	%rbx, -464(%rbp)
	leaq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1242:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1284:
	movzbl	-448(%rbp), %eax
	movb	%al, (%rdi)
	movq	-488(%rbp), %rax
	movq	-456(%rbp), %rdx
	movq	(%rax), %rdi
	jmp	.L1246
.L1274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10520:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0, .-_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0, @function
_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0:
.LFB10516:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$472, %rsp
	movq	%rdx, -488(%rbp)
	movq	.LC32(%rip), %xmm1
	movq	%rdi, -496(%rbp)
	movq	%r13, %rdi
	movhps	.LC33(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -480(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r12, %rdi
	movq	%r15, -320(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-480(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-496(%rbp), %rax
	cmpq	%rbx, %rax
	jnb	.L1286
	movq	%rbx, -480(%rbp)
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L1287:
	movzwl	(%rax), %esi
	leaq	2(%rax), %r12
	cmpw	$92, %si
	jne	.L1343
	cmpq	%r12, -480(%rbp)
	je	.L1327
	movzwl	2(%rax), %esi
	leaq	4(%rax), %r12
	cmpw	$120, %si
	je	.L1327
	cmpw	$34, %si
	je	.L1342
	leal	-47(%rsi), %edx
	cmpw	$71, %dx
	ja	.L1327
	leaq	.L1293(%rip), %rcx
	movzwl	%dx, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L1293:
	.long	.L1342-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1342-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1328-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1298-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1297-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1327-.L1293
	.long	.L1296-.L1293
	.long	.L1327-.L1293
	.long	.L1295-.L1293
	.long	.L1294-.L1293
	.long	.L1292-.L1293
	.text
.L1298:
	movl	$12, %esi
.L1299:
	movq	%rbx, %rdi
	call	_ZNSo3putEc@PLT
	movq	%r12, %rax
.L1289:
	cmpq	%rax, -480(%rbp)
	ja	.L1287
.L1286:
	movq	-384(%rbp), %rax
	leaq	-448(%rbp), %rbx
	movq	$0, -456(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%rbx, -464(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L1312
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1313
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1314:
	movq	-488(%rbp), %rax
	movq	-456(%rbp), %rdx
	movq	(%rax), %rdi
	movq	-464(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1344
	movq	-488(%rbp), %rcx
	movq	-448(%rbp), %rsi
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	je	.L1345
	movq	-488(%rbp), %r9
	movq	%rdx, %xmm0
	movq	%rsi, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	16(%r9), %rcx
	movq	%rax, (%r9)
	movups	%xmm0, 8(%r9)
	testq	%rdi, %rdi
	je	.L1320
	movq	%rdi, -464(%rbp)
	movq	%rcx, -448(%rbp)
.L1318:
	movq	$0, -456(%rbp)
	movb	$0, (%rdi)
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1321
	call	_ZdlPv@PLT
.L1321:
	movl	$1, %r12d
	jmp	.L1290
.L1328:
	movl	$8, %esi
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1353:
	sall	$4, %edi
	addl	%edi, %esi
.L1307:
	movzwl	10(%rax), %edx
	leal	-48(%rdx), %edi
	cmpw	$9, %di
	jbe	.L1346
	leal	-65(%rdx), %edi
	cmpw	$5, %di
	jbe	.L1347
	leal	-97(%rdx), %edi
	leal	-87(%rdx,%rsi), %edx
	cmpw	$6, %di
	cmovb	%edx, %esi
.L1310:
	leaq	12(%rax), %r12
.L1342:
	movsbl	%sil, %esi
	jmp	.L1299
.L1327:
	xorl	%r12d, %r12d
.L1290:
	movq	.LC32(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC34(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-504(%rbp), %rdi
	je	.L1322
	call	_ZdlPv@PLT
.L1322:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	%r15, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1348
	addq	$472, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1292:
	.cfi_restore_state
	movl	$11, %esi
	jmp	.L1299
.L1294:
	movzwl	4(%rax), %edx
	leal	-48(%rdx), %esi
	cmpw	$9, %si
	jbe	.L1341
	leal	-65(%rdx), %esi
	cmpw	$5, %si
	jbe	.L1349
	leal	-87(%rdx), %esi
	leal	-97(%rdx), %edi
	movl	%esi, %edx
	movl	$0, %esi
	sall	$12, %edx
	cmpw	$6, %di
	cmovb	%edx, %esi
	movzwl	6(%rax), %edx
	leal	-48(%rdx), %edi
	cmpw	$9, %di
	jbe	.L1350
.L1303:
	leal	-65(%rdx), %edi
	cmpw	$5, %di
	jbe	.L1351
	leal	-97(%rdx), %edi
	cmpw	$5, %di
	jbe	.L1352
.L1304:
	movzwl	8(%rax), %edx
	leal	-48(%rdx), %edi
	cmpw	$9, %di
	jbe	.L1353
	leal	-65(%rdx), %edi
	cmpw	$5, %di
	jbe	.L1354
	leal	-97(%rdx), %edi
	cmpw	$5, %di
	ja	.L1307
	subl	$87, %edx
	sall	$4, %edx
	addl	%edx, %esi
	jmp	.L1307
.L1295:
	movl	$9, %esi
	jmp	.L1299
.L1296:
	movl	$13, %esi
	jmp	.L1299
.L1297:
	movl	$10, %esi
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1343:
	movsbl	%sil, %esi
	movq	%rbx, %rdi
	call	_ZNSo3putEc@PLT
	movq	%r12, %rax
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1349:
	leal	-55(%rdx), %esi
.L1341:
	movzwl	6(%rax), %edx
	sall	$12, %esi
	leal	-48(%rdx), %edi
	cmpw	$9, %di
	ja	.L1303
.L1350:
	sall	$8, %edi
	addl	%edi, %esi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1313:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1352:
	subl	$87, %edx
	sall	$8, %edx
	addl	%edx, %esi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1346:
	addl	%edi, %esi
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1344:
	testq	%rdx, %rdx
	je	.L1316
	cmpq	$1, %rdx
	je	.L1355
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %rax
	movq	-456(%rbp), %rdx
	movq	(%rax), %rdi
.L1316:
	movq	-488(%rbp), %rax
	movq	%rdx, 8(%rax)
	movb	$0, (%rdi,%rdx)
	movq	-464(%rbp), %rdi
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1347:
	leal	-55(%rdx,%rsi), %esi
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1351:
	subl	$55, %edx
	sall	$8, %edx
	addl	%edx, %esi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1354:
	subl	$55, %edx
	sall	$4, %edx
	addl	%edx, %esi
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1345:
	movq	-488(%rbp), %rcx
	movq	%rdx, %xmm0
	movq	%rsi, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, (%rcx)
	movups	%xmm0, 8(%rcx)
.L1320:
	movq	%rbx, -464(%rbp)
	leaq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1312:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1355:
	movzbl	-448(%rbp), %eax
	movb	%al, (%rdi)
	movq	-488(%rbp), %rax
	movq	-456(%rbp), %rdx
	movq	(%rax), %rdi
	jmp	.L1316
.L1348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10516:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0, .-_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv:
.LFB9758:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1365
	ret
	.p2align 4,,10
	.p2align 3
.L1365:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L1360
	movq	16(%rbx), %rdx
.L1358:
	cmpq	%rdx, %r13
	ja	.L1366
.L1359:
	movb	$-10, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1366:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1360:
	movl	$15, %edx
	jmp	.L1358
	.cfi_endproc
.LFE9758:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd:
.LFB9755:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1401
	ret
	.p2align 4,,10
	.p2align 3
.L1401:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%xmm0, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	16(%rbx), %r13
	leaq	1(%r14), %r15
	cmpq	%r13, %rax
	je	.L1388
	movq	16(%rbx), %rdx
.L1369:
	cmpq	%rdx, %r15
	ja	.L1402
.L1370:
	movb	$-5, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$56, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1389
	movq	16(%rbx), %rdx
.L1371:
	cmpq	%r15, %rdx
	jb	.L1403
.L1372:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$48, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1390
	movq	16(%rbx), %rdx
.L1373:
	cmpq	%rdx, %r15
	ja	.L1404
.L1374:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$40, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1391
	movq	16(%rbx), %rdx
.L1375:
	cmpq	%rdx, %r15
	ja	.L1405
.L1376:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$32, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1392
	movq	16(%rbx), %rdx
.L1377:
	cmpq	%rdx, %r15
	ja	.L1406
.L1378:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$24, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1393
	movq	16(%rbx), %rdx
.L1379:
	cmpq	%rdx, %r15
	ja	.L1407
.L1380:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$16, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1394
	movq	16(%rbx), %rdx
.L1381:
	cmpq	%rdx, %r15
	ja	.L1408
.L1382:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$8, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1395
	movq	16(%rbx), %rdx
.L1383:
	cmpq	%rdx, %r15
	ja	.L1409
.L1384:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L1396
	movq	16(%rbx), %rdx
.L1385:
	cmpq	%rdx, %r15
	ja	.L1410
.L1386:
	movb	%r12b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1402:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1410:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1409:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1408:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1407:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1406:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1378
	.p2align 4,,10
	.p2align 3
.L1405:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1403:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1389:
	movl	$15, %edx
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1388:
	movl	$15, %edx
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1396:
	movl	$15, %edx
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1395:
	movl	$15, %edx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1394:
	movl	$15, %edx
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1393:
	movl	$15, %edx
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1392:
	movl	$15, %edx
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	$15, %edx
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1390:
	movl	$15, %edx
	jmp	.L1373
	.cfi_endproc
.LFE9755:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"void node::inspector::protocol::json::{anonymous}::State::StartElementTmpl(C*) [with C = std::__cxx11::basic_string<char>]"
	.align 8
.LC36:
	.string	"container_ != Container::NONE || size_ == 0"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv:
.LFB9734:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1411
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r12
	cmpq	88(%rdi), %r12
	je	.L1430
.L1413:
	movl	-8(%r12), %edx
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L1431
	testl	%eax, %eax
	je	.L1415
	cmpl	$2, %edx
	je	.L1419
	movl	$58, %r15d
	testb	$1, %al
	jne	.L1416
.L1419:
	movl	$44, %r15d
.L1416:
	movq	8(%rdi), %r13
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L1420
	movq	16(%rdi), %rdx
.L1417:
	cmpq	%rdx, %r14
	ja	.L1432
.L1418:
	movb	%r15b, (%rax,%r13)
	movq	(%rdi), %rax
	movq	%r14, 8(%rdi)
	movb	$0, 1(%rax,%r13)
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1431:
	testl	%eax, %eax
	jne	.L1433
.L1415:
	addl	$1, %eax
	movq	8(%rdi), %rsi
	movl	$4, %r8d
	xorl	%edx, %edx
	movl	%eax, -4(%r12)
	addq	$24, %rsp
	leaq	.LC0(%rip), %rcx
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L1411:
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	addq	$512, %r12
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1432:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	(%rdi), %rax
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1420:
	movl	$15, %edx
	jmp	.L1417
.L1433:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9734:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei:
.LFB9732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1434
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	movl	%esi, %r14d
	cmpq	88(%rdi), %r12
	je	.L1453
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L1454
.L1437:
	testl	%eax, %eax
	je	.L1438
	cmpl	$2, %edx
	je	.L1444
	movl	$58, %r10d
	testb	$1, %al
	jne	.L1439
.L1444:
	movl	$44, %r10d
.L1439:
	movq	16(%rbx), %r13
	movq	8(%r13), %r15
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r15), %r9
	cmpq	%rdx, %rax
	je	.L1445
	movq	16(%r13), %rdx
.L1440:
	cmpq	%rdx, %r9
	ja	.L1455
.L1441:
	movb	%r10b, (%rax,%r15)
	movq	0(%r13), %rax
	movq	%r9, 8(%r13)
	movb	$0, 1(%rax,%r15)
	movl	-4(%r12), %eax
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1454:
	testl	%eax, %eax
	jne	.L1456
.L1438:
	addl	$1, %eax
	leaq	-96(%rbp), %rdi
	movl	%r14d, %r8d
	movl	$16, %edx
	movl	%eax, -4(%r12)
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC30(%rip), %rcx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %r8
	xorl	%edx, %edx
	movq	-96(%rbp), %rcx
	movq	8(%rdi), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1434
	call	_ZdlPv@PLT
.L1434:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1457
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1453:
	.cfi_restore_state
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1437
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1455:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	movb	%r10b, -97(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movq	-112(%rbp), %r9
	movzbl	-97(%rbp), %r10d
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1445:
	movl	$15, %edx
	jmp	.L1440
.L1457:
	call	__stack_chk_fail@PLT
.L1456:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9732:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb:
.LFB9733:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1458
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r12
	cmpq	88(%rdi), %r12
	je	.L1479
.L1460:
	movl	-8(%r12), %edx
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L1480
	testl	%eax, %eax
	je	.L1462
	cmpl	$2, %edx
	je	.L1467
	movl	$58, %r9d
	testb	$1, %al
	jne	.L1463
.L1467:
	movl	$44, %r9d
.L1463:
	movq	8(%rdi), %r14
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L1468
	movq	16(%rdi), %rdx
.L1464:
	cmpq	%rdx, %r15
	ja	.L1481
.L1465:
	movb	%r9b, (%rax,%r14)
	movq	(%rdi), %rax
	movq	%r15, 8(%rdi)
	movb	$0, 1(%rax,%r14)
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1480:
	testl	%eax, %eax
	jne	.L1482
.L1462:
	addl	$1, %eax
	cmpb	$1, %r13b
	leaq	.LC3(%rip), %rdx
	movq	8(%rdi), %rsi
	movl	%eax, -4(%r12)
	leaq	.LC27(%rip), %rcx
	sbbq	%rax, %rax
	testb	%r13b, %r13b
	cmove	%rdx, %rcx
	notq	%rax
	addq	$24, %rsp
	xorl	%edx, %edx
	popq	%rbx
	.cfi_restore 3
	leaq	5(%rax), %r8
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L1458:
	ret
	.p2align 4,,10
	.p2align 3
.L1479:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	addq	$512, %r12
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1481:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movb	%r9b, -57(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movzbl	-57(%rbp), %r9d
	movq	(%rdi), %rax
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1468:
	movl	$15, %edx
	jmp	.L1464
.L1482:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9733:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleMapEnd() [with C = std::__cxx11::basic_string<char>]"
	.align 8
.LC38:
	.string	"state_.size() >= 2 && state_.top().container() == Container::MAP"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv:
.LFB9725:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1494
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %r8
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1485
	cmpq	%rsi, %rdi
	je	.L1497
	cmpl	$1, -8(%rdi)
	jne	.L1485
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L1490:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L1492
	movq	16(%rbx), %rdx
.L1488:
	cmpq	%rdx, %r13
	ja	.L1498
.L1489:
	movb	$125, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1494:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	-8(%r8), %rax
	cmpl	$1, 504(%rax)
	jne	.L1485
	call	_ZdlPv@PLT
	movq	104(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rsi
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1492:
	movl	$15, %edx
	jmp	.L1488
.L1485:
	leaq	.LC37(%rip), %rcx
	movl	$2998, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC38(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9725:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleArrayEnd() [with C = std::__cxx11::basic_string<char>]"
	.align 8
.LC40:
	.string	"state_.size() >= 2 && state_.top().container() == Container::ARRAY"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv:
.LFB9727:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1510
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %r8
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L1501
	cmpq	%rsi, %rdi
	je	.L1513
	cmpl	$2, -8(%rdi)
	jne	.L1501
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L1506:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L1508
	movq	16(%rbx), %rdx
.L1504:
	cmpq	%rdx, %r13
	ja	.L1514
.L1505:
	movb	$93, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1510:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L1514:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	-8(%r8), %rax
	cmpl	$2, 504(%rax)
	jne	.L1501
	call	_ZdlPv@PLT
	movq	104(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rsi
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	$15, %edx
	jmp	.L1504
.L1501:
	leaq	.LC39(%rip), %rcx
	movl	$3014, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC40(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9727:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"void node::inspector::protocol::cbor::{anonymous}::CBOREncoder<C>::HandleMapEnd() [with C = std::__cxx11::basic_string<char>]"
	.section	.rodata.str1.1
.LC42:
	.string	"!envelopes_.empty()"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"bool node::inspector::protocol::cbor::EncodeStopTmpl(C*, size_t*) [with C = std::__cxx11::basic_string<char>; size_t = long unsigned int]"
	.section	.rodata.str1.1
.LC44:
	.string	"*byte_size_pos != 0"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv:
.LFB9749:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L1529
	ret
	.p2align 4,,10
	.p2align 3
.L1529:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	16(%r12), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L1524
	movq	16(%r12), %rdx
.L1518:
	cmpq	%rdx, %r14
	ja	.L1530
.L1519:
	movb	$-1, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L1531
	movq	-8(%rax), %rsi
	movq	8(%rbx), %rcx
	testq	%rsi, %rsi
	je	.L1532
	movq	8(%rcx), %rdi
	movl	$4294967295, %r8d
	leaq	-4(%rdi), %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rdx
	ja	.L1522
	leaq	1(%rsi), %rdi
	movq	%rdx, %r8
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rdi
	shrq	$24, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rdx, %r8
	shrq	$16, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rdx, %r8
	shrq	$8, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rax
	movb	%dl, (%rax,%rsi)
	subq	$8, 24(%rbx)
.L1515:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1522:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L1515
	movl	$31, (%rax)
	movq	%rdi, 8(%rax)
	movq	(%rcx), %rax
	movq	$0, 8(%rcx)
	movb	$0, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1530:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1524:
	movl	$15, %edx
	jmp	.L1518
.L1532:
	leaq	.LC43(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	__assert_fail@PLT
.L1531:
	leaq	.LC41(%rip), %rcx
	movl	$2259, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9749:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"void node::inspector::protocol::cbor::{anonymous}::CBOREncoder<C>::HandleArrayEnd() [with C = std::__cxx11::basic_string<char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv:
.LFB9751:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L1547
	ret
	.p2align 4,,10
	.p2align 3
.L1547:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	16(%r12), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L1542
	movq	16(%r12), %rdx
.L1536:
	cmpq	%rdx, %r14
	ja	.L1548
.L1537:
	movb	$-1, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L1549
	movq	-8(%rax), %rsi
	movq	8(%rbx), %rcx
	testq	%rsi, %rsi
	je	.L1550
	movq	8(%rcx), %rdi
	movl	$4294967295, %r8d
	leaq	-4(%rdi), %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rdx
	ja	.L1540
	leaq	1(%rsi), %rdi
	movq	%rdx, %r8
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rdi
	shrq	$24, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rdx, %r8
	shrq	$16, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	%rdx, %r8
	shrq	$8, %r8
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rdi
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movq	(%rcx), %rax
	movb	%dl, (%rax,%rsi)
	subq	$8, 24(%rbx)
.L1533:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1540:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L1533
	movl	$31, (%rax)
	movq	%rdi, 8(%rax)
	movq	(%rcx), %rax
	movq	$0, 8(%rcx)
	movb	$0, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1548:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1542:
	movl	$15, %edx
	jmp	.L1536
.L1550:
	leaq	.LC43(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	__assert_fail@PLT
.L1549:
	leaq	.LC45(%rip), %rcx
	movl	$2280, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9751:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.section	.rodata.str1.1
.LC46:
	.string	"-0"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd:
.LFB9731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1551
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	88(%rdi), %r12
	je	.L1579
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1554
.L1586:
	testl	%eax, %eax
	jne	.L1580
.L1555:
	movsd	.LC29(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addl	$1, %eax
	andpd	.LC28(%rip), %xmm1
	movl	%eax, -4(%r12)
	ucomisd	%xmm1, %xmm2
	jb	.L1581
	movq	8(%rbx), %rsi
	leaq	-64(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-64(%rbp), %r12
	movzbl	(%r12), %eax
	cmpb	$46, %al
	je	.L1582
	movq	16(%rbx), %r13
	cmpb	$45, %al
	je	.L1583
.L1562:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	8(%r13), %rsi
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1551
	call	_ZdaPv@PLT
.L1551:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1584
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1554:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1555
	cmpl	$2, %edx
	je	.L1565
	movl	$58, %r9d
	testb	$1, %al
	jne	.L1556
.L1565:
	movl	$44, %r9d
.L1556:
	movq	16(%rbx), %r13
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L1566
	movq	16(%r13), %rdx
.L1557:
	cmpq	%rdx, %r15
	ja	.L1585
.L1558:
	movb	%r9b, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movl	-4(%r12), %eax
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1554
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1583:
	cmpb	$46, 1(%r12)
	jne	.L1562
	movq	%r13, %rdi
	leaq	.LC46(%rip), %rsi
	addq	$1, %r12
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0
	movq	16(%rbx), %r13
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	16(%rbx), %rdi
	movl	$4, %r8d
	leaq	.LC0(%rip), %rcx
	xorl	%edx, %edx
	movq	8(%rdi), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1585:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -65(%rbp)
	movsd	%xmm0, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movsd	-80(%rbp), %xmm0
	movzbl	-65(%rbp), %r9d
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	16(%rbx), %rdi
	movl	$48, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movq	16(%rbx), %r13
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1566:
	movl	$15, %edx
	jmp	.L1557
.L1584:
	call	__stack_chk_fail@PLT
.L1580:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9731:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv:
.LFB9726:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1617
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rax
	movq	%rax, %r12
	cmpq	88(%rdi), %rax
	je	.L1620
	movl	-8(%r12), %ecx
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L1590
.L1624:
	testl	%edx, %edx
	jne	.L1621
.L1591:
	movq	96(%rbx), %rsi
	addl	$1, %edx
	movl	%edx, -4(%r12)
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L1595
	movq	$2, (%rax)
	addq	$8, %rax
	movq	%rax, 80(%rbx)
.L1596:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L1609
	movq	16(%rbx), %rdx
.L1604:
	cmpq	%rdx, %r13
	ja	.L1622
.L1605:
	movb	$91, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L1591
	cmpl	$2, %ecx
	je	.L1607
	andl	$1, %edx
	movl	$58, %r9d
	jne	.L1592
.L1607:
	movl	$44, %r9d
.L1592:
	movq	16(%rbx), %r13
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L1608
	movq	16(%r13), %rdx
.L1593:
	cmpq	%rdx, %r15
	ja	.L1623
.L1594:
	movb	%r9b, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movq	80(%rbx), %rax
	movl	-4(%r12), %edx
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1617:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1622:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	104(%rdi), %rdx
	movq	-8(%rdx), %r12
	movl	504(%r12), %ecx
	addq	$512, %r12
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L1590
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1609:
	movl	$15, %edx
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L1625
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r10
	sarq	$3, %rax
	subq	%rax, %r10
	cmpq	$1, %r10
	jbe	.L1626
.L1598:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$2, (%rax)
	movq	104(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1623:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -49(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movzbl	-49(%rbp), %r9d
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1608:
	movl	$15, %edx
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1626:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L1599
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L1600
	cmpq	%r13, %rsi
	je	.L1601
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L1601:
	movq	(%r14), %rax
	movq	(%r14), %xmm0
	leaq	(%r14,%r12), %r13
	movq	%r14, 72(%rbx)
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1599:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L1627
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1603
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L1603:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L1601
.L1600:
	cmpq	%r13, %rsi
	je	.L1601
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1601
.L1621:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L1627:
	call	_ZSt17__throw_bad_allocv@PLT
.L1625:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9726:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleMapBegin() [with C = std::__cxx11::basic_string<char>]"
	.section	.rodata.str1.1
.LC48:
	.string	"!state_.empty()"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv:
.LFB9724:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1659
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %rax
	cmpq	48(%rdi), %rax
	je	.L1662
	movq	%rax, %r12
	cmpq	88(%rdi), %rax
	je	.L1663
	movl	-8(%r12), %ecx
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L1632
.L1667:
	testl	%edx, %edx
	jne	.L1664
.L1633:
	movq	96(%rbx), %rsi
	addl	$1, %edx
	movl	%edx, -4(%r12)
	leaq	-8(%rsi), %rdx
	cmpq	%rdx, %rax
	je	.L1637
	movq	$1, (%rax)
	addq	$8, %rax
	movq	%rax, 80(%rbx)
.L1638:
	movq	16(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L1651
	movq	16(%rbx), %rdx
.L1646:
	cmpq	%rdx, %r13
	ja	.L1665
.L1647:
	movb	$123, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1632:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L1633
	andl	$1, %edx
	je	.L1649
	movl	$58, %r9d
	cmpl	$2, %ecx
	jne	.L1634
.L1649:
	movl	$44, %r9d
.L1634:
	movq	16(%rbx), %r13
	movq	8(%r13), %r14
	movq	0(%r13), %rax
	leaq	16(%r13), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L1650
	movq	16(%r13), %rdx
.L1635:
	cmpq	%rdx, %r15
	ja	.L1666
.L1636:
	movb	%r9b, (%rax,%r14)
	movq	0(%r13), %rax
	movq	%r15, 8(%r13)
	movb	$0, 1(%rax,%r14)
	movq	80(%rbx), %rax
	movl	-4(%r12), %edx
	jmp	.L1633
	.p2align 4,,10
	.p2align 3
.L1659:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1665:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1663:
	movq	104(%rdi), %rdx
	movq	-8(%rdx), %r12
	movl	504(%r12), %ecx
	addq	$512, %r12
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L1632
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1651:
	movl	$15, %edx
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L1668
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r10
	sarq	$3, %rax
	subq	%rax, %r10
	cmpq	$1, %r10
	jbe	.L1669
.L1640:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$1, (%rax)
	movq	104(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1666:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r9b, -49(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movzbl	-49(%rbp), %r9d
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1650:
	movl	$15, %edx
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1669:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L1641
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L1642
	cmpq	%r13, %rsi
	je	.L1643
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L1643:
	movq	(%r14), %rax
	movq	(%r14), %xmm0
	leaq	(%r14,%r12), %r13
	movq	%r14, 72(%rbx)
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1641:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L1670
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1645
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L1645:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L1643
.L1642:
	cmpq	%r13, %rsi
	je	.L1643
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L1643
.L1662:
	leaq	.LC47(%rip), %rcx
	movl	$2989, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	call	__assert_fail@PLT
.L1664:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L1670:
	call	_ZSt17__throw_bad_allocv@PLT
.L1668:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9724:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.section	.rodata.str1.1
.LC49:
	.string	"\\\""
.LC50:
	.string	"\\\\"
.LC51:
	.string	"\\b"
.LC52:
	.string	"\\f"
.LC53:
	.string	"\\n"
.LC54:
	.string	"\\r"
.LC55:
	.string	"\\t"
.LC56:
	.string	"\\u"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE:
.LFB9729:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1733
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %r14
	cmpq	88(%rdi), %r14
	je	.L1736
	movl	-8(%r14), %edx
	movq	16(%r13), %rbx
	movl	-4(%r14), %eax
	testl	%edx, %edx
	jne	.L1674
.L1753:
	testl	%eax, %eax
	jne	.L1737
.L1675:
	addl	$1, %eax
	leaq	16(%rbx), %rdx
	movl	%eax, -4(%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r9
	cmpq	%rdx, %rax
	je	.L1712
	movq	16(%rbx), %rdx
.L1679:
	cmpq	%rdx, %r9
	ja	.L1738
.L1680:
	movb	$34, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	leaq	(%r12,%r15,2), %r14
	movl	$48, %r15d
	cmpq	%r12, %r14
	jne	.L1708
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1684:
	cmpw	$92, %bx
	je	.L1739
	cmpw	$8, %bx
	je	.L1740
	cmpw	$12, %bx
	je	.L1741
	cmpw	$10, %bx
	je	.L1742
	cmpw	$13, %bx
	je	.L1743
	cmpw	$9, %bx
	je	.L1744
	leal	-32(%rbx), %eax
	cmpw	$94, %ax
	ja	.L1692
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	leaq	1(%rsi), %r9
	cmpq	%rdx, %rax
	je	.L1714
	movq	16(%rdi), %rdx
.L1693:
	cmpq	%rdx, %r9
	ja	.L1745
.L1694:
	movb	%bl, (%rax,%rsi)
	movq	(%rdi), %rax
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rsi)
	.p2align 4,,10
	.p2align 3
.L1685:
	addq	$2, %r12
	cmpq	%r12, %r14
	je	.L1707
.L1708:
	movq	16(%r13), %rdi
	movzwl	(%r12), %ebx
	movq	8(%rdi), %rsi
	cmpw	$34, %bx
	jne	.L1684
	movl	$2, %r8d
	leaq	.LC49(%rip), %rcx
	xorl	%edx, %edx
	addq	$2, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	cmpq	%r12, %r14
	jne	.L1708
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	16(%r13), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L1746
	movq	16(%rbx), %rdx
.L1682:
	cmpq	%rdx, %r13
	ja	.L1747
.L1709:
	movb	$34, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1674:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1675
	cmpl	$2, %edx
	je	.L1710
	movl	$58, %r10d
	testb	$1, %al
	jne	.L1676
.L1710:
	movl	$44, %r10d
.L1676:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	leaq	16(%rbx), %rcx
	leaq	1(%rax), %r9
	cmpq	%rcx, %rdx
	je	.L1711
	movq	16(%rbx), %rcx
.L1677:
	cmpq	%rcx, %r9
	ja	.L1748
.L1678:
	movb	%r10b, (%rdx,%rax)
	movq	(%rbx), %rdx
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rdx,%rax)
	movq	16(%r13), %rbx
	movl	-4(%r14), %eax
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1739:
	movl	$2, %r8d
	leaq	.LC50(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1740:
	movl	$2, %r8d
	leaq	.LC51(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1741:
	movl	$2, %r8d
	leaq	.LC52(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1692:
	xorl	%edx, %edx
	movl	$2, %r8d
	leaq	.LC56(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	%ebx, %edx
	movq	16(%r13), %rdi
	movzwl	%bx, %eax
	shrw	$12, %dx
	cmpw	$10, %dx
	movq	8(%rdi), %rsi
	leaq	16(%rdi), %r10
	sbbl	%r9d, %r9d
	andl	$-39, %r9d
	leaq	1(%rsi), %r11
	leal	87(%r9,%rdx), %r9d
	movq	(%rdi), %rdx
	cmpq	%rdx, %r10
	je	.L1716
	movq	16(%rdi), %rcx
.L1696:
	cmpq	%r11, %rcx
	jb	.L1749
.L1697:
	movb	%r9b, (%rdx,%rsi)
	movl	%eax, %r9d
	movq	(%rdi), %rdx
	sarl	$8, %r9d
	movq	%r11, 8(%rdi)
	movb	$0, 1(%rdx,%rsi)
	movl	%r9d, %edx
	movl	$87, %r9d
	movq	8(%rdi), %rsi
	andl	$15, %edx
	cmpl	$10, %edx
	leaq	1(%rsi), %r11
	cmovl	%r15d, %r9d
	addl	%edx, %r9d
	movq	(%rdi), %rdx
	cmpq	%r10, %rdx
	je	.L1718
	movq	16(%rdi), %rcx
.L1699:
	cmpq	%rcx, %r11
	ja	.L1750
.L1700:
	movb	%r9b, (%rdx,%rsi)
	sarl	$4, %eax
	movq	(%rdi), %rdx
	andl	$15, %eax
	movq	%r11, 8(%rdi)
	cmpl	$10, %eax
	movb	$0, 1(%rdx,%rsi)
	movl	$87, %edx
	movq	8(%rdi), %rsi
	cmovl	%r15d, %edx
	leaq	1(%rsi), %r9
	addl	%edx, %eax
	movq	(%rdi), %rdx
	cmpq	%rdx, %r10
	je	.L1720
	movq	16(%rdi), %rcx
.L1702:
	cmpq	%rcx, %r9
	ja	.L1751
.L1703:
	movb	%al, (%rdx,%rsi)
	andl	$15, %ebx
	movq	(%rdi), %rax
	cmpw	$10, %bx
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rsi)
	sbbl	%eax, %eax
	movq	(%rdi), %rdx
	andl	$-39, %eax
	leal	87(%rax,%rbx), %eax
	movq	8(%rdi), %rbx
	leaq	1(%rbx), %r9
	cmpq	%rdx, %r10
	je	.L1722
	movq	16(%rdi), %rcx
.L1705:
	cmpq	%rcx, %r9
	ja	.L1752
.L1706:
	movb	%al, (%rdx,%rbx)
	movq	(%rdi), %rax
	movq	%r9, 8(%rdi)
	movb	$0, 1(%rax,%rbx)
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1742:
	movl	$2, %r8d
	leaq	.LC53(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1743:
	movl	$2, %r8d
	leaq	.LC54(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1733:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1744:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$2, %r8d
	leaq	.LC55(%rip), %rcx
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1738:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1747:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	104(%rdi), %rax
	movq	16(%r13), %rbx
	movq	-8(%rax), %r14
	movl	504(%r14), %edx
	addq	$512, %r14
	movl	-4(%r14), %eax
	testl	%edx, %edx
	jne	.L1674
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1745:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	(%rdi), %rax
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1752:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r9, -72(%rbp)
	movb	%al, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-72(%rbp), %r9
	movzbl	-64(%rbp), %eax
	movq	(%rdi), %rdx
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1751:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -88(%rbp)
	movb	%al, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %eax
	movq	-72(%rbp), %r10
	movq	(%rdi), %rdx
	movq	-64(%rbp), %rsi
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1750:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movb	%r9b, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movzbl	-96(%rbp), %r9d
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r11
	movq	(%rdi), %rdx
	movl	-72(%rbp), %eax
	movq	-64(%rbp), %rsi
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1749:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r11, -96(%rbp)
	movb	%r9b, -88(%rbp)
	movq	%r10, -80(%rbp)
	movl	%eax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	-96(%rbp), %r11
	movzbl	-88(%rbp), %r9d
	movq	-80(%rbp), %r10
	movq	(%rdi), %rdx
	movl	-72(%rbp), %eax
	movq	-64(%rbp), %rsi
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1712:
	movl	$15, %edx
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1746:
	movl	$15, %edx
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1748:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r9, -72(%rbp)
	movb	%r10b, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-72(%rbp), %r9
	movzbl	-64(%rbp), %r10d
	movq	-56(%rbp), %rax
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1714:
	movl	$15, %edx
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1716:
	movl	$15, %ecx
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1720:
	movl	$15, %ecx
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1718:
	movl	$15, %ecx
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1722:
	movl	$15, %ecx
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1711:
	movl	$15, %ecx
	jmp	.L1677
.L1737:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9729:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE:
.LFB9728:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1821
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	80(%rdi), %r12
	cmpq	88(%rdi), %r12
	je	.L1824
.L1756:
	movl	-8(%r12), %edx
	movq	16(%r15), %rbx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L1757
	testl	%eax, %eax
	jne	.L1825
.L1758:
	addl	$1, %eax
	leaq	16(%rbx), %rdx
	movl	%eax, -4(%r12)
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	1(%r12), %r9
	cmpq	%rdx, %rax
	je	.L1801
	movq	16(%rbx), %rdx
.L1762:
	cmpq	%rdx, %r9
	ja	.L1826
.L1763:
	movb	$34, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	movq	16(%r15), %rdi
	testq	%r13, %r13
	je	.L1793
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1792:
	movzbl	(%r14,%r12), %ebx
	cmpb	$34, %bl
	je	.L1827
	cmpb	$92, %bl
	je	.L1828
	cmpb	$8, %bl
	je	.L1829
	cmpb	$12, %bl
	je	.L1830
	cmpb	$10, %bl
	je	.L1831
	cmpb	$13, %bl
	je	.L1832
	cmpb	$9, %bl
	je	.L1833
	leal	-32(%rbx), %eax
	cmpb	$94, %al
	jbe	.L1834
	cmpb	$31, %bl
	jbe	.L1835
	movl	%ebx, %eax
	andl	$-32, %eax
	cmpb	$-64, %al
	je	.L1836
	movl	%ebx, %eax
	andl	$-16, %eax
	cmpb	$-32, %al
	je	.L1837
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-16, %al
	jne	.L1766
	andl	$7, %ebx
	movl	$3, %eax
	movl	$3, %ecx
.L1784:
	addq	%r12, %rax
	cmpq	%r13, %rax
	ja	.L1766
	movzbl	1(%r12,%r14), %edx
	leal	-1(%rcx), %eax
	leaq	1(%r12), %r8
	movl	%edx, %ecx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L1786
	sall	$6, %ebx
	andl	$63, %edx
	orl	%edx, %ebx
.L1786:
	testl	%eax, %eax
	je	.L1787
	movzbl	2(%r14,%r12), %edx
	movl	%edx, %ecx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L1788
	sall	$6, %ebx
	andl	$63, %edx
	orl	%edx, %ebx
.L1788:
	cmpl	$1, %eax
	je	.L1789
	movzbl	3(%r14,%r12), %edx
	movl	%edx, %ecx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L1789
	sall	$6, %ebx
	andl	$63, %edx
	orl	%edx, %ebx
.L1789:
	movslq	%eax, %r12
	leal	-127(%rbx), %eax
	addq	%r8, %r12
	cmpl	$1113984, %eax
	jbe	.L1838
	.p2align 4,,10
	.p2align 3
.L1766:
	addq	$1, %r12
	cmpq	%r12, %r13
	ja	.L1792
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	8(%rdi), %rbx
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	leaq	1(%rbx), %r12
	cmpq	%rdx, %rax
	je	.L1806
	movq	16(%rdi), %rdx
.L1794:
	cmpq	%rdx, %r12
	ja	.L1839
.L1795:
	movb	$34, (%rax,%rbx)
	movq	(%rdi), %rax
	movq	%r12, 8(%rdi)
	movb	$0, 1(%rax,%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1757:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1758
	cmpl	$2, %edx
	je	.L1799
	movl	$58, %r10d
	testb	$1, %al
	jne	.L1759
.L1799:
	movl	$44, %r10d
.L1759:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	leaq	16(%rbx), %rcx
	leaq	1(%rax), %r9
	cmpq	%rcx, %rdx
	je	.L1800
	movq	16(%rbx), %rcx
.L1760:
	cmpq	%rcx, %r9
	ja	.L1840
.L1761:
	movb	%r10b, (%rdx,%rax)
	movq	(%rbx), %rdx
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rdx,%rax)
	movq	16(%r15), %rbx
	movl	-4(%r12), %eax
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1827:
	movq	8(%rdi), %rsi
	movl	$2, %r8d
	xorl	%edx, %edx
	addq	$1, %r12
	leaq	.LC49(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1828:
	movq	8(%rdi), %rsi
	movl	$2, %r8d
	xorl	%edx, %edx
	addq	$1, %r12
	leaq	.LC50(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	8(%rdi), %rsi
	movl	$2, %r8d
	xorl	%edx, %edx
	addq	$1, %r12
	leaq	.LC51(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	8(%rdi), %rsi
	movl	$2, %r8d
	xorl	%edx, %edx
	addq	$1, %r12
	leaq	.LC52(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1831:
	movq	8(%rdi), %rsi
	movl	$2, %r8d
	xorl	%edx, %edx
	addq	$1, %r12
	leaq	.LC53(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1834:
	movzbl	%bl, %esi
	addq	$1, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1832:
	movq	8(%rdi), %rsi
	movl	$2, %r8d
	xorl	%edx, %edx
	addq	$1, %r12
	leaq	.LC54(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1821:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1833:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8(%rdi), %rsi
	movl	$2, %r8d
	xorl	%edx, %edx
	addq	$1, %r12
	leaq	.LC55(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1835:
	movq	8(%rdi), %rsi
	xorl	%edx, %edx
	movl	$2, %r8d
	leaq	.LC56(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	16(%r15), %rax
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	leaq	16(%rax), %r9
	leaq	1(%rsi), %r10
	cmpq	%rdx, %r9
	je	.L1802
	movq	16(%rax), %rcx
.L1775:
	cmpq	%r10, %rcx
	jb	.L1841
.L1776:
	movb	$48, (%rdx,%rsi)
	movq	(%rax), %rdx
	movq	%r10, 8(%rax)
	movb	$0, 1(%rdx,%rsi)
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	leaq	1(%rsi), %r10
	cmpq	%r9, %rdx
	je	.L1803
	movq	16(%rax), %rcx
.L1777:
	cmpq	%rcx, %r10
	ja	.L1842
.L1778:
	movb	$48, (%rdx,%rsi)
	movq	(%rax), %rdx
	movq	%r10, 8(%rax)
	movl	%ebx, %r10d
	movb	$0, 1(%rdx,%rsi)
	movq	8(%rax), %rsi
	shrb	$4, %r10b
	movq	(%rax), %rdx
	addl	$48, %r10d
	leaq	1(%rsi), %r11
	cmpq	%rdx, %r9
	je	.L1843
	movq	16(%rax), %rcx
.L1796:
	cmpq	%rcx, %r11
	ja	.L1844
.L1779:
	movb	%r10b, (%rdx,%rsi)
	andl	$15, %ebx
	movq	(%rax), %rdx
	cmpb	$10, %bl
	movq	%r11, 8(%rax)
	movb	$0, 1(%rdx,%rsi)
	sbbl	%edx, %edx
	movq	8(%rax), %rsi
	andl	$-39, %edx
	leal	87(%rbx,%rdx), %ebx
	movq	(%rax), %rdx
	leaq	1(%rsi), %r10
	cmpq	%rdx, %r9
	je	.L1805
	movq	16(%rax), %rcx
.L1781:
	cmpq	%rcx, %r10
	ja	.L1845
.L1782:
	movb	%bl, (%rdx,%rsi)
	addq	$1, %r12
	movq	%r10, 8(%rax)
	movq	(%rax), %rax
	movb	$0, 1(%rax,%rsi)
	movq	16(%r15), %rdi
	cmpq	%r12, %r13
	ja	.L1792
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1826:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1839:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rdi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rdi
	movq	(%rdi), %rax
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1824:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	addq	$512, %r12
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1836:
	andl	$31, %ebx
	movl	$1, %eax
	movl	$1, %ecx
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1801:
	movl	$15, %edx
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1806:
	movl	$15, %edx
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1837:
	andl	$15, %ebx
	movl	$2, %eax
	movl	$2, %ecx
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1840:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r9, -72(%rbp)
	movb	%r10b, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-72(%rbp), %r9
	movzbl	-64(%rbp), %r10d
	movq	-56(%rbp), %rax
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1845:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	movq	(%rax), %rdx
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1844:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r11, -88(%rbp)
	movb	%r10b, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %r11
	movzbl	-80(%rbp), %r10d
	movq	-72(%rbp), %r9
	movq	(%rax), %rdx
	movq	-64(%rbp), %rsi
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1842:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	movq	(%rax), %rdx
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1841:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-56(%rbp), %rax
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	(%rax), %rdx
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1800:
	movl	$15, %ecx
	jmp	.L1760
.L1787:
	leal	-127(%rbx), %eax
	movq	%r8, %r12
	cmpl	$1113984, %eax
	ja	.L1766
.L1798:
	leaq	.LC56(%rip), %rsi
	movq	%r8, -56(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0
	movq	16(%r15), %rsi
	movl	%ebx, %edi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvtPT_
	movq	-56(%rbp), %r8
	movq	16(%r15), %rdi
	movq	%r8, %r12
	jmp	.L1766
.L1838:
	cmpl	$65534, %ebx
	jbe	.L1846
	subl	$65536, %ebx
	leaq	.LC56(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0
	movl	%ebx, %edi
	movq	16(%r15), %rsi
	andw	$1023, %bx
	shrl	$10, %edi
	subw	$10240, %di
	movzwl	%di, %edi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvtPT_
	movq	16(%r15), %rdi
	leaq	.LC56(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4EmitEPKc.isra.0
	movq	16(%r15), %rsi
	leal	-9216(%rbx), %edi
	movzwl	%di, %edi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvtPT_
	movq	16(%r15), %rdi
	jmp	.L1766
.L1802:
	movl	$15, %ecx
	jmp	.L1775
.L1805:
	movl	$15, %ecx
	jmp	.L1781
.L1843:
	movl	$15, %ecx
	jmp	.L1796
.L1803:
	movl	$15, %ecx
	jmp	.L1777
.L1825:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L1846:
	movq	%r12, %r8
	jmp	.L1798
	.cfi_endproc
.LFE9728:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE:
.LFB9730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1847
	movq	80(%rdi), %r15
	movq	%rdi, %r12
	cmpq	88(%rdi), %r15
	je	.L1907
.L1849:
	movl	-8(%r15), %edx
	movq	16(%r12), %rbx
	movl	-4(%r15), %eax
	testl	%edx, %edx
	jne	.L1850
	testl	%eax, %eax
	jne	.L1908
.L1851:
	addl	$1, %eax
	leaq	16(%rbx), %rdx
	movl	%eax, -4(%r15)
	movq	8(%rbx), %r15
	movq	(%rbx), %rax
	leaq	1(%r15), %r13
	cmpq	%rdx, %rax
	je	.L1885
	movq	16(%rbx), %rdx
.L1855:
	cmpq	%rdx, %r13
	ja	.L1909
.L1856:
	movb	$34, (%rax,%r15)
	movq	(%rbx), %rax
	cmpq	$2, -56(%rbp)
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r15)
	movq	16(%r12), %r13
	jbe	.L1886
	leaq	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r15
	leaq	16(%r13), %rax
	movq	%r12, -96(%rbp)
	movl	$3, %r14d
	movq	%r15, %r12
	movq	%rax, %r15
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1859:
	movb	%r10b, (%rdx,%rsi)
	movq	0(%r13), %rdx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%rsi)
	movl	%ebx, %edx
	movq	8(%r13), %rsi
	shrl	$12, %edx
	andl	$63, %edx
	leaq	1(%rsi), %r9
	movzbl	(%r12,%rdx), %r10d
	movq	0(%r13), %rdx
	cmpq	%r15, %rdx
	je	.L1888
	movq	16(%r13), %rcx
.L1860:
	cmpq	%rcx, %r9
	ja	.L1910
.L1861:
	movb	%r10b, (%rdx,%rsi)
	movq	0(%r13), %rdx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%rsi)
	movl	%ebx, %edx
	movq	8(%r13), %rsi
	shrl	$6, %edx
	andl	$63, %edx
	leaq	1(%rsi), %r9
	movzbl	(%r12,%rdx), %r10d
	movq	0(%r13), %rdx
	cmpq	%r15, %rdx
	je	.L1889
	movq	16(%r13), %rcx
.L1862:
	cmpq	%rcx, %r9
	ja	.L1911
.L1863:
	movb	%r10b, (%rdx,%rsi)
	movq	0(%r13), %rdx
	andl	$63, %ebx
	movq	%r9, 8(%r13)
	movzbl	(%r12,%rbx), %r10d
	movb	$0, 1(%rdx,%rsi)
	movq	8(%r13), %rbx
	movq	0(%r13), %rdx
	leaq	1(%rbx), %r9
	cmpq	%r15, %rdx
	je	.L1890
	movq	16(%r13), %rcx
.L1864:
	cmpq	%rcx, %r9
	ja	.L1912
	movb	%r10b, (%rdx,%rbx)
.L1906:
	movq	0(%r13), %rdx
	movq	%r9, 8(%r13)
	movb	$0, 1(%rdx,%rbx)
	leaq	3(%r14), %rdx
	cmpq	%rdx, -56(%rbp)
	jb	.L1903
	movq	%rdx, %r14
.L1868:
	movq	-64(%rbp), %rax
	movq	8(%r13), %rsi
	movzbl	-3(%rax,%r14), %edx
	movzbl	-2(%rax,%r14), %ecx
	leaq	1(%rsi), %r9
	movzbl	-1(%rax,%r14), %ebx
	sall	$16, %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	orl	%edx, %ebx
	shrl	$18, %edx
	movzbl	(%r12,%rdx), %r10d
	movq	0(%r13), %rdx
	cmpq	%r15, %rdx
	je	.L1887
	movq	16(%r13), %rcx
.L1858:
	cmpq	%rcx, %r9
	jbe	.L1859
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	movb	%r10b, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %r10d
	movq	-72(%rbp), %rsi
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	-96(%rbp), %r12
	leaq	2(%r14), %rdx
	leaq	1(%r14), %rax
.L1857:
	cmpq	%rdx, -56(%rbp)
	jnb	.L1913
	cmpq	%rax, -56(%rbp)
	jnb	.L1914
.L1872:
	movq	16(%r12), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L1896
	movq	16(%rbx), %rdx
.L1881:
	cmpq	%rdx, %r13
	ja	.L1915
.L1882:
	movb	$34, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
.L1847:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1850:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L1851
	testb	$1, %al
	je	.L1883
	movl	$58, %r10d
	cmpl	$2, %edx
	jne	.L1852
.L1883:
	movl	$44, %r10d
.L1852:
	movq	8(%rbx), %r13
	movq	(%rbx), %rdx
	leaq	16(%rbx), %rcx
	leaq	1(%r13), %r14
	cmpq	%rcx, %rdx
	je	.L1884
	movq	16(%rbx), %rcx
.L1853:
	cmpq	%rcx, %r14
	ja	.L1916
.L1854:
	movb	%r10b, (%rdx,%r13)
	movq	(%rbx), %rdx
	movq	%r14, 8(%rbx)
	movb	$0, 1(%rdx,%r13)
	movq	16(%r12), %rbx
	movl	-4(%r15), %eax
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1887:
	movl	$15, %ecx
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1912:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r9, -80(%rbp)
	movb	%r10b, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movzbl	-72(%rbp), %r10d
	movq	-80(%rbp), %r9
	movb	%r10b, (%rdx,%rbx)
	jmp	.L1906
	.p2align 4,,10
	.p2align 3
.L1890:
	movl	$15, %ecx
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1911:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	movb	%r10b, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %r10d
	movq	-72(%rbp), %rsi
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1889:
	movl	$15, %ecx
	jmp	.L1862
	.p2align 4,,10
	.p2align 3
.L1910:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r9, -88(%rbp)
	movb	%r10b, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rdx
	movq	-88(%rbp), %r9
	movzbl	-80(%rbp), %r10d
	movq	-72(%rbp), %rsi
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1888:
	movl	$15, %ecx
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1909:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1915:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L1907:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r15
	addq	$512, %r15
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1914:
	movq	-64(%rbp), %rax
	leaq	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r15
	leaq	16(%r13), %r11
	movzbl	(%rax,%r14), %eax
	movq	8(%r13), %r14
	movl	%eax, %ebx
	shrl	$2, %eax
	leaq	1(%r14), %r9
	movzbl	(%r15,%rax), %r10d
	movq	0(%r13), %rax
	sall	$16, %ebx
	cmpq	%r11, %rax
	je	.L1892
	movq	16(%r13), %rdx
.L1873:
	cmpq	%rdx, %r9
	ja	.L1917
.L1874:
	movb	%r10b, (%rax,%r14)
	shrl	$12, %ebx
	movq	0(%r13), %rax
	andl	$48, %ebx
	movq	%r9, 8(%r13)
	movzbl	(%r15,%rbx), %r15d
	movb	$0, 1(%rax,%r14)
	movq	8(%r13), %rbx
	movq	0(%r13), %rax
	leaq	1(%rbx), %r14
	cmpq	%r11, %rax
	je	.L1893
	movq	16(%r13), %rdx
.L1875:
	cmpq	%rdx, %r14
	ja	.L1918
.L1876:
	movb	%r15b, (%rax,%rbx)
	movq	0(%r13), %rax
	movq	%r14, 8(%r13)
	movb	$0, 1(%rax,%rbx)
	movq	8(%r13), %rbx
	movq	0(%r13), %rax
	leaq	1(%rbx), %r14
	cmpq	%r11, %rax
	je	.L1894
	movq	16(%r13), %rdx
.L1877:
	cmpq	%rdx, %r14
	ja	.L1919
.L1878:
	movb	$61, (%rax,%rbx)
	movq	0(%r13), %rax
	movq	%r14, 8(%r13)
	movb	$0, 1(%rax,%rbx)
	movq	8(%r13), %rbx
	movq	0(%r13), %rax
	leaq	1(%rbx), %r14
	cmpq	%r11, %rax
	je	.L1895
	movq	16(%r13), %rdx
.L1879:
	cmpq	%rdx, %r14
	ja	.L1920
.L1880:
	movb	$61, (%rax,%rbx)
	movq	0(%r13), %rax
	movq	%r14, 8(%r13)
	movb	$0, 1(%rax,%rbx)
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	-64(%rbp), %rdi
	leaq	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r15
	movzbl	(%rdi,%r14), %edx
	movzbl	(%rdi,%rax), %ebx
	movq	%r13, %rdi
	sall	$16, %edx
	sall	$8, %ebx
	orl	%edx, %ebx
	shrl	$18, %edx
	movsbl	(%r15,%rdx), %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movl	%ebx, %eax
	movq	8(%r13), %rsi
	leaq	16(%r13), %rdx
	shrl	$12, %eax
	andl	$63, %eax
	leaq	1(%rsi), %r14
	movzbl	(%r15,%rax), %r9d
	movq	0(%r13), %rax
	cmpq	%rdx, %rax
	je	.L1891
	movq	16(%r13), %rdx
.L1870:
	cmpq	%rdx, %r14
	ja	.L1921
.L1871:
	movb	%r9b, (%rax,%rsi)
	movq	0(%r13), %rax
	shrl	$6, %ebx
	movq	%r13, %rdi
	movq	%r14, 8(%r13)
	andl	$60, %ebx
	movb	$0, 1(%rax,%rsi)
	movsbl	(%r15,%rbx), %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movl	$61, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1885:
	movl	$15, %edx
	jmp	.L1855
	.p2align 4,,10
	.p2align 3
.L1896:
	movl	$15, %edx
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L1916:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movb	%r10b, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movzbl	-72(%rbp), %r10d
	jmp	.L1854
	.p2align 4,,10
	.p2align 3
.L1886:
	movl	$1, %eax
	movl	$2, %edx
	xorl	%r14d, %r14d
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L1884:
	movl	$15, %ecx
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1918:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r11, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movq	-56(%rbp), %r11
	jmp	.L1876
	.p2align 4,,10
	.p2align 3
.L1917:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	movb	%r10b, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %r9
	movzbl	-56(%rbp), %r10d
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L1920:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1919:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r11, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movq	-56(%rbp), %r11
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L1921:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movb	%r9b, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	0(%r13), %rax
	movzbl	-64(%rbp), %r9d
	movq	-56(%rbp), %rsi
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1895:
	movl	$15, %edx
	jmp	.L1879
	.p2align 4,,10
	.p2align 3
.L1894:
	movl	$15, %edx
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1893:
	movl	$15, %edx
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1892:
	movl	$15, %edx
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1891:
	movl	$15, %edx
	jmp	.L1870
.L1908:
	leaq	.LC35(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9730:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE
	.section	.text._ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	8(%r13), %rdx
	movl	$4, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	addq	%rsi, %rdx
	movq	%rax, 16(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	%rbx, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4650:
	.size	_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node9inspector8protocol15DictionaryValue6createEv,"axG",@progbits,_ZN4node9inspector8protocol15DictionaryValue6createEv,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol15DictionaryValue6createEv
	.type	_ZN4node9inspector8protocol15DictionaryValue6createEv, @function
_ZN4node9inspector8protocol15DictionaryValue6createEv:
.LFB4686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$96, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rcx
	leaq	64(%rax), %rdx
	movl	$6, 8(%rax)
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	$1, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movl	$0x3f800000, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4686:
	.size	_ZN4node9inspector8protocol15DictionaryValue6createEv, .-_ZN4node9inspector8protocol15DictionaryValue6createEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupportC2Ev
	.type	_ZN4node9inspector8protocol12ErrorSupportC2Ev, @function
_ZN4node9inspector8protocol12ErrorSupportC2Ev:
.LFB5324:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE5324:
	.size	_ZN4node9inspector8protocol12ErrorSupportC2Ev, .-_ZN4node9inspector8protocol12ErrorSupportC2Ev
	.globl	_ZN4node9inspector8protocol12ErrorSupportC1Ev
	.set	_ZN4node9inspector8protocol12ErrorSupportC1Ev,_ZN4node9inspector8protocol12ErrorSupportC2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupportD2Ev
	.type	_ZN4node9inspector8protocol12ErrorSupportD2Ev, @function
_ZN4node9inspector8protocol12ErrorSupportD2Ev:
.LFB5327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %r13
	movq	24(%rdi), %r12
	cmpq	%r12, %r13
	je	.L1928
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1929
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1932
.L1930:
	movq	24(%rbx), %r12
.L1928:
	testq	%r12, %r12
	je	.L1933
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1933:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1934
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1935
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1938
.L1936:
	movq	(%rbx), %r12
.L1934:
	testq	%r12, %r12
	je	.L1927
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1935:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1938
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1929:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1932
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1927:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5327:
	.size	_ZN4node9inspector8protocol12ErrorSupportD2Ev, .-_ZN4node9inspector8protocol12ErrorSupportD2Ev
	.globl	_ZN4node9inspector8protocol12ErrorSupportD1Ev
	.set	_ZN4node9inspector8protocol12ErrorSupportD1Ev,_ZN4node9inspector8protocol12ErrorSupportD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc
	.type	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc, @function
_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc:
.LFB5329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -96(%rbp)
	testq	%rsi, %rsi
	je	.L1954
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	-96(%rbp), %r14
	movq	%rsi, %r15
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L1955
	cmpq	$1, %rax
	jne	.L1948
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L1949:
	movq	%rax, -88(%rbp)
	movq	%r14, %rsi
	movb	$0, (%rdx,%rax)
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1944
	call	_ZdlPv@PLT
.L1944:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1956
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1948:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L1957
	movq	%r13, %rdx
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	%r14, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1947:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1954:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1956:
	call	__stack_chk_fail@PLT
.L1957:
	movq	%r13, %rdi
	jmp	.L1947
	.cfi_endproc
.LFE5329:
	.size	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc, .-_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport7setNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol12ErrorSupport7setNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol12ErrorSupport7setNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5330:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	leaq	-32(%rax), %rdi
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	.cfi_endproc
.LFE5330:
	.size	_ZN4node9inspector8protocol12ErrorSupport7setNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol12ErrorSupport7setNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport3popEv
	.type	_ZN4node9inspector8protocol12ErrorSupport3popEv, @function
_ZN4node9inspector8protocol12ErrorSupport3popEv:
.LFB5332:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 8(%rdi)
	movq	-16(%rax), %rdi
	cmpq	%rax, %rdi
	je	.L1959
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1959:
	ret
	.cfi_endproc
.LFE5332:
	.size	_ZN4node9inspector8protocol12ErrorSupport3popEv, .-_ZN4node9inspector8protocol12ErrorSupport3popEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv
	.type	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv, @function
_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv:
.LFB5335:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE5335:
	.size	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv, .-_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv
	.section	.rodata.str1.1
.LC58:
	.string	"; "
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport6errorsB5cxx11Ev
	.type	_ZN4node9inspector8protocol12ErrorSupport6errorsB5cxx11Ev, @function
_ZN4node9inspector8protocol12ErrorSupport6errorsB5cxx11Ev:
.LFB5336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-432(%rbp), %rbx
	subq	$424, %rsp
	movq	.LC32(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	.LC33(%rip), %xmm1
	movaps	%xmm1, -448(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%rbx, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-448(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -456(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	24(%r15), %rax
	cmpq	%rax, 32(%r15)
	je	.L1968
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	%r14, %rdx
	movq	%rbx, %rdi
	addq	$1, %r14
	salq	$5, %rdx
	addq	%rdx, %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%r15), %rax
	movq	32(%r15), %rdx
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %r14
	jnb	.L1968
	testq	%r14, %r14
	je	.L1963
	movl	$2, %edx
	leaq	.LC58(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	movq	24(%r15), %rax
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L1968:
	leaq	16(%r12), %rax
	movb	$0, 16(%r12)
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	testq	%rax, %rax
	je	.L1981
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1982
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1970:
	movq	.LC32(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC34(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-448(%rbp), %rdi
	je	.L1971
	call	_ZdlPv@PLT
.L1971:
	movq	-456(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1983
	addq	$424, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1982:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1981:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1970
.L1983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5336:
	.size	_ZN4node9inspector8protocol12ErrorSupport6errorsB5cxx11Ev, .-_ZN4node9inspector8protocol12ErrorSupport6errorsB5cxx11Ev
	.p2align 4
	.type	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0, @function
_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0:
.LFB10231:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%edx, -124(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	$88, %edi
	movq	16(%rax), %r15
	call	_Znwm@PLT
	movl	-124(%rbp), %edx
	movq	-120(%rbp), %rcx
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorE(%rip), %rax
	movl	%edx, 8(%rbx)
	movq	(%rcx), %rsi
	leaq	16(%rbx), %rdi
	movq	8(%rcx), %rdx
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rbx)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	64(%rbx), %rax
	movb	$0, 64(%rbx)
	movq	%rax, 48(%rbx)
	movq	%rax, -120(%rbp)
	movq	$0, 56(%rbx)
	movl	%r14d, 80(%rbx)
	movb	$1, 84(%rbx)
	testq	%r12, %r12
	je	.L1985
	movq	32(%r12), %rax
	cmpq	%rax, 24(%r12)
	je	.L1985
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-80(%rbp), %r12
	call	_ZN4node9inspector8protocol12ErrorSupport6errorsB5cxx11Ev
	movq	-96(%rbp), %rcx
	movq	48(%rbx), %rdi
	movq	-88(%rbp), %rdx
	movq	-120(%rbp), %rax
	cmpq	%r12, %rcx
	je	.L2008
	movq	-80(%rbp), %rsi
	cmpq	%rdi, %rax
	je	.L2009
	movq	%rdx, %xmm0
	movq	%rsi, %xmm1
	movq	64(%rbx), %rax
	movq	%rcx, 48(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	testq	%rdi, %rdi
	je	.L1991
	movq	%rdi, -96(%rbp)
	movq	%rax, -80(%rbp)
.L1989:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1985
	call	_ZdlPv@PLT
.L1985:
	movq	%r13, %rdi
	movq	%rbx, -104(%rbp)
	leaq	-104(%rbp), %rdx
	movl	%r14d, %esi
	call	*%r15
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1984
	movq	(%rdi), %rax
	call	*24(%rax)
.L1984:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2010
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2009:
	.cfi_restore_state
	movq	%rdx, %xmm0
	movq	%rsi, %xmm2
	movq	%rcx, 48(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
.L1991:
	movq	%r12, -96(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2008:
	testq	%rdx, %rdx
	je	.L1987
	cmpq	$1, %rdx
	je	.L2011
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	48(%rbx), %rdi
.L1987:
	movq	%rdx, 56(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2011:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	48(%rbx), %rdi
	jmp	.L1987
.L2010:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10231:
	.size	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0, .-_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol5Value12toJSONStringB5cxx11Ev
	.type	_ZNK4node9inspector8protocol5Value12toJSONStringB5cxx11Ev, @function
_ZNK4node9inspector8protocol5Value12toJSONStringB5cxx11Ev:
.LFB5356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-432(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$424, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -456(%rbp)
	movq	.LC32(%rip), %xmm1
	movhps	.LC33(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -448(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rbx), %rdi
	movq	$0, -104(%rbp)
	addq	%r15, %rdi
	movq	%rbx, -432(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-448(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-456(%rbp), %r8
	movq	%r15, %rsi
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*72(%rax)
	leaq	16(%r14), %rax
	movq	$0, 8(%r14)
	movq	%rax, (%r14)
	movq	-384(%rbp), %rax
	movb	$0, 16(%r14)
	testq	%rax, %rax
	je	.L2013
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L2019
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2015:
	movq	.LC32(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC34(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-448(%rbp), %rdi
	je	.L2016
	call	_ZdlPv@PLT
.L2016:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-24(%rbx), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	%rbx, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2020
	addq	$424, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2019:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2013:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L2015
.L2020:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5356:
	.size	_ZNK4node9inspector8protocol5Value12toJSONStringB5cxx11Ev, .-_ZNK4node9inspector8protocol5Value12toJSONStringB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev:
.LFB5357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol5Value12toJSONStringB5cxx11Ev
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2024
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2024:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5357:
	.size	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValueC2Ev
	.type	_ZN4node9inspector8protocol15DictionaryValueC2Ev, @function
_ZN4node9inspector8protocol15DictionaryValueC2Ev:
.LFB5439:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$6, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	$1, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	$0x3f800000, 48(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE5439:
	.size	_ZN4node9inspector8protocol15DictionaryValueC2Ev, .-_ZN4node9inspector8protocol15DictionaryValueC2Ev
	.globl	_ZN4node9inspector8protocol15DictionaryValueC1Ev
	.set	_ZN4node9inspector8protocol15DictionaryValueC1Ev,_ZN4node9inspector8protocol15DictionaryValueC2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol9ListValueC2Ev
	.type	_ZN4node9inspector8protocol9ListValueC2Ev, @function
_ZN4node9inspector8protocol9ListValueC2Ev:
.LFB5459:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$7, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE5459:
	.size	_ZN4node9inspector8protocol9ListValueC2Ev, .-_ZN4node9inspector8protocol9ListValueC2Ev
	.globl	_ZN4node9inspector8protocol9ListValueC1Ev
	.set	_ZN4node9inspector8protocol9ListValueC1Ev,_ZN4node9inspector8protocol9ListValueC2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol9ListValue2atEm
	.type	_ZN4node9inspector8protocol9ListValue2atEm, @function
_ZN4node9inspector8protocol9ListValue2atEm:
.LFB5462:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE5462:
	.size	_ZN4node9inspector8protocol9ListValue2atEm, .-_ZN4node9inspector8protocol9ListValue2atEm
	.p2align 4
	.globl	_ZN4node9inspector8protocol24escapeLatinStringForJSONEPKhjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol24escapeLatinStringForJSONEPKhjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol24escapeLatinStringForJSONEPKhjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5463:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L2051
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rsi), %eax
	addq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	.L2033(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movzbl	0(%r13), %ebx
	movq	%rax, -56(%rbp)
	cmpb	$34, %bl
	ja	.L2030
	.p2align 4,,10
	.p2align 3
.L2054:
	cmpb	$7, %bl
	jbe	.L2031
	leal	-8(%rbx), %eax
	cmpb	$26, %al
	ja	.L2031
	movzbl	%al, %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2033:
	.long	.L2038-.L2033
	.long	.L2037-.L2033
	.long	.L2036-.L2033
	.long	.L2031-.L2033
	.long	.L2035-.L2033
	.long	.L2034-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2031-.L2033
	.long	.L2032-.L2033
	.text
.L2032:
	movl	$2, %edx
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	.p2align 4,,10
	.p2align 3
.L2042:
	leaq	1(%r13), %rax
	cmpq	%r13, -56(%rbp)
	je	.L2028
.L2055:
	movq	%rax, %r13
	movzbl	0(%r13), %ebx
	cmpb	$34, %bl
	jbe	.L2054
.L2030:
	cmpb	$92, %bl
	jne	.L2031
	movl	$2, %edx
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	leaq	1(%r13), %rax
	cmpq	%r13, -56(%rbp)
	jne	.L2055
.L2028:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2034:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2042
.L2035:
	movl	$2, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2042
.L2036:
	movl	$2, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2042
.L2037:
	movl	$2, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2042
.L2038:
	movl	$2, %edx
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2042
.L2031:
	leal	-32(%rbx), %eax
	cmpb	$94, %al
	jbe	.L2056
	movl	$2, %edx
	leaq	.LC56(%rip), %rsi
	movq	%r12, %rdi
	movl	$4, %r15d
	call	_ZNSo5writeEPKcl@PLT
.L2043:
	movl	%ebx, %eax
	leaq	_ZN4node9inspector8protocol12_GLOBAL__N_1L9hexDigitsE(%rip), %rcx
	movq	%r12, %rdi
	sall	$4, %ebx
	shrw	$12, %ax
	andl	$15, %eax
	movsbl	(%rcx,%rax), %esi
	call	_ZNSo3putEc@PLT
	subq	$1, %r15
	jne	.L2043
	jmp	.L2042
	.p2align 4,,10
	.p2align 3
.L2056:
	movzbl	%bl, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	jmp	.L2042
	.p2align 4,,10
	.p2align 3
.L2051:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE5463:
	.size	_ZN4node9inspector8protocol24escapeLatinStringForJSONEPKhjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol24escapeLatinStringForJSONEPKhjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol23escapeWideStringForJSONEPKtjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol23escapeWideStringForJSONEPKtjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol23escapeWideStringForJSONEPKtjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5464:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	je	.L2080
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rsi), %eax
	leaq	2(%rdi,%rax,2), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	.L2062(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L2074:
	movzwl	(%r12), %r15d
	cmpw	$34, %r15w
	ja	.L2059
	cmpw	$7, %r15w
	jbe	.L2060
	leal	-8(%r15), %eax
	cmpw	$26, %ax
	ja	.L2060
	movzwl	%ax, %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2062:
	.long	.L2067-.L2062
	.long	.L2066-.L2062
	.long	.L2065-.L2062
	.long	.L2060-.L2062
	.long	.L2064-.L2062
	.long	.L2063-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2060-.L2062
	.long	.L2061-.L2062
	.text
.L2061:
	movl	$2, %edx
	leaq	.LC49(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	.p2align 4,,10
	.p2align 3
.L2071:
	addq	$2, %r12
	cmpq	-56(%rbp), %r12
	jne	.L2074
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2063:
	.cfi_restore_state
	movl	$2, %edx
	leaq	.LC54(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2071
.L2064:
	movl	$2, %edx
	leaq	.LC52(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2071
.L2065:
	movl	$2, %edx
	leaq	.LC53(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2071
.L2066:
	movl	$2, %edx
	leaq	.LC55(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2071
.L2067:
	movl	$2, %edx
	leaq	.LC51(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2071
.L2060:
	leal	-32(%r15), %eax
	cmpw	$94, %ax
	jbe	.L2083
	movl	$2, %edx
	leaq	.LC56(%rip), %rsi
	movq	%rbx, %rdi
	movl	$4, %r14d
	call	_ZNSo5writeEPKcl@PLT
.L2072:
	movl	%r15d, %eax
	leaq	_ZN4node9inspector8protocol12_GLOBAL__N_1L9hexDigitsE(%rip), %rcx
	movq	%rbx, %rdi
	sall	$4, %r15d
	shrw	$12, %ax
	andl	$15, %eax
	movsbl	(%rcx,%rax), %esi
	call	_ZNSo3putEc@PLT
	subq	$1, %r14
	jne	.L2072
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2059:
	cmpw	$92, %r15w
	jne	.L2060
	movl	$2, %edx
	leaq	.LC50(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSo5writeEPKcl@PLT
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2083:
	movzwl	%r15w, %esi
	movq	%rbx, %rdi
	call	_ZNSo3putEc@PLT
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2080:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE5464:
	.size	_ZN4node9inspector8protocol23escapeWideStringForJSONEPKtjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol23escapeWideStringForJSONEPKtjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol6Object7toValueEv
	.type	_ZNK4node9inspector8protocol6Object7toValueEv, @function
_ZNK4node9inspector8protocol6Object7toValueEv:
.LFB5468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	-32(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2085
	cmpl	$6, 8(%rdx)
	movl	$0, %eax
	cmovne	%rax, %rdx
.L2085:
	movq	%rdx, (%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2092
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2092:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5468:
	.size	_ZNK4node9inspector8protocol6Object7toValueEv, .-_ZNK4node9inspector8protocol6Object7toValueEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol6Object5cloneEv
	.type	_ZNK4node9inspector8protocol6Object5cloneEv, @function
_ZNK4node9inspector8protocol6Object5cloneEv:
.LFB5469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-32(%rbp), %rdi
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	-32(%rbp), %rbx
	movq	$0, -32(%rbp)
	testq	%rbx, %rbx
	je	.L2094
	cmpl	$6, 8(%rbx)
	movl	$0, %eax
	cmovne	%rax, %rbx
.L2094:
	movl	$8, %edi
	call	_Znwm@PLT
	movq	-32(%rbp), %rdi
	movq	%rbx, (%rax)
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2093
	movq	(%rdi), %rax
	call	*24(%rax)
.L2093:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2105
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5469:
	.size	_ZNK4node9inspector8protocol6Object5cloneEv, .-_ZNK4node9inspector8protocol6Object5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol6ObjectC2ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE
	.type	_ZN4node9inspector8protocol6ObjectC2ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE, @function
_ZN4node9inspector8protocol6ObjectC2ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE:
.LFB5471:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE5471:
	.size	_ZN4node9inspector8protocol6ObjectC2ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE, .-_ZN4node9inspector8protocol6ObjectC2ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE
	.globl	_ZN4node9inspector8protocol6ObjectC1ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE
	.set	_ZN4node9inspector8protocol6ObjectC1ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE,_ZN4node9inspector8protocol6ObjectC2ESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS4_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol6ObjectD2Ev
	.type	_ZN4node9inspector8protocol6ObjectD2Ev, @function
_ZN4node9inspector8protocol6ObjectD2Ev:
.LFB5474:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2107
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2107:
	ret
	.cfi_endproc
.LFE5474:
	.size	_ZN4node9inspector8protocol6ObjectD2Ev, .-_ZN4node9inspector8protocol6ObjectD2Ev
	.globl	_ZN4node9inspector8protocol6ObjectD1Ev
	.set	_ZN4node9inspector8protocol6ObjectD1Ev,_ZN4node9inspector8protocol6ObjectD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16DispatchResponse2OKEv
	.type	_ZN4node9inspector8protocol16DispatchResponse2OKEv, @function
_ZN4node9inspector8protocol16DispatchResponse2OKEv:
.LFB5476:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rdx
	movb	$0, 24(%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$0, (%rdi)
	movl	$-32700, 40(%rdi)
	ret
	.cfi_endproc
.LFE5476:
	.size	_ZN4node9inspector8protocol16DispatchResponse2OKEv, .-_ZN4node9inspector8protocol16DispatchResponse2OKEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	subq	$8, %rsp
	movq	%rax, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movl	$1, (%r12)
	movl	$-32000, 40(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5486:
	.size	_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC59:
	.string	"Internal error"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16DispatchResponse13InternalErrorEv
	.type	_ZN4node9inspector8protocol16DispatchResponse13InternalErrorEv, @function
_ZN4node9inspector8protocol16DispatchResponse13InternalErrorEv:
.LFB5487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$14, %r8d
	leaq	.LC59(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	subq	$8, %rsp
	movq	%rax, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movl	$1, (%r12)
	movl	$-32603, 40(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5487:
	.size	_ZN4node9inspector8protocol16DispatchResponse13InternalErrorEv, .-_ZN4node9inspector8protocol16DispatchResponse13InternalErrorEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16DispatchResponse13InvalidParamsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol16DispatchResponse13InvalidParamsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol16DispatchResponse13InvalidParamsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	subq	$8, %rsp
	movq	%rax, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movl	$1, (%r12)
	movl	$-32602, 40(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5488:
	.size	_ZN4node9inspector8protocol16DispatchResponse13InvalidParamsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol16DispatchResponse13InvalidParamsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16DispatchResponse11FallThroughEv
	.type	_ZN4node9inspector8protocol16DispatchResponse11FallThroughEv, @function
_ZN4node9inspector8protocol16DispatchResponse11FallThroughEv:
.LFB5489:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rdx
	movb	$0, 24(%rdi)
	movq	%rdi, %rax
	movq	%rdx, 8(%rdi)
	movq	$0, 16(%rdi)
	movl	$2, (%rdi)
	movl	$-32700, 40(%rdi)
	ret
	.cfi_endproc
.LFE5489:
	.size	_ZN4node9inspector8protocol16DispatchResponse11FallThroughEv, .-_ZN4node9inspector8protocol16DispatchResponse11FallThroughEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC2EPS2_
	.type	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC2EPS2_, @function
_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC2EPS2_:
.LFB5491:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE5491:
	.size	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC2EPS2_, .-_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC2EPS2_
	.globl	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC1EPS2_
	.set	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC1EPS2_,_ZN4node9inspector8protocol14DispatcherBase7WeakPtrC2EPS2_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_
	.type	_ZN4node9inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_, @function
_ZN4node9inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_:
.LFB5498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	movq	%rax, -24(%rdi)
	movq	(%rsi), %rax
	movl	%edx, -8(%rdi)
	movq	8(%rcx), %rdx
	movq	%rax, -16(%rdi)
	leaq	40(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	$0, (%rsi)
	movq	(%rcx), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	72(%rbx), %rax
	movq	8(%r12), %rdx
	leaq	56(%rbx), %rdi
	movq	%rax, 56(%rbx)
	movq	(%r12), %rsi
	popq	%rbx
	popq	%r12
	addq	%rsi, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	.cfi_endproc
.LFE5498:
	.size	_ZN4node9inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_, .-_ZN4node9inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_
	.globl	_ZN4node9inspector8protocol14DispatcherBase8CallbackC1ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_
	.set	_ZN4node9inspector8protocol14DispatcherBase8CallbackC1ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_,_ZN4node9inspector8protocol14DispatcherBase8CallbackC2ESt10unique_ptrINS2_7WeakPtrESt14default_deleteIS5_EEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESG_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE
	.type	_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE, @function
_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE:
.LFB5529:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol14DispatcherBaseE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	$1, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movl	$0x3f800000, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	ret
	.cfi_endproc
.LFE5529:
	.size	_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE, .-_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE
	.globl	_ZN4node9inspector8protocol14DispatcherBaseC1EPNS1_15FrontendChannelE
	.set	_ZN4node9inspector8protocol14DispatcherBaseC1EPNS1_15FrontendChannelE,_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBaseD2Ev
	.type	_ZN4node9inspector8protocol14DispatcherBaseD2Ev, @function
_ZN4node9inspector8protocol14DispatcherBaseD2Ev:
.LFB5532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol14DispatcherBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	testq	%rbx, %rbx
	je	.L2122
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L2123:
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	$0, (%rdx)
	testq	%rax, %rax
	jne	.L2123
	.p2align 4,,10
	.p2align 3
.L2124:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2124
.L2122:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	addq	$64, %r12
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-40(%r12), %rax
	movq	-48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%r12), %rdi
	movq	$0, -24(%r12)
	movq	$0, -32(%r12)
	cmpq	%r12, %rdi
	je	.L2121
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2121:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5532:
	.size	_ZN4node9inspector8protocol14DispatcherBaseD2Ev, .-_ZN4node9inspector8protocol14DispatcherBaseD2Ev
	.globl	_ZN4node9inspector8protocol14DispatcherBaseD1Ev
	.set	_ZN4node9inspector8protocol14DispatcherBaseD1Ev,_ZN4node9inspector8protocol14DispatcherBaseD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBaseD0Ev
	.type	_ZN4node9inspector8protocol14DispatcherBaseD0Ev, @function
_ZN4node9inspector8protocol14DispatcherBaseD0Ev:
.LFB5534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol14DispatcherBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	testq	%rbx, %rbx
	je	.L2133
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	$0, (%rdx)
	testq	%rax, %rax
	jne	.L2134
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2135
.L2133:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L2136
	call	_ZdlPv@PLT
.L2136:
	popq	%rbx
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5534:
	.size	_ZN4node9inspector8protocol14DispatcherBaseD0Ev, .-_ZN4node9inspector8protocol14DispatcherBaseD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE:
.LFB5555:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2143
	jmp	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0
	.p2align 4,,10
	.p2align 3
.L2143:
	ret
	.cfi_endproc
.LFE5555:
	.size	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase13clearFrontendEv
	.type	_ZN4node9inspector8protocol14DispatcherBase13clearFrontendEv, @function
_ZN4node9inspector8protocol14DispatcherBase13clearFrontendEv:
.LFB5556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rbx
	movq	$0, 8(%rdi)
	testq	%rbx, %rbx
	je	.L2146
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L2147:
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	$0, (%rdx)
	testq	%rax, %rax
	jne	.L2147
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2148
.L2146:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	popq	%rbx
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5556:
	.size	_ZN4node9inspector8protocol14DispatcherBase13clearFrontendEv, .-_ZN4node9inspector8protocol14DispatcherBase13clearFrontendEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcherC2EPNS1_15FrontendChannelE
	.type	_ZN4node9inspector8protocol14UberDispatcherC2EPNS1_15FrontendChannelE, @function
_ZN4node9inspector8protocol14UberDispatcherC2EPNS1_15FrontendChannelE:
.LFB5601:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol14UberDispatcherE(%rip), %rax
	movss	.LC57(%rip), %xmm0
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 16(%rdi)
	leaq	120(%rdi), %rax
	movq	$1, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	%rax, 72(%rdi)
	movq	$1, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 120(%rdi)
	movss	%xmm0, 48(%rdi)
	movss	%xmm0, 104(%rdi)
	ret
	.cfi_endproc
.LFE5601:
	.size	_ZN4node9inspector8protocol14UberDispatcherC2EPNS1_15FrontendChannelE, .-_ZN4node9inspector8protocol14UberDispatcherC2EPNS1_15FrontendChannelE
	.globl	_ZN4node9inspector8protocol14UberDispatcherC1EPNS1_15FrontendChannelE
	.set	_ZN4node9inspector8protocol14UberDispatcherC1EPNS1_15FrontendChannelE,_ZN4node9inspector8protocol14UberDispatcherC2EPNS1_15FrontendChannelE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE
	.type	_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE, @function
_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE:
.LFB5603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$3339675911, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r15), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	80(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rcx
	movq	72(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L2157
	movq	(%rax), %rbx
	movq	%rdx, %r9
	movq	48(%rbx), %rsi
.L2160:
	cmpq	%rsi, %r8
	je	.L2201
.L2158:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2157
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	je	.L2160
.L2157:
	movl	$56, %edi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	(%r15), %rsi
	movq	8(%r15), %rdx
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	96(%r12), %rdx
	movq	80(%r12), %rsi
	movq	$0, 40(%rbx)
	leaq	104(%r12), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	movq	%rdx, %r15
	jne	.L2161
	movq	72(%r12), %r9
	movq	%r8, 48(%rbx)
	addq	%r9, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L2171
.L2204:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L2172:
	addq	$1, 96(%r12)
.L2200:
	movq	0(%r13), %rax
	movq	40(%rbx), %rdi
	addq	$40, %rbx
	movq	$0, 0(%r13)
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L2156
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2201:
	.cfi_restore_state
	movq	8(%r15), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L2158
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	testq	%rdx, %rdx
	je	.L2200
	movq	8(%rbx), %rsi
	movq	(%r15), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	movq	-72(%rbp), %r9
	je	.L2200
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2156:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2161:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L2202
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2203
	leaq	0(,%rdx,8), %r14
	movq	%r8, -56(%rbp)
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-56(%rbp), %r8
	leaq	120(%r12), %r11
	movq	%rax, %r9
.L2164:
	movq	88(%r12), %rsi
	movq	$0, 88(%r12)
	testq	%rsi, %rsi
	je	.L2166
	xorl	%edi, %edi
	leaq	88(%r12), %r10
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2168:
	movq	(%r14), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2169:
	testq	%rsi, %rsi
	je	.L2166
.L2167:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r15
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	jne	.L2168
	movq	88(%r12), %r14
	movq	%r14, (%rcx)
	movq	%rcx, 88(%r12)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L2176
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2167
	.p2align 4,,10
	.p2align 3
.L2166:
	movq	72(%r12), %rdi
	cmpq	%r11, %rdi
	je	.L2170
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
.L2170:
	movq	%r8, %rax
	xorl	%edx, %edx
	movq	%r15, 80(%r12)
	divq	%r15
	movq	%r9, 72(%r12)
	movq	%r8, 48(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r9, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L2204
.L2171:
	movq	88(%r12), %rax
	movq	%rbx, 88(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L2173
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	80(%r12)
	movq	%rbx, (%r9,%rdx,8)
.L2173:
	leaq	88(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L2172
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	%rdx, %rdi
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2202:
	movq	$0, 120(%r12)
	leaq	120(%r12), %r9
	movq	%r9, %r11
	jmp	.L2164
.L2203:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5603:
	.size	_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE, .-_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE
	.type	_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE, @function
_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE:
.LFB5605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	48(%rdi), %rax
	movq	%rax, -96(%rbp)
	leaq	32(%rdi), %rax
	movq	%rax, -104(%rbp)
	testq	%r12, %r12
	je	.L2205
	movq	%r12, %r15
	movq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L2206:
	movq	16(%r15), %rsi
	movq	8(%r15), %rdi
	leaq	40(%r15), %rax
	movl	$3339675911, %edx
	movq	%rax, -72(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	24(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%rcx
	movq	16(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L2207
	movq	(%rax), %rbx
	movq	72(%rbx), %rsi
.L2210:
	cmpq	%rsi, %r13
	je	.L2270
.L2208:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2207
	movq	72(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L2210
.L2207:
	movl	$80, %edi
	call	_Znwm@PLT
	movq	16(%r15), %r10
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	%rdi, 8(%rax)
	movq	8(%r15), %r11
	movq	%r11, %rax
	addq	%r10, %rax
	je	.L2211
	testq	%r11, %r11
	je	.L2271
.L2211:
	movq	%r10, -64(%rbp)
	cmpq	$15, %r10
	ja	.L2272
	cmpq	$1, %r10
	jne	.L2214
	movzbl	(%r11), %eax
	movb	%al, 24(%rbx)
.L2215:
	leaq	56(%rbx), %rax
	movq	%r10, 16(%rbx)
	movl	$1, %ecx
	movb	$0, (%rdi,%r10)
	movq	-96(%rbp), %rdi
	movq	40(%r12), %rdx
	movq	24(%r12), %rsi
	movq	%rax, 40(%rbx)
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r10
	testb	%al, %al
	jne	.L2216
	movq	16(%r12), %r11
	movq	%r13, 72(%rbx)
	addq	%r11, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L2226
.L2276:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L2227:
	addq	$1, 40(%r12)
.L2269:
	movq	-72(%rbp), %rsi
	leaq	40(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L2206
.L2205:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2273
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2270:
	.cfi_restore_state
	movq	16(%r15), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L2208
	movq	%r10, -88(%rbp)
	movq	%rcx, -80(%rbp)
	testq	%rdx, %rdx
	je	.L2269
	movq	8(%rbx), %rsi
	movq	8(%r15), %rdi
	call	memcmp@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %r10
	testl	%eax, %eax
	je	.L2269
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2214:
	testq	%r10, %r10
	je	.L2215
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2216:
	cmpq	$1, %rdx
	je	.L2274
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2275
	leaq	0(,%rdx,8), %r14
	movq	%rdx, -80(%rbp)
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-80(%rbp), %r10
	leaq	64(%r12), %r8
	movq	%rax, %r11
.L2219:
	movq	32(%r12), %rsi
	movq	$0, 32(%r12)
	testq	%rsi, %rsi
	je	.L2221
	movq	-104(%rbp), %r9
	xorl	%edi, %edi
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2223:
	movq	(%r14), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2224:
	testq	%rsi, %rsi
	je	.L2221
.L2222:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	72(%rcx), %rax
	divq	%r10
	leaq	(%r11,%rdx,8), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	jne	.L2223
	movq	32(%r12), %r14
	movq	%r14, (%rcx)
	movq	%rcx, 32(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2232
	movq	%rcx, (%r11,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2222
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	16(%r12), %rdi
	cmpq	%r8, %rdi
	je	.L2225
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r11
.L2225:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r10, 24(%r12)
	divq	%r10
	movq	%r11, 16(%r12)
	movq	%r13, 72(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r11, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L2276
.L2226:
	movq	32(%r12), %rax
	movq	%rbx, 32(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L2228
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	24(%r12)
	movq	%rbx, (%r11,%rdx,8)
.L2228:
	movq	-104(%rbp), %rax
	movq	%rax, (%r14)
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2272:
	leaq	8(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r10
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%rbx)
.L2213:
	movq	%r10, %rdx
	movq	%r11, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r10
	movq	8(%rbx), %rdi
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	%rdx, %rdi
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2274:
	movq	$0, 64(%r12)
	leaq	64(%r12), %r11
	movq	%r11, %r8
	jmp	.L2219
.L2275:
	call	_ZSt17__throw_bad_allocv@PLT
.L2271:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5605:
	.size	_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE, .-_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE
	.section	.rodata.str1.1
.LC60:
	.string	"."
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcher14findDispatcherERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol14UberDispatcher14findDispatcherERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol14UberDispatcher14findDispatcherERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	.LC60(%rip), %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	cmpq	%rax, _ZN4node9inspector8protocol10StringUtil9kNotFoundE(%rip)
	je	.L2294
	movq	8(%r14), %r12
	movq	(%r14), %r15
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	cmpq	%r12, %rax
	cmovbe	%rax, %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L2279
	testq	%r15, %r15
	je	.L2319
.L2279:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L2320
	cmpq	$1, %r12
	jne	.L2282
	movzbl	(%r15), %eax
	movb	%al, -80(%rbp)
	movq	%rbx, %rax
.L2283:
	movq	%r12, -88(%rbp)
	movl	$3339675911, %edx
	movb	$0, (%rax,%r12)
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	80(%r13), %r15
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%r15
	movq	72(%r13), %rax
	movq	(%rax,%rdx,8), %r12
	movq	%rdx, %r9
	testq	%r12, %r12
	je	.L2317
	movq	(%r12), %r12
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r13
	movq	48(%r12), %rcx
	testq	%r10, %r10
	je	.L2286
.L2289:
	cmpq	%rcx, %r8
	je	.L2321
.L2287:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L2285
	movq	48(%r12), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L2289
.L2297:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L2285:
	cmpq	%rbx, %r13
	je	.L2277
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2277:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2322
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2286:
	.cfi_restore_state
	cmpq	%rcx, %r8
	je	.L2323
	.p2align 4,,10
	.p2align 3
.L2290:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L2285
	movq	48(%r12), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	jne	.L2297
	cmpq	%rcx, %r8
	jne	.L2290
.L2323:
	cmpq	$0, 16(%r12)
	jne	.L2290
.L2288:
	movq	40(%r12), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L2324
	movq	40(%r12), %r12
.L2317:
	movq	-96(%rbp), %r13
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2282:
	testq	%r12, %r12
	jne	.L2325
	movq	%rbx, %rax
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2321:
	cmpq	16(%r12), %r10
	jne	.L2287
	movq	8(%r12), %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	movq	%r8, -128(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r10, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r8
	testl	%eax, %eax
	movq	-136(%rbp), %r9
	je	.L2288
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2320:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L2281:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	-96(%rbp), %r13
	xorl	%r12d, %r12d
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2294:
	xorl	%r12d, %r12d
	jmp	.L2277
.L2319:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2322:
	call	__stack_chk_fail@PLT
.L2325:
	movq	%rbx, %rdi
	jmp	.L2281
	.cfi_endproc
.LFE5607:
	.size	_ZN4node9inspector8protocol14UberDispatcher14findDispatcherERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol14UberDispatcher14findDispatcherERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS1_12SerializableESt14default_deleteIS4_EE
	.type	_ZN4node9inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS1_12SerializableESt14default_deleteIS4_EE, @function
_ZN4node9inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS1_12SerializableESt14default_deleteIS4_EE:
.LFB5614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdx)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdx
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%rbx)
	addq	%rsi, %rdx
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movl	%r13d, 8(%rbx)
	movq	%rax, 16(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	testq	%r15, %r15
	je	.L2327
	movq	%r15, 48(%rbx)
.L2328:
	movq	-96(%rbp), %rdi
	movq	%rbx, (%r12)
	cmpq	%r14, %rdi
	je	.L2326
	call	_ZdlPv@PLT
.L2326:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2332
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2327:
	.cfi_restore_state
	movq	$0, 48(%rbx)
	jmp	.L2328
.L2332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5614:
	.size	_ZN4node9inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS1_12SerializableESt14default_deleteIS4_EE, .-_ZN4node9inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS1_12SerializableESt14default_deleteIS4_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE
	.type	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE, @function
_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE:
.LFB5535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L2333
	cmpl	$1, (%rdx)
	movl	%esi, %r12d
	je	.L2359
	movq	0(%r13), %rax
	leaq	-56(%rbp), %rdi
	leaq	-64(%rbp), %rdx
	movq	16(%rax), %rbx
	movq	(%rcx), %rax
	movq	$0, (%rcx)
	movq	%rax, -64(%rbp)
	call	_ZN4node9inspector8protocol16InternalResponse14createResponseEiSt10unique_ptrINS1_12SerializableESt14default_deleteIS4_EE
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	leaq	-48(%rbp), %rdx
	movq	$0, -56(%rbp)
	movl	%r12d, %esi
	movq	%rax, -48(%rbp)
	call	*%rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2336
	movq	(%rdi), %rax
	call	*24(%rax)
.L2336:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L2337
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2338
	movq	48(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2339
	movq	(%rdi), %rax
	call	*24(%rax)
.L2339:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2340
	call	_ZdlPv@PLT
.L2340:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2337:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2333
	movq	(%rdi), %rax
	call	*24(%rax)
.L2333:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2360
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2359:
	.cfi_restore_state
	leaq	8(%rdx), %rcx
	movl	40(%rdx), %edx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2338:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2337
.L2360:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5535:
	.size	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE, .-_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE
	.type	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE, @function
_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE:
.LFB5538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$96, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	%r13d, %esi
	leaq	64(%rax), %rdx
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rcx
	movl	$6, 8(%rax)
	movq	%rcx, (%rax)
	leaq	-48(%rbp), %rcx
	movq	%rdx, 16(%rax)
	movq	%r14, %rdx
	movq	$1, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movl	$0x3f800000, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2361
	movq	(%rdi), %rax
	call	*24(%rax)
.L2361:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2368
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2368:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5538:
	.size	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE, .-_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE
	.type	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE, @function
_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE:
.LFB5616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdx), %r14
	movq	$0, (%rdx)
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	8(%r13), %rdx
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%rbx)
	addq	%rsi, %rdx
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movl	$0, 8(%rbx)
	movq	%rax, 16(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	testq	%r14, %r14
	je	.L2370
	movq	%r14, 48(%rbx)
	movq	%r12, %rax
	movq	%rbx, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2370:
	.cfi_restore_state
	movq	%rbx, (%r12)
	movq	%r12, %rax
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5616:
	.size	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE, .-_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16InternalResponseC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE
	.type	_ZN4node9inspector8protocol16InternalResponseC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE, @function
_ZN4node9inspector8protocol16InternalResponseC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE:
.LFB5620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%rdi)
	movl	%esi, 8(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$16, %rdi
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rbx)
	movq	8(%rdx), %rax
	movq	(%rdx), %rsi
	addq	%rsi, %rax
	movq	%rax, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L2374
	movq	$0, (%r12)
	movq	%rax, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2374:
	.cfi_restore_state
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5620:
	.size	_ZN4node9inspector8protocol16InternalResponseC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE, .-_ZN4node9inspector8protocol16InternalResponseC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE
	.globl	_ZN4node9inspector8protocol16InternalResponseC1EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE
	.set	_ZN4node9inspector8protocol16InternalResponseC1EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE,_ZN4node9inspector8protocol16InternalResponseC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE
	.section	.rodata.str1.1
.LC61:
	.string	"%lu"
.LC62:
	.string	"basic_string::append"
.LC63:
	.string	" at position "
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	.type	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc, @function
_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc:
.LFB5860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC61(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$32, %edx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-144(%rbp), %rbx
	subq	$152, %rsp
	movq	8(%rsi), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	%rbx, -160(%rbp)
	testq	%r15, %r15
	je	.L2402
	movq	%r15, %rdi
	leaq	-160(%rbp), %r14
	call	strlen@PLT
	movq	%rax, -168(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L2403
	cmpq	$1, %rax
	jne	.L2381
	movzbl	(%r15), %edx
	movb	%dl, -144(%rbp)
	movq	%rbx, %rdx
.L2382:
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movabsq	$4611686018427387903, %rax
	subq	-152(%rbp), %rax
	cmpq	$12, %rax
	jbe	.L2404
	movl	$13, %edx
	movq	%r14, %rdi
	leaq	.LC63(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-112(%rbp), %r14
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L2405
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L2385:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %r13
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-128(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	cmpq	%r14, %r9
	movq	%rax, %rsi
	cmovne	-112(%rbp), %rsi
	movq	-96(%rbp), %r10
	leaq	(%r8,%rdx), %rcx
	cmpq	%rsi, %rcx
	jbe	.L2387
	cmpq	%r13, %r10
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L2406
.L2387:
	leaq	-128(%rbp), %rdi
	movq	%r10, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L2389:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L2407
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L2391:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r14, %rdi
	je	.L2392
	call	_ZdlPv@PLT
.L2392:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2393
	call	_ZdlPv@PLT
.L2393:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2377
	call	_ZdlPv@PLT
.L2377:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2408
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2381:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L2409
	movq	%rbx, %rdx
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2403:
	movq	%r14, %rdi
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -144(%rbp)
.L2380:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2402:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L2407:
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%r12)
	jmp	.L2391
	.p2align 4,,10
	.p2align 3
.L2405:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2406:
	movq	-184(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2389
.L2404:
	leaq	.LC62(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2408:
	call	__stack_chk_fail@PLT
.L2409:
	movq	%rbx, %rdi
	jmp	.L2380
	.cfi_endproc
.LFE5860:
	.size	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc, .-_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	.section	.rodata.str1.1
.LC64:
	.string	"OK"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"JSON: unprocessed input remains"
	.section	.rodata.str1.1
.LC66:
	.string	"JSON: stack limit exceeded"
.LC67:
	.string	"JSON: no input"
.LC68:
	.string	"JSON: invalid token"
.LC69:
	.string	"JSON: invalid number"
.LC70:
	.string	"JSON: invalid string"
.LC71:
	.string	"JSON: unexpected array end"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"JSON: comma or array end expected"
	.section	.rodata.str1.1
.LC73:
	.string	"JSON: string literal expected"
.LC74:
	.string	"JSON: colon expected"
.LC75:
	.string	"JSON: unexpected map end"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"JSON: comma or map end expected"
	.section	.rodata.str1.1
.LC77:
	.string	"JSON: value expected"
.LC78:
	.string	"CBOR: invalid int32"
.LC79:
	.string	"CBOR: invalid double"
.LC80:
	.string	"CBOR: invalid envelope"
.LC81:
	.string	"CBOR: invalid string8"
.LC82:
	.string	"CBOR: invalid string16"
.LC83:
	.string	"CBOR: invalid binary"
.LC84:
	.string	"CBOR: unsupported value"
.LC85:
	.string	"CBOR: no input"
.LC86:
	.string	"CBOR: invalid start byte"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"CBOR: unexpected eof expected value"
	.section	.rodata.str1.1
.LC88:
	.string	"CBOR: unexpected eof in array"
.LC89:
	.string	"CBOR: unexpected eof in map"
.LC90:
	.string	"CBOR: invalid map key"
.LC91:
	.string	"CBOR: stack limit exceeded"
.LC92:
	.string	"CBOR: trailing junk"
.LC93:
	.string	"CBOR: map start expected"
.LC94:
	.string	"CBOR: map stop expected"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"CBOR: envelope size limit exceeded"
	.section	.rodata.str1.1
.LC96:
	.string	"INVALID ERROR CODE"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11Ev
	.type	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11Ev, @function
_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11Ev:
.LFB5859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$31, (%rsi)
	ja	.L2411
	movl	(%rsi), %eax
	leaq	.L2413(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2413:
	.long	.L2444-.L2413
	.long	.L2443-.L2413
	.long	.L2442-.L2413
	.long	.L2441-.L2413
	.long	.L2440-.L2413
	.long	.L2439-.L2413
	.long	.L2438-.L2413
	.long	.L2437-.L2413
	.long	.L2436-.L2413
	.long	.L2435-.L2413
	.long	.L2434-.L2413
	.long	.L2433-.L2413
	.long	.L2432-.L2413
	.long	.L2431-.L2413
	.long	.L2430-.L2413
	.long	.L2429-.L2413
	.long	.L2428-.L2413
	.long	.L2427-.L2413
	.long	.L2426-.L2413
	.long	.L2425-.L2413
	.long	.L2424-.L2413
	.long	.L2423-.L2413
	.long	.L2422-.L2413
	.long	.L2421-.L2413
	.long	.L2420-.L2413
	.long	.L2419-.L2413
	.long	.L2418-.L2413
	.long	.L2417-.L2413
	.long	.L2416-.L2413
	.long	.L2415-.L2413
	.long	.L2414-.L2413
	.long	.L2412-.L2413
	.text
	.p2align 4,,10
	.p2align 3
.L2414:
	leaq	.LC94(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	.p2align 4,,10
	.p2align 3
.L2410:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2448
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2415:
	.cfi_restore_state
	leaq	.LC93(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2430:
	leaq	.LC78(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2431:
	leaq	.LC77(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2432:
	leaq	.LC76(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2433:
	leaq	.LC75(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2434:
	leaq	.LC74(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2435:
	leaq	.LC73(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2436:
	leaq	.LC72(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2437:
	leaq	.LC71(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2438:
	leaq	.LC70(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2439:
	leaq	.LC69(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2440:
	leaq	.LC68(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2441:
	leaq	.LC67(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2442:
	leaq	.LC66(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2443:
	leaq	.LC65(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2444:
	leaq	.LC64(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2412:
	leaq	.LC95(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2422:
	leaq	.LC86(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2423:
	leaq	.LC85(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2424:
	leaq	.LC84(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2425:
	leaq	.LC83(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2426:
	leaq	.LC82(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2427:
	leaq	.LC81(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2428:
	leaq	.LC80(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2429:
	leaq	.LC79(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2418:
	leaq	.LC90(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2419:
	leaq	.LC89(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2420:
	leaq	.LC88(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2421:
	leaq	.LC87(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2416:
	leaq	.LC92(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2417:
	leaq	.LC91(%rip), %rdx
	call	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11EPKc
	jmp	.L2410
.L2411:
	leaq	.LC96(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2410
.L2448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5859:
	.size	_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11Ev, .-_ZNK4node9inspector8protocol6Status13ToASCIIStringB5cxx11Ev
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor9internals14ReadTokenStartENS1_4spanIhEEPNS2_9MajorTypeEPm
	.type	_ZN4node9inspector8protocol4cbor9internals14ReadTokenStartENS1_4spanIhEEPNS2_9MajorTypeEPm, @function
_ZN4node9inspector8protocol4cbor9internals14ReadTokenStartENS1_4spanIhEEPNS2_9MajorTypeEPm:
.LFB5864:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2454
	movzbl	(%rdi), %eax
	movl	%eax, %r8d
	andl	$31, %eax
	shrb	$5, %r8b
	movzbl	%r8b, %r8d
	movl	%r8d, (%rdx)
	cmpb	$23, %al
	jbe	.L2457
	cmpb	$24, %al
	je	.L2458
	cmpb	$25, %al
	je	.L2459
	cmpb	$26, %al
	je	.L2460
	cmpq	$8, %rsi
	jbe	.L2454
	cmpb	$27, %al
	jne	.L2454
	movzbl	7(%rdi), %edx
	movzbl	8(%rdi), %eax
	salq	$8, %rdx
	orq	%rax, %rdx
	movzbl	6(%rdi), %eax
	salq	$16, %rax
	orq	%rax, %rdx
	movzbl	5(%rdi), %eax
	salq	$24, %rax
	orq	%rdx, %rax
	movzbl	4(%rdi), %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	movzbl	3(%rdi), %edx
	salq	$40, %rdx
	orq	%rax, %rdx
	movzbl	2(%rdi), %eax
	salq	$48, %rax
	orq	%rax, %rdx
	movzbl	1(%rdi), %eax
	salq	$56, %rax
	orq	%rdx, %rax
	movq	%rax, (%rcx)
	movl	$9, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2458:
	cmpq	$1, %rsi
	jbe	.L2454
	movzbl	1(%rdi), %eax
	movq	%rax, (%rcx)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2457:
	movzbl	%al, %eax
	movq	%rax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2460:
	cmpq	$4, %rsi
	jbe	.L2454
	movzbl	3(%rdi), %edx
	movzbl	4(%rdi), %eax
	sall	$8, %edx
	orl	%eax, %edx
	movzbl	2(%rdi), %eax
	sall	$16, %eax
	orl	%eax, %edx
	movzbl	1(%rdi), %eax
	sall	$24, %eax
	orl	%edx, %eax
	movq	%rax, (%rcx)
	movl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2459:
	cmpq	$2, %rsi
	jbe	.L2454
	movzbl	1(%rdi), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	2(%rdi), %eax
	orl	%edx, %eax
	movzwl	%ax, %eax
	movq	%rax, (%rcx)
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2454:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE5864:
	.size	_ZN4node9inspector8protocol4cbor9internals14ReadTokenStartENS1_4spanIhEEPNS2_9MajorTypeEPm, .-_ZN4node9inspector8protocol4cbor9internals14ReadTokenStartENS1_4spanIhEEPNS2_9MajorTypeEPm
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor22InitialByteForEnvelopeEv
	.type	_ZN4node9inspector8protocol4cbor22InitialByteForEnvelopeEv, @function
_ZN4node9inspector8protocol4cbor22InitialByteForEnvelopeEv:
.LFB5868:
	.cfi_startproc
	endbr64
	movl	$-40, %eax
	ret
	.cfi_endproc
.LFE5868:
	.size	_ZN4node9inspector8protocol4cbor22InitialByteForEnvelopeEv, .-_ZN4node9inspector8protocol4cbor22InitialByteForEnvelopeEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor35InitialByteFor32BitLengthByteStringEv
	.type	_ZN4node9inspector8protocol4cbor35InitialByteFor32BitLengthByteStringEv, @function
_ZN4node9inspector8protocol4cbor35InitialByteFor32BitLengthByteStringEv:
.LFB5869:
	.cfi_startproc
	endbr64
	movl	$90, %eax
	ret
	.cfi_endproc
.LFE5869:
	.size	_ZN4node9inspector8protocol4cbor35InitialByteFor32BitLengthByteStringEv, .-_ZN4node9inspector8protocol4cbor35InitialByteFor32BitLengthByteStringEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13IsCBORMessageENS1_4spanIhEE
	.type	_ZN4node9inspector8protocol4cbor13IsCBORMessageENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4cbor13IsCBORMessageENS1_4spanIhEE:
.LFB5870:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$5, %rsi
	jbe	.L2463
	cmpb	$-40, (%rdi)
	je	.L2467
.L2463:
	ret
	.p2align 4,,10
	.p2align 3
.L2467:
	cmpb	$90, 1(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE5870:
	.size	_ZN4node9inspector8protocol4cbor13IsCBORMessageENS1_4spanIhEE, .-_ZN4node9inspector8protocol4cbor13IsCBORMessageENS1_4spanIhEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor10EncodeTrueEv
	.type	_ZN4node9inspector8protocol4cbor10EncodeTrueEv, @function
_ZN4node9inspector8protocol4cbor10EncodeTrueEv:
.LFB5871:
	.cfi_startproc
	endbr64
	movl	$-11, %eax
	ret
	.cfi_endproc
.LFE5871:
	.size	_ZN4node9inspector8protocol4cbor10EncodeTrueEv, .-_ZN4node9inspector8protocol4cbor10EncodeTrueEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor11EncodeFalseEv
	.type	_ZN4node9inspector8protocol4cbor11EncodeFalseEv, @function
_ZN4node9inspector8protocol4cbor11EncodeFalseEv:
.LFB5872:
	.cfi_startproc
	endbr64
	movl	$-12, %eax
	ret
	.cfi_endproc
.LFE5872:
	.size	_ZN4node9inspector8protocol4cbor11EncodeFalseEv, .-_ZN4node9inspector8protocol4cbor11EncodeFalseEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor10EncodeNullEv
	.type	_ZN4node9inspector8protocol4cbor10EncodeNullEv, @function
_ZN4node9inspector8protocol4cbor10EncodeNullEv:
.LFB5873:
	.cfi_startproc
	endbr64
	movl	$-10, %eax
	ret
	.cfi_endproc
.LFE5873:
	.size	_ZN4node9inspector8protocol4cbor10EncodeNullEv, .-_ZN4node9inspector8protocol4cbor10EncodeNullEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor32EncodeIndefiniteLengthArrayStartEv
	.type	_ZN4node9inspector8protocol4cbor32EncodeIndefiniteLengthArrayStartEv, @function
_ZN4node9inspector8protocol4cbor32EncodeIndefiniteLengthArrayStartEv:
.LFB5874:
	.cfi_startproc
	endbr64
	movl	$-97, %eax
	ret
	.cfi_endproc
.LFE5874:
	.size	_ZN4node9inspector8protocol4cbor32EncodeIndefiniteLengthArrayStartEv, .-_ZN4node9inspector8protocol4cbor32EncodeIndefiniteLengthArrayStartEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor30EncodeIndefiniteLengthMapStartEv
	.type	_ZN4node9inspector8protocol4cbor30EncodeIndefiniteLengthMapStartEv, @function
_ZN4node9inspector8protocol4cbor30EncodeIndefiniteLengthMapStartEv:
.LFB5875:
	.cfi_startproc
	endbr64
	movl	$-65, %eax
	ret
	.cfi_endproc
.LFE5875:
	.size	_ZN4node9inspector8protocol4cbor30EncodeIndefiniteLengthMapStartEv, .-_ZN4node9inspector8protocol4cbor30EncodeIndefiniteLengthMapStartEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor10EncodeStopEv
	.type	_ZN4node9inspector8protocol4cbor10EncodeStopEv, @function
_ZN4node9inspector8protocol4cbor10EncodeStopEv:
.LFB5876:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE5876:
	.size	_ZN4node9inspector8protocol4cbor10EncodeStopEv, .-_ZN4node9inspector8protocol4cbor10EncodeStopEv
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%xmm0, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r14
	movq	(%rdi), %rax
	leaq	1(%r14), %r15
	cmpq	%r13, %rax
	je	.L2493
	movq	16(%rdi), %rdx
.L2475:
	cmpq	%rdx, %r15
	ja	.L2503
.L2476:
	movb	$-5, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$56, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2494
	movq	16(%rbx), %rdx
.L2477:
	cmpq	%rdx, %r15
	ja	.L2504
.L2478:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$48, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2495
	movq	16(%rbx), %rdx
.L2479:
	cmpq	%rdx, %r15
	ja	.L2505
.L2480:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$40, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2496
	movq	16(%rbx), %rdx
.L2481:
	cmpq	%rdx, %r15
	ja	.L2506
.L2482:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$32, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2497
	movq	16(%rbx), %rdx
.L2483:
	cmpq	%rdx, %r15
	ja	.L2507
.L2484:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$24, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2498
	movq	16(%rbx), %rdx
.L2485:
	cmpq	%rdx, %r15
	ja	.L2508
.L2486:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$16, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2499
	movq	16(%rbx), %rdx
.L2487:
	cmpq	%rdx, %r15
	ja	.L2509
.L2488:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r12, %r9
	movq	%r15, 8(%rbx)
	shrq	$8, %r9
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2500
	movq	16(%rbx), %rdx
.L2489:
	cmpq	%rdx, %r15
	ja	.L2510
.L2490:
	movb	%r9b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r13
	je	.L2501
	movq	16(%rbx), %rdx
.L2491:
	cmpq	%rdx, %r15
	ja	.L2511
.L2492:
	movb	%r12b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%r15, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2503:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2511:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2510:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2509:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2508:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2507:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2506:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2505:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2504:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L2494:
	movl	$15, %edx
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2499:
	movl	$15, %edx
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L2500:
	movl	$15, %edx
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L2501:
	movl	$15, %edx
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2493:
	movl	$15, %edx
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2495:
	movl	$15, %edx
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2496:
	movl	$15, %edx
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L2497:
	movl	$15, %edx
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2498:
	movl	$15, %edx
	jmp	.L2485
	.cfi_endproc
.LFE5897:
	.size	_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"void node::inspector::protocol::cbor::EncodeStartTmpl(C*, size_t*) [with C = std::__cxx11::basic_string<char>; size_t = long unsigned int]"
	.section	.rodata.str1.1
.LC98:
	.string	"*byte_size_pos == 0"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, (%rdi)
	jne	.L2521
	movq	8(%rsi), %r13
	movq	(%rsi), %rax
	leaq	16(%rsi), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r12
	leaq	1(%r13), %r14
	cmpq	%r15, %rax
	je	.L2518
	movq	16(%rsi), %rdx
.L2514:
	cmpq	%rdx, %r14
	ja	.L2522
.L2515:
	movb	$-40, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r14
	cmpq	%r15, %rax
	je	.L2519
	movq	16(%r12), %rdx
.L2516:
	cmpq	%rdx, %r14
	ja	.L2523
.L2517:
	movb	$90, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %rsi
	movq	%rsi, (%rbx)
	addq	$8, %rsp
	addq	$4, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	.p2align 4,,10
	.p2align 3
.L2522:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2523:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2519:
	movl	$15, %edx
	jmp	.L2516
	.p2align 4,,10
	.p2align 3
.L2518:
	movl	$15, %edx
	jmp	.L2514
.L2521:
	leaq	.LC97(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC98(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5900:
	.size	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"bool node::inspector::protocol::cbor::EncodeStopTmpl(C*, size_t*) [with C = std::vector<unsigned char>; size_t = long unsigned int]"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE:
.LFB5902:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2531
	movq	(%rsi), %rcx
	movq	8(%rsi), %rax
	movl	$4294967295, %r9d
	xorl	%r8d, %r8d
	subq	%rcx, %rax
	subq	$4, %rax
	subq	%rdx, %rax
	cmpq	%r9, %rax
	ja	.L2524
	leaq	1(%rdx), %r8
	movq	%r8, (%rdi)
	movq	%rax, %r8
	shrq	$24, %r8
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	(%rsi), %rcx
	leaq	1(%rdx), %r8
	movq	%r8, (%rdi)
	movq	%rax, %r8
	shrq	$16, %r8
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	(%rsi), %rcx
	leaq	1(%rdx), %r8
	movq	%r8, (%rdi)
	movq	%rax, %r8
	shrq	$8, %r8
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movl	$1, %r8d
	movq	(%rsi), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rdi)
	movb	%al, (%rcx,%rdx)
.L2524:
	movl	%r8d, %eax
	ret
.L2531:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC99(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5902:
	.size	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5903:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2539
	movq	8(%rsi), %rax
	movl	$4294967295, %ecx
	xorl	%r8d, %r8d
	subq	$4, %rax
	subq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L2532
	leaq	1(%rdx), %rcx
	movq	%rax, %r8
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	shrq	$24, %r8
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	%rax, %r8
	shrq	$16, %r8
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movq	%rax, %r8
	shrq	$8, %r8
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%r8b, (%rcx,%rdx)
	movq	(%rdi), %rdx
	movl	$1, %r8d
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rcx
	movb	%al, (%rcx,%rdx)
.L2532:
	movl	%r8d, %eax
	ret
.L2539:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC43(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5903:
	.size	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor15EnvelopeEncoder10EncodeStopEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS1_6StatusE
	.type	_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS1_6StatusE, @function
_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS1_6StatusE:
.LFB5919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rcx
	movl	$0, (%rbx)
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	$-1, 8(%rbx)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5919:
	.size	_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS1_6StatusE, .-_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPSt6vectorIhSaIhEEPNS1_6StatusE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE
	.type	_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE, @function
_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE:
.LFB5920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rcx
	movl	$0, (%rbx)
	movq	%rcx, (%rax)
	movq	%r13, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	$-1, 8(%rbx)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5920:
	.size	_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE, .-_ZN4node9inspector8protocol4cbor14NewCBOREncoderEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizerD2Ev
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizerD2Ev, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizerD2Ev:
.LFB5928:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5928:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizerD2Ev, .-_ZN4node9inspector8protocol4cbor13CBORTokenizerD2Ev
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizerD1Ev
	.set	_ZN4node9inspector8protocol4cbor13CBORTokenizerD1Ev,_ZN4node9inspector8protocol4cbor13CBORTokenizerD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8TokenTagEv
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8TokenTagEv, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer8TokenTagEv:
.LFB5930:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE5930:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8TokenTagEv, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer8TokenTagEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer6StatusEv
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer6StatusEv, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer6StatusEv:
.LFB5933:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE5933:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer6StatusEv, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer6StatusEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev:
.LFB5934:
	.cfi_startproc
	endbr64
	cmpl	$4, 16(%rdi)
	jne	.L2553
	movq	56(%rdi), %rdx
	movl	%edx, %eax
	movl	48(%rdi), %edx
	testl	%edx, %edx
	jne	.L2554
	ret
	.p2align 4,,10
	.p2align 3
.L2554:
	notl	%eax
	ret
.L2553:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev.part.0
	.cfi_endproc
.LFE5934:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer8GetInt32Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv:
.LFB5935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$5, 16(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L2559
	movq	32(%rdi), %rax
	movq	(%rdi), %rdx
	movq	%rax, %rcx
	notq	%rcx
	addq	8(%rdi), %rcx
	cmpq	$7, %rcx
	jbe	.L2560
	movzbl	7(%rdx,%rax), %ecx
	movzbl	8(%rdx,%rax), %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	salq	$8, %rcx
	orq	%rcx, %rsi
	movzbl	6(%rdx,%rax), %ecx
	salq	$16, %rcx
	orq	%rsi, %rcx
	movzbl	5(%rdx,%rax), %esi
	salq	$24, %rsi
	orq	%rsi, %rcx
	movzbl	4(%rdx,%rax), %esi
	salq	$32, %rsi
	orq	%rcx, %rsi
	movzbl	3(%rdx,%rax), %ecx
	salq	$40, %rcx
	orq	%rcx, %rsi
	movzbl	2(%rdx,%rax), %ecx
	movzbl	1(%rdx,%rax), %eax
	salq	$48, %rcx
	salq	$56, %rax
	orq	%rsi, %rcx
	orq	%rcx, %rax
	movq	%rax, %xmm0
	ret
.L2559:
	.cfi_restore_state
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv.part.0
.L2560:
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS1_4spanIhEE.part.0
	.cfi_endproc
.LFE5935:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev:
.LFB5936:
	.cfi_startproc
	endbr64
	cmpl	$6, 16(%rdi)
	jne	.L2566
	movq	56(%rdi), %rdx
	movq	40(%rdi), %rax
	addq	32(%rdi), %rax
	subq	%rdx, %rax
	addq	(%rdi), %rax
	ret
.L2566:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev.part.0
	.cfi_endproc
.LFE5936:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer10GetString8Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv:
.LFB5937:
	.cfi_startproc
	endbr64
	cmpl	$7, 16(%rdi)
	jne	.L2572
	movq	56(%rdi), %rdx
	movq	40(%rdi), %rax
	addq	32(%rdi), %rax
	subq	%rdx, %rax
	addq	(%rdi), %rax
	ret
.L2572:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv.part.0
	.cfi_endproc
.LFE5937:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv:
.LFB5938:
	.cfi_startproc
	endbr64
	cmpl	$8, 16(%rdi)
	jne	.L2578
	movq	56(%rdi), %rdx
	movq	40(%rdi), %rax
	addq	32(%rdi), %rax
	subq	%rdx, %rax
	addq	(%rdi), %rax
	ret
.L2578:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv.part.0
	.cfi_endproc
.LFE5938:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"node::inspector::protocol::span<unsigned char> node::inspector::protocol::cbor::CBORTokenizer::GetEnvelopeContents() const"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol4cbor13CBORTokenizer19GetEnvelopeContentsEv
	.type	_ZNK4node9inspector8protocol4cbor13CBORTokenizer19GetEnvelopeContentsEv, @function
_ZNK4node9inspector8protocol4cbor13CBORTokenizer19GetEnvelopeContentsEv:
.LFB5939:
	.cfi_startproc
	endbr64
	cmpl	$12, 16(%rdi)
	jne	.L2584
	movq	32(%rdi), %rax
	movq	(%rdi), %rcx
	movq	56(%rdi), %rdx
	leaq	6(%rcx,%rax), %rax
	ret
.L2584:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC100(%rip), %rcx
	movl	$2434, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5939:
	.size	_ZNK4node9inspector8protocol4cbor13CBORTokenizer19GetEnvelopeContentsEv, .-_ZNK4node9inspector8protocol4cbor13CBORTokenizer19GetEnvelopeContentsEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb:
.LFB5940:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movq	%rdi, %r9
	testb	%sil, %sil
	je	.L2586
	leaq	6(%rdx), %rax
	movq	%rax, 32(%rdi)
.L2587:
	movq	8(%r9), %rsi
	movl	$0, 24(%r9)
	cmpq	%rax, %rsi
	jbe	.L2627
	movq	(%r9), %r11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	subq	%rax, %r10
	leaq	(%r11,%rax), %rdi
	movzbl	(%rdi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpb	$-43, %dl
	jbe	.L2628
	leal	42(%rdx), %ecx
	cmpb	$-42, %dl
	jb	.L2594
	leaq	.L2596(%rip), %r8
	movzbl	%cl, %ecx
	movslq	(%r8,%rcx,4), %rdx
	addq	%r8, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L2596:
	.long	.L2602-.L2596
	.long	.L2594-.L2596
	.long	.L2601-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2600-.L2596
	.long	.L2599-.L2596
	.long	.L2598-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2597-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2594-.L2596
	.long	.L2595-.L2596
	.text
	.p2align 4,,10
	.p2align 3
.L2628:
	cmpb	$-97, %dl
	je	.L2592
	cmpb	$-65, %dl
	jne	.L2594
	movl	$9, 16(%r9)
	movq	$1, 40(%r9)
.L2585:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2586:
	.cfi_restore 6
	xorl	%eax, %eax
	cmpq	$-1, %rdx
	je	.L2588
	movq	40(%rdi), %rax
	addq	%rdx, %rax
.L2588:
	movq	%rax, 32(%r9)
	jmp	.L2587
	.p2align 4,,10
	.p2align 3
.L2627:
	movl	$13, 16(%r9)
	ret
.L2595:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$11, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
.L2594:
	.cfi_restore_state
	leaq	56(%r9), %rcx
	leaq	48(%r9), %rdx
	movq	%r10, %rsi
	call	_ZN4node9inspector8protocol4cbor9internals14ReadTokenStartENS1_4spanIhEEPNS2_9MajorTypeEPm
	cmpl	$7, 48(%r9)
	ja	.L2585
	movl	48(%r9), %edx
	leaq	.L2610(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L2610:
	.long	.L2614-.L2610
	.long	.L2613-.L2610
	.long	.L2612-.L2610
	.long	.L2611-.L2610
	.long	.L2609-.L2610
	.long	.L2609-.L2610
	.long	.L2609-.L2610
	.long	.L2609-.L2610
	.text
.L2602:
	addq	$1, %rax
	leaq	56(%r9), %rcx
	leaq	48(%r9), %rdx
	subq	%rax, %rsi
	leaq	(%r11,%rax), %rdi
	call	_ZN4node9inspector8protocol4cbor9internals14ReadTokenStartENS1_4spanIhEEPNS2_9MajorTypeEPm
	testb	%al, %al
	js	.L2603
	cmpl	$2, 48(%r9)
	jne	.L2603
	movabsq	$4611686018427387903, %rcx
	movq	56(%r9), %rdx
	cmpq	%rcx, %rdx
	ja	.L2603
	movsbq	%al, %rax
	leaq	1(%rdx,%rax), %rax
	cmpq	%rax, %r10
	jb	.L2603
	movl	$8, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L2601:
	.cfi_restore_state
	cmpq	$5, %r10
	jbe	.L2607
	cmpb	$90, 1(%r11,%rax)
	jne	.L2607
	leaq	2(%rax), %rdx
	subq	%rdx, %rsi
	cmpq	$3, %rsi
	jbe	.L2629
	movzbl	4(%r11,%rax), %edx
	movzbl	5(%r11,%rax), %ecx
	sall	$8, %edx
	orl	%edx, %ecx
	movzbl	3(%r11,%rax), %edx
	movzbl	2(%r11,%rax), %eax
	sall	$16, %edx
	sall	$24, %eax
	orl	%ecx, %edx
	orl	%edx, %eax
	movq	%rax, 56(%r9)
	addq	$6, %rax
	cmpq	%rax, %r10
	jb	.L2607
	movl	$12, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L2600:
	.cfi_restore_state
	movl	$2, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
.L2598:
	.cfi_restore_state
	movl	$3, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
.L2597:
	.cfi_restore_state
	cmpq	$8, %r10
	jbe	.L2630
	movl	$5, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$9, 40(%r9)
	ret
.L2599:
	.cfi_restore_state
	movl	$1, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L2592:
	.cfi_restore_state
	movl	$10, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 40(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L2607:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$16, 24(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L2603:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$19, 24(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L2609:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$20, 24(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L2630:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$15, 24(%r9)
	ret
.L2611:
	.cfi_restore_state
	testb	%al, %al
	js	.L2617
	movabsq	$4611686018427387903, %rcx
	movq	56(%r9), %rdx
	cmpq	%rcx, %rdx
	ja	.L2617
	movsbq	%al, %rax
	addq	%rdx, %rax
	cmpq	%rax, %r10
	jb	.L2617
	movl	$6, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L2612:
	.cfi_restore_state
	testb	%al, %al
	js	.L2619
	movabsq	$4611686018427387903, %rcx
	movq	56(%r9), %rdx
	cmpq	%rcx, %rdx
	ja	.L2619
	testb	$1, %dl
	jne	.L2619
	movsbq	%al, %rax
	addq	%rdx, %rax
	cmpq	%rax, %r10
	jb	.L2619
	movl	$7, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L2613:
	.cfi_restore_state
	cmpq	$2147483647, 56(%r9)
	jg	.L2615
	testb	%al, %al
	js	.L2615
.L2626:
	movsbq	%al, %rax
	movl	$4, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, 40(%r9)
	ret
.L2614:
	.cfi_restore_state
	testb	%al, %al
	js	.L2615
	cmpq	$2147483647, 56(%r9)
	jbe	.L2626
.L2615:
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$14, 24(%r9)
	ret
.L2619:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$18, 24(%r9)
	ret
.L2617:
	.cfi_restore_state
	movl	$0, 16(%r9)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$17, 24(%r9)
	ret
.L2629:
	.cfi_restore_state
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstIjEET_NS1_4spanIhEE.part.0
	.cfi_endproc
.LFE5940:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb, .-_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizerC2ENS1_4spanIhEE
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizerC2ENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizerC2ENS1_4spanIhEE:
.LFB5925:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	xorl	%esi, %esi
	movq	%rdx, 8(%rdi)
	movl	$0, 24(%rdi)
	movq	$-1, 32(%rdi)
	jmp	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	.cfi_endproc
.LFE5925:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizerC2ENS1_4spanIhEE, .-_ZN4node9inspector8protocol4cbor13CBORTokenizerC2ENS1_4spanIhEE
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizerC1ENS1_4spanIhEE
	.set	_ZN4node9inspector8protocol4cbor13CBORTokenizerC1ENS1_4spanIhEE,_ZN4node9inspector8protocol4cbor13CBORTokenizerC2ENS1_4spanIhEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizer4NextEv
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizer4NextEv, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizer4NextEv:
.LFB5931:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L2632
	cmpl	$13, %eax
	je	.L2632
	xorl	%esi, %esi
	jmp	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	.p2align 4,,10
	.p2align 3
.L2632:
	ret
	.cfi_endproc
.LFE5931:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizer4NextEv, .-_ZN4node9inspector8protocol4cbor13CBORTokenizer4NextEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv:
.LFB5932:
	.cfi_startproc
	endbr64
	cmpl	$12, 16(%rdi)
	jne	.L2639
	movl	$1, %esi
	jmp	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
.L2639:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv.part.0
	.cfi_endproc
.LFE5932:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv, .-_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetTokenENS2_12CBORTokenTagEm
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetTokenENS2_12CBORTokenTagEm, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetTokenENS2_12CBORTokenTagEm:
.LFB5941:
	.cfi_startproc
	endbr64
	movl	%esi, 16(%rdi)
	movq	%rdx, 40(%rdi)
	ret
	.cfi_endproc
.LFE5941:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetTokenENS2_12CBORTokenTagEm, .-_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetTokenENS2_12CBORTokenTagEm
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetErrorENS1_5ErrorE
	.type	_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetErrorENS1_5ErrorE, @function
_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetErrorENS1_5ErrorE:
.LFB5942:
	.cfi_startproc
	endbr64
	movl	$0, 16(%rdi)
	movl	%esi, 24(%rdi)
	ret
	.cfi_endproc
.LFE5942:
	.size	_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetErrorENS1_5ErrorE, .-_ZN4node9inspector8protocol4cbor13CBORTokenizer8SetErrorENS1_5ErrorE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPSt6vectorIhSaIhEEPNS1_6StatusE
	.type	_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPSt6vectorIhSaIhEEPNS1_6StatusE, @function
_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPSt6vectorIhSaIhEEPNS1_6StatusE:
.LFB5988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$112, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	leaq	32(%rbx), %rdi
	movq	%rax, (%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	call	_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0
	movq	24(%rbx), %rax
	movq	96(%rbx), %rcx
	movl	$0, (%rax)
	leaq	-8(%rcx), %rdx
	movq	$-1, 8(%rax)
	movq	80(%rbx), %rax
	cmpq	%rdx, %rax
	je	.L2643
	movq	$0, (%rax)
	addq	$8, %rax
	movq	%rax, 80(%rbx)
.L2644:
	movq	%rbx, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2643:
	.cfi_restore_state
	movq	104(%rbx), %r14
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %r8
	subq	88(%rbx), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%r8, %rax
	je	.L2654
	movq	32(%rbx), %rdi
	movq	40(%rbx), %rdx
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L2655
.L2646:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	80(%rbx), %rax
	movq	$0, (%rax)
	movq	104(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L2644
	.p2align 4,,10
	.p2align 3
.L2655:
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L2656
	testq	%rdx, %rdx
	je	.L2652
	leaq	2(%rdx,%rdx), %r14
	cmpq	%r8, %r14
	ja	.L2657
.L2650:
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	104(%rbx), %rax
	movq	72(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2651
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L2651:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 40(%rbx)
	movq	%rax, 32(%rbx)
.L2649:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 72(%rbx)
	movq	%r14, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	(%r14), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L2656:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L2648
	cmpq	%r14, %rsi
	je	.L2649
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2649
	.p2align 4,,10
	.p2align 3
.L2648:
	cmpq	%r14, %rsi
	je	.L2649
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2649
.L2652:
	movl	$3, %r14d
	jmp	.L2650
.L2654:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2657:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5988:
	.size	_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPSt6vectorIhSaIhEEPNS1_6StatusE, .-_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPSt6vectorIhSaIhEEPNS1_6StatusE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE
	.type	_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE, @function
_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE:
.LFB5989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$112, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	leaq	32(%rbx), %rdi
	movq	%rax, (%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 96(%rbx)
	call	_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0
	movq	24(%rbx), %rax
	movq	96(%rbx), %rcx
	movl	$0, (%rax)
	leaq	-8(%rcx), %rdx
	movq	$-1, 8(%rax)
	movq	80(%rbx), %rax
	cmpq	%rdx, %rax
	je	.L2659
	movq	$0, (%rax)
	addq	$8, %rax
	movq	%rax, 80(%rbx)
.L2660:
	movq	%rbx, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2659:
	.cfi_restore_state
	movq	104(%rbx), %r14
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %r8
	subq	88(%rbx), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%r8, %rax
	je	.L2670
	movq	32(%rbx), %rdi
	movq	40(%rbx), %rdx
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L2671
.L2662:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	80(%rbx), %rax
	movq	$0, (%rax)
	movq	104(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L2660
	.p2align 4,,10
	.p2align 3
.L2671:
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L2672
	testq	%rdx, %rdx
	je	.L2668
	leaq	2(%rdx,%rdx), %r14
	cmpq	%r8, %r14
	ja	.L2673
.L2666:
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	104(%rbx), %rax
	movq	72(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2667
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L2667:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 40(%rbx)
	movq	%rax, 32(%rbx)
.L2665:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 72(%rbx)
	movq	%r14, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	(%r14), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2672:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L2664
	cmpq	%r14, %rsi
	je	.L2665
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2665
	.p2align 4,,10
	.p2align 3
.L2664:
	cmpq	%r14, %rsi
	je	.L2665
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2665
.L2668:
	movl	$3, %r14d
	jmp	.L2666
.L2670:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2673:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5989:
	.size	_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE, .-_ZN4node9inspector8protocol4json14NewJSONEncoderEPKNS2_8PlatformEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_6StatusE
	.section	.text._ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	.type	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_, @function
_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_:
.LFB7010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$5, %edi
	andl	$8160, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpq	$23, %rsi
	jbe	.L2735
	cmpq	$255, %rsi
	jbe	.L2736
	leaq	16(%rdx), %r14
	cmpq	$65535, %rsi
	jbe	.L2737
	movl	$4294967295, %eax
	cmpq	%rax, %rsi
	jbe	.L2738
	movq	8(%rdx), %r15
	movq	(%rdx), %rax
	movl	%edi, %r13d
	orl	$27, %r13d
	leaq	1(%r15), %r9
	cmpq	%r14, %rax
	je	.L2717
	movq	16(%rdx), %rdx
.L2693:
	cmpq	%rdx, %r9
	ja	.L2739
.L2694:
	movb	%r13b, (%rax,%r15)
	movq	(%r12), %rax
	movq	%r9, 8(%r12)
	movq	%rbx, %r9
	movb	$0, 1(%rax,%r15)
	movq	8(%r12), %r13
	shrq	$56, %r9
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2718
	movq	16(%r12), %rdx
.L2695:
	cmpq	%rdx, %r15
	ja	.L2740
.L2696:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movq	%rbx, %r9
	movq	%r15, 8(%r12)
	shrq	$48, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2719
	movq	16(%r12), %rdx
.L2697:
	cmpq	%rdx, %r15
	ja	.L2741
.L2698:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movq	%rbx, %r9
	movq	%r15, 8(%r12)
	shrq	$40, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2720
	movq	16(%r12), %rdx
.L2699:
	cmpq	%rdx, %r15
	ja	.L2742
.L2700:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movq	%rbx, %r9
	movq	%r15, 8(%r12)
	shrq	$32, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2721
	movq	16(%r12), %rdx
.L2701:
	cmpq	%rdx, %r15
	ja	.L2743
.L2702:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movq	%rbx, %r9
	movq	%r15, 8(%r12)
	shrq	$24, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2722
	movq	16(%r12), %rdx
.L2703:
	cmpq	%rdx, %r15
	ja	.L2744
.L2704:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movq	%rbx, %r9
	movq	%r15, 8(%r12)
	shrq	$16, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2723
	movq	16(%r12), %rdx
.L2705:
	cmpq	%rdx, %r15
	ja	.L2745
.L2706:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movq	%rbx, %r9
	movq	%r15, 8(%r12)
	shrq	$8, %r9
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2724
	movq	16(%r12), %rdx
.L2707:
	cmpq	%rdx, %r15
	ja	.L2746
.L2708:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L2725
	movq	16(%r12), %rdx
.L2709:
	cmpq	%rdx, %r15
	ja	.L2747
.L2710:
	movb	%bl, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2747:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L2710
	.p2align 4,,10
	.p2align 3
.L2735:
	orl	%esi, %edi
	movsbl	%dil, %esi
.L2727:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	.p2align 4,,10
	.p2align 3
.L2738:
	.cfi_restore_state
	orl	$26, %edi
	movsbl	%dil, %esi
	movq	%rdx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movq	8(%r12), %r13
	movq	(%r12), %rax
	movl	%ebx, %r9d
	shrl	$24, %r9d
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L2713
	movq	16(%r12), %rdx
.L2684:
	cmpq	%r15, %rdx
	jb	.L2748
.L2685:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movl	%ebx, %r9d
	movq	%r15, 8(%r12)
	shrl	$16, %r9d
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2714
	movq	16(%r12), %rdx
.L2686:
	cmpq	%rdx, %r15
	ja	.L2749
.L2687:
	movb	%r9b, (%rax,%r13)
	movq	(%r12), %rax
	movl	%ebx, %r9d
	movq	%r15, 8(%r12)
	shrl	$8, %r9d
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%rax, %r14
	je	.L2715
	movq	16(%r12), %rdx
.L2688:
	cmpq	%rdx, %r15
	jbe	.L2708
.L2734:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movl	-56(%rbp), %r9d
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2736:
	orl	$24, %edi
	movsbl	%dil, %esi
	movq	%rdx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movsbl	%bl, %esi
	jmp	.L2727
	.p2align 4,,10
	.p2align 3
.L2737:
	orl	$25, %edi
	movsbl	%dil, %esi
	movq	%rdx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movq	8(%r12), %r13
	movq	(%r12), %rax
	movl	%ebx, %r9d
	sarl	$8, %r9d
	leaq	1(%r13), %r15
	cmpq	%r14, %rax
	je	.L2711
	movq	16(%r12), %rdx
.L2678:
	cmpq	%r15, %rdx
	jnb	.L2708
	jmp	.L2734
	.p2align 4,,10
	.p2align 3
.L2725:
	movl	$15, %edx
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2746:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2745:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2744:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2743:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2702
	.p2align 4,,10
	.p2align 3
.L2742:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L2741:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2740:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2739:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2720:
	movl	$15, %edx
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2719:
	movl	$15, %edx
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2718:
	movl	$15, %edx
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2717:
	movl	$15, %edx
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2724:
	movl	$15, %edx
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2723:
	movl	$15, %edx
	jmp	.L2705
	.p2align 4,,10
	.p2align 3
.L2722:
	movl	$15, %edx
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2721:
	movl	$15, %edx
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2749:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movl	-56(%rbp), %r9d
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2748:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%r9d, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movl	-56(%rbp), %r9d
	jmp	.L2685
	.p2align 4,,10
	.p2align 3
.L2713:
	movl	$15, %edx
	jmp	.L2684
	.p2align 4,,10
	.p2align 3
.L2715:
	movl	$15, %edx
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2714:
	movl	$15, %edx
	jmp	.L2686
.L2711:
	movl	$15, %edx
	jmp	.L2678
	.cfi_endproc
.LFE7010:
	.size	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_, .-_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5867:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	.cfi_endproc
.LFE5867:
	.size	_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei:
.LFB9756:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2754
	ret
	.p2align 4,,10
	.p2align 3
.L2754:
	movq	8(%rdi), %rdx
	testl	%esi, %esi
	js	.L2753
	movslq	%esi, %rsi
	xorl	%edi, %edi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L2753:
	notl	%esi
	movl	$1, %edi
	movslq	%esi, %rsi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	.cfi_endproc
.LFE9756:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5879:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	testl	%edi, %edi
	js	.L2756
	movslq	%edi, %rsi
	xorl	%edi, %edi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L2756:
	notl	%edi
	movslq	%edi, %rsi
	movl	$1, %edi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	.cfi_endproc
.LFE5879:
	.size	_ZN4node9inspector8protocol4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor11EncodeInt32EiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	(%rsi,%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rsi
	movl	$2, %edi
	pushq	%rbx
	addq	%r12, %r13
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	leaq	16(%rbx), %r15
	subq	$24, %rsp
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	cmpq	%r12, %r13
	jne	.L2765
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2760:
	movb	%al, (%rdx,%r14)
	movq	(%rbx), %rdx
	movzbl	%ah, %eax
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rdx,%r14)
	movq	8(%rbx), %r14
	movq	(%rbx), %rdx
	leaq	1(%r14), %r9
	cmpq	%r15, %rdx
	je	.L2767
	movq	16(%rbx), %rcx
.L2761:
	cmpq	%rcx, %r9
	ja	.L2772
	movb	%al, (%rdx,%r14)
.L2771:
	movq	(%rbx), %rax
	addq	$2, %r12
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	cmpq	%r12, %r13
	je	.L2757
.L2765:
	movq	8(%rbx), %r14
	movq	(%rbx), %rdx
	movzwl	(%r12), %eax
	leaq	1(%r14), %r9
	cmpq	%r15, %rdx
	je	.L2766
	movq	16(%rbx), %rcx
.L2759:
	cmpq	%rcx, %r9
	jbe	.L2760
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movl	%eax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movq	-64(%rbp), %r9
	movl	-56(%rbp), %eax
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2766:
	movl	$15, %ecx
	jmp	.L2759
	.p2align 4,,10
	.p2align 3
.L2757:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2772:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%eax, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rdx
	movl	-64(%rbp), %eax
	movq	-56(%rbp), %r9
	movb	%al, (%rdx,%r14)
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2767:
	movl	$15, %ecx
	jmp	.L2761
	.cfi_endproc
.LFE5882:
	.size	_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.type	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_, @function
_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_:
.LFB7016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$3, %edi
	movq	%rbx, %r14
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r13), %rcx
	movq	%r15, -96(%rbp)
	addq	0(%r13), %rcx
	addq	%r12, %r14
	je	.L2774
	testq	%rbx, %rbx
	je	.L2799
.L2774:
	movq	%r12, -104(%rbp)
	movq	%r12, %rdx
	movq	%r15, %rax
	cmpq	$15, %r12
	ja	.L2800
.L2775:
	cmpq	%rbx, %r14
	je	.L2776
	leaq	15(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L2784
	leaq	-1(%r12), %rdx
	cmpq	$14, %rdx
	jbe	.L2784
	movq	%r12, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L2778:
	movdqu	(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L2778
	movq	%r12, %rdx
	andq	$-16, %rdx
	leaq	(%rbx,%rdx), %rdi
	addq	%rdx, %rax
	cmpq	%rdx, %r12
	je	.L2780
	movzbl	(%rdi), %edx
	movb	%dl, (%rax)
	leaq	1(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	1(%rdi), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	2(%rdi), %edx
	movb	%dl, 2(%rax)
	leaq	3(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	3(%rdi), %edx
	movb	%dl, 3(%rax)
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	4(%rdi), %edx
	movb	%dl, 4(%rax)
	leaq	5(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	5(%rdi), %edx
	movb	%dl, 5(%rax)
	leaq	6(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	6(%rdi), %edx
	movb	%dl, 6(%rax)
	leaq	7(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	7(%rdi), %edx
	movb	%dl, 7(%rax)
	leaq	8(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	8(%rdi), %edx
	movb	%dl, 8(%rax)
	leaq	9(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	9(%rdi), %edx
	movb	%dl, 9(%rax)
	leaq	10(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	10(%rdi), %edx
	movb	%dl, 10(%rax)
	leaq	11(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	11(%rdi), %edx
	movb	%dl, 11(%rax)
	leaq	12(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	12(%rdi), %edx
	movb	%dl, 12(%rax)
	leaq	13(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	13(%rdi), %edx
	movb	%dl, 13(%rax)
	leaq	14(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2780
	movzbl	14(%rdi), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rax
.L2776:
	movq	%rdx, -88(%rbp)
	movq	%r13, %rdi
	movb	$0, (%rax,%rdx)
	subq	0(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2773
	call	_ZdlPv@PLT
.L2773:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2801
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2800:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	jmp	.L2775
	.p2align 4,,10
	.p2align 3
.L2784:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2777:
	movzbl	(%rbx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r12
	jne	.L2777
	jmp	.L2780
.L2799:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2801:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7016:
	.size	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_, .-_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5885:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.cfi_endproc
.LFE5885:
	.size	_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE:
.LFB9752:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2805
	ret
	.p2align 4,,10
	.p2align 3
.L2805:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.cfi_endproc
.LFE9752:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"basic_string::at: __n (which is %zu) >= this->size() (which is %zu)"
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-128(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movq	%rdx, -136(%rbp)
	movq	8(%r8), %r12
	movq	%rcx, -144(%rbp)
	movq	(%r8), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -128(%rbp)
	movq	%r12, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jne	.L2807
	movl	-104(%rbp), %eax
	movq	-96(%rbp), %r8
.L2808:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2818
	addq	$120, %rsp
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2807:
	.cfi_restore_state
	xorl	%r8d, %r8d
	movl	$16, %eax
	cmpl	$12, %edx
	jne	.L2808
	movq	-72(%rbp), %r10
	movq	8(%rbx), %r11
	leaq	6(%r10), %rdx
	cmpq	%rdx, %r11
	jne	.L2808
	movl	$6, %r8d
	movl	$29, %eax
	testq	%r10, %r10
	je	.L2808
	movq	-128(%rbp), %rsi
	movq	-96(%rbp), %rdx
	cmpb	$-65, 6(%rsi,%rdx)
	jne	.L2808
	cmpb	$-1, -1(%r13,%r12)
	leaq	5(%r10), %r8
	movl	$30, %eax
	jne	.L2808
	movq	%r8, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r11, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE8_M_eraseEmm@PLT
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %rsi
	movq	%rbx, %rdx
	call	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	movq	8(%rbx), %rdx
	movl	$4294967295, %ecx
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r11
	addq	%rdx, %r10
	movq	%r10, %rax
	subq	%r11, %rax
	cmpq	%rcx, %rax
	ja	.L2816
	leaq	-4(%r11,%rdx), %rsi
	subq	%r10, %rsi
	cmpq	%rsi, %rdx
	jbe	.L2819
	movq	(%rbx), %rdx
	bswap	%eax
	orq	$-1, %r8
	movl	%eax, (%rdx,%rsi)
	xorl	%eax, %eax
	jmp	.L2808
.L2816:
	xorl	%r8d, %r8d
	movl	$31, %eax
	jmp	.L2808
.L2818:
	call	__stack_chk_fail@PLT
.L2819:
	leaq	.LC101(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE5960:
	.size	_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_
	.type	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_, @function
_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_:
.LFB7020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	(%rsi,%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	(%rdi,%r15), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r14, %rdi
	je	.L2821
	movq	%rdi, %rbx
	movq	%rdi, %rax
.L2823:
	cmpw	$127, (%rax)
	ja	.L2822
	addq	$2, %rax
	cmpq	%rax, %r14
	jne	.L2823
.L2821:
	movq	%r12, %rdx
	movl	$3, %edi
	movq	%rcx, -120(%rbp)
	leaq	-80(%rbp), %rbx
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r12), %r8
	addq	(%r12), %r8
	testq	%r14, %r14
	movq	%rbx, -96(%rbp)
	movq	-120(%rbp), %rcx
	je	.L2832
	testq	%r13, %r13
	je	.L2859
.L2832:
	sarq	%r15
	movq	%rbx, %rax
	movq	%r15, -104(%rbp)
	cmpq	$15, %r15
	ja	.L2860
.L2833:
	cmpq	%r14, %r13
	je	.L2834
	movq	%r14, %rdx
	subq	%r13, %rdx
	subq	$2, %rdx
	movq	%rdx, %rsi
	leaq	2(%rdx,%r13), %rdi
	shrq	%rsi
	addq	$1, %rsi
	cmpq	%rdi, %rax
	leaq	(%rax,%rsi), %rdi
	setnb	%r9b
	cmpq	%rdi, %r13
	setnb	%dil
	orb	%dil, %r9b
	je	.L2851
	cmpq	$28, %rdx
	jbe	.L2851
	movq	%rsi, %rcx
	movdqa	.LC102(%rip), %xmm2
	xorl	%edx, %edx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L2836:
	movdqu	0(%r13,%rdx,2), %xmm0
	movdqu	16(%r13,%rdx,2), %xmm1
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2836
	movq	%rsi, %rcx
	andq	$-16, %rcx
	leaq	0(%r13,%rcx,2), %rdx
	addq	%rcx, %rax
	cmpq	%rcx, %rsi
	je	.L2838
	movzwl	(%rdx), %ecx
	movb	%cl, (%rax)
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	2(%rdx), %ecx
	movb	%cl, 1(%rax)
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	4(%rdx), %ecx
	movb	%cl, 2(%rax)
	leaq	6(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	6(%rdx), %ecx
	movb	%cl, 3(%rax)
	leaq	8(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	8(%rdx), %ecx
	movb	%cl, 4(%rax)
	leaq	10(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	10(%rdx), %ecx
	movb	%cl, 5(%rax)
	leaq	12(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	12(%rdx), %ecx
	movb	%cl, 6(%rax)
	leaq	14(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	14(%rdx), %ecx
	movb	%cl, 7(%rax)
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	16(%rdx), %ecx
	movb	%cl, 8(%rax)
	leaq	18(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	18(%rdx), %ecx
	movb	%cl, 9(%rax)
	leaq	20(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	20(%rdx), %ecx
	movb	%cl, 10(%rax)
	leaq	22(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	22(%rdx), %ecx
	movb	%cl, 11(%rax)
	leaq	24(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	24(%rdx), %ecx
	movb	%cl, 12(%rax)
	leaq	26(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	26(%rdx), %ecx
	movb	%cl, 13(%rax)
	leaq	28(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L2838
	movzwl	28(%rdx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2838:
	movq	-104(%rbp), %r15
	movq	-96(%rbp), %rax
.L2834:
	movq	%r15, -88(%rbp)
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movb	$0, (%rax,%r15)
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	subq	(%r12), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2820
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2820:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2861
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2822:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdx
	leaq	16(%r12), %r15
	movl	$2, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2825:
	movb	%al, (%rdx,%r13)
	movq	(%r12), %rdx
	movzbl	%ah, %eax
	movq	%r9, 8(%r12)
	movb	$0, 1(%rdx,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rdx
	leaq	1(%r13), %r9
	cmpq	%r15, %rdx
	je	.L2842
	movq	16(%r12), %rcx
.L2826:
	cmpq	%rcx, %r9
	ja	.L2862
	movb	%al, (%rdx,%r13)
.L2858:
	movq	(%r12), %rax
	addq	$2, %rbx
	movq	%r9, 8(%r12)
	movb	$0, 1(%rax,%r13)
	cmpq	%rbx, %r14
	je	.L2820
.L2830:
	movq	8(%r12), %r13
	movq	(%r12), %rdx
	movzwl	(%rbx), %eax
	leaq	1(%r13), %r9
	cmpq	%r15, %rdx
	je	.L2841
	movq	16(%r12), %rcx
.L2824:
	cmpq	%rcx, %r9
	jbe	.L2825
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -128(%rbp)
	movl	%eax, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-128(%rbp), %r9
	movl	-120(%rbp), %eax
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2862:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movl	-128(%rbp), %eax
	movq	-120(%rbp), %r9
	movb	%al, (%rdx,%r13)
	jmp	.L2858
	.p2align 4,,10
	.p2align 3
.L2842:
	movl	$15, %ecx
	jmp	.L2826
	.p2align 4,,10
	.p2align 3
.L2841:
	movl	$15, %ecx
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2851:
	movzwl	(%rcx), %edx
	addq	$2, %rcx
	addq	$1, %rax
	movb	%dl, -1(%rax)
	cmpq	%rcx, %r14
	jne	.L2851
	jmp	.L2838
	.p2align 4,,10
	.p2align 3
.L2860:
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rcx, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %r15
	movq	-128(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %r8
	movq	%r15, -80(%rbp)
	jmp	.L2833
.L2861:
	call	__stack_chk_fail@PLT
.L2859:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7020:
	.size	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_, .-_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5891:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_
	.cfi_endproc
.LFE5891:
	.size	_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE:
.LFB9753:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2866
	ret
	.p2align 4,,10
	.p2align 3
.L2866:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanItEEPT_
	.cfi_endproc
.LFE9753:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE
	.section	.text._ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.type	_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_, @function
_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_:
.LFB7022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	addq	$16, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	-8(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-16(%rdx), %rax
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L2879
	movq	16(%r12), %rdx
.L2868:
	cmpq	%rdx, %r15
	ja	.L2896
.L2869:
	movb	$-42, (%rax,%r14)
	movq	(%r12), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, 8(%r12)
	movl	$2, %edi
	leaq	-80(%rbp), %r15
	movb	$0, 1(%rax,%r14)
	movq	%rbx, %r14
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r12), %rcx
	movq	%r15, -96(%rbp)
	addq	(%r12), %rcx
	addq	%r13, %r14
	je	.L2870
	testq	%rbx, %rbx
	je	.L2897
.L2870:
	movq	%r13, -104(%rbp)
	movq	%r13, %rdx
	movq	%r15, %rax
	cmpq	$15, %r13
	ja	.L2898
.L2871:
	cmpq	%rbx, %r14
	je	.L2872
	leaq	15(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L2881
	leaq	-1(%r13), %rdx
	cmpq	$14, %rdx
	jbe	.L2881
	movq	%r13, %rsi
	xorl	%edx, %edx
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L2874:
	movdqu	(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L2874
	movq	%r13, %rdx
	andq	$-16, %rdx
	leaq	(%rbx,%rdx), %rdi
	addq	%rdx, %rax
	cmpq	%rdx, %r13
	je	.L2876
	movzbl	(%rdi), %edx
	movb	%dl, (%rax)
	leaq	1(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	1(%rdi), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	2(%rdi), %edx
	movb	%dl, 2(%rax)
	leaq	3(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	3(%rdi), %edx
	movb	%dl, 3(%rax)
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	4(%rdi), %edx
	movb	%dl, 4(%rax)
	leaq	5(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	5(%rdi), %edx
	movb	%dl, 5(%rax)
	leaq	6(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	6(%rdi), %edx
	movb	%dl, 6(%rax)
	leaq	7(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	7(%rdi), %edx
	movb	%dl, 7(%rax)
	leaq	8(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	8(%rdi), %edx
	movb	%dl, 8(%rax)
	leaq	9(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	9(%rdi), %edx
	movb	%dl, 9(%rax)
	leaq	10(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	10(%rdi), %edx
	movb	%dl, 10(%rax)
	leaq	11(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	11(%rdi), %edx
	movb	%dl, 11(%rax)
	leaq	12(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	12(%rdi), %edx
	movb	%dl, 12(%rax)
	leaq	13(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	13(%rdi), %edx
	movb	%dl, 13(%rax)
	leaq	14(%rdi), %rdx
	cmpq	%rdx, %r14
	je	.L2876
	movzbl	14(%rdi), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L2876:
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rax
.L2872:
	movq	%rdx, -88(%rbp)
	movq	%r12, %rdi
	movb	$0, (%rax,%rdx)
	subq	(%r12), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2867
	call	_ZdlPv@PLT
.L2867:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2899
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2896:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L2869
	.p2align 4,,10
	.p2align 3
.L2898:
	xorl	%edx, %edx
	leaq	-104(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	jmp	.L2871
	.p2align 4,,10
	.p2align 3
.L2879:
	movl	$15, %edx
	jmp	.L2868
	.p2align 4,,10
	.p2align 3
.L2881:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2873:
	movzbl	(%rbx,%rdx), %esi
	movb	%sil, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r13
	jne	.L2873
	jmp	.L2876
.L2897:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L2899:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7022:
	.size	_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_, .-_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5894:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.cfi_endproc
.LFE5894:
	.size	_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE:
.LFB9754:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2903
	ret
	.p2align 4,,10
	.p2align 3
.L2903:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN4node9inspector8protocol4cbor16EncodeBinaryTmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.cfi_endproc
.LFE9754:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE
	.section	.rodata._ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_.str1.1,"aMS",@progbits,1
.LC103:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	.type	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_, @function
_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_:
.LFB7486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	je	.L2905
	movzbl	(%rsi), %eax
	movb	%al, (%rdx)
	addq	$1, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2905:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rax
	movq	(%rdi), %r15
	subq	%r15, %rdx
	movq	%rdx, %r12
	cmpq	%rax, %rdx
	je	.L2921
	movl	$1, %r14d
	testq	%rdx, %rdx
	je	.L2908
	leaq	(%rdx,%rdx), %r14
	cmpq	%r14, %rdx
	jbe	.L2922
	movq	%rax, %r14
.L2908:
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r14
	movzbl	(%rsi), %eax
	movb	%al, 0(%r13,%r12)
	leaq	1(%r13,%r12), %rax
	movq	%rax, -56(%rbp)
	testq	%r12, %r12
	jg	.L2923
	testq	%r15, %r15
	jne	.L2909
.L2910:
	movq	%r13, %xmm0
	movq	%r14, 16(%rbx)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2922:
	.cfi_restore_state
	testq	%r14, %r14
	cmovs	%rax, %r14
	jmp	.L2908
	.p2align 4,,10
	.p2align 3
.L2923:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L2909:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L2910
.L2921:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7486:
	.size	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_, .-_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK4node9inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK4node9inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-9(%rbp), %rsi
	movb	$-10, -9(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2927
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2927:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5354:
	.size	_ZNK4node9inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK4node9inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	.type	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_, @function
_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_:
.LFB7009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$5, %edi
	andl	$8160, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$23, %rsi
	jbe	.L2944
	cmpq	$255, %rsi
	jbe	.L2945
	cmpq	$65535, %rsi
	jbe	.L2946
	movl	$4294967295, %eax
	cmpq	%rax, %rsi
	jbe	.L2947
	orl	$27, %edi
	leaq	-41(%rbp), %r14
	movl	$56, %ebx
	movb	%dil, -41(%rbp)
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L2936:
	movl	%ebx, %ecx
	movq	%r12, %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	shrq	%cl, %rax
	subl	$8, %ebx
	movb	%al, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-8, %ebx
	jne	.L2936
.L2928:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2948
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2944:
	.cfi_restore_state
	orl	%esi, %edi
	leaq	-41(%rbp), %rsi
	movb	%dil, -41(%rbp)
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2947:
	orl	$26, %edi
	leaq	-41(%rbp), %r14
	movl	$24, %ebx
	movb	%dil, -41(%rbp)
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L2934:
	movl	%ebx, %ecx
	movl	%r12d, %eax
	movq	%r14, %rsi
	movq	%r13, %rdi
	shrl	%cl, %eax
	subl	$8, %ebx
	movb	%al, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-8, %ebx
	jne	.L2934
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2945:
	orl	$24, %edi
	leaq	-41(%rbp), %r14
	movb	%dil, -41(%rbp)
.L2943:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%r12b, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2946:
	orl	$25, %edi
	leaq	-41(%rbp), %r14
	movb	%dil, -41(%rbp)
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	%r12d, %eax
	sarl	$8, %eax
	movb	%al, -41(%rbp)
	jmp	.L2943
.L2948:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7009:
	.size	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_, .-_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPSt6vectorIhSaIhEE:
.LFB5866:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	.cfi_endproc
.LFE5866:
	.size	_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor9internals15WriteTokenStartENS2_9MajorTypeEmPSt6vectorIhSaIhEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor11EncodeInt32EiPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor11EncodeInt32EiPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor11EncodeInt32EiPSt6vectorIhSaIhEE:
.LFB5878:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	testl	%edi, %edi
	js	.L2951
	movslq	%edi, %rsi
	xorl	%edi, %edi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L2951:
	notl	%edi
	movslq	%edi, %rsi
	movl	$1, %edi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	.cfi_endproc
.LFE5878:
	.size	_ZN4node9inspector8protocol4cbor11EncodeInt32EiPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor11EncodeInt32EiPSt6vectorIhSaIhEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPSt6vectorIhSaIhEE:
.LFB5881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	(%rsi,%rsi), %r14
	pushq	%r13
	movq	%r14, %rsi
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$2, %edi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	leaq	0(%r13,%r14), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r13, %rax
	je	.L2952
	movq	8(%r15), %r12
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L2987:
	movb	%bl, (%r12)
	movq	8(%r15), %rax
	movzbl	%bh, %esi
	movq	16(%r15), %r9
	leaq	1(%rax), %r14
	movq	%r14, 8(%r15)
	cmpq	%r14, %r9
	je	.L2960
.L2990:
	movb	%sil, (%r14)
	movq	8(%r15), %rax
	addq	$2, %r13
	leaq	1(%rax), %r12
	movq	%r12, 8(%r15)
	cmpq	%r13, -56(%rbp)
	je	.L2952
.L2967:
	movzwl	0(%r13), %ebx
	cmpq	%r12, 16(%r15)
	jne	.L2987
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r11
	subq	%r11, %r12
	cmpq	%rax, %r12
	je	.L2962
	movl	$1, %r14d
	testq	%r12, %r12
	je	.L2957
	leaq	(%r12,%r12), %r14
	cmpq	%r14, %r12
	jbe	.L2988
	movabsq	$9223372036854775807, %r14
	jmp	.L2957
	.p2align 4,,10
	.p2align 3
.L2988:
	movabsq	$9223372036854775807, %rax
	testq	%r14, %r14
	cmovs	%rax, %r14
.L2957:
	movq	%r14, %rdi
	movq	%r11, -64(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	-64(%rbp), %r11
	leaq	(%rax,%r14), %r9
	movb	%bl, (%rax,%r12)
	movq	%rax, %r10
	leaq	1(%rax,%r12), %r14
	jg	.L2989
	testq	%r11, %r11
	jne	.L2958
.L2959:
	movq	%r10, %xmm0
	movq	%r14, %xmm1
	movq	%r9, 16(%r15)
	movzbl	%bh, %esi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%r14, %r9
	jne	.L2990
	.p2align 4,,10
	.p2align 3
.L2960:
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r14
	subq	%r14, %r9
	cmpq	%rax, %r9
	je	.L2962
	movl	$1, %r12d
	testq	%r9, %r9
	je	.L2963
	leaq	(%r9,%r9), %r12
	cmpq	%r12, %r9
	jbe	.L2991
	movabsq	$9223372036854775807, %r12
	jmp	.L2963
	.p2align 4,,10
	.p2align 3
.L2991:
	testq	%r12, %r12
	cmovs	%rax, %r12
.L2963:
	movq	%r12, %rdi
	movl	%esi, -76(%rbp)
	movq	%r9, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r9
	movl	-76(%rbp), %esi
	movq	%rax, %rbx
	leaq	(%rax,%r12), %rax
	movq	%rax, -64(%rbp)
	leaq	1(%rbx,%r9), %r12
	movb	%sil, (%rbx,%r9)
	testq	%r9, %r9
	jg	.L2992
	testq	%r14, %r14
	jne	.L2964
.L2965:
	movq	-64(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r12, %xmm2
	addq	$2, %r13
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	cmpq	%r13, -56(%rbp)
	jne	.L2967
.L2952:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2989:
	.cfi_restore_state
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r12, %rdx
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r9
	movq	%rax, %r10
.L2958:
	movq	%r11, %rdi
	movq	%r9, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r10
	jmp	.L2959
	.p2align 4,,10
	.p2align 3
.L2992:
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memmove@PLT
.L2964:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L2965
.L2962:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5881:
	.size	_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor14EncodeString16ENS1_4spanItEEPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei:
.LFB9768:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L2996
	ret
	.p2align 4,,10
	.p2align 3
.L2996:
	movq	8(%rdi), %rdx
	testl	%esi, %esi
	js	.L2995
	movslq	%esi, %rsi
	xorl	%edi, %edi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	.p2align 4,,10
	.p2align 3
.L2995:
	notl	%esi
	movl	$1, %edi
	movslq	%esi, %rsi
	jmp	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	.cfi_endproc
.LFE9768:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleArrayEnd() [with C = std::vector<unsigned char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv:
.LFB9739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2997
	movq	104(%rdi), %r8
	movq	%rdi, %rbx
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L2999
	cmpq	%rsi, %rdi
	je	.L3006
	cmpl	$2, -8(%rdi)
	jne	.L2999
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L3002:
	movq	16(%rbx), %rdi
	leaq	-25(%rbp), %rsi
	movb	$93, -25(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L2997:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3007
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3006:
	.cfi_restore_state
	movq	-8(%r8), %rax
	cmpl	$2, 504(%rax)
	jne	.L2999
	call	_ZdlPv@PLT
	movq	104(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L3002
.L2999:
	leaq	.LC104(%rip), %rcx
	movl	$3014, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC40(%rip), %rdi
	call	__assert_fail@PLT
.L3007:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9739:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleMapEnd() [with C = std::vector<unsigned char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv:
.LFB9737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3008
	movq	104(%rdi), %r8
	movq	%rdi, %rbx
	movq	80(%rdi), %rdi
	movq	88(%rbx), %rsi
	movq	%r8, %rdx
	subq	72(%rbx), %rdx
	movq	%rdi, %rax
	sarq	$3, %rdx
	subq	%rsi, %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L3010
	cmpq	%rsi, %rdi
	je	.L3017
	cmpl	$1, -8(%rdi)
	jne	.L3010
	subq	$8, %rdi
	movq	%rdi, 80(%rbx)
.L3013:
	movq	16(%rbx), %rdi
	leaq	-25(%rbp), %rsi
	movb	$125, -25(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L3008:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3018
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3017:
	.cfi_restore_state
	movq	-8(%r8), %rax
	cmpl	$1, 504(%rax)
	jne	.L3010
	call	_ZdlPv@PLT
	movq	104(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L3013
.L3010:
	leaq	.LC105(%rip), %rcx
	movl	$2998, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC38(%rip), %rdi
	call	__assert_fail@PLT
.L3018:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9737:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexISt6vectorIhSaIhEEEEvtPT_, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexISt6vectorIhSaIhEEEEvtPT_:
.LFB9794:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$12, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-41(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzwl	%di, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L3023:
	movl	%ebx, %eax
	movl	%r14d, %ecx
	sarl	%cl, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jle	.L3020
	addl	$87, %eax
.L3033:
	movq	%r13, %rsi
	movq	%r12, %rdi
	subl	$4, %r14d
	movb	%al, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-4, %r14d
	jne	.L3023
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3034
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3020:
	.cfi_restore_state
	addl	$48, %eax
	jmp	.L3033
.L3034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9794:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexISt6vectorIhSaIhEEEEvtPT_, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexISt6vectorIhSaIhEEEEvtPT_
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"void node::inspector::protocol::json::{anonymous}::State::StartElementTmpl(C*) [with C = std::vector<unsigned char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv:
.LFB9746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3035
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	88(%rdi), %r12
	je	.L3050
.L3037:
	movl	-8(%r12), %edx
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L3051
	testl	%eax, %eax
	je	.L3039
	cmpl	$2, %edx
	je	.L3042
	testb	$1, %al
	jne	.L3052
.L3042:
	movl	$44, %eax
.L3040:
	leaq	-25(%rbp), %rsi
	movb	%al, -25(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	jmp	.L3039
	.p2align 4,,10
	.p2align 3
.L3051:
	testl	%eax, %eax
	jne	.L3053
.L3039:
	addl	$1, %eax
	leaq	.LC0(%rip), %rsi
	movl	%eax, -4(%r12)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
.L3035:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3054
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3050:
	.cfi_restore_state
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	addq	$512, %r12
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3052:
	movl	$58, %eax
	jmp	.L3040
.L3054:
	call	__stack_chk_fail@PLT
.L3053:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9746:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv:
.LFB9738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3055
	movq	80(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	cmpq	88(%rdi), %rax
	je	.L3080
	movl	-8(%r12), %ecx
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L3058
.L3083:
	testl	%edx, %edx
	jne	.L3081
.L3059:
	movq	96(%rbx), %rcx
	addl	$1, %edx
	movl	%edx, -4(%r12)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L3061
	movq	$2, (%rax)
	addq	$8, %rax
	movq	%rax, 80(%rbx)
.L3062:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	$91, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L3055:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3082
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3058:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L3059
	cmpl	$2, %ecx
	je	.L3072
	andl	$1, %edx
	movl	$58, %eax
	jne	.L3060
.L3072:
	movl	$44, %eax
.L3060:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	80(%rbx), %rax
	movl	-4(%r12), %edx
	jmp	.L3059
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	104(%rdi), %rdx
	movq	-8(%rdx), %r12
	movl	504(%r12), %ecx
	addq	$512, %r12
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L3058
	jmp	.L3083
	.p2align 4,,10
	.p2align 3
.L3061:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L3084
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L3085
.L3064:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$2, (%rax)
	movq	104(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L3062
	.p2align 4,,10
	.p2align 3
.L3085:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L3065
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L3066
	cmpq	%r13, %rsi
	je	.L3067
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L3067:
	movq	(%r14), %rax
	movq	(%r14), %xmm0
	leaq	(%r14,%r12), %r13
	movq	%r14, 72(%rbx)
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L3064
	.p2align 4,,10
	.p2align 3
.L3065:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L3086
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L3069
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L3069:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L3067
.L3066:
	cmpq	%r13, %rsi
	je	.L3067
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L3067
.L3082:
	call	__stack_chk_fail@PLT
.L3081:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L3086:
	call	_ZSt17__throw_bad_allocv@PLT
.L3084:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9738:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb:
.LFB9745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3087
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	movl	%esi, %r13d
	cmpq	88(%rdi), %r12
	je	.L3104
.L3089:
	movl	-8(%r12), %edx
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L3105
	testl	%eax, %eax
	je	.L3091
	cmpl	$2, %edx
	je	.L3095
	testb	$1, %al
	jne	.L3106
.L3095:
	movl	$44, %eax
.L3092:
	leaq	-41(%rbp), %rsi
	movb	%al, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%rbx), %rdi
	movl	-4(%r12), %eax
	jmp	.L3091
	.p2align 4,,10
	.p2align 3
.L3105:
	testl	%eax, %eax
	jne	.L3107
.L3091:
	addl	$1, %eax
	leaq	.LC27(%rip), %rsi
	testb	%r13b, %r13b
	movl	%eax, -4(%r12)
	leaq	.LC3(%rip), %rax
	cmove	%rax, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
.L3087:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3108
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3104:
	.cfi_restore_state
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	addq	$512, %r12
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3106:
	movl	$58, %eax
	jmp	.L3092
.L3108:
	call	__stack_chk_fail@PLT
.L3107:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9745:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd:
.LFB9743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3109
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	cmpq	88(%rdi), %r12
	je	.L3134
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L3112
.L3141:
	testl	%eax, %eax
	jne	.L3135
.L3113:
	movsd	.LC29(%rip), %xmm2
	movapd	%xmm0, %xmm1
	addl	$1, %eax
	andpd	.LC28(%rip), %xmm1
	movl	%eax, -4(%r12)
	ucomisd	%xmm1, %xmm2
	jb	.L3136
	movq	8(%rbx), %rsi
	leaq	-32(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-32(%rbp), %r12
	movq	16(%rbx), %rdi
	movzbl	(%r12), %eax
	cmpb	$46, %al
	je	.L3137
	cmpb	$45, %al
	je	.L3138
.L3118:
	movq	%r12, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3109
	call	_ZdaPv@PLT
.L3109:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3139
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3112:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L3113
	cmpl	$2, %edx
	je	.L3121
	testb	$1, %al
	jne	.L3140
.L3121:
	movl	$44, %eax
.L3114:
	movq	16(%rbx), %rdi
	leaq	-32(%rbp), %rsi
	movb	%al, -32(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r12), %eax
	movsd	-56(%rbp), %xmm0
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3134:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L3112
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L3140:
	movl	$58, %eax
	jmp	.L3114
	.p2align 4,,10
	.p2align 3
.L3138:
	cmpb	$46, 1(%r12)
	jne	.L3118
	leaq	.LC46(%rip), %rsi
	addq	$1, %r12
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	jmp	.L3118
	.p2align 4,,10
	.p2align 3
.L3136:
	movq	16(%rbx), %rdi
	leaq	.LC0(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L3109
	.p2align 4,,10
	.p2align 3
.L3137:
	leaq	-33(%rbp), %rsi
	movb	$48, -33(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%rbx), %rdi
	jmp	.L3118
.L3139:
	call	__stack_chk_fail@PLT
.L3135:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9743:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"void node::inspector::protocol::json::{anonymous}::JSONEncoder<C>::HandleMapBegin() [with C = std::vector<unsigned char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv:
.LFB9736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3142
	movq	80(%rdi), %rax
	movq	%rdi, %rbx
	cmpq	48(%rdi), %rax
	je	.L3168
	movq	%rax, %r12
	cmpq	88(%rdi), %rax
	je	.L3169
	movl	-8(%r12), %ecx
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L3146
.L3172:
	testl	%edx, %edx
	jne	.L3170
.L3147:
	movq	96(%rbx), %rcx
	addl	$1, %edx
	movl	%edx, -4(%r12)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L3149
	movq	$1, (%rax)
	addq	$8, %rax
	movq	%rax, 80(%rbx)
.L3150:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	$123, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L3142:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3171
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3146:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L3147
	andl	$1, %edx
	je	.L3160
	movl	$58, %eax
	cmpl	$2, %ecx
	jne	.L3148
.L3160:
	movl	$44, %eax
.L3148:
	movq	16(%rbx), %rdi
	leaq	-57(%rbp), %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	80(%rbx), %rax
	movl	-4(%r12), %edx
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3169:
	movq	104(%rdi), %rdx
	movq	-8(%rdx), %r12
	movl	504(%r12), %ecx
	addq	$512, %r12
	movl	-4(%r12), %edx
	testl	%ecx, %ecx
	jne	.L3146
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3149:
	movq	104(%rbx), %r13
	movq	72(%rbx), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	88(%rbx), %rax
	movq	%r13, %r12
	sarq	$3, %rax
	subq	%rsi, %r12
	movq	%r12, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	64(%rbx), %rax
	subq	48(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L3173
	movq	32(%rbx), %r8
	movq	40(%rbx), %rdx
	movq	%r13, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L3174
.L3152:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r13)
	movq	80(%rbx), %rax
	movq	$1, (%rax)
	movq	104(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3174:
	leaq	2(%rcx), %r14
	leaq	(%r14,%r14), %rax
	cmpq	%rax, %rdx
	jbe	.L3153
	subq	%r14, %rdx
	addq	$8, %r13
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r14
	movq	%r13, %rdx
	subq	%rsi, %rdx
	cmpq	%r14, %rsi
	jbe	.L3154
	cmpq	%r13, %rsi
	je	.L3155
	movq	%r14, %rdi
	call	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L3155:
	movq	(%r14), %rax
	movq	(%r14), %xmm0
	leaq	(%r14,%r12), %r13
	movq	%r14, 72(%rbx)
	movq	%r13, 104(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 56(%rbx)
	movq	0(%r13), %rax
	movq	%rax, 88(%rbx)
	addq	$512, %rax
	movq	%rax, 96(%rbx)
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3153:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r13
	cmpq	%rdi, %r13
	ja	.L3175
	leaq	0(,%r13,8), %rdi
	call	_Znwm@PLT
	movq	72(%rbx), %rsi
	movq	%rax, %r15
	movq	%r13, %rax
	subq	%r14, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r14
	movq	104(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L3157
	subq	%rsi, %rdx
	movq	%r14, %rdi
	call	memmove@PLT
.L3157:
	movq	32(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	%r15, 32(%rbx)
	movq	%r13, 40(%rbx)
	jmp	.L3155
.L3154:
	cmpq	%r13, %rsi
	je	.L3155
	leaq	8(%r12), %rdi
	subq	%rdx, %rdi
	addq	%r14, %rdi
	call	memmove@PLT
	jmp	.L3155
.L3171:
	call	__stack_chk_fail@PLT
.L3168:
	leaq	.LC107(%rip), %rcx
	movl	$2989, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	call	__assert_fail@PLT
.L3170:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L3175:
	call	_ZSt17__throw_bad_allocv@PLT
.L3173:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9736:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE:
.LFB9742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3176
	movq	80(%rdi), %r12
	movq	%rdi, %r13
	movq	%rsi, %rcx
	cmpq	88(%rdi), %r12
	je	.L3199
.L3178:
	movl	-8(%r12), %edx
	movq	16(%r13), %rdi
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L3179
	testl	%eax, %eax
	jne	.L3194
.L3197:
	leaq	-57(%rbp), %r14
.L3180:
	addl	$1, %eax
	movq	%r14, %rsi
	movq	%rcx, -80(%rbp)
	movl	%eax, -4(%r12)
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpq	$2, -72(%rbp)
	movq	16(%r13), %r15
	movq	-80(%rbp), %rcx
	jbe	.L3188
	movl	$3, %edx
	leaq	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L3183:
	movzbl	-3(%rcx,%rdx), %eax
	movzbl	-2(%rcx,%rdx), %esi
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movzbl	-1(%rcx,%rdx), %r12d
	movq	%rcx, -80(%rbp)
	sall	$8, %esi
	sall	$16, %eax
	orl	%esi, %eax
	movq	%r14, %rsi
	orl	%eax, %r12d
	shrl	$18, %eax
	movzbl	(%rbx,%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	%r12d, %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	shrl	$12, %eax
	andl	$63, %eax
	movzbl	(%rbx,%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	%r12d, %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	shrl	$6, %eax
	andl	$63, %r12d
	andl	$63, %eax
	movzbl	(%rbx,%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movzbl	(%rbx,%r12), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rdx, %rax
	leaq	3(%rdx), %rdx
	cmpq	%rdx, -72(%rbp)
	jnb	.L3183
	leaq	2(%rax), %rsi
	leaq	1(%rax), %rdx
.L3182:
	cmpq	%rsi, -72(%rbp)
	jnb	.L3200
	cmpq	%rdx, -72(%rbp)
	jnb	.L3201
.L3185:
	movq	16(%r13), %rdi
	movq	%r14, %rsi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L3176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3202
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3179:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L3197
	cmpl	$2, %edx
	je	.L3187
	testb	$1, %al
	jne	.L3203
.L3187:
	movl	$44, %eax
.L3181:
	leaq	-57(%rbp), %r14
	movq	%rcx, -80(%rbp)
	movq	%r14, %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%r13), %rdi
	movl	-4(%r12), %eax
	movq	-80(%rbp), %rcx
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	addq	$512, %r12
	jmp	.L3178
	.p2align 4,,10
	.p2align 3
.L3203:
	movl	$58, %eax
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L3201:
	movzbl	(%rcx,%rax), %eax
	leaq	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r12
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%eax, %ebx
	shrl	$2, %eax
	sall	$16, %ebx
	movzbl	(%r12,%rax), %eax
	shrl	$12, %ebx
	andl	$48, %ebx
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movzbl	(%r12,%rbx), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movb	$61, -57(%rbp)
.L3198:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	$61, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	jmp	.L3185
	.p2align 4,,10
	.p2align 3
.L3200:
	movzbl	(%rcx,%rax), %eax
	movzbl	(%rcx,%rdx), %ebx
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE(%rip), %r12
	sall	$16, %eax
	sall	$8, %ebx
	orl	%eax, %ebx
	shrl	$18, %eax
	movzbl	(%r12,%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	%ebx, %eax
	shrl	$6, %ebx
	movq	%r14, %rsi
	shrl	$12, %eax
	movq	%r15, %rdi
	andl	$60, %ebx
	andl	$63, %eax
	movzbl	(%r12,%rax), %eax
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movzbl	(%r12,%rbx), %eax
	movb	%al, -57(%rbp)
	jmp	.L3198
	.p2align 4,,10
	.p2align 3
.L3188:
	movl	$1, %edx
	movl	$2, %esi
	xorl	%eax, %eax
	jmp	.L3182
.L3202:
	call	__stack_chk_fail@PLT
.L3194:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9742:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE:
.LFB9741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3204
	movq	80(%rdi), %r13
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r15
	cmpq	88(%rdi), %r13
	je	.L3245
.L3206:
	movl	-8(%r13), %edx
	movq	16(%r12), %rdi
	movl	-4(%r13), %eax
	testl	%edx, %edx
	jne	.L3207
	testl	%eax, %eax
	jne	.L3238
.L3241:
	leaq	-57(%rbp), %r14
.L3208:
	addl	$1, %eax
	movq	%r14, %rsi
	movl	%eax, -4(%r13)
	leaq	(%rbx,%r15,2), %r13
	movq	%rbx, %r15
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpq	%rbx, %r13
	jne	.L3224
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3211:
	cmpw	$92, %bx
	je	.L3246
	cmpw	$8, %bx
	je	.L3247
	cmpw	$12, %bx
	je	.L3248
	cmpw	$10, %bx
	je	.L3249
	cmpw	$13, %bx
	je	.L3250
	cmpw	$9, %bx
	je	.L3251
	leal	-32(%rbx), %edx
	cmpw	$94, %dx
	ja	.L3219
	movq	%r14, %rsi
	movb	%bl, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	.p2align 4,,10
	.p2align 3
.L3212:
	addq	$2, %r15
	cmpq	%r15, %r13
	je	.L3223
.L3224:
	movzwl	(%r15), %ebx
	movq	16(%r12), %rdi
	cmpw	$34, %bx
	jne	.L3211
	leaq	.LC49(%rip), %rsi
	addq	$2, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	cmpq	%r15, %r13
	jne	.L3224
	.p2align 4,,10
	.p2align 3
.L3223:
	movq	16(%r12), %rdi
	movq	%r14, %rsi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L3204:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3252
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3207:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L3241
	cmpl	$2, %edx
	je	.L3226
	testb	$1, %al
	jne	.L3253
.L3226:
	movl	$44, %eax
.L3209:
	leaq	-57(%rbp), %r14
	movb	%al, -57(%rbp)
	movq	%r14, %rsi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%r12), %rdi
	movl	-4(%r13), %eax
	jmp	.L3208
	.p2align 4,,10
	.p2align 3
.L3246:
	leaq	.LC50(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3247:
	leaq	.LC51(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3248:
	leaq	.LC52(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3219:
	leaq	.LC56(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%r12), %rax
	movq	%rax, -80(%rbp)
	movzwl	%bx, %eax
	movl	$12, %ebx
	movl	%eax, -68(%rbp)
	movl	-68(%rbp), %edx
	movl	%ebx, %ecx
	sarl	%cl, %edx
	andl	$15, %edx
	cmpl	$9, %edx
	jle	.L3220
.L3254:
	addl	$87, %edx
.L3243:
	movq	-80(%rbp), %rdi
	movq	%r14, %rsi
	subl	$4, %ebx
	movb	%dl, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-4, %ebx
	je	.L3212
	movl	-68(%rbp), %edx
	movl	%ebx, %ecx
	sarl	%cl, %edx
	andl	$15, %edx
	cmpl	$9, %edx
	jg	.L3254
.L3220:
	addl	$48, %edx
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3249:
	leaq	.LC53(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3250:
	leaq	.LC54(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3251:
	leaq	.LC55(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3245:
	movq	104(%rdi), %rax
	movq	-8(%rax), %r13
	addq	$512, %r13
	jmp	.L3206
	.p2align 4,,10
	.p2align 3
.L3253:
	movl	$58, %eax
	jmp	.L3209
.L3252:
	call	__stack_chk_fail@PLT
.L3238:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9741:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE:
.LFB9740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3255
	movq	80(%rdi), %r15
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%rdx, %r12
	cmpq	88(%rdi), %r15
	je	.L3312
	movl	-8(%r15), %edx
	movq	16(%rbx), %rdi
	movl	-4(%r15), %eax
	testl	%edx, %edx
	jne	.L3258
.L3327:
	testl	%eax, %eax
	jne	.L3305
.L3308:
	leaq	-57(%rbp), %r14
.L3259:
	addl	$1, %eax
	movq	%r14, %rsi
	movl	%eax, -4(%r15)
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%rbx), %rdi
	testq	%r12, %r12
	je	.L3286
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L3285:
	movzbl	0(%r13,%r15), %eax
	cmpb	$34, %al
	je	.L3313
	cmpb	$92, %al
	je	.L3314
	cmpb	$8, %al
	je	.L3315
	cmpb	$12, %al
	je	.L3316
	cmpb	$10, %al
	je	.L3317
	cmpb	$13, %al
	je	.L3318
	cmpb	$9, %al
	je	.L3319
	leal	-32(%rax), %ecx
	cmpb	$94, %cl
	jbe	.L3320
	cmpb	$31, %al
	jbe	.L3321
	movl	%eax, %ecx
	andl	$-32, %ecx
	cmpb	$-64, %cl
	je	.L3322
	movl	%eax, %ecx
	andl	$-16, %ecx
	cmpb	$-32, %cl
	je	.L3323
	movl	%eax, %ecx
	andl	$-8, %ecx
	cmpb	$-16, %cl
	jne	.L3263
	andl	$7, %eax
	movl	$3, %ecx
	movl	$3, %r8d
.L3277:
	addq	%r15, %rcx
	cmpq	%r12, %rcx
	ja	.L3263
	movzbl	1(%r15,%r13), %esi
	leal	-1(%r8), %ecx
	leaq	1(%r15), %r9
	movl	%esi, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L3279
	sall	$6, %eax
	andl	$63, %esi
	orl	%esi, %eax
.L3279:
	testl	%ecx, %ecx
	je	.L3280
	movzbl	2(%r13,%r15), %esi
	movl	%esi, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L3281
	sall	$6, %eax
	andl	$63, %esi
	orl	%esi, %eax
.L3281:
	cmpl	$1, %ecx
	je	.L3282
	movzbl	3(%r13,%r15), %edx
	movl	%edx, %esi
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L3282
	sall	$6, %eax
	andl	$63, %edx
	orl	%edx, %eax
.L3282:
	movslq	%ecx, %rdx
	leal	-127(%rax), %ecx
	leaq	(%rdx,%r9), %r15
	cmpl	$1113984, %ecx
	jbe	.L3324
	.p2align 4,,10
	.p2align 3
.L3263:
	addq	$1, %r15
	cmpq	%r15, %r12
	ja	.L3285
	.p2align 4,,10
	.p2align 3
.L3286:
	movq	%r14, %rsi
	movb	$34, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
.L3255:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3325
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3258:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L3308
	cmpl	$2, %edx
	je	.L3289
	testb	$1, %al
	jne	.L3326
.L3289:
	movl	$44, %eax
.L3260:
	leaq	-57(%rbp), %r14
	movb	%al, -57(%rbp)
	movq	%r14, %rsi
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%rbx), %rdi
	movl	-4(%r15), %eax
	jmp	.L3259
	.p2align 4,,10
	.p2align 3
.L3313:
	leaq	.LC49(%rip), %rsi
	addq	$1, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3314:
	leaq	.LC50(%rip), %rsi
	addq	$1, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3315:
	leaq	.LC51(%rip), %rsi
	addq	$1, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3316:
	leaq	.LC52(%rip), %rsi
	addq	$1, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3317:
	leaq	.LC53(%rip), %rsi
	addq	$1, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3320:
	movq	%r14, %rsi
	addq	$1, %r15
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3318:
	leaq	.LC54(%rip), %rsi
	addq	$1, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3319:
	leaq	.LC55(%rip), %rsi
	addq	$1, %r15
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3321:
	leaq	.LC56(%rip), %rsi
	movb	%al, -68(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movq	16(%rbx), %rax
	movl	$12, %ecx
	movq	%rax, -80(%rbp)
	movzbl	-68(%rbp), %eax
	movl	%eax, -68(%rbp)
.L3275:
	movl	-68(%rbp), %eax
	movl	%ecx, -72(%rbp)
	sarl	%cl, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jle	.L3272
	addl	$87, %eax
.L3310:
	movq	-80(%rbp), %rdi
	movq	%r14, %rsi
	movb	%al, -57(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-72(%rbp), %ecx
	subl	$4, %ecx
	cmpl	$-4, %ecx
	jne	.L3275
.L3311:
	addq	$1, %r15
	movq	16(%rbx), %rdi
	cmpq	%r15, %r12
	ja	.L3285
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3272:
	addl	$48, %eax
	jmp	.L3310
	.p2align 4,,10
	.p2align 3
.L3312:
	movq	104(%rdi), %rax
	movq	16(%rbx), %rdi
	movq	-8(%rax), %r15
	movl	504(%r15), %edx
	addq	$512, %r15
	movl	-4(%r15), %eax
	testl	%edx, %edx
	jne	.L3258
	jmp	.L3327
	.p2align 4,,10
	.p2align 3
.L3326:
	movl	$58, %eax
	jmp	.L3260
	.p2align 4,,10
	.p2align 3
.L3322:
	andl	$31, %eax
	movl	$1, %ecx
	movl	$1, %r8d
	jmp	.L3277
	.p2align 4,,10
	.p2align 3
.L3323:
	andl	$15, %eax
	movl	$2, %ecx
	movl	$2, %r8d
	jmp	.L3277
.L3280:
	leal	-127(%rax), %ecx
	movq	%r9, %r15
	cmpl	$1113984, %ecx
	ja	.L3263
.L3287:
	leaq	.LC56(%rip), %rsi
	movq	%r9, -80(%rbp)
	movl	%eax, -68(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movl	-68(%rbp), %eax
	movq	16(%rbx), %rsi
	movl	%eax, %edi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexISt6vectorIhSaIhEEEEvtPT_
	movq	-80(%rbp), %r9
	movq	16(%rbx), %rdi
	movq	%r9, %r15
	jmp	.L3263
.L3324:
	cmpl	$65534, %eax
	jbe	.L3328
	subl	$65536, %eax
	leaq	.LC56(%rip), %rsi
	movl	%eax, -68(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movl	-68(%rbp), %edi
	movq	16(%rbx), %rsi
	shrl	$10, %edi
	subw	$10240, %di
	movzwl	%di, %edi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexISt6vectorIhSaIhEEEEvtPT_
	movq	16(%rbx), %rdi
	leaq	.LC56(%rip), %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE4EmitEPKc.isra.0
	movzwl	-68(%rbp), %eax
	movq	16(%rbx), %rsi
	andw	$1023, %ax
	leal	-9216(%rax), %edi
	movzwl	%di, %edi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_18PrintHexISt6vectorIhSaIhEEEEvtPT_
	jmp	.L3311
.L3325:
	call	__stack_chk_fail@PLT
.L3305:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
.L3328:
	movq	%r15, %r9
	jmp	.L3287
	.cfi_endproc
.LFE9740:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE
	.section	.text._ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,"axG",@progbits,_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.type	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, @function
_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_:
.LFB7515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3330
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	48(%rbx), %rcx
.L3333:
	cmpq	%rcx, %r13
	je	.L3350
.L3331:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3330
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L3333
.L3330:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3350:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L3331
	testq	%rdx, %rdx
	je	.L3332
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L3331
.L3332:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7515:
	.size	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, .-_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3351
	movq	40(%rax), %rax
.L3351:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5397:
	.size	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue10getBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPb
	.type	_ZNK4node9inspector8protocol15DictionaryValue10getBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPb, @function
_ZNK4node9inspector8protocol15DictionaryValue10getBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPb:
.LFB5391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3357
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3357
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3357:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5391:
	.size	_ZNK4node9inspector8protocol15DictionaryValue10getBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPb, .-_ZNK4node9inspector8protocol15DictionaryValue10getBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPb
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue10getIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPi
	.type	_ZNK4node9inspector8protocol15DictionaryValue10getIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPi, @function
_ZNK4node9inspector8protocol15DictionaryValue10getIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPi:
.LFB5392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3364
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3364
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3364:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5392:
	.size	_ZNK4node9inspector8protocol15DictionaryValue10getIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPi, .-_ZNK4node9inspector8protocol15DictionaryValue10getIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPi
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue9getDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPd
	.type	_ZNK4node9inspector8protocol15DictionaryValue9getDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPd, @function
_ZNK4node9inspector8protocol15DictionaryValue9getDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPd:
.LFB5393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3371
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3371
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	40(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3371:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5393:
	.size	_ZNK4node9inspector8protocol15DictionaryValue9getDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPd, .-_ZNK4node9inspector8protocol15DictionaryValue9getDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPd
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue9getStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS8_
	.type	_ZNK4node9inspector8protocol15DictionaryValue9getStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS8_, @function
_ZNK4node9inspector8protocol15DictionaryValue9getStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS8_:
.LFB5394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3378
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3378
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	56(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3378:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5394:
	.size	_ZNK4node9inspector8protocol15DictionaryValue9getStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS8_, .-_ZNK4node9inspector8protocol15DictionaryValue9getStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS8_
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue9getObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol15DictionaryValue9getObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol15DictionaryValue9getObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3385
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L3385
	cmpl	$6, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L3385:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5395:
	.size	_ZNK4node9inspector8protocol15DictionaryValue9getObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol15DictionaryValue9getObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue8getArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol15DictionaryValue8getArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol15DictionaryValue8getArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3395
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L3395
	cmpl	$7, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L3395:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5396:
	.size	_ZNK4node9inspector8protocol15DictionaryValue8getArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol15DictionaryValue8getArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC108:
	.string	"Message must be a valid JSON"
.LC109:
	.string	"Message must be an object"
	.section	.rodata.str1.8
	.align 8
.LC110:
	.string	"Message must have integer 'id' property"
	.align 8
.LC111:
	.string	"Message must have string 'method' property"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcher12parseCommandEPNS1_5ValueEPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol14UberDispatcher12parseCommandEPNS1_5ValueEPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol14UberDispatcher12parseCommandEPNS1_5ValueEPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3448
	cmpl	$6, 8(%rsi)
	jne	.L3449
	leaq	16(%rsi), %r12
	leaq	-96(%rbp), %r15
	movq	%rdx, %r13
	movl	$25705, %edx
	leaq	-80(%rbp), %r14
	movq	%r15, %rsi
	movq	%r12, %rdi
	movw	%dx, -80(%rbp)
	movl	$0, -132(%rbp)
	movq	%r14, -96(%rbp)
	movq	$2, -88(%rbp)
	movb	$0, -78(%rbp)
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3413
	movq	-96(%rbp), %rdi
	movq	40(%rax), %r8
	cmpq	%r14, %rdi
	je	.L3414
	movq	%r8, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %r8
.L3414:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	leaq	-132(%rbp), %rsi
	movq	%r8, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L3416
	testq	%r13, %r13
	je	.L3422
	movl	-132(%rbp), %eax
	movl	%eax, 0(%r13)
.L3422:
	movl	$25711, %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movl	$1752458605, -80(%rbp)
	movw	%ax, -76(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3424
	movq	-96(%rbp), %rdi
	movq	40(%rax), %r13
	cmpq	%r14, %rdi
	je	.L3425
	call	_ZdlPv@PLT
.L3425:
	leaq	-112(%rbp), %r12
	movq	$0, -120(%rbp)
	leaq	-128(%rbp), %rsi
	movq	%r12, -128(%rbp)
	movb	$0, -112(%rbp)
	testq	%r13, %r13
	je	.L3427
	movq	0(%r13), %rax
	movq	%rsi, -160(%rbp)
	movq	%r13, %rdi
	call	*56(%rax)
	movq	-160(%rbp), %rsi
	testb	%al, %al
	movl	%eax, %r13d
	je	.L3427
	cmpq	$0, -152(%rbp)
	je	.L3433
	movq	-152(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L3433:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L3405
	call	_ZdlPv@PLT
	jmp	.L3405
	.p2align 4,,10
	.p2align 3
.L3413:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3416
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3416:
	movq	%r15, %rdi
	leaq	.LC110(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3420
	movq	%r15, %rdx
	movl	$-32600, %esi
	call	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelENS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0
.L3420:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3423
.L3446:
	call	_ZdlPv@PLT
.L3423:
	xorl	%r13d, %r13d
.L3405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3450
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3449:
	.cfi_restore_state
	leaq	-96(%rbp), %r12
	leaq	.LC109(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3411
	movq	%r12, %rdx
	movl	$-32600, %esi
	call	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelENS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0
.L3411:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3446
	jmp	.L3423
	.p2align 4,,10
	.p2align 3
.L3424:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3445
	call	_ZdlPv@PLT
.L3445:
	leaq	-112(%rbp), %r12
	movq	$0, -120(%rbp)
	movq	%r12, -128(%rbp)
	movb	$0, -112(%rbp)
.L3427:
	movq	%r15, %rdi
	leaq	.LC111(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3431
	movl	-132(%rbp), %esi
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$-32600, %edx
	call	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0
.L3431:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3434
	call	_ZdlPv@PLT
.L3434:
	xorl	%r13d, %r13d
	jmp	.L3433
	.p2align 4,,10
	.p2align 3
.L3448:
	leaq	-96(%rbp), %r12
	leaq	.LC108(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3411
	movq	%r12, %rdx
	movl	$-32700, %esi
	call	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelENS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0
	jmp	.L3411
.L3450:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5606:
	.size	_ZN4node9inspector8protocol14UberDispatcher12parseCommandEPNS1_5ValueEPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol14UberDispatcher12parseCommandEPNS1_5ValueEPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue2atB5cxx11Em
	.type	_ZNK4node9inspector8protocol15DictionaryValue2atB5cxx11Em, @function
_ZNK4node9inspector8protocol15DictionaryValue2atB5cxx11Em:
.LFB5398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	salq	$5, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	addq	72(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	movq	%r13, -80(%rbp)
	movq	(%rdx), %rsi
	addq	%rsi, %rax
	movq	%rax, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	movq	40(%rax), %rbx
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-80(%rbp), %rdi
	movq	%rbx, 32(%r12)
	cmpq	%r13, %rdi
	je	.L3451
	call	_ZdlPv@PLT
.L3451:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3455
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3455:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5398:
	.size	_ZNK4node9inspector8protocol15DictionaryValue2atB5cxx11Em, .-_ZNK4node9inspector8protocol15DictionaryValue2atB5cxx11Em
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE:
.LFB5413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$123, %esi
	leaq	16(%r13), %r14
	pushq	%rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZNSo3putEc@PLT
	movq	72(%r13), %rsi
	cmpq	%rsi, 80(%r13)
	je	.L3461
	.p2align 4,,10
	.p2align 3
.L3462:
	movq	%r15, %rax
	movq	%r14, %rdi
	salq	$5, %rax
	addq	%rax, %rsi
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3465
	testq	%r15, %r15
	jne	.L3466
	leaq	8(%rax), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	72(%r13), %rsi
	movq	80(%r13), %rax
	subq	%rsi, %rax
	cmpq	$63, %rax
	jbe	.L3461
	movl	$1, %r15d
	jmp	.L3462
	.p2align 4,,10
	.p2align 3
.L3466:
	movl	$44, %esi
	movq	%r12, %rdi
	addq	$1, %r15
	call	_ZNSo3putEc@PLT
	leaq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE@PLT
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	40(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	72(%r13), %rsi
	movq	80(%r13), %rax
	subq	%rsi, %rax
	sarq	$5, %rax
	cmpq	%rax, %r15
	jb	.L3462
.L3461:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$125, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSo3putEc@PLT
	.p2align 4,,10
	.p2align 3
.L3465:
	.cfi_restore_state
	leaq	_ZZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5413:
	.size	_ZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE, .-_ZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue14doublePropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd
	.type	_ZNK4node9inspector8protocol15DictionaryValue14doublePropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd, @function
_ZNK4node9inspector8protocol15DictionaryValue14doublePropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd:
.LFB5411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movsd	%xmm0, -16(%rbp)
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3468
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3468
	movq	(%rdi), %rax
	leaq	-16(%rbp), %rsi
	call	*40(%rax)
.L3468:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	movsd	-16(%rbp), %xmm0
	jne	.L3477
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3477:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5411:
	.size	_ZNK4node9inspector8protocol15DictionaryValue14doublePropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd, .-_ZNK4node9inspector8protocol15DictionaryValue14doublePropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue15booleanPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb
	.type	_ZNK4node9inspector8protocol15DictionaryValue15booleanPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb, @function
_ZNK4node9inspector8protocol15DictionaryValue15booleanPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb:
.LFB5409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movb	%dl, -9(%rbp)
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3479
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3479
	movq	(%rdi), %rax
	leaq	-9(%rbp), %rsi
	call	*32(%rax)
.L3479:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movzbl	-9(%rbp), %eax
	jne	.L3488
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3488:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5409:
	.size	_ZNK4node9inspector8protocol15DictionaryValue15booleanPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb, .-_ZNK4node9inspector8protocol15DictionaryValue15booleanPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue15integerPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.type	_ZNK4node9inspector8protocol15DictionaryValue15integerPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, @function
_ZNK4node9inspector8protocol15DictionaryValue15integerPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi:
.LFB5410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edx, -12(%rbp)
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3490
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3490
	movq	(%rdi), %rax
	leaq	-12(%rbp), %rsi
	call	*48(%rax)
.L3490:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movl	-12(%rbp), %eax
	jne	.L3499
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3499:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5410:
	.size	_ZNK4node9inspector8protocol15DictionaryValue15integerPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, .-_ZNK4node9inspector8protocol15DictionaryValue15integerPropertyERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_:
.LFB7678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3501
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	72(%rbx), %rcx
.L3504:
	cmpq	%rcx, %r13
	je	.L3521
.L3502:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3501
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L3504
.L3501:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3521:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L3502
	testq	%rdx, %rdx
	je	.L3503
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L3502
.L3503:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7678:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcher11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol14UberDispatcher11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol14UberDispatcher11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$56, %rsp
	movq	(%rsi), %r8
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -80(%rbp)
	addq	%r8, %rdx
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	16(%r12), %rdi
	movq	%r13, %rsi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3523
	leaq	40(%rax), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L3523:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol14UberDispatcher14findDispatcherERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-80(%rbp), %rdi
	testq	%rax, %rax
	setne	%r12b
	cmpq	%rbx, %rdi
	je	.L3522
	call	_ZdlPv@PLT
.L3522:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3530
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3530:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5608:
	.size	_ZN4node9inspector8protocol14UberDispatcher11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol14UberDispatcher11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC112:
	.string	"'"
.LC113:
	.string	"' wasn't found"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14UberDispatcher8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EESA_
	.type	_ZN4node9inspector8protocol14UberDispatcher8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EESA_, @function
_ZN4node9inspector8protocol14UberDispatcher8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EESA_:
.LFB5609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	leaq	-144(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$152, %rsp
	movq	%r8, -184(%rbp)
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	movq	%r15, -160(%rbp)
	addq	%rsi, %rax
	movq	%rax, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	leaq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L3532
	leaq	40(%rax), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L3532:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN4node9inspector8protocol14UberDispatcher14findDispatcherERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3563
	movq	(%r12), %rax
	movq	$0, (%r12)
	testq	%rax, %rax
	je	.L3543
	cmpl	$6, 8(%rax)
	movl	$0, %edx
	cmovne	%rdx, %rax
.L3543:
	movq	(%rdi), %r10
	movq	-184(%rbp), %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rax, -168(%rbp)
	leaq	-168(%rbp), %r8
	call	*24(%r10)
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3544
	movq	(%rdi), %rax
	call	*24(%rax)
.L3544:
	movq	-160(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3531
	call	_ZdlPv@PLT
.L3531:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3564
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3563:
	.cfi_restore_state
	movq	-152(%rbp), %rax
	leaq	-128(%rbp), %r14
	leaq	-112(%rbp), %r12
	movq	$0, -120(%rbp)
	movq	%r14, %rdi
	movq	%r12, -128(%rbp)
	leaq	1(%rax), %rsi
	movb	$0, -112(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rcx
	cmpq	%rcx, -120(%rbp)
	je	.L3535
	movl	$1, %edx
	leaq	.LC112(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movabsq	$4611686018427387903, %rcx
	subq	-120(%rbp), %rcx
	cmpq	$13, %rcx
	jbe	.L3535
	movl	$14, %edx
	movq	%r14, %rdi
	leaq	.LC113(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L3565
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L3537:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	8(%rbx), %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	testq	%rdi, %rdi
	je	.L3538
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$-32601, %edx
	movl	%r13d, %esi
	call	_ZN4node9inspector8protocolL21reportProtocolErrorToEPNS1_15FrontendChannelEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE.part.0
.L3538:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3539
	call	_ZdlPv@PLT
.L3539:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L3544
	call	_ZdlPv@PLT
	jmp	.L3544
	.p2align 4,,10
	.p2align 3
.L3565:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L3537
.L3564:
	call	__stack_chk_fail@PLT
.L3535:
	leaq	.LC62(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5609:
	.size	_ZN4node9inspector8protocol14UberDispatcher8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EESA_, .-_ZN4node9inspector8protocol14UberDispatcher8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EESA_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L3593
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L3584
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L3594
.L3568:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L3583:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L3595
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L3571:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L3572
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L3576
	.p2align 4,,10
	.p2align 3
.L3573:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L3592:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L3596
.L3576:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L3573
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L3592
	.p2align 4,,10
	.p2align 3
.L3596:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L3572:
	cmpq	%r12, %rbx
	je	.L3577
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L3581:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L3597
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L3581
.L3579:
	subq	%rbx, %r12
	addq	%r12, %r8
.L3577:
	testq	%r15, %r15
	je	.L3582
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L3582:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3597:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L3581
	jmp	.L3579
	.p2align 4,,10
	.p2align 3
.L3594:
	testq	%r8, %r8
	jne	.L3569
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L3583
	.p2align 4,,10
	.p2align 3
.L3584:
	movl	$32, %esi
	jmp	.L3568
	.p2align 4,,10
	.p2align 3
.L3595:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L3571
.L3569:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L3568
.L3593:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8210:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.rodata.str1.1
.LC114:
	.string	": "
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport8addErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol12ErrorSupport8addErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol12ErrorSupport8addErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-368(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$456, %rsp
	movq	%rsi, -472(%rbp)
	movq	.LC32(%rip), %xmm1
	movhps	.LC33(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -496(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r12, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-496(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%r15), %rax
	cmpq	%rax, 8(%r15)
	je	.L3604
	.p2align 4,,10
	.p2align 3
.L3599:
	movq	%rbx, %rdx
	movq	%r12, %rdi
	addq	$1, %rbx
	salq	$5, %rdx
	addq	%rdx, %rax
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movq	8(%r15), %rdx
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %rbx
	jnb	.L3604
	testq	%rbx, %rbx
	je	.L3599
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	(%r15), %rax
	jmp	.L3599
	.p2align 4,,10
	.p2align 3
.L3604:
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	.LC114(%rip), %rsi
	call	_ZNSo5writeEPKcl@PLT
	movq	-472(%rbp), %rax
	movq	%r12, %rdi
	leaq	-448(%rbp), %rbx
	leaq	-464(%rbp), %r12
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	movq	%rbx, -464(%rbp)
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L3621
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L3622
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L3606:
	movq	32(%r15), %rsi
	cmpq	40(%r15), %rsi
	je	.L3607
.L3625:
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-464(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3623
	movq	%rax, (%rsi)
	movq	-448(%rbp), %rax
	movq	%rax, 16(%rsi)
.L3609:
	movq	-456(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 32(%r15)
.L3610:
	movq	.LC32(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC34(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-496(%rbp), %rdi
	je	.L3611
	call	_ZdlPv@PLT
.L3611:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3624
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3622:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	32(%r15), %rsi
	cmpq	40(%r15), %rsi
	jne	.L3625
.L3607:
	leaq	24(%r15), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3610
	call	_ZdlPv@PLT
	jmp	.L3610
	.p2align 4,,10
	.p2align 3
.L3623:
	movdqa	-448(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L3609
	.p2align 4,,10
	.p2align 3
.L3621:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L3606
.L3624:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5334:
	.size	_ZN4node9inspector8protocol12ErrorSupport8addErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol12ErrorSupport8addErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc
	.type	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc, @function
_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc:
.LFB5333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -96(%rbp)
	testq	%rsi, %rsi
	je	.L3636
	movq	%rdi, %r13
	movq	%rsi, %rdi
	leaq	-96(%rbp), %r14
	movq	%rsi, %r15
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L3637
	cmpq	$1, %rax
	jne	.L3630
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%rbx, %rdx
.L3631:
	movq	%rax, -88(%rbp)
	movq	%r13, %rdi
	movq	%r14, %rsi
	movb	$0, (%rdx,%rax)
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3626
	call	_ZdlPv@PLT
.L3626:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3638
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3630:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L3639
	movq	%rbx, %rdx
	jmp	.L3631
	.p2align 4,,10
	.p2align 3
.L3637:
	movq	%r14, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L3629:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L3631
	.p2align 4,,10
	.p2align 3
.L3636:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L3638:
	call	__stack_chk_fail@PLT
.L3639:
	movq	%rbx, %rdi
	jmp	.L3629
	.cfi_endproc
.LFE5333:
	.size	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc, .-_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol6Object9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol6Object9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol6Object9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE:
.LFB5467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3641
	cmpl	$6, 8(%rsi)
	jne	.L3641
	movq	(%rsi), %rax
	leaq	-72(%rbp), %rdi
	call	*88(%rax)
	movq	-72(%rbp), %rbx
	movl	$8, %edi
	call	_Znwm@PLT
	movq	%rbx, (%rax)
	movq	%rax, (%r12)
.L3640:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3650
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3641:
	.cfi_restore_state
	movq	%rdx, %rdi
	leaq	-64(%rbp), %rsi
	leaq	-48(%rbp), %rbx
	movb	$100, -34(%rbp)
	movabsq	$7286952167337517679, %rax
	movq	%rbx, -64(%rbp)
	movq	%rax, -48(%rbp)
	movl	$25972, %eax
	movl	$1667592312, -40(%rbp)
	movw	%ax, -36(%rbp)
	movq	$15, -56(%rbp)
	movb	$0, -33(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3643
	call	_ZdlPv@PLT
.L3643:
	movq	$0, (%r12)
	jmp	.L3640
.L3650:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5467:
	.size	_ZN4node9inspector8protocol6Object9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol6Object9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12ErrorSupport4pushEv
	.type	_ZN4node9inspector8protocol12ErrorSupport4pushEv, @function
_ZN4node9inspector8protocol12ErrorSupport4pushEv:
.LFB5331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-48(%rbp), %rbx
	subq	$56, %rsp
	movq	8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movb	$0, -48(%rbp)
	cmpq	16(%rdi), %rsi
	je	.L3652
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-64(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L3658
	movq	%rax, (%rsi)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rsi)
.L3654:
	movq	-56(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 8(%rdi)
.L3651:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3659
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3652:
	.cfi_restore_state
	leaq	-64(%rbp), %rdx
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-64(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3651
	call	_ZdlPv@PLT
	jmp	.L3651
	.p2align 4,,10
	.p2align 3
.L3658:
	movdqa	-48(%rbp), %xmm0
	movups	%xmm0, 16(%rsi)
	jmp	.L3654
.L3659:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5331:
	.size	_ZN4node9inspector8protocol12ErrorSupport4pushEv, .-_ZN4node9inspector8protocol12ErrorSupport4pushEv
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_:
.LFB8266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	0(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L3661
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	48(%rbx), %rsi
.L3664:
	cmpq	%rsi, %r15
	je	.L3703
.L3662:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3661
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L3664
.L3661:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rsi
	movq	8(%r13), %rdx
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	8(%rax), %rdi
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	movq	$0, 40(%rbx)
	leaq	32(%r12), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L3665
	movq	(%r12), %r8
	movq	%r15, 48(%rbx)
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L3675
.L3706:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L3676:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3703:
	.cfi_restore_state
	movq	8(%r13), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L3662
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L3663
	movq	8(%rbx), %rsi
	movq	0(%r13), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L3662
.L3663:
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3665:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L3704
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L3705
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L3668:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L3670
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L3671
	.p2align 4,,10
	.p2align 3
.L3672:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3673:
	testq	%rsi, %rsi
	je	.L3670
.L3671:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L3672
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L3679
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L3671
	.p2align 4,,10
	.p2align 3
.L3670:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L3674
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L3674:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r8, (%r12)
	movq	%r15, 48(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L3706
.L3675:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L3677
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
.L3677:
	leaq	16(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L3676
	.p2align 4,,10
	.p2align 3
.L3679:
	movq	%rdx, %rdi
	jmp	.L3673
	.p2align 4,,10
	.p2align 3
.L3704:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L3668
.L3705:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8266:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movabsq	$288230376151711743, %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r14
	movq	%r13, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L3732
	movq	%r12, %rcx
	movq	%rdi, %r15
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L3723
	movabsq	$9223372036854775776, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3733
.L3709:
	movq	%rbx, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rax, %rbx
	movq	%rbx, -56(%rbp)
	leaq	32(%rax), %rbx
.L3722:
	leaq	(%rax,%rcx), %rdi
	movq	(%rdx), %rsi
	movq	%rax, -64(%rbp)
	leaq	16(%rdi), %rcx
	movq	8(%rdx), %rax
	movq	%rcx, (%rdi)
	addq	%rsi, %rax
	movq	%rax, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	cmpq	%r14, %r12
	movq	-64(%rbp), %rax
	je	.L3711
	movq	%rax, %rcx
	movq	%r14, %rdx
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3712:
	movq	%rsi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L3731:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	je	.L3734
.L3715:
	leaq	16(%rcx), %rsi
	leaq	16(%rdx), %rdi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rsi
	cmpq	%rdi, %rsi
	jne	.L3712
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L3731
	.p2align 4,,10
	.p2align 3
.L3734:
	movq	%r12, %rdx
	subq	%r14, %rdx
	leaq	32(%rax,%rdx), %rbx
.L3711:
	cmpq	%r13, %r12
	je	.L3716
	movq	%r12, %rdx
	movq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L3720:
	leaq	16(%rcx), %rsi
	leaq	16(%rdx), %rdi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rsi
	cmpq	%rdi, %rsi
	je	.L3735
	movq	%rsi, (%rcx)
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r13, %rdx
	jne	.L3720
.L3718:
	subq	%r12, %r13
	addq	%r13, %rbx
.L3716:
	testq	%r14, %r14
	je	.L3721
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
.L3721:
	movq	%rax, %xmm0
	movq	%rbx, %xmm3
	movq	-56(%rbp), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3735:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r13
	jne	.L3720
	jmp	.L3718
	.p2align 4,,10
	.p2align 3
.L3733:
	testq	%rdi, %rdi
	jne	.L3710
	movq	$0, -56(%rbp)
	movl	$32, %ebx
	xorl	%eax, %eax
	jmp	.L3722
	.p2align 4,,10
	.p2align 3
.L3723:
	movl	$32, %ebx
	jmp	.L3709
.L3710:
	cmpq	%rsi, %rdi
	movq	%rsi, %rbx
	cmovbe	%rdi, %rbx
	salq	$5, %rbx
	jmp	.L3709
.L3732:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8269:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue8setArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_9ListValueESt14default_deleteISC_EE
	.type	_ZN4node9inspector8protocol15DictionaryValue8setArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_9ListValueESt14default_deleteISC_EE, @function
_ZN4node9inspector8protocol15DictionaryValue8setArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_9ListValueESt14default_deleteISC_EE:
.LFB5390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$3339675911, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	24(%r13), %r8
	xorl	%edx, %edx
	movq	%rax, %r10
	divq	%r8
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3737
	movq	(%rax), %rbx
	movq	%rdx, %r9
	movq	48(%rbx), %rcx
.L3740:
	cmpq	%rcx, %r10
	je	.L3762
.L3738:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3737
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r9
	je	.L3740
.L3737:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3745
	movq	(%rdi), %rax
	call	*24(%rax)
.L3745:
	movq	80(%r13), %r8
	cmpq	88(%r13), %r8
	je	.L3763
	leaq	16(%r8), %rax
	movq	8(%r12), %rdx
	movq	%r8, %rdi
	movq	%rax, (%r8)
	movq	(%r12), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	addq	$32, 80(%r13)
.L3736:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3762:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L3738
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r10, -56(%rbp)
	testq	%rdx, %rdx
	je	.L3739
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	movq	-72(%rbp), %r9
	jne	.L3738
.L3739:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3736
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3763:
	.cfi_restore_state
	addq	$40, %rsp
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.cfi_endproc
.LFE5390:
	.size	_ZN4node9inspector8protocol15DictionaryValue8setArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_9ListValueESt14default_deleteISC_EE, .-_ZN4node9inspector8protocol15DictionaryValue8setArrayERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_9ListValueESt14default_deleteISC_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue9setObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrIS2_St14default_deleteIS2_EE
	.type	_ZN4node9inspector8protocol15DictionaryValue9setObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrIS2_St14default_deleteIS2_EE, @function
_ZN4node9inspector8protocol15DictionaryValue9setObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrIS2_St14default_deleteIS2_EE:
.LFB5389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$3339675911, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	24(%r13), %r8
	xorl	%edx, %edx
	movq	%rax, %r10
	divq	%r8
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3765
	movq	(%rax), %rbx
	movq	%rdx, %r9
	movq	48(%rbx), %rcx
.L3768:
	cmpq	%rcx, %r10
	je	.L3790
.L3766:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3765
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r9
	je	.L3768
.L3765:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3773
	movq	(%rdi), %rax
	call	*24(%rax)
.L3773:
	movq	80(%r13), %r8
	cmpq	88(%r13), %r8
	je	.L3791
	leaq	16(%r8), %rax
	movq	8(%r12), %rdx
	movq	%r8, %rdi
	movq	%rax, (%r8)
	movq	(%r12), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	addq	$32, 80(%r13)
.L3764:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3790:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L3766
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r10, -56(%rbp)
	testq	%rdx, %rdx
	je	.L3767
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	movq	-72(%rbp), %r9
	jne	.L3766
.L3767:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3764
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3791:
	.cfi_restore_state
	addq	$40, %rsp
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.cfi_endproc
.LFE5389:
	.size	_ZN4node9inspector8protocol15DictionaryValue9setObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrIS2_St14default_deleteIS2_EE, .-_ZN4node9inspector8protocol15DictionaryValue9setObjectERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrIS2_St14default_deleteIS2_EE
	.section	.text._ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE,"axG",@progbits,_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	.type	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE, @function
_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE:
.LFB6750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	$3339675911, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	24(%r13), %r8
	xorl	%edx, %edx
	movq	%rax, %r10
	divq	%r8
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3793
	movq	(%rax), %rbx
	movq	%rdx, %r9
	movq	48(%rbx), %rcx
.L3796:
	cmpq	%rcx, %r10
	je	.L3818
.L3794:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3793
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r9
	je	.L3796
.L3793:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3801
	movq	(%rdi), %rax
	call	*24(%rax)
.L3801:
	movq	80(%r13), %r8
	cmpq	88(%r13), %r8
	je	.L3819
	leaq	16(%r8), %rax
	movq	8(%r12), %rdx
	movq	%r8, %rdi
	movq	%rax, (%r8)
	movq	(%r12), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	addq	$32, 80(%r13)
.L3792:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3818:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L3794
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r10, -56(%rbp)
	testq	%rdx, %rdx
	je	.L3795
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	movq	-72(%rbp), %r9
	jne	.L3794
.L3795:
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%r14), %rdx
	movq	$0, (%r14)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L3792
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3819:
	.cfi_restore_state
	addq	$40, %rsp
	leaq	72(%r13), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.cfi_endproc
.LFE6750:
	.size	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE, .-_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE
	.type	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE, @function
_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE:
.LFB5388:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	.cfi_endproc
.LFE5388:
	.size	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE, .-_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue10setBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb
	.type	_ZN4node9inspector8protocol15DictionaryValue10setBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb, @function
_ZN4node9inspector8protocol15DictionaryValue10setBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb:
.LFB5384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$1, 8(%rax)
	movq	%rcx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3821
	movq	(%rdi), %rax
	call	*24(%rax)
.L3821:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3828
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3828:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5384:
	.size	_ZN4node9inspector8protocol15DictionaryValue10setBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb, .-_ZN4node9inspector8protocol15DictionaryValue10setBooleanERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue10setIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.type	_ZN4node9inspector8protocol15DictionaryValue10setIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, @function
_ZN4node9inspector8protocol15DictionaryValue10setIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi:
.LFB5385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$2, 8(%rax)
	movq	%rcx, (%rax)
	movl	%ebx, 16(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3829
	movq	(%rdi), %rax
	call	*24(%rax)
.L3829:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3836
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3836:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5385:
	.size	_ZN4node9inspector8protocol15DictionaryValue10setIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, .-_ZN4node9inspector8protocol15DictionaryValue10setIntegerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue9setDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd
	.type	_ZN4node9inspector8protocol15DictionaryValue9setDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd, @function
_ZN4node9inspector8protocol15DictionaryValue9setDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd:
.LFB5386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$24, %edi
	subq	$32, %rsp
	movsd	%xmm0, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movsd	-40(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$3, 8(%rax)
	leaq	-32(%rbp), %rdx
	movq	%rcx, (%rax)
	movsd	%xmm0, 16(%rax)
	movq	%rax, -32(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3837
	movq	(%rdi), %rax
	call	*24(%rax)
.L3837:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3844
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3844:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5386:
	.size	_ZN4node9inspector8protocol15DictionaryValue9setDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd, .-_ZN4node9inspector8protocol15DictionaryValue9setDoubleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEd
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.type	_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, @function
_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_:
.LFB5387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$48, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	movl	$4, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	addq	%rsi, %rdx
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rdi
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	%r13, %rdi
	leaq	-48(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, -48(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3845
	movq	(%rdi), %rax
	call	*24(%rax)
.L3845:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3852
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3852:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5387:
	.size	_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, .-_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv, @function
_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv:
.LFB5552:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$152, %rsp
	movq	%rdi, -120(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movl	8(%r12), %edx
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movl	$6, 8(%rax)
	movq	%rax, %r13
	addq	$64, %rax
	movq	%r15, -64(%rax)
	movl	%edx, -128(%rbp)
	movups	%xmm0, 64(%r13)
	movups	%xmm0, 80(%r13)
	movq	%rax, 16(%r13)
	movq	$1, 24(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
	movl	$0x3f800000, 48(%r13)
	movq	$0, 56(%r13)
	movq	%rbx, -96(%rbp)
	movl	$1701080931, -80(%rbp)
	movq	$4, -88(%rbp)
	movb	$0, -76(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rdx, (%rax)
	movl	-128(%rbp), %edx
	movl	$2, 8(%rax)
	movl	%edx, 16(%rax)
	movq	%rax, -104(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -144(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3854
	movq	(%rdi), %rax
	call	*24(%rax)
.L3854:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3855
	call	_ZdlPv@PLT
.L3855:
	movl	$26465, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rbx, -96(%rbp)
	movw	%dx, -76(%rbp)
	leaq	16(%r12), %rdx
	movl	$1936942445, -80(%rbp)
	movb	$101, -74(%rbp)
	movq	$7, -88(%rbp)
	movb	$0, -73(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3856
	call	_ZdlPv@PLT
.L3856:
	cmpq	$0, 56(%r12)
	jne	.L3905
.L3857:
	movl	$96, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%rbx, -96(%rbp)
	movq	%rax, %rcx
	movq	%r15, (%rax)
	leaq	16(%rax), %rax
	movl	$3339675911, %edx
	movl	$6, -8(%rax)
	movl	$5, %esi
	movq	%rax, -136(%rbp)
	leaq	64(%rcx), %rax
	movq	%rax, 16(%rcx)
	movq	-120(%rbp), %rax
	movq	$1, 24(%rcx)
	movq	$0, 32(%rcx)
	movq	$0, 40(%rcx)
	movl	$0x3f800000, 48(%rcx)
	movq	$0, 56(%rcx)
	movq	%rcx, (%rax)
	movups	%xmm0, 64(%rcx)
	movups	%xmm0, 80(%rcx)
	movq	%rcx, -128(%rbp)
	movl	$1869771365, -80(%rbp)
	movb	$114, -76(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movq	%rax, %r9
	movq	24(%rcx), %r8
	divq	%r8
	movq	16(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L3859
	movq	(%rax), %r15
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %rdi
	movq	48(%r15), %rsi
	testq	%r11, %r11
	je	.L3860
.L3863:
	cmpq	%rsi, %r9
	je	.L3906
.L3861:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L3859
	movq	48(%r15), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	je	.L3863
.L3859:
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rcx, -128(%rbp)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	-128(%rbp), %rcx
	movq	(%rax), %rdi
	movq	%r13, (%rax)
	testq	%rdi, %rdi
	je	.L3869
	movq	(%rdi), %rax
	movq	%rcx, -128(%rbp)
	call	*24(%rax)
	movq	-128(%rbp), %rcx
.L3869:
	movq	80(%rcx), %r8
	cmpq	88(%rcx), %r8
	je	.L3907
	leaq	16(%r8), %rax
	movq	-88(%rbp), %rdx
	movq	%r8, %rdi
	movq	%rcx, -128(%rbp)
	movq	%rax, (%r8)
	movq	-96(%rbp), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-128(%rbp), %rcx
	addq	$32, 80(%rcx)
	jmp	.L3865
	.p2align 4,,10
	.p2align 3
.L3860:
	cmpq	%rsi, %r9
	je	.L3908
	.p2align 4,,10
	.p2align 3
.L3864:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L3859
	movq	48(%r15), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L3859
	cmpq	%rsi, %r9
	jne	.L3864
.L3908:
	cmpq	$0, 16(%r15)
	jne	.L3864
.L3862:
	movq	-136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%rax), %rdi
	movq	%r13, (%rax)
	testq	%rdi, %rdi
	je	.L3865
	movq	(%rdi), %rax
	call	*24(%rax)
.L3865:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3871
	call	_ZdlPv@PLT
.L3871:
	cmpb	$0, 84(%r12)
	jne	.L3909
.L3853:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3910
	movq	-120(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3906:
	.cfi_restore_state
	cmpq	%r11, 16(%r15)
	jne	.L3861
	movq	8(%r15), %rsi
	movq	%r11, %rdx
	movq	%r10, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%r11, -152(%rbp)
	movq	%rdi, -128(%rbp)
	call	memcmp@PLT
	movq	-128(%rbp), %rdi
	movq	-152(%rbp), %r11
	testl	%eax, %eax
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %r9
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %r10
	je	.L3862
	jmp	.L3861
	.p2align 4,,10
	.p2align 3
.L3905:
	movq	%r13, %rdi
	leaq	48(%r12), %rdx
	movq	%r14, %rsi
	movq	%rbx, -96(%rbp)
	movl	$1635017060, -80(%rbp)
	movq	$4, -88(%rbp)
	movb	$0, -76(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3857
	call	_ZdlPv@PLT
	jmp	.L3857
	.p2align 4,,10
	.p2align 3
.L3909:
	movq	-120(%rbp), %rax
	movl	80(%r12), %r12d
	movq	%rbx, -96(%rbp)
	movl	$24, %edi
	movq	$2, -88(%rbp)
	movq	(%rax), %r15
	movl	$25705, %eax
	movb	$0, -78(%rbp)
	movw	%ax, -80(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rsi
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rsi, (%rax)
	movq	%r14, %rsi
	movl	$2, 8(%rax)
	movl	%r12d, 16(%rax)
	movq	%rax, -104(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3873
	movq	(%rdi), %rax
	call	*24(%rax)
.L3873:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3853
	call	_ZdlPv@PLT
	jmp	.L3853
	.p2align 4,,10
	.p2align 3
.L3907:
	leaq	72(%rcx), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L3865
.L3910:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5552:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv, .-_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv, @function
_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv:
.LFB5544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv
	movq	-32(%rbp), %rdi
	leaq	_ZN4node9inspector8protocol5Value17serializeToBinaryEv(%rip), %rcx
	movq	(%rdi), %rax
	movq	8(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L3912
	movq	$0, 16(%r12)
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movups	%xmm0, (%r12)
	call	*80(%rax)
.L3913:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3911
	movq	(%rdi), %rax
	call	*24(%rax)
.L3911:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3920
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3912:
	.cfi_restore_state
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3913
.L3920:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5544:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv, .-_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv, @function
_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv:
.LFB5543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError9serializeEv
	movq	-32(%rbp), %rsi
	leaq	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	(%rsi), %rax
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3922
	call	_ZNK4node9inspector8protocol5Value12toJSONStringB5cxx11Ev
.L3923:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3921
	movq	(%rdi), %rax
	call	*24(%rax)
.L3921:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3930
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3922:
	.cfi_restore_state
	call	*%rax
	jmp	.L3923
.L3930:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5543:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv, .-_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16InternalResponse15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol16InternalResponse15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol16InternalResponse15serializeToJSONB5cxx11Ev:
.LFB5617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	-488(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movl	$6, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rax
	movq	48(%rsi), %r13
	movq	%rax, (%r14)
	leaq	64(%r14), %rax
	movq	%rax, 16(%r14)
	movq	$1, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movl	$0x3f800000, 48(%r14)
	movq	$0, 56(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	testq	%r13, %r13
	je	.L3932
	movq	$0, 48(%rsi)
	leaq	-472(%rbp), %r15
.L3933:
	cmpq	$0, 24(%rsi)
	je	.L3934
	leaq	-432(%rbp), %rax
	movl	$25711, %edi
	leaq	16(%rsi), %rdx
	movl	$1752458605, -416(%rbp)
	movw	%di, -412(%rbp)
	movq	%rax, %rsi
	movq	%r14, %rdi
	leaq	-416(%rbp), %rbx
	movq	%rax, -488(%rbp)
	movq	%rbx, -432(%rbp)
	movq	$6, -424(%rbp)
	movb	$0, -410(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3935
	call	_ZdlPv@PLT
.L3935:
	movq	0(%r13), %rax
	movq	%r13, %rsi
	leaq	-464(%rbp), %rdi
	call	*(%rax)
	movl	$72, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol15SerializedValueE(%rip), %rcx
	movq	-464(%rbp), %rsi
	leaq	32(%rax), %rdx
	movl	$8, 8(%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	-456(%rbp), %rdx
	movq	%rcx, (%rax)
	addq	%rsi, %rdx
	movq	%rax, -512(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-512(%rbp), %rax
	movl	$29549, %esi
	movq	%rbx, -432(%rbp)
	movl	$1634886000, -416(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	movw	%si, -412(%rbp)
	movups	%xmm0, 48(%rax)
.L3972:
	movq	-488(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	$6, -424(%rbp)
	movb	$0, -410(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3942
	call	_ZdlPv@PLT
.L3942:
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3943
	movq	(%rdi), %rax
	call	*24(%rax)
.L3943:
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3939
	call	_ZdlPv@PLT
.L3939:
	movq	(%r14), %rax
	leaq	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev(%rip), %rdx
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3945
	movq	.LC32(%rip), %xmm1
	leaq	-320(%rbp), %r15
	leaq	-368(%rbp), %rbx
	movq	%r15, %rdi
	movhps	.LC33(%rip), %xmm1
	movaps	%xmm1, -512(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	-488(%rbp), %rdi
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-512(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%r14), %rax
	movq	-488(%rbp), %rsi
	movq	%r14, %rdi
	call	*72(%rax)
	leaq	16(%r12), %rax
	movb	$0, 16(%r12)
	movq	%rax, (%r12)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r12)
	testq	%rax, %rax
	je	.L3946
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L3973
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L3948:
	movq	.LC32(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC34(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L3949
	call	_ZdlPv@PLT
.L3949:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r15, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L3950:
	testq	%r13, %r13
	je	.L3951
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
.L3951:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3974
	addq	$472, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3934:
	.cfi_restore_state
	movl	8(%rsi), %edx
	movl	$25705, %ecx
	leaq	-432(%rbp), %rax
	movl	$24, %edi
	leaq	-416(%rbp), %rbx
	movw	%cx, -416(%rbp)
	movl	%edx, -512(%rbp)
	movq	%rax, -488(%rbp)
	movq	%rbx, -432(%rbp)
	movq	$2, -424(%rbp)
	movb	$0, -414(%rbp)
	call	_Znwm@PLT
	movl	-512(%rbp), %edx
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r14, %rdi
	movl	$2, 8(%rax)
	movq	-488(%rbp), %rsi
	movl	%edx, 16(%rax)
	movq	%r15, %rdx
	movq	%rcx, (%rax)
	movq	%rax, -472(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3940
	movq	(%rdi), %rax
	call	*24(%rax)
.L3940:
	movq	-432(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3941
	call	_ZdlPv@PLT
.L3941:
	movq	0(%r13), %rax
	movq	%r13, %rsi
	leaq	-464(%rbp), %rdi
	call	*(%rax)
	movl	$72, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol15SerializedValueE(%rip), %rcx
	movq	-464(%rbp), %rsi
	leaq	32(%rax), %rdx
	movl	$8, 8(%rax)
	leaq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	movq	-456(%rbp), %rdx
	movq	%rcx, (%rax)
	addq	%rsi, %rdx
	movq	%rax, -512(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-512(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$29804, %edx
	movq	%rbx, -432(%rbp)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	movl	$1970496882, -416(%rbp)
	movw	%dx, -412(%rbp)
	movups	%xmm0, 48(%rax)
	jmp	.L3972
	.p2align 4,,10
	.p2align 3
.L3973:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L3948
	.p2align 4,,10
	.p2align 3
.L3945:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3950
	.p2align 4,,10
	.p2align 3
.L3932:
	leaq	-472(%rbp), %r15
	movq	%rsi, -488(%rbp)
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol15DictionaryValue6createEv
	movq	-472(%rbp), %r13
	movq	-488(%rbp), %rsi
	jmp	.L3933
	.p2align 4,,10
	.p2align 3
.L3946:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L3948
.L3974:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5617:
	.size	_ZN4node9inspector8protocol16InternalResponse15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol16InternalResponse15serializeToJSONB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol16InternalResponse17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol16InternalResponse17serializeToBinaryEv, @function
_ZN4node9inspector8protocol16InternalResponse17serializeToBinaryEv:
.LFB5618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	-152(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	movl	$6, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rax
	movq	48(%rsi), %r13
	movq	%rax, (%r14)
	leaq	64(%r14), %rax
	movq	%rax, 16(%r14)
	movq	$1, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	movl	$0x3f800000, 48(%r14)
	movq	$0, 56(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	testq	%r13, %r13
	je	.L3976
	movq	$0, 48(%rsi)
	leaq	-128(%rbp), %r15
.L3977:
	cmpq	$0, 24(%rsi)
	je	.L3978
	leaq	-96(%rbp), %rax
	movl	$25711, %edi
	leaq	16(%rsi), %rdx
	movl	$1752458605, -80(%rbp)
	movw	%di, -76(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, -152(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue9setStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3979
	call	_ZdlPv@PLT
.L3979:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	*8(%rax)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-112(%rbp), %r15
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol15SerializedValueE(%rip), %rcx
	movq	%rbx, -96(%rbp)
	movq	-160(%rbp), %xmm0
	movq	%rcx, (%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, 16(%rax)
	movl	$29549, %ecx
	movhps	-168(%rbp), %xmm0
	movl	$8, 8(%rax)
	movq	$0, 24(%rax)
	movb	$0, 32(%rax)
	movq	%r15, 64(%rax)
	movq	%rax, -136(%rbp)
	movl	$1634886000, -80(%rbp)
	movw	%cx, -76(%rbp)
	movups	%xmm0, 48(%rax)
.L4018:
	movq	-152(%rbp), %rsi
	movq	%r14, %rdi
	movb	$0, -74(%rbp)
	leaq	-136(%rbp), %rdx
	movq	$6, -88(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3986
	call	_ZdlPv@PLT
.L3986:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3987
	movq	(%rdi), %rax
	call	*24(%rax)
.L3987:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3983
	call	_ZdlPv@PLT
.L3983:
	movq	(%r14), %rax
	leaq	_ZN4node9inspector8protocol5Value17serializeToBinaryEv(%rip), %rcx
	movq	8(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L3989
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
	call	*80(%rax)
.L3990:
	testq	%r13, %r13
	je	.L3991
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
.L3991:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4019
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3978:
	.cfi_restore_state
	movl	8(%rsi), %edx
	leaq	-96(%rbp), %rax
	movl	$24, %edi
	leaq	-80(%rbp), %rbx
	movq	%rax, -152(%rbp)
	movl	%edx, -160(%rbp)
	movl	$25705, %edx
	movw	%dx, -80(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$2, -88(%rbp)
	movb	$0, -78(%rbp)
	call	_Znwm@PLT
	movl	-160(%rbp), %edx
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rcx
	movq	%r14, %rdi
	movl	$2, 8(%rax)
	movq	-152(%rbp), %rsi
	movl	%edx, 16(%rax)
	movq	%r15, %rdx
	movq	%rcx, (%rax)
	movq	%rax, -128(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3984
	movq	(%rdi), %rax
	call	*24(%rax)
.L3984:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3985
	call	_ZdlPv@PLT
.L3985:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	*8(%rax)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-112(%rbp), %r15
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol15SerializedValueE(%rip), %rcx
	movq	%rbx, -96(%rbp)
	movq	-160(%rbp), %xmm0
	movq	%rcx, (%rax)
	leaq	32(%rax), %rcx
	movhps	-168(%rbp), %xmm0
	movl	$8, 8(%rax)
	movq	%rcx, 16(%rax)
	movq	$0, 24(%rax)
	movb	$0, 32(%rax)
	movq	%r15, 64(%rax)
	movq	%rax, -136(%rbp)
	movups	%xmm0, 48(%rax)
	movl	$29804, %eax
	movl	$1970496882, -80(%rbp)
	movw	%ax, -76(%rbp)
	jmp	.L4018
	.p2align 4,,10
	.p2align 3
.L3989:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3990
	.p2align 4,,10
	.p2align 3
.L3976:
	leaq	-128(%rbp), %r15
	movq	%rsi, -152(%rbp)
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol15DictionaryValue6createEv
	movq	-128(%rbp), %r13
	movq	-152(%rbp), %rsi
	jmp	.L3977
.L4019:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5618:
	.size	_ZN4node9inspector8protocol16InternalResponse17serializeToBinaryEv, .-_ZN4node9inspector8protocol16InternalResponse17serializeToBinaryEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue5cloneEv
	.type	_ZNK4node9inspector8protocol15DictionaryValue5cloneEv, @function
_ZNK4node9inspector8protocol15DictionaryValue5cloneEv:
.LFB5418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -184(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	leaq	-80(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	movl	$6, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rax, (%r12)
	leaq	64(%r12), %rax
	movq	%rcx, -120(%rbp)
	leaq	-104(%rbp), %rcx
	movq	%rax, 16(%r12)
	movq	72(%r14), %rax
	movq	$1, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movl	$0x3f800000, 48(%r12)
	movq	$0, 56(%r12)
	movq	%rcx, -136(%rbp)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	cmpq	80(%r14), %rax
	je	.L4042
	.p2align 4,,10
	.p2align 3
.L4021:
	movq	%r13, %rdx
	movq	-120(%rbp), %rcx
	salq	$5, %rdx
	addq	%rdx, %rax
	movq	%rcx, -96(%rbp)
	movq	(%rax), %r8
	movq	8(%rax), %rbx
	movq	%r8, %rax
	addq	%rbx, %rax
	je	.L4022
	testq	%r8, %r8
	je	.L4081
.L4022:
	movq	%rbx, -104(%rbp)
	cmpq	$15, %rbx
	ja	.L4082
	cmpq	$1, %rbx
	jne	.L4025
	movzbl	(%r8), %eax
	leaq	-96(%rbp), %r15
	movb	%al, -80(%rbp)
	movq	-120(%rbp), %rax
.L4026:
	movq	%rbx, -88(%rbp)
	leaq	16(%r14), %rdi
	movq	%r15, %rsi
	movb	$0, (%rax,%rbx)
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	movq	-136(%rbp), %rdi
	movq	40(%rax), %rsi
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	leaq	16(%r12), %r10
	movl	$3339675911, %edx
	movq	%r10, -128(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	24(%r12), %rcx
	xorl	%edx, %edx
	movq	-128(%rbp), %r10
	movq	%rax, %r8
	divq	%rcx
	movq	16(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L4027
	movq	(%rax), %rbx
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %rdi
	movq	48(%rbx), %rsi
	testq	%r11, %r11
	je	.L4028
.L4031:
	cmpq	%rsi, %r8
	je	.L4083
.L4029:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4027
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	je	.L4031
.L4027:
	movq	%r10, %rdi
	movq	%r15, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	-104(%rbp), %rdx
	movq	$0, -104(%rbp)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L4037
	movq	(%rdi), %rax
	call	*24(%rax)
.L4037:
	movq	80(%r12), %rdi
	cmpq	88(%r12), %rdi
	je	.L4084
	leaq	16(%rdi), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, (%rdi)
	movq	-96(%rbp), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	addq	$32, 80(%r12)
	.p2align 4,,10
	.p2align 3
.L4033:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4039
	movq	(%rdi), %rax
	call	*24(%rax)
.L4039:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L4040
	call	_ZdlPv@PLT
	movq	72(%r14), %rax
	movq	80(%r14), %rdx
	addq	$1, %r13
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %r13
	jb	.L4021
.L4042:
	movq	-184(%rbp), %rax
	movq	%r12, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4085
	movq	-184(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4028:
	.cfi_restore_state
	cmpq	%rsi, %r8
	je	.L4086
	.p2align 4,,10
	.p2align 3
.L4032:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4027
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L4027
	cmpq	%rsi, %r8
	jne	.L4032
.L4086:
	cmpq	$0, 16(%rbx)
	jne	.L4032
	jmp	.L4030
	.p2align 4,,10
	.p2align 3
.L4083:
	cmpq	16(%rbx), %r11
	jne	.L4029
	movq	8(%rbx), %rsi
	movq	%r11, %rdx
	movq	%r9, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%rdi, -128(%rbp)
	call	memcmp@PLT
	movq	-128(%rbp), %rdi
	movq	-144(%rbp), %r11
	testl	%eax, %eax
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r9
	jne	.L4029
.L4030:
	movq	%r10, %rdi
	movq	%r15, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	-104(%rbp), %rdx
	movq	$0, -104(%rbp)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L4033
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4033
	.p2align 4,,10
	.p2align 3
.L4040:
	movq	72(%r14), %rax
	movq	80(%r14), %rdx
	addq	$1, %r13
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%r13, %rdx
	ja	.L4021
	jmp	.L4042
	.p2align 4,,10
	.p2align 3
.L4025:
	testq	%rbx, %rbx
	jne	.L4087
	movq	-120(%rbp), %rax
	leaq	-96(%rbp), %r15
	jmp	.L4026
	.p2align 4,,10
	.p2align 3
.L4082:
	movq	-136(%rbp), %rsi
	leaq	-96(%rbp), %r15
	xorl	%edx, %edx
	movq	%r8, -128(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L4024:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rbx
	movq	-96(%rbp), %rax
	jmp	.L4026
	.p2align 4,,10
	.p2align 3
.L4084:
	leaq	72(%r12), %r8
	movq	%rdi, %rsi
	movq	%r15, %rdx
	movq	%r8, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L4033
.L4081:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4087:
	movq	-120(%rbp), %rdi
	leaq	-96(%rbp), %r15
	jmp	.L4024
.L4085:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5418:
	.size	_ZNK4node9inspector8protocol15DictionaryValue5cloneEv, .-_ZNK4node9inspector8protocol15DictionaryValue5cloneEv
	.section	.text._ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB8313:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L4112
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L4103
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4113
.L4090:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L4102:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L4092
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L4096:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4093
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*24(%rcx)
	cmpq	%r14, %r15
	jne	.L4096
.L4094:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L4092:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L4097
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L4105
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L4099:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L4099
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L4100
.L4098:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L4100:
	leaq	8(%rcx,%r9), %rcx
.L4097:
	testq	%r12, %r12
	je	.L4101
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L4101:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4093:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L4096
	jmp	.L4094
	.p2align 4,,10
	.p2align 3
.L4113:
	testq	%rdi, %rdi
	jne	.L4091
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L4102
	.p2align 4,,10
	.p2align 3
.L4103:
	movl	$8, %r13d
	jmp	.L4090
.L4105:
	movq	%rcx, %rdx
	jmp	.L4098
.L4091:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L4090
.L4112:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8313:
	.size	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol9ListValue5cloneEv
	.type	_ZNK4node9inspector8protocol9ListValue5cloneEv, @function
_ZNK4node9inspector8protocol9ListValue5cloneEv:
.LFB5447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$40, %edi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	24(%r12), %r13
	movq	16(%r12), %r15
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	movl	$7, 8(%rax)
	leaq	-64(%rbp), %r12
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rax
	movq	$0, 32(%rbx)
	movq	%rax, -72(%rbp)
	movups	%xmm0, 16(%rbx)
	cmpq	%r13, %r15
	je	.L4120
	.p2align 4,,10
	.p2align 3
.L4121:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rdx
	call	*88(%rdx)
	movq	24(%rbx), %rsi
	cmpq	32(%rbx), %rsi
	je	.L4116
	movq	-64(%rbp), %rdx
	addq	$8, %rsi
	movq	$0, -64(%rbp)
	movq	%rdx, -8(%rsi)
	movq	%rsi, 24(%rbx)
.L4117:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4118
	movq	(%rdi), %rdx
	addq	$8, %r15
	call	*24(%rdx)
	cmpq	%r15, %r13
	jne	.L4121
.L4120:
	movq	%rbx, (%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4126
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4118:
	.cfi_restore_state
	addq	$8, %r15
	cmpq	%r15, %r13
	jne	.L4121
	jmp	.L4120
	.p2align 4,,10
	.p2align 3
.L4116:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4117
.L4126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5447:
	.size	_ZNK4node9inspector8protocol9ListValue5cloneEv, .-_ZNK4node9inspector8protocol9ListValue5cloneEv
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_110parseValueEiPNS1_4cbor13CBORTokenizerE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_110parseValueEiPNS1_4cbor13CBORTokenizerE:
.LFB5342:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1001, %esi
	je	.L4131
	movl	16(%rdx), %eax
	movl	%esi, %ebx
	movq	%rdx, %r12
	cmpl	$12, %eax
	je	.L4200
.L4130:
	cmpl	$13, %eax
	ja	.L4131
	leaq	.L4133(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4133:
	.long	.L4131-.L4133
	.long	.L4143-.L4133
	.long	.L4142-.L4133
	.long	.L4141-.L4133
	.long	.L4140-.L4133
	.long	.L4139-.L4133
	.long	.L4138-.L4133
	.long	.L4137-.L4133
	.long	.L4136-.L4133
	.long	.L4135-.L4133
	.long	.L4134-.L4133
	.long	.L4131-.L4133
	.long	.L4131-.L4133
	.long	.L4131-.L4133
	.text
	.p2align 4,,10
	.p2align 3
.L4131:
	movq	$0, (%r15)
.L4127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4201
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4200:
	.cfi_restore_state
	movl	$1, %esi
	movq	%rdx, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
	jmp	.L4130
	.p2align 4,,10
	.p2align 3
.L4135:
	leaq	-104(%rbp), %rdi
	leal	1(%rbx), %esi
	movq	%r12, %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_18parseMapEiPNS1_4cbor13CBORTokenizerE
	movq	-104(%rbp), %rax
	movq	%rax, (%r15)
	jmp	.L4127
	.p2align 4,,10
	.p2align 3
.L4134:
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r14
	addl	$1, %ebx
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	$40, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$7, 8(%rax)
	movq	%rax, %r13
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, 0(%r13)
	movl	16(%r12), %eax
	movq	$0, 32(%r13)
	movups	%xmm0, 16(%r13)
	cmpl	$11, %eax
	je	.L4164
	.p2align 4,,10
	.p2align 3
.L4156:
	cmpl	$13, %eax
	je	.L4158
	testl	%eax, %eax
	je	.L4158
	movq	%r12, %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseValueEiPNS1_4cbor13CBORTokenizerE
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L4158
	movq	$0, -112(%rbp)
	movq	24(%r13), %rsi
	movq	%rax, -104(%rbp)
	cmpq	32(%r13), %rsi
	je	.L4159
	movq	$0, -104(%rbp)
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 24(%r13)
.L4160:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4161
	movq	(%rdi), %rax
	call	*24(%rax)
.L4161:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4162
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	16(%r12), %eax
	cmpl	$11, %eax
	jne	.L4156
.L4164:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
.L4157:
	movq	%r13, (%r15)
	jmp	.L4127
	.p2align 4,,10
	.p2align 3
.L4143:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movl	16(%r12), %eax
	movb	$1, 16(%rbx)
	cmpl	$13, %eax
	je	.L4154
	testl	%eax, %eax
	je	.L4154
	.p2align 4,,10
	.p2align 3
.L4195:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
.L4154:
	movq	%rbx, (%r15)
	jmp	.L4127
	.p2align 4,,10
	.p2align 3
.L4142:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movl	16(%r12), %eax
	movb	$0, 16(%rbx)
	testl	%eax, %eax
	je	.L4154
	.p2align 4,,10
	.p2align 3
.L4196:
	cmpl	$13, %eax
	jne	.L4195
	jmp	.L4154
	.p2align 4,,10
	.p2align 3
.L4141:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol5ValueE(%rip), %rax
	movq	%rax, (%rbx)
	movl	16(%r12), %eax
	movl	$0, 8(%rbx)
	testl	%eax, %eax
	jne	.L4196
	jmp	.L4154
	.p2align 4,,10
	.p2align 3
.L4140:
	movq	56(%r12), %rax
	movl	%eax, %r13d
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L4148
	notl	%r13d
.L4148:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$2, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movl	16(%r12), %eax
	movl	%r13d, 16(%rbx)
	testl	%eax, %eax
	jne	.L4196
	jmp	.L4154
	.p2align 4,,10
	.p2align 3
.L4139:
	movq	%r12, %rdi
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetDoubleEv
	movl	$24, %edi
	movsd	%xmm0, -120(%rbp)
	call	_Znwm@PLT
	movsd	-120(%rbp), %xmm0
	movl	$3, 8(%rax)
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rax
	movq	%rax, (%rbx)
	movl	16(%r12), %eax
	movsd	%xmm0, 16(%rbx)
	testl	%eax, %eax
	jne	.L4196
	jmp	.L4154
	.p2align 4,,10
	.p2align 3
.L4138:
	movq	56(%r12), %rdx
	movq	40(%r12), %rsi
	leaq	-96(%rbp), %rdi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	_ZN4node9inspector8protocol10StringUtil8fromUTF8B5cxx11EPKhm@PLT
.L4199:
	movl	$48, %edi
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rax
	leaq	16(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-96(%rbp), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4153
	call	_ZdlPv@PLT
.L4153:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4196
	jmp	.L4154
	.p2align 4,,10
	.p2align 3
.L4137:
	movq	56(%r12), %rax
	movq	40(%r12), %rsi
	leaq	-96(%rbp), %rdi
	addq	32(%r12), %rsi
	movq	%rax, %rdx
	subq	%rax, %rsi
	addq	(%r12), %rsi
	shrq	%rdx
	call	_ZN4node9inspector8protocol10StringUtil9fromUTF16B5cxx11EPKtm@PLT
	jmp	.L4199
.L4136:
	movq	%r12, %rdi
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer9GetBinaryEv
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L4155
	cmpl	$13, %eax
	je	.L4155
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
.L4155:
	leaq	_ZZN4node9inspector8protocol6Binary8fromSpanEPKhmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4162:
	movl	16(%r12), %eax
	cmpl	$11, %eax
	jne	.L4156
	jmp	.L4164
	.p2align 4,,10
	.p2align 3
.L4159:
	leaq	-104(%rbp), %rdx
	leaq	16(%r13), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4160
	.p2align 4,,10
	.p2align 3
.L4158:
	movq	0(%r13), %rax
	leaq	_ZN4node9inspector8protocol9ListValueD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4202
	movq	24(%r13), %rbx
	movq	16(%r13), %r12
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rax
	movq	%rax, 0(%r13)
	cmpq	%r12, %rbx
	je	.L4165
	.p2align 4,,10
	.p2align 3
.L4169:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4166
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L4169
.L4167:
	movq	16(%r13), %r12
.L4165:
	testq	%r12, %r12
	je	.L4170
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4170:
	movq	%r13, %rdi
	movl	$40, %esi
	xorl	%r13d, %r13d
	call	_ZdlPvm@PLT
	jmp	.L4157
	.p2align 4,,10
	.p2align 3
.L4166:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L4169
	jmp	.L4167
.L4202:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
	jmp	.L4157
.L4201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5342:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_110parseValueEiPNS1_4cbor13CBORTokenizerE, .-_ZN4node9inspector8protocol12_GLOBAL__N_110parseValueEiPNS1_4cbor13CBORTokenizerE
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_18parseMapEiPNS1_4cbor13CBORTokenizerE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_18parseMapEiPNS1_4cbor13CBORTokenizerE:
.LFB5343:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -208(%rbp)
	movl	$96, %edi
	movl	%esi, -156(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	movl	$6, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol15DictionaryValueE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	64(%r13), %rax
	movq	%rax, 16(%r13)
	movl	16(%r12), %eax
	movq	$1, 24(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
	movl	$0x3f800000, 48(%r13)
	movq	$0, 56(%r13)
	movups	%xmm0, 64(%r13)
	movups	%xmm0, 80(%r13)
	cmpl	$13, %eax
	je	.L4204
	testl	%eax, %eax
	je	.L4204
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
.L4204:
	cmpl	$11, %eax
	je	.L4205
	leaq	-112(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L4238:
	cmpl	$13, %eax
	je	.L4280
.L4206:
	testl	%eax, %eax
	je	.L4280
	movq	%r15, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	cmpl	$6, %eax
	jne	.L4280
	movq	56(%r12), %rdx
	movq	40(%r12), %rsi
	leaq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	_ZN4node9inspector8protocol10StringUtil8fromUTF8B5cxx11EPKhm@PLT
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rax
	je	.L4283
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	cmpq	%r15, %rdi
	je	.L4284
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	-112(%rbp), %rsi
	movq	%rax, -128(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -120(%rbp)
	testq	%rdi, %rdi
	je	.L4215
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L4213:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4216
	call	_ZdlPv@PLT
.L4216:
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L4217
	cmpl	$13, %eax
	je	.L4217
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
.L4217:
	movl	-156(%rbp), %esi
	leaq	-136(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseValueEiPNS1_4cbor13CBORTokenizerE
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L4285
	movq	-120(%rbp), %rsi
	movq	-128(%rbp), %rdi
	leaq	16(%r13), %r9
	movl	$3339675911, %edx
	movq	%r9, -152(%rbp)
	movq	$0, -136(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	24(%r13), %rcx
	xorl	%edx, %edx
	movq	-152(%rbp), %r9
	movq	%rax, %r8
	divq	%rcx
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L4223
	movq	(%rax), %rbx
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %rdi
	movq	48(%rbx), %rsi
	testq	%r11, %r11
	je	.L4224
.L4227:
	cmpq	%rsi, %r8
	je	.L4286
.L4225:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4223
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L4227
.L4223:
	leaq	-128(%rbp), %rbx
	movq	%r9, %rdi
	movq	%rbx, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%rax), %rdi
	movq	%r14, (%rax)
	testq	%rdi, %rdi
	je	.L4233
	movq	(%rdi), %rax
	call	*24(%rax)
.L4233:
	movq	80(%r13), %rdi
	cmpq	88(%r13), %rdi
	je	.L4287
	leaq	16(%rdi), %rax
	movq	-120(%rbp), %rdx
	movq	%rax, (%rdi)
	movq	-128(%rbp), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	addq	$32, 80(%r13)
	.p2align 4,,10
	.p2align 3
.L4229:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4235
	movq	(%rdi), %rax
	call	*24(%rax)
.L4235:
	movq	-128(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4236
	call	_ZdlPv@PLT
	movl	16(%r12), %eax
	cmpl	$11, %eax
	jne	.L4238
.L4205:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movq	-208(%rbp), %rax
	movq	%r13, (%rax)
	jmp	.L4203
	.p2align 4,,10
	.p2align 3
.L4224:
	cmpq	%rsi, %r8
	je	.L4288
	.p2align 4,,10
	.p2align 3
.L4228:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4223
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L4223
	cmpq	%rsi, %r8
	jne	.L4228
.L4288:
	cmpq	$0, 16(%rbx)
	jne	.L4228
	jmp	.L4226
	.p2align 4,,10
	.p2align 3
.L4286:
	cmpq	16(%rbx), %r11
	jne	.L4225
	movq	8(%rbx), %rsi
	movq	%r11, %rdx
	movq	%r10, -200(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%r9, -176(%rbp)
	movq	%r11, -168(%rbp)
	movq	%rdi, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %rdi
	movq	-168(%rbp), %r11
	testl	%eax, %eax
	movq	-176(%rbp), %r9
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %rcx
	movq	-200(%rbp), %r10
	jne	.L4225
.L4226:
	movq	%r9, %rdi
	leaq	-128(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISD_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%rax), %rdi
	movq	%r14, (%rax)
	testq	%rdi, %rdi
	je	.L4229
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4229
	.p2align 4,,10
	.p2align 3
.L4236:
	movl	16(%r12), %eax
	cmpl	$11, %eax
	je	.L4205
	cmpl	$13, %eax
	jne	.L4206
	.p2align 4,,10
	.p2align 3
.L4280:
	movq	-208(%rbp), %rax
	movq	$0, (%rax)
.L4207:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
.L4203:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4289
	movq	-208(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4283:
	.cfi_restore_state
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L4211
	cmpq	$1, %rdx
	je	.L4290
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rdi
.L4211:
	movq	%rdx, -120(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4284:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	%rax, -128(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -120(%rbp)
.L4215:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4287:
	leaq	72(%r13), %r8
	movq	%rdi, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L4229
	.p2align 4,,10
	.p2align 3
.L4290:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rdi
	jmp	.L4211
	.p2align 4,,10
	.p2align 3
.L4285:
	movq	-208(%rbp), %rax
	movq	-128(%rbp), %rdi
	movq	$0, (%rax)
	cmpq	%r15, %rdi
	je	.L4207
	call	_ZdlPv@PLT
	jmp	.L4207
.L4289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5343:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_18parseMapEiPNS1_4cbor13CBORTokenizerE, .-_ZN4node9inspector8protocol12_GLOBAL__N_18parseMapEiPNS1_4cbor13CBORTokenizerE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol5Value11parseBinaryEPKhm
	.type	_ZN4node9inspector8protocol5Value11parseBinaryEPKhm, @function
_ZN4node9inspector8protocol5Value11parseBinaryEPKhm:
.LFB5347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$120, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rcx
	movq	%rcx, -24(%rbp)
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.L4304
	cmpb	$-40, (%rsi)
	je	.L4294
.L4304:
	movq	$0, (%rax)
.L4291:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4305
	addq	$120, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4294:
	.cfi_restore_state
	leaq	-96(%rbp), %r12
	movq	%rdi, -120(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rdx, -88(%rbp)
	movl	$0, -72(%rbp)
	movq	$-1, -64(%rbp)
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-80(%rbp), %edx
	movq	-120(%rbp), %rax
	testl	%edx, %edx
	je	.L4304
	cmpl	$12, %edx
	jne	.L4306
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	cmpl	$9, -80(%rbp)
	movq	-120(%rbp), %rax
	jne	.L4304
	leaq	-104(%rbp), %rdi
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rax, -120(%rbp)
	call	_ZN4node9inspector8protocol12_GLOBAL__N_18parseMapEiPNS1_4cbor13CBORTokenizerE
	movq	-104(%rbp), %rdi
	movq	-120(%rbp), %rax
	testq	%rdi, %rdi
	je	.L4304
	movl	-80(%rbp), %edx
	cmpl	$13, %edx
	je	.L4307
	movq	(%rdi), %rdx
	movq	$0, (%rax)
	movq	%rax, -120(%rbp)
	call	*24(%rdx)
	movq	-120(%rbp), %rax
	jmp	.L4291
.L4307:
	movq	%rdi, (%rax)
	jmp	.L4291
.L4305:
	call	__stack_chk_fail@PLT
.L4306:
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13EnterEnvelopeEv.part.0
	.cfi_endproc
.LFE5347:
	.size	_ZN4node9inspector8protocol5Value11parseBinaryEPKhm, .-_ZN4node9inspector8protocol5Value11parseBinaryEPKhm
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE
	.type	_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE, @function
_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE:
.LFB5461:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L4309
	movq	(%rdx), %rax
	addq	$8, %rsi
	movq	$0, (%rdx)
	movq	%rax, -8(%rsi)
	movq	%rsi, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4309:
	addq	$16, %rdi
	jmp	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.cfi_endproc
.LFE5461:
	.size	_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE, .-_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE
	.section	.text._ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_,"axG",@progbits,_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
	.type	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_, @function
_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_:
.LFB8339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L4323
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L4315
	.p2align 4,,10
	.p2align 3
.L4330:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L4323
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L4323
	movq	%rsi, %rdi
.L4315:
	cmpq	%rcx, %r9
	jne	.L4330
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L4331
	testq	%rcx, %rcx
	je	.L4317
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L4317
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L4317:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4323:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4331:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L4324
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L4317
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L4316:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L4332
.L4318:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L4317
.L4324:
	movq	%r10, %rax
	jmp	.L4316
.L4332:
	movq	%rcx, 16(%rbx)
	jmp	.L4318
	.cfi_endproc
.LFE8339:
	.size	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_, .-_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD2Ev
	.type	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD2Ev, @function
_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD2Ev:
.LFB5494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L4333
	movq	%rdi, -16(%rbp)
	leaq	-16(%rbp), %rsi
	leaq	16(%rax), %rdi
	call	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
.L4333:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4340
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4340:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5494:
	.size	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD2Ev, .-_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD2Ev
	.globl	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev
	.set	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev,_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS5_EERKNS1_16DispatchResponseE
	.type	_ZN4node9inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS5_EERKNS1_16DispatchResponseE, @function
_ZN4node9inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS5_EERKNS1_16DispatchResponseE:
.LFB5505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L4341
	movq	%rdi, %rbx
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4341
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-48(%rbp), %r12
	movl	16(%rbx), %esi
	movq	%r12, %rcx
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4345
	movq	(%rdi), %rax
	call	*24(%rax)
.L4345:
	movq	8(%rbx), %r13
	movq	$0, 8(%rbx)
	testq	%r13, %r13
	je	.L4341
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4346
	addq	$16, %rdi
	movq	%r12, %rsi
	movq	%r13, -48(%rbp)
	call	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
.L4346:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4341:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4361
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4361:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5505:
	.size	_ZN4node9inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS5_EERKNS1_16DispatchResponseE, .-_ZN4node9inspector8protocol14DispatcherBase8Callback12sendIfActiveESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS5_EERKNS1_16DispatchResponseE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase8CallbackD0Ev
	.type	_ZN4node9inspector8protocol14DispatcherBase8CallbackD0Ev, @function
_ZN4node9inspector8protocol14DispatcherBase8CallbackD0Ev:
.LFB5503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4363
	call	_ZdlPv@PLT
.L4363:
	movq	24(%r12), %rdi
	leaq	40(%r12), %rax
	cmpq	%rax, %rdi
	je	.L4364
	call	_ZdlPv@PLT
.L4364:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L4365
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4366
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r13, -32(%rbp)
	call	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
.L4366:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L4365:
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4375
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4375:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5503:
	.size	_ZN4node9inspector8protocol14DispatcherBase8CallbackD0Ev, .-_ZN4node9inspector8protocol14DispatcherBase8CallbackD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase8Callback7disposeEv
	.type	_ZN4node9inspector8protocol14DispatcherBase8Callback7disposeEv, @function
_ZN4node9inspector8protocol14DispatcherBase8Callback7disposeEv:
.LFB5504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, 8(%rdi)
	testq	%r12, %r12
	je	.L4376
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4378
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r12, -32(%rbp)
	call	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
.L4378:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4376:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4387
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4387:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5504:
	.size	_ZN4node9inspector8protocol14DispatcherBase8Callback7disposeEv, .-_ZN4node9inspector8protocol14DispatcherBase8Callback7disposeEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase8CallbackD2Ev
	.type	_ZN4node9inspector8protocol14DispatcherBase8CallbackD2Ev, @function
_ZN4node9inspector8protocol14DispatcherBase8CallbackD2Ev:
.LFB5501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4389
	call	_ZdlPv@PLT
.L4389:
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4390
	call	_ZdlPv@PLT
.L4390:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L4388
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4392
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r12, -32(%rbp)
	call	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
.L4392:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4388:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4401
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4401:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5501:
	.size	_ZN4node9inspector8protocol14DispatcherBase8CallbackD2Ev, .-_ZN4node9inspector8protocol14DispatcherBase8CallbackD2Ev
	.globl	_ZN4node9inspector8protocol14DispatcherBase8CallbackD1Ev
	.set	_ZN4node9inspector8protocol14DispatcherBase8CallbackD1Ev,_ZN4node9inspector8protocol14DispatcherBase8CallbackD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv
	.type	_ZN4node9inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv, @function
_ZN4node9inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv:
.LFB5506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L4402
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L4402
	movq	%rdi, %rbx
	movq	8(%rax), %rdi
	movl	16(%rbx), %esi
	leaq	56(%rbx), %rcx
	leaq	24(%rbx), %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%rbx), %r12
	movq	$0, 8(%rbx)
	testq	%r12, %r12
	je	.L4402
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4406
	leaq	-32(%rbp), %rsi
	addq	$16, %rdi
	movq	%r12, -32(%rbp)
	call	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS5_
.L4406:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4402:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4418
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4418:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5506:
	.size	_ZN4node9inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv, .-_ZN4node9inspector8protocol14DispatcherBase8Callback19fallThroughIfActiveEv
	.section	.text._ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.type	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, @function
_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_:
.LFB8612:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L4433
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L4428
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L4434
.L4430:
	movq	%rcx, %r14
.L4421:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L4427:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L4435
	testq	%rsi, %rsi
	jg	.L4423
	testq	%r15, %r15
	jne	.L4426
.L4424:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4435:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L4423
.L4426:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L4424
	.p2align 4,,10
	.p2align 3
.L4434:
	testq	%r14, %r14
	js	.L4430
	jne	.L4421
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L4427
	.p2align 4,,10
	.p2align 3
.L4423:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L4424
	jmp	.L4426
	.p2align 4,,10
	.p2align 3
.L4428:
	movl	$1, %r14d
	jmp	.L4421
.L4433:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8612:
	.size	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, .-_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.section	.rodata.str1.1
.LC116:
	.string	"vector::reserve"
.LC119:
	.string	""
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i, @function
_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i:
.LFB7707:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1001, %r8d
	jne	.L4437
.L4474:
	movq	$0, (%r12)
.L4436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4561
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4437:
	.cfi_restore_state
	movq	%rdx, %r13
	leaq	-152(%rbp), %r14
	movq	%rsi, %rdi
	movl	%r8d, %ebx
	leaq	-160(%rbp), %r15
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$11, %eax
	ja	.L4474
	leaq	.L4441(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4441:
	.long	.L4448-.L4441
	.long	.L4474-.L4441
	.long	.L4447-.L4441
	.long	.L4474-.L4441
	.long	.L4446-.L4441
	.long	.L4445-.L4441
	.long	.L4444-.L4441
	.long	.L4443-.L4441
	.long	.L4442-.L4441
	.long	.L4474-.L4441
	.long	.L4474-.L4441
	.long	.L4474-.L4441
	.text
	.p2align 4,,10
	.p2align 3
.L4442:
	movl	$16, %edi
	leaq	16+_ZTVN4node9inspector8protocol5ValueE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rax, -176(%rbp)
	movq	%rbx, (%rax)
	movl	$0, 8(%rax)
.L4449:
	movq	-168(%rbp), %rdx
	movq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsItEEvPKT_S6_PS6_
	movq	-176(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4448:
	leaq	-144(%rbp), %rdi
	call	_ZN4node9inspector8protocol15DictionaryValue6createEv
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	-152(%rbp), %rdi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$1, %eax
	je	.L4487
	leaq	-96(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	leaq	-136(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	jmp	.L4559
	.p2align 4,,10
	.p2align 3
.L4568:
	movq	-176(%rbp), %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0
	testb	%al, %al
	je	.L4562
.L4492:
	movq	-152(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$10, %eax
	jne	.L4563
	movq	-152(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	leal	1(%rbx), %r8d
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L4564
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	$0, -136(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4498
	movq	(%rdi), %rax
	call	*24(%rax)
.L4498:
	movq	-152(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$9, %eax
	je	.L4565
	cmpl	$1, %eax
	jne	.L4560
.L4500:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4502
	movq	(%rdi), %rdx
	movl	%eax, -192(%rbp)
	call	*24(%rdx)
	movl	-192(%rbp), %eax
.L4502:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4503
	movl	%eax, -192(%rbp)
	call	_ZdlPv@PLT
	movl	-192(%rbp), %eax
.L4503:
	cmpl	$1, %eax
	je	.L4487
.L4559:
	cmpl	$4, %eax
	jne	.L4566
	movq	-184(%rbp), %rax
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	-152(%rbp), %rax
	leaq	-2(%rax), %rsi
	movq	-160(%rbp), %rax
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L4567
	jnb	.L4568
	movq	-184(%rbp), %rdi
.L4493:
	movq	$0, (%r12)
.L4494:
	cmpq	-184(%rbp), %rdi
	je	.L4489
	call	_ZdlPv@PLT
.L4489:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4436
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4447:
	movl	$40, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rcx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rdx
	movq	-152(%rbp), %r9
	movups	%xmm0, 16(%rax)
	movq	%rdx, (%rax)
	movq	%r15, %rdx
	movl	$7, 8(%rax)
	movq	%r9, %rdi
	movq	$0, 32(%rax)
	movq	%rax, -176(%rbp)
	movq	%r9, -192(%rbp)
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$3, %eax
	je	.L4449
	leal	1(%rbx), %eax
	movq	-192(%rbp), %r9
	leaq	-136(%rbp), %rbx
	movl	%eax, -184(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -200(%rbp)
.L4486:
	movl	-184(%rbp), %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L4569
	movq	-176(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movq	$0, -136(%rbp)
	movq	24(%rcx), %rsi
	cmpq	32(%rcx), %rsi
	je	.L4479
	movq	$0, -128(%rbp)
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 24(%rcx)
.L4480:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4481
	movq	(%rdi), %rax
	call	*24(%rax)
.L4481:
	movq	-152(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$9, %eax
	je	.L4570
	movq	-136(%rbp), %rdi
	cmpl	$3, %eax
	jne	.L4571
	testq	%rdi, %rdi
	je	.L4449
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4449
	.p2align 4,,10
	.p2align 3
.L4446:
	leaq	-96(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%rax, -96(%rbp)
	movq	-152(%rbp), %rax
	movb	$0, -80(%rbp)
	leaq	-2(%rax), %rsi
	movq	-160(%rbp), %rax
	leaq	2(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L4572
	jb	.L4474
	movq	-176(%rbp), %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringItEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0
	testb	%al, %al
	je	.L4573
.L4473:
	movq	-176(%rbp), %rsi
	leaq	-128(%rbp), %rdi
	call	_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-128(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -176(%rbp)
	cmpq	-184(%rbp), %rdi
	je	.L4449
	call	_ZdlPv@PLT
	jmp	.L4449
	.p2align 4,,10
	.p2align 3
.L4445:
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	subq	%rcx, %r15
	sarq	%r15
	movq	%r15, %rbx
	addq	$1, %rbx
	js	.L4574
	jne	.L4451
.L4457:
	xorl	%r14d, %r14d
	leaq	-136(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L4452:
	movzwl	(%rcx,%r14,2), %eax
	testl	$65408, %eax
	jne	.L4575
	movb	%al, -136(%rbp)
	movq	-120(%rbp), %rsi
	cmpq	-112(%rbp), %rsi
	je	.L4460
	addq	$1, %r14
	movb	%al, (%rsi)
	addq	$1, -120(%rbp)
	cmpq	%r14, %r15
	ja	.L4452
	leaq	-136(%rbp), %rbx
.L4461:
	movq	-120(%rbp), %r14
	movq	-112(%rbp), %rsi
.L4456:
	movb	$0, -136(%rbp)
	cmpq	%r14, %rsi
	je	.L4463
	movb	$0, (%r14)
	addq	$1, -120(%rbp)
.L4464:
	movq	-128(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN4node9inspector8protocol10StringUtil8toDoubleEPKcmPb@PLT
.L4459:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4465
	movsd	%xmm0, -176(%rbp)
	call	_ZdlPv@PLT
	movsd	-176(%rbp), %xmm0
.L4465:
	cmpb	$0, -136(%rbp)
	je	.L4474
	comisd	.LC117(%rip), %xmm0
	jb	.L4467
	movsd	.LC118(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L4467
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm0, %xmm1
	jnp	.L4576
.L4467:
	movl	$24, %edi
	movsd	%xmm0, -184(%rbp)
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rbx
	call	_Znwm@PLT
	movsd	-184(%rbp), %xmm0
	movq	%rax, -176(%rbp)
	movl	$3, 8(%rax)
	movq	%rbx, (%rax)
	movsd	%xmm0, 16(%rax)
	jmp	.L4449
	.p2align 4,,10
	.p2align 3
.L4444:
	movl	$24, %edi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rax, -176(%rbp)
	movl	$1, 8(%rax)
	movq	%rbx, (%rax)
	movb	$1, 16(%rax)
	jmp	.L4449
	.p2align 4,,10
	.p2align 3
.L4443:
	movl	$24, %edi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rax, -176(%rbp)
	movl	$1, 8(%rax)
	movq	%rbx, (%rax)
	movb	$0, 16(%rax)
	jmp	.L4449
	.p2align 4,,10
	.p2align 3
.L4460:
	leaq	-128(%rbp), %rdi
	addq	$1, %r14
	movq	%rdx, %rbx
	movq	%rcx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	cmpq	%r14, %r15
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %rcx
	ja	.L4452
	jmp	.L4461
	.p2align 4,,10
	.p2align 3
.L4570:
	movq	-152(%rbp), %r9
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%r9, -192(%rbp)
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	movq	-192(%rbp), %r9
	cmpl	$3, %eax
	je	.L4577
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4486
	movq	(%rdi), %rax
	movq	%r9, -192(%rbp)
	call	*24(%rax)
	movq	-192(%rbp), %r9
	jmp	.L4486
	.p2align 4,,10
	.p2align 3
.L4479:
	movq	-200(%rbp), %rdx
	leaq	16(%rcx), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L4480
	.p2align 4,,10
	.p2align 3
.L4565:
	movq	-152(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenItEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$1, %eax
	jne	.L4500
.L4560:
	movq	-136(%rbp), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L4497
	movq	(%rdi), %rax
	call	*24(%rax)
.L4497:
	movq	-96(%rbp), %rdi
	jmp	.L4494
	.p2align 4,,10
	.p2align 3
.L4567:
	movq	-176(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC119(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L4492
	.p2align 4,,10
	.p2align 3
.L4575:
	movb	$0, -136(%rbp)
	pxor	%xmm0, %xmm0
	jmp	.L4459
.L4573:
	movq	$0, (%r12)
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4436
	call	_ZdlPv@PLT
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4569:
	movq	$0, (%r12)
.L4478:
	movq	-176(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4451:
	movq	%rbx, %rdi
	movq	%rcx, -176(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	-176(%rbp), %rcx
	movq	%rax, %r14
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L4578
	testq	%r8, %r8
	jne	.L4454
.L4455:
	movq	%r14, %xmm0
	leaq	(%r14,%rbx), %rsi
	leaq	-136(%rbp), %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	testq	%r15, %r15
	jne	.L4457
	jmp	.L4456
.L4571:
	movq	$0, (%r12)
.L4484:
	testq	%rdi, %rdi
	je	.L4478
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L4478
.L4487:
	movq	-144(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L4449
.L4578:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	call	memmove@PLT
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %rcx
.L4454:
	movq	%r8, %rdi
	movq	%rcx, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rcx
	jmp	.L4455
.L4566:
	movq	$0, (%r12)
	jmp	.L4489
.L4563:
	movq	$0, (%r12)
	movq	-96(%rbp), %rdi
	jmp	.L4494
.L4564:
	movq	$0, (%r12)
	jmp	.L4497
.L4572:
	movq	-176(%rbp), %rdi
	leaq	.LC119(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6assignEPKc@PLT
	jmp	.L4473
.L4463:
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	jmp	.L4464
.L4577:
	movq	$0, (%r12)
	movq	-136(%rbp), %rdi
	jmp	.L4484
.L4576:
	jne	.L4467
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rdx
	movq	%rax, -176(%rbp)
	movl	$2, 8(%rax)
	movq	%rdx, (%rax)
	movl	%ebx, 16(%rax)
	jmp	.L4449
.L4562:
	movq	-96(%rbp), %rdi
	jmp	.L4493
.L4561:
	call	__stack_chk_fail@PLT
.L4574:
	leaq	.LC116(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7707:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i, .-_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	.p2align 4
	.globl	_ZN4node9inspector8protocol19parseJSONCharactersEPKtj
	.type	_ZN4node9inspector8protocol19parseJSONCharactersEPKtj, @function
_ZN4node9inspector8protocol19parseJSONCharactersEPKtj:
.LFB5648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	(%rsi,%rdx,2), %rbx
	leaq	-32(%rbp), %rdi
	leaq	-40(%rbp), %rcx
	movq	%rbx, %rdx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueItEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4580
	cmpq	-40(%rbp), %rbx
	je	.L4585
	movq	(%rdi), %rax
	movq	$0, (%r12)
	call	*24(%rax)
.L4579:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4586
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4585:
	.cfi_restore_state
	movq	%rdi, (%r12)
	jmp	.L4579
	.p2align 4,,10
	.p2align 3
.L4580:
	movq	$0, (%r12)
	jmp	.L4579
.L4586:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5648:
	.size	_ZN4node9inspector8protocol19parseJSONCharactersEPKtj, .-_ZN4node9inspector8protocol19parseJSONCharactersEPKtj
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB8634:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L4601
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L4596
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L4602
.L4598:
	movq	%rcx, %r14
.L4589:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L4595:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L4603
	testq	%rsi, %rsi
	jg	.L4591
	testq	%r15, %r15
	jne	.L4594
.L4592:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4603:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L4591
.L4594:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L4592
	.p2align 4,,10
	.p2align 3
.L4602:
	testq	%r14, %r14
	js	.L4598
	jne	.L4589
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L4595
	.p2align 4,,10
	.p2align 3
.L4591:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L4592
	jmp	.L4594
	.p2align 4,,10
	.p2align 3
.L4596:
	movl	$1, %r14d
	jmp	.L4589
.L4601:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8634:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"void node::inspector::protocol::cbor::{anonymous}::CBOREncoder<C>::HandleArrayEnd() [with C = std::vector<unsigned char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv:
.LFB9763:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	je	.L4617
	ret
	.p2align 4,,10
	.p2align 3
.L4617:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4607
	movb	$-1, (%rsi)
	addq	$1, 8(%rdi)
.L4608:
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L4618
	movq	-8(%rax), %rsi
	movq	8(%rbx), %rdx
	testq	%rsi, %rsi
	je	.L4619
	movq	8(%rdx), %r8
	movq	(%rdx), %rdi
	movl	$4294967295, %r10d
	movq	%r8, %r9
	subq	%rdi, %r9
	movq	%r9, %rcx
	subq	%rsi, %rcx
	subq	$4, %rcx
	cmpq	%r10, %rcx
	ja	.L4611
	leaq	1(%rsi), %r8
	movq	%r8, -8(%rax)
	movq	%rcx, %r8
	shrq	$24, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	(%rdx), %rdi
	leaq	1(%rsi), %r8
	movq	%r8, -8(%rax)
	movq	%rcx, %r8
	shrq	$16, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	(%rdx), %rdi
	leaq	1(%rsi), %r8
	movq	%r8, -8(%rax)
	movq	%rcx, %r8
	shrq	$8, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	(%rdx), %rdx
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movb	%cl, (%rdx,%rsi)
	subq	$8, 24(%rbx)
.L4604:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4611:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L4604
	movl	$31, (%rax)
	movq	%r9, 8(%rax)
	cmpq	%r8, %rdi
	je	.L4604
	movq	%rdi, 8(%rdx)
	jmp	.L4604
	.p2align 4,,10
	.p2align 3
.L4607:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L9kStopByteE(%rip), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L4608
.L4619:
	leaq	.LC99(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	__assert_fail@PLT
.L4618:
	leaq	.LC120(%rip), %rcx
	movl	$2280, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9763:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.section	.rodata._ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_.str1.8,"aMS",@progbits,1
	.align 8
.LC121:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.type	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_, @function
_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_:
.LFB7018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4621
	xorl	%r12d, %r12d
.L4623:
	cmpb	$0, (%r14,%r12)
	js	.L4622
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L4623
.L4621:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.p2align 4,,10
	.p2align 3
.L4620:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4644
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4622:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r12, %r12
	js	.L4645
	je	.L4625
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	leaq	(%rax,%r12), %rbx
	movq	%rax, -80(%rbp)
	movq	%rbx, -64(%rbp)
	call	memcpy@PLT
	movq	%rax, %rdi
.L4633:
	movq	%rbx, -72(%rbp)
	cmpq	%r12, %r15
	jbe	.L4626
	leaq	(%r14,%r12), %rbx
	leaq	(%r14,%r15), %r12
	leaq	-81(%rbp), %r14
	jmp	.L4630
	.p2align 4,,10
	.p2align 3
.L4647:
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L4628
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L4629:
	addq	$1, %rbx
	cmpq	%r12, %rbx
	je	.L4646
.L4630:
	movzbl	(%rbx), %eax
	testb	%al, %al
	jns	.L4647
	leaq	-80(%rbp), %r15
	shrb	$6, %al
	movq	%r14, %rsi
	addq	$1, %rbx
	orl	$-64, %eax
	movq	%r15, %rdi
	movb	%al, -81(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movzbl	-1(%rbx), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, -81(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpq	%r12, %rbx
	jne	.L4630
.L4646:
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %r12
	subq	%rdi, %r12
.L4626:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector8protocol4cbor17EncodeString8TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4620
	call	_ZdlPv@PLT
	jmp	.L4620
	.p2align 4,,10
	.p2align 3
.L4628:
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L4629
	.p2align 4,,10
	.p2align 3
.L4625:
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	jmp	.L4633
.L4644:
	call	__stack_chk_fail@PLT
.L4645:
	leaq	.LC121(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7018:
	.size	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_, .-_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5888:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvNS1_4spanIhEEPT_
	.cfi_endproc
.LFE5888:
	.size	_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE:
.LFB5896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%xmm0, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4650
	movb	$-5, (%rsi)
	movq	8(%rdi), %rax
	leaq	1(%rax), %r12
	movq	%r12, 8(%rdi)
.L4651:
	movq	%r13, -56(%rbp)
	movl	$56, %r14d
	jmp	.L4658
	.p2align 4,,10
	.p2align 3
.L4672:
	movb	%sil, (%r12)
	movq	8(%rbx), %rax
	subl	$8, %r14d
	leaq	1(%rax), %r12
	movq	%r12, 8(%rbx)
	cmpl	$-8, %r14d
	je	.L4671
.L4658:
	movq	-56(%rbp), %rsi
	movl	%r14d, %ecx
	shrq	%cl, %rsi
	cmpq	%r12, 16(%rbx)
	jne	.L4672
	movabsq	$9223372036854775807, %rax
	movq	(%rbx), %r15
	movq	%r12, %rdx
	subq	%r15, %rdx
	cmpq	%rax, %rdx
	je	.L4673
	movl	$1, %r12d
	testq	%rdx, %rdx
	je	.L4655
	leaq	(%rdx,%rdx), %r12
	cmpq	%r12, %rdx
	jbe	.L4674
	movabsq	$9223372036854775807, %r12
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4674:
	movabsq	$9223372036854775807, %rax
	testq	%r12, %r12
	cmovs	%rax, %r12
.L4655:
	movq	%r12, %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%rax, %r13
	leaq	(%rax,%r12), %rax
	movq	%rax, -64(%rbp)
	leaq	1(%r13,%rdx), %r12
	movb	%sil, 0(%r13,%rdx)
	testq	%rdx, %rdx
	jg	.L4675
	testq	%r15, %r15
	jne	.L4656
.L4657:
	movq	-64(%rbp), %rax
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	subl	$8, %r14d
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rbx)
	movups	%xmm0, (%rbx)
	cmpl	$-8, %r14d
	jne	.L4658
.L4671:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4675:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memmove@PLT
.L4656:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L4657
	.p2align 4,,10
	.p2align 3
.L4650:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE(%rip), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%rbx), %r12
	jmp	.L4651
.L4673:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5896:
	.size	_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor12EncodeDoubleEdPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK4node9inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK4node9inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	cmpl	$2, %eax
	je	.L4677
	cmpl	$3, %eax
	je	.L4678
	cmpl	$1, %eax
	je	.L4692
.L4676:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4693
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4677:
	.cfi_restore_state
	movslq	16(%rdi), %rsi
	testl	%esi, %esi
	js	.L4684
	movq	%r12, %rdx
	xorl	%edi, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	jmp	.L4676
	.p2align 4,,10
	.p2align 3
.L4692:
	cmpb	$0, 16(%rdi)
	leaq	-41(%rbp), %rsi
	movq	%r12, %rdi
	setne	%al
	subl	$12, %eax
	movb	%al, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	jmp	.L4676
	.p2align 4,,10
	.p2align 3
.L4678:
	movsd	16(%rdi), %xmm0
	movq	8(%rsi), %rsi
	cmpq	16(%r12), %rsi
	je	.L4681
	movb	$-5, (%rsi)
	addq	$1, 8(%r12)
.L4682:
	movq	%xmm0, %r13
	movl	$56, %ebx
	leaq	-41(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L4683:
	movl	%ebx, %ecx
	movq	%r13, %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	shrq	%cl, %rax
	subl	$8, %ebx
	movb	%al, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-8, %ebx
	jne	.L4683
	jmp	.L4676
	.p2align 4,,10
	.p2align 3
.L4684:
	notl	%esi
	movq	%r12, %rdx
	movl	$1, %edi
	movslq	%esi, %rsi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	jmp	.L4676
	.p2align 4,,10
	.p2align 3
.L4681:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE(%rip), %rdx
	movq	%r12, %rdi
	movsd	%xmm0, -56(%rbp)
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movsd	-56(%rbp), %xmm0
	jmp	.L4682
.L4693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5363:
	.size	_ZNK4node9inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK4node9inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb:
.LFB9769:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L4694
	cmpb	$1, %sil
	movq	8(%rdi), %rdi
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedTrueE(%rip), %rdx
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L13kEncodedFalseE(%rip), %rcx
	sbbl	%eax, %eax
	subl	$11, %eax
	testb	%sil, %sil
	movq	8(%rdi), %rsi
	cmove	%rcx, %rdx
	cmpq	16(%rdi), %rsi
	je	.L4697
	movb	%al, (%rsi)
	addq	$1, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4694:
	ret
	.p2align 4,,10
	.p2align 3
.L4697:
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.cfi_endproc
.LFE9769:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv:
.LFB9770:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L4702
	ret
	.p2align 4,,10
	.p2align 3
.L4702:
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4701
	movb	$-10, (%rsi)
	addq	$1, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4701:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedNullE(%rip), %rdx
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.cfi_endproc
.LFE9770:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd:
.LFB9767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L4711
.L4703:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4712
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4711:
	.cfi_restore_state
	movq	8(%rdi), %r13
	movq	%xmm0, %rbx
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L4705
	movb	$-5, (%rsi)
	addq	$1, 8(%r13)
.L4706:
	movl	$56, %r12d
	leaq	-41(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L4707:
	movl	%r12d, %ecx
	movq	%rbx, %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	shrq	%cl, %rax
	subl	$8, %r12d
	movb	%al, -41(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpl	$-8, %r12d
	jne	.L4707
	jmp	.L4703
	.p2align 4,,10
	.p2align 3
.L4705:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE(%rip), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L4706
.L4712:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9767:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"void node::inspector::protocol::cbor::{anonymous}::CBOREncoder<C>::HandleMapEnd() [with C = std::vector<unsigned char>]"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv:
.LFB9761:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	je	.L4726
	ret
	.p2align 4,,10
	.p2align 3
.L4726:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4716
	movb	$-1, (%rsi)
	addq	$1, 8(%rdi)
.L4717:
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L4727
	movq	-8(%rax), %rsi
	movq	8(%rbx), %rdx
	testq	%rsi, %rsi
	je	.L4728
	movq	8(%rdx), %r8
	movq	(%rdx), %rdi
	movl	$4294967295, %r10d
	movq	%r8, %r9
	subq	%rdi, %r9
	movq	%r9, %rcx
	subq	%rsi, %rcx
	subq	$4, %rcx
	cmpq	%r10, %rcx
	ja	.L4720
	leaq	1(%rsi), %r8
	movq	%r8, -8(%rax)
	movq	%rcx, %r8
	shrq	$24, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	(%rdx), %rdi
	leaq	1(%rsi), %r8
	movq	%r8, -8(%rax)
	movq	%rcx, %r8
	shrq	$16, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	(%rdx), %rdi
	leaq	1(%rsi), %r8
	movq	%r8, -8(%rax)
	movq	%rcx, %r8
	shrq	$8, %r8
	movb	%r8b, (%rdi,%rsi)
	movq	-8(%rax), %rsi
	movq	(%rdx), %rdx
	leaq	1(%rsi), %rdi
	movq	%rdi, -8(%rax)
	movb	%cl, (%rdx,%rsi)
	subq	$8, 24(%rbx)
.L4713:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4720:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L4713
	movl	$31, (%rax)
	movq	%r9, 8(%rax)
	cmpq	%r8, %rdi
	je	.L4713
	movq	%rdi, 8(%rdx)
	jmp	.L4713
	.p2align 4,,10
	.p2align 3
.L4716:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L9kStopByteE(%rip), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L4717
.L4728:
	leaq	.LC99(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	__assert_fail@PLT
.L4727:
	leaq	.LC122(%rip), %rcx
	movl	$2259, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9761:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC123:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.type	_ZNSt6vectorIhSaIhEE17_M_default_appendEm, @function
_ZNSt6vectorIhSaIhEE17_M_default_appendEm:
.LFB8639:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L4749
	movabsq	$9223372036854775807, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	%rcx, %rax
	subq	(%rdi), %r13
	subq	%r13, %rsi
	cmpq	%rbx, %rax
	jb	.L4731
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	call	memset@PLT
	addq	%rax, %rbx
	movq	%rbx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4731:
	.cfi_restore_state
	cmpq	%rbx, %rsi
	jb	.L4752
	cmpq	%r13, %rbx
	movq	%r13, %rax
	cmovnb	%rbx, %rax
	addq	%r13, %rax
	movq	%rax, %r15
	jc	.L4741
	testq	%rax, %rax
	js	.L4741
	jne	.L4735
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L4739:
	movq	%rbx, %rdx
	leaq	(%r14,%r13), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L4753
	testq	%r8, %r8
	jne	.L4737
.L4738:
	addq	%r13, %rbx
	movq	%r14, (%r12)
	addq	%rbx, %r14
	movq	%r15, 16(%r12)
	movq	%r14, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4749:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L4741:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L4735:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
	addq	%rax, %r15
	jmp	.L4739
	.p2align 4,,10
	.p2align 3
.L4753:
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L4737:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L4738
.L4752:
	leaq	.LC123(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8639:
	.size	_ZNSt6vectorIhSaIhEE17_M_default_appendEm, .-_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.section	.rodata._ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm.str1.8,"aMS",@progbits,1
	.align 8
.LC124:
	.string	"void node::inspector::protocol::cbor::EncodeStartTmpl(C*, size_t*) [with C = std::vector<unsigned char>; size_t = long unsigned int]"
	.section	.text._ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm,"axG",@progbits,_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm
	.type	_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm, @function
_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm:
.LFB7025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, (%rsi)
	jne	.L4765
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L4756
	movb	$-40, (%rsi)
	movq	8(%rdi), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%rdi)
.L4757:
	cmpq	16(%r12), %rsi
	je	.L4758
	movb	$90, (%rsi)
	movq	8(%r12), %rax
	addq	$1, %rax
	movq	%rax, 8(%r12)
.L4759:
	movq	(%r12), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	leaq	4(%rdx), %rsi
	movq	%rdx, (%rbx)
	cmpq	%rsi, %rdx
	jnb	.L4766
	popq	%rbx
	movq	%r12, %rdi
	movl	$4, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.p2align 4,,10
	.p2align 3
.L4766:
	.cfi_restore_state
	addq	%rsi, %rcx
	cmpq	%rcx, %rax
	je	.L4754
	movq	%rcx, 8(%r12)
.L4754:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4758:
	.cfi_restore_state
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L4759
	.p2align 4,,10
	.p2align 3
.L4756:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE(%rip), %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L4757
.L4765:
	leaq	.LC124(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC98(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE7025:
	.size	_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm, .-_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK4node9inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK4node9inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	leaq	-48(%rbp), %rsi
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-49(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	$-97, -49(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	16(%r13), %rdx
	cmpq	24(%r13), %rdx
	je	.L4771
	.p2align 4,,10
	.p2align 3
.L4768:
	movq	(%rdx,%rbx,8), %rdi
	movq	%r14, %rsi
	addq	$1, %rbx
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L4768
.L4771:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	$-1, -49(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	-48(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L4780
	movq	(%r14), %rcx
	movq	8(%r14), %rax
	movl	$4294967295, %esi
	subq	%rcx, %rax
	subq	%rdx, %rax
	subq	$4, %rax
	cmpq	%rsi, %rax
	ja	.L4767
	leaq	1(%rdx), %rsi
	movq	%rsi, -48(%rbp)
	movq	%rax, %rsi
	shrq	$24, %rsi
	movb	%sil, (%rcx,%rdx)
	movq	-48(%rbp), %rdx
	movq	(%r14), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, -48(%rbp)
	movq	%rax, %rsi
	shrq	$16, %rsi
	movb	%sil, (%rcx,%rdx)
	movq	-48(%rbp), %rdx
	movq	(%r14), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, -48(%rbp)
	movq	%rax, %rsi
	shrq	$8, %rsi
	movb	%sil, (%rcx,%rdx)
	movq	(%r14), %rcx
	movq	-48(%rbp), %rdx
	movb	%al, (%rcx,%rdx)
.L4767:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4781
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4780:
	.cfi_restore_state
	leaq	.LC99(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	__assert_fail@PLT
.L4781:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5446:
	.size	_ZNK4node9inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK4node9inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE:
.LFB5899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, (%rdi)
	jne	.L4793
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	8(%rsi), %rsi
	cmpq	16(%r12), %rsi
	je	.L4784
	movb	$-40, (%rsi)
	movq	8(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r12)
.L4785:
	cmpq	%rsi, 16(%r12)
	je	.L4786
	movb	$90, (%rsi)
	movq	8(%r12), %rax
	addq	$1, %rax
	movq	%rax, 8(%r12)
.L4787:
	movq	(%r12), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	leaq	4(%rdx), %rsi
	movq	%rdx, (%rbx)
	cmpq	%rsi, %rdx
	jnb	.L4794
	popq	%rbx
	movq	%r12, %rdi
	movl	$4, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
	.p2align 4,,10
	.p2align 3
.L4794:
	.cfi_restore_state
	addq	%rsi, %rcx
	cmpq	%rcx, %rax
	je	.L4782
	movq	%rcx, 8(%r12)
.L4782:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4786:
	.cfi_restore_state
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L4787
	.p2align 4,,10
	.p2align 3
.L4784:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L4785
.L4793:
	leaq	.LC124(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC98(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5899:
	.size	_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor15EnvelopeEncoder11EncodeStartEPSt6vectorIhSaIhEE
	.section	.text._ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.type	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, @function
_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_:
.LFB8696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movabsq	$4611686018427387903, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	%rax
	cmpq	%rsi, %rax
	je	.L4809
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L4805
	movabsq	$9223372036854775806, %r14
	cmpq	%rcx, %rax
	jbe	.L4810
.L4797:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L4804:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	2(%rbx,%rdx), %r10
	movq	%r9, %r13
	movw	%ax, (%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L4811
	testq	%r9, %r9
	jg	.L4800
	testq	%r15, %r15
	jne	.L4803
.L4801:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4811:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L4800
.L4803:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L4801
	.p2align 4,,10
	.p2align 3
.L4810:
	testq	%rcx, %rcx
	jne	.L4798
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L4804
	.p2align 4,,10
	.p2align 3
.L4800:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L4801
	jmp	.L4803
	.p2align 4,,10
	.p2align 3
.L4805:
	movl	$2, %r14d
	jmp	.L4797
.L4809:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4798:
	cmpq	%rsi, %rcx
	cmovbe	%rcx, %rsi
	movq	%rsi, %r14
	addq	%rsi, %r14
	jmp	.L4797
	.cfi_endproc
.LFE8696:
	.size	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, .-_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_116ParseUTF16StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_116ParseUTF16StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE:
.LFB5943:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$7, 16(%rdi)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	jne	.L4827
	movq	56(%rdi), %r15
	movq	40(%rdi), %r12
	movq	%rdi, %r13
	movq	%rsi, %r14
	addq	32(%rdi), %r12
	subq	%r15, %r12
	addq	(%rdi), %r12
	testq	%r15, %r15
	je	.L4822
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	leaq	-82(%rbp), %r8
	xorl	%ebx, %ebx
	jmp	.L4818
	.p2align 4,,10
	.p2align 3
.L4828:
	movw	%ax, (%rsi)
	addq	$2, %rbx
	addq	$2, %rsi
	movq	%rsi, -72(%rbp)
	cmpq	%r15, %rbx
	jnb	.L4817
.L4816:
	movq	-64(%rbp), %rcx
.L4818:
	movzbl	1(%r12,%rbx), %eax
	sall	$8, %eax
	movl	%eax, %edx
	movzbl	(%r12,%rbx), %eax
	orl	%edx, %eax
	movw	%ax, -82(%rbp)
	cmpq	%rcx, %rsi
	jne	.L4828
	movq	%r8, %rdx
	leaq	-80(%rbp), %rdi
	addq	$2, %rbx
	movq	%r8, -104(%rbp)
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	cmpq	%r15, %rbx
	movq	-72(%rbp), %rsi
	movq	-104(%rbp), %r8
	jb	.L4816
	.p2align 4,,10
	.p2align 3
.L4817:
	movq	-80(%rbp), %r8
	subq	%r8, %rsi
	sarq	%rsi
	movq	%rsi, %r15
.L4814:
	movq	(%r14), %rax
	movq	%r8, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	*56(%rax)
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L4819
	cmpl	$13, %eax
	je	.L4819
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
.L4819:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4812
	call	_ZdlPv@PLT
.L4812:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4829
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4822:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L4814
.L4829:
	call	__stack_chk_fail@PLT
.L4827:
	call	_ZNK4node9inspector8protocol4cbor13CBORTokenizer18GetString16WireRepEv.part.0
	.cfi_endproc
.LFE5943:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_116ParseUTF16StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_116ParseUTF16StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"bool node::inspector::protocol::cbor::{anonymous}::ParseMap(int32_t, node::inspector::protocol::cbor::CBORTokenizer*, node::inspector::protocol::StreamingParserHandler*)"
	.align 8
.LC126:
	.string	"tokenizer->TokenTag() == CBORTokenTag::MAP_START"
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE:
.LFB5956:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$9, 16(%rsi)
	jne	.L4877
	movq	(%rdx), %rax
	movq	%rsi, %r12
	movl	%edi, %ebx
	movq	%rdx, %r14
	movq	%rdx, %rdi
	call	*16(%rax)
	movl	16(%r12), %eax
	cmpl	$13, %eax
	je	.L4832
	testl	%eax, %eax
	je	.L4832
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
.L4832:
	cmpl	$11, %eax
	je	.L4834
	leaq	.L4845(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L4833:
	cmpl	$13, %eax
	je	.L4878
	testl	%eax, %eax
	je	.L4856
	cmpl	$6, %eax
	je	.L4879
	cmpl	$7, %eax
	jne	.L4840
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_116ParseUTF16StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
.L4839:
	cmpl	$301, %ebx
	je	.L4880
	movl	16(%r12), %eax
	cmpl	$12, %eax
	je	.L4881
	cmpl	$13, %eax
	ja	.L4843
.L4882:
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4845:
	.long	.L4856-.L4845
	.long	.L4855-.L4845
	.long	.L4854-.L4845
	.long	.L4853-.L4845
	.long	.L4852-.L4845
	.long	.L4851-.L4845
	.long	.L4850-.L4845
	.long	.L4849-.L4845
	.long	.L4848-.L4845
	.long	.L4847-.L4845
	.long	.L4846-.L4845
	.long	.L4843-.L4845
	.long	.L4843-.L4845
	.long	.L4844-.L4845
	.text
	.p2align 4,,10
	.p2align 3
.L4856:
	movq	(%r14), %rax
	movq	32(%r12), %rdx
	movq	%r14, %rdi
	movl	24(%r12), %esi
	call	*104(%rax)
	xorl	%eax, %eax
.L4830:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4880:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	32(%r12), %rdx
	movq	%r14, %rdi
	movl	$27, %esi
	call	*104(%rax)
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4879:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	56(%r12), %rdx
	movq	%r14, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*48(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L4839
	cmpl	$13, %eax
	je	.L4839
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	jmp	.L4839
	.p2align 4,,10
	.p2align 3
.L4881:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
	cmpl	$13, %eax
	jbe	.L4882
	.p2align 4,,10
	.p2align 3
.L4843:
	movq	(%r14), %rax
	movq	32(%r12), %rdx
	movl	$20, %esi
	movq	%r14, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L4830
	.p2align 4,,10
	.p2align 3
.L4844:
	movq	(%r14), %rax
	movq	32(%r12), %rdx
	movl	$23, %esi
	movq	%r14, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L4830
	.p2align 4,,10
	.p2align 3
.L4846:
	leal	1(%rbx), %edi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_110ParseArrayEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
.L4861:
	testb	%al, %al
	je	.L4830
	movl	16(%r12), %eax
	.p2align 4,,10
	.p2align 3
.L4857:
	cmpl	$11, %eax
	jne	.L4833
.L4834:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movl	16(%r12), %edx
	testl	%edx, %edx
	sete	%al
	cmpl	$13, %edx
	sete	%dl
	orb	%dl, %al
	jne	.L4830
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	$1, %eax
	jmp	.L4830
	.p2align 4,,10
	.p2align 3
.L4847:
	leal	1(%rbx), %edi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	jmp	.L4861
	.p2align 4,,10
	.p2align 3
.L4848:
	movq	(%r14), %rax
	movq	56(%r12), %rdx
	movq	%r14, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*64(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L4857
	.p2align 4,,10
	.p2align 3
.L4874:
	cmpl	$13, %eax
	je	.L4857
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4849:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_116ParseUTF16StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	movl	16(%r12), %eax
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4851:
	movq	(%r14), %rax
	movq	8(%r12), %rdi
	movq	(%r12), %rcx
	movq	72(%rax), %rdx
	movq	32(%r12), %rax
	addq	$1, %rax
	subq	%rax, %rdi
	addq	%rax, %rcx
	cmpq	$7, %rdi
	jbe	.L4859
	movq	(%rcx), %rax
	movq	%r14, %rdi
	bswap	%rax
	movq	%rax, %xmm0
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4874
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4852:
	movq	(%r14), %rax
	movq	80(%rax), %rdx
	movq	56(%r12), %rax
	movl	%eax, %esi
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L4858
	notl	%esi
.L4858:
	movq	%r14, %rdi
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4874
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4853:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*96(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4874
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4854:
	movq	(%r14), %rax
	xorl	%esi, %esi
.L4876:
	movq	%r14, %rdi
	call	*88(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4874
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4855:
	movq	(%r14), %rax
	movl	$1, %esi
	jmp	.L4876
	.p2align 4,,10
	.p2align 3
.L4850:
	movq	(%r14), %rax
	movq	56(%r12), %rdx
	movq	%r14, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*48(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4874
	jmp	.L4857
	.p2align 4,,10
	.p2align 3
.L4878:
	movq	(%r14), %rax
	movq	32(%r12), %rdx
	movl	$25, %esi
	movq	%r14, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L4830
	.p2align 4,,10
	.p2align 3
.L4840:
	movq	(%r14), %rax
	movq	32(%r12), %rdx
	movl	$26, %esi
	movq	%r14, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L4830
.L4859:
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS1_4spanIhEE.part.0
.L4877:
	leaq	.LC125(%rip), %rcx
	movl	$2770, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC126(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5956:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"bool node::inspector::protocol::cbor::{anonymous}::ParseArray(int32_t, node::inspector::protocol::cbor::CBORTokenizer*, node::inspector::protocol::StreamingParserHandler*)"
	.align 8
.LC128:
	.string	"tokenizer->TokenTag() == CBORTokenTag::ARRAY_START"
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_110ParseArrayEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_110ParseArrayEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE:
.LFB5955:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$10, 16(%rsi)
	jne	.L4926
	movq	%rsi, %r12
	movq	%rdx, %r13
	xorl	%esi, %esi
	movl	%edi, %ebx
	movq	%r12, %rdi
	leaq	.L4894(%rip), %r14
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	movl	16(%r12), %eax
	cmpl	$11, %eax
	je	.L4886
	.p2align 4,,10
	.p2align 3
.L4885:
	cmpl	$13, %eax
	je	.L4927
	testl	%eax, %eax
	je	.L4905
	cmpl	$301, %ebx
	je	.L4928
	cmpl	$12, %eax
	je	.L4929
	cmpl	$13, 16(%r12)
	ja	.L4892
.L4930:
	movl	16(%r12), %eax
	movslq	(%r14,%rax,4), %rax
	addq	%r14, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4894:
	.long	.L4905-.L4894
	.long	.L4904-.L4894
	.long	.L4903-.L4894
	.long	.L4902-.L4894
	.long	.L4901-.L4894
	.long	.L4900-.L4894
	.long	.L4899-.L4894
	.long	.L4898-.L4894
	.long	.L4897-.L4894
	.long	.L4896-.L4894
	.long	.L4895-.L4894
	.long	.L4892-.L4894
	.long	.L4892-.L4894
	.long	.L4893-.L4894
	.text
	.p2align 4,,10
	.p2align 3
.L4905:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	24(%r12), %esi
	call	*104(%rax)
	xorl	%eax, %eax
.L4883:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4928:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movq	%r13, %rdi
	movl	$27, %esi
	call	*104(%rax)
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4929:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	cmpl	$13, 16(%r12)
	jbe	.L4930
	.p2align 4,,10
	.p2align 3
.L4892:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$20, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L4883
	.p2align 4,,10
	.p2align 3
.L4893:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$23, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L4883
	.p2align 4,,10
	.p2align 3
.L4895:
	leal	1(%rbx), %edi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_110ParseArrayEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
.L4910:
	testb	%al, %al
	je	.L4883
	movl	16(%r12), %eax
	.p2align 4,,10
	.p2align 3
.L4906:
	cmpl	$11, %eax
	jne	.L4885
.L4886:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*40(%rax)
	movl	16(%r12), %edx
	testl	%edx, %edx
	sete	%al
	cmpl	$13, %edx
	sete	%dl
	orb	%dl, %al
	jne	.L4883
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	$1, %eax
	jmp	.L4883
	.p2align 4,,10
	.p2align 3
.L4896:
	leal	1(%rbx), %edi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	jmp	.L4910
	.p2align 4,,10
	.p2align 3
.L4897:
	movq	0(%r13), %rax
	movq	56(%r12), %rdx
	movq	%r13, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*64(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	je	.L4906
	.p2align 4,,10
	.p2align 3
.L4923:
	cmpl	$13, %eax
	je	.L4906
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	16(%r12), %eax
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4898:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_116ParseUTF16StringEPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	movl	16(%r12), %eax
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4899:
	movq	0(%r13), %rax
	movq	56(%r12), %rdx
	movq	%r13, %rdi
	movq	40(%r12), %rsi
	addq	32(%r12), %rsi
	subq	%rdx, %rsi
	addq	(%r12), %rsi
	call	*48(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4923
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4900:
	movq	0(%r13), %rax
	movq	8(%r12), %rdi
	movq	(%r12), %rcx
	movq	72(%rax), %rdx
	movq	32(%r12), %rax
	addq	$1, %rax
	subq	%rax, %rdi
	addq	%rax, %rcx
	cmpq	$7, %rdi
	jbe	.L4908
	movq	(%rcx), %rax
	movq	%r13, %rdi
	bswap	%rax
	movq	%rax, %xmm0
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4923
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4901:
	movq	0(%r13), %rax
	movq	80(%rax), %rdx
	movq	56(%r12), %rax
	movl	%eax, %esi
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L4907
	notl	%esi
.L4907:
	movq	%r13, %rdi
	call	*%rdx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4923
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4902:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*96(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4923
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4903:
	movq	0(%r13), %rax
	xorl	%esi, %esi
.L4925:
	movq	%r13, %rdi
	call	*88(%rax)
	movl	16(%r12), %eax
	testl	%eax, %eax
	jne	.L4923
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4904:
	movq	0(%r13), %rax
	movl	$1, %esi
	jmp	.L4925
	.p2align 4,,10
	.p2align 3
.L4927:
	movq	0(%r13), %rax
	movq	32(%r12), %rdx
	movl	$24, %esi
	movq	%r13, %rdi
	call	*104(%rax)
	xorl	%eax, %eax
	jmp	.L4883
.L4908:
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_133ReadBytesMostSignificantByteFirstImEET_NS1_4spanIhEE.part.0
.L4926:
	leaq	.LC127(%rip), %rcx
	movl	$2742, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC128(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5955:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_110ParseArrayEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_110ParseArrayEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"void node::inspector::protocol::cbor::ParseCBOR(node::inspector::protocol::span<unsigned char>, node::inspector::protocol::StreamingParserHandler*)"
	.align 8
.LC130:
	.string	"tokenizer.TokenTag() == CBORTokenTag::ENVELOPE"
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor9ParseCBORENS1_4spanIhEEPNS1_22StreamingParserHandlerE
	.type	_ZN4node9inspector8protocol4cbor9ParseCBORENS1_4spanIhEEPNS1_22StreamingParserHandlerE, @function
_ZN4node9inspector8protocol4cbor9ParseCBORENS1_4spanIhEEPNS1_22StreamingParserHandlerE:
.LFB5957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4948
	cmpb	$-40, (%rdi)
	je	.L4934
	movq	(%rdx), %rax
	movl	$22, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*104(%rax)
.L4931:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4949
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4934:
	.cfi_restore_state
	leaq	-96(%rbp), %r13
	movq	%rdi, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%r13, %rdi
	xorl	%esi, %esi
	movl	$0, -72(%rbp)
	movq	$-1, -64(%rbp)
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L4950
	cmpl	$12, %eax
	jne	.L4951
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	cmpl	$9, -80(%rbp)
	je	.L4937
	movq	(%r12), %rax
	movq	-64(%rbp), %rdx
	movl	$29, %esi
	movq	%r12, %rdi
	call	*104(%rax)
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4948:
	movq	(%rdx), %rax
	movl	$21, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*104(%rax)
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4950:
	movq	(%r12), %rax
	movl	-72(%rbp), %esi
	movq	%r12, %rdi
	movq	-64(%rbp), %rdx
	call	*104(%rax)
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4937:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$1, %edi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	testb	%al, %al
	je	.L4931
	movl	-80(%rbp), %eax
	cmpl	$13, %eax
	je	.L4931
	movq	(%r12), %rdx
	movq	104(%rdx), %rcx
	movq	-64(%rbp), %rdx
	testl	%eax, %eax
	je	.L4952
	movl	$28, %esi
	movq	%r12, %rdi
	call	*%rcx
	jmp	.L4931
.L4952:
	movl	-72(%rbp), %esi
	movq	%r12, %rdi
	call	*%rcx
	jmp	.L4931
.L4949:
	call	__stack_chk_fail@PLT
.L4951:
	leaq	.LC129(%rip), %rcx
	movl	$2820, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC130(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE5957:
	.size	_ZN4node9inspector8protocol4cbor9ParseCBORENS1_4spanIhEEPNS1_22StreamingParserHandlerE, .-_ZN4node9inspector8protocol4cbor9ParseCBORENS1_4spanIhEEPNS1_22StreamingParserHandlerE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE:
.LFB6010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$112, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -144(%rbp)
	movq	$-1, -136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%r13, 16(%r12)
	leaq	32(%r12), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	leaq	-144(%rbp), %rax
	movq	%r15, 8(%r12)
	leaq	-128(%rbp), %r15
	movq	%rax, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0
	movq	24(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$0, (%rax)
	movq	$-1, 8(%rax)
	movl	$0, -128(%rbp)
	call	_ZNSt5dequeIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE12emplace_backIJNS4_9ContainerEEEEvDpOT_
	testq	%rbx, %rbx
	je	.L4983
	cmpb	$-40, (%r14)
	je	.L4958
	movq	24(%r12), %rax
	movl	$22, (%rax)
.L4979:
	movq	$0, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L4957
.L4977:
	movq	%rdx, 8(%rax)
.L4957:
	movq	(%r12), %rax
	movl	-144(%rbp), %ebx
	movq	%r12, %rdi
	movq	-136(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4984
	addq	$104, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4958:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, -128(%rbp)
	movq	%rbx, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L4985
	cmpl	$12, %eax
	jne	.L4986
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	cmpl	$9, -112(%rbp)
	je	.L4963
	movq	24(%r12), %rax
	movq	-96(%rbp), %rdx
	movl	$29, (%rax)
.L4981:
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jne	.L4977
	jmp	.L4957
	.p2align 4,,10
	.p2align 3
.L4983:
	movq	24(%r12), %rax
	movl	$21, (%rax)
	jmp	.L4979
	.p2align 4,,10
	.p2align 3
.L4985:
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rcx
	testl	%edx, %edx
	je	.L4965
	movq	24(%r12), %rax
	movl	%edx, (%rax)
	movq	%rcx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jne	.L4977
	jmp	.L4957
	.p2align 4,,10
	.p2align 3
.L4963:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	$1, %edi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	testb	%al, %al
	je	.L4957
	movl	-112(%rbp), %eax
	cmpl	$13, %eax
	je	.L4957
	movq	-96(%rbp), %rdx
	testl	%eax, %eax
	je	.L4987
	movq	24(%r12), %rax
	movl	$28, (%rax)
	jmp	.L4981
.L4987:
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L4965
	movq	24(%r12), %rax
	movl	%ecx, (%rax)
	jmp	.L4981
.L4984:
	call	__stack_chk_fail@PLT
.L4986:
	leaq	.LC129(%rip), %rcx
	movl	$2820, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC130(%rip), %rdi
	call	__assert_fail@PLT
.L4965:
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE.part.0
	.cfi_endproc
.LFE6010:
	.size	_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$112, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -144(%rbp)
	movq	$-1, -136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	leaq	16+_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r13, 16(%r12)
	leaq	32(%r12), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	leaq	-144(%rbp), %rax
	movq	%r15, 8(%r12)
	leaq	-128(%rbp), %r15
	movq	%rax, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 48(%r12)
	movups	%xmm0, 64(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 96(%r12)
	call	_ZNSt11_Deque_baseIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE17_M_initialize_mapEm.constprop.0
	movq	24(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	$0, (%rax)
	movq	$-1, 8(%rax)
	movl	$0, -128(%rbp)
	call	_ZNSt5dequeIN4node9inspector8protocol4json12_GLOBAL__N_15StateESaIS5_EE12emplace_backIJNS4_9ContainerEEEEvDpOT_
	testq	%rbx, %rbx
	je	.L5012
	cmpb	$-40, (%r14)
	je	.L4991
	movq	24(%r12), %rax
	movl	$22, (%rax)
.L5011:
	movq	$0, 8(%rax)
	movq	16(%r12), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
.L4998:
	movq	(%r12), %rax
	movl	-144(%rbp), %ebx
	movq	%r12, %rdi
	movq	-136(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5013
	addq	$104, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4991:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, -128(%rbp)
	movq	%rbx, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L5014
	cmpl	$12, %eax
	jne	.L5015
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	cmpl	$9, -112(%rbp)
	je	.L4995
	movq	24(%r12), %rax
	movq	-96(%rbp), %rdx
	movl	$29, (%rax)
.L5010:
	movq	%rdx, 8(%rax)
	movq	16(%r12), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	jmp	.L4998
	.p2align 4,,10
	.p2align 3
.L5012:
	movq	24(%r12), %rax
	movl	$21, (%rax)
	jmp	.L5011
	.p2align 4,,10
	.p2align 3
.L5014:
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rcx
	testl	%edx, %edx
	je	.L4997
	movq	24(%r12), %rax
	movl	%edx, (%rax)
	movq	%rcx, 8(%rax)
	movq	16(%r12), %rax
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	movb	$0, (%rax)
	jmp	.L4998
	.p2align 4,,10
	.p2align 3
.L4995:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	$1, %edi
	call	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_18ParseMapEiPNS2_13CBORTokenizerEPNS1_22StreamingParserHandlerE
	testb	%al, %al
	je	.L4998
	movl	-112(%rbp), %eax
	cmpl	$13, %eax
	je	.L4998
	movq	-96(%rbp), %rdx
	testl	%eax, %eax
	je	.L5016
	movq	24(%r12), %rax
	movl	$28, (%rax)
	jmp	.L5010
.L5016:
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L4997
	movq	24(%r12), %rax
	movl	%ecx, (%rax)
	jmp	.L5010
.L5013:
	call	__stack_chk_fail@PLT
.L5015:
	leaq	.LC129(%rip), %rcx
	movl	$2820, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC130(%rip), %rdi
	call	__assert_fail@PLT
.L4997:
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE.part.0
	.cfi_endproc
.LFE6011:
	.size	_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4json17ConvertCBORToJSONERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB8739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L5018
	testq	%rsi, %rsi
	je	.L5034
.L5018:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L5035
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L5021
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L5022:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5036
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5021:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L5022
	jmp	.L5020
	.p2align 4,,10
	.p2align 3
.L5035:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L5020:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L5022
.L5034:
	leaq	.LC26(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L5036:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8739:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i, @function
_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i:
.LFB7711:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1001, %r8d
	jne	.L5038
.L5060:
	movq	$0, (%r12)
.L5037:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5138
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5038:
	.cfi_restore_state
	movq	%rdx, %r13
	leaq	-128(%rbp), %r14
	movq	%rsi, %rdi
	movl	%r8d, %ebx
	leaq	-136(%rbp), %r15
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$11, %eax
	ja	.L5060
	leaq	.L5042(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5042:
	.long	.L5049-.L5042
	.long	.L5060-.L5042
	.long	.L5048-.L5042
	.long	.L5060-.L5042
	.long	.L5047-.L5042
	.long	.L5046-.L5042
	.long	.L5045-.L5042
	.long	.L5044-.L5042
	.long	.L5043-.L5042
	.long	.L5060-.L5042
	.long	.L5060-.L5042
	.long	.L5060-.L5042
	.text
	.p2align 4,,10
	.p2align 3
.L5043:
	movl	$16, %edi
	leaq	16+_ZTVN4node9inspector8protocol5ValueE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rax, -160(%rbp)
	movq	%rbx, (%rax)
	movl	$0, 8(%rax)
.L5050:
	movq	-152(%rbp), %rdx
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_125skipWhitespaceAndCommentsIhEEvPKT_S6_PS6_
	movq	-160(%rbp), %rax
	movq	%rax, (%r12)
	jmp	.L5037
	.p2align 4,,10
	.p2align 3
.L5049:
	leaq	-120(%rbp), %rdi
	call	_ZN4node9inspector8protocol15DictionaryValue6createEv
	movq	-128(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$1, %eax
	je	.L5073
	leaq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	leaq	-112(%rbp), %rsi
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rsi, -184(%rbp)
	jmp	.L5136
	.p2align 4,,10
	.p2align 3
.L5145:
	movq	-160(%rbp), %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0
	testb	%al, %al
	je	.L5139
.L5078:
	movq	-128(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$10, %eax
	jne	.L5140
	movq	-128(%rbp), %rsi
	leal	1(%rbx), %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	-184(%rbp), %rdi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L5141
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	-160(%rbp), %rsi
	movq	$0, -112(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue3setINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSt10unique_ptrIT_St14default_deleteISE_EE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5084
	movq	(%rdi), %rax
	call	*24(%rax)
.L5084:
	movq	-128(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$9, %eax
	je	.L5142
	cmpl	$1, %eax
	jne	.L5137
.L5086:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5088
	movq	(%rdi), %rdx
	movl	%eax, -176(%rbp)
	call	*24(%rdx)
	movl	-176(%rbp), %eax
.L5088:
	movq	-96(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L5089
	movl	%eax, -176(%rbp)
	call	_ZdlPv@PLT
	movl	-176(%rbp), %eax
.L5089:
	cmpl	$1, %eax
	je	.L5073
.L5136:
	cmpl	$4, %eax
	jne	.L5143
	movq	-168(%rbp), %rax
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	leaq	-1(%rax), %rsi
	movq	-136(%rbp), %rax
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L5144
	jnb	.L5145
	movq	-168(%rbp), %rdi
.L5079:
	movq	$0, (%r12)
.L5080:
	cmpq	-168(%rbp), %rdi
	je	.L5075
	call	_ZdlPv@PLT
.L5075:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5037
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5037
	.p2align 4,,10
	.p2align 3
.L5048:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-128(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	%r14, %rcx
	leaq	16+_ZTVN4node9inspector8protocol9ListValueE(%rip), %rsi
	movl	$7, 8(%rax)
	movq	%r15, %rdx
	movq	%rsi, (%rax)
	movq	%r9, %rdi
	movq	%r13, %rsi
	movq	$0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, -160(%rbp)
	movq	%r9, -176(%rbp)
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$3, %eax
	je	.L5050
	leal	1(%rbx), %eax
	movq	-176(%rbp), %r9
	leaq	-112(%rbp), %rbx
	movl	%eax, -168(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -184(%rbp)
.L5072:
	movl	-168(%rbp), %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L5146
	movq	-160(%rbp), %rcx
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	24(%rcx), %rsi
	cmpq	32(%rcx), %rsi
	je	.L5065
	movq	$0, -104(%rbp)
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 24(%rcx)
.L5066:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5067
	movq	(%rdi), %rax
	call	*24(%rax)
.L5067:
	movq	-128(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$9, %eax
	je	.L5147
	movq	-112(%rbp), %rdi
	cmpl	$3, %eax
	jne	.L5148
	testq	%rdi, %rdi
	je	.L5050
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5050
	.p2align 4,,10
	.p2align 3
.L5047:
	leaq	-96(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	movb	$0, -80(%rbp)
	leaq	-1(%rax), %rsi
	movq	-136(%rbp), %rax
	leaq	1(%rax), %rdi
	cmpq	%rdi, %rsi
	je	.L5149
	jb	.L5060
	movq	-160(%rbp), %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_112decodeStringIhEEbPKT_S6_PNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.part.0.constprop.0
	testb	%al, %al
	je	.L5150
.L5059:
	movq	-160(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	call	_ZN4node9inspector8protocol11StringValue6createERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -160(%rbp)
	cmpq	-168(%rbp), %rdi
	je	.L5050
	call	_ZdlPv@PLT
	jmp	.L5050
	.p2align 4,,10
	.p2align 3
.L5046:
	movq	-128(%rbp), %rbx
	movq	-136(%rbp), %r15
	leaq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	%rbx, %rdx
	movq	%r15, %rsi
	subq	%r15, %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol10StringUtil8toDoubleEPKcmPb@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5051
	movsd	%xmm0, -160(%rbp)
	call	_ZdlPv@PLT
	movsd	-160(%rbp), %xmm0
.L5051:
	cmpb	$0, -104(%rbp)
	je	.L5060
	comisd	.LC117(%rip), %xmm0
	jb	.L5053
	movsd	.LC118(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L5053
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm0, %xmm1
	jnp	.L5151
.L5053:
	movl	$24, %edi
	movsd	%xmm0, -168(%rbp)
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rbx
	call	_Znwm@PLT
	movsd	-168(%rbp), %xmm0
	movq	%rax, -160(%rbp)
	movl	$3, 8(%rax)
	movq	%rbx, (%rax)
	movsd	%xmm0, 16(%rax)
	jmp	.L5050
	.p2align 4,,10
	.p2align 3
.L5045:
	movl	$24, %edi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rax, -160(%rbp)
	movl	$1, 8(%rax)
	movq	%rbx, (%rax)
	movb	$1, 16(%rax)
	jmp	.L5050
	.p2align 4,,10
	.p2align 3
.L5044:
	movl	$24, %edi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rbx
	call	_Znwm@PLT
	movq	%rax, -160(%rbp)
	movl	$1, 8(%rax)
	movq	%rbx, (%rax)
	movb	$0, 16(%rax)
	jmp	.L5050
	.p2align 4,,10
	.p2align 3
.L5147:
	movq	-128(%rbp), %r9
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r9, %rdi
	movq	%r9, -176(%rbp)
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	movq	-176(%rbp), %r9
	cmpl	$3, %eax
	je	.L5152
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5072
	movq	(%rdi), %rax
	movq	%r9, -176(%rbp)
	call	*24(%rax)
	movq	-176(%rbp), %r9
	jmp	.L5072
	.p2align 4,,10
	.p2align 3
.L5065:
	movq	-184(%rbp), %rdx
	leaq	16(%rcx), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L5066
	.p2align 4,,10
	.p2align 3
.L5142:
	movq	-128(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110parseTokenIhEENS2_5TokenEPKT_S7_PS7_S8_
	cmpl	$1, %eax
	jne	.L5086
.L5137:
	movq	-112(%rbp), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L5083
	movq	(%rdi), %rax
	call	*24(%rax)
.L5083:
	movq	-96(%rbp), %rdi
	jmp	.L5080
	.p2align 4,,10
	.p2align 3
.L5144:
	movq	-160(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC119(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L5078
.L5150:
	movq	$0, (%r12)
	movq	-96(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L5037
	call	_ZdlPv@PLT
	jmp	.L5037
	.p2align 4,,10
	.p2align 3
.L5146:
	movq	$0, (%r12)
.L5064:
	movq	-160(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5037
.L5148:
	movq	$0, (%r12)
.L5070:
	testq	%rdi, %rdi
	je	.L5064
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5064
.L5073:
	movq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L5050
.L5143:
	movq	$0, (%r12)
	jmp	.L5075
.L5140:
	movq	$0, (%r12)
	movq	-96(%rbp), %rdi
	jmp	.L5080
.L5141:
	movq	$0, (%r12)
	jmp	.L5083
.L5149:
	movq	-160(%rbp), %rdi
	leaq	.LC119(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6assignEPKc@PLT
	jmp	.L5059
.L5152:
	movq	$0, (%r12)
	movq	-112(%rbp), %rdi
	jmp	.L5070
.L5151:
	jne	.L5053
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rdx
	movq	%rax, -160(%rbp)
	movl	$2, 8(%rax)
	movq	%rdx, (%rax)
	movl	%ebx, 16(%rax)
	jmp	.L5050
.L5139:
	movq	-96(%rbp), %rdi
	jmp	.L5079
.L5138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7711:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i, .-_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	.p2align 4
	.globl	_ZN4node9inspector8protocol19parseJSONCharactersEPKhj
	.type	_ZN4node9inspector8protocol19parseJSONCharactersEPKhj, @function
_ZN4node9inspector8protocol19parseJSONCharactersEPKhj:
.LFB5649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	leaq	-32(%rbp), %rdi
	leaq	-40(%rbp), %rcx
	addq	%rsi, %rbx
	movq	%rbx, %rdx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12_GLOBAL__N_110buildValueIhEESt10unique_ptrINS1_5ValueESt14default_deleteIS5_EEPKT_SB_PSB_i
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5154
	cmpq	-40(%rbp), %rbx
	je	.L5159
	movq	(%rdi), %rax
	movq	$0, (%r12)
	call	*24(%rax)
.L5153:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5160
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5159:
	.cfi_restore_state
	movq	%rdi, (%r12)
	jmp	.L5153
	.p2align 4,,10
	.p2align 3
.L5154:
	movq	$0, (%r12)
	jmp	.L5153
.L5160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5649:
	.size	_ZN4node9inspector8protocol19parseJSONCharactersEPKhj, .-_ZN4node9inspector8protocol19parseJSONCharactersEPKhj
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag:
.LFB8887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	%rdi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$7, %r13
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	sarq	$5, %rax
	subq	$8, %rsp
	testq	%r13, %r13
	jle	.L5162
	salq	$7, %r13
	movq	8(%rdx), %r12
	addq	%rdi, %r13
	jmp	.L5172
	.p2align 4,,10
	.p2align 3
.L5163:
	cmpq	40(%rbx), %r12
	je	.L5207
.L5166:
	cmpq	72(%rbx), %r12
	je	.L5208
.L5168:
	cmpq	104(%rbx), %r12
	je	.L5209
.L5170:
	subq	$-128, %rbx
	cmpq	%rbx, %r13
	je	.L5210
.L5172:
	cmpq	%r12, 8(%rbx)
	jne	.L5163
	testq	%r12, %r12
	je	.L5183
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L5163
.L5183:
	movq	%rbx, %rax
.L5165:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5207:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L5167
	movq	32(%rbx), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L5166
.L5167:
	addq	$8, %rsp
	leaq	32(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5208:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L5169
	movq	64(%rbx), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L5168
.L5169:
	addq	$8, %rsp
	leaq	64(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5209:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L5171
	movq	96(%rbx), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L5170
.L5171:
	addq	$8, %rsp
	leaq	96(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5210:
	.cfi_restore_state
	movq	%r15, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
.L5162:
	cmpq	$2, %rax
	je	.L5173
	cmpq	$3, %rax
	je	.L5174
	cmpq	$1, %rax
	je	.L5211
.L5176:
	movq	%r15, %rax
	jmp	.L5165
.L5174:
	movq	8(%r14), %r12
	cmpq	%r12, 8(%rbx)
	je	.L5212
.L5179:
	addq	$32, %rbx
	jmp	.L5177
.L5211:
	movq	8(%r14), %r12
.L5178:
	cmpq	%r12, 8(%rbx)
	jne	.L5176
	testq	%r12, %r12
	je	.L5183
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L5176
	jmp	.L5183
.L5173:
	movq	8(%r14), %r12
.L5177:
	cmpq	%r12, 8(%rbx)
	je	.L5213
.L5181:
	addq	$32, %rbx
	jmp	.L5178
.L5212:
	testq	%r12, %r12
	je	.L5183
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L5179
	jmp	.L5183
.L5213:
	testq	%r12, %r12
	je	.L5183
	movq	(%r14), %rsi
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L5181
	jmp	.L5183
	.cfi_endproc
.LFE8887:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol15DictionaryValue6removeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol15DictionaryValue6removeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol15DictionaryValue6removeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r14), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	24(%r13), %r15
	xorl	%edx, %edx
	movq	16(%r13), %r10
	movq	%rax, %rbx
	divq	%r15
	leaq	(%r10,%rdx,8), %r11
	movq	(%r11), %rcx
	testq	%rcx, %rcx
	je	.L5215
	movq	(%rcx), %r12
	movq	%rdx, %r9
	movq	%rcx, %r8
	movq	48(%r12), %rsi
	cmpq	%rsi, %rbx
	je	.L5298
	.p2align 4,,10
	.p2align 3
.L5216:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5215
	movq	48(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%rsi, %rax
	divq	%r15
	cmpq	%rdx, %r9
	jne	.L5215
	movq	%rdi, %r12
	cmpq	%rsi, %rbx
	jne	.L5216
.L5298:
	movq	8(%r14), %rdx
	cmpq	16(%r12), %rdx
	jne	.L5216
	testq	%rdx, %rdx
	je	.L5217
	movq	8(%r12), %rsi
	movq	(%r14), %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r11
	testl	%eax, %eax
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	jne	.L5216
.L5217:
	movq	(%r12), %rsi
	cmpq	%r8, %rcx
	je	.L5299
	testq	%rsi, %rsi
	je	.L5220
	movq	48(%rsi), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r9
	je	.L5220
	movq	%r8, (%r10,%rdx,8)
	movq	(%r12), %rsi
.L5220:
	movq	40(%r12), %rdi
	movq	%rsi, (%r8)
	testq	%rdi, %rdi
	je	.L5222
	movq	(%rdi), %rax
	call	*24(%rax)
.L5222:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5223
	call	_ZdlPv@PLT
.L5223:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 40(%r13)
.L5215:
	movq	80(%r13), %rcx
	movq	72(%r13), %rdi
	movq	%r14, %rdx
	movq	%rcx, %rsi
	movq	%rcx, -56(%rbp)
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIKS7_EEET_SH_SH_T0_St26random_access_iterator_tag
	movq	-56(%rbp), %rcx
	movq	%rax, %rbx
	cmpq	%rax, %rcx
	je	.L5214
	leaq	32(%rax), %rax
	cmpq	%rax, %rcx
	je	.L5237
	leaq	16(%rcx), %rax
	leaq	48(%rbx), %r12
	movq	%rax, -72(%rbp)
	jmp	.L5236
	.p2align 4,,10
	.p2align 3
.L5227:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%r12, %r8
	je	.L5300
.L5254:
	cmpq	%rdi, %rax
	je	.L5301
	movq	%r8, (%rbx)
	movq	16(%rbx), %rax
	movq	%r15, 8(%rbx)
	movq	(%r12), %rdx
	movq	%rdx, 16(%rbx)
	testq	%rdi, %rdi
	je	.L5235
	movq	%rdi, -16(%r12)
	movq	%rax, (%r12)
.L5233:
	movq	$0, -8(%r12)
	addq	$32, %rbx
	movb	$0, (%rdi)
.L5228:
	addq	$32, %r12
	cmpq	-72(%rbp), %r12
	je	.L5302
.L5236:
	movq	-8(%r12), %r15
	movq	-16(%r12), %r8
	cmpq	8(%r14), %r15
	jne	.L5227
	testq	%r15, %r15
	je	.L5228
	movq	(%r14), %rsi
	movq	%r15, %rdx
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	movq	%r8, -64(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	testl	%eax, %eax
	je	.L5228
	movq	-64(%rbp), %r8
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%r12, %r8
	jne	.L5254
.L5253:
	cmpq	$1, %r15
	je	.L5303
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-8(%r12), %r15
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rcx
	jmp	.L5231
	.p2align 4,,10
	.p2align 3
.L5305:
	movq	80(%r13), %r14
	movq	%r14, %rax
	subq	%rcx, %rax
.L5238:
	addq	%rax, %rbx
	cmpq	%r14, %rbx
	je	.L5214
	movq	%rbx, %r12
	.p2align 4,,10
	.p2align 3
.L5250:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5247
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L5250
	movq	%rbx, 80(%r13)
.L5214:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5302:
	.cfi_restore_state
	cmpq	%rbx, %rcx
	je	.L5214
.L5237:
	movq	80(%r13), %r14
	movq	%r14, %rax
	subq	%rcx, %rax
	cmpq	%r14, %rcx
	je	.L5238
	movq	%rax, %r8
	sarq	$5, %r8
	testq	%rax, %rax
	jle	.L5238
	leaq	16(%rcx), %r12
	leaq	16(%rbx), %r14
	jmp	.L5245
	.p2align 4,,10
	.p2align 3
.L5239:
	cmpq	%r14, %rdi
	je	.L5304
	movq	%r15, -16(%r14)
	movq	(%r14), %rax
	movq	%rdx, -8(%r14)
	movq	(%r12), %rdx
	movq	%rdx, (%r14)
	testq	%rdi, %rdi
	je	.L5244
	movq	%rdi, -16(%r12)
	movq	%rax, (%r12)
.L5242:
	movq	$0, -8(%r12)
	addq	$32, %r14
	addq	$32, %r12
	movb	$0, (%rdi)
	subq	$1, %r8
	je	.L5305
.L5245:
	movq	-16(%r12), %r15
	movq	-16(%r14), %rdi
	movq	-8(%r12), %rdx
	cmpq	%r15, %r12
	jne	.L5239
	testq	%rdx, %rdx
	je	.L5240
	cmpq	$1, %rdx
	je	.L5306
	movq	%r12, %rsi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-8(%r12), %rdx
	movq	-16(%r14), %rdi
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L5240:
	movq	%rdx, -8(%r14)
	movb	$0, (%rdi,%rdx)
	movq	-16(%r15), %rdi
	jmp	.L5242
	.p2align 4,,10
	.p2align 3
.L5247:
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L5250
	movq	%rbx, 80(%r13)
	jmp	.L5214
	.p2align 4,,10
	.p2align 3
.L5304:
	movq	%r15, -16(%r14)
	movq	%rdx, -8(%r14)
	movq	(%r12), %rax
	movq	%rax, (%r14)
.L5244:
	movq	%r12, -16(%r12)
	movq	%r12, %rdi
	jmp	.L5242
	.p2align 4,,10
	.p2align 3
.L5301:
	movq	%r8, (%rbx)
	movq	%r15, 8(%rbx)
	movq	(%r12), %rax
	movq	%rax, 16(%rbx)
.L5235:
	movq	%r12, -16(%r12)
	movq	%r12, %rdi
	jmp	.L5233
	.p2align 4,,10
	.p2align 3
.L5300:
	testq	%r15, %r15
	jne	.L5253
.L5231:
	movq	%r15, 8(%rbx)
	movb	$0, (%rdi,%r15)
	movq	-16(%r12), %rdi
	jmp	.L5233
	.p2align 4,,10
	.p2align 3
.L5299:
	testq	%rsi, %rsi
	je	.L5255
	movq	48(%rsi), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r9
	je	.L5220
	movq	%r8, (%r10,%rdx,8)
	movq	(%r11), %rax
.L5219:
	leaq	32(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L5307
.L5221:
	movq	$0, (%r11)
	movq	(%r12), %rsi
	jmp	.L5220
	.p2align 4,,10
	.p2align 3
.L5306:
	movzbl	(%r12), %eax
	movb	%al, (%rdi)
	movq	-8(%r12), %rdx
	movq	-16(%r14), %rdi
	jmp	.L5240
	.p2align 4,,10
	.p2align 3
.L5303:
	movzbl	(%r12), %eax
	movb	%al, (%rdi)
	movq	-8(%r12), %r15
	movq	(%rbx), %rdi
	jmp	.L5231
	.p2align 4,,10
	.p2align 3
.L5255:
	movq	%r8, %rax
	jmp	.L5219
	.p2align 4,,10
	.p2align 3
.L5307:
	movq	%rsi, 32(%r13)
	jmp	.L5221
	.cfi_endproc
.LFE5412:
	.size	_ZN4node9inspector8protocol15DictionaryValue6removeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol15DictionaryValue6removeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.type	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, @function
_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm:
.LFB8953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L5309
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L5319
.L5335:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L5320:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5309:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L5333
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L5334
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L5312:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L5314
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L5315
	.p2align 4,,10
	.p2align 3
.L5316:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L5317:
	testq	%rsi, %rsi
	je	.L5314
.L5315:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L5316
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L5322
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L5315
	.p2align 4,,10
	.p2align 3
.L5314:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L5318
	call	_ZdlPv@PLT
.L5318:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L5335
.L5319:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L5321
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L5321:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L5320
	.p2align 4,,10
	.p2align 3
.L5322:
	movq	%rdx, %rdi
	jmp	.L5317
	.p2align 4,,10
	.p2align 3
.L5333:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L5312
.L5334:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8953:
	.size	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, .-_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv
	.type	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv, @function
_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv:
.LFB5557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$8, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	24(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rbx, (%rax)
	movq	%rax, %r12
	movq	%rax, 0(%r13)
	divq	%rsi
	movq	16(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L5337
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L5339
	.p2align 4,,10
	.p2align 3
.L5348:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5337
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L5337
.L5339:
	cmpq	%rdi, %r12
	jne	.L5348
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5337:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	16(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN4node9inspector8protocol14DispatcherBase7WeakPtrES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5557:
	.size	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv, .-_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv
	.section	.text._ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.type	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, @function
_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag:
.LFB9038:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L5386
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.L5352
	movq	%rdi, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r14
	jnb	.L5353
	movq	%rdi, %r15
	movq	%r14, %rdx
	subq	%r14, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	addq	%r14, 8(%rbx)
	movq	%rax, %rdi
	subq	%r13, %r15
	jne	.L5390
.L5354:
	movq	%r14, %rdx
.L5389:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L5352:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rdx
	movq	(%rbx), %r8
	movq	%rdx, %rax
	subq	%r8, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L5391
	cmpq	%rdi, %r14
	movq	%rdi, %rax
	cmovnb	%r14, %rax
	addq	%rax, %rdi
	movq	%rdi, %r15
	jc	.L5369
	testq	%rdi, %rdi
	js	.L5369
	jne	.L5360
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L5366:
	movq	%r13, %rdx
	subq	%r8, %rdx
	leaq	(%r14,%rdx), %r9
	leaq	(%rcx,%rdx), %r10
	addq	%rcx, %r9
	testq	%rdx, %rdx
	jne	.L5392
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L5362
.L5364:
	testq	%r8, %r8
	jne	.L5363
.L5365:
	movq	%rcx, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
.L5349:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5353:
	.cfi_restore_state
	leaq	(%rdx,%r15), %rsi
	subq	%rsi, %rcx
	jne	.L5393
.L5355:
	subq	%r15, %r14
	addq	%r14, %rdi
	movq	%rdi, 8(%rbx)
	testq	%r15, %r15
	je	.L5349
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	addq	%r15, 8(%rbx)
	movq	%r15, %rdx
	jmp	.L5389
	.p2align 4,,10
	.p2align 3
.L5386:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L5369:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L5360:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r8
	movq	%rax, %rcx
	addq	%rax, %r15
	jmp	.L5366
	.p2align 4,,10
	.p2align 3
.L5390:
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L5354
	.p2align 4,,10
	.p2align 3
.L5393:
	movq	%rcx, %rdx
	call	memmove@PLT
	movq	8(%rbx), %rdi
	jmp	.L5355
	.p2align 4,,10
	.p2align 3
.L5392:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L5362
.L5363:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L5365
	.p2align 4,,10
	.p2align 3
.L5362:
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L5364
.L5391:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9038:
	.size	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, .-_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.section	.text._ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_
	.type	_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_, @function
_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_:
.LFB7015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$3, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r12), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	addq	$8, %rsp
	leaq	0(%r13,%rbx), %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.cfi_endproc
.LFE7015:
	.size	_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_, .-_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PSt6vectorIhSaIhEE:
.LFB5959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	leaq	-128(%rbp), %rdi
	.cfi_offset 14, -32
	movq	%rsi, %r14
	xorl	%esi, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -152(%rbp)
	movq	8(%r8), %rbx
	movq	%rbx, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movl	$0, -104(%rbp)
	movq	$-1, -96(%rbp)
	subq	%rax, %rdx
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN4node9inspector8protocol4cbor13CBORTokenizer13ReadNextTokenEb
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jne	.L5397
	movl	-104(%rbp), %eax
	movq	-96(%rbp), %r9
.L5398:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5408
	addq	$136, %rsp
	movq	%r9, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5397:
	.cfi_restore_state
	xorl	%r9d, %r9d
	movl	$16, %eax
	cmpl	$12, %edx
	jne	.L5398
	movq	-72(%rbp), %rcx
	movq	8(%r12), %rdx
	leaq	6(%rcx), %rsi
	movq	%rdx, %r10
	subq	(%r12), %r10
	cmpq	%rsi, %r10
	jne	.L5398
	movl	$6, %r9d
	movl	$29, %eax
	testq	%rcx, %rcx
	je	.L5398
	movq	-128(%rbp), %rdi
	movq	-96(%rbp), %rsi
	cmpb	$-65, 6(%rdi,%rsi)
	jne	.L5398
	cmpb	$-1, -1(%rbx)
	je	.L5399
	leaq	5(%rcx), %r9
	movl	$30, %eax
	jmp	.L5398
.L5399:
	subq	$1, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r10, -168(%rbp)
	movq	%rdx, 8(%r12)
	movq	%r12, %rdx
	movq	%rcx, -160(%rbp)
	call	_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_
	movq	-152(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4cbor17EncodeString8TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_
	leaq	-129(%rbp), %rsi
	movq	%r12, %rdi
	movb	$-1, -129(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	(%r12), %rdi
	movq	8(%r12), %rdx
	movl	$4294967295, %esi
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %r10
	subq	%rdi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, %rax
	subq	%r10, %rax
	cmpq	%rsi, %rax
	ja	.L5406
	leaq	-4(%r10,%rdx), %rsi
	subq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jbe	.L5409
	bswap	%eax
	orq	$-1, %r9
	movl	%eax, (%rdi,%rsi)
	xorl	%eax, %eax
	jmp	.L5398
.L5406:
	xorl	%r9d, %r9d
	movl	$31, %eax
	jmp	.L5398
.L5408:
	call	__stack_chk_fail@PLT
.L5409:
	leaq	.LC131(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE5959:
	.size	_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor27AppendString8EntryToCBORMapENS1_4spanIhEES4_PSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE:
.LFB9766:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L5417
	ret
	.p2align 4,,10
	.p2align 3
.L5417:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r13
	movq	8(%r13), %rsi
	cmpq	16(%r13), %rsi
	je	.L5412
	movb	$-42, (%rsi)
	addq	$1, 8(%r13)
.L5413:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r13), %rsi
	addq	$8, %rsp
	movq	%r12, %rdx
	leaq	(%r12,%rbx), %rcx
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L5412:
	.cfi_restore_state
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE(%rip), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L5413
	.cfi_endproc
.LFE9766:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPSt6vectorIhSaIhEE:
.LFB5893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rdx), %rsi
	cmpq	16(%rdx), %rsi
	je	.L5419
	movb	$-42, (%rsi)
	addq	$1, 8(%rdx)
.L5420:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r12), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	addq	$8, %rsp
	leaq	0(%r13,%rbx), %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L5419:
	.cfi_restore_state
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L5420
	.cfi_endproc
.LFE5893:
	.size	_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor12EncodeBinaryENS1_4spanIhEEPSt6vectorIhSaIhEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPSt6vectorIhSaIhEE:
.LFB5884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$3, %edi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r12), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	addq	$8, %rsp
	leaq	0(%r13,%rbx), %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.cfi_endproc
.LFE5884:
	.size	_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor13EncodeString8ENS1_4spanIhEEPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK4node9inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK4node9inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	call	_ZN4node9inspector8protocol4cbor15EncodeStartTmplISt6vectorIhSaIhEEEEvPT_Pm
	leaq	-65(%rbp), %rax
	movq	%rbx, %rdi
	movb	$-65, -65(%rbp)
	movq	%rax, %rsi
	movq	%rax, -112(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	leaq	16(%r14), %rax
	movq	72(%r14), %r8
	movq	%rax, -88(%rbp)
	cmpq	80(%r14), %r8
	jne	.L5430
	jmp	.L5431
	.p2align 4,,10
	.p2align 3
.L5442:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movl	$3, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
.L5429:
	movq	40(%r13), %rdi
	movq	%rbx, %rsi
	addq	$1, %r12
	movq	(%rdi), %rax
	call	*80(%rax)
	movq	72(%r14), %r8
	movq	80(%r14), %rax
	subq	%r8, %rax
	sarq	$5, %rax
	cmpq	%r12, %rax
	jbe	.L5431
.L5430:
	movq	%r12, %rax
	movq	-88(%rbp), %rdi
	salq	$5, %rax
	leaq	(%r8,%rax), %r15
	movq	%r15, %rsi
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN4node9inspector8protocol5ValueESt14default_deleteISC_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testq	%rax, %rax
	je	.L5442
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testq	%rax, %rax
	je	.L5429
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %r15
	movq	%rbx, %rdx
	movl	$3, %edi
	movq	%rax, -104(%rbp)
	movq	%r15, %rsi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	-104(%rbp), %r8
	movq	%r15, %rcx
	movq	8(%rbx), %rsi
	movq	%rbx, %rdi
	addq	%r8, %rcx
	movq	%r8, %rdx
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	jmp	.L5429
	.p2align 4,,10
	.p2align 3
.L5431:
	movq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	movb	$-1, -65(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L5443
	movq	(%rbx), %rcx
	movq	8(%rbx), %rax
	movl	$4294967295, %esi
	subq	%rcx, %rax
	subq	%rdx, %rax
	subq	$4, %rax
	cmpq	%rsi, %rax
	ja	.L5424
	leaq	1(%rdx), %rsi
	movq	%rsi, -64(%rbp)
	movq	%rax, %rsi
	shrq	$24, %rsi
	movb	%sil, (%rcx,%rdx)
	movq	-64(%rbp), %rdx
	movq	(%rbx), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, -64(%rbp)
	movq	%rax, %rsi
	shrq	$16, %rsi
	movb	%sil, (%rcx,%rdx)
	movq	-64(%rbp), %rdx
	movq	(%rbx), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, -64(%rbp)
	movq	%rax, %rsi
	shrq	$8, %rsi
	movb	%sil, (%rcx,%rdx)
	movq	(%rbx), %rcx
	movq	-64(%rbp), %rdx
	movb	%al, (%rcx,%rdx)
.L5424:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5444
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5443:
	.cfi_restore_state
	leaq	.LC99(%rip), %rcx
	movl	$2212, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	__assert_fail@PLT
.L5444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5414:
	.size	_ZNK4node9inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK4node9inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE:
.LFB9764:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L5450
	ret
	.p2align 4,,10
	.p2align 3
.L5450:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	movq	%rbx, %rsi
	subq	$8, %rsp
	movq	8(%rdi), %r13
	movl	$3, %edi
	movq	%r13, %rdx
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r13), %rsi
	addq	$8, %rsp
	movq	%r12, %rdx
	leaq	(%r12,%rbx), %rcx
	movq	%r13, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.cfi_endproc
.LFE9764:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE
	.type	_ZNK4node9inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE, @function
_ZNK4node9inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE:
.LFB5368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	16(%rdi), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testq	%rax, %rax
	jne	.L5452
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	$3, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r12), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L5455:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L5452:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testq	%rax, %rax
	je	.L5451
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	$3, %edi
	movq	%rax, %r13
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r12), %rsi
	leaq	0(%r13,%rbx), %rcx
	movq	%r13, %rdx
	jmp	.L5455
	.p2align 4,,10
	.p2align 3
.L5451:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5368:
	.size	_ZNK4node9inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE, .-_ZNK4node9inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE
	.section	.text._ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_
	.type	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_, @function
_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_:
.LFB7017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L5457
	xorl	%r12d, %r12d
.L5459:
	cmpb	$0, (%r15,%r12)
	js	.L5458
	addq	$1, %r12
	cmpq	%r12, %r14
	jne	.L5459
.L5457:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	$3, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r13), %rsi
	leaq	(%r15,%r14), %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L5456:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5480
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5458:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%r12, %r12
	js	.L5481
	je	.L5461
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	leaq	(%rax,%r12), %rcx
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	movq	%rax, %rbx
	movq	%rcx, -64(%rbp)
	movq	%rcx, -104(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %rcx
.L5469:
	movq	%rcx, -72(%rbp)
	cmpq	%r12, %r14
	jbe	.L5462
	leaq	(%r15,%r12), %rbx
	leaq	(%r15,%r14), %r12
	leaq	-81(%rbp), %r14
	jmp	.L5466
	.p2align 4,,10
	.p2align 3
.L5483:
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L5464
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L5465:
	addq	$1, %rbx
	cmpq	%r12, %rbx
	je	.L5482
.L5466:
	movzbl	(%rbx), %eax
	testb	%al, %al
	jns	.L5483
	leaq	-80(%rbp), %r15
	shrb	$6, %al
	movq	%r14, %rsi
	addq	$1, %rbx
	orl	$-64, %eax
	movq	%r15, %rdi
	movb	%al, -81(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movzbl	-1(%rbx), %eax
	movq	%r14, %rsi
	movq	%r15, %rdi
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, -81(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	cmpq	%r12, %rbx
	jne	.L5466
.L5482:
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rbx
	movq	%rcx, %r12
	subq	%rbx, %r12
.L5462:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	$3, %edi
	movq	%rcx, -104(%rbp)
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r13), %rsi
	movq	-104(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5456
	call	_ZdlPv@PLT
	jmp	.L5456
	.p2align 4,,10
	.p2align 3
.L5464:
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L5465
	.p2align 4,,10
	.p2align 3
.L5461:
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	jmp	.L5469
.L5480:
	call	__stack_chk_fail@PLT
.L5481:
	leaq	.LC121(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7017:
	.size	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_, .-_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPSt6vectorIhSaIhEE:
.LFB5887:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor20EncodeFromLatin1TmplISt6vectorIhSaIhEEEEvNS1_4spanIhEEPT_
	.cfi_endproc
.LFE5887:
	.size	_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor16EncodeFromLatin1ENS1_4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.type	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, @function
_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag:
.LFB9046:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L5730
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rdi, %r8
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	sarq	%r13
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %r10
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r14
	movq	16(%rdi), %rax
	subq	%r14, %rax
	cmpq	%r13, %rax
	jb	.L5488
	movq	%r14, %rdx
	movq	%r9, -56(%rbp)
	subq	%rsi, %rdx
	cmpq	%rdx, %r13
	jnb	.L5489
	movq	%r14, %r15
	movq	%rdi, -64(%rbp)
	movq	%r13, %rdx
	movq	%r14, %rdi
	subq	%r13, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	addq	%r13, 8(%r8)
	subq	%r12, %r15
	jne	.L5734
.L5490:
	testq	%r9, %r9
	jle	.L5485
	leaq	(%rbx,%r9), %rax
	cmpq	%rax, %r12
	leaq	(%r12,%r13), %rax
	setnb	%dl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %dl
	je	.L5520
	cmpq	$30, %r9
	jle	.L5520
	movq	%r13, %rdx
	movdqa	.LC102(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L5494:
	movdqu	(%rbx,%rax,2), %xmm1
	movdqu	16(%rbx,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L5494
	movq	%r13, %rcx
	movq	%r13, %rdx
	andq	$-16, %rcx
	addq	%rcx, %r12
	leaq	(%rbx,%rcx,2), %rax
	subq	%rcx, %rdx
	cmpq	%r13, %rcx
	je	.L5485
	movzwl	(%rax), %ecx
	movb	%cl, (%r12)
	cmpq	$1, %rdx
	je	.L5485
	movzwl	2(%rax), %ecx
	movb	%cl, 1(%r12)
	cmpq	$2, %rdx
	je	.L5485
	movzwl	4(%rax), %ecx
	movb	%cl, 2(%r12)
	cmpq	$3, %rdx
	je	.L5485
	movzwl	6(%rax), %ecx
	movb	%cl, 3(%r12)
	cmpq	$4, %rdx
	je	.L5485
	movzwl	8(%rax), %ecx
	movb	%cl, 4(%r12)
	cmpq	$5, %rdx
	je	.L5485
	movzwl	10(%rax), %ecx
	movb	%cl, 5(%r12)
	cmpq	$6, %rdx
	je	.L5485
	movzwl	12(%rax), %ecx
	movb	%cl, 6(%r12)
	cmpq	$7, %rdx
	je	.L5485
	movzwl	14(%rax), %ecx
	movb	%cl, 7(%r12)
	cmpq	$8, %rdx
	je	.L5485
	movzwl	16(%rax), %ecx
	movb	%cl, 8(%r12)
	cmpq	$9, %rdx
	je	.L5485
	movzwl	18(%rax), %ecx
	movb	%cl, 9(%r12)
	cmpq	$10, %rdx
	je	.L5485
	movzwl	20(%rax), %ecx
	movb	%cl, 10(%r12)
	cmpq	$11, %rdx
	je	.L5485
	movzwl	22(%rax), %ecx
	movb	%cl, 11(%r12)
	cmpq	$12, %rdx
	je	.L5485
	movzwl	24(%rax), %ecx
	movb	%cl, 12(%r12)
	cmpq	$13, %rdx
	je	.L5485
	movzwl	26(%rax), %ecx
	movb	%cl, 13(%r12)
	cmpq	$14, %rdx
	jne	.L5733
.L5485:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5488:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rcx
	movq	(%rdi), %r11
	movq	%r14, %rax
	movq	%rcx, %rdx
	subq	%r11, %rax
	subq	%rax, %rdx
	cmpq	%rdx, %r13
	ja	.L5735
	cmpq	%rax, %r13
	movq	%rax, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L5524
	testq	%rax, %rax
	js	.L5524
	jne	.L5511
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L5519:
	movq	%r12, %rdx
	subq	%r11, %rdx
	jne	.L5736
.L5512:
	leaq	(%rcx,%rdx), %rdi
	testq	%r9, %r9
	jle	.L5513
	cmpq	$30, %r9
	jle	.L5525
	movq	%r13, %rdx
	movdqa	.LC102(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L5515:
	movdqu	(%rbx,%rax,2), %xmm1
	movdqu	16(%rbx,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L5515
	movq	%r13, %rdx
	movq	%r13, %r10
	andq	$-16, %rdx
	andl	$15, %r10d
	leaq	(%rbx,%rdx,2), %rbx
	leaq	(%rdi,%rdx), %rax
	cmpq	%rdx, %r13
	je	.L5516
.L5514:
	movzwl	(%rbx), %edx
	movb	%dl, (%rax)
	cmpq	$1, %r10
	je	.L5516
	movzwl	2(%rbx), %edx
	movb	%dl, 1(%rax)
	cmpq	$2, %r10
	je	.L5516
	movzwl	4(%rbx), %edx
	movb	%dl, 2(%rax)
	cmpq	$3, %r10
	je	.L5516
	movzwl	6(%rbx), %edx
	movb	%dl, 3(%rax)
	cmpq	$4, %r10
	je	.L5516
	movzwl	8(%rbx), %edx
	movb	%dl, 4(%rax)
	cmpq	$5, %r10
	je	.L5516
	movzwl	10(%rbx), %edx
	movb	%dl, 5(%rax)
	cmpq	$6, %r10
	je	.L5516
	movzwl	12(%rbx), %edx
	movb	%dl, 6(%rax)
	cmpq	$7, %r10
	je	.L5516
	movzwl	14(%rbx), %edx
	movb	%dl, 7(%rax)
	cmpq	$8, %r10
	je	.L5516
	movzwl	16(%rbx), %edx
	movb	%dl, 8(%rax)
	cmpq	$9, %r10
	je	.L5516
	movzwl	18(%rbx), %edx
	movb	%dl, 9(%rax)
	cmpq	$10, %r10
	je	.L5516
	movzwl	20(%rbx), %edx
	movb	%dl, 10(%rax)
	cmpq	$11, %r10
	je	.L5516
	movzwl	22(%rbx), %edx
	movb	%dl, 11(%rax)
	cmpq	$12, %r10
	je	.L5516
	movzwl	24(%rbx), %edx
	movb	%dl, 12(%rax)
	cmpq	$13, %r10
	je	.L5516
	movzwl	26(%rbx), %edx
	movb	%dl, 13(%rax)
	cmpq	$14, %r10
	je	.L5516
	movzwl	28(%rbx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L5516:
	addq	%r13, %rdi
.L5513:
	subq	%r12, %r14
	jne	.L5737
.L5517:
	addq	%rdi, %r14
	testq	%r11, %r11
	je	.L5518
	movq	%r11, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L5518:
	movq	%r14, %xmm3
	movq	%rcx, %xmm0
	movq	%r15, 16(%r8)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r8)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5730:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L5489:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	(%rdx,%rdx), %r9
	leaq	(%rbx,%r9), %r15
	subq	%r15, %rcx
	movq	%rcx, %rsi
	sarq	%rsi
	testq	%rcx, %rcx
	jle	.L5497
	leaq	(%rcx,%r9), %rax
	addq	%rbx, %rax
	cmpq	%rax, %r14
	leaq	(%r14,%rsi), %rax
	setnb	%dil
	cmpq	%rax, %r15
	setnb	%al
	orb	%al, %dil
	je	.L5521
	cmpq	$30, %rcx
	jle	.L5521
	movq	%rsi, %rcx
	movdqa	.LC102(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L5499:
	movdqu	(%r15,%rax,2), %xmm1
	movdqu	16(%r15,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L5499
	movq	%rsi, %rdi
	movq	%rsi, %rcx
	andq	$-16, %rdi
	andl	$15, %ecx
	leaq	(%r15,%rdi,2), %rax
	addq	%rdi, %r14
	cmpq	%rdi, %rsi
	je	.L5501
	movzwl	(%rax), %esi
	movb	%sil, (%r14)
	cmpq	$1, %rcx
	je	.L5501
	movzwl	2(%rax), %esi
	movb	%sil, 1(%r14)
	cmpq	$2, %rcx
	je	.L5501
	movzwl	4(%rax), %esi
	movb	%sil, 2(%r14)
	cmpq	$3, %rcx
	je	.L5501
	movzwl	6(%rax), %esi
	movb	%sil, 3(%r14)
	cmpq	$4, %rcx
	je	.L5501
	movzwl	8(%rax), %esi
	movb	%sil, 4(%r14)
	cmpq	$5, %rcx
	je	.L5501
	movzwl	10(%rax), %esi
	movb	%sil, 5(%r14)
	cmpq	$6, %rcx
	je	.L5501
	movzwl	12(%rax), %esi
	movb	%sil, 6(%r14)
	cmpq	$7, %rcx
	je	.L5501
	movzwl	14(%rax), %esi
	movb	%sil, 7(%r14)
	cmpq	$8, %rcx
	je	.L5501
	movzwl	16(%rax), %esi
	movb	%sil, 8(%r14)
	cmpq	$9, %rcx
	je	.L5501
	movzwl	18(%rax), %esi
	movb	%sil, 9(%r14)
	cmpq	$10, %rcx
	je	.L5501
	movzwl	20(%rax), %esi
	movb	%sil, 10(%r14)
	cmpq	$11, %rcx
	je	.L5501
	movzwl	22(%rax), %esi
	movb	%sil, 11(%r14)
	cmpq	$12, %rcx
	je	.L5501
	movzwl	24(%rax), %esi
	movb	%sil, 12(%r14)
	cmpq	$13, %rcx
	je	.L5501
	movzwl	26(%rax), %esi
	movb	%sil, 13(%r14)
	cmpq	$14, %rcx
	je	.L5501
	movzwl	28(%rax), %eax
	movb	%al, 14(%r14)
	.p2align 4,,10
	.p2align 3
.L5501:
	movq	8(%r8), %r14
.L5497:
	subq	%rdx, %r13
	leaq	(%r14,%r13), %rdi
	movq	%rdi, 8(%r8)
	testq	%rdx, %rdx
	jne	.L5738
.L5502:
	addq	%rdx, %rdi
	movq	%r9, %rdx
	movq	%rdi, 8(%r8)
	sarq	%rdx
	testq	%r9, %r9
	jle	.L5485
	cmpq	%r15, %r12
	leaq	(%r12,%rdx), %rax
	setnb	%cl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %cl
	je	.L5522
	cmpq	$30, %r9
	jle	.L5522
	movq	%rdx, %rcx
	movdqa	.LC102(%rip), %xmm0
	xorl	%eax, %eax
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L5505:
	movdqu	(%rbx,%rax,2), %xmm1
	movdqu	16(%rbx,%rax,2), %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L5505
	movq	%rdx, %rsi
	movq	%rdx, %rcx
	andq	$-16, %rsi
	andl	$15, %ecx
	leaq	(%rbx,%rsi,2), %rax
	addq	%rsi, %r12
	cmpq	%rsi, %rdx
	je	.L5485
	movzwl	(%rax), %edx
	movb	%dl, (%r12)
	cmpq	$1, %rcx
	je	.L5485
	movzwl	2(%rax), %edx
	movb	%dl, 1(%r12)
	cmpq	$2, %rcx
	je	.L5485
	movzwl	4(%rax), %edx
	movb	%dl, 2(%r12)
	cmpq	$3, %rcx
	je	.L5485
	movzwl	6(%rax), %edx
	movb	%dl, 3(%r12)
	cmpq	$4, %rcx
	je	.L5485
	movzwl	8(%rax), %edx
	movb	%dl, 4(%r12)
	cmpq	$5, %rcx
	je	.L5485
	movzwl	10(%rax), %edx
	movb	%dl, 5(%r12)
	cmpq	$6, %rcx
	je	.L5485
	movzwl	12(%rax), %edx
	movb	%dl, 6(%r12)
	cmpq	$7, %rcx
	je	.L5485
	movzwl	14(%rax), %edx
	movb	%dl, 7(%r12)
	cmpq	$8, %rcx
	je	.L5485
	movzwl	16(%rax), %edx
	movb	%dl, 8(%r12)
	cmpq	$9, %rcx
	je	.L5485
	movzwl	18(%rax), %edx
	movb	%dl, 9(%r12)
	cmpq	$10, %rcx
	je	.L5485
	movzwl	20(%rax), %edx
	movb	%dl, 10(%r12)
	cmpq	$11, %rcx
	je	.L5485
	movzwl	22(%rax), %edx
	movb	%dl, 11(%r12)
	cmpq	$12, %rcx
	je	.L5485
	movzwl	24(%rax), %edx
	movb	%dl, 12(%r12)
	cmpq	$13, %rcx
	je	.L5485
	movzwl	26(%rax), %edx
	movb	%dl, 13(%r12)
	cmpq	$14, %rcx
	je	.L5485
.L5733:
	movzwl	28(%rax), %eax
	movb	%al, 14(%r12)
	jmp	.L5485
	.p2align 4,,10
	.p2align 3
.L5524:
	movq	%rcx, %r15
.L5511:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	%rax, %rcx
	addq	%rax, %r15
	movq	(%r8), %r11
	movq	8(%r8), %r14
	jmp	.L5519
	.p2align 4,,10
	.p2align 3
.L5738:
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rdx
	movq	8(%r8), %rdi
	jmp	.L5502
	.p2align 4,,10
	.p2align 3
.L5734:
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	subq	%r15, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r9
	jmp	.L5490
	.p2align 4,,10
	.p2align 3
.L5737:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %rdi
	jmp	.L5517
	.p2align 4,,10
	.p2align 3
.L5736:
	movq	%r11, %rsi
	movq	%rcx, %rdi
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	%rax, %rcx
	movq	-56(%rbp), %r11
	jmp	.L5512
	.p2align 4,,10
	.p2align 3
.L5520:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5492:
	movzwl	(%rbx,%rax,2), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	movq	%r13, %rdx
	subq	%rax, %rdx
	testq	%rdx, %rdx
	jg	.L5492
	jmp	.L5485
	.p2align 4,,10
	.p2align 3
.L5521:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5498:
	movzwl	(%r15,%rax,2), %ecx
	movb	%cl, (%r14,%rax)
	addq	$1, %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	testq	%rcx, %rcx
	jg	.L5498
	jmp	.L5501
	.p2align 4,,10
	.p2align 3
.L5522:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5503:
	movzwl	(%rbx,%rax,2), %ecx
	movb	%cl, (%r12,%rax)
	addq	$1, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	testq	%rcx, %rcx
	jg	.L5503
	jmp	.L5485
.L5525:
	movq	%rdi, %rax
	jmp	.L5514
.L5735:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9046:
	.size	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, .-_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.section	.text._ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_,"axG",@progbits,_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_,comdat
	.p2align 4
	.weak	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_
	.type	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_, @function
_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_:
.LFB7019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	(%rsi,%rsi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	(%rdi,%r9), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	%r14, %rdi
	je	.L5740
	movq	%rdi, %r13
	movq	%rdi, %rax
.L5742:
	cmpw	$127, (%rax)
	ja	.L5741
	addq	$2, %rax
	cmpq	%rax, %r14
	jne	.L5742
.L5740:
	movq	%r15, %rdx
	movl	$3, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r15), %rsi
	addq	$40, %rsp
	movq	%r14, %rcx
	popq	%rbx
	movq	%r12, %rdx
	movq	%r15, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKtEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.p2align 4,,10
	.p2align 3
.L5741:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r9, %rsi
	movl	$2, %edi
	call	_ZN4node9inspector8protocol4cbor9internals19WriteTokenStartTmplISt6vectorIhSaIhEEEEvNS2_9MajorTypeEmPT_
	movq	8(%r15), %rbx
	jmp	.L5755
	.p2align 4,,10
	.p2align 3
.L5779:
	movb	%cl, (%rbx)
	movq	8(%r15), %rax
	movzbl	%ch, %ebx
	movq	16(%r15), %r11
	leaq	1(%rax), %r12
	movq	%r12, 8(%r15)
	cmpq	%r12, %r11
	je	.L5749
.L5782:
	movb	%bl, (%r12)
	movq	8(%r15), %rax
	addq	$2, %r13
	leaq	1(%rax), %rbx
	movq	%rbx, 8(%r15)
	cmpq	%r13, %r14
	je	.L5778
.L5755:
	movzwl	0(%r13), %ecx
	cmpq	%rbx, 16(%r15)
	jne	.L5779
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r10
	subq	%r10, %rbx
	cmpq	%rax, %rbx
	je	.L5751
	movl	$1, %r12d
	testq	%rbx, %rbx
	je	.L5746
	leaq	(%rbx,%rbx), %r12
	cmpq	%r12, %rbx
	jbe	.L5780
	movabsq	$9223372036854775807, %r12
	jmp	.L5746
	.p2align 4,,10
	.p2align 3
.L5780:
	movabsq	$9223372036854775807, %rax
	testq	%r12, %r12
	cmovs	%rax, %r12
.L5746:
	movq	%r12, %rdi
	movq	%r10, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_Znwm@PLT
	movl	-56(%rbp), %ecx
	testq	%rbx, %rbx
	movq	-64(%rbp), %r10
	leaq	(%rax,%r12), %r11
	movq	%rax, %r9
	leaq	1(%rax,%rbx), %r12
	movb	%cl, (%rax,%rbx)
	jg	.L5781
	testq	%r10, %r10
	jne	.L5747
.L5748:
	movq	%r9, %xmm0
	movq	%r12, %xmm1
	movq	%r11, 16(%r15)
	movzbl	%ch, %ebx
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%r12, %r11
	jne	.L5782
	.p2align 4,,10
	.p2align 3
.L5749:
	movabsq	$9223372036854775807, %rax
	movq	(%r15), %r9
	subq	%r9, %r11
	cmpq	%rax, %r11
	je	.L5751
	movl	$1, %r12d
	testq	%r11, %r11
	je	.L5752
	leaq	(%r11,%r11), %r12
	cmpq	%r12, %r11
	jbe	.L5783
	movabsq	$9223372036854775807, %r12
	jmp	.L5752
	.p2align 4,,10
	.p2align 3
.L5783:
	testq	%r12, %r12
	cmovs	%rax, %r12
.L5752:
	movq	%r12, %rdi
	movq	%r11, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r11
	movq	-56(%rbp), %r9
	addq	%rax, %r12
	movq	%rax, %rcx
	testq	%r11, %r11
	movb	%bl, (%rax,%r11)
	leaq	1(%rax,%r11), %rbx
	jg	.L5784
	testq	%r9, %r9
	jne	.L5753
.L5754:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm2
	addq	$2, %r13
	movq	%r12, 16(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r15)
	cmpq	%r13, %r14
	jne	.L5755
.L5778:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5781:
	.cfi_restore_state
	movq	%r10, %rsi
	movq	%r9, %rdi
	movq	%rbx, %rdx
	movl	%ecx, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r11
	movl	-72(%rbp), %ecx
	movq	%rax, %r9
.L5747:
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r9
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %r11
	jmp	.L5748
	.p2align 4,,10
	.p2align 3
.L5784:
	movq	%r9, %rsi
	movq	%rcx, %rdi
	movq	%r11, %rdx
	movq	%r9, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r9
	movq	%rax, %rcx
.L5753:
	movq	%r9, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L5754
.L5751:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7019:
	.size	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_, .-_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPSt6vectorIhSaIhEE:
.LFB5890:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_
	.cfi_endproc
.LFE5890:
	.size	_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4cbor15EncodeFromUTF16ENS1_4spanItEEPSt6vectorIhSaIhEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE:
.LFB9765:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L5788
	ret
	.p2align 4,,10
	.p2align 3
.L5788:
	movq	8(%rdi), %rdx
	movq	%r8, %rdi
	jmp	_ZN4node9inspector8protocol4cbor19EncodeFromUTF16TmplISt6vectorIhSaIhEEEEvNS1_4spanItEEPT_
	.cfi_endproc
.LFE9765:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE
	.section	.text._ZNSt6vectorItSaItEE7reserveEm,"axG",@progbits,_ZNSt6vectorItSaItEE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE7reserveEm
	.type	_ZNSt6vectorItSaItEE7reserveEm, @function
_ZNSt6vectorItSaItEE7reserveEm:
.LFB9088:
	.cfi_startproc
	endbr64
	movabsq	$4611686018427387903, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	ja	.L5801
	movq	(%rdi), %r12
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	subq	%r12, %rax
	sarq	%rax
	cmpq	%rax, %rsi
	ja	.L5802
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5802:
	.cfi_restore_state
	movq	8(%rdi), %r13
	leaq	(%rsi,%rsi), %r14
	subq	%r12, %r13
	testq	%rsi, %rsi
	je	.L5796
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r12
	movq	8(%rbx), %rdx
	movq	%rax, %r15
	subq	%r12, %rdx
.L5792:
	testq	%rdx, %rdx
	jg	.L5803
	testq	%r12, %r12
	jne	.L5794
.L5795:
	addq	%r15, %r13
	addq	%r15, %r14
	movq	%r15, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%r14, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5803:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	memmove@PLT
.L5794:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L5795
	.p2align 4,,10
	.p2align 3
.L5796:
	movq	%r13, %rdx
	xorl	%r15d, %r15d
	jmp	.L5792
.L5801:
	leaq	.LC116(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9088:
	.size	_ZNSt6vectorItSaItEE7reserveEm, .-_ZNSt6vectorItSaItEE7reserveEm
	.section	.text._ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.type	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, @function
_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_:
.LFB9352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movabsq	$4611686018427387903, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	%rax
	cmpq	%rsi, %rax
	je	.L5818
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L5814
	movabsq	$9223372036854775806, %r14
	cmpq	%rcx, %rax
	jbe	.L5819
.L5806:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L5813:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	2(%rbx,%rdx), %r10
	movq	%r9, %r13
	movw	%ax, (%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L5820
	testq	%r9, %r9
	jg	.L5809
	testq	%r15, %r15
	jne	.L5812
.L5810:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5820:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L5809
.L5812:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L5810
	.p2align 4,,10
	.p2align 3
.L5819:
	testq	%rcx, %rcx
	jne	.L5807
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L5813
	.p2align 4,,10
	.p2align 3
.L5809:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L5810
	jmp	.L5812
	.p2align 4,,10
	.p2align 3
.L5814:
	movl	$2, %r14d
	jmp	.L5806
.L5818:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5807:
	cmpq	%rsi, %rcx
	cmovbe	%rcx, %rsi
	movq	%rsi, %r14
	addq	%rsi, %r14
	jmp	.L5806
	.cfi_endproc
.LFE9352:
	.size	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_, .-_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	.text
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS7_PSt6vectorItSaItEE.part.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS7_PSt6vectorItSaItEE.part.0:
.LFB10397:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	.L5833(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	%rdi, %rsi
	pushq	%rbx
	sarq	%rsi
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt6vectorItSaItEE7reserveEm
	cmpq	%rbx, %r12
	jbe	.L5824
	.p2align 4,,10
	.p2align 3
.L5822:
	movzwl	(%rbx), %eax
	leaq	2(%rbx), %r15
	movw	%ax, -58(%rbp)
	cmpw	$92, %ax
	jne	.L5830
	cmpq	%r15, %r12
	je	.L5831
	movzwl	2(%rbx), %eax
	leaq	4(%rbx), %r15
	movw	%ax, -58(%rbp)
	cmpw	$120, %ax
	je	.L5831
	cmpw	$34, %ax
	je	.L5830
	leal	-47(%rax), %edx
	cmpw	$71, %dx
	ja	.L5831
	movzwl	%dx, %edx
	movslq	0(%r13,%rdx,4), %rdx
	addq	%r13, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L5833:
	.long	.L5830-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5830-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5839-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5838-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5837-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5831-.L5833
	.long	.L5836-.L5833
	.long	.L5831-.L5833
	.long	.L5835-.L5833
	.long	.L5834-.L5833
	.long	.L5832-.L5833
	.text
.L5834:
	movzwl	4(%rbx), %eax
	leal	-48(%rax), %edx
	cmpw	$9, %dx
	jbe	.L5840
	leal	-65(%rax), %edx
	cmpw	$5, %dx
	jbe	.L5863
	leal	-97(%rax), %edx
	cmpw	$5, %dx
	ja	.L5842
	leal	-87(%rax), %edx
.L5840:
	movzwl	6(%rbx), %ecx
	sall	$12, %edx
	leal	-48(%rcx), %eax
	cmpw	$9, %ax
	jbe	.L5843
.L5869:
	leal	-65(%rcx), %eax
	cmpw	$5, %ax
	jbe	.L5864
	leal	-97(%rcx), %eax
	cmpw	$5, %ax
	ja	.L5842
	leal	-87(%rcx), %eax
.L5843:
	movzwl	8(%rbx), %ecx
	sall	$8, %eax
	addl	%eax, %edx
	leal	-48(%rcx), %eax
	cmpw	$9, %ax
	jbe	.L5845
	leal	-65(%rcx), %eax
	cmpw	$5, %ax
	jbe	.L5865
	leal	-97(%rcx), %eax
	cmpw	$5, %ax
	ja	.L5842
	leal	-87(%rcx), %eax
.L5845:
	sall	$4, %eax
	addl	%edx, %eax
	movzwl	10(%rbx), %edx
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	jbe	.L5847
	leal	-65(%rdx), %ecx
	cmpw	$5, %cx
	jbe	.L5866
	leal	-97(%rdx), %ecx
	cmpw	$5, %cx
	ja	.L5842
	leal	-87(%rdx), %ecx
.L5847:
	addl	%ecx, %eax
	leaq	12(%rbx), %r15
	movw	%ax, -58(%rbp)
.L5830:
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L5849
.L5868:
	movw	%ax, (%rsi)
	addq	$2, %rsi
	movq	%rsi, 8(%r14)
.L5850:
	movq	%r15, %rbx
	cmpq	%rbx, %r12
	ja	.L5822
.L5824:
	movl	$1, %eax
	jmp	.L5821
.L5831:
	xorl	%eax, %eax
.L5821:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L5867
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5832:
	.cfi_restore_state
	movl	$11, %eax
	movq	8(%r14), %rsi
	movw	%ax, -58(%rbp)
	movl	$11, %eax
	cmpq	16(%r14), %rsi
	jne	.L5868
	.p2align 4,,10
	.p2align 3
.L5849:
	leaq	-58(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L5850
.L5835:
	movl	$9, %edx
	movl	$9, %eax
	movw	%dx, -58(%rbp)
	jmp	.L5830
.L5836:
	movl	$13, %ecx
	movl	$13, %eax
	movw	%cx, -58(%rbp)
	jmp	.L5830
.L5837:
	movl	$10, %esi
	movl	$10, %eax
	movw	%si, -58(%rbp)
	jmp	.L5830
.L5838:
	movl	$12, %edi
	movl	$12, %eax
	movw	%di, -58(%rbp)
	jmp	.L5830
.L5839:
	movl	$8, %r8d
	movl	$8, %eax
	movw	%r8w, -58(%rbp)
	jmp	.L5830
	.p2align 4,,10
	.p2align 3
.L5866:
	leal	-55(%rdx), %ecx
	leaq	12(%rbx), %r15
	addl	%ecx, %eax
	movw	%ax, -58(%rbp)
	jmp	.L5830
	.p2align 4,,10
	.p2align 3
.L5863:
	movzwl	6(%rbx), %ecx
	leal	-55(%rax), %edx
	sall	$12, %edx
	leal	-48(%rcx), %eax
	cmpw	$9, %ax
	ja	.L5869
	jmp	.L5843
	.p2align 4,,10
	.p2align 3
.L5864:
	leal	-55(%rcx), %eax
	jmp	.L5843
	.p2align 4,,10
	.p2align 3
.L5865:
	leal	-55(%rcx), %eax
	jmp	.L5845
.L5867:
	call	__stack_chk_fail@PLT
.L5842:
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE8HexToIntEt.part.0
	.cfi_endproc
.LFE10397:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS7_PSt6vectorItSaItEE.part.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS7_PSt6vectorItSaItEE.part.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i:
.LFB7799:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$301, %r8d
	je	.L5975
	movq	%rdx, %rbx
	movl	%r8d, %r13d
	leaq	-136(%rbp), %r14
	movq	$0, -144(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	$0, -136(%rbp)
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$12, %eax
	ja	.L5874
	leaq	.L5876(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5876:
	.long	.L5884-.L5876
	.long	.L5874-.L5876
	.long	.L5883-.L5876
	.long	.L5874-.L5876
	.long	.L5882-.L5876
	.long	.L5881-.L5876
	.long	.L5880-.L5876
	.long	.L5879-.L5876
	.long	.L5878-.L5876
	.long	.L5874-.L5876
	.long	.L5874-.L5876
	.long	.L5877-.L5876
	.long	.L5875-.L5876
	.text
	.p2align 4,,10
	.p2align 3
.L5975:
	cmpb	$0, 8(%r12)
	je	.L5976
	.p2align 4,,10
	.p2align 3
.L5870:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5977
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5976:
	.cfi_restore_state
	movq	24(%r12), %r8
	subq	(%r12), %rsi
	sarq	%rsi
	movq	(%r8), %rax
	movq	%rsi, %rdx
	movq	%r8, %rdi
	movl	$2, %esi
	call	*104(%rax)
	movb	$1, 8(%r12)
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5874:
	movq	-144(%rbp), %rdx
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5883:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-136(%rbp), %r10
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$3, %eax
	je	.L5902
	addl	$1, %r13d
.L5904:
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i
	cmpb	$0, 8(%r12)
	jne	.L5870
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$9, %eax
	je	.L5978
	cmpl	$3, %eax
	jne	.L5979
.L5902:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
.L5885:
	movq	-136(%rbp), %rax
	cmpq	%rbx, %rax
	jnb	.L5920
	.p2align 4,,10
	.p2align 3
.L5921:
	movzwl	(%rax), %edx
	cmpw	$32, %dx
	je	.L5922
	leal	-9(%rdx), %ecx
	cmpw	$4, %cx
	jbe	.L5922
	cmpw	$47, %dx
	jne	.L5920
	cmpq	%rax, %rbx
	je	.L5920
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L5920
	movzwl	2(%rax), %edx
	cmpw	$47, %dx
	je	.L5980
	cmpw	$42, %dx
	je	.L5981
	.p2align 4,,10
	.p2align 3
.L5920:
	movq	-152(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5882:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	leaq	-2(%rax), %rsi
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L5935
	jnb	.L5982
.L5898:
	movq	%r12, %rdi
	movl	$6, %esi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5870
.L5973:
	call	_ZdlPv@PLT
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5884:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$1, %eax
	je	.L5905
.L5972:
	cmpl	$4, %eax
	jne	.L5918
.L5906:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm2, %xmm2
	movq	$0, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	leaq	-2(%rax), %rsi
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L5936
	jnb	.L5983
.L5909:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
.L5911:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5973
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5877:
	movq	-144(%rbp), %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5875:
	movq	-144(%rbp), %rdx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5881:
	movq	-144(%rbp), %r14
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %rcx
	movb	$0, -80(%rbp)
	movq	-136(%rbp), %r13
	movq	%r15, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rcx, -168(%rbp)
	subq	%r14, %r13
	movq	$0, -88(%rbp)
	movq	%r13, %rax
	xorl	%r13d, %r13d
	sarq	%rax
	leaq	1(%rax), %rsi
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	cmpq	$0, -160(%rbp)
	jne	.L5886
	jmp	.L5890
	.p2align 4,,10
	.p2align 3
.L5984:
	movsbl	%sil, %esi
	movq	%r15, %rdi
	addq	$1, %r13
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc@PLT
	cmpq	%r13, -160(%rbp)
	je	.L5890
.L5886:
	movzwl	(%r14,%r13,2), %esi
	testl	$65408, %esi
	je	.L5984
	movq	-96(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L5931
	call	_ZdlPv@PLT
.L5931:
	movq	-144(%rbp), %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5880:
	movq	24(%r12), %rdi
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L5885
	.p2align 4,,10
	.p2align 3
.L5879:
	movq	24(%r12), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L5885
	.p2align 4,,10
	.p2align 3
.L5878:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	jmp	.L5885
	.p2align 4,,10
	.p2align 3
.L5922:
	addq	$2, %rax
.L5924:
	cmpq	%rax, %rbx
	ja	.L5921
	jmp	.L5920
	.p2align 4,,10
	.p2align 3
.L5980:
	addq	$4, %rax
	jmp	.L5974
	.p2align 4,,10
	.p2align 3
.L5985:
	movzwl	(%rax), %edx
	addq	$2, %rax
	cmpw	$10, %dx
	je	.L5924
	cmpw	$13, %dx
	je	.L5924
.L5974:
	cmpq	%rax, %rbx
	ja	.L5985
	movq	%rbx, %rax
	jmp	.L5920
	.p2align 4,,10
	.p2align 3
.L5978:
	movq	-136(%rbp), %r10
	movq	%r15, %rdx
	movq	%r10, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$3, %eax
	jne	.L5904
	movq	-144(%rbp), %rdx
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5981:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbx
	ja	.L5927
	jmp	.L5920
	.p2align 4,,10
	.p2align 3
.L5928:
	movq	%rdx, %rcx
.L5927:
	leaq	2(%rcx), %rdx
	movzwl	-2(%rdx), %esi
	cmpq	%rdx, %rbx
	jbe	.L5920
	cmpw	$42, %si
	jne	.L5928
	cmpw	$47, (%rdx)
	jne	.L5928
	leaq	4(%rcx), %rax
	jmp	.L5924
	.p2align 4,,10
	.p2align 3
.L5982:
	leaq	-128(%rbp), %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS7_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L5899
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L5897:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5885
	call	_ZdlPv@PLT
	jmp	.L5885
	.p2align 4,,10
	.p2align 3
.L5983:
	leaq	-128(%rbp), %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE12DecodeStringEPKtS7_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L5910
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L5908:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$10, %eax
	jne	.L5986
	movq	-136(%rbp), %rsi
	leal	1(%r13), %r8d
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i
	cmpb	$0, 8(%r12)
	jne	.L5911
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$9, %eax
	je	.L5987
	cmpl	$1, %eax
	jne	.L5988
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5905
	call	_ZdlPv@PLT
.L5905:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L5885
	.p2align 4,,10
	.p2align 3
.L5890:
	movq	16(%r12), %rdi
	movq	-96(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	movl	%eax, %r13d
	cmpq	-168(%rbp), %rdi
	je	.L5888
	call	_ZdlPv@PLT
.L5888:
	testb	%r13b, %r13b
	je	.L5931
	movsd	-128(%rbp), %xmm0
	comisd	.LC117(%rip), %xmm0
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
	jb	.L5892
	movsd	.LC118(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L5892
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L5892
	jne	.L5892
	call	*80(%rax)
	jmp	.L5885
	.p2align 4,,10
	.p2align 3
.L5892:
	call	*72(%rax)
	jmp	.L5885
	.p2align 4,,10
	.p2align 3
.L5936:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L5908
	.p2align 4,,10
	.p2align 3
.L5979:
	movq	-144(%rbp), %rdx
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5987:
	movq	-136(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseTokenEPKtS7_PS7_S8_
	cmpl	$1, %eax
	je	.L5989
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5972
	movl	%eax, -160(%rbp)
	call	_ZdlPv@PLT
	movl	-160(%rbp), %eax
	cmpl	$4, %eax
	je	.L5906
.L5918:
	movq	-144(%rbp), %rdx
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5870
	.p2align 4,,10
	.p2align 3
.L5935:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L5897
.L5899:
	movq	-144(%rbp), %rdx
	jmp	.L5898
.L5986:
	movq	-144(%rbp), %rdx
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5911
.L5988:
	movq	-144(%rbp), %rdx
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5911
.L5910:
	movq	-144(%rbp), %rdx
	jmp	.L5909
.L5989:
	movq	-144(%rbp), %rdx
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE11HandleErrorENS1_5ErrorEPKt
	jmp	.L5911
.L5977:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7799:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanItEEPNS1_22StreamingParserHandlerE
	.type	_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanItEEPNS1_22StreamingParserHandlerE, @function
_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanItEEPNS1_22StreamingParserHandlerE:
.LFB6008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdi, %xmm0
	xorl	%r8d, %r8d
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	(%rsi,%rdx,2), %rbx
	leaq	-72(%rbp), %rcx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i
	cmpb	$0, -56(%rbp)
	jne	.L5990
	movq	-72(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L5990
	movq	-40(%rbp), %rdi
	subq	-64(%rbp), %rdx
	movl	$1, %esi
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
.L5990:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5995
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5995:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6008:
	.size	_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanItEEPNS1_22StreamingParserHandlerE, .-_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanItEEPNS1_22StreamingParserHandlerE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	leaq	0(%r13,%rbx,2), %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i
	cmpb	$0, -72(%rbp)
	jne	.L5998
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L5998
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
.L5998:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L6002
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6002:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6014:
	.size	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPSt6vectorIhSaIhEE:
.LFB6016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	leaq	0(%r13,%rbx,2), %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserItE10ParseValueEPKtS7_PS7_i
	cmpb	$0, -72(%rbp)
	jne	.L6005
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L6005
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	sarq	%rdx
	movq	(%rdi), %rax
	call	*104(%rax)
.L6005:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L6009
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6009:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6016:
	.size	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanItEEPSt6vectorIhSaIhEE
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS7_PSt6vectorItSaItEE.part.0, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS7_PSt6vectorItSaItEE.part.0:
.LFB10396:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	subq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt6vectorItSaItEE7reserveEm
	cmpq	%r12, %r13
	jbe	.L6011
	leaq	.L6034(%rip), %rbx
.L6012:
	movzbl	(%r12), %edx
	leaq	1(%r12), %r15
	movw	%dx, -60(%rbp)
	movl	%edx, %eax
	cmpw	$126, %dx
	jbe	.L6013
	andl	$-32, %edx
	cmpb	$-64, %dl
	je	.L6075
	movl	%eax, %edx
	andl	$-16, %edx
	cmpb	$-32, %dl
	je	.L6076
	movl	%eax, %edx
	andl	$-8, %edx
	cmpb	$-16, %dl
	jne	.L6018
	andl	$7, %eax
	movl	$3, %edx
	movl	$3, %ecx
.L6015:
	addq	%r15, %rdx
	cmpq	%rdx, %r13
	jb	.L6018
	movzbl	1(%r12), %edi
	leaq	2(%r12), %r15
	subl	$1, %ecx
	movl	%edi, %esi
	movl	%edi, %edx
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L6018
	sall	$6, %eax
	andl	$63, %edx
	orl	%edx, %eax
	testl	%ecx, %ecx
	je	.L6020
	movzbl	2(%r12), %edi
	leaq	3(%r12), %r15
	movl	%edi, %esi
	movl	%edi, %edx
	andl	$-64, %esi
	cmpb	$-128, %sil
	jne	.L6018
	sall	$6, %eax
	andl	$63, %edx
	orl	%edx, %eax
	cmpl	$1, %ecx
	je	.L6021
	movzbl	3(%r12), %edi
	leaq	4(%r12), %r15
	movl	%edi, %ecx
	movl	%edi, %edx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L6018
	sall	$6, %eax
	andl	$63, %edx
	orl	%edx, %eax
.L6021:
	leal	-127(%rax), %edx
	movw	%di, -60(%rbp)
	cmpl	$1113984, %edx
	ja	.L6018
	movq	8(%r14), %rsi
	movq	16(%r14), %r8
	cmpl	$65534, %eax
	jbe	.L6058
	leal	-65536(%rax), %r12d
	movl	%r12d, %eax
	shrl	$10, %eax
	subw	$10240, %ax
	movw	%ax, -58(%rbp)
	cmpq	%rsi, %r8
	je	.L6026
	movw	%ax, (%rsi)
	addq	$2, %rsi
	movq	%rsi, 8(%r14)
.L6027:
	movl	%r12d, %eax
	andw	$1023, %ax
	subw	$9216, %ax
	movw	%ax, -58(%rbp)
	cmpq	%rsi, %r8
	je	.L6028
.L6071:
	movw	%ax, (%rsi)
	addq	$2, %rsi
	movq	%rsi, 8(%r14)
	jmp	.L6070
	.p2align 4,,10
	.p2align 3
.L6013:
	cmpw	$92, %dx
	jne	.L6074
	cmpq	%r15, %r13
	je	.L6018
	movzbl	1(%r12), %edx
	leaq	2(%r12), %r15
	movw	%dx, -60(%rbp)
	movl	%edx, %eax
	cmpw	$120, %dx
	je	.L6018
	cmpb	$34, %dl
	je	.L6061
	subl	$47, %eax
	cmpb	$71, %al
	ja	.L6018
	movzbl	%al, %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6034:
	.long	.L6074-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6074-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6040-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6039-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6038-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6018-.L6034
	.long	.L6037-.L6034
	.long	.L6018-.L6034
	.long	.L6036-.L6034
	.long	.L6035-.L6034
	.long	.L6033-.L6034
	.text
.L6035:
	movzbl	2(%r12), %eax
	leal	-48(%rax), %ecx
	movl	%eax, %edx
	cmpb	$9, %cl
	jbe	.L6077
	leal	-65(%rax), %ecx
	cmpb	$5, %cl
	jbe	.L6078
	subl	$97, %edx
	cmpb	$5, %dl
	ja	.L6044
	subl	$87, %eax
.L6042:
	movl	%eax, %edx
	movzbl	3(%r12), %eax
	sall	$12, %edx
	leal	-48(%rax), %esi
	movl	%eax, %ecx
	cmpb	$9, %sil
	jbe	.L6079
	leal	-65(%rax), %esi
	cmpb	$5, %sil
	jbe	.L6080
	subl	$97, %ecx
	cmpb	$5, %cl
	ja	.L6044
	subl	$87, %eax
.L6046:
	sall	$8, %eax
	addl	%edx, %eax
	movzbl	4(%r12), %edx
	leal	-48(%rdx), %esi
	movl	%edx, %ecx
	cmpb	$9, %sil
	jbe	.L6081
	leal	-65(%rdx), %esi
	cmpb	$5, %sil
	jbe	.L6082
	subl	$97, %ecx
	cmpb	$5, %cl
	ja	.L6044
	subl	$87, %edx
.L6049:
	movzbl	5(%r12), %ecx
	sall	$4, %edx
	addl	%eax, %edx
	leal	-48(%rcx), %esi
	movl	%ecx, %eax
	cmpb	$9, %sil
	jbe	.L6083
	leal	-65(%rcx), %esi
	cmpb	$5, %sil
	jbe	.L6084
	subl	$97, %eax
	cmpb	$5, %al
	ja	.L6044
	leal	-87(%rcx), %eax
.L6052:
	addl	%eax, %edx
	leaq	6(%r12), %r15
	movw	%dx, -60(%rbp)
.L6074:
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	je	.L6054
	movw	%dx, (%rsi)
	addq	$2, %rsi
	movq	%rsi, 8(%r14)
.L6070:
	movq	%r15, %r12
	cmpq	%r12, %r13
	ja	.L6012
.L6011:
	movl	$1, %eax
	jmp	.L6010
.L6018:
	xorl	%eax, %eax
.L6010:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L6085
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6020:
	.cfi_restore_state
	leal	-127(%rax), %edx
	movw	%di, -60(%rbp)
	cmpl	$1113984, %edx
	ja	.L6018
	movq	8(%r14), %rsi
	movq	16(%r14), %r8
.L6058:
	movw	%ax, -58(%rbp)
	cmpq	%r8, %rsi
	jne	.L6071
	leaq	-58(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L6070
	.p2align 4,,10
	.p2align 3
.L6076:
	andl	$15, %eax
	movl	$2, %edx
	movl	$2, %ecx
	jmp	.L6015
	.p2align 4,,10
	.p2align 3
.L6075:
	andl	$31, %eax
	movl	$1, %edx
	movl	$1, %ecx
	jmp	.L6015
	.p2align 4,,10
	.p2align 3
.L6054:
	leaq	-60(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJRKtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L6070
.L6033:
	movl	$11, %eax
	movl	$11, %edx
	movw	%ax, -60(%rbp)
	jmp	.L6074
.L6040:
	movl	$8, %r8d
	movl	$8, %edx
	movw	%r8w, -60(%rbp)
	jmp	.L6074
.L6039:
	movl	$12, %edi
	movl	$12, %edx
	movw	%di, -60(%rbp)
	jmp	.L6074
.L6038:
	movl	$10, %esi
	movl	$10, %edx
	movw	%si, -60(%rbp)
	jmp	.L6074
.L6037:
	movl	$13, %ecx
	movl	$13, %edx
	movw	%cx, -60(%rbp)
	jmp	.L6074
.L6036:
	movl	$9, %edx
	movw	%dx, -60(%rbp)
	movl	$9, %edx
	jmp	.L6074
.L6028:
	leaq	-58(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	jmp	.L6070
.L6026:
	movq	%r8, %rsi
	leaq	-58(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorItSaItEE17_M_realloc_insertIJtEEEvN9__gnu_cxx17__normal_iteratorIPtS1_EEDpOT_
	movq	8(%r14), %rsi
	movq	16(%r14), %r8
	jmp	.L6027
.L6078:
	subl	$55, %eax
	jmp	.L6042
.L6061:
	movl	$34, %edx
	jmp	.L6074
.L6077:
	subl	$48, %eax
	jmp	.L6042
.L6083:
	leal	-48(%rcx), %eax
	jmp	.L6052
.L6081:
	subl	$48, %edx
	jmp	.L6049
.L6079:
	subl	$48, %eax
	jmp	.L6046
.L6084:
	leal	-55(%rcx), %eax
	jmp	.L6052
.L6082:
	subl	$55, %edx
	jmp	.L6049
.L6080:
	subl	$55, %eax
	jmp	.L6046
.L6044:
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE8HexToIntEh.part.0
.L6085:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10396:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS7_PSt6vectorItSaItEE.part.0, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS7_PSt6vectorItSaItEE.part.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i:
.LFB7797:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$301, %r8d
	je	.L6182
	movq	%rdx, %rbx
	movl	%r8d, %r12d
	leaq	-136(%rbp), %r14
	movq	$0, -144(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	$0, -136(%rbp)
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$12, %eax
	ja	.L6090
	leaq	.L6092(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6092:
	.long	.L6100-.L6092
	.long	.L6090-.L6092
	.long	.L6099-.L6092
	.long	.L6090-.L6092
	.long	.L6098-.L6092
	.long	.L6097-.L6092
	.long	.L6096-.L6092
	.long	.L6095-.L6092
	.long	.L6094-.L6092
	.long	.L6090-.L6092
	.long	.L6090-.L6092
	.long	.L6093-.L6092
	.long	.L6091-.L6092
	.text
	.p2align 4,,10
	.p2align 3
.L6182:
	cmpb	$0, 8(%r13)
	je	.L6183
	.p2align 4,,10
	.p2align 3
.L6086:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6184
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6183:
	.cfi_restore_state
	movq	24(%r13), %r8
	subq	0(%r13), %rdi
	movl	$2, %esi
	movq	%rdi, %rdx
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*104(%rax)
	movb	$1, 8(%r13)
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6090:
	movq	-144(%rbp), %rdx
	movl	$13, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6099:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-136(%rbp), %r10
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r10, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$3, %eax
	je	.L6114
	addl	$1, %r12d
.L6116:
	movl	%r12d, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i
	cmpb	$0, 8(%r13)
	jne	.L6086
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$9, %eax
	je	.L6185
	cmpl	$3, %eax
	jne	.L6186
.L6114:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
.L6101:
	movq	-136(%rbp), %rax
	cmpq	%rbx, %rax
	jnb	.L6132
	.p2align 4,,10
	.p2align 3
.L6133:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	je	.L6134
	leal	-9(%rdx), %ecx
	cmpb	$4, %cl
	jbe	.L6134
	cmpb	$47, %dl
	jne	.L6132
	cmpq	%rax, %rbx
	je	.L6132
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L6132
	movzbl	1(%rax), %edx
	cmpb	$47, %dl
	je	.L6187
	cmpb	$42, %dl
	je	.L6188
	.p2align 4,,10
	.p2align 3
.L6132:
	movq	-152(%rbp), %rbx
	movq	%rax, (%rbx)
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6098:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	leaq	-1(%rax), %rsi
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L6144
	jnb	.L6189
.L6110:
	movq	%r13, %rdi
	movl	$6, %esi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6086
.L6181:
	call	_ZdlPv@PLT
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6100:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$1, %eax
	je	.L6117
.L6180:
	cmpl	$4, %eax
	jne	.L6130
.L6118:
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rdx
	pxor	%xmm2, %xmm2
	movq	$0, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	leaq	-1(%rax), %rsi
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rsi
	je	.L6145
	jnb	.L6190
.L6121:
	movl	$6, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
.L6123:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L6181
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6093:
	movq	-144(%rbp), %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6091:
	movq	-144(%rbp), %rdx
	movl	$3, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6097:
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	16(%r13), %rdi
	movq	-96(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-96(%rbp), %rdi
	movl	%eax, %r12d
	cmpq	%r14, %rdi
	je	.L6102
	call	_ZdlPv@PLT
.L6102:
	testb	%r12b, %r12b
	je	.L6191
	movsd	-128(%rbp), %xmm0
	comisd	.LC117(%rip), %xmm0
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	jb	.L6104
	movsd	.LC118(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L6104
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L6104
	jne	.L6104
	call	*80(%rax)
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6096:
	movq	24(%r13), %rdi
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6095:
	movq	24(%r13), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %rax
	call	*88(%rax)
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6094:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6134:
	addq	$1, %rax
.L6136:
	cmpq	%rax, %rbx
	ja	.L6133
	jmp	.L6132
	.p2align 4,,10
	.p2align 3
.L6187:
	addq	$2, %rax
	cmpq	%rax, %rbx
	jbe	.L6146
	.p2align 4,,10
	.p2align 3
.L6138:
	movzbl	(%rax), %edx
	addq	$1, %rax
	cmpb	$10, %dl
	je	.L6136
	cmpb	$13, %dl
	je	.L6136
	cmpq	%rax, %rbx
	jne	.L6138
	jmp	.L6132
	.p2align 4,,10
	.p2align 3
.L6185:
	movq	-136(%rbp), %r10
	movq	%r15, %rdx
	movq	%r10, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$3, %eax
	jne	.L6116
	movq	-144(%rbp), %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6188:
	leaq	2(%rax), %rcx
	cmpq	%rcx, %rbx
	ja	.L6139
	jmp	.L6132
	.p2align 4,,10
	.p2align 3
.L6140:
	movq	%rdx, %rcx
.L6139:
	leaq	1(%rcx), %rdx
	movzbl	-1(%rdx), %esi
	cmpq	%rdx, %rbx
	je	.L6132
	cmpb	$42, %sil
	jne	.L6140
	cmpb	$47, (%rdx)
	jne	.L6140
	leaq	2(%rcx), %rax
	jmp	.L6136
	.p2align 4,,10
	.p2align 3
.L6104:
	call	*72(%rax)
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6189:
	leaq	-128(%rbp), %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS7_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L6111
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L6109:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6101
	call	_ZdlPv@PLT
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6190:
	leaq	-128(%rbp), %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE12DecodeStringEPKhS7_PSt6vectorItSaItEE.part.0
	testb	%al, %al
	je	.L6122
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	%rdx
.L6120:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$10, %eax
	jne	.L6192
	movq	-136(%rbp), %rsi
	leal	1(%r12), %r8d
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i
	cmpb	$0, 8(%r13)
	jne	.L6123
	movq	-136(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$9, %eax
	je	.L6193
	cmpl	$1, %eax
	jne	.L6194
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6117
	call	_ZdlPv@PLT
.L6117:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6145:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L6120
	.p2align 4,,10
	.p2align 3
.L6186:
	movq	-144(%rbp), %rdx
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6193:
	movq	-136(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseTokenEPKhS7_PS7_S8_
	cmpl	$1, %eax
	je	.L6195
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6180
	movl	%eax, -156(%rbp)
	call	_ZdlPv@PLT
	movl	-156(%rbp), %eax
	cmpl	$4, %eax
	je	.L6118
.L6130:
	movq	-144(%rbp), %rdx
	movl	$9, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6191:
	movq	-144(%rbp), %rdx
	movl	$5, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6086
	.p2align 4,,10
	.p2align 3
.L6144:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L6109
.L6111:
	movq	-144(%rbp), %rdx
	jmp	.L6110
.L6146:
	movq	%rbx, %rax
	jmp	.L6132
.L6192:
	movq	-144(%rbp), %rdx
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6123
.L6194:
	movq	-144(%rbp), %rdx
	movl	$12, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6123
.L6122:
	movq	-144(%rbp), %rdx
	jmp	.L6121
.L6184:
	call	__stack_chk_fail@PLT
.L6195:
	movq	-144(%rbp), %rdx
	movl	$11, %esi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE11HandleErrorENS1_5ErrorEPKh
	jmp	.L6123
	.cfi_endproc
.LFE7797:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanIhEEPNS1_22StreamingParserHandlerE
	.type	_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanIhEEPNS1_22StreamingParserHandlerE, @function
_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanIhEEPNS1_22StreamingParserHandlerE:
.LFB6007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm1
	movq	%rdi, %xmm0
	xorl	%r8d, %r8d
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	(%rsi,%rdx), %rbx
	leaq	-72(%rbp), %rcx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	$0, -72(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i
	cmpb	$0, -56(%rbp)
	jne	.L6196
	movq	-72(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L6196
	movq	-40(%rbp), %rdi
	subq	-64(%rbp), %rdx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*104(%rax)
.L6196:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6201
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6201:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6007:
	.size	_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanIhEEPNS1_22StreamingParserHandlerE, .-_ZN4node9inspector8protocol4json9ParseJSONERKNS2_8PlatformENS1_4spanIhEEPNS1_22StreamingParserHandlerE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	addq	%r13, %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i
	cmpb	$0, -72(%rbp)
	jne	.L6204
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L6204
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*104(%rax)
.L6204:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L6208
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6208:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6013:
	.size	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE:
.LFB6015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	addq	%r13, %rbx
	subq	$96, %rsp
	movq	%rdi, -120(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	leaq	-104(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%rax, %r12
	movq	-120(%rbp), %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE(%rip), %rax
	movq	%r12, %xmm1
	movq	%r14, 8(%r12)
	movq	%rax, (%r12)
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	%rax, 40(%r12)
	movl	$0, -96(%rbp)
	movq	$-1, -88(%rbp)
	movb	$0, -72(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -104(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node9inspector8protocol4json12_GLOBAL__N_110JsonParserIhE10ParseValueEPKhS7_PS7_i
	cmpb	$0, -72(%rbp)
	jne	.L6211
	movq	-104(%rbp), %rdx
	cmpq	%rdx, %rbx
	je	.L6211
	movq	-56(%rbp), %rdi
	subq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	(%rdi), %rax
	call	*104(%rax)
.L6211:
	movq	(%r12), %rax
	movl	-96(%rbp), %ebx
	movq	%r12, %rdi
	movq	-88(%rbp), %r13
	call	*8(%rax)
	movl	%ebx, %eax
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L6215
	addq	$96, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6215:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6015:
	.size	_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol4json17ConvertJSONToCBORERKNS2_8PlatformENS1_4spanIhEEPSt6vectorIhSaIhEE
	.section	.text._ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB9824:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L6243
	movq	%rsi, %r13
	movq	%rdi, %r15
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L6228
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L6244
.L6218:
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L6227:
	movq	$0, (%rbx,%rsi)
	cmpq	%r12, %r13
	je	.L6220
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L6230
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L6230
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L6222:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L6222
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L6224
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L6224:
	leaq	16(%rbx,%rsi), %r14
.L6220:
	cmpq	%rcx, %r13
	je	.L6225
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L6225:
	testq	%r12, %r12
	je	.L6226
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L6226:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6244:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L6219
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L6227
	.p2align 4,,10
	.p2align 3
.L6228:
	movl	$8, %r14d
	jmp	.L6218
	.p2align 4,,10
	.p2align 3
.L6230:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L6221:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L6221
	jmp	.L6224
.L6219:
	cmpq	%rdx, %rdi
	cmovbe	%rdi, %rdx
	movq	%rdx, %r14
	salq	$3, %r14
	jmp	.L6218
.L6243:
	leaq	.LC103(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9824:
	.size	_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv:
.LFB9750:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L6262
	ret
	.p2align 4,,10
	.p2align 3
.L6262:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L6247
	leaq	8(%rsi), %r14
	movq	$0, (%rsi)
	movq	8(%rdi), %r12
	movq	%r14, 24(%rdi)
.L6248:
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	16(%r12), %r9
	leaq	1(%r13), %r15
	cmpq	%r9, %rax
	je	.L6255
	movq	16(%r12), %rdx
.L6249:
	cmpq	%rdx, %r15
	ja	.L6263
.L6250:
	movb	$-40, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%r9, %rax
	je	.L6256
	movq	16(%r12), %rdx
.L6251:
	cmpq	%rdx, %r15
	ja	.L6264
.L6252:
	movb	$90, (%rax,%r13)
	movq	(%r12), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %rsi
	movq	%rsi, -8(%r14)
	addq	$4, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movq	8(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L6257
	movq	16(%rbx), %rdx
.L6253:
	cmpq	%rdx, %r13
	ja	.L6265
.L6254:
	movb	$-97, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6263:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L6250
	.p2align 4,,10
	.p2align 3
.L6265:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L6254
	.p2align 4,,10
	.p2align 3
.L6264:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L6252
	.p2align 4,,10
	.p2align 3
.L6257:
	movl	$15, %edx
	jmp	.L6253
	.p2align 4,,10
	.p2align 3
.L6256:
	movl	$15, %edx
	jmp	.L6251
	.p2align 4,,10
	.p2align 3
.L6255:
	movl	$15, %edx
	jmp	.L6249
	.p2align 4,,10
	.p2align 3
.L6247:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	24(%rbx), %r14
	movq	8(%rbx), %r12
	cmpq	$0, -8(%r14)
	je	.L6248
	leaq	.LC97(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC98(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9750:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv:
.LFB9748:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L6283
	ret
	.p2align 4,,10
	.p2align 3
.L6283:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L6268
	leaq	8(%rsi), %r14
	movq	$0, (%rsi)
	movq	8(%rdi), %r12
	movq	%r14, 24(%rdi)
.L6269:
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	16(%r12), %r9
	leaq	1(%r13), %r15
	cmpq	%r9, %rax
	je	.L6276
	movq	16(%r12), %rdx
.L6270:
	cmpq	%rdx, %r15
	ja	.L6284
.L6271:
	movb	$-40, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %r13
	movq	(%r12), %rax
	leaq	1(%r13), %r15
	cmpq	%r9, %rax
	je	.L6277
	movq	16(%r12), %rdx
.L6272:
	cmpq	%rdx, %r15
	ja	.L6285
.L6273:
	movb	$90, (%rax,%r13)
	movq	(%r12), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%r13)
	movq	8(%r12), %rsi
	movq	%rsi, -8(%r14)
	addq	$4, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movq	8(%rbx), %rbx
	movq	8(%rbx), %r12
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L6278
	movq	16(%rbx), %rdx
.L6274:
	cmpq	%rdx, %r13
	ja	.L6286
.L6275:
	movb	$-65, (%rax,%r12)
	movq	(%rbx), %rax
	movq	%r13, 8(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6284:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	jmp	.L6271
	.p2align 4,,10
	.p2align 3
.L6286:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L6275
	.p2align 4,,10
	.p2align 3
.L6285:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L6273
	.p2align 4,,10
	.p2align 3
.L6278:
	movl	$15, %edx
	jmp	.L6274
	.p2align 4,,10
	.p2align 3
.L6277:
	movl	$15, %edx
	jmp	.L6272
	.p2align 4,,10
	.p2align 3
.L6276:
	movl	$15, %edx
	jmp	.L6270
	.p2align 4,,10
	.p2align 3
.L6268:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	24(%rbx), %r14
	movq	8(%rbx), %r12
	cmpq	$0, -8(%r14)
	je	.L6269
	leaq	.LC97(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC98(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9748:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv:
.LFB9762:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L6304
	ret
	.p2align 4,,10
	.p2align 3
.L6304:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L6289
	leaq	8(%rsi), %r13
	movq	$0, (%rsi)
	movq	8(%rdi), %r12
	movq	%r13, 24(%rdi)
.L6290:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L6291
	movb	$-40, (%rsi)
	movq	8(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r12)
.L6292:
	cmpq	16(%r12), %rsi
	je	.L6293
	movb	$90, (%rsi)
	movq	8(%r12), %rax
	addq	$1, %rax
	movq	%rax, 8(%r12)
.L6294:
	movq	(%r12), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	leaq	4(%rdx), %rsi
	movq	%rdx, -8(%r13)
	cmpq	%rsi, %rdx
	jnb	.L6305
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
.L6295:
	movq	8(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L6296
	movb	$-97, (%rsi)
	addq	$1, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6305:
	.cfi_restore_state
	addq	%rsi, %rcx
	cmpq	%rcx, %rax
	je	.L6295
	movq	%rcx, 8(%r12)
	jmp	.L6295
	.p2align 4,,10
	.p2align 3
.L6293:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L6294
	.p2align 4,,10
	.p2align 3
.L6291:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L6292
	.p2align 4,,10
	.p2align 3
.L6289:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	24(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	$0, -8(%r13)
	je	.L6290
	leaq	.LC124(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC98(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L6296:
	addq	$8, %rsp
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE(%rip), %rdx
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.cfi_endproc
.LFE9762:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, @function
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv:
.LFB9760:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L6323
	ret
	.p2align 4,,10
	.p2align 3
.L6323:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rsi
	cmpq	32(%rdi), %rsi
	je	.L6308
	leaq	8(%rsi), %r13
	movq	$0, (%rsi)
	movq	8(%rdi), %r12
	movq	%r13, 24(%rdi)
.L6309:
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L6310
	movb	$-40, (%rsi)
	movq	8(%r12), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r12)
.L6311:
	cmpq	16(%r12), %rsi
	je	.L6312
	movb	$90, (%rsi)
	movq	8(%r12), %rax
	addq	$1, %rax
	movq	%rax, 8(%r12)
.L6313:
	movq	(%r12), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	leaq	4(%rdx), %rsi
	movq	%rdx, -8(%r13)
	cmpq	%rsi, %rdx
	jnb	.L6324
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_default_appendEm
.L6314:
	movq	8(%rbx), %rdi
	movq	8(%rdi), %rsi
	cmpq	16(%rdi), %rsi
	je	.L6315
	movb	$-65, (%rsi)
	addq	$1, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6324:
	.cfi_restore_state
	addq	%rsi, %rcx
	cmpq	%rcx, %rax
	je	.L6314
	movq	%rcx, 8(%r12)
	jmp	.L6314
	.p2align 4,,10
	.p2align 3
.L6312:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rax
	jmp	.L6313
	.p2align 4,,10
	.p2align 3
.L6310:
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	movq	8(%r12), %rsi
	jmp	.L6311
	.p2align 4,,10
	.p2align 3
.L6308:
	leaq	16(%rdi), %rdi
	call	_ZNSt6vectorIN4node9inspector8protocol4cbor15EnvelopeEncoderESaIS4_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	24(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	$0, -8(%r13)
	je	.L6309
	leaq	.LC124(%rip), %rcx
	movl	$2195, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC98(%rip), %rdi
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L6315:
	addq	$8, %rsp
	leaq	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE(%rip), %rdx
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.cfi_endproc
.LFE9760:
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv, .-_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.section	.text._ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag
	.type	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag, @function
_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag:
.LFB9896:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	je	.L6570
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	subq	%rdx, %r13
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r13, %r11
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %r14
	movq	16(%rdi), %rax
	subq	%r14, %rax
	cmpq	%r13, %rax
	jb	.L6328
	movq	%r14, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r13
	jnb	.L6329
	movq	%r14, %r15
	movq	%rdi, -56(%rbp)
	movq	%r13, %rdx
	movq	%r14, %rdi
	subq	%r13, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	movq	-56(%rbp), %r8
	addq	%r13, 8(%r8)
	subq	%r12, %r15
	jne	.L6588
.L6330:
	testq	%r13, %r13
	jle	.L6325
	leaq	15(%r12), %rax
	subq	%rbx, %rax
	cmpq	$30, %rax
	jbe	.L6361
	leaq	-1(%r13), %rax
	cmpq	$14, %rax
	jbe	.L6361
	movq	%r13, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L6334:
	movdqu	(%rbx,%rax), %xmm2
	movups	%xmm2, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L6334
	movq	%r13, %rdx
	movq	%r13, %rax
	andq	$-16, %rdx
	addq	%rdx, %rbx
	subq	%rdx, %rax
	addq	%rdx, %r12
	cmpq	%rdx, %r13
	je	.L6325
.L6587:
	movzbl	(%rbx), %edx
	movb	%dl, (%r12)
	cmpq	$1, %rax
	je	.L6325
	movzbl	1(%rbx), %edx
	movb	%dl, 1(%r12)
	cmpq	$2, %rax
	je	.L6325
	movzbl	2(%rbx), %edx
	movb	%dl, 2(%r12)
	cmpq	$3, %rax
	je	.L6325
	movzbl	3(%rbx), %edx
	movb	%dl, 3(%r12)
	cmpq	$4, %rax
	je	.L6325
	movzbl	4(%rbx), %edx
	movb	%dl, 4(%r12)
	cmpq	$5, %rax
	je	.L6325
	movzbl	5(%rbx), %edx
	movb	%dl, 5(%r12)
	cmpq	$6, %rax
	je	.L6325
	movzbl	6(%rbx), %edx
	movb	%dl, 6(%r12)
	cmpq	$7, %rax
	je	.L6325
	movzbl	7(%rbx), %edx
	movb	%dl, 7(%r12)
	cmpq	$8, %rax
	je	.L6325
	movzbl	8(%rbx), %edx
	movb	%dl, 8(%r12)
	cmpq	$9, %rax
	je	.L6325
	movzbl	9(%rbx), %edx
	movb	%dl, 9(%r12)
	cmpq	$10, %rax
	je	.L6325
	movzbl	10(%rbx), %edx
	movb	%dl, 10(%r12)
	cmpq	$11, %rax
	je	.L6325
	movzbl	11(%rbx), %edx
	movb	%dl, 11(%r12)
	cmpq	$12, %rax
	je	.L6325
	movzbl	12(%rbx), %edx
	movb	%dl, 12(%r12)
	cmpq	$13, %rax
	je	.L6325
	movzbl	13(%rbx), %edx
	movb	%dl, 13(%r12)
	cmpq	$14, %rax
	je	.L6325
	movzbl	14(%rbx), %eax
	movb	%al, 14(%r12)
.L6325:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6328:
	.cfi_restore_state
	movq	(%rdi), %r10
	movq	%r14, %rax
	movq	%rdx, %r9
	movabsq	$9223372036854775807, %rcx
	movq	%rcx, %rdx
	subq	%r10, %rax
	subq	%rax, %rdx
	cmpq	%rdx, %r13
	ja	.L6589
	cmpq	%rax, %r13
	movq	%rax, %rdx
	cmovnb	%r13, %rdx
	addq	%rdx, %rax
	movq	%rax, %r15
	jc	.L6364
	testq	%rax, %rax
	js	.L6364
	jne	.L6352
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L6360:
	movq	%r12, %rdx
	subq	%r10, %rdx
	jne	.L6590
.L6353:
	leaq	(%rcx,%rdx), %rdi
	testq	%r13, %r13
	jle	.L6354
	cmpq	$15, %r13
	jle	.L6365
	movq	%r13, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L6356:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L6356
	movq	%r13, %rdx
	andq	$-16, %rdx
	subq	%rdx, %r13
	leaq	(%rbx,%rdx), %r9
	leaq	(%rdi,%rdx), %rax
	cmpq	%rdx, %r11
	je	.L6357
.L6355:
	movzbl	(%r9), %edx
	movb	%dl, (%rax)
	cmpq	$1, %r13
	je	.L6357
	movzbl	1(%r9), %edx
	movb	%dl, 1(%rax)
	cmpq	$2, %r13
	je	.L6357
	movzbl	2(%r9), %edx
	movb	%dl, 2(%rax)
	cmpq	$3, %r13
	je	.L6357
	movzbl	3(%r9), %edx
	movb	%dl, 3(%rax)
	cmpq	$4, %r13
	je	.L6357
	movzbl	4(%r9), %edx
	movb	%dl, 4(%rax)
	cmpq	$5, %r13
	je	.L6357
	movzbl	5(%r9), %edx
	movb	%dl, 5(%rax)
	cmpq	$6, %r13
	je	.L6357
	movzbl	6(%r9), %edx
	movb	%dl, 6(%rax)
	cmpq	$7, %r13
	je	.L6357
	movzbl	7(%r9), %edx
	movb	%dl, 7(%rax)
	cmpq	$8, %r13
	je	.L6357
	movzbl	8(%r9), %edx
	movb	%dl, 8(%rax)
	cmpq	$9, %r13
	je	.L6357
	movzbl	9(%r9), %edx
	movb	%dl, 9(%rax)
	cmpq	$10, %r13
	je	.L6357
	movzbl	10(%r9), %edx
	movb	%dl, 10(%rax)
	cmpq	$11, %r13
	je	.L6357
	movzbl	11(%r9), %edx
	movb	%dl, 11(%rax)
	cmpq	$12, %r13
	je	.L6357
	movzbl	12(%r9), %edx
	movb	%dl, 12(%rax)
	cmpq	$13, %r13
	je	.L6357
	movzbl	13(%r9), %edx
	movb	%dl, 13(%rax)
	cmpq	$14, %r13
	je	.L6357
	movzbl	14(%r9), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L6357:
	addq	%r11, %rdi
.L6354:
	subq	%r12, %r14
	jne	.L6591
.L6358:
	addq	%rdi, %r14
	testq	%r10, %r10
	je	.L6359
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
.L6359:
	movq	%r14, %xmm5
	movq	%rcx, %xmm0
	movq	%r15, 16(%r8)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, (%r8)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6570:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L6329:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	(%rdx,%r15), %rdx
	subq	%rdx, %rcx
	testq	%rcx, %rcx
	jle	.L6337
	leaq	16(%r15,%rbx), %rax
	cmpq	%rax, %r14
	leaq	16(%r14), %rax
	setnb	%sil
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %sil
	je	.L6338
	leaq	-1(%rcx), %rax
	cmpq	$14, %rax
	jbe	.L6338
	movq	%rcx, %rsi
	xorl	%eax, %eax
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L6339:
	movdqu	(%rdx,%rax), %xmm3
	movups	%xmm3, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L6339
	movq	%rcx, %rsi
	movq	%rcx, %rax
	andq	$-16, %rsi
	andl	$15, %eax
	addq	%rsi, %rdx
	addq	%rsi, %r14
	cmpq	%rsi, %rcx
	je	.L6341
	movzbl	(%rdx), %ecx
	movb	%cl, (%r14)
	cmpq	$1, %rax
	je	.L6341
	movzbl	1(%rdx), %ecx
	movb	%cl, 1(%r14)
	cmpq	$2, %rax
	je	.L6341
	movzbl	2(%rdx), %ecx
	movb	%cl, 2(%r14)
	cmpq	$3, %rax
	je	.L6341
	movzbl	3(%rdx), %ecx
	movb	%cl, 3(%r14)
	cmpq	$4, %rax
	je	.L6341
	movzbl	4(%rdx), %ecx
	movb	%cl, 4(%r14)
	cmpq	$5, %rax
	je	.L6341
	movzbl	5(%rdx), %ecx
	movb	%cl, 5(%r14)
	cmpq	$6, %rax
	je	.L6341
	movzbl	6(%rdx), %ecx
	movb	%cl, 6(%r14)
	cmpq	$7, %rax
	je	.L6341
	movzbl	7(%rdx), %ecx
	movb	%cl, 7(%r14)
	cmpq	$8, %rax
	je	.L6341
	movzbl	8(%rdx), %ecx
	movb	%cl, 8(%r14)
	cmpq	$9, %rax
	je	.L6341
	movzbl	9(%rdx), %ecx
	movb	%cl, 9(%r14)
	cmpq	$10, %rax
	je	.L6341
	movzbl	10(%rdx), %ecx
	movb	%cl, 10(%r14)
	cmpq	$11, %rax
	je	.L6341
	movzbl	11(%rdx), %ecx
	movb	%cl, 11(%r14)
	cmpq	$12, %rax
	je	.L6341
	movzbl	12(%rdx), %ecx
	movb	%cl, 12(%r14)
	cmpq	$13, %rax
	je	.L6341
	movzbl	13(%rdx), %ecx
	movb	%cl, 13(%r14)
	cmpq	$14, %rax
	je	.L6341
	movzbl	14(%rdx), %eax
	movb	%al, 14(%r14)
	.p2align 4,,10
	.p2align 3
.L6341:
	movq	8(%r8), %r14
.L6337:
	subq	%r15, %r13
	leaq	(%r14,%r13), %rdi
	movq	%rdi, 8(%r8)
	testq	%r15, %r15
	je	.L6325
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
	addq	%r15, 8(%r8)
	testq	%r15, %r15
	jle	.L6325
	leaq	15(%r12), %rax
	subq	%rbx, %rax
	cmpq	$30, %rax
	jbe	.L6362
	leaq	-1(%r15), %rax
	cmpq	$14, %rax
	jbe	.L6362
	movq	%r15, %rdx
	xorl	%eax, %eax
	andq	$-16, %rdx
	.p2align 4,,10
	.p2align 3
.L6346:
	movdqu	(%rbx,%rax), %xmm4
	movups	%xmm4, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L6346
	movq	%r15, %rdx
	movq	%r15, %rax
	andq	$-16, %rdx
	andl	$15, %eax
	addq	%rdx, %rbx
	addq	%rdx, %r12
	cmpq	%rdx, %r15
	jne	.L6587
	jmp	.L6325
	.p2align 4,,10
	.p2align 3
.L6364:
	movq	%rcx, %r15
.L6352:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r9
	movq	%rax, %rcx
	addq	%rax, %r15
	movq	(%r8), %r10
	movq	8(%r8), %r14
	jmp	.L6360
	.p2align 4,,10
	.p2align 3
.L6588:
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	subq	%r15, %rdi
	call	memmove@PLT
	jmp	.L6330
	.p2align 4,,10
	.p2align 3
.L6591:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r10
	movq	%rax, %rdi
	jmp	.L6358
	.p2align 4,,10
	.p2align 3
.L6590:
	movq	%r10, %rsi
	movq	%rcx, %rdi
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %rdx
	movq	%rax, %rcx
	movq	-56(%rbp), %r10
	jmp	.L6353
	.p2align 4,,10
	.p2align 3
.L6338:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6342:
	movzbl	(%rdx,%rax), %esi
	movb	%sil, (%r14,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L6342
	jmp	.L6341
	.p2align 4,,10
	.p2align 3
.L6362:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6344:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L6344
	jmp	.L6325
	.p2align 4,,10
	.p2align 3
.L6361:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6332:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L6332
	jmp	.L6325
.L6365:
	movq	%rdi, %rax
	jmp	.L6355
.L6589:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9896:
	.size	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag, .-_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei, @function
_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei:
.LFB9744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L6592
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	movl	%esi, %r13d
	cmpq	88(%rdi), %r12
	je	.L6608
	movl	-8(%r12), %edx
	movl	-4(%r12), %eax
	testl	%edx, %edx
	je	.L6609
.L6595:
	testl	%eax, %eax
	je	.L6596
	cmpl	$2, %edx
	je	.L6600
	testb	$1, %al
	jne	.L6610
.L6600:
	movl	$44, %eax
.L6597:
	movq	16(%rbx), %rdi
	leaq	-81(%rbp), %rsi
	movb	%al, -81(%rbp)
	call	_ZNSt6vectorIhSaIhEE12emplace_backIJhEEEvDpOT_
	movl	-4(%r12), %eax
	jmp	.L6596
	.p2align 4,,10
	.p2align 3
.L6609:
	testl	%eax, %eax
	jne	.L6611
.L6596:
	addl	$1, %eax
	leaq	-80(%rbp), %rdi
	movl	%r13d, %r8d
	movl	$16, %edx
	movl	%eax, -4(%r12)
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC30(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	16(%rbx), %rdi
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	8(%rdi), %rsi
	addq	%rdx, %rcx
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEEvNS4_IPhS1_EET_SG_St20forward_iterator_tag
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L6592
	call	_ZdlPv@PLT
.L6592:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6612
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6608:
	.cfi_restore_state
	movq	104(%rdi), %rax
	movq	-8(%rax), %r12
	movl	504(%r12), %edx
	addq	$512, %r12
	movl	-4(%r12), %eax
	testl	%edx, %edx
	jne	.L6595
	jmp	.L6609
	.p2align 4,,10
	.p2align 3
.L6610:
	movl	$58, %eax
	jmp	.L6597
.L6612:
	call	__stack_chk_fail@PLT
.L6611:
	leaq	.LC106(%rip), %rcx
	movl	$2928, %edx
	leaq	.LC2(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	call	__assert_fail@PLT
	.cfi_endproc
.LFE9744:
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei, .-_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.weak	_ZTVN4node9inspector8protocol5ValueE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol5ValueE,"awG",@progbits,_ZTVN4node9inspector8protocol5ValueE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol5ValueE, @object
	.size	_ZTVN4node9inspector8protocol5ValueE, 112
_ZTVN4node9inspector8protocol5ValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol5ValueD1Ev
	.quad	_ZN4node9inspector8protocol5ValueD0Ev
	.quad	_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK4node9inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK4node9inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE
	.quad	_ZNK4node9inspector8protocol5Value9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol5Value11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK4node9inspector8protocol5Value5cloneEv
	.weak	_ZTVN4node9inspector8protocol16FundamentalValueE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol16FundamentalValueE,"awG",@progbits,_ZTVN4node9inspector8protocol16FundamentalValueE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol16FundamentalValueE, @object
	.size	_ZTVN4node9inspector8protocol16FundamentalValueE, 112
_ZTVN4node9inspector8protocol16FundamentalValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol16FundamentalValueD1Ev
	.quad	_ZN4node9inspector8protocol16FundamentalValueD0Ev
	.quad	_ZNK4node9inspector8protocol16FundamentalValue9asBooleanEPb
	.quad	_ZNK4node9inspector8protocol16FundamentalValue8asDoubleEPd
	.quad	_ZNK4node9inspector8protocol16FundamentalValue9asIntegerEPi
	.quad	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE
	.quad	_ZNK4node9inspector8protocol16FundamentalValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol16FundamentalValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK4node9inspector8protocol16FundamentalValue5cloneEv
	.weak	_ZTVN4node9inspector8protocol11StringValueE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol11StringValueE,"awG",@progbits,_ZTVN4node9inspector8protocol11StringValueE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol11StringValueE, @object
	.size	_ZTVN4node9inspector8protocol11StringValueE, 112
_ZTVN4node9inspector8protocol11StringValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol11StringValueD1Ev
	.quad	_ZN4node9inspector8protocol11StringValueD0Ev
	.quad	_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK4node9inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK4node9inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK4node9inspector8protocol11StringValue8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE
	.quad	_ZNK4node9inspector8protocol11StringValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol11StringValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK4node9inspector8protocol11StringValue5cloneEv
	.weak	_ZTVN4node9inspector8protocol11BinaryValueE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol11BinaryValueE,"awG",@progbits,_ZTVN4node9inspector8protocol11BinaryValueE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol11BinaryValueE, @object
	.size	_ZTVN4node9inspector8protocol11BinaryValueE, 112
_ZTVN4node9inspector8protocol11BinaryValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol11BinaryValueD1Ev
	.quad	_ZN4node9inspector8protocol11BinaryValueD0Ev
	.quad	_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK4node9inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK4node9inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol11BinaryValue8asBinaryEPNS1_6BinaryE
	.quad	_ZNK4node9inspector8protocol11BinaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol11BinaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK4node9inspector8protocol11BinaryValue5cloneEv
	.weak	_ZTVN4node9inspector8protocol15SerializedValueE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol15SerializedValueE,"awG",@progbits,_ZTVN4node9inspector8protocol15SerializedValueE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol15SerializedValueE, @object
	.size	_ZTVN4node9inspector8protocol15SerializedValueE, 112
_ZTVN4node9inspector8protocol15SerializedValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol15SerializedValueD1Ev
	.quad	_ZN4node9inspector8protocol15SerializedValueD0Ev
	.quad	_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK4node9inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK4node9inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE
	.quad	_ZNK4node9inspector8protocol15SerializedValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol15SerializedValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK4node9inspector8protocol15SerializedValue5cloneEv
	.weak	_ZTVN4node9inspector8protocol15DictionaryValueE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol15DictionaryValueE,"awG",@progbits,_ZTVN4node9inspector8protocol15DictionaryValueE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol15DictionaryValueE, @object
	.size	_ZTVN4node9inspector8protocol15DictionaryValueE, 112
_ZTVN4node9inspector8protocol15DictionaryValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol15DictionaryValueD1Ev
	.quad	_ZN4node9inspector8protocol15DictionaryValueD0Ev
	.quad	_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK4node9inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK4node9inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE
	.quad	_ZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol15DictionaryValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK4node9inspector8protocol15DictionaryValue5cloneEv
	.weak	_ZTVN4node9inspector8protocol9ListValueE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol9ListValueE,"awG",@progbits,_ZTVN4node9inspector8protocol9ListValueE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol9ListValueE, @object
	.size	_ZTVN4node9inspector8protocol9ListValueE, 112
_ZTVN4node9inspector8protocol9ListValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol5Value15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol5Value17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol9ListValueD1Ev
	.quad	_ZN4node9inspector8protocol9ListValueD0Ev
	.quad	_ZNK4node9inspector8protocol5Value9asBooleanEPb
	.quad	_ZNK4node9inspector8protocol5Value8asDoubleEPd
	.quad	_ZNK4node9inspector8protocol5Value9asIntegerEPi
	.quad	_ZNK4node9inspector8protocol5Value8asStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol5Value8asBinaryEPNS1_6BinaryE
	.quad	_ZNK4node9inspector8protocol9ListValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE
	.quad	_ZNK4node9inspector8protocol9ListValue11writeBinaryEPSt6vectorIhSaIhEE
	.quad	_ZNK4node9inspector8protocol9ListValue5cloneEv
	.weak	_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol14DispatcherBase8CallbackE,"awG",@progbits,_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE, @object
	.size	_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE, 32
_ZTVN4node9inspector8protocol14DispatcherBase8CallbackE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol14DispatcherBase8CallbackD1Ev
	.quad	_ZN4node9inspector8protocol14DispatcherBase8CallbackD0Ev
	.weak	_ZTVN4node9inspector8protocol14DispatcherBaseE
	.section	.data.rel.ro._ZTVN4node9inspector8protocol14DispatcherBaseE,"awG",@progbits,_ZTVN4node9inspector8protocol14DispatcherBaseE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol14DispatcherBaseE, @object
	.size	_ZTVN4node9inspector8protocol14DispatcherBaseE, 48
_ZTVN4node9inspector8protocol14DispatcherBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorE, 48
_ZTVN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError15serializeToJSONEv
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolError17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_113ProtocolErrorD0Ev
	.weak	_ZTVN4node9inspector8protocol14UberDispatcherE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol14UberDispatcherE,"awG",@progbits,_ZTVN4node9inspector8protocol14UberDispatcherE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol14UberDispatcherE, @object
	.size	_ZTVN4node9inspector8protocol14UberDispatcherE, 32
_ZTVN4node9inspector8protocol14UberDispatcherE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol14UberDispatcherD1Ev
	.quad	_ZN4node9inspector8protocol14UberDispatcherD0Ev
	.weak	_ZTVN4node9inspector8protocol16InternalResponseE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol16InternalResponseE,"awG",@progbits,_ZTVN4node9inspector8protocol16InternalResponseE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol16InternalResponseE, @object
	.size	_ZTVN4node9inspector8protocol16InternalResponseE, 48
_ZTVN4node9inspector8protocol16InternalResponseE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol16InternalResponse15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol16InternalResponse17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol16InternalResponseD1Ev
	.quad	_ZN4node9inspector8protocol16InternalResponseD0Ev
	.section	.data.rel.ro.local
	.align 8
	.type	_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE, @object
	.size	_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE, 128
_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED1Ev
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEED0Ev
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE10HandleNullEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE
	.align 8
	.type	_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 128
_ZTVN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.quad	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_111CBOREncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE
	.align 8
	.type	_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE, @object
	.size	_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE, 128
_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED1Ev
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEED0Ev
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleMapBeginEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleMapEndEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE16HandleArrayBeginEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleArrayEndEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE13HandleString8ENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE14HandleString16ENS1_4spanItEE
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleBinaryENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE12HandleDoubleEd
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleInt32Ei
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleBoolEb
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE10HandleNullEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderISt6vectorIhSaIhEEE11HandleErrorENS1_6StatusE
	.align 8
	.type	_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 128
_ZTVN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleMapBeginEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleMapEndEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE16HandleArrayBeginEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleArrayEndEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE13HandleString8ENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE14HandleString16ENS1_4spanItEE
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleBinaryENS1_4spanIhEE
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE12HandleDoubleEd
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleInt32Ei
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleBoolEb
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10HandleNullEv
	.quad	_ZN4node9inspector8protocol4json12_GLOBAL__N_111JSONEncoderINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11HandleErrorENS1_6StatusE
	.section	.rodata
	.align 32
	.type	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE, @object
	.size	_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE, 65
_ZN4node9inspector8protocol4json12_GLOBAL__N_1L12kBase64TableE:
	.string	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L30kExpectedConversionToBase64TagE:
	.byte	-42
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L21kInitialByteForDoubleE:
	.byte	-5
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedNullE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedNullE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedNullE:
	.byte	-10
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L13kEncodedFalseE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L13kEncodedFalseE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L13kEncodedFalseE:
	.byte	-12
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedTrueE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedTrueE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L12kEncodedTrueE:
	.byte	-11
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L9kStopByteE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L9kStopByteE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L9kStopByteE:
	.byte	-1
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L31kInitialByteIndefiniteLengthMapE:
	.byte	-65
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L33kInitialByteIndefiniteLengthArrayE:
	.byte	-97
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L36kInitialByteFor32BitLengthByteStringE:
	.byte	90
	.type	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE, @object
	.size	_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE, 1
_ZN4node9inspector8protocol4cbor12_GLOBAL__N_1L23kInitialByteForEnvelopeE:
	.byte	-40
	.globl	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE
	.align 16
	.type	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE, @object
	.size	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE, 19
_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE:
	.string	"Invalid parameters"
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/src/node/inspector/protocol/Protocol.cpp:664"
	.section	.rodata.str1.1
.LC133:
	.string	"it != m_data.end()"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"virtual void node::inspector::protocol::DictionaryValue::writeJSON(node::inspector::protocol::StringBuilder*) const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZNK4node9inspector8protocol15DictionaryValue9writeJSONEPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.section	.rodata
	.align 16
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_1L9hexDigitsE, @object
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_1L9hexDigitsE, 17
_ZN4node9inspector8protocol12_GLOBAL__N_1L9hexDigitsE:
	.string	"0123456789ABCDEF"
	.weak	_ZZN4node9inspector8protocol6Binary8fromSpanEPKhmE4args
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"../src/inspector/node_string.h:98"
	.section	.rodata.str1.1
.LC136:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"static node::inspector::protocol::Binary node::inspector::protocol::Binary::fromSpan(const uint8_t*, size_t)"
	.section	.data.rel.ro.local._ZZN4node9inspector8protocol6Binary8fromSpanEPKhmE4args,"awG",@progbits,_ZZN4node9inspector8protocol6Binary8fromSpanEPKhmE4args,comdat
	.align 16
	.type	_ZZN4node9inspector8protocol6Binary8fromSpanEPKhmE4args, @gnu_unique_object
	.size	_ZZN4node9inspector8protocol6Binary8fromSpanEPKhmE4args, 24
_ZZN4node9inspector8protocol6Binary8fromSpanEPKhmE4args:
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.weak	_ZZNK4node9inspector8protocol6Binary8toBase64B5cxx11EvE4args
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"../src/inspector/node_string.h:94"
	.align 8
.LC139:
	.string	"node::inspector::protocol::String node::inspector::protocol::Binary::toBase64() const"
	.section	.data.rel.ro.local._ZZNK4node9inspector8protocol6Binary8toBase64B5cxx11EvE4args,"awG",@progbits,_ZZNK4node9inspector8protocol6Binary8toBase64B5cxx11EvE4args,comdat
	.align 16
	.type	_ZZNK4node9inspector8protocol6Binary8toBase64B5cxx11EvE4args, @gnu_unique_object
	.size	_ZZNK4node9inspector8protocol6Binary8toBase64B5cxx11EvE4args, 24
_ZZNK4node9inspector8protocol6Binary8toBase64B5cxx11EvE4args:
	.quad	.LC138
	.quad	.LC136
	.quad	.LC139
	.weak	_ZZNK4node9inspector8protocol6Binary4sizeEvE4args
	.section	.rodata.str1.8
	.align 8
.LC140:
	.string	"../src/inspector/node_string.h:93"
	.align 8
.LC141:
	.string	"size_t node::inspector::protocol::Binary::size() const"
	.section	.data.rel.ro.local._ZZNK4node9inspector8protocol6Binary4sizeEvE4args,"awG",@progbits,_ZZNK4node9inspector8protocol6Binary4sizeEvE4args,comdat
	.align 16
	.type	_ZZNK4node9inspector8protocol6Binary4sizeEvE4args, @gnu_unique_object
	.size	_ZZNK4node9inspector8protocol6Binary4sizeEvE4args, 24
_ZZNK4node9inspector8protocol6Binary4sizeEvE4args:
	.quad	.LC140
	.quad	.LC136
	.quad	.LC141
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC28:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC29:
	.long	4294967295
	.long	2146435071
	.section	.data.rel.ro,"aw"
	.align 8
.LC32:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC33:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC34:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC57:
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC102:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.section	.rodata.cst8
	.align 8
.LC117:
	.long	0
	.long	-1042284544
	.align 8
.LC118:
	.long	4290772992
	.long	1105199103
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
