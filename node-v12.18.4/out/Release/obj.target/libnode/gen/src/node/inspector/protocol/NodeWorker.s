	.file	"NodeWorker.cpp"
	.text
	.section	.text._ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev:
.LFB4828:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movb	$0, 24(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4828:
	.size	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.section	.text._ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4829:
	.cfi_startproc
	endbr64
	movdqu	40(%rsi), %xmm1
	movq	56(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 56(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 40(%rsi)
	ret
	.cfi_endproc
.LFE4829:
	.size	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev:
.LFB4859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	40(%rbx), %rdi
	leaq	56(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4859:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD1Ev
	.set	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD1Ev,_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD2Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD2Ev
	.type	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD2Ev, @function
_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD2Ev:
.LFB4925:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L13
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	ret
	.cfi_endproc
.LFE4925:
	.size	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD2Ev, .-_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD2Ev
	.weak	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD1Ev
	.set	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD1Ev,_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD2Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev
	.type	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev, @function
_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev:
.LFB4927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4927:
	.size	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev, .-_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD2Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD2Ev
	.type	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD2Ev, @function
_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD2Ev:
.LFB4950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	leaq	56(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L18
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4950:
	.size	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD2Ev, .-_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD2Ev
	.weak	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD1Ev
	.set	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD1Ev,_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD2Ev
	.section	.text._ZN4node9inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN4node9inspector8protocol23InternalRawNotificationD2Ev:
.LFB4825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L22
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4825:
	.size	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev, .-_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev,_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker14DispatcherImplD2Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD2Ev
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD2Ev, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD2Ev:
.LFB5057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L34
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L60:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L30
.L33:
	movq	%rbx, %r13
.L34:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L60
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L33
.L30:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L39
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L61:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L36
.L38:
	movq	%rbx, %r13
.L39:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L61
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L38
.L36:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE5057:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD2Ev, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD2Ev
	.weak	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD1Ev
	.set	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD1Ev,_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD2Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev
	.type	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev, @function
_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev:
.LFB4952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4952:
	.size	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev, .-_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev
	.section	.text._ZN4node9inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN4node9inspector8protocol23InternalRawNotificationD0Ev:
.LFB4827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4827:
	.size	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev, .-_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.section	.text._ZN4node9inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD0Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD0Ev, @function
_ZN4node9inspector8protocol16InternalResponseD0Ev:
.LFB4818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L74
	movq	(%rdi), %rax
	call	*24(%rax)
.L74:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4818:
	.size	_ZN4node9inspector8protocol16InternalResponseD0Ev, .-_ZN4node9inspector8protocol16InternalResponseD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl7disableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl7disableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl7disableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB5072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	cmpl	$2, -112(%rbp)
	je	.L93
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L82
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE@PLT
.L82:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L80
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L82
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5072:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl7disableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl7disableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.text._ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev:
.LFB5059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L100
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L126:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L96
.L99:
	movq	%rbx, %r13
.L100:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L126
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L99
.L96:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L105
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L127:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L102
.L104:
	movq	%rbx, %r13
.L105:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L127
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L104
.L102:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5059:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev:
.LFB4861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4861:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev
	.type	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev, @function
_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev:
.LFB4895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	40(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L135
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L136
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	104(%r12), %rdi
	movq	%rax, (%r12)
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L138
	call	_ZdlPv@PLT
.L138:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L139
	call	_ZdlPv@PLT
.L139:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L135:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	popq	%r12
	movq	%r13, %rdi
	movl	$56, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L135
	.cfi_endproc
.LFE4895:
	.size	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev, .-_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev
	.section	.text._ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD2Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD2Ev
	.type	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD2Ev, @function
_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD2Ev:
.LFB4893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L147
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L148
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	104(%r12), %rdi
	movq	%rax, (%r12)
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L147:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L146
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L147
	.cfi_endproc
.LFE4893:
	.size	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD2Ev, .-_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD2Ev
	.weak	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD1Ev
	.set	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD1Ev,_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	80(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	72(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L165
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	56(%rbx), %rcx
.L161:
	cmpq	%rcx, %r13
	je	.L168
.L160:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L165
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L161
.L165:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L160
	testq	%rdx, %rdx
	je	.L164
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L160
.L164:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5061:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"waitForDebuggerOnStart"
.LC1:
	.string	"boolean value expected"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6enableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6enableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6enableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB5071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movl	%esi, -132(%rbp)
	movq	(%r8), %rdi
	movq	%r14, %rsi
	movq	%rcx, -160(%rbp)
	movl	$29549, %ecx
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -112(%rbp)
	movl	$1634886000, -96(%rbp)
	movw	%cx, -92(%rbp)
	movq	$6, -104(%rbp)
	movb	$0, -90(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L171
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	jne	.L171
	cmpq	%r12, %rdi
	je	.L205
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-144(%rbp), %r8
.L191:
	leaq	-120(%rbp), %r15
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r8, -144(%rbp)
	movq	%r15, %rsi
	movq	%r12, -112(%rbp)
	movq	$22, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %rdx
	movdqa	.LC2(%rip), %xmm0
	movq	%r14, %rsi
	movq	%rax, -112(%rbp)
	movq	-144(%rbp), %r8
	movq	%rdx, -96(%rbp)
	movl	$29810, %edx
	movl	$1635013486, 16(%rax)
	movq	%r8, %rdi
	movw	%dx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r12, %rdi
	je	.L176
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L176:
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-144(%rbp), %r8
	movb	$0, -120(%rbp)
	testq	%r8, %r8
	je	.L177
	movq	(%r8), %rax
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L179
.L177:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L179:
	movq	%rbx, %rdi
	movzbl	-120(%rbp), %r15d
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L206
	leaq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movzbl	%r15b, %edx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	cmpl	$2, -112(%rbp)
	je	.L207
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L189
	movl	-132(%rbp), %esi
	movq	%r14, %rdx
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE@PLT
.L189:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L190
	call	_ZdlPv@PLT
.L190:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L169
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-144(%rbp), %r8
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L171:
	cmpq	%r12, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movb	$0, -120(%rbp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rdi
	movq	%r12, -112(%rbp)
	call	strlen@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L209
	cmpq	$1, %rax
	jne	.L184
	movzbl	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %edx
	movb	%dl, -96(%rbp)
	movq	%r12, %rdx
.L185:
	movq	%rax, -104(%rbp)
	movl	-132(%rbp), %esi
	movq	%r13, %rdi
	movq	%rbx, %r8
	movb	$0, (%rdx,%rax)
	movq	%r14, %rcx
	movl	$-32602, %edx
	call	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L169
	call	_ZdlPv@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L184:
	testq	%rax, %rax
	jne	.L210
	movq	%r12, %rdx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L207:
	movq	8(%r13), %rdi
	movq	-160(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movl	-132(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r14, %rdi
	leaq	-120(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
.L183:
	movq	%r15, %rdx
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	jmp	.L185
.L208:
	call	__stack_chk_fail@PLT
.L210:
	movq	%r12, %rdi
	jmp	.L183
	.cfi_endproc
.LFE5071:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6enableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6enableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.rodata.str1.1
.LC3:
	.string	"sessionId"
.LC4:
	.string	"string value expected"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6detachEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6detachEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6detachEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB5073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$152, %rsp
	movl	%esi, -164(%rbp)
	movq	(%r8), %rdi
	movq	%r14, %rsi
	movq	%rdx, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$29549, %eax
	movq	%r12, -112(%rbp)
	movl	$1634886000, -96(%rbp)
	movw	%ax, -92(%rbp)
	movq	$6, -104(%rbp)
	movb	$0, -90(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	testq	%rax, %rax
	je	.L213
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	jne	.L213
	cmpq	%r12, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, -112(%rbp)
	movabsq	$5291289110906103155, %rax
	movb	$100, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	$9, -104(%rbp)
	movb	$0, -87(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r12, %rdi
	je	.L218
	movq	%rax, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %r8
.L218:
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -176(%rbp)
	leaq	-128(%rbp), %r15
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-176(%rbp), %r8
	movb	$0, -128(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%r15, -144(%rbp)
	movq	$0, -136(%rbp)
	testq	%r8, %r8
	je	.L219
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	je	.L219
.L221:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L250
	leaq	-152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	leaq	-144(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*40(%rax)
	cmpl	$2, -112(%rbp)
	je	.L251
	movq	-152(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L231
	movl	-164(%rbp), %esi
	movq	%r14, %rdx
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE@PLT
.L231:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L232
.L255:
	call	_ZdlPv@PLT
.L232:
	movq	-152(%rbp), %r12
	testq	%r12, %r12
	je	.L229
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L229:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	cmpq	%r12, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r15
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r15, -144(%rbp)
	movq	$0, -136(%rbp)
	movb	$0, -128(%rbp)
.L219:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rdi
	movq	%r12, -112(%rbp)
	call	strlen@PLT
	movq	%rax, -152(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L253
	cmpq	$1, %rax
	jne	.L226
	movzbl	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %edx
	movb	%dl, -96(%rbp)
	movq	%r12, %rdx
.L227:
	movq	%rax, -104(%rbp)
	movl	-164(%rbp), %esi
	movq	%r13, %rdi
	movq	%rbx, %r8
	movb	$0, (%rdx,%rax)
	movq	%r14, %rcx
	movl	$-32602, %edx
	call	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L229
	call	_ZdlPv@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L226:
	testq	%rax, %rax
	jne	.L254
	movq	%r12, %rdx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L251:
	movq	8(%r13), %rdi
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movl	-164(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L255
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r14, %rdi
	leaq	-152(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %r8
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	%rax, -96(%rbp)
.L225:
	movq	%r8, %rdx
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %rax
	movq	-112(%rbp), %rdx
	jmp	.L227
.L252:
	call	__stack_chk_fail@PLT
.L254:
	movq	%r12, %rdi
	jmp	.L225
	.cfi_endproc
.LFE5073:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6detachEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6detachEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.rodata.str1.1
.LC5:
	.string	"message"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl19sendMessageToWorkerEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl19sendMessageToWorkerEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl19sendMessageToWorkerEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB5064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movl	%esi, -204(%rbp)
	movq	(%r8), %rdi
	movq	%r15, %rsi
	movq	%rdx, -224(%rbp)
	movl	$29549, %edx
	movq	%rcx, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -112(%rbp)
	movl	$1634886000, -96(%rbp)
	movw	%dx, -92(%rbp)
	movq	$6, -104(%rbp)
	movb	$0, -90(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testq	%rax, %rax
	je	.L257
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r13
	jne	.L258
	cmpq	%r12, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$26465, %eax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, -112(%rbp)
	movl	$1936942445, -96(%rbp)
	movw	%ax, -92(%rbp)
	movb	$101, -90(%rbp)
	movq	$7, -104(%rbp)
	movb	$0, -89(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r12, %rdi
	je	.L262
	movq	%rax, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r8
.L262:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-216(%rbp), %r8
	leaq	-160(%rbp), %rax
	movq	$0, -168(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movb	$0, -160(%rbp)
	testq	%r8, %r8
	je	.L264
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	je	.L264
.L265:
	movabsq	$5291289110906103155, %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, -112(%rbp)
	movq	%rax, -96(%rbp)
	movb	$100, -88(%rbp)
	movq	$9, -104(%rbp)
	movb	$0, -87(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%r12, %rdi
	je	.L266
	movq	%rax, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %r8
.L266:
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r8, -216(%rbp)
	leaq	-128(%rbp), %r13
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-216(%rbp), %r8
	movb	$0, -128(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%r13, -144(%rbp)
	movq	$0, -136(%rbp)
	testq	%r8, %r8
	je	.L263
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	je	.L263
.L268:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L304
	leaq	-184(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r14), %rsi
	leaq	-144(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-176(%rbp), %rdx
	movq	(%rsi), %rax
	call	*16(%rax)
	cmpl	$2, -112(%rbp)
	je	.L305
	movq	-184(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L278
	movl	-204(%rbp), %esi
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE@PLT
.L278:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L279
.L310:
	call	_ZdlPv@PLT
.L279:
	movq	-184(%rbp), %r12
	testq	%r12, %r12
	je	.L276
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L276:
	movq	-144(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	-112(%rbp), %rdi
.L258:
	cmpq	%r12, %rdi
	je	.L307
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
.L260:
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r13
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-160(%rbp), %rax
	movq	%rbx, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -160(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%r13, -144(%rbp)
	movq	$0, -136(%rbp)
	movb	$0, -128(%rbp)
.L263:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rdi
	movq	%r12, -112(%rbp)
	call	strlen@PLT
	movq	%rax, -184(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L308
	cmpq	$1, %rax
	jne	.L273
	movzbl	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %edx
	movb	%dl, -96(%rbp)
	movq	%r12, %rdx
.L274:
	movq	%rax, -104(%rbp)
	movl	-204(%rbp), %esi
	movq	%r14, %rdi
	movq	%rbx, %r8
	movb	$0, (%rdx,%rax)
	movq	%r15, %rcx
	movl	$-32602, %edx
	call	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L276
	call	_ZdlPv@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L273:
	testq	%rax, %rax
	jne	.L309
	movq	%r12, %rdx
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L305:
	movq	8(%r14), %rdi
	movq	-232(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movl	-204(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L310
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r15, %rdi
	leaq	-184(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -216(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %r8
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-184(%rbp), %rax
	movq	%rax, -96(%rbp)
.L272:
	movq	%r8, %rdx
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	call	memcpy@PLT
	movq	-184(%rbp), %rax
	movq	-112(%rbp), %rdx
	jmp	.L274
.L306:
	call	__stack_chk_fail@PLT
.L309:
	movq	%r12, %rdi
	jmp	.L272
	.cfi_endproc
.LFE5064:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl19sendMessageToWorkerEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl19sendMessageToWorkerEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.text._ZN4node9inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD2Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD2Ev, @function
_ZN4node9inspector8protocol16InternalResponseD2Ev:
.LFB4816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L312
	movq	(%rdi), %rax
	call	*24(%rax)
.L312:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L311
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4816:
	.size	_ZN4node9inspector8protocol16InternalResponseD2Ev, .-_ZN4node9inspector8protocol16InternalResponseD2Ev
	.weak	_ZN4node9inspector8protocol16InternalResponseD1Ev
	.set	_ZN4node9inspector8protocol16InternalResponseD1Ev,_ZN4node9inspector8protocol16InternalResponseD2Ev
	.section	.rodata.str1.1
.LC6:
	.string	"object expected"
.LC7:
	.string	"workerId"
.LC8:
	.string	"type"
.LC9:
	.string	"title"
.LC10:
	.string	"url"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfo9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE:
.LFB4985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L319
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r13
	je	.L320
.L319:
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r15)
.L318:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$104, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movl	$136, %edi
	leaq	-80(%rbp), %rbx
	call	_Znwm@PLT
	cmpl	$6, 8(%r13)
	movq	%r14, %rdi
	movq	%rax, %r12
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	movq	%rax, -112(%rbp)
	movq	%rax, 8(%r12)
	leaq	56(%r12), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, 40(%r12)
	leaq	88(%r12), %rax
	movq	%rax, -128(%rbp)
	movq	%rax, 72(%r12)
	leaq	120(%r12), %rax
	movq	%rax, -136(%rbp)
	movq	%rax, 104(%r12)
	movl	$0, %eax
	cmovne	%rax, %r13
	movb	$0, 24(%r12)
	movq	$0, 16(%r12)
	movq	$0, 48(%r12)
	movb	$0, 56(%r12)
	movq	$0, 80(%r12)
	movb	$0, 88(%r12)
	movq	$0, 112(%r12)
	movb	$0, 120(%r12)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	-96(%rbp), %rax
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movabsq	$7226432857012596599, %rcx
	movq	%rax, %rsi
	movq	%rax, -104(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$8, -88(%rbp)
	movb	$0, -72(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L323
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L323:
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-144(%rbp), %r8
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r8, %r8
	je	.L326
	movq	(%r8), %rax
	movq	-104(%rbp), %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L325
.L326:
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L325:
	movq	-96(%rbp), %rax
	movq	8(%r12), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L419
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -112(%rbp)
	je	.L420
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r12), %rsi
	movq	%rax, 8(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	testq	%rdi, %rdi
	je	.L332
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L330:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	-104(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movl	$1701869940, -80(%rbp)
	movq	$4, -88(%rbp)
	movb	$0, -76(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L334
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L334:
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-144(%rbp), %r8
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r8, %r8
	je	.L337
	movq	(%r8), %rax
	movq	-104(%rbp), %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L336
.L337:
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L336:
	movq	-96(%rbp), %rax
	movq	40(%r12), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L421
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -120(%rbp)
	je	.L422
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	56(%r12), %rsi
	movq	%rax, 40(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 48(%r12)
	testq	%rdi, %rdi
	je	.L343
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L341:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	-104(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movl	$1819568500, -80(%rbp)
	movb	$101, -76(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L345
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L345:
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-144(%rbp), %r8
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r8, %r8
	je	.L348
	movq	(%r8), %rax
	movq	-104(%rbp), %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L347
.L348:
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L347:
	movq	-96(%rbp), %rax
	movq	72(%r12), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L423
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -128(%rbp)
	je	.L424
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	88(%r12), %rsi
	movq	%rax, 72(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 80(%r12)
	testq	%rdi, %rdi
	je	.L354
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L352:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L355
	call	_ZdlPv@PLT
.L355:
	movq	-104(%rbp), %rsi
	movq	%r13, %rdi
	movl	$29301, %eax
	movq	%rbx, -96(%rbp)
	movw	%ax, -80(%rbp)
	movb	$108, -78(%rbp)
	movq	$3, -88(%rbp)
	movb	$0, -77(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%rbx, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r13, %r13
	je	.L359
	movq	0(%r13), %rax
	movq	-104(%rbp), %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L358
.L359:
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L358:
	movq	-96(%rbp), %rax
	movq	104(%r12), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L425
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -136(%rbp)
	je	.L426
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	120(%r12), %rsi
	movq	%rax, 104(%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 112(%r12)
	testq	%rdi, %rdi
	je	.L365
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L363:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L427
	movq	%r12, (%r15)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L427:
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rdx
	movq	$0, (%r15)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L428
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	104(%r12), %rdi
	movq	%rax, (%r12)
	cmpq	%rdi, -136(%rbp)
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	72(%r12), %rdi
	cmpq	%rdi, -128(%rbp)
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	40(%r12), %rdi
	cmpq	%rdi, -120(%rbp)
	je	.L369
	call	_ZdlPv@PLT
.L369:
	movq	8(%r12), %rdi
	cmpq	%rdi, -112(%rbp)
	je	.L370
	call	_ZdlPv@PLT
.L370:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L425:
	testq	%rdx, %rdx
	je	.L361
	cmpq	$1, %rdx
	je	.L429
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	104(%r12), %rdi
.L361:
	movq	%rdx, 112(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L423:
	testq	%rdx, %rdx
	je	.L350
	cmpq	$1, %rdx
	je	.L430
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	72(%r12), %rdi
.L350:
	movq	%rdx, 80(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L421:
	testq	%rdx, %rdx
	je	.L339
	cmpq	$1, %rdx
	je	.L431
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	40(%r12), %rdi
.L339:
	movq	%rdx, 48(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L419:
	testq	%rdx, %rdx
	je	.L328
	cmpq	$1, %rdx
	je	.L432
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	8(%r12), %rdi
.L328:
	movq	%rdx, 16(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm5
	movq	%rax, 104(%r12)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 112(%r12)
.L365:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm7
	movq	%rax, 72(%r12)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 80(%r12)
.L354:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L422:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm6
	movq	%rax, 40(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 48(%r12)
.L343:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L420:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm5
	movq	%rax, 8(%r12)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 16(%r12)
.L332:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L318
.L429:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	104(%r12), %rdi
	jmp	.L361
.L432:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	8(%r12), %rdi
	jmp	.L328
.L431:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	40(%r12), %rdi
	jmp	.L339
.L430:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	72(%r12), %rdi
	jmp	.L350
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4985:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfo9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv
	.type	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv, @function
_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv:
.LFB4989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-120(%rbp), %r8
	movl	$48, %edi
	movq	%r8, 0(%r13)
	call	_Znwm@PLT
	movq	8(%rbx), %r15
	movq	16(%rbx), %r9
	movq	%rax, %r12
	movl	$4, 8(%rax)
	movq	-120(%rbp), %r8
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	leaq	32(%r12), %rdi
	addq	%r9, %rax
	movq	%rdi, 16(%r12)
	je	.L434
	testq	%r15, %r15
	je	.L441
.L434:
	movq	%r9, -104(%rbp)
	cmpq	$15, %r9
	ja	.L506
	cmpq	$1, %r9
	jne	.L437
	movzbl	(%r15), %eax
	leaq	-104(%rbp), %r14
	movb	%al, 32(%r12)
.L438:
	movq	%r9, 24(%r12)
	leaq	-96(%rbp), %r15
	movq	%r14, %rdx
	movabsq	$7226432857012596599, %rax
	movb	$0, (%rdi,%r9)
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r12, -104(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$8, -88(%rbp)
	movb	$0, -72(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L440
	movq	(%rdi), %rax
	call	*24(%rax)
.L440:
	movq	0(%r13), %rax
	movl	$48, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	40(%rbx), %r8
	movq	48(%rbx), %r9
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rcx)
	movq	%r8, %rax
	leaq	32(%rcx), %rdi
	addq	%r9, %rax
	movq	%rdi, 16(%rcx)
	je	.L464
	testq	%r8, %r8
	je	.L441
.L464:
	movq	%r9, -104(%rbp)
	cmpq	$15, %r9
	ja	.L507
	cmpq	$1, %r9
	jne	.L445
	movzbl	(%r8), %eax
	movb	%al, 32(%rcx)
.L446:
	movq	%r9, 24(%rcx)
	movq	%r14, %rdx
	movq	%r15, %rsi
	movb	$0, (%rdi,%r9)
	movq	-120(%rbp), %rdi
	movq	%rcx, -104(%rbp)
	movq	%r12, -96(%rbp)
	movl	$1701869940, -80(%rbp)
	movq	$4, -88(%rbp)
	movb	$0, -76(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L448
	movq	(%rdi), %rax
	call	*24(%rax)
.L448:
	movq	0(%r13), %rax
	movl	$48, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	72(%rbx), %r8
	movq	80(%rbx), %r9
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rcx)
	movq	%r8, %rax
	leaq	32(%rcx), %rdi
	addq	%r9, %rax
	movq	%rdi, 16(%rcx)
	je	.L465
	testq	%r8, %r8
	je	.L441
.L465:
	movq	%r9, -104(%rbp)
	cmpq	$15, %r9
	ja	.L508
	cmpq	$1, %r9
	jne	.L452
	movzbl	(%r8), %eax
	movb	%al, 32(%rcx)
.L453:
	movq	%r9, 24(%rcx)
	movq	%r14, %rdx
	movq	%r15, %rsi
	movb	$0, (%rdi,%r9)
	movq	-120(%rbp), %rdi
	movq	%rcx, -104(%rbp)
	movq	%r12, -96(%rbp)
	movl	$1819568500, -80(%rbp)
	movb	$101, -76(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L455
	movq	(%rdi), %rax
	call	*24(%rax)
.L455:
	movq	0(%r13), %rax
	movl	$48, %edi
	movq	%rax, -120(%rbp)
	call	_Znwm@PLT
	movq	104(%rbx), %r8
	movq	112(%rbx), %rbx
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rcx)
	movq	%r8, %rax
	leaq	32(%rcx), %rdi
	addq	%rbx, %rax
	movq	%rdi, 16(%rcx)
	je	.L466
	testq	%r8, %r8
	je	.L441
.L466:
	movq	%rbx, -104(%rbp)
	cmpq	$15, %rbx
	ja	.L509
	cmpq	$1, %rbx
	jne	.L459
	movzbl	(%r8), %eax
	movb	%al, 32(%rcx)
.L460:
	movq	%rbx, 24(%rcx)
	movl	$29301, %eax
	movq	%r14, %rdx
	movq	%r15, %rsi
	movb	$0, (%rdi,%rbx)
	movq	-120(%rbp), %rdi
	movq	%rcx, -104(%rbp)
	movq	%r12, -96(%rbp)
	movw	%ax, -80(%rbp)
	movb	$108, -78(%rbp)
	movq	$3, -88(%rbp)
	movb	$0, -77(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L433
	movq	(%rdi), %rax
	call	*24(%rax)
.L433:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	leaq	-104(%rbp), %r14
	testq	%r9, %r9
	je	.L438
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L452:
	testq	%r9, %r9
	je	.L453
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L445:
	testq	%r9, %r9
	je	.L446
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L459:
	testq	%rbx, %rbx
	je	.L460
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L506:
	leaq	-104(%rbp), %r14
	leaq	16(%r12), %rdi
	xorl	%edx, %edx
	movq	%r9, -128(%rbp)
	movq	%r14, %rsi
	movq	%r8, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %r9
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 32(%r12)
.L436:
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	%r8, -120(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	movq	16(%r12), %rdi
	movq	-120(%rbp), %r8
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	16(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	%rax, %rdi
	movq	-144(%rbp), %r9
	movq	%rax, 16(%rcx)
	movq	-104(%rbp), %rax
	movq	%rax, 32(%rcx)
.L444:
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%rcx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rcx
	movq	-104(%rbp), %r9
	movq	16(%rcx), %rdi
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L508:
	leaq	16(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	%rax, %rdi
	movq	-144(%rbp), %r9
	movq	%rax, 16(%rcx)
	movq	-104(%rbp), %rax
	movq	%rax, 32(%rcx)
.L451:
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%rcx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rcx
	movq	-104(%rbp), %r9
	movq	16(%rcx), %rdi
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	16(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, 16(%rcx)
	movq	-104(%rbp), %rax
	movq	%rax, 32(%rcx)
.L458:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%rcx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rcx
	movq	-104(%rbp), %rbx
	movq	16(%rcx), %rdi
	jmp	.L460
.L441:
	leaq	.LC11(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4989:
	.size	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv, .-_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv:
.LFB4871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L511
	movq	(%rdi), %rax
	call	*24(%rax)
.L511:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L518
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L518:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4871:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev:
.LFB4870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L519
	movq	(%rdi), %rax
	call	*24(%rax)
.L519:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L526:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4870:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo5cloneEv
	.type	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo5cloneEv, @function
_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo5cloneEv:
.LFB4990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L528
	movq	(%rdi), %rax
	call	*24(%rax)
.L528:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L534
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L534:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4990:
	.size	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo5cloneEv, .-_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo5cloneEv
	.section	.rodata.str1.1
.LC12:
	.string	"workerInfo"
.LC13:
	.string	"waitingForDebugger"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE:
.LFB4991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L536
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r12
	je	.L537
.L536:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r14)
.L535:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L603
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	movl	$56, %edi
	leaq	-80(%rbp), %rbx
	call	_Znwm@PLT
	cmpl	$6, 8(%r12)
	movq	%r13, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	movq	%rax, -136(%rbp)
	movq	%rax, 8(%r15)
	movl	$0, %eax
	cmovne	%rax, %r12
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	-96(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movabsq	$5291289110906103155, %rcx
	movq	%rax, %rsi
	movq	%rax, -120(%rbp)
	movq	%rcx, -80(%rbp)
	movb	$100, -72(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L540
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r8
.L540:
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r8
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r8, %r8
	je	.L543
	movq	(%r8), %rax
	movq	-120(%rbp), %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L542
.L543:
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L542:
	movq	-96(%rbp), %rax
	movq	8(%r15), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L604
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -136(%rbp)
	je	.L605
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r15), %rsi
	movq	%rax, 8(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r15)
	testq	%rdi, %rdi
	je	.L549
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L547:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movq	-120(%rbp), %rsi
	movl	$28518, %ecx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movabsq	$7947008797391875959, %rax
	movw	%cx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	cmpq	%rbx, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-104(%rbp), %rax
	movq	-144(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	-104(%rbp), %rax
	movq	40(%r15), %r8
	movq	$0, -104(%rbp)
	movq	%rax, 40(%r15)
	testq	%r8, %r8
	je	.L553
	movq	(%r8), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L554
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	104(%r8), %rdi
	movq	%rax, (%r8)
	leaq	120(%r8), %rax
	cmpq	%rax, %rdi
	je	.L555
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L555:
	movq	72(%r8), %rdi
	leaq	88(%r8), %rax
	cmpq	%rax, %rdi
	je	.L556
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L556:
	movq	40(%r8), %rdi
	leaq	56(%r8), %rax
	cmpq	%rax, %rdi
	je	.L557
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L557:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L558
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L558:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L559:
	movq	-104(%rbp), %r8
	testq	%r8, %r8
	je	.L553
	movq	(%r8), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L561
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	104(%r8), %rdi
	movq	%rax, (%r8)
	leaq	120(%r8), %rax
	cmpq	%rax, %rdi
	je	.L562
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L562:
	movq	72(%r8), %rdi
	leaq	88(%r8), %rax
	cmpq	%rax, %rdi
	je	.L563
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L563:
	movq	40(%r8), %rdi
	leaq	56(%r8), %rax
	cmpq	%rax, %rdi
	je	.L564
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L564:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L565
	movq	%r8, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %r8
.L565:
	movl	$136, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L553:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rbx, -96(%rbp)
	movq	$18, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	movdqa	.LC14(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$29285, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rbx, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	leaq	.LC13(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movb	$0, -104(%rbp)
	testq	%r12, %r12
	je	.L569
	movq	(%r12), %rax
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	testb	%al, %al
	jne	.L568
.L569:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L568:
	movzbl	-104(%rbp), %eax
	movq	%r13, %rdi
	movb	%al, 48(%r15)
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L606
	movq	%r15, (%r14)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L606:
	movq	(%r15), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev(%rip), %rdx
	movq	$0, (%r14)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L607
	movq	40(%r15), %r12
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L570
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L571
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	104(%r12), %rdi
	movq	%rax, (%r12)
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L572
	call	_ZdlPv@PLT
.L572:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L573
	call	_ZdlPv@PLT
.L573:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L575
	call	_ZdlPv@PLT
.L575:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L570:
	movq	8(%r15), %rdi
	cmpq	%rdi, -136(%rbp)
	je	.L576
	call	_ZdlPv@PLT
.L576:
	movl	$56, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L604:
	testq	%rdx, %rdx
	je	.L545
	cmpq	$1, %rdx
	je	.L608
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	8(%r15), %rdi
.L545:
	movq	%rdx, 16(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	%rax, 8(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%r15)
.L549:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L561:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L554:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L570
.L608:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	8(%r15), %rdi
	jmp	.L545
.L603:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4991:
	.size	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv
	.type	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv, @function
_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv:
.LFB4992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$96, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValueC1Ev@PLT
	movq	-120(%rbp), %r8
	movl	$48, %edi
	movq	%r8, (%r12)
	call	_Znwm@PLT
	movq	8(%rbx), %r14
	movq	16(%rbx), %r9
	movq	%rax, %r13
	movl	$4, 8(%rax)
	movq	-120(%rbp), %r8
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	%r14, %rax
	leaq	32(%r13), %rdi
	addq	%r9, %rax
	movq	%rdi, 16(%r13)
	je	.L610
	testq	%r14, %r14
	je	.L640
.L610:
	movq	%r9, -104(%rbp)
	cmpq	$15, %r9
	ja	.L641
	cmpq	$1, %r9
	jne	.L613
	movzbl	(%r14), %eax
	leaq	-104(%rbp), %r15
	movb	%al, 32(%r13)
.L614:
	movq	%r9, 24(%r13)
	leaq	-96(%rbp), %r14
	movq	%r15, %rdx
	movabsq	$5291289110906103155, %rax
	movb	$0, (%rdi,%r9)
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r13, -104(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	movq	%rax, -80(%rbp)
	movb	$100, -72(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L616
	movq	(%rdi), %rax
	call	*24(%rax)
.L616:
	movq	(%r12), %r8
	movq	40(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r8, -128(%rbp)
	call	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r8
	movq	%r14, %rsi
	movl	$28518, %ecx
	movq	%r13, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	%r8, %rdi
	movabsq	$7947008797391875959, %rax
	movq	%rax, -80(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, %rdx
	movw	%cx, -72(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L618
	movq	(%rdi), %rax
	call	*24(%rax)
.L618:
	movq	(%r12), %r8
	movzbl	48(%rbx), %ebx
	movl	$24, %edi
	movq	%r8, -128(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	leaq	16+_ZTVN4node9inspector8protocol16FundamentalValueE(%rip), %rcx
	movl	$1, 8(%rax)
	movq	%rcx, (%rax)
	movb	%bl, 16(%rax)
	movq	%rax, -112(%rbp)
	movq	%r13, -96(%rbp)
	movq	$18, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %r8
	movq	%r14, %rsi
	movdqa	.LC14(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$29285, %edx
	movq	%r8, %rdi
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-120(%rbp), %rdx
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L609
	movq	(%rdi), %rax
	call	*24(%rax)
.L609:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L642
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L613:
	.cfi_restore_state
	leaq	-104(%rbp), %r15
	testq	%r9, %r9
	je	.L614
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	-104(%rbp), %r15
	leaq	16(%r13), %rdi
	xorl	%edx, %edx
	movq	%r8, -128(%rbp)
	movq	%r15, %rsi
	movq	%r9, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r8
	movq	%rax, 16(%r13)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 32(%r13)
.L612:
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%r8, -120(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	movq	16(%r13), %rdi
	movq	-120(%rbp), %r8
	jmp	.L614
.L640:
	leaq	.LC11(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4992:
	.size	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv, .-_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv, @function
_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv:
.LFB4904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	movq	(%rdi), %rax
	call	*24(%rax)
.L643:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L650
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L650:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4904:
	.size	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv, .-_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev:
.LFB4903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L651
	movq	(%rdi), %rax
	call	*24(%rax)
.L651:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L658
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L658:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4903:
	.size	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification5cloneEv
	.type	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification5cloneEv, @function
_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification5cloneEv:
.LFB4993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L660
	movq	(%rdi), %rax
	call	*24(%rax)
.L660:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L666
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L666:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4993:
	.size	_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification5cloneEv, .-_ZNK4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE:
.LFB4994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L668
	cmpl	$6, 8(%rsi)
	je	.L669
.L668:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r12)
.L667:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L704
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movl	$40, %edi
	movq	%rsi, -112(%rbp)
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %rbx
	call	_Znwm@PLT
	movq	-112(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpl	$6, 8(%r8)
	movq	%rax, -104(%rbp)
	movq	%rax, 8(%r15)
	movl	$0, %eax
	cmovne	%rax, %r8
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%r8, -112(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	-112(%rbp), %r8
	movq	%r14, %rsi
	movq	%rbx, -96(%rbp)
	movabsq	$5291289110906103155, %rax
	movb	$100, -72(%rbp)
	movq	%r8, %rdi
	movq	%rax, -80(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L672
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %r8
.L672:
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-112(%rbp), %r8
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r8, %r8
	je	.L675
	movq	(%r8), %rax
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L674
.L675:
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L674:
	movq	-96(%rbp), %rax
	movq	8(%r15), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L705
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -104(%rbp)
	je	.L706
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r15), %rsi
	movq	%rax, 8(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r15)
	testq	%rdi, %rdi
	je	.L681
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L679:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L682
	call	_ZdlPv@PLT
.L682:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L707
	movq	%r15, (%r12)
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L707:
	movq	(%r15), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev(%rip), %rdx
	movq	$0, (%r12)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L708
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	cmpq	%rdi, -104(%rbp)
	je	.L683
	call	_ZdlPv@PLT
.L683:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L705:
	testq	%rdx, %rdx
	je	.L677
	cmpq	$1, %rdx
	je	.L709
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	8(%r15), %rdi
.L677:
	movq	%rdx, 16(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	%rax, 8(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%r15)
.L681:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L667
.L709:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	8(%r15), %rdi
	jmp	.L677
.L704:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4994:
	.size	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv
	.type	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv, @function
_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv:
.LFB4995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$96, %edi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN4node9inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r13, (%r14)
	movl	$48, %edi
	call	_Znwm@PLT
	movq	8(%r12), %r15
	movq	16(%r12), %r12
	movq	%rax, %rbx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%r15, %rax
	leaq	32(%rbx), %rdi
	addq	%r12, %rax
	movq	%rdi, 16(%rbx)
	je	.L711
	testq	%r15, %r15
	je	.L731
.L711:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L732
	cmpq	$1, %r12
	jne	.L714
	movzbl	(%r15), %eax
	leaq	-104(%rbp), %r8
	movb	%al, 32(%rbx)
.L715:
	movq	%r12, 24(%rbx)
	leaq	-96(%rbp), %rsi
	movq	%r8, %rdx
	movabsq	$5291289110906103155, %rax
	movb	$0, (%rdi,%r12)
	movq	%r13, %rdi
	movq	%rbx, -104(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	%rax, -80(%rbp)
	movb	$100, -72(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L716
	call	_ZdlPv@PLT
.L716:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L710
	movq	(%rdi), %rax
	call	*24(%rax)
.L710:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L733
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L714:
	.cfi_restore_state
	leaq	-104(%rbp), %r8
	testq	%r12, %r12
	je	.L715
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L732:
	leaq	-104(%rbp), %r8
	leaq	16(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r8, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 32(%rbx)
.L713:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -120(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	16(%rbx), %rdi
	movq	-120(%rbp), %r8
	jmp	.L715
.L731:
	leaq	.LC11(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L733:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4995:
	.size	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv, .-_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv, @function
_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv:
.LFB4931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L734
	movq	(%rdi), %rax
	call	*24(%rax)
.L734:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L741
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L741:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4931:
	.size	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv, .-_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev:
.LFB4930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L742
	movq	(%rdi), %rax
	call	*24(%rax)
.L742:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L749
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L749:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4930:
	.size	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification5cloneEv
	.type	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification5cloneEv, @function
_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification5cloneEv:
.LFB4996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L751
	movq	(%rdi), %rax
	call	*24(%rax)
.L751:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L757
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L757:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4996:
	.size	_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification5cloneEv, .-_ZNK4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE:
.LFB4997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L759
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L760
.L759:
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, (%r12)
.L758:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L816
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movl	$72, %edi
	leaq	-80(%rbp), %rbx
	call	_Znwm@PLT
	cmpl	$6, 8(%r14)
	movq	%r15, %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	24(%r13), %rax
	movq	%rax, -112(%rbp)
	movq	%rax, 8(%r13)
	leaq	56(%r13), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, 40(%r13)
	movl	$0, %eax
	cmovne	%rax, %r14
	movq	$0, 16(%r13)
	movb	$0, 24(%r13)
	movq	$0, 48(%r13)
	movb	$0, 56(%r13)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	-96(%rbp), %rax
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movabsq	$5291289110906103155, %rcx
	movq	%rax, %rsi
	movq	%rax, -104(%rbp)
	movq	%rcx, -80(%rbp)
	movb	$100, -72(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r10
	cmpq	%rbx, %rdi
	je	.L763
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r10
.L763:
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r10
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r10, %r10
	je	.L766
	movq	(%r10), %rax
	movq	-104(%rbp), %rsi
	movq	%r10, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L765
.L766:
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L765:
	movq	-96(%rbp), %rax
	movq	8(%r13), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L817
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -112(%rbp)
	je	.L818
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	24(%r13), %rsi
	movq	%rax, 8(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r13)
	testq	%rdi, %rdi
	je	.L772
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L770:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L773
	call	_ZdlPv@PLT
.L773:
	movq	-104(%rbp), %rsi
	movl	$26465, %eax
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movl	$1936942445, -80(%rbp)
	movw	%ax, -76(%rbp)
	movb	$101, -74(%rbp)
	movq	$7, -88(%rbp)
	movb	$0, -73(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r8
	cmpq	%rbx, %rdi
	je	.L774
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r8
.L774:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-128(%rbp), %r8
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r8, %r8
	je	.L777
	movq	(%r8), %rax
	movq	-104(%rbp), %rsi
	movq	%r8, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L776
.L777:
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L776:
	movq	-96(%rbp), %rax
	movq	40(%r13), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L819
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -120(%rbp)
	je	.L820
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	56(%r13), %rsi
	movq	%rax, 40(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 48(%r13)
	testq	%rdi, %rdi
	je	.L783
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L781:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L784
	call	_ZdlPv@PLT
.L784:
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L821
	movq	%r13, (%r12)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L821:
	movq	0(%r13), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev(%rip), %rdx
	movq	$0, (%r12)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L822
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE(%rip), %rax
	movq	40(%r13), %rdi
	movq	%rax, 0(%r13)
	cmpq	%rdi, -120(%rbp)
	je	.L785
	call	_ZdlPv@PLT
.L785:
	movq	8(%r13), %rdi
	cmpq	%rdi, -112(%rbp)
	je	.L786
	call	_ZdlPv@PLT
.L786:
	movl	$72, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L819:
	testq	%rdx, %rdx
	je	.L779
	cmpq	$1, %rdx
	je	.L823
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	40(%r13), %rdi
.L779:
	movq	%rdx, 48(%r13)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L817:
	testq	%rdx, %rdx
	je	.L768
	cmpq	$1, %rdx
	je	.L824
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	8(%r13), %rdi
.L768:
	movq	%rdx, 16(%r13)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L820:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	%rax, 40(%r13)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 48(%r13)
.L783:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L818:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 8(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 16(%r13)
.L772:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L822:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L758
.L823:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	40(%r13), %rdi
	jmp	.L779
.L824:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	8(%r13), %rdi
	jmp	.L768
.L816:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4997:
	.size	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv
	.type	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv, @function
_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv:
.LFB4998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%rdi, -120(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN4node9inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r12, (%r14)
	movl	$48, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %r15
	movq	16(%rbx), %r9
	movq	%rax, %r13
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	%r15, %rax
	leaq	32(%r13), %rdi
	addq	%r9, %rax
	movq	%rdi, 16(%r13)
	je	.L826
	testq	%r15, %r15
	je	.L833
.L826:
	movq	%r9, -104(%rbp)
	cmpq	$15, %r9
	ja	.L864
	cmpq	$1, %r9
	jne	.L829
	movzbl	(%r15), %eax
	leaq	-104(%rbp), %r14
	movb	%al, 32(%r13)
.L830:
	movq	%r9, 24(%r13)
	leaq	-96(%rbp), %r15
	movq	%r14, %rdx
	movabsq	$5291289110906103155, %rax
	movb	$0, (%rdi,%r9)
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r13, -104(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	movq	%rax, -80(%rbp)
	movb	$100, -72(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L831
	call	_ZdlPv@PLT
.L831:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L832
	movq	(%rdi), %rax
	call	*24(%rax)
.L832:
	movq	-120(%rbp), %rax
	movl	$48, %edi
	movq	(%rax), %rax
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	40(%rbx), %r8
	movq	48(%rbx), %rbx
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%rcx)
	movq	%r8, %rax
	leaq	32(%rcx), %rdi
	addq	%rbx, %rax
	movq	%rdi, 16(%rcx)
	je	.L842
	testq	%r8, %r8
	je	.L833
.L842:
	movq	%rbx, -104(%rbp)
	cmpq	$15, %rbx
	ja	.L865
	cmpq	$1, %rbx
	jne	.L837
	movzbl	(%r8), %eax
	movb	%al, 32(%rcx)
.L838:
	movq	%rbx, 24(%rcx)
	movl	$26465, %eax
	movq	%r14, %rdx
	movq	%r15, %rsi
	movb	$0, (%rdi,%rbx)
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	movq	%r13, -96(%rbp)
	movl	$1936942445, -80(%rbp)
	movw	%ax, -76(%rbp)
	movb	$101, -74(%rbp)
	movq	$7, -88(%rbp)
	movb	$0, -73(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L825
	movq	(%rdi), %rax
	call	*24(%rax)
.L825:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L866
	movq	-120(%rbp), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L829:
	.cfi_restore_state
	leaq	-104(%rbp), %r14
	testq	%r9, %r9
	je	.L830
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L837:
	testq	%rbx, %rbx
	je	.L838
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L864:
	leaq	-104(%rbp), %r14
	leaq	16(%r13), %rdi
	xorl	%edx, %edx
	movq	%r9, -128(%rbp)
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %r9
	movq	%rax, 16(%r13)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 32(%r13)
.L828:
	movq	%r9, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r9
	movq	16(%r13), %rdi
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L865:
	leaq	16(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, 16(%rcx)
	movq	-104(%rbp), %rax
	movq	%rax, 32(%rcx)
.L836:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%rcx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rcx
	movq	-104(%rbp), %rbx
	movq	16(%rcx), %rdi
	jmp	.L838
.L833:
	leaq	.LC11(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L866:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4998:
	.size	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv, .-_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv, @function
_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv:
.LFB4958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L867
	movq	(%rdi), %rax
	call	*24(%rax)
.L867:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L874
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L874:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4958:
	.size	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv, .-_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev:
.LFB4957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L875
	movq	(%rdi), %rax
	call	*24(%rax)
.L875:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L882
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L882:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4957:
	.size	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification5cloneEv
	.type	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification5cloneEv, @function
_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification5cloneEv:
.LFB4999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L884
	movq	(%rdi), %rax
	call	*24(%rax)
.L884:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L890
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L890:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4999:
	.size	_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification5cloneEv, .-_ZNK4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker8Frontend16attachedToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS2_10WorkerInfoESt14default_deleteISD_EEb
	.type	_ZN4node9inspector8protocol10NodeWorker8Frontend16attachedToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS2_10WorkerInfoESt14default_deleteISD_EEb, @function
_ZN4node9inspector8protocol10NodeWorker8Frontend16attachedToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS2_10WorkerInfoESt14default_deleteISD_EEb:
.LFB5000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L891
	movq	%rdi, %r13
	movl	$56, %edi
	movq	%rdx, %r12
	movq	%rsi, %r15
	movl	%ecx, %r14d
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movb	$0, 24(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	40(%rbx), %r12
	movq	%rax, 40(%rbx)
	testq	%r12, %r12
	je	.L893
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L894
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	104(%r12), %rdi
	movq	%rax, (%r12)
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L895
	call	_ZdlPv@PLT
.L895:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L897
	call	_ZdlPv@PLT
.L897:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L893:
	movq	0(%r13), %r12
	movb	%r14b, 48(%rbx)
	leaq	-104(%rbp), %r13
	leaq	-96(%rbp), %r14
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	(%r12), %rax
	movq	%rbx, -120(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	24(%rax), %r15
	movq	$27, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC15(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8245905578407519592, %rcx
	movq	%rdx, -80(%rbp)
	movl	$25963, %edx
	movups	%xmm0, (%rax)
	movq	%rcx, 16(%rax)
	movw	%dx, 24(%rax)
	movq	-96(%rbp), %rdx
	movb	$114, 26(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-120(%rbp), %rdx
	call	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L899
	movq	(%rdi), %rax
	call	*24(%rax)
.L899:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L900
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L901
	movq	48(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L902
	movq	(%rdi), %rax
	call	*24(%rax)
.L902:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L903
	call	_ZdlPv@PLT
.L903:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L900:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L904
	call	_ZdlPv@PLT
.L904:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L891
	movq	(%rdi), %rax
	call	*24(%rax)
.L891:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L923
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L894:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L893
.L923:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5000:
	.size	_ZN4node9inspector8protocol10NodeWorker8Frontend16attachedToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS2_10WorkerInfoESt14default_deleteISD_EEb, .-_ZN4node9inspector8protocol10NodeWorker8Frontend16attachedToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS2_10WorkerInfoESt14default_deleteISD_EEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker8Frontend18detachedFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol10NodeWorker8Frontend18detachedFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol10NodeWorker8Frontend18detachedFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L924
	movq	%rdi, %r12
	movl	$40, %edi
	leaq	-96(%rbp), %r14
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r13, %rsi
	leaq	-104(%rbp), %r13
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movb	$0, 24(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	(%r12), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	(%r12), %rax
	movq	%rbx, -120(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	24(%rax), %r15
	movq	$29, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC16(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$6299813990359131496, %rcx
	movq	%rdx, -80(%rbp)
	movl	$1701540463, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-96(%rbp), %rdx
	movq	%rcx, 16(%rax)
	movb	$114, 28(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-120(%rbp), %rdx
	call	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L926
	movq	(%rdi), %rax
	call	*24(%rax)
.L926:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L927
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L928
	movq	48(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L929
	movq	(%rdi), %rax
	call	*24(%rax)
.L929:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L930
	call	_ZdlPv@PLT
.L930:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L927:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L931
	call	_ZdlPv@PLT
.L931:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L924
	movq	(%rdi), %rax
	call	*24(%rax)
.L924:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L947
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L928:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L927
.L947:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5004:
	.size	_ZN4node9inspector8protocol10NodeWorker8Frontend18detachedFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol10NodeWorker8Frontend18detachedFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker8Frontend25receivedMessageFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_
	.type	_ZN4node9inspector8protocol10NodeWorker8Frontend25receivedMessageFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_, @function
_ZN4node9inspector8protocol10NodeWorker8Frontend25receivedMessageFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_:
.LFB5006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L948
	movq	%rdi, %r12
	movl	$72, %edi
	movq	%rdx, %r13
	movq	%rsi, %r14
	call	_Znwm@PLT
	movq	%r14, %rsi
	leaq	-96(%rbp), %r14
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	24(%rbx), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	leaq	56(%rbx), %rax
	leaq	40(%rbx), %r15
	movq	%rax, 40(%rbx)
	movq	$0, 16(%rbx)
	movb	$0, 24(%rbx)
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-104(%rbp), %r13
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	(%r12), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	(%r12), %rax
	movq	%rbx, -120(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	movq	24(%rax), %r15
	movq	$36, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC17(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC18(%rip), %xmm0
	movq	-96(%rbp), %rdx
	movl	$1919249266, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-120(%rbp), %rdx
	call	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L950
	movq	(%rdi), %rax
	call	*24(%rax)
.L950:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L951
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L952
	movq	48(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L953
	movq	(%rdi), %rax
	call	*24(%rax)
.L953:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L951:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L955
	call	_ZdlPv@PLT
.L955:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L948
	movq	(%rdi), %rax
	call	*24(%rax)
.L948:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L971
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L952:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L951
.L971:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5006:
	.size	_ZN4node9inspector8protocol10NodeWorker8Frontend25receivedMessageFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_, .-_ZN4node9inspector8protocol10NodeWorker8Frontend25receivedMessageFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker8Frontend5flushEv
	.type	_ZN4node9inspector8protocol10NodeWorker8Frontend5flushEv, @function
_ZN4node9inspector8protocol10NodeWorker8Frontend5flushEv:
.LFB5008:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE5008:
	.size	_ZN4node9inspector8protocol10NodeWorker8Frontend5flushEv, .-_ZN4node9inspector8protocol10NodeWorker8Frontend5flushEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L987
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	leaq	-96(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rdx, (%rsi)
	movq	%rcx, -96(%rbp)
	movq	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L975
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L977:
	movq	%rdi, -72(%rbp)
	movl	$64, %edi
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movb	$0, -96(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L988
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L979:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movq	$0, 56(%rax)
	leaq	-120(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movups	%xmm0, 40(%rax)
	call	*%r14
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L980
	movq	(%rdi), %rax
	call	*24(%rax)
.L980:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L973
	call	_ZdlPv@PLT
.L973:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L989
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movb	$0, 16(%rsi)
	movq	$0, 8(%rsi)
	leaq	-96(%rbp), %rbx
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
.L975:
	movdqa	-96(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L988:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L979
.L989:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5009:
	.size	_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB5010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$64, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movb	$0, 24(%rax)
	leaq	-64(%rbp), %rsi
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rcx
	movq	%rdx, 8(%rax)
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movq	%r15, 56(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 40(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L990
	movq	(%rdi), %rax
	call	*24(%rax)
.L990:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L997
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L997:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5010:
	.size	_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol10NodeWorker8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_:
.LFB6424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	0(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L999
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	56(%rbx), %rsi
.L1002:
	cmpq	%rsi, %r15
	je	.L1043
.L1000:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L999
	movq	56(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L1002
.L999:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	leaq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L1044
	movq	%rdx, 8(%rbx)
	movq	16(%r13), %rdx
	movq	%rdx, 24(%rbx)
.L1004:
	movq	8(%r13), %rdx
	movb	$0, 16(%r13)
	leaq	32(%r12), %rdi
	movl	$1, %ecx
	movq	$0, 8(%r13)
	movq	8(%r12), %rsi
	movq	%rdx, 16(%rbx)
	movq	24(%r12), %rdx
	movq	%rax, 0(%r13)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L1005
	movq	(%r12), %r8
	movq	%r15, 56(%rbx)
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L1015
.L1047:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L1016:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1043:
	.cfi_restore_state
	movq	8(%r13), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L1000
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1001
	movq	8(%rbx), %rsi
	movq	0(%r13), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L1000
.L1001:
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L1045
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1046
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L1008:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L1010
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1013:
	testq	%rsi, %rsi
	je	.L1010
.L1011:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1012
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1019
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1011
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	(%r12), %rdi
	cmpq	%rdi, %r10
	je	.L1014
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1014:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r8, (%r12)
	movq	%r15, 56(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L1047
.L1015:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L1017
	movq	56(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
.L1017:
	leaq	16(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1044:
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 24(%rbx)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	%rdx, %rdi
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L1008
.L1046:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6424:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE
	.type	_ZN4node9inspector8protocol10NodeWorker10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE, @function
_ZN4node9inspector8protocol10NodeWorker10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE:
.LFB5074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdi, -120(%rbp)
	movq	8(%rdi), %r12
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r12, %rsi
	leaq	-96(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE@PLT
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE(%rip), %rax
	xorl	%edx, %edx
	movss	.LC19(%rip), %xmm0
	movq	%rax, 0(%r13)
	leaq	120(%r13), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, 72(%r13)
	leaq	176(%r13), %rax
	leaq	72(%r13), %r15
	movq	%rbx, 184(%r13)
	leaq	-80(%rbp), %rbx
	movq	$1, 80(%r13)
	movq	$0, 88(%r13)
	movq	$0, 96(%r13)
	movq	$0, 112(%r13)
	movq	$0, 120(%r13)
	movq	%rax, 128(%r13)
	movq	$1, 136(%r13)
	movq	$0, 144(%r13)
	movq	$0, 152(%r13)
	movq	$0, 168(%r13)
	movq	$0, 176(%r13)
	movss	%xmm0, 104(%r13)
	movss	%xmm0, 160(%r13)
	movq	%rbx, -96(%rbp)
	movq	$30, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC20(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$8022148330944688997, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movl	$29285, %ecx
	movw	%cx, 28(%rax)
	movl	$1802661719, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	leaq	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl19sendMessageToWorkerEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1049
	call	_ZdlPv@PLT
.L1049:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	$17, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC21(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movb	$101, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	leaq	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6enableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	$18, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC22(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$25964, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	leaq	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl7disableEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	$17, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC16(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movb	$104, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	leaq	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl6detachEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1052
	call	_ZdlPv@PLT
.L1052:
	movq	-120(%rbp), %r15
	leaq	128(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movabsq	$7742373130557222734, %rax
	movq	%r13, -104(%rbp)
	movq	%rax, -80(%rbp)
	movl	$29285, %eax
	movq	%rbx, -96(%rbp)
	movw	%ax, -72(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
	call	_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1048
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1055
	movq	144(%r12), %r13
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L1060
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L1065
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L1066
	call	_ZdlPv@PLT
.L1066:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1048:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1090
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1091:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1056
.L1059:
	movq	%rbx, %r13
.L1060:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L1091
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1059
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1092:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1062
.L1064:
	movq	%rbx, %r13
.L1065:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L1092
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1064
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1048
.L1090:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5074:
	.size	_ZN4node9inspector8protocol10NodeWorker10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE, .-_ZN4node9inspector8protocol10NodeWorker10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_:
.LFB6427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1094
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	56(%rbx), %rcx
.L1097:
	cmpq	%rcx, %r13
	je	.L1114
.L1095:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1094
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L1097
.L1094:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1114:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L1095
	testq	%rdx, %rdx
	je	.L1096
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1095
.L1096:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6427:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.type	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE, @function
_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE:
.LFB5062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movq	%rcx, -136(%rbp)
	movl	%esi, -140(%rbp)
	movq	%rdx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol10NodeWorker14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	movq	40(%r14), %rax
	addq	48(%r14), %rbx
	movq	-136(%rbp), %rcx
	movl	-140(%rbp), %r10d
	movq	%rbx, %rdi
	testb	$1, %al
	je	.L1116
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
.L1116:
	movq	0(%r13), %rdx
	movq	%r15, %r9
	leaq	-120(%rbp), %r8
	movl	%r10d, %esi
	movq	$0, 0(%r13)
	movq	%rdx, -120(%rbp)
	movq	%r12, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1117
	movq	(%rdi), %rax
	call	*24(%rax)
.L1117:
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1126
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5062:
	.size	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE, .-_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.weak	_ZTVN4node9inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN4node9inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN4node9inspector8protocol23InternalRawNotificationE, 48
_ZTVN4node9inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE,"awG",@progbits,_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE, @object
	.size	_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE, 48
_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD1Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev
	.weak	_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE,"awG",@progbits,_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE, @object
	.size	_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE, 48
_ZTVN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotification17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD1Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker28AttachedToWorkerNotificationD0Ev
	.weak	_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE,"awG",@progbits,_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE, @object
	.size	_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE, 48
_ZTVN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotification17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD1Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker30DetachedFromWorkerNotificationD0Ev
	.weak	_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE,"awG",@progbits,_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE, @object
	.size	_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE, 48
_ZTVN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotification17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD1Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker37ReceivedMessageFromWorkerNotificationD0Ev
	.weak	_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE,"awG",@progbits,_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE, @object
	.size	_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE, 48
_ZTVN4node9inspector8protocol10NodeWorker14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD1Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker14DispatcherImplD0Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector8protocol10NodeWorker14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.globl	_ZN4node9inspector8protocol10NodeWorker8Metainfo7versionE
	.section	.rodata
	.type	_ZN4node9inspector8protocol10NodeWorker8Metainfo7versionE, @object
	.size	_ZN4node9inspector8protocol10NodeWorker8Metainfo7versionE, 4
_ZN4node9inspector8protocol10NodeWorker8Metainfo7versionE:
	.string	"1.0"
	.globl	_ZN4node9inspector8protocol10NodeWorker8Metainfo13commandPrefixE
	.align 8
	.type	_ZN4node9inspector8protocol10NodeWorker8Metainfo13commandPrefixE, @object
	.size	_ZN4node9inspector8protocol10NodeWorker8Metainfo13commandPrefixE, 12
_ZN4node9inspector8protocol10NodeWorker8Metainfo13commandPrefixE:
	.string	"NodeWorker."
	.globl	_ZN4node9inspector8protocol10NodeWorker8Metainfo10domainNameE
	.align 8
	.type	_ZN4node9inspector8protocol10NodeWorker8Metainfo10domainNameE, @object
	.size	_ZN4node9inspector8protocol10NodeWorker8Metainfo10domainNameE, 11
_ZN4node9inspector8protocol10NodeWorker8Metainfo10domainNameE:
	.string	"NodeWorker"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	4932126890315571575
	.quad	5724749571133104741
	.align 16
.LC14:
	.quad	5073144904459837815
	.quad	7451053173974004335
	.align 16
.LC15:
	.quad	7742373130557222734
	.quad	7161132925691261541
	.align 16
.LC16:
	.quad	7742373130557222734
	.quad	7161132861317083749
	.align 16
.LC17:
	.quad	7742373130557222734
	.quad	7594585633988702821
	.align 16
.LC18:
	.quad	7022083122928051574
	.quad	8023001586609841511
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC19:
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC20:
	.quad	7742373130557222734
	.quad	5576703620594692709
	.align 16
.LC21:
	.quad	7742373130557222734
	.quad	7809911830585700965
	.align 16
.LC22:
	.quad	7742373130557222734
	.quad	7089074184947397221
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
