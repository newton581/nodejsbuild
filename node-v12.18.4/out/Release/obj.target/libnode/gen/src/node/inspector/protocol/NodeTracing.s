	.file	"NodeTracing.cpp"
	.text
	.section	.text._ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev:
.LFB4828:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	8(%rsi), %rcx
	leaq	24(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L5
	movq	%rcx, (%rdi)
	movq	24(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L3:
	movq	16(%rsi), %rcx
	movq	%rdx, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	%rcx, 8(%rax)
	movb	$0, 24(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L3
	.cfi_endproc
.LFE4828:
	.size	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.section	.text._ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv, @function
_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv:
.LFB4829:
	.cfi_startproc
	endbr64
	movdqu	40(%rsi), %xmm1
	movq	56(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	$0, 56(%rsi)
	movq	%rdx, 16(%rdi)
	movups	%xmm1, (%rdi)
	movups	%xmm0, 40(%rsi)
	ret
	.cfi_endproc
.LFE4829:
	.size	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv, .-_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol23InternalRawNotificationD2Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev, @function
_ZN4node9inspector8protocol23InternalRawNotificationD2Ev:
.LFB4825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L7
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4825:
	.size	_ZN4node9inspector8protocol23InternalRawNotificationD2Ev, .-_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev
	.set	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev,_ZN4node9inspector8protocol23InternalRawNotificationD2Ev
	.section	.text._ZN4node9inspector8protocol11NodeTracing14DispatcherImplD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD2Ev
	.type	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD2Ev, @function
_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD2Ev:
.LFB4996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L19
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L45:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L15
.L18:
	movq	%rbx, %r13
.L19:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L45
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L18
.L15:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L24
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L46:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L21
.L23:
	movq	%rbx, %r13
.L24:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L46
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L23
.L21:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	.cfi_endproc
.LFE4996:
	.size	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD2Ev, .-_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD2Ev
	.weak	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD1Ev
	.set	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD1Ev,_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD2Ev
	.section	.text._ZN4node9inspector8protocol23InternalRawNotificationD0Ev,"axG",@progbits,_ZN4node9inspector8protocol23InternalRawNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.type	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev, @function
_ZN4node9inspector8protocol23InternalRawNotificationD0Ev:
.LFB4827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4827:
	.size	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev, .-_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl4stopEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl4stopEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl4stopEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB5031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*32(%rax)
	cmpl	$2, -112(%rbp)
	je	.L67
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	%r15, %rdx
	movl	%r12d, %esi
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE@PLT
.L56:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L54
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L54:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L56
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5031:
	.size	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl4stopEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl4stopEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.text._ZN4node9inspector8protocol16InternalResponseD0Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD0Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD0Ev, @function
_ZN4node9inspector8protocol16InternalResponseD0Ev:
.LFB4818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	call	*24(%rax)
.L70:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4818:
	.size	_ZN4node9inspector8protocol16InternalResponseD0Ev, .-_ZN4node9inspector8protocol16InternalResponseD0Ev
	.section	.text._ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing11TraceConfigD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev, @function
_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev:
.LFB4866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %r14
	movq	%rax, (%rdi)
	testq	%r14, %r14
	je	.L77
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L79
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L82
	movq	(%r14), %r12
.L78:
	testq	%r12, %r12
	je	.L83
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L83:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L77:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$56, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L82
	movq	(%r14), %r12
	jmp	.L78
	.cfi_endproc
.LFE4866:
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev, .-_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev
	.section	.text._ZN4node9inspector8protocol11NodeTracing11TraceConfigD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing11TraceConfigD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD2Ev
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD2Ev, @function
_ZN4node9inspector8protocol11NodeTracing11TraceConfigD2Ev:
.LFB4864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L93
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	je	.L94
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L95
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r14
	jne	.L98
	movq	0(%r13), %r12
.L94:
	testq	%r12, %r12
	je	.L99
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L99:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L93:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L92
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r14
	jne	.L98
	movq	0(%r13), %r12
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L92:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4864:
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD2Ev, .-_ZN4node9inspector8protocol11NodeTracing11TraceConfigD2Ev
	.weak	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD1Ev
	.set	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD1Ev,_ZN4node9inspector8protocol11NodeTracing11TraceConfigD2Ev
	.section	.text._ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD2Ev
	.type	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD2Ev, @function
_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD2Ev:
.LFB4896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L108
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L111
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L114
	movq	0(%r13), %r12
.L110:
	testq	%r12, %r12
	je	.L115
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L115:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L114
	movq	0(%r13), %r12
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L108:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4896:
	.size	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD2Ev, .-_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD2Ev
	.weak	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD1Ev
	.set	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD1Ev,_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD2Ev
	.section	.text._ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev
	.type	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev, @function
_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev:
.LFB4898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	movq	%rax, (%rdi)
	testq	%r14, %r14
	je	.L121
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L122
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L123
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L126
	movq	(%r14), %r12
.L122:
	testq	%r12, %r12
	je	.L127
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L127:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L121:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$16, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L126
	movq	(%r14), %r12
	jmp	.L122
	.cfi_endproc
.LFE4898:
	.size	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev, .-_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev
	.section	.text._ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev
	.type	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev, @function
_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev:
.LFB4998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L140
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L166:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L136
.L139:
	movq	%rbx, %r13
.L140:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L166
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L139
.L136:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L145
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L167:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L142
.L144:
	movq	%rbx, %r13
.L145:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L167
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L144
.L142:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4998:
	.size	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev, .-_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	80(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	72(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L175
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	56(%rbx), %rcx
.L171:
	cmpq	%rcx, %r13
	je	.L178
.L170:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L175
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L171
.L175:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L170
	testq	%rdx, %rdx
	je	.L174
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L170
.L174:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5000:
	.size	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.text
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB7915:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L180
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L180:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L182
.L201:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L201
.L182:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L184
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L185:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L186
	testb	$4, %al
	jne	.L203
	testl	%eax, %eax
	je	.L187
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L204
.L187:
	movq	(%r12), %rcx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L187
	andl	$-8, %eax
	xorl	%edx, %edx
.L190:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L190
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L203:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L187
.L204:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L187
.L202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7915:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl13getCategoriesEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl13getCategoriesEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl13getCategoriesEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB5003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-160(%rbp), %rdi
	subq	$184, %rsp
	movl	%esi, -196(%rbp)
	movq	%rbx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -168(%rbp)
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%rbx), %rsi
	leaq	-168(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	cmpl	$2, -112(%rbp)
	je	.L263
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9inspector8protocol15DictionaryValueC1Ev@PLT
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	je	.L264
.L208:
	movq	-160(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L265
.L232:
	movl	-196(%rbp), %esi
	leaq	-152(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, -152(%rbp)
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseESt10unique_ptrINS1_15DictionaryValueESt14default_deleteIS7_EE@PLT
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L207
	movq	(%rdi), %rax
	call	*24(%rax)
.L207:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	-160(%rbp), %r12
	testq	%r12, %r12
	je	.L224
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L224:
	movq	-168(%rbp), %r13
	testq	%r13, %r13
	je	.L205
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L226
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L227
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L230
.L228:
	movq	0(%r13), %r12
.L226:
	testq	%r12, %r12
	je	.L231
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L231:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L205:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$40, %edi
	movq	-168(%rbp), %r13
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN4node9inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r13), %rax
	movq	0(%r13), %rbx
	leaq	-152(%rbp), %r13
	movq	%rax, -192(%rbp)
	cmpq	%rax, %rbx
	je	.L267
	movq	%r12, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L220:
	movl	$48, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %r9
	movq	%rax, %r12
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	leaq	32(%r12), %rdi
	movq	%rax, (%r12)
	movq	%rdi, 16(%r12)
	movq	(%rbx), %r8
	movq	%r8, %rax
	addq	%r9, %rax
	je	.L212
	testq	%r8, %r8
	je	.L268
.L212:
	movq	%r9, -152(%rbp)
	cmpq	$15, %r9
	ja	.L269
	cmpq	$1, %r9
	jne	.L215
	movzbl	(%r8), %eax
	movq	%r13, -184(%rbp)
	movb	%al, 32(%r12)
.L216:
	movq	%r9, 24(%r12)
	movq	%r13, %rsi
	movb	$0, (%rdi,%r9)
	movq	%r15, %rdi
	movq	%r12, -152(%rbp)
	call	_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE@PLT
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L217
	movq	(%rdi), %rax
	addq	$32, %rbx
	call	*24(%rax)
	cmpq	%rbx, -192(%rbp)
	jne	.L220
.L262:
	movq	-224(%rbp), %r12
.L219:
	movq	-184(%rbp), %rdx
	movq	%r12, %rdi
	movabsq	$7598258011201888611, %rax
	leaq	-144(%rbp), %rsi
	movq	%rax, -128(%rbp)
	leaq	-128(%rbp), %rbx
	movl	$29541, %eax
	movq	%r15, -152(%rbp)
	movq	%rbx, -144(%rbp)
	movw	%ax, -120(%rbp)
	movq	$10, -136(%rbp)
	movb	$0, -118(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-160(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L232
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L227:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L230
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L263:
	movq	8(%rbx), %rdi
	movl	-196(%rbp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%r13, -184(%rbp)
	testq	%r9, %r9
	je	.L216
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L269:
	leaq	16(%r12), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, -216(%rbp)
	movq	%r9, -208(%rbp)
	movq	%r13, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %r8
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	%rax, 32(%r12)
.L214:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %r9
	movq	16(%r12), %rdi
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	addq	$32, %rbx
	cmpq	%rbx, -192(%rbp)
	jne	.L220
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r13, -184(%rbp)
	jmp	.L219
.L266:
	call	__stack_chk_fail@PLT
.L268:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE5003:
	.size	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl13getCategoriesEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl13getCategoriesEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.text._ZN4node9inspector8protocol16InternalResponseD2Ev,"axG",@progbits,_ZN4node9inspector8protocol16InternalResponseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol16InternalResponseD2Ev
	.type	_ZN4node9inspector8protocol16InternalResponseD2Ev, @function
_ZN4node9inspector8protocol16InternalResponseD2Ev:
.LFB4816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L271
	movq	(%rdi), %rax
	call	*24(%rax)
.L271:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L270
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4816:
	.size	_ZN4node9inspector8protocol16InternalResponseD2Ev, .-_ZN4node9inspector8protocol16InternalResponseD2Ev
	.weak	_ZN4node9inspector8protocol16InternalResponseD1Ev
	.set	_ZN4node9inspector8protocol16InternalResponseD1Ev,_ZN4node9inspector8protocol16InternalResponseD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv
	.type	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv, @function
_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv:
.LFB4937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movq	%rdi, -160(%rbp)
	movl	$96, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9inspector8protocol15DictionaryValueC1Ev@PLT
	cmpb	$0, 8(%rbx)
	movq	%r12, (%r15)
	jne	.L334
.L278:
	movl	$40, %edi
	movq	48(%rbx), %r13
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN4node9inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r13), %rax
	movq	0(%r13), %rbx
	leaq	-136(%rbp), %r13
	movq	%rax, -152(%rbp)
	cmpq	%rax, %rbx
	je	.L335
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$48, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %r8
	movq	%rax, %rcx
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	leaq	32(%rcx), %rdi
	movq	%rax, (%rcx)
	movq	%rdi, 16(%rcx)
	movq	(%rbx), %r9
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L309
	testq	%r9, %r9
	je	.L284
.L309:
	movq	%r8, -136(%rbp)
	cmpq	$15, %r8
	ja	.L336
	cmpq	$1, %r8
	jne	.L299
	movzbl	(%r9), %eax
	movq	%r13, %r15
	movb	%al, 32(%rcx)
.L300:
	movq	%r8, 24(%rcx)
	movq	%r13, %rsi
	movb	$0, (%rdi,%r8)
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE@PLT
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	movq	(%rdi), %rax
	addq	$32, %rbx
	call	*24(%rax)
	cmpq	%rbx, -152(%rbp)
	jne	.L304
.L303:
	leaq	-96(%rbp), %r13
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, -144(%rbp)
	movq	%r13, %rdi
	leaq	-80(%rbp), %rbx
	movq	$18, -136(%rbp)
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	.LC2(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$29541, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-144(%rbp), %rdx
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L277
	movq	(%rdi), %rax
	call	*24(%rax)
.L277:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	movq	-160(%rbp), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	addq	$32, %rbx
	cmpq	%rbx, -152(%rbp)
	jne	.L304
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%r13, %r15
	testq	%r8, %r8
	je	.L300
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	16(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r9, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r13, %r15
	movq	%rcx, -168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %r8
	movq	%rax, %rdi
	movq	-184(%rbp), %r9
	movq	%rax, 16(%rcx)
	movq	-136(%rbp), %rax
	movq	%rax, 32(%rcx)
.L298:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%rcx, -168(%rbp)
	call	memcpy@PLT
	movq	-168(%rbp), %rcx
	movq	-136(%rbp), %r8
	movq	16(%rcx), %rdi
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L334:
	movq	16(%rbx), %r8
	movq	24(%rbx), %r14
	leaq	-112(%rbp), %r13
	movq	%r13, -128(%rbp)
	movq	%r8, %rax
	addq	%r14, %rax
	je	.L279
	testq	%r8, %r8
	je	.L284
.L279:
	movq	%r14, -136(%rbp)
	cmpq	$15, %r14
	ja	.L338
	cmpq	$1, %r14
	jne	.L282
	movzbl	(%r8), %eax
	movb	%al, -112(%rbp)
	movq	%r13, %rax
.L283:
	movq	%r14, -120(%rbp)
	movl	$48, %edi
	movb	$0, (%rax,%r14)
	call	_Znwm@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %r9
	movq	%rax, %r14
	movl	$4, 8(%rax)
	leaq	16+_ZTVN4node9inspector8protocol11StringValueE(%rip), %rax
	movq	%rax, (%r14)
	movq	%r8, %rax
	leaq	32(%r14), %rdi
	addq	%r9, %rax
	movq	%rdi, 16(%r14)
	je	.L308
	testq	%r8, %r8
	je	.L284
.L308:
	movq	%r9, -136(%rbp)
	cmpq	$15, %r9
	ja	.L339
	cmpq	$1, %r9
	jne	.L288
	movzbl	(%r8), %eax
	leaq	-136(%rbp), %r15
	movb	%al, 32(%r14)
.L289:
	movq	%r9, 24(%r14)
	movl	$25956, %ecx
	leaq	-96(%rbp), %rsi
	movq	%r15, %rdx
	movabsq	$8020176954074555762, %rax
	movb	$0, (%rdi,%r9)
	movq	%r12, %rdi
	movq	%r14, -136(%rbp)
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	%rax, -80(%rbp)
	movw	%cx, -72(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	movq	(%rdi), %rax
	call	*24(%rax)
.L291:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L292
	call	_ZdlPv@PLT
.L292:
	movq	-160(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L282:
	testq	%r14, %r14
	jne	.L340
	movq	%r13, %rax
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	-136(%rbp), %r15
	testq	%r9, %r9
	je	.L289
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r13, %r15
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L339:
	leaq	-136(%rbp), %r15
	leaq	16(%r14), %rdi
	xorl	%edx, %edx
	movq	%r9, -168(%rbp)
	movq	%r15, %rsi
	movq	%r8, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	-168(%rbp), %r9
	movq	%rax, 16(%r14)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, 32(%r14)
.L287:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r9
	movq	16(%r14), %rdi
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	-136(%rbp), %r15
	leaq	-128(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -152(%rbp)
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L281:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r14
	movq	-128(%rbp), %rax
	jmp	.L283
.L284:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L337:
	call	__stack_chk_fail@PLT
.L340:
	movq	%r13, %rdi
	jmp	.L281
	.cfi_endproc
.LFE4937:
	.size	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv, .-_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv
	.section	.text._ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv, @function
_ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv:
.LFB4874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L341
	movq	(%rdi), %rax
	call	*24(%rax)
.L341:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L348:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4874:
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv, .-_ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev:
.LFB4873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rax
	call	*24(%rax)
.L349:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L356:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4873:
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv
	.type	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv, @function
_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv:
.LFB4940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$96, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN4node9inspector8protocol15DictionaryValueC1Ev@PLT
	movq	%r12, (%r14)
	movl	$40, %edi
	movq	8(%rbx), %r15
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node9inspector8protocol9ListValueC1Ev@PLT
	movq	8(%r15), %rax
	movq	(%r15), %rbx
	leaq	-104(%rbp), %r15
	movq	%rax, -120(%rbp)
	cmpq	%rax, %rbx
	je	.L358
	.p2align 4,,10
	.p2align 3
.L361:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
	call	*88(%rax)
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN4node9inspector8protocol9ListValue9pushValueESt10unique_ptrINS1_5ValueESt14default_deleteIS4_EE@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L359
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*24(%rax)
	cmpq	%rbx, -120(%rbp)
	jne	.L361
.L358:
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rbx
	movq	%r15, %rdx
	movq	%r13, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movl	$1970037110, -80(%rbp)
	movb	$101, -76(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
	call	_ZN4node9inspector8protocol15DictionaryValue8setValueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L357
	movq	(%rdi), %rax
	call	*24(%rax)
.L357:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L371
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L361
	jmp	.L358
.L371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4940:
	.size	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv, .-_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv
	.section	.text._ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv, @function
_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv:
.LFB4903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L372
	movq	(%rdi), %rax
	call	*24(%rax)
.L372:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L379
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L379:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4903:
	.size	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv, .-_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev:
.LFB4902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L380
	movq	(%rdi), %rax
	call	*24(%rax)
.L380:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L387:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4902:
	.size	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing8Frontend13dataCollectedESt10unique_ptrINS1_5ArrayINS1_15DictionaryValueEEESt14default_deleteIS7_EE
	.type	_ZN4node9inspector8protocol11NodeTracing8Frontend13dataCollectedESt10unique_ptrINS1_5ArrayINS1_15DictionaryValueEEESt14default_deleteIS7_EE, @function
_ZN4node9inspector8protocol11NodeTracing8Frontend13dataCollectedESt10unique_ptrINS1_5ArrayINS1_15DictionaryValueEEESt14default_deleteIS7_EE:
.LFB4942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdi)
	je	.L388
	movq	%rsi, %r12
	leaq	-96(%rbp), %r14
	leaq	-104(%rbp), %r13
	movq	%rdi, %rbx
	movl	$16, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, (%r12)
	movq	(%rbx), %r12
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE(%rip), %rcx
	leaq	-80(%rbp), %rbx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	(%r12), %rdx
	movq	%rax, -120(%rbp)
	movq	24(%rdx), %r15
	xorl	%edx, %edx
	movq	%rbx, -96(%rbp)
	movq	$25, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$7310577382391050051, %rcx
	movq	%rdx, -80(%rbp)
	movb	$100, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-96(%rbp), %rdx
	movq	%rcx, 16(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-120(%rbp), %rdx
	call	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L390
	movq	(%rdi), %rax
	call	*24(%rax)
.L390:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L391
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L392
	movq	48(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L393
	movq	(%rdi), %rax
	call	*24(%rax)
.L393:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L394
	call	_ZdlPv@PLT
.L394:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L391:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L395
	call	_ZdlPv@PLT
.L395:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L388
	movq	(%rdi), %rax
	call	*24(%rax)
.L388:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L391
.L411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4942:
	.size	_ZN4node9inspector8protocol11NodeTracing8Frontend13dataCollectedESt10unique_ptrINS1_5ArrayINS1_15DictionaryValueEEESt14default_deleteIS7_EE, .-_ZN4node9inspector8protocol11NodeTracing8Frontend13dataCollectedESt10unique_ptrINS1_5ArrayINS1_15DictionaryValueEEESt14default_deleteIS7_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing8Frontend15tracingCompleteEv
	.type	_ZN4node9inspector8protocol11NodeTracing8Frontend15tracingCompleteEv, @function
_ZN4node9inspector8protocol11NodeTracing8Frontend15tracingCompleteEv:
.LFB4946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L412
	movq	(%r12), %rax
	leaq	-96(%rbp), %r14
	leaq	-104(%rbp), %r13
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-80(%rbp), %rbx
	movq	$0, -120(%rbp)
	movq	24(%rax), %r15
	movq	%rbx, -96(%rbp)
	movq	$27, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$7813865678127459945, %rcx
	movq	%rdx, -80(%rbp)
	movl	$29797, %edx
	movups	%xmm0, (%rax)
	movq	%rcx, 16(%rax)
	movw	%dx, 24(%rax)
	movq	-96(%rbp), %rdx
	movb	$101, 26(%rax)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-120(%rbp), %rdx
	call	_ZN4node9inspector8protocol16InternalResponse18createNotificationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_12SerializableESt14default_deleteISC_EE@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	*%r15
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L414
	movq	(%rdi), %rax
	call	*24(%rax)
.L414:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L415
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol16InternalResponseD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L416
	movq	48(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol16InternalResponseE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L417
	movq	(%rdi), %rax
	call	*24(%rax)
.L417:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L415:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L419
	call	_ZdlPv@PLT
.L419:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L412
	movq	(%rdi), %rax
	call	*24(%rax)
.L412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L415
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4946:
	.size	_ZN4node9inspector8protocol11NodeTracing8Frontend15tracingCompleteEv, .-_ZN4node9inspector8protocol11NodeTracing8Frontend15tracingCompleteEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing8Frontend5flushEv
	.type	_ZN4node9inspector8protocol11NodeTracing8Frontend5flushEv, @function
_ZN4node9inspector8protocol11NodeTracing8Frontend5flushEv:
.LFB4947:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE4947:
	.size	_ZN4node9inspector8protocol11NodeTracing8Frontend5flushEv, .-_ZN4node9inspector8protocol11NodeTracing8Frontend5flushEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	24(%rax), %r14
	movq	(%rsi), %rax
	cmpq	%rdx, %rax
	je	.L454
	movq	16(%rsi), %rcx
	leaq	-64(%rbp), %r13
	leaq	-96(%rbp), %rbx
	movq	8(%rsi), %rdi
	movq	%rdx, (%rsi)
	movq	%rcx, -96(%rbp)
	movq	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	movq	%r13, -80(%rbp)
	cmpq	%rbx, %rax
	je	.L442
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L444:
	movq	%rdi, -72(%rbp)
	movl	$64, %edi
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movb	$0, -96(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	-80(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L455
	movq	%rdx, 8(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 24(%rax)
.L446:
	movq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	movq	$0, 56(%rax)
	leaq	-120(%rbp), %rsi
	movq	%rdx, 16(%rax)
	movups	%xmm0, 40(%rax)
	call	*%r14
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	movq	(%rdi), %rax
	call	*24(%rax)
.L447:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L456
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movdqu	16(%rsi), %xmm1
	movq	8(%rsi), %rdi
	movb	$0, 16(%rsi)
	movq	$0, 8(%rsi)
	leaq	-96(%rbp), %rbx
	movq	%r13, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
.L442:
	movdqa	-96(%rbp), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L455:
	movdqa	-64(%rbp), %xmm3
	movups	%xmm3, 24(%rax)
	jmp	.L446
.L456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4948:
	.size	_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE:
.LFB4949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	(%rsi), %rbx
	movl	$64, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r15
	movq	8(%rsi), %r12
	movq	0(%r13), %rax
	movq	24(%rax), %r14
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	movq	%rbx, %xmm0
	movq	%r13, %rdi
	leaq	24(%rax), %rdx
	punpcklqdq	%xmm1, %xmm0
	movb	$0, 24(%rax)
	leaq	-64(%rbp), %rsi
	leaq	16+_ZTVN4node9inspector8protocol23InternalRawNotificationE(%rip), %rcx
	movq	%rdx, 8(%rax)
	movq	%rcx, (%rax)
	movq	$0, 16(%rax)
	movq	%r15, 56(%rax)
	movq	%rax, -64(%rbp)
	movups	%xmm0, 40(%rax)
	call	*%r14
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L457
	movq	(%rdi), %rax
	call	*24(%rax)
.L457:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L464:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4949:
	.size	_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawCBORNotificationESt6vectorIhSaIhEE
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_:
.LFB6359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	0(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L466
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	56(%rbx), %rsi
.L469:
	cmpq	%rsi, %r15
	je	.L510
.L467:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L466
	movq	56(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L469
.L466:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	leaq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L511
	movq	%rdx, 8(%rbx)
	movq	16(%r13), %rdx
	movq	%rdx, 24(%rbx)
.L471:
	movq	8(%r13), %rdx
	movb	$0, 16(%r13)
	leaq	32(%r12), %rdi
	movl	$1, %ecx
	movq	$0, 8(%r13)
	movq	8(%r12), %rsi
	movq	%rdx, 16(%rbx)
	movq	24(%r12), %rdx
	movq	%rax, 0(%r13)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L472
	movq	(%r12), %r8
	movq	%r15, 56(%rbx)
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L482
.L514:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L483:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movq	8(%r13), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L467
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L468
	movq	8(%rbx), %rsi
	movq	0(%r13), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L467
.L468:
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L512
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L513
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L475:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L477
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L479:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L480:
	testq	%rsi, %rsi
	je	.L477
.L478:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L479
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L486
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L478
	.p2align 4,,10
	.p2align 3
.L477:
	movq	(%r12), %rdi
	cmpq	%rdi, %r10
	je	.L481
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L481:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r8, (%r12)
	movq	%r15, 56(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L514
.L482:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L484
	movq	56(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
.L484:
	leaq	16(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L511:
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 24(%rbx)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%rdx, %rdi
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L512:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L475
.L513:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6359:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE
	.type	_ZN4node9inspector8protocol11NodeTracing10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE, @function
_ZN4node9inspector8protocol11NodeTracing10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE:
.LFB5032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%rdi, -120(%rbp)
	movq	8(%rdi), %r12
	movl	$192, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r12, %rsi
	leaq	-96(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node9inspector8protocol14DispatcherBaseC2EPNS1_15FrontendChannelE@PLT
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE(%rip), %rax
	xorl	%edx, %edx
	movss	.LC5(%rip), %xmm0
	movq	%rax, 0(%r13)
	leaq	120(%r13), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, 72(%r13)
	leaq	176(%r13), %rax
	leaq	72(%r13), %r15
	movq	%rbx, 184(%r13)
	leaq	-80(%rbp), %rbx
	movq	$1, 80(%r13)
	movq	$0, 88(%r13)
	movq	$0, 96(%r13)
	movq	$0, 112(%r13)
	movq	$0, 120(%r13)
	movq	%rax, 128(%r13)
	movq	$1, 136(%r13)
	movq	$0, 144(%r13)
	movq	$0, 152(%r13)
	movq	$0, 168(%r13)
	movq	$0, 176(%r13)
	movss	%xmm0, 104(%r13)
	movss	%xmm0, 160(%r13)
	movq	%rbx, -96(%rbp)
	movq	$25, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC6(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$7307497693186978913, %rcx
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movb	$115, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	leaq	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl13getCategoriesEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	$17, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC7(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movb	$116, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	leaq	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl5startEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	$16, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC8(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS8_SE_St10unique_ptrINSB_15DictionaryValueESt14default_deleteISG_EEPNSB_12ErrorSupportEEESaISO_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	leaq	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl4stopEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	$0, 8(%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L518
	call	_ZdlPv@PLT
.L518:
	movq	-120(%rbp), %r15
	leaq	128(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol14UberDispatcher14setupRedirectsERKSt13unordered_mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_St4hashIS9_ESt8equal_toIS9_ESaISt4pairIKS9_S9_EEE@PLT
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movabsq	$7161130589299699534, %rax
	movq	%r13, -104(%rbp)
	movq	%rax, -80(%rbp)
	movl	$28265, %eax
	movq	%rbx, -96(%rbp)
	movw	%ax, -72(%rbp)
	movb	$103, -70(%rbp)
	movq	$11, -88(%rbp)
	movb	$0, -69(%rbp)
	call	_ZN4node9inspector8protocol14UberDispatcher15registerBackendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_14DispatcherBaseESt14default_deleteISC_EE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L519
	call	_ZdlPv@PLT
.L519:
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L515
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L521
	movq	144(%r12), %r13
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L526
	.p2align 4,,10
	.p2align 3
.L522:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L527
	call	_ZdlPv@PLT
.L527:
	movq	88(%r12), %r13
	testq	%r13, %r13
	jne	.L531
	.p2align 4,,10
	.p2align 3
.L528:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L532
	call	_ZdlPv@PLT
.L532:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBaseD2Ev@PLT
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L515:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L556
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L522
.L525:
	movq	%rbx, %r13
.L526:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L523
	call	_ZdlPv@PLT
.L523:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L557
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L525
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L558:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L528
.L530:
	movq	%rbx, %r13
.L531:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	jne	.L558
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L530
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L521:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L515
.L556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5032:
	.size	_ZN4node9inspector8protocol11NodeTracing10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE, .-_ZN4node9inspector8protocol11NodeTracing10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_:
.LFB6364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L560
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	56(%rbx), %rcx
.L563:
	cmpq	%rcx, %r13
	je	.L580
.L561:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L560
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L563
.L560:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L561
	testq	%rdx, %rdx
	je	.L562
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L561
.L562:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6364:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.type	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE, @function
_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE:
.LFB5001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$72, %rdi
	subq	$104, %rsp
	movq	%rcx, -136(%rbp)
	movl	%esi, -140(%rbp)
	movq	%rdx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_MN4node9inspector8protocol11NodeTracing14DispatcherImplEFviRS7_SD_St10unique_ptrINSA_15DictionaryValueESt14default_deleteISF_EEPNSA_12ErrorSupportEEESaISN_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSP_18_Mod_range_hashingENSP_20_Default_ranged_hashENSP_20_Prime_rehash_policyENSP_17_Hashtable_traitsILb1ELb0ELb1EEEE4findESD_
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	movq	40(%r14), %rax
	addq	48(%r14), %rbx
	movq	-136(%rbp), %rcx
	movl	-140(%rbp), %r10d
	movq	%rbx, %rdi
	testb	$1, %al
	je	.L582
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
.L582:
	movq	0(%r13), %rdx
	movq	%r15, %r9
	leaq	-120(%rbp), %r8
	movl	%r10d, %esi
	movq	$0, 0(%r13)
	movq	%rdx, -120(%rbp)
	movq	%r12, %rdx
	call	*%rax
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L583
	movq	(%rdi), %rax
	call	*24(%rax)
.L583:
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L592
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L592:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5001:
	.size	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE, .-_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB7331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L635
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L615
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L636
	movabsq	$9223372036854775776, %rcx
.L595:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L613:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L597
	testq	%r10, %r10
	je	.L637
.L597:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L638
	cmpq	$1, %r9
	jne	.L600
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L601:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L617
.L642:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L603:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L633:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L639
.L606:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L603
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L636:
	testq	%rcx, %rcx
	jne	.L596
	xorl	%r13d, %r13d
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L602:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L607
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L611:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L640
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L611
.L609:
	subq	%rbx, %r12
	addq	%r12, %r8
.L607:
	testq	%r14, %r14
	je	.L612
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L612:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L611
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L615:
	movl	$32, %ecx
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L600:
	testq	%r9, %r9
	jne	.L599
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L642
.L617:
	movq	%r13, %r8
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %rdi
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L599:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L601
.L637:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L596:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L595
.L641:
	call	__stack_chk_fail@PLT
.L635:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7331:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.rodata.str1.1
.LC10:
	.string	"object expected"
.LC11:
	.string	"recordMode"
.LC12:
	.string	"string value expected"
.LC13:
	.string	"includedCategories"
.LC14:
	.string	"array expected"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing11TraceConfig9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfig9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol11NodeTracing11TraceConfig9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE:
.LFB4936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L644
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r13
	je	.L645
.L644:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-128(%rbp), %rax
	movq	$0, (%rax)
.L643:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L738
	movq	-128(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movl	$56, %edi
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %rbx
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE(%rip), %rcx
	cmpl	$6, 8(%r13)
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, -136(%rbp)
	movb	$0, 8(%rax)
	movq	$0, 24(%rax)
	movb	$0, 32(%rax)
	movq	$0, 48(%rax)
	movl	$0, %eax
	cmovne	%rax, %r13
	movq	%rcx, -176(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$25956, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$8020176954074555762, %rax
	movq	%rbx, -120(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rax, -80(%rbp)
	movw	%cx, -72(%rbp)
	movq	$10, -88(%rbp)
	movb	$0, -70(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L648
	call	_ZdlPv@PLT
.L648:
	testq	%r14, %r14
	je	.L649
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	movq	-120(%rbp), %rax
	movb	$0, -80(%rbp)
	movq	%r15, %rsi
	movq	$0, -88(%rbp)
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movq	(%r14), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L739
.L650:
	movq	-136(%rbp), %rbx
	movq	%r15, %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movb	$1, 8(%rbx)
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	leaq	-104(%rbp), %rax
	movq	-120(%rbp), %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	$18, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	.LC2(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$29541, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%rbx, %rdi
	je	.L652
	call	_ZdlPv@PLT
.L652:
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r14, %r14
	je	.L653
	cmpl	$7, 8(%r14)
	jne	.L653
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	movq	16(%r14), %rax
	cmpq	%rax, 24(%r14)
	je	.L671
	.p2align 4,,10
	.p2align 3
.L672:
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movl	%ebx, %r8d
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L658
	call	_ZdlPv@PLT
.L658:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN4node9inspector8protocol9ListValue2atEm@PLT
	movq	$0, -88(%rbp)
	movq	%rax, %rdi
	movq	-120(%rbp), %rax
	movb	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	testq	%rdi, %rdi
	je	.L661
	movq	(%rdi), %rax
	movq	%r15, %rsi
	call	*56(%rax)
	testb	%al, %al
	jne	.L660
.L661:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
.L660:
	movq	8(%r13), %r8
	cmpq	16(%r13), %r8
	je	.L662
	leaq	16(%r8), %rdi
	movq	-88(%rbp), %r9
	movq	%rdi, (%r8)
	movq	-96(%rbp), %r10
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L663
	testq	%r10, %r10
	je	.L740
.L663:
	movq	%r9, -104(%rbp)
	cmpq	$15, %r9
	ja	.L741
	cmpq	$1, %r9
	jne	.L666
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L667:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	addq	$32, 8(%r13)
.L668:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L669
	call	_ZdlPv@PLT
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L672
.L671:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L655
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	je	.L673
	.p2align 4,,10
	.p2align 3
.L677:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L674
	call	_ZdlPv@PLT
	addq	$32, %r14
	cmpq	%r14, %rbx
	jne	.L677
.L675:
	movq	0(%r13), %r14
.L673:
	testq	%r14, %r14
	je	.L678
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L678:
	movq	%r13, %rdi
	movl	$24, %esi
	xorl	%r13d, %r13d
	call	_ZdlPvm@PLT
.L655:
	movq	-136(%rbp), %rax
	movq	48(%rax), %r14
	movq	%r13, 48(%rax)
	testq	%r14, %r14
	je	.L679
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L680
	.p2align 4,,10
	.p2align 3
.L684:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L681
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L684
.L682:
	movq	(%r14), %r13
.L680:
	testq	%r13, %r13
	je	.L685
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L685:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L679:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L742
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rcx
	movq	%rcx, (%rax)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L739:
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L669:
	movq	24(%r14), %rax
	subq	16(%r14), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	ja	.L672
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L681:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L684
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L674:
	addq	$32, %r14
	cmpq	%r14, %rbx
	jne	.L677
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L666:
	testq	%r9, %r9
	je	.L667
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L741:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r9, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r10
	movq	%rax, %rdi
	movq	-160(%rbp), %r9
	movq	%rax, (%r8)
	movq	-104(%rbp), %rax
	movq	%rax, 16(%r8)
.L665:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r8, -144(%rbp)
	call	memcpy@PLT
	movq	-144(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	(%r8), %rdi
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L742:
	movq	-128(%rbp), %rax
	leaq	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev(%rip), %rdx
	movq	$0, (%rax)
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L743
	movq	-136(%rbp), %rax
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE(%rip), %rcx
	movq	48(%rax), %r12
	movq	%rcx, (%rax)
	testq	%r12, %r12
	je	.L686
	movq	8(%r12), %rbx
	movq	(%r12), %r13
	cmpq	%r13, %rbx
	je	.L687
	.p2align 4,,10
	.p2align 3
.L691:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L688
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L691
.L689:
	movq	(%r12), %r13
.L687:
	testq	%r13, %r13
	je	.L692
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L692:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L686:
	movq	-136(%rbp), %rax
	movq	16(%rax), %rdi
	cmpq	%rdi, -176(%rbp)
	je	.L693
	call	_ZdlPv@PLT
.L693:
	movq	-136(%rbp), %rdi
	movl	$56, %esi
	call	_ZdlPvm@PLT
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L688:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L691
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L653:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L743:
	movq	-136(%rbp), %rdi
	call	*%rax
	jmp	.L643
.L738:
	call	__stack_chk_fail@PLT
.L740:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE4936:
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfig9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol11NodeTracing11TraceConfig9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig5cloneEv
	.type	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig5cloneEv, @function
_ZNK4node9inspector8protocol11NodeTracing11TraceConfig5cloneEv:
.LFB4938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN4node9inspector8protocol11NodeTracing11TraceConfig9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L745
	movq	(%rdi), %rax
	call	*24(%rax)
.L745:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L751
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L751:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4938:
	.size	_ZNK4node9inspector8protocol11NodeTracing11TraceConfig5cloneEv, .-_ZNK4node9inspector8protocol11NodeTracing11TraceConfig5cloneEv
	.section	.rodata.str1.1
.LC15:
	.string	"traceConfig"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl5startEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl5startEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl5startEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE:
.LFB5029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movl	%esi, -148(%rbp)
	movq	(%r8), %rdi
	movq	%r14, %rsi
	movq	%rdx, -160(%rbp)
	movl	$29549, %edx
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -112(%rbp)
	movl	$1634886000, -96(%rbp)
	movw	%dx, -92(%rbp)
	movq	$6, -104(%rbp)
	movb	$0, -90(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testq	%rax, %rax
	je	.L753
	cmpl	$6, 8(%rax)
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	jne	.L754
	cmpq	%r12, %rdi
	je	.L823
	call	_ZdlPv@PLT
.L823:
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, -112(%rbp)
	movabsq	$7957653169325044340, %rax
	movb	$103, -86(%rbp)
	movq	%rax, -96(%rbp)
	movl	$26982, %eax
	movw	%ax, -88(%rbp)
	movq	$11, -104(%rbp)
	movb	$0, -85(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%r12, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	leaq	-136(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN4node9inspector8protocol11NodeTracing11TraceConfig9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%rbx, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L824
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol14DispatcherBase7weakPtrEv@PLT
	movq	184(%r13), %rsi
	movq	-136(%rbp), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	movq	$0, -136(%rbp)
	movq	24(%rax), %rax
	movq	%rdx, -120(%rbp)
	leaq	-120(%rbp), %rdx
	call	*%rax
	movq	-120(%rbp), %r15
	testq	%r15, %r15
	je	.L764
	movq	(%r15), %rax
	leaq	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L765
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE(%rip), %rax
	movq	%rax, (%r15)
	movq	48(%r15), %rax
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L766
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	cmpq	%r12, %rbx
	je	.L767
	.p2align 4,,10
	.p2align 3
.L771:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L768
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L771
.L769:
	movq	-176(%rbp), %rax
	movq	(%rax), %r12
.L767:
	testq	%r12, %r12
	je	.L772
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L772:
	movq	-176(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L766:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L773
	call	_ZdlPv@PLT
.L773:
	movl	$56, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L764:
	cmpl	$2, -112(%rbp)
	je	.L825
	movq	-128(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L775
	movl	-148(%rbp), %esi
	movq	%r14, %rdx
	call	_ZN4node9inspector8protocol14DispatcherBase12sendResponseEiRKNS1_16DispatchResponseE@PLT
.L775:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L776
.L830:
	call	_ZdlPv@PLT
.L776:
	movq	-128(%rbp), %r12
	testq	%r12, %r12
	je	.L763
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol14DispatcherBase7WeakPtrD1Ev@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L763:
	movq	-136(%rbp), %r13
	testq	%r13, %r13
	je	.L752
	movq	0(%r13), %rax
	leaq	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L779
	movq	48(%r13), %r14
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L780
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L781
	.p2align 4,,10
	.p2align 3
.L785:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L782
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L785
.L783:
	movq	(%r14), %r12
.L781:
	testq	%r12, %r12
	je	.L786
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L786:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L780:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L787
	call	_ZdlPv@PLT
.L787:
	movl	$56, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L752:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L826
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L785
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L753:
	movq	-112(%rbp), %rdi
.L754:
	cmpq	%r12, %rdi
	je	.L827
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L824:
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rdi
	movq	%r12, -112(%rbp)
	call	strlen@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L828
	cmpq	$1, %rax
	jne	.L760
	movzbl	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %edx
	movb	%dl, -96(%rbp)
	movq	%r12, %rdx
.L761:
	movq	%rax, -104(%rbp)
	movl	-148(%rbp), %esi
	movq	%r13, %rdi
	movq	%rbx, %r8
	movb	$0, (%rdx,%rax)
	movq	%r14, %rcx
	movl	$-32602, %edx
	call	_ZN4node9inspector8protocol14DispatcherBase19reportProtocolErrorEiNS1_16DispatchResponse9ErrorCodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_12ErrorSupportE@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L763
	call	_ZdlPv@PLT
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L827:
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L760:
	testq	%rax, %rax
	jne	.L829
	movq	%r12, %rdx
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L768:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L771
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L825:
	movq	8(%r13), %rdi
	movq	-168(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movl	-148(%rbp), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L830
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L828:
	movq	%r14, %rdi
	leaq	-120(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
.L759:
	movq	%r15, %rdx
	leaq	_ZN4node9inspector8protocol14DispatcherBase20kInvalidParamsStringE(%rip), %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L765:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L764
.L826:
	call	__stack_chk_fail@PLT
.L829:
	movq	%r12, %rdi
	jmp	.L759
	.cfi_endproc
.LFE5029:
	.size	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl5startEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl5startEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EEPNS1_12ErrorSupportE
	.section	.text._ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB7557:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L855
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L846
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L856
.L833:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L845:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L835
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L839:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L836
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*24(%rcx)
	cmpq	%r14, %r15
	jne	.L839
.L837:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L835:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L840
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L848
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L842:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L842
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L843
.L841:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L843:
	leaq	8(%rcx,%r9), %rcx
.L840:
	testq	%r12, %r12
	je	.L844
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L844:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L836:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L839
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L856:
	testq	%rdi, %rdi
	jne	.L834
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L846:
	movl	$8, %r13d
	jmp	.L833
.L848:
	movq	%rcx, %rdx
	jmp	.L841
.L834:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L833
.L855:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7557:
	.size	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata.str1.1
.LC16:
	.string	"value"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.type	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, @function
_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE:
.LFB4939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L858
	cmpl	$6, 8(%rsi)
	movq	%rsi, %r14
	je	.L859
.L858:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	$0, 0(%r13)
.L857:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L934
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	movl	$16, %edi
	leaq	-80(%rbp), %rbx
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE(%rip), %rcx
	cmpl	$6, 8(%r14)
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -144(%rbp)
	movq	$0, 8(%rax)
	movl	$0, %eax
	cmovne	%rax, %r14
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rbx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rbx, -96(%rbp)
	movl	$1970037110, -80(%rbp)
	movb	$101, -76(%rbp)
	movq	$5, -88(%rbp)
	movb	$0, -75(%rbp)
	call	_ZNK4node9inspector8protocol15DictionaryValue3getERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	cmpq	%rbx, %rdi
	je	.L862
	call	_ZdlPv@PLT
.L862:
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameEPKc@PLT
	testq	%r15, %r15
	je	.L863
	cmpl	$7, 8(%r15)
	jne	.L863
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 16(%rax)
	movq	%rax, %r14
	movups	%xmm0, (%rax)
	call	_ZN4node9inspector8protocol12ErrorSupport4pushEv@PLT
	movq	16(%r15), %rax
	cmpq	%rax, 24(%r15)
	je	.L876
	leaq	-104(%rbp), %rax
	movq	%r13, -152(%rbp)
	xorl	%ebx, %ebx
	movq	-120(%rbp), %r13
	movq	%rax, -136(%rbp)
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L936:
	movq	$0, -112(%rbp)
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 8(%r14)
.L873:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L874
	movq	(%rdi), %rax
	call	*24(%rax)
.L874:
	movq	24(%r15), %rax
	subq	16(%r15), %rax
	addq	$1, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jbe	.L935
.L877:
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movl	%ebx, %r8d
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol12ErrorSupport7setNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	-128(%rbp), %rdi
	je	.L868
	call	_ZdlPv@PLT
.L868:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol9ListValue2atEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L869
	cmpl	$6, 8(%rax)
	je	.L870
.L869:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	movq	-120(%rbp), %r8
.L870:
	movq	(%r8), %rax
	movq	-136(%rbp), %rdi
	movq	%r8, %rsi
	call	*88(%rax)
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L871
	cmpl	$6, 8(%rax)
	movl	$0, %ecx
	cmovne	%rcx, %rax
.L871:
	movq	%rax, -112(%rbp)
	movq	8(%r14), %rsi
	cmpq	16(%r14), %rsi
	jne	.L936
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN4node9inspector8protocol15DictionaryValueESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L935:
	movq	-152(%rbp), %r13
.L876:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	je	.L865
	movq	8(%r14), %rbx
	movq	(%r14), %r15
	cmpq	%r15, %rbx
	je	.L878
	.p2align 4,,10
	.p2align 3
.L882:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L879
	movq	(%rdi), %rax
	addq	$8, %r15
	call	*24(%rax)
	cmpq	%r15, %rbx
	jne	.L882
.L880:
	movq	(%r14), %r15
.L878:
	testq	%r15, %r15
	je	.L883
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L883:
	movq	%r14, %rdi
	movl	$24, %esi
	xorl	%r14d, %r14d
	call	_ZdlPvm@PLT
.L865:
	movq	-144(%rbp), %rax
	movq	8(%rax), %r15
	movq	%r14, 8(%rax)
	testq	%r15, %r15
	je	.L884
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L885
	.p2align 4,,10
	.p2align 3
.L889:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L886
	movq	(%rdi), %rax
	addq	$8, %r14
	call	*24(%rax)
	cmpq	%r14, %rbx
	jne	.L889
.L887:
	movq	(%r15), %r14
.L885:
	testq	%r14, %r14
	je	.L890
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L890:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L884:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport3popEv@PLT
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupport9hasErrorsEv@PLT
	testb	%al, %al
	jne	.L937
	movq	-144(%rbp), %rax
	movq	%rax, 0(%r13)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L886:
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.L889
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L879:
	addq	$8, %r15
	cmpq	%r15, %rbx
	jne	.L882
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L937:
	movq	-144(%rbp), %rax
	leaq	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev(%rip), %rdx
	movq	$0, 0(%r13)
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L938
	movq	-144(%rbp), %rax
	leaq	16+_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE(%rip), %rcx
	movq	8(%rax), %r14
	movq	%rcx, (%rax)
	testq	%r14, %r14
	je	.L891
	movq	8(%r14), %rbx
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L892
	.p2align 4,,10
	.p2align 3
.L896:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L893
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L896
.L894:
	movq	(%r14), %r12
.L892:
	testq	%r12, %r12
	je	.L897
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L897:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L891:
	movq	-144(%rbp), %rdi
	movl	$16, %esi
	call	_ZdlPvm@PLT
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L893:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L896
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L863:
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN4node9inspector8protocol12ErrorSupport8addErrorEPKc@PLT
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L938:
	movq	-144(%rbp), %rdi
	call	*%rax
	jmp	.L857
.L934:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4939:
	.size	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE, .-_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification5cloneEv
	.type	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification5cloneEv, @function
_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification5cloneEv:
.LFB4941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol12ErrorSupportC1Ev@PLT
	leaq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification7toValueEv
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification9fromValueEPNS1_5ValueEPNS1_12ErrorSupportE
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L940
	movq	(%rdi), %rax
	call	*24(%rax)
.L940:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol12ErrorSupportD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L946
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L946:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4941:
	.size	_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification5cloneEv, .-_ZNK4node9inspector8protocol11NodeTracing25DataCollectedNotification5cloneEv
	.weak	_ZTVN4node9inspector8protocol23InternalRawNotificationE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol23InternalRawNotificationE,"awG",@progbits,_ZTVN4node9inspector8protocol23InternalRawNotificationE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol23InternalRawNotificationE, @object
	.size	_ZTVN4node9inspector8protocol23InternalRawNotificationE, 48
_ZTVN4node9inspector8protocol23InternalRawNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol23InternalRawNotification15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol23InternalRawNotification17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol23InternalRawNotificationD1Ev
	.quad	_ZN4node9inspector8protocol23InternalRawNotificationD0Ev
	.weak	_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE,"awG",@progbits,_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE, @object
	.size	_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE, 48
_ZTVN4node9inspector8protocol11NodeTracing11TraceConfigE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol11NodeTracing11TraceConfig15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol11NodeTracing11TraceConfig17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD1Ev
	.quad	_ZN4node9inspector8protocol11NodeTracing11TraceConfigD0Ev
	.weak	_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE,"awG",@progbits,_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE, @object
	.size	_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE, 48
_ZTVN4node9inspector8protocol11NodeTracing25DataCollectedNotificationE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotification17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD1Ev
	.quad	_ZN4node9inspector8protocol11NodeTracing25DataCollectedNotificationD0Ev
	.weak	_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE,"awG",@progbits,_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE, @object
	.size	_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE, 48
_ZTVN4node9inspector8protocol11NodeTracing14DispatcherImplE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD1Ev
	.quad	_ZN4node9inspector8protocol11NodeTracing14DispatcherImplD0Ev
	.quad	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl11canDispatchERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector8protocol11NodeTracing14DispatcherImpl8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_St10unique_ptrINS1_15DictionaryValueESt14default_deleteISD_EE
	.globl	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum22RecordAsMuchAsPossibleE
	.section	.rodata.str1.1
.LC17:
	.string	"recordAsMuchAsPossible"
	.section	.data.rel.local,"aw"
	.align 8
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum22RecordAsMuchAsPossibleE, @object
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum22RecordAsMuchAsPossibleE, 8
_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum22RecordAsMuchAsPossibleE:
	.quad	.LC17
	.globl	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum18RecordContinuouslyE
	.section	.rodata.str1.1
.LC18:
	.string	"recordContinuously"
	.section	.data.rel.local
	.align 8
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum18RecordContinuouslyE, @object
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum18RecordContinuouslyE, 8
_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum18RecordContinuouslyE:
	.quad	.LC18
	.globl	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum15RecordUntilFullE
	.section	.rodata.str1.1
.LC19:
	.string	"recordUntilFull"
	.section	.data.rel.local
	.align 8
	.type	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum15RecordUntilFullE, @object
	.size	_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum15RecordUntilFullE, 8
_ZN4node9inspector8protocol11NodeTracing11TraceConfig14RecordModeEnum15RecordUntilFullE:
	.quad	.LC19
	.globl	_ZN4node9inspector8protocol11NodeTracing8Metainfo7versionE
	.section	.rodata
	.type	_ZN4node9inspector8protocol11NodeTracing8Metainfo7versionE, @object
	.size	_ZN4node9inspector8protocol11NodeTracing8Metainfo7versionE, 4
_ZN4node9inspector8protocol11NodeTracing8Metainfo7versionE:
	.string	"1.0"
	.globl	_ZN4node9inspector8protocol11NodeTracing8Metainfo13commandPrefixE
	.align 8
	.type	_ZN4node9inspector8protocol11NodeTracing8Metainfo13commandPrefixE, @object
	.size	_ZN4node9inspector8protocol11NodeTracing8Metainfo13commandPrefixE, 13
_ZN4node9inspector8protocol11NodeTracing8Metainfo13commandPrefixE:
	.string	"NodeTracing."
	.globl	_ZN4node9inspector8protocol11NodeTracing8Metainfo10domainNameE
	.align 8
	.type	_ZN4node9inspector8protocol11NodeTracing8Metainfo10domainNameE, @object
	.size	_ZN4node9inspector8protocol11NodeTracing8Metainfo10domainNameE, 12
_ZN4node9inspector8protocol11NodeTracing8Metainfo10domainNameE:
	.string	"NodeTracing"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	7234298831932976745
	.quad	7598258011201888579
	.align 16
.LC3:
	.quad	7161130589299699534
	.quad	7022344801880600169
	.align 16
.LC4:
	.quad	7161130589299699534
	.quad	7161130725816102505
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC5:
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC6:
	.quad	7161130589299699534
	.quad	4860621391674175081
	.align 16
.LC7:
	.quad	7161130589299699534
	.quad	8241996831113309801
	.align 16
.LC8:
	.quad	7161130589299699534
	.quad	8101822292711403113
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
