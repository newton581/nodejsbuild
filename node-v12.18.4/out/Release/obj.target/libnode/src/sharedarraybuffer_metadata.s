	.file	"sharedarraybuffer_metadata.cc"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7375:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7375:
	.size	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner8SelfSizeEv, @function
_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner8SelfSizeEv:
.LFB7377:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE7377:
	.size	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner8SelfSizeEv, .-_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner8SelfSizeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB9798:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9798:
	.size	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB9838:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9838:
	.size	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB9800:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9800:
	.size	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB9837:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9837:
	.size	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,"axG",@progbits,_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.type	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, @function
_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_:
.LFB7115:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %rdi
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7115:
	.size	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, .-_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.text
	.align 2
	.p2align 4
	.type	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0, @function
_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0:
.LFB9941:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r8
	movq	8(%rdi), %rcx
	movq	(%rdi), %r13
	movq	%r8, %rax
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r10
	testq	%r10, %r10
	je	.L21
	movq	%rdi, %rbx
	movq	(%r10), %rdi
	movq	%rdx, %r12
	movq	%r10, %r11
	movq	32(%rdi), %r9
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rdi), %r15
	testq	%r15, %r15
	je	.L21
	movq	32(%r15), %r9
	xorl	%edx, %edx
	movq	%rdi, %r11
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r12
	jne	.L21
	movq	%r15, %rdi
.L13:
	cmpq	%r9, %r8
	jne	.L11
	movq	8(%rdi), %rax
	cmpq	%rax, (%rsi)
	jne	.L11
	cmpq	16(%rdi), %r8
	jne	.L11
	movq	(%rdi), %rsi
	cmpq	%r11, %r10
	je	.L28
	testq	%rsi, %rsi
	je	.L15
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r12
	je	.L15
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L15:
	movq	%rsi, (%r11)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L22
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r12
	je	.L15
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L14:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L29
.L16:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r11, %rax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, 16(%rbx)
	jmp	.L16
	.cfi_endproc
.LFE9941:
	.size	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0, .-_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	.align 2
	.p2align 4
	.type	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner14MemoryInfoNameEv, @function
_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner14MemoryInfoNameEv:
.LFB7376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$29285, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7376:
	.size	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner14MemoryInfoNameEv, .-_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner14MemoryInfoNameEv
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv, @function
_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv:
.LFB7374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %rdx
	movq	16(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movdqu	80(%rdx), %xmm0
	movq	88(%rdx), %rax
	movaps	%xmm0, -48(%rbp)
	testq	%rax, %rax
	je	.L35
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L36
	lock addl	$1, 8(%rax)
.L35:
	leaq	-48(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEE@PLT
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L34
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L39
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L49
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L34
.L49:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L42
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L43:
	cmpl	$1, %eax
	jne	.L34
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L42:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L43
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7374:
	.size	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv, .-_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB9836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L51
	movq	64(%r12), %rdx
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	call	*56(%r12)
	movq	88(%r12), %r13
	testq	%r13, %r13
	je	.L54
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L55
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L56:
	cmpl	$1, %eax
	jne	.L54
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L58
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L59:
	cmpl	$1, %eax
	je	.L66
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L61
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L62
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L63:
	cmpl	$1, %eax
	jne	.L61
	movq	(%rdi), %rax
	call	*24(%rax)
.L61:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L55:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L66:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L58:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L59
	.cfi_endproc
.LFE9836:
	.size	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	subq	$1, 2656(%rdi)
	addq	$2584, %rdi
	movq	%rax, -48(%rbp)
	movq	%rbx, -40(%rbp)
	movq	$0, -32(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L83
.L69:
	cmpq	$0, 8(%rbx)
	je	.L67
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L73
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L84
.L73:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L67:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L83:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L86
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L69
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L69
.L86:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7097:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD2Ev, @function
_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD2Ev:
.LFB7371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-64(%rbp), %rsi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE(%rip), %rax
	movq	%rdi, -56(%rbp)
	movq	%rax, (%rdi)
	leaq	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv(%rip), %rax
	movq	%rax, -64(%rbp)
	movq	16(%rdi), %rax
	movq	$0, -48(%rbp)
	leaq	2584(%rax), %rdi
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L89
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L90
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L97
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L89
.L97:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L93
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L94:
	cmpl	$1, %eax
	jne	.L89
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L93:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L94
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7371:
	.size	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD2Ev, .-_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD2Ev
	.set	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD1Ev,_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD0Ev, @function
_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD0Ev:
.LFB7373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-64(%rbp), %rsi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE(%rip), %rax
	movq	%rdi, -56(%rbp)
	movq	%rax, (%rdi)
	leaq	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv(%rip), %rax
	movq	%rax, -64(%rbp)
	movq	16(%rdi), %rax
	movq	$0, -48(%rbp)
	leaq	2584(%rax), %rdi
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L101
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L102
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L109
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L101
.L109:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L105
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L106:
	cmpl	$1, %eax
	jne	.L101
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L105:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L106
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7373:
	.size	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD0Ev, .-_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD0Ev
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L112
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE(%rip), %rax
	movq	%rdi, -56(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%rax, (%rdi)
	leaq	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv(%rip), %rax
	movq	%rax, -64(%rbp)
	movq	16(%rdi), %rax
	movq	$0, -48(%rbp)
	leaq	2584(%rax), %rdi
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L114
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L115
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L116:
	cmpl	$1, %eax
	je	.L123
.L114:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L111:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	call	*%rax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L123:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L118
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L119:
	cmpl	$1, %eax
	jne	.L114
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L115:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L118:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L119
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7111:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L126:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L146
.L127:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L128
	movq	8(%rax), %rax
	leaq	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L129
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE(%rip), %rax
	movq	16(%r12), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rax, (%r12)
	leaq	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv(%rip), %rax
	addq	$2584, %rdi
	movq	%rax, -64(%rbp)
	movq	%r12, -56(%rbp)
	movq	$0, -48(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L131
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L132
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L133:
	cmpl	$1, %eax
	jne	.L131
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L135
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L136:
	cmpl	$1, %eax
	je	.L147
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L125:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L146:
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L127
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L147:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L131
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7109:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7099:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"SABLifetimePartner"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE
	.type	_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE, @function
_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE:
.LFB7391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rcx, -64(%rbp)
	call	_ZNK2v817SharedArrayBuffer10IsExternalEv@PLT
	testb	%al, %al
	je	.L257
	movq	3208(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L162
	movq	352(%rbx), %r10
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %r13
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r10, -56(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	testq	%r15, %r15
	movq	-56(%rbp), %r10
	movq	$0, 3208(%rbx)
	je	.L256
.L159:
	movq	%r10, %rdi
	movq	%r15, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3208(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L162
.L256:
	movq	352(%rbx), %r10
.L153:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	movq	%r10, %rdi
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$18, %ecx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L258
.L157:
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3208(%rbx), %rdi
	movq	352(%rbx), %r10
	testq	%rdi, %rdi
	jne	.L259
	testq	%r15, %r15
	jne	.L159
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r14, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L260
.L154:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L261
	movq	8(%r12), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L262
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L164:
	testl	%eax, %eax
	je	.L207
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L164
	movq	(%r12), %rax
	movl	$48, %edi
	movq	%rax, -72(%rbp)
	call	_Znwm@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L263
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	2592(%rbx), %r8
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r12, %rax
	divq	%r8
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r9
	testq	%rdi, %rdi
	je	.L166
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L166
	movq	32(%rsi), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L166
	movq	%rsi, %rax
.L169:
	cmpq	%rcx, %r12
	jne	.L167
	cmpq	%r11, 8(%rax)
	jne	.L167
	cmpq	16(%rax), %r12
	jne	.L167
	cmpq	$0, (%rdi)
	je	.L166
.L203:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	2616(%rbx), %rax
	movl	$1, %ecx
	movq	%r8, %rsi
	movq	%r9, -88(%rbp)
	movq	2608(%rbx), %rdx
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rcx
	testb	%al, %al
	jne	.L170
	movq	2584(%rbx), %r8
	movq	-88(%rbp), %r9
.L171:
	addq	%r8, %r9
	movq	%r12, 32(%r15)
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L180
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%r9), %rax
	movq	%r15, (%rax)
.L181:
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE(%rip), %rax
	movq	-72(%rbp), %xmm0
	addq	$1, 2608(%rbx)
	movq	%rax, (%r12)
	movq	24(%r12), %rax
	movhps	-56(%rbp), %xmm0
	addq	$1, 2656(%rbx)
	movups	%xmm0, 32(%r12)
	testq	%rax, %rax
	je	.L185
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L184
.L185:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L184:
	movq	2640(%rbx), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -56(%rbp)
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	2592(%rbx), %r8
	leaq	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartner11CleanupHookEPv(%rip), %r11
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r12, %rax
	divq	%r8
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r9
	testq	%rdi, %rdi
	je	.L186
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L187:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L186
	movq	32(%rsi), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L186
	movq	%rsi, %rax
.L189:
	cmpq	%r12, %rcx
	jne	.L187
	cmpq	%r11, 8(%rax)
	jne	.L187
	cmpq	16(%rax), %rcx
	jne	.L187
	cmpq	$0, (%rdi)
	jne	.L203
.L186:
	movq	-80(%rbp), %rdi
	movq	%r8, %rsi
	movl	$1, %ecx
	movq	%r9, -56(%rbp)
	movq	2608(%rbx), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L190
	movq	2584(%rbx), %r10
	movq	-56(%rbp), %r9
.L191:
	addq	%r10, %r9
	movq	%r12, 32(%r15)
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L200
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%r9), %rax
	movq	%r15, (%rax)
.L201:
	movq	360(%rbx), %rax
	movq	-64(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r14, %rsi
	addq	$1, 2608(%rbx)
	movq	120(%rax), %rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	movb	$0, %ah
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L264
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L194
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -72(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r10
	leaq	2632(%rbx), %rax
	movq	%rax, -56(%rbp)
.L193:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L195
	xorl	%r9d, %r9d
	leaq	2600(%rbx), %r11
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L197:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L198:
	testq	%rsi, %rsi
	je	.L195
.L196:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r8
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L197
	movq	2600(%rbx), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L209
	movq	%rcx, (%r10,%r9,8)
	movq	%rdx, %r9
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L170:
	cmpq	$1, %rdx
	je	.L265
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L194
	leaq	0(,%rdx,8), %rdx
	movq	%rcx, -96(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-96(%rbp), %rcx
	movq	%rax, %r8
	leaq	2632(%rbx), %rax
	movq	%rax, -88(%rbp)
.L173:
	movq	2600(%rbx), %rdi
	movq	$0, 2600(%rbx)
	testq	%rdi, %rdi
	je	.L175
	xorl	%r10d, %r10d
	leaq	2600(%rbx), %r11
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L177:
	movq	(%r9), %rdx
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
.L178:
	testq	%rdi, %rdi
	je	.L175
.L176:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%rcx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r9
	testq	%r9, %r9
	jne	.L177
	movq	2600(%rbx), %r9
	movq	%r9, (%rsi)
	movq	%rsi, 2600(%rbx)
	movq	%r11, (%rax)
	cmpq	$0, (%rsi)
	je	.L208
	movq	%rsi, (%r8,%r10,8)
	movq	%rdx, %r10
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L175:
	movq	2584(%rbx), %rdi
	cmpq	%rdi, -88(%rbp)
	je	.L179
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r8
.L179:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rcx, 2592(%rbx)
	divq	%rcx
	movq	%r8, 2584(%rbx)
	leaq	0(,%rdx,8), %r9
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	_ZZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	movq	2600(%rbx), %rax
	movq	%r15, 2600(%rbx)
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L182
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r15, (%r8,%rdx,8)
.L182:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r9)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L195:
	movq	2584(%rbx), %rdi
	cmpq	-56(%rbp), %rdi
	je	.L199
	movq	%r8, -72(%rbp)
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %r10
.L199:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r8, 2592(%rbx)
	divq	%r8
	movq	%r10, 2584(%rbx)
	leaq	0(,%rdx,8), %r9
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rdx, %r10
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L200:
	movq	2600(%rbx), %rax
	movq	%r15, 2600(%rbx)
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L202
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r15, (%r10,%rdx,8)
.L202:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r9)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%rdx, %r9
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	2632(%rbx), %r8
	movq	$0, 2632(%rbx)
	movq	%r8, -88(%rbp)
	jmp	.L173
.L264:
	leaq	2632(%rbx), %r10
	movq	$0, 2632(%rbx)
	movq	%r10, -56(%rbp)
	jmp	.L193
.L194:
	call	_ZSt17__throw_bad_allocv@PLT
.L262:
	jmp	.L207
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE.cold, @function
_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE.cold:
.LFSB7391:
.L207:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE7391:
	.text
	.size	_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE, .-_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE
	.section	.text.unlikely
	.size	_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE.cold, .-_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE.cold
.LCOLDE2:
	.text
.LHOTE2:
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"ERR_TRANSFERRING_EXTERNALIZED_SHAREDARRAYBUFFER"
	.align 8
.LC4:
	.string	"Cannot serialize externalized SharedArrayBuffer"
	.section	.rodata.str1.1
.LC5:
	.string	"code"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE
	.type	_ZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE, @function
_ZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE:
.LFB7378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rcx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	360(%rsi), %rax
	movq	%rbx, %rsi
	movq	120(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	testq	%rax, %rax
	je	.L346
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L327
.L269:
	movq	%r12, %rdi
	call	_ZNK2v817SharedArrayBuffer10IsExternalEv@PLT
	testb	%al, %al
	jne	.L347
	movq	2368(%r14), %r9
	testq	%r9, %r9
	je	.L287
	leaq	-128(%rbp), %r15
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN4node6worker6Worker22array_buffer_allocatorEv@PLT
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZN2v817SharedArrayBuffer11ExternalizeEv@PLT
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r9
	testq	%r8, %r8
	je	.L289
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	leaq	8(%r8), %rcx
	testq	%rdx, %rdx
	je	.L290
	lock addl	$1, (%rcx)
.L291:
	movq	%r8, %xmm2
	movq	%r9, %xmm0
	movl	$96, %edi
	movq	%rdx, -168(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, -160(%rbp)
	movq	%r8, -152(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	_Znwm@PLT
	movq	-168(%rbp), %rdx
	movdqa	-128(%rbp), %xmm3
	pxor	%xmm1, %xmm1
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	movq	%rax, %r15
	movups	%xmm1, (%rax)
	movdqa	-80(%rbp), %xmm6
	testq	%rdx, %rdx
	movdqa	-144(%rbp), %xmm0
	movups	%xmm3, 16(%rax)
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rcx
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm0, 80(%rax)
	je	.L348
	lock addl	$1, (%rcx)
.L292:
	movl	$24, %edi
	movq	%r8, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %r8
	movq	%rax, %r9
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r9)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r9)
	movq	8(%r15), %rax
	movq	%r15, 16(%r9)
	testq	%rax, %rax
	je	.L293
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L293
.L294:
	testq	%r8, %r8
	je	.L302
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L303
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r8)
.L304:
	cmpl	$1, %eax
	je	.L349
.L302:
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r9
	testb	%al, %al
	jne	.L308
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	pxor	%xmm0, %xmm0
	movups	%xmm0, 0(%r13)
	testq	%rdx, %rdx
	je	.L350
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r9)
.L312:
	cmpl	$1, %eax
	je	.L351
.L311:
	testq	%r8, %r8
	je	.L266
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L318
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r8)
.L319:
	cmpl	$1, %eax
	jne	.L266
	movq	(%r8), %rax
	movq	%rdx, -152(%rbp)
	movq	%r8, %rdi
	movq	%r8, -144(%rbp)
	call	*16(%rax)
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %r8
	testq	%rdx, %rdx
	je	.L320
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r8)
.L321:
	cmpl	$1, %eax
	jne	.L266
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L327:
	movq	3208(%r14), %rdi
	movq	%r15, %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L269
	movq	%r12, %rdi
	call	_ZNK2v817SharedArrayBuffer10IsExternalEv@PLT
	testb	%al, %al
	je	.L352
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L353
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L329
	cmpw	$1040, %cx
	je	.L329
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L346:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 0(%r13)
.L266:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L354
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	leaq	-128(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r9, -144(%rbp)
	call	_ZN2v817SharedArrayBuffer11ExternalizeEv@PLT
	movq	-144(%rbp), %r9
.L289:
	movl	$96, %edi
	movq	%r9, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	movq	-144(%rbp), %r9
	movq	$0, 88(%rax)
	xorl	%r8d, %r8d
	movups	%xmm7, 16(%rax)
	movq	%r9, 80(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L293:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movq	%r15, (%r15)
	testq	%rdx, %rdx
	je	.L355
	lock addl	$1, 12(%r9)
.L295:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L297
	testq	%rdx, %rdx
	je	.L298
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L299:
	cmpl	$1, %eax
	jne	.L297
	movq	(%rdi), %rax
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	*24(%rax)
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r9, 8(%r15)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L329:
	movq	23(%rdx), %rax
.L277:
	testq	%rax, %rax
	je	.L356
	movq	32(%rax), %rdx
	movq	40(%rax), %rax
	movq	%rdx, 0(%r13)
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L266
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L281
	lock addl	$1, 8(%rax)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r15, %xmm0
	movq	%r9, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L347:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L357
.L282:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L358
.L283:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L359
.L284:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L360
.L285:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L361
.L286:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 0(%r13)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L290:
	addl	$1, 8(%r8)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L348:
	addl	$1, 8(%r8)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L318:
	movl	8(%r8), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r8)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L350:
	movl	8(%r9), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r9)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L303:
	movl	8(%r8), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r8)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L349:
	movq	(%r8), %rax
	movq	%rdx, -160(%rbp)
	movq	%r8, %rdi
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	*16(%rax)
	movq	-160(%rbp), %rdx
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r9
	testq	%rdx, %rdx
	je	.L306
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r8)
.L307:
	cmpl	$1, %eax
	jne	.L302
	movq	(%r8), %rax
	movq	%r9, -152(%rbp)
	movq	%r8, %rdi
	movq	%r8, -144(%rbp)
	call	*24(%rax)
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %r8
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L351:
	movq	(%r9), %rax
	movq	%rdx, -160(%rbp)
	movq	%r9, %rdi
	movq	%r8, -152(%rbp)
	movq	%r9, -144(%rbp)
	call	*16(%rax)
	movq	-160(%rbp), %rdx
	movq	-144(%rbp), %r9
	movq	-152(%rbp), %r8
	testq	%rdx, %rdx
	je	.L314
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r9)
.L315:
	cmpl	$1, %eax
	jne	.L311
	movq	(%r9), %rax
	movq	%r8, -144(%rbp)
	movq	%r9, %rdi
	call	*24(%rax)
	movq	-144(%rbp), %r8
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L298:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L306:
	movl	12(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r8)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L320:
	movl	12(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r8)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L314:
	movl	12(%r9), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r9)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L355:
	addl	$1, 12(%r9)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L281:
	addl	$1, 8(%rax)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L360:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L359:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L358:
	movq	%rax, -144(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-144(%rbp), %rdi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L357:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L282
.L352:
	leaq	_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L353:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L356:
	leaq	_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7378:
	.size	_ZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE, .-_ZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker25SharedArrayBufferMetadataC2ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE
	.type	_ZN4node6worker25SharedArrayBufferMetadataC2ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE, @function
_ZN4node6worker25SharedArrayBufferMetadataC2ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE:
.LFB7408:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movdqu	(%rdx), %xmm5
	movq	8(%rdx), %rax
	movups	%xmm0, (%rdi)
	movdqu	(%rsi), %xmm1
	movdqu	16(%rsi), %xmm2
	movdqu	32(%rsi), %xmm3
	movdqu	48(%rsi), %xmm4
	movups	%xmm5, 80(%rdi)
	movups	%xmm1, 16(%rdi)
	movups	%xmm2, 32(%rdi)
	movups	%xmm3, 48(%rdi)
	movups	%xmm4, 64(%rdi)
	testq	%rax, %rax
	je	.L362
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L364
	lock addl	$1, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	addl	$1, 8(%rax)
.L362:
	ret
	.cfi_endproc
.LFE7408:
	.size	_ZN4node6worker25SharedArrayBufferMetadataC2ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE, .-_ZN4node6worker25SharedArrayBufferMetadataC2ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE
	.globl	_ZN4node6worker25SharedArrayBufferMetadataC1ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE
	.set	_ZN4node6worker25SharedArrayBufferMetadataC1ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE,_ZN4node6worker25SharedArrayBufferMetadataC2ERKN2v817SharedArrayBuffer8ContentsESt10shared_ptrINS2_11ArrayBuffer9AllocatorEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker25SharedArrayBufferMetadataD2Ev
	.type	_ZN4node6worker25SharedArrayBufferMetadataD2Ev, @function
_ZN4node6worker25SharedArrayBufferMetadataD2Ev:
.LFB7411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdx
	movq	24(%rdi), %rsi
	movq	16(%rdi), %rdi
	call	*56(%rbx)
	movq	88(%rbx), %r12
	testq	%r12, %r12
	je	.L370
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L371
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L382
	.p2align 4,,10
	.p2align 3
.L370:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L368
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L378
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L383
.L368:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L370
.L382:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L374
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L375:
	cmpl	$1, %eax
	jne	.L370
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L378:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L368
.L383:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L375
	.cfi_endproc
.LFE7411:
	.size	_ZN4node6worker25SharedArrayBufferMetadataD2Ev, .-_ZN4node6worker25SharedArrayBufferMetadataD2Ev
	.globl	_ZN4node6worker25SharedArrayBufferMetadataD1Ev
	.set	_ZN4node6worker25SharedArrayBufferMetadataD1Ev,_ZN4node6worker25SharedArrayBufferMetadataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node6worker25SharedArrayBufferMetadata20GetSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE
	.type	_ZN4node6worker25SharedArrayBufferMetadata20GetSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE, @function
_ZN4node6worker25SharedArrayBufferMetadata20GetSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE:
.LFB7413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rsi
	movq	352(%r13), %rdi
	call	_ZN2v817SharedArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%rax, %rbx
	call	_ZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7413:
	.size	_ZN4node6worker25SharedArrayBufferMetadata20GetSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE, .-_ZN4node6worker25SharedArrayBufferMetadata20GetSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE, @object
	.size	_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE, 88
_ZTVN4node6worker12_GLOBAL__N_118SABLifetimePartnerE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD1Ev
	.quad	_ZN4node6worker12_GLOBAL__N_118SABLifetimePartnerD0Ev
	.quad	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner14MemoryInfoNameEv
	.quad	_ZNK4node6worker12_GLOBAL__N_118SABLifetimePartner8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.weak	_ZTVSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node6worker25SharedArrayBufferMetadataELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"../src/sharedarraybuffer_metadata.cc:138"
	.section	.rodata.str1.1
.LC7:
	.string	"target->IsExternal()"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"v8::Maybe<bool> node::worker::SharedArrayBufferMetadata::AssignToSharedArrayBuffer(node::Environment*, v8::Local<v8::Context>, v8::Local<v8::SharedArrayBuffer>)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args, @object
	.size	_ZZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args, 24
_ZZN4node6worker25SharedArrayBufferMetadata25AssignToSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args:
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"../src/sharedarraybuffer_metadata.cc:107"
	.section	.rodata.str1.1
.LC10:
	.string	"(partner) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"static node::worker::SharedArrayBufferMetadataReference node::worker::SharedArrayBufferMetadata::ForSharedArrayBuffer(node::Environment*, v8::Local<v8::Context>, v8::Local<v8::SharedArrayBuffer>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args_0, @object
	.size	_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args_0, 24
_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args_0:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"../src/sharedarraybuffer_metadata.cc:104"
	.section	.rodata.str1.1
.LC13:
	.string	"source->IsExternal()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args, @object
	.size	_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args, 24
_ZZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEEE4args:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC11
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC14:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC16:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC19:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC20:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC22:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC23:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC25:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC26:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC28:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	8387222483484426579
	.quad	7959112203756727657
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
