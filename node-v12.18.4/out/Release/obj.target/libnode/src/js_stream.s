	.file	"js_stream.cc"
	.text
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6063:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6063:
	.size	_ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node8JSStream8SelfSizeEv,"axG",@progbits,_ZNK4node8JSStream8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8JSStream8SelfSizeEv
	.type	_ZNK4node8JSStream8SelfSizeEv, @function
_ZNK4node8JSStream8SelfSizeEv:
.LFB6065:
	.cfi_startproc
	endbr64
	movl	$120, %eax
	ret
	.cfi_endproc
.LFE6065:
	.size	_ZNK4node8JSStream8SelfSizeEv, .-_ZNK4node8JSStream8SelfSizeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7288:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7288:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream12GetAsyncWrapEv
	.type	_ZN4node8JSStream12GetAsyncWrapEv, @function
_ZN4node8JSStream12GetAsyncWrapEv:
.LFB7766:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE7766:
	.size	_ZN4node8JSStream12GetAsyncWrapEv, .-_ZN4node8JSStream12GetAsyncWrapEv
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream7IsAliveEv
	.type	_ZN4node8JSStream7IsAliveEv, @function
_ZN4node8JSStream7IsAliveEv:
.LFB7767:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7767:
	.size	_ZN4node8JSStream7IsAliveEv, .-_ZN4node8JSStream7IsAliveEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9857:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9857:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9858:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9858:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9860:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE9860:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9861:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9861:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9862:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9862:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9864:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE9864:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7724:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L21:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7724:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7723:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7723:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev:
.LFB9816:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	addq	$16, %rdi
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9816:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB9818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9818:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB9814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	movq	%rax, (%r12)
	call	uv_buf_init@PLT
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	testq	%r13, %r13
	je	.L32
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L38
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L32:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9814:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$72, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L44
	cmpw	$1040, %cx
	jne	.L40
.L44:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L46
.L43:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	32(%rbx), %rsi
	leaq	16(%r12), %rdi
	movsd	.LC0(%rip), %xmm0
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movq	%r13, %rdx
	movl	$26, %ecx
	movq	%rax, (%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L43
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7749:
	.size	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream10ReadBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8JSStream10ReadBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8JSStream10ReadBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L76
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L65
	cmpw	$1040, %cx
	jne	.L49
.L65:
	movq	23(%rdx), %r13
.L51:
	testq	%r13, %r13
	je	.L47
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L77
	movq	8(%rbx), %r12
.L54:
	movq	%r12, %rdi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L78
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	ja	.L58
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L79
.L58:
	movq	%r12, %rdi
	leaq	-208(%rbp), %r15
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r14
	movq	%r14, -80(%rbp)
.L57:
	movq	-72(%rbp), %rax
	leaq	-208(%rbp), %r15
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L47
	.p2align 4,,10
	.p2align 3
.L59:
	movq	64(%r13), %rdi
	movslq	%ebx, %r12
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r14, %rsi
	cmpq	%r12, %rdx
	movq	%rdx, -200(%rbp)
	movq	%rax, %rdi
	cmovle	%rdx, %r12
	movq	%rax, -208(%rbp)
	movq	%r12, %rdx
	addq	%r12, %r14
	subl	%r12d, %ebx
	call	memcpy@PLT
	movq	64(%r13), %rdi
	testq	%r12, %r12
	jle	.L61
	addq	%r12, 72(%r13)
.L61:
	movq	(%rdi), %rax
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	*24(%rax)
	testl	%ebx, %ebx
	jne	.L59
.L47:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	-144(%rbp), %r14
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -80(%rbp)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L78:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7777:
	.size	_ZN4node8JSStream10ReadBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8JSStream10ReadBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9904:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9904:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9905:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE9905:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9906:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9906:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9907:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE9907:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.text
	.p2align 4
	.globl	_ZThn56_N4node8JSStream12GetAsyncWrapEv
	.type	_ZThn56_N4node8JSStream12GetAsyncWrapEv, @function
_ZThn56_N4node8JSStream12GetAsyncWrapEv:
.LFB9908:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE9908:
	.size	_ZThn56_N4node8JSStream12GetAsyncWrapEv, .-_ZThn56_N4node8JSStream12GetAsyncWrapEv
	.p2align 4
	.globl	_ZThn56_N4node8JSStream7IsAliveEv
	.type	_ZThn56_N4node8JSStream7IsAliveEv, @function
_ZThn56_N4node8JSStream7IsAliveEv:
.LFB9909:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE9909:
	.size	_ZThn56_N4node8JSStream7IsAliveEv, .-_ZThn56_N4node8JSStream7IsAliveEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev:
.LFB9910:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9910:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L89
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L104
.L91:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L105
	movq	8(%rbx), %rdi
.L93:
	movq	(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L102
	cmpw	$1040, %si
	jne	.L94
.L102:
	movq	31(%rcx), %r12
.L96:
	cmpl	$1, %edx
	jg	.L97
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L98:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L106
	cmpl	$1, 16(%rbx)
	jg	.L100
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L101:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	movq	(%r12), %rax
	call	*16(%rax)
	movq	(%r12), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L91
.L104:
	leaq	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L100:
	movq	8(%rbx), %rdi
	subq	$8, %rdi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movl	16(%rbx), %edx
	movq	%rax, %r12
	jmp	.L96
	.cfi_endproc
.LFE8534:
	.size	_ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9863:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB9915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9915:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node8JSStreamD2Ev,"axG",@progbits,_ZN4node8JSStreamD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node8JSStreamD1Ev
	.type	_ZThn56_N4node8JSStreamD1Ev, @function
_ZThn56_N4node8JSStreamD1Ev:
.LFB9913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8JSStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -56(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L114
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L115
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L117
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L133:
	cmpq	%rax, %rdx
	je	.L136
.L117:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L133
.L115:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L135:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L137
	movq	%rax, %rbx
.L121:
	testq	%rbx, %rbx
	jne	.L138
	popq	%rbx
	leaq	-56(%r12), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L136:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L114
	.cfi_endproc
.LFE9913:
	.size	_ZThn56_N4node8JSStreamD1Ev, .-_ZThn56_N4node8JSStreamD1Ev
	.section	.text._ZN4node8JSStreamD0Ev,"axG",@progbits,_ZN4node8JSStreamD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node8JSStreamD0Ev
	.type	_ZThn56_N4node8JSStreamD0Ev, @function
_ZThn56_N4node8JSStreamD0Ev:
.LFB9916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8JSStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -56(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L140
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L141
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L143
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L159:
	cmpq	%rax, %rdx
	je	.L162
.L143:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L159
.L141:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L161:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L163
	movq	%rax, %rbx
.L147:
	testq	%rbx, %rbx
	jne	.L164
	subq	$56, %r12
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L162:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L140
	.cfi_endproc
.LFE9916:
	.size	_ZThn56_N4node8JSStreamD0Ev, .-_ZThn56_N4node8JSStreamD0Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9912:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9911:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE9911:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9859:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE9859:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev:
.LFB6064:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$8, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$7881692365127373642, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE6064:
	.size	_ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev, .-_ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node8JSStreamD2Ev,"axG",@progbits,_ZN4node8JSStreamD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8JSStreamD2Ev
	.type	_ZN4node8JSStreamD2Ev, @function
_ZN4node8JSStreamD2Ev:
.LFB9808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8JSStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	104(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 56(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 96(%rdi)
	testq	%rdx, %rdx
	je	.L173
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L174
	leaq	96(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L176
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L192:
	cmpq	%rax, %rcx
	je	.L195
.L176:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L192
.L174:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	movq	112(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	64(%r12), %rbx
	movq	%rax, 56(%r12)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L197:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	64(%r12), %rax
	cmpq	%rbx, %rax
	je	.L196
	movq	%rax, %rbx
.L180:
	testq	%rbx, %rbx
	jne	.L197
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L195:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L173
	.cfi_endproc
.LFE9808:
	.size	_ZN4node8JSStreamD2Ev, .-_ZN4node8JSStreamD2Ev
	.weak	_ZN4node8JSStreamD1Ev
	.set	_ZN4node8JSStreamD1Ev,_ZN4node8JSStreamD2Ev
	.section	.text._ZN4node8JSStreamD0Ev,"axG",@progbits,_ZN4node8JSStreamD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8JSStreamD0Ev
	.type	_ZN4node8JSStreamD0Ev, @function
_ZN4node8JSStreamD0Ev:
.LFB9810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8JSStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	104(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 56(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 96(%rdi)
	testq	%rdx, %rdx
	je	.L199
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L200
	leaq	96(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L202
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L218:
	cmpq	%rax, %rcx
	je	.L221
.L202:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L218
.L200:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L220:
	movq	112(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L199:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	64(%r12), %rbx
	movq	%rax, 56(%r12)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L223:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	64(%r12), %rax
	cmpq	%rbx, %rax
	je	.L222
	movq	%rax, %rbx
.L206:
	testq	%rbx, %rbx
	jne	.L223
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L221:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L199
	.cfi_endproc
.LFE9810:
	.size	_ZN4node8JSStreamD0Ev, .-_ZN4node8JSStreamD0Ev
	.section	.text._ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L225
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L240
.L227:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L241
	movq	8(%rbx), %rdi
.L229:
	movq	(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L238
	cmpw	$1040, %si
	jne	.L230
.L238:
	movq	31(%rcx), %r12
.L232:
	cmpl	$1, %edx
	jg	.L233
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L234:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L242
	cmpl	$1, 16(%rbx)
	jg	.L236
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L237:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	movq	(%r12), %rax
	call	*16(%rax)
	movq	(%r12), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L227
.L240:
	leaq	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L233:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L236:
	movq	8(%rbx), %rdi
	subq	$8, %rdi
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movl	16(%rbx), %edx
	movq	%rax, %r12
	jmp	.L232
	.cfi_endproc
.LFE8535:
	.size	_ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev:
.LFB9914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r13
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L243
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L247
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9914:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB9917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-40(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r14
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L249
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L255
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L249:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$96, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9917:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev:
.LFB9812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rbx), %r12
	movq	32(%rbx), %r13
	movq	%rax, (%rbx)
	call	uv_buf_init@PLT
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%r12, %r12
	je	.L256
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L260
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9812:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$96, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L266
	cmpw	$1040, %cx
	jne	.L262
.L266:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L268
.L265:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	32(%rbx), %rsi
	leaq	40(%r12), %rdi
	movsd	.LC0(%rip), %xmm0
	movq	%rdx, 32(%r12)
	movl	$39, %ecx
	movq	%r13, %rdx
	movq	%rax, 24(%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L265
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7751:
	.size	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream7EmitEOFERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8JSStream7EmitEOFERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8JSStream7EmitEOFERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L281
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L276
	cmpw	$1040, %cx
	jne	.L271
.L276:
	movq	23(%rdx), %r12
.L273:
	testq	%r12, %r12
	je	.L269
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	64(%r12), %rdi
	movq	$-4095, %rsi
	movq	%rax, -48(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	call	*24(%rax)
.L269:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7778:
	.size	_ZN4node8JSStream7EmitEOFERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8JSStream7EmitEOFERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB7773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	jne	.L344
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rdx, %r15
	movq	%rcx, %r12
	leaq	-320(%rbp), %r14
	movq	352(%rax), %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movdqa	.LC2(%rip), %xmm0
	leaq	-184(%rbp), %rax
	movq	%rax, -344(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	cmpq	$16, %r12
	jbe	.L285
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r12,8), %r13
	andq	%r12, %rax
	cmpq	%rax, %r12
	jne	.L345
	testq	%r13, %r13
	je	.L287
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L346
	movq	%rax, -192(%rbp)
	movq	%r12, -200(%rbp)
.L290:
	movq	%r12, -208(%rbp)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r12, -208(%rbp)
	testq	%r12, %r12
	je	.L296
.L291:
	movq	%r15, %r13
	xorl	%r15d, %r15d
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L294:
	cmpq	-208(%rbp), %r15
	jnb	.L347
.L295:
	movq	-192(%rbp), %rdx
	addq	$16, %r13
	movq	%rax, (%rdx,%r15,8)
	addq	$1, %r15
	cmpq	%r15, %r12
	jbe	.L296
.L297:
	movq	8(%r13), %rdx
	movq	16(%rbx), %rdi
	movq	0(%r13), %rsi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	testq	%rax, %rax
	jne	.L294
	movq	%rax, -352(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-352(%rbp), %rax
	cmpq	-208(%rbp), %r15
	jb	.L295
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	movq	-328(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L292
	movzbl	11(%rax), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L348
.L292:
	movq	%rax, -224(%rbp)
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	leaq	-288(%rbp), %r12
	movq	-192(%rbp), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	movq	16(%rbx), %rax
	movq	352(%rax), %rsi
	movq	%rax, -328(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-328(%rbp), %rax
	movq	8(%rbx), %rdi
	movl	$0, -232(%rbp)
	movq	%rax, -240(%rbp)
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	movq	1264(%rdx), %r8
	testq	%rdi, %rdi
	je	.L298
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L349
.L298:
	movq	3280(%rax), %rsi
	movq	%r8, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L309
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-328(%rbp), %rsi
	testb	%al, %al
	jne	.L350
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	addq	$88, %rdi
.L301:
	testq	%rdi, %rdi
	je	.L309
	movq	16(%rbx), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, %r15
	testb	%al, %al
	je	.L309
.L304:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L307
	cmpq	-344(%rbp), %rdi
	je	.L307
	call	free@PLT
.L307:
	movq	-336(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$312, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L309:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L305
.L343:
	movl	$-71, %r15d
	jmp	.L304
.L349:
	movq	352(%rax), %r9
	movq	(%rdi), %rsi
	movq	%r8, -328(%rbp)
	movq	%r9, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-328(%rbp), %r8
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L298
.L348:
	movq	16(%rdx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L292
.L344:
	leaq	_ZZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L287:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L305:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L343
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	352(%rax), %rdi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L343
.L346:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L287
	movq	-208(%rbp), %rax
	movq	%rdi, -192(%rbp)
	movq	%r12, -200(%rbp)
	testq	%rax, %rax
	je	.L290
	movq	-344(%rbp), %rsi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	jmp	.L290
.L350:
	movq	%rbx, %rdi
	leaq	-224(%rbp), %rcx
	movl	$2, %edx
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%rax, %rdi
	jmp	.L301
.L345:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7773:
	.size	_ZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.set	.LTHUNK6,_ZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.p2align 4
	.globl	_ZThn56_N4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZThn56_N4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZThn56_N4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB9936:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK6
	.cfi_endproc
.LFE9936:
	.size	_ZThn56_N4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZThn56_N4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream9IsClosingEv
	.type	_ZN4node8JSStream9IsClosingEv, @function
_ZN4node8JSStream9IsClosingEv:
.LFB7768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%rbx), %r15
	movq	%r12, %rdi
	movq	352(%r15), %rsi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	%r15, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	360(%rax), %rdx
	movq	944(%rdx), %r15
	testq	%rdi, %rdi
	je	.L353
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L380
.L353:
	movq	3280(%rax), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L357
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L381
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	addq	$88, %rdi
.L356:
	testq	%rdi, %rdi
	je	.L357
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	%eax, %r15d
.L360:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%rax, %rdi
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movl	%eax, %r15d
	testb	%al, %al
	jne	.L383
.L362:
	movl	$1, %r15d
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L362
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	352(%rax), %rdi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L360
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7768:
	.size	_ZN4node8JSStream9IsClosingEv, .-_ZN4node8JSStream9IsClosingEv
	.set	.LTHUNK2,_ZN4node8JSStream9IsClosingEv
	.p2align 4
	.globl	_ZThn56_N4node8JSStream9IsClosingEv
	.type	_ZThn56_N4node8JSStream9IsClosingEv, @function
_ZThn56_N4node8JSStream9IsClosingEv:
.LFB9937:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE9937:
	.size	_ZThn56_N4node8JSStream9IsClosingEv, .-_ZThn56_N4node8JSStream9IsClosingEv
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream9ReadStartEv
	.type	_ZN4node8JSStream9ReadStartEv, @function
_ZN4node8JSStream9ReadStartEv:
.LFB7769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%rbx), %r15
	movq	%r12, %rdi
	movq	352(%r15), %rsi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	%r15, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	360(%rax), %rdx
	movq	1224(%rdx), %r15
	testq	%rdi, %rdi
	je	.L385
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L414
.L385:
	movq	3280(%rax), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L395
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L415
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	addq	$88, %rdi
.L388:
	testq	%rdi, %rdi
	je	.L395
	movq	16(%rbx), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, %r15
	testb	%al, %al
	jne	.L391
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L392
.L413:
	movl	$-71, %r15d
.L391:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L416
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L413
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	352(%rax), %rdi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L414:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%rax, %rdi
	jmp	.L388
.L416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7769:
	.size	_ZN4node8JSStream9ReadStartEv, .-_ZN4node8JSStream9ReadStartEv
	.set	.LTHUNK3,_ZN4node8JSStream9ReadStartEv
	.p2align 4
	.globl	_ZThn56_N4node8JSStream9ReadStartEv
	.type	_ZThn56_N4node8JSStream9ReadStartEv, @function
_ZThn56_N4node8JSStream9ReadStartEv:
.LFB9938:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK3
	.cfi_endproc
.LFE9938:
	.size	_ZThn56_N4node8JSStream9ReadStartEv, .-_ZThn56_N4node8JSStream9ReadStartEv
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream8ReadStopEv
	.type	_ZN4node8JSStream8ReadStopEv, @function
_ZN4node8JSStream8ReadStopEv:
.LFB7770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%rbx), %r15
	movq	%r12, %rdi
	movq	352(%r15), %rsi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	%r15, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	360(%rax), %rdx
	movq	1232(%rdx), %r15
	testq	%rdi, %rdi
	je	.L418
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L447
.L418:
	movq	3280(%rax), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L428
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L448
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	addq	$88, %rdi
.L421:
	testq	%rdi, %rdi
	je	.L428
	movq	16(%rbx), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, %r15
	testb	%al, %al
	jne	.L424
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L425
.L446:
	movl	$-71, %r15d
.L424:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L449
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L446
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	352(%rax), %rdi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L447:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%rax, %rdi
	jmp	.L421
.L449:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7770:
	.size	_ZN4node8JSStream8ReadStopEv, .-_ZN4node8JSStream8ReadStopEv
	.set	.LTHUNK4,_ZN4node8JSStream8ReadStopEv
	.p2align 4
	.globl	_ZThn56_N4node8JSStream8ReadStopEv
	.type	_ZThn56_N4node8JSStream8ReadStopEv, @function
_ZThn56_N4node8JSStream8ReadStopEv:
.LFB9939:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK4
	.cfi_endproc
.LFE9939:
	.size	_ZThn56_N4node8JSStream8ReadStopEv, .-_ZThn56_N4node8JSStream8ReadStopEv
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZN4node8JSStream10DoShutdownEPNS_12ShutdownWrapE, @function
_ZN4node8JSStream10DoShutdownEPNS_12ShutdownWrapE:
.LFB7771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	(%r12), %rax
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L451
	addq	$16, %r12
.L452:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L453
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L486
.L453:
	movq	16(%rbx), %r15
	leaq	-128(%rbp), %r12
	movq	%rax, -64(%rbp)
	movq	%r12, %rdi
	movq	352(%r15), %rsi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	%r15, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	360(%rax), %rdx
	movq	1240(%rdx), %r15
	testq	%rdi, %rdi
	je	.L454
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L487
.L454:
	movq	3280(%rax), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L464
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L488
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	addq	$88, %rdi
.L457:
	testq	%rdi, %rdi
	je	.L464
	movq	16(%rbx), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	shrq	$32, %rcx
	movq	%rcx, %r15
	testb	%al, %al
	jne	.L460
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L461
.L485:
	movl	$-71, %r15d
.L460:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L489
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L485
	movq	16(%rbx), %rax
	movq	%r12, %rsi
	movq	352(%rax), %rdi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L487:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L486:
	movq	16(%r12), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rbx, %rdi
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%rax, %rdi
	jmp	.L457
.L489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7771:
	.size	_ZN4node8JSStream10DoShutdownEPNS_12ShutdownWrapE, .-_ZN4node8JSStream10DoShutdownEPNS_12ShutdownWrapE
	.set	.LTHUNK5,_ZN4node8JSStream10DoShutdownEPNS_12ShutdownWrapE
	.p2align 4
	.globl	_ZThn56_N4node8JSStream10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZThn56_N4node8JSStream10DoShutdownEPNS_12ShutdownWrapE, @function
_ZThn56_N4node8JSStream10DoShutdownEPNS_12ShutdownWrapE:
.LFB9940:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK5
	.cfi_endproc
.LFE9940:
	.size	_ZThn56_N4node8JSStream10DoShutdownEPNS_12ShutdownWrapE, .-_ZThn56_N4node8JSStream10DoShutdownEPNS_12ShutdownWrapE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"JSStream"
.LC4:
	.string	"finishWrite"
.LC5:
	.string	"finishShutdown"
.LC6:
	.string	"readBuffer"
.LC7:
	.string	"emitEOF"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB8:
	.text
.LHOTB8:
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L491
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L491
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L491
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r14
	movq	%rax, %r15
	popq	%rax
	testq	%r15, %r15
	je	.L503
.L492:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L504
.L493:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L505
.L494:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8JSStream10ReadBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L506
.L495:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8JSStream7EmitEOFERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L507
.L496:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L508
.L497:
	movq	3280(%rbx), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L509
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L505:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L506:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L507:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L508:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7779:
.L491:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7779:
	.text
	.size	_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE8:
	.text
.LHOTE8:
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L511
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L511:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L521
.L512:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L513
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L512
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7286:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStreamC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node8JSStreamC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node8JSStreamC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	.LC0(%rip), %xmm0
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	56(%r12), %r14
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node22EmitToJSStreamListenerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, 88(%r12)
	movq	%rax, 96(%r12)
	leaq	96(%r12), %rax
	movq	%rax, 64(%r12)
	leaq	16+_ZTVN4node8JSStreamE(%rip), %rax
	movq	%rax, (%r12)
	addq	$152, %rax
	movq	%rax, 56(%r12)
	movq	24(%r12), %rax
	movq	$0, 112(%r12)
	movq	%r14, 104(%r12)
	movups	%xmm0, 72(%r12)
	testq	%rax, %rax
	je	.L525
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L524
.L525:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L524:
	popq	%rbx
	movq	%r14, %rdx
	popq	%r12
	movq	%r13, %rdi
	movl	$1, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7764:
	.size	_ZN4node8JSStreamC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node8JSStreamC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.globl	_ZN4node8JSStreamC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.set	_ZN4node8JSStreamC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE,_ZN4node8JSStreamC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L528
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L534
.L528:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L532
	cmpw	$1040, %cx
	jne	.L529
.L532:
	movq	23(%rdx), %r13
.L531:
	movq	8(%rbx), %r12
	movl	$120, %edi
	call	_Znwm@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	addq	$8, %r12
	popq	%rbx
	movq	%rax, %rdi
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node8JSStreamC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L528
	leaq	_ZZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L529:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L531
	.cfi_endproc
.LFE7775:
	.size	_ZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_Z19_register_js_streamv
	.type	_Z19_register_js_streamv, @function
_Z19_register_js_streamv:
.LFB7780:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7780:
	.size	_Z19_register_js_streamv, .-_Z19_register_js_streamv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L553
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L554
.L539:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L543
.L536:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L544
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L540:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L539
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L545
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L541:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L543:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L536
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L545:
	xorl	%edx, %edx
	jmp	.L541
	.cfi_endproc
.LFE8493:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L556
	leaq	16(%r12), %rsi
.L557:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L558
	addq	$16, %r12
.L559:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L560
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L576
.L560:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L577
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L564
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L578
.L555:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L579
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L558:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L556:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L577:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L567
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L562:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L564:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L567:
	xorl	%edx, %edx
	jmp	.L562
.L579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7754:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L581
	leaq	40(%r12), %rsi
.L582:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L583
	addq	$40, %r12
.L584:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L585
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L601
.L585:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L602
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L589
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L603
.L580:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L604
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L601:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L583:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L603:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L592
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L587:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L589:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L592:
	xorl	%edx, %edx
	jmp	.L587
.L604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7756:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node9StreamReqE
	.section	.data.rel.ro._ZTVN4node9StreamReqE,"awG",@progbits,_ZTVN4node9StreamReqE,comdat
	.align 8
	.type	_ZTVN4node9StreamReqE, @object
	.size	_ZTVN4node9StreamReqE, 48
_ZTVN4node9StreamReqE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, 168
_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-16
	.quad	0
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, 168
_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-40
	.quad	0
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node8JSStreamE
	.section	.data.rel.ro._ZTVN4node8JSStreamE,"awG",@progbits,_ZTVN4node8JSStreamE,comdat
	.align 8
	.type	_ZTVN4node8JSStreamE, @object
	.size	_ZTVN4node8JSStreamE, 312
_ZTVN4node8JSStreamE:
	.quad	0
	.quad	0
	.quad	_ZN4node8JSStreamD1Ev
	.quad	_ZN4node8JSStreamD0Ev
	.quad	_ZNK4node8JSStream10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node8JSStream14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node8JSStream8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node8JSStream7IsAliveEv
	.quad	_ZN4node8JSStream9IsClosingEv
	.quad	_ZN4node8JSStream9ReadStartEv
	.quad	_ZN4node8JSStream8ReadStopEv
	.quad	_ZN4node8JSStream10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node8JSStream12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node8JSStreamD1Ev
	.quad	_ZThn56_N4node8JSStreamD0Ev
	.quad	_ZThn56_N4node8JSStream9ReadStartEv
	.quad	_ZThn56_N4node8JSStream8ReadStopEv
	.quad	_ZThn56_N4node8JSStream10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn56_N4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn56_N4node8JSStream7IsAliveEv
	.quad	_ZThn56_N4node8JSStream9IsClosingEv
	.quad	_ZN4node10StreamBase9IsIPCPipeEv
	.quad	_ZN4node10StreamBase5GetFDEv
	.quad	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn56_N4node8JSStream12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC9:
	.string	"../src/util-inl.h:374"
.LC10:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC12:
	.string	"../src/util-inl.h:325"
.LC13:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.weak	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0
	.section	.rodata.str1.1
.LC15:
	.string	"../src/js_stream.cc:159"
.LC16:
	.string	"args[1]->IsInt32()"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"static void node::JSStream::Finish(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::ShutdownWrap]"
	.section	.data.rel.ro.local._ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, 24
_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.weak	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args
	.section	.rodata.str1.1
.LC18:
	.string	"../src/js_stream.cc:156"
.LC19:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local._ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,"awG",@progbits,_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node8JSStream6FinishINS_12ShutdownWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC17
	.weak	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"static void node::JSStream::Finish(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::WriteWrap]"
	.section	.data.rel.ro.local._ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, 24
_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC20
	.weak	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args
	.section	.data.rel.ro.local._ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,"awG",@progbits,_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node8JSStream6FinishINS_9WriteWrapEEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC21:
	.string	"../src/util-inl.h:495"
.LC22:
	.string	"value->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Value>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args
	.section	.rodata.str1.1
.LC24:
	.string	"../src/util.h:352"
.LC25:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 16; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"../src/js_stream.cc"
.LC28:
	.string	"js_stream"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC27
	.quad	0
	.quad	_ZN4node8JSStream10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC28
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC29:
	.string	"../src/js_stream.cc:148"
.LC30:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"static void node::JSStream::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node8JSStream3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"../src/js_stream.cc:114"
.LC33:
	.string	"(send_handle) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"virtual int node::JSStream::DoWrite(node::WriteWrap*, uv_buf_t*, size_t, uv_stream_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args, @object
	.size	_ZZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args, 24
_ZZN4node8JSStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC35:
	.string	"../src/stream_base-inl.h:103"
.LC36:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC38:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC40:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC41:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC41
	.quad	.LC39
	.quad	.LC42
	.weak	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC43:
	.string	"../src/stream_base-inl.h:26"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"(req_wrap_obj->GetAlignedPointerFromInternalField( StreamReq::kStreamReqField)) == (nullptr)"
	.align 8
.LC45:
	.string	"void node::StreamReq::AttachToObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC46:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC48:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC49:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC51:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC52:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC54:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC55:
	.string	"../src/env-inl.h:955"
.LC56:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	7517463719428581715
	.quad	8239175502546629749
	.align 16
.LC2:
	.quad	0
	.quad	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
