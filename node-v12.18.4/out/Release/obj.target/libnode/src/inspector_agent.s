	.file	"inspector_agent.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient11muteMetricsEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient11muteMetricsEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient11muteMetricsEi
	.type	_ZN12v8_inspector17V8InspectorClient11muteMetricsEi, @function
_ZN12v8_inspector17V8InspectorClient11muteMetricsEi:
.LFB7124:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7124:
	.size	_ZN12v8_inspector17V8InspectorClient11muteMetricsEi, .-_ZN12v8_inspector17V8InspectorClient11muteMetricsEi
	.section	.text._ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi
	.type	_ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi, @function
_ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi:
.LFB7125:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7125:
	.size	_ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi, .-_ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi
	.section	.text._ZN12v8_inspector17V8InspectorClient16beginUserGestureEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient16beginUserGestureEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient16beginUserGestureEv
	.type	_ZN12v8_inspector17V8InspectorClient16beginUserGestureEv, @function
_ZN12v8_inspector17V8InspectorClient16beginUserGestureEv:
.LFB7126:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7126:
	.size	_ZN12v8_inspector17V8InspectorClient16beginUserGestureEv, .-_ZN12v8_inspector17V8InspectorClient16beginUserGestureEv
	.section	.text._ZN12v8_inspector17V8InspectorClient14endUserGestureEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient14endUserGestureEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient14endUserGestureEv
	.type	_ZN12v8_inspector17V8InspectorClient14endUserGestureEv, @function
_ZN12v8_inspector17V8InspectorClient14endUserGestureEv:
.LFB7127:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7127:
	.size	_ZN12v8_inspector17V8InspectorClient14endUserGestureEv, .-_ZN12v8_inspector17V8InspectorClient14endUserGestureEv
	.section	.text._ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE
	.type	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE, @function
_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE:
.LFB7130:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE7130:
	.size	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE, .-_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE
	.type	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE, @function
_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE:
.LFB7131:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7131:
	.size	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE, .-_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE
	.type	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE, @function
_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE:
.LFB7132:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7132:
	.size	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE, .-_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi
	.type	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi, @function
_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi:
.LFB7134:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7134:
	.size	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi, .-_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi
	.section	.text._ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi
	.type	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi, @function
_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi:
.LFB7135:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7135:
	.size	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi, .-_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi
	.section	.text._ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE
	.type	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE, @function
_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE:
.LFB7137:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7137:
	.size	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE, .-_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE
	.section	.text._ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE
	.type	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE, @function
_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE:
.LFB7138:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7138:
	.size	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE, .-_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE:
.LFB7139:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7139:
	.size	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE
	.section	.text._ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE:
.LFB7140:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7140:
	.size	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE
	.section	.text._ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE:
.LFB7141:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7141:
	.size	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE
	.section	.text._ZN12v8_inspector17V8InspectorClient12consoleClearEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient12consoleClearEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient12consoleClearEi
	.type	_ZN12v8_inspector17V8InspectorClient12consoleClearEi, @function
_ZN12v8_inspector17V8InspectorClient12consoleClearEi:
.LFB7142:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7142:
	.size	_ZN12v8_inspector17V8InspectorClient12consoleClearEi, .-_ZN12v8_inspector17V8InspectorClient12consoleClearEi
	.section	.text._ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi
	.type	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi, @function
_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi:
.LFB7146:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7146:
	.size	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi, .-_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl12sendResponseEiSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImpl12sendResponseEiSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE:
.LFB8433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	40(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8433:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl12sendResponseEiSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImpl12sendResponseEiSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl16sendNotificationESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImpl16sendNotificationESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE:
.LFB8434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	40(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8434:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl16sendNotificationESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImpl16sendNotificationESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv:
.LFB8435:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8435:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_:
.LFB8440:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8440:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.section	.text._ZN4node9inspector12_GLOBAL__N_114InspectorTimer7OnTimerEP10uv_timer_s,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_,comdat
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer7OnTimerEP10uv_timer_s, @function
_ZN4node9inspector12_GLOBAL__N_114InspectorTimer7OnTimerEP10uv_timer_s:
.LFB8447:
	.cfi_startproc
	endbr64
	movq	160(%rdi), %r8
	movq	152(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE8447:
	.size	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer7OnTimerEP10uv_timer_s, .-_ZN4node9inspector12_GLOBAL__N_114InspectorTimer7OnTimerEP10uv_timer_s
	.section	.text._ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv
	.type	_ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv, @function
_ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv:
.LFB8569:
	.cfi_startproc
	endbr64
	movb	$0, 148(%rdi)
	ret
	.cfi_endproc
.LFE8569:
	.size	_ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv, .-_ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv
	.section	.text._ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi
	.type	_ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi, @function
_ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi:
.LFB8570:
	.cfi_startproc
	endbr64
	movb	$0, 149(%rdi)
	ret
	.cfi_endproc
.LFE8570:
	.size	_ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi, .-_ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi
	.section	.text._ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi
	.type	_ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi, @function
_ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi:
.LFB8574:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	3280(%rax), %rax
	ret
	.cfi_endproc
.LFE8574:
	.size	_ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi, .-_ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi
	.section	.text._ZN4node9inspector19NodeInspectorClient13currentTimeMSEv,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient13currentTimeMSEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient13currentTimeMSEv
	.type	_ZN4node9inspector19NodeInspectorClient13currentTimeMSEv, @function
_ZN4node9inspector19NodeInspectorClient13currentTimeMSEv:
.LFB8599:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	360(%rax), %rax
	movq	2392(%rax), %rdi
	movq	(%rdi), %rax
	jmp	*128(%rax)
	.cfi_endproc
.LFE8599:
	.size	_ZN4node9inspector19NodeInspectorClient13currentTimeMSEv, .-_ZN4node9inspector19NodeInspectorClient13currentTimeMSEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD2Ev:
.LFB11528:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11528:
	.size	_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD2Ev, .-_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD1Ev,_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB14446:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE14446:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB14449:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r8
	movq	%r8, %rdi
	movq	(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE14449:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB14453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE14453:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB14460:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE14460:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s, @function
_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s:
.LFB8450:
	.cfi_startproc
	endbr64
	cmpq	$8, %rdi
	je	.L34
	subq	$8, %rdi
	movl	$176, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE8450:
	.size	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s, .-_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD0Ev:
.LFB11530:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11530:
	.size	_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD0Ev, .-_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB14462:
	.cfi_startproc
	endbr64
	movl	$384, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14462:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB14455:
	.cfi_startproc
	endbr64
	movl	$224, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14455:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB14448:
	.cfi_startproc
	endbr64
	movl	$200, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14448:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE:
.LFB8438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*(%rax)
	movq	%r12, %rsi
	leaq	-72(%rbp), %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	40(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L41
	movq	(%rdi), %rax
	call	*8(%rax)
.L41:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8438:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB14450:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE14450:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB14457:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE14457:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB14464:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE14464:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE
	.type	_ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE, @function
_ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE:
.LFB8575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	2920(%rax), %rdi
	testq	%rdi, %rdi
	je	.L52
	leaq	-16(%rbp), %r8
	movl	$1, %ecx
	movq	%rdx, -16(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
.L52:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L59:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8575:
	.size	_ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE, .-_ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE
	.section	.text._ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv,"axG",@progbits,_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv
	.type	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv, @function
_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv:
.LFB7427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7427:
	.size	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv, .-_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv
	.section	.text._ZN4node9inspector8protocol11NodeTracing7Backend7disableEv,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv
	.type	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv, @function
_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv:
.LFB7507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L67:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7507:
	.size	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv, .-_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB14463:
	.cfi_startproc
	endbr64
	addq	$16, %rdi
	jmp	_ZN4node9inspector19MainThreadInterfaceD1Ev@PLT
	.cfi_endproc
.LFE14463:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB14465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L69
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L69:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14465:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB14458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L73
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L73:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14458:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB14451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L77
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L77:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14451:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_1L19StartIoThreadWakeupEi, @function
_ZN4node9inspector12_GLOBAL__N_1L19StartIoThreadWakeupEi:
.LFB8390:
	.cfi_startproc
	endbr64
	leaq	_ZN4node9inspector12_GLOBAL__N_1L25start_io_thread_semaphoreE(%rip), %rdi
	jmp	uv_sem_post@PLT
	.cfi_endproc
.LFE8390:
	.size	_ZN4node9inspector12_GLOBAL__N_1L19StartIoThreadWakeupEi, .-_ZN4node9inspector12_GLOBAL__N_1L19StartIoThreadWakeupEi
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE:
.LFB8384:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-2096(%rbp), %rdi
	subq	$2120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node12TwoByteValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2096(%rbp), %rax
	movq	%r12, %rdi
	leaq	-2128(%rbp), %rsi
	movb	$0, -2128(%rbp)
	movq	%rax, -2120(%rbp)
	movq	-2080(%rbp), %rax
	movq	%rax, -2112(%rbp)
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	leaq	-2072(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L82
	call	free@PLT
.L82:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	addq	$2120, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L92:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8384:
	.size	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE
	.section	.text._ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev,"axG",@progbits,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.type	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, @function
_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev:
.LFB9341:
	.cfi_startproc
	endbr64
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE9341:
	.size	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, .-_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev
	.set	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.section	.rodata._ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"basic_string::append"
.LC2:
	.string	"//"
	.section	.text._ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE
	.type	_ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE:
.LFB8600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-416(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$416, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	pushq	16(%rdx)
	pushq	8(%rdx)
	pushq	(%rdx)
	call	_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE@PLT
	addq	$32, %rsp
	movq	-416(%rbp), %rdi
	cmpq	$0, -408(%rbp)
	je	.L96
	cmpb	$47, (%rdi)
	je	.L145
.L96:
	movq	$0, (%r12)
.L118:
	leaq	-400(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L94
	call	_ZdlPv@PLT
.L94:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	leaq	-288(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node3url3URL12FromFilePathERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	-336(%rbp), %rax
	movq	-88(%rbp), %r9
	movq	$0, -344(%rbp)
	movq	%rax, -448(%rbp)
	movq	%rax, -352(%rbp)
	movq	-80(%rbp), %rax
	movb	$0, -336(%rbp)
	movq	%rax, -440(%rbp)
	cmpq	%rax, %r9
	je	.L103
	movq	%r9, %r14
	leaq	-320(%rbp), %rbx
	leaq	-304(%rbp), %r13
	leaq	-352(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L104:
	movq	$0, -312(%rbp)
	movq	%rbx, %rdi
	movb	$0, -304(%rbp)
	movq	8(%r14), %rax
	movq	%r13, -320(%rbp)
	leaq	1(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movl	$47, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	-312(%rbp), %rsi
	movl	$1, %ecx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	8(%r14), %rdx
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-320(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L101
	call	_ZdlPv@PLT
	addq	$32, %r14
	cmpq	%r14, -440(%rbp)
	jne	.L104
.L103:
	movq	-280(%rbp), %r14
	movq	-272(%rbp), %r13
	leaq	-368(%rbp), %rbx
	movq	%rbx, -384(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L100
	testq	%r14, %r14
	je	.L147
.L100:
	movq	%r13, -424(%rbp)
	cmpq	$15, %r13
	ja	.L148
	cmpq	$1, %r13
	jne	.L107
	movzbl	(%r14), %eax
	movb	%al, -368(%rbp)
	movq	%rbx, %rax
.L108:
	movq	%r13, -376(%rbp)
	movb	$0, (%rax,%r13)
	movabsq	$4611686018427387903, %rax
	subq	-376(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L149
	leaq	-384(%rbp), %r13
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-384(%rbp), %r9
	movl	$15, %eax
	movq	-376(%rbp), %r8
	movq	-344(%rbp), %rdx
	movq	%rax, %rdi
	movq	-352(%rbp), %rsi
	cmpq	%rbx, %r9
	cmovne	-368(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L111
	cmpq	-448(%rbp), %rsi
	cmovne	-336(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L150
.L111:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L113:
	leaq	-304(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -320(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L151
	movq	%rcx, -320(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -304(%rbp)
.L115:
	movq	8(%rax), %rcx
	movq	%r12, %rdi
	leaq	-320(%rbp), %rsi
	movq	%rcx, -312(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-320(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	-384(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movq	-352(%rbp), %rdi
	cmpq	-448(%rbp), %rdi
	je	.L120
	call	_ZdlPv@PLT
.L120:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L122
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L125
.L123:
	movq	-88(%rbp), %r13
.L121:
	testq	%r13, %r13
	je	.L126
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L126:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	movq	-152(%rbp), %rdi
	leaq	-136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L128
	call	_ZdlPv@PLT
.L128:
	movq	-184(%rbp), %rdi
	leaq	-168(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	-216(%rbp), %rdi
	leaq	-200(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	-248(%rbp), %rdi
	leaq	-232(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	-280(%rbp), %rdi
	leaq	-264(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	-416(%rbp), %rdi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L101:
	addq	$32, %r14
	cmpq	%r14, -440(%rbp)
	jne	.L104
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L125
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L107:
	testq	%r13, %r13
	jne	.L152
	movq	%rbx, %rax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	-384(%rbp), %rdi
	leaq	-424(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -384(%rbp)
	movq	%rax, %rdi
	movq	-424(%rbp), %rax
	movq	%rax, -368(%rbp)
.L106:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-424(%rbp), %r13
	movq	-384(%rbp), %rax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L151:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -304(%rbp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	-352(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L113
.L146:
	call	__stack_chk_fail@PLT
.L149:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L152:
	movq	%rbx, %rdi
	jmp	.L106
.L147:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE8600:
	.size	_ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE, .-_ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE
	.text
	.p2align 4
	.type	_ZZN4node11Environment11CloseHandleI10uv_async_sZZNS_9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESO_EUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESW_, @function
_ZZN4node11Environment11CloseHandleI10uv_async_sZZNS_9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESO_EUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESW_:
.LFB10411:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	movq	(%r8), %rax
	subl	$1, 2152(%rax)
	movq	16(%r8), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	xchgb	_ZN4node9inspector12_GLOBAL__N_1L33start_io_thread_async_initializedE(%rip), %al
	testb	%al, %al
	je	.L158
	movl	$24, %esi
	movq	%r8, %rdi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESL_ENKUlP10uv_async_sE_clESO_E4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10411:
	.size	_ZZN4node11Environment11CloseHandleI10uv_async_sZZNS_9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESO_EUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESW_, .-_ZZN4node11Environment11CloseHandleI10uv_async_sZZNS_9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESO_EUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESW_
	.section	.text._ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_,"axG",@progbits,_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_,comdat
	.p2align 4
	.weak	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_
	.type	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_, @function
_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_:
.LFB10085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	(%r12), %rax
	subl	$1, 2152(%rax)
	movq	16(%r12), %rax
	movq	%rax, (%rdi)
	call	*8(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10085:
	.size	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_, .-_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_
	.text
	.p2align 4
	.type	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE_4_FUNESL_, @function
_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE_4_FUNESL_:
.LFB8644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rdi
	movq	$0, _ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip)
	call	uv_mutex_unlock@PLT
	addl	$1, 2152(%rbx)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip), %rdx
	leaq	_ZZN4node11Environment11CloseHandleI10uv_async_sZZNS_9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESO_EUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESW_(%rip), %rsi
	leaq	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip), %rdi
	movq	%rbx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, _ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_close@PLT
	.cfi_endproc
.LFE8644:
	.size	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE_4_FUNESL_, .-_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE_4_FUNESL_
	.p2align 4
	.type	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, @function
_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_:
.LFB14860:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE14860:
	.size	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, .-_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.p2align 4
	.type	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv, @function
_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv:
.LFB14861:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE14861:
	.size	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv, .-_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv
	.align 2
	.p2align 4
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector12_GLOBAL__N_111ChannelImplESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector12_GLOBAL__N_111ChannelImplESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB11342:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	(%rsi), %r14
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L166
	movq	(%rax), %rcx
	movq	%r14, %r9
	movq	%rdx, %rsi
	movl	8(%rcx), %r8d
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L201:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L166
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	cmpq	%rdx, %rsi
	jne	.L166
.L168:
	cmpl	%r8d, %r9d
	jne	.L201
	addq	$24, %rsp
	leaq	16(%rcx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	32(%r12), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movl	0(%r13), %eax
	movl	$1, %ecx
	movq	$0, 16(%rbx)
	movl	%eax, 8(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L169
	movq	(%r12), %r13
.L170:
	addq	%r13, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L179
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r15), %rax
	movq	%rbx, (%rax)
.L180:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	16(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L202
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L203
	leaq	0(,%rdx,8), %r15
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	movq	-56(%rbp), %r8
	leaq	48(%r12), %r10
.L172:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L174
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L176:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L177:
	testq	%rsi, %rsi
	je	.L174
.L175:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r8
	leaq	0(%r13,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L176
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L183
	movq	%rcx, 0(%r13,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L174:
	movq	(%r12), %rdi
	cmpq	%rdi, %r10
	je	.L178
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L178:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r8, 8(%r12)
	divq	%r8
	movq	%r13, (%r12)
	leaq	0(,%rdx,8), %r15
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L179:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L181
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, 0(%r13,%rdx,8)
.L181:
	leaq	16(%r12), %rax
	movq	%rax, (%r15)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rdx, %rdi
	jmp	.L177
.L202:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r13
	movq	%r13, %r10
	jmp	.L172
.L203:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11342:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector12_GLOBAL__N_111ChannelImplESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector12_GLOBAL__N_111ChannelImplESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.p2align 4
	.type	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, @function
_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE:
.LFB14864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*(%rax)
	movq	%r12, %rsi
	leaq	-72(%rbp), %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	(%rdi), %rax
	call	*8(%rax)
.L205:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L212:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14864:
	.size	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, .-_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.p2align 4
	.type	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, @function
_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE:
.LFB14863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	(%rdx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*(%rax)
	movq	%r12, %rsi
	leaq	-72(%rbp), %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	32(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	movq	(%rdi), %rax
	call	*8(%rax)
.L214:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L221:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14863:
	.size	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, .-_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv, @function
_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv:
.LFB8449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	2592(%r12), %rcx
	movq	2584(%r12), %r13
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L223
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv(%rip), %r15
	movq	32(%rdi), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L224:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L223
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L223
	movq	%r9, %rdi
.L226:
	cmpq	%rsi, %rbx
	jne	.L224
	cmpq	%r15, 8(%rdi)
	jne	.L224
	cmpq	16(%rdi), %rbx
	jne	.L224
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L245
	testq	%rsi, %rsi
	je	.L228
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L228
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L228:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L223:
	cmpq	8(%rbx), %rbx
	je	.L246
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L233
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L228
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L227:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L247
.L229:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r10, %rax
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L246:
	movq	$0, 8(%rbx)
	leaq	8(%rbx), %r13
	movq	%r13, %rdi
	call	uv_timer_stop@PLT
	movq	(%rbx), %r12
	movl	$24, %edi
	addl	$1, 2152(%r12)
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movq	%r13, %rdi
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s(%rip), %rcx
	movq	%r12, (%rax)
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_(%rip), %rsi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_close@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	%rsi, 2600(%r12)
	jmp	.L229
	.cfi_endproc
.LFE8449:
	.size	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv, .-_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE:
.LFB8439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-64(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$64, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*(%rax)
	movq	%r12, %rsi
	leaq	-72(%rbp), %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	40(%rbx), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L249
	movq	(%rdi), %rax
	call	*8(%rax)
.L249:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L256:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8439:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.section	.text._ZN4node9inspector19NodeInspectorClient11cancelTimerEPv,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient11cancelTimerEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient11cancelTimerEPv
	.type	_ZN4node9inspector19NodeInspectorClient11cancelTimerEPv, @function
_ZN4node9inspector19NodeInspectorClient11cancelTimerEPv:
.LFB8585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	96(%rdi), %r8
	movq	88(%rdi), %r13
	divq	%r8
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r9
	testq	%r9, %r9
	je	.L257
	movq	(%r9), %r12
	movq	%rdi, %rbx
	movq	%rdx, %r11
	movq	%r9, %r10
	movq	8(%r12), %rcx
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L257
	movq	8(%rdi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r11
	jne	.L257
	movq	%rdi, %r12
.L260:
	cmpq	%rcx, %rsi
	jne	.L302
	movq	(%r12), %rcx
	cmpq	%r10, %r9
	je	.L303
	testq	%rcx, %rcx
	je	.L262
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L262
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r12), %rcx
.L262:
	movq	16(%r12), %r13
	movq	%rcx, (%r10)
	testq	%r13, %r13
	je	.L304
	movq	0(%r13), %r14
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	2592(%r14), %rsi
	divq	%rsi
	movq	2584(%r14), %rax
	movq	%rax, -56(%rbp)
	leaq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	movq	(%rax), %r9
	movq	%rax, -64(%rbp)
	testq	%r9, %r9
	je	.L265
	movq	(%r9), %rdi
	movq	%r9, %r10
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv(%rip), %r15
	movq	32(%rdi), %rcx
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L266:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L265
	movq	32(%r8), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r11
	jne	.L265
	movq	%r8, %rdi
.L268:
	cmpq	%rcx, %r13
	jne	.L266
	cmpq	%r15, 8(%rdi)
	jne	.L266
	cmpq	16(%rdi), %r13
	jne	.L266
	movq	(%rdi), %rcx
	cmpq	%r10, %r9
	je	.L305
	testq	%rcx, %rcx
	je	.L270
	movq	32(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L270
	movq	-56(%rbp), %rax
	movq	%r10, (%rax,%rdx,8)
	movq	(%rdi), %rcx
.L270:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r14)
.L265:
	cmpq	8(%r13), %r13
	je	.L306
.L272:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 112(%rbx)
.L257:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L277
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L262
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L261:
	leaq	104(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L307
.L263:
	movq	$0, (%r14)
	movq	(%r12), %rcx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L305:
	testq	%rcx, %rcx
	je	.L278
	movq	32(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r11
	je	.L270
	movq	-56(%rbp), %rax
	movq	%r10, (%rax,%rdx,8)
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
.L269:
	leaq	2600(%r14), %rdx
	cmpq	%rdx, %rax
	je	.L308
.L271:
	movq	-64(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rcx
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L306:
	movq	$0, 8(%r13)
	leaq	8(%r13), %r15
	movq	%r15, %rdi
	call	uv_timer_stop@PLT
	movq	0(%r13), %r14
	movl	$24, %edi
	addl	$1, 2152(%r14)
	call	_Znwm@PLT
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s(%rip), %rdx
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_(%rip), %rsi
	movq	%r15, %rdi
	movq	%rdx, 8(%rax)
	movq	8(%r13), %rdx
	movq	%r14, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 8(%r13)
	call	uv_close@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120InspectorTimerHandleD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r10, %rax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r10, %rax
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%rcx, 104(%rbx)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%rcx, 2600(%r14)
	jmp	.L271
	.cfi_endproc
.LFE8585:
	.size	_ZN4node9inspector19NodeInspectorClient11cancelTimerEPv, .-_ZN4node9inspector19NodeInspectorClient11cancelTimerEPv
	.section	.text._ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_
	.type	_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_, @function
_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_:
.LFB8580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movl	$24, %edi
	movq	%rsi, -56(%rbp)
	movsd	%xmm0, -64(%rbp)
	call	_Znwm@PLT
	movl	$176, %edi
	movq	%r14, 8(%rax)
	movq	%rax, %r12
	movq	$0, (%rax)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	movq	%r14, 168(%rax)
	leaq	8(%rax), %r14
	movq	%rsi, 160(%rax)
	movq	%r14, %rsi
	movq	%r13, (%rax)
	movq	360(%r13), %rax
	movq	%r14, -72(%rbp)
	movq	2360(%rax), %rdi
	call	uv_timer_init@PLT
	movsd	-64(%rbp), %xmm0
	mulsd	.LC3(%rip), %xmm0
	movq	%r14, %rdi
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer7OnTimerEP10uv_timer_s(%rip), %rsi
	cvttsd2siq	%xmm0, %rdx
	movq	%rdx, %rcx
	call	uv_timer_start@PLT
	movq	2640(%r13), %rdx
	movq	%rbx, 8(%rbx)
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -56(%rbp)
	movq	%rax, 2640(%r13)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	$0, (%rax)
	movq	%rax, %r14
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv(%rip), %rax
	movq	%rax, 8(%r14)
	movq	%rbx, %rax
	movq	%rdx, 24(%r14)
	xorl	%edx, %edx
	movq	%rbx, 16(%r14)
	movq	2592(%r13), %rsi
	divq	%rsi
	movq	2584(%r13), %rax
	movq	(%rax,%rdx,8), %rdi
	leaq	0(,%rdx,8), %r8
	testq	%rdi, %rdi
	je	.L310
	movq	(%rdi), %rax
	movq	%rdx, %r9
	movq	32(%rax), %rcx
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L311:
	movq	(%rax), %r10
	testq	%r10, %r10
	je	.L310
	movq	32(%r10), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L310
	movq	%r10, %rax
.L313:
	cmpq	%rcx, %rbx
	jne	.L311
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv(%rip), %rcx
	cmpq	%rcx, 8(%rax)
	jne	.L311
	cmpq	16(%rax), %rbx
	jne	.L311
	cmpq	$0, (%rdi)
	je	.L310
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L310:
	movq	2608(%r13), %rdx
	movl	$1, %ecx
	leaq	2616(%r13), %rdi
	movq	%r8, -56(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rcx
	testb	%al, %al
	jne	.L314
	movq	2584(%r13), %r9
	movq	-56(%rbp), %r8
	movq	%rbx, 32(%r14)
	addq	%r9, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L324
.L412:
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	(%r8), %rax
	movq	%r14, (%rax)
.L325:
	addq	$1, 2608(%r13)
	movq	8(%r12), %r13
	xorl	%edx, %edx
	movq	96(%r15), %rsi
	movq	%rbx, 16(%r12)
	movq	%r13, %rax
	divq	%rsi
	movq	88(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L327
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L408:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L327
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L327
.L329:
	cmpq	%rdi, %r13
	jne	.L408
	movq	(%rbx), %r13
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	2592(%r13), %rsi
	movq	2584(%r13), %r14
	divq	%rsi
	leaq	(%r14,%rdx,8), %r15
	movq	%rdx, %r10
	movq	(%r15), %r9
	testq	%r9, %r9
	je	.L332
	movq	(%r9), %rdi
	movq	%r9, %r11
	movq	32(%rdi), %rcx
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L330:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L332
	movq	32(%r8), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r11
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L332
	movq	%r8, %rdi
.L333:
	cmpq	%rcx, %rbx
	jne	.L330
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L330
	cmpq	16(%rdi), %rbx
	jne	.L330
	movq	(%rdi), %rcx
	cmpq	%r11, %r9
	je	.L409
	testq	%rcx, %rcx
	je	.L335
	movq	32(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L335
	movq	%r11, (%r14,%rdx,8)
	movq	(%rdi), %rcx
.L335:
	movq	%rcx, (%r11)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r13)
.L332:
	cmpq	8(%rbx), %rbx
	je	.L410
.L337:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	112(%r15), %rdx
	leaq	120(%r15), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L338
	movq	88(%r15), %r8
.L339:
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L348
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%r14), %rax
	movq	%r12, (%rax)
.L349:
	addq	$1, 112(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L411
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L342
	leaq	0(,%rdx,8), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-64(%rbp), %rcx
	movq	%rax, %r9
	leaq	2632(%r13), %rax
	movq	%rax, -56(%rbp)
.L317:
	movq	2600(%r13), %rdi
	movq	$0, 2600(%r13)
	testq	%rdi, %rdi
	je	.L319
	xorl	%r10d, %r10d
	leaq	2600(%r13), %r11
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L321:
	movq	(%r8), %rdx
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
.L322:
	testq	%rdi, %rdi
	je	.L319
.L320:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	movq	32(%rsi), %rax
	divq	%rcx
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L321
	movq	2600(%r13), %r8
	movq	%r8, (%rsi)
	movq	%rsi, 2600(%r13)
	movq	%r11, (%rax)
	cmpq	$0, (%rsi)
	je	.L355
	movq	%rsi, (%r9,%r10,8)
	movq	%rdx, %r10
	testq	%rdi, %rdi
	jne	.L320
	.p2align 4,,10
	.p2align 3
.L319:
	movq	2584(%r13), %rdi
	cmpq	%rdi, -56(%rbp)
	je	.L323
	movq	%rcx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r9
.L323:
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	%rcx, 2592(%r13)
	divq	%rcx
	movq	%r9, 2584(%r13)
	movq	%rbx, 32(%r14)
	leaq	0(,%rdx,8), %r8
	addq	%r9, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L412
.L324:
	movq	2600(%r13), %rax
	movq	%rax, (%r14)
	movq	%r14, 2600(%r13)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L326
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r13)
	movq	%r14, (%r9,%rdx,8)
.L326:
	leaq	2600(%r13), %rax
	movq	%rax, (%r8)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L338:
	cmpq	$1, %rdx
	je	.L413
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L342
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	136(%r15), %r10
	movq	%rax, %r8
.L341:
	movq	104(%r15), %rsi
	movq	$0, 104(%r15)
	testq	%rsi, %rsi
	je	.L343
	xorl	%edi, %edi
	leaq	104(%r15), %r9
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L345:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L346:
	testq	%rsi, %rsi
	je	.L343
.L344:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%rbx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L345
	movq	104(%r15), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 104(%r15)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L357
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%rdx, %r10
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L409:
	testq	%rcx, %rcx
	je	.L356
	movq	32(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L335
	movq	%r11, (%r14,%rdx,8)
	movq	(%r15), %rax
.L334:
	leaq	2600(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L414
.L336:
	movq	$0, (%r15)
	movq	(%rdi), %rcx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L410:
	movq	-72(%rbp), %r15
	movq	$0, 8(%rbx)
	movq	%r15, %rdi
	call	uv_timer_stop@PLT
	movq	(%rbx), %r13
	movl	$24, %edi
	addl	$1, 2152(%r13)
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s(%rip), %rcx
	movq	%r15, %rdi
	movq	%r13, (%rax)
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_(%rip), %rsi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 8(%rbx)
	call	uv_close@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L343:
	movq	88(%r15), %rdi
	cmpq	%r10, %rdi
	je	.L347
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L347:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%rbx, 96(%r15)
	divq	%rbx
	movq	%r8, 88(%r15)
	leaq	0(,%rdx,8), %r14
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L348:
	movq	104(%r15), %rax
	movq	%r12, 104(%r15)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L350
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	96(%r15)
	movq	%r12, (%r8,%rdx,8)
.L350:
	leaq	104(%r15), %rax
	movq	%rax, (%r14)
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%rdx, %rdi
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L356:
	movq	%r11, %rax
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	2632(%r13), %r9
	movq	$0, 2632(%r13)
	movq	%r9, -56(%rbp)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%rcx, 2600(%r13)
	jmp	.L336
.L413:
	leaq	136(%r15), %r8
	movq	$0, 136(%r15)
	movq	%r8, %r10
	jmp	.L341
.L342:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8580:
	.size	_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_, .-_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_
	.section	.text._ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi
	.type	_ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi, @function
_ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi:
.LFB8538:
	.cfi_startproc
	endbr64
	cmpb	$0, 17(%rdi)
	movb	$1, 148(%rdi)
	jne	.L433
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	movb	$1, 17(%rdi)
	movq	360(%rax), %rax
	movq	2392(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L423:
	cmpb	$0, 149(%rbx)
	jne	.L417
	cmpb	$0, 150(%rbx)
	jne	.L418
	cmpb	$0, 148(%rbx)
	jne	.L418
.L419:
	movb	$0, 17(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	48(%rbx), %rax
	testq	%rax, %rax
	jne	.L420
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L436:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L419
.L420:
	movq	16(%rax), %rdx
	cmpb	$0, 64(%rdx)
	je	.L436
.L417:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L422
	call	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv@PLT
	.p2align 4,,10
	.p2align 3
.L422:
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	movq	(%r12), %rax
	call	*168(%rax)
	testb	%al, %al
	jne	.L422
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE8538:
	.size	_ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi, .-_ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB14456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	176(%rdi), %r12
	testq	%r12, %r12
	je	.L438
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L439
.L438:
	movq	168(%rbx), %rax
	movq	160(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	160(%rbx), %rdi
	leaq	208(%rbx), %rax
	movq	$0, 184(%rbx)
	movq	$0, 176(%rbx)
	cmpq	%rax, %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	120(%rbx), %r12
	testq	%r12, %r12
	jne	.L444
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L494:
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L441
.L443:
	movq	%r13, %r12
.L444:
	movq	16(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L494
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L443
.L441:
	movq	112(%rbx), %rax
	movq	104(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	104(%rbx), %rdi
	leaq	152(%rbx), %rax
	movq	$0, 128(%rbx)
	movq	$0, 120(%rbx)
	cmpq	%rax, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	jne	.L457
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L496:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L446
.L456:
	movq	%r14, %r13
.L457:
	movq	88(%r13), %r12
	movq	0(%r13), %r14
	testq	%r12, %r12
	je	.L448
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L449
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L450:
	cmpl	$1, %eax
	je	.L495
.L448:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L496
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L456
.L446:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L460
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L461
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L497
	.p2align 4,,10
	.p2align 3
.L460:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L437
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L468
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L498
.L437:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L452
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L453:
	cmpl	$1, %eax
	jne	.L448
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L449:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L452:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L461:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L460
.L497:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L464
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L465:
	cmpl	$1, %eax
	jne	.L460
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L468:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L437
.L498:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L465
	.cfi_endproc
.LFE14456:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN4node9inspector8protocol11WorkerAgentD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11WorkerAgentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11WorkerAgentD0Ev
	.type	_ZN4node9inspector8protocol11WorkerAgentD0Ev, @function
_ZN4node9inspector8protocol11WorkerAgentD0Ev:
.LFB11212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	56(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L501
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L502
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L524
	.p2align 4,,10
	.p2align 3
.L501:
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L507
	movq	%r13, %rdi
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L507:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L509
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L510
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L525
	.p2align 4,,10
	.p2align 3
.L509:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L514
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L515
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L526
.L514:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L514
.L526:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L518
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L519:
	cmpl	$1, %eax
	jne	.L514
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L510:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L509
.L525:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L502:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L501
.L524:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L505
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L506:
	cmpl	$1, %eax
	jne	.L501
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L505:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L518:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L519
	.cfi_endproc
.LFE11212:
	.size	_ZN4node9inspector8protocol11WorkerAgentD0Ev, .-_ZN4node9inspector8protocol11WorkerAgentD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE:
.LFB8693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L527
	leaq	8(%r12), %rax
	movq	%rsi, %r13
	movq	%rax, -160(%rbp)
	movl	8(%r12), %eax
.L530:
	testl	%eax, %eax
	je	.L527
	movq	-160(%rbp), %rbx
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L530
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L531
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L531
	movl	8(%rdi), %edx
	leaq	32(%rax), %rdi
	leaq	-144(%rbp), %rsi
	leaq	-128(%rbp), %rbx
	leaq	-136(%rbp), %r15
	movl	%edx, -144(%rbp)
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector12_GLOBAL__N_111ChannelImplESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	subq	$8, %rsp
	movq	%rbx, %rdi
	movq	(%rax), %rax
	pushq	16(%r13)
	pushq	8(%r13)
	pushq	0(%r13)
	movq	%rax, -152(%rbp)
	call	_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE@PLT
	addq	$32, %rsp
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol10StringUtil12parseMessageERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb@PLT
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L532
	cmpl	$6, 8(%r14)
	movl	$0, %eax
	cmovne	%rax, %r14
.L532:
	leaq	-80(%rbp), %rcx
	leaq	-96(%rbp), %rax
	movq	%r14, %rsi
	movb	$0, -80(%rbp)
	movq	%rcx, -176(%rbp)
	leaq	-140(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	movq	-152(%rbp), %rcx
	movq	$0, -88(%rbp)
	movq	56(%rcx), %rdi
	movq	%rax, %rcx
	movq	%rax, -168(%rbp)
	call	_ZN4node9inspector8protocol14UberDispatcher12parseCommandEPNS1_5ValueEPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-136(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %rdi
	call	_ZN12v8_inspector18V8InspectorSession17canDispatchMethodERKNS_10StringViewE@PLT
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L533
	movq	(%rdi), %rdx
	movb	%al, -177(%rbp)
	call	*8(%rdx)
	movzbl	-177(%rbp), %eax
.L533:
	testb	%al, %al
	movq	-152(%rbp), %rax
	je	.L534
	movq	48(%rax), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	testq	%r14, %r14
	je	.L539
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
.L539:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L575
	movq	-160(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L576
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L577
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L527
.L576:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L544
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L545:
	cmpl	$1, %eax
	jne	.L527
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L534:
	movq	56(%rax), %rdi
	movl	-140(%rbp), %esi
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-168(%rbp), %rdx
	movq	%r14, -136(%rbp)
	call	_ZN4node9inspector8protocol14UberDispatcher8dispatchEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS1_5ValueESt14default_deleteISC_EESA_@PLT
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L537
	movq	(%rdi), %rax
	call	*24(%rax)
.L537:
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L539
	call	_ZdlPv@PLT
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L544:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L545
.L577:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8693:
	.size	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE, .-_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"NODE_DEBUG_ENABLED"
.LC5:
	.string	"cmd"
.LC6:
	.string	"internalMessage"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector5Agent13StartIoThreadEv.part.0, @function
_ZN4node9inspector5Agent13StartIoThreadEv.part.0:
.LFB14824:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	120(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movdqu	168(%rdi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	176(%rdi), %rax
	movaps	%xmm1, -80(%rbp)
	testq	%rax, %rax
	je	.L579
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L580
	lock addl	$1, 8(%rax)
.L579:
	movq	8(%rbx), %r13
	leaq	40(%rbx), %r9
	movq	152(%r13), %r12
	testq	%r12, %r12
	je	.L640
.L581:
	leaq	-96(%rbp), %r14
	movq	%r12, %rsi
	movq	%r9, -120(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN4node9inspector19MainThreadInterface9GetHandleEv@PLT
	movq	-120(%rbp), %r9
	leaq	-104(%rbp), %rdi
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r9, %rdx
	call	_ZN4node9inspector11InspectorIo5StartESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE@PLT
	movq	-104(%rbp), %rax
	movq	24(%rbx), %r12
	movq	$0, -104(%rbp)
	movq	%rax, 24(%rbx)
	testq	%r12, %r12
	je	.L598
	movq	%r12, %rdi
	call	_ZN4node9inspector11InspectorIoD1Ev@PLT
	movq	%r12, %rdi
	movl	$216, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L598
	movq	%r12, %rdi
	call	_ZN4node9inspector11InspectorIoD1Ev@PLT
	movl	$216, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L598:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L601
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L602
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L641
	.p2align 4,,10
	.p2align 3
.L601:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L608
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L609
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L642
	.p2align 4,,10
	.p2align 3
.L608:
	xorl	%eax, %eax
	cmpq	$0, 24(%rbx)
	je	.L578
	movq	(%rbx), %r14
	movq	%r13, %rdi
	movq	352(%r14), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r14), %rax
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	$18, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L643
.L615:
	xorl	%edx, %edx
	movl	$3, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L644
.L616:
	movq	-120(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L645
.L617:
	movq	%r12, %rdx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$1, %eax
.L578:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L646
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L609:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L608
.L642:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L612
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L613:
	cmpl	$1, %eax
	jne	.L608
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L602:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L601
.L641:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L605
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L606:
	cmpl	$1, %eax
	jne	.L601
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L640:
	movq	8(%r13), %rax
	movl	$384, %edi
	movq	%r9, -152(%rbp)
	movq	360(%rax), %rdx
	movq	352(%rax), %rcx
	movq	2080(%rax), %r14
	movq	2392(%rdx), %r8
	movq	2360(%rdx), %rdx
	movq	%rcx, -136(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rdx, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rdx
	movq	-144(%rbp), %r8
	movq	%r14, %rsi
	movabsq	$4294967297, %rcx
	leaq	16(%rax), %r12
	movq	%rax, -120(%rbp)
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movq	-136(%rbp), %rcx
	call	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE@PLT
	movq	-120(%rbp), %rax
	movq	-152(%rbp), %r9
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L582
	movl	8(%rdx), %edx
	testl	%edx, %edx
	jne	.L583
.L582:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	movq	%r12, 16(%rax)
	testq	%r14, %r14
	je	.L647
	lock addl	$1, 12(%rax)
.L584:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L586
	testq	%r14, %r14
	je	.L587
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L588:
	cmpl	$1, %edx
	jne	.L586
	movq	(%rdi), %rdx
	movq	%rax, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	*24(%rdx)
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L586:
	movq	%rax, 24(%rax)
.L583:
	movq	160(%r13), %rdi
	movq	%r12, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 152(%r13)
	testq	%rdi, %rdi
	je	.L581
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L591
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L592:
	cmpl	$1, %eax
	je	.L593
.L639:
	movq	152(%r13), %r12
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L612:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L605:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rdx
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L645:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L643:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L591:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L593:
	movq	(%rdi), %rax
	movq	%r9, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	*16(%rax)
	testq	%r14, %r14
	movq	-120(%rbp), %rdi
	movq	-128(%rbp), %r9
	je	.L594
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L595:
	cmpl	$1, %eax
	jne	.L639
	movq	(%rdi), %rax
	movq	%r9, -120(%rbp)
	call	*24(%rax)
	movq	152(%r13), %r12
	movq	-120(%rbp), %r9
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L647:
	addl	$1, 12(%rax)
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L587:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L594:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L595
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14824:
	.size	_ZN4node9inspector5Agent13StartIoThreadEv.part.0, .-_ZN4node9inspector5Agent13StartIoThreadEv.part.0
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116StartIoInterruptEPN2v87IsolateEPv, @function
_ZN4node9inspector12_GLOBAL__N_116StartIoInterruptEPN2v87IsolateEPv:
.LFB8389:
	.cfi_startproc
	endbr64
	cmpq	$0, 24(%rsi)
	je	.L654
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	cmpq	$0, 8(%rsi)
	movq	%rsi, %rdi
	je	.L655
	jmp	_ZN4node9inspector5Agent13StartIoThreadEv.part.0
	.p2align 4,,10
	.p2align 3
.L655:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector5Agent13StartIoThreadEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8389:
	.size	_ZN4node9inspector12_GLOBAL__N_116StartIoInterruptEPN2v87IsolateEPv, .-_ZN4node9inspector12_GLOBAL__N_116StartIoInterruptEPN2v87IsolateEPv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111StartIoTask3RunEv, @function
_ZN4node9inspector12_GLOBAL__N_111StartIoTask3RunEv:
.LFB8383:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	cmpq	$0, 24(%rdi)
	je	.L662
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	cmpq	$0, 8(%rdi)
	je	.L663
	jmp	_ZN4node9inspector5Agent13StartIoThreadEv.part.0
	.p2align 4,,10
	.p2align 3
.L663:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector5Agent13StartIoThreadEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8383:
	.size	_ZN4node9inspector12_GLOBAL__N_111StartIoTask3RunEv, .-_ZN4node9inspector12_GLOBAL__N_111StartIoTask3RunEv
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_126StartIoThreadAsyncCallbackEP10uv_async_s, @function
_ZN4node9inspector12_GLOBAL__N_126StartIoThreadAsyncCallbackEP10uv_async_s:
.LFB8388:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	cmpq	$0, 24(%rdi)
	je	.L670
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	cmpq	$0, 8(%rdi)
	je	.L671
	jmp	_ZN4node9inspector5Agent13StartIoThreadEv.part.0
	.p2align 4,,10
	.p2align 3
.L671:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector5Agent13StartIoThreadEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8388:
	.size	_ZN4node9inspector12_GLOBAL__N_126StartIoThreadAsyncCallbackEP10uv_async_s, .-_ZN4node9inspector12_GLOBAL__N_126StartIoThreadAsyncCallbackEP10uv_async_s
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImplD2Ev:
.LFB8424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	96+_ZTVN4node9inspector12_GLOBAL__N_111ChannelImplE(%rip), %rax
	leaq	-80(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	movq	(%rsi), %rax
	movq	%r14, %rdi
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L673
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
.L674:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %r13
	cmpq	%r13, %rdi
	je	.L675
	call	_ZdlPv@PLT
.L675:
	movq	24(%rbx), %rdi
	movq	$0, 24(%rbx)
	testq	%rdi, %rdi
	je	.L676
	movq	(%rdi), %rax
	call	*8(%rax)
.L676:
	movq	32(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L678
	movq	(%rsi), %rax
	movq	%r14, %rdi
	call	*32(%rax)
	movq	-104(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movq	32(%rbx), %r12
	movq	$0, 32(%rbx)
	testq	%r12, %r12
	je	.L678
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol11WorkerAgentD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L681
	movq	56(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L683
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L684
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L685:
	cmpl	$1, %eax
	jne	.L683
	movq	(%rdi), %rax
	movq	%rdi, -120(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-120(%rbp), %rdi
	je	.L687
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L688:
	cmpl	$1, %eax
	jne	.L683
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L683:
	movq	40(%r12), %r15
	testq	%r15, %r15
	je	.L689
	movq	%r15, %rdi
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L689:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L691
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L692
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L693:
	cmpl	$1, %eax
	jne	.L691
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L691:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L696
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L697
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L698:
	cmpl	$1, %eax
	jne	.L696
	movq	(%rdi), %rax
	movq	%rdi, -120(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-120(%rbp), %rdi
	je	.L700
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L701:
	cmpl	$1, %eax
	jne	.L696
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L696:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L678:
	movq	16(%rbx), %rsi
	leaq	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv(%rip), %rdx
	movq	%r14, %rdi
	movq	(%rsi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L702
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
.L703:
	movq	-104(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L704
	call	_ZdlPv@PLT
.L704:
	movq	16(%rbx), %rdi
	movq	$0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L705
	movq	(%rdi), %rax
	call	*8(%rax)
.L705:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L706
	movq	(%rdi), %rax
	call	*8(%rax)
.L706:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L707
	movq	(%rdi), %rax
	call	*8(%rax)
.L707:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L708
	movq	(%rdi), %rax
	call	*8(%rax)
.L708:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L709
	movq	(%r12), %rax
	leaq	_ZN4node9inspector8protocol11WorkerAgentD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L710
	movq	56(%r12), %r13
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L712
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L713
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L714:
	cmpl	$1, %eax
	jne	.L712
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L716
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L717:
	cmpl	$1, %eax
	je	.L768
	.p2align 4,,10
	.p2align 3
.L712:
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L718
	movq	%r13, %rdi
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L718:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L720
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L721
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L722:
	cmpl	$1, %eax
	jne	.L720
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L720:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L725
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L726
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L727:
	cmpl	$1, %eax
	jne	.L725
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L729
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L730:
	cmpl	$1, %eax
	je	.L769
	.p2align 4,,10
	.p2align 3
.L725:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L709:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L731
	movq	(%rdi), %rax
	call	*8(%rax)
.L731:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L672
	movq	(%rdi), %rax
	call	*8(%rax)
.L672:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L770
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	call	*%rax
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L702:
	call	*%rax
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L721:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L726:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L713:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L684:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L692:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L697:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L710:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L769:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L768:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L687:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L700:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L716:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L729:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L730
.L770:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8424:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD2Ev, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImplD2Ev
	.set	.LTHUNK0,_ZN4node9inspector12_GLOBAL__N_111ChannelImplD2Ev
	.p2align 4
	.type	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD1Ev, @function
_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD1Ev:
.LFB14876:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK0
	.cfi_endproc
.LFE14876:
	.size	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD1Ev, .-_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	.set	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev,_ZN4node9inspector12_GLOBAL__N_111ChannelImplD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_111ChannelImplD0Ev:
.LFB8426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8426:
	.size	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD0Ev, .-_ZN4node9inspector12_GLOBAL__N_111ChannelImplD0Ev
	.section	.text._ZN4node9inspector19NodeInspectorClientD2Ev,"axG",@progbits,_ZN4node9inspector19NodeInspectorClientD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClientD2Ev
	.type	_ZN4node9inspector19NodeInspectorClientD2Ev, @function
_ZN4node9inspector19NodeInspectorClientD2Ev:
.LFB14417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector19NodeInspectorClientE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	176(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L775
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L776
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L845
	.p2align 4,,10
	.p2align 3
.L775:
	movq	160(%r15), %r12
	testq	%r12, %r12
	je	.L782
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L783
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L846
	.p2align 4,,10
	.p2align 3
.L782:
	movq	104(%r15), %r13
	testq	%r13, %r13
	je	.L800
	movq	16(%r13), %rbx
	movq	0(%r13), %r14
	movq	%r15, -64(%rbp)
	testq	%rbx, %rbx
	je	.L847
	.p2align 4,,10
	.p2align 3
.L791:
	movq	(%rbx), %r12
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	2592(%r12), %rcx
	movq	2584(%r12), %r15
	divq	%rcx
	leaq	(%r15,%rdx,8), %rax
	movq	%rdx, %r10
	movq	(%rax), %r8
	movq	%rax, -56(%rbp)
	testq	%r8, %r8
	je	.L792
	movq	(%r8), %rdi
	movq	%r8, %rsi
	movq	32(%rdi), %r9
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L793:
	movq	(%rdi), %r11
	testq	%r11, %r11
	je	.L792
	movq	32(%r11), %r9
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L792
	movq	%r11, %rdi
.L795:
	cmpq	%r9, %rbx
	jne	.L793
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L793
	cmpq	16(%rdi), %rbx
	jne	.L793
	movq	(%rdi), %r9
	cmpq	%rsi, %r8
	je	.L848
	testq	%r9, %r9
	je	.L797
	movq	32(%r9), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L797
	movq	%rsi, (%r15,%rdx,8)
	movq	(%rdi), %r9
.L797:
	movq	%r9, (%rsi)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L792:
	cmpq	8(%rbx), %rbx
	je	.L849
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L841
.L801:
	movq	%r14, %r13
	movq	16(%r13), %rbx
	movq	0(%r13), %r14
	testq	%rbx, %rbx
	jne	.L791
.L847:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120InspectorTimerHandleD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L848:
	testq	%r9, %r9
	je	.L811
	movq	32(%r9), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L797
	movq	-56(%rbp), %rax
	movq	%rsi, (%r15,%rdx,8)
	leaq	2600(%r12), %rdx
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L850
.L798:
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %r9
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L849:
	movq	$0, 8(%rbx)
	leaq	8(%rbx), %r15
	movq	%r15, %rdi
	call	uv_timer_stop@PLT
	movq	(%rbx), %r12
	movl	$24, %edi
	addl	$1, 2152(%r12)
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movq	%r15, %rdi
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s(%rip), %rcx
	movq	%r12, (%rax)
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_(%rip), %rsi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 8(%rbx)
	call	uv_close@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L801
	.p2align 4,,10
	.p2align 3
.L841:
	movq	-64(%rbp), %r15
.L800:
	movq	96(%r15), %rax
	movq	88(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	88(%r15), %rdi
	leaq	136(%r15), %rax
	movq	$0, 112(%r15)
	movq	$0, 104(%r15)
	cmpq	%rax, %rdi
	je	.L789
	call	_ZdlPv@PLT
.L789:
	movq	48(%r15), %r12
	testq	%r12, %r12
	jne	.L802
	.p2align 4,,10
	.p2align 3
.L806:
	movq	40(%r15), %rax
	movq	32(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	32(%r15), %rdi
	leaq	80(%r15), %rax
	movq	$0, 56(%r15)
	movq	$0, 48(%r15)
	cmpq	%rax, %rdi
	je	.L803
	call	_ZdlPv@PLT
.L803:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L773
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	movq	%r13, %rdi
	movl	$72, %esi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L806
.L807:
	movq	%rbx, %r12
.L802:
	movq	16(%r12), %r13
	movq	(%r12), %rbx
	testq	%r13, %r13
	jne	.L851
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L807
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L783:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L782
.L846:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L786
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L787:
	cmpl	$1, %eax
	jne	.L782
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L776:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L775
.L845:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L779
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L780:
	cmpl	$1, %eax
	jne	.L775
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%rsi, %rax
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	jne	.L798
.L850:
	movq	%r9, 2600(%r12)
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L773:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L779:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L780
	.cfi_endproc
.LFE14417:
	.size	_ZN4node9inspector19NodeInspectorClientD2Ev, .-_ZN4node9inspector19NodeInspectorClientD2Ev
	.weak	_ZN4node9inspector19NodeInspectorClientD1Ev
	.set	_ZN4node9inspector19NodeInspectorClientD1Ev,_ZN4node9inspector19NodeInspectorClientD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD2Ev:
.LFB8690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L852
	movl	8(%r13), %eax
	movq	%rdi, %rbx
	leaq	8(%r13), %r15
.L857:
	testl	%eax, %eax
	je	.L908
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%r15)
	jne	.L857
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.L858
	movq	16(%rbx), %r14
	testq	%r14, %r14
	je	.L858
	movslq	8(%rbx), %r10
	movq	40(%r14), %rcx
	xorl	%edx, %edx
	movq	32(%r14), %rdi
	movq	%r10, %rax
	movq	%r10, %r8
	divq	%rcx
	movq	(%rdi,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L858
	movq	(%rax), %r12
	movl	8(%r12), %esi
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L909:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L858
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L858
.L861:
	cmpl	%esi, %r8d
	jne	.L909
	movq	16(%r12), %r9
	xorl	%edx, %edx
	movzbl	65(%r9), %eax
	movb	%al, -49(%rbp)
	movq	%r10, %rax
	divq	%rcx
	leaq	(%rdi,%rdx,8), %r11
	movq	%rdx, %r10
	movq	(%r11), %rdx
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%rax, %rsi
	movq	(%rax), %rax
	cmpq	%rax, %r12
	jne	.L862
	movq	(%r12), %r8
	cmpq	%rsi, %rdx
	je	.L910
	testq	%r8, %r8
	je	.L865
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L865
	movq	%rsi, (%rdi,%rdx,8)
	movq	(%r12), %r8
.L865:
	movq	%r8, (%rsi)
	movq	%r9, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	movq	-64(%rbp), %r9
	movl	$72, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 56(%r14)
	cmpb	$0, -49(%rbp)
	je	.L882
	movq	48(%r14), %rax
	testq	%rax, %rax
	jne	.L868
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L911:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L867
.L868:
	movq	16(%rax), %rdx
	cmpb	$0, 65(%rdx)
	je	.L911
	.p2align 4,,10
	.p2align 3
.L858:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	testq	%r12, %r12
	je	.L912
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
	cmpl	$1, %eax
	je	.L870
.L908:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L852
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L876
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L913
.L852:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L912:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L908
.L870:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L871
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L872:
	cmpl	$1, %eax
	jne	.L908
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L876:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L852
.L913:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	movq	8(%r14), %rax
	movq	24(%r14), %rdi
	movq	3280(%rax), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
.L882:
	cmpb	$0, 150(%r14)
	je	.L858
	cmpb	$0, 16(%r14)
	jne	.L858
	movb	$0, 150(%r14)
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L910:
	testq	%r8, %r8
	je	.L885
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L865
	movq	%rsi, (%rdi,%rdx,8)
	movq	(%r11), %rax
.L864:
	leaq	48(%r14), %rdx
	cmpq	%rdx, %rax
	je	.L914
.L866:
	movq	$0, (%r11)
	movq	(%r12), %r8
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L871:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L885:
	movq	%rsi, %rax
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L914:
	movq	%r8, 48(%r14)
	jmp	.L866
	.cfi_endproc
.LFE8690:
	.size	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD2Ev, .-_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD1Ev,_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD0Ev:
.LFB8692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8692:
	.size	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD0Ev, .-_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD0Ev
	.p2align 4
	.type	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD0Ev, @function
_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD0Ev:
.LFB14862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14862:
	.size	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD0Ev, .-_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD0Ev
	.section	.text._ZN4node9inspector19NodeInspectorClientD0Ev,"axG",@progbits,_ZN4node9inspector19NodeInspectorClientD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClientD0Ev
	.type	_ZN4node9inspector19NodeInspectorClientD0Ev, @function
_ZN4node9inspector19NodeInspectorClientD0Ev:
.LFB14419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector19NodeInspectorClientE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	176(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L921
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L922
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L994
	.p2align 4,,10
	.p2align 3
.L921:
	movq	160(%r15), %r12
	testq	%r12, %r12
	je	.L928
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L929
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L995
	.p2align 4,,10
	.p2align 3
.L928:
	movq	104(%r15), %r13
	testq	%r13, %r13
	je	.L946
	movq	16(%r13), %rbx
	movq	0(%r13), %r14
	movq	%r15, -64(%rbp)
	testq	%rbx, %rbx
	je	.L996
	.p2align 4,,10
	.p2align 3
.L937:
	movq	(%rbx), %r12
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	2592(%r12), %rcx
	movq	2584(%r12), %r15
	divq	%rcx
	leaq	(%r15,%rdx,8), %rax
	movq	%rdx, %r10
	movq	(%rax), %r8
	movq	%rax, -56(%rbp)
	testq	%r8, %r8
	je	.L938
	movq	(%r8), %rdi
	movq	%r8, %rsi
	movq	32(%rdi), %r9
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L939:
	movq	(%rdi), %r11
	testq	%r11, %r11
	je	.L938
	movq	32(%r11), %r9
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L938
	movq	%r11, %rdi
.L941:
	cmpq	%r9, %rbx
	jne	.L939
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer11CleanupHookEPv(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L939
	cmpq	16(%rdi), %rbx
	jne	.L939
	movq	(%rdi), %r9
	cmpq	%rsi, %r8
	je	.L997
	testq	%r9, %r9
	je	.L943
	movq	32(%r9), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L943
	movq	%rsi, (%r15,%rdx,8)
	movq	(%rdi), %r9
.L943:
	movq	%r9, (%rsi)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L938:
	cmpq	8(%rbx), %rbx
	je	.L998
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L990
.L947:
	movq	%r14, %r13
	movq	16(%r13), %rbx
	movq	0(%r13), %r14
	testq	%rbx, %rbx
	jne	.L937
.L996:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120InspectorTimerHandleD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L997:
	testq	%r9, %r9
	je	.L957
	movq	32(%r9), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r10
	je	.L943
	movq	-56(%rbp), %rax
	movq	%rsi, (%r15,%rdx,8)
	leaq	2600(%r12), %rdx
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L999
.L944:
	movq	-56(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %r9
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L998:
	movq	$0, 8(%rbx)
	leaq	8(%rbx), %r15
	movq	%r15, %rdi
	call	uv_timer_stop@PLT
	movq	(%rbx), %r12
	movl	$24, %edi
	addl	$1, 2152(%r12)
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movq	%r15, %rdi
	leaq	_ZN4node9inspector12_GLOBAL__N_114InspectorTimer13TimerClosedCbEP11uv_handle_s(%rip), %rcx
	movq	%r12, (%rax)
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sPFvPS2_EEEvPT_T0_ENUlS3_E_4_FUNES3_(%rip), %rsi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 8(%rbx)
	call	uv_close@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L947
	.p2align 4,,10
	.p2align 3
.L990:
	movq	-64(%rbp), %r15
.L946:
	movq	96(%r15), %rax
	movq	88(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	88(%r15), %rdi
	leaq	136(%r15), %rax
	movq	$0, 112(%r15)
	movq	$0, 104(%r15)
	cmpq	%rax, %rdi
	je	.L935
	call	_ZdlPv@PLT
.L935:
	movq	48(%r15), %r12
	testq	%r12, %r12
	jne	.L948
	.p2align 4,,10
	.p2align 3
.L952:
	movq	40(%r15), %rax
	movq	32(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	32(%r15), %rdi
	leaq	80(%r15), %rax
	movq	$0, 56(%r15)
	movq	$0, 48(%r15)
	cmpq	%rax, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L954
	movq	(%rdi), %rax
	call	*8(%rax)
.L954:
	addq	$24, %rsp
	movq	%r15, %rdi
	movl	$184, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1000:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	movq	%r13, %rdi
	movl	$72, %esi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L952
.L953:
	movq	%rbx, %r12
.L948:
	movq	16(%r12), %r13
	movq	(%r12), %rbx
	testq	%r13, %r13
	jne	.L1000
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L953
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L922:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L921
.L994:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L925
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L926:
	cmpl	$1, %eax
	jne	.L921
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L929:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L928
.L995:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L932
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L933:
	cmpl	$1, %eax
	jne	.L928
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%rsi, %rax
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	jne	.L944
.L999:
	movq	%r9, 2600(%r12)
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L932:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L925:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L926
	.cfi_endproc
.LFE14419:
	.size	_ZN4node9inspector19NodeInspectorClientD0Ev, .-_ZN4node9inspector19NodeInspectorClientD0Ev
	.section	.text._ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.type	_ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, @function
_ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb:
.LFB8571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movl	%edx, -144(%rbp)
	movq	152(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	144(%rdi), %eax
	leal	1(%rax), %edx
	movl	%eax, -108(%rbp)
	movl	%edx, 144(%rdi)
	testq	%r13, %r13
	je	.L1211
.L1002:
	leaq	-96(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9inspector19MainThreadInterface9GetHandleEv@PLT
	cmpb	$0, 16(%r12)
	je	.L1142
	movq	168(%r12), %rax
	leaq	-80(%rbp), %r15
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L1212
.L1019:
	movq	176(%r12), %r14
	testq	%r14, %r14
	je	.L1018
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1066
	lock addl	$1, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	(%rbx), %rdx
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-88(%rbp), %r13
	movq	8(%r12), %rax
	movq	$0, (%rbx)
	movq	%rdx, -152(%rbp)
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdi
	movq	%r15, %r8
	movq	%rax, %rbx
	leaq	96+_ZTVN4node9inspector12_GLOBAL__N_111ChannelImplE(%rip), %rax
	movq	24(%r12), %rsi
	movq	-152(%rbp), %rdx
	leaq	-80(%rax), %rcx
	movq	%rax, %xmm2
	movups	%xmm0, 16(%rbx)
	movzbl	-144(%rbp), %eax
	movq	%rcx, %xmm1
	movups	%xmm0, 48(%rbx)
	movq	%rbx, %rcx
	punpcklqdq	%xmm2, %xmm1
	movb	%al, 64(%rbx)
	movups	%xmm1, (%rbx)
	movq	(%rsi), %rax
	movq	%rdx, 40(%rbx)
	movl	$1, %edx
	movq	144(%rax), %rax
	movq	$0, 32(%rbx)
	movb	$0, 65(%rbx)
	movb	$1, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	*%rax
	movq	-104(%rbp), %rax
	movq	48(%rbx), %rdi
	movq	$0, -104(%rbp)
	movq	%rax, 48(%rbx)
	testq	%rdi, %rdi
	je	.L1068
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1068
	movq	(%rdi), %rax
	call	*8(%rax)
.L1068:
	movl	$128, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN4node9inspector8protocol14UberDispatcherC1EPNS1_15FrontendChannelE@PLT
	movq	56(%rbx), %rdi
	movq	-144(%rbp), %rax
	movq	%rax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L1133
	movq	(%rdi), %rax
	call	*8(%rax)
.L1133:
	movq	-136(%rbp), %xmm0
	movq	%r13, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%r13, %r13
	je	.L1069
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1070
	lock addl	$1, 8(%r13)
.L1069:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN4node9inspector8protocol12TracingAgentC1EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE@PLT
	movq	-72(%rbp), %rdi
	movq	-128(%rbp), %r8
	testq	%rdi, %rdi
	je	.L1072
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1073
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
	cmpl	$1, %eax
	je	.L1213
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	24(%rbx), %rdi
	movq	%r8, 24(%rbx)
	testq	%rdi, %rdi
	je	.L1078
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	24(%rbx), %r8
.L1078:
	movq	56(%rbx), %rsi
	movq	%r8, %rdi
	call	_ZN4node9inspector8protocol12TracingAgent4WireEPNS1_14UberDispatcherE@PLT
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L1079
	movq	%rax, %xmm0
	movq	%r14, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%r14, %r14
	je	.L1080
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1081
	lock addl	$1, 12(%r14)
.L1080:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN4node9inspector8protocol11WorkerAgentC1ESt8weak_ptrINS0_13WorkerManagerEE@PLT
	movq	-72(%rbp), %rdi
	movq	-120(%rbp), %r8
	testq	%rdi, %rdi
	je	.L1083
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1084
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1085:
	cmpl	$1, %eax
	jne	.L1083
	movq	(%rdi), %rax
	movq	%r8, -120(%rbp)
	call	*24(%rax)
	movq	-120(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	32(%rbx), %r9
	movq	%r8, 32(%rbx)
	testq	%r9, %r9
	je	.L1087
	movq	(%r9), %rax
	leaq	_ZN4node9inspector8protocol11WorkerAgentD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1088
	movq	56(%r9), %r15
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	%rax, (%r9)
	testq	%r15, %r15
	je	.L1090
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1091
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L1092:
	cmpl	$1, %eax
	jne	.L1090
	movq	(%r15), %rax
	movq	%rdx, -128(%rbp)
	movq	%r15, %rdi
	movq	%r9, -120(%rbp)
	call	*16(%rax)
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %r9
	testq	%rdx, %rdx
	je	.L1094
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1095:
	cmpl	$1, %eax
	jne	.L1090
	movq	(%r15), %rax
	movq	%r9, -120(%rbp)
	movq	%r15, %rdi
	call	*24(%rax)
	movq	-120(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	40(%r9), %r15
	testq	%r15, %r15
	je	.L1096
	movq	%r15, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	movq	-120(%rbp), %r9
.L1096:
	movq	32(%r9), %rdi
	testq	%rdi, %rdi
	je	.L1098
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1099
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1100:
	cmpl	$1, %eax
	jne	.L1098
	movq	(%rdi), %rax
	movq	%r9, -120(%rbp)
	call	*24(%rax)
	movq	-120(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	16(%r9), %r15
	testq	%r15, %r15
	je	.L1103
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1104
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L1105:
	cmpl	$1, %eax
	jne	.L1103
	movq	(%r15), %rax
	movq	%rdx, -128(%rbp)
	movq	%r15, %rdi
	movq	%r9, -120(%rbp)
	call	*16(%rax)
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %r9
	testq	%rdx, %rdx
	je	.L1107
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1108:
	cmpl	$1, %eax
	jne	.L1103
	movq	(%r15), %rax
	movq	%r9, -120(%rbp)
	movq	%r15, %rdi
	call	*24(%rax)
	movq	-120(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1103:
	movl	$64, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
	movq	32(%rbx), %r8
.L1087:
	movq	56(%rbx), %rsi
	movq	%r8, %rdi
	call	_ZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherE@PLT
.L1079:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN4node9inspector8protocol12RuntimeAgentC1Ev@PLT
	movq	16(%rbx), %rdi
	movq	%r15, 16(%rbx)
	testq	%rdi, %rdi
	je	.L1109
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	16(%rbx), %r15
.L1109:
	movq	56(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector8protocol12RuntimeAgent4WireEPNS1_14UberDispatcherE@PLT
	testq	%r13, %r13
	je	.L1111
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1112
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1214
	.p2align 4,,10
	.p2align 3
.L1111:
	testq	%r14, %r14
	je	.L1118
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1119
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L1215
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	32(%r12), %rdi
	leaq	-108(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector12_GLOBAL__N_111ChannelImplESt14default_deleteIS7_EEESaISB_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	(%rax), %r12
	movq	%rbx, (%rax)
	testq	%r12, %r12
	je	.L1124
	movq	%r12, %rdi
	call	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1124:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L1126
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1127
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1216
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movl	-108(%rbp), %eax
	jne	.L1217
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1142:
	.cfi_restore_state
	movq	$0, -120(%rbp)
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %r15
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1070:
	addl	$1, 8(%r13)
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1119:
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %eax
	jne	.L1118
.L1215:
	movq	(%r14), %rax
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1122
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1123:
	cmpl	$1, %eax
	jne	.L1118
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1112:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1111
.L1214:
	movq	0(%r13), %rax
	movq	%rdx, -120(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1115
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1116:
	cmpl	$1, %eax
	jne	.L1111
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1073:
	movl	8(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %eax
	jne	.L1072
.L1213:
	movq	(%rdi), %rax
	movq	%rdx, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdi, -128(%rbp)
	call	*16(%rax)
	movq	-144(%rbp), %rdx
	movq	-128(%rbp), %rdi
	movq	-136(%rbp), %r8
	testq	%rdx, %rdx
	je	.L1076
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1077:
	cmpl	$1, %eax
	jne	.L1072
	movq	(%rdi), %rax
	movq	%r8, -128(%rbp)
	call	*24(%rax)
	movq	-128(%rbp), %r8
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1127:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1126
.L1216:
	movq	(%r12), %rax
	movq	%rdx, -120(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1130
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1131:
	cmpl	$1, %eax
	jne	.L1126
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1066:
	addl	$1, 8(%r14)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1084:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	152(%r12), %r13
	testq	%r13, %r13
	je	.L1218
.L1020:
	leaq	-80(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector19MainThreadInterface9GetHandleEv@PLT
	movl	$224, %edi
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm1
	movq	-72(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	movaps	%xmm0, -80(%rbp)
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r13)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, 0(%r13)
	movups	%xmm0, 16(%r13)
	movups	%xmm1, 32(%r13)
	testq	%r14, %r14
	je	.L1036
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1037
	lock addl	$1, 8(%r14)
.L1036:
	leaq	96(%r13), %rax
	movss	.LC7(%rip), %xmm0
	movq	$1, 56(%r13)
	movq	%rax, 48(%r13)
	leaq	152(%r13), %rax
	movq	%rax, 104(%r13)
	leaq	208(%r13), %rax
	movq	$0, 64(%r13)
	movq	$0, 72(%r13)
	movq	$0, 88(%r13)
	movq	$0, 96(%r13)
	movq	$1, 112(%r13)
	movq	$0, 120(%r13)
	movq	$0, 128(%r13)
	movq	$0, 144(%r13)
	movq	$0, 152(%r13)
	movq	%rax, 160(%r13)
	movq	$1, 168(%r13)
	movq	$0, 176(%r13)
	movq	$0, 184(%r13)
	movq	$0, 200(%r13)
	movq	$0, 208(%r13)
	movl	$0, 216(%r13)
	movss	%xmm0, 80(%r13)
	movss	%xmm0, 136(%r13)
	movss	%xmm0, 192(%r13)
	testq	%r14, %r14
	je	.L1043
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1039
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L1040:
	cmpl	$1, %eax
	je	.L1219
.L1043:
	leaq	16(%r13), %rax
	movq	%rax, %xmm0
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1044
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1045
.L1044:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movq	%xmm0, 16(%r13)
	testq	%rdx, %rdx
	je	.L1220
	lock addl	$1, 12(%r13)
.L1046:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1048
	testq	%rdx, %rdx
	je	.L1049
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1050:
	cmpl	$1, %eax
	jne	.L1048
	movq	(%rdi), %rax
	movq	%xmm0, -120(%rbp)
	call	*24(%rax)
	movq	-120(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	%r13, 24(%r13)
.L1045:
	movq	176(%r12), %r14
	movq	%r13, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 168(%r12)
	testq	%r14, %r14
	je	.L1053
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1054
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L1055:
	cmpl	$1, %eax
	je	.L1221
.L1053:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L1060
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1061
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1062:
	cmpl	$1, %eax
	je	.L1222
.L1060:
	movq	168(%r12), %rax
	movq	%rax, -120(%rbp)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	8(%rdi), %rax
	movl	$384, %edi
	movq	360(%rax), %rdx
	movq	352(%rax), %rcx
	movq	2080(%rax), %r15
	movq	2392(%rdx), %r8
	movq	2360(%rdx), %rdx
	movq	%rcx, -128(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -120(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %r14
	movq	-136(%rbp), %r8
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r14), %r13
	movq	%rax, (%r14)
	movq	%r13, %rdi
	call	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE@PLT
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L1003
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1004
.L1003:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movq	%r13, 16(%r14)
	testq	%rdx, %rdx
	je	.L1223
	lock addl	$1, 12(%r14)
.L1005:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1007
	testq	%rdx, %rdx
	je	.L1008
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1009:
	cmpl	$1, %eax
	jne	.L1007
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	%r14, 24(%r14)
.L1004:
	movq	160(%r12), %r15
	movq	%r13, %xmm0
	movq	%r14, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 152(%r12)
	testq	%r15, %r15
	je	.L1002
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1012
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L1013:
	cmpl	$1, %eax
	je	.L1014
.L1209:
	movq	152(%r12), %r13
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1081:
	addl	$1, 12(%r14)
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1130:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1115:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1122:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	%r9, %rdi
	call	*%rax
	movq	32(%rbx), %r8
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1099:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1091:
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1037:
	addl	$1, 8(%r14)
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1094:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1107:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	(%r15), %rax
	movq	%rdx, -120(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1015
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1016:
	cmpl	$1, %eax
	jne	.L1209
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1223:
	addl	$1, 12(%r14)
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1054:
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1061:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	0(%r13), %rax
	movq	%rdx, -120(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1064
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1065:
	cmpl	$1, %eax
	jne	.L1060
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	(%r14), %rax
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1041
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1042:
	cmpl	$1, %eax
	jne	.L1043
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	(%r14), %rax
	movq	%rdx, -120(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1057
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1058:
	cmpl	$1, %eax
	jne	.L1053
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	8(%r12), %rax
	movl	$384, %edi
	movq	360(%rax), %rdx
	movq	352(%rax), %rcx
	movq	2080(%rax), %r15
	movq	2392(%rdx), %r8
	movq	2360(%rdx), %rdx
	movq	%rcx, -128(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -120(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rax, %r14
	movq	-136(%rbp), %r8
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r14), %r13
	movq	%rax, (%r14)
	movq	%r13, %rdi
	call	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE@PLT
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L1021
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L1022
.L1021:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movq	%r13, 16(%r14)
	testq	%rdx, %rdx
	je	.L1224
	lock addl	$1, 12(%r14)
.L1023:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1025
	testq	%rdx, %rdx
	je	.L1026
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1027:
	cmpl	$1, %eax
	jne	.L1025
	movq	(%rdi), %rax
	call	*24(%rax)
.L1025:
	movq	%r14, 24(%r14)
.L1022:
	movq	160(%r12), %r15
	movq	%r13, %xmm0
	movq	%r14, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 152(%r12)
	testq	%r15, %r15
	je	.L1020
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1030
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L1031:
	cmpl	$1, %eax
	je	.L1032
.L1210:
	movq	152(%r12), %r13
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1220:
	addl	$1, 12(%r13)
	jmp	.L1046
.L1049:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1050
.L1057:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1058
.L1064:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1065
.L1041:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1042
.L1030:
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L1031
.L1032:
	movq	(%r15), %rax
	movq	%rdx, -120(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1033
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1034:
	cmpl	$1, %eax
	jne	.L1210
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1210
.L1224:
	addl	$1, 12(%r14)
	jmp	.L1023
.L1026:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1027
.L1033:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1034
.L1217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8571:
	.size	_ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, .-_ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5AgentC2EPNS_11EnvironmentE
	.type	_ZN4node9inspector5AgentC2EPNS_11EnvironmentE, @function
_ZN4node9inspector5AgentC2EPNS_11EnvironmentE:
.LFB8629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 56(%rdi)
	leaq	56(%rdi), %rax
	movq	1640(%rsi), %r13
	movq	$0, 16(%rdi)
	movq	1648(%rsi), %r15
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	$0, 48(%rdi)
	testq	%r15, %r15
	je	.L1226
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1227
	lock addl	$1, 8(%r15)
.L1226:
	leaq	16+_ZTVN4node12DebugOptionsE(%rip), %rax
	leaq	104(%rbx), %rdi
	movq	696(%r13), %r14
	movq	%rax, 72(%rbx)
	movl	680(%r13), %eax
	movq	%rdi, 88(%rbx)
	movq	688(%r13), %r8
	movl	%eax, 80(%rbx)
	movq	%r8, %rax
	addq	%r14, %rax
	je	.L1228
	testq	%r8, %r8
	je	.L1233
.L1228:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L1272
	cmpq	$1, %r14
	jne	.L1231
	movzbl	(%r8), %eax
	movb	%al, 104(%rbx)
.L1232:
	movq	%r14, 96(%rbx)
	movb	$0, (%rdi,%r14)
	leaq	144(%rbx), %rdi
	movzwl	720(%r13), %eax
	movq	%rdi, 128(%rbx)
	movq	728(%r13), %r8
	movq	736(%r13), %r14
	movw	%ax, 120(%rbx)
	movq	%r8, %rax
	addq	%r14, %rax
	je	.L1249
	testq	%r8, %r8
	je	.L1233
.L1249:
	movq	%r14, -64(%rbp)
	cmpq	$15, %r14
	ja	.L1273
	cmpq	$1, %r14
	jne	.L1237
	movzbl	(%r8), %eax
	movb	%al, 144(%rbx)
.L1238:
	movq	%r14, 136(%rbx)
	movb	$0, (%rdi,%r14)
	movl	760(%r13), %eax
	movl	%eax, 160(%rbx)
	testq	%r15, %r15
	je	.L1240
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1241
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L1274
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	1656(%r12), %rax
	movq	%rax, 168(%rbx)
	movq	1664(%r12), %rax
	movq	%rax, 176(%rbx)
	testq	%rax, %rax
	je	.L1246
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1247
	lock addl	$1, 8(%rax)
.L1246:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movw	%ax, 184(%rbx)
	movups	%xmm0, 192(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1275
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1231:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L1232
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1227:
	addl	$1, 8(%r15)
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1237:
	testq	%r14, %r14
	je	.L1238
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1272:
	leaq	88(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 88(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 104(%rbx)
.L1230:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	88(%rbx), %rdi
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1273:
	leaq	128(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 128(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 144(%rbx)
.L1236:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r14
	movq	128(%rbx), %rdi
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1247:
	addl	$1, 8(%rax)
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1241:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L1240
.L1274:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1244
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1245:
	cmpl	$1, %eax
	jne	.L1240
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1244:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1245
.L1233:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8629:
	.size	_ZN4node9inspector5AgentC2EPNS_11EnvironmentE, .-_ZN4node9inspector5AgentC2EPNS_11EnvironmentE
	.globl	_ZN4node9inspector5AgentC1EPNS_11EnvironmentE
	.set	_ZN4node9inspector5AgentC1EPNS_11EnvironmentE,_ZN4node9inspector5AgentC2EPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5AgentD2Ev
	.type	_ZN4node9inspector5AgentD2Ev, @function
_ZN4node9inspector5AgentD2Ev:
.LFB8632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	200(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1277
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1277:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1278
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1278:
	movq	176(%rbx), %r12
	testq	%r12, %r12
	je	.L1280
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1281
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1311
	.p2align 4,,10
	.p2align 3
.L1280:
	leaq	16+_ZTVN4node12DebugOptionsE(%rip), %rax
	movq	128(%rbx), %rdi
	movq	%rax, 72(%rbx)
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1286
	call	_ZdlPv@PLT
.L1286:
	movq	88(%rbx), %rdi
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1287
	call	_ZdlPv@PLT
.L1287:
	movq	40(%rbx), %rdi
	leaq	56(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1288
	call	_ZdlPv@PLT
.L1288:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L1289
	movq	%r12, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1289:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L1290
	movq	%r12, %rdi
	call	_ZN4node9inspector11InspectorIoD1Ev@PLT
	movl	$216, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1290:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L1276
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1293
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1312
.L1276:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1281:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1280
.L1311:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1284
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1285:
	cmpl	$1, %eax
	jne	.L1280
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1293:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1276
.L1312:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1296
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1297:
	cmpl	$1, %eax
	jne	.L1276
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1284:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1296:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1297
	.cfi_endproc
.LFE8632:
	.size	_ZN4node9inspector5AgentD2Ev, .-_ZN4node9inspector5AgentD2Ev
	.globl	_ZN4node9inspector5AgentD1Ev
	.set	_ZN4node9inspector5AgentD1Ev,_ZN4node9inspector5AgentD2Ev
	.section	.rodata.str1.1
.LC8:
	.string	"Worker["
.LC9:
	.string	"]"
.LC10:
	.string	"node[%u]: pthread_create: %s\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEb
	.type	_ZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEb, @function
_ZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEb:
.LFB8634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$792, %rsp
	movl	%r8d, -760(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movzbl	8(%r12), %eax
	leaq	16(%r12), %rsi
	leaq	88(%rbx), %rdi
	movb	%al, 80(%rbx)
	movzbl	9(%r12), %eax
	movb	%al, 81(%rbx)
	movzbl	10(%r12), %eax
	movb	%al, 82(%rbx)
	movzbl	11(%r12), %eax
	movb	%al, 83(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movzwl	48(%r12), %eax
	leaq	56(%r12), %rsi
	leaq	128(%rbx), %rdi
	movw	%ax, 120(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	88(%r12), %eax
	movl	%eax, 160(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1508
	movq	8(%r13), %r15
	movq	176(%rbx), %r13
	movq	%rax, 168(%rbx)
	cmpq	%r13, %r15
	je	.L1315
	testq	%r15, %r15
	je	.L1316
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1317
	lock addl	$1, 8(%r15)
	movq	176(%rbx), %r13
.L1316:
	testq	%r13, %r13
	je	.L1319
.L1532:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1320
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1321:
	cmpl	$1, %eax
	jne	.L1319
	movq	0(%r13), %rax
	movq	%rdx, -768(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-768(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1323
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1324:
	cmpl	$1, %eax
	je	.L1509
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	%r15, 176(%rbx)
.L1315:
	movl	$200, %edi
	call	_Znwm@PLT
	movss	.LC7(%rip), %xmm0
	movq	(%rbx), %r13
	xorl	%edi, %edi
	movq	%rax, %r14
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r14), %rdx
	movq	%rax, (%r14)
	leaq	16+_ZTVN4node9inspector19NodeInspectorClientE(%rip), %rax
	movq	%rax, 16(%r14)
	movzbl	-760(%rbp), %eax
	movw	%di, 164(%r14)
	movb	%al, 32(%r14)
	leaq	96(%r14), %rax
	movq	%rax, 48(%r14)
	leaq	152(%r14), %rax
	movq	%rax, 104(%r14)
	leaq	-720(%rbp), %rax
	movss	%xmm0, 80(%r14)
	movq	%rax, %rdi
	movss	%xmm0, 136(%r14)
	pxor	%xmm0, %xmm0
	movq	%r13, 24(%r14)
	movb	$0, 33(%r14)
	movq	$0, 40(%r14)
	movq	$1, 56(%r14)
	movq	$0, 64(%r14)
	movq	$0, 72(%r14)
	movq	$0, 88(%r14)
	movq	$0, 96(%r14)
	movq	$1, 112(%r14)
	movq	$0, 120(%r14)
	movq	$0, 128(%r14)
	movq	$0, 144(%r14)
	movq	$0, 152(%r14)
	movl	$1, 160(%r14)
	movb	$0, 166(%r14)
	movups	%xmm0, 168(%r14)
	movups	%xmm0, 184(%r14)
	movq	352(%r13), %rsi
	movq	%rax, -768(%rbp)
	movq	%rdx, -808(%rbp)
	call	_ZN12v8_inspector11V8Inspector6createEPN2v87IsolateEPNS_17V8InspectorClientE@PLT
	movq	-720(%rbp), %rax
	movq	40(%r14), %rdi
	movq	$0, -720(%rbp)
	movq	%rax, 40(%r14)
	testq	%rdi, %rdi
	je	.L1326
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1326
	movq	(%rdi), %rax
	call	*8(%rax)
.L1326:
	cmpb	$0, 32(%r14)
	je	.L1328
	leaq	-624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN4node27GetHumanReadableProcessNameB5cxx11Ev@PLT
	leaq	-432(%rbp), %rax
	movq	%rax, -760(%rbp)
.L1329:
	movq	-624(%rbp), %r8
	leaq	-416(%rbp), %rax
	movq	-616(%rbp), %r15
	movq	%rax, -792(%rbp)
	movq	%rax, -432(%rbp)
	movq	%r8, %rax
	addq	%r15, %rax
	setne	%dl
	testq	%r8, %r8
	sete	%al
	andb	%al, %dl
	movb	%dl, -800(%rbp)
	jne	.L1510
	movq	%r15, -720(%rbp)
	cmpq	$15, %r15
	ja	.L1511
	cmpq	$1, %r15
	jne	.L1337
	movzbl	(%r8), %eax
	movb	%al, -416(%rbp)
	movq	-792(%rbp), %rax
.L1338:
	movq	%r15, -424(%rbp)
	movq	-760(%rbp), %rsi
	leaq	-744(%rbp), %rdi
	movb	$0, (%rax,%r15)
	leaq	-384(%rbp), %rax
	movq	%rax, -816(%rbp)
	movq	%rax, -400(%rbp)
	movb	$0, -384(%rbp)
	movq	3280(%r13), %r13
	movq	$0, -392(%rbp)
	movb	$1, -368(%rbp)
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	-400(%rbp), %rsi
	leaq	-736(%rbp), %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-744(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-736(%rbp), %rdi
	movq	%r13, -720(%rbp)
	leaq	-560(%rbp), %r13
	movl	$1, -712(%rbp)
	movdqu	(%rax), %xmm2
	movaps	%xmm2, -704(%rbp)
	movq	16(%rax), %rax
	movb	$1, -680(%rbp)
	movq	%rax, -688(%rbp)
	movq	(%rdi), %rax
	movq	$0, -672(%rbp)
	movq	$0, -664(%rbp)
	movb	$1, -656(%rbp)
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movb	$0, -632(%rbp)
	call	*16(%rax)
	leaq	-544(%rbp), %rcx
	cmpb	$0, -368(%rbp)
	movdqu	(%rax), %xmm3
	movq	%rcx, -824(%rbp)
	movups	%xmm3, -680(%rbp)
	movq	16(%rax), %rax
	movq	%rcx, -560(%rbp)
	movq	%rax, -664(%rbp)
	je	.L1339
	leaq	-728(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	$18, -728(%rbp)
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-728(%rbp), %rdx
	movl	$32101, %ecx
	movdqa	.LC11(%rip), %xmm0
	movq	%rax, -560(%rbp)
	movq	%rdx, -544(%rbp)
	movw	%cx, 16(%rax)
	movups	%xmm0, (%rax)
.L1507:
	movq	-728(%rbp), %rax
	movq	-560(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, -552(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-560(%rbp), %rdi
	movq	-824(%rbp), %rcx
	movq	-728(%rbp), %r15
	cmpq	%rcx, %rdi
	je	.L1341
	call	_ZdlPv@PLT
.L1341:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movq	40(%r14), %rdi
	movq	-768(%rbp), %rsi
	movdqu	(%rax), %xmm4
	movaps	%xmm4, -656(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -640(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1343
	movq	(%rdi), %rax
	call	*8(%rax)
.L1343:
	movq	-744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1344
	movq	(%rdi), %rax
	call	*8(%rax)
.L1344:
	movq	-400(%rbp), %rdi
	cmpq	-816(%rbp), %rdi
	je	.L1345
	call	_ZdlPv@PLT
.L1345:
	movq	-432(%rbp), %rdi
	cmpq	-792(%rbp), %rdi
	je	.L1346
	call	_ZdlPv@PLT
.L1346:
	movq	-624(%rbp), %rdi
	leaq	-608(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1347
	call	_ZdlPv@PLT
.L1347:
	movq	-808(%rbp), %xmm0
	movq	16(%rbx), %r15
	movq	%r14, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%r15, %r15
	je	.L1349
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1350
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L1512
	.p2align 4,,10
	.p2align 3
.L1349:
	movq	(%rbx), %rdi
	testb	$4, 1932(%rdi)
	je	.L1355
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movl	$1, %eax
	xchgb	_ZN4node9inspector12_GLOBAL__N_1L33start_io_thread_async_initializedE(%rip), %al
	testb	%al, %al
	jne	.L1513
	movq	(%rbx), %rax
	leaq	_ZN4node9inspector12_GLOBAL__N_126StartIoThreadAsyncCallbackEP10uv_async_s(%rip), %rdx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip), %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L1514
	leaq	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip), %rdi
	call	uv_unref@PLT
	xorl	%esi, %esi
	leaq	_ZN4node9inspector12_GLOBAL__N_1L25start_io_thread_semaphoreE(%rip), %rdi
	movq	%rbx, _ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip)
	call	uv_sem_init@PLT
	testl	%eax, %eax
	jne	.L1515
	movq	-784(%rbp), %rdi
	call	pthread_attr_init@PLT
	testl	%eax, %eax
	jne	.L1516
	movq	-784(%rbp), %rdi
	movl	$32768, %esi
	call	pthread_attr_setstacksize@PLT
	testl	%eax, %eax
	jne	.L1517
	movq	-784(%rbp), %rdi
	movl	$1, %esi
	call	pthread_attr_setdetachstate@PLT
	testl	%eax, %eax
	jne	.L1518
	movq	%r13, %rdi
	call	sigfillset@PLT
	movq	-760(%rbp), %rdx
	movq	%r13, %rsi
	movl	$2, %edi
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L1519
	movq	-784(%rbp), %rsi
	movq	-768(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPv(%rip), %rdx
	movdqa	-432(%rbp), %xmm6
	movdqa	-416(%rbp), %xmm7
	movaps	%xmm6, -560(%rbp)
	movdqa	-400(%rbp), %xmm6
	movaps	%xmm7, -544(%rbp)
	movdqa	-384(%rbp), %xmm7
	movaps	%xmm6, -528(%rbp)
	movdqa	-368(%rbp), %xmm6
	movaps	%xmm7, -512(%rbp)
	movdqa	-352(%rbp), %xmm7
	movaps	%xmm6, -496(%rbp)
	movdqa	-336(%rbp), %xmm6
	movaps	%xmm7, -480(%rbp)
	movdqa	-320(%rbp), %xmm7
	movaps	%xmm6, -464(%rbp)
	movaps	%xmm7, -448(%rbp)
	call	pthread_create@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$2, %edi
	movl	%eax, %r14d
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L1520
	movq	-784(%rbp), %rdi
	call	pthread_attr_destroy@PLT
	testl	%eax, %eax
	jne	.L1521
	testl	%r14d, %r14d
	jne	.L1522
	xorl	%edx, %edx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L19StartIoThreadWakeupEi(%rip), %rsi
	movl	$10, %edi
	call	_ZN4node21RegisterSignalHandlerEiPFviEb@PLT
	movq	%r13, %rdi
	call	sigemptyset@PLT
	movl	$10, %esi
	movq	%r13, %rdi
	call	sigaddset@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$1, %edi
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L1523
.L1366:
	movq	(%rbx), %r13
	movl	$40, %edi
	movq	2640(%r13), %r15
	leaq	1(%r15), %rax
	movq	%rax, 2640(%r13)
	call	_Znwm@PLT
	leaq	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE_4_FUNESL_(%rip), %r10
	xorl	%edx, %edx
	movq	%r15, 24(%rax)
	movq	%rax, %r14
	movq	%r10, 8(%rax)
	movq	%r13, 16(%rax)
	movq	2592(%r13), %r8
	movq	$0, (%rax)
	movq	%r13, %rax
	divq	%r8
	movq	2584(%r13), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%rdi, %rdi
	je	.L1367
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1367
	movq	32(%rsi), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1367
	movq	%rsi, %rax
.L1370:
	cmpq	%rcx, %r13
	jne	.L1368
	cmpq	%r10, 8(%rax)
	jne	.L1368
	cmpq	16(%rax), %r13
	jne	.L1368
	cmpq	$0, (%rdi)
	je	.L1367
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	.LC13(%rip), %xmm1
	leaq	-320(%rbp), %r15
	leaq	-432(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -760(%rbp)
	movhps	.LC14(%rip), %xmm1
	movaps	%xmm1, -784(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rcx, -320(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%si, -96(%rbp)
	movq	-760(%rbp), %rdi
	xorl	%esi, %esi
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	addq	-24(%rcx), %rdi
	movq	%rcx, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	$0, -104(%rbp)
	movq	%rcx, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-784(%rbp), %xmm1
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rcx, -792(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r15, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rcx, -424(%rbp)
	leaq	-336(%rbp), %rcx
	movq	%rcx, -800(%rbp)
	movq	%rcx, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-760(%rbp), %rdi
	movl	$7, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	1936(%r13), %rsi
	movq	-760(%rbp), %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-608(%rbp), %rax
	leaq	-624(%rbp), %rdi
	movq	$0, -616(%rbp)
	movq	%rax, -624(%rbp)
	movq	-384(%rbp), %rax
	movq	%rdi, -784(%rbp)
	movb	$0, -608(%rbp)
	testq	%rax, %rax
	je	.L1330
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1524
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1332:
	movq	.LC13(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC15(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-800(%rbp), %rdi
	je	.L1333
	call	_ZdlPv@PLT
.L1333:
	movq	-792(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r15, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	2608(%r13), %rdx
	movq	%r8, %rsi
	movl	$1, %ecx
	leaq	2616(%r13), %rdi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L1371
	movq	2584(%r13), %r9
.L1372:
	addq	%r9, %r15
	movq	%r13, 32(%r14)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L1381
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	(%r15), %rax
	movq	%r14, (%rax)
.L1382:
	addq	$1, 2608(%r13)
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	(%rbx), %rdi
.L1355:
	movq	%rdi, %rdx
	leaq	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE0_4_FUNESL_(%rip), %rsi
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movzbl	10(%r12), %r13d
	testb	%r13b, %r13b
	jne	.L1384
	movzbl	11(%r12), %r13d
.L1384:
	movq	32(%rbx), %r14
	testq	%r14, %r14
	je	.L1385
	movq	8(%rbx), %r15
	movzbl	56(%r14), %r9d
	movq	152(%r15), %r12
	movl	%r9d, %r13d
	testq	%r12, %r12
	je	.L1525
.L1386:
	movq	-768(%rbp), %r15
	movq	%r12, %rsi
	movl	%r9d, -760(%rbp)
	movq	%r15, %rdi
	call	_ZN4node9inspector19MainThreadInterface9GetHandleEv@PLT
	movl	-760(%rbp), %r9d
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r9d, %edx
	call	_ZN4node9inspector21ParentInspectorHandle13WorkerStartedESt10shared_ptrINS0_16MainThreadHandleEEb@PLT
	movq	-712(%rbp), %r12
	testq	%r12, %r12
	je	.L1409
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1404
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1405:
	cmpl	$1, %eax
	jne	.L1409
	movq	(%r12), %rax
	movq	%rdx, -760(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-760(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1407
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1408:
	cmpl	$1, %eax
	jne	.L1409
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1409:
	testb	%r13b, %r13b
	je	.L1425
	movq	(%rbx), %rax
	cmpb	$0, 1929(%rax)
	jne	.L1526
	movq	1648(%rax), %r12
	movb	$1, 80(%rbx)
	movb	$1, 82(%rbx)
	movq	1640(%rax), %rcx
	testq	%r12, %r12
	je	.L1416
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	leaq	8(%r12), %rax
	testq	%rdx, %rdx
	je	.L1417
	lock addl	$1, (%rax)
.L1418:
	movb	$1, 680(%rcx)
	movb	$1, 682(%rcx)
	testq	%rdx, %rdx
	je	.L1527
	movl	$-1, %ecx
	lock xaddl	%ecx, (%rax)
	movl	%ecx, %eax
.L1419:
	cmpl	$1, %eax
	je	.L1528
.L1421:
	movq	8(%rbx), %rbx
	cmpb	$0, 17(%rbx)
	movb	$1, 149(%rbx)
	jne	.L1425
	movq	8(%rbx), %rax
	movb	$1, 17(%rbx)
	movq	360(%rax), %rax
	movq	2392(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1431
	call	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv@PLT
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	movq	(%r12), %rax
	call	*168(%rax)
	testb	%al, %al
	jne	.L1431
	cmpb	$0, 149(%rbx)
	jne	.L1477
	cmpb	$0, 150(%rbx)
	jne	.L1427
	cmpb	$0, 148(%rbx)
	jne	.L1427
.L1428:
	movb	$0, 17(%rbx)
	movb	%r13b, -800(%rbp)
.L1313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1529
	movzbl	-800(%rbp), %eax
	addq	$792, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1339:
	.cfi_restore_state
	leaq	-728(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	$19, -728(%rbp)
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-728(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movq	%rax, -560(%rbp)
	movq	%rdx, -544(%rbp)
	movl	$25971, %edx
	movw	%dx, 16(%rax)
	movb	$125, 18(%rax)
	movups	%xmm0, (%rax)
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1337:
	testq	%r15, %r15
	jne	.L1530
	movq	-792(%rbp), %rax
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	-760(%rbp), %rdi
	movq	-768(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -816(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-816(%rbp), %r8
	movq	%rax, -432(%rbp)
	movq	%rax, %rdi
	movq	-720(%rbp), %rax
	movq	%rax, -416(%rbp)
.L1336:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-720(%rbp), %r15
	movq	-432(%rbp), %rax
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1385:
	cmpb	$0, 8(%r12)
	je	.L1313
	cmpq	$0, 24(%rbx)
	jne	.L1409
	cmpq	$0, 8(%rbx)
	je	.L1531
	movq	%rbx, %rdi
	call	_ZN4node9inspector5Agent13StartIoThreadEv.part.0
	testb	%al, %al
	je	.L1313
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1425:
	movb	$1, -800(%rbp)
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1350:
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	cmpl	$1, %eax
	jne	.L1349
.L1512:
	movq	(%r15), %rax
	movq	%rdx, -792(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-792(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1353
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1354:
	cmpl	$1, %eax
	jne	.L1349
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1317:
	addl	$1, 8(%r15)
	testq	%r13, %r13
	jne	.L1532
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1524:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1320:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	48(%rbx), %rax
	testq	%rax, %rax
	jne	.L1429
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1428
.L1429:
	movq	16(%rax), %rdx
	cmpb	$0, 64(%rdx)
	je	.L1533
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1508:
	leaq	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L1405
	.p2align 4,,10
	.p2align 3
.L1330:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1353:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1417:
	addl	$1, 8(%r12)
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1522:
	movl	%r14d, %edi
	call	strerror@PLT
	movq	%rax, %r13
	call	uv_os_getpid@PLT
	movq	stderr(%rip), %rdi
	movq	%r13, %r8
	leaq	.LC10(%rip), %rdx
	movl	%eax, %ecx
	movl	$1, %esi
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	8(%r15), %rax
	movl	$384, %edi
	movl	%r9d, -816(%rbp)
	movq	360(%rax), %rdx
	movq	352(%rax), %rcx
	movq	2080(%rax), %rsi
	movq	2392(%rdx), %r8
	movq	2360(%rdx), %rdx
	movq	%rcx, -800(%rbp)
	movq	%rsi, -784(%rbp)
	movq	%r8, -808(%rbp)
	movq	%rdx, -792(%rbp)
	call	_Znwm@PLT
	movq	-792(%rbp), %rdx
	movabsq	$4294967297, %rcx
	movq	-808(%rbp), %r8
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	leaq	16(%rax), %r12
	movq	-784(%rbp), %rsi
	movq	%rcx, (%rax)
	movq	-800(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, -760(%rbp)
	call	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE@PLT
	movq	-760(%rbp), %rax
	movl	-816(%rbp), %r9d
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1387
	movl	8(%rdx), %edx
	testl	%edx, %edx
	jne	.L1388
.L1387:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movq	%r12, 16(%rax)
	testq	%rdx, %rdx
	je	.L1534
	lock addl	$1, 12(%rax)
.L1389:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1391
	testq	%rdx, %rdx
	je	.L1392
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L1393:
	cmpl	$1, %edx
	jne	.L1391
	movq	(%rdi), %rdx
	movq	%rax, -784(%rbp)
	movl	%r9d, -760(%rbp)
	call	*24(%rdx)
	movq	-784(%rbp), %rax
	movl	-760(%rbp), %r9d
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	%rax, 24(%rax)
.L1388:
	movq	160(%r15), %rdi
	movq	%r12, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 152(%r15)
	testq	%rdi, %rdi
	je	.L1386
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1396
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L1397:
	cmpl	$1, %eax
	je	.L1398
.L1505:
	movq	152(%r15), %r12
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1416:
	movb	$1, 680(%rcx)
	movb	$1, 682(%rcx)
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1323:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1509:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1407:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1520:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1526:
	leaq	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1527:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1513:
	leaq	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1514:
	leaq	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	(%r12), %rax
	movq	%rdx, -760(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-760(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1422
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1423:
	cmpl	$1, %eax
	jne	.L1421
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1516:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1371:
	cmpq	$1, %rdx
	je	.L1535
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1536
	leaq	0(,%rdx,8), %r15
	movq	%rdx, -760(%rbp)
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	leaq	2632(%r13), %r15
	movq	%rax, %rdi
	call	memset@PLT
	movq	-760(%rbp), %r8
	movq	%rax, %r9
.L1374:
	movq	2600(%r13), %rsi
	movq	$0, 2600(%r13)
	testq	%rsi, %rsi
	je	.L1376
	xorl	%edi, %edi
	leaq	2600(%r13), %r11
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1379:
	testq	%rsi, %rsi
	je	.L1376
.L1377:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L1378
	movq	2600(%r13), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 2600(%r13)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L1440
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1376:
	movq	2584(%r13), %rdi
	cmpq	%rdi, %r15
	je	.L1380
	movq	%r8, -784(%rbp)
	movq	%r9, -760(%rbp)
	call	_ZdlPv@PLT
	movq	-784(%rbp), %r8
	movq	-760(%rbp), %r9
.L1380:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r8, 2592(%r13)
	divq	%r8
	movq	%r9, 2584(%r13)
	leaq	0(,%rdx,8), %r15
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1517:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	2600(%r13), %rax
	movq	%rax, (%r14)
	movq	%r14, 2600(%r13)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L1383
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r13)
	movq	%r14, (%r9,%rdx,8)
.L1383:
	leaq	2600(%r13), %rax
	movq	%rax, (%r15)
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1518:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1519:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	%rdx, %rdi
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1521:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1422:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1396:
	movl	8(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1523:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	(%rdi), %rax
	movq	%rdx, -792(%rbp)
	movl	%r9d, -784(%rbp)
	movq	%rdi, -760(%rbp)
	call	*16(%rax)
	movq	-792(%rbp), %rdx
	movq	-760(%rbp), %rdi
	movl	-784(%rbp), %r9d
	testq	%rdx, %rdx
	je	.L1399
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1400:
	cmpl	$1, %eax
	jne	.L1505
	movq	(%rdi), %rax
	movl	%r9d, -760(%rbp)
	call	*24(%rax)
	movq	152(%r15), %r12
	movl	-760(%rbp), %r9d
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1534:
	addl	$1, 12(%rax)
	jmp	.L1389
.L1531:
	leaq	_ZZN4node9inspector5Agent13StartIoThreadEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1392:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L1393
.L1399:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1400
.L1535:
	leaq	2632(%r13), %r9
	movq	$0, 2632(%r13)
	movq	%r9, %r15
	jmp	.L1374
.L1529:
	call	__stack_chk_fail@PLT
.L1510:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1536:
	call	_ZSt17__throw_bad_allocv@PLT
.L1530:
	movq	-792(%rbp), %rdi
	jmp	.L1336
	.cfi_endproc
.LFE8634:
	.size	_ZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEb, .-_ZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent13StartIoThreadEv
	.type	_ZN4node9inspector5Agent13StartIoThreadEv, @function
_ZN4node9inspector5Agent13StartIoThreadEv:
.LFB8649:
	.cfi_startproc
	endbr64
	cmpq	$0, 24(%rdi)
	je	.L1543
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1543:
	cmpq	$0, 8(%rdi)
	je	.L1544
	jmp	_ZN4node9inspector5Agent13StartIoThreadEv.part.0
	.p2align 4,,10
	.p2align 3
.L1544:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector5Agent13StartIoThreadEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8649:
	.size	_ZN4node9inspector5Agent13StartIoThreadEv, .-_ZN4node9inspector5Agent13StartIoThreadEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent4StopEv
	.type	_ZN4node9inspector5Agent4StopEv, @function
_ZN4node9inspector5Agent4StopEv:
.LFB8650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	24(%rdi), %r12
	movq	$0, 24(%rdi)
	testq	%r12, %r12
	je	.L1545
	movq	%r12, %rdi
	call	_ZN4node9inspector11InspectorIoD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$216, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1545:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8650:
	.size	_ZN4node9inspector5Agent4StopEv, .-_ZN4node9inspector5Agent4StopEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.type	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, @function
_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb:
.LFB8651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1572
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	movq	%rsi, %r12
	movzbl	%cl, %edx
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node9inspector19NodeInspectorClient15connectFrontendESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	movq	-64(%rbp), %rdi
	movl	%eax, %ebx
	testq	%rdi, %rdi
	je	.L1550
	movq	(%rdi), %rax
	call	*8(%rax)
.L1550:
	movq	8(%r12), %rdx
	movq	16(%r12), %r12
	testq	%r12, %r12
	je	.L1551
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	8(%r12), %r15
	testq	%r14, %r14
	je	.L1552
	lock addl	$1, (%r15)
.L1553:
	movq	%rdx, %xmm0
	movq	%r12, %xmm1
	movl	$32, %edi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm0
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionE(%rip), %rcx
	movl	%ebx, 8(%rax)
	leaq	12(%r12), %rbx
	movq	%rcx, (%rax)
	movups	%xmm0, 16(%rax)
	testq	%r14, %r14
	je	.L1573
	lock addl	$1, (%rbx)
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1574
.L1561:
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
	cmpl	$1, %eax
	je	.L1575
.L1548:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1576
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1552:
	.cfi_restore_state
	addl	$1, 8(%r12)
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1573:
	addl	$1, 12(%r12)
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	jne	.L1561
.L1574:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1548
.L1575:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L1558
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
.L1559:
	cmpl	$1, %eax
	jne	.L1548
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1551:
	movl	$32, %edi
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionE(%rip), %rsi
	movq	%rsi, (%rax)
	movl	%ebx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rax, 0(%r13)
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1572:
	leaq	_ZZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1558:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1559
.L1576:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8651:
	.size	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, .-_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.type	_ZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, @function
_ZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb:
.LFB8658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rsi)
	je	.L1630
	movq	8(%rsi), %r15
	movq	%rsi, %rbx
	testq	%r15, %r15
	je	.L1631
	movq	152(%r15), %r9
	movq	%rdi, %r14
	movq	%rdx, %r13
	movl	%ecx, %r12d
	testq	%r9, %r9
	je	.L1632
.L1580:
	leaq	-80(%rbp), %r15
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN4node9inspector19MainThreadInterface9GetHandleEv@PLT
	movq	0(%r13), %rax
	movq	-80(%rbp), %rsi
	movq	$0, 0(%r13)
	leaq	-96(%rbp), %rdi
	leaq	-88(%rbp), %rdx
	movq	%rax, -88(%rbp)
	call	_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1596
	movq	(%rdi), %rax
	call	*8(%rax)
.L1596:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L1598
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1599
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1633
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	-96(%rbp), %rax
	movq	32(%rbx), %rsi
	movq	%r14, %rdi
	movzbl	%r12b, %ecx
	movq	%r15, %rdx
	movq	$0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN4node9inspector21ParentInspectorHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1604
	movq	(%rdi), %rax
	call	*8(%rax)
.L1604:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1577
	movq	(%rdi), %rax
	call	*8(%rax)
.L1577:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1634
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1599:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1598
.L1633:
	movq	0(%r13), %rax
	movq	%rdx, -104(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1602
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1603:
	cmpl	$1, %eax
	jne	.L1598
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	8(%r15), %rax
	movl	$384, %edi
	movq	360(%rax), %rdx
	movq	352(%rax), %rcx
	movq	2080(%rax), %rsi
	movq	2392(%rdx), %r8
	movq	2360(%rdx), %rdx
	movq	%rcx, -128(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdx, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	leaq	16(%rax), %r9
	movq	-136(%rbp), %r8
	movq	%rcx, (%rax)
	movq	-128(%rbp), %rcx
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE@PLT
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r9
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1581
	movl	8(%rdx), %edx
	testl	%edx, %edx
	je	.L1581
.L1582:
	movq	160(%r15), %rdi
	movq	%r9, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 152(%r15)
	testq	%rdi, %rdi
	je	.L1580
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1590
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L1591:
	cmpl	$1, %eax
	je	.L1592
.L1629:
	movq	152(%r15), %r9
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movq	%r9, 16(%rax)
	testq	%rdx, %rdx
	je	.L1635
	lock addl	$1, 12(%rax)
.L1583:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1585
	testq	%rdx, %rdx
	je	.L1586
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L1587:
	cmpl	$1, %edx
	jne	.L1585
	movq	(%rdi), %rdx
	movq	%rax, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	*24(%rdx)
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	%rax, 24(%rax)
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1630:
	leaq	_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1631:
	leaq	_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1602:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1635:
	addl	$1, 12(%rax)
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1590:
	movl	8(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	(%rdi), %rax
	movq	%rdx, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	*16(%rax)
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rdi
	testq	%rdx, %rdx
	je	.L1593
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1594:
	cmpl	$1, %eax
	jne	.L1629
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1586:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1593:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1594
.L1634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8658:
	.size	_ZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, .-_ZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"Waiting for the debugger to disconnect...\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent17WaitForDisconnectEv
	.type	_ZN4node9inspector5Agent17WaitForDisconnectEv, @function
_ZN4node9inspector5Agent17WaitForDisconnectEv:
.LFB8659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L1699
	movq	32(%rdi), %r13
	movq	%rdi, %r12
	movq	$0, 32(%rdi)
	testq	%r13, %r13
	je	.L1638
	movq	%r13, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	8(%r12), %rax
.L1638:
	movq	48(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1646
	movq	%rbx, %rax
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1642
.L1641:
	movq	16(%rax), %rdx
	cmpb	$0, 64(%rdx)
	je	.L1700
	testq	%r13, %r13
	je	.L1701
.L1642:
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	16(%rbx), %r14
	movq	16(%r14), %rdi
	call	_ZN4node9inspector8protocol12RuntimeAgent26notifyWaitingForDisconnectEv@PLT
	movq	(%rbx), %rbx
	testb	%al, %al
	movb	%al, 65(%r14)
	cmovne	%eax, %r15d
	testq	%rbx, %rbx
	jne	.L1644
	testb	%r15b, %r15b
	je	.L1702
	testq	%r13, %r13
	je	.L1648
	movq	8(%r12), %r13
	cmpb	$0, 17(%r13)
	movb	$1, 150(%r13)
	jne	.L1648
	movq	8(%r13), %rax
	movb	$1, 17(%r13)
	movq	360(%rax), %rax
	movq	2392(%rax), %rbx
	.p2align 4,,10
	.p2align 3
.L1657:
	cmpb	$0, 149(%r13)
	jne	.L1651
	cmpb	$0, 150(%r13)
	jne	.L1652
	cmpb	$0, 148(%r13)
	jne	.L1652
.L1653:
	movb	$0, 17(%r13)
.L1648:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1636
	call	_ZN4node9inspector11InspectorIo27StopAcceptingNewConnectionsEv@PLT
	movq	8(%r12), %rbx
	cmpb	$0, 17(%rbx)
	movb	$1, 150(%rbx)
	jne	.L1636
	movq	8(%rbx), %rax
	movb	$1, 17(%rbx)
	movq	360(%rax), %rax
	movq	2392(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L1667:
	cmpb	$0, 149(%rbx)
	jne	.L1661
	cmpb	$0, 150(%rbx)
	jne	.L1662
	cmpb	$0, 148(%rbx)
	jne	.L1662
.L1663:
	movb	$0, 17(%rbx)
.L1636:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1702:
	.cfi_restore_state
	movq	8(%r12), %rax
.L1646:
	movq	24(%rax), %rdi
	movq	(%r12), %rdx
	movq	(%rdi), %rax
	movq	3280(%rdx), %rsi
	call	*24(%rax)
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	48(%rbx), %rax
	testq	%rax, %rax
	jne	.L1664
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1663
.L1664:
	movq	16(%rax), %rdx
	cmpb	$0, 64(%rdx)
	je	.L1703
.L1661:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1666
	call	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv@PLT
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	movq	(%r12), %rax
	call	*168(%rax)
	testb	%al, %al
	jne	.L1666
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	48(%r13), %rax
	testq	%rax, %rax
	jne	.L1654
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1653
.L1654:
	movq	16(%rax), %rdx
	cmpb	$0, 64(%rdx)
	je	.L1704
.L1651:
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1656
	call	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv@PLT
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	8(%r13), %rax
	movq	%rbx, %rdi
	movq	352(%rax), %rsi
	movq	(%rbx), %rax
	call	*168(%rax)
	testb	%al, %al
	jne	.L1656
	jmp	.L1657
.L1701:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdi
	call	fwrite@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movq	8(%r12), %rax
	movq	48(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1646
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1699:
	leaq	_ZZN4node9inspector5Agent17WaitForDisconnectEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8659:
	.size	_ZN4node9inspector5Agent17WaitForDisconnectEv, .-_ZN4node9inspector5Agent17WaitForDisconnectEv
	.p2align 4
	.type	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE0_4_FUNESL_, @function
_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE0_4_FUNESL_:
.LFB8647:
	.cfi_startproc
	endbr64
	movq	2080(%rdi), %rdi
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L1705
	cmpq	$0, 24(%rdi)
	je	.L1711
.L1707:
	jmp	_ZN4node9inspector5Agent17WaitForDisconnectEv
	.p2align 4,,10
	.p2align 3
.L1711:
	cmpq	$0, 56(%rax)
	jne	.L1707
.L1705:
	ret
	.cfi_endproc
.LFE8647:
	.size	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE0_4_FUNESL_, .-_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENUlPvE0_4_FUNESL_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE
	.type	_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE, @function
_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE:
.LFB8660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 24(%rdi)
	je	.L1712
	movq	8(%rdi), %rbx
	movq	%rdx, %rsi
	movq	%rdi, %r13
	movq	%rdx, %r12
	movq	8(%rbx), %rax
	movq	352(%rax), %r15
	movq	3280(%rax), %r14
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZNK2v87Message15GetScriptOriginEv@PLT
	movq	-96(%rbp), %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	movl	%eax, -172(%rbp)
	call	_ZNK2v87Message13GetStackTraceEv@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1715
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZNK2v810StackTrace13GetFrameCountEv@PLT
	movq	-200(%rbp), %r8
	testl	%eax, %eax
	jg	.L1737
.L1715:
	movq	24(%rbx), %r10
	movb	$0, -57(%rbp)
	movq	%r8, %rdx
	movabsq	$8388068059820289621, %rax
	movq	%rax, -65(%rbp)
	leaq	-136(%rbp), %r11
	movq	(%r10), %rax
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r10, -216(%rbp)
	movq	%r11, -208(%rbp)
	movq	128(%rax), %rbx
	movq	%rbx, -184(%rbp)
	xorl	%ebx, %ebx
	call	*152(%rax)
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v87Message14GetStartColumnENS_5LocalINS_7ContextEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	sarq	$32, %rcx
	testb	%al, %al
	cmove	%ebx, %ecx
	movl	%ecx, -176(%rbp)
	call	_ZNK2v87Message13GetLineNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testb	%al, %al
	cmovne	%edx, %ebx
	call	_ZNK2v87Message21GetScriptResourceNameEv@PLT
	movq	%r15, %rsi
	leaq	-144(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE
	movq	-144(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZNK2v87Message3GetEv@PLT
	movq	%r15, %rsi
	leaq	-152(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE
	movq	-152(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	-176(%rbp), %ecx
	movb	$1, -128(%rbp)
	movq	%r14, %rsi
	movq	%rax, %r8
	leaq	-65(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %r10
	movq	%rax, -112(%rbp)
	movl	-172(%rbp), %eax
	movq	%r10, %rdi
	movq	$8, -120(%rbp)
	movq	-200(%rbp), %r9
	pushq	%rax
	movq	-192(%rbp), %rdx
	pushq	%r11
	pushq	%rcx
	movq	-168(%rbp), %rcx
	pushq	%rbx
	movq	-184(%rbp), %rbx
	call	*%rbx
	movq	-152(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L1718
	movq	(%rdi), %rax
	call	*8(%rax)
.L1718:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1719
	movq	(%rdi), %rax
	call	*8(%rax)
.L1719:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1720
	movq	(%rdi), %rax
	call	*64(%rax)
.L1720:
	movq	%r13, %rdi
	call	_ZN4node9inspector5Agent17WaitForDisconnectEv
.L1712:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1738
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1737:
	.cfi_restore_state
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZNK2v810StackTrace8GetFrameEPNS_7IsolateEj@PLT
	movq	%rax, %rdi
	call	_ZNK2v810StackFrame11GetScriptIdEv@PLT
	cmpl	-184(%rbp), %eax
	movl	$0, %eax
	movq	-200(%rbp), %r8
	movl	%eax, %esi
	cmovne	-172(%rbp), %esi
	movl	%esi, -172(%rbp)
	jmp	.L1715
.L1738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8660:
	.size	_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE, .-_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	48(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1739
	leaq	-64(%rbp), %rax
	movq	%rsi, %r12
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L1743:
	movq	16(%rbx), %r14
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	48(%r14), %r15
	movq	-64(%rbp), %rdi
	movq	(%r15), %rax
	movq	48(%rax), %r13
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-64(%rbp), %rdi
	movq	%rax, %r14
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	*%r13
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1741
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1743
.L1739:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1752
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1741:
	.cfi_restore_state
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1743
	jmp	.L1739
.L1752:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8661:
	.size	_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Cannot toggle Inspector's AsyncHook, please report this."
	.align 8
.LC18:
	.string	"\nnode::inspector::Agent::ToggleAsyncHook"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE
	.type	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE, @function
_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE:
.LFB8665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpb	$0, 1928(%rax)
	je	.L1763
	leaq	-144(%rbp), %r15
	movq	%rdx, %r13
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpq	$0, 0(%r13)
	je	.L1764
	movq	(%rbx), %rax
	leaq	-112(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	3280(%rax), %rbx
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1756
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1756:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	88(%r12), %rdx
	movq	%rbx, %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1765
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1766
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1763:
	.cfi_restore_state
	leaq	_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1764:
	leaq	_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1765:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE@PLT
	leaq	.LC17(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	call	_ZN4node10FatalErrorEPKcS1_@PLT
.L1766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8665:
	.size	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE, .-_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent15EnableAsyncHookEv
	.type	_ZN4node9inspector5Agent15EnableAsyncHookEv, @function
_ZN4node9inspector5Agent15EnableAsyncHookEv:
.LFB8663:
	.cfi_startproc
	endbr64
	cmpq	$0, 192(%rdi)
	je	.L1768
	movq	(%rdi), %rax
	leaq	192(%rdi), %rdx
	movq	352(%rax), %rsi
	jmp	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE
	.p2align 4,,10
	.p2align 3
.L1768:
	cmpb	$0, 185(%rdi)
	je	.L1769
	cmpb	$0, 184(%rdi)
	jne	.L1775
	movb	$0, 185(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1769:
	movb	$1, 184(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1775:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector5Agent15EnableAsyncHookEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8663:
	.size	_ZN4node9inspector5Agent15EnableAsyncHookEv, .-_ZN4node9inspector5Agent15EnableAsyncHookEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent16DisableAsyncHookEv
	.type	_ZN4node9inspector5Agent16DisableAsyncHookEv, @function
_ZN4node9inspector5Agent16DisableAsyncHookEv:
.LFB8664:
	.cfi_startproc
	endbr64
	cmpq	$0, 200(%rdi)
	je	.L1777
	movq	(%rdi), %rax
	leaq	200(%rdi), %rdx
	movq	352(%rax), %rsi
	jmp	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE
	.p2align 4,,10
	.p2align 3
.L1777:
	cmpb	$0, 184(%rdi)
	je	.L1778
	cmpb	$0, 185(%rdi)
	jne	.L1784
	movb	$0, 184(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	movb	$1, 185(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1784:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector5Agent16DisableAsyncHookEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8664:
	.size	_ZN4node9inspector5Agent16DisableAsyncHookEv, .-_ZN4node9inspector5Agent16DisableAsyncHookEv
	.section	.text._ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi,"axG",@progbits,_ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi
	.type	_ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi, @function
_ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi:
.LFB8541:
	.cfi_startproc
	endbr64
	cmpb	$0, 150(%rdi)
	jne	.L1798
	movq	8(%rdi), %rax
	movq	2080(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1798
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	jne	.L1787
	cmpq	$0, 200(%rdi)
	je	.L1788
	movq	(%rdi), %rax
	leaq	200(%rdi), %rdx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	352(%rax), %rsi
	jmp	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE
	.p2align 4,,10
	.p2align 3
.L1798:
	ret
	.p2align 4,,10
	.p2align 3
.L1787:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	cmpq	$0, 192(%rdi)
	je	.L1791
	movq	(%rdi), %rax
	leaq	192(%rdi), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	352(%rax), %rsi
	jmp	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore_state
	cmpb	$0, 184(%rdi)
	je	.L1789
	cmpb	$0, 185(%rdi)
	jne	.L1801
	movb	$0, 184(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1791:
	.cfi_restore_state
	cmpb	$0, 185(%rdi)
	je	.L1792
	cmpb	$0, 184(%rdi)
	jne	.L1802
	movb	$0, 185(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1789:
	.cfi_restore_state
	movb	$1, 185(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1792:
	.cfi_restore_state
	movb	$1, 184(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1802:
	.cfi_restore_state
	leaq	_ZZN4node9inspector5Agent15EnableAsyncHookEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1801:
	leaq	_ZZN4node9inspector5Agent16DisableAsyncHookEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8541:
	.size	_ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi, .-_ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_
	.type	_ZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_, @function
_ZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_:
.LFB8662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	192(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1804
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 192(%r12)
.L1804:
	testq	%r14, %r14
	je	.L1805
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 192(%r12)
.L1805:
	movq	200(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1806
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 200(%r12)
.L1806:
	testq	%r13, %r13
	je	.L1807
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 200(%r12)
.L1807:
	cmpb	$0, 184(%r12)
	movzbl	185(%r12), %eax
	je	.L1808
	testb	%al, %al
	jne	.L1830
	cmpq	$0, 192(%r12)
	leaq	192(%r12), %rdx
	movb	$0, 184(%r12)
	je	.L1831
.L1829:
	movq	(%r12), %rax
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	movq	352(%rax), %rsi
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEE
	.p2align 4,,10
	.p2align 3
.L1808:
	.cfi_restore_state
	testb	%al, %al
	jne	.L1832
.L1803:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1830:
	.cfi_restore_state
	leaq	_ZZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1832:
	cmpq	$0, 200(%r12)
	movb	$0, 185(%r12)
	je	.L1812
	leaq	200(%r12), %rdx
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1831:
	movb	$1, 184(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1812:
	.cfi_restore_state
	movb	$1, 185(%r12)
	jmp	.L1803
	.cfi_endproc
.LFE8662:
	.size	_ZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_, .-_ZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent18AsyncTaskScheduledERKN12v8_inspector10StringViewEPvb
	.type	_ZN4node9inspector5Agent18AsyncTaskScheduledERKN12v8_inspector10StringViewEPvb, @function
_ZN4node9inspector5Agent18AsyncTaskScheduledERKN12v8_inspector10StringViewEPvb:
.LFB8666:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movzbl	%cl, %ecx
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE8666:
	.size	_ZN4node9inspector5Agent18AsyncTaskScheduledERKN12v8_inspector10StringViewEPvb, .-_ZN4node9inspector5Agent18AsyncTaskScheduledERKN12v8_inspector10StringViewEPvb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent17AsyncTaskCanceledEPv
	.type	_ZN4node9inspector5Agent17AsyncTaskCanceledEPv, @function
_ZN4node9inspector5Agent17AsyncTaskCanceledEPv:
.LFB8667:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE8667:
	.size	_ZN4node9inspector5Agent17AsyncTaskCanceledEPv, .-_ZN4node9inspector5Agent17AsyncTaskCanceledEPv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent16AsyncTaskStartedEPv
	.type	_ZN4node9inspector5Agent16AsyncTaskStartedEPv, @function
_ZN4node9inspector5Agent16AsyncTaskStartedEPv:
.LFB8668:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	jmp	*80(%rax)
	.cfi_endproc
.LFE8668:
	.size	_ZN4node9inspector5Agent16AsyncTaskStartedEPv, .-_ZN4node9inspector5Agent16AsyncTaskStartedEPv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent17AsyncTaskFinishedEPv
	.type	_ZN4node9inspector5Agent17AsyncTaskFinishedEPv, @function
_ZN4node9inspector5Agent17AsyncTaskFinishedEPv:
.LFB8669:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	jmp	*88(%rax)
	.cfi_endproc
.LFE8669:
	.size	_ZN4node9inspector5Agent17AsyncTaskFinishedEPv, .-_ZN4node9inspector5Agent17AsyncTaskFinishedEPv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent21AllAsyncTasksCanceledEv
	.type	_ZN4node9inspector5Agent21AllAsyncTasksCanceledEv, @function
_ZN4node9inspector5Agent21AllAsyncTasksCanceledEv:
.LFB8670:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.cfi_endproc
.LFE8670:
	.size	_ZN4node9inspector5Agent21AllAsyncTasksCanceledEv, .-_ZN4node9inspector5Agent21AllAsyncTasksCanceledEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent20RequestIoThreadStartEv
	.type	_ZN4node9inspector5Agent20RequestIoThreadStartEv, @function
_ZN4node9inspector5Agent20RequestIoThreadStartEv:
.LFB8671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN4node9inspector12_GLOBAL__N_1L33start_io_thread_async_initializedE(%rip), %eax
	testb	%al, %al
	je	.L1854
	movq	%rdi, %r12
	leaq	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip), %rdi
	call	uv_async_send@PLT
	movq	(%r12), %rax
	leaq	-64(%rbp), %rdi
	movq	352(%rax), %r13
	movq	360(%rax), %rax
	movq	2392(%rax), %rsi
	movq	%r13, %rdx
	movq	(%rsi), %rax
	call	*48(%rax)
	movq	-64(%rbp), %r14
	movl	$16, %edi
	movq	(%r14), %rax
	movq	(%rax), %rbx
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111StartIoTaskE(%rip), %rcx
	movq	%r14, %rdi
	leaq	-72(%rbp), %rsi
	movq	%rcx, (%rax)
	movq	%r12, 8(%rax)
	movq	%rax, -72(%rbp)
	call	*%rbx
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1840
	movq	(%rdi), %rax
	call	*8(%rax)
.L1840:
	movq	%r12, %rdx
	leaq	_ZN4node9inspector12_GLOBAL__N_116StartIoInterruptEPN2v87IsolateEPv(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v87Isolate16RequestInterruptEPFvPS0_PvES2_@PLT
	movzbl	_ZN4node9inspector12_GLOBAL__N_1L33start_io_thread_async_initializedE(%rip), %eax
	testb	%al, %al
	je	.L1855
	leaq	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip), %rdi
	call	uv_async_send@PLT
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L1838
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1844
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1856
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1857
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1844:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1838
.L1856:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1847
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1848:
	cmpl	$1, %eax
	jne	.L1838
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1854:
	leaq	_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1855:
	leaq	_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1847:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1848
.L1857:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8671:
	.size	_ZN4node9inspector5Agent20RequestIoThreadStartEv, .-_ZN4node9inspector5Agent20RequestIoThreadStartEv
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPv, @function
_ZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPv:
.LFB8391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN4node9inspector12_GLOBAL__N_1L25start_io_thread_semaphoreE(%rip), %rbx
	subq	$8, %rsp
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1860
	call	_ZN4node9inspector5Agent20RequestIoThreadStartEv
.L1860:
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
.L1861:
	movq	%rbx, %rdi
	call	uv_sem_wait@PLT
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movzbl	_ZN4node9inspector12_GLOBAL__N_1L33start_io_thread_async_initializedE(%rip), %eax
	testb	%al, %al
	jne	.L1859
	leaq	_ZZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8391:
	.size	_ZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPv, .-_ZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent14ContextCreatedEN2v85LocalINS2_7ContextEEERKNS_11ContextInfoE
	.type	_ZN4node9inspector5Agent14ContextCreatedEN2v85LocalINS2_7ContextEEERKNS_11ContextInfoE, @function
_ZN4node9inspector5Agent14ContextCreatedEN2v85LocalINS2_7ContextEEERKNS_11ContextInfoE:
.LFB8680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1866
	movq	%rdx, %rbx
	movq	%rsi, %r12
	leaq	-80(%rbp), %r14
	movq	%rdx, %rsi
	leaq	-200(%rbp), %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	leaq	32(%rbx), %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-200(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-192(%rbp), %rdi
	movq	%r12, -176(%rbp)
	movl	$1, -168(%rbp)
	movdqu	(%rax), %xmm1
	movaps	%xmm1, -160(%rbp)
	movq	16(%rax), %rax
	movb	$1, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	(%rdi), %rax
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$1, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movb	$0, -88(%rbp)
	call	*16(%rax)
	cmpb	$0, 64(%rbx)
	leaq	-64(%rbp), %rbx
	movdqu	(%rax), %xmm2
	movups	%xmm2, -136(%rbp)
	movq	16(%rax), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, -120(%rbp)
	jne	.L1888
	leaq	-184(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	$19, -184(%rbp)
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-184(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movl	$25971, %edx
	movw	%dx, 16(%rax)
	movb	$125, 18(%rax)
	movups	%xmm0, (%rax)
.L1887:
	movq	-184(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, -72(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-80(%rbp), %rdi
	movq	-184(%rbp), %r12
	cmpq	%rbx, %rdi
	je	.L1870
	call	_ZdlPv@PLT
.L1870:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	24(%r13), %rdi
	leaq	-176(%rbp), %rsi
	movdqu	(%rax), %xmm3
	movaps	%xmm3, -112(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -96(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1872
	movq	(%rdi), %rax
	call	*8(%rax)
.L1872:
	movq	-200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1866
	movq	(%rdi), %rax
	call	*8(%rax)
.L1866:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1889
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1888:
	.cfi_restore_state
	leaq	-184(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	$18, -184(%rbp)
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-184(%rbp), %rdx
	movl	$32101, %ecx
	movdqa	.LC11(%rip), %xmm0
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movw	%cx, 16(%rax)
	movups	%xmm0, (%rax)
	jmp	.L1887
.L1889:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8680:
	.size	_ZN4node9inspector5Agent14ContextCreatedEN2v85LocalINS2_7ContextEEERKNS_11ContextInfoE, .-_ZN4node9inspector5Agent14ContextCreatedEN2v85LocalINS2_7ContextEEERKNS_11ContextInfoE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent18WillWaitForConnectEv
	.type	_ZN4node9inspector5Agent18WillWaitForConnectEv, @function
_ZN4node9inspector5Agent18WillWaitForConnectEv:
.LFB8681:
	.cfi_startproc
	endbr64
	movzbl	82(%rdi), %eax
	testb	%al, %al
	jne	.L1890
	movzbl	83(%rdi), %eax
	testb	%al, %al
	je	.L1895
.L1890:
	ret
	.p2align 4,,10
	.p2align 3
.L1895:
	movq	32(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1890
	movzbl	56(%rdx), %eax
	ret
	.cfi_endproc
.LFE8681:
	.size	_ZN4node9inspector5Agent18WillWaitForConnectEv, .-_ZN4node9inspector5Agent18WillWaitForConnectEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent8IsActiveEv
	.type	_ZN4node9inspector5Agent8IsActiveEv, @function
_ZN4node9inspector5Agent8IsActiveEv:
.LFB8682:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L1898
	cmpq	$0, 24(%rdi)
	movl	$1, %r8d
	je	.L1900
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1900:
	cmpq	$0, 56(%rax)
	setne	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1898:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE8682:
	.size	_ZN4node9inspector5Agent8IsActiveEv, .-_ZN4node9inspector5Agent8IsActiveEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent15SetParentHandleESt10unique_ptrINS0_21ParentInspectorHandleESt14default_deleteIS3_EE
	.type	_ZN4node9inspector5Agent15SetParentHandleESt10unique_ptrINS0_21ParentInspectorHandleESt14default_deleteIS3_EE, @function
_ZN4node9inspector5Agent15SetParentHandleESt10unique_ptrINS0_21ParentInspectorHandleESt14default_deleteIS3_EE:
.LFB8683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rsi), %rax
	movq	32(%rdi), %r12
	movq	$0, (%rsi)
	movq	%rax, 32(%rdi)
	testq	%r12, %r12
	je	.L1901
	movq	%r12, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1901:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8683:
	.size	_ZN4node9inspector5Agent15SetParentHandleESt10unique_ptrINS0_21ParentInspectorHandleESt14default_deleteIS3_EE, .-_ZN4node9inspector5Agent15SetParentHandleESt10unique_ptrINS0_21ParentInspectorHandleESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent15GetParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector5Agent15GetParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector5Agent15GetParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rsi
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	je	.L2009
	movq	48(%rsi), %rax
	movdqu	40(%rsi), %xmm2
	movaps	%xmm2, -80(%rbp)
	testq	%rax, %rax
	je	.L1963
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1964
	lock addl	$1, 8(%rax)
.L1963:
	movzbl	56(%rsi), %r15d
	movl	$64, %edi
	call	_Znwm@PLT
	movl	%r13d, %esi
	leaq	-80(%rbp), %rcx
	movq	%r14, %rdx
	movl	%r15d, %r8d
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN4node9inspector21ParentInspectorHandleC1EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb@PLT
	movq	-72(%rbp), %r13
	movq	%rbx, (%r12)
	testq	%r13, %r13
	je	.L1904
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1967
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1968:
	cmpl	$1, %eax
	je	.L2010
.L1904:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2011
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2009:
	.cfi_restore_state
	movq	8(%rax), %rbx
	cmpb	$0, 16(%rbx)
	je	.L1906
	movq	168(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2012
.L1907:
	movq	176(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1906
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%r15, %r15
	je	.L1954
	lock addl	$1, (%rax)
.L1955:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN4node9inspector13WorkerManager15NewParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testq	%r15, %r15
	je	.L2013
	movq	-88(%rbp), %rdx
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
.L1956:
	cmpl	$1, %eax
	jne	.L1904
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1959
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L1960:
	cmpl	$1, %eax
	jne	.L1904
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1964:
	addl	$1, 8(%rax)
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN4node9inspector13WorkerManager15NewParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1970
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1971:
	cmpl	$1, %eax
	jne	.L1904
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1967:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2013:
	movl	8(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rbx)
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1970:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L1959:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L1954:
	addl	$1, 8(%rbx)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2012:
	movq	152(%rbx), %r9
	testq	%r9, %r9
	je	.L2014
.L1908:
	leaq	-80(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZN4node9inspector19MainThreadInterface9GetHandleEv@PLT
	movl	$224, %edi
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm1
	movq	-72(%rbp), %rdi
	movabsq	$4294967297, %rdx
	pxor	%xmm0, %xmm0
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rdx
	movq	%rdx, (%rax)
	movaps	%xmm0, -80(%rbp)
	movups	%xmm0, 16(%rax)
	movups	%xmm1, 32(%rax)
	testq	%rdi, %rdi
	je	.L1924
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1925
	lock addl	$1, 8(%rdi)
.L1924:
	leaq	96(%rax), %rdx
	movss	.LC7(%rip), %xmm0
	movq	$1, 56(%rax)
	movq	%rdx, 48(%rax)
	leaq	152(%rax), %rdx
	movq	%rdx, 104(%rax)
	leaq	208(%rax), %rdx
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movq	$1, 112(%rax)
	movq	$0, 120(%rax)
	movq	$0, 128(%rax)
	movq	$0, 144(%rax)
	movq	$0, 152(%rax)
	movq	%rdx, 160(%rax)
	movq	$1, 168(%rax)
	movq	$0, 176(%rax)
	movq	$0, 184(%rax)
	movq	$0, 200(%rax)
	movq	$0, 208(%rax)
	movl	$0, 216(%rax)
	movss	%xmm0, 80(%rax)
	movss	%xmm0, 136(%rax)
	movss	%xmm0, 192(%rax)
	testq	%rdi, %rdi
	je	.L1931
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1927
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
.L1928:
	cmpl	$1, %edx
	je	.L2015
.L1931:
	leaq	16(%rax), %rdx
	movq	%rdx, %xmm0
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1932
	movl	8(%rdx), %edx
	testl	%edx, %edx
	jne	.L1933
.L1932:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	movq	%xmm0, 16(%rax)
	testq	%r15, %r15
	je	.L2016
	lock addl	$1, 12(%rax)
.L1934:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1936
	testq	%r15, %r15
	je	.L1937
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L1938:
	cmpl	$1, %edx
	jne	.L1936
	movq	(%rdi), %rdx
	movq	%rax, -88(%rbp)
	movq	%xmm0, -96(%rbp)
	call	*24(%rdx)
	movq	-96(%rbp), %xmm0
	movq	-88(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	%rax, 24(%rax)
.L1933:
	movq	176(%rbx), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 168(%rbx)
	testq	%rdi, %rdi
	je	.L1941
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1942
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L1943:
	cmpl	$1, %eax
	je	.L2017
.L1941:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1948
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1949
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L1950:
	cmpl	$1, %eax
	je	.L2018
.L1948:
	movq	168(%rbx), %rsi
	jmp	.L1907
.L1925:
	addl	$1, 8(%rdi)
	jmp	.L1924
.L1949:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L1950
.L1927:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	jmp	.L1928
.L1942:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L1943
.L2017:
	movq	(%rdi), %rax
	movq	%rdi, -88(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-88(%rbp), %rdi
	je	.L1945
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1946:
	cmpl	$1, %eax
	jne	.L1941
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1941
.L2018:
	movq	(%rdi), %rax
	movq	%rdi, -88(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-88(%rbp), %rdi
	je	.L1952
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1953:
	cmpl	$1, %eax
	jne	.L1948
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1948
.L2015:
	movq	(%rdi), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	*16(%rdx)
	testq	%r15, %r15
	movq	-88(%rbp), %rdi
	movq	-96(%rbp), %rax
	je	.L1929
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L1930:
	cmpl	$1, %edx
	jne	.L1931
	movq	(%rdi), %rdx
	movq	%rax, -88(%rbp)
	call	*24(%rdx)
	movq	-88(%rbp), %rax
	jmp	.L1931
.L2014:
	movq	8(%rbx), %rax
	movl	$384, %edi
	movq	360(%rax), %rdx
	movq	352(%rax), %rcx
	movq	2080(%rax), %r15
	movq	2392(%rdx), %r8
	movq	2360(%rdx), %rdx
	movq	%rcx, -104(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -88(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	%r15, %rsi
	movabsq	$4294967297, %rdx
	leaq	16(%rax), %r9
	movq	%rax, -96(%rbp)
	movq	%rdx, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rdx
	movq	%r9, %rdi
	movq	%rdx, (%rax)
	movq	-88(%rbp), %rdx
	movq	%r9, -88(%rbp)
	call	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE@PLT
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r9
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1909
	movl	8(%rdx), %edx
	testl	%edx, %edx
	jne	.L1910
.L1909:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	movq	%r9, 16(%rax)
	testq	%r15, %r15
	je	.L2019
	lock addl	$1, 12(%rax)
.L1911:
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1913
	testq	%r15, %r15
	je	.L1914
	orl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L1915:
	subl	$1, %edx
	jne	.L1913
	movq	(%rdi), %rdx
	movq	%rax, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	*24(%rdx)
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r9
.L1913:
	movq	%rax, 24(%rax)
.L1910:
	movq	160(%rbx), %rdi
	movq	%r9, %xmm0
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 152(%rbx)
	testq	%rdi, %rdi
	je	.L1908
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1918
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L1919:
	cmpl	$1, %eax
	je	.L1920
.L2008:
	movq	152(%rbx), %r9
	jmp	.L1908
.L2016:
	addl	$1, 12(%rax)
	jmp	.L1934
.L1937:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L1938
.L1945:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1946
.L1952:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1953
.L1929:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L1930
.L1918:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L1919
.L1920:
	movq	(%rdi), %rax
	movq	%rdi, -88(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-88(%rbp), %rdi
	je	.L1921
	orl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1922:
	subl	$1, %eax
	jne	.L2008
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2008
.L2019:
	addl	$1, 12(%rax)
	jmp	.L1911
.L2011:
	call	__stack_chk_fail@PLT
.L1914:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L1915
.L1921:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1922
	.cfi_endproc
.LFE8685:
	.size	_ZN4node9inspector5Agent15GetParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector5Agent15GetParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent14WaitForConnectEv
	.type	_ZN4node9inspector5Agent14WaitForConnectEv, @function
_ZN4node9inspector5Agent14WaitForConnectEv:
.LFB8686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L2041
	cmpb	$0, 17(%rbx)
	movb	$1, 149(%rbx)
	jne	.L2020
	movq	8(%rbx), %rax
	movb	$1, 17(%rbx)
	movq	360(%rax), %rax
	movq	2392(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2028
	call	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv@PLT
	.p2align 4,,10
	.p2align 3
.L2028:
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	movq	(%r12), %rax
	call	*168(%rax)
	testb	%al, %al
	jne	.L2028
	cmpb	$0, 149(%rbx)
	jne	.L2036
	cmpb	$0, 150(%rbx)
	jne	.L2024
	cmpb	$0, 148(%rbx)
	jne	.L2024
.L2025:
	movb	$0, 17(%rbx)
.L2020:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2024:
	.cfi_restore_state
	movq	48(%rbx), %rax
	testq	%rax, %rax
	jne	.L2026
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L2025
.L2026:
	movq	16(%rax), %rdx
	cmpb	$0, 64(%rdx)
	je	.L2042
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2041:
	leaq	_ZZN4node9inspector5Agent14WaitForConnectEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8686:
	.size	_ZN4node9inspector5Agent14WaitForConnectEv, .-_ZN4node9inspector5Agent14WaitForConnectEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector5Agent16GetWorkerManagerEv
	.type	_ZN4node9inspector5Agent16GetWorkerManagerEv, @function
_ZN4node9inspector5Agent16GetWorkerManagerEv:
.LFB8687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L2126
	cmpb	$0, 16(%rbx)
	movq	%rdi, %r12
	jne	.L2045
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
.L2043:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2127
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2045:
	.cfi_restore_state
	movq	168(%rbx), %rax
	testq	%rax, %rax
	je	.L2128
.L2047:
	movq	%rax, (%r12)
	movq	176(%rbx), %rax
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L2043
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2094
	lock addl	$1, 8(%rax)
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2094:
	addl	$1, 8(%rax)
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2126:
	leaq	_ZZN4node9inspector5Agent16GetWorkerManagerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L2129
.L2048:
	movq	%r13, %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN4node9inspector19MainThreadInterface9GetHandleEv@PLT
	movl	$224, %edi
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm1
	movq	-72(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	movaps	%xmm0, -80(%rbp)
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r13)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, 0(%r13)
	movups	%xmm0, 16(%r13)
	movups	%xmm1, 32(%r13)
	testq	%r14, %r14
	je	.L2064
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2065
	lock addl	$1, 8(%r14)
.L2064:
	leaq	96(%r13), %rax
	movss	.LC7(%rip), %xmm0
	movq	$1, 56(%r13)
	movq	%rax, 48(%r13)
	leaq	152(%r13), %rax
	movq	%rax, 104(%r13)
	leaq	208(%r13), %rax
	movq	$0, 64(%r13)
	movq	$0, 72(%r13)
	movq	$0, 88(%r13)
	movq	$0, 96(%r13)
	movq	$1, 112(%r13)
	movq	$0, 120(%r13)
	movq	$0, 128(%r13)
	movq	$0, 144(%r13)
	movq	$0, 152(%r13)
	movq	%rax, 160(%r13)
	movq	$1, 168(%r13)
	movq	$0, 176(%r13)
	movq	$0, 184(%r13)
	movq	$0, 200(%r13)
	movq	$0, 208(%r13)
	movl	$0, 216(%r13)
	movss	%xmm0, 80(%r13)
	movss	%xmm0, 136(%r13)
	movss	%xmm0, 192(%r13)
	testq	%r14, %r14
	je	.L2071
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2067
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L2068:
	cmpl	$1, %eax
	je	.L2130
.L2071:
	leaq	16(%r13), %rax
	movq	%rax, %xmm0
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2072
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L2073
.L2072:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	movq	%xmm0, 16(%r13)
	testq	%r15, %r15
	je	.L2131
	lock addl	$1, 12(%r13)
.L2074:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2076
	testq	%r15, %r15
	je	.L2077
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L2078:
	cmpl	$1, %eax
	jne	.L2076
	movq	(%rdi), %rax
	movq	%xmm0, -88(%rbp)
	call	*24(%rax)
	movq	-88(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	%r13, 24(%r13)
.L2073:
	movq	176(%rbx), %r14
	movq	%r13, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 168(%rbx)
	testq	%r14, %r14
	je	.L2081
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2082
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L2083:
	cmpl	$1, %eax
	je	.L2132
.L2081:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L2088
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2089
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L2090:
	cmpl	$1, %eax
	je	.L2133
.L2088:
	movq	168(%rbx), %rax
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2065:
	addl	$1, 8(%r14)
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2089:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L2090
	.p2align 4,,10
	.p2align 3
.L2067:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2082:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2132:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L2085
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2086:
	cmpl	$1, %eax
	jne	.L2081
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2133:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L2092
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2093:
	cmpl	$1, %eax
	jne	.L2088
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2130:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L2069
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2070:
	cmpl	$1, %eax
	jne	.L2071
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2129:
	movq	8(%rbx), %rax
	movl	$384, %edi
	movq	360(%rax), %rdx
	movq	352(%rax), %rcx
	movq	2080(%rax), %r15
	movq	2392(%rdx), %r8
	movq	2360(%rdx), %rdx
	movq	%rcx, -96(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rdx, -88(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movq	%rax, %r14
	movq	-88(%rbp), %rdx
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r14), %r13
	movq	%rax, (%r14)
	movq	%r13, %rdi
	call	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE@PLT
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L2049
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L2050
.L2049:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	movq	%r13, 16(%r14)
	testq	%r15, %r15
	je	.L2134
	lock addl	$1, 12(%r14)
.L2051:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2053
	testq	%r15, %r15
	je	.L2054
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L2055:
	cmpl	$1, %eax
	jne	.L2053
	movq	(%rdi), %rax
	call	*24(%rax)
.L2053:
	movq	%r14, 24(%r14)
.L2050:
	movq	160(%rbx), %rdi
	movq	%r13, %xmm0
	movq	%r14, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 152(%rbx)
	testq	%rdi, %rdi
	je	.L2048
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2058
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L2059:
	cmpl	$1, %eax
	je	.L2060
.L2125:
	movq	152(%rbx), %r13
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2131:
	addl	$1, 12(%r13)
	jmp	.L2074
.L2077:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L2078
.L2085:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2086
.L2092:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2093
.L2069:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2070
.L2058:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L2059
.L2060:
	movq	(%rdi), %rax
	movq	%rdi, -88(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-88(%rbp), %rdi
	je	.L2061
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L2062:
	cmpl	$1, %eax
	jne	.L2125
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2125
.L2134:
	addl	$1, 12(%r14)
	jmp	.L2051
.L2054:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L2055
.L2061:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L2062
.L2127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8687:
	.size	_ZN4node9inspector5Agent16GetWorkerManagerEv, .-_ZN4node9inspector5Agent16GetWorkerManagerEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector5Agent8GetWsUrlB5cxx11Ev
	.type	_ZNK4node9inspector5Agent8GetWsUrlB5cxx11Ev, @function
_ZNK4node9inspector5Agent8GetWsUrlB5cxx11Ev:
.LFB8688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	24(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L2140
	call	_ZNK4node9inspector11InspectorIo8GetWsUrlB5cxx11Ev@PLT
.L2135:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2141
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2140:
	.cfi_restore_state
	leaq	16(%rdi), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 16(%rdi)
	jmp	.L2135
.L2141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8688:
	.size	_ZNK4node9inspector5Agent8GetWsUrlB5cxx11Ev, .-_ZNK4node9inspector5Agent8GetWsUrlB5cxx11Ev
	.section	.text._ZN4node9inspector8protocol11WorkerAgentD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11WorkerAgentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11WorkerAgentD2Ev
	.type	_ZN4node9inspector8protocol11WorkerAgentD2Ev, @function
_ZN4node9inspector8protocol11WorkerAgentD2Ev:
.LFB11210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L2144
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L2145
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L2167
	.p2align 4,,10
	.p2align 3
.L2144:
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L2150
	movq	%r12, %rdi
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2150:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2152
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2153
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L2168
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L2142
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L2158
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L2169
.L2142:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2158:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L2142
.L2169:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L2161
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L2162:
	cmpl	$1, %eax
	jne	.L2142
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2153:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L2152
.L2168:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2145:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L2144
.L2167:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L2148
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L2149:
	cmpl	$1, %eax
	jne	.L2144
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2148:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2161:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L2162
	.cfi_endproc
.LFE11210:
	.size	_ZN4node9inspector8protocol11WorkerAgentD2Ev, .-_ZN4node9inspector8protocol11WorkerAgentD2Ev
	.weak	_ZN4node9inspector8protocol11WorkerAgentD1Ev
	.set	_ZN4node9inspector8protocol11WorkerAgentD1Ev,_ZN4node9inspector8protocol11WorkerAgentD2Ev
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node9inspector5AgentC2EPNS_11EnvironmentE, @function
_GLOBAL__sub_I__ZN4node9inspector5AgentC2EPNS_11EnvironmentE:
.LFB14487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L2173
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
.L2173:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE14487:
	.size	_GLOBAL__sub_I__ZN4node9inspector5AgentC2EPNS_11EnvironmentE, .-_GLOBAL__sub_I__ZN4node9inspector5AgentC2EPNS_11EnvironmentE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node9inspector5AgentC2EPNS_11EnvironmentE
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_111StartIoTaskE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_111StartIoTaskE, 40
_ZTVN4node9inspector12_GLOBAL__N_111StartIoTaskE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111StartIoTaskD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111StartIoTask3RunEv
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_111ChannelImplE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_111ChannelImplE, 144
_ZTVN4node9inspector12_GLOBAL__N_111ChannelImplE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImplD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl12sendResponseEiSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl16sendNotificationESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.quad	_ZN4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.quad	-8
	.quad	0
	.quad	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD1Ev
	.quad	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImplD0Ev
	.quad	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl20sendProtocolResponseEiSt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.quad	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl24sendProtocolNotificationESt10unique_ptrINS0_8protocol12SerializableESt14default_deleteIS5_EE
	.quad	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl11fallThroughEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.quad	_ZThn8_N4node9inspector12_GLOBAL__N_111ChannelImpl26flushProtocolNotificationsEv
	.weak	_ZTVN4node9inspector19NodeInspectorClientE
	.section	.data.rel.ro.local._ZTVN4node9inspector19NodeInspectorClientE,"awG",@progbits,_ZTVN4node9inspector19NodeInspectorClientE,comdat
	.align 8
	.type	_ZTVN4node9inspector19NodeInspectorClientE, @object
	.size	_ZTVN4node9inspector19NodeInspectorClientE, 240
_ZTVN4node9inspector19NodeInspectorClientE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector19NodeInspectorClientD1Ev
	.quad	_ZN4node9inspector19NodeInspectorClientD0Ev
	.quad	_ZN4node9inspector19NodeInspectorClient21runMessageLoopOnPauseEi
	.quad	_ZN4node9inspector19NodeInspectorClient22quitMessageLoopOnPauseEv
	.quad	_ZN4node9inspector19NodeInspectorClient23runIfWaitingForDebuggerEi
	.quad	_ZN12v8_inspector17V8InspectorClient11muteMetricsEi
	.quad	_ZN12v8_inspector17V8InspectorClient13unmuteMetricsEi
	.quad	_ZN12v8_inspector17V8InspectorClient16beginUserGestureEv
	.quad	_ZN12v8_inspector17V8InspectorClient14endUserGestureEv
	.quad	_ZN12v8_inspector17V8InspectorClient12valueSubtypeEN2v85LocalINS1_5ValueEEE
	.quad	_ZN12v8_inspector17V8InspectorClient27formatAccessorsAsPropertiesEN2v85LocalINS1_5ValueEEE
	.quad	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node9inspector19NodeInspectorClient27ensureDefaultContextInGroupEi
	.quad	_ZN12v8_inspector17V8InspectorClient29beginEnsureAllContextsInGroupEi
	.quad	_ZN12v8_inspector17V8InspectorClient27endEnsureAllContextsInGroupEi
	.quad	_ZN4node9inspector19NodeInspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEE
	.quad	_ZN12v8_inspector17V8InspectorClient17consoleAPIMessageEiN2v87Isolate17MessageErrorLevelERKNS_10StringViewES6_jjPNS_12V8StackTraceE
	.quad	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE
	.quad	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE
	.quad	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE
	.quad	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE
	.quad	_ZN12v8_inspector17V8InspectorClient12consoleClearEi
	.quad	_ZN4node9inspector19NodeInspectorClient13currentTimeMSEv
	.quad	_ZN4node9inspector19NodeInspectorClient19startRepeatingTimerEdPFvPvES2_
	.quad	_ZN4node9inspector19NodeInspectorClient11cancelTimerEPv
	.quad	_ZN12v8_inspector17V8InspectorClient17canExecuteScriptsEi
	.quad	_ZN4node9inspector19NodeInspectorClient29maxAsyncCallStackDepthChangedEi
	.quad	_ZN4node9inspector19NodeInspectorClient17resourceNameToUrlERKN12v8_inspector10StringViewE
	.section	.data.rel.ro.local
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionE, 40
_ZTVN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSessionD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_126SameThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19MainThreadInterfaceESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector13WorkerManagerESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector19NodeInspectorClientESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC21:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"../src/inspector_agent.cc:1020"
	.section	.rodata.str1.1
.LC23:
	.string	"(client_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"std::shared_ptr<node::inspector::WorkerManager> node::inspector::Agent::GetWorkerManager()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent16GetWorkerManagerEvE4args, @object
	.size	_ZZN4node9inspector5Agent16GetWorkerManagerEvE4args, 24
_ZZN4node9inspector5Agent16GetWorkerManagerEvE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"../src/inspector_agent.cc:1015"
	.align 8
.LC26:
	.string	"void node::inspector::Agent::WaitForConnect()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent14WaitForConnectEvE4args, @object
	.size	_ZZN4node9inspector5Agent14WaitForConnectEvE4args, 24
_ZZN4node9inspector5Agent14WaitForConnectEvE4args:
	.quad	.LC25
	.quad	.LC23
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"../src/inspector_agent.cc:977"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"start_io_thread_async_initialized"
	.align 8
.LC29:
	.string	"void node::inspector::Agent::RequestIoThreadStart()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args_0, @object
	.size	_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args_0, 24
_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args_0:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/inspector_agent.cc:969"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args, @object
	.size	_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args, 24
_ZZN4node9inspector5Agent20RequestIoThreadStartEvE4args:
	.quad	.LC30
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC31:
	.string	"../src/inspector_agent.cc:933"
.LC32:
	.string	"!fn.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"void node::inspector::Agent::ToggleAsyncHook(v8::Isolate*, const v8::Global<v8::Function>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args_0, @object
	.size	_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args_0, 24
_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args_0:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.section	.rodata.str1.1
.LC34:
	.string	"../src/inspector_agent.cc:931"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"parent_env_->has_run_bootstrapping_code()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args, @object
	.size	_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args, 24
_ZZN4node9inspector5Agent15ToggleAsyncHookEPN2v87IsolateERKNS2_6GlobalINS2_8FunctionEEEE4args:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC33
	.section	.rodata.str1.1
.LC36:
	.string	"../src/inspector_agent.cc:922"
.LC37:
	.string	"!pending_disable_async_hook_"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"void node::inspector::Agent::DisableAsyncHook()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent16DisableAsyncHookEvE4args, @object
	.size	_ZZN4node9inspector5Agent16DisableAsyncHookEvE4args, 24
_ZZN4node9inspector5Agent16DisableAsyncHookEvE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/inspector_agent.cc:911"
.LC40:
	.string	"!pending_enable_async_hook_"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"void node::inspector::Agent::EnableAsyncHook()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent15EnableAsyncHookEvE4args, @object
	.size	_ZZN4node9inspector5Agent15EnableAsyncHookEvE4args, 24
_ZZN4node9inspector5Agent15EnableAsyncHookEvE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"../src/inspector_agent.cc:897"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::inspector::Agent::RegisterAsyncHook(v8::Isolate*, v8::Local<v8::Function>, v8::Local<v8::Function>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_E4args, @object
	.size	_ZZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_E4args, 24
_ZZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_E4args:
	.quad	.LC42
	.quad	.LC37
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/inspector_agent.cc:861"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"void node::inspector::Agent::WaitForDisconnect()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent17WaitForDisconnectEvE4args, @object
	.size	_ZZN4node9inspector5Agent17WaitForDisconnectEvE4args, 24
_ZZN4node9inspector5Agent17WaitForDisconnectEvE4args:
	.quad	.LC44
	.quad	.LC23
	.quad	.LC45
	.section	.rodata.str1.1
.LC46:
	.string	"../src/inspector_agent.cc:853"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"std::unique_ptr<node::inspector::InspectorSession> node::inspector::Agent::ConnectToMainThread(std::unique_ptr<node::inspector::InspectorSessionDelegate>, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args_0, @object
	.size	_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args_0, 24
_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args_0:
	.quad	.LC46
	.quad	.LC23
	.quad	.LC47
	.section	.rodata.str1.1
.LC48:
	.string	"../src/inspector_agent.cc:852"
.LC49:
	.string	"(parent_handle_) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args, @object
	.size	_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args, 24
_ZZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC47
	.section	.rodata.str1.1
.LC50:
	.string	"../src/inspector_agent.cc:842"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"std::unique_ptr<node::inspector::InspectorSession> node::inspector::Agent::Connect(std::unique_ptr<node::inspector::InspectorSessionDelegate>, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args, @object
	.size	_ZZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args, 24
_ZZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEbE4args:
	.quad	.LC50
	.quad	.LC23
	.quad	.LC51
	.section	.rodata.str1.1
.LC52:
	.string	"../src/inspector_agent.cc:822"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"bool node::inspector::Agent::StartIoThread()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent13StartIoThreadEvE4args, @object
	.size	_ZZN4node9inspector5Agent13StartIoThreadEvE4args, 24
_ZZN4node9inspector5Agent13StartIoThreadEvE4args:
	.quad	.LC52
	.quad	.LC23
	.quad	.LC53
	.section	.rodata.str1.1
.LC54:
	.string	"../src/inspector_agent.cc:810"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"!parent_env_->has_serialized_options()"
	.align 8
.LC56:
	.string	"bool node::inspector::Agent::Start(const string&, const node::DebugOptions&, std::shared_ptr<node::ExclusiveAccess<node::HostPort> >, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_2, @object
	.size	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_2, 24
_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_2:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.section	.rodata.str1.1
.LC57:
	.string	"../src/inspector_agent.cc:787"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"start_io_thread_async_initialized.exchange(false)"
	.align 8
.LC59:
	.string	"node::inspector::Agent::Start(const string&, const node::DebugOptions&, std::shared_ptr<node::ExclusiveAccess<node::HostPort> >, bool)::<lambda(void*)>::<lambda(uv_async_t*)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESL_ENKUlP10uv_async_sE_clESO_E4args, @object
	.size	_ZZZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESL_ENKUlP10uv_async_sE_clESO_E4args, 24
_ZZZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbENKUlPvE_clESL_ENKUlP10uv_async_sE_clESO_E4args:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.section	.rodata.str1.1
.LC60:
	.string	"../src/inspector_agent.cc:769"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"(0) == (uv_async_init(parent_env_->event_loop(), &start_io_thread_async, StartIoThreadAsyncCallback))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_1, @object
	.size	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_1, 24
_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_1:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC56
	.section	.rodata.str1.1
.LC62:
	.string	"../src/inspector_agent.cc:768"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"(start_io_thread_async_initialized.exchange(true)) == (false)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_0, @object
	.size	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_0, 24
_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args_0:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC56
	.section	.rodata.str1.1
.LC64:
	.string	"../src/inspector_agent.cc:762"
.LC65:
	.string	"(host_port) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args, @object
	.size	_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args, 24
_ZZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEbE4args:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC56
	.section	.rodata.str1.1
.LC66:
	.string	"../src/inspector_agent.cc:411"
.LC67:
	.string	"(timer_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"node::inspector::{anonymous}::InspectorTimerHandle::~InspectorTimerHandle()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120InspectorTimerHandleD4EvE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120InspectorTimerHandleD4EvE4args, 24
_ZZN4node9inspector12_GLOBAL__N_120InspectorTimerHandleD4EvE4args:
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.section	.rodata.str1.1
.LC69:
	.string	"../src/inspector_agent.cc:154"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"(0) == (pthread_sigmask(1, &sigmask, nullptr))"
	.align 8
.LC71:
	.string	"int node::inspector::{anonymous}::StartDebugSignalHandler()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_6, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_6, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_6:
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.section	.rodata.str1.1
.LC72:
	.string	"../src/inspector_agent.cc:141"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"(0) == (pthread_attr_destroy(&attr))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_5, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_5, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_5:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC71
	.section	.rodata.str1.1
.LC74:
	.string	"../src/inspector_agent.cc:140"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"(0) == (pthread_sigmask(2, &sigmask, nullptr))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_4, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_4, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_4:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC71
	.section	.rodata.str1.1
.LC76:
	.string	"../src/inspector_agent.cc:134"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"(0) == (pthread_sigmask(2, &sigmask, &savemask))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_3, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_3, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_3:
	.quad	.LC76
	.quad	.LC77
	.quad	.LC71
	.section	.rodata.str1.1
.LC78:
	.string	"../src/inspector_agent.cc:129"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"(0) == (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_2, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_2, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_2:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC71
	.section	.rodata.str1.1
.LC80:
	.string	"../src/inspector_agent.cc:127"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"(0) == (pthread_attr_setstacksize(&attr, stack_size))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_1, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_1, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_1:
	.quad	.LC80
	.quad	.LC81
	.quad	.LC71
	.section	.rodata.str1.1
.LC82:
	.string	"../src/inspector_agent.cc:116"
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"(0) == (pthread_attr_init(&attr))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_0, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_0, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args_0:
	.quad	.LC82
	.quad	.LC83
	.quad	.LC71
	.section	.rodata.str1.1
.LC84:
	.string	"../src/inspector_agent.cc:114"
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"(0) == (uv_sem_init(&start_io_thread_semaphore, 0))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args, 24
_ZZN4node9inspector12_GLOBAL__N_1L23StartDebugSignalHandlerEvE4args:
	.quad	.LC84
	.quad	.LC85
	.quad	.LC71
	.section	.rodata.str1.1
.LC86:
	.string	"../src/inspector_agent.cc:102"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"void* node::inspector::{anonymous}::StartIoThreadMain(void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPvE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPvE4args, 24
_ZZN4node9inspector12_GLOBAL__N_117StartIoThreadMainEPvE4args:
	.quad	.LC86
	.quad	.LC28
	.quad	.LC87
	.local	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE
	.comm	_ZN4node9inspector12_GLOBAL__N_1L27start_io_thread_async_mutexE,40,32
	.local	_ZN4node9inspector12_GLOBAL__N_1L33start_io_thread_async_initializedE
	.comm	_ZN4node9inspector12_GLOBAL__N_1L33start_io_thread_async_initializedE,1,1
	.local	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE
	.comm	_ZN4node9inspector12_GLOBAL__N_1L21start_io_thread_asyncE,128,32
	.local	_ZN4node9inspector12_GLOBAL__N_1L25start_io_thread_semaphoreE
	.comm	_ZN4node9inspector12_GLOBAL__N_1L25start_io_thread_semaphoreE,32,32
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC88:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC90:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1083129856
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC7:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.quad	7018408413971948155
	.quad	8462954442817563765
	.align 16
.LC12:
	.quad	7018408413971948155
	.quad	7809635628709342325
	.section	.data.rel.ro,"aw"
	.align 8
.LC13:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC14:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC15:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
