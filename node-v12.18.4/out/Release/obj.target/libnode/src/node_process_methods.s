	.file	"node_process_methods.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"uv_getrusage"
	.text
	.p2align 4
	.type	_ZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$224, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L42
	cmpw	$1040, %cx
	jne	.L2
.L42:
	movq	23(%rdx), %r12
.L4:
	leaq	-176(%rbp), %rdi
	call	uv_getrusage@PLT
	testl	%eax, %eax
	jne	.L45
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L46
	movq	8(%rbx), %rdi
.L8:
	call	_ZNK2v85Value14IsFloat64ArrayEv@PLT
	testb	%al, %al
	je	.L47
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L10
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L11:
	movq	%r12, %rdi
	call	_ZN2v810TypedArray6LengthEv@PLT
	cmpq	$16, %rax
	jne	.L48
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-240(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	movsd	.LC1(%rip), %xmm2
	cvtsi2sdq	-176(%rbp), %xmm1
	movq	-240(%rbp), %rax
	movq	-144(%rbp), %rdx
	cvtsi2sdq	-168(%rbp), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	-160(%rbp), %xmm0
	movsd	%xmm1, (%rax)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-152(%rbp), %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rax)
	testq	%rdx, %rdx
	js	.L13
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L14:
	movq	-136(%rbp), %rdx
	movsd	%xmm0, 16(%rax)
	testq	%rdx, %rdx
	js	.L15
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L16:
	movq	-128(%rbp), %rdx
	movsd	%xmm0, 24(%rax)
	testq	%rdx, %rdx
	js	.L17
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L18:
	movq	-120(%rbp), %rdx
	movsd	%xmm0, 32(%rax)
	testq	%rdx, %rdx
	js	.L19
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L20:
	movq	-112(%rbp), %rdx
	movsd	%xmm0, 40(%rax)
	testq	%rdx, %rdx
	js	.L21
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-104(%rbp), %rdx
	movsd	%xmm0, 48(%rax)
	testq	%rdx, %rdx
	js	.L23
.L50:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-96(%rbp), %rdx
	movsd	%xmm0, 56(%rax)
	testq	%rdx, %rdx
	js	.L25
.L51:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-88(%rbp), %rdx
	movsd	%xmm0, 64(%rax)
	testq	%rdx, %rdx
	js	.L27
.L52:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-80(%rbp), %rdx
	movsd	%xmm0, 72(%rax)
	testq	%rdx, %rdx
	js	.L29
.L53:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-72(%rbp), %rdx
	movsd	%xmm0, 80(%rax)
	testq	%rdx, %rdx
	js	.L31
.L54:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-64(%rbp), %rdx
	movsd	%xmm0, 88(%rax)
	testq	%rdx, %rdx
	js	.L33
.L55:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-56(%rbp), %rdx
	movsd	%xmm0, 96(%rax)
	testq	%rdx, %rdx
	js	.L35
.L56:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	-48(%rbp), %rdx
	movsd	%xmm0, 104(%rax)
	testq	%rdx, %rdx
	js	.L37
.L57:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L38:
	movq	-40(%rbp), %rdx
	movsd	%xmm0, 112(%rax)
	testq	%rdx, %rdx
	js	.L39
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L40:
	movsd	%xmm0, 120(%rax)
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	movq	8(%rbx), %r12
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-104(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 48(%rax)
	testq	%rdx, %rdx
	jns	.L50
.L23:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-96(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 56(%rax)
	testq	%rdx, %rdx
	jns	.L51
.L25:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-88(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 64(%rax)
	testq	%rdx, %rdx
	jns	.L52
.L27:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-80(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rax)
	testq	%rdx, %rdx
	jns	.L53
.L29:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-72(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 80(%rax)
	testq	%rdx, %rdx
	jns	.L54
.L31:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-64(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 88(%rax)
	testq	%rdx, %rdx
	jns	.L55
.L33:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-56(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 96(%rax)
	testq	%rdx, %rdx
	jns	.L56
.L35:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	-48(%rbp), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 104(%rax)
	testq	%rdx, %rdx
	jns	.L57
.L37:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L45:
	movq	352(%r12), %r12
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC0(%rip), %rdx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7717:
	.size	_ZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL6HrtimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL6HrtimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_hrtime@PLT
	movq	%rax, %rbx
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L59
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L60:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%rbx, %rdx
	movq	-80(%rbp), %rcx
	movabsq	$19342813113834067, %rsi
	shrq	$9, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movq	%rdx, %rax
	shrq	$43, %rdx
	shrq	$11, %rax
	movl	%edx, (%rcx)
	movl	%eax, 4(%rcx)
	imulq	$1000000000, %rax, %rax
	subq	%rax, %rbx
	movl	%ebx, 8(%rcx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	8(%r12), %rdi
	jmp	.L60
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7697:
	.size	_ZN4nodeL6HrtimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL6HrtimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL12HrtimeBigIntERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL12HrtimeBigIntERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L65
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L66:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rbx
	call	uv_hrtime@PLT
	movq	%rax, (%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L66
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7698:
	.size	_ZN4nodeL12HrtimeBigIntERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL12HrtimeBigIntERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-176(%rbp), %rdi
	subq	$224, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_getrusage@PLT
	testl	%eax, %eax
	jne	.L83
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L84
	movq	8(%rbx), %rdi
.L75:
	call	_ZNK2v85Value14IsFloat64ArrayEv@PLT
	testb	%al, %al
	je	.L85
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L77
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L78:
	movq	%r12, %rdi
	call	_ZN2v810TypedArray6LengthEv@PLT
	cmpq	$2, %rax
	jne	.L86
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-240(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	movsd	.LC1(%rip), %xmm2
	cvtsi2sdq	-176(%rbp), %xmm1
	movq	-240(%rbp), %rax
	cvtsi2sdq	-168(%rbp), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	-160(%rbp), %xmm0
	movsd	%xmm1, (%rax)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-152(%rbp), %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rax)
.L70:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	movq	8(%rbx), %r12
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%eax, %edi
	call	uv_strerror@PLT
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %rsi
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L88
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L70
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7695:
	.size	_ZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL5AbortERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL5AbortERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node5AbortEv@PLT
	.cfi_endproc
.LFE7690:
	.size	_ZN4nodeL5AbortERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL5AbortERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.text._ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev,"axG",@progbits,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.type	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, @function
_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev:
.LFB9912:
	.cfi_startproc
	endbr64
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE9912:
	.size	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, .-_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev
	.set	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.text
	.p2align 4
	.type	_ZN4nodeL25StartProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL25StartProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7702:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L96
	cmpw	$1040, %cx
	jne	.L93
.L96:
	movq	23(%rdx), %rdi
	jmp	_ZN4node11Environment25StartProfilerIdleNotifierEv@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	_ZN4node11Environment25StartProfilerIdleNotifierEv@PLT
	.cfi_endproc
.LFE7702:
	.size	_ZN4nodeL25StartProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL25StartProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL24StopProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL24StopProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7703:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L104
	cmpw	$1040, %cx
	jne	.L101
.L104:
	movq	23(%rdx), %rdi
	jmp	_ZN4node11Environment24StopProfilerIdleNotifierEv@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rax, %rdi
	jmp	_ZN4node11Environment24StopProfilerIdleNotifierEv@PLT
	.cfi_endproc
.LFE7703:
	.size	_ZN4nodeL24StopProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL24StopProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL8DebugEndERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL8DebugEndERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7719:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L113
	cmpw	$1040, %cx
	jne	.L119
.L113:
	movq	23(%rdx), %rax
	movq	2080(%rax), %rdi
	cmpq	$0, 24(%rdi)
	je	.L117
	jmp	_ZN4node9inspector5Agent4StopEv@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	2080(%rax), %rdi
	cmpq	$0, 24(%rdi)
	je	.L108
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector5Agent4StopEv@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7719:
	.size	_ZN4nodeL8DebugEndERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL8DebugEndERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL10ReallyExitERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL10ReallyExitERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L128
	cmpw	$1040, %cx
	jne	.L121
.L128:
	movq	23(%rdx), %r12
.L123:
	movq	%r12, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L124
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L125:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movl	$0, %esi
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarq	$32, %rdx
	testb	%al, %al
	cmovne	%edx, %esi
	jmp	_ZN4node11Environment4ExitEi@PLT
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L123
	.cfi_endproc
.LFE7720:
	.size	_ZN4nodeL10ReallyExitERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL10ReallyExitERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL6UptimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL6UptimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L138
	cmpw	$1040, %cx
	jne	.L131
.L138:
	movq	23(%rdx), %r12
.L133:
	movq	360(%r12), %rax
	movq	2360(%rax), %rdi
	call	uv_update_time@PLT
	call	uv_hrtime@PLT
	subq	_ZN4node11per_process15node_start_timeE(%rip), %rax
	js	.L134
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L135:
	movq	352(%r12), %rdi
	divsd	.LC2(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L140
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L140:
	movq	16(%rdx), %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7705:
	.size	_ZN4nodeL6UptimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL6UptimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC3:
	.string	"uv_cwd"
	.text
	.p2align 4
	.type	_ZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L151
	cmpw	$1040, %cx
	jne	.L142
.L151:
	movq	23(%rdx), %r12
	cmpb	$0, 1928(%r12)
	je	.L153
.L145:
	leaq	-4144(%rbp), %r13
	leaq	-4152(%rbp), %rsi
	movq	$4096, -4152(%rbp)
	movq	%r13, %rdi
	call	uv_cwd@PLT
	testl	%eax, %eax
	jne	.L154
	movq	352(%r12), %rdi
	movl	-4152(%rbp), %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L155
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L141:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$4136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	352(%r12), %r12
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC3(%rip), %rdx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	cmpb	$0, 1928(%r12)
	jne	.L145
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	_ZZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L141
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7696:
	.size	_ZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC4:
	.string	"Invalid number of arguments."
.LC5:
	.string	"kill"
	.text
	.p2align 4
	.type	_ZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L168
	cmpw	$1040, %cx
	jne	.L158
.L168:
	movq	23(%rdx), %r12
.L160:
	cmpl	$1, 16(%rbx)
	je	.L161
	movq	352(%r12), %rsi
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r12), %r12
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L173
.L162:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L157:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L175
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L165
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L166:
	call	_ZNK2v87Integer5ValueEv@PLT
	movl	$10, %esi
	movq	%rax, %rdi
	call	kill@PLT
	testl	%eax, %eax
	je	.L157
	call	__errno_location@PLT
	movq	352(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC5(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L165:
	movq	8(%rbx), %rdi
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	_ZZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7718:
	.size	_ZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC6:
	.string	"Bad argument."
	.text
	.p2align 4
	.type	_ZN4nodeL4KillERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL4KillERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L193
	cmpw	$1040, %cx
	jne	.L177
.L193:
	movq	23(%rdx), %r12
.L179:
	cmpl	$2, 16(%rbx)
	movq	3280(%r12), %r14
	je	.L180
	movq	352(%r12), %rsi
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r12), %r12
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L195
.L181:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L176:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L176
	sarq	$32, %rax
	cmpl	$1, 16(%rbx)
	movq	%rax, %r13
	jg	.L184
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L185:
	movq	%r14, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L176
	sarq	$32, %rsi
	movq	%rsi, %r14
	call	uv_os_getpid@PLT
	testl	%r14d, %r14d
	jle	.L188
	leal	1(%r13), %edx
	cmpl	$1, %edx
	jbe	.L189
	cmpl	%r13d, %eax
	je	.L189
	addl	%r13d, %eax
	je	.L189
	.p2align 4,,10
	.p2align 3
.L188:
	movl	%r14d, %esi
	movl	%r13d, %edi
	call	uv_kill@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L177:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L189:
	movl	%r14d, %edi
	call	_ZN4node18HasSignalJSHandlerEi@PLT
	testb	%al, %al
	jne	.L188
	movq	%r12, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L184:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L185
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7699:
	.size	_ZN4nodeL4KillERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL4KillERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC7:
	.string	"chdir"
	.text
	.p2align 4
	.type	_ZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1096, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L211
	cmpw	$1040, %cx
	jne	.L198
.L211:
	movq	23(%rdx), %r12
	testb	$2, 1932(%r12)
	je	.L225
.L201:
	cmpl	$1, 16(%rbx)
	jne	.L226
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L203
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L203
	movq	352(%r12), %rsi
	leaq	-5200(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-5184(%rbp), %rdi
	call	uv_chdir@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L227
	movq	-5184(%rbp), %rdi
	leaq	-5176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L197
.L224:
	testq	%rdi, %rdi
	je	.L197
	call	free@PLT
.L197:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	addq	$5192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	leaq	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L198:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	testb	$2, 1932(%r12)
	jne	.L201
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	-4144(%rbp), %rbx
	leaq	-5208(%rbp), %rsi
	movq	$4096, -5208(%rbp)
	movq	%rbx, %rdi
	call	uv_cwd@PLT
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	movq	352(%r12), %r12
	movq	-5184(%rbp), %r9
	leaq	.LC7(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-5184(%rbp), %rdi
	leaq	-5176(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L224
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7691:
	.size	_ZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC9:
	.string	"uv_resident_set_memory"
	.text
	.p2align 4
	.type	_ZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$192, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L254
	cmpw	$1040, %cx
	jne	.L230
.L254:
	movq	23(%rdx), %r12
.L232:
	leaq	-216(%rbp), %rdi
	call	uv_resident_set_memory@PLT
	testl	%eax, %eax
	jne	.L257
	movq	352(%r12), %r14
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v814HeapStatisticsC1Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetHeapStatisticsEPNS_14HeapStatisticsE@PLT
	movq	360(%r12), %rax
	movl	16(%rbx), %edx
	movq	2376(%rax), %r12
	testl	%edx, %edx
	jle	.L258
	movq	8(%rbx), %rdi
.L236:
	call	_ZNK2v85Value14IsFloat64ArrayEv@PLT
	testb	%al, %al
	je	.L259
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L238
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L239:
	movq	%r13, %rdi
	call	_ZN2v810TypedArray6LengthEv@PLT
	cmpq	$5, %rax
	jne	.L260
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movq	-208(%rbp), %rdx
	testq	%rax, %rax
	js	.L241
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	js	.L243
.L262:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movq	-216(%rbp), %rax
	unpcklpd	%xmm1, %xmm0
	testq	%rax, %rax
	js	.L245
.L263:
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L246:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	js	.L247
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L248:
	unpcklpd	%xmm2, %xmm1
	movups	%xmm0, 16(%rdx)
	pxor	%xmm0, %xmm0
	movups	%xmm1, (%rdx)
	testq	%r12, %r12
	je	.L249
	movq	16(%r12), %rax
	testq	%rax, %rax
	js	.L250
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L249:
	movsd	%xmm0, 32(%rdx)
.L229:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	addq	$192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L257:
	movq	352(%r12), %r12
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC9(%rip), %rdx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L238:
	movq	8(%rbx), %r13
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm2, %xmm2
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	movq	-88(%rbp), %rax
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	testq	%rax, %rax
	jns	.L262
.L243:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rcx
	orq	%rax, %rcx
	movq	-216(%rbp), %rax
	cvtsi2sdq	%rcx, %xmm1
	addsd	%xmm1, %xmm1
	unpcklpd	%xmm1, %xmm0
	testq	%rax, %rax
	jns	.L263
.L245:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L230:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7700:
	.size	_ZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L279
	cmpw	$1040, %cx
	jne	.L265
.L279:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L284
.L268:
	cmpl	$1, 16(%rbx)
	jne	.L285
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L270
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L286
.L270:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L287
.L271:
	leaq	_ZN4node11per_process11umask_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L272
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L273:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L274
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L274
	cmpl	$5, 43(%rax)
	je	.L288
	.p2align 4,,10
	.p2align 3
.L274:
	call	_ZNK2v86Uint325ValueEv@PLT
	movl	%eax, %edi
	call	umask@PLT
	movl	%eax, %r12d
.L275:
	movq	(%rbx), %rbx
	testl	%r12d, %r12d
	js	.L276
	salq	$32, %r12
	movq	%r12, 24(%rbx)
.L277:
	popq	%rbx
	leaq	_ZN4node11per_process11umask_mutexE(%rip), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L286:
	cmpl	$5, 43(%rax)
	je	.L271
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L265:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L268
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L288:
	xorl	%edi, %edi
	call	umask@PLT
	movl	%eax, %edi
	movl	%eax, %r12d
	call	umask@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L285:
	leaq	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L289
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L277
.L287:
	leaq	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L289:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L277
	.cfi_endproc
.LFE7704:
	.size	_ZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC10:
	.string	"_debugProcess"
.LC11:
	.string	"_debugEnd"
.LC12:
	.string	"abort"
.LC13:
	.string	"_startProfilerIdleNotifier"
.LC14:
	.string	"_stopProfilerIdleNotifier"
.LC15:
	.string	"umask"
.LC16:
	.string	"_rawDebug"
.LC17:
	.string	"memoryUsage"
.LC18:
	.string	"cpuUsage"
.LC19:
	.string	"hrtime"
.LC20:
	.string	"hrtimeBigInt"
.LC21:
	.string	"resourceUsage"
.LC22:
	.string	"_getActiveRequests"
.LC23:
	.string	"_getActiveHandles"
.LC24:
	.string	"_kill"
.LC25:
	.string	"cwd"
.LC26:
	.string	"dlopen"
.LC27:
	.string	"reallyExit"
.LC28:
	.string	"uptime"
.LC29:
	.string	"patchProcessObject"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB30:
	.text
.LHOTB30:
	.p2align 4
	.type	_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, @function
_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv:
.LFB7721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L291
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L291
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L291
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	testb	$2, 1932(%rbx)
	jne	.L363
.L292:
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4nodeL25StartProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L364
.L305:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L365
.L306:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L366
.L307:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL24StopProfilerIdleNotifierERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L367
.L308:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L368
.L309:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L369
.L310:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L370
.L311:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L371
.L312:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L372
.L313:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L373
.L314:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L374
.L315:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L375
.L316:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L376
.L317:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L377
.L318:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L378
.L319:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L379
.L320:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L380
.L321:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L381
.L322:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL6HrtimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L382
.L323:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L383
.L324:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L384
.L325:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL12HrtimeBigIntERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L385
.L326:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L386
.L327:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L387
.L328:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L388
.L329:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L389
.L330:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L390
.L331:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL17GetActiveRequestsERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L391
.L332:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L392
.L333:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L393
.L334:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node16GetActiveHandlesERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L394
.L335:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L395
.L336:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L396
.L337:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL4KillERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L397
.L338:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L398
.L339:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L399
.L340:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L400
.L341:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L401
.L342:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L402
.L343:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	movq	_ZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE@GOTPCREL(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L403
.L344:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L404
.L345:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L405
.L346:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL10ReallyExitERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L406
.L347:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L407
.L348:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L408
.L349:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4nodeL6UptimeERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L409
.L350:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L410
.L351:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L411
.L352:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	movq	_ZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEE@GOTPCREL(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L412
.L353:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L413
.L354:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L414
.L355:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L415
.L293:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L416
.L294:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L417
.L295:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL8DebugEndERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L418
.L296:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L419
.L297:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L420
.L298:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL5AbortERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L421
.L299:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L422
.L300:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L423
.L301:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L424
.L302:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L425
.L303:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L426
.L304:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L414:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L364:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L366:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L367:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L368:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L369:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L370:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L371:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L372:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L373:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L374:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L375:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L376:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L377:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L379:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L380:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L382:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L383:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L384:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L385:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L386:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L387:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L388:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L389:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L390:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L391:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L392:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L393:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L394:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L395:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L396:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L397:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L398:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L399:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L400:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L401:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L402:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L403:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L404:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L405:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L406:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L407:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L408:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L409:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L410:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L411:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L412:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L413:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L426:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L416:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L417:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L418:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L419:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L415:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L424:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L425:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L420:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L421:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L422:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L423:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L301
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, @function
_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold:
.LFSB7721:
.L291:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	1932, %eax
	ud2
	.cfi_endproc
.LFE7721:
	.text
	.size	_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, .-_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, .-_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold
.LCOLDE30:
	.text
.LHOTE30:
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB7140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L428
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L457
	cmpq	$1, %rax
	jne	.L431
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L432:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L427:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L458
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L432
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L430:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L428:
	cmpb	$37, 1(%rax)
	jne	.L459
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L460
	cmpq	$1, %r13
	jne	.L437
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L438:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L440
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L461
.L440:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L442:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L462
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L444:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L427
	call	_ZdlPv@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L437:
	testq	%r13, %r13
	jne	.L463
	movq	%rbx, %rax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L436:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L462:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L442
.L459:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L458:
	call	__stack_chk_fail@PLT
.L463:
	movq	%rbx, %rdi
	jmp	.L436
	.cfi_endproc
.LFE7140:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.text
	.p2align 4
	.globl	_Z25_register_process_methodsv
	.type	_Z25_register_process_methodsv, @function
_Z25_register_process_methodsv:
.LFB7722:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7722:
	.size	_Z25_register_process_methodsv, .-_Z25_register_process_methodsv
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L472
	movq	16(%rdi), %r10
.L466:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L467
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L473
	movq	16(%r9), %r10
.L468:
	cmpq	%r10, %rax
	jbe	.L475
.L467:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L469:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L476
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L471:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L476:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L472:
	movl	$15, %r10d
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L473:
	movl	$15, %r10d
	jmp	.L468
	.cfi_endproc
.LFE8231:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.rodata._ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC31:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB8912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L504
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L489
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L505
.L479:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L488:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L481
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L491
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L491
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L483:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L483
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L485
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L485:
	leaq	16(%rbx,%rsi), %r14
.L481:
	cmpq	%rcx, %r13
	je	.L486
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L486:
	testq	%r12, %r12
	je	.L487
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L487:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L480
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L489:
	movl	$8, %r14d
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L482:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L482
	jmp	.L485
.L480:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L479
.L504:
	leaq	.LC31(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8912:
	.size	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.text
	.p2align 4
	.globl	_ZN4node16GetActiveHandlesERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node16GetActiveHandlesERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node16GetActiveHandlesERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L522
	cmpw	$1040, %cx
	jne	.L507
.L522:
	movq	23(%rdx), %r14
.L509:
	movq	2104(%r14), %rbx
	pxor	%xmm0, %xmm0
	leaq	2096(%r14), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r12, %rbx
	je	.L521
	leaq	-88(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L516:
	cmpq	$56, %rbx
	je	.L512
	movq	-56(%rbx), %rax
	leaq	-56(%rbx), %r15
	movq	%r15, %rdi
	call	*56(%rax)
	testb	%al, %al
	je	.L512
	cmpl	$2, 16(%rbx)
	je	.L512
	movq	24(%rbx), %rdi
	call	uv_has_ref@PLT
	testl	%eax, %eax
	je	.L512
	movq	%r15, %rdi
	call	_ZN4node9AsyncWrap8GetOwnerEv@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -88(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L514
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, -72(%rbp)
.L512:
	movq	8(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L516
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
.L510:
	movq	352(%r14), %rdi
	movq	0(%r13), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L534
	movq	(%rax), %rax
.L518:
	movq	-80(%rbp), %rdi
	movq	%rax, 24(%rbx)
	testq	%rdi, %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L535
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	movq	-104(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L521:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L534:
	movq	16(%rbx), %rax
	jmp	.L518
.L535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7716:
	.size	_ZN4node16GetActiveHandlesERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node16GetActiveHandlesERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL17GetActiveRequestsERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL17GetActiveRequestsERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L550
	cmpw	$1040, %cx
	jne	.L537
.L550:
	movq	23(%rdx), %r14
.L539:
	movq	2120(%r14), %rbx
	pxor	%xmm0, %xmm0
	leaq	2112(%r14), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r12, %rbx
	je	.L549
	leaq	-88(%rbp), %r15
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L557:
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, -72(%rbp)
.L541:
	movq	8(%rbx), %rbx
	cmpq	%rbx, %r12
	je	.L556
.L544:
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %rdi
	call	*24(%rax)
	cmpq	$0, 8(%rax)
	movq	%rax, %rdi
	je	.L541
	call	_ZN4node9AsyncWrap8GetOwnerEv@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -88(%rbp)
	cmpq	-64(%rbp), %rsi
	jne	.L557
	leaq	-80(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_6ObjectEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	8(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L544
	.p2align 4,,10
	.p2align 3
.L556:
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
.L540:
	movq	352(%r14), %rdi
	movq	0(%r13), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L558
	movq	(%rax), %rax
.L546:
	movq	-80(%rbp), %rdi
	movq	%rax, 24(%rbx)
	testq	%rdi, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L549:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L558:
	movq	16(%rbx), %rax
	jmp	.L546
.L559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7706:
	.size	_ZN4nodeL17GetActiveRequestsERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL17GetActiveRequestsERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB9439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L561
	testq	%r14, %r14
	je	.L577
.L561:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L578
	cmpq	$1, %r13
	jne	.L564
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L565:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L579
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L565
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L563:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L565
.L577:
	leaq	.LC32(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9439:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	.type	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_, @function
_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_:
.LFB9462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	16(%rsi), %r14
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L581
	testq	%r14, %r14
	je	.L600
.L581:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L601
	cmpq	$1, %r13
	jne	.L584
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L585:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
.L588:
	movq	%r14, %rdi
	call	free@PLT
.L580:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L602
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L583
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%r14, %r14
	je	.L580
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L601:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L583:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L585
.L600:
	leaq	.LC32(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L602:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9462:
	.size	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_, .-_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	.section	.rodata._ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC33:
	.string	"lz"
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$37, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$1208, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -1224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-1224(%rbp), %r8
	testq	%rax, %rax
	jne	.L604
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L604:
	movq	%rax, %r9
	leaq	-1184(%rbp), %rax
	leaq	-1200(%rbp), %r14
	movq	%r9, %rbx
	movq	%rax, -1224(%rbp)
	subq	%r13, %rbx
	movq	%rax, -1200(%rbp)
	movq	%rbx, -1208(%rbp)
	cmpq	$15, %rbx
	jbe	.L605
	leaq	-1208(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r8, -1240(%rbp)
	movq	%r9, -1232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1240(%rbp), %r8
	movq	-1232(%rbp), %r9
	movq	%rax, -1200(%rbp)
	movq	-1208(%rbp), %rax
	movq	%rax, -1184(%rbp)
.L605:
	movq	-1200(%rbp), %rax
	cmpq	$1, %rbx
	jne	.L606
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L607
.L606:
	testq	%rbx, %rbx
	je	.L607
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L607:
	movq	-1208(%rbp), %rax
	movq	-1200(%rbp), %rdx
	leaq	.LC33(%rip), %r13
	movq	%rax, -1192(%rbp)
	movb	$0, (%rdx,%rax)
.L608:
	movq	%r9, %rbx
	movq	%r13, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -1248(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -1240(%rbp)
	movb	%sil, -1232(%rbp)
	call	strchr@PLT
	movb	-1232(%rbp), %dl
	movq	-1240(%rbp), %r9
	testq	%rax, %rax
	movq	-1248(%rbp), %r8
	jne	.L608
	cmpb	$120, %dl
	leaq	-1104(%rbp), %r13
	leaq	-1088(%rbp), %r15
	jg	.L609
	cmpb	$99, %dl
	jg	.L610
	cmpb	$37, %dl
	leaq	-1136(%rbp), %rax
	je	.L611
	cmpb	$88, %dl
	je	.L612
	jmp	.L609
.L610:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L609
	leaq	.L614(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L614:
	.long	.L615-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L615-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L617-.L614
	.long	.L616-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L615-.L614
	.long	.L609-.L614
	.long	.L615-.L614
	.long	.L609-.L614
	.long	.L609-.L614
	.long	.L613-.L614
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L611:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rax, -1232(%rbp)
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-1232(%rbp), %rax
	movl	$37, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-1232(%rbp), %rax
	movq	%r13, %rdx
	movq	%rax, %rsi
	jmp	.L682
.L609:
	movq	%r8, %rdx
	movq	%r9, %rsi
	leaq	-1136(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdx
	movq	%rbx, %rsi
.L682:
	movq	%r12, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L678
	call	_ZdlPv@PLT
	jmp	.L678
.L615:
	movq	(%r8), %rcx
	movq	16(%r8), %r8
	leaq	-1152(%rbp), %rax
	movq	%rax, -1232(%rbp)
	movq	%rax, -1168(%rbp)
	movq	%r8, %rax
	addq	%rcx, %rax
	je	.L623
	testq	%r8, %r8
	jne	.L623
.L629:
	leaq	.LC32(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L623:
	movq	%rcx, -1208(%rbp)
	cmpq	$15, %rcx
	jbe	.L624
	leaq	-1208(%rbp), %rsi
	leaq	-1168(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -1248(%rbp)
	movq	%rcx, -1240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1248(%rbp), %r8
	movq	-1240(%rbp), %rcx
	movq	%rax, -1168(%rbp)
	movq	-1208(%rbp), %rax
	movq	%rax, -1152(%rbp)
.L624:
	movq	-1168(%rbp), %rax
	cmpq	$1, %rcx
	jne	.L625
	movb	(%r8), %dl
	movb	%dl, (%rax)
	jmp	.L626
.L625:
	testq	%rcx, %rcx
	je	.L626
	movq	%rax, %rdi
	movq	%r8, %rsi
	rep movsb
.L626:
	movq	-1208(%rbp), %rax
	movq	-1168(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -1160(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-1160(%rbp), %rdx
	movq	-1168(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1168(%rbp), %rdi
	cmpq	-1232(%rbp), %rdi
	jne	.L675
	jmp	.L628
.L617:
	movq	%r8, %rsi
	movl	$262, %ecx
	movq	%r13, %rdi
	rep movsl
	leaq	-1120(%rbp), %rax
	movq	%rax, -1232(%rbp)
	movq	-1088(%rbp), %r8
	movq	-1104(%rbp), %rcx
	movq	%rax, -1136(%rbp)
	movq	%r8, %rax
	addq	%rcx, %rax
	je	.L647
	testq	%r8, %r8
	je	.L629
.L647:
	movq	%rcx, -1208(%rbp)
	cmpq	$15, %rcx
	jbe	.L631
	leaq	-1208(%rbp), %rsi
	leaq	-1136(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -1248(%rbp)
	movq	%rcx, -1240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1248(%rbp), %r8
	movq	-1240(%rbp), %rcx
	movq	%rax, -1136(%rbp)
	movq	-1208(%rbp), %rax
	movq	%rax, -1120(%rbp)
.L631:
	movq	-1136(%rbp), %rax
	cmpq	$1, %rcx
	jne	.L632
	movb	(%r8), %dl
	movb	%dl, (%rax)
	jmp	.L633
.L632:
	testq	%rcx, %rcx
	je	.L633
	movq	%rax, %rdi
	movq	%r8, %rsi
	rep movsb
.L633:
	movq	-1208(%rbp), %rax
	movq	-1136(%rbp), %rdx
	movq	%rax, -1128(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %r8
	je	.L634
	testq	%r8, %r8
	je	.L634
	movq	%r8, %rdi
	call	free@PLT
.L634:
	movq	-1128(%rbp), %rdx
	movq	-1136(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1136(%rbp), %rdi
	cmpq	-1232(%rbp), %rdi
	jne	.L675
	jmp	.L628
.L613:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	movq	-1096(%rbp), %rdx
	movq	-1104(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1104(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L675
	jmp	.L628
.L612:
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	movq	-1128(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r15, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	xorl	%eax, %eax
.L639:
	movq	-1104(%rbp), %rsi
	cmpq	%rax, -1128(%rbp)
	jbe	.L637
	movq	-1136(%rbp), %rdx
	addq	%rax, %rsi
	movb	(%rdx,%rax), %dl
	leal	-97(%rdx), %ecx
	cmpb	$25, %cl
	ja	.L638
	subl	$32, %edx
.L638:
	movb	%dl, (%rsi)
	incq	%rax
	jmp	.L639
.L637:
	movq	-1096(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1104(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L628
.L675:
	call	_ZdlPv@PLT
	jmp	.L628
.L616:
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L628:
	leaq	2(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-1200(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	-1192(%rbp), %r8
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	jne	.L642
	movups	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L643
.L642:
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L643:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
.L678:
	movq	-1104(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L620
	call	_ZdlPv@PLT
.L620:
	movq	-1200(%rbp), %rdi
	cmpq	-1224(%rbp), %rdi
	je	.L603
	call	_ZdlPv@PLT
.L603:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L646
	call	__stack_chk_fail@PLT
.L646:
	addq	$1208, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9197:
	.size	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB8889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L684
	call	__stack_chk_fail@PLT
.L684:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8889:
	.size	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_:
.LFB8452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L686
	call	_ZdlPv@PLT
.L686:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L688
	call	__stack_chk_fail@PLT
.L688:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8452:
	.size	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC34:
	.string	"%s\n"
	.text
	.p2align 4
	.globl	_ZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$1064, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	jne	.L691
	movq	8(%rdi), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L691
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L691
	movq	(%rdi), %rax
	leaq	-1072(%rbp), %r12
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC34(%rip), %rsi
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movq	-1056(%rbp), %rdi
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L690
	testq	%rdi, %rdi
	je	.L690
	call	free@PLT
.L690:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L702
	addq	$1064, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	leaq	_ZZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L702:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7701:
	.size	_ZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11per_process11umask_mutexE, @function
_GLOBAL__sub_I__ZN4node11per_process11umask_mutexE:
.LFB9914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_process11umask_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L706
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node11per_process11umask_mutexE(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
.L706:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9914:
	.size	_GLOBAL__sub_I__ZN4node11per_process11umask_mutexE, .-_GLOBAL__sub_I__ZN4node11per_process11umask_mutexE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11per_process11umask_mutexE
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC35:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC37:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = node::Utf8Value&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC38:
	.string	"../src/debug_utils-inl.h:76"
.LC39:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC37
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC40:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC42:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"../src/node_process_methods.cc"
	.section	.rodata.str1.1
.LC44:
	.string	"process_methods"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC43
	.quad	0
	.quad	_ZN4nodeL24InitializeProcessMethodsEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.quad	.LC44
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"../src/node_process_methods.cc:329"
	.section	.rodata.str1.1
.LC46:
	.string	"args[0]->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"void node::DebugProcess(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL12DebugProcessERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"../src/node_process_methods.cc:299"
	.section	.rodata.str1.1
.LC49:
	.string	"(array->Length()) == (16)"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"void node::ResourceUsage(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"../src/node_process_methods.cc:297"
	.section	.rodata.str1.1
.LC52:
	.string	"args[0]->IsFloat64Array()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL13ResourceUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC50
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"../src/node_process_methods.cc:234"
	.align 8
.LC54:
	.string	"args[0]->IsUndefined() || args[0]->IsUint32()"
	.align 8
.LC55:
	.string	"void node::Umask(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, @object
	.size	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, 24
_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"../src/node_process_methods.cc:233"
	.section	.rodata.str1.1
.LC57:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC55
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"../src/node_process_methods.cc:232"
	.align 8
.LC59:
	.string	"env->has_run_bootstrapping_code()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL5UmaskERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC55
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"../src/node_process_methods.cc:213"
	.align 8
.LC61:
	.string	"args.Length() == 1 && args[0]->IsString() && \"must be called with a single string\""
	.align 8
.LC62:
	.string	"void node::RawDebug(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"../src/node_process_methods.cc:200"
	.section	.rodata.str1.1
.LC64:
	.string	"(array->Length()) == (5)"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"void node::MemoryUsage(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"../src/node_process_methods.cc:198"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL11MemoryUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC66
	.quad	.LC52
	.quad	.LC65
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"../src/node_process_methods.cc:113"
	.align 8
.LC68:
	.string	"void node::Cwd(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL3CwdERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC67
	.quad	.LC59
	.quad	.LC68
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"../src/node_process_methods.cc:102"
	.section	.rodata.str1.1
.LC70:
	.string	"(array->Length()) == (2)"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"void node::CPUUsage(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"../src/node_process_methods.cc:100"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL8CPUUsageERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC72
	.quad	.LC52
	.quad	.LC71
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"../src/node_process_methods.cc:70"
	.section	.rodata.str1.1
.LC74:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"void node::Chdir(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, @object
	.size	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, 24
_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1:
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"../src/node_process_methods.cc:69"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC76
	.quad	.LC57
	.quad	.LC75
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"../src/node_process_methods.cc:67"
	.section	.rodata.str1.1
.LC78:
	.string	"env->owns_process_state()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL5ChdirERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC75
	.globl	_ZN4node11per_process11umask_mutexE
	.bss
	.align 32
	.type	_ZN4node11per_process11umask_mutexE, @object
	.size	_ZN4node11per_process11umask_mutexE, 40
_ZN4node11per_process11umask_mutexE:
	.zero	40
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC79:
	.string	"../src/debug_utils-inl.h:67"
.LC80:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1093567616
	.align 8
.LC2:
	.long	0
	.long	1104006501
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
