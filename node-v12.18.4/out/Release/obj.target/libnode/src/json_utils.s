	.file	"json_utils.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\\\\"
.LC1:
	.string	"\\\""
.LC2:
	.string	"basic_string::substr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.text
	.p2align 4
	.globl	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB2388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$12592, %edx
	movl	$12848, %ecx
	movl	$13104, %r8d
	movl	$13360, %r9d
	movl	$13616, %r10d
	movl	$13872, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$28252, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	$29788, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$25180, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$14128, %ebx
	subq	$1192, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1072(%rbp), %rax
	movw	%dx, -1036(%rbp)
	movl	$12337, %edx
	movq	%rax, -1088(%rbp)
	movl	$12336, %eax
	movw	%ax, -1068(%rbp)
	leaq	-1040(%rbp), %rax
	movq	%rax, -1056(%rbp)
	leaq	-1008(%rbp), %rax
	movq	%rax, -1024(%rbp)
	leaq	-976(%rbp), %rax
	movq	%rax, -992(%rbp)
	leaq	-944(%rbp), %rax
	movq	%rax, -960(%rbp)
	leaq	-912(%rbp), %rax
	movw	%cx, -1004(%rbp)
	movl	$12593, %ecx
	movw	%r8w, -972(%rbp)
	movl	$12849, %r8d
	movw	%r9w, -940(%rbp)
	movl	$13105, %r9d
	movl	$808482140, -1072(%rbp)
	movq	$6, -1080(%rbp)
	movb	$0, -1066(%rbp)
	movl	$808482140, -1040(%rbp)
	movq	$6, -1048(%rbp)
	movb	$0, -1034(%rbp)
	movl	$808482140, -1008(%rbp)
	movq	$6, -1016(%rbp)
	movb	$0, -1002(%rbp)
	movl	$808482140, -976(%rbp)
	movq	$6, -984(%rbp)
	movb	$0, -970(%rbp)
	movl	$808482140, -944(%rbp)
	movq	$6, -952(%rbp)
	movq	%rax, -928(%rbp)
	leaq	-880(%rbp), %rax
	movq	%rax, -896(%rbp)
	leaq	-848(%rbp), %rax
	movq	%rax, -864(%rbp)
	leaq	-816(%rbp), %rax
	movq	%rax, -832(%rbp)
	leaq	-784(%rbp), %rax
	movq	%rax, -800(%rbp)
	leaq	-752(%rbp), %rax
	movq	%rax, -768(%rbp)
	leaq	-720(%rbp), %rax
	movq	%rax, -736(%rbp)
	movl	$30300, %eax
	movw	%ax, -720(%rbp)
	leaq	-688(%rbp), %rax
	movw	%r10w, -908(%rbp)
	movl	$13361, %r10d
	movw	%r11w, -876(%rbp)
	movl	$13617, %r11d
	movw	%bx, -844(%rbp)
	movl	$13873, %ebx
	movw	%r12w, -816(%rbp)
	movl	$14129, %r12d
	movw	%r14w, -784(%rbp)
	movl	$14385, %r14d
	movw	%r15w, -752(%rbp)
	movl	$14641, %r15d
	movb	$0, -938(%rbp)
	movl	$808482140, -912(%rbp)
	movq	$6, -920(%rbp)
	movb	$0, -906(%rbp)
	movl	$808482140, -880(%rbp)
	movq	$6, -888(%rbp)
	movb	$0, -874(%rbp)
	movl	$808482140, -848(%rbp)
	movq	$6, -856(%rbp)
	movb	$0, -842(%rbp)
	movq	$2, -824(%rbp)
	movb	$0, -814(%rbp)
	movq	$2, -792(%rbp)
	movb	$0, -782(%rbp)
	movq	$2, -760(%rbp)
	movb	$0, -750(%rbp)
	movq	$2, -728(%rbp)
	movb	$0, -718(%rbp)
	movq	%rax, -704(%rbp)
	movl	$26204, %eax
	movw	%ax, -688(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rax, -672(%rbp)
	movl	$29276, %eax
	movw	%ax, -656(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rax, -640(%rbp)
	movl	$25904, %eax
	movw	%ax, -620(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -608(%rbp)
	movl	$26160, %eax
	movw	%ax, -588(%rbp)
	leaq	-560(%rbp), %rax
	movq	%rax, -576(%rbp)
	leaq	-528(%rbp), %rax
	movq	%rax, -544(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, -512(%rbp)
	leaq	-464(%rbp), %rax
	movw	%dx, -556(%rbp)
	movl	$26161, %edx
	movq	$2, -696(%rbp)
	movb	$0, -686(%rbp)
	movq	$2, -664(%rbp)
	movb	$0, -654(%rbp)
	movl	$808482140, -624(%rbp)
	movq	$6, -632(%rbp)
	movb	$0, -618(%rbp)
	movl	$808482140, -592(%rbp)
	movq	$6, -600(%rbp)
	movb	$0, -586(%rbp)
	movl	$808482140, -560(%rbp)
	movq	$6, -568(%rbp)
	movb	$0, -554(%rbp)
	movl	$808482140, -528(%rbp)
	movw	%cx, -524(%rbp)
	movq	$6, -536(%rbp)
	movb	$0, -522(%rbp)
	movl	$808482140, -496(%rbp)
	movw	%r8w, -492(%rbp)
	movq	$6, -504(%rbp)
	movb	$0, -490(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-432(%rbp), %rax
	movq	%rax, -448(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rax, -416(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, -384(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -352(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -320(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -288(%rbp)
	leaq	-240(%rbp), %rax
	movl	$808482140, -464(%rbp)
	movw	%r9w, -460(%rbp)
	movq	$6, -472(%rbp)
	movb	$0, -458(%rbp)
	movl	$808482140, -432(%rbp)
	movw	%r10w, -428(%rbp)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
	movl	$808482140, -400(%rbp)
	movw	%r11w, -396(%rbp)
	movq	$6, -408(%rbp)
	movb	$0, -394(%rbp)
	movl	$808482140, -368(%rbp)
	movw	%bx, -364(%rbp)
	movq	$6, -376(%rbp)
	movb	$0, -362(%rbp)
	movl	$808482140, -336(%rbp)
	movw	%r12w, -332(%rbp)
	movq	$6, -344(%rbp)
	movb	$0, -330(%rbp)
	movl	$808482140, -304(%rbp)
	movw	%r14w, -300(%rbp)
	movq	$6, -312(%rbp)
	movb	$0, -298(%rbp)
	movl	$808482140, -272(%rbp)
	movw	%r15w, -268(%rbp)
	movq	$6, -280(%rbp)
	movq	%rax, -256(%rbp)
	movl	$24881, %eax
	movw	%ax, -236(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	$25137, %eax
	movw	%ax, -204(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$25393, %eax
	movw	%ax, -172(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -160(%rbp)
	movl	$25649, %eax
	movw	%ax, -140(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -128(%rbp)
	movl	$25905, %eax
	movw	%ax, -108(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	16(%rdi), %rax
	movb	$0, -266(%rbp)
	movl	$808482140, -240(%rbp)
	movq	$6, -248(%rbp)
	movb	$0, -234(%rbp)
	movl	$808482140, -208(%rbp)
	movq	$6, -216(%rbp)
	movb	$0, -202(%rbp)
	movl	$808482140, -176(%rbp)
	movq	$6, -184(%rbp)
	movb	$0, -170(%rbp)
	movl	$808482140, -144(%rbp)
	movq	$6, -152(%rbp)
	movb	$0, -138(%rbp)
	movl	$808482140, -112(%rbp)
	movq	$6, -120(%rbp)
	movb	$0, -106(%rbp)
	movl	$808482140, -80(%rbp)
	movw	%dx, -76(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	cmpq	$0, 8(%rsi)
	je	.L3
	leaq	-1184(%rbp), %rax
	movq	%rsi, %rbx
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movq	%rax, -1216(%rbp)
	leaq	-1168(%rbp), %rax
	movq	%rax, -1208(%rbp)
	leaq	-1136(%rbp), %rax
	movq	%rax, -1224(%rbp)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	-1208(%rbp), %rax
	movb	$0, -1168(%rbp)
	movq	$0, -1176(%rbp)
	movq	%rax, -1184(%rbp)
	movq	(%rbx), %rax
	movsbq	(%rax,%r14), %rax
	cmpb	$92, %al
	je	.L49
	cmpb	$34, %al
	je	.L50
	leaq	1(%r14), %r15
	cmpq	$31, %rax
	jbe	.L51
.L8:
	movq	8(%rbx), %rax
	movq	%r15, %r14
	cmpq	%r15, %rax
	ja	.L2
.L20:
	cmpq	%rax, %r12
	jb	.L52
.L3:
	leaq	-1088(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L53:
	call	_ZdlPv@PLT
	leaq	-32(%rbx), %rax
	cmpq	%rbx, %r12
	je	.L1
.L31:
	movq	%rax, %rbx
.L32:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	jne	.L53
	leaq	-32(%rbx), %rax
	cmpq	%rbx, %r12
	jne	.L31
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$1192, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	salq	$5, %rax
	movq	-1216(%rbp), %rdi
	leaq	-1088(%rbp,%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L5:
	movq	-1176(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L55
.L9:
	cmpq	%r12, %r14
	ja	.L56
.L11:
	movq	-1184(%rbp), %rsi
	movq	%r13, %rdi
	leaq	1(%r14), %r15
	movq	%r15, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1184(%rbp), %rdi
.L10:
	cmpq	-1208(%rbp), %rdi
	je	.L8
	call	_ZdlPv@PLT
	movq	8(%rbx), %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jb	.L2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-1216(%rbp), %rdi
	xorl	%edx, %edx
	movl	$2, %r8d
	xorl	%esi, %esi
	leaq	.LC0(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1176(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L9
.L55:
	movq	-1184(%rbp), %rdi
	leaq	1(%r14), %r15
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L56:
	movq	8(%rbx), %rcx
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	%r12, %rcx
	jb	.L57
	movq	(%rbx), %r15
	subq	%r12, %rcx
	movq	-1224(%rbp), %rsi
	addq	%r12, %r15
	cmpq	%rax, %rcx
	movq	%rsi, -1152(%rbp)
	cmovbe	%rcx, %rax
	movq	%rax, %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L13
	testq	%r15, %r15
	je	.L22
.L13:
	movq	%r12, -1192(%rbp)
	cmpq	$15, %r12
	ja	.L58
	cmpq	$1, %r12
	jne	.L16
	movzbl	(%r15), %eax
	movb	%al, -1136(%rbp)
	movq	-1224(%rbp), %rax
.L17:
	movq	%r12, -1144(%rbp)
	movq	%r13, %rdi
	movb	$0, (%rax,%r12)
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1224(%rbp), %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	-1176(%rbp), %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-1216(%rbp), %rdi
	movl	$2, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC1(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	testq	%r12, %r12
	jne	.L59
	movq	-1224(%rbp), %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	-1152(%rbp), %rdi
	leaq	-1192(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1152(%rbp)
	movq	%rax, %rdi
	movq	-1192(%rbp), %rax
	movq	%rax, -1136(%rbp)
.L15:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-1192(%rbp), %r12
	movq	-1152(%rbp), %rax
	jmp	.L17
.L52:
	movq	(%rbx), %rcx
	subq	%r12, %rax
	leaq	-1104(%rbp), %r14
	movq	%r14, -1120(%rbp)
	addq	%r12, %rcx
	movq	%rcx, %rbx
	movq	%r15, %rcx
	subq	%r12, %rcx
	cmpq	%rax, %rcx
	cmovbe	%rcx, %rax
	movq	%rax, %r12
	movq	%rbx, %rax
	addq	%r12, %rax
	je	.L37
	testq	%rbx, %rbx
	je	.L22
.L37:
	movq	%r12, -1192(%rbp)
	cmpq	$15, %r12
	ja	.L60
	cmpq	$1, %r12
	jne	.L26
	movzbl	(%rbx), %eax
	movb	%al, -1104(%rbp)
	movq	%r14, %rax
.L27:
	movq	%r12, -1112(%rbp)
	movq	%r13, %rdi
	movb	$0, (%rax,%r12)
	movq	-1112(%rbp), %rdx
	movq	-1120(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1120(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L3
	call	_ZdlPv@PLT
	jmp	.L3
.L26:
	testq	%r12, %r12
	jne	.L61
	movq	%r14, %rax
	jmp	.L27
.L60:
	leaq	-1120(%rbp), %rdi
	leaq	-1192(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1120(%rbp)
	movq	%rax, %rdi
	movq	-1192(%rbp), %rax
	movq	%rax, -1104(%rbp)
.L25:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1192(%rbp), %r12
	movq	-1120(%rbp), %rax
	jmp	.L27
.L61:
	movq	%r14, %rdi
	jmp	.L25
.L57:
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L54:
	call	__stack_chk_fail@PLT
.L59:
	movq	-1224(%rbp), %rdi
	jmp	.L15
.L22:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE2388:
	.size	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC5:
	.string	"basic_string::append"
	.text
	.p2align 4
	.globl	_ZN4node8ReindentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.type	_ZN4node8ReindentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, @function
_ZN4node8ReindentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi:
.LFB2389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L88
	leaq	-112(%rbp), %rax
	movslq	%edx, %rsi
	leaq	-128(%rbp), %rdi
	movl	$32, %edx
	movq	%rax, -160(%rbp)
	xorl	%r14d, %r14d
	leaq	-96(%rbp), %r12
	movq	%rax, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	leaq	-80(%rbp), %rax
	movb	$0, -80(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L70:
	movq	8(%r15), %r9
	addq	$1, %rbx
	movq	(%r15), %rsi
	movq	%rbx, %rdx
	movq	%r9, %rax
	subq	%r14, %rdx
	subq	%r14, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	cmpq	%r9, %r14
	ja	.L87
	addq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, %r14
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L75:
	movq	%r14, %rdx
	movl	$10, %esi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEcm@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	cmpq	$-1, %rbx
	jne	.L70
	movq	8(%r15), %r9
	movq	(%r15), %rsi
	movq	%r9, %rdx
	subq	%r14, %rdx
	cmpq	%r9, %r14
	ja	.L87
	addq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	movq	-96(%rbp), %rax
	cmpq	-152(%rbp), %rax
	je	.L89
	movq	%rax, 0(%r13)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%r13)
.L73:
	movq	-88(%rbp), %rax
	movq	-128(%rbp), %rdi
	movq	%rax, 8(%r13)
	cmpq	-160(%rbp), %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	leaq	16(%rdi), %rdi
	movq	8(%rsi), %r12
	movq	%rdi, 0(%r13)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L64
	testq	%r14, %r14
	je	.L91
.L64:
	movq	%r12, -136(%rbp)
	cmpq	$15, %r12
	ja	.L92
	cmpq	$1, %r12
	jne	.L67
	movzbl	(%r14), %eax
	movq	0(%r13), %rdi
	movb	%al, 16(%r13)
.L68:
	movq	%r12, 8(%r13)
	movb	$0, (%rdi,%r12)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L89:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 16(%r13)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L67:
	testq	%r12, %r12
	je	.L68
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%r13, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r13)
.L66:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r12
	movq	0(%r13), %rdi
	jmp	.L68
.L87:
	movq	%r9, %rcx
	movq	%r14, %rdx
	leaq	.LC5(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L90:
	call	__stack_chk_fail@PLT
.L91:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE2389:
	.size	_ZN4node8ReindentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, .-_ZN4node8ReindentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
