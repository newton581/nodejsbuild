	.file	"node_report_module.cc"
	.text
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7813:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7813:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB7870:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7870:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB7871:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7871:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB7887:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7887:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB10024:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE10024:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB10028:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE10028:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7906:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L13:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7906:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7905:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7905:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.text
	.p2align 4
	.type	_ZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jg	.L21
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L27
.L23:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L28
	movq	8(%rbx), %rdi
.L25:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rbx
	call	_ZNK2v85Value6IsTrueEv@PLT
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	movb	%al, 307(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L23
.L27:
	leaq	_ZZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L25
	.cfi_endproc
.LFE7960:
	.size	_ZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL24ShouldReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL24ShouldReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	(%rbx), %rdx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	cmpb	$1, 307(%rax)
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE7959:
	.size	_ZN6reportL24ShouldReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL24ShouldReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL10GetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL10GetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	(%rbx), %rdx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	cmpb	$1, 308(%rax)
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE7948:
	.size	_ZN6reportL10GetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL10GetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.globl	_ZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L65
	cmpw	$1040, %cx
	jne	.L38
.L65:
	movq	23(%rdx), %r13
.L40:
	movq	352(%r13), %r12
	leaq	-160(%rbp), %r14
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-112(%rbp), %rax
	cmpl	$4, 16(%rbx)
	movq	$0, -120(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rax, -128(%rbp)
	movb	$0, -112(%rbp)
	jne	.L80
	movq	8(%rbx), %rdx
	leaq	-208(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jle	.L81
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdx
.L43:
	leaq	-192(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movl	16(%rbx), %eax
	cmpl	$2, %eax
	jg	.L44
	movq	(%rbx), %rdx
	leaq	-128(%rbp), %r9
	movq	8(%rdx), %rdx
	movq	88(%rdx), %rcx
	leaq	88(%rdx), %rdi
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L45
	movq	-1(%rcx), %rdx
	leaq	-128(%rbp), %r9
	cmpw	$63, 11(%rdx)
	jbe	.L82
.L47:
	cmpl	$3, %eax
	jle	.L83
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
.L49:
	movq	%r9, -232(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-232(%rbp), %r9
	testb	%al, %al
	je	.L51
	cmpl	$3, 16(%rbx)
	jg	.L52
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	leaq	88(%rdx), %rdi
.L45:
	cmpq	$-88, %rdx
	jne	.L49
.L51:
	xorl	%eax, %eax
.L50:
	subq	$8, %rsp
	movq	%r13, %rdx
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	pushq	%rax
	movq	-208(%rbp), %rcx
	leaq	-80(%rbp), %r13
	movq	-192(%rbp), %r8
	call	_ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE@PLT
	movq	-96(%rbp), %rdx
	popq	%rax
	movq	-128(%rbp), %rdi
	popq	%rcx
	cmpq	%r13, %rdx
	je	.L84
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	cmpq	-216(%rbp), %rdi
	je	.L85
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	-112(%rbp), %rsi
	movq	%rdx, -128(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -120(%rbp)
	testq	%rdi, %rdi
	je	.L59
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L57:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movq	(%rbx), %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L86
	movq	(%rax), %rax
	movq	%rax, 24(%r13)
.L63:
	movq	-224(%rbp), %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	-128(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	leaq	-128(%rbp), %r9
	movq	-16(%rdi), %rcx
	movq	%rcx, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L47
	movq	-1(%rcx), %rdx
	subq	$16, %rdi
	leaq	-128(%rbp), %r9
	cmpw	$63, 11(%rdx)
	ja	.L47
.L82:
	leaq	-176(%rbp), %r11
	movq	%rdi, %rdx
	movq	%r12, %rsi
	movq	%r9, -248(%rbp)
	movq	%r11, %rdi
	movq	%r11, -240(%rbp)
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-176(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -232(%rbp)
	call	strlen@PLT
	movq	-248(%rbp), %r9
	movq	-232(%rbp), %rcx
	xorl	%esi, %esi
	movq	-120(%rbp), %rdx
	movq	%rax, %r8
	movq	%r9, %rdi
	movq	%r9, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-240(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movl	16(%rbx), %eax
	movq	-232(%rbp), %r9
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L84:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L55
	cmpq	$1, %rdx
	je	.L88
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rdi
.L55:
	movq	%rdx, -120(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%rbx), %rax
	subq	$24, %rax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, -128(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -120(%rbp)
.L59:
	movq	%r13, -96(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	_ZZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r13), %rax
	movq	%rax, 24(%r13)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L88:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rdi
	jmp	.L55
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7946:
	.size	_ZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL20ShouldReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL20ShouldReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L107
	cmpw	$1040, %cx
	jne	.L90
.L107:
	movq	23(%rdx), %rax
.L92:
	movq	360(%rax), %rax
	movq	2408(%rax), %r12
	movq	2400(%rax), %rdx
	testq	%r12, %r12
	je	.L93
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	leaq	8(%r12), %rax
	testq	%rbx, %rbx
	je	.L94
	lock addl	$1, (%rax)
	cmpb	$0, 27(%rdx)
	movq	8(%rdi), %rcx
	jne	.L106
.L111:
	movq	120(%rcx), %rdx
	movq	%rdx, 24(%rdi)
	testq	%rbx, %rbx
	je	.L96
.L112:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L110
.L89:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	addl	$1, 8(%r12)
	cmpb	$0, 27(%rdx)
	movq	8(%rdi), %rcx
	je	.L111
.L106:
	movq	112(%rcx), %rdx
	movq	%rdx, 24(%rdi)
	testq	%rbx, %rbx
	jne	.L112
.L96:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L89
.L110:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L100
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L101:
	cmpl	$1, %eax
	jne	.L89
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	cmpb	$0, 27(%rdx)
	movq	8(%rdi), %rax
	jne	.L113
	movq	120(%rax), %rax
	movq	%rax, 24(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	112(%rax), %rax
	movq	%rax, 24(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r12), %rdi
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L101
	.cfi_endproc
.LFE7961:
	.size	_ZN6reportL20ShouldReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL20ShouldReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL31ShouldReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL31ShouldReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L132
	cmpw	$1040, %cx
	jne	.L115
.L132:
	movq	23(%rdx), %rax
.L117:
	movq	360(%rax), %rax
	movq	2408(%rax), %r12
	movq	2400(%rax), %rdx
	testq	%r12, %r12
	je	.L118
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	leaq	8(%r12), %rax
	testq	%rbx, %rbx
	je	.L119
	lock addl	$1, (%rax)
	cmpb	$0, 26(%rdx)
	movq	8(%rdi), %rcx
	jne	.L131
.L136:
	movq	120(%rcx), %rdx
	movq	%rdx, 24(%rdi)
	testq	%rbx, %rbx
	je	.L121
.L137:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L135
.L114:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	addl	$1, 8(%r12)
	cmpb	$0, 26(%rdx)
	movq	8(%rdi), %rcx
	je	.L136
.L131:
	movq	112(%rcx), %rdx
	movq	%rdx, 24(%rdi)
	testq	%rbx, %rbx
	jne	.L137
.L121:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L114
.L135:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L125
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L126:
	cmpl	$1, %eax
	jne	.L114
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	cmpb	$0, 26(%rdx)
	movq	8(%rdi), %rax
	jne	.L138
	movq	120(%rax), %rax
	movq	%rax, 24(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	112(%rax), %rax
	movq	%rax, 24(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r12), %rdi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L125:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L126
	.cfi_endproc
.LFE7963:
	.size	_ZN6reportL31ShouldReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL31ShouldReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL10SetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL10SetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L145
	cmpw	$1040, %cx
	jne	.L140
.L145:
	movq	23(%rdx), %rax
.L142:
	movq	352(%rax), %rsi
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L143
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L144:
	call	_ZNK2v85Value9ToBooleanEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	call	_ZNK2v87Boolean5ValueEv@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rdx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	movb	%al, 308(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L142
	.cfi_endproc
.LFE7949:
	.size	_ZN6reportL10SetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL10SetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L167
	cmpw	$1040, %cx
	jne	.L148
.L167:
	movq	23(%rdx), %r12
.L150:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L151
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L170
.L153:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L171
	movq	8(%rbx), %rdi
.L155:
	movq	360(%r12), %rax
	movq	2408(%rax), %r12
	movq	2400(%rax), %r14
	testq	%r12, %r12
	je	.L156
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	leaq	8(%r12), %rbx
	testq	%r13, %r13
	je	.L157
	lock addl	$1, (%rbx)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, 27(%r14)
	testq	%r13, %r13
	je	.L172
.L165:
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L173
.L147:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L151:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L153
.L170:
	leaq	_ZZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	addl	$1, 8(%r12)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, 27(%r14)
	testq	%r13, %r13
	jne	.L165
.L172:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L147
.L173:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L162
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L163:
	cmpl	$1, %eax
	jne	.L147
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	24(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, 27(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L162:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L163
	.cfi_endproc
.LFE7962:
	.size	_ZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L194
	cmpw	$1040, %cx
	jne	.L175
.L194:
	movq	23(%rdx), %r12
.L177:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L178
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L197
.L180:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L198
	movq	8(%rbx), %rdi
.L182:
	movq	360(%r12), %rax
	movq	2408(%rax), %r12
	movq	2400(%rax), %r14
	testq	%r12, %r12
	je	.L183
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	leaq	8(%r12), %rbx
	testq	%r13, %r13
	je	.L184
	lock addl	$1, (%rbx)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, 26(%r14)
	testq	%r13, %r13
	je	.L199
.L192:
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L200
.L174:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L178:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L180
.L197:
	leaq	_ZZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	addl	$1, 8(%r12)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, 26(%r14)
	testq	%r13, %r13
	jne	.L192
.L199:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L174
.L200:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L189
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L190:
	cmpl	$1, %eax
	jne	.L174
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	24(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, 26(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L189:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L190
	.cfi_endproc
.LFE7964:
	.size	_ZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7951:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	subq	$1056, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L211
	cmpw	$1040, %cx
	jne	.L202
.L211:
	movq	23(%rdx), %rax
.L204:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L205
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
.L206:
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L219
.L207:
	leaq	_ZZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L219:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L207
	movq	352(%rax), %rsi
	leaq	-1072(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %r12
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rbx
	movq	%r12, %rdi
	call	strlen@PLT
	movq	320(%rbx), %rdx
	leaq	312(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r12, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1056(%rbp), %rdi
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L209
	testq	%rdi, %rdi
	je	.L209
	call	free@PLT
.L209:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$1056, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L202:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L204
.L220:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7951:
	.size	_ZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	subq	$1056, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L231
	cmpw	$1040, %cx
	jne	.L222
.L231:
	movq	23(%rdx), %rax
.L224:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L225
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
.L226:
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L239
.L227:
	leaq	_ZZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L227
	movq	352(%rax), %rsi
	leaq	-1072(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %r12
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rbx
	movq	%r12, %rdi
	call	strlen@PLT
	movq	352(%rbx), %rdx
	leaq	344(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r12, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1056(%rbp), %rdi
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L229
	testq	%rdi, %rdi
	je	.L229
	call	free@PLT
.L229:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$1056, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L222:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L224
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7956:
	.size	_ZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.text
	.p2align 4
	.type	_ZN6reportL11GetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL11GetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L255
	cmpw	$1040, %cx
	jne	.L242
.L255:
	movq	23(%rdx), %r15
.L244:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	movq	344(%rax), %r14
	movq	352(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L245
	testq	%r14, %r14
	je	.L264
.L245:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L265
	cmpq	$1, %r12
	jne	.L248
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L249:
	movq	%r12, -88(%rbp)
	movl	$-1, %ecx
	xorl	%edx, %edx
	movb	$0, (%rax,%r12)
	movq	352(%r15), %rdi
	movq	-96(%rbp), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L266
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L252:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L268
	movq	%r13, %rax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L247:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L266:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L252
.L264:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L267:
	call	__stack_chk_fail@PLT
.L268:
	movq	%r13, %rdi
	jmp	.L247
	.cfi_endproc
.LFE7955:
	.size	_ZN6reportL11GetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL11GetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL12GetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL12GetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L283
	cmpw	$1040, %cx
	jne	.L270
.L283:
	movq	23(%rdx), %r15
.L272:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	movq	312(%rax), %r14
	movq	320(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L273
	testq	%r14, %r14
	je	.L292
.L273:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L293
	cmpq	$1, %r12
	jne	.L276
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L277:
	movq	%r12, -88(%rbp)
	movl	$-1, %ecx
	xorl	%edx, %edx
	movb	$0, (%rax,%r12)
	movq	352(%r15), %rdi
	movq	-96(%rbp), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L294
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L280:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L296
	movq	%r13, %rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L293:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L275:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L270:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L294:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L280
.L292:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L295:
	call	__stack_chk_fail@PLT
.L296:
	movq	%r13, %rdi
	jmp	.L275
	.cfi_endproc
.LFE7950:
	.size	_ZN6reportL12GetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL12GetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL9GetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL9GetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L320
	cmpw	$1040, %cx
	jne	.L298
.L320:
	movq	23(%rdx), %r15
.L300:
	movq	360(%r15), %rdx
	movq	2408(%rdx), %r13
	movq	2400(%rdx), %rax
	testq	%r13, %r13
	je	.L301
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L302
	lock addl	$1, 8(%r13)
.L301:
	movq	32(%rax), %r8
	movq	40(%rax), %r12
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	movq	%r8, %rax
	addq	%r12, %rax
	je	.L303
	testq	%r8, %r8
	je	.L331
.L303:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L332
	cmpq	$1, %r12
	jne	.L306
	movzbl	(%r8), %eax
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L307:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	testq	%r13, %r13
	je	.L309
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	testq	%r12, %r12
	je	.L310
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L333
	.p2align 4,,10
	.p2align 3
.L309:
	movq	352(%r15), %rdi
	movq	-96(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L334
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L317:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L336
	movq	%r14, %rax
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L302:
	addl	$1, 8(%r13)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L305:
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L310:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L309
.L333:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L313
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L314:
	cmpl	$1, %eax
	jne	.L309
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L298:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L334:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L313:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L314
.L331:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L335:
	call	__stack_chk_fail@PLT
.L336:
	movq	%r14, %rdi
	jmp	.L305
	.cfi_endproc
.LFE7957:
	.size	_ZN6reportL9GetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL9GetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1064, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L358
	cmpw	$1040, %cx
	jne	.L338
.L358:
	movq	23(%rdx), %r12
.L340:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L341
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L342:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L367
.L343:
	leaq	_ZZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L367:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L343
	movq	352(%r12), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	360(%r12), %rax
	movq	-1088(%rbp), %r13
	movq	2408(%rax), %r12
	movq	2400(%rax), %rbx
	testq	%r12, %r12
	je	.L345
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	8(%r12), %r15
	testq	%r14, %r14
	je	.L346
	lock addl	$1, (%r15)
.L347:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	40(%rbx), %rdx
	xorl	%esi, %esi
	leaq	32(%rbx), %rdi
	movq	%rax, %r8
	movq	%r13, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	testq	%r14, %r14
	je	.L368
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
	cmpl	$1, %eax
	je	.L369
.L350:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L337
	testq	%rdi, %rdi
	je	.L337
	call	free@PLT
.L337:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L370
	addq	$1064, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L346:
	addl	$1, 8(%r12)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	40(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movq	%r13, %rcx
	movq	%rax, %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L368:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L350
.L369:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L351
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L352:
	cmpl	$1, %eax
	jne	.L350
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L338:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L351:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L352
.L370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7958:
	.size	_ZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"GetReport"
.LC2:
	.string	"JavaScript API"
	.text
	.p2align 4
	.globl	_ZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$488, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L389
	cmpw	$1040, %cx
	jne	.L372
.L389:
	movq	23(%rdx), %r11
.L374:
	movq	352(%r11), %r13
	leaq	-496(%rbp), %r15
	movq	.LC3(%rip), %xmm1
	leaq	-320(%rbp), %r12
	movq	%r15, %rdi
	movq	%r11, -504(%rbp)
	leaq	-432(%rbp), %r14
	movhps	.LC4(%rip), %xmm1
	movq	%r13, %rsi
	movaps	%xmm1, -528(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%r14, -512(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%dx, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r14, %rdi
	movq	$0, -104(%rbp)
	leaq	-368(%rbp), %r14
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-528(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$16, -360(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	cmpl	$1, 16(%rbx)
	movq	-504(%rbp), %r11
	jne	.L399
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L376
	movq	%r11, -504(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-504(%rbp), %r11
	testb	%al, %al
	je	.L376
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L400
	movq	(%rbx), %rax
	movq	8(%rax), %r8
	addq	$88, %r8
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L376:
	xorl	%r8d, %r8d
.L379:
	movq	-512(%rbp), %r9
	movq	%r13, %rdi
	movq	%r11, %rsi
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rdx
	call	_ZN6report13GetNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_NS0_5LocalINS0_6ObjectEEERSo@PLT
	movq	(%rbx), %rax
	leaq	-448(%rbp), %rbx
	movq	$0, -456(%rbp)
	movq	%rbx, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movq	%rax, -504(%rbp)
	movq	-384(%rbp), %rax
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L381
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L401
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L383:
	movq	-464(%rbp), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L402
	movq	(%rax), %rax
	movq	-504(%rbp), %rdx
	movq	%rax, 24(%rdx)
.L387:
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L385
	call	_ZdlPv@PLT
.L385:
	movq	.LC3(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC5(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L386
	call	_ZdlPv@PLT
.L386:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L400:
	movq	8(%rbx), %r8
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L372:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r11
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	_ZZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L402:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-504(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	%rax, 24(%rcx)
	jmp	.L387
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7947:
	.size	_ZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L405
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L405:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L415
.L406:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L407
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L406
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7811:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.1
.LC6:
	.string	"writeReport"
.LC7:
	.string	"getReport"
.LC8:
	.string	"getCompact"
.LC9:
	.string	"setCompact"
.LC10:
	.string	"getDirectory"
.LC11:
	.string	"setDirectory"
.LC12:
	.string	"getFilename"
.LC13:
	.string	"setFilename"
.LC14:
	.string	"getSignal"
.LC15:
	.string	"setSignal"
.LC16:
	.string	"shouldReportOnFatalError"
.LC17:
	.string	"setReportOnFatalError"
.LC18:
	.string	"shouldReportOnSignal"
.LC19:
	.string	"setReportOnSignal"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"shouldReportOnUncaughtException"
	.section	.rodata.str1.1
.LC21:
	.string	"setReportOnUncaughtException"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB22:
	.text
.LHOTB22:
	.p2align 4
	.type	_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, @function
_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv:
.LFB7965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L417
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L417
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L417
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L470
.L418:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L471
.L419:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L472
.L420:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L473
.L421:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L474
.L422:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L475
.L423:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL10GetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L476
.L424:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L477
.L425:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L478
.L426:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL10SetCompactERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L479
.L427:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L480
.L428:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L481
.L429:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN6reportL12GetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L482
.L430:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L483
.L431:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L484
.L432:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L485
.L433:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L486
.L434:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L487
.L435:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL11GetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L488
.L436:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L489
.L437:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L490
.L438:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L491
.L439:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L492
.L440:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L493
.L441:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL9GetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L494
.L442:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L495
.L443:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L496
.L444:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L497
.L445:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L498
.L446:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L499
.L447:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL24ShouldReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L500
.L448:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L501
.L449:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L502
.L450:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L503
.L451:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L504
.L452:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L505
.L453:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL20ShouldReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L506
.L454:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L507
.L455:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L508
.L456:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L509
.L457:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L510
.L458:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L511
.L459:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN6reportL31ShouldReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L512
.L460:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L513
.L461:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L514
.L462:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L515
.L463:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L516
.L464:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L517
.L465:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L471:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L472:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L473:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L474:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L475:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L476:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L477:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L478:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L479:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L480:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L481:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L482:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L483:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L484:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L485:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L486:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L487:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L488:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L489:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L490:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L491:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L492:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L493:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L494:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L495:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L496:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L497:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L498:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L499:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L500:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L501:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L502:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L503:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L504:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L505:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L506:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L507:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L508:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L509:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L510:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L511:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L512:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L513:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L514:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L515:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L516:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L517:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L465
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, @function
_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold:
.LFSB7965:
.L417:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7965:
	.text
	.size	_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, .-_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, .-_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold
.LCOLDE22:
	.text
.LHOTE22:
	.p2align 4
	.globl	_Z16_register_reportv
	.type	_Z16_register_reportv, @function
_Z16_register_reportv:
.LFB7966:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7966:
	.size	_Z16_register_reportv, .-_Z16_register_reportv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L536
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L537
.L522:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L526
.L519:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L527
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L523:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L522
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L528
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L524:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L526:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L519
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L528:
	xorl	%edx, %edx
	jmp	.L524
	.cfi_endproc
.LFE8679:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L539
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L542
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L541
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L542
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L539
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L542
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L541
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L542
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L539
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L542
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%r12, %rsi
	call	*%rax
.L546:
	movq	(%r12), %rax
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L547
	leaq	16(%r12), %rsi
.L548:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L549
	addq	$16, %r12
.L550:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L551
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L579
.L551:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L580
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L555
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L538
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L538:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L581
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L579:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L549:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L580:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L558
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L553:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L555:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L558:
	xorl	%edx, %edx
	jmp	.L553
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7936:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L583
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L586
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L585
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L586
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L583
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L586
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L585
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L586
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L583
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L586
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*32(%rax)
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L583:
	movq	%r12, %rsi
	call	*%rax
.L590:
	movq	(%r12), %rax
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L591
	leaq	40(%r12), %rsi
.L592:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L593
	addq	$40, %r12
.L594:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L595
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L623
.L595:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L624
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L599
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L582
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L582:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L623:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L624:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L602
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L597:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L599:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	xorl	%edx, %edx
	jmp	.L597
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7938:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.section	.rodata.str1.1
.LC23:
	.string	"../src/node_report_module.cc"
.LC24:
	.string	"report"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC23
	.quad	0
	.quad	_ZN6reportL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.quad	.LC24
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"../src/node_report_module.cc:178"
	.section	.rodata.str1.1
.LC26:
	.string	"info[0]->IsBoolean()"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"void report::SetReportOnUncaughtException(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6reportL28SetReportOnUncaughtExceptionERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"../src/node_report_module.cc:164"
	.align 8
.LC29:
	.string	"void report::SetReportOnSignal(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6reportL17SetReportOnSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC28
	.quad	.LC26
	.quad	.LC29
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"../src/node_report_module.cc:152"
	.align 8
.LC31:
	.string	"void report::SetReportOnFatalError(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6reportL21SetReportOnFatalErrorERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC30
	.quad	.LC26
	.quad	.LC31
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"../src/node_report_module.cc:140"
	.section	.rodata.str1.1
.LC33:
	.string	"info[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"void report::SetSignal(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6reportL9SetSignalERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"../src/node_report_module.cc:124"
	.align 8
.LC36:
	.string	"void report::SetFilename(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6reportL11SetFilenameERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC35
	.quad	.LC33
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"../src/node_report_module.cc:106"
	.align 8
.LC38:
	.string	"void report::SetDirectory(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6reportL12SetDirectoryERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC37
	.quad	.LC33
	.quad	.LC38
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"../src/node_report_module.cc:64"
	.section	.rodata.str1.1
.LC40:
	.string	"(info.Length()) == (1)"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"void report::GetReport(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6report9GetReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"../src/node_report_module.cc:37"
	.section	.rodata.str1.1
.LC43:
	.string	"(info.Length()) == (4)"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"void report::WriteReport(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN6report11WriteReportERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC45:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC47:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC48:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC48
	.quad	.LC46
	.quad	.LC49
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC50:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC52:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC53:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC55:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC3:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC4:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC5:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
