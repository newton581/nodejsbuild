	.file	"node_report_utils.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1338:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"null"
	.text
	.p2align 4
	.type	_ZN6reportL14ReportEndpointEP11uv_handle_sP8sockaddrPKcPN4node10JSONWriterE.isra.0, @function
_ZN6reportL14ReportEndpointEP11uv_handle_sP8sockaddrPKcPN4node10JSONWriterE.isra.0:
.LFB8060:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1512, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L81
	movzwl	2(%rsi), %eax
	movzwl	(%rsi), %r12d
	movq	%rsi, %rbx
	xorl	%edx, %edx
	movq	(%rdi), %rdi
	leaq	-1440(%rbp), %rsi
	movl	$2, %r8d
	movq	%rbx, %rcx
	rolw	$8, %ax
	movzwl	%ax, %eax
	movl	%eax, -1548(%rbp)
	cmpl	$2, %r12d
	je	.L82
	call	uv_getnameinfo@PLT
	testl	%eax, %eax
	jne	.L83
.L18:
	leaq	-1188(%rbp), %rax
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	movq	%rax, -1536(%rbp)
	je	.L84
.L21:
	cmpb	$0, 8(%r14)
	jne	.L22
	leaq	-1512(%rbp), %r12
	movl	$1, %edx
	movb	$10, -1512(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L22
	movl	12(%r14), %esi
	testl	%esi, %esi
	jle	.L22
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L23
.L22:
	leaq	-1504(%rbp), %rax
	leaq	-1488(%rbp), %rbx
	movq	%rax, -1528(%rbp)
	movq	%rbx, -1504(%rbp)
	testq	%r13, %r13
	jne	.L85
.L61:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	8(%rbx), %rsi
.L20:
	leaq	-112(%rbp), %rbx
	movl	%r12d, %edi
	movl	$46, %ecx
	movq	%rbx, %rdx
	call	uv_inet_ntop@PLT
	movq	(%r14), %rdi
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rbx, %rax
	cmpl	$1, 16(%r14)
	movq	%rax, -1536(%rbp)
	jne	.L21
.L84:
	leaq	-1512(%rbp), %r12
	movl	$1, %edx
	movb	$44, -1512(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -1512(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L86
	cmpq	$1, %rax
	jne	.L25
	movzbl	0(%r13), %edx
	leaq	-1512(%rbp), %r12
	movb	%dl, -1488(%rbp)
	movq	%rbx, %rdx
.L26:
	movq	%rax, -1496(%rbp)
	movq	%r12, %rsi
	leaq	-1472(%rbp), %r15
	movb	$0, (%rdx,%rax)
	movq	(%r14), %rdi
	movl	$1, %edx
	movb	$34, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1464(%rbp), %rdx
	movq	-1472(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1512(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1472(%rbp), %rdi
	leaq	-1456(%rbp), %rax
	movq	%rax, -1544(%rbp)
	cmpq	%rax, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-1504(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L29
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L29:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r14)
	cmpq	$0, -1536(%rbp)
	movl	$0, 16(%r14)
	je	.L30
	cmpb	$0, 8(%r14)
	jne	.L31
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L31
	movl	12(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L31
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%r14)
	jg	.L35
.L31:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, -1504(%rbp)
	movl	$1953722216, -1488(%rbp)
	movq	$4, -1496(%rbp)
	movb	$0, -1484(%rbp)
	movb	$34, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1464(%rbp), %rdx
	movq	-1472(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1512(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1472(%rbp), %rdi
	cmpq	-1544(%rbp), %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	-1504(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L37
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L37:
	movq	%rbx, -1504(%rbp)
	movq	-1536(%rbp), %r13
.L38:
	movl	0(%r13), %edx
	addq	$4, %r13
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L38
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r13), %rdx
	cmove	%rdx, %r13
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %r13
	subq	-1536(%rbp), %r13
	movq	%r13, -1512(%rbp)
	cmpq	$15, %r13
	ja	.L87
	cmpq	$1, %r13
	jne	.L42
	movq	-1536(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1488(%rbp)
	movq	%rbx, %rax
.L43:
	movq	%r13, -1496(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, (%rax,%r13)
	movq	(%r14), %rdi
	movb	$34, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1464(%rbp), %rdx
	movq	-1472(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1512(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1472(%rbp), %rdi
	cmpq	-1544(%rbp), %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	-1504(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L30:
	cmpb	$0, 8(%r14)
	jne	.L46
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L46
	movl	12(%r14), %edx
	testl	%edx, %edx
	jle	.L46
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%r14)
	jg	.L50
.L46:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, -1504(%rbp)
	movl	$1953656688, -1488(%rbp)
	movq	$4, -1496(%rbp)
	movb	$0, -1484(%rbp)
	movb	$34, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1464(%rbp), %rdx
	movq	-1472(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1512(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1472(%rbp), %rdi
	cmpq	-1544(%rbp), %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	-1504(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L52
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L52:
	movl	-1548(%rbp), %esi
	call	_ZNSolsEi@PLT
	cmpb	$0, 8(%r14)
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	je	.L88
	subl	$2, 12(%r14)
.L54:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	testl	%eax, %eax
	je	.L89
.L57:
	movl	$1, 16(%r14)
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$1512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L54
	testl	%eax, %eax
	jle	.L54
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1512(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	movq	(%r14), %rdi
	jg	.L56
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L82:
	call	uv_getnameinfo@PLT
	leaq	4(%rbx), %rsi
	testl	%eax, %eax
	jne	.L20
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L25:
	testq	%r15, %r15
	jne	.L91
	movq	%rbx, %rdx
	leaq	-1512(%rbp), %r12
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L86:
	movq	-1528(%rbp), %rdi
	leaq	-1512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1504(%rbp)
	movq	%rax, %rdi
	movq	-1512(%rbp), %rax
	movq	%rax, -1488(%rbp)
.L24:
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-1512(%rbp), %rax
	movq	-1504(%rbp), %rdx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L42:
	testq	%r13, %r13
	jne	.L92
	movq	%rbx, %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L87:
	movq	-1528(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1504(%rbp)
	movq	%rax, %rdi
	movq	-1512(%rbp), %rax
	movq	%rax, -1488(%rbp)
.L41:
	movq	-1536(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-1512(%rbp), %r13
	movq	-1504(%rbp), %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L81:
	cmpl	$1, 16(%rcx)
	movq	(%rcx), %rdi
	je	.L93
.L5:
	cmpb	$0, 8(%r14)
	je	.L94
.L6:
	leaq	-1456(%rbp), %rax
	leaq	-1472(%rbp), %r15
	movq	%rax, -1544(%rbp)
	movq	%rax, -1472(%rbp)
	testq	%r13, %r13
	je	.L61
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -1512(%rbp)
	movq	%rax, %rbx
	cmpq	$15, %rax
	ja	.L95
	cmpq	$1, %rax
	jne	.L9
	movzbl	0(%r13), %edx
	leaq	-1512(%rbp), %r12
	movb	%dl, -1456(%rbp)
	movq	-1544(%rbp), %rdx
.L10:
	movq	%rax, -1464(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%r14), %rdi
	movl	$1, %edx
	movb	$34, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1440(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1432(%rbp), %rdx
	movq	-1440(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1512(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1440(%rbp), %rdi
	leaq	-1424(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	-1472(%rbp), %rdi
	cmpq	-1544(%rbp), %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L13
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L13:
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r14)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	-1512(%rbp), %r12
	movl	$1, %edx
	movb	$10, -1512(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L6
	movl	12(%r14), %edi
	testl	%edi, %edi
	jle	.L6
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1512(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L7
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	testq	%rbx, %rbx
	jne	.L96
	movq	-1544(%rbp), %rdx
	leaq	-1512(%rbp), %r12
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	-1512(%rbp), %r12
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1472(%rbp)
	movq	%rax, %rdi
	movq	-1512(%rbp), %rax
	movq	%rax, -1456(%rbp)
.L8:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-1512(%rbp), %rax
	movq	-1472(%rbp), %rdx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	-1512(%rbp), %r12
	movl	$1, %edx
	movb	$44, -1512(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L5
.L90:
	call	__stack_chk_fail@PLT
.L96:
	movq	-1544(%rbp), %rdi
	leaq	-1512(%rbp), %r12
	jmp	.L8
.L92:
	movq	%rbx, %rdi
	jmp	.L41
.L91:
	movq	%rbx, %rdi
	leaq	-1512(%rbp), %r12
	jmp	.L24
	.cfi_endproc
.LFE8060:
	.size	_ZN6reportL14ReportEndpointEP11uv_handle_sP8sockaddrPKcPN4node10JSONWriterE.isra.0, .-_ZN6reportL14ReportEndpointEP11uv_handle_sP8sockaddrPKcPN4node10JSONWriterE.isra.0
	.p2align 4
	.type	_ZN6reportL10ReportPathEP11uv_handle_sPN4node10JSONWriterE, @function
_ZN6reportL10ReportPathEP11uv_handle_sPN4node10JSONWriterE:
.LFB6616:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$1, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L156
.L98:
	movq	$0, -144(%rbp)
	movl	16(%r12), %eax
	cmpl	$3, %eax
	je	.L99
	cmpl	$4, %eax
	jne	.L151
	leaq	-144(%rbp), %r13
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	uv_fs_poll_getpath@PLT
	cmpl	$-105, %eax
	je	.L103
.L151:
	movl	16(%rbx), %eax
	movq	(%rbx), %rdi
.L104:
	leaq	-136(%rbp), %r12
	cmpl	$1, %eax
	je	.L157
.L123:
	cmpb	$0, 8(%rbx)
	je	.L158
.L124:
	movq	(%rbx), %rdi
	leaq	-128(%rbp), %r8
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$7308604897068083558, %rax
	leaq	-112(%rbp), %r13
	movb	$0, -104(%rbp)
	movq	%r8, -152(%rbp)
	movq	%r13, -128(%rbp)
	movq	%rax, -112(%rbp)
	movq	$8, -120(%rbp)
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %r8
	leaq	-96(%rbp), %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L159
.L130:
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%rbx)
.L122:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$136, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L124
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L124
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L128:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%rbx)
	jg	.L128
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	-144(%rbp), %r13
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	uv_fs_event_getpath@PLT
	cmpl	$-105, %eax
	jne	.L151
.L103:
	movq	-144(%rbp), %rax
	movl	$1, %edi
	addq	$1, %rax
	cmovne	%rax, %rdi
	movq	%rax, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L161
.L105:
	movq	%r15, %rdi
	call	free@PLT
	movl	16(%r12), %eax
	cmpl	$3, %eax
	je	.L106
	cmpl	$4, %eax
	je	.L107
	movl	16(%rbx), %eax
	movq	(%rbx), %rdi
	movq	%r14, %r15
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L156:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$1, %edi
	call	malloc@PLT
	movq	%rax, %r15
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	uv_fs_event_getpath@PLT
	movl	%eax, %r13d
.L108:
	movl	16(%rbx), %eax
	movq	(%rbx), %rdi
	testl	%r13d, %r13d
	jne	.L139
	movq	-144(%rbp), %rdx
	leaq	-136(%rbp), %r12
	movb	$0, (%r14,%rdx)
	cmpl	$1, %eax
	je	.L162
.L109:
	cmpb	$0, 8(%rbx)
	jne	.L110
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L110
	movl	12(%rbx), %edx
	testl	%edx, %edx
	jle	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%rbx)
	jg	.L114
.L110:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-112(%rbp), %r13
	movabsq	$7308604897068083558, %rax
	movq	%r13, -128(%rbp)
	leaq	-128(%rbp), %r15
	movq	%rax, -112(%rbp)
	movq	$8, -120(%rbp)
	movb	$0, -104(%rbp)
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%rax, -160(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-160(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -160(%rbp)
	cmpq	%rax, %rdi
	je	.L113
	call	_ZdlPv@PLT
.L113:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L116
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L116:
	movq	%r13, -128(%rbp)
	testq	%r14, %r14
	jne	.L163
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	uv_fs_poll_getpath@PLT
	movl	%eax, %r13d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%r14, %r8
.L132:
	movl	(%r8), %edx
	addq	$4, %r8
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L132
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r8), %rdx
	cmove	%rdx, %r8
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %r8
	subq	%r14, %r8
	movq	%r8, -136(%rbp)
	cmpq	$15, %r8
	ja	.L164
	cmpq	$1, %r8
	jne	.L118
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%r13, %rax
.L119:
	movq	%r8, -120(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, (%rax,%r8)
	movq	(%rbx), %rdi
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -168(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-168(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-160(%rbp), %rdi
	je	.L120
	call	_ZdlPv@PLT
.L120:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	movl	$1, 16(%rbx)
	movq	%r14, %r15
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r14, %r15
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L118:
	testq	%r8, %r8
	jne	.L165
	movq	%r13, %rax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L161:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-152(%rbp), %rdi
	call	malloc@PLT
	cmpq	$0, -160(%rbp)
	movq	%rax, %r14
	je	.L105
	testq	%rax, %rax
	jne	.L105
	leaq	_ZZN4node6MallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, -168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L117:
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rax
	jmp	.L119
.L162:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L109
.L160:
	call	__stack_chk_fail@PLT
.L165:
	movq	%r13, %rdi
	jmp	.L117
	.cfi_endproc
.LFE6616:
	.size	_ZN6reportL10ReportPathEP11uv_handle_sPN4node10JSONWriterE, .-_ZN6reportL10ReportPathEP11uv_handle_sPN4node10JSONWriterE
	.section	.text._ZN4node10JSONWriter12write_stringEPKc,"axG",@progbits,_ZN4node10JSONWriter12write_stringEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10JSONWriter12write_stringEPKc
	.type	_ZN4node10JSONWriter12write_stringEPKc, @function
_ZN4node10JSONWriter12write_stringEPKc:
.LFB2387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -128(%rbp)
	testq	%rsi, %rsi
	je	.L177
	movq	%rsi, %rdi
	movq	%rsi, %r15
	leaq	-128(%rbp), %r14
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L178
	cmpq	$1, %rax
	jne	.L170
	movzbl	(%r15), %edx
	leaq	-136(%rbp), %rbx
	movb	%dl, -112(%rbp)
	movq	%r13, %rdx
.L171:
	movq	%rax, -120(%rbp)
	movq	%rbx, %rsi
	movb	$0, (%rdx,%rax)
	movq	-152(%rbp), %rax
	movl	$1, %edx
	movb	$34, -136(%rbp)
	movq	(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%rbx, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L180
	movq	%r13, %rdx
	leaq	-136(%rbp), %rbx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	-136(%rbp), %rbx
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L169:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L179:
	call	__stack_chk_fail@PLT
.L180:
	movq	%r13, %rdi
	leaq	-136(%rbp), %rbx
	jmp	.L169
	.cfi_endproc
.LFE2387:
	.size	_ZN4node10JSONWriter12write_stringEPKc, .-_ZN4node10JSONWriter12write_stringEPKc
	.section	.rodata.str1.1
.LC2:
	.string	"true"
.LC3:
	.string	"false"
.LC4:
	.string	"localEndpoint"
.LC5:
	.string	"remoteEndpoint"
.LC6:
	.string	"width"
.LC7:
	.string	"height"
.LC8:
	.string	"stdin"
.LC9:
	.string	"stdio"
.LC10:
	.string	"stdout"
.LC11:
	.string	"stderr"
.LC12:
	.string	"0x"
	.text
	.p2align 4
	.globl	_ZN6report10WalkHandleEP11uv_handle_sPv
	.type	_ZN6report10WalkHandleEP11uv_handle_sPv, @function
_ZN6report10WalkHandleEP11uv_handle_sPv:
.LFB6617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-520(%rbp), %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -560(%rbp)
	movl	16(%rdi), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_handle_type_name@PLT
	cmpl	$1, 16(%r13)
	movq	%rax, %r14
	je	.L452
.L182:
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	je	.L453
.L183:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r13)
	cmpb	$0, 8(%r13)
	movl	$0, 16(%r13)
	je	.L454
.L186:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-464(%rbp), %rax
	leaq	-480(%rbp), %rbx
	movq	%rax, -536(%rbp)
	movq	%rbx, -568(%rbp)
	movq	%rax, -480(%rbp)
	movl	$1701869940, -464(%rbp)
	movq	$4, -472(%rbp)
	movb	$0, -460(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%rax, %r15
	leaq	-448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	leaq	-432(%rbp), %rax
	movq	%rax, -544(%rbp)
	cmpq	%rax, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L192
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L192:
	leaq	-496(%rbp), %rax
	leaq	-512(%rbp), %r15
	movq	%rax, -576(%rbp)
	movq	%rax, -512(%rbp)
	testq	%r14, %r14
	jne	.L455
.L397:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L455:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -520(%rbp)
	movq	%rax, %rbx
	cmpq	$15, %rax
	ja	.L456
	cmpq	$1, %rax
	jne	.L194
	movzbl	(%r14), %edx
	movb	%dl, -496(%rbp)
	movq	-576(%rbp), %rdx
.L195:
	movq	%rax, -504(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	0(%r13), %rdi
	movl	$1, %edx
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L196
	call	_ZdlPv@PLT
.L196:
	movq	-512(%rbp), %rdi
	cmpq	-576(%rbp), %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movl	$1, 16(%r13)
	movq	-560(%rbp), %rdi
	call	uv_is_active@PLT
	cmpl	$1, 16(%r13)
	movl	%eax, %ebx
	je	.L457
.L198:
	cmpb	$0, 8(%r13)
	jne	.L199
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L199
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L199
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L203:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r13)
	jg	.L203
.L199:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$101, -456(%rbp)
	movq	%rax, -480(%rbp)
	movabsq	$8532478939214345065, %rax
	movq	%rax, -464(%rbp)
	movq	$9, -472(%rbp)
	movb	$0, -455(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L205
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L205:
	cmpl	$1, %ebx
	leaq	.LC3(%rip), %rax
	leaq	.LC2(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testl	%ebx, %ebx
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r13)
	movq	-560(%rbp), %rdi
	call	uv_has_ref@PLT
	cmpl	$1, 16(%r13)
	movl	%eax, %ebx
	je	.L458
.L401:
	cmpb	$0, 8(%r13)
	jne	.L207
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L207
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L207
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L211:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r13)
	jg	.L211
.L207:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1701015141, -456(%rbp)
	movq	%rax, -480(%rbp)
	movabsq	$8243107278868149097, %rax
	movq	%rax, -464(%rbp)
	movb	$100, -452(%rbp)
	movq	$13, -472(%rbp)
	movb	$0, -451(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L213
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L213:
	cmpl	$1, %ebx
	leaq	.LC3(%rip), %rax
	movq	.LC14(%rip), %xmm1
	leaq	.LC2(%rip), %rsi
	sbbq	%rdx, %rdx
	leaq	-320(%rbp), %r14
	notq	%rdx
	movhps	.LC15(%rip), %xmm1
	addq	$5, %rdx
	testl	%ebx, %ebx
	movaps	%xmm1, -592(%rbp)
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r13)
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-552(%rbp), %rdi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	addq	-24(%rbx), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-544(%rbp), %rdi
	xorl	%esi, %esi
	addq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-592(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -592(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -600(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-544(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %rax
	movq	-544(%rbp), %rdx
	addq	-24(%rax), %rdx
	cmpb	$0, 225(%rdx)
	je	.L459
.L398:
	movb	$48, 224(%rdx)
	movq	-24(%rax), %rdx
	movq	-544(%rbp), %rdi
	movq	-560(%rbp), %rsi
	movq	$16, -416(%rbp,%rdx)
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-576(%rbp), %rax
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	-384(%rbp), %rax
	testq	%rax, %rax
	je	.L218
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L219
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L220:
	movq	.LC14(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC16(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-600(%rbp), %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-592(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpl	$1, 16(%r13)
	je	.L460
.L222:
	cmpb	$0, 8(%r13)
	jne	.L223
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L223
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L223
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L227:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L227
.L223:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1919181921, -464(%rbp)
	movq	%rax, -480(%rbp)
	movl	$29541, %eax
	movw	%ax, -460(%rbp)
	movb	$115, -458(%rbp)
	movq	$7, -472(%rbp)
	movb	$0, -457(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L228
	call	_ZdlPv@PLT
.L228:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L229
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L229:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-552(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movl	$1, 16(%r13)
	movq	-512(%rbp), %rdi
	cmpq	-576(%rbp), %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	-560(%rbp), %rax
	movl	16(%rax), %eax
	leal	-3(%rax), %edx
	cmpl	$13, %edx
	ja	.L232
	leaq	.L234(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L234:
	.long	.L239-.L234
	.long	.L239-.L234
	.long	.L232-.L234
	.long	.L232-.L234
	.long	.L232-.L234
	.long	.L232-.L234
	.long	.L232-.L234
	.long	.L238-.L234
	.long	.L232-.L234
	.long	.L235-.L234
	.long	.L237-.L234
	.long	.L236-.L234
	.long	.L235-.L234
	.long	.L233-.L234
	.text
	.p2align 4,,10
	.p2align 3
.L454:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L186
	movl	12(%r13), %edx
	testl	%edx, %edx
	jle	.L186
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L190:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L190
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L183
	movl	12(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L183
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	movq	0(%r13), %rdi
	jg	.L185
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L194:
	testq	%rbx, %rbx
	jne	.L461
	movq	-576(%rbp), %rdx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	$1, 16(%r13)
	movq	0(%r13), %rdi
	je	.L462
.L240:
	cmpb	$0, 8(%r13)
	jne	.L241
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L241
	movl	12(%r13), %ebx
	testl	%ebx, %ebx
	jle	.L241
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L245:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L245
.L241:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movl	$26992, %r14d
	movq	%r12, %rsi
	movw	%r14w, -464(%rbp)
	movq	%rax, -480(%rbp)
	movb	$100, -462(%rbp)
	movq	$3, -472(%rbp)
	movb	$0, -461(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L247
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L247:
	movq	-560(%rbp), %rbx
	movl	104(%rbx), %esi
	call	_ZNSolsEi@PLT
	movl	$1, 16(%r13)
	movl	16(%rbx), %eax
	.p2align 4,,10
	.p2align 3
.L232:
	movl	%eax, %edx
	andl	$-9, %edx
	cmpl	$7, %edx
	je	.L414
	cmpl	$12, %eax
	je	.L414
	cmpl	$15, %eax
	jbe	.L463
.L330:
	movq	0(%r13), %rdi
	.p2align 4,,10
	.p2align 3
.L359:
	cmpb	$0, 8(%r13)
	je	.L464
	subl	$2, 12(%r13)
.L387:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r13), %eax
	movq	0(%r13), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r13)
	movl	%eax, 12(%r13)
	jne	.L387
	testl	%eax, %eax
	jle	.L387
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	movq	0(%r13), %rdi
	jg	.L389
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L414:
	movq	-560(%rbp), %rbx
	leaq	-524(%rbp), %r15
	movl	$0, -524(%rbp)
	leaq	-525(%rbp), %r14
	movl	$0, -520(%rbp)
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	uv_send_buffer_size@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	uv_recv_buffer_size@PLT
	cmpl	$1, 16(%r13)
	je	.L466
.L315:
	cmpb	$0, 8(%r13)
	jne	.L316
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L316
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L316
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L320:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L320
.L316:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movl	$1767076453, -456(%rbp)
	movq	%rax, -480(%rbp)
	movabsq	$7378713967506384243, %rax
	movq	%rax, -464(%rbp)
	movl	$25978, %eax
	movw	%ax, -452(%rbp)
	movq	$14, -472(%rbp)
	movb	$0, -450(%rbp)
	movb	$34, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -525(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$58, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L322
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L322:
	movl	-524(%rbp), %esi
	call	_ZNSolsEi@PLT
	movl	$1, 16(%r13)
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	movb	$44, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L323
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L323
	movl	12(%r13), %ebx
	testl	%ebx, %ebx
	jle	.L323
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L327:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L327
.L323:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movl	$25978, %r15d
	movq	%r14, %rsi
	movw	%r15w, -452(%rbp)
	movq	%rax, -480(%rbp)
	movabsq	$7378713967807653234, %rax
	movq	%rax, -464(%rbp)
	movl	$1767076453, -456(%rbp)
	movq	$14, -472(%rbp)
	movb	$0, -450(%rbp)
	movb	$34, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -525(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L328
	call	_ZdlPv@PLT
.L328:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$58, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L329
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L329:
	movl	-520(%rbp), %esi
	call	_ZNSolsEi@PLT
	movq	-560(%rbp), %rax
	movl	$1, 16(%r13)
	movl	16(%rax), %eax
	cmpl	$15, %eax
	ja	.L330
.L463:
	movl	$53632, %edx
	btq	%rax, %rdx
	jnc	.L330
	movq	-560(%rbp), %rdi
	movq	%r12, %rsi
	call	uv_fileno@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L467
.L331:
	movq	-560(%rbp), %rax
	movq	0(%r13), %rdi
	movl	16(%rax), %eax
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$12, %edx
	je	.L415
	cmpl	$7, %eax
	jne	.L359
.L415:
	cmpl	$1, 16(%r13)
	je	.L468
.L361:
	cmpb	$0, 8(%r13)
	jne	.L362
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L362
	movl	12(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L362
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L366:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L366
.L362:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$25978, %esi
	movl	$1, %edx
	movw	%si, -452(%rbp)
	movq	%r12, %rsi
	movq	%rax, -480(%rbp)
	movabsq	$7310839066292482679, %rax
	movq	%rax, -464(%rbp)
	movl	$1767073141, -456(%rbp)
	movq	$14, -472(%rbp)
	movb	$0, -450(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L368
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L368:
	movq	-560(%rbp), %rbx
	movq	96(%rbx), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, 16(%r13)
	movq	%rbx, %rdi
	call	uv_is_readable@PLT
	cmpl	$1, 16(%r13)
	movl	%eax, %ebx
	je	.L469
.L369:
	cmpb	$0, 8(%r13)
	jne	.L370
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L370
	movl	12(%r13), %edx
	testl	%edx, %edx
	jle	.L370
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L374:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r13)
	jg	.L374
.L370:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	$8, -472(%rbp)
	movq	%rax, -480(%rbp)
	movabsq	$7308324465750926706, %rax
	movq	%rax, -464(%rbp)
	movb	$0, -456(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L376
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L376:
	cmpl	$1, %ebx
	leaq	.LC3(%rip), %rax
	leaq	.LC2(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testl	%ebx, %ebx
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r13)
	movq	-560(%rbp), %rdi
	call	uv_is_writable@PLT
	cmpl	$1, 16(%r13)
	movl	%eax, %ebx
	je	.L470
.L391:
	cmpb	$0, 8(%r13)
	jne	.L378
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L378
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L378
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L382:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r13)
	jg	.L382
.L378:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	$8, -472(%rbp)
	movq	%rax, -480(%rbp)
	movabsq	$7308324466019889783, %rax
	movq	%rax, -464(%rbp)
	movb	$0, -456(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L381
	call	_ZdlPv@PLT
.L381:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L383
	call	_ZdlPv@PLT
.L383:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L384
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L384:
	cmpl	$1, %ebx
	leaq	.LC3(%rip), %rax
	leaq	.LC2(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testl	%ebx, %ebx
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r13)
	movq	0(%r13), %rdi
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L219:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L459:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L471
	cmpb	$0, 56(%rdi)
	je	.L472
.L216:
	movb	$1, 225(%rdx)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -512(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %rax
	movq	%rax, -496(%rbp)
.L193:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-520(%rbp), %rax
	movq	-512(%rbp), %rdx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L467:
	cmpl	$1, 16(%r13)
	movl	-520(%rbp), %eax
	leaq	-524(%rbp), %r15
	movq	0(%r13), %rdi
	movl	%eax, -576(%rbp)
	je	.L473
.L332:
	cmpb	$0, 8(%r13)
	je	.L474
.L333:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movl	$25702, %r11d
	movb	$0, -462(%rbp)
	movw	%r11w, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$2, -472(%rbp)
	movb	$34, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$34, -524(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$58, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	je	.L475
.L339:
	movl	-576(%rbp), %esi
	call	_ZNSolsEi@PLT
	movl	-520(%rbp), %r14d
	movl	$1, 16(%r13)
	cmpl	$1, %r14d
	je	.L340
	cmpl	$2, %r14d
	je	.L341
	testl	%r14d, %r14d
	jne	.L331
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$44, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	je	.L476
.L342:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movl	$1768191091, -464(%rbp)
	movq	%rax, -480(%rbp)
	movb	$111, -460(%rbp)
	movq	$5, -472(%rbp)
	movb	$0, -459(%rbp)
	movb	$34, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$34, -524(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L347
	call	_ZdlPv@PLT
.L347:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$58, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	je	.L477
.L348:
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movl	$1, 16(%r13)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L457:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L452:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L460:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L458:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%rdx, -616(%rbp)
	movq	%rdi, -608(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-608(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-616(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L217
	movq	-432(%rbp), %rax
	movb	$1, 225(%rdx)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L235:
	movq	-560(%rbp), %rdi
	movl	$128, -520(%rbp)
	leaq	8(%rdi), %r14
	cmpl	$12, %eax
	je	.L248
	cmpl	$15, %eax
	jne	.L249
	movq	-552(%rbp), %rsi
	movq	%r12, %rdx
	call	uv_udp_getsockname@PLT
.L250:
	testl	%eax, %eax
	jne	.L249
	movq	-552(%rbp), %rbx
	movq	%r13, %rcx
	leaq	.LC4(%rip), %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN6reportL14ReportEndpointEP11uv_handle_sP8sockaddrPKcPN4node10JSONWriterE.isra.0
	movq	-560(%rbp), %rax
	movl	16(%rax), %eax
	cmpl	$12, %eax
	je	.L392
	movq	%rbx, %rsi
	cmpl	$15, %eax
	jne	.L256
.L393:
	movq	-552(%rbp), %rsi
	movq	-560(%rbp), %rdi
	movq	%r12, %rdx
	call	uv_udp_getpeername@PLT
.L255:
	movq	-552(%rbp), %rsi
	testl	%eax, %eax
	je	.L256
.L254:
	xorl	%esi, %esi
.L256:
	movq	%r13, %rcx
	leaq	.LC5(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN6reportL14ReportEndpointEP11uv_handle_sP8sockaddrPKcPN4node10JSONWriterE.isra.0
	movq	-560(%rbp), %rax
	movl	16(%rax), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-560(%rbp), %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6reportL10ReportPathEP11uv_handle_sPN4node10JSONWriterE
	movl	16(%rbx), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$10, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L333
	movl	12(%r13), %r10d
	testl	%r10d, %r10d
	jle	.L333
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L337:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	addl	$1, %r14d
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r13)
	jg	.L337
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L475:
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L341:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$44, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	je	.L478
.L354:
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	movl	$1, %edx
	movb	$58, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	je	.L479
.L356:
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movl	$1, 16(%r13)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L233:
	cmpl	$1, 16(%r13)
	movq	0(%r13), %rdi
	je	.L480
.L292:
	cmpb	$0, 8(%r13)
	jne	.L293
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L293
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L293
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L297:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L297
.L293:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$28021, %edx
	movq	%r12, %rsi
	movw	%dx, -460(%rbp)
	movl	$1, %edx
	movq	%rax, -480(%rbp)
	movl	$1852270963, -464(%rbp)
	movq	$6, -472(%rbp)
	movb	$0, -458(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L296
	call	_ZdlPv@PLT
.L296:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L299
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L299:
	movq	-560(%rbp), %rbx
	movl	104(%rbx), %esi
	call	_ZNSolsEi@PLT
	movl	$1, 16(%r13)
	movl	104(%rbx), %edi
	call	_ZN4node12signo_stringEi@PLT
	cmpl	$1, 16(%r13)
	movq	%rax, %r14
	je	.L481
.L300:
	cmpb	$0, 8(%r13)
	jne	.L301
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L301
	movl	12(%r13), %eax
	testl	%eax, %eax
	jle	.L301
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L305:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L305
.L301:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1852270963, -464(%rbp)
	movq	%rax, -480(%rbp)
	movl	$27745, %eax
	movw	%ax, -460(%rbp)
	movq	$6, -472(%rbp)
	movb	$0, -458(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L307
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L307:
	movq	-536(%rbp), %rax
	movq	%rax, -480(%rbp)
	testq	%r14, %r14
	je	.L397
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -520(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L482
	cmpq	$1, %rax
	jne	.L309
	movzbl	(%r14), %edx
	movb	%dl, -464(%rbp)
	movq	-536(%rbp), %rdx
.L310:
	movq	%rax, -472(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	0(%r13), %rdi
	movl	$1, %edx
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L311
	call	_ZdlPv@PLT
.L311:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movl	$1, 16(%r13)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L236:
	movq	-560(%rbp), %rdi
	leaq	-524(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdx
	call	uv_tty_get_winsize@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L483
.L450:
	movq	-560(%rbp), %rax
	movl	16(%rax), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L237:
	movq	-560(%rbp), %r15
	movq	8(%r15), %rdi
	movq	128(%r15), %r14
	call	uv_now@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	uv_timer_get_repeat@PLT
	cmpl	$1, 16(%r13)
	movq	%rax, -576(%rbp)
	je	.L484
.L257:
	cmpb	$0, 8(%r13)
	jne	.L258
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L258
	movl	12(%r13), %r10d
	testl	%r10d, %r10d
	jle	.L258
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L262:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r13)
	jg	.L262
.L258:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$29793, %r11d
	movb	$0, -458(%rbp)
	movw	%r11w, -460(%rbp)
	movq	%rax, -480(%rbp)
	movl	$1701864818, -464(%rbp)
	movq	$6, -472(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L261
	call	_ZdlPv@PLT
.L261:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L263
	call	_ZdlPv@PLT
.L263:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L264
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L264:
	movq	-576(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	0(%r13), %rdi
	movq	%r14, %rax
	movq	%r12, %rsi
	movl	$1, 16(%r13)
	subq	%rbx, %rax
	movl	$1, %edx
	movq	%rax, -576(%rbp)
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L265
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L265
	movl	12(%r13), %r9d
	testl	%r9d, %r9d
	jle	.L265
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L269:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r13)
	jg	.L269
.L265:
	movq	-536(%rbp), %rax
	movq	-568(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	$16, -520(%rbp)
	movq	%rax, -480(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-520(%rbp), %rdx
	movdqa	.LC13(%rip), %xmm0
	movq	%r12, %rsi
	movq	%rax, -480(%rbp)
	movq	%rdx, -464(%rbp)
	movups	%xmm0, (%rax)
	movq	-520(%rbp), %rax
	movq	-480(%rbp), %rdx
	movq	%rax, -472(%rbp)
	movb	$0, (%rdx,%rax)
	movq	0(%r13), %rdi
	movl	$1, %edx
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L268
	call	_ZdlPv@PLT
.L268:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L270
	call	_ZdlPv@PLT
.L270:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L271
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L271:
	movq	-576(%rbp), %rsi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movl	$1, 16(%r13)
	movq	0(%r13), %rdi
	movq	%r12, %rsi
	movl	$1, %edx
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L272
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L272
	movl	12(%r13), %edi
	testl	%edi, %edi
	jle	.L272
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L276:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r13)
	jg	.L276
.L272:
	movq	-536(%rbp), %rax
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$25970, %r8d
	movl	$1768978533, -464(%rbp)
	movw	%r8w, -460(%rbp)
	movq	%rax, -480(%rbp)
	movb	$100, -458(%rbp)
	movq	$7, -472(%rbp)
	movb	$0, -457(%rbp)
	movb	$34, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-568(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -520(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-448(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movq	-480(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L277
	call	_ZdlPv@PLT
.L277:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L278
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L278:
	cmpq	%r14, %rbx
	leaq	.LC3(%rip), %rax
	leaq	.LC2(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	cmpq	%rbx, %r14
	cmova	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-560(%rbp), %rax
	movl	$1, 16(%r13)
	movl	16(%rax), %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L340:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$44, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	je	.L485
.L349:
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	0(%r13), %rdi
	movq	%r15, %rsi
	movl	$1, %edx
	movb	$58, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	je	.L486
.L351:
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movl	$1, 16(%r13)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r13, %rcx
	leaq	.LC4(%rip), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6reportL14ReportEndpointEP11uv_handle_sP8sockaddrPKcPN4node10JSONWriterE.isra.0
	movq	-560(%rbp), %rax
	movl	16(%rax), %eax
	cmpl	$12, %eax
	je	.L392
	cmpl	$15, %eax
	je	.L393
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L470:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L469:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L468:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L466:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$44, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L473:
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$44, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L309:
	testq	%r15, %r15
	jne	.L487
	movq	-536(%rbp), %rdx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L478:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$10, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L354
	movl	12(%r13), %edi
	testl	%edi, %edi
	jle	.L354
	.p2align 4,,10
	.p2align 3
.L358:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	addl	$1, %ebx
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L358
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L476:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$10, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L342
	movl	12(%r13), %r9d
	testl	%r9d, %r9d
	jle	.L342
	.p2align 4,,10
	.p2align 3
.L346:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	addl	$1, %r14d
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r13)
	jg	.L346
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L485:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$10, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L349
	movl	12(%r13), %r8d
	testl	%r8d, %r8d
	jle	.L349
	.p2align 4,,10
	.p2align 3
.L353:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	addl	$1, %ebx
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L353
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L477:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L486:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L479:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	movb	$32, -524(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L392:
	movq	-552(%rbp), %rsi
	movq	-560(%rbp), %rdi
	movq	%r12, %rdx
	call	uv_tcp_getpeername@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L248:
	movq	-552(%rbp), %rsi
	movq	%r12, %rdx
	call	uv_tcp_getsockname@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L482:
	movq	-568(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %rax
	movq	%rax, -464(%rbp)
.L308:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-520(%rbp), %rax
	movq	-480(%rbp), %rdx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L483:
	cmpl	$1, 16(%r13)
	movq	0(%r13), %rdi
	leaq	-525(%rbp), %r14
	je	.L488
.L281:
	cmpb	$0, 8(%r13)
	jne	.L282
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L282
	movl	12(%r13), %esi
	testl	%esi, %esi
	jle	.L282
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L286:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	addl	$1, %r15d
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r13)
	jg	.L286
.L282:
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	movb	$58, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L284
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L284:
	movl	-520(%rbp), %esi
	call	_ZNSolsEi@PLT
	movl	$1, 16(%r13)
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	movb	$44, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L287
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	jne	.L287
	movl	12(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L287
	.p2align 4,,10
	.p2align 3
.L291:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	addl	$1, %ebx
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L291
.L287:
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	0(%r13), %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	movb	$58, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r13)
	movq	0(%r13), %rdi
	jne	.L289
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
.L289:
	movl	-524(%rbp), %esi
	call	_ZNSolsEi@PLT
	movl	$1, 16(%r13)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%rdx, -608(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-608(%rbp), %rdx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L484:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L481:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L480:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -520(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
	jmp	.L240
.L488:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$44, -525(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rdi
	jmp	.L281
.L465:
	call	__stack_chk_fail@PLT
.L471:
	call	_ZSt16__throw_bad_castv@PLT
.L487:
	movq	-536(%rbp), %rdi
	jmp	.L308
.L461:
	movq	-576(%rbp), %rdi
	jmp	.L193
	.cfi_endproc
.LFE6617:
	.size	_ZN6report10WalkHandleEP11uv_handle_sPv, .-_ZN6report10WalkHandleEP11uv_handle_sPv
	.weak	_ZZN4node6MallocIcEEPT_mE4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/util-inl.h:381"
.LC18:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"T* node::Malloc(size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIcEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIcEEPT_mE4args, 24
_ZZN4node6MallocIcEEPT_mE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC13:
	.quad	5579477748330686822
	.quad	8606183644856469107
	.section	.data.rel.ro,"aw"
	.align 8
.LC14:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC15:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC16:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
