	.file	"trace_event.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE
	.type	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE, @function
_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE:
.LFB5560:
	.cfi_startproc
	endbr64
	movq	%rdi, _ZN4node7tracing7g_agentE(%rip)
	ret
	.cfi_endproc
.LFE5560:
	.size	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE, .-_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing16TraceEventHelper8GetAgentEv
	.type	_ZN4node7tracing16TraceEventHelper8GetAgentEv, @function
_ZN4node7tracing16TraceEventHelper8GetAgentEv:
.LFB5561:
	.cfi_startproc
	endbr64
	movq	_ZN4node7tracing7g_agentE(%rip), %rax
	ret
	.cfi_endproc
.LFE5561:
	.size	_ZN4node7tracing16TraceEventHelper8GetAgentEv, .-_ZN4node7tracing16TraceEventHelper8GetAgentEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv
	.type	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv, @function
_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv:
.LFB5562:
	.cfi_startproc
	endbr64
	movq	_ZN4node7tracing7g_agentE(%rip), %rax
	movq	976(%rax), %rax
	testq	%rax, %rax
	je	.L9
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5562:
	.size	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv, .-_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv
	.globl	_ZN4node7tracing7g_agentE
	.bss
	.align 8
	.type	_ZN4node7tracing7g_agentE, @object
	.size	_ZN4node7tracing7g_agentE, 8
_ZN4node7tracing7g_agentE:
	.zero	8
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../src/tracing/agent.h:91"
.LC1:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
