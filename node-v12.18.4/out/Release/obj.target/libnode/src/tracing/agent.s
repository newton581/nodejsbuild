	.file	"agent.cc"
	.text
	.section	.text._ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s,"axG",@progbits,_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s
	.type	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s, @function
_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s:
.LFB5097:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5097:
	.size	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s, .-_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s
	.section	.text._ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv,"axG",@progbits,_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.type	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv, @function
_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv:
.LFB5101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_hrtime@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movabsq	$2361183241434822607, %rdx
	shrq	$3, %rax
	mulq	%rdx
	movq	%rdx, %rax
	shrq	$4, %rax
	ret
	.cfi_endproc
.LFE5101:
	.size	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv, .-_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.section	.text._ZN4node7tracing17TracingControllerD2Ev,"axG",@progbits,_ZN4node7tracing17TracingControllerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing17TracingControllerD2Ev
	.type	_ZN4node7tracing17TracingControllerD2Ev, @function
_ZN4node7tracing17TracingControllerD2Ev:
.LFB8843:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7tracing17TracingControllerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88platform7tracing17TracingControllerD2Ev@PLT
	.cfi_endproc
.LFE8843:
	.size	_ZN4node7tracing17TracingControllerD2Ev, .-_ZN4node7tracing17TracingControllerD2Ev
	.weak	_ZN4node7tracing17TracingControllerD1Ev
	.set	_ZN4node7tracing17TracingControllerD1Ev,_ZN4node7tracing17TracingControllerD2Ev
	.section	.text._ZN4node7tracing17TracingControllerD0Ev,"axG",@progbits,_ZN4node7tracing17TracingControllerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing17TracingControllerD0Ev
	.type	_ZN4node7tracing17TracingControllerD0Ev, @function
_ZN4node7tracing17TracingControllerD0Ev:
.LFB8845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing17TracingControllerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88platform7tracing17TracingControllerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8845:
	.size	_ZN4node7tracing17TracingControllerD0Ev, .-_ZN4node7tracing17TracingControllerD0Ev
	.text
	.p2align 4
	.type	_ZZN4node7tracing5Agent5StartEvENUlPvE_4_FUNES2_, @function
_ZZN4node7tracing5Agent5StartEvENUlPvE_4_FUNES2_:
.LFB7575:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	xorl	%esi, %esi
	jmp	uv_run@PLT
	.cfi_endproc
.LFE7575:
	.size	_ZZN4node7tracing5Agent5StartEvENUlPvE_4_FUNES2_, .-_ZZN4node7tracing5Agent5StartEvENUlPvE_4_FUNES2_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0:
.LFB10617:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L36
.L10:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	0(%r13), %r8
	movq	8(%r13), %r13
	leaq	48(%rax), %rdi
	movq	%rax, %r12
	movq	%rdi, 32(%rax)
	movq	%r8, %rax
	addq	%r13, %rax
	je	.L13
	testq	%r8, %r8
	je	.L37
.L13:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L38
	cmpq	$1, %r13
	jne	.L16
	movzbl	(%r8), %eax
	movb	%al, 48(%r12)
.L17:
	movq	%r13, 40(%r12)
	leaq	8(%rbx), %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movb	$0, (%rdi,%r13)
	movzbl	%r14b, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L17
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	32(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L15:
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rdi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	8(%rdi), %rax
	cmpq	%rax, %rdx
	je	.L10
	movq	8(%rcx), %r12
	movq	40(%rdx), %rcx
	cmpq	%rcx, %r12
	movq	%rcx, %rdx
	cmovbe	%r12, %rdx
	testq	%rdx, %rdx
	je	.L11
	movq	32(%r15), %rsi
	movq	0(%r13), %rdi
	movq	%rcx, -72(%rbp)
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L12
.L11:
	subq	%rcx, %r12
	xorl	%r14d, %r14d
	cmpq	$2147483647, %r12
	jg	.L10
	cmpq	$-2147483648, %r12
	jl	.L22
	movl	%r12d, %r14d
.L12:
	shrl	$31, %r14d
	jmp	.L10
.L37:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, %r14d
	jmp	.L10
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10617:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5AgentC2Ev
	.type	_ZN4node7tracing5AgentC2Ev, @function
_ZN4node7tracing5AgentC2Ev:
.LFB7567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	912(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, 864(%rdi)
	leaq	968(%rdi), %rax
	movss	.LC1(%rip), %xmm0
	movb	$0, 856(%rdi)
	movl	$1, 860(%rdi)
	movq	$1, 872(%rdi)
	movq	$0, 880(%rdi)
	movq	$0, 888(%rdi)
	movq	$0, 904(%rdi)
	movq	$0, 912(%rdi)
	movq	%rax, 920(%rdi)
	movq	$1, 928(%rdi)
	movq	$0, 936(%rdi)
	movq	$0, 944(%rdi)
	movq	$0, 960(%rdi)
	movq	$0, 968(%rdi)
	movss	%xmm0, 896(%rdi)
	movss	%xmm0, 952(%rdi)
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88platform7tracing17TracingControllerC2Ev@PLT
	leaq	16+_ZTVN4node7tracing17TracingControllerE(%rip), %rax
	movq	%r12, 976(%rbx)
	leaq	984(%rbx), %rdi
	movq	%rax, (%r12)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L43
	leaq	1024(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L47
	leaq	1208(%rbx), %rax
	movl	$0, 1208(%rbx)
	leaq	1248(%rbx), %rdi
	movq	$0, 1216(%rbx)
	movq	%rax, 1224(%rbx)
	movq	%rax, 1232(%rbx)
	movq	$0, 1240(%rbx)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L43
	leaq	1288(%rbx), %rax
	movq	976(%rbx), %rdi
	xorl	%esi, %esi
	leaq	8(%rbx), %r13
	movq	$0, 1304(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 1288(%rbx)
	call	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE@PLT
	movq	%r13, %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L48
	leaq	1072(%rbx), %r12
	leaq	_ZZN4node7tracing5AgentC4EvENUlP10uv_async_sE_4_FUNES3_(%rip), %rdx
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L49
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	_ZZN4node7tracing5AgentC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	_ZZN4node7tracing5AgentC4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7567:
	.size	_ZN4node7tracing5AgentC2Ev, .-_ZN4node7tracing5AgentC2Ev
	.globl	_ZN4node7tracing5AgentC1Ev
	.set	_ZN4node7tracing5AgentC1Ev,_ZN4node7tracing5AgentC2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent5StartEv
	.type	_ZN4node7tracing5Agent5StartEv, @function
_ZN4node7tracing5Agent5StartEv:
.LFB7573:
	.cfi_startproc
	endbr64
	cmpb	$0, 856(%rdi)
	je	.L57
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$584, %edi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	leaq	8(%rbx), %rcx
	movl	$1024, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing15NodeTraceBufferC1EmPNS0_5AgentEP9uv_loop_s@PLT
	movq	976(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE@PLT
	movq	%rbx, %rdx
	leaq	_ZZN4node7tracing5Agent5StartEvENUlPvE_4_FUNES2_(%rip), %rsi
	movq	%rbx, %rdi
	call	uv_thread_create@PLT
	testl	%eax, %eax
	jne	.L58
	movb	$1, 856(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	leaq	_ZZN4node7tracing5Agent5StartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7573:
	.size	_ZN4node7tracing5Agent5StartEv, .-_ZN4node7tracing5Agent5StartEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent13DefaultHandleEv
	.type	_ZN4node7tracing5Agent13DefaultHandleEv, @function
_ZN4node7tracing5Agent13DefaultHandleEv:
.LFB7586:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	%rdi, %rax
	movl	$-1, 8(%rdi)
	ret
	.cfi_endproc
.LFE7586:
	.size	_ZN4node7tracing5Agent13DefaultHandleEv, .-_ZN4node7tracing5Agent13DefaultHandleEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent11StopTracingEv
	.type	_ZN4node7tracing5Agent11StopTracingEv, @function
_ZN4node7tracing5Agent11StopTracingEv:
.LFB7587:
	.cfi_startproc
	endbr64
	cmpb	$0, 856(%rdi)
	jne	.L65
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	976(%rdi), %rdi
	call	_ZN2v88platform7tracing17TracingController11StopTracingEv@PLT
	movq	976(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE@PLT
	movq	%r12, %rdi
	movb	$0, 856(%r12)
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uv_thread_join@PLT
	.cfi_endproc
.LFE7587:
	.size	_ZN4node7tracing5Agent11StopTracingEv, .-_ZN4node7tracing5Agent11StopTracingEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE
	.type	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE, @function
_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE:
.LFB7593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	936(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L66
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L68:
	movq	16(%rbx), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L68
.L66:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7593:
	.size	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE, .-_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent16AddMetadataEventESt10unique_ptrIN2v88platform7tracing11TraceObjectESt14default_deleteIS6_EE
	.type	_ZN4node7tracing5Agent16AddMetadataEventESt10unique_ptrIN2v88platform7tracing11TraceObjectESt14default_deleteIS6_EE, @function
_ZN4node7tracing5Agent16AddMetadataEventESt10unique_ptrIN2v88platform7tracing11TraceObjectESt14default_deleteIS6_EE:
.LFB7596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	1248(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	1288(%rbx), %rsi
	movq	%rax, %rdi
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	%rax, 16(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 1304(%rbx)
	movq	%r13, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE7596:
	.size	_ZN4node7tracing5Agent16AddMetadataEventESt10unique_ptrIN2v88platform7tracing11TraceObjectESt14default_deleteIS6_EE, .-_ZN4node7tracing5Agent16AddMetadataEventESt10unique_ptrIN2v88platform7tracing11TraceObjectESt14default_deleteIS6_EE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent5FlushEb
	.type	_ZN4node7tracing5Agent5FlushEb, @function
_ZN4node7tracing5Agent5FlushEb:
.LFB7598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1248(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rax, %rdi
	pushq	%r12
	leaq	1288(%r13), %r14
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -60(%rbp)
	movq	%rax, -56(%rbp)
	call	uv_mutex_lock@PLT
	movq	1288(%r13), %r12
	cmpq	%r12, %r14
	je	.L81
	.p2align 4,,10
	.p2align 3
.L77:
	movq	936(%r13), %r15
	movq	16(%r12), %rbx
	testq	%r15, %r15
	je	.L82
	.p2align 4,,10
	.p2align 3
.L80:
	movq	16(%r15), %rdi
	movq	%rbx, %rsi
	movq	(%rdi), %rdx
	call	*16(%rdx)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L80
.L82:
	movq	(%r12), %r12
	cmpq	%r12, %r14
	jne	.L77
.L81:
	movq	-56(%rbp), %rdi
	call	uv_mutex_unlock@PLT
	movq	936(%r13), %rbx
	movzbl	-60(%rbp), %r12d
	testq	%rbx, %rbx
	je	.L76
	.p2align 4,,10
	.p2align 3
.L83:
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L83
.L76:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7598:
	.size	_ZN4node7tracing5Agent5FlushEb, .-_ZN4node7tracing5Agent5FlushEb
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj
	.type	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj, @function
_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj:
.LFB7599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$168, %edi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %rax
	movl	%ecx, -52(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rax, -72(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movl	$0, 56(%rax)
	movq	%rax, %r14
	movq	$0, 120(%rax)
	movups	%xmm0, 104(%rax)
	movq	(%r15), %rax
	call	*72(%rax)
	leaq	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv(%rip), %rdx
	movq	%rax, %rcx
	movq	(%r15), %rax
	movq	%rcx, -88(%rbp)
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L92
	call	uv_hrtime@PLT
	movq	-88(%rbp), %rcx
	movabsq	$2361183241434822607, %rsi
	shrq	$3, %rax
	mulq	%rsi
	shrq	$4, %rdx
.L93:
	movl	-52(%rbp), %eax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rcx
	movl	$77, %esi
	movq	%r13, %rcx
	movq	%r14, %rdi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	-80(%rbp)
	pushq	-72(%rbp)
	pushq	%rbx
	pushq	-64(%rbp)
	pushq	%rax
	pushq	$0
	call	_ZN2v88platform7tracing11TraceObject10InitializeEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjll@PLT
	addq	$80, %rsp
	call	_ZN4node7tracing16TraceEventHelper8GetAgentEv@PLT
	leaq	1248(%rax), %r12
	movq	%rax, %rbx
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	1288(%rbx), %rsi
	movq	%r14, 16(%rax)
	movq	%rax, %rdi
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 1304(%rbx)
	movq	%r12, %rdi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%r15, %rdi
	call	*%rax
	movq	-88(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L93
	.cfi_endproc
.LFE7599:
	.size	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj, .-_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8808:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L110
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L99:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L97
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L95
.L98:
	movq	%rbx, %r12
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L98
.L95:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8808:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,"axG",@progbits,_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.type	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, @function
_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E:
.LFB8855:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L121
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L115:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L115
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE8855:
	.size	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, .-_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5AgentD2Ev
	.type	_ZN4node7tracing5AgentD2Ev, @function
_ZN4node7tracing5AgentD2Ev:
.LFB7571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	880(%rdi), %r12
	testq	%r12, %r12
	je	.L125
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	32(%r13), %r15
	leaq	16(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%r15, %r15
	je	.L126
.L129:
	movq	-56(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r14
	cmpq	%rax, %rdi
	je	.L127
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L126
.L128:
	movq	%r14, %r15
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L128
.L126:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L130
.L125:
	movq	872(%rbx), %rax
	movq	864(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	936(%rbx), %r12
	movq	$0, 888(%rbx)
	movq	$0, 880(%rbx)
	testq	%r12, %r12
	jne	.L134
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L207:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L131
.L133:
	movq	%r13, %r12
.L134:
	movq	16(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L207
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L133
.L131:
	movq	928(%rbx), %rax
	movq	920(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	cmpb	$0, 856(%rbx)
	movq	$0, 944(%rbx)
	movq	$0, 936(%rbx)
	je	.L135
	movq	976(%rbx), %rdi
	call	_ZN2v88platform7tracing17TracingController11StopTracingEv@PLT
	movq	976(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE@PLT
	movb	$0, 856(%rbx)
	movq	%rbx, %rdi
	call	uv_thread_join@PLT
.L135:
	leaq	8(%rbx), %r12
	leaq	1072(%rbx), %rdi
	xorl	%esi, %esi
	call	uv_close@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	1288(%rbx), %r13
	call	uv_run@PLT
	movq	%r12, %rdi
	call	_ZN4node18CheckedUvLoopCloseEP9uv_loop_s@PLT
	movq	1288(%rbx), %r12
	cmpq	%r13, %r12
	jne	.L139
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%r15, %rdi
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	movq	%r15, %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	je	.L136
.L138:
	movq	%r14, %r12
.L139:
	movq	16(%r12), %r15
	movq	(%r12), %r14
	testq	%r15, %r15
	jne	.L208
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	jne	.L138
.L136:
	leaq	1248(%rbx), %rdi
	leaq	1200(%rbx), %r13
	call	uv_mutex_destroy@PLT
	movq	1216(%rbx), %r12
	testq	%r12, %r12
	je	.L140
.L141:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L141
.L140:
	leaq	1024(%rbx), %rdi
	call	uv_cond_destroy@PLT
	leaq	984(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	movq	976(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	call	*8(%rax)
.L142:
	movq	936(%rbx), %r12
	testq	%r12, %r12
	jne	.L146
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L143
.L145:
	movq	%r13, %r12
.L146:
	movq	16(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L209
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L145
.L143:
	movq	928(%rbx), %rax
	movq	920(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	920(%rbx), %rdi
	leaq	968(%rbx), %rax
	movq	$0, 944(%rbx)
	movq	$0, 936(%rbx)
	cmpq	%rax, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	880(%rbx), %r12
	testq	%r12, %r12
	je	.L148
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	32(%r13), %r15
	leaq	16(%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%r15, %r15
	je	.L149
.L152:
	movq	-56(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r14
	cmpq	%rax, %rdi
	je	.L150
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L149
.L151:
	movq	%r14, %r15
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L151
.L149:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L153
.L148:
	movq	872(%rbx), %rax
	movq	864(%rbx), %rdi
	xorl	%esi, %esi
	addq	$912, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L124
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7571:
	.size	_ZN4node7tracing5AgentD2Ev, .-_ZN4node7tracing5AgentD2Ev
	.globl	_ZN4node7tracing5AgentD1Ev
	.set	_ZN4node7tracing5AgentD1Ev,_ZN4node7tracing5AgentD2Ev
	.section	.text._ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_,"axG",@progbits,_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_
	.type	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_, @function
_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_:
.LFB8867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	testq	%r15, %r15
	je	.L211
	movq	(%rsi), %rcx
	movq	%r13, %r14
	movq	%r15, %rbx
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L255:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L254
.L232:
	movq	%rax, %rbx
.L212:
	cmpq	%rcx, 32(%rbx)
	jb	.L255
	movq	16(%rbx), %rax
	jbe	.L256
	movq	%rbx, %r14
	testq	%rax, %rax
	jne	.L232
.L254:
	movq	40(%r12), %r8
	cmpq	%r14, 24(%r12)
	jne	.L215
	cmpq	%r14, %r13
	je	.L229
.L215:
	xorl	%r8d, %r8d
.L210:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L219
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%rdx, %r14
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L225
.L219:
	cmpq	%rcx, 32(%rdx)
	ja	.L257
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L219
	.p2align 4,,10
	.p2align 3
.L225:
	testq	%rax, %rax
	je	.L220
.L258:
	cmpq	%rcx, 32(%rax)
	jb	.L224
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L258
.L220:
	movq	40(%r12), %r8
	cmpq	%rbx, 24(%r12)
	jne	.L227
	cmpq	%r14, %r13
	je	.L229
.L227:
	cmpq	%rbx, %r14
	je	.L215
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%rbx, %rdi
	movq	%rbx, %r15
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	movq	40(%r12), %rax
	movq	-56(%rbp), %r8
	subq	$1, %rax
	cmpq	%r14, %rbx
	movq	%rax, 40(%r12)
	jne	.L231
	subq	%rax, %r8
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L224:
	movq	24(%rax), %rax
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L229:
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r15, %rdi
	movq	16(%r15), %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
	testq	%r15, %r15
	jne	.L229
.L228:
	movq	$0, 16(%r12)
	movq	%r13, 24(%r12)
	movq	%r13, 32(%r12)
	movq	$0, 40(%r12)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L211:
	movq	40(%rdi), %r8
	cmpq	%r13, 24(%rdi)
	jne	.L215
	jmp	.L228
	.cfi_endproc
.LFE8867:
	.size	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_, .-_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent25InitializeWritersOnThreadEv
	.type	_ZN4node7tracing5Agent25InitializeWritersOnThreadEv, @function
_ZN4node7tracing5Agent25InitializeWritersOnThreadEv:
.LFB7569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	984(%rdi), %r14
	pushq	%r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	cmpq	$0, 1240(%r15)
	je	.L263
	leaq	8(%r15), %rax
	leaq	1200(%r15), %r13
	movq	%rax, -72(%rbp)
	leaq	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s(%rip), %r12
	leaq	-64(%rbp), %rbx
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_
	cmpq	$0, 1240(%r15)
	je	.L263
.L264:
	movq	1224(%r15), %rax
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	32(%rax), %rax
	cmpq	%r12, %rax
	je	.L261
	movq	-72(%rbp), %rsi
	call	*%rax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	1024(%r15), %rdi
	call	uv_cond_broadcast@PLT
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L269
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L269:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7569:
	.size	_ZN4node7tracing5Agent25InitializeWritersOnThreadEv, .-_ZN4node7tracing5Agent25InitializeWritersOnThreadEv
	.p2align 4
	.type	_ZZN4node7tracing5AgentC4EvENUlP10uv_async_sE_4_FUNES3_, @function
_ZZN4node7tracing5AgentC4EvENUlP10uv_async_sE_4_FUNES3_:
.LFB7565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1072(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-88(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	leaq	-1064(%rbx), %r15
	subq	$-128, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	cmpq	$0, 1240(%r14)
	jne	.L275
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_
	cmpq	$0, 1240(%r14)
	je	.L274
.L275:
	movq	1224(%r14), %rax
	leaq	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s(%rip), %rdx
	movq	32(%rax), %rdi
	movq	(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L272
	movq	%r15, %rsi
	call	*%rax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L274:
	leaq	1024(%r14), %rdi
	call	uv_cond_broadcast@PLT
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L280:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7565:
	.size	_ZZN4node7tracing5AgentC4EvENUlP10uv_async_sE_4_FUNES3_, .-_ZZN4node7tracing5AgentC4EvENUlP10uv_async_sE_4_FUNES3_
	.section	.text._ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.type	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, @function
_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_:
.LFB8891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L282
	movq	(%rsi), %r13
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L299:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L284
.L300:
	movq	%rax, %r12
.L283:
	movq	32(%r12), %rcx
	cmpq	%rcx, %r13
	jb	.L299
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L300
.L284:
	testb	%dl, %dl
	jne	.L301
	cmpq	%rcx, %r13
	jbe	.L292
.L291:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L302
.L289:
	movl	$40, %edi
	movl	%r8d, -52(%rbp)
	call	_Znwm@PLT
	movl	-52(%rbp), %r8d
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rax, %r13
	movq	(%r14), %rax
	movq	%r13, %rsi
	movl	%r8d, %edi
	movq	%rax, 32(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	cmpq	24(%rbx), %r12
	je	.L291
.L293:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	cmpq	32(%rax), %r13
	ja	.L291
	movq	%rax, %r12
.L292:
	addq	$24, %rsp
	movq	%r12, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	xorl	%r8d, %r8d
	cmpq	32(%r12), %r13
	setb	%r8b
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r15, %r12
	cmpq	24(%rdi), %r15
	je	.L295
	movq	(%rsi), %r13
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$1, %r8d
	jmp	.L289
	.cfi_endproc
.LFE8891:
	.size	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_, .-_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm:
.LFB9311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L304
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L314
.L330:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L315:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L328
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L329
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L307:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L309
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L311:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L312:
	testq	%rsi, %rsi
	je	.L309
.L310:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L311
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L317
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L310
	.p2align 4,,10
	.p2align 3
.L309:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L313
	call	_ZdlPv@PLT
.L313:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L330
.L314:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L316
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L316:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%rdx, %rdi
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L307
.L329:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9311:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi,"axG",@progbits,_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi:
.LFB8872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L332
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L344:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L332
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L332
.L334:
	cmpl	%esi, %r8d
	jne	.L344
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movl	$64, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movl	(%rbx), %eax
	movl	$1, %r8d
	movl	$0, 24(%rcx)
	movl	%eax, 8(%rcx)
	leaq	24(%rcx), %rax
	movq	$0, 32(%rcx)
	movq	%rax, 40(%rcx)
	movq	%rax, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8872:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi
	.section	.text._ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB8888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L346
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L358:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L346
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L346
.L348:
	cmpl	%esi, %r8d
	jne	.L358
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movl	$64, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movl	(%rbx), %eax
	movl	$1, %r8d
	movl	$0, 24(%rcx)
	movl	%eax, 8(%rcx)
	leaq	24(%rcx), %rax
	movq	$0, 32(%rcx)
	movq	%rax, 40(%rcx)
	movq	%rax, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSF_10_Hash_nodeISD_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8888:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB9347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L360
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L370
.L386:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L371:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L384
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L385
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L363:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L365
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L367:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L368:
	testq	%rsi, %rsi
	je	.L365
.L366:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L367
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L373
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L366
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L369
	call	_ZdlPv@PLT
.L369:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L386
.L370:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L372
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L372:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%rdx, %rdi
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L363
.L385:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9347:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB8876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L388
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L400:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L388
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L388
.L390:
	cmpl	%esi, %r8d
	jne	.L400
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	(%rbx), %eax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8876:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB9397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r12
	movq	%rax, %r8
	divq	%rdi
	leaq	(%r12,%rdx,8), %r13
	movq	0(%r13), %r11
	testq	%r11, %r11
	je	.L413
	movq	(%r11), %r14
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r14), %esi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L423:
	testq	%rcx, %rcx
	je	.L413
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L413
	movq	%rcx, %r14
.L404:
	movq	(%r14), %rcx
	cmpl	%esi, %r8d
	jne	.L423
	cmpq	%r9, %r11
	je	.L424
	testq	%rcx, %rcx
	je	.L406
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L406
	movq	%r9, (%r12,%rdx,8)
	movq	(%r14), %rcx
.L406:
	movq	16(%r14), %rdi
	movq	%rcx, (%r9)
	testq	%rdi, %rdi
	je	.L408
	movq	(%rdi), %rax
	call	*8(%rax)
.L408:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L414
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L406
	movq	%r9, (%r12,%rdx,8)
	movq	0(%r13), %rax
.L405:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L425
.L407:
	movq	$0, 0(%r13)
	movq	(%r14), %rcx
	jmp	.L406
.L414:
	movq	%r9, %rax
	jmp	.L405
.L425:
	movq	%rcx, 16(%rbx)
	jmp	.L407
	.cfi_endproc
.LFE9397:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB9398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r13
	movq	%rax, %r8
	divq	%rdi
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r11
	testq	%r11, %r11
	je	.L441
	movq	(%r11), %r12
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r12), %esi
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L455:
	testq	%rcx, %rcx
	je	.L441
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r12, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L441
	movq	%rcx, %r12
.L429:
	movq	(%r12), %rcx
	cmpl	%esi, %r8d
	jne	.L455
	cmpq	%r9, %r11
	je	.L456
	testq	%rcx, %rcx
	je	.L431
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L431
	movq	%r9, 0(%r13,%rdx,8)
	movq	(%r12), %rcx
.L431:
	movq	32(%r12), %r13
	movq	%rcx, (%r9)
	leaq	16(%r12), %r15
	testq	%r13, %r13
	je	.L435
.L433:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %r14
	cmpq	%rax, %rdi
	je	.L434
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L435
.L436:
	movq	%r14, %r13
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L441:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L436
.L435:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L442
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L431
	movq	%r9, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L430:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L457
.L432:
	movq	$0, (%r14)
	movq	(%r12), %rcx
	jmp	.L431
.L442:
	movq	%r9, %rax
	jmp	.L430
.L457:
	movq	%rcx, 16(%rbx)
	jmp	.L432
	.cfi_endproc
.LFE9398:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_:
.LFB9772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %r15
	je	.L522
	movq	40(%rsi), %rbx
	movq	8(%rdx), %r14
	movq	(%r12), %rsi
	cmpq	%r14, %rbx
	movq	%r14, %rdx
	cmovbe	%rbx, %rdx
	movq	%rsi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L472
	movq	%rdi, -64(%rbp)
	movq	32(%r13), %rdi
	call	memcmp@PLT
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L473
.L472:
	subq	%r14, %rbx
	cmpq	$2147483647, %rbx
	jg	.L474
	cmpq	$-2147483648, %rbx
	jl	.L475
	movl	%ebx, %eax
.L473:
	testl	%eax, %eax
	jns	.L474
.L475:
	xorl	%eax, %eax
	movq	%r13, %rdx
	cmpq	%r13, 32(%r8)
	je	.L464
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %rbx
	movq	%rax, %r12
	cmpq	%rbx, %r14
	movq	%rbx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L490
	movq	32(%rax), %rdi
	movq	-56(%rbp), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L491
.L490:
	subq	%r14, %rbx
	cmpq	$2147483647, %rbx
	jg	.L492
	cmpq	$-2147483648, %rbx
	jl	.L493
	movl	%ebx, %eax
.L491:
	testl	%eax, %eax
	jns	.L492
.L493:
	xorl	%eax, %eax
	xorl	%edx, %edx
.L464:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	%r13, %rax
	movq	%r13, %rdx
	cmpq	%r13, 24(%r8)
	je	.L464
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %r8
	movq	40(%rax), %rbx
	movq	%rax, %r12
	cmpq	%rbx, %r14
	movq	%rbx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L477
	movq	32(%rax), %rsi
	movq	-56(%rbp), %rdi
	call	memcmp@PLT
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L478
.L477:
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$2147483647, %rax
	jg	.L479
	cmpq	$-2147483648, %rax
	jl	.L480
.L478:
	testl	%eax, %eax
	js	.L480
.L479:
	cmpq	$0, 24(%r12)
	movq	%r13, %rdx
	movl	$0, %eax
	cmovne	%r13, %rax
	cmove	%r12, %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L460
	movq	32(%rdi), %rbx
	movq	8(%rdx), %r14
	movq	40(%rbx), %r15
	cmpq	%r15, %r14
	movq	%r15, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L461
	movq	%rdi, -56(%rbp)
	movq	32(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L462
.L461:
	subq	%r15, %r14
	cmpq	$2147483647, %r14
	jg	.L482
	cmpq	$-2147483648, %r14
	jl	.L460
	movl	%r14d, %eax
.L462:
	testl	%eax, %eax
	jns	.L482
.L460:
	movq	16(%r8), %rbx
	testq	%rbx, %rbx
	je	.L495
	movq	8(%r12), %r14
	movq	(%r12), %r13
	movl	$2147483648, %r12d
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L470:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L482
.L523:
	movq	%rax, %rbx
.L466:
	movq	40(%rbx), %r15
	cmpq	%r15, %r14
	movq	%r15, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L467
	movq	32(%rbx), %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L468
.L467:
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	%r12, %rax
	jge	.L469
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L470
.L468:
	testl	%eax, %eax
	js	.L470
.L469:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L523
.L482:
	addq	$24, %rsp
	movq	%rbx, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	cmpq	$0, 24(%r13)
	movq	%r13, %rdx
	movl	$0, %eax
	cmovne	%r12, %rax
	cmovne	%r12, %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movq	16(%r8), %rbx
	movl	$2147483648, %r12d
	movabsq	$-2147483649, %r13
	testq	%rbx, %rbx
	jne	.L483
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L487:
	movq	16(%rbx), %rax
.L488:
	testq	%rax, %rax
	je	.L482
	movq	%rax, %rbx
.L483:
	movq	40(%rbx), %r15
	cmpq	%r15, %r14
	movq	%r15, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L484
	movq	32(%rbx), %rsi
	movq	-56(%rbp), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L485
.L484:
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	%r12, %rax
	jge	.L486
	cmpq	%r13, %rax
	jle	.L487
.L485:
	testl	%eax, %eax
	js	.L487
.L486:
	movq	24(%rbx), %rax
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L495:
	movq	%r13, %rbx
	jmp	.L482
.L524:
	movq	%r15, %rbx
	jmp	.L482
	.cfi_endproc
.LFE9772:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_:
.LFB10011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L551
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L533:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L529
.L552:
	movq	%rax, %r15
.L528:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L530
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L531
.L530:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L532
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L533
.L531:
	testl	%eax, %eax
	js	.L533
.L532:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L552
.L529:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L527
.L535:
	testq	%rdx, %rdx
	je	.L538
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L539
.L538:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L540
	cmpq	$-2147483648, %rcx
	jl	.L541
	movl	%ecx, %eax
.L539:
	testl	%eax, %eax
	js	.L541
.L540:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L527:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L553
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L553:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10011:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node7tracing5Agent20GetEnabledCategoriesB5cxx11Ev
	.type	_ZNK4node7tracing5Agent20GetEnabledCategoriesB5cxx11Ev, @function
_ZNK4node7tracing5Agent20GetEnabledCategoriesB5cxx11Ev:
.LFB7592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movb	$0, 16(%rdi)
	movq	880(%rsi), %rbx
	movq	%rax, -168(%rbp)
	movq	%rax, (%rdi)
	leaq	-104(%rbp), %rax
	movq	$0, 8(%rdi)
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	testq	%rbx, %rbx
	je	.L554
	movq	%rdi, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L557:
	movq	40(%rbx), %r12
	leaq	24(%rbx), %rax
	leaq	-112(%rbp), %rsi
	movq	%rax, -136(%rbp)
	movq	%rsi, -144(%rbp)
	cmpq	%r12, %rax
	je	.L578
	movq	%rbx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L556:
	cmpq	$0, -72(%rbp)
	je	.L559
	movq	-80(%rbp), %r13
	movq	40(%r12), %rbx
	movq	40(%r13), %r14
	movq	%rbx, %rdx
	cmpq	%rbx, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L560
	movq	32(%r12), %rsi
	movq	32(%r13), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L561
.L560:
	movq	%r14, %rcx
	movl	$2147483648, %eax
	subq	%rbx, %rcx
	cmpq	%rax, %rcx
	jge	.L559
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L563
	movl	%ecx, %eax
.L561:
	testl	%eax, %eax
	js	.L563
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-96(%rbp), %r13
	testq	%r13, %r13
	je	.L590
	movq	40(%r12), %r14
	movq	32(%r12), %r11
	movq	%r12, -120(%rbp)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L570:
	movq	16(%r13), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L566
.L631:
	movq	%rax, %r13
.L565:
	movq	40(%r13), %r15
	movq	32(%r13), %r12
	cmpq	%r15, %r14
	movq	%r15, %rbx
	cmovbe	%r14, %rbx
	testq	%rbx, %rbx
	je	.L567
	movq	%r11, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r11, -128(%rbp)
	call	memcmp@PLT
	movq	-128(%rbp), %r11
	testl	%eax, %eax
	jne	.L568
.L567:
	movq	%r14, %rax
	movl	$2147483648, %esi
	subq	%r15, %rax
	cmpq	%rsi, %rax
	jge	.L569
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L570
.L568:
	testl	%eax, %eax
	js	.L570
.L569:
	movq	24(%r13), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L631
.L566:
	movq	%rbx, %r9
	movq	%r12, %rbx
	movq	-120(%rbp), %r12
	testb	%dl, %dl
	jne	.L564
.L572:
	testq	%r9, %r9
	je	.L573
	movq	%r9, %rdx
	movq	%r11, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L574
.L573:
	movq	%r15, %rcx
	movl	$2147483648, %eax
	subq	%r14, %rcx
	cmpq	%rax, %rcx
	jge	.L576
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L577
	movl	%ecx, %eax
.L574:
	testl	%eax, %eax
	jns	.L576
.L577:
	testq	%r13, %r13
	je	.L576
.L563:
	movq	-144(%rbp), %rdi
	leaq	32(%r12), %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0
.L576:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -136(%rbp)
	jne	.L556
	movq	-152(%rbp), %rbx
.L578:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L557
	movq	-176(%rbp), %r12
	movq	-88(%rbp), %r13
	cmpq	-160(%rbp), %r13
	je	.L630
	movq	-160(%rbp), %r14
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L581:
	movq	40(%r13), %rdx
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%r14, %rax
	je	.L630
.L579:
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.L581
	movq	(%r12), %rax
	leaq	1(%rbx), %r15
	cmpq	%rax, -168(%rbp)
	je	.L591
	movq	16(%r12), %rdx
.L582:
	cmpq	%rdx, %r15
	ja	.L632
.L583:
	movb	$44, (%rax,%rbx)
	movq	(%r12), %rax
	movq	%r15, 8(%r12)
	movb	$0, 1(%rax,%rbx)
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L590:
	movq	-160(%rbp), %r13
.L564:
	cmpq	%r13, -88(%rbp)
	je	.L563
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	40(%r12), %r14
	movq	32(%r12), %r11
	movq	40(%rax), %r15
	movq	32(%rax), %rbx
	cmpq	%r15, %r14
	movq	%r15, %r9
	cmovbe	%r14, %r9
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L586
.L554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L633
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movq	-96(%rbp), %r13
	leaq	-112(%rbp), %r14
	testq	%r13, %r13
	je	.L554
.L584:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L585
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L554
.L586:
	movq	%rbx, %r13
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L632:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L591:
	movl	$15, %edx
	jmp	.L582
.L633:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7592:
	.size	_ZNK4node7tracing5Agent20GetEnabledCategoriesB5cxx11Ev, .-_ZNK4node7tracing5Agent20GetEnabledCategoriesB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node7tracing5Agent17CreateTraceConfigEv
	.type	_ZNK4node7tracing5Agent17CreateTraceConfigEv, @function
_ZNK4node7tracing5Agent17CreateTraceConfigEv:
.LFB7591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 888(%rdi)
	movq	$0, -160(%rbp)
	je	.L634
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	880(%rbx), %rbx
	movl	$0, -104(%rbp)
	andb	$-4, 4(%rax)
	movq	%rax, -160(%rbp)
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	leaq	-104(%rbp), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -72(%rbp)
	testq	%rbx, %rbx
	je	.L634
	.p2align 4,,10
	.p2align 3
.L638:
	movq	40(%rbx), %r13
	leaq	24(%rbx), %rax
	leaq	-112(%rbp), %rdi
	movq	%rax, -136(%rbp)
	movq	%rdi, -144(%rbp)
	cmpq	%r13, %rax
	je	.L659
	movq	%rbx, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L637:
	cmpq	$0, -72(%rbp)
	je	.L640
	movq	-80(%rbp), %r12
	movq	40(%r13), %rbx
	movq	40(%r12), %r14
	movq	%rbx, %rdx
	cmpq	%rbx, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L641
	movq	32(%r13), %rsi
	movq	32(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L642
.L641:
	movq	%r14, %rcx
	movl	$2147483648, %eax
	subq	%rbx, %rcx
	cmpq	%rax, %rcx
	jge	.L640
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L644
	movl	%ecx, %eax
.L642:
	testl	%eax, %eax
	js	.L644
	.p2align 4,,10
	.p2align 3
.L640:
	movq	-96(%rbp), %r12
	testq	%r12, %r12
	je	.L669
	movq	40(%r13), %r14
	movq	32(%r13), %r11
	movq	%r13, -120(%rbp)
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L651:
	movq	16(%r12), %rax
	movl	$1, %edx
	testq	%rax, %rax
	je	.L647
.L704:
	movq	%rax, %r12
.L646:
	movq	40(%r12), %r15
	movq	32(%r12), %r13
	cmpq	%r15, %r14
	movq	%r15, %rbx
	cmovbe	%r14, %rbx
	testq	%rbx, %rbx
	je	.L648
	movq	%r11, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r11, -128(%rbp)
	call	memcmp@PLT
	movq	-128(%rbp), %r11
	testl	%eax, %eax
	jne	.L649
.L648:
	movq	%r14, %rax
	movl	$2147483648, %esi
	subq	%r15, %rax
	cmpq	%rsi, %rax
	jge	.L650
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rax
	jle	.L651
.L649:
	testl	%eax, %eax
	js	.L651
.L650:
	movq	24(%r12), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	jne	.L704
.L647:
	movq	%rbx, %r9
	movq	%r13, %rbx
	movq	-120(%rbp), %r13
	testb	%dl, %dl
	jne	.L645
.L653:
	testq	%r9, %r9
	je	.L654
	movq	%r9, %rdx
	movq	%r11, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L655
.L654:
	movq	%r15, %rcx
	movl	$2147483648, %eax
	subq	%r14, %rcx
	cmpq	%rax, %rcx
	jge	.L657
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L658
	movl	%ecx, %eax
.L655:
	testl	%eax, %eax
	jns	.L657
.L658:
	testq	%r12, %r12
	je	.L657
.L644:
	movq	-144(%rbp), %rdi
	leaq	32(%r13), %rcx
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0
.L657:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, -136(%rbp)
	jne	.L637
	movq	-152(%rbp), %rbx
.L659:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L638
	movq	-88(%rbp), %r12
	cmpq	-168(%rbp), %r12
	je	.L703
	movq	-160(%rbp), %rbx
	movq	-168(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L660:
	movq	32(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88platform7tracing11TraceConfig19AddIncludedCategoryEPKc@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%r13, %rax
	jne	.L660
.L703:
	movq	-96(%rbp), %r12
	leaq	-112(%rbp), %r13
	testq	%r12, %r12
	je	.L634
.L662:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L663
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L634
.L664:
	movq	%rbx, %r12
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L669:
	movq	-168(%rbp), %r12
.L645:
	cmpq	%r12, -88(%rbp)
	je	.L644
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	40(%r13), %r14
	movq	32(%r13), %r11
	movq	40(%rax), %r15
	movq	32(%rax), %rbx
	cmpq	%r15, %r14
	movq	%r15, %r9
	cmovbe	%r14, %r9
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L664
.L634:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L705
	movq	-160(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L705:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7591:
	.size	_ZNK4node7tracing5Agent17CreateTraceConfigEv, .-_ZNK4node7tracing5Agent17CreateTraceConfigEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent7DisableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE
	.type	_ZN4node7tracing5Agent7DisableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE, @function
_ZN4node7tracing5Agent7DisableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE:
.LFB7590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -52(%rbp)
	movq	976(%rdi), %rsi
	movq	%rdi, -96(%rbp)
	movq	%rsi, -104(%rbp)
	cmpl	$-1, %eax
	jne	.L707
	addq	$864, %rcx
	leaq	-52(%rbp), %rsi
	movq	%rcx, %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	24(%r13), %r12
	movq	$0, -96(%rbp)
	movq	%rax, -80(%rbp)
	leaq	8(%r13), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, %r12
	je	.L706
.L708:
	movq	-80(%rbp), %rax
	leaq	8(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L722:
	movq	-80(%rbp), %rax
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L711
	movq	40(%r12), %rbx
	movq	32(%r12), %r9
	movq	%r12, -64(%rbp)
	movq	%r13, %r12
	movq	%r13, -72(%rbp)
	movq	%rbx, %r14
	movq	%r9, %r13
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L717:
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.L713
.L712:
	movq	40(%r15), %rbx
	movq	%r14, %rdx
	cmpq	%r14, %rbx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L714
	movq	32(%r15), %rdi
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L715
.L714:
	movq	%rbx, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L716
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rax
	jle	.L717
.L715:
	testl	%eax, %eax
	js	.L717
.L716:
	movq	%r15, %r12
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L712
.L713:
	movq	%r13, %r9
	movq	-72(%rbp), %r13
	movq	%r14, %rbx
	movq	%r12, %r14
	movq	-64(%rbp), %r12
	cmpq	%r13, %r14
	je	.L711
	movq	40(%r14), %r15
	cmpq	%r15, %rbx
	movq	%r15, %rdx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L719
	movq	32(%r14), %rsi
	movq	%r9, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L720
.L719:
	movq	%rbx, %rcx
	movl	$2147483648, %eax
	subq	%r15, %rcx
	cmpq	%rax, %rcx
	jge	.L721
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L711
	movl	%ecx, %eax
.L720:
	testl	%eax, %eax
	js	.L711
.L721:
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	32(%rax), %rdi
	movq	%rax, %r15
	leaq	48(%rax), %rax
	cmpq	%rax, %rdi
	je	.L727
	call	_ZdlPv@PLT
.L727:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rax
	subq	$1, 40(%rax)
.L711:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	-88(%rbp), %rax
	jne	.L722
	cmpq	$0, -96(%rbp)
	je	.L706
.L710:
	movq	-96(%rbp), %rdi
	call	_ZNK4node7tracing5Agent17CreateTraceConfigEv
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L706
	movq	-104(%rbp), %rdi
	call	_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE@PLT
.L706:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	cmpb	$0, 856(%rdi)
	je	.L747
	movq	%rsi, %rdi
	call	_ZN2v88platform7tracing17TracingController11StopTracingEv@PLT
	movq	-96(%rbp), %rax
	leaq	-52(%rbp), %rsi
	leaq	864(%rax), %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	24(%r13), %r12
	movq	%rax, -80(%rbp)
	leaq	8(%r13), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rax, %r12
	jne	.L708
	jmp	.L710
.L747:
	leaq	_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7590:
	.size	_ZN4node7tracing5Agent7DisableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE, .-_ZN4node7tracing5Agent7DisableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent6EnableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE
	.type	_ZN4node7tracing5Agent6EnableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE, @function
_ZN4node7tracing5Agent6EnableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE:
.LFB7589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -88(%rbp)
	movl	%esi, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 40(%rdx)
	jne	.L804
.L748:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L805
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	.cfi_restore_state
	movq	976(%rdi), %rcx
	movq	%rdx, %rbx
	movq	%rdi, %rax
	movq	%rcx, -128(%rbp)
	cmpl	$-1, %esi
	jne	.L750
	addq	$864, %rax
	leaq	-68(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	24(%rbx), %r12
	movq	$0, -88(%rbp)
	movq	%rax, %r13
	leaq	8(%rbx), %rax
	movq	%rax, -120(%rbp)
	cmpq	%r12, %rax
	je	.L748
.L751:
	leaq	8(%r13), %r15
	leaq	-64(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%r15, -80(%rbp)
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L807:
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0
.L755:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	-120(%rbp), %rax
	je	.L806
.L771:
	movq	-80(%rbp), %rsi
	leaq	32(%r12), %r14
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	movq	%rax, %rsi
	testq	%rdx, %rdx
	jne	.L807
	movq	16(%r13), %r15
	testq	%r15, %r15
	je	.L779
	movq	40(%r12), %r14
	movq	32(%r12), %rsi
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L762:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L758
.L808:
	movq	%rax, %r15
.L757:
	movq	40(%r15), %rbx
	movq	%r14, %r12
	movq	32(%r15), %r13
	cmpq	%r14, %rbx
	cmovbe	%rbx, %r12
	testq	%r12, %r12
	je	.L759
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	call	memcmp@PLT
	movq	-112(%rbp), %rsi
	testl	%eax, %eax
	jne	.L760
.L759:
	movq	%rbx, %rax
	movl	$2147483648, %ecx
	subq	%r14, %rax
	cmpq	%rcx, %rax
	jge	.L761
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L762
.L760:
	testl	%eax, %eax
	js	.L762
.L761:
	movq	16(%r15), %rax
	testq	%rax, %rax
	jne	.L808
.L758:
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	-104(%rbp), %r13
	movq	-96(%rbp), %r12
	movl	$1, %ebx
	cmpq	-80(%rbp), %r15
	je	.L756
	testq	%rdx, %rdx
	je	.L764
	movq	%rcx, -96(%rbp)
	call	memcmp@PLT
	movq	-96(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L765
.L764:
	subq	%r14, %rcx
	movl	$2147483648, %eax
	movl	$1, %ebx
	cmpq	%rax, %rcx
	jge	.L756
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L782
	movl	%ecx, %ebx
.L765:
	notl	%ebx
	shrl	$31, %ebx
.L756:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	40(%r12), %r10
	leaq	48(%rax), %rdi
	movq	%rax, %r14
	movq	%rdi, 32(%rax)
	movq	32(%r12), %r11
	movq	%r11, %rax
	addq	%r10, %rax
	je	.L766
	testq	%r11, %r11
	je	.L809
.L766:
	movq	%r10, -64(%rbp)
	cmpq	$15, %r10
	ja	.L810
	cmpq	$1, %r10
	jne	.L769
	movzbl	(%r11), %eax
	movb	%al, 48(%r14)
.L770:
	movq	%r10, 40(%r14)
	movq	-80(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movb	$0, (%rdi,%r10)
	movzbl	%bl, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r13)
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L806:
	cmpq	$0, -88(%rbp)
	je	.L748
.L772:
	movq	-88(%rbp), %rdi
	call	_ZNK4node7tracing5Agent17CreateTraceConfigEv
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L748
	movq	-128(%rbp), %rdi
	call	_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE@PLT
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L769:
	testq	%r10, %r10
	je	.L770
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L810:
	movq	-136(%rbp), %rsi
	leaq	32(%r14), %rdi
	xorl	%edx, %edx
	movq	%r11, -104(%rbp)
	movq	%r10, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r11
	movq	%rax, 32(%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r14)
.L768:
	movq	%r10, %rdx
	movq	%r11, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r10
	movq	32(%r14), %rdi
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L750:
	cmpb	$0, 856(%rdi)
	je	.L811
	movq	%rcx, %rdi
	call	_ZN2v88platform7tracing17TracingController11StopTracingEv@PLT
	movq	-88(%rbp), %rax
	leaq	-68(%rbp), %rsi
	leaq	864(%rax), %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	24(%rbx), %r12
	movq	%rax, %r13
	leaq	8(%rbx), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, %r12
	jne	.L751
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L779:
	movq	-80(%rbp), %r15
	movl	$1, %ebx
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L811:
	leaq	_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L782:
	xorl	%ebx, %ebx
	jmp	.L756
.L805:
	call	__stack_chk_fail@PLT
.L809:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7589:
	.size	_ZN4node7tracing5Agent6EnableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE, .-_ZN4node7tracing5Agent6EnableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent10DisconnectEi
	.type	_ZN4node7tracing5Agent10DisconnectEi, @function
_ZN4node7tracing5Agent10DisconnectEi:
.LFB7588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, -52(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -40(%rbp)
	xorl	%edx, %edx
	cmpl	$-1, %esi
	je	.L812
	movq	%rdi, %r12
	leaq	984(%rdi), %r14
	leaq	-52(%rbp), %r13
	movq	%r14, %rdi
	leaq	920(%r12), %r15
	call	uv_mutex_lock@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	leaq	-48(%rbp), %rsi
	leaq	1200(%r12), %rdi
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE5eraseERKS3_
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	976(%r12), %r14
	cmpb	$0, 856(%r12)
	je	.L821
	movq	%r14, %rdi
	call	_ZN2v88platform7tracing17TracingController11StopTracingEv@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	leaq	864(%r12), %rdi
	movq	%r13, %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EEESaISD_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSF_18_Mod_range_hashingENSF_20_Default_ranged_hashENSF_20_Prime_rehash_policyENSF_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	movq	%r12, %rdi
	call	_ZNK4node7tracing5Agent17CreateTraceConfigEv
	testq	%rax, %rax
	je	.L812
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE@PLT
.L812:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L822
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	.cfi_restore_state
	leaq	_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L822:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7588:
	.size	_ZN4node7tracing5Agent10DisconnectEi, .-_ZN4node7tracing5Agent10DisconnectEi
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE
	.type	_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE, @function
_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE:
.LFB7577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$248, %rsp
	movq	%rdi, -248(%rbp)
	movq	%rsi, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 856(%rsi)
	je	.L958
.L824:
	leaq	-152(%rbp), %rax
	movq	$0, -120(%rbp)
	movl	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rax, -128(%rbp)
	testl	%r15d, %r15d
	je	.L959
	movq	-232(%rbp), %rax
	movq	976(%rax), %rax
	movq	%rax, -256(%rbp)
.L836:
	movq	-256(%rbp), %rdi
	call	_ZN2v88platform7tracing17TracingController11StopTracingEv@PLT
	movq	-232(%rbp), %rcx
	leaq	-180(%rbp), %rsi
	movq	%rsi, -264(%rbp)
	movl	860(%rcx), %eax
	leaq	920(%rcx), %rdi
	leal	1(%rax), %edx
	movl	%eax, -180(%rbp)
	movq	(%r12), %rax
	movl	%edx, 860(%rcx)
	movq	%rax, -176(%rbp)
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node7tracing16AsyncTraceWriterESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L845
	movq	(%rdi), %rax
	call	*8(%rax)
.L845:
	leaq	-104(%rbp), %rax
	movq	24(%rbx), %r12
	leaq	8(%rbx), %rcx
	movl	$0, -104(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-112(%rbp), %r15
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rcx, -216(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -272(%rbp)
	cmpq	%r12, %rcx
	je	.L866
	movq	%r15, -224(%rbp)
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L960:
	movq	%r13, %rcx
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0
.L850:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -216(%rbp)
	je	.L866
.L846:
	movq	-224(%rbp), %rbx
	movq	-208(%rbp), %rsi
	leaq	32(%r12), %r13
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	movq	%rax, %rsi
	testq	%rdx, %rdx
	jne	.L960
	movq	-96(%rbp), %r14
	testq	%r14, %r14
	je	.L885
	movq	32(%r12), %rsi
	movq	%r12, -240(%rbp)
	movq	40(%r12), %rbx
	movq	%rsi, -200(%rbp)
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L857:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L853
.L961:
	movq	%rax, %r14
.L852:
	movq	40(%r14), %r15
	movq	%rbx, %r12
	movq	32(%r14), %r13
	cmpq	%rbx, %r15
	cmovbe	%r15, %r12
	testq	%r12, %r12
	je	.L854
	movq	-200(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L855
.L854:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%rbx, %rax
	cmpq	%rcx, %rax
	jge	.L856
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L857
.L855:
	testl	%eax, %eax
	js	.L857
.L856:
	movq	16(%r14), %rax
	testq	%rax, %rax
	jne	.L961
.L853:
	movq	-200(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rax
	movb	$1, -200(%rbp)
	movq	-240(%rbp), %r12
	movq	%r15, %r13
	cmpq	-208(%rbp), %r14
	je	.L851
	testq	%rdx, %rdx
	je	.L859
	movq	%rax, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L859
.L860:
	notl	%eax
	movl	%eax, %ebx
	shrl	$31, %ebx
	movl	%ebx, -200(%rbp)
.L851:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	40(%r12), %r10
	leaq	48(%rax), %rdi
	movq	%rax, %r9
	movq	%rdi, 32(%rax)
	movq	32(%r12), %r11
	movq	%r11, %rax
	addq	%r10, %rax
	je	.L861
	testq	%r11, %r11
	je	.L962
.L861:
	movq	%r10, -168(%rbp)
	cmpq	$15, %r10
	ja	.L963
	cmpq	$1, %r10
	jne	.L864
	movzbl	(%r11), %eax
	movb	%al, 48(%r9)
.L865:
	movq	%r10, 40(%r9)
	movq	-208(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r9, %rsi
	movb	$0, (%rdi,%r10)
	movzbl	-200(%rbp), %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -72(%rbp)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r13, %rcx
	movl	$2147483648, %eax
	movb	$1, -200(%rbp)
	subq	%rbx, %rcx
	cmpq	%rax, %rcx
	jge	.L851
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L888
	movl	%ecx, %eax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L866:
	movq	-232(%rbp), %rax
	movq	-264(%rbp), %rsi
	leaq	864(%rax), %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	16(%rax), %r12
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.L847
.L848:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L869
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L847
.L870:
	movq	%r13, %r12
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L869:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L870
.L847:
	leaq	8(%rbx), %rax
	movq	$0, 16(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 32(%rbx)
	movq	-96(%rbp), %rdx
	movq	$0, 40(%rbx)
	testq	%rdx, %rdx
	je	.L868
	movl	-104(%rbp), %ecx
	movq	%rdx, 16(%rbx)
	movl	%ecx, 8(%rbx)
	movq	-88(%rbp), %rcx
	movq	%rcx, 24(%rbx)
	movq	-80(%rbp), %rcx
	movq	%rcx, 32(%rbx)
	movq	%rax, 8(%rdx)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%rbx)
.L868:
	movq	-232(%rbp), %r13
	leaq	984(%r13), %r12
	leaq	1208(%r13), %rbx
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	leaq	-176(%rbp), %rsi
	leaq	1200(%r13), %rdi
	call	_ZNSt8_Rb_treeIPN4node7tracing16AsyncTraceWriterES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE16_M_insert_uniqueIRKS3_EESt4pairISt17_Rb_tree_iteratorIS3_EbEOT_
	leaq	1072(%r13), %rdi
	call	uv_async_send@PLT
.L882:
	movq	1216(%r13), %rax
	testq	%rax, %rax
	je	.L871
	movq	-176(%rbp), %rdx
	movq	%rbx, %rcx
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L964:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L873
.L872:
	cmpq	%rdx, 32(%rax)
	jnb	.L964
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L872
.L873:
	cmpq	%rbx, %rcx
	je	.L871
	cmpq	%rdx, 32(%rcx)
	jbe	.L876
.L871:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	-248(%rbp), %rcx
	movq	-232(%rbp), %rdi
	movl	-180(%rbp), %eax
	movq	%rdi, (%rcx)
	movl	%eax, 8(%rcx)
	call	_ZNK4node7tracing5Agent17CreateTraceConfigEv
	testq	%rax, %rax
	je	.L877
	movq	-256(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88platform7tracing17TracingController12StartTracingEPNS1_11TraceConfigE@PLT
.L877:
	movq	-144(%rbp), %r12
	leaq	-160(%rbp), %rbx
	testq	%r12, %r12
	je	.L823
.L878:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L879
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L823
.L881:
	movq	%r13, %r12
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L876:
	leaq	1024(%r13), %rdi
	movq	%r12, %rsi
	call	uv_cond_wait@PLT
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L879:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L881
.L823:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L965
	movq	-248(%rbp), %rax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L865
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L963:
	movq	-272(%rbp), %rsi
	leaq	32(%r9), %rdi
	xorl	%edx, %edx
	movq	%r11, -288(%rbp)
	movq	%r10, -280(%rbp)
	movq	%r9, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-240(%rbp), %r9
	movq	-280(%rbp), %r10
	movq	%rax, %rdi
	movq	-288(%rbp), %r11
	movq	%rax, 32(%r9)
	movq	-168(%rbp), %rax
	movq	%rax, 48(%r9)
.L863:
	movq	%r10, %rdx
	movq	%r11, %rsi
	movq	%r9, -240(%rbp)
	call	memcpy@PLT
	movq	-240(%rbp), %r9
	movq	-168(%rbp), %r10
	movq	32(%r9), %rdi
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L958:
	movl	$584, %edi
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r13, %rdx
	leaq	8(%r13), %rcx
	movl	$1024, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN4node7tracing15NodeTraceBufferC1EmPNS0_5AgentEP9uv_loop_s@PLT
	movq	976(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN2v88platform7tracing17TracingController10InitializeEPNS1_11TraceBufferE@PLT
	movq	%r13, %rdx
	leaq	_ZZN4node7tracing5Agent5StartEvENUlPvE_4_FUNES2_(%rip), %rsi
	movq	%r13, %rdi
	call	uv_thread_create@PLT
	testl	%eax, %eax
	jne	.L966
	movq	-232(%rbp), %rax
	movb	$1, 856(%rax)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L959:
	movq	24(%rbx), %r15
	leaq	8(%rbx), %r14
	cmpq	%r14, %r15
	je	.L827
	movq	%r12, -216(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L834:
	leaq	32(%r15), %r12
	testq	%rax, %rax
	je	.L828
	movq	-128(%rbp), %r13
	movq	40(%r15), %r9
	movq	40(%r13), %r8
	movq	%r9, %rdx
	cmpq	%r9, %r8
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L829
	movq	32(%r15), %rsi
	movq	32(%r13), %rdi
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	memcmp@PLT
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jne	.L830
.L829:
	subq	%r9, %r8
	movl	$2147483648, %eax
	cmpq	%rax, %r8
	jge	.L828
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L831
	movl	%r8d, %eax
.L830:
	testl	%eax, %eax
	js	.L831
	.p2align 4,,10
	.p2align 3
.L828:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rax, %rsi
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L833
.L832:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0
.L833:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %r14
	je	.L952
	movq	-120(%rbp), %rax
	jmp	.L834
.L831:
	xorl	%esi, %esi
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L952:
	movq	-216(%rbp), %r12
.L827:
	movq	-232(%rbp), %rax
	leaq	-112(%rbp), %r15
	movl	$-1, -112(%rbp)
	movq	%r15, %rsi
	leaq	864(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi
	movq	%rbx, %rdi
	leaq	-168(%rbp), %rsi
	movl	$-1, -168(%rbp)
	leaq	8(%rax), %r14
	leaq	-160(%rbp), %rbx
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt8multisetINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS9_ESaIS9_EEESaISE_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOi
	movq	24(%rax), %r15
	cmpq	%r14, %r15
	je	.L844
	movq	%r12, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L835:
	cmpq	$0, -120(%rbp)
	leaq	32(%r15), %r12
	je	.L838
	movq	-128(%rbp), %r13
	movq	40(%r15), %r9
	movq	40(%r13), %r8
	movq	%r9, %rdx
	cmpq	%r9, %r8
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L839
	movq	32(%r15), %rsi
	movq	32(%r13), %rdi
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	memcmp@PLT
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jne	.L840
.L839:
	subq	%r9, %r8
	movl	$2147483648, %eax
	cmpq	%rax, %r8
	jge	.L838
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L841
	movl	%r8d, %eax
.L840:
	testl	%eax, %eax
	js	.L841
	.p2align 4,,10
	.p2align 3
.L838:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rax, %rsi
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L843
.L842:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE10_M_insert_IRKS5_NSB_11_Alloc_nodeEEESt17_Rb_tree_iteratorIS5_EPSt18_Rb_tree_node_baseSJ_OT_RT0_.isra.0
.L843:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, %r14
	jne	.L835
	movq	-216(%rbp), %r12
.L844:
	movq	-232(%rbp), %rax
	leaq	-160(%rbp), %rbx
	movq	976(%rax), %rsi
	cmpb	$0, 856(%rax)
	movq	%rsi, -256(%rbp)
	jne	.L836
	leaq	_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L841:
	xorl	%esi, %esi
	jmp	.L842
.L885:
	movb	$1, -200(%rbp)
	movq	-208(%rbp), %r14
	jmp	.L851
.L966:
	leaq	_ZZN4node7tracing5Agent5StartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L888:
	movb	$0, -200(%rbp)
	jmp	.L851
.L965:
	call	__stack_chk_fail@PLT
.L962:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7577:
	.size	_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE, .-_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE
	.weak	_ZTVN4node7tracing17TracingControllerE
	.section	.data.rel.ro._ZTVN4node7tracing17TracingControllerE,"awG",@progbits,_ZTVN4node7tracing17TracingControllerE,comdat
	.align 8
	.type	_ZTVN4node7tracing17TracingControllerE, @object
	.size	_ZTVN4node7tracing17TracingControllerE, 96
_ZTVN4node7tracing17TracingControllerE:
	.quad	0
	.quad	0
	.quad	_ZN4node7tracing17TracingControllerD1Ev
	.quad	_ZN4node7tracing17TracingControllerD0Ev
	.quad	_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc
	.quad	_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj
	.quad	_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl
	.quad	_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm
	.quad	_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.quad	_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.quad	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.quad	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC4:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC5:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC7:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"../src/tracing/agent.cc:97"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"(0) == (uv_thread_create(&thread_, [](void* arg) { Agent* agent = static_cast<Agent*>(arg); uv_run(&agent->tracing_loop_, UV_RUN_DEFAULT); }, this))"
	.align 8
.LC10:
	.string	"void node::tracing::Agent::Start()"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node7tracing5Agent5StartEvE4args, @object
	.size	_ZZN4node7tracing5Agent5StartEvE4args, 24
_ZZN4node7tracing5Agent5StartEvE4args:
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"../src/tracing/agent.cc:56"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"(uv_async_init(&tracing_loop_, &initialize_writer_async_, [](uv_async_t* async) { Agent* agent = ContainerOf(&Agent::initialize_writer_async_, async); agent->InitializeWritersOnThread(); })) == (0)"
	.section	.rodata.str1.1
.LC13:
	.string	"node::tracing::Agent::Agent()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing5AgentC4EvE4args_0, @object
	.size	_ZZN4node7tracing5AgentC4EvE4args_0, 24
_ZZN4node7tracing5AgentC4EvE4args_0:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.section	.rodata.str1.1
.LC14:
	.string	"../src/tracing/agent.cc:55"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"(uv_loop_init(&tracing_loop_)) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing5AgentC4EvE4args, @object
	.size	_ZZN4node7tracing5AgentC4EvE4args, 24
_ZZN4node7tracing5AgentC4EvE4args:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC13
	.weak	_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args
	.section	.rodata.str1.1
.LC16:
	.string	"../src/tracing/agent.cc:18"
.LC17:
	.string	"agent_->started_"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"node::tracing::Agent::ScopedSuspendTracing::ScopedSuspendTracing(node::tracing::TracingController*, node::tracing::Agent*, bool)"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args,"awG",@progbits,_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args, 24
_ZZN4node7tracing5Agent20ScopedSuspendTracingC4EPNS0_17TracingControllerEPS1_bE4args:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
