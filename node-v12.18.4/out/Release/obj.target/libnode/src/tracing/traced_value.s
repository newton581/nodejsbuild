	.file	"traced_value.cc"
	.text
	.section	.text._ZN4node7tracing11TracedValueD2Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD2Ev
	.type	_ZN4node7tracing11TracedValueD2Ev, @function
_ZN4node7tracing11TracedValueD2Ev:
.LFB5327:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L1
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE5327:
	.size	_ZN4node7tracing11TracedValueD2Ev, .-_ZN4node7tracing11TracedValueD2Ev
	.weak	_ZN4node7tracing11TracedValueD1Ev
	.set	_ZN4node7tracing11TracedValueD1Ev,_ZN4node7tracing11TracedValueD2Ev
	.section	.text._ZN4node7tracing11TracedValueD0Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD0Ev
	.type	_ZN4node7tracing11TracedValueD0Ev, @function
_ZN4node7tracing11TracedValueD0Ev:
.LFB5329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5329:
	.size	_ZN4node7tracing11TracedValueD0Ev, .-_ZN4node7tracing11TracedValueD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.text
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB5657:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L8
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L8:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L10
.L29:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L29
.L10:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L12
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L13:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L14
	testb	$4, %al
	jne	.L31
	testl	%eax, %eax
	je	.L15
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L32
.L15:
	movq	(%r12), %rcx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L15
	andl	$-8, %eax
	xorl	%edx, %edx
.L18:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L18
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L15
.L32:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L15
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5657:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.1
.LC1:
	.string	"\"-Infinity\""
.LC2:
	.string	"\"Infinity\""
.LC7:
	.string	"C"
	.text
	.p2align 4
	.type	_ZN4node7tracing12_GLOBAL__N_115DoubleToCStringEd, @function
_ZN4node7tracing12_GLOBAL__N_115DoubleToCStringEd:
.LFB4681:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movapd	%xmm0, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	andpd	.LC3(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	ucomisd	%xmm1, %xmm1
	jp	.L62
	ucomisd	.LC4(%rip), %xmm1
	ja	.L59
	comisd	.LC5(%rip), %xmm1
	jnb	.L38
	ucomisd	.LC6(%rip), %xmm0
	jp	.L38
	jne	.L38
	movl	$48, %edx
	movq	%r15, (%rdi)
	movq	$1, 8(%rdi)
	movw	%dx, 16(%rdi)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L59:
	pxor	%xmm1, %xmm1
	leaq	.LC1(%rip), %rbx
	leaq	.LC2(%rip), %rax
	movq	%r15, (%rdi)
	comisd	%xmm0, %xmm1
	cmovbe	%rax, %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	cmpl	$8, %eax
	jb	.L63
	movq	(%rbx), %rdx
	leaq	24(%r14), %rdi
	movq	%r15, %rcx
	andq	$-8, %rdi
	movq	%rdx, 16(%r14)
	subq	%rdi, %rcx
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rsi
	subq	%rcx, %rbx
	addl	%eax, %ecx
	andl	$-8, %ecx
	movq	%rsi, -8(%r15,%rdx)
	cmpl	$8, %ecx
	jb	.L46
	andl	$-8, %ecx
	xorl	%edx, %edx
.L49:
	movl	%edx, %esi
	addl	$8, %edx
	movq	(%rbx,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%ecx, %edx
	jb	.L49
.L46:
	movq	%rax, 8(%r14)
	movb	$0, 16(%r14,%rax)
.L33:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$456, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	.LC8(%rip), %xmm2
	leaq	-320(%rbp), %r12
	movsd	%xmm0, -488(%rbp)
	leaq	-432(%rbp), %rbx
	movq	%r12, %rdi
	leaq	-368(%rbp), %r13
	movhps	.LC9(%rip), %xmm2
	movaps	%xmm2, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm1, %xmm1
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm1, -88(%rbp)
	movups	%xmm1, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%rbx, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-464(%rbp), %xmm2
	pxor	%xmm1, %xmm1
	movq	%r13, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm1, -416(%rbp)
	movaps	%xmm2, -432(%rbp)
	movaps	%xmm1, -400(%rbp)
	movaps	%xmm1, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-448(%rbp), %r8
	leaq	.LC7(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -472(%rbp)
	call	_ZNSt6localeC1EPKc@PLT
	movq	-472(%rbp), %r8
	movq	%r12, %rsi
	leaq	-440(%rbp), %rdi
	movq	%rdi, -472(%rbp)
	movq	%r8, %rdx
	movq	%r8, -480(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5imbueERKSt6locale@PLT
	movq	-472(%rbp), %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	-480(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNSt6localeD1Ev@PLT
	movsd	-488(%rbp), %xmm0
	movq	%rbx, %rdi
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r15, (%r14)
	movq	$0, 8(%r14)
	movb	$0, 16(%r14)
	testq	%rax, %rax
	je	.L41
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L65
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L43:
	movq	.LC8(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC10(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-464(%rbp), %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r15, (%rdi)
	movl	$1314999842, 16(%rdi)
	movb	$34, 4(%r15)
	movq	$5, 8(%rdi)
	movb	$0, 21(%rdi)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L65:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L63:
	testb	$4, %al
	jne	.L66
	testl	%eax, %eax
	je	.L46
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r14)
	testb	$2, %al
	je	.L46
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %esi
	movw	%si, -2(%r15,%rdx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L66:
	movl	(%rbx), %edx
	movl	%edx, 16(%r14)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %esi
	movl	%esi, -4(%r15,%rdx)
	jmp	.L46
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4681:
	.size	_ZN4node7tracing12_GLOBAL__N_115DoubleToCStringEd, .-_ZN4node7tracing12_GLOBAL__N_115DoubleToCStringEd
	.align 2
	.p2align 4
	.globl	_ZNK4node7tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZNK4node7tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZNK4node7tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpb	$1, 41(%rdi)
	movq	8(%rsi), %r14
	sbbl	%r13d, %r13d
	movq	(%rsi), %rax
	andl	$32, %r13d
	leaq	1(%r14), %r9
	addl	$91, %r13d
	cmpq	%r15, %rax
	je	.L75
	movq	16(%rsi), %rdx
.L69:
	cmpq	%rdx, %r9
	ja	.L79
.L70:
	movb	%r13b, (%rax,%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r9, 8(%rbx)
	movb	$0, 1(%rax,%r14)
	movq	16(%r12), %rdx
	movq	8(%r12), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	cmpb	$1, 41(%r12)
	movq	8(%rbx), %r13
	sbbl	%r12d, %r12d
	movq	(%rbx), %rax
	andl	$32, %r12d
	leaq	1(%r13), %r14
	addl	$93, %r12d
	cmpq	%rax, %r15
	je	.L77
	movq	16(%rbx), %rdx
.L72:
	cmpq	%rdx, %r14
	ja	.L80
.L73:
	movb	%r12b, (%rax,%r13)
	movq	(%rbx), %rax
	movq	%r14, 8(%rbx)
	movb	$0, 1(%rax,%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%rbx), %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$15, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$15, %edx
	jmp	.L72
	.cfi_endproc
.LFE4714:
	.size	_ZNK4node7tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZNK4node7tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC13:
	.string	"basic_string::append"
.LC14:
	.string	"\\b"
.LC15:
	.string	"\\f"
.LC16:
	.string	"\\n"
.LC17:
	.string	"\\r"
.LC18:
	.string	"\\t"
.LC19:
	.string	"\\\\"
.LC20:
	.string	"\\\""
.LC21:
	.string	"\\u%04X"
.LC11:
	.string	" 000000000000\02000"
	.section	.rodata
.LC12:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.text
	.p2align 4
	.type	_ZN4node7tracing12_GLOBAL__N_112EscapeStringEPKc, @function
_ZN4node7tracing12_GLOBAL__N_112EscapeStringEPKc:
.LFB4679:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$1, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rax, -96(%rbp)
	movl	$34, %eax
	movw	%ax, 16(%rdi)
	movq	%rsi, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L109
	movl	%eax, %r13d
	xorl	%ecx, %ecx
	leaq	.L95(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L83:
	movslq	%ecx, %rsi
	leal	1(%rcx), %r14d
	addq	%r12, %rsi
	movzbl	(%rsi), %eax
	movl	%eax, %edi
	testb	%al, %al
	js	.L123
.L86:
	cmpl	$34, %eax
	jg	.L92
	cmpl	$7, %eax
	jle	.L93
	leal	-8(%rax), %edx
	cmpl	$26, %edx
	ja	.L93
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L95:
	.long	.L100-.L95
	.long	.L99-.L95
	.long	.L98-.L95
	.long	.L93-.L95
	.long	.L97-.L95
	.long	.L96-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L93-.L95
	.long	.L94-.L95
	.text
.L93:
	leal	-32(%rax), %edx
	cmpl	$94, %edx
	ja	.L124
	movabsq	$4611686018427387903, %rax
	movl	%r14d, %edx
	subq	8(%r15), %rax
	subl	%ecx, %edx
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	ja	.L104
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	.p2align 4,,10
	.p2align 3
.L103:
	cmpl	%r14d, %r13d
	jg	.L83
	movq	8(%r15), %r12
	movq	(%r15), %rax
	leaq	1(%r12), %rbx
	cmpq	%rax, -96(%rbp)
	je	.L125
	movq	16(%r15), %rdx
.L84:
	cmpq	%rdx, %rbx
	jbe	.L107
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r15), %rax
.L107:
	movb	$34, (%rax,%r12)
	movq	(%r15), %rax
	movq	%rbx, 8(%r15)
	movb	$0, (%rax,%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L94:
	.cfi_restore_state
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L104
	movl	$2, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
.L97:
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L104
	movl	$2, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
.L98:
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L104
	movl	$2, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
.L99:
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L104
	movl	$2, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
.L100:
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L104
	movl	$2, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
.L96:
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L104
	movl	$2, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L92:
	cmpl	$92, %eax
	jne	.L93
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	$1, %rax
	jbe	.L104
	movl	$2, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L123:
	cmpl	%r14d, %r13d
	je	.L119
	cmpb	$-33, %al
	jbe	.L88
	cmpb	$-17, %al
	ja	.L89
	movl	%eax, %edx
	movslq	%r14d, %rax
	leaq	.LC11(%rip), %r11
	andl	$15, %edi
	movzbl	(%r12,%rax), %eax
	movsbl	(%r11,%rdi), %r8d
	andl	$15, %edx
	movl	%eax, %edi
	shrb	$5, %dil
	btl	%edi, %r8d
	jnc	.L119
	andl	$63, %eax
	movl	%eax, %edi
.L90:
	addl	$1, %r14d
	cmpl	%r14d, %r13d
	je	.L119
	sall	$6, %edx
	movzbl	%dil, %edi
	movslq	%r14d, %rax
	orl	%edx, %edi
	movzbl	(%r12,%rax), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	jbe	.L127
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$65533, %r9d
.L87:
	leaq	-66(%rbp), %r10
	movl	$1, %edx
	movl	$10, %ecx
	xorl	%eax, %eax
	movq	%r10, %rdi
	leaq	.LC21(%rip), %r8
	movl	$10, %esi
	movq	%r10, -88(%rbp)
	call	__snprintf_chk@PLT
	movq	-88(%rbp), %r10
	movq	%r10, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r15), %rax
	cmpq	%rax, %rdx
	ja	.L104
	movq	-88(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movl	%r14d, %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L88:
	cmpb	$-63, %al
	jbe	.L119
	movslq	%r14d, %rax
	andl	$31, %edi
	movzbl	(%r12,%rax), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L119
.L127:
	sall	$6, %edi
	movzbl	%dl, %eax
	addl	$1, %r14d
	orl	%edi, %eax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L89:
	subl	$240, %eax
	cmpl	$4, %eax
	jg	.L119
	movslq	%r14d, %rdx
	leaq	.LC12(%rip), %r11
	movzbl	(%r12,%rdx), %edx
	movq	%rdx, %rdi
	shrq	$4, %rdi
	andl	$15, %edi
	movsbl	(%r11,%rdi), %edi
	btl	%eax, %edi
	jnc	.L119
	leal	2(%rcx), %r14d
	cmpl	%r14d, %r13d
	je	.L119
	movslq	%r14d, %rdi
	movzbl	(%r12,%rdi), %edi
	addl	$-128, %edi
	cmpb	$63, %dil
	ja	.L119
	sall	$6, %eax
	andl	$63, %edx
	orl	%eax, %edx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-96(%rbp), %rax
	movl	$2, %ebx
	movl	$1, %r12d
	jmp	.L107
.L125:
	movl	$15, %edx
	jmp	.L84
.L104:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L126:
	call	__stack_chk_fail@PLT
.L124:
	movzwl	%ax, %r9d
	jmp	.L87
	.cfi_endproc
.LFE4679:
	.size	_ZN4node7tracing12_GLOBAL__N_112EscapeStringEPKc, .-_ZN4node7tracing12_GLOBAL__N_112EscapeStringEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue6CreateEv
	.type	_ZN4node7tracing11TracedValue6CreateEv, @function
_ZN4node7tracing11TracedValue6CreateEv:
.LFB4684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movl	$1, %edx
	movq	$0, 16(%rax)
	movb	$0, 24(%rax)
	movw	%dx, 40(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4684:
	.size	_ZN4node7tracing11TracedValue6CreateEv, .-_ZN4node7tracing11TracedValue6CreateEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue11CreateArrayEv
	.type	_ZN4node7tracing11TracedValue11CreateArrayEv, @function
_ZN4node7tracing11TracedValue11CreateArrayEv:
.LFB4685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$48, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rcx
	leaq	24(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movl	$257, %edx
	movq	$0, 16(%rax)
	movb	$0, 24(%rax)
	movw	%dx, 40(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4685:
	.size	_ZN4node7tracing11TracedValue11CreateArrayEv, .-_ZN4node7tracing11TracedValue11CreateArrayEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValueC2Eb
	.type	_ZN4node7tracing11TracedValueC2Eb, @function
_ZN4node7tracing11TracedValueC2Eb:
.LFB4694:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	movb	$0, 24(%rdi)
	movb	$1, 40(%rdi)
	movb	%sil, 41(%rdi)
	ret
	.cfi_endproc
.LFE4694:
	.size	_ZN4node7tracing11TracedValueC2Eb, .-_ZN4node7tracing11TracedValueC2Eb
	.globl	_ZN4node7tracing11TracedValueC1Eb
	.set	_ZN4node7tracing11TracedValueC1Eb,_ZN4node7tracing11TracedValueC2Eb
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue13AppendIntegerEi
	.type	_ZN4node7tracing11TracedValue13AppendIntegerEi, @function
_ZN4node7tracing11TracedValue13AppendIntegerEi:
.LFB4703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L134
	movb	$0, 40(%rdi)
.L135:
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-96(%rbp), %rdi
	movl	%r12d, %r8d
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	16(%rdi), %r14
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L140
	movq	24(%rdi), %rdx
.L136:
	cmpq	%rdx, %r15
	ja	.L143
.L137:
	movb	$44, (%rax,%r14)
	movq	8(%rbx), %rax
	movq	%r15, 16(%rbx)
	movb	$0, 1(%rax,%r14)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$15, %edx
	jmp	.L136
.L142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4703:
	.size	_ZN4node7tracing11TracedValue13AppendIntegerEi, .-_ZN4node7tracing11TracedValue13AppendIntegerEi
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue12AppendDoubleEd
	.type	_ZN4node7tracing11TracedValue12AppendDoubleEd, @function
_ZN4node7tracing11TracedValue12AppendDoubleEd:
.LFB4704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L145
	movb	$0, 40(%rdi)
.L146:
	leaq	-80(%rbp), %rdi
	call	_ZN4node7tracing12_GLOBAL__N_115DoubleToCStringEd
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	16(%rdi), %r13
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L151
	movq	24(%rdi), %rdx
.L147:
	cmpq	%rdx, %r14
	ja	.L154
.L148:
	movb	$44, (%rax,%r13)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, 1(%rax,%r13)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	movsd	-88(%rbp), %xmm0
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$15, %edx
	jmp	.L147
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4704:
	.size	_ZN4node7tracing11TracedValue12AppendDoubleEd, .-_ZN4node7tracing11TracedValue12AppendDoubleEd
	.section	.rodata.str1.1
.LC22:
	.string	"true"
.LC23:
	.string	"false"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue13AppendBooleanEb
	.type	_ZN4node7tracing11TracedValue13AppendBooleanEb, @function
_ZN4node7tracing11TracedValue13AppendBooleanEb:
.LFB4705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$16, %rsp
	cmpb	$0, 40(%rbx)
	je	.L156
	movb	$0, 40(%rbx)
.L157:
	cmpb	$1, %r12b
	leaq	.LC23(%rip), %rax
	leaq	.LC22(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testb	%r12b, %r12b
	cmove	%rax, %rsi
	movabsq	$4611686018427387903, %rax
	subq	16(%rbx), %rax
	cmpq	%rdx, %rax
	jb	.L165
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	16(%rbx), %r13
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L162
	movq	24(%rbx), %rdx
.L158:
	cmpq	%rdx, %r14
	ja	.L166
.L159:
	movb	$44, (%rax,%r13)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, 1(%rax,%r13)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	movq	-40(%rbp), %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$15, %edx
	jmp	.L158
.L165:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4705:
	.size	_ZN4node7tracing11TracedValue13AppendBooleanEb, .-_ZN4node7tracing11TracedValue13AppendBooleanEb
	.section	.rodata.str1.1
.LC24:
	.string	"null"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue10AppendNullEv
	.type	_ZN4node7tracing11TracedValue10AppendNullEv, @function
_ZN4node7tracing11TracedValue10AppendNullEv:
.LFB4706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$24, %rsp
	cmpb	$0, 40(%rbx)
	movq	16(%rbx), %r12
	je	.L168
	movb	$0, 40(%rbx)
.L169:
	movabsq	$4611686018427387903, %rax
	subq	%r12, %rax
	cmpq	$3, %rax
	jbe	.L175
	addq	$24, %rsp
	movl	$4, %edx
	leaq	.LC24(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L173
	movq	24(%rbx), %rdx
.L170:
	cmpq	%rdx, %r13
	ja	.L176
.L171:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, (%rax,%r13)
	movq	16(%rbx), %r12
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rdi, -40(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	movq	-40(%rbp), %rdi
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$15, %edx
	jmp	.L170
.L175:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4706:
	.size	_ZN4node7tracing11TracedValue10AppendNullEv, .-_ZN4node7tracing11TracedValue10AppendNullEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue12AppendStringEPKc
	.type	_ZN4node7tracing11TracedValue12AppendStringEPKc, @function
_ZN4node7tracing11TracedValue12AppendStringEPKc:
.LFB4707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 40(%rdi)
	je	.L178
	movb	$0, 40(%rdi)
.L179:
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node7tracing12_GLOBAL__N_112EscapeStringEPKc
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	16(%rdi), %r14
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r14), %r15
	cmpq	%rdx, %rax
	je	.L184
	movq	24(%rdi), %rdx
.L180:
	cmpq	%rdx, %r15
	ja	.L187
.L181:
	movb	$44, (%rax,%r14)
	movq	8(%rbx), %rax
	movq	%r15, 16(%rbx)
	movb	$0, 1(%rax,%r14)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L184:
	movl	$15, %edx
	jmp	.L180
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4707:
	.size	_ZN4node7tracing11TracedValue12AppendStringEPKc, .-_ZN4node7tracing11TracedValue12AppendStringEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue15BeginDictionaryEv
	.type	_ZN4node7tracing11TracedValue15BeginDictionaryEv, @function
_ZN4node7tracing11TracedValue15BeginDictionaryEv:
.LFB4708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	cmpb	$0, 40(%rdi)
	movq	8(%rdi), %rax
	leaq	1(%r12), %r14
	je	.L189
	movb	$0, 40(%rdi)
	cmpq	%rax, %r13
	je	.L196
.L200:
	movq	24(%rbx), %rdx
	cmpq	%r14, %rdx
	jb	.L198
.L194:
	movb	$123, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	cmpq	%r13, %rax
	je	.L195
	movq	24(%rdi), %rdx
.L191:
	cmpq	%r14, %rdx
	jb	.L199
.L192:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	1(%r12), %r14
	cmpq	%rax, %r13
	jne	.L200
.L196:
	movl	$15, %edx
	cmpq	%r14, %rdx
	jnb	.L194
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$15, %edx
	jmp	.L191
	.cfi_endproc
.LFE4708:
	.size	_ZN4node7tracing11TracedValue15BeginDictionaryEv, .-_ZN4node7tracing11TracedValue15BeginDictionaryEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue10BeginArrayEv
	.type	_ZN4node7tracing11TracedValue10BeginArrayEv, @function
_ZN4node7tracing11TracedValue10BeginArrayEv:
.LFB4709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	cmpb	$0, 40(%rdi)
	movq	8(%rdi), %rax
	leaq	1(%r12), %r14
	je	.L202
	movb	$0, 40(%rdi)
	cmpq	%rax, %r13
	je	.L209
.L213:
	movq	24(%rbx), %rdx
	cmpq	%r14, %rdx
	jb	.L211
.L207:
	movb	$91, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	cmpq	%r13, %rax
	je	.L208
	movq	24(%rdi), %rdx
.L204:
	cmpq	%r14, %rdx
	jb	.L212
.L205:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r14, 16(%rbx)
	movb	$0, (%rax,%r14)
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	1(%r12), %r14
	cmpq	%rax, %r13
	jne	.L213
.L209:
	movl	$15, %edx
	cmpq	%r14, %rdx
	jnb	.L207
	.p2align 4,,10
	.p2align 3
.L211:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$15, %edx
	jmp	.L204
	.cfi_endproc
.LFE4709:
	.size	_ZN4node7tracing11TracedValue10BeginArrayEv, .-_ZN4node7tracing11TracedValue10BeginArrayEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue13EndDictionaryEv
	.type	_ZN4node7tracing11TracedValue13EndDictionaryEv, @function
_ZN4node7tracing11TracedValue13EndDictionaryEv:
.LFB4710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	movq	8(%rdi), %rax
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L217
	movq	24(%rdi), %rdx
.L215:
	cmpq	%rdx, %r13
	ja	.L219
.L216:
	movb	$125, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$15, %edx
	jmp	.L215
	.cfi_endproc
.LFE4710:
	.size	_ZN4node7tracing11TracedValue13EndDictionaryEv, .-_ZN4node7tracing11TracedValue13EndDictionaryEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue8EndArrayEv
	.type	_ZN4node7tracing11TracedValue8EndArrayEv, @function
_ZN4node7tracing11TracedValue8EndArrayEv:
.LFB4711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	movq	8(%rdi), %rax
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L223
	movq	24(%rdi), %rdx
.L221:
	cmpq	%rdx, %r13
	ja	.L225
.L222:
	movb	$93, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$15, %edx
	jmp	.L221
	.cfi_endproc
.LFE4711:
	.size	_ZN4node7tracing11TracedValue8EndArrayEv, .-_ZN4node7tracing11TracedValue8EndArrayEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue10WriteCommaEv
	.type	_ZN4node7tracing11TracedValue10WriteCommaEv, @function
_ZN4node7tracing11TracedValue10WriteCommaEv:
.LFB4712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 40(%rdi)
	je	.L227
	movb	$0, 40(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	movq	16(%rdi), %r12
	movq	8(%rdi), %rax
	leaq	24(%rdi), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L231
	movq	24(%rdi), %rdx
.L229:
	cmpq	%rdx, %r13
	ja	.L233
.L230:
	movb	$44, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$15, %edx
	jmp	.L229
	.cfi_endproc
.LFE4712:
	.size	_ZN4node7tracing11TracedValue10WriteCommaEv, .-_ZN4node7tracing11TracedValue10WriteCommaEv
	.section	.rodata.str1.1
.LC25:
	.string	"\":"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue9WriteNameEPKc
	.type	_ZN4node7tracing11TracedValue9WriteNameEPKc, @function
_ZN4node7tracing11TracedValue9WriteNameEPKc:
.LFB4713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r15
	cmpb	$0, 40(%rdi)
	movq	8(%rdi), %rax
	leaq	1(%r15), %r12
	je	.L235
	movb	$0, 40(%rdi)
	cmpq	%rax, %r9
	je	.L244
.L248:
	movq	24(%rbx), %rdx
	cmpq	%r12, %rdx
	jb	.L246
.L240:
	movb	$34, (%rax,%r15)
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movq	%r12, 16(%rbx)
	movb	$0, (%rax,%r12)
	movabsq	$4611686018427387903, %r12
	call	strlen@PLT
	movq	%rax, %rdx
	movq	%r12, %rax
	subq	16(%rbx), %rax
	cmpq	%rax, %rdx
	ja	.L242
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	subq	16(%rbx), %r12
	cmpq	$1, %r12
	jbe	.L242
	addq	$24, %rsp
	movq	%r14, %rdi
	movl	$2, %edx
	popq	%rbx
	leaq	.LC25(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	cmpq	%r9, %rax
	je	.L243
	movq	24(%rdi), %rdx
.L237:
	cmpq	%rdx, %r12
	ja	.L247
.L238:
	movb	$44, (%rax,%r15)
	movq	8(%rbx), %rax
	movq	%r12, 16(%rbx)
	movb	$0, (%rax,%r12)
	movq	16(%rbx), %r15
	movq	8(%rbx), %rax
	leaq	1(%r15), %r12
	cmpq	%rax, %r9
	jne	.L248
.L244:
	movl	$15, %edx
	cmpq	%r12, %rdx
	jnb	.L240
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r9, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %r9
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$15, %edx
	jmp	.L237
.L242:
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4713:
	.size	_ZN4node7tracing11TracedValue9WriteNameEPKc, .-_ZN4node7tracing11TracedValue9WriteNameEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue15BeginDictionaryEPKc
	.type	_ZN4node7tracing11TracedValue15BeginDictionaryEPKc, @function
_ZN4node7tracing11TracedValue15BeginDictionaryEPKc:
.LFB4701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node7tracing11TracedValue9WriteNameEPKc
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L252
	movq	24(%rbx), %rdx
.L250:
	cmpq	%rdx, %r13
	ja	.L254
.L251:
	movb	$123, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L252:
	movl	$15, %edx
	jmp	.L250
	.cfi_endproc
.LFE4701:
	.size	_ZN4node7tracing11TracedValue15BeginDictionaryEPKc, .-_ZN4node7tracing11TracedValue15BeginDictionaryEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue10BeginArrayEPKc
	.type	_ZN4node7tracing11TracedValue10BeginArrayEPKc, @function
_ZN4node7tracing11TracedValue10BeginArrayEPKc:
.LFB4702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node7tracing11TracedValue9WriteNameEPKc
	movq	16(%rbx), %r12
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	leaq	1(%r12), %r13
	cmpq	%rdx, %rax
	je	.L258
	movq	24(%rbx), %rdx
.L256:
	cmpq	%rdx, %r13
	ja	.L260
.L257:
	movb	$91, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	%r13, 16(%rbx)
	movb	$0, 1(%rax,%r12)
	movb	$1, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	leaq	8(%rbx), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	8(%rbx), %rax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L258:
	movl	$15, %edx
	jmp	.L256
	.cfi_endproc
.LFE4702:
	.size	_ZN4node7tracing11TracedValue10BeginArrayEPKc, .-_ZN4node7tracing11TracedValue10BeginArrayEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue10SetBooleanEPKcb
	.type	_ZN4node7tracing11TracedValue10SetBooleanEPKcb, @function
_ZN4node7tracing11TracedValue10SetBooleanEPKcb:
.LFB4698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN4node7tracing11TracedValue9WriteNameEPKc
	cmpb	$1, %r12b
	leaq	.LC23(%rip), %rax
	leaq	.LC22(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testb	%r12b, %r12b
	cmove	%rax, %rsi
	movabsq	$4611686018427387903, %rax
	subq	16(%rbx), %rax
	cmpq	%rdx, %rax
	jb	.L266
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L266:
	.cfi_restore_state
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4698:
	.size	_ZN4node7tracing11TracedValue10SetBooleanEPKcb, .-_ZN4node7tracing11TracedValue10SetBooleanEPKcb
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue7SetNullEPKc
	.type	_ZN4node7tracing11TracedValue7SetNullEPKc, @function
_ZN4node7tracing11TracedValue7SetNullEPKc:
.LFB4699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node7tracing11TracedValue9WriteNameEPKc
	movabsq	$4611686018427387903, %rax
	subq	16(%rbx), %rax
	cmpq	$3, %rax
	jbe	.L270
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	movl	$4, %edx
	popq	%rbx
	leaq	.LC24(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L270:
	.cfi_restore_state
	leaq	.LC13(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4699:
	.size	_ZN4node7tracing11TracedValue7SetNullEPKc, .-_ZN4node7tracing11TracedValue7SetNullEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue9SetStringEPKcS3_
	.type	_ZN4node7tracing11TracedValue9SetStringEPKcS3_, @function
_ZN4node7tracing11TracedValue9SetStringEPKcS3_:
.LFB4700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7tracing11TracedValue9WriteNameEPKc
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node7tracing12_GLOBAL__N_112EscapeStringEPKc
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	leaq	8(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L271
	call	_ZdlPv@PLT
.L271:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L275:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4700:
	.size	_ZN4node7tracing11TracedValue9SetStringEPKcS3_, .-_ZN4node7tracing11TracedValue9SetStringEPKcS3_
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue10SetIntegerEPKci
	.type	_ZN4node7tracing11TracedValue10SetIntegerEPKci, @function
_ZN4node7tracing11TracedValue10SetIntegerEPKci:
.LFB4696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7tracing11TracedValue9WriteNameEPKc
	leaq	-64(%rbp), %rdi
	movl	%r12d, %r8d
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	leaq	8(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L280:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4696:
	.size	_ZN4node7tracing11TracedValue10SetIntegerEPKci, .-_ZN4node7tracing11TracedValue10SetIntegerEPKci
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing11TracedValue9SetDoubleEPKcd
	.type	_ZN4node7tracing11TracedValue9SetDoubleEPKcd, @function
_ZN4node7tracing11TracedValue9SetDoubleEPKcd:
.LFB4697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$72, %rsp
	movsd	%xmm0, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7tracing11TracedValue9WriteNameEPKc
	movsd	-72(%rbp), %xmm0
	leaq	-64(%rbp), %rdi
	call	_ZN4node7tracing12_GLOBAL__N_115DoubleToCStringEd
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	leaq	8(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L285:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4697:
	.size	_ZN4node7tracing11TracedValue9SetDoubleEPKcd, .-_ZN4node7tracing11TracedValue9SetDoubleEPKcd
	.weak	_ZTVN4node7tracing11TracedValueE
	.section	.data.rel.ro.local._ZTVN4node7tracing11TracedValueE,"awG",@progbits,_ZTVN4node7tracing11TracedValueE,comdat
	.align 8
	.type	_ZTVN4node7tracing11TracedValueE, @object
	.size	_ZTVN4node7tracing11TracedValueE, 40
_ZTVN4node7tracing11TracedValueE:
	.quad	0
	.quad	0
	.quad	_ZN4node7tracing11TracedValueD1Ev
	.quad	_ZN4node7tracing11TracedValueD0Ev
	.quad	_ZNK4node7tracing11TracedValue19AppendAsTraceFormatEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	4294967295
	.long	2146435071
	.align 8
.LC5:
	.long	0
	.long	1048576
	.align 8
.LC6:
	.long	0
	.long	0
	.section	.data.rel.ro,"aw"
	.align 8
.LC8:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC9:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC10:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
