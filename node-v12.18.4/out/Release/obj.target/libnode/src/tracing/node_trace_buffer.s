	.file	"node_trace_buffer.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_s
	.type	_ZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_s, @function
_ZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_s:
.LFB5452:
	.cfi_startproc
	endbr64
	addq	$-128, %rdi
	leaq	_ZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_(%rip), %rsi
	jmp	uv_close@PLT
	.cfi_endproc
.LFE5452:
	.size	_ZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_s, .-_ZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_s
	.p2align 4
	.type	_ZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_, @function
_ZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_:
.LFB5458:
	.cfi_startproc
	endbr64
	subq	$-128, %rdi
	leaq	_ZZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_(%rip), %rsi
	jmp	uv_close@PLT
	.cfi_endproc
.LFE5458:
	.size	_ZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_, .-_ZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_
	.p2align 4
	.type	_ZZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_, @function
_ZZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_:
.LFB5456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	136(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movb	$1, 128(%rbx)
	leaq	176(%rbx), %rdi
	call	uv_cond_signal@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE5456:
	.size	_ZZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_, .-_ZZZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBufferD2Ev
	.type	_ZN4node7tracing15NodeTraceBufferD2Ev, @function
_ZN4node7tracing15NodeTraceBufferD2Ev:
.LFB5444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing15NodeTraceBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$144, %rdi
	leaq	280(%rbx), %r13
	leaq	320(%rbx), %r14
	subq	$40, %rsp
	movq	%rax, -144(%rdi)
	call	uv_async_send@PLT
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	cmpb	$0, 272(%rbx)
	jne	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	uv_cond_wait@PLT
	cmpb	$0, 272(%rbx)
	je	.L7
.L10:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	552(%rbx), %rax
	movq	544(%rbx), %r15
	movq	%rax, -72(%rbp)
	cmpq	%r15, %rax
	je	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L11
	leaq	-160(%r12), %rsi
	leaq	10592(%r12), %rax
	movq	%rsi, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	movq	-56(%rbp), %rax
	subq	$168, %rax
	cmpq	%rax, -64(%rbp)
	jne	.L12
	movl	$10768, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L11:
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L9
	movq	544(%rbx), %r15
.L8:
	testq	%r15, %r15
	je	.L13
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L13:
	leaq	480(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	movq	448(%rbx), %rax
	movq	440(%rbx), %r15
	movq	%rax, -72(%rbp)
	cmpq	%r15, %rax
	je	.L14
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L15
	leaq	-160(%r12), %rdx
	leaq	10592(%r12), %rax
	movq	%rdx, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	movq	-56(%rbp), %rax
	subq	$168, %rax
	cmpq	%rax, -64(%rbp)
	jne	.L16
	movl	$10768, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L15:
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L17
	movq	440(%rbx), %r15
.L14:
	testq	%r15, %r15
	je	.L18
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L18:
	leaq	376(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	movq	%r14, %rdi
	call	uv_cond_destroy@PLT
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE5444:
	.size	_ZN4node7tracing15NodeTraceBufferD2Ev, .-_ZN4node7tracing15NodeTraceBufferD2Ev
	.globl	_ZN4node7tracing15NodeTraceBufferD1Ev
	.set	_ZN4node7tracing15NodeTraceBufferD1Ev,_ZN4node7tracing15NodeTraceBufferD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBufferD0Ev
	.type	_ZN4node7tracing15NodeTraceBufferD0Ev, @function
_ZN4node7tracing15NodeTraceBufferD0Ev:
.LFB5446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node7tracing15NodeTraceBufferD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$584, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5446:
	.size	_ZN4node7tracing15NodeTraceBufferD0Ev, .-_ZN4node7tracing15NodeTraceBufferD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBuffer16GetEventByHandleEm
	.type	_ZN4node7tracing15NodeTraceBuffer16GetEventByHandleEm, @function
_ZN4node7tracing15NodeTraceBuffer16GetEventByHandleEm:
.LFB5448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	368(%rdi), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	testq	%rbx, %rbx
	je	.L41
	movl	%ebx, %eax
	xorl	%r13d, %r13d
	andl	$1, %eax
	cmpl	100(%r12), %eax
	jne	.L40
	movq	48(%r12), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	shrq	%rax
	salq	$6, %rcx
	divq	%rcx
	movq	%rdx, %rcx
	shrq	$6, %rcx
	cmpq	88(%r12), %rcx
	jnb	.L40
	movq	64(%r12), %rsi
	movq	(%rsi,%rcx,8), %rcx
	cmpl	%eax, 10760(%rcx)
	jne	.L40
	movq	%rdx, %rax
	andl	$63, %eax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	8(%rcx,%rax,8), %r13
.L40:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L40
	.cfi_endproc
.LFE5448:
	.size	_ZN4node7tracing15NodeTraceBuffer16GetEventByHandleEm, .-_ZN4node7tracing15NodeTraceBuffer16GetEventByHandleEm
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBuffer5FlushEv
	.type	_ZN4node7tracing15NodeTraceBuffer5FlushEv, @function
_ZN4node7tracing15NodeTraceBuffer5FlushEv:
.LFB5449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	376(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$24, %rsp
	call	uv_mutex_lock@PLT
	movq	464(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L47
	movb	$1, 416(%rbx)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L53:
	movq	440(%rbx), %rax
	leaq	(%rax,%r14,8), %r8
	movq	(%r8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L48
	movl	$8, %r13d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	(%rdx,%r13), %rsi
	cmpq	$0, 16(%rsi)
	je	.L49
	movq	432(%rbx), %rdi
	movq	%r8, -56(%rbp)
	addq	$1, %r12
	addq	$168, %r13
	call	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE@PLT
	movq	-56(%rbp), %r8
	movq	(%r8), %rdx
	movq	(%rdx), %rax
	cmpq	%rax, %r12
	jb	.L52
.L51:
	movq	464(%rbx), %rcx
.L48:
	addq	$1, %r14
	cmpq	%rcx, %r14
	jb	.L53
	movq	$0, 464(%rbx)
	movb	$0, 416(%rbx)
.L47:
	movq	%r15, %rdi
	leaq	480(%rbx), %r15
	call	uv_mutex_unlock@PLT
	movq	432(%rbx), %rdi
	movl	$1, %esi
	call	_ZN4node7tracing5Agent5FlushEb@PLT
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	568(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L54
	movb	$1, 520(%rbx)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L60:
	movq	544(%rbx), %rax
	leaq	(%rax,%r14,8), %r8
	movq	(%r8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L55
	movl	$8, %r13d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	(%rdx,%r13), %rsi
	cmpq	$0, 16(%rsi)
	je	.L56
	movq	536(%rbx), %rdi
	movq	%r8, -56(%rbp)
	addq	$1, %r12
	addq	$168, %r13
	call	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE@PLT
	movq	-56(%rbp), %r8
	movq	(%r8), %rdx
	movq	(%rdx), %rax
	cmpq	%rax, %r12
	jb	.L59
.L58:
	movq	568(%rbx), %rcx
.L55:
	addq	$1, %r14
	cmpq	%rcx, %r14
	jb	.L60
	movq	$0, 568(%rbx)
	movb	$0, 520(%rbx)
.L54:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	movq	536(%rbx), %rdi
	movl	$1, %esi
	call	_ZN4node7tracing5Agent5FlushEb@PLT
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	addq	$1, %r12
	addq	$168, %r13
	cmpq	%rax, %r12
	jb	.L59
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$1, %r12
	addq	$168, %r13
	cmpq	%rax, %r12
	jb	.L52
	jmp	.L51
	.cfi_endproc
.LFE5449:
	.size	_ZN4node7tracing15NodeTraceBuffer5FlushEv, .-_ZN4node7tracing15NodeTraceBuffer5FlushEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBuffer24NonBlockingFlushSignalCbEP10uv_async_s
	.type	_ZN4node7tracing15NodeTraceBuffer24NonBlockingFlushSignalCbEP10uv_async_s, @function
_ZN4node7tracing15NodeTraceBuffer24NonBlockingFlushSignalCbEP10uv_async_s:
.LFB5451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	464(%rbx), %rax
	cmpq	%rax, 424(%rbx)
	je	.L108
.L77:
	movq	568(%rbx), %rax
	cmpq	528(%rbx), %rax
	je	.L109
.L76:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	440(%rbx), %rdx
	movq	-8(%rdx,%rax,8), %rax
	cmpq	$64, (%rax)
	jne	.L77
	cmpb	$0, 416(%rbx)
	jne	.L77
	leaq	376(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	uv_mutex_lock@PLT
	movq	464(%rbx), %rax
	testq	%rax, %rax
	je	.L78
	movb	$1, 416(%rbx)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L84:
	movq	440(%rbx), %rdx
	leaq	(%rdx,%r14,8), %r13
	movq	0(%r13), %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L79
	movl	$8, %r15d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	(%rdx,%r15), %rsi
	cmpq	$0, 16(%rsi)
	je	.L80
	movq	432(%rbx), %rdi
	addq	$1, %r12
	addq	$168, %r15
	call	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE@PLT
	movq	0(%r13), %rdx
	movq	(%rdx), %rcx
	cmpq	%rcx, %r12
	jb	.L83
.L82:
	movq	464(%rbx), %rax
.L79:
	addq	$1, %r14
	cmpq	%rax, %r14
	jb	.L84
	movq	$0, 464(%rbx)
	movb	$0, 416(%rbx)
.L78:
	movq	-56(%rbp), %rdi
	call	uv_mutex_unlock@PLT
	movq	432(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN4node7tracing5Agent5FlushEb@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L109:
	movq	544(%rbx), %rdx
	movq	-8(%rdx,%rax,8), %rax
	cmpq	$64, (%rax)
	jne	.L76
	cmpb	$0, 520(%rbx)
	jne	.L76
	leaq	480(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	uv_mutex_lock@PLT
	movq	568(%rbx), %rax
	testq	%rax, %rax
	je	.L86
	movb	$1, 520(%rbx)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L92:
	movq	544(%rbx), %rdx
	leaq	(%rdx,%r14,8), %r13
	movq	0(%r13), %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L87
	movl	$8, %r15d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	(%rdx,%r15), %rsi
	cmpq	$0, 16(%rsi)
	je	.L88
	movq	536(%rbx), %rdi
	addq	$1, %r12
	addq	$168, %r15
	call	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE@PLT
	movq	0(%r13), %rdx
	movq	(%rdx), %rcx
	cmpq	%rcx, %r12
	jb	.L91
.L90:
	movq	568(%rbx), %rax
.L87:
	addq	$1, %r14
	cmpq	%rax, %r14
	jb	.L92
	movq	$0, 568(%rbx)
	movb	$0, 520(%rbx)
.L86:
	movq	-56(%rbp), %rdi
	call	uv_mutex_unlock@PLT
	movq	536(%rbx), %rdi
	addq	$24, %rsp
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node7tracing5Agent5FlushEb@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	addq	$1, %r12
	addq	$168, %r15
	cmpq	%rcx, %r12
	jb	.L91
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$1, %r12
	addq	$168, %r15
	cmpq	%rcx, %r12
	jb	.L83
	jmp	.L82
	.cfi_endproc
.LFE5451:
	.size	_ZN4node7tracing15NodeTraceBuffer24NonBlockingFlushSignalCbEP10uv_async_s, .-_ZN4node7tracing15NodeTraceBuffer24NonBlockingFlushSignalCbEP10uv_async_s
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing19InternalTraceBuffer13AddTraceEventEPm
	.type	_ZN4node7tracing19InternalTraceBuffer13AddTraceEventEPm, @function
_ZN4node7tracing19InternalTraceBuffer13AddTraceEventEPm:
.LFB5425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	88(%r12), %rax
	movq	64(%r12), %rdx
	testq	%rax, %rax
	je	.L112
	leaq	-8(%rdx,%rax,8), %r14
	movq	(%r14), %rdi
	cmpq	$64, (%rdi)
	je	.L112
.L113:
	leaq	-64(%rbp), %rsi
	call	_ZN2v88platform7tracing16TraceBufferChunk13AddTraceEventEPm@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	(%r14), %rax
	movl	10760(%rax), %edx
	imulq	48(%r12), %rdx
	movq	88(%r12), %rax
	leaq	-1(%rdx,%rax), %rdx
	movl	100(%r12), %eax
	salq	$6, %rdx
	addq	-64(%rbp), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, (%rbx)
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movl	96(%r12), %r13d
	leaq	(%rdx,%rax,8), %r14
	leaq	1(%rax), %rcx
	movq	(%r14), %rdi
	movq	%rcx, 88(%r12)
	leal	1(%r13), %eax
	movl	%eax, 96(%r12)
	testq	%rdi, %rdi
	je	.L114
	movl	%r13d, %esi
	call	_ZN2v88platform7tracing16TraceBufferChunk5ResetEj@PLT
	movq	88(%r12), %rdx
	movq	64(%r12), %rax
	leaq	-8(%rax,%rdx,8), %r14
	movq	(%r14), %rdi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$10768, %edi
	call	_Znwm@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88platform7tracing16TraceBufferChunkC1Ej@PLT
	movq	(%r14), %r15
	movq	-72(%rbp), %rax
	movq	%rax, (%r14)
	testq	%r15, %r15
	je	.L120
	leaq	10592(%r15), %r13
	leaq	-160(%r15), %r14
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r13, %rdi
	subq	$168, %r13
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	cmpq	%r13, %r14
	jne	.L116
	movl	$10768, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L120:
	movq	88(%r12), %rdx
	movq	64(%r12), %rax
	leaq	-8(%rax,%rdx,8), %r14
	movq	(%r14), %rdi
	jmp	.L113
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5425:
	.size	_ZN4node7tracing19InternalTraceBuffer13AddTraceEventEPm, .-_ZN4node7tracing19InternalTraceBuffer13AddTraceEventEPm
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBuffer13AddTraceEventEPm
	.type	_ZN4node7tracing15NodeTraceBuffer13AddTraceEventEPm, @function
_ZN4node7tracing15NodeTraceBuffer13AddTraceEventEPm:
.LFB5447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	368(%rdi), %r12
	movq	88(%r12), %rax
	cmpq	48(%r12), %rax
	je	.L130
.L123:
	movq	368(%rbx), %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node7tracing19InternalTraceBuffer13AddTraceEventEPm
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	64(%r12), %rdx
	movq	-8(%rdx,%rax,8), %rax
	cmpq	$64, (%rax)
	jne	.L123
	leaq	16(%rdi), %rdi
	movq	%rsi, -24(%rbp)
	call	uv_async_send@PLT
	leaq	376(%rbx), %rax
	leaq	480(%rbx), %rdx
	movq	-24(%rbp), %rsi
	cmpq	%r12, %rax
	cmove	%rdx, %rax
	movq	88(%rax), %rdx
	cmpq	48(%rax), %rdx
	je	.L131
.L125:
	movq	%rax, 368(%rbx)
	mfence
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L131:
	movq	64(%rax), %rcx
	movq	-8(%rcx,%rdx,8), %rdx
	cmpq	$64, (%rdx)
	jne	.L125
	movq	$0, (%rsi)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5447:
	.size	_ZN4node7tracing15NodeTraceBuffer13AddTraceEventEPm, .-_ZN4node7tracing15NodeTraceBuffer13AddTraceEventEPm
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing19InternalTraceBuffer16GetEventByHandleEm
	.type	_ZN4node7tracing19InternalTraceBuffer16GetEventByHandleEm, @function
_ZN4node7tracing19InternalTraceBuffer16GetEventByHandleEm:
.LFB5426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	testq	%rbx, %rbx
	je	.L134
	movl	%ebx, %eax
	xorl	%r13d, %r13d
	andl	$1, %eax
	cmpl	%eax, 100(%r12)
	jne	.L133
	movq	48(%r12), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	shrq	%rax
	salq	$6, %rcx
	divq	%rcx
	movq	%rdx, %rcx
	shrq	$6, %rcx
	cmpq	%rcx, 88(%r12)
	jbe	.L133
	movq	64(%r12), %rsi
	movq	(%rsi,%rcx,8), %rcx
	cmpl	%eax, 10760(%rcx)
	jne	.L133
	movq	%rdx, %rax
	andl	$63, %eax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	8(%rcx,%rax,8), %r13
.L133:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L133
	.cfi_endproc
.LFE5426:
	.size	_ZN4node7tracing19InternalTraceBuffer16GetEventByHandleEm, .-_ZN4node7tracing19InternalTraceBuffer16GetEventByHandleEm
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing19InternalTraceBuffer5FlushEb
	.type	_ZN4node7tracing19InternalTraceBuffer5FlushEb, @function
_ZN4node7tracing19InternalTraceBuffer5FlushEb:
.LFB5427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -52(%rbp)
	call	uv_mutex_lock@PLT
	movq	88(%r15), %rsi
	testq	%rsi, %rsi
	je	.L140
	movb	$1, 40(%r15)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L146:
	movq	64(%r15), %rax
	leaq	(%rax,%r14,8), %r13
	movq	0(%r13), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L141
	movl	$8, %ebx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	(%rax,%rbx), %rsi
	cmpq	$0, 16(%rsi)
	je	.L142
	movq	56(%r15), %rdi
	addq	$1, %r12
	addq	$168, %rbx
	call	_ZN4node7tracing5Agent16AppendTraceEventEPN2v88platform7tracing11TraceObjectE@PLT
	movq	0(%r13), %rax
	movq	(%rax), %rdx
	cmpq	%rdx, %r12
	jb	.L145
.L144:
	movq	88(%r15), %rsi
.L141:
	addq	$1, %r14
	cmpq	%rsi, %r14
	jb	.L146
	movq	$0, 88(%r15)
	movb	$0, 40(%r15)
.L140:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	movzbl	-52(%rbp), %esi
	movq	56(%r15), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node7tracing5Agent5FlushEb@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	addq	$1, %r12
	addq	$168, %rbx
	cmpq	%rdx, %r12
	jb	.L145
	jmp	.L144
	.cfi_endproc
.LFE5427:
	.size	_ZN4node7tracing19InternalTraceBuffer5FlushEb, .-_ZN4node7tracing19InternalTraceBuffer5FlushEb
	.align 2
	.p2align 4
	.globl	_ZNK4node7tracing19InternalTraceBuffer10MakeHandleEmjm
	.type	_ZNK4node7tracing19InternalTraceBuffer10MakeHandleEmjm, @function
_ZNK4node7tracing19InternalTraceBuffer10MakeHandleEmjm:
.LFB5428:
	.cfi_startproc
	endbr64
	movl	%edx, %edx
	imulq	48(%rdi), %rdx
	movl	100(%rdi), %eax
	addq	%rsi, %rdx
	salq	$6, %rdx
	addq	%rcx, %rdx
	leaq	(%rax,%rdx,2), %rax
	ret
	.cfi_endproc
.LFE5428:
	.size	_ZNK4node7tracing19InternalTraceBuffer10MakeHandleEmjm, .-_ZNK4node7tracing19InternalTraceBuffer10MakeHandleEmjm
	.align 2
	.p2align 4
	.globl	_ZNK4node7tracing19InternalTraceBuffer13ExtractHandleEmPjPmS2_S3_
	.type	_ZNK4node7tracing19InternalTraceBuffer13ExtractHandleEmPjPmS2_S3_, @function
_ZNK4node7tracing19InternalTraceBuffer13ExtractHandleEmPjPmS2_S3_:
.LFB5429:
	.cfi_startproc
	endbr64
	movq	%rcx, %r10
	movl	%esi, %ecx
	movq	%rsi, %rax
	andl	$1, %ecx
	shrq	%rax
	movl	%ecx, (%rdx)
	movq	48(%rdi), %rcx
	xorl	%edx, %edx
	salq	$6, %rcx
	divq	%rcx
	movl	%eax, (%r8)
	movq	%rdx, %rax
	shrq	$6, %rax
	movq	%rax, (%r10)
	movq	%rdx, %rax
	andl	$63, %eax
	movq	%rax, (%r9)
	ret
	.cfi_endproc
.LFE5429:
	.size	_ZNK4node7tracing19InternalTraceBuffer13ExtractHandleEmPjPmS2_S3_, .-_ZNK4node7tracing19InternalTraceBuffer13ExtractHandleEmPjPmS2_S3_
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBuffer22TryLoadAvailableBufferEv
	.type	_ZN4node7tracing15NodeTraceBuffer22TryLoadAvailableBufferEv, @function
_ZN4node7tracing15NodeTraceBuffer22TryLoadAvailableBufferEv:
.LFB5450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	368(%rdi), %r12
	movq	88(%r12), %rax
	cmpq	%rax, 48(%r12)
	je	.L165
.L157:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	64(%r12), %rdx
	movq	-8(%rdx,%rax,8), %rax
	cmpq	$64, (%rax)
	jne	.L157
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	call	uv_async_send@PLT
	leaq	376(%rbx), %rax
	leaq	480(%rbx), %rdx
	cmpq	%r12, %rax
	cmove	%rdx, %rax
	movq	88(%rax), %rdx
	cmpq	48(%rax), %rdx
	je	.L166
.L160:
	movl	$1, %r8d
	movq	%rax, 368(%rbx)
	movl	%r8d, %eax
	mfence
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	64(%rax), %rcx
	xorl	%r8d, %r8d
	movq	-8(%rcx,%rdx,8), %rdx
	cmpq	$64, (%rdx)
	jne	.L160
	jmp	.L157
	.cfi_endproc
.LFE5450:
	.size	_ZN4node7tracing15NodeTraceBuffer22TryLoadAvailableBufferEv, .-_ZN4node7tracing15NodeTraceBuffer22TryLoadAvailableBufferEv
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm
	.type	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm, @function
_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm:
.LFB6138:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L204
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %rcx
	movq	%rcx, %r13
	subq	(%rdi), %r13
	movq	%r13, %rax
	sarq	$3, %rax
	movq	%rax, -80(%rbp)
	subq	%rax, %rsi
	movq	16(%rdi), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jb	.L169
	cmpq	$1, %rbx
	je	.L185
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rax, %rdx
	ja	.L171
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L172
.L170:
	movq	$0, (%rdx)
.L172:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 8(%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L207
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	movq	%rdi, %rax
	cmovb	%rbx, %rax
	addq	%rdi, %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	leaq	(%rax,%r13), %rdx
	cmpq	$1, %rbx
	je	.L184
	leaq	-2(%rbx), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rdx,%rsi)
	cmpq	%rax, %rcx
	ja	.L178
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L176
.L184:
	movq	$0, (%rdx)
.L176:
	movq	8(%r12), %rax
	movq	(%r12), %r15
	movq	%rax, -72(%rbp)
	cmpq	%r15, %rax
	je	.L179
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%r15), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r15)
	movq	%rax, (%rcx)
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L180
	leaq	-160(%r13), %rax
	leaq	10592(%r13), %r14
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%r14, %rdi
	subq	$168, %r14
	call	_ZN2v88platform7tracing11TraceObjectD1Ev@PLT
	cmpq	-56(%rbp), %r14
	jne	.L181
	movl	$10768, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L180:
	addq	$8, -64(%rbp)
	addq	$8, %r15
	cmpq	%r15, -72(%rbp)
	jne	.L182
	movq	(%r12), %r15
.L179:
	testq	%r15, %r15
	je	.L183
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L183:
	movq	-88(%rbp), %rdi
	addq	-80(%rbp), %rbx
	leaq	(%rdi,%rbx,8), %rax
	movq	%rdi, (%r12)
	movq	%rax, 8(%r12)
	movq	-96(%rbp), %rax
	addq	%rdi, %rax
	movq	%rax, 16(%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	%rcx, %rdx
	jmp	.L170
.L207:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6138:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm, .-_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing19InternalTraceBufferC2EmjPNS0_5AgentE
	.type	_ZN4node7tracing19InternalTraceBufferC2EmjPNS0_5AgentE, @function
_ZN4node7tracing19InternalTraceBufferC2EmjPNS0_5AgentE:
.LFB5423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L212
	movb	$0, 40(%rbx)
	movq	%r12, 48(%rbx)
	movq	%r14, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movl	$1, 96(%rbx)
	movl	%r13d, 100(%rbx)
	testq	%r12, %r12
	jne	.L213
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6vectorISt10unique_ptrIN2v88platform7tracing16TraceBufferChunkESt14default_deleteIS4_EESaIS7_EE17_M_default_appendEm
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5423:
	.size	_ZN4node7tracing19InternalTraceBufferC2EmjPNS0_5AgentE, .-_ZN4node7tracing19InternalTraceBufferC2EmjPNS0_5AgentE
	.globl	_ZN4node7tracing19InternalTraceBufferC1EmjPNS0_5AgentE
	.set	_ZN4node7tracing19InternalTraceBufferC1EmjPNS0_5AgentE,_ZN4node7tracing19InternalTraceBufferC2EmjPNS0_5AgentE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceBufferC2EmPNS0_5AgentEP9uv_loop_s
	.type	_ZN4node7tracing15NodeTraceBufferC2EmPNS0_5AgentEP9uv_loop_s, @function
_ZN4node7tracing15NodeTraceBufferC2EmPNS0_5AgentEP9uv_loop_s:
.LFB5441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing15NodeTraceBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$280, %rdi
	movq	%rax, -280(%rdi)
	movq	%rcx, -272(%rdi)
	movb	$0, -8(%rdi)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L220
	leaq	320(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L221
	leaq	376(%rbx), %r14
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN4node7tracing19InternalTraceBufferC1EmjPNS0_5AgentE
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rcx
	leaq	480(%rbx), %rdi
	call	_ZN4node7tracing19InternalTraceBufferC1EmjPNS0_5AgentE
	leaq	16(%rbx), %rsi
	leaq	_ZN4node7tracing15NodeTraceBuffer24NonBlockingFlushSignalCbEP10uv_async_s(%rip), %rdx
	movq	%r14, 368(%rbx)
	mfence
	movq	%rbx, 16(%rbx)
	movq	8(%rbx), %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L222
	movq	%rbx, 144(%rbx)
	movq	8(%rbx), %rdi
	leaq	144(%rbx), %rsi
	leaq	_ZN4node7tracing15NodeTraceBuffer12ExitSignalCbEP10uv_async_s(%rip), %rdx
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L223
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5441:
	.size	_ZN4node7tracing15NodeTraceBufferC2EmPNS0_5AgentEP9uv_loop_s, .-_ZN4node7tracing15NodeTraceBufferC2EmPNS0_5AgentEP9uv_loop_s
	.globl	_ZN4node7tracing15NodeTraceBufferC1EmPNS0_5AgentEP9uv_loop_s
	.set	_ZN4node7tracing15NodeTraceBufferC1EmPNS0_5AgentEP9uv_loop_s,_ZN4node7tracing15NodeTraceBufferC2EmPNS0_5AgentEP9uv_loop_s
	.weak	_ZTVN4node7tracing15NodeTraceBufferE
	.section	.data.rel.ro.local._ZTVN4node7tracing15NodeTraceBufferE,"awG",@progbits,_ZTVN4node7tracing15NodeTraceBufferE,comdat
	.align 8
	.type	_ZTVN4node7tracing15NodeTraceBufferE, @object
	.size	_ZTVN4node7tracing15NodeTraceBufferE, 56
_ZTVN4node7tracing15NodeTraceBufferE:
	.quad	0
	.quad	0
	.quad	_ZN4node7tracing15NodeTraceBufferD1Ev
	.quad	_ZN4node7tracing15NodeTraceBufferD0Ev
	.quad	_ZN4node7tracing15NodeTraceBuffer13AddTraceEventEPm
	.quad	_ZN4node7tracing15NodeTraceBuffer16GetEventByHandleEm
	.quad	_ZN4node7tracing15NodeTraceBuffer5FlushEv
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC3:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC4:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC6:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"../src/tracing/node_trace_buffer.cc:112"
	.section	.rodata.str1.1
.LC8:
	.string	"(err) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"node::tracing::NodeTraceBuffer::NodeTraceBuffer(size_t, node::tracing::Agent*, uv_loop_t*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args_0, @object
	.size	_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args_0, 24
_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args_0:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"../src/tracing/node_trace_buffer.cc:108"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args, @object
	.size	_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args, 24
_ZZN4node7tracing15NodeTraceBufferC4EmPNS0_5AgentEP9uv_loop_sE4args:
	.quad	.LC10
	.quad	.LC8
	.quad	.LC9
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
