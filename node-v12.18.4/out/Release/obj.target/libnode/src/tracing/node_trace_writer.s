	.file	"node_trace_writer.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_s
	.type	_ZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_s, @function
_ZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_s:
.LFB5732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 8(%rdi)
	jne	.L7
	movq	%rsi, %xmm0
	movq	%rbx, %xmm1
	movq	%rsi, %rdi
	punpcklqdq	%xmm1, %xmm0
	leaq	16(%rbx), %rsi
	leaq	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sENUlP10uv_async_sE_4_FUNES5_(%rip), %rdx
	movups	%xmm0, 8(%rbx)
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L8
	movq	%rbx, 144(%rbx)
	movq	8(%rbx), %rdi
	leaq	144(%rbx), %rsi
	leaq	_ZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_s(%rip), %rdx
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L9
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	leaq	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5732:
	.size	_ZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_s, .-_ZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_s
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter5FlushEb
	.type	_ZN4node7tracing15NodeTraceWriter5FlushEb, @function
_ZN4node7tracing15NodeTraceWriter5FlushEb:
.LFB5750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	312(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	leaq	272(%rbx), %r12
	call	uv_mutex_lock@PLT
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	cmpq	$0, 1400(%rbx)
	movq	%r12, %rdi
	je	.L17
	call	uv_mutex_unlock@PLT
	movl	976(%rbx), %eax
	leaq	16(%rbx), %rdi
	leal	1(%rax), %r12d
	movl	%r12d, 976(%rbx)
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L18
	testb	%r14b, %r14b
	je	.L14
	cmpl	980(%rbx), %r12d
	jle	.L14
	leaq	352(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	uv_cond_wait@PLT
	cmpl	980(%rbx), %r12d
	jg	.L15
.L14:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	call	uv_mutex_unlock@PLT
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	leaq	_ZZN4node7tracing15NodeTraceWriter5FlushEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5750:
	.size	_ZN4node7tracing15NodeTraceWriter5FlushEb, .-_ZN4node7tracing15NodeTraceWriter5FlushEb
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_s
	.type	_ZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_s, @function
_ZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_s:
.LFB5761:
	.cfi_startproc
	endbr64
	addq	$-128, %rdi
	leaq	_ZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_(%rip), %rsi
	jmp	uv_close@PLT
	.cfi_endproc
.LFE5761:
	.size	_ZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_s, .-_ZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_s
	.p2align 4
	.type	_ZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_, @function
_ZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_:
.LFB5766:
	.cfi_startproc
	endbr64
	subq	$-128, %rdi
	leaq	_ZZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_(%rip), %rsi
	jmp	uv_close@PLT
	.cfi_endproc
.LFE5766:
	.size	_ZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_, .-_ZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENUlP11uv_handle_sE_4_FUNES5_
	.p2align 4
	.type	_ZZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_, @function
_ZZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_:
.LFB5764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	168(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movb	$1, 1264(%rbx)
	leaq	256(%rbx), %rdi
	call	uv_cond_signal@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE5764:
	.size	_ZZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_, .-_ZZZN4node7tracing15NodeTraceWriter12ExitSignalCbEP10uv_async_sENKUlP11uv_handle_sE_clES5_ENUlS5_E_4_FUNES5_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.text
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB6985:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L24
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L24:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L26
.L45:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L45
.L26:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L28
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L29:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L30
	testb	$4, %al
	jne	.L47
	testl	%eax, %eax
	je	.L31
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L48
.L31:
	movq	(%r12), %rcx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L31
	andl	$-8, %eax
	xorl	%edx, %edx
.L34:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L34
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L47:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L31
.L48:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L31
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6985:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriterD2Ev
	.type	_ZN4node7tracing15NodeTraceWriterD2Ev, @function
_ZN4node7tracing15NodeTraceWriterD2Ev:
.LFB5739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$536, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node7tracing15NodeTraceWriterE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	272(%rdi), %rax
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	movq	%rax, %r15
	call	uv_mutex_lock@PLT
	movl	984(%rbx), %eax
	testl	%eax, %eax
	jle	.L106
	movq	-520(%rbp), %r15
	leaq	312(%rbx), %r13
	movl	$524288, 984(%rbx)
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	cmpq	$0, 1400(%rbx)
	movq	-520(%rbp), %rdi
	je	.L107
	call	uv_mutex_unlock@PLT
	movl	976(%rbx), %eax
	leaq	16(%rbx), %rdi
	leal	1(%rax), %r12d
	movl	%r12d, 976(%rbx)
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L108
	leaq	352(%rbx), %r14
	cmpl	980(%rbx), %r12d
	jle	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	uv_cond_wait@PLT
	cmpl	980(%rbx), %r12d
	jg	.L56
.L55:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L51:
	movl	448(%rbx), %edx
	cmpl	$-1, %edx
	je	.L57
	leaq	-496(%rbp), %r12
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r12, %rsi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	jne	.L109
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
.L57:
	leaq	144(%rbx), %rdi
	leaq	312(%rbx), %r13
	call	uv_async_send@PLT
	movq	%r13, %rdi
	leaq	400(%rbx), %r15
	call	uv_mutex_lock@PLT
	cmpb	$0, 1408(%rbx)
	jne	.L62
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	uv_cond_wait@PLT
	cmpb	$0, 1408(%rbx)
	je	.L59
.L62:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	1400(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	1104(%rbx), %rdi
	movq	%rax, 1136(%rbx)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	leaq	1120(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 1024(%rbx)
	cmpq	%rax, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	1088(%rbx), %rdi
	movq	%rax, 1032(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	1136(%rbx), %rdi
	movq	%rax, 1024(%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, 1024(%rbx,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 1136(%rbx)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	992(%rbx), %rdi
	leaq	1008(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	944(%rbx), %rax
	movq	936(%rbx), %rcx
	movq	928(%rbx), %rdx
	movq	952(%rbx), %r12
	movq	%rax, -504(%rbp)
	movq	968(%rbx), %rax
	movq	%rcx, -536(%rbp)
	addq	$8, %rcx
	movq	912(%rbx), %r14
	movq	%rax, -528(%rbp)
	movq	%rdx, -512(%rbp)
	cmpq	%rcx, %rax
	jbe	.L72
	movq	%r13, -544(%rbp)
	movq	%r14, -560(%rbp)
	movq	%rbx, -568(%rbp)
	movq	-528(%rbp), %rbx
	movq	%r12, -552(%rbp)
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%r12), %r14
	leaq	480(%r14), %r13
	.p2align 4,,10
	.p2align 3
.L71:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L68
	call	_ZdlPv@PLT
	addq	$40, %r14
	cmpq	%r14, %r13
	jne	.L71
	addq	$8, %r12
	cmpq	%r12, %rbx
	ja	.L65
.L111:
	movq	-544(%rbp), %r13
	movq	-552(%rbp), %r12
	movq	-560(%rbp), %r14
	movq	-568(%rbp), %rbx
.L72:
	movq	-536(%rbp), %rdx
	cmpq	%rdx, -528(%rbp)
	je	.L105
	cmpq	-512(%rbp), %r14
	je	.L78
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L77
	call	_ZdlPv@PLT
	addq	$40, %r14
	cmpq	%r14, -512(%rbp)
	jne	.L73
.L78:
	cmpq	%r12, -504(%rbp)
	je	.L81
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L80
	call	_ZdlPv@PLT
	addq	$40, %r12
	cmpq	%r12, -504(%rbp)
	jne	.L74
.L81:
	movq	896(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L76
	movq	968(%rbx), %rax
	movq	936(%rbx), %r14
	leaq	8(%rax), %r12
	cmpq	%r14, %r12
	jbe	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	_ZdlPv@PLT
	cmpq	%r14, %r12
	ja	.L87
	movq	896(%rbx), %rdi
.L86:
	call	_ZdlPv@PLT
.L76:
	movq	%r15, %rdi
	call	uv_cond_destroy@PLT
	leaq	352(%rbx), %rdi
	call	uv_cond_destroy@PLT
	movq	%r13, %rdi
	call	uv_mutex_destroy@PLT
	movq	-520(%rbp), %rdi
	call	uv_mutex_destroy@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	addq	$40, %r14
.L105:
	cmpq	%r14, -504(%rbp)
	je	.L81
.L85:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L83
	call	_ZdlPv@PLT
	addq	$40, %r14
	cmpq	-504(%rbp), %r14
	jne	.L85
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L68:
	addq	$40, %r14
	cmpq	%r14, %r13
	jne	.L71
	addq	$8, %r12
	cmpq	%r12, %rbx
	ja	.L65
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$40, %r14
	cmpq	%r14, -512(%rbp)
	jne	.L73
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$40, %r12
	cmpq	%r12, -504(%rbp)
	jne	.L74
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L107:
	call	uv_mutex_unlock@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L51
.L109:
	leaq	_ZZN4node7tracing15NodeTraceWriterD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L108:
	leaq	_ZZN4node7tracing15NodeTraceWriter5FlushEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5739:
	.size	_ZN4node7tracing15NodeTraceWriterD2Ev, .-_ZN4node7tracing15NodeTraceWriterD2Ev
	.globl	_ZN4node7tracing15NodeTraceWriterD1Ev
	.set	_ZN4node7tracing15NodeTraceWriterD1Ev,_ZN4node7tracing15NodeTraceWriterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriterD0Ev
	.type	_ZN4node7tracing15NodeTraceWriterD0Ev, @function
_ZN4node7tracing15NodeTraceWriterD0Ev:
.LFB5741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node7tracing15NodeTraceWriterD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1416, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5741:
	.size	_ZN4node7tracing15NodeTraceWriterD0Ev, .-_ZN4node7tracing15NodeTraceWriterD0Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriterC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7tracing15NodeTraceWriterC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7tracing15NodeTraceWriterC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$272, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node7tracing15NodeTraceWriterE(%rip), %rax
	movq	$0, -264(%rdi)
	movq	%rax, -272(%rdi)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L116
	leaq	312(%rbx), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L116
	leaq	352(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L118
	leaq	400(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L118
	movl	$-1, 448(%rbx)
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, 896(%rbx)
	movq	$8, 904(%rbx)
	movups	%xmm0, 912(%rbx)
	movups	%xmm0, 928(%rbx)
	movups	%xmm0, 944(%rbx)
	movups	%xmm0, 960(%rbx)
	call	_Znwm@PLT
	movq	904(%rbx), %rdx
	movl	$480, %edi
	movq	%rax, 896(%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	leaq	1008(%rbx), %rdi
	movq	%r12, 936(%rbx)
	movq	%rax, (%r12)
	movq	%rax, %xmm0
	leaq	480(%rax), %rdx
	movq	%rdi, 992(%rbx)
	movq	0(%r13), %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 968(%rbx)
	movq	8(%r13), %r12
	movq	%rax, 952(%rbx)
	movq	%rax, 944(%rbx)
	movq	%r14, %rax
	movups	%xmm0, 912(%rbx)
	addq	%r12, %rax
	pxor	%xmm0, %xmm0
	movq	%rdx, 928(%rbx)
	movq	%rdx, 960(%rbx)
	movups	%xmm0, 976(%rbx)
	je	.L119
	testq	%r14, %r14
	je	.L135
.L119:
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L136
	cmpq	$1, %r12
	jne	.L122
	movzbl	(%r14), %eax
	movb	%al, 1008(%rbx)
.L123:
	movq	%r12, 1000(%rbx)
	leaq	1032(%rbx), %r13
	movb	$0, (%rdi,%r12)
	leaq	1136(%rbx), %r12
	movq	%r12, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, 1136(%rbx)
	xorl	%eax, %eax
	movw	%ax, 1360(%rbx)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, 1368(%rbx)
	movups	%xmm0, 1384(%rbx)
	movq	%rax, 1024(%rbx)
	movq	-24(%rax), %rax
	movq	$0, 1352(%rbx)
	leaq	1024(%rbx,%rax), %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 1136(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm1
	leaq	1088(%rbx), %rdi
	movq	%rax, %xmm2
	movups	%xmm0, 1040(%rbx)
	punpcklqdq	%xmm2, %xmm1
	movups	%xmm0, 1056(%rbx)
	movups	%xmm1, 1024(%rbx)
	movups	%xmm0, 1072(%rbx)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, 1032(%rbx)
	leaq	1120(%rbx), %rax
	movq	%rax, 1104(%rbx)
	movl	$16, 1096(%rbx)
	movq	$0, 1112(%rbx)
	movb	$0, 1120(%rbx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movb	$0, 1408(%rbx)
	movq	$0, 1400(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	leaq	992(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 992(%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 1008(%rbx)
.L121:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	992(%rbx), %rdi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L122:
	testq	%r12, %r12
	je	.L123
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L116:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L135:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5730:
	.size	_ZN4node7tracing15NodeTraceWriterC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7tracing15NodeTraceWriterC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.globl	_ZN4node7tracing15NodeTraceWriterC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.set	_ZN4node7tracing15NodeTraceWriterC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,_ZN4node7tracing15NodeTraceWriterC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter11WriteSuffixEv
	.type	_ZN4node7tracing15NodeTraceWriter11WriteSuffixEv, @function
_ZN4node7tracing15NodeTraceWriter11WriteSuffixEv:
.LFB5737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	272(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	call	uv_mutex_lock@PLT
	movl	984(%r12), %eax
	movq	%r13, %rdi
	testl	%eax, %eax
	jle	.L148
	movl	$524288, 984(%r12)
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	leaq	_ZN4node7tracing15NodeTraceWriter5FlushEb(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L140
	leaq	312(%r12), %r14
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	%r13, %rdi
	cmpq	$0, 1400(%r12)
	je	.L150
	call	uv_mutex_unlock@PLT
	movl	976(%r12), %eax
	leaq	16(%r12), %rdi
	leal	1(%rax), %r13d
	movl	%r13d, 976(%r12)
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L151
	leaq	352(%r12), %rbx
	cmpl	980(%r12), %r13d
	jle	.L149
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	uv_cond_wait@PLT
	cmpl	980(%r12), %r13d
	jg	.L145
.L149:
	movq	%r14, %rdi
.L148:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	call	uv_mutex_unlock@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	_ZZN4node7tracing15NodeTraceWriter5FlushEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5737:
	.size	_ZN4node7tracing15NodeTraceWriter11WriteSuffixEv, .-_ZN4node7tracing15NodeTraceWriter11WriteSuffixEv
	.section	.rodata.str1.1
.LC2:
	.string	"basic_string::replace"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.text
	.p2align 4
	.globl	_ZN4node7tracing17replace_substringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKS6_S9_
	.type	_ZN4node7tracing17replace_substringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKS6_S9_, @function
_ZN4node7tracing17replace_substringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKS6_S9_:
.LFB5742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	8(%rsi), %rcx
	movq	(%rsi), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	cmpq	$-1, %rax
	je	.L152
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L155:
	movq	8(%r14), %rcx
	movq	8(%r13), %r8
	movq	0(%r13), %r9
	movq	%rcx, %rdx
	subq	%r12, %rdx
	cmpq	%rdx, 8(%rbx)
	cmovbe	8(%rbx), %rdx
	cmpq	%r12, %rcx
	jb	.L161
	movq	%r9, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r13), %rdx
	movq	8(%rbx), %rcx
	movq	%r14, %rdi
	movq	(%rbx), %rsi
	addq	%r12, %rdx
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	movq	%rax, %r12
	cmpq	$-1, %rax
	jne	.L155
.L152:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE5742:
	.size	_ZN4node7tracing17replace_substringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKS6_S9_, .-_ZN4node7tracing17replace_substringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKS6_S9_
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Could not open trace file %s: %s\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEv
	.type	_ZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEv, @function
_ZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEv:
.LFB5743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$552, %rsp
	movq	992(%rdi), %r13
	movq	1000(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-544(%rbp), %rax
	addl	$1, 988(%rdi)
	movq	%rax, -584(%rbp)
	movq	%rax, -560(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L163
	testq	%r13, %r13
	je	.L188
.L163:
	movq	%r12, -568(%rbp)
	cmpq	$15, %r12
	ja	.L189
	cmpq	$1, %r12
	jne	.L166
	movzbl	0(%r13), %eax
	leaq	-560(%rbp), %r15
	movb	%al, -544(%rbp)
	movq	-584(%rbp), %rax
.L167:
	movq	%r12, -552(%rbp)
	leaq	-528(%rbp), %r14
	leaq	-480(%rbp), %r13
	movb	$0, (%rax,%r12)
	leaq	-496(%rbp), %r12
	call	uv_os_getpid@PLT
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movl	$16, %edx
	movq	%r14, %rdi
	movl	%eax, %r8d
	leaq	.LC0(%rip), %rcx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movl	$32100, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movw	%dx, -476(%rbp)
	movq	%r14, %rdx
	movq	%r13, -496(%rbp)
	movl	$1768979236, -480(%rbp)
	movq	$6, -488(%rbp)
	movb	$0, -474(%rbp)
	call	_ZN4node7tracing17replace_substringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKS6_S9_
	movq	-496(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	movq	%rax, -592(%rbp)
	cmpq	%rax, %rdi
	je	.L169
	call	_ZdlPv@PLT
.L169:
	movl	988(%rbx), %r8d
	movl	$16, %edx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movabsq	$7598805623994743588, %rax
	movq	%r13, -496(%rbp)
	movq	%rax, -480(%rbp)
	movl	$28271, %eax
	movw	%ax, -472(%rbp)
	movb	$125, -470(%rbp)
	movq	$11, -488(%rbp)
	movb	$0, -469(%rbp)
	call	_ZN4node7tracing17replace_substringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKS6_S9_
	movq	-496(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	movq	-528(%rbp), %rdi
	cmpq	-592(%rbp), %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movl	448(%rbx), %edx
	cmpl	$-1, %edx
	je	.L172
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%r12, %rsi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	jne	.L190
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
.L172:
	movq	-560(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	$420, %r8d
	movl	$577, %ecx
	call	uv_fs_open@PLT
	movq	%r12, %rdi
	movl	%eax, 448(%rbx)
	call	uv_fs_req_cleanup@PLT
	movl	448(%rbx), %edi
	testl	%edi, %edi
	js	.L191
.L174:
	movq	-560(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L193
	movq	-584(%rbp), %rax
	leaq	-560(%rbp), %r15
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	-560(%rbp), %r15
	leaq	-568(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -560(%rbp)
	movq	%rax, %rdi
	movq	-568(%rbp), %rax
	movq	%rax, -544(%rbp)
.L165:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-568(%rbp), %r12
	movq	-560(%rbp), %rax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L191:
	call	uv_strerror@PLT
	movq	-560(%rbp), %rcx
	movl	$1, %esi
	movq	stderr(%rip), %rdi
	movq	%rax, %r8
	leaq	.LC4(%rip), %rdx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movl	$-1, 448(%rbx)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	_ZZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L188:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L192:
	call	__stack_chk_fail@PLT
.L193:
	movq	-584(%rbp), %rdi
	leaq	-560(%rbp), %r15
	jmp	.L165
	.cfi_endproc
.LFE5743:
	.size	_ZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEv, .-_ZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEv
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE
	.type	_ZN4node7tracing15NodeTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE, @function
_ZN4node7tracing15NodeTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE:
.LFB5744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	272(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movl	984(%rbx), %edx
	testl	%edx, %edx
	je	.L195
.L199:
	movq	1400(%rbx), %rdi
.L196:
	movq	(%rdi), %rax
	addl	$1, %edx
	movq	%r13, %rsi
	movl	%edx, 984(%rbx)
	call	*16(%rax)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEv
	leaq	1024(%rbx), %rdi
	call	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSo@PLT
	movq	1400(%rbx), %r8
	movq	%rax, %rdi
	movq	%rax, 1400(%rbx)
	testq	%r8, %r8
	je	.L200
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	movl	984(%rbx), %edx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L200:
	movl	984(%rbx), %edx
	jmp	.L196
	.cfi_endproc
.LFE5744:
	.size	_ZN4node7tracing15NodeTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE, .-_ZN4node7tracing15NodeTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_t
	.type	_ZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_t, @function
_ZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_t:
.LFB5755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_(%rip), %rax
	movq	$-1, %r9
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	movq	%rsi, -16(%rbp)
	leaq	456(%rdi), %rsi
	leaq	-16(%rbp), %rcx
	movq	%rdx, -8(%rbp)
	movl	448(%rdi), %edx
	movq	8(%rdi), %rdi
	pushq	%rax
	call	uv_fs_write@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L204
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5755:
	.size	_ZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_t, .-_ZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_t
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter10AfterWriteEv
	.type	_ZN4node7tracing15NodeTraceWriter10AfterWriteEv, @function
_ZN4node7tracing15NodeTraceWriter10AfterWriteEv:
.LFB5760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 544(%rdi)
	js	.L225
	leaq	456(%rdi), %r15
	movq	%rdi, %rbx
	movq	%r15, %rdi
	leaq	312(%rbx), %r14
	call	uv_fs_req_cleanup@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rdx, %r13
	call	uv_mutex_lock@PLT
	movq	928(%rbx), %rdx
	movq	912(%rbx), %rax
	leaq	-40(%rdx), %rsi
	movl	32(%rax), %ecx
	movq	(%rax), %rdi
	cmpq	%rsi, %rax
	je	.L207
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rdi
	je	.L208
	movl	%ecx, -84(%rbp)
	call	_ZdlPv@PLT
	movq	912(%rbx), %rax
	movl	-84(%rbp), %ecx
.L208:
	addq	$40, %rax
	movq	%rax, 912(%rbx)
.L209:
	movl	%ecx, 980(%rbx)
	leaq	352(%rbx), %rdi
	call	uv_cond_broadcast@PLT
	movq	912(%rbx), %rax
	cmpq	944(%rbx), %rax
	je	.L211
	movl	8(%rax), %esi
	movq	(%rax), %rdi
	call	uv_buf_init@PLT
	movq	%rax, %r12
	movq	%rdx, %r13
.L211:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testq	%r12, %r12
	je	.L212
	movl	448(%rbx), %edx
	cmpl	$-1, %edx
	jne	.L226
.L212:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	subq	$8, %rsp
	movq	8(%rbx), %rdi
	leaq	-80(%rbp), %rcx
	movq	%r15, %rsi
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_(%rip), %rax
	movq	$-1, %r9
	movl	$1, %r8d
	movq	%r12, -80(%rbp)
	pushq	%rax
	movq	%r13, -72(%rbp)
	call	uv_fs_write@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	je	.L212
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	subq	$24, %rdx
	cmpq	%rdx, %rdi
	je	.L210
	movl	%ecx, -84(%rbp)
	call	_ZdlPv@PLT
	movl	-84(%rbp), %ecx
.L210:
	movq	920(%rbx), %rdi
	movl	%ecx, -84(%rbp)
	call	_ZdlPv@PLT
	movq	936(%rbx), %rdx
	movl	-84(%rbp), %ecx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$480, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 912(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 928(%rbx)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	_ZZN4node7tracing15NodeTraceWriter10AfterWriteEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5760:
	.size	_ZN4node7tracing15NodeTraceWriter10AfterWriteEv, .-_ZN4node7tracing15NodeTraceWriter10AfterWriteEv
	.p2align 4
	.type	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_, @function
_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_:
.LFB5758:
	.cfi_startproc
	endbr64
	subq	$456, %rdi
	jmp	_ZN4node7tracing15NodeTraceWriter10AfterWriteEv
	.cfi_endproc
.LFE5758:
	.size	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_, .-_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_
	.section	.rodata._ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_
	.type	_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_, @function
_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_:
.LFB6670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rdi), %rdx
	subq	56(%rdi), %rdx
	movabsq	$-3689348814741910323, %rdi
	movq	%r14, %r13
	sarq	$3, %rdx
	subq	%rsi, %r13
	imulq	%rdi, %rdx
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-3(%rcx,%rcx,2), %rax
	leaq	(%rdx,%rax,4), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	addq	%rdx, %rax
	movabsq	$230584300921369395, %rdx
	cmpq	%rdx, %rax
	je	.L240
	movq	(%rbx), %rdi
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%rdi, %rax
	movq	%rdx, %r8
	sarq	$3, %rax
	subq	%rax, %r8
	cmpq	$1, %r8
	jbe	.L241
.L231:
	movl	$480, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	movq	(%r12), %rcx
	leaq	16(%r12), %rdx
	cmpq	%rdx, %rcx
	je	.L242
	movq	%rcx, (%rax)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
.L238:
	movq	8(%r12), %rcx
	movq	%rdx, (%r12)
	movq	$0, 8(%r12)
	movl	32(%r12), %edx
	movq	%rcx, 8(%rax)
	movl	%edx, 32(%rax)
	movb	$0, 16(%r12)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$480, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L243
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L244
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L236
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L236:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L234:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$480, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$480, %rax
	movq	%rax, 64(%rbx)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L242:
	movdqu	16(%r12), %xmm3
	movups	%xmm3, 16(%rax)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L243:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%rdi,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L233
	cmpq	%r14, %rsi
	je	.L234
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L233:
	cmpq	%r14, %rsi
	je	.L234
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L234
.L240:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L244:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6670:
	.size	_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_, .-_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter12FlushPrivateEv
	.type	_ZN4node7tracing15NodeTraceWriter12FlushPrivateEv, @function
_ZN4node7tracing15NodeTraceWriter12FlushPrivateEv:
.LFB5745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	272(%rdi), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	call	uv_mutex_lock@PLT
	cmpl	$524287, 984(%r14)
	jle	.L247
	movq	1400(%r14), %rdi
	movl	$0, 984(%r14)
	movq	$0, 1400(%r14)
	testq	%rdi, %rdi
	je	.L247
	movq	(%rdi), %rax
	call	*8(%rax)
.L247:
	movq	1072(%r14), %rax
	leaq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rbx
	movq	$0, -88(%rbp)
	movq	%rdi, -152(%rbp)
	leaq	1104(%r14), %r15
	movq	%rbx, -96(%rbp)
	movb	$0, -80(%rbp)
	testq	%rax, %rax
	je	.L249
	movq	1056(%r14), %r8
	movq	1064(%r14), %rcx
	cmpq	%r8, %rax
	ja	.L296
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L251:
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L297
	movq	-80(%rbp), %rcx
	cmpq	%r12, %rdi
	je	.L298
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	-112(%rbp), %rsi
	movq	%rax, -128(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -120(%rbp)
	testq	%rdi, %rdi
	je	.L257
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L255:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L258
	call	_ZdlPv@PLT
.L258:
	movq	1112(%r14), %rdx
	movq	%rbx, %rcx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%ecx, %ecx
	testb	$3, 1096(%r14)
	je	.L259
	movq	1112(%r14), %rcx
.L259:
	movq	1104(%r14), %rsi
	leaq	1032(%r14), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	xorl	%esi, %esi
	leaq	1136(%r14), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	movq	%r13, %rdi
	leaq	312(%r14), %r13
	call	uv_mutex_unlock@PLT
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movl	976(%r14), %eax
	movq	%r13, %rdi
	movl	%eax, -156(%rbp)
	call	uv_mutex_unlock@PLT
	cmpl	$-1, 448(%r14)
	je	.L261
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	movq	%rdx, -168(%rbp)
	call	uv_mutex_lock@PLT
	movq	-128(%rbp), %rax
	movq	%rbx, -96(%rbp)
	cmpq	%r12, %rax
	je	.L299
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -80(%rbp)
.L263:
	movq	-120(%rbp), %rax
	movq	960(%r14), %rsi
	movq	%r12, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -88(%rbp)
	movl	-156(%rbp), %eax
	leaq	-40(%rsi), %rdx
	movb	$0, -112(%rbp)
	movl	%eax, -64(%rbp)
	movq	944(%r14), %rax
	movq	%rax, %rcx
	cmpq	%rdx, %rax
	je	.L264
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdx
	cmpq	%rbx, %rdx
	je	.L300
	movq	%rdx, (%rax)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rax)
.L266:
	movq	-88(%rbp), %rdx
	addq	$40, %rax
	movq	%rdx, 8(%rcx)
	movl	-64(%rbp), %edx
	movl	%edx, 32(%rcx)
	movq	%rax, 944(%r14)
.L267:
	subq	952(%r14), %rax
	movq	968(%r14), %rcx
	movabsq	$-3689348814741910323, %rsi
	sarq	$3, %rax
	subq	936(%r14), %rcx
	movq	912(%r14), %rdx
	imulq	%rsi, %rax
	sarq	$3, %rcx
	leaq	-3(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,4), %rcx
	movq	928(%r14), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	addq	%rcx, %rax
	cmpq	$1, %rax
	je	.L301
.L269:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	testq	%r15, %r15
	je	.L261
	movl	448(%r14), %edx
	cmpl	$-1, %edx
	je	.L261
	movq	-168(%rbp), %rax
	subq	$8, %rsp
	movq	8(%r14), %rdi
	leaq	-144(%rbp), %rcx
	leaq	456(%r14), %rsi
	movq	$-1, %r9
	movl	$1, %r8d
	movq	%r15, -144(%rbp)
	movq	%rax, -136(%rbp)
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_(%rip), %rax
	pushq	%rax
	call	uv_fs_write@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L302
.L261:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-152(%rbp), %rsi
	leaq	896(%r14), %rdi
	call	_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	944(%r14), %rax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L297:
	testq	%rdx, %rdx
	je	.L253
	cmpq	$1, %rdx
	je	.L304
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rdi
.L253:
	movq	%rdx, -120(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	%rax, -128(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -120(%rbp)
.L257:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L299:
	movdqa	-112(%rbp), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L300:
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 16(%rax)
	movq	944(%r14), %rax
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L301:
	movl	8(%rdx), %esi
	movq	(%rdx), %rdi
	call	uv_buf_init@PLT
	movq	%rdx, -168(%rbp)
	movq	%rax, %r15
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L304:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	-128(%rbp), %rdi
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L302:
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5745:
	.size	_ZN4node7tracing15NodeTraceWriter12FlushPrivateEv, .-_ZN4node7tracing15NodeTraceWriter12FlushPrivateEv
	.p2align 4
	.type	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sENUlP10uv_async_sE_4_FUNES5_, @function
_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sENUlP10uv_async_sE_4_FUNES5_:
.LFB5735:
	.cfi_startproc
	endbr64
	subq	$16, %rdi
	jmp	_ZN4node7tracing15NodeTraceWriter12FlushPrivateEv
	.cfi_endproc
.LFE5735:
	.size	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sENUlP10uv_async_sE_4_FUNES5_, .-_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sENUlP10uv_async_sE_4_FUNES5_
	.align 2
	.p2align 4
	.globl	_ZN4node7tracing15NodeTraceWriter11WriteToFileEONSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.type	_ZN4node7tracing15NodeTraceWriter11WriteToFileEONSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, @function
_ZN4node7tracing15NodeTraceWriter11WriteToFileEONSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi:
.LFB5751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, 448(%rdi)
	je	.L306
	movq	%rdi, %rbx
	movq	%rsi, %r15
	xorl	%edi, %edi
	xorl	%esi, %esi
	leaq	312(%rbx), %r14
	movl	%edx, %r13d
	call	uv_buf_init@PLT
	movq	%r14, %rdi
	movq	%rdx, -120(%rbp)
	movq	%rax, %r12
	call	uv_mutex_lock@PLT
	movq	(%r15), %rsi
	leaq	-80(%rbp), %rdx
	leaq	16(%r15), %rax
	movq	%rdx, -96(%rbp)
	cmpq	%rax, %rsi
	je	.L330
	movq	%rsi, -96(%rbp)
	movq	16(%r15), %rsi
	movq	%rsi, -80(%rbp)
.L309:
	movb	$0, 16(%r15)
	movq	8(%r15), %rsi
	movq	%rax, (%r15)
	movq	960(%rbx), %rax
	movq	944(%rbx), %rcx
	movq	%rsi, -88(%rbp)
	subq	$40, %rax
	movq	$0, 8(%r15)
	movl	%r13d, -64(%rbp)
	movq	%rcx, %rsi
	cmpq	%rax, %rcx
	je	.L310
	leaq	16(%rcx), %rax
	movq	%rax, (%rcx)
	movq	-96(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L331
	movq	%rax, (%rcx)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rcx)
.L312:
	movq	-88(%rbp), %rax
	addq	$40, %rcx
	movq	%rax, 8(%rsi)
	movl	-64(%rbp), %eax
	movl	%eax, 32(%rsi)
	movq	%rcx, 944(%rbx)
.L313:
	subq	952(%rbx), %rcx
	movq	968(%rbx), %rax
	movabsq	$-3689348814741910323, %rsi
	sarq	$3, %rcx
	subq	936(%rbx), %rax
	movq	912(%rbx), %rdx
	imulq	%rsi, %rcx
	sarq	$3, %rax
	leaq	-3(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,4), %rcx
	movq	928(%rbx), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	addq	%rcx, %rax
	cmpq	$1, %rax
	je	.L332
.L315:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testq	%r12, %r12
	je	.L306
	movl	448(%rbx), %edx
	cmpl	$-1, %edx
	je	.L306
	movq	-120(%rbp), %rax
	subq	$8, %rsp
	movq	8(%rbx), %rdi
	leaq	-112(%rbp), %rcx
	leaq	456(%rbx), %rsi
	movq	$-1, %r9
	movl	$1, %r8d
	movq	%r12, -112(%rbp)
	movq	%rax, -104(%rbp)
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tENUlP7uv_fs_sE_4_FUNES4_(%rip), %rax
	pushq	%rax
	call	uv_fs_write@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L333
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	leaq	896(%rbx), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rdx, -128(%rbp)
	call	_ZNSt5dequeIN4node7tracing15NodeTraceWriter12WriteRequestESaIS3_EE16_M_push_back_auxIJS3_EEEvDpOT_
	movq	-96(%rbp), %rdi
	movq	-128(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L329
	call	_ZdlPv@PLT
.L329:
	movq	944(%rbx), %rcx
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L330:
	movdqu	16(%r15), %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L331:
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 16(%rcx)
	movq	944(%rbx), %rcx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L332:
	movl	8(%rdx), %esi
	movq	(%rdx), %rdi
	call	uv_buf_init@PLT
	movq	%rdx, -120(%rbp)
	movq	%rax, %r12
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5751:
	.size	_ZN4node7tracing15NodeTraceWriter11WriteToFileEONSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, .-_ZN4node7tracing15NodeTraceWriter11WriteToFileEONSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.weak	_ZTVN4node7tracing15NodeTraceWriterE
	.section	.data.rel.ro.local._ZTVN4node7tracing15NodeTraceWriterE,"awG",@progbits,_ZTVN4node7tracing15NodeTraceWriterE,comdat
	.align 8
	.type	_ZTVN4node7tracing15NodeTraceWriterE, @object
	.size	_ZTVN4node7tracing15NodeTraceWriterE, 56
_ZTVN4node7tracing15NodeTraceWriterE:
	.quad	0
	.quad	0
	.quad	_ZN4node7tracing15NodeTraceWriterD1Ev
	.quad	_ZN4node7tracing15NodeTraceWriterD0Ev
	.quad	_ZN4node7tracing15NodeTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE
	.quad	_ZN4node7tracing15NodeTraceWriter5FlushEb
	.quad	_ZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_s
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC6:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC8:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC9:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC11:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"../src/tracing/node_trace_writer.cc:196"
	.section	.rodata.str1.1
.LC13:
	.string	"(write_req_.result) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"void node::tracing::NodeTraceWriter::AfterWrite()"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriter10AfterWriteEvE4args, @object
	.size	_ZZN4node7tracing15NodeTraceWriter10AfterWriteEvE4args, 24
_ZZN4node7tracing15NodeTraceWriter10AfterWriteEvE4args:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"../src/tracing/node_trace_writer.cc:192"
	.section	.rodata.str1.1
.LC16:
	.string	"(err) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"void node::tracing::NodeTraceWriter::StartWrite(uv_buf_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tE4args, @object
	.size	_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tE4args, 24
_ZZN4node7tracing15NodeTraceWriter10StartWriteE8uv_buf_tE4args:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"../src/tracing/node_trace_writer.cc:151"
	.align 8
.LC19:
	.string	"virtual void node::tracing::NodeTraceWriter::Flush(bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriter5FlushEbE4args, @object
	.size	_ZZN4node7tracing15NodeTraceWriter5FlushEbE4args, 24
_ZZN4node7tracing15NodeTraceWriter5FlushEbE4args:
	.quad	.LC18
	.quad	.LC16
	.quad	.LC19
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"../src/tracing/node_trace_writer.cc:84"
	.align 8
.LC21:
	.string	"(uv_fs_close(nullptr, &req, fd_, nullptr)) == (0)"
	.align 8
.LC22:
	.string	"void node::tracing::NodeTraceWriter::OpenNewFileForStreaming()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEvE4args, @object
	.size	_ZZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEvE4args, 24
_ZZN4node7tracing15NodeTraceWriter23OpenNewFileForStreamingEvE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"../src/tracing/node_trace_writer.cc:53"
	.align 8
.LC24:
	.string	"(0) == (uv_fs_close(nullptr, &req, fd_, nullptr))"
	.align 8
.LC25:
	.string	"virtual node::tracing::NodeTraceWriter::~NodeTraceWriter()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriterD4EvE4args, @object
	.size	_ZZN4node7tracing15NodeTraceWriterD4EvE4args, 24
_ZZN4node7tracing15NodeTraceWriterD4EvE4args:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"../src/tracing/node_trace_writer.cc:29"
	.align 8
.LC27:
	.string	"virtual void node::tracing::NodeTraceWriter::InitializeOnThread(uv_loop_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_1, @object
	.size	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_1, 24
_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_1:
	.quad	.LC26
	.quad	.LC16
	.quad	.LC27
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"../src/tracing/node_trace_writer.cc:25"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_0, @object
	.size	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_0, 24
_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args_0:
	.quad	.LC28
	.quad	.LC16
	.quad	.LC27
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"../src/tracing/node_trace_writer.cc:15"
	.section	.rodata.str1.1
.LC30:
	.string	"(tracing_loop_) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args, @object
	.size	_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args, 24
_ZZN4node7tracing15NodeTraceWriter18InitializeOnThreadEP9uv_loop_sE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC27
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
