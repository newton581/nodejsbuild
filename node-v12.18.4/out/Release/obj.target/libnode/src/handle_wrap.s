	.file	"handle_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB4624:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4624:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE, @function
_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE:
.LFB7528:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	testl	%eax, %eax
	je	.L22
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	_ZN4node10HandleWrap7OnCloseEP11uv_handle_s(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	80(%rdi), %rdi
	call	uv_close@PLT
	movl	$1, 72(%rbx)
	testq	%r12, %r12
	je	.L3
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L3
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3
	movzbl	11(%rdi), %eax
	movq	16(%rbx), %rdx
	andl	$7, %eax
	cmpb	$2, %al
	je	.L23
.L7:
	movq	360(%rdx), %rax
	movq	3280(%rdx), %rsi
	movq	%r12, %rcx
	movq	128(%rax), %r8
	movq	%r8, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L24
.L3:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L7
.L24:
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
.LFE7528:
	.size	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE, .-_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap11OnGCCollectEv
	.type	_ZN4node10HandleWrap11OnGCCollectEv, @function
_ZN4node10HandleWrap11OnGCCollectEv:
.LFB7529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L26
	movl	72(%rdi), %eax
	testl	%eax, %eax
	je	.L30
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	80(%rdi), %rdi
	leaq	_ZN4node10HandleWrap7OnCloseEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
	movl	$1, 72(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7529:
	.size	_ZN4node10HandleWrap11OnGCCollectEv, .-_ZN4node10HandleWrap11OnGCCollectEv
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap6HasRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10HandleWrap6HasRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10HandleWrap6HasRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L48
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L40
	cmpw	$1040, %cx
	jne	.L33
.L40:
	movq	23(%rdx), %r12
.L35:
	testq	%r12, %r12
	je	.L31
	movq	(%r12), %rax
	movq	(%rbx), %rbx
	movq	%r12, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L37
.L39:
	movl	$64, %edx
.L38:
	movq	8(%rbx), %rax
	movq	56(%rax,%rdx), %rax
	movq	%rax, 24(%rbx)
.L31:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	cmpl	$2, 72(%r12)
	je	.L39
	movq	80(%r12), %rdi
	call	uv_has_ref@PLT
	testl	%eax, %eax
	je	.L39
	movl	$56, %edx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7526:
	.size	_ZN4node10HandleWrap6HasRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10HandleWrap6HasRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap3RefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10HandleWrap3RefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10HandleWrap3RefERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L62
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L57
	cmpw	$1040, %cx
	jne	.L51
.L57:
	movq	23(%rdx), %rbx
.L53:
	testq	%rbx, %rbx
	je	.L49
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	testb	%al, %al
	je	.L49
	cmpl	$2, 72(%rbx)
	je	.L49
	movq	80(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_ref@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7524:
	.size	_ZN4node10HandleWrap3RefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10HandleWrap3RefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap5UnrefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10HandleWrap5UnrefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10HandleWrap5UnrefERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L76
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L71
	cmpw	$1040, %cx
	jne	.L65
.L71:
	movq	23(%rdx), %rbx
.L67:
	testq	%rbx, %rbx
	je	.L63
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	testb	%al, %al
	je	.L63
	cmpl	$2, 72(%rbx)
	je	.L63
	movq	80(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7525:
	.size	_ZN4node10HandleWrap5UnrefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10HandleWrap5UnrefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L105
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L91
	cmpw	$1040, %cx
	jne	.L79
.L91:
	movq	23(%rdx), %r13
.L81:
	testq	%r13, %r13
	je	.L77
	movq	0(%r13), %rax
	movl	16(%rbx), %edx
	movq	80(%rax), %rax
	testl	%edx, %edx
	jle	.L106
	leaq	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE(%rip), %rdx
	movq	8(%rbx), %r12
	cmpq	%rdx, %rax
	jne	.L85
.L108:
	movl	72(%r13), %eax
	testl	%eax, %eax
	jne	.L77
	movq	80(%r13), %rdi
	leaq	_ZN4node10HandleWrap7OnCloseEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
	movl	$1, 72(%r13)
	testq	%r12, %r12
	je	.L77
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L77
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L77
	movzbl	11(%rdi), %eax
	movq	16(%r13), %rdx
	andl	$7, %eax
	cmpb	$2, %al
	je	.L107
.L89:
	movq	360(%rdx), %rax
	movq	3280(%rdx), %rsi
	movq	%r12, %rcx
	movq	128(%rax), %r8
	movq	%r8, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L77
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %r12
	leaq	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE(%rip), %rdx
	addq	$88, %r12
	cmpq	%rdx, %rax
	je	.L108
.L85:
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L107:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r13), %rdx
	movq	%rax, %rdi
	jmp	.L89
	.cfi_endproc
.LFE7527:
	.size	_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L110:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L119
.L111:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L111
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap17MarkAsInitializedEv
	.type	_ZN4node10HandleWrap17MarkAsInitializedEv, @function
_ZN4node10HandleWrap17MarkAsInitializedEv:
.LFB7530:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	leaq	56(%rdi), %rcx
	movq	2096(%rdx), %rax
	leaq	2096(%rdx), %rsi
	movq	%rsi, %xmm1
	movq	%rax, %xmm0
	movq	%rcx, 8(%rax)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rdi)
	movq	%rcx, 2096(%rdx)
	movl	$0, 72(%rdi)
	ret
	.cfi_endproc
.LFE7530:
	.size	_ZN4node10HandleWrap17MarkAsInitializedEv, .-_ZN4node10HandleWrap17MarkAsInitializedEv
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap19MarkAsUninitializedEv
	.type	_ZN4node10HandleWrap19MarkAsUninitializedEv, @function
_ZN4node10HandleWrap19MarkAsUninitializedEv:
.LFB7531:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movq	56(%rdi), %rdx
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	leaq	56(%rdi), %rax
	movq	%rax, %xmm0
	movl	$2, 72(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 56(%rdi)
	ret
	.cfi_endproc
.LFE7531:
	.size	_ZN4node10HandleWrap19MarkAsUninitializedEv, .-_ZN4node10HandleWrap19MarkAsUninitializedEv
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE
	.type	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE, @function
_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE:
.LFB7533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	movl	%r8d, %ecx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	56(%rbx), %r14
	subq	$32, %rsp
	movsd	.LC0(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	movq	%r14, %xmm0
	movq	%r13, 80(%rbx)
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movups	%xmm0, 56(%rbx)
	movl	$0, 72(%rbx)
	movq	%rbx, 0(%r13)
	leaq	-64(%rbp), %r13
	movq	352(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpb	$0, 1928(%r12)
	je	.L126
	movq	2096(%r12), %rax
	leaq	2096(%r12), %rdx
	movq	%r13, %rdi
	movq	%rdx, %xmm1
	movq	%r14, 8(%rax)
	movq	%rax, %xmm0
	movq	%r14, 2096(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%rbx)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	leaq	_ZZN4node10HandleWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7533:
	.size	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE, .-_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE
	.globl	_ZN4node10HandleWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE
	.set	_ZN4node10HandleWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE,_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"HandleWrap"
.LC2:
	.string	"close"
.LC3:
	.string	"hasRef"
.LC4:
	.string	"ref"
.LC5:
	.string	"unref"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE
	.type	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE, @function
_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE:
.LFB7536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	3128(%rdi), %r12
	testq	%r12, %r12
	je	.L144
.L129:
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	subq	$8, %rsp
	movq	2680(%rdi), %rdx
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	movq	352(%rdi), %rdi
	pushq	$0
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r11
	popq	%r13
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L145
.L130:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L146
.L131:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	movq	%rax, %rcx
	leaq	_ZN4node10HandleWrap6HasRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L147
.L132:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node10HandleWrap3RefERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L148
.L133:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node10HandleWrap5UnrefERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L149
.L134:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3128(%rbx), %rdi
	movq	352(%rbx), %r13
	testq	%rdi, %rdi
	je	.L135
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3128(%rbx)
.L135:
	testq	%r12, %r12
	je	.L129
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3128(%rbx)
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rsi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L149:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L148:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L147:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L146:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L131
	.cfi_endproc
.LFE7536:
	.size	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE, .-_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE
	.section	.text._ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC2EPS1_:
.LFB8249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L167
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L168
.L153:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L157
.L150:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L158
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L154:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L153
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L159
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L155:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L157:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L150
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L159:
	xorl	%edx, %edx
	jmp	.L155
	.cfi_endproc
.LFE8249:
	.size	_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC2EPS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10HandleWrap7OnCloseEP11uv_handle_s
	.type	_ZN4node10HandleWrap7OnCloseEP11uv_handle_s, @function
_ZN4node10HandleWrap7OnCloseEP11uv_handle_s:
.LFB7535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L210
	leaq	-88(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_10HandleWrapELb0EEC1EPS1_
	movq	-88(%rbp), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L211
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L191
	movq	16(%rbx), %rbx
	movb	$1, 9(%rax)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	-88(%rbp), %rdi
	cmpl	$1, 72(%rdi)
	jne	.L212
	movq	(%rdi), %rax
	leaq	_ZN4node10HandleWrap7OnCloseEv(%rip), %rdx
	movl	$2, 72(%rdi)
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L213
.L174:
	movq	64(%rdi), %rax
	movq	56(%rdi), %rdx
	movq	8(%rdi), %r8
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	leaq	56(%rdi), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 56(%rdi)
	testq	%r8, %r8
	je	.L176
	movzbl	11(%r8), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L214
.L177:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r8, %rdi
	movq	128(%rax), %rdx
	call	_ZN2v86Object3HasENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L176
	shrw	$8, %ax
	je	.L176
	movq	-88(%rbp), %r14
	movq	360(%rbx), %rax
	movq	8(%r14), %rdi
	movq	128(%rax), %r15
	movq	16(%r14), %rdx
	testq	%rdi, %rdi
	je	.L179
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L179
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %rdi
.L179:
	movq	3280(%rdx), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L176
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L176
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L215
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L216
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L169
	cmpb	$0, 9(%rdx)
	je	.L187
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	call	*%rax
	movq	-88(%rbp), %rdi
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L211:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L194
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L172:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
.L191:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L214:
	movq	16(%rdi), %rax
	movq	(%r8), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r8
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	cmpb	$0, 8(%rdx)
	je	.L169
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L169
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%edx, %edx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7535:
	.size	_ZN4node10HandleWrap7OnCloseEP11uv_handle_s, .-_ZN4node10HandleWrap7OnCloseEP11uv_handle_s
	.weak	_ZTVN4node10HandleWrapE
	.section	.data.rel.ro._ZTVN4node10HandleWrapE,"awG",@progbits,_ZTVN4node10HandleWrapE,comdat
	.align 8
	.type	_ZTVN4node10HandleWrapE, @object
	.size	_ZTVN4node10HandleWrapE, 112
_ZTVN4node10HandleWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.section	.rodata.str1.1
.LC6:
	.string	"../src/handle_wrap.cc:126"
.LC7:
	.string	"(wrap->state_) == (kClosing)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"static void node::HandleWrap::OnClose(uv_handle_t*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args_0, @object
	.size	_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args_0, 24
_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args_0:
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.section	.rodata.str1.1
.LC9:
	.string	"../src/handle_wrap.cc:118"
.LC10:
	.string	"(handle->data) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args, @object
	.size	_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args, 24
_ZZN4node10HandleWrap7OnCloseEP11uv_handle_sE4args:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC8
	.section	.rodata.str1.1
.LC11:
	.string	"../src/handle_wrap.cc:112"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC13:
	.string	"node::HandleWrap::HandleWrap(node::Environment*, v8::Local<v8::Object>, uv_handle_t*, node::AsyncWrap::ProviderType)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10HandleWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeEE4args, @object
	.size	_ZZN4node10HandleWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeEE4args, 24
_ZZN4node10HandleWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeEE4args:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC14:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC16:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/base_object-inl.h:195"
.LC18:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC16
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC21:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC22:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC24:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC25:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC27:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
