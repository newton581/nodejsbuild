	.file	"node_errors.cc"
	.text
	.p2align 4
	.type	_ZN4node6errorsL21NoSideEffectsToStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node6errorsL21NoSideEffectsToStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rsi
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L2
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3:
	call	_ZNK2v85Value14ToDetailStringENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L1
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L3
	.cfi_endproc
.LFE7877:
	.size	_ZN4node6errorsL21NoSideEffectsToStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node6errorsL21NoSideEffectsToStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev,"axG",@progbits,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.type	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, @function
_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev:
.LFB10485:
	.cfi_startproc
	endbr64
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE10485:
	.size	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, .-_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev
	.set	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0:
.LFB10605:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L10
	testq	%rdx, %rdx
	je	.L10
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L10:
	subq	%r13, %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L11
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L11:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L12
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L13
.L12:
	testq	%rbx, %rbx
	je	.L13
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L13:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L14
	call	__stack_chk_fail@PLT
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10605:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"setPrepareStackTraceCallback"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"setEnhanceStackForFatalException"
	.section	.rodata.str1.1
.LC3:
	.string	"noSideEffectsToString"
.LC4:
	.string	"triggerUncaughtException"
	.section	.text.unlikely
.LCOLDB5:
	.text
.LHOTB5:
	.p2align 4
	.globl	_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L26
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L26
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L26
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L43
.L27:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L44
.L28:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L45
.L29:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L46
.L30:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L47
.L31:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L48
.L32:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node6errorsL21NoSideEffectsToStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L49
.L33:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L50
.L34:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L51
.L35:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6errorsL24TriggerUncaughtExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L52
.L36:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L53
.L37:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L54
.L38:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L44:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L45:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L46:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L48:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L49:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L50:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L51:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L52:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L53:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L54:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L38
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7879:
.L26:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7879:
	.text
	.size	_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE5:
	.text
.LHOTE5:
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB10586:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, (%rdi)
	testq	%rsi, %rsi
	je	.L56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L68
	cmpq	$1, %rax
	jne	.L60
	movzbl	(%r12), %edx
	movb	%dl, 16(%rbx)
.L61:
	movq	%rax, 8(%rbx)
	movb	$0, 0(%r13,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L61
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r13
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L59:
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %rax
	movq	(%rbx), %r13
	jmp	.L61
.L69:
	call	__stack_chk_fail@PLT
.L56:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE10586:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata.str1.1
.LC6:
	.string	"0123456789abcdef"
	.section	.text.unlikely
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB10601:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC6(%rip), %rax
.L71:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L71
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L72
	call	__stack_chk_fail@PLT
.L72:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10601:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text._ZNK4node9Utf8Value8ToStringB5cxx11Ev,"axG",@progbits,_ZNK4node9Utf8Value8ToStringB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node9Utf8Value8ToStringB5cxx11Ev
	.type	_ZNK4node9Utf8Value8ToStringB5cxx11Ev, @function
_ZNK4node9Utf8Value8ToStringB5cxx11Ev:
.LFB4050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	16(%rsi), %r14
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L76
	testq	%r14, %r14
	je	.L92
.L76:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L93
	cmpq	$1, %r13
	jne	.L79
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L80:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L80
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L78:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L80
.L92:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4050:
	.size	_ZNK4node9Utf8Value8ToStringB5cxx11Ev, .-_ZNK4node9Utf8Value8ToStringB5cxx11Ev
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L95
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L97
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L99
.L95:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L99
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11Environment10GetCurrentEPN2v87IsolateE,"axG",@progbits,_ZN4node11Environment10GetCurrentEPN2v87IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11Environment10GetCurrentEPN2v87IsolateE
	.type	_ZN4node11Environment10GetCurrentEPN2v87IsolateE, @function
_ZN4node11Environment10GetCurrentEPN2v87IsolateE:
.LFB6975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L107
	leaq	-48(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L105
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L105
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L105
	movq	271(%rax), %r12
.L104:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L101:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%r12d, %r12d
	jmp	.L101
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6975:
	.size	_ZN4node11Environment10GetCurrentEPN2v87IsolateE, .-_ZN4node11Environment10GetCurrentEPN2v87IsolateE
	.text
	.p2align 4
	.globl	_ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE
	.type	_ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE, @function
_ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE:
.LFB7853:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L119
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L122
.L110:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	104(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L110
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v85Value6IsTrueEv@PLT
	.cfi_endproc
.LFE7853:
	.size	_ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE, .-_ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE
	.p2align 4
	.globl	_ZN4node5AbortEv
	.type	_ZN4node5AbortEv, @function
_ZN4node5AbortEv:
.LFB7862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	stderr(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node13DumpBacktraceEP8_IO_FILE@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	call	abort@PLT
	.cfi_endproc
.LFE7862:
	.size	_ZN4node5AbortEv, .-_ZN4node5AbortEv
	.section	.rodata.str1.1
.LC7:
	.string	":"
.LC8:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"%s: %s:%s%s Assertion `%s' failed.\n"
	.text
	.p2align 4
	.globl	_ZN4node6AssertERKNS_13AssertionInfoE
	.type	_ZN4node6AssertERKNS_13AssertionInfoE, @function
_ZN4node6AssertERKNS_13AssertionInfoE:
.LFB7863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node27GetHumanReadableProcessNameB5cxx11Ev@PLT
	movq	16(%rbx), %r9
	movq	(%rbx), %r8
	movl	$1, %esi
	leaq	.LC8(%rip), %rdx
	leaq	.LC7(%rip), %rax
	movq	-64(%rbp), %rcx
	movq	stderr(%rip), %rdi
	cmpb	$0, (%r9)
	pushq	8(%rbx)
	cmove	%rdx, %rax
	leaq	.LC9(%rip), %rdx
	pushq	%rax
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	call	_ZN4node5AbortEv
	.cfi_endproc
.LFE7863:
	.size	_ZN4node6AssertERKNS_13AssertionInfoE, .-_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4
	.globl	_ZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L141
	cmpw	$1040, %cx
	jne	.L131
.L141:
	movq	23(%rdx), %r12
.L133:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L134
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L149
.L136:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L150
	movq	8(%rbx), %r13
.L138:
	movq	2960(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L139
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2960(%r12)
.L139:
	testq	%r13, %r13
	je	.L130
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2960(%r12)
.L130:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L134:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L136
.L149:
	leaq	_ZZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L133
	.cfi_endproc
.LFE7875:
	.size	_ZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L170
	cmpw	$1040, %cx
	jne	.L152
.L170:
	movq	23(%rdx), %r12
.L154:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L155
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L187
.L157:
	cmpl	$1, 16(%rbx)
	jle	.L188
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L159:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L189
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L161
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L162:
	movq	2776(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L163
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2776(%r12)
.L163:
	testq	%r13, %r13
	je	.L164
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2776(%r12)
.L164:
	cmpl	$1, 16(%rbx)
	movq	352(%r12), %r14
	movq	2768(%r12), %rdi
	jg	.L165
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	testq	%rdi, %rdi
	je	.L166
.L167:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2768(%r12)
.L166:
	testq	%r13, %r13
	je	.L151
.L168:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2768(%r12)
.L151:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L155:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L157
.L187:
	leaq	_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L165:
	movq	8(%rbx), %r13
	subq	$8, %r13
	testq	%rdi, %rdi
	jne	.L167
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L161:
	movq	8(%rbx), %r13
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.cfi_endproc
.LFE7876:
	.size	_ZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB6032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L191
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L220
	cmpq	$1, %rax
	jne	.L194
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L195:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L190:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L195
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L193:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L191:
	cmpb	$37, 1(%rax)
	jne	.L222
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L223
	cmpq	$1, %r13
	jne	.L200
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L201:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L203
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L224
.L203:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L205:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L225
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L207:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L190
	call	_ZdlPv@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L200:
	testq	%r13, %r13
	jne	.L226
	movq	%rbx, %rax
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L199:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L225:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L205
.L222:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L221:
	call	__stack_chk_fail@PLT
.L226:
	movq	%rbx, %rdi
	jmp	.L199
	.cfi_endproc
.LFE6032:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.section	.rodata.str1.1
.LC10:
	.string	"EACCES"
.LC11:
	.string	"EADDRNOTAVAIL"
.LC12:
	.string	"EAFNOSUPPORT"
.LC13:
	.string	"EAGAIN"
.LC14:
	.string	"EALREADY"
.LC15:
	.string	"EBADF"
.LC16:
	.string	"EBADMSG"
.LC17:
	.string	"EBUSY"
.LC18:
	.string	"ECANCELED"
.LC19:
	.string	"ECHILD"
.LC20:
	.string	"ECONNABORTED"
.LC21:
	.string	"ECONNREFUSED"
.LC22:
	.string	"ECONNRESET"
.LC23:
	.string	"EDEADLK"
.LC24:
	.string	"EDESTADDRREQ"
.LC25:
	.string	"EDOM"
.LC26:
	.string	"EDQUOT"
.LC27:
	.string	"EEXIST"
.LC28:
	.string	"EFAULT"
.LC29:
	.string	"EFBIG"
.LC30:
	.string	"EHOSTUNREACH"
.LC31:
	.string	"EIDRM"
.LC32:
	.string	"EILSEQ"
.LC33:
	.string	"EINPROGRESS"
.LC34:
	.string	"EINTR"
.LC35:
	.string	"EINVAL"
.LC36:
	.string	"EIO"
.LC37:
	.string	"EISCONN"
.LC38:
	.string	"EISDIR"
.LC39:
	.string	"ELOOP"
.LC40:
	.string	"EMFILE"
.LC41:
	.string	"EMLINK"
.LC42:
	.string	"EMSGSIZE"
.LC43:
	.string	"EMULTIHOP"
.LC44:
	.string	"ENAMETOOLONG"
.LC45:
	.string	"ENETDOWN"
.LC46:
	.string	"ENETRESET"
.LC47:
	.string	"ENETUNREACH"
.LC48:
	.string	"ENFILE"
.LC49:
	.string	"ENOBUFS"
.LC50:
	.string	"ENODATA"
.LC51:
	.string	"ENODEV"
.LC52:
	.string	"ENOENT"
.LC53:
	.string	"ENOEXEC"
.LC54:
	.string	"ENOLINK"
.LC55:
	.string	"ENOLCK"
.LC56:
	.string	"ENOMEM"
.LC57:
	.string	"ENOMSG"
.LC58:
	.string	"ENOPROTOOPT"
.LC59:
	.string	"ENOSPC"
.LC60:
	.string	"ENOSR"
.LC61:
	.string	"ENOSTR"
.LC62:
	.string	"ENOSYS"
.LC63:
	.string	"ENOTCONN"
.LC64:
	.string	"ENOTDIR"
.LC65:
	.string	"ENOTEMPTY"
.LC66:
	.string	"ENOTSOCK"
.LC67:
	.string	"ENOTSUP"
.LC68:
	.string	"ENOTTY"
.LC69:
	.string	"ENXIO"
.LC70:
	.string	"EOVERFLOW"
.LC71:
	.string	"EPERM"
.LC72:
	.string	"EPIPE"
.LC73:
	.string	"EPROTO"
.LC74:
	.string	"EPROTONOSUPPORT"
.LC75:
	.string	"EPROTOTYPE"
.LC76:
	.string	"ERANGE"
.LC77:
	.string	"EROFS"
.LC78:
	.string	"ESPIPE"
.LC79:
	.string	"ESRCH"
.LC80:
	.string	"ESTALE"
.LC81:
	.string	"ETIME"
.LC82:
	.string	"ETIMEDOUT"
.LC83:
	.string	"ETXTBSY"
.LC84:
	.string	"EXDEV"
.LC85:
	.string	"EADDRINUSE"
	.text
	.p2align 4
	.globl	_ZN4node6errors12errno_stringEi
	.type	_ZN4node6errors12errno_stringEi, @function
_ZN4node6errors12errno_stringEi:
.LFB7873:
	.cfi_startproc
	endbr64
	cmpl	$125, %edi
	ja	.L228
	leaq	.L230(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L230:
	.long	.L228-.L230
	.long	.L305-.L230
	.long	.L304-.L230
	.long	.L303-.L230
	.long	.L302-.L230
	.long	.L301-.L230
	.long	.L300-.L230
	.long	.L228-.L230
	.long	.L299-.L230
	.long	.L298-.L230
	.long	.L297-.L230
	.long	.L296-.L230
	.long	.L295-.L230
	.long	.L306-.L230
	.long	.L293-.L230
	.long	.L228-.L230
	.long	.L292-.L230
	.long	.L291-.L230
	.long	.L290-.L230
	.long	.L289-.L230
	.long	.L288-.L230
	.long	.L287-.L230
	.long	.L286-.L230
	.long	.L285-.L230
	.long	.L284-.L230
	.long	.L283-.L230
	.long	.L282-.L230
	.long	.L281-.L230
	.long	.L280-.L230
	.long	.L279-.L230
	.long	.L278-.L230
	.long	.L277-.L230
	.long	.L276-.L230
	.long	.L275-.L230
	.long	.L274-.L230
	.long	.L273-.L230
	.long	.L272-.L230
	.long	.L271-.L230
	.long	.L270-.L230
	.long	.L269-.L230
	.long	.L268-.L230
	.long	.L228-.L230
	.long	.L267-.L230
	.long	.L266-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L265-.L230
	.long	.L264-.L230
	.long	.L263-.L230
	.long	.L262-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L261-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L260-.L230
	.long	.L259-.L230
	.long	.L228-.L230
	.long	.L258-.L230
	.long	.L257-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L256-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L255-.L230
	.long	.L254-.L230
	.long	.L253-.L230
	.long	.L252-.L230
	.long	.L251-.L230
	.long	.L250-.L230
	.long	.L228-.L230
	.long	.L249-.L230
	.long	.L228-.L230
	.long	.L248-.L230
	.long	.L247-.L230
	.long	.L246-.L230
	.long	.L245-.L230
	.long	.L244-.L230
	.long	.L243-.L230
	.long	.L242-.L230
	.long	.L241-.L230
	.long	.L240-.L230
	.long	.L239-.L230
	.long	.L238-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L237-.L230
	.long	.L236-.L230
	.long	.L228-.L230
	.long	.L235-.L230
	.long	.L234-.L230
	.long	.L233-.L230
	.long	.L232-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L231-.L230
	.long	.L228-.L230
	.long	.L228-.L230
	.long	.L229-.L230
	.text
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	.LC8(%rip), %rax
	ret
.L229:
	leaq	.LC18(%rip), %rax
	ret
.L231:
	leaq	.LC26(%rip), %rax
	ret
.L232:
	leaq	.LC80(%rip), %rax
	ret
.L233:
	leaq	.LC33(%rip), %rax
	ret
.L234:
	leaq	.LC14(%rip), %rax
	ret
.L235:
	leaq	.LC30(%rip), %rax
	ret
.L236:
	leaq	.LC21(%rip), %rax
	ret
.L237:
	leaq	.LC82(%rip), %rax
	ret
.L238:
	leaq	.LC63(%rip), %rax
	ret
.L239:
	leaq	.LC37(%rip), %rax
	ret
.L240:
	leaq	.LC49(%rip), %rax
	ret
.L241:
	leaq	.LC22(%rip), %rax
	ret
.L242:
	leaq	.LC20(%rip), %rax
	ret
.L243:
	leaq	.LC46(%rip), %rax
	ret
.L244:
	leaq	.LC47(%rip), %rax
	ret
.L245:
	leaq	.LC45(%rip), %rax
	ret
.L246:
	leaq	.LC11(%rip), %rax
	ret
.L248:
	leaq	.LC12(%rip), %rax
	ret
.L249:
	leaq	.LC67(%rip), %rax
	ret
.L250:
	leaq	.LC74(%rip), %rax
	ret
.L251:
	leaq	.LC58(%rip), %rax
	ret
.L252:
	leaq	.LC75(%rip), %rax
	ret
.L253:
	leaq	.LC42(%rip), %rax
	ret
.L254:
	leaq	.LC24(%rip), %rax
	ret
.L255:
	leaq	.LC66(%rip), %rax
	ret
.L256:
	leaq	.LC32(%rip), %rax
	ret
.L257:
	leaq	.LC70(%rip), %rax
	ret
.L258:
	leaq	.LC16(%rip), %rax
	ret
.L259:
	leaq	.LC43(%rip), %rax
	ret
.L260:
	leaq	.LC73(%rip), %rax
	ret
.L261:
	leaq	.LC54(%rip), %rax
	ret
.L262:
	leaq	.LC60(%rip), %rax
	ret
.L263:
	leaq	.LC81(%rip), %rax
	ret
.L264:
	leaq	.LC50(%rip), %rax
	ret
.L265:
	leaq	.LC61(%rip), %rax
	ret
.L266:
	leaq	.LC31(%rip), %rax
	ret
.L267:
	leaq	.LC57(%rip), %rax
	ret
.L268:
	leaq	.LC39(%rip), %rax
	ret
.L269:
	leaq	.LC65(%rip), %rax
	ret
.L270:
	leaq	.LC62(%rip), %rax
	ret
.L271:
	leaq	.LC55(%rip), %rax
	ret
.L272:
	leaq	.LC44(%rip), %rax
	ret
.L273:
	leaq	.LC23(%rip), %rax
	ret
.L274:
	leaq	.LC76(%rip), %rax
	ret
.L275:
	leaq	.LC25(%rip), %rax
	ret
.L276:
	leaq	.LC72(%rip), %rax
	ret
.L277:
	leaq	.LC41(%rip), %rax
	ret
.L278:
	leaq	.LC77(%rip), %rax
	ret
.L279:
	leaq	.LC78(%rip), %rax
	ret
.L280:
	leaq	.LC59(%rip), %rax
	ret
.L281:
	leaq	.LC29(%rip), %rax
	ret
.L282:
	leaq	.LC83(%rip), %rax
	ret
.L283:
	leaq	.LC68(%rip), %rax
	ret
.L284:
	leaq	.LC40(%rip), %rax
	ret
.L285:
	leaq	.LC48(%rip), %rax
	ret
.L286:
	leaq	.LC35(%rip), %rax
	ret
.L287:
	leaq	.LC38(%rip), %rax
	ret
.L288:
	leaq	.LC64(%rip), %rax
	ret
.L289:
	leaq	.LC51(%rip), %rax
	ret
.L290:
	leaq	.LC84(%rip), %rax
	ret
.L291:
	leaq	.LC27(%rip), %rax
	ret
.L292:
	leaq	.LC17(%rip), %rax
	ret
.L293:
	leaq	.LC28(%rip), %rax
	ret
.L295:
	leaq	.LC56(%rip), %rax
	ret
.L296:
	leaq	.LC13(%rip), %rax
	ret
.L297:
	leaq	.LC19(%rip), %rax
	ret
.L298:
	leaq	.LC15(%rip), %rax
	ret
.L299:
	leaq	.LC53(%rip), %rax
	ret
.L300:
	leaq	.LC69(%rip), %rax
	ret
.L301:
	leaq	.LC36(%rip), %rax
	ret
.L302:
	leaq	.LC34(%rip), %rax
	ret
.L303:
	leaq	.LC79(%rip), %rax
	ret
.L304:
	leaq	.LC52(%rip), %rax
	ret
.L305:
	leaq	.LC71(%rip), %rax
	ret
.L306:
	leaq	.LC10(%rip), %rax
	ret
.L247:
	leaq	.LC85(%rip), %rax
	ret
	.cfi_endproc
.LFE7873:
	.size	_ZN4node6errors12errno_stringEi, .-_ZN4node6errors12errno_stringEi
	.p2align 4
	.globl	_Z16_register_errorsv
	.type	_Z16_register_errorsv, @function
_Z16_register_errorsv:
.LFB7884:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7884:
	.size	_Z16_register_errorsv, .-_Z16_register_errorsv
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB7958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L309
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L309:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L311
.L325:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L325
.L311:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L326
.L312:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r14
	movl	$32, -240(%rbp)
	movq	%rax, -232(%rbp)
	andq	$-16, %r14
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$48, -236(%rbp)
	call	*%r10
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L327
	cmpq	$1, %r13
	jne	.L315
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L316:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L328
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L316
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L314:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L326:
	orq	$0, -8(%rsp,%rcx)
	jmp	.L312
.L328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7958:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L336
	movq	16(%rdi), %r10
.L330:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L331
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L337
	movq	16(%r9), %r10
.L332:
	cmpq	%r10, %rax
	jbe	.L339
.L331:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L333:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L340
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L335:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L340:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$15, %r10d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$15, %r10d
	jmp	.L332
	.cfi_endproc
.LFE8280:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB8662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L345
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L343:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L343
	.cfi_endproc
.LFE8662:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.text.unlikely._ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L347
	call	__stack_chk_fail@PLT
.L347:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9203:
	.size	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_:
.LFB8698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L351
	call	__stack_chk_fail@PLT
.L351:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8698:
	.size	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB9600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L354
	testq	%r14, %r14
	je	.L370
.L354:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L371
	cmpq	$1, %r13
	jne	.L357
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L358:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L358
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L356:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L358
.L370:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9600:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC86:
	.string	"(null)"
.LC87:
	.string	"lz"
.LC88:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB9369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L374
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L374:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC87(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r9
.L375:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L375
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L376
	cmpb	$99, %dl
	jg	.L377
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L378
	cmpb	$88, %dl
	je	.L379
	jmp	.L376
.L377:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L376
	leaq	.L381(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L381:
	.long	.L380-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L380-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L380-.L381
	.long	.L383-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L380-.L381
	.long	.L376-.L381
	.long	.L380-.L381
	.long	.L376-.L381
	.long	.L376-.L381
	.long	.L380-.L381
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L378:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L415
	jmp	.L412
.L376:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L412
.L415:
	call	_ZdlPv@PLT
	jmp	.L412
.L380:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L395
	leaq	.LC86(%rip), %rsi
.L395:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L407
	jmp	.L392
.L379:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L397
	leaq	.LC86(%rip), %rsi
.L397:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L398
	call	_ZdlPv@PLT
.L398:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L392
.L407:
	call	_ZdlPv@PLT
	jmp	.L392
.L383:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC88(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L400
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L400:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L392:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L412:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L387
	call	_ZdlPv@PLT
.L387:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L403
	call	__stack_chk_fail@PLT
.L403:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9369:
	.size	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB8972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L417
	call	__stack_chk_fail@PLT
.L417:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8972:
	.size	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_:
.LFB8420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L419
	call	_ZdlPv@PLT
.L419:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L421
	call	__stack_chk_fail@PLT
.L421:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8420:
	.size	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB9620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L424
	leaq	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L424:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %r9
.L425:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -216(%rbp)
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	movq	-216(%rbp), %r8
	jne	.L425
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L426
	cmpb	$99, %dl
	jg	.L427
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L428
	cmpb	$88, %dl
	je	.L429
	jmp	.L426
.L427:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L426
	leaq	.L431(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L431:
	.long	.L430-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L430-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L430-.L431
	.long	.L433-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L430-.L431
	.long	.L426-.L431
	.long	.L430-.L431
	.long	.L426-.L431
	.long	.L426-.L431
	.long	.L430-.L431
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L428:
	movq	-184(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L462
	jmp	.L438
.L426:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	leaq	-144(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L438
.L462:
	call	_ZdlPv@PLT
.L438:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L458
	jmp	.L437
.L430:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L445
	leaq	.LC86(%rip), %rsi
.L445:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L457
	jmp	.L442
.L429:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L447
	leaq	.LC86(%rip), %rsi
.L447:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L448
	call	_ZdlPv@PLT
.L448:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L442
.L457:
	call	_ZdlPv@PLT
	jmp	.L442
.L433:
	movq	(%r8), %r9
	leaq	-80(%rbp), %rbx
	xorl	%eax, %eax
	leaq	.LC88(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%rbx, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L450
	leaq	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L450:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L442:
	movq	-184(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L437
.L458:
	call	_ZdlPv@PLT
.L437:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L423
	call	_ZdlPv@PLT
.L423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L453
	call	__stack_chk_fail@PLT
.L453:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9620:
	.size	_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB9204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L465
	call	__stack_chk_fail@PLT
.L465:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9204:
	.size	_ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_:
.LFB8699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPKcS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L469
	call	__stack_chk_fail@PLT
.L469:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8699:
	.size	_ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_
	.section	.rodata.str1.1
.LC89:
	.string	"FATAL ERROR: %s %s\n"
.LC90:
	.string	"FATAL ERROR: %s\n"
.LC91:
	.string	"FatalError"
	.section	.text.unlikely
	.globl	_ZN4node12OnFatalErrorEPKcS1_
	.type	_ZN4node12OnFatalErrorEPKcS1_, @function
_ZN4node12OnFatalErrorEPKcS1_:
.LFB7869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-128(%rbp), %rdx
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, -120(%rbp)
	movq	stderr(%rip), %rdi
	movq	%rsi, -128(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L472
	leaq	-120(%rbp), %r8
	movq	%rdx, %rcx
	leaq	.LC89(%rip), %rsi
	movq	%r8, %rdx
	call	_ZN4node7FPrintFIJRPKcS3_EEEvP8_IO_FILES2_DpOT_
	jmp	.L473
.L472:
	leaq	.LC90(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
.L473:
	call	_ZN2v87Isolate10GetCurrentEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node11Environment10GetCurrentEPN2v87IsolateE
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	movq	%rax, %r13
	call	uv_mutex_lock@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	movb	307(%rax), %bl
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	je	.L474
	leaq	-112(%rbp), %rbx
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rcx
	leaq	-80(%rbp), %rdi
	movq	%r13, %rdx
	pushq	%rax
	movq	%rbx, %r9
	leaq	.LC91(%rip), %r8
	movq	%r12, %rsi
	pushq	$0
	call	_ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L475
	call	_ZdlPv@PLT
.L475:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	call	_ZN4node5AbortEv
	.cfi_endproc
.LFE7869:
	.size	_ZN4node12OnFatalErrorEPKcS1_, .-_ZN4node12OnFatalErrorEPKcS1_
	.globl	_ZN4node10FatalErrorEPKcS1_
	.type	_ZN4node10FatalErrorEPKcS1_, @function
_ZN4node10FatalErrorEPKcS1_:
.LFB7868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node12OnFatalErrorEPKcS1_
	.cfi_endproc
.LFE7868:
	.size	_ZN4node10FatalErrorEPKcS1_, .-_ZN4node10FatalErrorEPKcS1_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB9698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L485
	testq	%rsi, %rsi
	je	.L501
.L485:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L502
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L488
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L489:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L503
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L489
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L502:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L487:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L489
.L501:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L503:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9698:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB9969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L505
	leaq	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L505:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC87(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r9
.L506:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L506
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L507
	cmpb	$99, %dl
	jg	.L508
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L509
	cmpb	$88, %dl
	je	.L510
	jmp	.L507
.L508:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L507
	leaq	.L512(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L512:
	.long	.L511-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L511-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L511-.L512
	.long	.L514-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L511-.L512
	.long	.L507-.L512
	.long	.L511-.L512
	.long	.L507-.L512
	.long	.L507-.L512
	.long	.L511-.L512
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L509:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L546
	jmp	.L543
.L507:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L543
.L546:
	call	_ZdlPv@PLT
	jmp	.L543
.L511:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L526
	leaq	.LC86(%rip), %rsi
.L526:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L538
	jmp	.L523
.L510:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L528
	leaq	.LC86(%rip), %rsi
.L528:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L529
	call	_ZdlPv@PLT
.L529:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L523
.L538:
	call	_ZdlPv@PLT
	jmp	.L523
.L514:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC88(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L531
	leaq	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L531:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L523:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L543:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L518
	call	_ZdlPv@PLT
.L518:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L534
	call	__stack_chk_fail@PLT
.L534:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9969:
	.size	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.rodata._ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC92:
	.string	"%d"
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_:
.LFB9606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L548
	leaq	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L548:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC87(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L549:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L549
	cmpb	$120, %dl
	jg	.L550
	cmpb	$99, %dl
	jg	.L551
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L552
	cmpb	$88, %dl
	je	.L553
	jmp	.L550
.L551:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L550
	leaq	.L555(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,comdat
	.align 4
	.align 4
.L555:
	.long	.L556-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L556-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L558-.L555
	.long	.L557-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L556-.L555
	.long	.L550-.L555
	.long	.L556-.L555
	.long	.L550-.L555
	.long	.L550-.L555
	.long	.L554-.L555
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,comdat
.L552:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L579
	jmp	.L561
.L550:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L584
	call	_ZdlPv@PLT
	jmp	.L584
.L556:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC92(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L581
.L558:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L566:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L566
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L581
.L554:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L581:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L578
	jmp	.L565
.L553:
	movl	(%r8), %esi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L569
	call	_ZdlPv@PLT
.L569:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L565
.L578:
	call	_ZdlPv@PLT
	jmp	.L565
.L557:
	leaq	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L565:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L584:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L561
.L579:
	call	_ZdlPv@PLT
.L561:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L573
	call	__stack_chk_fail@PLT
.L573:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9606:
	.size	_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_, .-_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB9189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L586
	leaq	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L586:
	movq	%rax, %r10
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	movq	%r9, -216(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-216(%rbp), %r9
	movq	-184(%rbp), %r10
	movq	%r13, -216(%rbp)
	movq	%r10, %r15
	movq	%r9, %r12
.L587:
	movq	%r15, %rax
	movq	%rbx, %rdi
	movq	%r15, -184(%rbp)
	leaq	1(%r15), %r15
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L587
	movl	%r13d, %edx
	movq	%r12, %r9
	movq	-216(%rbp), %r13
	movq	%r15, %r10
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L588
	cmpb	$99, %dl
	jg	.L589
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r15
	movq	%rax, -216(%rbp)
	je	.L590
	cmpb	$88, %dl
	je	.L591
	jmp	.L588
.L589:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L588
	leaq	.L593(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L593:
	.long	.L592-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L592-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L592-.L593
	.long	.L595-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L592-.L593
	.long	.L588-.L593
	.long	.L592-.L593
	.long	.L588-.L593
	.long	.L588-.L593
	.long	.L592-.L593
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L590:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L624
	jmp	.L600
.L588:
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L600
.L624:
	call	_ZdlPv@PLT
.L600:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L620
	jmp	.L599
.L592:
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L607
	leaq	.LC86(%rip), %rsi
.L607:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L619
	jmp	.L604
.L591:
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L609
	leaq	.LC86(%rip), %rsi
.L609:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L604
.L619:
	call	_ZdlPv@PLT
	jmp	.L604
.L595:
	leaq	-80(%rbp), %r15
	movq	(%r9), %r9
	xorl	%eax, %eax
	leaq	.LC88(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r15, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L612
	leaq	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L612:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L604:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %rcx
	movq	%r12, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L599
.L620:
	call	_ZdlPv@PLT
.L599:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L615
	call	__stack_chk_fail@PLT
.L615:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9189:
	.size	_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB8661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L627
	call	__stack_chk_fail@PLT
.L627:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8661:
	.size	_ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"node-do-not-add-exception-line"
	.section	.rodata.str1.1
.LC94:
	.string	"%s:%i\n%s\n"
	.section	.text.unlikely
.LCOLDB95:
	.text
.LHOTB95:
	.p2align 4
	.type	_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0, @function
_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0:
.LFB10606:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdx, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rcx, %rdi
	pushq	%rbx
	subq	$3384, %rsp
	.cfi_offset 3, -56
	movq	%r8, -3400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v87Message13GetSourceLineENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L689
.L630:
	leaq	-3200(%rbp), %rdi
	movq	%r15, %rsi
	leaq	-3280(%rbp), %rbx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-3184(%rbp), %r10
	movq	-3200(%rbp), %r8
	movq	%rbx, -3296(%rbp)
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L631
	testq	%r10, %r10
	je	.L690
.L631:
	movq	%r8, -3360(%rbp)
	cmpq	$15, %r8
	ja	.L691
	cmpq	$1, %r8
	jne	.L634
	movzbl	(%r10), %eax
	leaq	-3296(%rbp), %r11
	movb	%al, -3280(%rbp)
	movq	%rbx, %rax
.L635:
	movq	%r8, -3288(%rbp)
	xorl	%edx, %edx
	movl	$30, %ecx
	movq	%r11, %rdi
	movb	$0, (%rax,%r8)
	leaq	.LC93(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	cmpq	$-1, %rax
	je	.L636
	movq	-3400(%rbp), %rax
	movb	$0, (%rax)
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	-3296(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L692
	movq	%rax, (%r12)
	movq	-3280(%rbp), %rax
	movq	%rax, 16(%r12)
.L638:
	movq	-3288(%rbp), %rax
	movq	%rax, 8(%r12)
.L639:
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L629
	leaq	-3176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L629
	call	free@PLT
.L629:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	addq	$3384, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L694
	movq	%rbx, %rax
	leaq	-3296(%rbp), %r11
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L691:
	leaq	-3296(%rbp), %r11
	leaq	-3360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r10, -3424(%rbp)
	movq	%r11, %rdi
	movq	%r8, -3416(%rbp)
	movq	%r11, -3408(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-3408(%rbp), %r11
	movq	-3416(%rbp), %r8
	movq	%rax, -3296(%rbp)
	movq	%rax, %rdi
	movq	-3360(%rbp), %rax
	movq	-3424(%rbp), %r10
	movq	%rax, -3280(%rbp)
.L633:
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r11, -3408(%rbp)
	call	memcpy@PLT
	movq	-3360(%rbp), %r8
	movq	-3296(%rbp), %rax
	movq	-3408(%rbp), %r11
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L692:
	movdqa	-3280(%rbp), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%rax, -3408(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3408(%rbp), %rdx
	jmp	.L630
.L690:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L693:
	call	__stack_chk_fail@PLT
.L636:
	movq	%r13, %rsi
	leaq	-3360(%rbp), %rdi
	call	_ZNK2v87Message15GetScriptOriginEv@PLT
	movq	%r13, %rdi
	call	_ZNK2v87Message21GetScriptResourceNameEv@PLT
	movq	%r15, %rsi
	leaq	-2144(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2128(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -3376(%rbp)
	call	_ZNK2v87Message13GetLineNumberENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	testb	%al, %al
	je	.L695
.L640:
	movq	-3352(%rbp), %rdi
	sarq	$32, %r15
	movl	%r15d, -3380(%rbp)
	call	_ZNK2v87Integer5ValueEv@PLT
	xorl	%edx, %edx
	subq	%rax, %r15
	subq	$1, %r15
	je	.L696
.L641:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%edx, -3408(%rbp)
	call	_ZNK2v87Message14GetStartColumnENS_5LocalINS_7ContextEEE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	sarq	$32, %r15
	testb	%al, %al
	cmove	%ecx, %r15d
	call	_ZNK2v87Message12GetEndColumnENS_5LocalINS_7ContextEEE@PLT
	movl	-3408(%rbp), %edx
	movl	$0, %ecx
	movq	%rax, %r14
	sarq	$32, %r14
	testb	%al, %al
	cmove	%ecx, %r14d
	cmpl	%r15d, %edx
	jg	.L644
	cmpl	%r14d, %r15d
	jle	.L645
	leaq	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L696:
	movq	-3344(%rbp), %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	movl	%eax, %edx
	jmp	.L641
.L695:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L640
.L694:
	movq	%rbx, %rdi
	leaq	-3296(%rbp), %r11
	jmp	.L633
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0.cold, @function
_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0.cold:
.LFSB10606:
.L645:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	subl	%edx, %r15d
	subl	%edx, %r14d
.L644:
	movq	-3296(%rbp), %rax
	leaq	-3264(%rbp), %r13
	leaq	-3380(%rbp), %rcx
	leaq	-3376(%rbp), %rdx
	leaq	-3368(%rbp), %r8
	movq	%r13, %rdi
	leaq	.LC94(%rip), %rsi
	movq	%rax, -3368(%rbp)
	call	_ZN4node7SPrintFIJRPKcRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	cmpq	$0, -3256(%rbp)
	jne	.L697
	leaq	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L697:
	movq	-3296(%rbp), %rsi
	xorl	%eax, %eax
.L647:
	movslq	%eax, %rdx
	cmpl	%eax, %r15d
	jg	.L698
.L650:
	movslq	%r15d, %r15
.L649:
	movl	%edx, %eax
	cmpl	%r15d, %r14d
	jg	.L699
.L652:
	cmpl	$1020, %eax
	jle	.L653
	leaq	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L698:
	movzbl	(%rsi,%rax), %ecx
	testb	%cl, %cl
	je	.L650
	cmpq	$1020, %rax
	jne	.L700
	movl	$1020, %edx
	jmp	.L650
.L699:
	cmpb	$0, (%rsi,%r15)
	sete	%dil
	cmpl	$1019, %edx
	setg	%cl
	addq	$1, %r15
	orb	%cl, %dil
	jne	.L652
	movb	$94, -1088(%rbp,%rdx)
	addq	$1, %rdx
	jmp	.L649
.L653:
	movq	-3400(%rbp), %rcx
	movslq	%eax, %rdx
	leaq	-1088(%rbp), %rsi
	leaq	-3232(%rbp), %r14
	movb	$10, -1088(%rbp,%rdx)
	leal	1(%rax), %edx
	movq	%r14, %rdi
	leaq	-3216(%rbp), %r15
	movb	$1, (%rcx)
	movslq	%edx, %rdx
	addq	%rsi, %rdx
	movq	%r15, -3232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-3232(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	-3264(%rbp), %rdi
	leaq	-3248(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L655
	call	_ZdlPv@PLT
.L655:
	movq	-2128(%rbp), %rdi
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L656
	testq	%rdi, %rdi
	je	.L656
	call	free@PLT
.L656:
	movq	-3296(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L639
	call	_ZdlPv@PLT
	jmp	.L639
.L700:
	cmpb	$9, %cl
	jne	.L701
.L651:
	movb	%cl, -1088(%rbp,%rax)
	addq	$1, %rax
	jmp	.L647
.L701:
	movl	$32, %ecx
	jmp	.L651
	.cfi_endproc
.LFE10606:
	.text
	.size	_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0, .-_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0
	.section	.text.unlikely
	.size	_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0.cold, .-_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0.cold
.LCOLDE95:
	.text
.LHOTE95:
	.section	.text.unlikely._ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L703
	leaq	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L703:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC87(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L704:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L704
	cmpb	$120, %dl
	jg	.L705
	cmpb	$99, %dl
	jg	.L706
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L707
	cmpb	$88, %dl
	je	.L708
	jmp	.L705
.L706:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L705
	leaq	.L710(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L710:
	.long	.L711-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L711-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L713-.L710
	.long	.L712-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L711-.L710
	.long	.L705-.L710
	.long	.L711-.L710
	.long	.L705-.L710
	.long	.L705-.L710
	.long	.L709-.L710
	.section	.text.unlikely._ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L707:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L734
	jmp	.L716
.L705:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L739
	call	_ZdlPv@PLT
	jmp	.L739
.L711:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC92(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L736
.L713:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L721:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L721
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L736
.L709:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L736:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L733
	jmp	.L720
.L708:
	movl	(%r8), %esi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L724
	call	_ZdlPv@PLT
.L724:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L720
.L733:
	call	_ZdlPv@PLT
	jmp	.L720
.L712:
	leaq	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L720:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L739:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L716
.L734:
	call	_ZdlPv@PLT
.L716:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L702
	call	_ZdlPv@PLT
.L702:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L728
	call	__stack_chk_fail@PLT
.L728:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9970:
	.size	_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L741
	leaq	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L741:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC87(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L742:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L742
	cmpb	$120, %dl
	jg	.L743
	cmpb	$99, %dl
	jg	.L744
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L745
	cmpb	$88, %dl
	je	.L746
	jmp	.L743
.L744:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L743
	leaq	.L748(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L748:
	.long	.L749-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L749-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L751-.L748
	.long	.L750-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L749-.L748
	.long	.L743-.L748
	.long	.L749-.L748
	.long	.L743-.L748
	.long	.L743-.L748
	.long	.L747-.L748
	.section	.text.unlikely._ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L745:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L772
	jmp	.L754
.L743:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L777
	call	_ZdlPv@PLT
	jmp	.L777
.L749:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC92(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L774
.L751:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L759:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L759
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L774
.L747:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L774:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L771
	jmp	.L758
.L746:
	movl	(%r8), %esi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L762
	call	_ZdlPv@PLT
.L762:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L758
.L771:
	call	_ZdlPv@PLT
	jmp	.L758
.L750:
	leaq	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L758:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L777:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L754
.L772:
	call	_ZdlPv@PLT
.L754:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L740
	call	_ZdlPv@PLT
.L740:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L766
	call	__stack_chk_fail@PLT
.L766:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9607:
	.size	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L779
	call	__stack_chk_fail@PLT
.L779:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9190:
	.size	_ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_:
.LFB8665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRKiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L781
	call	_ZdlPv@PLT
.L781:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L783
	call	__stack_chk_fail@PLT
.L783:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8665:
	.size	_ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L786
	leaq	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L786:
	movq	%rax, %r10
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	movq	%r9, -216(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-216(%rbp), %r9
	movq	-184(%rbp), %r10
	movq	%r13, -216(%rbp)
	movq	%r10, %r15
	movq	%r9, %r12
.L787:
	movq	%r15, %rax
	movq	%rbx, %rdi
	movq	%r15, -184(%rbp)
	leaq	1(%r15), %r15
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L787
	movl	%r13d, %edx
	movq	%r12, %r9
	movq	-216(%rbp), %r13
	movq	%r15, %r10
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L788
	cmpb	$99, %dl
	jg	.L789
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r15
	movq	%rax, -216(%rbp)
	je	.L790
	cmpb	$88, %dl
	je	.L791
	jmp	.L788
.L789:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L788
	leaq	.L793(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L793:
	.long	.L792-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L792-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L792-.L793
	.long	.L795-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L792-.L793
	.long	.L788-.L793
	.long	.L792-.L793
	.long	.L788-.L793
	.long	.L788-.L793
	.long	.L792-.L793
	.section	.text.unlikely._ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L790:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L824
	jmp	.L800
.L788:
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L800
.L824:
	call	_ZdlPv@PLT
.L800:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L820
	jmp	.L799
.L792:
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L807
	leaq	.LC86(%rip), %rsi
.L807:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L819
	jmp	.L804
.L791:
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L809
	leaq	.LC86(%rip), %rsi
.L809:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L810
	call	_ZdlPv@PLT
.L810:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L804
.L819:
	call	_ZdlPv@PLT
	jmp	.L804
.L795:
	leaq	-80(%rbp), %r15
	movq	(%r9), %r9
	xorl	%eax, %eax
	leaq	.LC88(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r15, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L812
	leaq	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L812:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L804:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %rcx
	movq	%r12, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L799
.L820:
	call	_ZdlPv@PLT
.L799:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L785
	call	_ZdlPv@PLT
.L785:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L815
	call	__stack_chk_fail@PLT
.L815:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9608:
	.size	_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L827
	call	__stack_chk_fail@PLT
.L827:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9191:
	.size	_ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_:
.LFB8667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJPcRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L829
	call	_ZdlPv@PLT
.L829:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L831
	call	__stack_chk_fail@PLT
.L831:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8667:
	.size	_ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_
	.section	.text._ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	.type	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_, @function
_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_:
.LFB9975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	16(%rsi), %r14
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L834
	testq	%r14, %r14
	je	.L853
.L834:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L854
	cmpq	$1, %r13
	jne	.L837
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L838:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
.L841:
	movq	%r14, %rdi
	call	free@PLT
.L833:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L855
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L837:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L836
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%r14, %r14
	je	.L833
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L836:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L838
.L853:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L855:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9975:
	.size	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_, .-_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	.section	.text._ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	.type	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_, @function
_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_:
.LFB9976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	16(%rsi), %r14
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L857
	testq	%r14, %r14
	je	.L876
.L857:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L877
	cmpq	$1, %r13
	jne	.L860
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L861:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
.L864:
	movq	%r14, %rdi
	call	free@PLT
.L856:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L878
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L859
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	testq	%r14, %r14
	je	.L856
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L877:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L859:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L861
.L876:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L878:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9976:
	.size	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_, .-_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r9
	testq	%rax, %rax
	jne	.L880
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L880:
	movq	%rax, %r10
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	movq	%r9, -192(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r10, -168(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-192(%rbp), %r9
	movq	-168(%rbp), %r10
.L881:
	movq	%r10, %rax
	movq	%r10, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L881
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L882
	cmpb	$99, %dl
	jg	.L883
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r11
	movq	%rax, -192(%rbp)
	je	.L884
	cmpb	$88, %dl
	je	.L885
	jmp	.L882
.L883:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L882
	leaq	.L887(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L887:
	.long	.L888-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L888-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L890-.L887
	.long	.L889-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L888-.L887
	.long	.L882-.L887
	.long	.L888-.L887
	.long	.L882-.L887
	.long	.L882-.L887
	.long	.L886-.L887
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L884:
	movq	-168(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-176(%rbp), %rcx
	movq	%r11, -200(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-200(%rbp), %r11
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r11, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r11
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	jne	.L916
	jmp	.L914
.L882:
	movq	-176(%rbp), %rcx
	movq	%r15, %r8
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r15
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L914
.L916:
	call	_ZdlPv@PLT
	jmp	.L914
.L888:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZNK4node9Utf8Value8ToStringB5cxx11Ev
	jmp	.L911
.L890:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	jmp	.L911
.L886:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
.L911:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L908
	jmp	.L897
.L885:
	movq	%r9, %rsi
	movq	%r11, %rdi
	movq	%r11, -200(%rbp)
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	movq	-200(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L900
	call	_ZdlPv@PLT
.L900:
	movq	-128(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L897
.L908:
	call	_ZdlPv@PLT
	jmp	.L897
.L889:
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L897:
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L914:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	movq	-160(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L879
	call	_ZdlPv@PLT
.L879:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L904
	call	__stack_chk_fail@PLT
.L904:
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9609:
	.size	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L918
	call	__stack_chk_fail@PLT
.L918:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9192:
	.size	_ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_:
.LFB8669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRNS_9Utf8ValueERKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L920
	call	_ZdlPv@PLT
.L920:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L922
	call	__stack_chk_fail@PLT
.L922:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8669:
	.size	_ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	%r9, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r10
	testq	%rax, %rax
	jne	.L925
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L925:
	movq	%rax, %r11
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r11, %rdx
	movq	%r13, %rdi
	movq	%r10, -208(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r11, -168(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-208(%rbp), %r10
	movq	-168(%rbp), %r11
.L926:
	movq	%r11, %rax
	movq	%r11, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r11), %r11
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movq	%r11, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r11
	testq	%rax, %rax
	movq	-224(%rbp), %r10
	jne	.L926
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L927
	cmpb	$99, %dl
	jg	.L928
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r15
	movq	%rax, -208(%rbp)
	je	.L929
	cmpb	$88, %dl
	je	.L930
	jmp	.L927
.L928:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L927
	leaq	.L932(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L932:
	.long	.L933-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L933-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L935-.L932
	.long	.L934-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L933-.L932
	.long	.L927-.L932
	.long	.L933-.L932
	.long	.L927-.L932
	.long	.L927-.L932
	.long	.L931-.L932
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L929:
	movq	-168(%rbp), %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	jne	.L961
	jmp	.L959
.L927:
	movq	-192(%rbp), %r9
	movq	%r10, %rdx
	movq	%r11, %rsi
	movq	%r12, %rdi
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %rcx
	leaq	-128(%rbp), %r15
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L959
.L961:
	call	_ZdlPv@PLT
	jmp	.L959
.L933:
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZNK4node9Utf8Value8ToStringB5cxx11Ev
	jmp	.L956
.L935:
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	jmp	.L956
.L931:
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
.L956:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L953
	jmp	.L942
.L930:
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L945
	call	_ZdlPv@PLT
.L945:
	movq	-128(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L942
.L953:
	call	_ZdlPv@PLT
	jmp	.L942
.L934:
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L942:
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdx
	movq	%r12, %rdi
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L959:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L938
	call	_ZdlPv@PLT
.L938:
	movq	-160(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L924
	call	_ZdlPv@PLT
.L924:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L949
	call	__stack_chk_fail@PLT
.L949:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9610:
	.size	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L963
	call	__stack_chk_fail@PLT
.L963:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9193:
	.size	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_:
.LFB8671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L965
	call	_ZdlPv@PLT
.L965:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L967
	call	__stack_chk_fail@PLT
.L967:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8671:
	.size	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC96:
	.string	"    at [eval]:%i:%i\n"
.LC97:
	.string	"    at [eval] (%s:%i:%i)\n"
.LC98:
	.string	"    at %s:%i:%i\n"
.LC99:
	.string	"    at %s (%s:%i:%i)\n"
	.section	.text.unlikely
.LCOLDB100:
	.text
.LHOTB100:
	.p2align 4
	.globl	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE
	.type	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE, @function
_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE:
.LFB7858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2160(%rbp), %r15
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L978:
	movq	%r13, %rdi
	call	_ZNK2v810StackTrace13GetFrameCountEv@PLT
	cmpl	%r14d, %eax
	jle	.L970
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK2v810StackTrace8GetFrameEPNS_7IsolateEj@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v810StackFrame15GetFunctionNameEv@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZNK2v810StackFrame13GetScriptNameEv@PLT
	leaq	-1104(%rbp), %r10
	movq	%rbx, %rsi
	movq	%r10, %rdi
	movq	%rax, %rdx
	movq	%r10, -2184(%rbp)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZNK2v810StackFrame13GetLineNumberEv@PLT
	movq	%r12, %rdi
	movl	%eax, -2176(%rbp)
	call	_ZNK2v810StackFrame9GetColumnEv@PLT
	movq	%r12, %rdi
	movl	%eax, -2172(%rbp)
	call	_ZNK2v810StackFrame6IsEvalEv@PLT
	movq	-2184(%rbp), %r10
	testb	%al, %al
	je	.L971
	movq	%r12, %rdi
	call	_ZNK2v810StackFrame11GetScriptIdEv@PLT
	testl	%eax, %eax
	jne	.L972
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L970:
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1008
	addq	$2152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1008:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE.cold, @function
_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE.cold:
.LFSB7858:
.L972:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-1088(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-2176(%rbp), %rcx
	leaq	-2168(%rbp), %rdx
	leaq	-2172(%rbp), %r8
	leaq	.LC97(%rip), %rsi
	movq	%rax, -2168(%rbp)
	call	_ZN4node7FPrintFIJPcRKiS3_EEEvP8_IO_FILEPKcDpOT_
.L973:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L979
	testq	%rdi, %rdi
	je	.L979
	call	free@PLT
.L979:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L970
	testq	%rdi, %rdi
	je	.L970
	call	free@PLT
	jmp	.L970
.L971:
	cmpq	$0, -2160(%rbp)
	movq	stderr(%rip), %rdi
	je	.L1009
	leaq	-2172(%rbp), %r9
	leaq	-2176(%rbp), %r8
	movq	%r10, %rcx
	movq	%r15, %rdx
	leaq	.LC99(%rip), %rsi
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_RKiS4_EEEvP8_IO_FILEPKcDpOT_
.L975:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L976
	testq	%rdi, %rdi
	je	.L976
	call	free@PLT
.L976:
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L977
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L977
	call	free@PLT
.L977:
	addl	$1, %r14d
	jmp	.L978
.L1006:
	movq	stderr(%rip), %rdi
	leaq	-2172(%rbp), %rcx
	leaq	-2176(%rbp), %rdx
	leaq	.LC96(%rip), %rsi
	call	_ZN4node7FPrintFIJRKiS2_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L973
.L1009:
	leaq	-2176(%rbp), %rcx
	leaq	-2172(%rbp), %r8
	movq	%r10, %rdx
	leaq	.LC98(%rip), %rsi
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueERKiS4_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L975
	.cfi_endproc
.LFE7858:
	.text
	.size	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE, .-_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE
	.section	.text.unlikely
	.size	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE.cold, .-_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE.cold
.LCOLDE100:
	.text
.LHOTE100:
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L1011
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1011:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC87(%rip), %r12
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %r9
.L1012:
	movq	%r9, %rbx
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -192(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -184(%rbp)
	movb	%sil, -176(%rbp)
	call	strchr@PLT
	movb	-176(%rbp), %dl
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	-192(%rbp), %r8
	jne	.L1012
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %r14
	jg	.L1013
	cmpb	$99, %dl
	jg	.L1014
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -176(%rbp)
	je	.L1015
	cmpb	$88, %dl
	je	.L1016
	jmp	.L1013
.L1014:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1013
	leaq	.L1018(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1018:
	.long	.L1019-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1019-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1021-.L1018
	.long	.L1020-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1019-.L1018
	.long	.L1013-.L1018
	.long	.L1019-.L1018
	.long	.L1013-.L1018
	.long	.L1013-.L1018
	.long	.L1017-.L1018
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1015:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-184(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	jne	.L1047
	jmp	.L1045
.L1013:
	movq	%r8, %rdx
	movq	%r9, %rsi
	leaq	-128(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1045
.L1047:
	call	_ZdlPv@PLT
	jmp	.L1045
.L1019:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNK4node9Utf8Value8ToStringB5cxx11Ev
	jmp	.L1042
.L1021:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	jmp	.L1042
.L1017:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
.L1042:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L1039
	jmp	.L1028
.L1016:
	movq	%r8, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	movq	-184(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1031
	call	_ZdlPv@PLT
.L1031:
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L1028
.L1039:
	call	_ZdlPv@PLT
	jmp	.L1028
.L1020:
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1028:
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1045:
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1024
	call	_ZdlPv@PLT
.L1024:
	movq	-160(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L1010
	call	_ZdlPv@PLT
.L1010:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1035
	call	__stack_chk_fail@PLT
.L1035:
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9612:
	.size	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1049
	call	__stack_chk_fail@PLT
.L1049:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9195:
	.size	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_:
.LFB8674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRNS_9Utf8ValueEEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1053
	call	__stack_chk_fail@PLT
.L1053:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8674:
	.size	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L1056
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1056:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %r9
.L1057:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	-200(%rbp), %r8
	jne	.L1057
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L1058
	cmpb	$99, %dl
	jg	.L1059
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -184(%rbp)
	je	.L1060
	cmpb	$88, %dl
	je	.L1061
	jmp	.L1058
.L1059:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1058
	leaq	.L1063(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1063:
	.long	.L1064-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1064-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1066-.L1063
	.long	.L1065-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1064-.L1063
	.long	.L1058-.L1063
	.long	.L1064-.L1063
	.long	.L1058-.L1063
	.long	.L1058-.L1063
	.long	.L1062-.L1063
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1060:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -192(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L1092
	jmp	.L1090
.L1058:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	leaq	-128(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1090
.L1092:
	call	_ZdlPv@PLT
	jmp	.L1090
.L1064:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNK4node9Utf8Value8ToStringB5cxx11Ev
	jmp	.L1087
.L1066:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	jmp	.L1087
.L1062:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
.L1087:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L1084
	jmp	.L1073
.L1061:
	movq	%r8, %rsi
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	movq	-192(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1076
	call	_ZdlPv@PLT
.L1076:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1073
.L1084:
	call	_ZdlPv@PLT
	jmp	.L1073
.L1065:
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1073:
	movq	-168(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1090:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L1055
	call	_ZdlPv@PLT
.L1055:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1080
	call	__stack_chk_fail@PLT
.L1080:
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9613:
	.size	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1094
	call	__stack_chk_fail@PLT
.L1094:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9196:
	.size	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_:
.LFB8691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1096
	call	_ZdlPv@PLT
.L1096:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1098
	call	__stack_chk_fail@PLT
.L1098:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8691:
	.size	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r9
	testq	%rax, %rax
	jne	.L1101
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1101:
	movq	%rax, %r10
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	movq	%r9, -192(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r10, -168(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-192(%rbp), %r9
	movq	-168(%rbp), %r10
.L1102:
	movq	%r10, %rax
	movq	%r10, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L1102
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L1103
	cmpb	$99, %dl
	jg	.L1104
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r11
	movq	%rax, -192(%rbp)
	je	.L1105
	cmpb	$88, %dl
	je	.L1106
	jmp	.L1103
.L1104:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1103
	leaq	.L1108(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1108:
	.long	.L1109-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1109-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1111-.L1108
	.long	.L1110-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1109-.L1108
	.long	.L1103-.L1108
	.long	.L1109-.L1108
	.long	.L1103-.L1108
	.long	.L1103-.L1108
	.long	.L1107-.L1108
	.section	.text.unlikely._ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1105:
	movq	-168(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	-176(%rbp), %rcx
	movq	%r11, -200(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-200(%rbp), %r11
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r11, -168(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-168(%rbp), %r11
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	jne	.L1137
	jmp	.L1135
.L1103:
	movq	-176(%rbp), %rcx
	movq	%r15, %r8
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r15
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1135
.L1137:
	call	_ZdlPv@PLT
	jmp	.L1135
.L1109:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZNK4node9Utf8Value8ToStringB5cxx11Ev
	jmp	.L1132
.L1111:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	jmp	.L1132
.L1107:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
.L1132:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L1129
	jmp	.L1118
.L1106:
	movq	%r9, %rsi
	movq	%r11, %rdi
	movq	%r11, -200(%rbp)
	call	_ZN4node12ToBaseStringILj4ENS_9Utf8ValueEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_
	movq	-200(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1121
	call	_ZdlPv@PLT
.L1121:
	movq	-128(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1118
.L1129:
	call	_ZdlPv@PLT
	jmp	.L1118
.L1110:
	leaq	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1118:
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1135:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1114
	call	_ZdlPv@PLT
.L1114:
	movq	-160(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1100
	call	_ZdlPv@PLT
.L1100:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1125
	call	__stack_chk_fail@PLT
.L1125:
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9615:
	.size	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1139
	call	__stack_chk_fail@PLT
.L1139:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9198:
	.size	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_:
.LFB8694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRNS_9Utf8ValueES2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1141
	call	_ZdlPv@PLT
.L1141:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1143
	call	__stack_chk_fail@PLT
.L1143:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8694:
	.size	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_
	.section	.text._ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.type	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, @function
_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_:
.LFB9978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	subq	$64, %rsp
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -80(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1146
	testq	%r14, %r14
	je	.L1151
.L1146:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L1174
	cmpq	$1, %r13
	jne	.L1149
	movzbl	(%r14), %eax
	movb	%al, -64(%rbp)
	movq	%rbx, %rax
.L1150:
	movq	%r13, -72(%rbp)
	leaq	16(%r12), %rdi
	movb	$0, (%rax,%r13)
	movq	-80(%rbp), %r14
	movq	-72(%rbp), %r13
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1160
	testq	%r14, %r14
	je	.L1151
.L1160:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L1175
	cmpq	$1, %r13
	jne	.L1155
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L1156:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1145
	call	_ZdlPv@PLT
.L1145:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1176
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1149:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1177
	movq	%rbx, %rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1155:
	testq	%r13, %r13
	je	.L1156
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1174:
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
.L1148:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	-80(%rbp), %rax
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, 16(%r12)
.L1154:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L1156
.L1151:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1176:
	call	__stack_chk_fail@PLT
.L1177:
	movq	%rbx, %rdi
	jmp	.L1148
	.cfi_endproc
.LFE9978:
	.size	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, .-_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.section	.text._ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.type	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, @function
_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_:
.LFB9979:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	subq	$64, %rsp
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -80(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1179
	testq	%r14, %r14
	je	.L1184
.L1179:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L1207
	cmpq	$1, %r13
	jne	.L1182
	movzbl	(%r14), %eax
	movb	%al, -64(%rbp)
	movq	%rbx, %rax
.L1183:
	movq	%r13, -72(%rbp)
	leaq	16(%r12), %rdi
	movb	$0, (%rax,%r13)
	movq	-80(%rbp), %r14
	movq	-72(%rbp), %r13
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1193
	testq	%r14, %r14
	je	.L1184
.L1193:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L1208
	cmpq	$1, %r13
	jne	.L1188
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L1189:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1178
	call	_ZdlPv@PLT
.L1178:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1209
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1210
	movq	%rbx, %rax
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1188:
	testq	%r13, %r13
	je	.L1189
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1207:
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
.L1181:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	-80(%rbp), %rax
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, 16(%r12)
.L1187:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L1189
.L1184:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1209:
	call	__stack_chk_fail@PLT
.L1210:
	movq	%rbx, %rdi
	jmp	.L1181
	.cfi_endproc
.LFE9979:
	.size	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, .-_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_:
.LFB9611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L1212
	leaq	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1212:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %r9
.L1213:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	-200(%rbp), %r8
	jne	.L1213
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L1214
	cmpb	$99, %dl
	jg	.L1215
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r14
	movq	%rax, -184(%rbp)
	je	.L1216
	cmpb	$88, %dl
	je	.L1217
	jmp	.L1214
.L1215:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1214
	leaq	.L1219(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1219:
	.long	.L1220-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1220-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1222-.L1219
	.long	.L1221-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1220-.L1219
	.long	.L1214-.L1219
	.long	.L1220-.L1219
	.long	.L1214-.L1219
	.long	.L1214-.L1219
	.long	.L1218-.L1219
	.section	.text.unlikely._ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
.L1216:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L1248
	jmp	.L1246
.L1214:
	movq	%r8, %rdx
	movq	%r9, %rsi
	leaq	-128(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1246
.L1248:
	call	_ZdlPv@PLT
	jmp	.L1246
.L1220:
	movq	(%r8), %rsi
	movq	8(%r8), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	jmp	.L1243
.L1222:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	jmp	.L1243
.L1218:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
.L1243:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L1240
	jmp	.L1229
.L1217:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1232
	call	_ZdlPv@PLT
.L1232:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1229
.L1240:
	call	_ZdlPv@PLT
	jmp	.L1229
.L1221:
	leaq	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1229:
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1246:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1225
	call	_ZdlPv@PLT
.L1225:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L1211
	call	_ZdlPv@PLT
.L1211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1236
	call	__stack_chk_fail@PLT
.L1236:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9611:
	.size	_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	.type	_ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_, @function
_ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_:
.LFB9194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1250
	call	__stack_chk_fail@PLT
.L1250:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9194:
	.size	_ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_, .-_ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_:
.LFB8672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1252
	call	_ZdlPv@PLT
.L1252:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1254
	call	__stack_chk_fail@PLT
.L1254:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8672:
	.size	_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC101:
	.string	"%s\n"
	.text
	.p2align 4
	.globl	_ZN4node14PrintExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_5ValueEEENS3_INS0_7MessageEEE
	.type	_ZN4node14PrintExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_5ValueEEENS3_INS0_7MessageEEE, @function
_ZN4node14PrintExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_5ValueEEENS3_INS0_7MessageEEE:
.LFB7859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1104(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1136(%rbp), %rbx
	subq	$1112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value14ToDetailStringENS_5LocalINS_7ContextEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	leaq	-1137(%rbp), %r8
	movq	%rbx, %rdi
	movb	$0, -1137(%rbp)
	call	_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0
	movq	stderr(%rip), %rdi
	movq	%rbx, %rdx
	leaq	.LC101(%rip), %rsi
	call	_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	movq	stderr(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC101(%rip), %rsi
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_
	movq	%r13, %rdi
	call	_ZNK2v87Message13GetStackTraceEv@PLT
	testq	%rax, %rax
	je	.L1257
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE
.L1257:
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1258
	call	_ZdlPv@PLT
.L1258:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1256
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1256
	call	free@PLT
.L1256:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1271
	addq	$1112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1271:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7859:
	.size	_ZN4node14PrintExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_5ValueEEENS3_INS0_7MessageEEE, .-_ZN4node14PrintExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_5ValueEEENS3_INS0_7MessageEEE
	.p2align 4
	.globl	_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE
	.type	_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE, @function
_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE:
.LFB7860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1275
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	popq	%r12
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node14PrintExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_5ValueEEENS3_INS0_7MessageEEE
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore_state
	leaq	_ZZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.cfi_endproc
.LFE7860:
	.size	_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE, .-_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE
	.section	.rodata.str1.1
.LC102:
	.string	"\n%s"
	.text
	.p2align 4
	.globl	_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE
	.type	_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE, @function
_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE:
.LFB7861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1276
	movq	%rsi, %r12
	leaq	-128(%rbp), %r14
	movq	%rdi, %rbx
	movq	%rdx, %r13
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	je	.L1280
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1279
.L1280:
	xorl	%r12d, %r12d
.L1279:
	movq	3280(%rbx), %rdx
	leaq	-96(%rbp), %r15
	movq	%r13, %rcx
	movq	352(%rbx), %rsi
	leaq	-129(%rbp), %r8
	movq	%r15, %rdi
	movb	$0, -129(%rbp)
	call	_ZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPb.constprop.0
	cmpb	$0, -129(%rbp)
	jne	.L1309
.L1288:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1293
	call	_ZdlPv@PLT
.L1293:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1276:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1310
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1309:
	.cfi_restore_state
	movq	3280(%rbx), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L1311
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%r12, %r12
	je	.L1283
	testq	%rax, %rax
	je	.L1283
	cmpl	$1, -152(%rbp)
	je	.L1312
.L1286:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	80(%rax), %rdx
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L1294
.L1289:
	leaq	_ZZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L1311:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L1283:
	cmpb	$0, 1400(%rbx)
	jne	.L1288
	leaq	_ZN4node11per_processL9tty_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movb	$1, 1400(%rbx)
	call	_ZN4node10ResetStdioEv@PLT
	movq	stderr(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC102(%rip), %rsi
	call	_ZN4node7FPrintFIJRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	leaq	_ZN4node11per_processL9tty_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1294:
	shrw	$8, %ax
	jne	.L1288
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1312:
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNK2v85Value13IsNativeErrorEv@PLT
	movq	-152(%rbp), %rcx
	testb	%al, %al
	jne	.L1286
	jmp	.L1283
.L1310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7861:
	.size	_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE, .-_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE
	.section	.text.unlikely._ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_, @function
_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_:
.LFB9614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L1314
	leaq	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1314:
	movq	%rax, %r9
	leaq	-160(%rbp), %r13
	leaq	-144(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC87(%rip), %rbx
	movq	%r9, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-168(%rbp), %r9
.L1315:
	movq	%r9, %rax
	movq	%r9, -168(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -200(%rbp)
	movq	%r9, -192(%rbp)
	movb	%sil, -184(%rbp)
	call	strchr@PLT
	movb	-184(%rbp), %dl
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	-200(%rbp), %r8
	jne	.L1315
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L1316
	cmpb	$99, %dl
	jg	.L1317
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r14
	movq	%rax, -184(%rbp)
	je	.L1318
	cmpb	$88, %dl
	je	.L1319
	jmp	.L1316
.L1317:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1316
	leaq	.L1321(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1321:
	.long	.L1322-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1322-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1324-.L1321
	.long	.L1323-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1322-.L1321
	.long	.L1316-.L1321
	.long	.L1322-.L1321
	.long	.L1316-.L1321
	.long	.L1316-.L1321
	.long	.L1320-.L1321
	.section	.text.unlikely._ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_,comdat
.L1318:
	movq	-168(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L1350
	jmp	.L1348
.L1316:
	movq	%r8, %rdx
	movq	%r9, %rsi
	leaq	-128(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1348
.L1350:
	call	_ZdlPv@PLT
	jmp	.L1348
.L1322:
	movq	(%r8), %rsi
	movq	8(%r8), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	jmp	.L1345
.L1324:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj3ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	jmp	.L1345
.L1320:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
.L1345:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L1342
	jmp	.L1331
.L1319:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1334
	call	_ZdlPv@PLT
.L1334:
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1331
.L1342:
	call	_ZdlPv@PLT
	jmp	.L1331
.L1323:
	leaq	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1331:
	movq	-168(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1348:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1327
	call	_ZdlPv@PLT
.L1327:
	movq	-160(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L1313
	call	_ZdlPv@PLT
.L1313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1338
	call	__stack_chk_fail@PLT
.L1338:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9614:
	.size	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_, .-_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	.type	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_, @function
_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_:
.LFB9197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1352
	call	__stack_chk_fail@PLT
.L1352:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9197:
	.size	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_, .-_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_:
.LFB8692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEES6_PKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1354
	call	_ZdlPv@PLT
.L1354:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1356
	call	__stack_chk_fail@PLT
.L1356:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8692:
	.size	_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC103:
	.string	"%s\n%s\n"
.LC104:
	.string	"<toString() threw exception>"
.LC105:
	.string	"%s\n%s: %s\n"
.LC106:
	.string	"%s: %s\n"
.LC107:
	.string	"node"
.LC108:
	.string	".exe"
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"(Use `%s --trace-uncaught ...` to show where the exception was thrown)\n"
	.section	.rodata.str1.1
.LC110:
	.string	"Thrown at:\n"
	.section	.text.unlikely
.LCOLDB111:
	.text
.LHOTB111:
	.p2align 4
	.type	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE, @function
_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE:
.LFB7864:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r15
	movq	%rsi, %r12
	movq	%rdx, %r13
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	jne	.L1513
.L1359:
	movl	$1, %ebx
	testq	%r12, %r12
	je	.L1514
.L1361:
	testq	%r13, %r13
	je	.L1515
	movq	352(%r15), %r14
	leaq	-4304(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -4336(%rbp)
	movq	%r14, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movb	%al, -4328(%rbp)
	testb	%al, %al
	jne	.L1516
.L1363:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1517
	cmpl	$1, %ebx
	jne	.L1518
	movq	360(%r15), %rax
	movq	3280(%r15), %rsi
	movq	%r12, %rdi
	movq	1648(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	2080(%r15), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rbx
	call	_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE@PLT
.L1376:
	movq	360(%r15), %rax
	movq	3280(%r15), %rsi
	movq	%r12, %rdi
	movq	80(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1519
.L1366:
	leaq	-4272(%rbp), %rcx
	movq	352(%r15), %rsi
	movq	%rbx, %rdx
	movq	%rcx, %rdi
	movq	%rcx, -4344(%rbp)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -4272(%rbp)
	je	.L1378
	movq	(%rbx), %rax
	movq	-4344(%rbp), %rcx
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1379
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1520
.L1379:
	testq	%r14, %r14
	je	.L1382
	movq	(%r14), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1382
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1382
	cmpb	$0, -4328(%rbp)
	jne	.L1382
	movq	352(%r15), %rsi
	leaq	-1104(%rbp), %r12
	movq	%r14, %rdx
	movq	%rcx, -4328(%rbp)
	movq	%r12, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	movq	-4328(%rbp), %rcx
	leaq	.LC103(%rip), %rsi
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1386
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1386
	call	free@PLT
.L1386:
	movq	1648(%r15), %r12
	movq	1640(%r15), %rdx
	testq	%r12, %r12
	je	.L1419
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	leaq	8(%r12), %rax
	testq	%rbx, %rbx
	je	.L1420
	lock addl	$1, (%rax)
	movzbl	470(%rdx), %r14d
	testq	%rbx, %rbx
	je	.L1521
.L1429:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L1522
.L1423:
	testb	%r14b, %r14b
	jne	.L1523
.L1426:
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movq	-4256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1428
	leaq	-4248(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1428
	call	free@PLT
.L1428:
	movq	-4336(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1524
	addq	$4312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1518:
	.cfi_restore_state
	movq	2776(%r15), %rdi
	movq	%r12, -1104(%rbp)
	testq	%rdi, %rdi
	je	.L1370
	movq	3280(%r15), %rsi
	leaq	88(%r14), %rdx
	movl	$1, %ecx
	leaq	-1104(%rbp), %r8
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1370
.L1369:
	movq	2080(%r15), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE@PLT
	movq	2768(%r15), %rdi
	movq	%r12, -1104(%rbp)
	testq	%rdi, %rdi
	je	.L1376
	movq	3280(%r15), %rsi
	leaq	88(%r14), %rdx
	movl	$1, %ecx
	leaq	-1104(%rbp), %r8
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	cmovne	%rax, %rbx
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1520:
	cmpl	$5, 43(%rax)
	jne	.L1379
.L1378:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1525
.L1381:
	movq	352(%r15), %rsi
	leaq	-1104(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r14
	testq	%r14, %r14
	je	.L1391
	movq	-1104(%rbp), %rbx
	leaq	-2144(%rbp), %r8
	leaq	-2160(%rbp), %r12
	movq	%r8, -2160(%rbp)
	movq	%rbx, -4312(%rbp)
	cmpq	$15, %rbx
	jbe	.L1392
	movq	%r12, %rdi
	leaq	-4312(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -4328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-4328(%rbp), %r8
	movq	%rax, -2160(%rbp)
	movq	%rax, %rdi
	movq	-4312(%rbp), %rax
	movq	%rax, -2144(%rbp)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1513:
	movl	%ecx, %ebx
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L1359
	testq	%r12, %r12
	jne	.L1361
	.p2align 4,,10
	.p2align 3
.L1514:
	leaq	_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	2080(%r15), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	88(%r14), %rbx
	xorl	%r14d, %r14d
	call	_ZN4node9inspector5Agent23ReportUncaughtExceptionEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEE@PLT
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1523:
	movq	%r13, %rdi
	call	_ZNK2v87Message13GetStackTraceEv@PLT
	testq	%rax, %rax
	je	.L1426
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1420:
	addl	$1, 8(%r12)
	movzbl	470(%rdx), %r14d
	testq	%rbx, %rbx
	jne	.L1429
.L1521:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1423
.L1522:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1424
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1425:
	cmpl	$1, %eax
	jne	.L1423
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1419:
	movzbl	470(%rdx), %r14d
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1370:
	xorl	%ebx, %ebx
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	360(%r15), %rax
	movq	3280(%r15), %rsi
	movq	%r12, %rdi
	movq	104(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1526
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, -4328(%rbp)
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	360(%r15), %rax
	movq	3280(%r15), %rsi
	movq	%r12, %rdi
	movq	1024(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	3280(%r15), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	360(%r15), %rax
	movq	1056(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rbx, %rbx
	je	.L1381
	movq	(%rbx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1387
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1387
	cmpl	$5, 43(%rax)
	je	.L1381
.L1387:
	testq	%rdx, %rdx
	je	.L1381
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1388
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1388
	cmpl	$5, 43(%rax)
	je	.L1381
	.p2align 4,,10
	.p2align 3
.L1388:
	leaq	-3216(%rbp), %r10
	movq	352(%r15), %rsi
	leaq	-2160(%rbp), %r12
	movq	%r10, %rdi
	movq	%r10, -4344(%rbp)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	352(%r15), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	testq	%r14, %r14
	movq	-4344(%rbp), %r10
	je	.L1527
	movq	(%r14), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1389
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1389
	cmpb	$0, -4328(%rbp)
	jne	.L1389
	movq	352(%r15), %rsi
	leaq	-1104(%rbp), %rbx
	movq	%r14, %rdx
	movq	%r10, -4328(%rbp)
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-4328(%rbp), %r10
	movq	%r12, %r8
	movq	%rbx, %rdx
	movq	stderr(%rip), %rdi
	leaq	.LC105(%rip), %rsi
	movq	%r10, %rcx
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_S2_EEEvP8_IO_FILEPKcDpOT_
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1403
	testq	%rdi, %rdi
	je	.L1403
	call	free@PLT
.L1403:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1404
	testq	%rdi, %rdi
	je	.L1404
	call	free@PLT
.L1404:
	movq	-3200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1399
	leaq	-3192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1399
	call	free@PLT
.L1399:
	movq	1648(%r15), %r14
	movq	1640(%r15), %rdx
	testq	%r14, %r14
	je	.L1406
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	leaq	8(%r14), %rax
	testq	%rbx, %rbx
	je	.L1407
	lock addl	$1, (%rax)
.L1408:
	movzbl	470(%rdx), %ecx
	testq	%rbx, %rbx
	je	.L1528
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L1409:
	cmpl	$1, %eax
	je	.L1529
.L1410:
	testb	%cl, %cl
	jne	.L1386
	leaq	-3200(%rbp), %rbx
	movb	$0, -3200(%rbp)
	movq	1696(%r15), %rsi
	leaq	-3216(%rbp), %r10
	movq	%rbx, -3216(%rbp)
	movq	$0, -3208(%rbp)
	cmpq	%rsi, 1704(%r15)
	je	.L1415
	movq	%r10, %rdi
	movq	%r10, -4328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	cmpq	$0, -3208(%rbp)
	movq	-4328(%rbp), %r10
	je	.L1415
.L1414:
	leaq	.LC108(%rip), %rsi
	movq	%r12, %rdi
	movq	%r10, -4328(%rbp)
	leaq	-1104(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-4328(%rbp), %r10
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r10, %rsi
	call	_ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_@PLT
	movq	stderr(%rip), %rdi
	movq	%r14, %rdx
	leaq	.LC109(%rip), %rsi
	call	_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	movq	-1104(%rbp), %rdi
	leaq	-1088(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1416
	call	_ZdlPv@PLT
.L1416:
	movq	-2160(%rbp), %rdi
	leaq	-2144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1417
	call	_ZdlPv@PLT
.L1417:
	movq	-3216(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1386
	call	_ZdlPv@PLT
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1407:
	addl	$1, 8(%r14)
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1406:
	movzbl	470(%rdx), %ecx
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L1528:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1424:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	(%r14), %rax
	movb	%cl, -4328(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	movzbl	-4328(%rbp), %ecx
	je	.L1411
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1412:
	cmpl	$1, %eax
	jne	.L1410
	movq	(%r14), %rax
	movb	%cl, -4328(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movzbl	-4328(%rbp), %ecx
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1519:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1411:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1526:
	movb	$0, -4328(%rbp)
	jmp	.L1363
.L1524:
	call	__stack_chk_fail@PLT
.L1415:
	movq	%r10, %rdi
	leaq	.LC107(%rip), %rsi
	movq	%r10, -4328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6assignEPKc@PLT
	movq	-4328(%rbp), %r10
	jmp	.L1414
.L1527:
	jmp	.L1389
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE.cold, @function
_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE.cold:
.LFSB7864:
.L1391:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-2160(%rbp), %r12
	leaq	-2144(%rbp), %r8
	xorl	%edx, %edx
	movq	$28, -4312(%rbp)
	leaq	-4312(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r8, -2160(%rbp)
	movq	%r8, -4328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-4312(%rbp), %rdx
	leaq	.LC104(%rip), %rsi
	movl	$7, %ecx
	movq	%rax, -2160(%rbp)
	movq	%rax, %rdi
	movq	-4328(%rbp), %r8
	movq	%rdx, -2144(%rbp)
	rep movsl
	movq	-4312(%rbp), %rax
	movq	-2160(%rbp), %rdx
	movq	%rax, -2152(%rbp)
	movb	$0, (%rdx,%rax)
.L1396:
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	leaq	.LC101(%rip), %rsi
	movq	%r8, -4328(%rbp)
	call	_ZN4node7FPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvP8_IO_FILEPKcDpOT_
	movq	-2160(%rbp), %rdi
	movq	-4328(%rbp), %r8
	cmpq	%r8, %rdi
	je	.L1397
	call	_ZdlPv@PLT
.L1397:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1399
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1399
	call	free@PLT
	jmp	.L1399
.L1392:
	cmpq	$1, %rbx
	je	.L1530
	testq	%rbx, %rbx
	jne	.L1531
.L1395:
	movq	-4312(%rbp), %rax
	movq	-2160(%rbp), %rdx
	movq	%rax, -2152(%rbp)
	movb	$0, (%rdx,%rax)
	jmp	.L1396
.L1531:
	movq	%r8, %rdi
.L1393:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r8, -4328(%rbp)
	call	memcpy@PLT
	movq	-4328(%rbp), %r8
	jmp	.L1395
.L1389:
	movq	stderr(%rip), %rdi
	movq	%r12, %rcx
	movq	%r10, %rdx
	leaq	.LC106(%rip), %rsi
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueES2_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1403
.L1530:
	movzbl	(%r14), %eax
	movb	%al, -2144(%rbp)
	jmp	.L1395
.L1506:
	movq	stderr(%rip), %rdi
	leaq	.LC110(%rip), %rsi
	movq	%rax, -4328(%rbp)
	call	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_
	movq	-4328(%rbp), %rax
	movq	352(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE
	jmp	.L1426
.L1382:
	movq	stderr(%rip), %rdi
	movq	%rcx, %rdx
	leaq	.LC101(%rip), %rsi
	call	_ZN4node7FPrintFIJRNS_9Utf8ValueEEEEvP8_IO_FILEPKcDpOT_
	jmp	.L1386
	.cfi_endproc
.LFE7864:
	.text
	.size	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE, .-_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE
	.section	.text.unlikely
	.size	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE.cold, .-_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE.cold
.LCOLDE111:
	.text
.LHOTE111:
	.align 2
	.p2align 4
	.globl	_ZN4node6errors13TryCatchScopeD2Ev
	.type	_ZN4node6errors13TryCatchScopeD2Ev, @function
_ZN4node6errors13TryCatchScopeD2Ev:
.LFB7871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1539
.L1534:
	movq	%r12, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1540
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1539:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L1534
	cmpl	$1, 56(%r12)
	jne	.L1534
	movq	48(%r12), %rax
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88TryCatch11CanContinueEv@PLT
	xorl	$1, %eax
	movzbl	%al, %ecx
	testq	%r13, %r13
	je	.L1541
.L1536:
	movq	48(%r12), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE
	movq	48(%r12), %rdi
	movl	$7, %esi
	call	_ZN4node11Environment4ExitEi@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1534
.L1541:
	movq	48(%r12), %rax
	movq	%r14, %rsi
	movl	%ecx, -68(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v89Exception13CreateMessageEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movl	-68(%rbp), %ecx
	movq	%rax, %r13
	jmp	.L1536
.L1540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7871:
	.size	_ZN4node6errors13TryCatchScopeD2Ev, .-_ZN4node6errors13TryCatchScopeD2Ev
	.globl	_ZN4node6errors13TryCatchScopeD1Ev
	.set	_ZN4node6errors13TryCatchScopeD1Ev,_ZN4node6errors13TryCatchScopeD2Ev
	.section	.rodata.str1.1
.LC112:
	.string	"\n"
	.text
	.p2align 4
	.globl	_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE
	.type	_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE, @function
_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE:
.LFB7880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1568
.L1542:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1569
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1545
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1570
.L1545:
	movq	%r13, %rdi
	leaq	-128(%rbp), %r15
	call	_ZNK2v88TryCatch7MessageEv@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE
	movq	352(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	360(%rbx), %rax
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movq	3280(%rbx), %rsi
	movl	$0, -72(%rbp)
	movq	1648(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	360(%rbx), %rax
	movq	80(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1548
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1548
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1548
	testq	%r14, %r14
	je	.L1548
	movq	(%r14), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1548
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1548
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	leaq	.LC112(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1571
.L1550:
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	352(%rbx), %rdi
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	352(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	leaq	112(%rax), %rcx
	movq	360(%rbx), %rax
	movq	104(%rax), %rdx
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	104(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1545
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	je	.L1545
	jmp	.L1542
.L1571:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdx
	jmp	.L1550
.L1569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7880:
	.size	_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE, .-_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE
	.p2align 4
	.globl	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb
	.type	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb, @function
_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb:
.LFB7881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -188(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1606
	leaq	-176(%rbp), %r15
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	%rdi, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r13, %r13
	je	.L1607
.L1574:
	movq	%r12, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L1608
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1577
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	movq	-184(%rbp), %rsi
	cmpl	$35, %eax
	jbe	.L1577
	movq	(%rsi), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L1577
	movq	271(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1577
	movq	360(%rbx), %rax
	movq	2968(%rbx), %r12
	movq	3280(%rbx), %rsi
	movq	720(%rax), %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L1609
.L1578:
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-184(%rbp), %r10
	testb	%al, %al
	je	.L1610
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	jne	.L1611
.L1580:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1612
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1610:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	call	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE
	movl	$6, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment4ExitEi@PLT
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN4node14PrintExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_5ValueEEENS3_INS0_7MessageEEE
	call	_ZN4node5AbortEv
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	%r10, -208(%rbp)
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L1580
	leaq	-144(%rbp), %r11
	movq	352(%rbx), %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-184(%rbp), %r11
	xorl	%esi, %esi
	movq	%rbx, -96(%rbp)
	movl	$1, -88(%rbp)
	movq	%r11, %rdi
	movq	%r11, -200(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	movq	352(%rbx), %rax
	movq	-208(%rbp), %r10
	leaq	-80(%rbp), %r8
	movq	3280(%rbx), %rsi
	movl	$2, %ecx
	movq	%r14, -80(%rbp)
	leaq	112(%rax), %rdx
	addq	$120, %rax
	cmpb	$0, -188(%rbp)
	movq	%r10, %rdi
	cmovne	%rdx, %rax
	movq	%r12, %rdx
	movq	%rax, -72(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	-200(%rbp), %r11
	movq	%rax, -184(%rbp)
	movq	%r11, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev
	movq	-184(%rbp), %r8
	testq	%r8, %r8
	je	.L1580
	movq	%r8, %rdi
	call	_ZNK2v85Value7IsFalseEv@PLT
	testb	%al, %al
	je	.L1580
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE
	movq	%rbx, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	664(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1584
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L1613
.L1584:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment4ExitEi@PLT
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v89Exception13CreateMessageEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	%rax, %r13
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1606:
	leaq	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L1608:
	leaq	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L1609:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %r10
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	%r12, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN4node11Environment4ExitEi@PLT
	jmp	.L1580
.L1612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7881:
	.size	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb, .-_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb
	.p2align 4
	.globl	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE
	.type	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE, @function
_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE:
.LFB7883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88TryCatch9IsVerboseEv@PLT
	testb	%al, %al
	je	.L1621
.L1614:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1622
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1621:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L1623
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1624
	leaq	-64(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1623:
	leaq	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.p2align 4,,10
	.p2align 3
.L1624:
	leaq	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
.L1622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7883:
	.size	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE, .-_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE
	.section	.rodata.str1.1
.LC113:
	.string	" "
.LC114:
	.string	"V8"
	.text
	.p2align 4
	.globl	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE
	.type	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE, @function
_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE:
.LFB7874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1608, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v87Message10GetIsolateEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v87Message10ErrorLevelEv@PLT
	cmpl	$8, %eax
	je	.L1626
	cmpl	$16, %eax
	jne	.L1625
	movq	%r13, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L1625
	leaq	-1600(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1631
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L1631
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L1631
	movq	271(%rax), %rbx
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%rbx, %rbx
	je	.L1625
	movq	.LC115(%rip), %xmm1
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-1504(%rbp), %r15
	movhps	.LC116(%rip), %xmm1
	movaps	%xmm1, -1632(%rbp)
	call	_ZNK2v87Message15GetScriptOriginEv@PLT
	movq	-1600(%rbp), %rdx
	movq	%r13, %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	leaq	-1376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rdx
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%rdx, -1376(%rbp)
	pxor	%xmm0, %xmm0
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	movq	%rdx, %rcx
	movw	%ax, -1152(%rbp)
	movq	-24(%rdx), %rax
	movq	%rdx, -1504(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	$0, -1160(%rbp)
	movq	%rdx, -1504(%rbp,%rax)
	movq	$0, -1496(%rbp)
	addq	-24(%rcx), %r15
	movq	%r15, %rdi
	leaq	-1488(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	xorl	%esi, %esi
	movq	-24(%rdx), %rcx
	movq	%rdx, -1488(%rbp)
	addq	%r15, %rcx
	movq	%rcx, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-1632(%rbp), %xmm1
	movq	-24(%rcx), %rax
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, -1504(%rbp,%rax)
	leaq	80(%rcx), %rdx
	movq	%rcx, -1504(%rbp)
	leaq	-1424(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rdx, -1376(%rbp)
	movq	%rcx, -1632(%rbp)
	movaps	%xmm1, -1488(%rbp)
	movaps	%xmm0, -1472(%rbp)
	movaps	%xmm0, -1456(%rbp)
	movaps	%xmm0, -1440(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1608(%rbp), %rdi
	leaq	-1480(%rbp), %rsi
	movq	%rcx, -1480(%rbp)
	leaq	-1392(%rbp), %rcx
	movl	$24, -1416(%rbp)
	movq	%rcx, -1616(%rbp)
	movq	%rcx, -1408(%rbp)
	movq	$0, -1400(%rbp)
	movb	$0, -1392(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-1088(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1653
	movq	%rsi, %rdi
	movq	%rsi, -1648(%rbp)
	call	strlen@PLT
	movq	-1648(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1634:
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v87Message13GetLineNumberENS_5LocalINS_7ContextEEE@PLT
	movl	$-1, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testb	%al, %al
	cmovne	%edx, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC113(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	call	_ZNK2v87Message3GetEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-1600(%rbp), %r12
	testq	%r12, %r12
	je	.L1654
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1637:
	movq	-1440(%rbp), %rax
	leaq	-1520(%rbp), %r12
	movq	$0, -1528(%rbp)
	leaq	-1536(%rbp), %rdi
	movq	%r12, -1536(%rbp)
	movb	$0, -1520(%rbp)
	testq	%rax, %rax
	je	.L1638
	movq	-1456(%rbp), %r8
	movq	-1448(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1639
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1640:
	movq	-1536(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	leaq	.LC114(%rip), %rdx
	call	_ZN4node25ProcessEmitWarningGenericEPNS_11EnvironmentEPKcS3_S3_@PLT
	movq	-1536(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1641
	call	_ZdlPv@PLT
.L1641:
	movq	.LC115(%rip), %xmm0
	movq	%r14, %rdi
	movhps	.LC117(%rip), %xmm0
	movaps	%xmm0, -1648(%rbp)
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-1648(%rbp), %xmm0
	movq	-1408(%rbp), %rdi
	movq	%rax, -1504(%rbp)
	addq	$80, %rax
	movq	%rax, -1376(%rbp)
	movaps	%xmm0, -1488(%rbp)
	cmpq	-1616(%rbp), %rdi
	je	.L1642
	call	_ZdlPv@PLT
.L1642:
	movq	-1632(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1480(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	-1608(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rbx, -1504(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -1488(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -1488(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -1504(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -1504(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1376(%rbp)
	movq	$0, -1496(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1625
	testq	%rdi, %rdi
	je	.L1625
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L1625:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1655
	addq	$1608, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1626:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1631:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1639:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	-1488(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	-1488(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1638:
	leaq	-1408(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1640
.L1655:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7874:
	.size	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE, .-_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE
	.p2align 4
	.type	_ZN4node6errorsL24TriggerUncaughtExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node6errorsL24TriggerUncaughtExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L1667
	leaq	-80(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1660
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L1660
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L1660
	movq	271(%rax), %r15
.L1659:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1657:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1661
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L1662:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v89Exception13CreateMessageEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%r15, %r15
	je	.L1663
	movq	1640(%r15), %rax
	cmpb	$0, 8(%rax)
	jne	.L1672
.L1663:
	cmpl	$1, 16(%rbx)
	jle	.L1673
	movq	8(%rbx), %rdi
	subq	$8, %rdi
.L1665:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movzbl	%al, %ecx
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1674
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1673:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	8(%rbx), %r13
	jmp	.L1662
	.p2align 4,,10
	.p2align 3
.L1660:
	xorl	%r15d, %r15d
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1667:
	xorl	%r15d, %r15d
	jmp	.L1657
.L1674:
	call	__stack_chk_fail@PLT
.L1672:
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionE
	call	_ZN4node5AbortEv
	.cfi_endproc
.LFE7878:
	.size	_ZN4node6errorsL24TriggerUncaughtExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node6errorsL24TriggerUncaughtExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE, @function
_GLOBAL__sub_I__ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE:
.LFB10536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_processL9tty_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1678
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node11per_processL9tty_mutexE(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
.L1678:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE
	.cfi_endproc
.LFE10536:
	.size	_GLOBAL__sub_I__ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE, .-_GLOBAL__sub_I__ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node20IsExceptionDecoratedEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEE
	.weak	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC118:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC120:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.weak	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC121:
	.string	"../src/debug_utils-inl.h:76"
.LC122:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRKiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC120
	.weak	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC123:
	.string	"../src/debug_utils-inl.h:113"
.LC124:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.weak	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC125
	.weak	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {const char*&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC126
	.weak	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC126
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = node::Utf8Value&; Args = {node::Utf8Value&, node::Utf8Value&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC127
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC127
	.weak	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = std::__cxx11::basic_string<char>; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC128
	.weak	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC128
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = node::Utf8Value&; Args = {node::Utf8Value&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC129
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC129
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = node::Utf8Value&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC130
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC130
	.weak	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = std::__cxx11::basic_string<char>&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC131
	.weak	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJEEES6_PKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC131
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = node::Utf8Value&; Args = {node::Utf8Value&, const int&, const int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC132
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJS2_RKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC132
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = node::Utf8Value&; Args = {const int&, const int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC133
	.weak	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRNS_9Utf8ValueEJRKiS4_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC133
	.weak	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = char*; Args = {const int&, const int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC134
	.weak	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPcJRKiS3_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC134
	.weak	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const int&; Args = {const int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC135
	.weak	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRKiJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC135
	.weak	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = int&; Args = {const char*}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC136
	.weak	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRiJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC136
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC137
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC137
	.weak	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {int&, const char*}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC138
	.weak	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJRiS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC138
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC139:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC140:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC141:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.section	.rodata.str1.1
.LC142:
	.string	"../src/node_errors.cc"
.LC143:
	.string	"errors"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC142
	.quad	0
	.quad	_ZN4node6errors10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC143
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC144:
	.string	"../src/node_errors.cc:992"
.LC145:
	.string	"try_catch.HasCaught()"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"void node::errors::TriggerUncaughtException(v8::Isolate*, const v8::TryCatch&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args_0, @object
	.size	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args_0, 24
_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args_0:
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.section	.rodata.str1.1
.LC147:
	.string	"../src/node_errors.cc:991"
.LC148:
	.string	"!try_catch.HasTerminated()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args, @object
	.size	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args, 24
_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchEE4args:
	.quad	.LC147
	.quad	.LC148
	.quad	.LC146
	.section	.rodata.str1.1
.LC149:
	.string	"../src/node_errors.cc:895"
.LC150:
	.string	"isolate->InContext()"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"void node::errors::TriggerUncaughtException(v8::Isolate*, v8::Local<v8::Value>, v8::Local<v8::Message>, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args_0, @object
	.size	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args_0, 24
_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args_0:
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.section	.rodata.str1.1
.LC152:
	.string	"../src/node_errors.cc:890"
.LC153:
	.string	"!error.IsEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args, @object
	.size	_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args, 24
_ZZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEbE4args:
	.quad	.LC152
	.quad	.LC153
	.quad	.LC151
	.section	.rodata.str1.1
.LC154:
	.string	"../src/node_errors.cc:808"
.LC155:
	.string	"args[1]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC156:
	.string	"void node::errors::SetEnhanceStackForFatalException(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.section	.rodata.str1.1
.LC157:
	.string	"../src/node_errors.cc:807"
.LC158:
	.string	"args[0]->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node6errorsL32SetEnhanceStackForFatalExceptionERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC157
	.quad	.LC158
	.quad	.LC156
	.section	.rodata.str1.1
.LC159:
	.string	"../src/node_errors.cc:800"
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"void node::errors::SetPrepareStackTraceCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node6errors28SetPrepareStackTraceCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC159
	.quad	.LC158
	.quad	.LC160
	.section	.rodata.str1.1
.LC161:
	.string	"../src/node_errors.cc:275"
.LC162:
	.string	"!message.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"void node::ReportFatalException(node::Environment*, v8::Local<v8::Value>, v8::Local<v8::Message>, node::EnhanceFatalException)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args_0, @object
	.size	_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args_0, 24
_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args_0:
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.section	.rodata.str1.1
.LC164:
	.string	"../src/node_errors.cc:274"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args, @object
	.size	_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args, 24
_ZZN4nodeL20ReportFatalExceptionEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_21EnhanceFatalExceptionEE4args:
	.quad	.LC164
	.quad	.LC153
	.quad	.LC163
	.section	.rodata.str1.1
.LC165:
	.string	"../src/node_errors.cc:228"
	.section	.rodata.str1.8
	.align 8
.LC166:
	.string	"err_obj ->SetPrivate(env->context(), env->arrow_message_private_symbol(), arrow_str.ToLocalChecked()) .FromMaybe(false)"
	.align 8
.LC167:
	.string	"void node::AppendExceptionLine(node::Environment*, v8::Local<v8::Value>, v8::Local<v8::Message>, node::ErrorHandlingMode)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeEE4args, @object
	.size	_ZZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeEE4args, 24
_ZZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeEE4args:
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.section	.rodata.str1.1
.LC168:
	.string	"../src/node_errors.cc:188"
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"void node::PrintCaughtException(v8::Isolate*, v8::Local<v8::Context>, const v8::TryCatch&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchEE4args, @object
	.size	_ZZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchEE4args, 24
_ZZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchEE4args:
	.quad	.LC168
	.quad	.LC145
	.quad	.LC169
	.section	.rodata.str1.1
.LC170:
	.string	"../src/node_errors.cc:126"
.LC171:
	.string	"(off) <= (kUnderlineBufsize)"
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"std::string node::GetErrorSource(v8::Isolate*, v8::Local<v8::Context>, v8::Local<v8::Message>, bool*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_3, @object
	.size	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_3, 24
_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_3:
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.section	.rodata.str1.1
.LC173:
	.string	"../src/node_errors.cc:106"
.LC174:
	.string	"(buf.size()) > (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_0, @object
	.size	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_0, 24
_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args_0:
	.quad	.LC173
	.quad	.LC174
	.quad	.LC172
	.section	.rodata.str1.1
.LC175:
	.string	"../src/node_errors.cc:97"
.LC176:
	.string	"(end) >= (start)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args, @object
	.size	_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args, 24
_ZZN4nodeL14GetErrorSourceEPN2v87IsolateENS0_5LocalINS0_7ContextEEENS3_INS0_7MessageEEEPbE4args:
	.quad	.LC175
	.quad	.LC176
	.quad	.LC172
	.local	_ZN4node11per_processL9tty_mutexE
	.comm	_ZN4node11per_processL9tty_mutexE,40,32
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC177:
	.string	"../src/debug_utils-inl.h:67"
.LC178:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC179:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC177
	.quad	.LC178
	.quad	.LC179
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC115:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC116:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC117:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
