	.file	"stream_base.cc"
	.text
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZN4node22CustomBufferJSListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node22CustomBufferJSListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22CustomBufferJSListener15OnStreamDestroyEv
	.type	_ZN4node22CustomBufferJSListener15OnStreamDestroyEv, @function
_ZN4node22CustomBufferJSListener15OnStreamDestroyEv:
.LFB6040:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE6040:
	.size	_ZN4node22CustomBufferJSListener15OnStreamDestroyEv, .-_ZN4node22CustomBufferJSListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7138:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7138:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase9IsIPCPipeEv
	.type	_ZN4node10StreamBase9IsIPCPipeEv, @function
_ZN4node10StreamBase9IsIPCPipeEv:
.LFB7777:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7777:
	.size	_ZN4node10StreamBase9IsIPCPipeEv, .-_ZN4node10StreamBase9IsIPCPipeEv
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase5GetFDEv
	.type	_ZN4node10StreamBase5GetFDEv, @function
_ZN4node10StreamBase5GetFDEv:
.LFB7778:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE7778:
	.size	_ZN4node10StreamBase5GetFDEv, .-_ZN4node10StreamBase5GetFDEv
	.align 2
	.p2align 4
	.globl	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.type	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm, @function
_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm:
.LFB7787:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7787:
	.size	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm, .-_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.align 2
	.p2align 4
	.globl	_ZNK4node14StreamResource5ErrorEv
	.type	_ZNK4node14StreamResource5ErrorEv, @function
_ZNK4node14StreamResource5ErrorEv:
.LFB7788:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7788:
	.size	_ZNK4node14StreamResource5ErrorEv, .-_ZNK4node14StreamResource5ErrorEv
	.align 2
	.p2align 4
	.globl	_ZN4node14StreamResource10ClearErrorEv
	.type	_ZN4node14StreamResource10ClearErrorEv, @function
_ZN4node14StreamResource10ClearErrorEv:
.LFB7789:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7789:
	.size	_ZN4node14StreamResource10ClearErrorEv, .-_ZN4node14StreamResource10ClearErrorEv
	.align 2
	.p2align 4
	.globl	_ZN4node22CustomBufferJSListener13OnStreamAllocEm
	.type	_ZN4node22CustomBufferJSListener13OnStreamAllocEm, @function
_ZN4node22CustomBufferJSListener13OnStreamAllocEm:
.LFB7792:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	ret
	.cfi_endproc
.LFE7792:
	.size	_ZN4node22CustomBufferJSListener13OnStreamAllocEm, .-_ZN4node22CustomBufferJSListener13OnStreamAllocEm
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9941:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9941:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9942:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9942:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9944:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE9944:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9945:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9945:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9946:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9946:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9948:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE9948:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7573:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L24
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7573:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7574:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L30:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7574:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev:
.LFB9908:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	addq	$16, %rdi
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9908:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB9910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9910:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB8571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rbx
	movq	48(%rbx), %rdi
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %edx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L40
	cmpw	$1040, %dx
	je	.L40
	leaq	48(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v86Object20SlowGetInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L42
.L38:
	movq	(%rax), %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	39(%rdi), %r12
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	jne	.L38
.L42:
	movq	24(%rbx), %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8571:
	.size	_ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE,"axG",@progbits,_ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE
	.type	_ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE, @function
_ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE:
.LFB8572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L46
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	popq	%rbx
	movl	$2, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$48, %rdi
	jmp	_ZN2v86Object16SetInternalFieldEiNS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8572:
	.size	_ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE, .-_ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE
	.section	.text._ZN4node22CustomBufferJSListenerD0Ev,"axG",@progbits,_ZN4node22CustomBufferJSListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22CustomBufferJSListenerD0Ev
	.type	_ZN4node22CustomBufferJSListenerD0Ev, @function
_ZN4node22CustomBufferJSListenerD0Ev:
.LFB9898:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L48
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L49
	cmpq	%rax, %rdi
	jne	.L51
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	%rax, %rdi
	je	.L67
.L51:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L63
.L49:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
.L48:
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	movq	16(%rdi), %rax
	movl	$40, %esi
	movq	%rax, 16(%rdx)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9898:
	.size	_ZN4node22CustomBufferJSListenerD0Ev, .-_ZN4node22CustomBufferJSListenerD0Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9995:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9995:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9996:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE9996:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9997:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9997:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9998:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE9998:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev:
.LFB9999:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9999:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB10001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10001:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9943:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE9943:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB10000:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE10000:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9947:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB10003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10003:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$96, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L90
	cmpw	$1040, %cx
	jne	.L86
.L90:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L92
.L89:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	32(%rbx), %rsi
	leaq	40(%r12), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	%rdx, 32(%r12)
	movl	$39, %ecx
	movq	%r13, %rdx
	movq	%rax, 24(%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L89
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7601:
	.size	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$72, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L98
	cmpw	$1040, %cx
	jne	.L94
.L98:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L100
.L97:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	32(%rbx), %rsi
	leaq	16(%r12), %rdi
	movsd	.LC1(%rip), %xmm0
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movq	%r13, %rdx
	movl	$26, %ecx
	movq	%rax, (%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L97
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7599:
	.size	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase9GetObjectEv
	.type	_ZN4node10StreamBase9GetObjectEv, @function
_ZN4node10StreamBase9GetObjectEv:
.LFB7779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*128(%rax)
	movq	%rax, %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L102
	movzbl	11(%rax), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L107
.L102:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movq	16(%rdx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7779:
	.size	_ZN4node10StreamBase9GetObjectEv, .-_ZN4node10StreamBase9GetObjectEv
	.section	.text._ZN4node22CustomBufferJSListenerD2Ev,"axG",@progbits,_ZN4node22CustomBufferJSListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22CustomBufferJSListenerD2Ev
	.type	_ZN4node22CustomBufferJSListenerD2Ev, @function
_ZN4node22CustomBufferJSListenerD2Ev:
.LFB9896:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L108
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L110
	cmpq	%rax, %rdi
	jne	.L112
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L124:
	cmpq	%rax, %rdi
	je	.L128
.L112:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L124
.L110:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movq	16(%rdi), %rax
	movq	%rax, 16(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
	ret
	.cfi_endproc
.LFE9896:
	.size	_ZN4node22CustomBufferJSListenerD2Ev, .-_ZN4node22CustomBufferJSListenerD2Ev
	.weak	_ZN4node22CustomBufferJSListenerD1Ev
	.set	_ZN4node22CustomBufferJSListenerD1Ev,_ZN4node22CustomBufferJSListenerD2Ev
	.section	.text._ZN4node22EmitToJSStreamListenerD2Ev,"axG",@progbits,_ZN4node22EmitToJSStreamListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22EmitToJSStreamListenerD2Ev
	.type	_ZN4node22EmitToJSStreamListenerD2Ev, @function
_ZN4node22EmitToJSStreamListenerD2Ev:
.LFB9900:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L129
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L131
	cmpq	%rax, %rdi
	jne	.L133
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L145:
	cmpq	%rax, %rdi
	je	.L149
.L133:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L145
.L131:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	movq	16(%rdi), %rax
	movq	%rax, 16(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
	ret
	.cfi_endproc
.LFE9900:
	.size	_ZN4node22EmitToJSStreamListenerD2Ev, .-_ZN4node22EmitToJSStreamListenerD2Ev
	.weak	_ZN4node22EmitToJSStreamListenerD1Ev
	.set	_ZN4node22EmitToJSStreamListenerD1Ev,_ZN4node22EmitToJSStreamListenerD2Ev
	.section	.text._ZN4node22EmitToJSStreamListenerD0Ev,"axG",@progbits,_ZN4node22EmitToJSStreamListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22EmitToJSStreamListenerD0Ev
	.type	_ZN4node22EmitToJSStreamListenerD0Ev, @function
_ZN4node22EmitToJSStreamListenerD0Ev:
.LFB9902:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L151
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L152
	cmpq	%rax, %rdi
	jne	.L154
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L166:
	cmpq	%rax, %rdi
	je	.L170
.L154:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L166
.L152:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
.L151:
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	movq	16(%rdi), %rax
	movl	$24, %esi
	movq	%rax, 16(%rdx)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9902:
	.size	_ZN4node22EmitToJSStreamListenerD0Ev, .-_ZN4node22EmitToJSStreamListenerD0Ev
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB8557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L183
	cmpw	$1040, %cx
	jne	.L172
.L183:
	cmpq	$0, 23(%rdx)
	je	.L171
.L174:
	movq	31(%rdx), %r12
.L179:
	testq	%r12, %r12
	je	.L171
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L191
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*128(%rax)
	movq	16(%rax), %r13
	movsd	40(%rax), %xmm0
	movq	1216(%r13), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L192
.L181:
	movq	1256(%r13), %rax
	movq	(%rbx), %rbx
	movq	%r12, %rdi
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movq	(%r12), %rax
	movsd	%xmm1, -40(%rbp)
	call	*24(%rax)
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	movq	1256(%r13), %rax
	movsd	%xmm1, 24(%rax)
.L171:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L181
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L191:
	movabsq	$-94489280512, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L171
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L174
	cmpw	$1040, %cx
	je	.L174
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L179
	.cfi_endproc
.LFE8557:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev:
.LFB10002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r13
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L193
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L197
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10002:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB10004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-40(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r14
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L199
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L205
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L199:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$96, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10004:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev:
.LFB9904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rbx), %r12
	movq	32(%rbx), %r13
	movq	%rax, (%rbx)
	call	uv_buf_init@PLT
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%r12, %r12
	je	.L206
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L210
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9904:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB9906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	movq	%rax, (%r12)
	call	uv_buf_init@PLT
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	testq	%r13, %r13
	je	.L212
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L218
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L212:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9906:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node22EmitToJSStreamListener13OnStreamAllocEm
	.type	_ZN4node22EmitToJSStreamListener13OnStreamAllocEm, @function
_ZN4node22EmitToJSStreamListener13OnStreamAllocEm:
.LFB7790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L227
	movq	32(%rax), %r13
	movq	%rsi, %rbx
	movq	360(%r13), %rax
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L228
	movl	%ebx, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r15
	movq	%rdx, %r14
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rdx, %rbx
	movq	%rax, %r12
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r12, %r12
	je	.L222
	movq	360(%r13), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L222:
	addq	$40, %rsp
	movq	%r15, %rax
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	leaq	_ZZN4node22EmitToJSStreamListener13OnStreamAllocEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7790:
	.size	_ZN4node22EmitToJSStreamListener13OnStreamAllocEm, .-_ZN4node22EmitToJSStreamListener13OnStreamAllocEm
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L230:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L240
.L231:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L232
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L231
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7136:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase11GetExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase11GetExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase11GetExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L254
	cmpw	$1040, %cx
	jne	.L242
.L254:
	cmpq	$0, 23(%rdx)
	je	.L241
.L244:
	movq	31(%rdx), %rsi
.L249:
	testq	%rsi, %rsi
	je	.L241
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v88External3NewEPNS_7IsolateEPv@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L259
	movq	(%rax), %rax
.L253:
	movq	%rax, 24(%rdx)
.L241:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	leaq	8(%r12), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L241
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L244
	cmpw	$1040, %cx
	je	.L244
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rsi
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L259:
	movq	16(%rdx), %rax
	jmp	.L253
	.cfi_endproc
.LFE7785:
	.size	_ZN4node10StreamBase11GetExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase11GetExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase12GetBytesReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase12GetBytesReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase12GetBytesReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L274
	cmpw	$1040, %cx
	jne	.L261
.L274:
	cmpq	$0, 23(%rdx)
	je	.L265
.L263:
	movq	31(%rdx), %rax
.L268:
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L264
	movq	16(%rax), %rax
	testq	%rax, %rax
	js	.L271
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L272:
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L279
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L260:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%rbx), %rbx
.L264:
	movq	$0, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	leaq	8(%r12), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L265
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L263
	cmpw	$1040, %cx
	je	.L263
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L279:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L260
	.cfi_endproc
.LFE7783:
	.size	_ZN4node10StreamBase12GetBytesReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase12GetBytesReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase15GetBytesWrittenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase15GetBytesWrittenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase15GetBytesWrittenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L294
	cmpw	$1040, %cx
	jne	.L281
.L294:
	cmpq	$0, 23(%rdx)
	je	.L285
.L283:
	movq	31(%rdx), %rax
.L288:
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L284
	movq	24(%rax), %rax
	testq	%rax, %rax
	js	.L291
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L292:
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L299
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L280:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L285:
	movq	(%rbx), %rbx
.L284:
	movq	$0, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	leaq	8(%r12), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L285
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L283
	cmpw	$1040, %cx
	je	.L283
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L299:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L280
	.cfi_endproc
.LFE7784:
	.size	_ZN4node10StreamBase15GetBytesWrittenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase15GetBytesWrittenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L314
	cmpw	$1040, %cx
	jne	.L301
.L314:
	cmpq	$0, 23(%rdx)
	je	.L319
.L303:
	movq	31(%rdx), %r12
.L308:
	testq	%r12, %r12
	je	.L319
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L319
	movq	(%r12), %rax
	leaq	_ZN4node10StreamBase5GetFDEv(%rip), %rcx
	movq	(%rbx), %rbx
	movq	104(%rax), %rdx
	movabsq	$-4294967296, %rax
	cmpq	%rcx, %rdx
	jne	.L320
.L312:
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movabsq	$-94489280512, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rdx
	salq	$32, %rax
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	8(%r12), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L319
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L303
	cmpw	$1040, %cx
	je	.L303
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L308
	.cfi_endproc
.LFE7782:
	.size	_ZN4node10StreamBase5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB8556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L333
	cmpw	$1040, %cx
	jne	.L322
.L333:
	cmpq	$0, 23(%rdx)
	je	.L321
.L324:
	movq	31(%rdx), %r12
.L329:
	testq	%r12, %r12
	je	.L321
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L341
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*128(%rax)
	movq	16(%rax), %r13
	movsd	40(%rax), %xmm0
	movq	1216(%r13), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L342
.L331:
	movq	1256(%r13), %rax
	movq	(%rbx), %rbx
	movq	%r12, %rdi
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movq	(%r12), %rax
	movsd	%xmm1, -40(%rbp)
	call	*16(%rax)
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	movq	1256(%r13), %rax
	movsd	%xmm1, 24(%rax)
.L321:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L331
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L341:
	movabsq	$-94489280512, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L321
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L324
	cmpw	$1040, %cx
	je	.L324
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L329
	.cfi_endproc
.LFE8556:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase11ReadStartJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase11ReadStartJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase11ReadStartJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7767:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.cfi_endproc
.LFE7767:
	.size	_ZN4node10StreamBase11ReadStartJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase11ReadStartJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase10ReadStopJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase10ReadStopJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase10ReadStopJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7768:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE7768:
	.size	_ZN4node10StreamBase10ReadStopJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase10ReadStopJSERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L346
	movq	(%rsi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L354
.L348:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L355
	movq	8(%rbx), %rdi
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r13
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L351
.L356:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L352:
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	movl	$40, %edi
	movq	%rdx, %rbx
	movq	%rax, %r13
	call	_Znwm@PLT
	movq	8(%r12), %rdx
	leaq	16+_ZTVN4node22CustomBufferJSListenerE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r13, 24(%rax)
	movq	%rbx, 32(%rax)
	movq	%rdx, 16(%rax)
	movq	%r12, 8(%rax)
	movq	%rax, 8(%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	8(%rsi), %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	jne	.L348
.L354:
	leaq	_ZZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L355:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r13
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L356
.L351:
	movq	8(%rbx), %rdi
	jmp	.L352
	.cfi_endproc
.LFE7769:
	.size	_ZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB8559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L369
	cmpw	$1040, %cx
	jne	.L358
.L369:
	cmpq	$0, 23(%rdx)
	je	.L357
.L360:
	movq	31(%rdx), %r13
.L365:
	testq	%r13, %r13
	je	.L357
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L377
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L378
.L367:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L357:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L367
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L377:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L357
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L360
	cmpw	$1040, %cx
	je	.L360
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L365
	.cfi_endproc
.LFE8559:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase14SetWriteResultERKNS_17StreamWriteResultE
	.type	_ZN4node10StreamBase14SetWriteResultERKNS_17StreamWriteResultE, @function
_ZN4node10StreamBase14SetWriteResultERKNS_17StreamWriteResultE:
.LFB7771:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	16(%rsi), %rdx
	movq	1848(%rax), %rax
	movl	%edx, 8(%rax)
	movzbl	(%rsi), %edx
	movl	%edx, 12(%rax)
	ret
	.cfi_endproc
.LFE7771:
	.size	_ZN4node10StreamBase14SetWriteResultERKNS_17StreamWriteResultE, .-_ZN4node10StreamBase14SetWriteResultERKNS_17StreamWriteResultE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE
	.type	_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE, @function
_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE:
.LFB7775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	32(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	1848(%rcx), %rax
	movl	%esi, (%rax)
	movl	%r8d, 4(%rax)
	testq	%rdx, %rdx
	je	.L391
.L381:
	movq	(%rdi), %rax
	movq	%rdx, -32(%rbp)
	call	*128(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L392
	movq	8(%rax), %rdi
	movzbl	11(%rdi), %eax
	movq	(%rdi), %r8
	andl	$7, %eax
	cmpb	$2, %al
	je	.L393
.L383:
	movq	-1(%r8), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %edx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L389
	cmpw	$1040, %dx
	je	.L389
	movl	$2, %esi
	call	_ZN2v86Object20SlowGetInternalFieldEi@PLT
	movq	%rax, %r13
.L386:
	movq	%r13, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L394
	movq	%r13, %rsi
	leaq	-32(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L395
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movq	39(%r8), %r13
	movq	%r8, %rdi
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L391:
	movq	352(%rcx), %rdx
	addq	$88, %rdx
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L393:
	movq	16(%r12), %rax
	movq	%r8, %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %r8
	movq	%rax, %rdi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7775:
	.size	_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE, .-_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE
	.align 2
	.p2align 4
	.globl	_ZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_t
	.type	_ZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_t, @function
_ZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_t:
.LFB7791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L414
	movq	32(%r12), %rbx
	movq	%rsi, %r13
	leaq	-112(%rbp), %r14
	movq	%rdx, %r15
	movq	%r14, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	(%r15), %r10
	movq	8(%r15), %r15
	testq	%r13, %r13
	jle	.L415
	cmpq	%r15, %r13
	ja	.L416
	testq	%r13, %r13
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rdx
	cmovne	%r13, %rcx
	movq	%r10, %rsi
	call	_ZN4node11Environment10ReallocateEPcmm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L417
	movl	%r13d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -136(%rbp)
	movq	%rdi, %r15
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-136(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rdx, %rdi
	movq	%r15, %rdx
	movq	%rax, -72(%rbp)
	movq	%rdi, -64(%rbp)
	movq	%r8, %rsi
	movq	%rdi, -144(%rbp)
	movq	352(%rbx), %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-128(%rbp), %r10
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r10, %r10
	je	.L403
	movq	360(%rbx), %rax
	movq	-144(%rbp), %rdx
	movq	%r10, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L403:
	movq	-120(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	jne	.L419
.L399:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r10, -128(%rbp)
	call	uv_buf_init@PLT
	movq	-128(%rbp), %r10
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r10, %r10
	je	.L403
	movq	360(%rbx), %rax
	movq	%r15, %rdx
	movq	%r10, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L419:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE
	movq	-128(%rbp), %r10
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L417:
	leaq	_ZZN4node15AllocatedBuffer6ResizeEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7791:
	.size	_ZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_t, .-_ZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_t
	.align 2
	.p2align 4
	.globl	_ZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_t
	.type	_ZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_t, @function
_ZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_t:
.LFB7793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L440
	movq	32(%r9), %r13
	movq	%rsi, %r12
	leaq	-80(%rbp), %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	movq	%r9, -88(%rbp)
	movq	%rdx, %r15
	movq	352(%r13), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r13), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpq	$-4095, %r12
	movq	(%r15), %rax
	movq	-88(%rbp), %r9
	je	.L441
.L423:
	cmpq	24(%rbx), %rax
	jne	.L442
	movq	%r12, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	call	_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L426
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L443
.L428:
	movq	%r12, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r12, %rdi
	movq	%rax, 24(%rbx)
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, 32(%rbx)
.L426:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L420:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L444
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L428
	cmpl	$5, 43(%rax)
	je	.L426
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L441:
	testq	%rax, %rax
	jne	.L423
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$-4095, %rsi
	call	_ZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksE
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L440:
	leaq	_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7793:
	.size	_ZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_t, .-_ZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_t
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase9AddMethodEPNS_11EnvironmentEN2v85LocalINS3_9SignatureEEENS3_17PropertyAttributeENS4_INS3_16FunctionTemplateEEEPFvRKNS3_20FunctionCallbackInfoINS3_5ValueEEEENS4_INS3_6StringEEE
	.type	_ZN4node10StreamBase9AddMethodEPNS_11EnvironmentEN2v85LocalINS3_9SignatureEEENS3_17PropertyAttributeENS4_INS3_16FunctionTemplateEEEPFvRKNS3_20FunctionCallbackInfoINS3_5ValueEEEENS4_INS3_6StringEEE, @function
_ZN4node10StreamBase9AddMethodEPNS_11EnvironmentEN2v85LocalINS3_9SignatureEEENS3_17PropertyAttributeENS4_INS3_16FunctionTemplateEEEPFvRKNS3_20FunctionCallbackInfoINS3_5ValueEEEENS4_INS3_6StringEEE:
.LFB7780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%r8, %rsi
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	movq	%r10, %rcx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%r9, %r12
	xorl	%r9d, %r9d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	movq	2680(%rdi), %rdx
	movq	352(%rdi), %rdi
	subq	$8, %rsp
	pushq	$1
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movl	%ebx, %r8d
	movq	%r12, %rsi
	xorl	%r9d, %r9d
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	xorl	%ecx, %ecx
	leaq	-32(%rbp), %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	.cfi_endproc
.LFE7780:
	.size	_ZN4node10StreamBase9AddMethodEPNS_11EnvironmentEN2v85LocalINS3_9SignatureEEENS3_17PropertyAttributeENS4_INS3_16FunctionTemplateEEEPFvRKNS3_20FunctionCallbackInfoINS3_5ValueEEEENS4_INS3_6StringEEE, .-_ZN4node10StreamBase9AddMethodEPNS_11EnvironmentEN2v85LocalINS3_9SignatureEEENS3_17PropertyAttributeENS4_INS3_16FunctionTemplateEEEPFvRKNS3_20FunctionCallbackInfoINS3_5ValueEEEENS4_INS3_6StringEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"readStart"
.LC4:
	.string	"readStop"
.LC5:
	.string	"shutdown"
.LC6:
	.string	"useUserBuffer"
.LC7:
	.string	"writev"
.LC8:
	.string	"writeBuffer"
.LC9:
	.string	"writeAsciiString"
.LC10:
	.string	"writeUtf8String"
.LC11:
	.string	"writeUcs2String"
.LC12:
	.string	"writeLatin1String"
.LC13:
	.string	"isStreamBase"
.LC14:
	.string	"onread"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE
	.type	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE, @function
_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE:
.LFB7781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rax, %r13
	movq	360(%rbx), %rax
	movq	2680(%rbx), %rdx
	leaq	_ZN4node10StreamBase5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	movq	%r13, %rcx
	movq	728(%rax), %r15
	pushq	$1
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-88(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movl	$7, %r8d
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	2680(%rbx), %rdx
	movq	360(%rbx), %rax
	leaq	_ZN4node10StreamBase11GetExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	movq	704(%rax), %r15
	movl	$1, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-88(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movl	$7, %r8d
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	2680(%rbx), %rdx
	movq	360(%rbx), %rax
	leaq	_ZN4node10StreamBase12GetBytesReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	movq	248(%rax), %r15
	movl	$1, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-88(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movl	$7, %r8d
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	2680(%rbx), %rdx
	movq	360(%rbx), %rax
	leaq	_ZN4node10StreamBase15GetBytesWrittenERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	movq	256(%rax), %r15
	movl	$1, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movl	$7, %r8d
	movq	%r15, %rsi
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11ReadStartJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	popq	%rdx
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L462
.L448:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_10ReadStopJSERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L463
.L449:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L464
.L450:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_13UseUserBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L465
.L451:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L466
.L452:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r13
	popq	%rax
	testq	%r13, %r13
	je	.L467
.L453:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L468
.L454:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L469
.L455:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L470
.L456:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L471
.L457:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	352(%rbx), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%rax, %r13
	leaq	112(%rdi), %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L472
.L458:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	352(%rbx), %rdi
	leaq	.LC14(%rip), %rsi
	xorl	%edx, %edx
	movl	$6, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L473
.L459:
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	_ZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEE(%rip), %rcx
	pushq	$0
	leaq	_ZN4node10BaseObject16InternalFieldGetILi2EEEvN2v85LocalINS2_6StringEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rdx
	movq	%r12, %rdi
	pushq	$0
	pushq	$0
	call	_ZN2v814ObjectTemplate11SetAccessorENS_5LocalINS_6StringEEEPFvS3_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS3_NS1_IS5_EERKNS4_IvEEESB_NS_13AccessControlENS_17PropertyAttributeENS1_INS_17AccessorSignatureEEENS_14SideEffectTypeESL_@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L474
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L463:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L464:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L465:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L466:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L467:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L468:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L469:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L470:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L471:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L459
.L474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7781:
	.size	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE, .-_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE
	.align 2
	.p2align 4
	.globl	_ZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEi
	.type	_ZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEi, @function
_ZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEi:
.LFB7794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -120(%rbp)
	movq	8(%r8), %r12
	movq	32(%r12), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*16(%rax)
	movq	352(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	%rax, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%r13), %r13
	movl	-120(%rbp), %r9d
	testq	%r13, %r13
	je	.L511
	movzbl	11(%r13), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L512
.L477:
	movq	352(%rbx), %rdi
	movl	%r9d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	leaq	_ZN4node10StreamBase9GetObjectEv(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	(%r12), %rax
	movq	136(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L478
	call	*128(%rax)
	movq	%rax, %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L480
	movzbl	11(%rax), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L513
.L480:
	movq	%rax, -72(%rbp)
	movq	352(%rbx), %rax
	leaq	_ZNK4node14StreamResource5ErrorEv(%rip), %rdx
	addq	$88, %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L514
.L482:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	1136(%rax), %rdx
	call	_ZN2v86Object3HasENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L515
.L485:
	shrw	$8, %ax
	jne	.L516
.L486:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L517
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	movq	360(%rbx), %rax
	movq	1136(%rax), %r12
	movq	-128(%rbp), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	testq	%rdi, %rdi
	je	.L487
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L518
.L487:
	movq	3280(%rdx), %rsi
	movq	%r12, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L486
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L486
	movq	-128(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%r12, %rsi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L512:
	movq	-128(%rbp), %rax
	movq	0(%r13), %rsi
	movl	%r9d, -120(%rbp)
	movq	16(%rax), %rax
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movl	-120(%rbp), %r9d
	movq	%rax, %r13
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L513:
	movq	16(%rdx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L518:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	-128(%rbp), %rax
	movq	16(%rax), %rdx
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L482
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L519
.L484:
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	leaq	_ZN4node14StreamResource10ClearErrorEv(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	je	.L482
	movq	%r12, %rdi
	call	*%rax
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L478:
	call	*%rdx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	_ZZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L515:
	movl	%eax, -120(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-120(%rbp), %eax
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rax
	jmp	.L484
.L517:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7794:
	.size	_ZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEi, .-_ZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEi
	.align 2
	.p2align 4
	.globl	_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7796:
	.cfi_startproc
	endbr64
	jmp	_ZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEi
	.cfi_endproc
.LFE7796:
	.size	_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.align 2
	.p2align 4
	.globl	_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7797:
	.cfi_startproc
	endbr64
	jmp	_ZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEi
	.cfi_endproc
.LFE7797:
	.size	_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L539
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L540
.L525:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L529
.L522:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L530
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L526:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L525
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L531
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L527:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L529:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L522
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L531:
	xorl	%edx, %edx
	jmp	.L527
	.cfi_endproc
.LFE8495:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L542
	leaq	16(%r12), %rsi
.L543:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L544
	addq	$16, %r12
.L545:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L546
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L562
.L546:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L563
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L550
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L564
.L541:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L565
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L564:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L563:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L553
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L548:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L550:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L553:
	xorl	%edx, %edx
	jmp	.L548
.L565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7604:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L567
	leaq	40(%r12), %rsi
.L568:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L569
	addq	$40, %r12
.L570:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L571
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L587
.L571:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L588
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L575
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L589
.L566:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L590
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L569:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L567:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L589:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L578
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L573:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L575:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L578:
	xorl	%edx, %edx
	jmp	.L573
.L590:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7606:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movl	16(%rsi), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L592
	movq	(%rsi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L642
.L594:
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L643
	movq	8(%rbx), %r13
.L596:
	movq	32(%r12), %r14
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	movq	352(%r14), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r13, %r13
	je	.L644
.L597:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	je	.L600
	comisd	.LC2(%rip), %xmm0
	jb	.L645
.L600:
	movq	1256(%rbx), %rax
	leaq	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movq	(%r12), %rax
	movq	112(%rax), %rax
	movsd	%xmm1, -112(%rbp)
	cmpq	%rdx, %rax
	jne	.L601
	movl	$72, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r15
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r15)
	movq	-1(%rdx), %rax
	movq	%r12, 8(%r15)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L622
	cmpw	$1040, %cx
	jne	.L602
.L622:
	movq	31(%rdx), %rax
.L604:
	testq	%rax, %rax
	jne	.L646
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	leaq	16(%r15), %rdi
	movq	%r13, %rdx
	movq	%rax, (%r15)
	movq	32(%r12), %rsi
	movl	$26, %ecx
	movsd	.LC1(%rip), %xmm0
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r15)
	addq	$72, %rax
	movq	%rax, 16(%r15)
.L606:
	movq	(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	movl	%eax, -116(%rbp)
	testl	%eax, %eax
	je	.L607
	testq	%r15, %r15
	je	.L607
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	leaq	-88(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L608
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L647
.L608:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-88(%rbp), %r8
	movq	24(%r8), %rdx
	testq	%rdx, %rdx
	je	.L648
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L618
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L649
	.p2align 4,,10
	.p2align 3
.L607:
	movq	(%r12), %rax
	leaq	_ZNK4node14StreamResource5ErrorEv(%rip), %rdx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L650
.L613:
	movq	1256(%rbx), %rax
	movsd	-112(%rbp), %xmm2
	movsd	%xmm2, 24(%rax)
.L617:
	movq	-104(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L651
	movl	-116(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movq	8(%rsi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L594
.L642:
	leaq	_ZZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L643:
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L647:
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L601:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r15
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L644:
	movq	3232(%r14), %rdi
	movq	3280(%r14), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L652
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L613
	movq	352(%r14), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L653
.L615:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r13, %rdi
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L654
.L616:
	movq	(%r12), %rax
	leaq	_ZN4node14StreamResource10ClearErrorEv(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	je	.L613
	movq	%r12, %rdi
	call	*%rax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L646:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L649:
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L648:
	movl	$24, %edi
	movq	%r8, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r8
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r8), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L621
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L610:
	movb	%dl, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%rax, 24(%r8)
.L618:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L654:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L615
.L621:
	xorl	%edx, %edx
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$-16, -116(%rbp)
	jmp	.L617
.L651:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7770:
	.size	_ZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB8558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L667
	cmpw	$1040, %cx
	jne	.L656
.L667:
	cmpq	$0, 23(%rdx)
	je	.L655
.L658:
	movq	31(%rdx), %r13
.L663:
	testq	%r13, %r13
	je	.L655
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L675
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L676
.L665:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L655:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L665
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L675:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L655
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L658
	cmpw	$1040, %cx
	je	.L658
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L663
	.cfi_endproc
.LFE8558:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_8ShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.section	.text._ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	.type	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE, @function
_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE:
.LFB7597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -104(%rbp)
	movq	32(%rsi), %r14
	movq	%rcx, -112(%rbp)
	movq	%r8, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -120(%rbp)
	testq	%rcx, %rcx
	je	.L711
	leaq	-1(%rcx), %rdi
	movq	%rdx, %rsi
	cmpq	$4, %rdi
	jbe	.L712
	movq	%rdx, %rax
	movq	%rdi, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rdx
	salq	$5, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L681:
	movdqu	8(%rax), %xmm0
	movdqu	24(%rax), %xmm2
	addq	$32, %rax
	punpcklqdq	%xmm2, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rax, %rdx
	jne	.L681
	movdqa	%xmm1, %xmm0
	movq	%rdi, %rax
	psrldq	$8, %xmm0
	andq	$-2, %rax
	paddq	%xmm0, %xmm1
	movq	%xmm1, %rbx
.L679:
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	8(%rsi,%rdx), %rbx
	leaq	1(%rax), %rdx
	cmpq	%rcx, %rdx
	jnb	.L678
	salq	$4, %rdx
	addq	8(%rsi,%rdx), %rbx
	leaq	2(%rax), %rdx
	cmpq	%rcx, %rdx
	jnb	.L678
	salq	$4, %rdx
	addq	8(%rsi,%rdx), %rbx
	leaq	3(%rax), %rdx
	cmpq	%rcx, %rdx
	jnb	.L678
	salq	$4, %rdx
	addq	$4, %rax
	addq	8(%rsi,%rdx), %rbx
	cmpq	%rcx, %rax
	jnb	.L678
	salq	$4, %rax
	addq	8(%rsi,%rax), %rbx
.L678:
	addq	%rbx, 24(%r13)
	cmpq	$0, -128(%rbp)
	je	.L730
.L683:
	leaq	-80(%rbp), %rax
	movq	352(%r14), %rsi
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r15, %r15
	je	.L731
.L687:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movsd	40(%rax), %xmm0
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	movq	1216(%rax), %rax
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	je	.L690
	comisd	.LC2(%rip), %xmm0
	jb	.L732
.L690:
	movq	-136(%rbp), %rax
	leaq	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movq	1256(%rax), %rax
	movsd	24(%rax), %xmm3
	movsd	%xmm0, 24(%rax)
	movq	0(%r13), %rax
	movq	120(%rax), %rax
	movsd	%xmm3, -152(%rbp)
	cmpq	%rdx, %rax
	jne	.L691
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r15)
	movq	-120(%rbp), %rax
	movq	%r13, 8(%r15)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L714
	cmpw	$1040, %cx
	jne	.L692
.L714:
	movq	31(%rdx), %rax
.L694:
	testq	%rax, %rax
	jne	.L733
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r15)
	movq	$0, 16(%r15)
	call	uv_buf_init@PLT
	movq	32(%r13), %rsi
	leaq	40(%r15), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	%rax, 24(%r15)
	movl	$39, %ecx
	movq	%rdx, 32(%r15)
	movq	-120(%rbp), %rdx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r15)
	addq	$72, %rax
	movq	%rax, 40(%r15)
.L696:
	movq	0(%r13), %rax
	movq	-128(%rbp), %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
	call	*48(%rax)
	testl	%eax, %eax
	movl	%eax, -128(%rbp)
	sete	-153(%rbp)
	jne	.L734
.L697:
	movq	0(%r13), %rax
	leaq	_ZNK4node14StreamResource5ErrorEv(%rip), %rdx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L735
.L703:
	movzbl	-153(%rbp), %eax
	movsd	-152(%rbp), %xmm4
	movq	%r15, 8(%r12)
	movq	%rbx, 16(%r12)
	movb	%al, (%r12)
	movl	-128(%rbp), %eax
	movl	%eax, 4(%r12)
	movq	-136(%rbp), %rax
	movq	1256(%rax), %rax
	movsd	%xmm4, 24(%rax)
.L707:
	movq	-144(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L677:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L736
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	movq	0(%r13), %rax
	leaq	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L737
.L684:
	testq	%rcx, %rcx
	jne	.L683
	xorl	%eax, %eax
.L685:
	movb	$0, (%r12)
	movl	%eax, 4(%r12)
	movq	$0, 8(%r12)
	movq	%rbx, 16(%r12)
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L691:
	movq	-120(%rbp), %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r15
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L734:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	leaq	-88(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L698
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L738
.L698:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-88(%rbp), %r8
	movq	24(%r8), %rdx
	testq	%rdx, %rdx
	je	.L739
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L708
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L701
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L701:
	xorl	%r15d, %r15d
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L731:
	movq	3264(%r14), %rdi
	movq	3280(%r14), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L740
	movq	%rax, %r15
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L711:
	xorl	%ebx, %ebx
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L737:
	leaq	-112(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L685
	movq	-112(%rbp), %rcx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L735:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L703
	movq	352(%r14), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L741
.L705:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	-120(%rbp), %rdi
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L742
.L706:
	movq	0(%r13), %rax
	leaq	_ZN4node14StreamResource10ClearErrorEv(%rip), %rdx
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	je	.L703
	movq	%r13, %rdi
	call	*%rax
	jmp	.L703
.L712:
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	jmp	.L679
.L740:
	movb	$0, (%r12)
	movl	$-16, 4(%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	jmp	.L707
.L732:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L738:
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L698
.L692:
	movq	-120(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L694
.L733:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L739:
	movl	$24, %edi
	movq	%r8, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %r8
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r8), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L713
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L700:
	movb	%dl, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%rax, 24(%r8)
.L708:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L742:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L706
.L741:
	movq	%rax, -168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-168(%rbp), %rcx
	jmp	.L705
.L713:
	xorl	%edx, %edx
	jmp	.L700
.L736:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7597:
	.size	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE, .-_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	.section	.text._ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL0:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL0
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	movq	%rsi, %r13
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L796
	cmpw	$1040, %cx
	jne	.L744
.L796:
	movq	23(%rdx), %r14
.L746:
	movl	16(%r13), %eax
	testl	%eax, %eax
	jg	.L747
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L830
.L749:
	movl	16(%r13), %eax
	cmpl	$1, %eax
	jle	.L831
	movq	8(%r13), %rcx
	movq	-8(%rcx), %rdx
	leaq	-8(%rcx), %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L832
.L752:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L831:
	movq	0(%r13), %rdx
	movq	8(%rdx), %r12
	movq	88(%r12), %rdx
	addq	$88, %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L752
.L832:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L752
	testl	%eax, %eax
	jg	.L754
	movq	0(%r13), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	movq	%r15, %rdi
.L757:
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	$0, -16536(%rbp)
	testb	%al, %al
	je	.L758
	cmpl	$2, 16(%r13)
	jg	.L759
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -16536(%rbp)
.L758:
	movq	352(%r14), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movq	%rdx, %r13
	testb	%al, %al
	je	.L760
	cmpq	$2147483647, %rdx
	ja	.L794
	cmpq	$16384, %rdx
	ja	.L826
	movq	(%rbx), %rax
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L764
.L767:
	leaq	-16448(%rbp), %r10
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	352(%r14), %rdi
	movq	%r10, %rsi
	movq	%r13, %rdx
	movq	%r10, -16552(%rbp)
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	-16552(%rbp), %r10
	movl	%eax, %esi
	movq	%rax, -16544(%rbp)
	movq	%r10, %rdi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rcx
	movq	$1, -16480(%rbp)
	movq	%rax, -16528(%rbp)
	leaq	-16528(%rbp), %rax
	movq	%rax, -16512(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L765
	subq	-16520(%rbp), %rcx
	addq	%rcx, 24(%rbx)
	movq	%rcx, -16560(%rbp)
.L766:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16520(%rbp), %r12
	movq	%rax, -16504(%rbp)
	movq	%rax, -16568(%rbp)
	movq	360(%r14), %rax
	movq	%r12, %rsi
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L771
	movl	%r12d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16544(%rbp)
	movq	%rax, %r12
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16568(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	je	.L773
.L776:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L747:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L749
.L830:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L754:
	movq	8(%r13), %r15
	cmpl	$2, %eax
	jle	.L833
	leaq	-16(%r15), %rdi
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L764:
	movq	%rbx, %rdi
	call	*%rax
	cmpb	$1, %al
	jne	.L767
	cmpq	$0, -16536(%rbp)
	je	.L767
.L826:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r13, %rsi
	movq	%rax, -16504(%rbp)
	movq	%rax, -16560(%rbp)
	movq	360(%r14), %rax
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L771
	movl	%r13d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	movq	%rax, -16544(%rbp)
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16560(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	jne	.L776
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16560(%rbp)
	call	uv_buf_init@PLT
	movq	-16560(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L777
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L777:
	movq	-16568(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	352(%r14), %rdi
	movq	%r13, %rdx
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	$0, -16560(%rbp)
	movq	%rax, %rsi
.L775:
	cmpq	%r13, %rsi
	ja	.L834
	movq	-16544(%rbp), %rdi
	call	uv_buf_init@PLT
	movq	%rax, -16528(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L779
.L781:
	xorl	%r8d, %r8d
.L780:
	leaq	-16528(%rbp), %rdx
	movq	%r15, %r9
	movl	$1, %ecx
	movq	%rbx, %rsi
	leaq	-16480(%rbp), %rdi
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%rbx), %rax
	movq	-16472(%rbp), %r12
	movq	1848(%rax), %rdx
	movq	-16560(%rbp), %rax
	addq	-16464(%rbp), %rax
	movl	%eax, 8(%rdx)
	movzbl	-16480(%rbp), %eax
	movl	%eax, 12(%rdx)
	testq	%r12, %r12
	je	.L788
	cmpq	$0, 24(%r12)
	jne	.L835
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r14, 16(%r12)
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rbx
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	movq	%rbx, 24(%r12)
	movq	-16552(%rbp), %rbx
	movq	%rax, -16544(%rbp)
	movq	%rbx, 32(%r12)
	movq	%rdx, -16552(%rbp)
.L788:
	movl	-16476(%rbp), %r12d
.L786:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rsi
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	testq	%rsi, %rsi
	je	.L743
	movq	360(%r14), %rax
	movq	-16552(%rbp), %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L743:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L836
	addq	$16536, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	.cfi_restore_state
	movq	8(%r13), %rax
	subq	$16, %rax
	movq	%rax, -16536(%rbp)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L744:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L773:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	call	uv_buf_init@PLT
	movq	-16568(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L774
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L774:
	movq	-16528(%rbp), %rsi
	movq	-16520(%rbp), %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	-16520(%rbp), %rsi
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L760:
	xorl	%r12d, %r12d
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%rbx, %rdi
	call	*%rax
	movq	-16536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L781
	testb	%al, %al
	je	.L781
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L837
	movq	-16536(%rbp), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L797
	cmpw	$1040, %cx
	jne	.L783
.L797:
	movq	23(%rdx), %rax
.L785:
	testq	%rax, %rax
	je	.L795
	movq	80(%rax), %r8
	movq	360(%r14), %rax
	movq	%r15, %rdi
	movq	3280(%r14), %rsi
	movq	-16536(%rbp), %rcx
	movq	824(%rax), %rdx
	movq	%r8, -16568(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-16568(%rbp), %r8
	testb	%al, %al
	jne	.L780
	movq	%r8, -16536(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-16536(%rbp), %r8
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L771:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L765:
	movq	%rcx, -16544(%rbp)
	leaq	-16480(%rbp), %rdx
	leaq	-16512(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	-16544(%rbp), %rcx
	movl	%eax, %r12d
	movq	-16480(%rbp), %rax
	testq	%rax, %rax
	je	.L769
	movq	%rcx, %rsi
	subq	-16520(%rbp), %rsi
	addq	%rsi, 24(%rbx)
	movq	%rsi, -16560(%rbp)
	testl	%r12d, %r12d
	jne	.L790
	cmpq	$1, %rax
	je	.L766
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L769:
	addq	%rcx, 24(%rbx)
.L790:
	movq	32(%rbx), %rax
	movq	1848(%rax), %rax
	movl	%ecx, 8(%rax)
	movl	$0, 12(%rax)
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L834:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L795:
	movl	$-22, %r12d
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L835:
	leaq	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L837:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L783:
	movq	-16536(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L785
.L794:
	movl	$-105, %r12d
	jmp	.L743
.L836:
	call	__stack_chk_fail@PLT
.L833:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L757
	.cfi_endproc
.LFE8537:
	.size	_ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_:
.LFB8562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L850
	cmpw	$1040, %cx
	jne	.L839
.L850:
	cmpq	$0, 23(%rdx)
	je	.L838
.L841:
	movq	31(%rdx), %r13
.L846:
	testq	%r13, %r13
	je	.L838
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L858
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L859
.L848:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L838:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L848
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L858:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L838
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L841
	cmpw	$1040, %cx
	je	.L841
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L846
	.cfi_endproc
.LFE8562:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.section	.text._ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL1:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL1
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	movq	%rsi, %r13
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L917
	cmpw	$1040, %cx
	jne	.L861
.L917:
	movq	23(%rdx), %r14
.L863:
	movl	16(%r13), %eax
	testl	%eax, %eax
	jg	.L864
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L953
.L866:
	movl	16(%r13), %eax
	cmpl	$1, %eax
	jle	.L954
	movq	8(%r13), %rcx
	movq	-8(%rcx), %rdx
	leaq	-8(%rcx), %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L955
.L869:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L954:
	movq	0(%r13), %rdx
	movq	8(%rdx), %r12
	movq	88(%r12), %rdx
	addq	$88, %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L869
.L955:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L869
	testl	%eax, %eax
	jg	.L871
	movq	0(%r13), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	movq	%r15, %rdi
.L874:
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	$0, -16536(%rbp)
	testb	%al, %al
	je	.L875
	cmpl	$2, 16(%r13)
	jg	.L876
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -16536(%rbp)
.L875:
	movq	%r12, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	cmpl	$65535, %eax
	jle	.L880
	movq	352(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	testb	%al, %al
	je	.L948
.L880:
	movq	352(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movq	%rdx, %r13
	testb	%al, %al
	je	.L948
	cmpq	$2147483647, %rdx
	ja	.L915
	cmpq	$16384, %rdx
	ja	.L949
	movq	(%rbx), %rax
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L885
.L888:
	leaq	-16448(%rbp), %r10
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	352(%r14), %rdi
	movq	%r10, %rsi
	movl	$1, %r8d
	movq	%r10, -16552(%rbp)
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	-16552(%rbp), %r10
	movl	%eax, %esi
	movq	%rax, -16544(%rbp)
	movq	%r10, %rdi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rcx
	movq	$1, -16480(%rbp)
	movq	%rax, -16528(%rbp)
	leaq	-16528(%rbp), %rax
	movq	%rax, -16512(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L886
	subq	-16520(%rbp), %rcx
	addq	%rcx, 24(%rbx)
	movq	%rcx, -16560(%rbp)
.L887:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16520(%rbp), %r12
	movq	%rax, -16504(%rbp)
	movq	%rax, -16568(%rbp)
	movq	360(%r14), %rax
	movq	%r12, %rsi
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L892
	movl	%r12d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16544(%rbp)
	movq	%rax, %r12
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16568(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	jne	.L897
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	call	uv_buf_init@PLT
	movq	-16568(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L895
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L895:
	movq	-16528(%rbp), %rsi
	movq	-16520(%rbp), %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	-16520(%rbp), %rsi
.L896:
	cmpq	%r13, %rsi
	ja	.L956
	movq	-16544(%rbp), %rdi
	call	uv_buf_init@PLT
	movq	%rax, -16528(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L900
.L902:
	xorl	%r8d, %r8d
.L901:
	leaq	-16528(%rbp), %rdx
	movq	%r15, %r9
	movl	$1, %ecx
	movq	%rbx, %rsi
	leaq	-16480(%rbp), %rdi
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%rbx), %rax
	movq	-16472(%rbp), %r12
	movq	1848(%rax), %rdx
	movq	-16560(%rbp), %rax
	addq	-16464(%rbp), %rax
	movl	%eax, 8(%rdx)
	movzbl	-16480(%rbp), %eax
	movl	%eax, 12(%rdx)
	testq	%r12, %r12
	je	.L909
	cmpq	$0, 24(%r12)
	jne	.L957
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r14, 16(%r12)
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rbx
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	movq	%rbx, 24(%r12)
	movq	-16552(%rbp), %rbx
	movq	%rax, -16544(%rbp)
	movq	%rbx, 32(%r12)
	movq	%rdx, -16552(%rbp)
.L909:
	movl	-16476(%rbp), %r12d
.L907:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rsi
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	testq	%rsi, %rsi
	je	.L860
	movq	360(%r14), %rax
	movq	-16552(%rbp), %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L864:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L866
.L953:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L871:
	movq	8(%r13), %r15
	cmpl	$2, %eax
	jle	.L958
	leaq	-16(%r15), %rdi
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L885:
	movq	%rbx, %rdi
	call	*%rax
	cmpq	$0, -16536(%rbp)
	je	.L888
	cmpb	$1, %al
	jne	.L888
.L949:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r13, %rsi
	movq	%rax, -16504(%rbp)
	movq	%rax, -16560(%rbp)
	movq	360(%r14), %rax
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L892
	movl	%r13d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	movq	%rax, -16544(%rbp)
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16560(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	je	.L959
.L897:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L948:
	xorl	%r12d, %r12d
.L860:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L960
	addq	$16536, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	movq	8(%r13), %rax
	subq	$16, %rax
	movq	%rax, -16536(%rbp)
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L861:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L959:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16560(%rbp)
	call	uv_buf_init@PLT
	movq	-16560(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L898
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L898:
	movq	-16568(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	352(%r14), %rdi
	movl	$1, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	$0, -16560(%rbp)
	movq	%rax, %rsi
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L892:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L900:
	movq	%rbx, %rdi
	call	*%rax
	movq	-16536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L902
	testb	%al, %al
	je	.L902
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L961
	movq	-16536(%rbp), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L918
	cmpw	$1040, %cx
	jne	.L904
.L918:
	movq	23(%rdx), %rax
.L906:
	testq	%rax, %rax
	je	.L916
	movq	80(%rax), %r8
	movq	360(%r14), %rax
	movq	%r15, %rdi
	movq	3280(%r14), %rsi
	movq	-16536(%rbp), %rcx
	movq	824(%rax), %rdx
	movq	%r8, -16568(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-16568(%rbp), %r8
	testb	%al, %al
	jne	.L901
	movq	%r8, -16536(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-16536(%rbp), %r8
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L886:
	movq	%rcx, -16544(%rbp)
	leaq	-16480(%rbp), %rdx
	leaq	-16512(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	-16544(%rbp), %rcx
	movl	%eax, %r12d
	movq	-16480(%rbp), %rax
	testq	%rax, %rax
	je	.L890
	movq	%rcx, %rsi
	subq	-16520(%rbp), %rsi
	addq	%rsi, 24(%rbx)
	movq	%rsi, -16560(%rbp)
	testl	%r12d, %r12d
	jne	.L911
	cmpq	$1, %rax
	je	.L887
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L890:
	addq	%rcx, 24(%rbx)
.L911:
	movq	32(%rbx), %rax
	movq	1848(%rax), %rax
	movl	%ecx, 8(%rax)
	movl	$0, 12(%rax)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L956:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L916:
	movl	$-22, %r12d
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L957:
	leaq	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L961:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L904:
	movq	-16536(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L906
.L915:
	movl	$-105, %r12d
	jmp	.L860
.L960:
	call	__stack_chk_fail@PLT
.L958:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L874
	.cfi_endproc
.LFE8538:
	.size	_ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_:
.LFB8563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L974
	cmpw	$1040, %cx
	jne	.L963
.L974:
	cmpq	$0, 23(%rdx)
	je	.L962
.L965:
	movq	31(%rdx), %r13
.L970:
	testq	%r13, %r13
	je	.L962
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L982
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L983
.L972:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L962:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L972
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L982:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L963:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L962
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L965
	cmpw	$1040, %cx
	je	.L965
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L970
	.cfi_endproc
.LFE8563:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.section	.text._ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL2:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL2
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	movq	%rsi, %r13
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1037
	cmpw	$1040, %cx
	jne	.L985
.L1037:
	movq	23(%rdx), %r14
.L987:
	movl	16(%r13), %eax
	testl	%eax, %eax
	jg	.L988
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1071
.L990:
	movl	16(%r13), %eax
	cmpl	$1, %eax
	jle	.L1072
	movq	8(%r13), %rcx
	movq	-8(%rcx), %rdx
	leaq	-8(%rcx), %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1073
.L993:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	0(%r13), %rdx
	movq	8(%rdx), %r12
	movq	88(%r12), %rdx
	addq	$88, %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L993
.L1073:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L993
	testl	%eax, %eax
	jg	.L995
	movq	0(%r13), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	movq	%r15, %rdi
.L998:
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	$0, -16536(%rbp)
	testb	%al, %al
	je	.L999
	cmpl	$2, 16(%r13)
	jg	.L1000
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -16536(%rbp)
.L999:
	movq	352(%r14), %rdi
	movl	$3, %edx
	movq	%r12, %rsi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movq	%rdx, %r13
	testb	%al, %al
	je	.L1001
	cmpq	$2147483647, %rdx
	ja	.L1035
	cmpq	$16384, %rdx
	ja	.L1067
	movq	(%rbx), %rax
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1005
.L1008:
	leaq	-16448(%rbp), %r10
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	352(%r14), %rdi
	movq	%r10, %rsi
	movl	$3, %r8d
	movq	%r10, -16552(%rbp)
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	-16552(%rbp), %r10
	movl	%eax, %esi
	movq	%rax, -16544(%rbp)
	movq	%r10, %rdi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rcx
	movq	$1, -16480(%rbp)
	movq	%rax, -16528(%rbp)
	leaq	-16528(%rbp), %rax
	movq	%rax, -16512(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1006
	subq	-16520(%rbp), %rcx
	addq	%rcx, 24(%rbx)
	movq	%rcx, -16560(%rbp)
.L1007:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16520(%rbp), %r12
	movq	%rax, -16504(%rbp)
	movq	%rax, -16568(%rbp)
	movq	360(%r14), %rax
	movq	%r12, %rsi
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1012
	movl	%r12d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16544(%rbp)
	movq	%rax, %r12
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16568(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	je	.L1014
.L1017:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L988:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L990
.L1071:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L995:
	movq	8(%r13), %r15
	cmpl	$2, %eax
	jle	.L1074
	leaq	-16(%r15), %rdi
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	%rbx, %rdi
	call	*%rax
	cmpb	$1, %al
	jne	.L1008
	cmpq	$0, -16536(%rbp)
	je	.L1008
.L1067:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r13, %rsi
	movq	%rax, -16504(%rbp)
	movq	%rax, -16560(%rbp)
	movq	360(%r14), %rax
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1012
	movl	%r13d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	movq	%rax, -16544(%rbp)
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16560(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	jne	.L1017
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16560(%rbp)
	call	uv_buf_init@PLT
	movq	-16560(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L1018
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L1018:
	movq	-16568(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	352(%r14), %rdi
	movl	$3, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	$0, -16560(%rbp)
	movq	%rax, %rsi
.L1016:
	cmpq	%r13, %rsi
	ja	.L1075
	movq	-16544(%rbp), %rdi
	call	uv_buf_init@PLT
	movq	%rax, -16528(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1020
.L1022:
	xorl	%r8d, %r8d
.L1021:
	leaq	-16528(%rbp), %rdx
	movq	%r15, %r9
	movl	$1, %ecx
	movq	%rbx, %rsi
	leaq	-16480(%rbp), %rdi
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%rbx), %rax
	movq	-16472(%rbp), %r12
	movq	1848(%rax), %rdx
	movq	-16560(%rbp), %rax
	addq	-16464(%rbp), %rax
	movl	%eax, 8(%rdx)
	movzbl	-16480(%rbp), %eax
	movl	%eax, 12(%rdx)
	testq	%r12, %r12
	je	.L1029
	cmpq	$0, 24(%r12)
	jne	.L1076
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r14, 16(%r12)
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rbx
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	movq	%rbx, 24(%r12)
	movq	-16552(%rbp), %rbx
	movq	%rax, -16544(%rbp)
	movq	%rbx, 32(%r12)
	movq	%rdx, -16552(%rbp)
.L1029:
	movl	-16476(%rbp), %r12d
.L1027:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rsi
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	testq	%rsi, %rsi
	je	.L984
	movq	360(%r14), %rax
	movq	-16552(%rbp), %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L984:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1077
	addq	$16536, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1000:
	.cfi_restore_state
	movq	8(%r13), %rax
	subq	$16, %rax
	movq	%rax, -16536(%rbp)
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L985:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1014:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	call	uv_buf_init@PLT
	movq	-16568(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L1015
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L1015:
	movq	-16528(%rbp), %rsi
	movq	-16520(%rbp), %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	-16520(%rbp), %rsi
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1001:
	xorl	%r12d, %r12d
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	%rbx, %rdi
	call	*%rax
	movq	-16536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1022
	testb	%al, %al
	je	.L1022
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1078
	movq	-16536(%rbp), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1038
	cmpw	$1040, %cx
	jne	.L1024
.L1038:
	movq	23(%rdx), %rax
.L1026:
	testq	%rax, %rax
	je	.L1036
	movq	80(%rax), %r8
	movq	360(%r14), %rax
	movq	%r15, %rdi
	movq	3280(%r14), %rsi
	movq	-16536(%rbp), %rcx
	movq	824(%rax), %rdx
	movq	%r8, -16568(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-16568(%rbp), %r8
	testb	%al, %al
	jne	.L1021
	movq	%r8, -16536(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-16536(%rbp), %r8
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1012:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	%rcx, -16544(%rbp)
	leaq	-16480(%rbp), %rdx
	leaq	-16512(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	-16544(%rbp), %rcx
	movl	%eax, %r12d
	movq	-16480(%rbp), %rax
	testq	%rax, %rax
	je	.L1010
	movq	%rcx, %rsi
	subq	-16520(%rbp), %rsi
	addq	%rsi, 24(%rbx)
	movq	%rsi, -16560(%rbp)
	testl	%r12d, %r12d
	jne	.L1031
	cmpq	$1, %rax
	je	.L1007
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1010:
	addq	%rcx, 24(%rbx)
.L1031:
	movq	32(%rbx), %rax
	movq	1848(%rax), %rax
	movl	%ecx, 8(%rax)
	movl	$0, 12(%rax)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1075:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	$-22, %r12d
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1076:
	leaq	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1078:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1024:
	movq	-16536(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1026
.L1035:
	movl	$-105, %r12d
	jmp	.L984
.L1077:
	call	__stack_chk_fail@PLT
.L1074:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L998
	.cfi_endproc
.LFE8539:
	.size	_ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_:
.LFB8564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1091
	cmpw	$1040, %cx
	jne	.L1080
.L1091:
	cmpq	$0, 23(%rdx)
	je	.L1079
.L1082:
	movq	31(%rdx), %r13
.L1087:
	testq	%r13, %r13
	je	.L1079
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L1099
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1100
.L1089:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L1079:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L1089
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1099:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1080:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L1079
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1082
	cmpw	$1040, %cx
	je	.L1082
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1087
	.cfi_endproc
.LFE8564:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.section	.text._ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL3:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL3
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	movq	%rsi, %r13
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1154
	cmpw	$1040, %cx
	jne	.L1102
.L1154:
	movq	23(%rdx), %r14
.L1104:
	movl	16(%r13), %eax
	testl	%eax, %eax
	jg	.L1105
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1188
.L1107:
	movl	16(%r13), %eax
	cmpl	$1, %eax
	jle	.L1189
	movq	8(%r13), %rcx
	movq	-8(%rcx), %rdx
	leaq	-8(%rcx), %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1190
.L1110:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	0(%r13), %rdx
	movq	8(%rdx), %r12
	movq	88(%r12), %rdx
	addq	$88, %r12
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1110
.L1190:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1110
	testl	%eax, %eax
	jg	.L1112
	movq	0(%r13), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	movq	%r15, %rdi
.L1115:
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	$0, -16536(%rbp)
	testb	%al, %al
	je	.L1116
	cmpl	$2, 16(%r13)
	jg	.L1117
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -16536(%rbp)
.L1116:
	movq	352(%r14), %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movq	%rdx, %r13
	testb	%al, %al
	je	.L1118
	cmpq	$2147483647, %rdx
	ja	.L1152
	cmpq	$16384, %rdx
	ja	.L1184
	movq	(%rbx), %rax
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1122
.L1125:
	leaq	-16448(%rbp), %r10
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	352(%r14), %rdi
	movq	%r10, %rsi
	movl	$4, %r8d
	movq	%r10, -16552(%rbp)
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	-16552(%rbp), %r10
	movl	%eax, %esi
	movq	%rax, -16544(%rbp)
	movq	%r10, %rdi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rcx
	movq	$1, -16480(%rbp)
	movq	%rax, -16528(%rbp)
	leaq	-16528(%rbp), %rax
	movq	%rax, -16512(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1123
	subq	-16520(%rbp), %rcx
	addq	%rcx, 24(%rbx)
	movq	%rcx, -16560(%rbp)
.L1124:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16520(%rbp), %r12
	movq	%rax, -16504(%rbp)
	movq	%rax, -16568(%rbp)
	movq	360(%r14), %rax
	movq	%r12, %rsi
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1129
	movl	%r12d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16544(%rbp)
	movq	%rax, %r12
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16568(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	je	.L1131
.L1134:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1107
.L1188:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	8(%r13), %r15
	cmpl	$2, %eax
	jle	.L1191
	leaq	-16(%r15), %rdi
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	%rbx, %rdi
	call	*%rax
	cmpb	$1, %al
	jne	.L1125
	cmpq	$0, -16536(%rbp)
	je	.L1125
.L1184:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r13, %rsi
	movq	%rax, -16504(%rbp)
	movq	%rax, -16560(%rbp)
	movq	360(%r14), %rax
	movq	%rdx, -16496(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1129
	movl	%r13d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	movq	%rax, -16544(%rbp)
	movq	%rdx, -16552(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -16560(%rbp)
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	jne	.L1134
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16560(%rbp)
	call	uv_buf_init@PLT
	movq	-16560(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L1135
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L1135:
	movq	-16568(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	352(%r14), %rdi
	movl	$4, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	$0, -16560(%rbp)
	movq	%rax, %rsi
.L1133:
	cmpq	%r13, %rsi
	ja	.L1192
	movq	-16544(%rbp), %rdi
	call	uv_buf_init@PLT
	movq	%rax, -16528(%rbp)
	movq	(%rbx), %rax
	movq	%rdx, -16520(%rbp)
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1137
.L1139:
	xorl	%r8d, %r8d
.L1138:
	leaq	-16528(%rbp), %rdx
	movq	%r15, %r9
	movl	$1, %ecx
	movq	%rbx, %rsi
	leaq	-16480(%rbp), %rdi
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%rbx), %rax
	movq	-16472(%rbp), %r12
	movq	1848(%rax), %rdx
	movq	-16560(%rbp), %rax
	addq	-16464(%rbp), %rax
	movl	%eax, 8(%rdx)
	movzbl	-16480(%rbp), %eax
	movl	%eax, 12(%rdx)
	testq	%r12, %r12
	je	.L1146
	cmpq	$0, 24(%r12)
	jne	.L1193
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r14, 16(%r12)
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rbx
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	movq	%rbx, 24(%r12)
	movq	-16552(%rbp), %rbx
	movq	%rax, -16544(%rbp)
	movq	%rbx, 32(%r12)
	movq	%rdx, -16552(%rbp)
.L1146:
	movl	-16476(%rbp), %r12d
.L1144:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-16544(%rbp), %rsi
	movq	%rax, -16504(%rbp)
	movq	%rdx, -16496(%rbp)
	testq	%rsi, %rsi
	je	.L1101
	movq	360(%r14), %rax
	movq	-16552(%rbp), %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1194
	addq	$16536, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1117:
	.cfi_restore_state
	movq	8(%r13), %rax
	subq	$16, %rax
	movq	%rax, -16536(%rbp)
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1102:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1131:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -16472(%rbp)
	movq	%rdi, -16464(%rbp)
	movq	%rdi, -16576(%rbp)
	xorl	%edi, %edi
	movq	%rax, -16568(%rbp)
	call	uv_buf_init@PLT
	movq	-16568(%rbp), %r8
	movq	%rax, -16472(%rbp)
	movq	%rdx, -16464(%rbp)
	testq	%r8, %r8
	je	.L1132
	movq	360(%r14), %rax
	movq	-16576(%rbp), %r9
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L1132:
	movq	-16528(%rbp), %rsi
	movq	-16520(%rbp), %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	-16520(%rbp), %rsi
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1118:
	xorl	%r12d, %r12d
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	%rbx, %rdi
	call	*%rax
	movq	-16536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1139
	testb	%al, %al
	je	.L1139
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1195
	movq	-16536(%rbp), %rax
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1155
	cmpw	$1040, %cx
	jne	.L1141
.L1155:
	movq	23(%rdx), %rax
.L1143:
	testq	%rax, %rax
	je	.L1153
	movq	80(%rax), %r8
	movq	360(%r14), %rax
	movq	%r15, %rdi
	movq	3280(%r14), %rsi
	movq	-16536(%rbp), %rcx
	movq	824(%rax), %rdx
	movq	%r8, -16568(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-16568(%rbp), %r8
	testb	%al, %al
	jne	.L1138
	movq	%r8, -16536(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-16536(%rbp), %r8
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1129:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	%rcx, -16544(%rbp)
	leaq	-16480(%rbp), %rdx
	leaq	-16512(%rbp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	-16544(%rbp), %rcx
	movl	%eax, %r12d
	movq	-16480(%rbp), %rax
	testq	%rax, %rax
	je	.L1127
	movq	%rcx, %rsi
	subq	-16520(%rbp), %rsi
	addq	%rsi, 24(%rbx)
	movq	%rsi, -16560(%rbp)
	testl	%r12d, %r12d
	jne	.L1148
	cmpq	$1, %rax
	je	.L1124
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1127:
	addq	%rcx, 24(%rbx)
.L1148:
	movq	32(%rbx), %rax
	movq	1848(%rax), %rax
	movl	%ecx, 8(%rax)
	movl	$0, 12(%rax)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1192:
	leaq	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1153:
	movl	$-22, %r12d
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1195:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1141:
	movq	-16536(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1143
.L1152:
	movl	$-105, %r12d
	jmp	.L1101
.L1194:
	call	__stack_chk_fail@PLT
.L1191:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1115
	.cfi_endproc
.LFE8540:
	.size	_ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_:
.LFB8565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1208
	cmpw	$1040, %cx
	jne	.L1197
.L1208:
	cmpq	$0, 23(%rdx)
	je	.L1196
.L1199:
	movq	31(%rdx), %r13
.L1204:
	testq	%r13, %r13
	je	.L1196
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L1216
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1217
.L1206:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L1196:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1217:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L1206
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1216:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L1196
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1199
	cmpw	$1040, %cx
	je	.L1199
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1204
	.cfi_endproc
.LFE8565:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS4_5ValueEEEEEEEvS9_
	.section	.rodata.str1.1
.LC15:
	.string	"ERR_INVALID_ARG_TYPE"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"Second argument must be a buffer"
	.section	.rodata.str1.1
.LC17:
	.string	"code"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jg	.L1219
	movq	(%rsi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1260
.L1221:
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1254
	cmpw	$1040, %cx
	jne	.L1222
.L1254:
	movq	23(%rdx), %r13
.L1224:
	cmpl	$1, 16(%rbx)
	jle	.L1261
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value12IsUint8ArrayEv@PLT
	testb	%al, %al
	je	.L1262
.L1227:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1234
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	movq	%r14, %rdi
.L1237:
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	movq	%rax, -96(%rbp)
	jg	.L1238
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1239:
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, -88(%rbp)
	jg	.L1240
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1241:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1244
	movq	(%r12), %rax
	leaq	_ZN4node10StreamBase9IsIPCPipeEv(%rip), %rdx
	movq	96(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1263
.L1244:
	xorl	%ebx, %ebx
.L1243:
	leaq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	movq	%r14, %r9
	movq	%rbx, %r8
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%r12), %rax
	movq	-64(%rbp), %rdx
	movq	1848(%rax), %rax
	movl	%edx, 8(%rax)
	movzbl	-80(%rbp), %edx
	movl	%edx, 12(%rax)
	movl	-76(%rbp), %eax
.L1218:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1264
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1219:
	.cfi_restore_state
	movq	8(%rsi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1221
.L1260:
	leaq	_ZZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value12IsUint8ArrayEv@PLT
	testb	%al, %al
	jne	.L1227
.L1262:
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1265
.L1228:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1266
.L1229:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1267
.L1230:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1268
.L1231:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1269
.L1232:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%eax, %eax
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1234:
	movq	8(%rbx), %r14
	cmpl	$1, %eax
	je	.L1270
	leaq	-8(%r14), %rdi
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	%r12, %rdi
	call	*%rax
	testb	%al, %al
	je	.L1244
	cmpl	$2, 16(%rbx)
	jg	.L1245
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
.L1246:
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1271
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1255
	cmpw	$1040, %cx
	jne	.L1248
.L1255:
	movq	23(%rdx), %rax
.L1250:
	testq	%rax, %rax
	je	.L1253
	movq	80(%rax), %rbx
	movq	360(%r13), %rax
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	3280(%r13), %rsi
	movq	824(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1243
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1222:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	8(%rbx), %r15
	subq	$16, %r15
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1248:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1269:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1268:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1267:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rdi
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1265:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1228
.L1253:
	movl	$-22, %eax
	jmp	.L1218
.L1264:
	call	__stack_chk_fail@PLT
.L1270:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1237
	.cfi_endproc
.LFE7773:
	.size	_ZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB8561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1284
	cmpw	$1040, %cx
	jne	.L1273
.L1284:
	cmpq	$0, 23(%rdx)
	je	.L1272
.L1275:
	movq	31(%rdx), %r13
.L1280:
	testq	%r13, %r13
	je	.L1272
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L1292
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1293
.L1282:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L1272:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1293:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L1282
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1292:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1273:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L1272
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1275
	cmpw	$1040, %cx
	je	.L1275
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1280
	.cfi_endproc
.LFE8561:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_11WriteBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rsi
	movq	%rdi, -440(%rbp)
	movq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1356
	cmpw	$1040, %cx
	jne	.L1295
.L1356:
	movq	23(%rdx), %rbx
.L1297:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L1298
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1391
.L1300:
	cmpl	$1, 16(%r12)
	jle	.L1392
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L1302:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L1393
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L1304
	movq	(%r12), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r15
	movq	%r15, -424(%rbp)
.L1305:
	movq	%r15, %rdi
.L1309:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	testb	%al, %al
	je	.L1310
	call	_ZNK2v85Array6LengthEv@PLT
	movdqa	.LC18(%rip), %xmm0
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movl	%eax, %ecx
	movq	%rcx, -408(%rbp)
	leaq	-312(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	%rcx, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	cmpl	$16, %eax
	ja	.L1311
	movq	-408(%rbp), %rax
	movq	%rax, -336(%rbp)
	testq	%rax, %rax
	je	.L1333
.L1351:
	xorl	%r12d, %r12d
.L1334:
	movq	3280(%rbx), %rsi
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1394
.L1330:
	cmpq	%r12, -336(%rbp)
	jbe	.L1332
	movq	-320(%rbp), %rcx
	movq	%r12, %r14
	movq	%r13, %rdi
	salq	$4, %r14
	addq	%r14, %rcx
	movq	%rcx, -416(%rbp)
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	-416(%rbp), %rcx
	movq	%rax, (%rcx)
	cmpq	%r12, -336(%rbp)
	jbe	.L1332
	movq	%r13, %rdi
	addq	-320(%rbp), %r14
	addq	$1, %r12
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, 8(%r14)
	cmpq	-408(%rbp), %r12
	jne	.L1334
.L1333:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-440(%rbp), %r15
	xorl	%r8d, %r8d
	movq	-424(%rbp), %r9
	movq	%rdx, %rsi
	movq	-408(%rbp), %rcx
	movq	-320(%rbp), %rdx
	movq	%rax, %rbx
	movq	%rsi, -384(%rbp)
	leaq	-368(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, -392(%rbp)
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%r15), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-352(%rbp), %rdx
	movl	-364(%rbp), %r12d
	movq	1848(%rax), %rax
	movl	%edx, 8(%rax)
	movzbl	-368(%rbp), %edx
	movl	%edx, 12(%rax)
	call	uv_buf_init@PLT
	testq	%rbx, %rbx
	jne	.L1348
.L1328:
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1294
	cmpq	-448(%rbp), %rdi
	je	.L1294
	call	free@PLT
.L1294:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1395
	addq	$456, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore_state
	movq	8(%r12), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1300
.L1391:
	leaq	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1310:
	call	_ZNK2v85Array6LengthEv@PLT
	movdqa	.LC18(%rip), %xmm0
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	shrl	%eax
	movl	%eax, %ecx
	movaps	%xmm0, -336(%rbp)
	movq	%rcx, -408(%rbp)
	leaq	-312(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	%rcx, -320(%rbp)
	cmpl	$16, %eax
	ja	.L1311
	movq	-408(%rbp), %rax
	movq	%rax, -336(%rbp)
	testq	%rax, %rax
	je	.L1396
.L1352:
	movq	$0, -416(%rbp)
	xorl	%r12d, %r12d
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	352(%rbx), %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	testb	%al, %al
	je	.L1388
	addq	%rdx, -416(%rbp)
.L1317:
	addq	$1, %r12
	cmpq	-408(%rbp), %r12
	jnb	.L1397
.L1326:
	movq	3280(%rbx), %rsi
	leal	(%r12,%r12), %r13d
	movq	%r15, %rdi
	movl	%r13d, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1398
.L1316:
	movq	%r14, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	jne	.L1317
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1399
.L1318:
	movq	3280(%rbx), %rsi
	leal	1(%r13), %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1400
.L1319:
	movq	352(%rbx), %rdi
	movl	$4, %edx
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	movl	%eax, %r13d
	cmpl	$1, %eax
	jne	.L1323
	movq	%r14, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	cmpl	$65535, %eax
	jle	.L1323
	movq	352(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	call	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	testb	%al, %al
	jne	.L1323
	.p2align 4,,10
	.p2align 3
.L1388:
	xorl	%r12d, %r12d
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1398:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	8(%r12), %rcx
	movq	%rcx, -424(%rbp)
	cmpl	$1, %eax
	je	.L1401
	leaq	-8(%rcx), %r15
	cmpl	$2, %eax
	je	.L1402
	leaq	-16(%rcx), %rdi
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1332:
	leaq	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	-408(%rbp), %r14
	movq	%r14, %r13
	salq	$4, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L1350
	movq	%rax, -320(%rbp)
	movq	%r14, -328(%rbp)
.L1315:
	movq	-408(%rbp), %rax
	movq	%rax, -336(%rbp)
	testb	%r12b, %r12b
	je	.L1352
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	%rax, -432(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-432(%rbp), %rsi
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1399:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1397:
	cmpq	$2147483647, -416(%rbp)
	ja	.L1403
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-416(%rbp), %rsi
	movq	%rax, -392(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rax, -456(%rbp)
	testq	%rsi, %rsi
	jne	.L1404
	movq	-384(%rbp), %rax
	movq	$0, -488(%rbp)
	movq	%rax, -480(%rbp)
.L1339:
	movq	$0, -432(%rbp)
	xorl	%r12d, %r12d
.L1346:
	movq	3280(%rbx), %rsi
	leal	(%r12,%r12), %r13d
	movq	%r15, %rdi
	movl	%r13d, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1405
.L1340:
	movq	%r14, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L1341
	cmpq	%r12, -336(%rbp)
	jbe	.L1332
	movq	-320(%rbp), %rdx
	movq	%r12, %r13
	movq	%r14, %rdi
	salq	$4, %r13
	addq	%r13, %rdx
	movq	%rdx, -464(%rbp)
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	-464(%rbp), %rdx
	movq	%rax, (%rdx)
	cmpq	%r12, -336(%rbp)
	jbe	.L1332
	movq	%r14, %rdi
	addq	-320(%rbp), %r13
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, 8(%r13)
.L1342:
	addq	$1, %r12
	cmpq	-408(%rbp), %r12
	jne	.L1346
	movq	-440(%rbp), %r15
	movq	-320(%rbp), %rdx
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movq	-424(%rbp), %r9
	leaq	-368(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%r15), %rax
	movq	-352(%rbp), %rdx
	movq	-360(%rbp), %rbx
	movq	1848(%rax), %rax
	movl	%edx, 8(%rax)
	movzbl	-368(%rbp), %edx
	movl	%edx, 12(%rax)
	testq	%rbx, %rbx
	je	.L1327
	cmpq	$0, -416(%rbp)
	je	.L1327
	cmpq	$0, 24(%rbx)
	jne	.L1406
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, 24(%rbx)
	movq	-488(%rbp), %rax
	movq	%rdx, 32(%rbx)
	movq	%rax, 16(%rbx)
	call	uv_buf_init@PLT
	movq	-456(%rbp), %rcx
	movq	%rax, -456(%rbp)
	movq	%rcx, 24(%rbx)
	movq	-480(%rbp), %rcx
	movq	%rdx, -480(%rbp)
	movq	%rcx, 32(%rbx)
.L1327:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	-364(%rbp), %r12d
	call	uv_buf_init@PLT
	movq	-456(%rbp), %rsi
	movq	%rax, -392(%rbp)
	movq	%rdx, -384(%rbp)
	testq	%rsi, %rsi
	je	.L1328
	movq	-488(%rbp), %rax
	testq	%rax, %rax
	je	.L1348
	movq	360(%rax), %rax
	movq	-480(%rbp), %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1295:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	360(%rbx), %rax
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1407
	movl	-416(%rbp), %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rdx, -480(%rbp)
	movq	%rax, %r12
	call	uv_buf_init@PLT
	cmpq	$0, -456(%rbp)
	movq	%rax, -392(%rbp)
	movq	%rdx, -384(%rbp)
	je	.L1337
.L1348:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1341:
	movq	-416(%rbp), %rcx
	cmpq	%rcx, -432(%rbp)
	ja	.L1408
	movq	-432(%rbp), %rcx
	movq	-456(%rbp), %rax
	movq	%r14, %rdi
	movq	3280(%rbx), %rsi
	addq	%rcx, %rax
	movq	%rax, -464(%rbp)
	movq	-480(%rbp), %rax
	subq	%rcx, %rax
	movq	%rax, -472(%rbp)
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1409
.L1344:
	movq	3280(%rbx), %rsi
	leal	1(%r13), %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1410
.L1345:
	movq	352(%rbx), %rdi
	movl	$4, %edx
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	movq	%r14, %rcx
	movq	-464(%rbp), %r14
	xorl	%r9d, %r9d
	movq	352(%rbx), %rdi
	movq	-472(%rbp), %rdx
	movl	%eax, %r8d
	movq	%r14, %rsi
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	cmpq	%r12, -336(%rbp)
	jbe	.L1332
	movq	%r12, %rdx
	addq	%rax, -432(%rbp)
	salq	$4, %rdx
	addq	-320(%rbp), %rdx
	movq	%r14, (%rdx)
	movq	%rax, 8(%rdx)
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1394:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1393:
	leaq	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1405:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1340
.L1337:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, %r13
	movq	%rdi, %r14
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -360(%rbp)
	movq	%rdx, -352(%rbp)
	testq	%r13, %r13
	je	.L1338
	movq	360(%rbx), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1338:
	movq	%r12, -456(%rbp)
	movq	%rbx, -488(%rbp)
	jmp	.L1339
.L1350:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1411
	movq	%rax, -320(%rbp)
	movq	-336(%rbp), %rdx
	movq	-408(%rbp), %rax
	movq	%rax, -328(%rbp)
	testq	%rdx, %rdx
	je	.L1315
	movq	-448(%rbp), %rsi
	salq	$4, %rdx
	call	memcpy@PLT
	jmp	.L1315
.L1396:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-440(%rbp), %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rdx, -384(%rbp)
	movq	-424(%rbp), %r9
	leaq	-368(%rbp), %rdi
	movq	%rdx, -480(%rbp)
	movq	-320(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rax, -392(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movq	32(%rbx), %rax
	movq	-352(%rbp), %rdx
	movq	$0, -488(%rbp)
	movq	1848(%rax), %rax
	movl	%edx, 8(%rax)
	movzbl	-368(%rbp), %edx
	movl	%edx, 12(%rax)
	jmp	.L1327
.L1408:
	leaq	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1410:
	movq	%rax, -496(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-496(%rbp), %rsi
	jmp	.L1345
.L1409:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1344
.L1411:
	leaq	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1407:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1406:
	leaq	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1403:
	movl	$-105, %r12d
	jmp	.L1328
.L1402:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1309
.L1395:
	call	__stack_chk_fail@PLT
.L1401:
	movq	(%r12), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r15
	jmp	.L1305
	.cfi_endproc
.LFE7772:
	.size	_ZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB8560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1424
	cmpw	$1040, %cx
	jne	.L1413
.L1424:
	cmpq	$0, 23(%rdx)
	je	.L1412
.L1415:
	movq	31(%rdx), %r13
.L1420:
	testq	%r13, %r13
	je	.L1412
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L1432
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L1433
.L1422:
	movq	1256(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%r12), %r14
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -40(%rbp)
	call	_ZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	movsd	-40(%rbp), %xmm1
	salq	$32, %rax
	movq	%rax, 24(%r14)
	movq	1256(%rbx), %rax
	movsd	%xmm1, 24(%rax)
.L1412:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1433:
	.cfi_restore_state
	comisd	.LC2(%rip), %xmm0
	jnb	.L1422
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1432:
	movabsq	$-94489280512, %rsi
	movq	(%r12), %rax
	movq	%rsi, 24(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1413:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L1412
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1415
	cmpw	$1040, %cx
	je	.L1415
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1420
	.cfi_endproc
.LFE8560:
	.size	_ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN4node10StreamBase8JSMethodIXadL_ZNS0_6WritevERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.weak	_ZTVN4node9StreamReqE
	.section	.data.rel.ro._ZTVN4node9StreamReqE,"awG",@progbits,_ZTVN4node9StreamReqE,comdat
	.align 8
	.type	_ZTVN4node9StreamReqE, @object
	.size	_ZTVN4node9StreamReqE, 48
_ZTVN4node9StreamReqE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, 168
_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-16
	.quad	0
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, 168
_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-40
	.quad	0
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node10StreamBaseE
	.section	.data.rel.ro._ZTVN4node10StreamBaseE,"awG",@progbits,_ZTVN4node10StreamBaseE,comdat
	.align 8
	.type	_ZTVN4node10StreamBaseE, @object
	.size	_ZTVN4node10StreamBaseE, 160
_ZTVN4node10StreamBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node10StreamBase9IsIPCPipeEv
	.quad	_ZN4node10StreamBase5GetFDEv
	.quad	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	__cxa_pure_virtual
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZTVN4node22EmitToJSStreamListenerE
	.section	.data.rel.ro.local._ZTVN4node22EmitToJSStreamListenerE,"awG",@progbits,_ZTVN4node22EmitToJSStreamListenerE,comdat
	.align 8
	.type	_ZTVN4node22EmitToJSStreamListenerE, @object
	.size	_ZTVN4node22EmitToJSStreamListenerE, 80
_ZTVN4node22EmitToJSStreamListenerE:
	.quad	0
	.quad	0
	.quad	_ZN4node22EmitToJSStreamListenerD1Ev
	.quad	_ZN4node22EmitToJSStreamListenerD0Ev
	.quad	_ZN4node22EmitToJSStreamListener13OnStreamAllocEm
	.quad	_ZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_t
	.quad	_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node22CustomBufferJSListenerE
	.section	.data.rel.ro.local._ZTVN4node22CustomBufferJSListenerE,"awG",@progbits,_ZTVN4node22CustomBufferJSListenerE,comdat
	.align 8
	.type	_ZTVN4node22CustomBufferJSListenerE, @object
	.size	_ZTVN4node22CustomBufferJSListenerE, 80
_ZTVN4node22CustomBufferJSListenerE:
	.quad	0
	.quad	0
	.quad	_ZN4node22CustomBufferJSListenerD1Ev
	.quad	_ZN4node22CustomBufferJSListenerD0Ev
	.quad	_ZN4node22CustomBufferJSListener13OnStreamAllocEm
	.quad	_ZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_t
	.quad	_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node22CustomBufferJSListener15OnStreamDestroyEv
	.weak	_ZTVN4node30ReportWritesToJSStreamListenerE
	.section	.data.rel.ro._ZTVN4node30ReportWritesToJSStreamListenerE,"awG",@progbits,_ZTVN4node30ReportWritesToJSStreamListenerE,comdat
	.align 8
	.type	_ZTVN4node30ReportWritesToJSStreamListenerE, @object
	.size	_ZTVN4node30ReportWritesToJSStreamListenerE, 80
_ZTVN4node30ReportWritesToJSStreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node30ReportWritesToJSStreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node30ReportWritesToJSStreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/util-inl.h:374"
.LC20:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"T* node::Realloc(T*, size_t) [with T = uv_buf_t; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args,"awG",@progbits,_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args, 24
_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.weak	_ZZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEEE4args
	.section	.rodata.str1.1
.LC22:
	.string	"../src/base_object-inl.h:175"
.LC23:
	.string	"((*value)->*typecheck)()"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"static void node::BaseObject::InternalFieldSet(v8::Local<v8::String>, v8::Local<v8::Value>, const v8::PropertyCallbackInfo<void>&) [with int Field = 2; bool (v8::Value::* typecheck)() const = &v8::Value::IsFunction]"
	.section	.data.rel.ro.local._ZZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEEE4args,"awG",@progbits,_ZZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEEE4args, 24
_ZZN4node10BaseObject16InternalFieldSetILi2EXadL_ZNK2v85Value10IsFunctionEvEEEEvNS2_5LocalINS2_6StringEEENS4_IS3_EERKNS2_20PropertyCallbackInfoIvEEE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.weak	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args
	.section	.rodata.str1.1
.LC25:
	.string	"../src/util.h:352"
.LC26:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = uv_buf_t; long unsigned int kStackStorageSize = 16; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args, 24
_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2
	.section	.rodata.str1.1
.LC28:
	.string	"../src/stream_base.cc:287"
.LC29:
	.string	"(data_size) <= (storage_size)"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"int node::StreamBase::WriteString(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding enc = node::BINARY]"
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1
	.section	.rodata.str1.1
.LC31:
	.string	"../src/stream_base.cc:267"
.LC32:
	.string	"(count) == (1)"
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC30
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0
	.section	.rodata.str1.1
.LC33:
	.string	"../src/stream_base.cc:213"
.LC34:
	.string	"args[1]->IsString()"
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC30
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args
	.section	.rodata.str1.1
.LC35:
	.string	"../src/stream_base.cc:212"
.LC36:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE4EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC30
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"int node::StreamBase::WriteString(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding enc = node::UCS2]"
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC37
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC37
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC37
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE3EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"int node::StreamBase::WriteString(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding enc = node::UTF8]"
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC38
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC38
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC38
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE1EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC38
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"int node::StreamBase::WriteString(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding enc = node::ASCII]"
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_2:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC39
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC39
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC39
	.weak	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args
	.section	.data.rel.ro.local._ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,"awG",@progbits,_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node10StreamBase11WriteStringILNS_8encodingE0EEEiRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC39
	.section	.rodata.str1.1
.LC40:
	.string	"../src/stream_base.cc:549"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"!async_wrap->persistent().IsEmpty()"
	.align 8
.LC42:
	.string	"void node::ReportWritesToJSStreamListener::OnStreamAfterReqFinished(node::StreamReq*, int)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEiE4args, @object
	.size	_ZZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEiE4args, 24
_ZZN4node30ReportWritesToJSStreamListener24OnStreamAfterReqFinishedEPNS_9StreamReqEiE4args:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.section	.rodata.str1.1
.LC43:
	.string	"../src/stream_base.cc:528"
.LC44:
	.string	"(buf.base) == (buffer_.base)"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"virtual void node::CustomBufferJSListener::OnStreamRead(ssize_t, const uv_buf_t&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args_0, @object
	.size	_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args_0, 24
_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args_0:
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.section	.rodata.str1.1
.LC46:
	.string	"../src/stream_base.cc:514"
.LC47:
	.string	"(stream_) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args, @object
	.size	_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args, 24
_ZZN4node22CustomBufferJSListener12OnStreamReadElRK8uv_buf_tE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC45
	.section	.rodata.str1.1
.LC48:
	.string	"../src/stream_base.cc:501"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"(static_cast<size_t>(nread)) <= (buf.size())"
	.align 8
.LC50:
	.string	"virtual void node::EmitToJSStreamListener::OnStreamRead(ssize_t, const uv_buf_t&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args_0, @object
	.size	_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args_0, 24
_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args_0:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.section	.rodata.str1.1
.LC51:
	.string	"../src/stream_base.cc:488"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args, @object
	.size	_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args, 24
_ZZN4node22EmitToJSStreamListener12OnStreamReadElRK8uv_buf_tE4args:
	.quad	.LC51
	.quad	.LC47
	.quad	.LC50
	.section	.rodata.str1.1
.LC52:
	.string	"../src/stream_base.cc:482"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"virtual uv_buf_t node::EmitToJSStreamListener::OnStreamAlloc(size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22EmitToJSStreamListener13OnStreamAllocEmE4args, @object
	.size	_ZZN4node22EmitToJSStreamListener13OnStreamAllocEmE4args, 24
_ZZN4node22EmitToJSStreamListener13OnStreamAllocEmE4args:
	.quad	.LC52
	.quad	.LC47
	.quad	.LC53
	.section	.rodata.str1.1
.LC54:
	.string	"../src/stream_base.cc:345"
.LC55:
	.string	"onread->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"v8::MaybeLocal<v8::Value> node::StreamBase::CallJSOnreadMethod(ssize_t, v8::Local<v8::ArrayBuffer>, size_t, node::StreamBase::StreamBaseJSChecks)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args_0, @object
	.size	_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args_0, 24
_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args_0:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.section	.rodata.str1.1
.LC57:
	.string	"../src/stream_base.cc:342"
.LC58:
	.string	"(wrap) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args, @object
	.size	_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args, 24
_ZZN4node10StreamBase18CallJSOnreadMethodElN2v85LocalINS1_11ArrayBufferEEEmNS0_18StreamBaseJSChecksEE4args:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC56
	.section	.rodata.str1.1
.LC59:
	.string	"../src/stream_base.cc:173"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"int node::StreamBase::WriteBuffer(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10StreamBase11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC59
	.quad	.LC36
	.quad	.LC60
	.section	.rodata.str1.1
.LC61:
	.string	"../src/stream_base.cc:145"
.LC62:
	.string	"(offset) <= (storage_size)"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"int node::StreamBase::Writev(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.section	.rodata.str1.1
.LC64:
	.string	"../src/stream_base.cc:78"
.LC65:
	.string	"args[1]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC63
	.section	.rodata.str1.1
.LC66:
	.string	"../src/stream_base.cc:77"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10StreamBase6WritevERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC66
	.quad	.LC36
	.quad	.LC63
	.section	.rodata.str1.1
.LC67:
	.string	"../src/stream_base.cc:63"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"int node::StreamBase::Shutdown(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10StreamBase8ShutdownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC67
	.quad	.LC36
	.quad	.LC68
	.section	.rodata.str1.1
.LC69:
	.string	"../src/stream_base.cc:55"
.LC70:
	.string	"Buffer::HasInstance(args[0])"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"int node::StreamBase::UseUserBuffer(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10StreamBase13UseUserBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.weak	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args
	.section	.rodata.str1.1
.LC72:
	.string	"../src/stream_base-inl.h:289"
.LC73:
	.string	"(storage_.data()) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"void node::WriteWrap::SetAllocatedStorage(node::AllocatedBuffer&&)"
	.section	.data.rel.ro.local._ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args,"awG",@progbits,_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args,comdat
	.align 16
	.type	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args, @gnu_unique_object
	.size	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args, 24
_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC75:
	.string	"../src/stream_base-inl.h:103"
.LC76:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC78:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC80:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC81:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC81
	.quad	.LC79
	.quad	.LC82
	.weak	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC83:
	.string	"../src/stream_base-inl.h:26"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"(req_wrap_obj->GetAlignedPointerFromInternalField( StreamReq::kStreamReqField)) == (nullptr)"
	.align 8
.LC85:
	.string	"void node::StreamReq::AttachToObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC86:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC88:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC89:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC91:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC92:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC94:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC95:
	.string	"../src/env-inl.h:955"
.LC96:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.weak	_ZZN4node15AllocatedBuffer6ResizeEmE4args
	.section	.rodata.str1.1
.LC98:
	.string	"../src/env-inl.h:911"
.LC99:
	.string	"(new_data) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"void node::AllocatedBuffer::Resize(size_t)"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer6ResizeEmE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer6ResizeEmE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer6ResizeEmE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer6ResizeEmE4args, 24
_ZZN4node15AllocatedBuffer6ResizeEmE4args:
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.weak	_ZZN4node11Environment8AllocateEmE4args
	.section	.rodata.str1.1
.LC101:
	.string	"../src/env-inl.h:889"
.LC102:
	.string	"(ret) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"char* node::Environment::Allocate(size_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8AllocateEmE4args,"awG",@progbits,_ZZN4node11Environment8AllocateEmE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8AllocateEmE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8AllocateEmE4args, 24
_ZZN4node11Environment8AllocateEmE4args:
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC104:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC106:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	7517463719428581715
	.quad	8239175502546629749
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-1074790400
	.align 8
.LC2:
	.long	0
	.long	0
	.section	.rodata.cst16
	.align 16
.LC18:
	.quad	0
	.quad	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
