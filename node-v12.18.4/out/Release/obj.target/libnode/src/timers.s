	.file	"timers.cc"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	16(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jg	.L2
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L38
.L4:
	cmpl	$1, 16(%rbx)
	jle	.L39
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L6:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L40
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L20
	cmpw	$1040, %cx
	jne	.L8
.L20:
	movq	23(%rdx), %r12
.L10:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L11
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L12:
	movq	2912(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L13
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2912(%r12)
.L13:
	testq	%r13, %r13
	je	.L14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2912(%r12)
.L14:
	cmpl	$1, 16(%rbx)
	movq	352(%r12), %r14
	movq	3016(%r12), %rdi
	jg	.L15
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	testq	%rdi, %rdi
	je	.L16
.L17:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3016(%r12)
.L16:
	testq	%r13, %r13
	je	.L1
.L18:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3016(%r12)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L4
.L38:
	leaq	_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	movq	8(%rbx), %r13
	subq	$8, %r13
	testq	%rdi, %rdi
	jne	.L17
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L11:
	movq	8(%rbx), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L10
	.cfi_endproc
.LFE7080:
	.size	_ZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111GetLibuvNowERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111GetLibuvNowERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L47
	cmpw	$1040, %cx
	jne	.L42
.L47:
	movq	23(%rdx), %rdi
	call	_ZN4node11Environment6GetNowEv@PLT
	testq	%rax, %rax
	je	.L49
.L45:
	movq	(%rax), %rax
	movq	%rax, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%rdi, %rbx
	xorl	%esi, %esi
	leaq	32(%r12), %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %rdi
	call	_ZN4node11Environment6GetNowEv@PLT
	testq	%rax, %rax
	jne	.L45
.L49:
	movq	16(%r12), %rax
	movq	%rax, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7081:
	.size	_ZN4node12_GLOBAL__N_111GetLibuvNowERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_111GetLibuvNowERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_114ToggleTimerRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_114ToggleTimerRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L56
	cmpw	$1040, %cx
	jne	.L51
.L56:
	movq	23(%rdx), %r12
.L53:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L54
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L55:
	call	_ZNK2v85Value6IsTrueEv@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movzbl	%al, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11Environment14ToggleTimerRefEb@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L53
	.cfi_endproc
.LFE7083:
	.size	_ZN4node12_GLOBAL__N_114ToggleTimerRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_114ToggleTimerRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_118ToggleImmediateRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_118ToggleImmediateRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L64
	cmpw	$1040, %cx
	jne	.L59
.L64:
	movq	23(%rdx), %r12
.L61:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L62
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L63:
	call	_ZNK2v85Value6IsTrueEv@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movzbl	%al, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L61
	.cfi_endproc
.LFE7084:
	.size	_ZN4node12_GLOBAL__N_118ToggleImmediateRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_118ToggleImmediateRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_113ScheduleTimerERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_113ScheduleTimerERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L73
	cmpw	$1040, %cx
	jne	.L67
.L73:
	movq	23(%rdx), %r12
.L69:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L70
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L71:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %rsi
	testb	%al, %al
	je	.L75
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11Environment13ScheduleTimerEl@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rdx, -24(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-24(%rbp), %rsi
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11Environment13ScheduleTimerEl@PLT
	.cfi_endproc
.LFE7082:
	.size	_ZN4node12_GLOBAL__N_113ScheduleTimerERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_113ScheduleTimerERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getLibuvNow"
.LC1:
	.string	"setupTimers"
.LC2:
	.string	"scheduleTimer"
.LC3:
	.string	"toggleTimerRef"
.LC4:
	.string	"toggleImmediateRef"
.LC5:
	.string	"immediateInfo"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L77
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L77
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L77
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_111GetLibuvNowERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r13
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L103
.L78:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L104
.L79:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L105
.L80:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L106
.L81:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L107
.L82:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L108
.L83:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_113ScheduleTimerERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L109
.L84:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L110
.L85:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L111
.L86:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node12_GLOBAL__N_114ToggleTimerRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L112
.L87:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L113
.L88:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L114
.L89:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_118ToggleImmediateRefERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L115
.L90:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L116
.L91:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L117
.L92:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	1320(%rbx), %r13
	testq	%r13, %r13
	je	.L93
	movq	0(%r13), %rsi
	movq	1288(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
.L93:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$13, %ecx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L118
.L94:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L119
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L104:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L105:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L106:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L107:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L108:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L109:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L110:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L111:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L112:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L115:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L116:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L117:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7085:
.L77:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7085:
	.text
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE6:
	.text
.LHOTE6:
	.p2align 4
	.globl	_Z16_register_timersv
	.type	_Z16_register_timersv, @function
_Z16_register_timersv:
.LFB7086:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7086:
	.size	_Z16_register_timersv, .-_Z16_register_timersv
	.section	.rodata.str1.1
.LC7:
	.string	"../src/timers.cc"
.LC8:
	.string	"timers"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC7
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC8
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC9:
	.string	"../src/timers.cc:19"
.LC10:
	.string	"args[1]->IsFunction()"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"void node::{anonymous}::SetupTimers(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.1
.LC12:
	.string	"../src/timers.cc:18"
.LC13:
	.string	"args[0]->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_111SetupTimersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC11
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
