	.file	"node.cc"
	.text
	.section	.text._ZN4node22NodeTraceStateObserverD2Ev,"axG",@progbits,_ZN4node22NodeTraceStateObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22NodeTraceStateObserverD2Ev
	.type	_ZN4node22NodeTraceStateObserverD2Ev, @function
_ZN4node22NodeTraceStateObserverD2Ev:
.LFB10792:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10792:
	.size	_ZN4node22NodeTraceStateObserverD2Ev, .-_ZN4node22NodeTraceStateObserverD2Ev
	.weak	_ZN4node22NodeTraceStateObserverD1Ev
	.set	_ZN4node22NodeTraceStateObserverD1Ev,_ZN4node22NodeTraceStateObserverD2Ev
	.section	.text._ZN4node22NodeTraceStateObserver15OnTraceDisabledEv,"axG",@progbits,_ZN4node22NodeTraceStateObserver15OnTraceDisabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22NodeTraceStateObserver15OnTraceDisabledEv
	.type	_ZN4node22NodeTraceStateObserver15OnTraceDisabledEv, @function
_ZN4node22NodeTraceStateObserver15OnTraceDisabledEv:
.LFB8055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node22NodeTraceStateObserver15OnTraceDisabledEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8055:
	.size	_ZN4node22NodeTraceStateObserver15OnTraceDisabledEv, .-_ZN4node22NodeTraceStateObserver15OnTraceDisabledEv
	.section	.text._ZN4node22NodeTraceStateObserverD0Ev,"axG",@progbits,_ZN4node22NodeTraceStateObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22NodeTraceStateObserverD0Ev
	.type	_ZN4node22NodeTraceStateObserverD0Ev, @function
_ZN4node22NodeTraceStateObserverD0Ev:
.LFB10794:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10794:
	.size	_ZN4node22NodeTraceStateObserverD0Ev, .-_ZN4node22NodeTraceStateObserverD0Ev
	.section	.text._ZN4node7tracing11TracedValueD0Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD0Ev
	.type	_ZN4node7tracing11TracedValueD0Ev, @function
_ZN4node7tracing11TracedValueD0Ev:
.LFB10760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10760:
	.size	_ZN4node7tracing11TracedValueD0Ev, .-_ZN4node7tracing11TracedValueD0Ev
	.text
	.p2align 4
	.globl	_ZN4node21MarkBootstrapCompleteERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node21MarkBootstrapCompleteERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node21MarkBootstrapCompleteERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB9194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L13
	cmpw	$1040, %cx
	jne	.L10
.L13:
	movq	23(%rdx), %rax
.L12:
	movq	1864(%rax), %r12
	call	uv_hrtime@PLT
	addq	$8, %rsp
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L12
	.cfi_endproc
.LFE9194:
	.size	_ZN4node21MarkBootstrapCompleteERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node21MarkBootstrapCompleteERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node10ResetStdioEv
	.type	_ZN4node10ResetStdioEv, @function
_ZN4node10ResetStdioEv:
.LFB9200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	movabsq	$-8881765665119413741, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN4nodeL5stdioE(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r13, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_tty_reset_mode@PLT
.L32:
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movl	$1, %edi
	subq	%r13, %rsi
	sarq	$3, %rsi
	imulq	%r14, %rsi
	movl	%esi, %r12d
	call	__fxstat64@PLT
	cmpl	$-1, %eax
	je	.L38
	movq	-208(%rbp), %rax
	cmpq	%rax, 8(%rbx)
	jne	.L30
	movq	-200(%rbp), %rax
	cmpq	%rax, 16(%rbx)
	je	.L20
.L30:
	addq	$216, %rbx
	leaq	648+_ZN4nodeL5stdioE(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L32
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L40
.L20:
	xorl	%eax, %eax
	movl	$3, %esi
	movl	%r12d, %edi
	call	fcntl64@PLT
	cmpl	$-1, %eax
	je	.L41
	movl	(%rbx), %edx
	movl	%edx, %ecx
	xorl	%eax, %ecx
	andb	$8, %ch
	jne	.L42
.L21:
	cmpb	$0, 4(%rbx)
	je	.L30
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	call	sigemptyset@PLT
	movq	-352(%rbp), %rdi
	movl	$22, %esi
	call	sigaddset@PLT
	movq	-352(%rbp), %rsi
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L43
	leaq	152(%rbx), %rax
	movq	%rax, -344(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L45:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L44
.L27:
	movq	-344(%rbp), %rdx
	xorl	%esi, %esi
	movl	%r12d, %edi
	call	tcsetattr@PLT
	cmpl	$-1, %eax
	je	.L45
	movq	-352(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %edi
	movl	%eax, -344(%rbp)
	call	pthread_sigmask@PLT
	movl	-344(%rbp), %ecx
	testl	%eax, %eax
	jne	.L33
	testl	%ecx, %ecx
	je	.L30
.L29:
	leaq	_ZZN4node10ResetStdioEvE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	call	__errno_location@PLT
	cmpl	$9, (%rax)
	je	.L30
	leaq	_ZZN4node10ResetStdioEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	andb	$-9, %ah
	andl	$2048, %edx
	orl	%eax, %edx
	movl	%edx, -344(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L47:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L46
.L23:
	movl	-344(%rbp), %edx
	xorl	%eax, %eax
	movl	$4, %esi
	movl	%r12d, %edi
	call	fcntl64@PLT
	cmpl	$-1, %eax
	je	.L47
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	_ZZN4node10ResetStdioEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L44:
	movq	-352(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %edi
	movq	%rax, -344(%rbp)
	call	pthread_sigmask@PLT
	movq	-344(%rbp), %rcx
	testl	%eax, %eax
	je	.L48
.L33:
	leaq	_ZZN4node10ResetStdioEvE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L46:
	leaq	_ZZN4node10ResetStdioEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L43:
	leaq	_ZZN4node10ResetStdioEvE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L48:
	cmpl	$1, (%rcx)
	je	.L30
	jmp	.L29
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9200:
	.size	_ZN4node10ResetStdioEv, .-_ZN4node10ResetStdioEv
	.p2align 4
	.globl	_ZN4node10SignalExitEi
	.type	_ZN4node10SignalExitEi, @function
_ZN4node10SignalExitEi:
.LFB9183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	subq	$8, %rsp
	call	_ZN4node10ResetStdioEv
	addq	$8, %rsp
	movl	%r12d, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	raise@PLT
	.cfi_endproc
.LFE9183:
	.size	_ZN4node10SignalExitEi, .-_ZN4node10SignalExitEi
	.section	.text._ZN4node10V8PlatformD2Ev,"axG",@progbits,_ZN4node10V8PlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10V8PlatformD2Ev
	.type	_ZN4node10V8PlatformD2Ev, @function
_ZN4node10V8PlatformD2Ev:
.LFB12861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L52
	movl	24(%rbx), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L52:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L53
	movq	%r12, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L53:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L51
	movq	(%rdi), %rax
	leaq	_ZN4node22NodeTraceStateObserverD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
	popq	%rbx
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE12861:
	.size	_ZN4node10V8PlatformD2Ev, .-_ZN4node10V8PlatformD2Ev
	.weak	_ZN4node10V8PlatformD1Ev
	.set	_ZN4node10V8PlatformD1Ev,_ZN4node10V8PlatformD2Ev
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB12887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L65
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L66
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L92
	.p2align 4,,10
	.p2align 3
.L65:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L71
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L77
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L93
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L71
.L72:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L79
.L93:
	lock subl	$1, 8(%r13)
	jne	.L79
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L79
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L72
	.p2align 4,,10
	.p2align 3
.L71:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L75
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L71
.L77:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L75
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L75
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L66:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L65
.L92:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L69
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L70:
	cmpl	$1, %eax
	jne	.L65
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L69:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L70
	.cfi_endproc
.LFE12887:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata._ZN4node22NodeTraceStateObserver14OnTraceEnabledEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"__metadata"
.LC2:
	.string	"name"
.LC3:
	.string	"process_name"
.LC4:
	.string	"node"
.LC5:
	.string	"version"
.LC6:
	.string	"JavaScriptMainThread"
.LC7:
	.string	"thread_name"
.LC8:
	.string	"versions"
.LC9:
	.string	"v8"
.LC10:
	.string	"uv"
.LC11:
	.string	"zlib"
.LC12:
	.string	"brotli"
.LC13:
	.string	"ares"
.LC14:
	.string	"modules"
.LC15:
	.string	"nghttp2"
.LC16:
	.string	"napi"
.LC17:
	.string	"llhttp"
.LC18:
	.string	"http_parser"
.LC19:
	.string	"openssl"
.LC20:
	.string	"cldr"
.LC21:
	.string	"icu"
.LC22:
	.string	"tz"
.LC23:
	.string	"unicode"
.LC24:
	.string	"arch"
.LC25:
	.string	"platform"
.LC26:
	.string	"release"
.LC27:
	.string	"lts"
.LC28:
	.string	"process"
	.section	.text._ZN4node22NodeTraceStateObserver14OnTraceEnabledEv,"axG",@progbits,_ZN4node22NodeTraceStateObserver14OnTraceEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22NodeTraceStateObserver14OnTraceEnabledEv
	.type	_ZN4node22NodeTraceStateObserver14OnTraceEnabledEv, @function
_ZN4node22NodeTraceStateObserver14OnTraceEnabledEv:
.LFB8051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-136(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-96(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node15GetProcessTitleB5cxx11EPKc@PLT
	cmpq	$0, -88(%rbp)
	jne	.L174
.L96:
	movq	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31(%rip), %r15
	testq	%r15, %r15
	je	.L175
.L106:
	testb	$5, (%r15)
	jne	.L176
.L108:
	movq	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35(%rip), %r15
	testq	%r15, %r15
	je	.L177
.L114:
	testb	$5, (%r15)
	jne	.L178
.L116:
	movq	%r14, %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	-136(%rbp), %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN4node7tracing11TracedValue15BeginDictionaryEPKc@PLT
	movq	_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	32+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	64+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	96+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC11(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	128+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC12(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	160+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC13(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	192+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC14(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	224+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC15(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	256+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	288+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC17(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	320+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC18(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	352+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC19(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	384+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC20(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	416+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC21(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	448+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC22(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	480+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC23(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	-136(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue13EndDictionaryEv@PLT
	movq	640+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC24(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	672+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC25(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	-136(%rbp), %rdi
	leaq	.LC26(%rip), %rsi
	call	_ZN4node7tracing11TracedValue15BeginDictionaryEPKc@PLT
	movq	512+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	544+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	-136(%rbp), %rdi
	leaq	.LC27(%rip), %rsi
	call	_ZN4node7tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	-136(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue13EndDictionaryEv@PLT
	movq	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60(%rip), %r14
	testq	%r14, %r14
	je	.L179
.L122:
	testb	$5, (%r14)
	jne	.L180
.L124:
	movq	8(%r12), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L129
	movq	(%r12), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L130
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r12), %rdi
	movq	%rax, (%r12)
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L129:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L94
	call	_ZdlPv@PLT
.L94:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	leaq	.LC28(%rip), %rax
	movb	$8, -137(%rbp)
	leaq	-112(%rbp), %r13
	movq	%rax, -128(%rbp)
	movq	-136(%rbp), %rax
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
	movq	$0, -136(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	movq	%r14, %rsi
	leaq	-128(%rbp), %r8
	movq	%rax, %rdi
	leaq	-120(%rbp), %rax
	pushq	$0
	movl	$1, %ecx
	pushq	%r13
	leaq	-137(%rbp), %r9
	leaq	.LC4(%rip), %rdx
	pushq	%rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	addq	$32, %rsp
.L128:
	movq	-8(%rbx), %r8
	subq	$8, %rbx
	testq	%r8, %r8
	je	.L125
	movq	(%r8), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L126
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	movq	%r15, (%r8)
	cmpq	%rax, %rdi
	je	.L127
	movq	%r8, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r8
.L127:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L125:
	cmpq	%r13, %rbx
	jne	.L128
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L179:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L123
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r14
.L123:
	movq	%r14, _ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60(%rip)
	mfence
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	.LC2(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$6, -136(%rbp)
	leaq	-112(%rbp), %r13
	movq	%rax, -128(%rbp)
	leaq	.LC6(%rip), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r14, %r9
	movq	%rax, %rdi
	leaq	-120(%rbp), %rax
	pushq	$0
	leaq	-128(%rbp), %r8
	pushq	%r13
	movl	$1, %ecx
	leaq	.LC7(%rip), %rdx
	movq	%rbx, %r15
	pushq	%rax
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	addq	$32, %rsp
.L120:
	movq	-8(%r15), %r8
	subq	$8, %r15
	testq	%r8, %r8
	je	.L117
	movq	(%r8), %rdx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rax
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L118
	movq	8(%r8), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r8), %rdx
	movq	%rax, (%r8)
	cmpq	%rdx, %rdi
	je	.L119
	movq	%r8, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r8
.L119:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L117:
	cmpq	%r13, %r15
	jne	.L120
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L177:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r15
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L115
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r15
.L115:
	movq	%r15, _ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35(%rip)
	mfence
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	.LC4(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$6, -136(%rbp)
	leaq	-112(%rbp), %r13
	movq	%rax, -128(%rbp)
	movq	_ZN4node11per_process8metadataE(%rip), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r14, %r9
	movq	%rax, %rdi
	leaq	-120(%rbp), %rax
	pushq	$0
	leaq	-128(%rbp), %r8
	pushq	%r13
	movl	$1, %ecx
	leaq	.LC5(%rip), %rdx
	movq	%rbx, %r15
	pushq	%rax
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	addq	$32, %rsp
.L112:
	movq	-8(%r15), %r8
	subq	$8, %r15
	testq	%r8, %r8
	je	.L109
	movq	(%r8), %rdx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rax
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L110
	movq	8(%r8), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r8), %rdx
	movq	%rax, (%r8)
	cmpq	%rdx, %rdi
	je	.L111
	movq	%r8, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r8
.L111:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L109:
	cmpq	%r13, %r15
	jne	.L112
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L175:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r15
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L107
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r15
.L107:
	movq	%r15, _ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31(%rip)
	mfence
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L174:
	movq	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28(%rip), %r15
	testq	%r15, %r15
	je	.L182
.L98:
	leaq	-136(%rbp), %r14
	testb	$5, (%r15)
	je	.L96
	leaq	.LC2(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$7, -136(%rbp)
	leaq	-112(%rbp), %r13
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	movq	%r15, %rsi
	movq	%r14, %r9
	movq	%rax, %rdi
	leaq	-120(%rbp), %rax
	pushq	$0
	leaq	-128(%rbp), %r8
	pushq	%r13
	movl	$1, %ecx
	leaq	.LC3(%rip), %rdx
	movq	%rbx, %r15
	pushq	%rax
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	addq	$32, %rsp
.L104:
	movq	-8(%r15), %r8
	subq	$8, %r15
	testq	%r8, %r8
	je	.L101
	movq	(%r8), %rdx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rax
	movq	8(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L102
	movq	8(%r8), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r8), %rdx
	movq	%rax, (%r8)
	cmpq	%rdx, %rdi
	je	.L103
	movq	%r8, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %r8
.L103:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L101:
	cmpq	%r13, %r15
	jne	.L104
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L182:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r15
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L99
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r15
.L99:
	movq	%r15, _ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28(%rip)
	mfence
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r8, %rdi
	call	*%rdx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r8, %rdi
	call	*%rdx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%r8, %rdi
	call	*%rdx
	jmp	.L101
.L181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8051:
	.size	_ZN4node22NodeTraceStateObserver14OnTraceEnabledEv, .-_ZN4node22NodeTraceStateObserver14OnTraceEnabledEv
	.text
	.p2align 4
	.globl	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE
	.type	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE, @function
_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE:
.LFB9184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	3280(%rbx), %rdi
	xorl	%r12d, %r12d
	call	_ZN4node13native_module15NativeModuleEnv16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS_11EnvironmentE@PLT
	testq	%rax, %rax
	je	.L188
	movq	(%r15), %r8
	movq	8(%r15), %rcx
	movq	%rax, %rdi
	movq	352(%rbx), %rax
	movq	3280(%rbx), %rsi
	subq	%r8, %rcx
	sarq	$3, %rcx
	leaq	88(%rax), %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L202
.L190:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L188:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	352(%rbx), %r13
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L186
	movq	%rax, -136(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-136(%rbp), %rsi
	movq	$0, 1272(%rbx)
.L186:
	testq	%rsi, %rsi
	je	.L187
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%rbx)
.L187:
	movq	1256(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movups	%xmm0, (%rax)
	movq	1216(%rbx), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L190
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9184:
	.size	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE, .-_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EE
	.type	_ZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EE, @function
_ZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EE:
.LFB9188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	testq	%rsi, %rsi
	je	.L205
	addq	$8, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	(%r12), %rax
	movq	2080(%rbx), %rdi
	movq	$0, (%r12)
	leaq	-144(%rbp), %rsi
	movq	%rax, -144(%rbp)
	call	_ZN4node9inspector5Agent15SetParentHandleESt10unique_ptrINS0_21ParentInspectorHandleESt14default_deleteIS3_EE@PLT
	movq	-144(%rbp), %r12
	testq	%r12, %r12
	je	.L207
	movq	%r12, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L207:
	movq	2080(%rbx), %rdi
	cmpq	$0, 24(%rdi)
	jne	.L234
.L209:
	movl	1932(%rbx), %r8d
	movdqu	1656(%rbx), %xmm1
	movq	1664(%rbx), %rax
	andl	$1, %r8d
	movaps	%xmm1, -144(%rbp)
	testq	%rax, %rax
	je	.L210
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L211
	lock addl	$1, 8(%rax)
.L210:
	movq	1640(%rbx), %rax
	leaq	-144(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rcx
	leaq	672(%rax), %rdx
	call	_ZN4node9inspector5Agent5StartERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_12DebugOptionsESt10shared_ptrINS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEEb@PLT
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L213
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L214
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L235
	.p2align 4,,10
	.p2align 3
.L213:
	movq	1640(%rbx), %rax
	cmpb	$0, 680(%rax)
	je	.L219
	movq	2080(%rbx), %rax
	cmpq	$0, 24(%rax)
	je	.L225
.L219:
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN4node8profiler14StartProfilersEPNS_11EnvironmentE@PLT
	movq	2080(%rbx), %r15
	cmpb	$0, 83(%r15)
	jne	.L236
.L220:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L205:
	movq	1696(%rdi), %rax
	movq	1704(%rdi), %rdx
	xorl	%r8d, %r8d
	leaq	.LC0(%rip), %r12
	subq	%rax, %rdx
	cmpq	$63, %rdx
	jbe	.L208
	movq	32(%rax), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %r8
.L208:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r12, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	2080(%rbx), %rdi
	cmpq	$0, 24(%rdi)
	je	.L209
.L234:
	leaq	_ZZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	-96(%rbp), %r12
	xorl	%edx, %edx
	leaq	-80(%rbp), %rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	$18, -144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC29(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$28769, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-96(%rbp), %rdx
	movq	-144(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	xorl	%r12d, %r12d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L214:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L213
.L235:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L217
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L218:
	cmpl	$1, %eax
	jne	.L213
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L217:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$12, %r12d
	jmp	.L220
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9188:
	.size	_ZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EE, .-_ZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment21InitializeDiagnosticsEv
	.type	_ZN4node11Environment21InitializeDiagnosticsEv, @function
_ZN4node11Environment21InitializeDiagnosticsEv:
.LFB9190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	352(%rdi), %rdi
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler29AddBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES5_@PLT
	movq	1640(%rbx), %rax
	cmpb	$0, 470(%rax)
	jne	.L241
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movl	$15, %ecx
	movl	$10, %edx
	movl	$1, %esi
	movq	352(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate41SetCaptureStackTraceForUncaughtExceptionsEbiNS_10StackTrace17StackTraceOptionsE@PLT
	.cfi_endproc
.LFE9190:
	.size	_ZN4node11Environment21InitializeDiagnosticsEv, .-_ZN4node11Environment21InitializeDiagnosticsEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC30:
	.string	"getLinkedBinding"
.LC31:
	.string	"getInternalBinding"
.LC32:
	.string	"internal/bootstrap/loaders"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment24BootstrapInternalLoadersEv
	.type	_ZN4node11Environment24BootstrapInternalLoadersEv, @function
_ZN4node11Environment24BootstrapInternalLoadersEv:
.LFB9191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	352(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	360(%rbx), %rax
	xorl	%edx, %edx
	movq	352(%rbx), %rdi
	movl	$16, %ecx
	leaq	.LC30(%rip), %rsi
	movq	1416(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L285
.L243:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$18, %ecx
	leaq	.LC31(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L286
.L244:
	movq	%rax, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	360(%rbx), %rax
	movl	$32, %edi
	movaps	%xmm0, -192(%rbp)
	movq	1400(%rax), %rax
	movq	$0, -176(%rbp)
	movq	%rax, -72(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm1
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movdqa	-80(%rbp), %xmm2
	leaq	32(%rax), %rdx
	movq	%rax, -192(%rbp)
	movl	$1, %r9d
	movups	%xmm1, (%rax)
	movq	352(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	_ZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE@GOTPCREL(%rip), %rsi
	movups	%xmm2, 16(%rax)
	movq	2968(%rbx), %rax
	pushq	$0
	movq	%rdx, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	2680(%rbx), %rdx
	movq	%rax, -96(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	3280(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	je	.L287
.L245:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	$0
	movq	352(%rbx), %rdi
	movl	$1, %r9d
	movq	_ZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE@GOTPCREL(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	3280(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L288
.L246:
	movq	%rax, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	2976(%rbx), %rax
	movl	$32, %edi
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movq	%rbx, %rdi
	movdqa	-80(%rbp), %xmm4
	leaq	32(%rax), %rdx
	leaq	-160(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	leaq	.LC32(%rip), %rsi
	movq	%rdx, -152(%rbp)
	leaq	-192(%rbp), %rdx
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	call	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L289
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L290
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	912(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L291
.L249:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L292
	movq	2904(%rbx), %rdi
	movq	352(%rbx), %r15
	testq	%rdi, %rdi
	je	.L251
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2904(%rbx)
.L251:
	testq	%r14, %r14
	je	.L252
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2904(%rbx)
.L252:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	1520(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L293
.L253:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L294
	movq	2936(%rbx), %rdi
	movq	352(%rbx), %r15
	testq	%rdi, %rdi
	je	.L255
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2936(%rbx)
.L255:
	testq	%r14, %r14
	je	.L256
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2936(%rbx)
.L256:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
.L259:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L258
	call	_ZdlPv@PLT
.L258:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rax
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L292:
	leaq	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L291:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L253
.L295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9191:
	.size	_ZN4node11Environment24BootstrapInternalLoadersEv, .-_ZN4node11Environment24BootstrapInternalLoadersEv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"internal/bootstrap/switches/is_main_thread"
	.align 8
.LC34:
	.string	"internal/bootstrap/switches/is_not_main_thread"
	.align 8
.LC35:
	.string	"internal/bootstrap/switches/does_own_process_state"
	.align 8
.LC36:
	.string	"internal/bootstrap/switches/does_not_own_process_state"
	.section	.rodata.str1.1
.LC37:
	.string	"global"
.LC38:
	.string	"internal/bootstrap/node"
.LC39:
	.string	"env"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment13BootstrapNodeEv
	.type	_ZN4node11Environment13BootstrapNodeEv, @function
_ZN4node11Environment13BootstrapNodeEv:
.LFB9192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	352(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC37(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L324
.L297:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L325
.L298:
	movq	360(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$32, %edi
	movq	$0, -160(%rbp)
	movaps	%xmm1, -176(%rbp)
	leaq	-144(%rbp), %r14
	leaq	-176(%rbp), %r13
	movq	1416(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movq	1520(%rax), %rdx
	movq	%rdx, -72(%rbp)
	movq	912(%rax), %rdx
	movq	1400(%rax), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm3
	movdqa	-64(%rbp), %xmm4
	pxor	%xmm1, %xmm1
	movdqu	2968(%rbx), %xmm2
	movdqu	2936(%rbx), %xmm5
	leaq	32(%rax), %rdx
	movl	$32, %edi
	movdqu	2904(%rbx), %xmm0
	movdqu	2968(%rbx), %xmm6
	movups	%xmm3, (%rax)
	punpcklqdq	%xmm5, %xmm2
	movups	%xmm4, 16(%rax)
	movq	%rdx, -160(%rbp)
	shufpd	$2, %xmm6, %xmm0
	movq	%rdx, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	$0, -128(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movaps	%xmm1, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm7
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movdqa	-64(%rbp), %xmm3
	leaq	32(%rax), %rdx
	leaq	.LC38(%rip), %rsi
	movq	%rax, -144(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%r13, %rdx
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	call	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE
	testq	%rax, %rax
	je	.L323
	testb	$1, 1932(%rbx)
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC33(%rip), %rsi
	leaq	.LC34(%rip), %rax
	cmove	%rax, %rsi
	call	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE
	testq	%rax, %rax
	je	.L323
	testb	$2, 1932(%rbx)
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%rbx, %rdi
	leaq	.LC36(%rip), %rax
	leaq	.LC35(%rip), %rsi
	cmove	%rax, %rsi
	call	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L323
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L326
.L305:
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rsi
	movq	3280(%rbx), %rdi
	call	_ZN4node17CreateEnvVarProxyEN2v85LocalINS0_7ContextEEEPNS0_7IsolateENS1_INS0_6ObjectEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L310
	movq	2968(%rbx), %rdi
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L327
.L310:
	xorl	%r13d, %r13d
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L323:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
.L300:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L307
	call	_ZdlPv@PLT
.L307:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L328
	addq	$160, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %rdx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L325:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L326:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L305
.L328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9192:
	.size	_ZN4node11Environment13BootstrapNodeEv, .-_ZN4node11Environment13BootstrapNodeEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment16RunBootstrappingEv
	.type	_ZN4node11Environment16RunBootstrappingEv, @function
_ZN4node11Environment16RunBootstrappingEv:
.LFB9193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	352(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	cmpb	$0, 1928(%rbx)
	jne	.L343
	movq	%rbx, %rdi
	call	_ZN4node11Environment24BootstrapInternalLoadersEv
	testq	%rax, %rax
	je	.L333
	movq	%rbx, %rdi
	call	_ZN4node11Environment13BootstrapNodeEv
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L333
	leaq	2112(%rbx), %rax
	cmpq	%rax, 2112(%rbx)
	jne	.L344
	leaq	2096(%rbx), %rax
	cmpq	%rax, 2096(%rbx)
	jne	.L345
	movb	$1, 1928(%rbx)
	movq	%r12, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L333:
	xorl	%r13d, %r13d
.L332:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	leaq	_ZZN4node11Environment16RunBootstrappingEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L344:
	leaq	_ZZN4node11Environment16RunBootstrappingEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	_ZZN4node11Environment16RunBootstrappingEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L346:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9193:
	.size	_ZN4node11Environment16RunBootstrappingEv, .-_ZN4node11Environment16RunBootstrappingEv
	.section	.rodata.str1.1
.LC40:
	.string	"markBootstrapComplete"
	.text
	.p2align 4
	.globl	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	.type	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc, @function
_ZN4node14StartExecutionEPNS_11EnvironmentEPKc:
.LFB9195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$224, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	testq	%r13, %r13
	je	.L361
	movq	360(%r12), %rax
	movq	352(%r12), %rdi
	movl	$21, %ecx
	leaq	.LC40(%rip), %rsi
	movq	1416(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movq	1520(%rax), %rdx
	movq	%rdx, -72(%rbp)
	movq	912(%rax), %rdx
	movq	1400(%rax), %rax
	movq	%rdx, -64(%rbp)
	xorl	%edx, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L362
.L349:
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	$0, -208(%rbp)
	call	_Znwm@PLT
	movq	-48(%rbp), %rcx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movdqa	-80(%rbp), %xmm1
	movdqa	-64(%rbp), %xmm2
	leaq	40(%rax), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, 32(%rax)
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	352(%r12), %rdi
	movups	%xmm1, (%rax)
	leaq	_ZN4node21MarkBootstrapCompleteERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movups	%xmm2, 16(%rax)
	movq	2968(%r12), %rax
	movq	%rdx, -208(%rbp)
	movq	%rax, -80(%rbp)
	movq	2936(%r12), %rax
	movq	%rdx, -216(%rbp)
	movq	2680(%r12), %rdx
	movq	%rax, -72(%rbp)
	movq	2904(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	2976(%r12), %rax
	pushq	$0
	movq	%rax, -56(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	3280(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L363
.L350:
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -48(%rbp)
	leaq	-128(%rbp), %r15
	movaps	%xmm0, -192(%rbp)
	movq	$0, -176(%rbp)
	call	_Znwm@PLT
	movq	-48(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movdqa	-64(%rbp), %xmm4
	movq	.LC41(%rip), %xmm0
	leaq	40(%rax), %rdx
	movq	%rax, -192(%rbp)
	movq	%rcx, 32(%rax)
	movq	352(%r12), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movaps	%xmm0, -240(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	leaq	-240(%rbp), %rcx
	call	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi@PLT
	leaq	-192(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	-224(%rbp), %rdx
	call	_ZN4node19ExecuteBootstrapperEPNS_11EnvironmentEPKcPSt6vectorIN2v85LocalINS5_6StringEEESaIS8_EEPS4_INS6_INS5_5ValueEEESaISD_EE
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L351
	call	_ZdlPv@PLT
.L351:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	leaq	_ZZN4node14StartExecutionEPNS_11EnvironmentEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%rax, -248(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-248(%rbp), %rax
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%rax, -248(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-248(%rbp), %rax
	jmp	.L350
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9195:
	.size	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc, .-_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	.section	.rodata.str1.1
.LC42:
	.string	"_third_party_main"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"internal/main/run_third_party_main"
	.section	.rodata.str1.1
.LC44:
	.string	"inspect"
.LC45:
	.string	"internal/main/inspect"
.LC46:
	.string	"debug"
.LC47:
	.string	"internal/main/print_help"
.LC48:
	.string	"internal/main/prof_process"
.LC49:
	.string	"internal/main/eval_string"
.LC50:
	.string	"internal/main/check_syntax"
.LC51:
	.string	"-"
.LC52:
	.string	"internal/main/run_main_module"
.LC53:
	.string	"internal/main/repl"
.LC54:
	.string	"internal/main/eval_stdin"
	.text
	.p2align 4
	.globl	_ZN4node24StartMainThreadExecutionEPNS_11EnvironmentE
	.type	_ZN4node24StartMainThreadExecutionEPNS_11EnvironmentE, @function
_ZN4node24StartMainThreadExecutionEPNS_11EnvironmentE:
.LFB9196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	.LC42(%rip), %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13native_module15NativeModuleEnv6ExistsEPKc@PLT
	testb	%al, %al
	jne	.L463
	movq	1696(%r12), %rsi
	leaq	-80(%rbp), %rbx
	movb	$0, -80(%rbp)
	leaq	-96(%rbp), %r13
	movq	1704(%r12), %rax
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	subq	%rsi, %rax
	cmpq	$63, %rax
	ja	.L464
.L368:
	leaq	.LC44(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L369
.L371:
	movq	%r12, %rdi
	leaq	.LC45(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
.L370:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L420
	call	_ZdlPv@PLT
.L420:
	movq	%r12, %rax
.L367:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L465
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	leaq	.LC46(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L371
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpb	$0, 201(%rax)
	je	.L372
	movq	%r12, %rdi
	leaq	.LC47(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L464:
	addq	$32, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L372:
	movq	1648(%r12), %r14
	movq	1640(%r12), %rdx
	testq	%r14, %r14
	je	.L373
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r14), %rax
	testq	%r15, %r15
	je	.L374
	lock addl	$1, (%rax)
.L375:
	movzbl	271(%rdx), %ecx
	testq	%r15, %r15
	je	.L466
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L376:
	cmpl	$1, %eax
	je	.L467
.L377:
	testb	%cl, %cl
	je	.L380
	movq	%r12, %rdi
	leaq	.LC48(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L374:
	addl	$1, 8(%r14)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L380:
	movq	1648(%r12), %r14
	movq	1640(%r12), %rax
	testq	%r14, %r14
	je	.L381
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r14), %rdx
	testq	%r15, %r15
	je	.L382
	lock addl	$1, (%rdx)
.L383:
	movzbl	537(%rax), %eax
	testb	%al, %al
	je	.L434
.L432:
	movq	1648(%r12), %rdi
	movq	1640(%r12), %rax
	testq	%rdi, %rdi
	je	.L384
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%rdi), %rcx
	testq	%r15, %r15
	je	.L385
	lock addl	$1, (%rcx)
.L386:
	movzbl	577(%rax), %eax
	xorl	$1, %eax
	testq	%r15, %r15
	je	.L468
	movl	$-1, %edx
	lock xaddl	%edx, (%rcx)
.L387:
	cmpl	$1, %edx
	je	.L469
.L388:
	testq	%r14, %r14
	je	.L391
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r14), %rdx
.L434:
	testq	%r15, %r15
	je	.L392
	movl	$-1, %ecx
	lock xaddl	%ecx, (%rdx)
	movl	%ecx, %edx
.L393:
	cmpl	$1, %edx
	je	.L470
.L391:
	testb	%al, %al
	je	.L396
	movq	%r12, %rdi
	leaq	.LC49(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L373:
	movzbl	271(%rdx), %ecx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L466:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L467:
	movq	(%r14), %rax
	movb	%cl, -104(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	movzbl	-104(%rbp), %ecx
	je	.L378
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L379:
	cmpl	$1, %eax
	jne	.L377
	movq	(%r14), %rax
	movb	%cl, -104(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movzbl	-104(%rbp), %ecx
	jmp	.L377
.L396:
	movq	1648(%r12), %r14
	movq	1640(%r12), %rax
	testq	%r14, %r14
	je	.L397
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r14), %rdx
	testq	%r15, %r15
	je	.L398
	lock addl	$1, (%rdx)
.L399:
	movzbl	536(%rax), %ecx
	testq	%r15, %r15
	je	.L471
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
.L400:
	cmpl	$1, %eax
	je	.L472
.L401:
	testb	%cl, %cl
	jne	.L473
	cmpq	$0, -88(%rbp)
	je	.L409
	leaq	.LC51(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L474
.L409:
	movq	1648(%r12), %r13
	movq	1640(%r12), %rax
	testq	%r13, %r13
	je	.L475
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r13), %rdx
	testq	%r15, %r15
	je	.L410
	lock addl	$1, (%rdx)
.L411:
	movzbl	577(%rax), %r14d
	testb	%r14b, %r14b
	jne	.L422
	xorl	%edi, %edi
	movq	%rdx, -104(%rbp)
	call	uv_guess_handle@PLT
	movq	-104(%rbp), %rdx
	cmpl	$14, %eax
	sete	%r14b
.L422:
	testq	%r15, %r15
	je	.L412
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
.L413:
	cmpl	$1, %eax
	je	.L476
.L415:
	testb	%r14b, %r14b
	jne	.L421
.L419:
	movq	%r12, %rdi
	leaq	.LC54(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L382:
	addl	$1, 8(%r14)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L381:
	cmpb	$0, 537(%rax)
	jne	.L432
.L397:
	movzbl	536(%rax), %ecx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L392:
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L393
.L470:
	movq	(%r14), %rdx
	movb	%al, -104(%rbp)
	movq	%r14, %rdi
	call	*16(%rdx)
	testq	%r15, %r15
	movzbl	-104(%rbp), %eax
	je	.L394
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r14)
.L395:
	cmpl	$1, %edx
	jne	.L391
	movq	(%r14), %rdx
	movb	%al, -104(%rbp)
	movq	%r14, %rdi
	call	*24(%rdx)
	movzbl	-104(%rbp), %eax
	jmp	.L391
.L378:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L379
.L473:
	movq	%r12, %rdi
	leaq	.LC50(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
	jmp	.L370
.L398:
	addl	$1, 8(%r14)
	jmp	.L399
.L468:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	jmp	.L387
.L475:
	cmpb	$0, 577(%rax)
	jne	.L421
	xorl	%edi, %edi
	call	uv_guess_handle@PLT
	cmpl	$14, %eax
	jne	.L419
.L421:
	movq	%r12, %rdi
	leaq	.LC53(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
	jmp	.L370
.L471:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L400
.L385:
	addl	$1, 8(%rdi)
	jmp	.L386
.L469:
	movq	(%rdi), %rdx
	movb	%al, -105(%rbp)
	movq	%rdi, -104(%rbp)
	call	*16(%rdx)
	testq	%r15, %r15
	movq	-104(%rbp), %rdi
	movzbl	-105(%rbp), %eax
	je	.L389
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L390:
	cmpl	$1, %edx
	jne	.L388
	movq	(%rdi), %rdx
	movb	%al, -104(%rbp)
	call	*24(%rdx)
	movzbl	-104(%rbp), %eax
	jmp	.L388
.L384:
	movzbl	577(%rax), %eax
	xorl	$1, %eax
	jmp	.L388
.L410:
	addl	$1, 8(%r13)
	jmp	.L411
.L472:
	movq	(%r14), %rax
	movb	%cl, -104(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	movzbl	-104(%rbp), %ecx
	je	.L402
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L403:
	cmpl	$1, %eax
	jne	.L401
	movq	(%r14), %rax
	movb	%cl, -104(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movzbl	-104(%rbp), %ecx
	jmp	.L401
.L394:
	movl	12(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r14)
	jmp	.L395
.L412:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L413
.L474:
	movq	%r12, %rdi
	leaq	.LC52(%rip), %rsi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc
	movq	%rax, %r12
	jmp	.L370
.L476:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L416
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L417:
	cmpl	$1, %eax
	jne	.L415
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L415
.L389:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L390
.L402:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L403
.L416:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L417
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9196:
	.size	_ZN4node24StartMainThreadExecutionEPNS_11EnvironmentE, .-_ZN4node24StartMainThreadExecutionEPNS_11EnvironmentE
	.p2align 4
	.globl	_ZN4node15LoadEnvironmentEPNS_11EnvironmentE
	.type	_ZN4node15LoadEnvironmentEPNS_11EnvironmentE, @function
_ZN4node15LoadEnvironmentEPNS_11EnvironmentE:
.LFB9197:
	.cfi_startproc
	endbr64
	testb	$1, 1932(%rdi)
	je	.L482
	jmp	_ZN4node24StartMainThreadExecutionEPNS_11EnvironmentE
	.p2align 4,,10
	.p2align 3
.L482:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node15LoadEnvironmentEPNS_11EnvironmentEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9197:
	.size	_ZN4node15LoadEnvironmentEPNS_11EnvironmentE, .-_ZN4node15LoadEnvironmentEPNS_11EnvironmentE
	.p2align 4
	.globl	_ZN4node21RegisterSignalHandlerEiPFviEb
	.type	_ZN4node21RegisterSignalHandlerEiPFviEb, @function
_ZN4node21RegisterSignalHandlerEiPFviEb:
.LFB9198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$18, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-168(%rbp), %r8
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edi, %r12d
	leaq	-176(%rbp), %r13
	movq	%r8, %rdi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$1, %dl
	movq	%rsi, -176(%rbp)
	rep stosq
	sbbl	%eax, %eax
	movq	%r8, %rdi
	notl	%eax
	andl	$-2147483648, %eax
	movl	%eax, -40(%rbp)
	call	sigfillset@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	sigaction@PLT
	testl	%eax, %eax
	jne	.L489
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L490
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	leaq	_ZZN4node21RegisterSignalHandlerEiPFviEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9198:
	.size	_ZN4node21RegisterSignalHandlerEiPFviEb, .-_ZN4node21RegisterSignalHandlerEiPFviEb
	.p2align 4
	.globl	_ZN4node22TearDownOncePerProcessEv
	.type	_ZN4node22TearDownOncePerProcessEv, @function
_ZN4node22TearDownOncePerProcessEv:
.LFB9218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movb	$0, _ZN4node11per_process14v8_initializedE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	_ZN2v82V87DisposeEv@PLT
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L492
	movl	24+_ZN4node11per_process11v8_platformE(%rip), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L492:
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %rdi
	movq	$0, 16+_ZN4node11per_process11v8_platformE(%rip)
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %r12
	testq	%r12, %r12
	je	.L493
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L494
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L496
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L497
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L498:
	cmpl	$1, %eax
	jne	.L496
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L500
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L501:
	cmpl	$1, %eax
	je	.L534
	.p2align 4,,10
	.p2align 3
.L496:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L502
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L503
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L502
.L503:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L510
	lock subl	$1, 8(%r13)
	jne	.L510
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L510
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L503
	.p2align 4,,10
	.p2align 3
.L502:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L512
	call	_ZdlPv@PLT
.L512:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L493:
	movq	8+_ZN4node11per_process11v8_platformE(%rip), %r12
	movq	$0, 32+_ZN4node11per_process11v8_platformE(%rip)
	movq	$0, 8+_ZN4node11per_process11v8_platformE(%rip)
	testq	%r12, %r12
	je	.L513
	movq	%r12, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L513:
	movq	_ZN4node11per_process11v8_platformE(%rip), %rdi
	movq	$0, _ZN4node11per_process11v8_platformE(%rip)
	testq	%rdi, %rdi
	je	.L491
	movq	(%rdi), %rax
	leaq	_ZN4node22NodeTraceStateObserverD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L515
	popq	%rbx
	movl	$16, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L506
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L506:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L502
.L508:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L506
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L506
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L491:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L515:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L500:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L501
	.cfi_endproc
.LFE9218:
	.size	_ZN4node22TearDownOncePerProcessEv, .-_ZN4node22TearDownOncePerProcessEv
	.p2align 4
	.globl	_ZN4node4StopEPNS_11EnvironmentE
	.type	_ZN4node4StopEPNS_11EnvironmentE, @function
_ZN4node4StopEPNS_11EnvironmentE:
.LFB9229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node11Environment7ExitEnvEv@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9229:
	.size	_ZN4node4StopEPNS_11EnvironmentE, .-_ZN4node4StopEPNS_11EnvironmentE
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC55:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_:
.LFB10276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L538
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	(%rsi), %rdx
	leaq	16(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L562
	movq	%rdx, (%r12)
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%r12)
.L540:
	movq	8(%rbx), %rdx
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movq	%rdx, 8(%r12)
	movb	$0, 16(%rbx)
	addq	$32, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movdqu	16(%rsi), %xmm2
	movups	%xmm2, 16(%r12)
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L538:
	movabsq	$288230376151711743, %rsi
	movq	(%rdi), %r15
	movq	%r12, %rdx
	subq	%r15, %rdx
	movq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L563
	testq	%rax, %rax
	je	.L553
	movabsq	$9223372036854775776, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L564
.L543:
	movq	%rcx, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	leaq	(%rax,%rcx), %rax
	leaq	32(%r14), %rcx
.L544:
	addq	%r14, %rdx
	movq	(%rbx), %rdi
	leaq	16(%rdx), %rsi
	movq	%rsi, (%rdx)
	leaq	16(%rbx), %rsi
	cmpq	%rsi, %rdi
	je	.L565
	movq	%rdi, (%rdx)
	movq	16(%rbx), %rdi
	movq	%rdi, 16(%rdx)
.L546:
	movq	8(%rbx), %rdi
	movq	%rsi, (%rbx)
	movq	$0, 8(%rbx)
	movq	%rdi, 8(%rdx)
	movb	$0, 16(%rbx)
	cmpq	%r15, %r12
	je	.L547
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%rsi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L561:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	je	.L566
.L551:
	leaq	16(%rcx), %rsi
	leaq	16(%rdx), %rdi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rsi
	cmpq	%rdi, %rsi
	jne	.L548
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L566:
	subq	%r15, %r12
	leaq	32(%r14,%r12), %rcx
.L547:
	testq	%r15, %r15
	je	.L552
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rcx
.L552:
	movq	%r14, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L567
	movl	$32, %ecx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L553:
	movl	$32, %ecx
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L565:
	movdqu	16(%rbx), %xmm4
	movups	%xmm4, 16(%rdx)
	jmp	.L546
.L563:
	leaq	.LC55(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L567:
	cmpq	%rsi, %rdi
	movq	%rsi, %rax
	cmovbe	%rdi, %rax
	salq	$5, %rax
	movq	%rax, %rcx
	jmp	.L543
	.cfi_endproc
.LFE10276:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_:
.LFB10299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	%rdi, %rsi
	je	.L569
	movq	8(%rsi), %rbx
	movq	(%rsi), %r12
	movq	(%rdi), %r14
	movq	16(%rdi), %rdx
	movq	%rbx, %rax
	subq	%r12, %rax
	subq	%r14, %rdx
	movq	%rax, %rcx
	sarq	$5, %rdx
	movq	%rax, -80(%rbp)
	sarq	$5, %rcx
	movq	%rcx, %r15
	cmpq	%rcx, %rdx
	jb	.L639
	movq	8(%rdi), %r8
	movq	%r8, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %r15
	ja	.L588
	cmpq	$0, -80(%rbp)
	movq	%r14, %rbx
	jle	.L638
	.p2align 4,,10
	.p2align 3
.L589:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	addq	$32, %rbx
	subq	$1, %r15
	movq	-72(%rbp), %r8
	jne	.L589
	movq	-80(%rbp), %rax
	movl	$32, %edx
	testq	%rax, %rax
	cmovg	%rax, %rdx
	addq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L638:
	cmpq	%r14, %r8
	je	.L636
.L596:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L593
	movq	%r8, -72(%rbp)
	addq	$32, %r14
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	cmpq	%r14, %r8
	jne	.L596
.L636:
	movq	-80(%rbp), %r14
	addq	0(%r13), %r14
.L587:
	movq	%r14, 8(%r13)
.L569:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L640
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L607
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rcx
	ja	.L641
	movq	-80(%rbp), %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
.L571:
	movq	%r12, %r15
	movq	-96(%rbp), %r14
	leaq	-64(%rbp), %r12
	cmpq	%rbx, %r15
	jne	.L581
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L577:
	cmpq	$1, %r8
	jne	.L579
	movzbl	(%r10), %eax
	movb	%al, 16(%r14)
.L580:
	addq	$32, %r15
	movq	%r8, 8(%r14)
	addq	$32, %r14
	movb	$0, (%rdi,%r8)
	cmpq	%r15, %rbx
	je	.L582
.L581:
	leaq	16(%r14), %rdi
	movq	8(%r15), %r8
	movq	%rdi, (%r14)
	movq	(%r15), %r10
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L576
	testq	%r10, %r10
	je	.L600
.L576:
	movq	%r8, -64(%rbp)
	cmpq	$15, %r8
	jbe	.L577
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, -88(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r14)
.L578:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	(%r14), %rdi
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L593:
	addq	$32, %r14
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L588:
	testq	%rcx, %rcx
	jle	.L597
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-72(%rbp), %rdx
	addq	$32, %r14
	subq	$1, %rdx
	jne	.L598
	movq	-88(%rbp), %rax
	movq	8(%r13), %r8
	movq	0(%r13), %r14
	movq	%r8, %rcx
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	subq	%r14, %rcx
.L597:
	leaq	-64(%rbp), %rax
	leaq	(%r12,%rcx), %r15
	addq	-80(%rbp), %r14
	movq	%rax, -88(%rbp)
	cmpq	%rbx, %r15
	jne	.L599
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L643:
	movzbl	(%r14), %eax
	movb	%al, 16(%r8)
.L605:
	addq	$32, %r15
	movq	%r12, 8(%r8)
	addq	$32, %r8
	movb	$0, (%rdi,%r12)
	cmpq	%r15, %rbx
	je	.L636
.L599:
	leaq	16(%r8), %rdi
	movq	8(%r15), %r12
	movq	%rdi, (%r8)
	movq	(%r15), %r14
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L609
	testq	%r14, %r14
	je	.L600
.L609:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L642
	cmpq	$1, %r12
	je	.L643
	testq	%r12, %r12
	je	.L605
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L642:
	movq	-88(%rbp), %rsi
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L603:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r12
	movq	(%r8), %rdi
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L607:
	movq	$0, -96(%rbp)
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L579:
	testq	%r8, %r8
	je	.L580
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L582:
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L574
	.p2align 4,,10
	.p2align 3
.L575:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L583
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L575
.L584:
	movq	0(%r13), %r12
.L574:
	testq	%r12, %r12
	je	.L586
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L586:
	movq	-96(%rbp), %r14
	movq	%r14, 0(%r13)
	addq	-80(%rbp), %r14
	movq	%r14, 16(%r13)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L583:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L575
	jmp	.L584
.L600:
	leaq	.LC56(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L640:
	call	__stack_chk_fail@PLT
.L641:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10299:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB10736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L686
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L666
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L687
	movabsq	$9223372036854775776, %rcx
.L646:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L664:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L648
	testq	%r10, %r10
	je	.L688
.L648:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L689
	cmpq	$1, %r9
	jne	.L651
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L652:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L668
.L693:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L654:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L684:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L690
.L657:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L654
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L687:
	testq	%rcx, %rcx
	jne	.L647
	xorl	%r13d, %r13d
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L690:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L653:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L658
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L662:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L691
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L662
.L660:
	subq	%rbx, %r12
	addq	%r12, %r8
.L658:
	testq	%r14, %r14
	je	.L663
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L663:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L692
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L662
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L666:
	movl	$32, %ecx
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L651:
	testq	%r9, %r9
	jne	.L650
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L693
.L668:
	movq	%r13, %r8
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	%rax, %rdi
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L650:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L652
.L688:
	leaq	.LC56(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L647:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L646
.L692:
	call	__stack_chk_fail@PLT
.L686:
	leaq	.LC55(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10736:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_:
.LFB10301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	subq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	16(%rdi), %rbx
	je	.L695
	movq	8(%rdx), %r15
	cmpq	%rsi, %rbx
	je	.L761
	movq	(%rdx), %rbx
	leaq	-72(%rbp), %rcx
	movq	%rdi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rbx, %rax
	addq	%r15, %rax
	je	.L730
	testq	%rbx, %rbx
	je	.L703
.L730:
	movq	%r15, -104(%rbp)
	cmpq	$15, %r15
	ja	.L762
	cmpq	$1, %r15
	jne	.L707
	movzbl	(%rbx), %eax
	movb	%al, -72(%rbp)
	movq	%rcx, %rax
.L708:
	movq	%r15, -80(%rbp)
	movb	$0, (%rax,%r15)
	movq	8(%r12), %rax
	leaq	16(%rax), %rdx
	movq	-32(%rax), %rsi
	movq	%rdx, (%rax)
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L763
	movq	%rsi, (%rax)
	movq	-16(%rax), %rsi
	movq	%rsi, 16(%rax)
.L710:
	movq	-24(%rax), %rsi
	movb	$0, -16(%rax)
	movq	8(%r12), %rbx
	movq	%rdx, -32(%rax)
	movq	%rsi, 8(%rax)
	movq	$0, -24(%rax)
	leaq	32(%rbx), %rax
	movq	%rax, 8(%r12)
	leaq	-32(%rbx), %rax
	subq	$48, %rbx
	subq	%r13, %rax
	movq	%rax, %r8
	sarq	$5, %r8
	testq	%rax, %rax
	jg	.L721
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	32(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L764
	movq	%rdx, 24(%rbx)
	movq	(%rbx), %rdx
	movq	32(%rbx), %rax
	movq	%r15, 16(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%rdi, %rdi
	je	.L719
	movq	%rdi, -16(%rbx)
	movq	%rax, (%rbx)
.L717:
	movq	-16(%rbx), %rax
	movq	$0, -8(%rbx)
	subq	$32, %rbx
	movb	$0, (%rax)
	subq	$1, %r8
	je	.L720
.L721:
	movq	-16(%rbx), %r15
	movq	16(%rbx), %rdi
	movq	-8(%rbx), %rdx
	cmpq	%r15, %rbx
	jne	.L714
	testq	%rdx, %rdx
	je	.L715
	cmpq	$1, %rdx
	je	.L765
	movq	%rbx, %rsi
	movq	%rcx, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r8
.L715:
	movq	-8(%r15), %rax
	movq	16(%r15), %rdx
	movq	%rax, 24(%r15)
	movb	$0, (%rdx,%rax)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L761:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%rdx), %r13
	movq	%r13, %rax
	addq	%r15, %rax
	je	.L697
	testq	%r13, %r13
	je	.L703
.L697:
	movq	%r15, -104(%rbp)
	cmpq	$15, %r15
	ja	.L766
	cmpq	$1, %r15
	jne	.L700
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L701:
	movq	%r15, 8(%rbx)
	movb	$0, (%rdi,%r15)
	addq	$32, 8(%r12)
.L702:
	movq	(%r12), %rax
	addq	%r14, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L767
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L768
	movq	%rcx, %rax
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	-88(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -72(%rbp)
.L706:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rcx, -120(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r15
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rcx
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L766:
	movq	%rbx, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%rbx)
.L699:
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r15
	movq	(%rbx), %rdi
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L764:
	movq	(%rbx), %rax
	movq	%r15, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movq	%rax, 32(%rbx)
.L719:
	movq	%rbx, -16(%rbx)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L720:
	movq	-88(%rbp), %rax
	movq	0(%r13), %rdi
	movq	-80(%rbp), %rdx
	cmpq	%rcx, %rax
	je	.L769
	leaq	16(%r13), %r8
	movq	-72(%rbp), %rsi
	cmpq	%r8, %rdi
	je	.L770
	movq	%rdx, %xmm0
	movq	%rsi, %xmm1
	movq	16(%r13), %r8
	movq	%rax, 0(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r13)
	testq	%rdi, %rdi
	je	.L726
	movq	%rdi, -88(%rbp)
	movq	%r8, -72(%rbp)
.L724:
	movq	$0, -80(%rbp)
	movb	$0, (%rdi)
	movq	-88(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L702
	call	_ZdlPv@PLT
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L765:
	movzbl	(%rbx), %eax
	movb	%al, (%rdi)
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L695:
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L763:
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, 16(%rax)
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L769:
	testq	%rdx, %rdx
	je	.L722
	cmpq	$1, %rdx
	je	.L771
	movq	%rcx, %rsi
	movq	%rcx, -120(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %rdx
	movq	0(%r13), %rdi
	movq	-120(%rbp), %rcx
.L722:
	movq	%rdx, 8(%r13)
	movb	$0, (%rdi,%rdx)
	movq	-88(%rbp), %rdi
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%rdx, %xmm0
	movq	%rsi, %xmm3
	movq	%rax, 0(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
.L726:
	movq	%rcx, -88(%rbp)
	leaq	-72(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L700:
	testq	%r15, %r15
	je	.L701
	jmp	.L699
.L703:
	leaq	.LC56(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L771:
	movzbl	-72(%rbp), %eax
	movb	%al, (%rdi)
	movq	-80(%rbp), %rdx
	movq	0(%r13), %rdi
	jmp	.L722
.L767:
	call	__stack_chk_fail@PLT
.L768:
	movq	%rcx, %rdi
	jmp	.L706
	.cfi_endproc
.LFE10301:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_
	.section	.text._ZN4node7tracing11TracedValueD2Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD2Ev
	.type	_ZN4node7tracing11TracedValueD2Ev, @function
_ZN4node7tracing11TracedValueD2Ev:
.LFB10758:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L772
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L772:
	ret
	.cfi_endproc
.LFE10758:
	.size	_ZN4node7tracing11TracedValueD2Ev, .-_ZN4node7tracing11TracedValueD2Ev
	.weak	_ZN4node7tracing11TracedValueD1Ev
	.set	_ZN4node7tracing11TracedValueD1Ev,_ZN4node7tracing11TracedValueD2Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB10817:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L789
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L778:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L776
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L774
.L777:
	movq	%rbx, %r12
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L776:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L777
.L774:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE10817:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB10952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movq	%rdx, -72(%rbp)
	movabsq	$288230376151711743, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdx, %rax
	je	.L826
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r15, %r8
	testq	%rax, %rax
	je	.L813
	movabsq	$9223372036854775776, %r14
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L827
.L794:
	movq	%r14, %rdi
	movq	%r8, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rcx
	leaq	(%rax,%r14), %rax
	movq	%rax, -80(%rbp)
	leaq	32(%rcx), %r14
.L811:
	addq	%rcx, %r8
	movq	-72(%rbp), %rdi
	movq	%rcx, -104(%rbp)
	leaq	16(%r8), %r11
	movq	%r8, -88(%rbp)
	movq	%r11, (%r8)
	movq	%r11, -96(%rbp)
	call	strlen@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r11
	cmpq	$15, %rax
	movq	%rax, -64(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rax, %r10
	ja	.L828
	cmpq	$1, %rax
	jne	.L798
	movq	-72(%rbp), %rdi
	movzbl	(%rdi), %edx
	movb	%dl, 16(%r8)
.L799:
	movq	%rax, 8(%r8)
	movb	$0, (%r11,%rax)
	cmpq	%r15, %rbx
	je	.L800
.L830:
	movq	%rcx, %rdx
	movq	%r15, %rax
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L801:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L824:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L829
.L804:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L801
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L798:
	testq	%rax, %rax
	jne	.L797
	movq	%rax, 8(%r8)
	movb	$0, (%r11,%rax)
	cmpq	%r15, %rbx
	jne	.L830
	.p2align 4,,10
	.p2align 3
.L800:
	cmpq	%r12, %rbx
	je	.L805
	movq	%rbx, %rax
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L809:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L831
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L809
.L807:
	subq	%rbx, %r12
	addq	%r12, %r14
.L805:
	testq	%r15, %r15
	je	.L810
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L810:
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%r14, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L832
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L795
	movq	$0, -80(%rbp)
	movl	$32, %r14d
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L831:
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L809
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L829:
	movq	%rbx, %rax
	subq	%r15, %rax
	leaq	32(%rcx,%rax), %r14
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L813:
	movl	$32, %r14d
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L828:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	%rax, %r11
	movq	-104(%rbp), %r10
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L797:
	movq	-72(%rbp), %rsi
	movq	%r11, %rdi
	movq	%r10, %rdx
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %r8
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	(%r8), %r11
	jmp	.L799
.L795:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	salq	$5, %rdx
	movq	%rdx, %r14
	jmp	.L794
.L832:
	call	__stack_chk_fail@PLT
.L826:
	leaq	.LC55(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10952:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.rodata._ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC57:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm, @function
_ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm:
.LFB10978:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L852
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L835
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L855
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L856
	testq	%r8, %r8
	jne	.L839
.L840:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L856:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L839:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L840
.L855:
	leaq	.LC57(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10978:
	.size	_ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm, .-_ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag:
.LFB11620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	%rdi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$7, %r13
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	sarq	$5, %rax
	subq	$8, %rsp
	testq	%r13, %r13
	jle	.L858
	salq	$7, %r13
	addq	%rdi, %r13
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L859:
	leaq	32(%rbx), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L882
	leaq	64(%rbx), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L882
	leaq	96(%rbx), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L882
	subq	$-128, %rbx
	cmpq	%r13, %rbx
	je	.L883
.L864:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L859
.L881:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	%r12, %rax
.L860:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
.L858:
	cmpq	$2, %rax
	je	.L865
	cmpq	$3, %rax
	je	.L866
	cmpq	$1, %rax
	je	.L867
.L868:
	movq	%r14, %rax
	jmp	.L860
.L866:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L881
	addq	$32, %rbx
.L865:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L881
	addq	$32, %rbx
.L867:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L868
	jmp	.L881
	.cfi_endproc
.LFE11620:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag:
.LFB11624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	subq	%rdi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$7, %r13
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	sarq	$5, %rax
	subq	$8, %rsp
	testq	%r13, %r13
	jle	.L885
	salq	$7, %r13
	addq	%rdi, %r13
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L886:
	leaq	32(%rbx), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L909
	leaq	64(%rbx), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L909
	leaq	96(%rbx), %r12
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L909
	subq	$-128, %rbx
	cmpq	%r13, %rbx
	je	.L910
.L891:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L886
.L908:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	movq	%r12, %rax
.L887:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$5, %rax
.L885:
	cmpq	$2, %rax
	je	.L892
	cmpq	$3, %rax
	je	.L893
	cmpq	$1, %rax
	je	.L894
.L895:
	movq	%r14, %rax
	jmp	.L887
.L893:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L908
	addq	$32, %rbx
.L892:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L908
	addq	$32, %rbx
.L894:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L895
	jmp	.L908
	.cfi_endproc
.LFE11624:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag
	.section	.rodata.str1.1
.LC58:
	.string	"CVE-2019-9512"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"CVE-2019-9512: HTTP/2 Ping/Settings Flood"
	.align 8
.LC60:
	.string	"SECURITY WARNING: Reverting %s\n"
	.section	.rodata.str1.1
.LC61:
	.string	"CVE-2019-9514"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"CVE-2019-9514: HTTP/2 Reset Flood"
	.section	.rodata.str1.1
.LC63:
	.string	"CVE-2019-9516"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"CVE-2019-9516: HTTP/2 0-Length Headers Leak"
	.section	.rodata.str1.1
.LC65:
	.string	"CVE-2019-9518"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"CVE-2019-9518: HTTP/2 Empty DATA Frame Flooding"
	.align 8
.LC67:
	.string	"Error: Attempt to revert an unknown CVE ["
	.section	.rodata.str1.1
.LC68:
	.string	"basic_string::append"
.LC69:
	.string	"delete"
.LC70:
	.string	"throw"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"invalid mode passed to --disable-proto"
	.section	.rodata.str1.1
.LC72:
	.string	"--abort-on-uncaught-exception"
.LC73:
	.string	"--abort_on_uncaught_exception"
.LC74:
	.string	"--prof"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata.str1.1
.LC76:
	.string	"bad option: "
	.text
	.p2align 4
	.globl	_ZN4node17ProcessGlobalArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_NS_20OptionEnvvarSettingsE
	.type	_ZN4node17ProcessGlobalArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_NS_20OptionEnvvarSettingsE, @function
_ZN4node17ProcessGlobalArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_NS_20OptionEnvvarSettingsE:
.LFB9201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -208(%rbp)
	call	uv_mutex_lock@PLT
	movq	%r15, %r9
	movl	%ebx, %r8d
	movq	%r13, %rsi
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rcx
	leaq	-224(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN4node14options_parser5ParseINS_17PerProcessOptionsENS_7OptionsEEEvPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EESD_SD_PT_NS_20OptionEnvvarSettingsESD_@PLT
	movq	(%r15), %rax
	cmpq	%rax, 8(%r15)
	jne	.L975
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rdi
	leaq	-144(%rbp), %rax
	movq	$0, -152(%rbp)
	movq	%rax, -256(%rbp)
	movq	176(%rdi), %r10
	movq	184(%rdi), %rbx
	movq	%rax, -160(%rbp)
	movb	$0, -144(%rbp)
	cmpq	%rbx, %r10
	je	.L913
	movq	%r15, -248(%rbp)
	movq	%r10, %r14
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L914:
	movl	$14, %ecx
	movq	%r12, %rsi
	leaq	.LC61(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1003
	movl	$14, %ecx
	movq	%r12, %rsi
	leaq	.LC63(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1004
	movl	$14, %ecx
	movq	%r12, %rsi
	leaq	.LC65(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1005
	leaq	-160(%rbp), %r15
	xorl	%edx, %edx
	movl	$41, %r8d
	xorl	%esi, %esi
	leaq	.LC67(%rip), %rcx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	-152(%rbp), %rax
	cmpq	%rax, %rdx
	ja	.L1006
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-152(%rbp), %r12
	movl	$15, %eax
	movq	-160(%rbp), %rdx
	cmpq	-256(%rbp), %rdx
	cmovne	-144(%rbp), %rax
	leaq	1(%r12), %r13
	cmpq	%rax, %r13
	ja	.L1007
.L921:
	movb	$93, (%rdx,%r12)
	movq	-160(%rbp), %rax
	movq	%r13, -152(%rbp)
	movb	$0, 1(%rax,%r12)
.L915:
	cmpq	$0, -152(%rbp)
	jne	.L1008
.L922:
	addq	$32, %r14
	cmpq	%r14, %rbx
	je	.L1009
.L924:
	movq	(%r14), %r12
	movl	$14, %ecx
	leaq	.LC58(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L914
	leaq	.LC59(%rip), %rdx
	leaq	.LC60(%rip), %rsi
	movl	$1, %edi
	orl	$1, _ZN4node11per_process12reverted_cveE(%rip)
	call	__printf_chk@PLT
	cmpq	$0, -152(%rbp)
	je	.L922
.L1008:
	movq	-248(%rbp), %r15
	leaq	-160(%rbp), %rsi
	movl	$12, %r13d
	movq	%r15, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
.L923:
	movq	-160(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L912
	call	_ZdlPv@PLT
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L1005:
	leaq	.LC66(%rip), %rdx
	leaq	.LC60(%rip), %rsi
	movl	$1, %edi
	orl	$8, _ZN4node11per_process12reverted_cveE(%rip)
	call	__printf_chk@PLT
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L1003:
	leaq	.LC62(%rip), %rdx
	leaq	.LC60(%rip), %rsi
	movl	$1, %edi
	orl	$2, _ZN4node11per_process12reverted_cveE(%rip)
	call	__printf_chk@PLT
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L1004:
	leaq	.LC64(%rip), %rdx
	leaq	.LC60(%rip), %rsi
	movl	$1, %edi
	orl	$4, _ZN4node11per_process12reverted_cveE(%rip)
	call	__printf_chk@PLT
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L1007:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-160(%rbp), %rdx
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L975:
	movl	$9, %r13d
.L912:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L967
	.p2align 4,,10
	.p2align 3
.L971:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L968
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L971
.L969:
	movq	-224(%rbp), %r12
.L967:
	testq	%r12, %r12
	je	.L911
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L911:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1010
	addq	$232, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L971
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	-248(%rbp), %r15
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rdi
.L913:
	addq	$144, %rdi
	leaq	.LC69(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L925
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	.LC70(%rip), %rsi
	leaq	144(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L925
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	.LC0(%rip), %rsi
	leaq	144(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1011
	.p2align 4,,10
	.p2align 3
.L925:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L927
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L928
	lock addl	$1, 8(%rax)
.L927:
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %rdi
	leaq	.LC72(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag
	cmpq	%rax, %rbx
	je	.L929
.L931:
	movb	$1, 8(%r12)
.L930:
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %rdi
	leaq	.LC74(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA7_KcEEET_SI_SI_T0_St26random_access_iterator_tag
	cmpq	%rax, %rbx
	je	.L932
	movb	$1, _ZN4node11per_process15v8_is_profilingE(%rip)
.L933:
	call	uv_default_loop@PLT
	movl	$27, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	uv_loop_configure@PLT
.L934:
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rcx
	movabsq	$1152921504606846975, %rsi
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	ja	.L1012
	pxor	%xmm0, %xmm0
	leaq	0(,%rax,8), %r12
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	testq	%rax, %rax
	je	.L936
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r12), %rbx
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	movq	%rbx, -176(%rbp)
	call	memset@PLT
	movq	-216(%rbp), %rdx
	movq	%rbx, -184(%rbp)
	movq	%rax, %r8
	movq	-224(%rbp), %rax
	movq	%rdx, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rdi
	sarq	$5, %rdi
	cmpq	%rax, %rdx
	je	.L973
	testq	%rdi, %rdi
	je	.L943
	movq	%r8, %rdx
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L942:
	movq	(%rax), %rcx
	addq	$32, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rsi, %rax
	jne	.L942
.L943:
	movq	%r8, %rsi
	movl	%edi, -232(%rbp)
	movl	$1, %edx
	leaq	-232(%rbp), %rdi
	call	_ZN2v82V823SetFlagsFromCommandLineEPiPPcb@PLT
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %r8
	movslq	-232(%rbp), %rsi
	movq	%rdx, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rsi
	ja	.L1013
	jnb	.L938
	salq	$3, %rsi
	leaq	(%r8,%rsi), %rcx
	cmpq	%rcx, %rdx
	je	.L938
	movq	%rsi, %rax
	movq	%rcx, -184(%rbp)
	sarq	$3, %rax
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L932:
	cmpb	$0, _ZN4node11per_process15v8_is_profilingE(%rip)
	je	.L934
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L928:
	addl	$1, 8(%rax)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L936:
	movq	$0, -184(%rbp)
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	cmpq	%rdx, %rcx
	jne	.L943
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%rbx, %rax
	subq	%r8, %rax
	sarq	$3, %rax
.L938:
	cmpq	$1, %rax
	jbe	.L977
	leaq	-232(%rbp), %rax
	movl	$1, %ebx
	leaq	-128(%rbp), %r13
	movq	%rax, -264(%rbp)
	leaq	-112(%rbp), %r12
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L1018:
	movzbl	(%r8), %edx
	movb	%dl, -112(%rbp)
	movq	%r12, %rdx
.L950:
	movq	%rax, -120(%rbp)
	leaq	.LC76(%rip), %rcx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movb	$0, (%rdx,%rax)
	movl	$12, %r8d
	xorl	%edx, %edx
	leaq	-80(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r14, -96(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1014
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L952:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%r15, %rdi
	leaq	-96(%rbp), %rsi
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L953
	call	_ZdlPv@PLT
.L953:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rax
	addq	$1, %rbx
	subq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L1015
.L957:
	movq	(%r8,%rbx,8), %r8
	movq	%r12, -128(%rbp)
	testq	%r8, %r8
	je	.L1016
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	strlen@PLT
	movq	-248(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -232(%rbp)
	movq	%rax, %r14
	ja	.L1017
	cmpq	$1, %rax
	je	.L1018
	testq	%rax, %rax
	jne	.L1019
	movq	%r12, %rdx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	-264(%rbp), %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-248(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, -112(%rbp)
.L948:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1016:
	leaq	.LC56(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1014:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -80(%rbp)
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1015:
	cmpq	$2, %rax
	sbbl	%r13d, %r13d
	notl	%r13d
	andl	$9, %r13d
.L945:
	testq	%r8, %r8
	je	.L958
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L958:
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L923
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L961
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rax)
	movl	%edx, %eax
.L962:
	cmpl	$1, %eax
	jne	.L923
	movq	-272(%rbp), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L964
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L965:
	cmpl	$1, %eax
	jne	.L923
	movq	-272(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L961:
	movq	%rax, %rcx
	movl	8(%rax), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rcx)
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1013:
	subq	%rax, %rsi
	leaq	-192(%rbp), %rdi
	call	_ZNSt6vectorIPcSaIS0_EE17_M_default_appendEm
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rax
	subq	%r8, %rax
	sarq	$3, %rax
	jmp	.L938
.L929:
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %rdi
	leaq	.LC73(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt6vectorIS7_SaIS7_EEEENS0_5__ops16_Iter_equals_valIA30_KcEEET_SI_SI_T0_St26random_access_iterator_tag
	cmpq	%rax, %rbx
	jne	.L931
	jmp	.L930
.L964:
	movq	-272(%rbp), %rbx
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L965
.L1011:
	movq	8(%r15), %r12
	cmpq	16(%r15), %r12
	je	.L926
	leaq	16(%r12), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$12, %r13d
	movq	%rax, (%r12)
	leaq	-192(%rbp), %rsi
	movq	$38, -192(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-192(%rbp), %rdx
	movdqa	.LC77(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28532, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC78(%rip), %xmm0
	movl	$1869770797, 32(%rax)
	movw	%dx, 36(%rax)
	movups	%xmm0, 16(%rax)
	movq	-192(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	addq	$32, 8(%r15)
	jmp	.L923
.L977:
	xorl	%r13d, %r13d
	jmp	.L945
.L926:
	leaq	.LC71(%rip), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	$12, %r13d
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA39_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L923
.L1006:
	leaq	.LC68(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1010:
	call	__stack_chk_fail@PLT
.L1012:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1019:
	movq	%r12, %rdi
	jmp	.L948
	.cfi_endproc
.LFE9201:
	.size	_ZN4node17ProcessGlobalArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_NS_20OptionEnvvarSettingsE, .-_ZN4node17ProcessGlobalArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_NS_20OptionEnvvarSettingsE
	.section	.rodata.str1.1
.LC79:
	.string	"NODE_OPTIONS"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata.str1.1
.LC81:
	.string	"NODE_ICU_DATA"
	.text
	.p2align 4
	.globl	_ZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_
	.type	_ZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_, @function
_ZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_:
.LFB9203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	xchgb	_ZN4nodeL11init_calledE(%rip), %al
	testb	%al, %al
	jne	.L1067
	movq	%rdi, %r13
	movq	%rdx, %r12
	call	uv_hrtime@PLT
	movq	%rax, _ZN4node11per_process15node_start_timeE(%rip)
	call	_ZN4node7binding22RegisterBuiltinModulesEv@PLT
	call	uv_disable_stdio_inheritance@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	%r13, %rsi
	leaq	416(%rax), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	movq	%rdx, -160(%rbp)
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L1022
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1023
	lock addl	$1, 8(%rax)
.L1022:
	leaq	-160(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN4node16HandleEnvOptionsESt10shared_ptrINS_18EnvironmentOptionsEE@PLT
	movq	-152(%rbp), %r14
	testq	%r14, %r14
	je	.L1025
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1026
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L1068
	.p2align 4,,10
	.p2align 3
.L1025:
	leaq	-128(%rbp), %r14
	leaq	-112(%rbp), %rbx
	xorl	%edx, %edx
	movb	$0, -112(%rbp)
	movq	%r14, %rsi
	leaq	.LC79(%rip), %rdi
	movq	%rbx, -128(%rbp)
	movq	$0, -120(%rbp)
	call	_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE@PLT
	testb	%al, %al
	je	.L1031
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	$9, %r14d
	call	_ZN4node22ParseNodeOptionsEnvVarERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSt6vectorIS5_SaIS5_EE@PLT
	movq	8(%r12), %rax
	cmpq	%rax, (%r12)
	je	.L1069
.L1032:
	movq	-152(%rbp), %r13
	movq	-160(%rbp), %r12
	cmpq	%r12, %r13
	je	.L1040
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1041
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L1044
.L1042:
	movq	-160(%rbp), %r12
.L1040:
	testq	%r12, %r12
	je	.L1046
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1046:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1020
.L1075:
	call	_ZdlPv@PLT
.L1020:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1070
	addq	$136, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	movq	0(%r13), %rdx
	cmpq	8(%r13), %rdx
	je	.L1071
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE6insertEN9__gnu_cxx17__normal_iteratorIPKS5_S7_EERSA_
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN4node17ProcessGlobalArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_NS_20OptionEnvvarSettingsE
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1032
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r14
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rax
	je	.L1034
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1035
	call	_ZdlPv@PLT
	addq	$32, %r14
	cmpq	%r14, -176(%rbp)
	jne	.L1038
.L1036:
	movq	-160(%rbp), %r14
.L1034:
	testq	%r14, %r14
	je	.L1031
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	-168(%rbp), %rsi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node17ProcessGlobalArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_NS_20OptionEnvvarSettingsE
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L1046
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpq	$0, 32(%rax)
	jne	.L1072
.L1047:
	cmpq	$0, 216(%rax)
	leaq	208(%rax), %rdi
	je	.L1073
.L1048:
	call	_ZN4node4i18n22InitializeICUDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	testb	%al, %al
	je	.L1074
	leaq	_ZN4node11per_process8metadataE(%rip), %rdi
	call	_ZN4node8Metadata8Versions22InitializeIntlVersionsEv@PLT
	call	_ZN4node13native_module15NativeModuleEnv19InitializeCodeCacheEv@PLT
	movq	-128(%rbp), %rdi
	movb	$1, _ZN4node19node_is_initializedE(%rip)
	cmpq	%rbx, %rdi
	jne	.L1075
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1023:
	addl	$1, 8(%rax)
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1041:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1044
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1026:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L1025
.L1068:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1029
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1030:
	cmpl	$1, %eax
	jne	.L1025
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1035:
	addq	$32, %r14
	cmpq	%r14, -176(%rbp)
	jne	.L1038
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	24(%rax), %rdi
	call	uv_set_process_title@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	leaq	.LC81(%rip), %rdi
	call	_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	208(%rax), %rdi
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1067:
	leaq	_ZZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1074:
	leaq	-96(%rbp), %r14
	xorl	%edx, %edx
	leaq	-80(%rbp), %r13
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	$76, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	.LC82(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movabsq	$7310579615589884272, %rcx
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC83(%rip), %xmm0
	movq	-96(%rbp), %rdx
	movq	%rcx, 64(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC84(%rip), %xmm0
	movl	$170488690, 72(%rax)
	movups	%xmm0, 32(%rax)
	movdqa	.LC85(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movq	-160(%rbp), %rax
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movl	$9, %r14d
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1029:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1030
.L1070:
	call	__stack_chk_fail@PLT
.L1071:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC80(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE9203:
	.size	_ZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_, .-_ZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_
	.section	.rodata.str1.1
.LC86:
	.string	"%s: %s\n"
.LC87:
	.string	"v12.18.4"
.LC88:
	.string	"--help"
	.text
	.p2align 4
	.globl	_ZN4node4InitEPiPPKcS0_PS3_
	.type	_ZN4node4InitEPiPPKcS0_PS3_, @function
_ZN4node4InitEPiPPKcS0_PS3_:
.LFB9204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -224(%rbp)
	movabsq	$288230376151711743, %rdx
	movq	%rdi, -216(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	(%rdi), %rax
	movaps	%xmm0, -192(%rbp)
	movq	$0, -176(%rbp)
	salq	$3, %rax
	leaq	(%rsi,%rax), %rbx
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L1151
	movq	%rax, %r12
	xorl	%r15d, %r15d
	salq	$5, %r12
	testq	%rax, %rax
	je	.L1078
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%rax, %r15
.L1078:
	movq	-208(%rbp), %rax
	addq	%r15, %r12
	movq	%r15, -192(%rbp)
	movq	%r12, -176(%rbp)
	cmpq	%rax, %rbx
	je	.L1079
	movq	%rax, %r12
	leaq	-128(%rbp), %rax
	movq	%rax, -232(%rbp)
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1081:
	cmpq	$1, %rax
	jne	.L1083
	movzbl	0(%r13), %edx
	movb	%dl, 16(%r15)
.L1084:
	addq	$8, %r12
	movq	%rax, 8(%r15)
	addq	$32, %r15
	movb	$0, (%r14,%rax)
	cmpq	%r12, %rbx
	je	.L1079
.L1085:
	movq	(%r12), %r13
	leaq	16(%r15), %r14
	movq	%r14, (%r15)
	testq	%r13, %r13
	je	.L1152
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	jbe	.L1081
	movq	-232(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, -200(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-200(%rbp), %r8
	movq	%rax, (%r15)
	movq	%rax, %r14
	movq	-128(%rbp), %rax
	movq	%rax, 16(%r15)
.L1082:
	movq	%r14, %rdi
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-128(%rbp), %rax
	movq	(%r15), %r14
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1083:
	testq	%rax, %rax
	je	.L1084
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1152:
	leaq	.LC56(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1079:
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdx
	leaq	-160(%rbp), %rsi
	movq	%r15, -184(%rbp)
	leaq	-192(%rbp), %rdi
	movq	$0, -112(%rbp)
	leaq	.LC86(%rip), %r12
	movq	$0, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r15
	movl	%eax, %r14d
	cmpq	%rbx, %r15
	je	.L1091
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	(%r15), %r8
	movq	-192(%rbp), %rax
	cmpq	-184(%rbp), %rax
	je	.L1153
	movq	(%rax), %rcx
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	addq	$32, %r15
	call	__fprintf_chk@PLT
	cmpq	%r15, %rbx
	jne	.L1090
.L1091:
	testl	%r14d, %r14d
	jne	.L1154
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpb	$0, 203(%rax)
	jne	.L1155
	cmpb	$0, 200(%rax)
	jne	.L1156
	cmpb	$0, 202(%rax)
	jne	.L1157
	movq	-184(%rbp), %rax
	movq	-216(%rbp), %rcx
	movabsq	$2305843009213693951, %rdx
	subq	-192(%rbp), %rax
	movq	-152(%rbp), %r12
	sarq	$5, %rax
	subq	-160(%rbp), %r12
	movl	%eax, (%rcx)
	movq	-224(%rbp), %rax
	sarq	$5, %r12
	movl	%r12d, (%rax)
	movslq	%r12d, %r12
	movl	$1, %eax
	testq	%r12, %r12
	cmovne	%r12, %rax
	andq	%rax, %rdx
	leaq	0(,%rax,8), %rdi
	cmpq	%rdx, %rax
	jne	.L1158
	testq	%rdi, %rdi
	je	.L1124
	movq	%rdi, -200(%rbp)
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1159
.L1097:
	movq	-240(%rbp), %rax
	movq	-160(%rbp), %r13
	xorl	%r12d, %r12d
	movq	%rbx, (%rax)
	movq	-224(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L1102
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	%r12, %rax
	salq	$5, %rax
	movq	0(%r13,%rax), %rdi
	call	strdup@PLT
	movq	%rax, (%rbx,%r12,8)
	movq	-224(%rbp), %rax
	addq	$1, %r12
	cmpl	%r12d, (%rax)
	jg	.L1103
.L1102:
	movq	-216(%rbp), %rax
	movq	-192(%rbp), %r12
	xorl	%ebx, %ebx
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L1104
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	%rbx, %rax
	salq	$5, %rax
	movq	(%r12,%rax), %rdi
	call	strdup@PLT
	movq	-208(%rbp), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	-216(%rbp), %rax
	addq	$1, %rbx
	cmpl	%ebx, (%rax)
	jg	.L1105
.L1104:
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1100
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1106
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1101
.L1107:
	movq	-128(%rbp), %r12
.L1100:
	testq	%r12, %r12
	je	.L1109
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1109:
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1110
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1111
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1114
.L1112:
	movq	-160(%rbp), %r12
.L1110:
	testq	%r12, %r12
	je	.L1115
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1115:
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1116
	.p2align 4,,10
	.p2align 3
.L1120:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1117
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1120
.L1118:
	movq	-192(%rbp), %r12
.L1116:
	testq	%r12, %r12
	je	.L1076
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1076:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1160
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1156:
	.cfi_restore_state
	leaq	-96(%rbp), %rdi
	call	_ZN4node14options_parser17GetBashCompletionB5cxx11Ev@PLT
	movq	-96(%rbp), %rdi
	call	puts@PLT
	xorl	%edi, %edi
	call	exit@PLT
	.p2align 4,,10
	.p2align 3
.L1117:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1120
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1106:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1101
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1111:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1114
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1155:
	leaq	.LC87(%rip), %rdi
	call	puts@PLT
	xorl	%edi, %edi
	call	exit@PLT
.L1124:
	movl	$1, %eax
	xorl	%ebx, %ebx
.L1096:
	testq	%r12, %r12
	je	.L1097
	testb	%al, %al
	je	.L1097
	leaq	_ZZN4node6MallocIPKcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1158:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1159:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-200(%rbp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	sete	%al
	jmp	.L1096
.L1153:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC80(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1154:
	movl	%r14d, %edi
	call	exit@PLT
.L1157:
	leaq	.LC88(%rip), %rdi
	movl	$6, %esi
	call	_ZN2v82V818SetFlagsFromStringEPKci@PLT
	leaq	_ZZN4node4InitEPiPPKcS0_PS3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1151:
	leaq	.LC75(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9204:
	.size	_ZN4node4InitEPiPPKcS0_PS3_, .-_ZN4node4InitEPiPPKcS0_PS3_
	.section	.text._ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_
	.type	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_, @function
_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_:
.LFB11676:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1175
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1171
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1176
.L1163:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1170:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1177
	testq	%r13, %r13
	jg	.L1166
	testq	%r9, %r9
	jne	.L1169
.L1167:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1177:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1166
.L1169:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1176:
	testq	%rsi, %rsi
	jne	.L1164
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1167
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	$8, %r14d
	jmp	.L1163
.L1175:
	leaq	.LC55(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1164:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1163
	.cfi_endproc
.LFE11676:
	.size	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_, .-_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_:
.LFB12389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L1204
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L1182
.L1205:
	movq	%rax, %r15
.L1181:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L1183
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L1184
.L1183:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L1185
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L1186
.L1184:
	testl	%eax, %eax
	js	.L1186
.L1185:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L1205
.L1182:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L1180
.L1188:
	testq	%rdx, %rdx
	je	.L1191
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L1192
.L1191:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L1193
	cmpq	$-2147483648, %rcx
	jl	.L1194
	movl	%ecx, %eax
.L1192:
	testl	%eax, %eax
	js	.L1194
.L1193:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1204:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L1180:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L1206
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1206:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12389:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.section	.text._ZN4node10V8Platform17StartTracingAgentEv,"axG",@progbits,_ZN4node10V8Platform17StartTracingAgentEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10V8Platform17StartTracingAgentEv
	.type	_ZN4node10V8Platform17StartTracingAgentEv, @function
_ZN4node10V8Platform17StartTracingAgentEv:
.LFB8066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 16(%rdi)
	je	.L1207
	cmpl	$-1, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1269
.L1207:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1270
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	.cfi_restore_state
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movl	$44, %edx
	leaq	-144(%rbp), %rdi
	leaq	56(%rax), %rsi
	call	_ZN4node11SplitStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEc@PLT
	movq	8(%rbx), %rax
	movl	$1416, %edi
	movq	%rax, -208(%rbp)
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	88(%rax), %r13
	call	_Znwm@PLT
	movq	%r13, %rsi
	leaq	-112(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing15NodeTraceWriterC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-136(%rbp), %r14
	movq	-144(%rbp), %r8
	movq	%r12, -168(%rbp)
	leaq	-104(%rbp), %r12
	movl	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -72(%rbp)
	cmpq	%r14, %r8
	je	.L1209
	movq	%rbx, -216(%rbp)
	leaq	16(%r14), %rax
	leaq	16(%r8), %r15
	movq	%rax, -184(%rbp)
	leaq	-112(%rbp), %r13
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1221:
	leaq	-16(%r15), %r14
	testq	%rax, %rax
	je	.L1210
	movq	-80(%rbp), %rbx
	movq	-8(%r15), %r10
	movq	40(%rbx), %rcx
	movq	%r10, %rdx
	cmpq	%r10, %rcx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L1211
	movq	-16(%r15), %rsi
	movq	32(%rbx), %rdi
	movq	%r10, -200(%rbp)
	movq	%rcx, -192(%rbp)
	call	memcmp@PLT
	movq	-192(%rbp), %rcx
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jne	.L1212
.L1211:
	subq	%r10, %rcx
	movl	$2147483648, %eax
	cmpq	%rax, %rcx
	jge	.L1210
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L1213
	movl	%ecx, %eax
.L1212:
	testl	%eax, %eax
	js	.L1213
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	je	.L1215
	testq	%rax, %rax
	setne	%al
.L1214:
	cmpq	%r12, %rbx
	je	.L1238
	testb	%al, %al
	je	.L1271
.L1238:
	movl	$1, %r14d
.L1216:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	%rax, %rsi
	leaq	48(%rax), %rax
	movq	%rax, 32(%rsi)
	movq	-16(%r15), %rax
	cmpq	%r15, %rax
	je	.L1272
	movq	%rax, 32(%rsi)
	movq	(%r15), %rax
	movq	%rax, 48(%rsi)
.L1220:
	movq	-8(%r15), %rax
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movl	%r14d, %edi
	movq	%rax, 40(%rsi)
	movq	%r15, -16(%r15)
	movq	$0, -8(%r15)
	movb	$0, (%r15)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -72(%rbp)
.L1215:
	addq	$32, %r15
	cmpq	%r15, -184(%rbp)
	je	.L1266
	movq	-72(%rbp), %rax
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1272:
	movdqu	(%r15), %xmm0
	movups	%xmm0, 48(%rsi)
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	-216(%rbp), %rbx
.L1209:
	movq	-208(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	leaq	-168(%rbp), %rcx
	call	_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1222
	movl	24(%rbx), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L1222:
	movq	-160(%rbp), %rax
	movq	-96(%rbp), %r12
	movq	%rax, 16(%rbx)
	movl	-152(%rbp), %eax
	movl	%eax, 24(%rbx)
	testq	%r12, %r12
	je	.L1227
.L1223:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1226
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1227
.L1228:
	movq	%rbx, %r12
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1228
.L1227:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1225
	movq	(%rdi), %rax
	call	*8(%rax)
.L1225:
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1229
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1230
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1233
.L1231:
	movq	-144(%rbp), %r12
.L1229:
	testq	%r12, %r12
	je	.L1207
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1230:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1233
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	-8(%r15), %rcx
	movq	40(%rbx), %r14
	cmpq	%r14, %rcx
	movq	%r14, %rdx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L1217
	movq	32(%rbx), %rsi
	movq	-16(%r15), %rdi
	movq	%rcx, -192(%rbp)
	call	memcmp@PLT
	movq	-192(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %r9d
	jne	.L1218
.L1217:
	subq	%r14, %rcx
	movl	$2147483648, %eax
	xorl	%r14d, %r14d
	cmpq	%rax, %rcx
	jge	.L1216
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L1238
	movl	%ecx, %r9d
.L1218:
	shrl	$31, %r9d
	movl	%r9d, %r14d
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1213:
	xorl	%eax, %eax
	jmp	.L1214
.L1270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8066:
	.size	_ZN4node10V8Platform17StartTracingAgentEv, .-_ZN4node10V8Platform17StartTracingAgentEv
	.section	.rodata.str1.1
.LC89:
	.string	"/dev/null"
.LC90:
	.string	"on"
.LC91:
	.string	"silent"
.LC92:
	.string	"%s\n"
.LC93:
	.string	"NODE_EXTRA_CA_CERTS"
	.text
	.p2align 4
	.globl	_ZN4node24InitializeOncePerProcessEiPPc
	.type	_ZN4node24InitializeOncePerProcessEiPPc, @function
_ZN4node24InitializeOncePerProcessEiPPc:
.LFB9205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	_ZN4node11per_process18enabled_debug_listE(%rip), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZN4nodeL5stdioE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-656(%rbp), %r12
	pushq	%rbx
	subq	$712, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -736(%rbp)
	movl	%esi, -720(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node16EnabledDebugList5ParseEPNS_11EnvironmentE@PLT
	leaq	_ZN4node10ResetStdioEv(%rip), %rdi
	call	atexit@PLT
	movq	%r12, %rdi
	call	sigemptyset@PLT
	movq	%r12, %rdi
	movl	$10, %esi
	call	sigaddset@PLT
	movq	%r12, %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	pthread_sigmask@PLT
	movq	%r13, %r12
	movl	%eax, -712(%rbp)
.L1277:
	leaq	_ZN4nodeL5stdioE(%rip), %rax
	movq	%r12, %rbx
	leaq	8(%r12), %r15
	movl	$1, %edi
	subq	%rax, %rbx
	movq	%r15, %rdx
	movabsq	$-8881765665119413741, %rax
	sarq	$3, %rbx
	imulq	%rax, %rbx
	movl	%ebx, %esi
	call	__fxstat64@PLT
	testl	%eax, %eax
	je	.L1274
	call	__errno_location@PLT
	cmpl	$9, (%rax)
	jne	.L1276
	xorl	%eax, %eax
	movl	$2, %esi
	leaq	.LC89(%rip), %rdi
	call	open64@PLT
	cmpl	%ebx, %eax
	jne	.L1276
	movq	%r15, %rdx
	movl	%ebx, %esi
	movl	$1, %edi
	call	__fxstat64@PLT
	testl	%eax, %eax
	jne	.L1276
.L1274:
	addq	$216, %r12
	leaq	648+_ZN4nodeL5stdioE(%rip), %rax
	cmpq	%rax, %r12
	jne	.L1277
	movl	-712(%rbp), %edx
	testl	%edx, %edx
	jne	.L1386
	leaq	-528(%rbp), %r12
	movl	$19, %ecx
	xorl	%eax, %eax
	movl	$1, %ebx
	movq	%r12, %rdi
	rep stosq
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1279:
	addl	$1, %ebx
	cmpl	$32, %ebx
	je	.L1387
.L1280:
	cmpl	$9, %ebx
	je	.L1279
	cmpl	$19, %ebx
	je	.L1279
	cmpl	$13, %ebx
	movq	%r12, %rsi
	movl	%ebx, %edi
	sete	%al
	cmpl	$25, %ebx
	sete	%dl
	orl	%edx, %eax
	xorl	%edx, %edx
	movzbl	%al, %eax
	movq	%rax, -528(%rbp)
	call	sigaction@PLT
	testl	%eax, %eax
	je	.L1279
	leaq	_ZZN4node12PlatformInitEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1387:
	movabsq	$-8881765665119413741, %r15
.L1288:
	movq	%r13, %r12
	leaq	_ZN4nodeL5stdioE(%rip), %rax
	subq	%rax, %r12
	sarq	$3, %r12
	imulq	%r15, %r12
	movl	%r12d, %ebx
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1389:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L1388
.L1283:
	movl	$3, %esi
	movl	%ebx, %edi
	xorl	%eax, %eax
	call	fcntl64@PLT
	movl	%eax, 0(%r13)
	cmpl	$-1, %eax
	je	.L1389
	movl	%r12d, %edi
	call	uv_guess_handle@PLT
	cmpl	$14, %eax
	je	.L1390
.L1287:
	addq	$216, %r13
	leaq	648+_ZN4nodeL5stdioE(%rip), %rax
	cmpq	%rax, %r13
	jne	.L1288
	leaq	-200(%rbp), %r8
	xorl	%ebx, %ebx
	movl	$18, %ecx
	movq	%r8, %rdi
	movq	%rbx, %rax
	leaq	-208(%rbp), %r15
	rep stosq
	leaq	_ZN4node10SignalExitEi(%rip), %r12
	movq	%r8, %rdi
	movq	%r15, -752(%rbp)
	movq	%r12, -208(%rbp)
	movl	$-2147483648, -72(%rbp)
	call	sigfillset@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$2, %edi
	call	sigaction@PLT
	testl	%eax, %eax
	jne	.L1290
	leaq	-360(%rbp), %r8
	movq	%rbx, %rax
	movl	$18, %ecx
	movq	%r12, -368(%rbp)
	movq	%r8, %rdi
	leaq	-368(%rbp), %r15
	rep stosq
	movq	%r8, %rdi
	movl	$-2147483648, -232(%rbp)
	call	sigfillset@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$15, %edi
	call	sigaction@PLT
	testl	%eax, %eax
	jne	.L1290
	leaq	-688(%rbp), %rax
	movl	$7, %edi
	movq	%rax, %rsi
	movq	%rax, -728(%rbp)
	call	getrlimit64@PLT
	testl	%eax, %eax
	jne	.L1294
	movq	-688(%rbp), %rbx
	movq	-680(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L1294
	cmpq	$-1, %r15
	je	.L1341
	movq	%r15, %rbx
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r15, %rax
	movq	-728(%rbp), %rsi
	movl	$7, %edi
	subq	%rbx, %rax
	shrq	%rax
	addq	%rbx, %rax
	movq	%rax, -688(%rbp)
	call	setrlimit64@PLT
	testl	%eax, %eax
	je	.L1296
	movq	-688(%rbp), %r15
	leaq	1(%rbx), %rax
	cmpq	%rax, %r15
	ja	.L1298
	.p2align 4,,10
	.p2align 3
.L1294:
	movl	-720(%rbp), %eax
	testl	%eax, %eax
	jle	.L1391
	movslq	-720(%rbp), %r15
	movq	-736(%rbp), %rsi
	movl	%r15d, %edi
	salq	$3, %r15
	call	uv_setup_args@PLT
	movl	$0, (%r14)
	pxor	%xmm0, %xmm0
	movb	$0, 56(%r14)
	movq	%rax, %rbx
	leaq	(%rax,%r15), %rax
	salq	$2, %r15
	movq	%r15, %rdi
	movups	%xmm0, 8(%r14)
	movups	%xmm0, 24(%r14)
	movups	%xmm0, 40(%r14)
	movq	%rax, -720(%rbp)
	call	_Znwm@PLT
	addq	%rax, %r15
	movq	%rax, -744(%rbp)
	movq	%rax, %r13
	movq	%r15, -736(%rbp)
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1300:
	cmpq	$1, %rax
	jne	.L1302
	movzbl	(%r12), %edx
	movb	%dl, 16(%r13)
.L1303:
	movq	%rax, 8(%r13)
	addq	$8, %rbx
	addq	$32, %r13
	movb	$0, (%r8,%rax)
	cmpq	%rbx, -720(%rbp)
	je	.L1392
.L1304:
	movq	(%rbx), %r12
	leaq	16(%r13), %r8
	movq	%r8, 0(%r13)
	testq	%r12, %r12
	je	.L1393
	movq	%r12, %rdi
	movq	%r8, -712(%rbp)
	call	strlen@PLT
	movq	-712(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -688(%rbp)
	movq	%rax, %r15
	jbe	.L1300
	movq	-728(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %r8
	movq	-688(%rbp), %rax
	movq	%rax, 16(%r13)
.L1301:
	movq	%r8, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-688(%rbp), %rax
	movq	0(%r13), %r8
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1302:
	testq	%rax, %rax
	je	.L1303
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1393:
	leaq	.LC56(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1390:
	movb	$1, 4(%r13)
	leaq	152(%r13), %r12
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1394:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L1285
.L1286:
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	tcgetattr@PLT
	cmpl	$-1, %eax
	je	.L1394
	testl	%eax, %eax
	je	.L1287
.L1285:
	leaq	_ZZN4node12PlatformInitEvE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1392:
	leaq	8(%r14), %rax
	movq	8(%r14), %r15
	movq	%r13, %xmm1
	movq	-744(%rbp), %xmm0
	movq	16(%r14), %rbx
	movq	%rax, %r12
	movq	-736(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%r15, %r13
	movq	%rax, 24(%r14)
	movups	%xmm0, 8(%r14)
	cmpq	%rbx, %r15
	je	.L1309
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L1308
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L1305
.L1309:
	testq	%r15, %r15
	je	.L1307
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1307:
	movq	-728(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	leaq	32(%r14), %rsi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_ZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_
	movq	-688(%rbp), %rbx
	movq	-680(%rbp), %r12
	movl	%eax, (%r14)
	cmpq	%r12, %rbx
	je	.L1311
	leaq	.LC86(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	(%rbx), %r8
	movq	8(%r14), %rax
	cmpq	%rax, 16(%r14)
	je	.L1395
	movq	(%rax), %rcx
	movq	stderr(%rip), %rdi
	movq	%r15, %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	addq	$32, %rbx
	call	__fprintf_chk@PLT
	cmpq	%rbx, %r12
	jne	.L1313
	movl	(%r14), %eax
.L1311:
	testl	%eax, %eax
	je	.L1314
.L1385:
	movb	$1, 56(%r14)
.L1315:
	movq	-680(%rbp), %rbx
	movq	-688(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1333
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1334
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1337
.L1335:
	movq	-688(%rbp), %r12
.L1333:
	testq	%r12, %r12
	je	.L1273
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1273:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1396
	addq	$712, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1308:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1305
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1334:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1337
	jmp	.L1335
.L1386:
	leaq	_ZZN4node12PlatformInitEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1388:
	leaq	_ZZN4node12PlatformInitEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1314:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	.LC90(%rip), %rsi
	leaq	376(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1316
.L1320:
	call	_ZN4node25MapStaticCodeToLargePagesEv@PLT
	leaq	.LC90(%rip), %rsi
	movl	%eax, %r12d
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	376(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1321
	testl	%r12d, %r12d
	jne	.L1397
.L1321:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpb	$0, 203(%rax)
	jne	.L1398
	cmpb	$0, 200(%rax)
	jne	.L1399
	cmpb	$0, 202(%rax)
	jne	.L1400
	movq	-752(%rbp), %rsi
	leaq	-192(%rbp), %rbx
	xorl	%edx, %edx
	leaq	.LC93(%rip), %rdi
	movq	%rbx, -208(%rbp)
	movq	$0, -200(%rbp)
	movb	$0, -192(%rbp)
	call	_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE@PLT
	testb	%al, %al
	jne	.L1401
.L1325:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1326
	call	_ZdlPv@PLT
.L1326:
	movq	_ZN4node6crypto13EntropySourceEPhm@GOTPCREL(%rip), %rdi
	call	_ZN2v82V816SetEntropySourceEPFbPhmE@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movl	$1312, %edi
	movq	128(%rax), %rbx
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing5AgentC1Ev@PLT
	movq	8+_ZN4node11per_process11v8_platformE(%rip), %r13
	movq	%r12, 8+_ZN4node11per_process11v8_platformE(%rip)
	testq	%r13, %r13
	je	.L1327
	movq	%r13, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	8+_ZN4node11per_process11v8_platformE(%rip), %r12
.L1327:
	movq	%r12, %rdi
	call	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE@PLT
	movq	8+_ZN4node11per_process11v8_platformE(%rip), %rax
	movq	976(%rax), %r12
	testq	%r12, %r12
	je	.L1402
	movl	$16, %edi
	call	_Znwm@PLT
	movq	_ZN4node11per_process11v8_platformE(%rip), %rdi
	movq	%rax, %rsi
	leaq	16+_ZTVN4node22NodeTraceStateObserverE(%rip), %rax
	movq	%rax, (%rsi)
	movq	%r12, 8(%rsi)
	movq	%rsi, _ZN4node11per_process11v8_platformE(%rip)
	testq	%rdi, %rdi
	je	.L1329
	movq	(%rdi), %rax
	leaq	_ZN4node22NodeTraceStateObserverD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1330
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	_ZN4node11per_process11v8_platformE(%rip), %rsi
.L1329:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	movq	8+_ZN4node11per_process11v8_platformE(%rip), %rsi
	leaq	-704(%rbp), %rdi
	call	_ZN4node7tracing5Agent13DefaultHandleEv@PLT
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1331
	movl	24+_ZN4node11per_process11v8_platformE(%rip), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L1331:
	movq	-704(%rbp), %rax
	movq	%rax, 16+_ZN4node11per_process11v8_platformE(%rip)
	movl	-696(%rbp), %eax
	movl	%eax, 24+_ZN4node11per_process11v8_platformE(%rip)
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpq	$0, 64(%rax)
	jne	.L1403
.L1332:
	movl	$128, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movl	%ebx, %esi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	movq	%r13, %rdi
	movq	%r13, 32+_ZN4node11per_process11v8_platformE(%rip)
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	call	uv_hrtime@PLT
	movb	$1, _ZN4node11per_process14v8_initializedE(%rip)
	movq	%rax, _ZN4node11performance20performance_v8_startE(%rip)
	jmp	.L1315
.L1316:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	.LC91(%rip), %rsi
	leaq	376(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L1320
	jmp	.L1321
.L1290:
	leaq	_ZZN4node21RegisterSignalHandlerEiPFviEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1341:
	movl	$1048576, %r15d
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	-688(%rbp), %rbx
	leaq	1(%rbx), %rax
	cmpq	%rax, %r15
	ja	.L1298
	jmp	.L1294
.L1391:
	leaq	_ZZN4node24InitializeOncePerProcessEiPPcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1398:
	leaq	.LC87(%rip), %rdi
	call	puts@PLT
	movl	$0, (%r14)
	jmp	.L1385
.L1399:
	movq	-752(%rbp), %rdi
	call	_ZN4node14options_parser17GetBashCompletionB5cxx11Ev@PLT
	movq	-208(%rbp), %rdi
	call	puts@PLT
	xorl	%edi, %edi
	call	exit@PLT
.L1401:
	movq	-752(%rbp), %rdi
	call	_ZN4node6crypto15UseExtraCaCertsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	jmp	.L1325
.L1403:
	leaq	_ZN4node11per_process11v8_platformE(%rip), %rdi
	call	_ZN4node10V8Platform17StartTracingAgentEv
	jmp	.L1332
.L1397:
	movl	%r12d, %edi
	call	_ZN4node15LargePagesErrorEi@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC92(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	jmp	.L1321
.L1330:
	call	*%rax
	movq	_ZN4node11per_process11v8_platformE(%rip), %rsi
	jmp	.L1329
.L1402:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1276:
	call	_ZN4node5AbortEv@PLT
.L1395:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC80(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1400:
	leaq	.LC88(%rip), %rdi
	movl	$6, %esi
	call	_ZN2v82V818SetFlagsFromStringEPKci@PLT
	leaq	_ZZN4node24InitializeOncePerProcessEiPPcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1396:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9205:
	.size	_ZN4node24InitializeOncePerProcessEiPPc, .-_ZN4node24InitializeOncePerProcessEiPPc
	.p2align 4
	.globl	_ZN4node5StartEiPPc
	.type	_ZN4node5StartEiPPc, @function
_ZN4node5StartEiPPc:
.LFB9219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdx
	movl	%r8d, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-304(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	subq	$320, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node24InitializeOncePerProcessEiPPc
	cmpb	$0, -248(%rbp)
	je	.L1405
.L1438:
	movq	-264(%rbp), %r13
	movq	-272(%rbp), %r12
	movl	-304(%rbp), %r14d
	cmpq	%r12, %r13
	je	.L1440
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1441
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L1444
.L1442:
	movq	-272(%rbp), %r12
.L1440:
	testq	%r12, %r12
	je	.L1445
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1445:
	movq	-288(%rbp), %r13
	movq	-296(%rbp), %r12
	cmpq	%r12, %r13
	je	.L1446
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1447
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1450
.L1448:
	movq	-296(%rbp), %r12
.L1446:
	testq	%r12, %r12
	je	.L1404
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1404:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1485
	leaq	-32(%rbp), %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1450
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1441:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L1444
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1405:
	leaq	-136(%rbp), %rdi
	movq	$0, -144(%rbp)
	call	_ZN2v819ResourceConstraintsC1Ev@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$1, %ecx
	movups	%xmm0, -88(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	8(%rax), %rax
	movw	%cx, -48(%rbp)
	cmpb	$0, 25(%rax)
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	je	.L1407
.L1409:
	xorl	%r13d, %r13d
	leaq	-240(%rbp), %r12
.L1408:
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %r14
	call	uv_default_loop@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-144(%rbp), %rsi
	pushq	%r13
	movq	%rax, %rdx
	leaq	-272(%rbp), %r9
	movq	%r14, %rcx
	leaq	-296(%rbp), %r8
	call	_ZN4node16NodeMainInstanceC1EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE@PLT
	movq	%r12, %rdi
	call	_ZN4node16NodeMainInstance3RunEv@PLT
	movq	%r12, %rdi
	movl	%eax, -304(%rbp)
	call	_ZN4node16NodeMainInstanceD1Ev@PLT
	movq	-336(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1412
	call	_ZdlPv@PLT
.L1412:
	movb	$0, _ZN4node11per_process14v8_initializedE(%rip)
	call	_ZN2v82V87DisposeEv@PLT
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L1413
	movl	24+_ZN4node11per_process11v8_platformE(%rip), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L1413:
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %rdi
	movq	$0, 16+_ZN4node11per_process11v8_platformE(%rip)
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %r14
	testq	%r14, %r14
	je	.L1414
	movq	(%r14), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1415
	movq	120(%r14), %r12
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r14)
	testq	%r12, %r12
	je	.L1417
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1418
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1419:
	cmpl	$1, %eax
	je	.L1486
.L1417:
	movq	64(%r14), %r12
	testq	%r12, %r12
	je	.L1436
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1432
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	jne	.L1487
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1436
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L1434
.L1487:
	lock subl	$1, 8(%r13)
	jne	.L1434
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L1434
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L1429
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1436
.L1432:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L1429
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	je	.L1430
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1432
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	56(%r14), %rax
	movq	48(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r14), %rdi
	leaq	96(%r14), %rax
	movq	$0, 72(%r14)
	movq	$0, 64(%r14)
	cmpq	%rax, %rdi
	je	.L1424
	call	_ZdlPv@PLT
.L1424:
	leaq	8(%r14), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1414:
	movq	8+_ZN4node11per_process11v8_platformE(%rip), %r12
	movq	$0, 32+_ZN4node11per_process11v8_platformE(%rip)
	movq	$0, 8+_ZN4node11per_process11v8_platformE(%rip)
	testq	%r12, %r12
	je	.L1437
	movq	%r12, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1437:
	movq	_ZN4node11per_process11v8_platformE(%rip), %rdi
	movq	$0, _ZN4node11per_process11v8_platformE(%rip)
	testq	%rdi, %rdi
	je	.L1438
	movq	(%rdi), %rax
	leaq	_ZN4node22NodeTraceStateObserverD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1439
	movl	$16, %esi
	call	_ZdlPvm@PLT
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1407:
	call	_ZN4node16NodeMainInstance23GetEmbeddedSnapshotBlobEv@PLT
	testq	%rax, %rax
	je	.L1409
	movq	$0, -240(%rbp)
	movq	-328(%rbp), %rsi
	cmpq	-320(%rbp), %rsi
	je	.L1410
	movq	$0, (%rsi)
	addq	$8, %rsi
	leaq	-240(%rbp), %r12
	movq	%rsi, -328(%rbp)
.L1411:
	movq	-336(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN4node16NodeMainInstance21GetIsolateDataIndexesEv@PLT
	movq	%rax, %r13
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1439:
	call	*%rax
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1418:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1421
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1422:
	cmpl	$1, %eax
	jne	.L1417
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1421:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1410:
	leaq	-240(%rbp), %r12
	leaq	-336(%rbp), %rdi
	movq	%rax, -344(%rbp)
	movq	%r12, %rdx
	call	_ZNSt6vectorIlSaIlEE17_M_realloc_insertIJlEEEvN9__gnu_cxx17__normal_iteratorIPlS1_EEDpOT_
	movq	-344(%rbp), %rax
	jmp	.L1411
.L1485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9219:
	.size	_ZN4node5StartEiPPc, .-_ZN4node5StartEiPPc
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB12885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1490
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1491
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1517
	.p2align 4,,10
	.p2align 3
.L1490:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L1496
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1502
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L1518
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1496
.L1497:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L1504
.L1518:
	lock subl	$1, 8(%r13)
	jne	.L1504
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L1504
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1497
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L1506
	call	_ZdlPv@PLT
.L1506:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L1501:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L1500
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1496
.L1502:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L1500
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1500
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1491:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1490
.L1517:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1494
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1495:
	cmpl	$1, %eax
	jne	.L1490
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1494:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1495
	.cfi_endproc
.LFE12885:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11per_process12reverted_cveE, @function
_GLOBAL__sub_I__ZN4node11per_process12reverted_cveE:
.LFB12950:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node11per_process11v8_platformE(%rip), %rsi
	movq	$0, 16+_ZN4node11per_process11v8_platformE(%rip)
	leaq	_ZN4node10V8PlatformD1Ev(%rip), %rdi
	movaps	%xmm0, _ZN4node11per_process11v8_platformE(%rip)
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12950:
	.size	_GLOBAL__sub_I__ZN4node11per_process12reverted_cveE, .-_GLOBAL__sub_I__ZN4node11per_process12reverted_cveE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11per_process12reverted_cveE
	.weak	_ZTVN4node22NodeTraceStateObserverE
	.section	.data.rel.ro.local._ZTVN4node22NodeTraceStateObserverE,"awG",@progbits,_ZTVN4node22NodeTraceStateObserverE,comdat
	.align 8
	.type	_ZTVN4node22NodeTraceStateObserverE, @object
	.size	_ZTVN4node22NodeTraceStateObserverE, 48
_ZTVN4node22NodeTraceStateObserverE:
	.quad	0
	.quad	0
	.quad	_ZN4node22NodeTraceStateObserverD1Ev
	.quad	_ZN4node22NodeTraceStateObserverD0Ev
	.quad	_ZN4node22NodeTraceStateObserver14OnTraceEnabledEv
	.quad	_ZN4node22NodeTraceStateObserver15OnTraceDisabledEv
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC94:
	.string	"../src/util-inl.h:325"
.LC95:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.weak	_ZZN4node6MallocIPKcEEPT_mE4args
	.section	.rodata.str1.1
.LC97:
	.string	"../src/util-inl.h:381"
.LC98:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"T* node::Malloc(size_t) [with T = const char*; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIPKcEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIPKcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIPKcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIPKcEEPT_mE4args, 24
_ZZN4node6MallocIPKcEEPT_mE4args:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.section	.rodata.str1.1
.LC100:
	.string	"../src/node.cc:924"
.LC101:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC102:
	.string	"node::InitializationResult node::InitializeOncePerProcess(int, char**)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node24InitializeOncePerProcessEiPPcE4args_0, @object
	.size	_ZZN4node24InitializeOncePerProcessEiPPcE4args_0, 24
_ZZN4node24InitializeOncePerProcessEiPPcE4args_0:
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.section	.rodata.str1.1
.LC103:
	.string	"../src/node.cc:880"
.LC104:
	.string	"(argc) > (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node24InitializeOncePerProcessEiPPcE4args, @object
	.size	_ZZN4node24InitializeOncePerProcessEiPPcE4args, 24
_ZZN4node24InitializeOncePerProcessEiPPcE4args:
	.quad	.LC103
	.quad	.LC104
	.quad	.LC102
	.section	.rodata.str1.1
.LC105:
	.string	"../src/node.cc:857"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"void node::Init(int*, const char**, int*, const char***)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4InitEPiPPKcS0_PS3_E4args, @object
	.size	_ZZN4node4InitEPiPPKcS0_PS3_E4args, 24
_ZZN4node4InitEPiPPKcS0_PS3_E4args:
	.quad	.LC105
	.quad	.LC101
	.quad	.LC106
	.section	.rodata.str1.1
.LC107:
	.string	"../src/node.cc:730"
.LC108:
	.string	"!init_called.exchange(true)"
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"int node::InitializeNodeWithArgs(std::vector<std::__cxx11::basic_string<char> >*, std::vector<std::__cxx11::basic_string<char> >*, std::vector<std::__cxx11::basic_string<char> >*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_E4args, @object
	.size	_ZZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_E4args, 24
_ZZN4node22InitializeNodeWithArgsEPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EES9_S9_E4args:
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.local	_ZN4nodeL11init_calledE
	.comm	_ZN4nodeL11init_calledE,1,1
	.section	.rodata.str1.1
.LC110:
	.string	"../src/node.cc:640"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"!(err != 0) || (err == -1 && (*__errno_location ()) == 1)"
	.section	.rodata.str1.1
.LC112:
	.string	"void node::ResetStdio()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10ResetStdioEvE4args_4, @object
	.size	_ZZN4node10ResetStdioEvE4args_4, 24
_ZZN4node10ResetStdioEvE4args_4:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.section	.rodata.str1.1
.LC113:
	.string	"../src/node.cc:636"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"(0) == (pthread_sigmask(1, &sa, nullptr))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10ResetStdioEvE4args_3, @object
	.size	_ZZN4node10ResetStdioEvE4args_3, 24
_ZZN4node10ResetStdioEvE4args_3:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC112
	.section	.rodata.str1.1
.LC115:
	.string	"../src/node.cc:632"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"(0) == (pthread_sigmask(0, &sa, nullptr))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10ResetStdioEvE4args_2, @object
	.size	_ZZN4node10ResetStdioEvE4args_2, 24
_ZZN4node10ResetStdioEvE4args_2:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC112
	.section	.rodata.str1.1
.LC117:
	.string	"../src/node.cc:620"
.LC118:
	.string	"(err) != (-1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10ResetStdioEvE4args_1, @object
	.size	_ZZN4node10ResetStdioEvE4args_1, 24
_ZZN4node10ResetStdioEvE4args_1:
	.quad	.LC117
	.quad	.LC118
	.quad	.LC112
	.section	.rodata.str1.1
.LC119:
	.string	"../src/node.cc:609"
.LC120:
	.string	"(flags) != (-1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10ResetStdioEvE4args_0, @object
	.size	_ZZN4node10ResetStdioEvE4args_0, 24
_ZZN4node10ResetStdioEvE4args_0:
	.quad	.LC119
	.quad	.LC120
	.quad	.LC112
	.section	.rodata.str1.1
.LC121:
	.string	"../src/node.cc:597"
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"((*__errno_location ())) == (9)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10ResetStdioEvE4args, @object
	.size	_ZZN4node10ResetStdioEvE4args, 24
_ZZN4node10ResetStdioEvE4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC112
	.weak	_ZZN4node12PlatformInitEvE4args_2
	.section	.rodata.str1.1
.LC123:
	.string	"../src/node.cc:545"
.LC124:
	.string	"(err) == (0)"
.LC125:
	.string	"void node::PlatformInit()"
	.section	.data.rel.ro.local._ZZN4node12PlatformInitEvE4args_2,"awG",@progbits,_ZZN4node12PlatformInitEvE4args_2,comdat
	.align 16
	.type	_ZZN4node12PlatformInitEvE4args_2, @gnu_unique_object
	.size	_ZZN4node12PlatformInitEvE4args_2, 24
_ZZN4node12PlatformInitEvE4args_2:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.weak	_ZZN4node12PlatformInitEvE4args_1
	.section	.rodata.str1.1
.LC126:
	.string	"../src/node.cc:537"
.LC127:
	.string	"(s.flags) != (-1)"
	.section	.data.rel.ro.local._ZZN4node12PlatformInitEvE4args_1,"awG",@progbits,_ZZN4node12PlatformInitEvE4args_1,comdat
	.align 16
	.type	_ZZN4node12PlatformInitEvE4args_1, @gnu_unique_object
	.size	_ZZN4node12PlatformInitEvE4args_1, 24
_ZZN4node12PlatformInitEvE4args_1:
	.quad	.LC126
	.quad	.LC127
	.quad	.LC125
	.weak	_ZZN4node12PlatformInitEvE4args_0
	.section	.rodata.str1.1
.LC128:
	.string	"../src/node.cc:523"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"(0) == (sigaction(nr, &act, nullptr))"
	.section	.data.rel.ro.local._ZZN4node12PlatformInitEvE4args_0,"awG",@progbits,_ZZN4node12PlatformInitEvE4args_0,comdat
	.align 16
	.type	_ZZN4node12PlatformInitEvE4args_0, @gnu_unique_object
	.size	_ZZN4node12PlatformInitEvE4args_0, 24
_ZZN4node12PlatformInitEvE4args_0:
	.quad	.LC128
	.quad	.LC129
	.quad	.LC125
	.weak	_ZZN4node12PlatformInitEvE4args
	.section	.rodata.str1.1
.LC130:
	.string	"../src/node.cc:507"
	.section	.data.rel.ro.local._ZZN4node12PlatformInitEvE4args,"awG",@progbits,_ZZN4node12PlatformInitEvE4args,comdat
	.align 16
	.type	_ZZN4node12PlatformInitEvE4args, @gnu_unique_object
	.size	_ZZN4node12PlatformInitEvE4args, 24
_ZZN4node12PlatformInitEvE4args:
	.quad	.LC130
	.quad	.LC124
	.quad	.LC125
	.local	_ZN4nodeL5stdioE
	.comm	_ZN4nodeL5stdioE,648,32
	.section	.rodata.str1.1
.LC131:
	.string	"../src/node.cc:467"
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"(sigaction(signal, &sa, nullptr)) == (0)"
	.align 8
.LC133:
	.string	"void node::RegisterSignalHandler(int, void (*)(int), bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node21RegisterSignalHandlerEiPFviEbE4args, @object
	.size	_ZZN4node21RegisterSignalHandlerEiPFviEbE4args, 24
_ZZN4node21RegisterSignalHandlerEiPFviEbE4args:
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.section	.rodata.str1.1
.LC134:
	.string	"../src/node.cc:444"
.LC135:
	.string	"env->is_main_thread()"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"void node::LoadEnvironment(node::Environment*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15LoadEnvironmentEPNS_11EnvironmentEE4args, @object
	.size	_ZZN4node15LoadEnvironmentEPNS_11EnvironmentEE4args, 24
_ZZN4node15LoadEnvironmentEPNS_11EnvironmentEE4args:
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.section	.rodata.str1.1
.LC137:
	.string	"../src/node.cc:369"
.LC138:
	.string	"(main_script_id) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC139:
	.string	"v8::MaybeLocal<v8::Value> node::StartExecution(node::Environment*, const char*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node14StartExecutionEPNS_11EnvironmentEPKcE4args, @object
	.size	_ZZN4node14StartExecutionEPNS_11EnvironmentEPKcE4args, 24
_ZZN4node14StartExecutionEPNS_11EnvironmentEPKcE4args:
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.section	.rodata.str1.1
.LC140:
	.string	"../src/node.cc:354"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"handle_wrap_queue()->IsEmpty()"
	.align 8
.LC142:
	.string	"v8::MaybeLocal<v8::Value> node::Environment::RunBootstrapping()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment16RunBootstrappingEvE4args_1, @object
	.size	_ZZN4node11Environment16RunBootstrappingEvE4args_1, 24
_ZZN4node11Environment16RunBootstrappingEvE4args_1:
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.section	.rodata.str1.1
.LC143:
	.string	"../src/node.cc:353"
.LC144:
	.string	"req_wrap_queue()->IsEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment16RunBootstrappingEvE4args_0, @object
	.size	_ZZN4node11Environment16RunBootstrappingEvE4args_0, 24
_ZZN4node11Environment16RunBootstrappingEvE4args_0:
	.quad	.LC143
	.quad	.LC144
	.quad	.LC142
	.section	.rodata.str1.1
.LC145:
	.string	"../src/node.cc:338"
.LC146:
	.string	"!has_run_bootstrapping_code()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment16RunBootstrappingEvE4args, @object
	.size	_ZZN4node11Environment16RunBootstrappingEvE4args, 24
_ZZN4node11Environment16RunBootstrappingEvE4args:
	.quad	.LC145
	.quad	.LC146
	.quad	.LC142
	.section	.rodata.str1.1
.LC147:
	.string	"../src/node.cc:270"
.LC148:
	.string	"require->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"v8::MaybeLocal<v8::Value> node::Environment::BootstrapInternalLoaders()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_1, @object
	.size	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_1, 24
_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_1:
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.section	.rodata.str1.1
.LC150:
	.string	"../src/node.cc:266"
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"internal_binding_loader->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_0, @object
	.size	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_0, 24
_ZZN4node11Environment24BootstrapInternalLoadersEvE4args_0:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC149
	.section	.rodata.str1.1
.LC152:
	.string	"../src/node.cc:261"
.LC153:
	.string	"loader_exports->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args, @object
	.size	_ZZN4node11Environment24BootstrapInternalLoadersEvE4args, 24
_ZZN4node11Environment24BootstrapInternalLoadersEvE4args:
	.quad	.LC152
	.quad	.LC153
	.quad	.LC149
	.section	.rodata.str1.1
.LC154:
	.string	"../src/node.cc:201"
	.section	.rodata.str1.8
	.align 8
.LC155:
	.string	"!inspector_agent_->IsListening()"
	.align 8
.LC156:
	.string	"int node::Environment::InitializeInspector(std::unique_ptr<node::inspector::ParentInspectorHandle>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EEE4args, @object
	.size	_ZZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EEE4args, 24
_ZZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EEE4args:
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.globl	_ZN4node11per_process11v8_platformE
	.bss
	.align 32
	.type	_ZN4node11per_process11v8_platformE, @object
	.size	_ZN4node11per_process11v8_platformE, 40
_ZN4node11per_process11v8_platformE:
	.zero	40
	.globl	_ZN4node11per_process15v8_is_profilingE
	.type	_ZN4node11per_process15v8_is_profilingE, @object
	.size	_ZN4node11per_process15v8_is_profilingE, 1
_ZN4node11per_process15v8_is_profilingE:
	.zero	1
	.globl	_ZN4node11per_process15node_start_timeE
	.align 8
	.type	_ZN4node11per_process15node_start_timeE, @object
	.size	_ZN4node11per_process15node_start_timeE, 8
_ZN4node11per_process15node_start_timeE:
	.zero	8
	.globl	_ZN4node11per_process14v8_initializedE
	.type	_ZN4node11per_process14v8_initializedE, @object
	.size	_ZN4node11per_process14v8_initializedE, 1
_ZN4node11per_process14v8_initializedE:
	.zero	1
	.globl	_ZN4node11per_process12reverted_cveE
	.align 4
	.type	_ZN4node11per_process12reverted_cveE, @object
	.size	_ZN4node11per_process12reverted_cveE, 4
_ZN4node11per_process12reverted_cveE:
	.zero	4
	.weak	_ZZN4node22NodeTraceStateObserver15OnTraceDisabledEvE4args
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"../src/node_v8_platform-inl.h:70"
	.align 8
.LC158:
	.string	"virtual void node::NodeTraceStateObserver::OnTraceDisabled()"
	.section	.data.rel.ro.local._ZZN4node22NodeTraceStateObserver15OnTraceDisabledEvE4args,"awG",@progbits,_ZZN4node22NodeTraceStateObserver15OnTraceDisabledEvE4args,comdat
	.align 16
	.type	_ZZN4node22NodeTraceStateObserver15OnTraceDisabledEvE4args, @gnu_unique_object
	.size	_ZZN4node22NodeTraceStateObserver15OnTraceDisabledEvE4args, 24
_ZZN4node22NodeTraceStateObserver15OnTraceDisabledEvE4args:
	.quad	.LC157
	.quad	.LC101
	.quad	.LC158
	.weak	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60
	.section	.bss._ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60,"awG",@nobits,_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60,comdat
	.align 8
	.type	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60, @gnu_unique_object
	.size	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60, 8
_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic60:
	.zero	8
	.weak	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35
	.section	.bss._ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35,"awG",@nobits,_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35,comdat
	.align 8
	.type	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35, @gnu_unique_object
	.size	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35, 8
_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic35:
	.zero	8
	.weak	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31
	.section	.bss._ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31,"awG",@nobits,_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31,comdat
	.align 8
	.type	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31, @gnu_unique_object
	.size	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31, 8
_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic31:
	.zero	8
	.weak	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28
	.section	.bss._ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28,"awG",@nobits,_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28,comdat
	.align 8
	.type	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28, @gnu_unique_object
	.size	_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28, 8
_ZZN4node22NodeTraceStateObserver14OnTraceEnabledEvE27trace_event_unique_atomic28:
	.zero	8
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC159:
	.string	"../src/tracing/agent.h:91"
.LC160:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC29:
	.quad	8386019626708202050
	.quad	8247343761545191968
	.align 16
.LC41:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.align 16
.LC77:
	.quad	2334106421097295465
	.quad	8314049671544991597
	.align 16
.LC78:
	.quad	3251721475511838067
	.quad	7308324466003108909
	.align 16
.LC82:
	.quad	8029390801336627043
	.quad	7019269490406400116
	.align 16
.LC83:
	.quad	6143834720153790828
	.quad	2336070118915909664
	.align 16
.LC84:
	.quad	6143834990196313934
	.quad	8245844907697194079
	.align 16
.LC85:
	.quad	7218554847571553568
	.quad	2338047035537716321
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
