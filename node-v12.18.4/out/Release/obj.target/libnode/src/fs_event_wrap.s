	.file	"fs_event_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5801:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5801:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111FSEventWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_111FSEventWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7526:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7526:
	.size	_ZNK4node12_GLOBAL__N_111FSEventWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_111FSEventWrap10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111FSEventWrap8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_111FSEventWrap8SelfSizeEv:
.LFB7528:
	.cfi_startproc
	endbr64
	movl	$232, %eax
	ret
	.cfi_endproc
.LFE7528:
	.size	_ZNK4node12_GLOBAL__N_111FSEventWrap8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_111FSEventWrap8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$1056, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L34
	cmpw	$1040, %cx
	jne	.L6
.L34:
	movq	23(%rdx), %r13
.L8:
	movq	8(%rbx), %r12
	leaq	8(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L49
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L35
	cmpw	$1040, %cx
	jne	.L10
.L35:
	movq	23(%rdx), %r12
.L12:
	testq	%r12, %r12
	je	.L50
	movl	72(%r12), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L51
	cmpl	$3, 16(%rbx)
	jle	.L52
	movq	8(%rbx), %rdx
	movq	352(%r13), %rsi
	leaq	-1088(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L53
	cmpl	$2, 16(%rbx)
	jle	.L54
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L18:
	call	_ZNK2v85Value6IsTrueEv@PLT
	xorl	%r14d, %r14d
	testb	%al, %al
	setne	%r14b
	sall	$2, %r14d
	cmpl	$3, 16(%rbx)
	jg	.L20
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L21:
	movq	352(%r13), %rdi
	movl	$1, %edx
	leaq	88(%r12), %r13
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	movq	%r13, %rsi
	movl	%eax, 224(%r12)
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_event_init@PLT
	testl	%eax, %eax
	je	.L22
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L23:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5
.L48:
	testq	%rdi, %rdi
	je	.L5
	call	free@PLT
.L5:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$1056, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-1072(%rbp), %rdx
	movl	%r14d, %ecx
	movq	%r13, %rdi
	leaq	_ZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKcii(%rip), %rsi
	call	uv_fs_event_start@PLT
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN4node10HandleWrap17MarkAsInitializedEv@PLT
	testl	%r14d, %r14d
	jne	.L56
	cmpl	$1, 16(%rbx)
	jle	.L57
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L26:
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	je	.L58
.L27:
	movq	(%rbx), %rax
	movq	-1072(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L48
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r13, %rdi
	call	uv_unref@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rbx, %rdi
	salq	$32, %r14
	call	_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE@PLT
	movq	(%rbx), %rax
	movq	%r14, 24(%rax)
	jmp	.L23
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7539:
	.size	_ZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FSEventWrapD2Ev, @function
_ZN4node12_GLOBAL__N_111FSEventWrapD2Ev:
.LFB9483:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9483:
	.size	_ZN4node12_GLOBAL__N_111FSEventWrapD2Ev, .-_ZN4node12_GLOBAL__N_111FSEventWrapD2Ev
	.set	_ZN4node12_GLOBAL__N_111FSEventWrapD1Ev,_ZN4node12_GLOBAL__N_111FSEventWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FSEventWrapD0Ev, @function
_ZN4node12_GLOBAL__N_111FSEventWrapD0Ev:
.LFB9485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdx
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$232, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9485:
	.size	_ZN4node12_GLOBAL__N_111FSEventWrapD0Ev, .-_ZN4node12_GLOBAL__N_111FSEventWrapD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKcii, @function
_ZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKcii:
.LFB7543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$104, %rsp
	movq	(%rdi), %r12
	movq	%r15, %rdi
	movl	%ecx, -140(%rbp)
	movq	%rsi, -136(%rbp)
	movq	16(%r12), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%r13), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpq	$0, 8(%r12)
	movl	-140(%rbp), %ecx
	je	.L90
	testl	%ecx, %ecx
	jne	.L91
	testb	$1, %bl
	je	.L66
	movq	360(%r13), %rax
	movq	352(%r13), %rdi
	movq	1504(%rax), %rbx
.L65:
	movl	%ecx, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%r13), %rdi
	movq	-136(%rbp), %rsi
	movq	%rbx, -72(%rbp)
	movq	%rax, -80(%rbp)
	leaq	104(%rdi), %rax
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L68
	leaq	-120(%rbp), %rbx
	movl	224(%r12), %edx
	movq	$0, -120(%rbp)
	movq	%rbx, %rcx
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L92
.L69:
	movq	%rax, -64(%rbp)
.L68:
	movq	360(%r13), %rax
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	1120(%rax), %r13
	testq	%rdi, %rdi
	je	.L72
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L93
.L72:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L75
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L94
.L75:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	352(%r13), %rdi
	leaq	128(%rdi), %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L93:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L66:
	andl	$2, %ebx
	je	.L67
	movq	360(%r13), %rax
	movq	352(%r13), %rdi
	movq	296(%rax), %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	movq	352(%r13), %rdi
	movl	$-22, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-136(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	strlen@PLT
	movq	352(%r13), %rdi
	movq	%rbx, %r8
	movq	-136(%rbp), %rsi
	movq	%rax, %rdx
	movl	$6, %ecx
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	jne	.L69
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rax
	jmp	.L69
.L95:
	call	__stack_chk_fail@PLT
.L67:
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7543:
	.size	_ZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKcii, .-_ZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKcii
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111FSEventWrap14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_111FSEventWrap14MemoryInfoNameEv:
.LFB7527:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6301783160653173574, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7527:
	.size	_ZNK4node12_GLOBAL__N_111FSEventWrap14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_111FSEventWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L98
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L104
.L98:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L102
	cmpw	$1040, %cx
	jne	.L99
.L102:
	movq	23(%rdx), %r14
.L101:
	movq	8(%r12), %r13
	movl	$232, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	movl	$6, %r8d
	addq	$8, %r13
	movq	%rax, %rdi
	leaq	88(%rax), %rcx
	movq	%rax, %r12
	movq	%r13, %rdx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node12_GLOBAL__N_111FSEventWrapE(%rip), %rax
	movq	%r12, %rdi
	movl	$1, 224(%r12)
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10HandleWrap19MarkAsUninitializedEv@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L98
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L101
	.cfi_endproc
.LFE7538:
	.size	_ZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L115
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L113
	cmpw	$1040, %cx
	jne	.L107
.L113:
	movq	23(%rdx), %rax
.L109:
	testq	%rax, %rax
	je	.L116
	movl	72(%rax), %eax
	movq	(%rbx), %rdx
	subl	$1, %eax
	movq	8(%rdx), %rcx
	cmpl	$2, %eax
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	leaq	_ZZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7536:
	.size	_ZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"FSEvent"
.LC1:
	.string	"start"
.LC2:
	.string	"close"
.LC3:
	.string	"initialized"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L118
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L118
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L118
	movq	271(%rax), %rbx
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L129
.L119:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L130
.L120:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	movq	_ZN4node10HandleWrap5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE@GOTPCREL(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L131
.L121:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movl	$11, %ecx
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %r10
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L132
.L122:
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movl	$7, %r8d
	movq	%r10, %rdx
	movq	%r14, %rdi
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L133
.L123:
	movq	3280(%rbx), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L134
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%r10, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %rsi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7537:
.L118:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7537:
	.text
	.size	_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE4:
	.text
.LHOTE4:
	.p2align 4
	.globl	_Z23_register_fs_event_wrapv
	.type	_Z23_register_fs_event_wrapv, @function
_Z23_register_fs_event_wrapv:
.LFB7545:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7545:
	.size	_Z23_register_fs_event_wrapv, .-_Z23_register_fs_event_wrapv
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_111FSEventWrapE, @object
	.size	_ZTVN4node12_GLOBAL__N_111FSEventWrapE, 112
_ZTVN4node12_GLOBAL__N_111FSEventWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_111FSEventWrapD1Ev
	.quad	_ZN4node12_GLOBAL__N_111FSEventWrapD0Ev
	.quad	_ZNK4node12_GLOBAL__N_111FSEventWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_111FSEventWrap14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_111FSEventWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.section	.rodata.str1.1
.LC5:
	.string	"../src/fs_event_wrap.cc"
.LC6:
	.string	"fs_event_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC5
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_111FSEventWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC6
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC7:
	.string	"../src/fs_event_wrap.cc:203"
.LC8:
	.string	"0 && \"bad fs events flag\""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"static void node::{anonymous}::FSEventWrap::OnEvent(uv_fs_event_t*, const char*, int, int)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args_0, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args_0:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.section	.rodata.str1.1
.LC10:
	.string	"../src/fs_event_wrap.cc:182"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"(wrap->persistent().IsEmpty()) == (false)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap7OnEventEP13uv_fs_event_sPKciiE4args:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC9
	.section	.rodata.str1.1
.LC12:
	.string	"../src/fs_event_wrap.cc:144"
.LC13:
	.string	"(*path) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"static void node::{anonymous}::FSEventWrap::Start(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.section	.rodata.str1.1
.LC15:
	.string	"../src/fs_event_wrap.cc:141"
.LC16:
	.string	"(argc) >= (4)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC14
	.section	.rodata.str1.1
.LC17:
	.string	"../src/fs_event_wrap.cc:138"
.LC18:
	.string	"wrap->IsHandleClosing()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC14
	.section	.rodata.str1.1
.LC19:
	.string	"../src/fs_event_wrap.cc:137"
.LC20:
	.string	"(wrap) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC14
	.section	.rodata.str1.1
.LC21:
	.string	"../src/fs_event_wrap.cc:127"
.LC22:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"static void node::{anonymous}::FSEventWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"../src/fs_event_wrap.cc:88"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"static void node::{anonymous}::FSEventWrap::GetInitialized(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_111FSEventWrap14GetInitializedERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC24
	.quad	.LC20
	.quad	.LC25
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC26:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC28:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
