	.file	"node_file.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB2785:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE2785:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB2786:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2786:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4687:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4687:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4689:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4689:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4971:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4971:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4972:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4972:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZNK4node2fs18FSContinuationData8SelfSizeEv,"axG",@progbits,_ZNK4node2fs18FSContinuationData8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs18FSContinuationData8SelfSizeEv
	.type	_ZNK4node2fs18FSContinuationData8SelfSizeEv, @function
_ZNK4node2fs18FSContinuationData8SelfSizeEv:
.LFB6067:
	.cfi_startproc
	endbr64
	movl	$88, %eax
	ret
	.cfi_endproc
.LFE6067:
	.size	_ZNK4node2fs18FSContinuationData8SelfSizeEv, .-_ZNK4node2fs18FSContinuationData8SelfSizeEv
	.section	.text._ZNK4node2fs13FSReqCallback8SelfSizeEv,"axG",@progbits,_ZNK4node2fs13FSReqCallback8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs13FSReqCallback8SelfSizeEv
	.type	_ZNK4node2fs13FSReqCallback8SelfSizeEv, @function
_ZNK4node2fs13FSReqCallback8SelfSizeEv:
.LFB6079:
	.cfi_startproc
	endbr64
	movl	$648, %eax
	ret
	.cfi_endproc
.LFE6079:
	.size	_ZNK4node2fs13FSReqCallback8SelfSizeEv, .-_ZNK4node2fs13FSReqCallback8SelfSizeEv
	.section	.text._ZNK4node2fs18FileHandleReadWrap8SelfSizeEv,"axG",@progbits,_ZNK4node2fs18FileHandleReadWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs18FileHandleReadWrap8SelfSizeEv
	.type	_ZNK4node2fs18FileHandleReadWrap8SelfSizeEv, @function
_ZNK4node2fs18FileHandleReadWrap8SelfSizeEv:
.LFB6084:
	.cfi_startproc
	endbr64
	movl	$552, %eax
	ret
	.cfi_endproc
.LFE6084:
	.size	_ZNK4node2fs18FileHandleReadWrap8SelfSizeEv, .-_ZNK4node2fs18FileHandleReadWrap8SelfSizeEv
	.section	.text._ZN4node2fs10FileHandle5GetFDEv,"axG",@progbits,_ZN4node2fs10FileHandle5GetFDEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs10FileHandle5GetFDEv
	.type	_ZN4node2fs10FileHandle5GetFDEv, @function
_ZN4node2fs10FileHandle5GetFDEv:
.LFB6087:
	.cfi_startproc
	endbr64
	movl	120(%rdi), %eax
	ret
	.cfi_endproc
.LFE6087:
	.size	_ZN4node2fs10FileHandle5GetFDEv, .-_ZN4node2fs10FileHandle5GetFDEv
	.section	.text._ZN4node2fs10FileHandle7IsAliveEv,"axG",@progbits,_ZN4node2fs10FileHandle7IsAliveEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs10FileHandle7IsAliveEv
	.type	_ZN4node2fs10FileHandle7IsAliveEv, @function
_ZN4node2fs10FileHandle7IsAliveEv:
.LFB6088:
	.cfi_startproc
	endbr64
	movzbl	125(%rdi), %eax
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE6088:
	.size	_ZN4node2fs10FileHandle7IsAliveEv, .-_ZN4node2fs10FileHandle7IsAliveEv
	.section	.text._ZN4node2fs10FileHandle9IsClosingEv,"axG",@progbits,_ZN4node2fs10FileHandle9IsClosingEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs10FileHandle9IsClosingEv
	.type	_ZN4node2fs10FileHandle9IsClosingEv, @function
_ZN4node2fs10FileHandle9IsClosingEv:
.LFB6089:
	.cfi_startproc
	endbr64
	movzbl	124(%rdi), %eax
	ret
	.cfi_endproc
.LFE6089:
	.size	_ZN4node2fs10FileHandle9IsClosingEv, .-_ZN4node2fs10FileHandle9IsClosingEv
	.section	.text._ZN4node2fs10FileHandle12GetAsyncWrapEv,"axG",@progbits,_ZN4node2fs10FileHandle12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs10FileHandle12GetAsyncWrapEv
	.type	_ZN4node2fs10FileHandle12GetAsyncWrapEv, @function
_ZN4node2fs10FileHandle12GetAsyncWrapEv:
.LFB6090:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE6090:
	.size	_ZN4node2fs10FileHandle12GetAsyncWrapEv, .-_ZN4node2fs10FileHandle12GetAsyncWrapEv
	.section	.text._ZNK4node2fs10FileHandle8SelfSizeEv,"axG",@progbits,_ZNK4node2fs10FileHandle8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs10FileHandle8SelfSizeEv
	.type	_ZNK4node2fs10FileHandle8SelfSizeEv, @function
_ZNK4node2fs10FileHandle8SelfSizeEv:
.LFB6092:
	.cfi_startproc
	endbr64
	movl	$160, %eax
	ret
	.cfi_endproc
.LFE6092:
	.size	_ZNK4node2fs10FileHandle8SelfSizeEv, .-_ZNK4node2fs10FileHandle8SelfSizeEv
	.section	.text._ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv,"axG",@progbits,_ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv
	.type	_ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv, @function
_ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv:
.LFB6094:
	.cfi_startproc
	endbr64
	movl	$544, %eax
	ret
	.cfi_endproc
.LFE6094:
	.size	_ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv, .-_ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7176:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7176:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7664:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7664:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7665:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7665:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7666:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7666:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7668:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L25
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L25:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7668:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZN4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZN4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB7776:
	.cfi_startproc
	endbr64
	movl	$-38, %eax
	ret
	.cfi_endproc
.LFE7776:
	.size	_ZN4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZN4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle8ReadStopEv
	.type	_ZN4node2fs10FileHandle8ReadStopEv, @function
_ZN4node2fs10FileHandle8ReadStopEv:
.LFB7839:
	.cfi_startproc
	endbr64
	movb	$0, 144(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7839:
	.size	_ZN4node2fs10FileHandle8ReadStopEv, .-_ZN4node2fs10FileHandle8ReadStopEv
	.align 2
	.p2align 4
	.globl	_ZN4node2fs13FSReqCallback14SetReturnValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node2fs13FSReqCallback14SetReturnValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node2fs13FSReqCallback14SetReturnValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7849:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	ret
	.cfi_endproc
.LFE7849:
	.size	_ZN4node2fs13FSReqCallback14SetReturnValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node2fs13FSReqCallback14SetReturnValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED2Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED2Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED2Ev:
.LFB10782:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L29:
	ret
	.cfi_endproc
.LFE10782:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED2Ev
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED1Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED2Ev
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED2Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED2Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED2Ev:
.LFB10832:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L31:
	ret
	.cfi_endproc
.LFE10832:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED2Ev
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED1Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED2Ev
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv:
.LFB11370:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE11370:
	.size	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv:
.LFB11371:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE11371:
	.size	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11372:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11372:
	.size	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv
	.type	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv, @function
_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv:
.LFB11374:
	.cfi_startproc
	endbr64
	movl	$544, %eax
	ret
	.cfi_endproc
.LFE11374:
	.size	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv, .-_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB11375:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE11375:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11376:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11376:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB11378:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE11378:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv:
.LFB11389:
	.cfi_startproc
	endbr64
	movl	$696, %eax
	ret
	.cfi_endproc
.LFE11389:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv:
.LFB11400:
	.cfi_startproc
	endbr64
	movl	$696, %eax
	ret
	.cfi_endproc
.LFE11400:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB11324:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L42
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	ret
	.cfi_endproc
.LFE11324:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node2fs18FSContinuationDataD2Ev,"axG",@progbits,_ZN4node2fs18FSContinuationDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs18FSContinuationDataD2Ev
	.type	_ZN4node2fs18FSContinuationDataD2Ev, @function
_ZN4node2fs18FSContinuationDataD2Ev:
.LFB9296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs18FSContinuationDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	40(%rbx), %r13
	movq	32(%rbx), %r12
	cmpq	%r12, %r13
	je	.L46
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L47
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L50
.L48:
	movq	32(%rbx), %r12
.L46:
	testq	%r12, %r12
	je	.L44
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L50
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L44:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9296:
	.size	_ZN4node2fs18FSContinuationDataD2Ev, .-_ZN4node2fs18FSContinuationDataD2Ev
	.weak	_ZN4node2fs18FSContinuationDataD1Ev
	.set	_ZN4node2fs18FSContinuationDataD1Ev,_ZN4node2fs18FSContinuationDataD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB11326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11326:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED0Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED0Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED0Ev:
.LFB10784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	(%rdi), %rax
	call	*8(%rax)
.L57:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10784:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED0Ev
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED0Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED0Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED0Ev:
.LFB10834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L63
	movq	(%rdi), %rax
	call	*8(%rax)
.L63:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10834:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED0Ev
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7714:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L73
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L73:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7714:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7713:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L79
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L79:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7713:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_
	.type	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_, @function
_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_:
.LFB10080:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rdx
	leaq	-88(%rdi), %rcx
	subl	$1, 2156(%rdx)
	js	.L85
	jmp	*80(%rcx)
	.p2align 4,,10
	.p2align 3
.L85:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10080:
	.size	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_, .-_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.type	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv, @function
_ZN4node7ReqWrapI7uv_fs_sE6CancelEv:
.LFB11369:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L88
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE11369:
	.size	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv, .-_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB11318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	movq	%rax, (%r12)
	call	uv_buf_init@PLT
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	testq	%r13, %r13
	je	.L90
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L96
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L90:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11318:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.rodata._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Closing file descriptor %d on garbage collection"
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_:
.LFB11366:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	xorl	%eax, %eax
	movq	%rsi, %rdi
	movl	28(%r8), %edx
	leaq	.LC1(%rip), %rsi
	jmp	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz@PLT
	.cfi_endproc
.LFE11366:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_
	.section	.rodata._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Closing file descriptor %d on garbage collection failed"
	.section	.rodata._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_.str1.1,"aMS",@progbits,1
.LC3:
	.string	"close"
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_:
.LFB11368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$70, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	leaq	-144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$70, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movl	28(%rdi), %r9d
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	352(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	24(%rbx), %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%r12), %r12
	movq	%r14, %rcx
	leaq	.LC3(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11368:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"promise"
.LC5:
	.string	"ref"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node2fs10FileHandle8CloseReq10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs10FileHandle8CloseReq10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs10FileHandle8CloseReq10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	528(%rdi), %rax
	movq	(%rsi), %rdi
	testq	%rax, %rax
	je	.L103
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L122
	movq	8(%rbx), %r13
	leaq	-48(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L113
	cmpq	72(%rbx), %rcx
	je	.L123
.L106:
	movq	-8(%rcx), %rsi
.L105:
	leaq	.LC4(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L122:
	movq	(%rbx), %rdi
.L103:
	movq	536(%r12), %rax
	testq	%rax, %rax
	je	.L102
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, -48(%rbp)
	testq	%rax, %rax
	je	.L102
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	16(%rax), %r13
	call	*(%rax)
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L114
	cmpq	72(%rbx), %rcx
	je	.L124
.L111:
	movq	-8(%rcx), %rsi
.L110:
	leaq	.LC5(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L102:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L123:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%esi, %esi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%esi, %esi
	jmp	.L105
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7792:
	.size	_ZNK4node2fs10FileHandle8CloseReq10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs10FileHandle8CloseReq10MemoryInfoEPNS_13MemoryTrackerE
	.p2align 4
	.type	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0, @function
_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0:
.LFB11767:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edi, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	testq	%rax, %rax
	je	.L131
	movq	%rax, %rdi
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	xorl	%r14d, %r14d
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L140
.L127:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %rax
	call	*8(%rax)
.L128:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L126
	movq	(%rdi), %rax
	call	*8(%rax)
.L126:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L140:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	movsbl	%bl, %esi
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L127
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11767:
	.size	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0, .-_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	.section	.rodata.str1.1
.LC6:
	.string	"node,node.fs,node.fs.sync"
	.text
	.p2align 4
	.type	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0, @function
_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0:
.LFB11765:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	testq	%rax, %rax
	je	.L144
	movq	%rax, %rdi
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r8
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L147
	movq	%r8, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	leaq	.LC6(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r8
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE11765:
	.size	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0, .-_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	.p2align 4
	.type	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1, @function
_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1:
.LFB11766:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movq	%rdx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$8, (%rcx)
	movaps	%xmm0, -80(%rbp)
	jne	.L149
	movq	(%r8), %rax
	movq	%rax, -80(%rbp)
.L149:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L154
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	xorl	%r14d, %r14d
	movq	24(%rax), %r10
	cmpq	%rdx, %r10
	jne	.L163
.L150:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L151
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L151:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L148
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L148:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L163:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$69, %esi
	pushq	%rax
	pushq	%rbx
	pushq	%r15
	pushq	-88(%rbp)
	pushq	$1
	pushq	$0
	call	*%r10
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L150
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11766:
	.size	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1, .-_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	.section	.text._ZN4node2fs10FileHandle12GetAsyncWrapEv,"axG",@progbits,_ZN4node2fs10FileHandle12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs10FileHandle12GetAsyncWrapEv
	.type	_ZThn56_N4node2fs10FileHandle12GetAsyncWrapEv, @function
_ZThn56_N4node2fs10FileHandle12GetAsyncWrapEv:
.LFB11719:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE11719:
	.size	_ZThn56_N4node2fs10FileHandle12GetAsyncWrapEv, .-_ZThn56_N4node2fs10FileHandle12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv:
.LFB11720:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE11720:
	.size	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.type	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv, @function
_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv:
.LFB11721:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE11721:
	.size	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv, .-_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv, @function
_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv:
.LFB11722:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE11722:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv, .-_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11723:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11723:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv:
.LFB11724:
	.cfi_startproc
	endbr64
	movl	$544, %eax
	ret
	.cfi_endproc
.LFE11724:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv, .-_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11725:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11725:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB11726:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE11726:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node2fs10FileHandle9IsClosingEv,"axG",@progbits,_ZN4node2fs10FileHandle9IsClosingEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs10FileHandle9IsClosingEv
	.type	_ZThn56_N4node2fs10FileHandle9IsClosingEv, @function
_ZThn56_N4node2fs10FileHandle9IsClosingEv:
.LFB11729:
	.cfi_startproc
	endbr64
	movzbl	68(%rdi), %eax
	ret
	.cfi_endproc
.LFE11729:
	.size	_ZThn56_N4node2fs10FileHandle9IsClosingEv, .-_ZThn56_N4node2fs10FileHandle9IsClosingEv
	.section	.text._ZN4node2fs10FileHandle5GetFDEv,"axG",@progbits,_ZN4node2fs10FileHandle5GetFDEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs10FileHandle5GetFDEv
	.type	_ZThn56_N4node2fs10FileHandle5GetFDEv, @function
_ZThn56_N4node2fs10FileHandle5GetFDEv:
.LFB11730:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	ret
	.cfi_endproc
.LFE11730:
	.size	_ZThn56_N4node2fs10FileHandle5GetFDEv, .-_ZThn56_N4node2fs10FileHandle5GetFDEv
	.text
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZThn56_N4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZThn56_N4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB11731:
	.cfi_startproc
	endbr64
	movl	$-38, %eax
	ret
	.cfi_endproc
.LFE11731:
	.size	_ZThn56_N4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZThn56_N4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandle8ReadStopEv
	.type	_ZThn56_N4node2fs10FileHandle8ReadStopEv, @function
_ZThn56_N4node2fs10FileHandle8ReadStopEv:
.LFB11732:
	.cfi_startproc
	endbr64
	movb	$0, 88(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11732:
	.size	_ZThn56_N4node2fs10FileHandle8ReadStopEv, .-_ZThn56_N4node2fs10FileHandle8ReadStopEv
	.section	.text._ZN4node2fs10FileHandle7IsAliveEv,"axG",@progbits,_ZN4node2fs10FileHandle7IsAliveEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs10FileHandle7IsAliveEv
	.type	_ZThn56_N4node2fs10FileHandle7IsAliveEv, @function
_ZThn56_N4node2fs10FileHandle7IsAliveEv:
.LFB11733:
	.cfi_startproc
	endbr64
	movzbl	69(%rdi), %eax
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE11733:
	.size	_ZThn56_N4node2fs10FileHandle7IsAliveEv, .-_ZThn56_N4node2fs10FileHandle7IsAliveEv
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv:
.LFB11740:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L180
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE11740:
	.size	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.section	.text._ZN4node2fs18FSContinuationDataD0Ev,"axG",@progbits,_ZN4node2fs18FSContinuationDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs18FSContinuationDataD0Ev
	.type	_ZN4node2fs18FSContinuationDataD0Ev, @function
_ZN4node2fs18FSContinuationDataD0Ev:
.LFB9298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs18FSContinuationDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	40(%r13), %rbx
	movq	32(%r13), %r12
	cmpq	%r12, %rbx
	je	.L183
	.p2align 4,,10
	.p2align 3
.L187:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L184
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L187
.L185:
	movq	32(%r13), %r12
.L183:
	testq	%r12, %r12
	je	.L188
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L188:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L187
	jmp	.L185
	.cfi_endproc
.LFE9298:
	.size	_ZN4node2fs18FSContinuationDataD0Ev, .-_ZN4node2fs18FSContinuationDataD0Ev
	.text
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZThn56_N4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE, @function
_ZThn56_N4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE:
.LFB11756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpq	$0, 96(%rsi)
	movl	64(%rdi), %edx
	movb	$1, 68(%rdi)
	movq	%rax, 104(%rsi)
	jne	.L197
	movq	%rsi, %rbx
	leaq	_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_(%rip), %rax
	leaq	104(%rsi), %rsi
	movq	%rax, -8(%rsi)
	movq	32(%rbx), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	js	.L195
	movq	32(%rbx), %rax
	addl	$1, 2156(%rax)
.L195:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11756:
	.size	_ZThn56_N4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE, .-_ZThn56_N4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE, @function
_ZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE:
.LFB7841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpq	$0, 96(%rsi)
	movl	120(%rdi), %edx
	movb	$1, 124(%rdi)
	movq	%rax, 104(%rsi)
	jne	.L202
	movq	%rsi, %rbx
	leaq	_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_(%rip), %rax
	leaq	104(%rsi), %rsi
	movq	%rax, -8(%rsi)
	movq	32(%rbx), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	js	.L200
	movq	32(%rbx), %rax
	addl	$1, 2156(%rax)
.L200:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7841:
	.size	_ZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE, .-_ZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev:
.LFB11746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r13
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L203
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L207
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11746:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB11752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-40(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r14
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L209
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L215
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L209:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$96, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11752:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev:
.LFB11316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rbx), %r12
	movq	32(%rbx), %r13
	movq	%rax, (%rbx)
	call	uv_buf_init@PLT
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%r12, %r12
	je	.L216
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L220
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11316:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE
	.type	_ZN4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE, @function
_ZN4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE:
.LFB7840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$544, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	leaq	56(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L227
	cmpw	$1040, %cx
	jne	.L222
.L227:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L229
.L225:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	88(%rbx), %rbx
	leaq	16(%r12), %rdi
	movq	%r13, %rdx
	movsd	.LC7(%rip), %xmm0
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movl	$26, %ecx
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 72(%r12)
	leaq	80(%r12), %rax
	movq	%rax, 80(%r12)
	movq	%rax, 88(%r12)
	je	.L230
	movq	2112(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rdx, 80(%r12)
	leaq	2112(%rbx), %rdx
	movq	%rax, 16(%r12)
	addq	$112, %rax
	movq	%rax, 72(%r12)
	movq	%r12, %rax
	movq	%rdx, 88(%r12)
	movq	$0, 96(%r12)
	movq	$0, 104(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L225
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7840:
	.size	_ZN4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE, .-_ZN4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE
	.type	_ZThn56_N4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE, @function
_ZThn56_N4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE:
.LFB11759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$544, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L237
	cmpw	$1040, %cx
	jne	.L232
.L237:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L239
.L235:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	32(%rbx), %rbx
	leaq	16(%r12), %rdi
	movq	%r13, %rdx
	movsd	.LC7(%rip), %xmm0
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movl	$26, %ecx
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 72(%r12)
	leaq	80(%r12), %rax
	movq	%rax, 80(%r12)
	movq	%rax, 88(%r12)
	je	.L240
	movq	2112(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rdx, 80(%r12)
	leaq	2112(%rbx), %rdx
	movq	%rax, 16(%r12)
	addq	$112, %rax
	movq	%rax, 72(%r12)
	movq	%r12, %rax
	movq	%rdx, 88(%r12)
	movq	$0, 96(%r12)
	movq	$0, 104(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L235
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11759:
	.size	_ZThn56_N4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE, .-_ZThn56_N4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE
	.section	.text._ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$96, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L246
	cmpw	$1040, %cx
	jne	.L242
.L246:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L248
.L245:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	32(%rbx), %rsi
	leaq	40(%r12), %rdi
	movsd	.LC7(%rip), %xmm0
	movq	%rdx, 32(%r12)
	movl	$39, %ecx
	movq	%r13, %rdx
	movq	%rax, 24(%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L245
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7741:
	.size	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.type	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev, @function
_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev:
.LFB11753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -72(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L252
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	leaq	-72(%rdi), %r12
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$544, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11753:
	.size	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev, .-_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev:
.LFB11754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -16(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L256
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	leaq	-16(%rdi), %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$544, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11754:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.text
	.p2align 4
	.globl	_ZThn56_N4node2fs18FileHandleReadWrapD1Ev
	.type	_ZThn56_N4node2fs18FileHandleReadWrapD1Ev, @function
_ZThn56_N4node2fs18FileHandleReadWrapD1Ev:
.LFB11739:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L262
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11739:
	.size	_ZThn56_N4node2fs18FileHandleReadWrapD1Ev, .-_ZThn56_N4node2fs18FileHandleReadWrapD1Ev
	.p2align 4
	.globl	_ZThn56_N4node2fs18FileHandleReadWrapD0Ev
	.type	_ZThn56_N4node2fs18FileHandleReadWrapD0Ev, @function
_ZThn56_N4node2fs18FileHandleReadWrapD0Ev:
.LFB11755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L266
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$552, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11755:
	.size	_ZThn56_N4node2fs18FileHandleReadWrapD0Ev, .-_ZThn56_N4node2fs18FileHandleReadWrapD0Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.type	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev, @function
_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev:
.LFB11744:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rax, -72(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L272
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11744:
	.size	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev, .-_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev:
.LFB11745:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rax, -16(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L278
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11745:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs18FileHandleReadWrapD2Ev
	.type	_ZN4node2fs18FileHandleReadWrapD2Ev, @function
_ZN4node2fs18FileHandleReadWrapD2Ev:
.LFB7755:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L284
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L284:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7755:
	.size	_ZN4node2fs18FileHandleReadWrapD2Ev, .-_ZN4node2fs18FileHandleReadWrapD2Ev
	.globl	_ZN4node2fs18FileHandleReadWrapD1Ev
	.set	_ZN4node2fs18FileHandleReadWrapD1Ev,_ZN4node2fs18FileHandleReadWrapD2Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev
	.type	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev, @function
_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev:
.LFB11312:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, 16(%rdi)
	addq	$112, %rax
	cmpq	$0, 24(%rdi)
	movq	%rax, 72(%rdi)
	je	.L290
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rax
	leaq	16(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11312:
	.size	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev, .-_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.set	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs18FileHandleReadWrapD0Ev
	.type	_ZN4node2fs18FileHandleReadWrapD0Ev, @function
_ZN4node2fs18FileHandleReadWrapD0Ev:
.LFB7757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L294
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$552, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7757:
	.size	_ZN4node2fs18FileHandleReadWrapD0Ev, .-_ZN4node2fs18FileHandleReadWrapD0Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.type	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev, @function
_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev:
.LFB11314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, 16(%rdi)
	addq	$112, %rax
	cmpq	$0, 24(%rdi)
	movq	%rax, 72(%rdi)
	je	.L298
	movq	80(%r12), %rdx
	movq	88(%r12), %rax
	addq	$16, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$544, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11314:
	.size	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev, .-_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB11737:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE11737:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev:
.LFB11736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC8(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11736:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev, .-_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev:
.LFB11388:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8030569533516436294, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1702062445, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE11388:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev:
.LFB11373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC8(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L308
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L308:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11373:
	.size	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev:
.LFB6091:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$10, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$7236828614798108998, %rcx
	movq	%rdx, (%rdi)
	movl	$25964, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE6091:
	.size	_ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev:
.LFB6078:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1667326572, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7809597383147410246, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$107, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE6078:
	.size	_ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB11377:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE11377:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev:
.LFB6066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC9(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$24948, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L315:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6066:
	.size	_ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev:
.LFB6083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC10(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L319:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6083:
	.size	_ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev:
.LFB11399:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8030569533516436294, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1702062445, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE11399:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev:
.LFB6093:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$8, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8171027694615489603, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE6093:
	.size	_ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle8CloseReqD2Ev
	.type	_ZN4node2fs10FileHandle8CloseReqD2Ev, @function
_ZN4node2fs10FileHandle8CloseReqD2Ev:
.LFB7789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs10FileHandle8CloseReqE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$88, %rdi
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	addq	$112, %rax
	movq	%rax, -32(%rdi)
	call	uv_fs_req_cleanup@PLT
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L323
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	536(%r12), %rdi
	movq	$0, 528(%r12)
	testq	%rdi, %rdi
	je	.L324
.L325:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L324
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L324:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L337
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movq	536(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L325
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7789:
	.size	_ZN4node2fs10FileHandle8CloseReqD2Ev, .-_ZN4node2fs10FileHandle8CloseReqD2Ev
	.globl	_ZN4node2fs10FileHandle8CloseReqD1Ev
	.set	_ZN4node2fs10FileHandle8CloseReqD1Ev,_ZN4node2fs10FileHandle8CloseReqD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node2fs13FSReqCallback6RejectEN2v85LocalINS2_5ValueEEE
	.type	_ZN4node2fs13FSReqCallback6RejectEN2v85LocalINS2_5ValueEEE, @function
_ZN4node2fs13FSReqCallback6RejectEN2v85LocalINS2_5ValueEEE:
.LFB7845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	%rsi, -24(%rbp)
	movq	360(%rax), %rdx
	movq	1136(%rdx), %r13
	testq	%rdi, %rdi
	je	.L339
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L352
.L339:
	movq	3280(%rax), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L338
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L343
.L338:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%r12), %rax
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	-24(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7845:
	.size	_ZN4node2fs13FSReqCallback6RejectEN2v85LocalINS2_5ValueEEE, .-_ZN4node2fs13FSReqCallback6RejectEN2v85LocalINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0, @function
_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0:
.LFB11769:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L354
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L388
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L372
	cmpw	$1040, %cx
	jne	.L356
.L372:
	movq	23(%rdx), %r12
.L353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L389
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	2784(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L360
	movq	3120(%rbx), %rdi
	movq	3280(%rbx), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L360
	movq	3280(%rbx), %rdi
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L360
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	1424(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L360
	movl	$696, %edi
	call	_Znwm@PLT
	movl	$8, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movsd	.LC7(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L390
	movq	2112(%rbx), %rdx
	movdqa	.LC11(%rip), %xmm0
	leaq	-144(%rbp), %r14
	leaq	-112(%rbp), %r15
	movq	%r14, %rdi
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	584(%r12), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%rbx), %rdx
	movq	352(%rbx), %rbx
	movq	%rax, 576(%r12)
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rbx, %rsi
	movq	%rdx, 72(%r12)
	movq	%rax, 56(%r12)
	movups	%xmm0, 560(%r12)
	movdqa	.LC12(%rip), %xmm0
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 528(%r12)
	movl	$1, 536(%r12)
	movb	$0, 540(%r12)
	movq	$0, 544(%r12)
	movb	$0, 552(%r12)
	movb	$0, 584(%r12)
	movb	$0, 648(%r12)
	movq	%rbx, 656(%r12)
	movq	$0, 688(%r12)
	movups	%xmm0, 664(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	656(%r12), %rdi
	movl	$144, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movl	$18, %edx
	movq	%r13, %rdi
	movq	672(%r12), %rsi
	movq	%rax, 680(%r12)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L391
	movq	%rbx, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	688(%r12), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L366
.L365:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-112(%rbp), %rax
	movq	$0, 688(%r12)
.L366:
	testq	%rax, %rax
	je	.L364
	movq	%rax, 688(%r12)
	leaq	688(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L364:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L360:
	xorl	%r12d, %r12d
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L391:
	movq	688(%r12), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L365
	jmp	.L364
.L390:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11769:
	.size	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0, .-_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	.align 2
	.p2align 4
	.globl	_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE
	.type	_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE, @function
_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE:
.LFB7847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	$2, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rsi, -56(%rbp)
	movq	352(%rax), %r8
	leaq	104(%r8), %rdx
	movq	%rdx, -64(%rbp)
	movq	(%rsi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L393
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L411
.L393:
	movq	360(%rax), %rdx
	movq	8(%r12), %rdi
	movq	1136(%rdx), %r13
	testq	%rdi, %rdi
	je	.L394
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L412
.L394:
	movq	3280(%rax), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L392
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L397
.L392:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L413
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	xorl	%r14d, %r14d
	cmpl	$5, 43(%rdx)
	setne	%r14b
	addl	$1, %r14d
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L412:
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%r12), %rax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	-64(%rbp), %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L392
.L413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7847:
	.size	_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE, .-_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t:
.LFB11403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	680(%rdi), %rax
	movq	%rdx, (%rax)
	movq	8(%rsi), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rsi), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rsi), %rdx
	movq	%rdx, 40(%rax)
	movq	64(%rsi), %rdx
	movq	%rdx, 48(%rax)
	movq	48(%rsi), %rdx
	movq	%rdx, 56(%rax)
	movq	56(%rsi), %rdx
	movq	%rdx, 64(%rax)
	movq	72(%rsi), %rdx
	movq	%rdx, 72(%rax)
	movq	96(%rsi), %rdx
	movq	%rdx, 80(%rax)
	movq	104(%rsi), %rdx
	movq	%rdx, 88(%rax)
	movq	112(%rsi), %rdx
	movq	688(%rdi), %r12
	movq	%rdx, 96(%rax)
	movq	120(%rsi), %rdx
	movq	%rdx, 104(%rax)
	movq	128(%rsi), %rdx
	movq	%rdx, 112(%rax)
	movq	136(%rsi), %rdx
	movq	%rdx, 120(%rax)
	movq	144(%rsi), %rdx
	movq	%rdx, 128(%rax)
	movq	152(%rsi), %rdx
	movq	%rdx, 136(%rax)
	testq	%r12, %r12
	je	.L415
	movq	(%r12), %rsi
	movq	656(%rdi), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L415:
	movq	16(%rbx), %rax
	movb	$1, 648(%rbx)
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L416
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L427
.L416:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L428
.L417:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L429
.L418:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L429:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L417
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11403:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE:
.LFB11393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L432
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L440
.L432:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L441
.L433:
	movq	(%r12), %rbx
	call	_ZN2v87Promise8Resolver10GetPromiseEv@PLT
	testq	%rax, %rax
	je	.L442
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L442:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rdi
	jmp	.L433
	.cfi_endproc
.LFE11393:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE:
.LFB11404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L444
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L452
.L444:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L453
.L445:
	movq	(%r12), %rbx
	call	_ZN2v87Promise8Resolver10GetPromiseEv@PLT
	testq	%rax, %rax
	je	.L454
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L454:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rdi
	jmp	.L445
	.cfi_endproc
.LFE11404:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE:
.LFB11401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L456
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L464
.L456:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L465
.L457:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L466
.L458:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L467
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L466:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L457
.L467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11401:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE:
.LFB11402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L469
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L477
.L469:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L478
.L470:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L479
.L471:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L480
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L479:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L470
.L480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11402:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE:
.LFB11390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L482
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L490
.L482:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L491
.L483:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L492
.L484:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L492:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L483
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11390:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE:
.LFB11391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L495
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L503
.L495:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L504
.L496:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L505
.L497:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L505:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L496
.L506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11391:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.section	.text.unlikely,"ax",@progbits
.LCOLDB13:
	.text
.LHOTB13:
	.p2align 4
	.type	_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_, @function
_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_:
.LFB7843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$88, %rdi
	je	.L508
	movq	-96(%rdi), %rbx
	leaq	-88(%rdi), %r13
	movq	%rdi, %r12
	testq	%rbx, %rbx
	je	.L509
	movl	$256, %eax
	cmpb	$0, 88(%rbx)
	movl	$-1, 64(%rbx)
	movw	%ax, 68(%rbx)
	jne	.L514
.L510:
	movq	%r12, %rdi
	movq	88(%r12), %rbx
	leaq	-16(%r13), %r12
	call	uv_fs_req_cleanup@PLT
	movq	-16(%r13), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-16(%r13), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L515
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	cmpq	$0, -48(%rbx)
	je	.L510
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	8(%rbx), %rdi
	movq	$-4095, %rsi
	movq	%rax, -64(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -56(%rbp)
	leaq	-64(%rbp), %rdx
	call	*24(%rax)
	jmp	.L510
.L515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_.cold, @function
_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_.cold:
.LFSB7843:
.L509:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movb	$0, 124
	ud2
.L508:
	movq	8, %rax
	ud2
	.cfi_endproc
.LFE7843:
	.text
	.size	_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_, .-_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_
	.section	.text.unlikely
	.size	_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_.cold, .-_ZZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapEENUlP7uv_fs_sE_4_FUNES5_.cold
.LCOLDE13:
	.text
.LHOTE13:
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle9ReadStartEv
	.type	_ZN4node2fs10FileHandle9ReadStartEv, @function
_ZN4node2fs10FileHandle9ReadStartEv:
.LFB7809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 125(%rdi)
	jne	.L535
	cmpb	$0, 124(%rdi)
	movq	%rdi, %rbx
	jne	.L535
	cmpq	$0, 152(%rdi)
	movb	$1, 144(%rdi)
	je	.L518
.L546:
	xorl	%eax, %eax
.L516:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L547
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	cmpq	$0, 136(%rdi)
	je	.L548
	movq	16(%rdi), %rax
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %r12
	movsd	40(%rbx), %xmm0
	movq	1216(%r12), %rax
	movl	24(%rax), %esi
	testl	%esi, %esi
	je	.L520
	comisd	.LC14(%rip), %xmm0
	jb	.L549
.L520:
	movq	1256(%r12), %rax
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movq	2352(%r12), %rax
	movsd	%xmm1, -88(%rbp)
	cmpq	%rax, 2344(%r12)
	je	.L521
	movq	-8(%rax), %r13
	subq	$8, %rax
	movq	352(%r12), %rdi
	movq	%rax, 2352(%r12)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	8(%r13), %rcx
	movq	%rax, %r15
	testq	%rcx, %rcx
	je	.L522
	movzbl	11(%rcx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L550
.L522:
	movq	16(%rbx), %rax
	movq	%r15, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	824(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movsd	.LC7(%rip), %xmm0
	call	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb@PLT
	movq	%rbx, 528(%r13)
.L523:
	movq	1256(%r12), %rax
	movsd	-88(%rbp), %xmm2
	movq	%r14, %rdi
	movsd	%xmm2, 24(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	136(%rbx), %rax
	movl	$65536, %esi
	testq	%rax, %rax
	js	.L526
	cmpq	$65536, %rax
	cmovle	%rax, %rsi
.L526:
	movq	64(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, 536(%r13)
	movq	%rdx, 544(%r13)
	movq	152(%rbx), %r12
	movq	%r13, 152(%rbx)
	testq	%r12, %r12
	je	.L527
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L551
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movl	$552, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	152(%rbx), %r13
.L527:
	cmpq	$0, 80(%r13)
	movq	128(%rbx), %r9
	leaq	536(%r13), %rcx
	movl	120(%rbx), %edx
	movq	%r13, 88(%r13)
	jne	.L552
	leaq	_ZZN4node2fs10FileHandle9ReadStartEvENUlP7uv_fs_sE_4_FUNES3_(%rip), %rax
	subq	$8, %rsp
	leaq	88(%r13), %rsi
	movl	$1, %r8d
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rax
	pushq	%rax
	call	uv_fs_read@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	js	.L546
	movq	16(%r13), %rax
	addl	$1, 2156(%rax)
	xorl	%eax, %eax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L521:
	movq	3112(%r12), %rdi
	movq	3280(%r12), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L553
	movl	$552, %edi
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	16(%rbx), %r15
	movq	-96(%rbp), %rdx
	movl	$7, %ecx
	movsd	.LC7(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%r15, %rsi
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r15)
	movq	%rax, 56(%r13)
	leaq	64(%r13), %rax
	movq	%rax, 64(%r13)
	movq	%rax, 72(%r13)
	je	.L554
	movq	2112(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r15)
	leaq	16+_ZTVN4node2fs18FileHandleReadWrapE(%rip), %rax
	movq	%rdx, 64(%r13)
	leaq	2112(%r15), %rdx
	movq	%rax, 0(%r13)
	addq	$112, %rax
	movq	%rdx, 72(%r13)
	movq	$0, 80(%r13)
	movq	$0, 88(%r13)
	movq	%rax, 56(%r13)
	movq	%rbx, 528(%r13)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L548:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	64(%rbx), %rdi
	movq	$-4095, %rsi
	movq	%rax, -80(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -72(%rbp)
	leaq	-80(%rbp), %rdx
	call	*24(%rax)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L550:
	movq	16(%r13), %rax
	movq	(%rcx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L549:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L552:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L535:
	movl	$-4095, %eax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L551:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L554:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L547:
	call	__stack_chk_fail@PLT
.L553:
	movq	1256(%r12), %rax
	movsd	-88(%rbp), %xmm3
	movq	%r14, %rdi
	movsd	%xmm3, 24(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$-16, %eax
	jmp	.L516
	.cfi_endproc
.LFE7809:
	.size	_ZN4node2fs10FileHandle9ReadStartEv, .-_ZN4node2fs10FileHandle9ReadStartEv
	.set	.LTHUNK13,_ZN4node2fs10FileHandle9ReadStartEv
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandle9ReadStartEv
	.type	_ZThn56_N4node2fs10FileHandle9ReadStartEv, @function
_ZThn56_N4node2fs10FileHandle9ReadStartEv:
.LFB11797:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK13
	.cfi_endproc
.LFE11797:
	.size	_ZThn56_N4node2fs10FileHandle9ReadStartEv, .-_ZThn56_N4node2fs10FileHandle9ReadStartEv
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle9ReleaseFDERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node2fs10FileHandle9ReleaseFDERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node2fs10FileHandle9ReleaseFDERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L566
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L564
	cmpw	$1040, %cx
	jne	.L557
.L564:
	movq	23(%rdx), %rbx
.L559:
	testq	%rbx, %rbx
	je	.L555
	movl	$256, %eax
	cmpb	$0, 144(%rbx)
	movl	$-1, 120(%rbx)
	movw	%ax, 124(%rbx)
	jne	.L567
.L555:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L568
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	cmpq	$0, 8(%rbx)
	je	.L555
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	64(%rbx), %rdi
	movq	$-4095, %rsi
	movq	%rax, -48(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	call	*24(%rax)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L557:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7803:
	.size	_ZN4node2fs10FileHandle9ReleaseFDERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node2fs10FileHandle9ReleaseFDERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t:
.LFB11392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	680(%rdi), %rax
	testq	%rdx, %rdx
	js	.L570
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	8(%rsi), %rdx
	movsd	%xmm0, (%rax)
	testq	%rdx, %rdx
	js	.L572
.L622:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	16(%rsi), %rdx
	movsd	%xmm0, 8(%rax)
	testq	%rdx, %rdx
	js	.L574
.L623:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	24(%rsi), %rdx
	movsd	%xmm0, 16(%rax)
	testq	%rdx, %rdx
	js	.L576
.L624:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	32(%rsi), %rdx
	movsd	%xmm0, 24(%rax)
	testq	%rdx, %rdx
	js	.L578
.L625:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	40(%rsi), %rdx
	movsd	%xmm0, 32(%rax)
	testq	%rdx, %rdx
	js	.L580
.L626:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	64(%rsi), %rdx
	movsd	%xmm0, 40(%rax)
	testq	%rdx, %rdx
	js	.L582
.L627:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	48(%rsi), %rdx
	movsd	%xmm0, 48(%rax)
	testq	%rdx, %rdx
	js	.L584
.L628:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	56(%rsi), %rdx
	movsd	%xmm0, 56(%rax)
	testq	%rdx, %rdx
	js	.L586
.L629:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	72(%rsi), %rdx
	movsd	%xmm0, 64(%rax)
	testq	%rdx, %rdx
	js	.L588
.L630:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	96(%rsi), %rdx
	movsd	%xmm0, 72(%rax)
	testq	%rdx, %rdx
	js	.L590
.L631:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	104(%rsi), %rdx
	movsd	%xmm0, 80(%rax)
	testq	%rdx, %rdx
	js	.L592
.L632:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	112(%rsi), %rdx
	movsd	%xmm0, 88(%rax)
	testq	%rdx, %rdx
	js	.L594
.L633:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	120(%rsi), %rdx
	movsd	%xmm0, 96(%rax)
	testq	%rdx, %rdx
	js	.L596
.L634:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L597:
	movq	128(%rsi), %rdx
	movsd	%xmm0, 104(%rax)
	testq	%rdx, %rdx
	js	.L598
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L599:
	movq	136(%rsi), %rdx
	movsd	%xmm0, 112(%rax)
	testq	%rdx, %rdx
	js	.L600
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L601:
	movq	144(%rsi), %rdx
	movsd	%xmm0, 120(%rax)
	testq	%rdx, %rdx
	js	.L602
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L603:
	movq	152(%rsi), %rdx
	movsd	%xmm0, 128(%rax)
	testq	%rdx, %rdx
	js	.L604
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L605:
	movq	688(%rbx), %r12
	movsd	%xmm0, 136(%rax)
	testq	%r12, %r12
	je	.L606
	movq	(%r12), %rsi
	movq	656(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L606:
	movq	16(%rbx), %rax
	movb	$1, 648(%rbx)
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L607
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L618
.L607:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L619
.L608:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L620
.L609:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L621
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	8(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, (%rax)
	testq	%rdx, %rdx
	jns	.L622
.L572:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	16(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rax)
	testq	%rdx, %rdx
	jns	.L623
.L574:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	24(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rax)
	testq	%rdx, %rdx
	jns	.L624
.L576:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	32(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rax)
	testq	%rdx, %rdx
	jns	.L625
.L578:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	40(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rax)
	testq	%rdx, %rdx
	jns	.L626
.L580:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	64(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 40(%rax)
	testq	%rdx, %rdx
	jns	.L627
.L582:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	48(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 48(%rax)
	testq	%rdx, %rdx
	jns	.L628
.L584:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	56(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 56(%rax)
	testq	%rdx, %rdx
	jns	.L629
.L586:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	72(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 64(%rax)
	testq	%rdx, %rdx
	jns	.L630
.L588:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	96(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rax)
	testq	%rdx, %rdx
	jns	.L631
.L590:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	104(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 80(%rax)
	testq	%rdx, %rdx
	jns	.L632
.L592:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	112(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 88(%rax)
	testq	%rdx, %rdx
	jns	.L633
.L594:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	120(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 96(%rax)
	testq	%rdx, %rdx
	jns	.L634
.L596:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L602:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L600:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L618:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L620:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L619:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L608
.L621:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11392:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev:
.LFB11741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L651
	movq	%rdi, %rbx
	leaq	-56(%rdi), %r12
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L637
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L637:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	520(%rbx), %rdi
	movq	%rax, -56(%rbx)
	addq	$144, %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L638
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L638
	call	free@PLT
.L638:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L639
	movq	(%rdi), %rax
	call	*8(%rax)
.L639:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L652
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L652:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11741:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev:
.LFB11758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L669
	movq	%rdi, %rbx
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L655
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L655:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	520(%rbx), %rdi
	leaq	-56(%rbx), %r12
	movq	%rax, -56(%rbx)
	addq	$144, %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L656
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L656
	call	free@PLT
.L656:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L657
	movq	(%rdi), %rax
	call	*8(%rax)
.L657:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L670
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11758:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.section	.text._ZN4node2fs13FSReqCallbackD2Ev,"axG",@progbits,_ZN4node2fs13FSReqCallbackD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs13FSReqCallbackD1Ev
	.type	_ZThn56_N4node2fs13FSReqCallbackD1Ev, @function
_ZThn56_N4node2fs13FSReqCallbackD1Ev:
.LFB11734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-56(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	movq	%rax, (%rdi)
	movq	520(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L672
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L672
	call	free@PLT
.L672:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L673
	movq	(%rdi), %rax
	call	*8(%rax)
.L673:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L682
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11734:
	.size	_ZThn56_N4node2fs13FSReqCallbackD1Ev, .-_ZThn56_N4node2fs13FSReqCallbackD1Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev:
.LFB11757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L699
	movq	%rdi, %rbx
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L685
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L685:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	520(%rbx), %rdi
	leaq	-56(%rbx), %r12
	movq	%rax, -56(%rbx)
	addq	$144, %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L686
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L686
	call	free@PLT
.L686:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L687
	movq	(%rdi), %rax
	call	*8(%rax)
.L687:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L700
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L700:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11757:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.section	.text._ZN4node2fs13FSReqCallbackD0Ev,"axG",@progbits,_ZN4node2fs13FSReqCallbackD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs13FSReqCallbackD0Ev
	.type	_ZThn56_N4node2fs13FSReqCallbackD0Ev, @function
_ZThn56_N4node2fs13FSReqCallbackD0Ev:
.LFB11748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-56(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	movq	%rax, (%rdi)
	movq	520(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L702
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L702
	call	free@PLT
.L702:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L703
	movq	(%rdi), %rax
	call	*8(%rax)
.L703:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L712
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$648, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11748:
	.size	_ZThn56_N4node2fs13FSReqCallbackD0Ev, .-_ZThn56_N4node2fs13FSReqCallbackD0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev:
.LFB11743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L729
	movq	%rdi, %rbx
	leaq	-56(%rdi), %r12
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L715
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L715:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	520(%rbx), %rdi
	movq	%rax, -56(%rbx)
	addq	$144, %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L716
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L716
	call	free@PLT
.L716:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L717
	movq	(%rdi), %rax
	call	*8(%rax)
.L717:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L730
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L729:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L730:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11743:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.section	.text._ZN4node2fs13FSReqCallbackD2Ev,"axG",@progbits,_ZN4node2fs13FSReqCallbackD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs13FSReqCallbackD2Ev
	.type	_ZN4node2fs13FSReqCallbackD2Ev, @function
_ZN4node2fs13FSReqCallbackD2Ev:
.LFB11308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$144, %rax
	movq	%rax, 56(%rdi)
	movq	576(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L732
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L732
	call	free@PLT
.L732:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L733
	movq	(%rdi), %rax
	call	*8(%rax)
.L733:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L742
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11308:
	.size	_ZN4node2fs13FSReqCallbackD2Ev, .-_ZN4node2fs13FSReqCallbackD2Ev
	.weak	_ZN4node2fs13FSReqCallbackD1Ev
	.set	_ZN4node2fs13FSReqCallbackD1Ev,_ZN4node2fs13FSReqCallbackD2Ev
	.text
	.p2align 4
	.globl	_ZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L744
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L752
.L744:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L750
	cmpw	$1040, %cx
	jne	.L745
.L750:
	movq	23(%rdx), %r12
.L747:
	movq	8(%rbx), %rdi
	movl	16(%rbx), %eax
	leaq	8(%rdi), %r14
	testl	%eax, %eax
	jle	.L753
.L748:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	$648, %edi
	movl	%eax, %r13d
	call	_Znwm@PLT
	movl	$7, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movsd	.LC7(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r12)
	movq	%rax, 56(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	%rax, 72(%rbx)
	je	.L754
	movq	2112(%r12), %rdx
	movdqa	.LC11(%rip), %xmm0
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r12)
	leaq	584(%rbx), %rax
	movq	%rax, 576(%rbx)
	leaq	16+_ZTVN4node2fs13FSReqCallbackE(%rip), %rax
	movq	%rdx, 64(%rbx)
	leaq	2112(%r12), %rdx
	movq	%rax, (%rbx)
	addq	$144, %rax
	movq	%rdx, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 528(%rbx)
	movl	$1, 536(%rbx)
	movb	$0, 540(%rbx)
	movq	$0, 544(%rbx)
	movb	%r13b, 552(%rbx)
	movb	$0, 584(%rbx)
	movq	%rax, 56(%rbx)
	movups	%xmm0, 560(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L752:
	cmpl	$5, 43(%rax)
	jne	.L744
	leaq	_ZZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L745:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L754:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7850:
	.size	_ZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandle8CloseReqD0Ev
	.type	_ZThn56_N4node2fs10FileHandle8CloseReqD0Ev, @function
_ZThn56_N4node2fs10FileHandle8CloseReqD0Ev:
.LFB11750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs10FileHandle8CloseReqE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-56(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$32, %rdi
	movq	%rax, -88(%rdi)
	addq	$112, %rax
	movq	%rax, -32(%rdi)
	call	uv_fs_req_cleanup@PLT
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L756
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	480(%rbx), %rdi
	movq	$0, 472(%rbx)
	testq	%rdi, %rdi
	je	.L757
.L758:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L757
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L757:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L770
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$544, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movq	480(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L758
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L770:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11750:
	.size	_ZThn56_N4node2fs10FileHandle8CloseReqD0Ev, .-_ZThn56_N4node2fs10FileHandle8CloseReqD0Ev
	.section	.text._ZN4node2fs13FSReqCallbackD0Ev,"axG",@progbits,_ZN4node2fs13FSReqCallbackD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs13FSReqCallbackD0Ev
	.type	_ZN4node2fs13FSReqCallbackD0Ev, @function
_ZN4node2fs13FSReqCallbackD0Ev:
.LFB11310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$144, %rax
	movq	%rax, 56(%rdi)
	movq	576(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L772
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L772
	call	free@PLT
.L772:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L773
	movq	(%rdi), %rax
	call	*8(%rax)
.L773:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L782
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$648, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11310:
	.size	_ZN4node2fs13FSReqCallbackD0Ev, .-_ZN4node2fs13FSReqCallbackD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle8CloseReqD0Ev
	.type	_ZN4node2fs10FileHandle8CloseReqD0Ev, @function
_ZN4node2fs10FileHandle8CloseReqD0Ev:
.LFB7791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs10FileHandle8CloseReqE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$88, %rdi
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	addq	$112, %rax
	movq	%rax, -32(%rdi)
	call	uv_fs_req_cleanup@PLT
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L784
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	536(%r12), %rdi
	movq	$0, 528(%r12)
	testq	%rdi, %rdi
	je	.L785
.L786:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L785
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L785:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L798
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$544, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L784:
	.cfi_restore_state
	movq	536(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L786
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L798:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7791:
	.size	_ZN4node2fs10FileHandle8CloseReqD0Ev, .-_ZN4node2fs10FileHandle8CloseReqD0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev:
.LFB11397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L815
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L801
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L801:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	576(%r12), %rdi
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rax, 56(%r12)
	testq	%rdi, %rdi
	je	.L802
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L802
	call	free@PLT
.L802:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L803
	movq	(%rdi), %rax
	call	*8(%rax)
.L803:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L816
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L816:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11397:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev:
.LFB11386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L833
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L819
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L819:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	576(%r12), %rdi
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rax, 56(%r12)
	testq	%rdi, %rdi
	je	.L820
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L820
	call	free@PLT
.L820:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L821
	movq	(%rdi), %rax
	call	*8(%rax)
.L821:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L834
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L834:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11386:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.text
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandle8CloseReqD1Ev
	.type	_ZThn56_N4node2fs10FileHandle8CloseReqD1Ev, @function
_ZThn56_N4node2fs10FileHandle8CloseReqD1Ev:
.LFB11762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs10FileHandle8CloseReqE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	addq	$112, %rax
	movq	%rax, -32(%rdi)
	call	uv_fs_req_cleanup@PLT
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L836
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	480(%rbx), %rdi
	movq	$0, 472(%rbx)
	testq	%rdi, %rdi
	je	.L837
.L838:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L837
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L837:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	leaq	-56(%rbx), %rdi
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L850
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L836:
	.cfi_restore_state
	movq	480(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L838
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L850:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11762:
	.size	_ZThn56_N4node2fs10FileHandle8CloseReqD1Ev, .-_ZThn56_N4node2fs10FileHandle8CloseReqD1Ev
	.p2align 4
	.type	_ZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1512, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L862
	cmpw	$1040, %cx
	jne	.L852
.L862:
	movq	23(%rdx), %rbx
.L854:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L855
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L856:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L870
.L857:
	leaq	_ZZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L870:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L857
	movq	352(%rbx), %rsi
	leaq	-1088(%rbp), %rdi
	leaq	-1536(%rbp), %r13
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	360(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	-1072(%rbp), %rdx
	movq	2360(%rax), %rdi
	call	uv_fs_stat@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L859
	movq	-1440(%rbp), %rax
	movq	8(%rax), %rbx
	shrq	$14, %rbx
	andl	$1, %ebx
.L859:
	movq	%r13, %rdi
	salq	$32, %rbx
	call	uv_fs_req_cleanup@PLT
	movq	(%r12), %rax
	movq	-1072(%rbp), %rdi
	movq	%rbx, 24(%rax)
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L851
	testq	%rdi, %rdi
	je	.L851
	call	free@PLT
.L851:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L871
	addq	$1512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L855:
	.cfi_restore_state
	movq	8(%r12), %rdx
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L852:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L854
.L871:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7901:
	.size	_ZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev:
.LFB11395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L888
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L874
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L874:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	576(%r12), %rdi
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rax, 56(%r12)
	testq	%rdi, %rdi
	je	.L875
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L875
	call	free@PLT
.L875:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L876
	movq	(%rdi), %rax
	call	*8(%rax)
.L876:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L889
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L889:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11395:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.set	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev:
.LFB11384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L906
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L892
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L892:
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	576(%r12), %rdi
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rax, 56(%r12)
	testq	%rdi, %rdi
	je	.L893
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L893
	call	free@PLT
.L893:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L894
	movq	(%rdi), %rax
	call	*8(%rax)
.L894:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L907
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L907:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11384:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.set	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L909
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L909:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L918
.L910:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L910
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7174:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node2fs13FSReqWrapSyncD2Ev,"axG",@progbits,_ZN4node2fs13FSReqWrapSyncD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs13FSReqWrapSyncD2Ev
	.type	_ZN4node2fs13FSReqWrapSyncD2Ev, @function
_ZN4node2fs13FSReqWrapSyncD2Ev:
.LFB6097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	uv_fs_req_cleanup@PLT
	movq	440(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L919
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L919:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6097:
	.size	_ZN4node2fs13FSReqWrapSyncD2Ev, .-_ZN4node2fs13FSReqWrapSyncD2Ev
	.weak	_ZN4node2fs13FSReqWrapSyncD1Ev
	.set	_ZN4node2fs13FSReqWrapSyncD1Ev,_ZN4node2fs13FSReqWrapSyncD2Ev
	.section	.text._ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE,"axG",@progbits,_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	.type	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE, @function
_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE:
.LFB6511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	352(%rdi), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	pushq	$0
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L927
.L923:
	movq	352(%rbx), %rdi
	movq	%r15, %rsi
	movl	$-1, %ecx
	movl	$1, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L928
.L924:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L929
.L925:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L927:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L928:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L929:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L925
	.cfi_endproc
.LFE6511:
	.size	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE, .-_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	.section	.rodata.str1.1
.LC15:
	.string	"access"
.LC16:
	.string	"open"
.LC17:
	.string	"openFileHandle"
.LC18:
	.string	"read"
.LC19:
	.string	"readBuffers"
.LC20:
	.string	"fdatasync"
.LC21:
	.string	"fsync"
.LC22:
	.string	"rename"
.LC23:
	.string	"ftruncate"
.LC24:
	.string	"rmdir"
.LC25:
	.string	"mkdir"
.LC26:
	.string	"readdir"
.LC27:
	.string	"internalModuleReadJSON"
.LC28:
	.string	"internalModuleStat"
.LC29:
	.string	"stat"
.LC30:
	.string	"lstat"
.LC31:
	.string	"fstat"
.LC32:
	.string	"link"
.LC33:
	.string	"symlink"
.LC34:
	.string	"readlink"
.LC35:
	.string	"unlink"
.LC36:
	.string	"writeBuffer"
.LC37:
	.string	"writeBuffers"
.LC38:
	.string	"writeString"
.LC39:
	.string	"realpath"
.LC40:
	.string	"copyFile"
.LC41:
	.string	"chmod"
.LC42:
	.string	"fchmod"
.LC43:
	.string	"chown"
.LC44:
	.string	"fchown"
.LC45:
	.string	"lchown"
.LC46:
	.string	"utimes"
.LC47:
	.string	"futimes"
.LC48:
	.string	"mkdtemp"
.LC49:
	.string	"kFsStatsFieldsNumber"
.LC50:
	.string	"statValues"
.LC51:
	.string	"bigintStatValues"
.LC52:
	.string	"FSReqCallback"
.LC53:
	.string	"FileHandleReqWrap"
.LC54:
	.string	"FSReqPromise"
.LC55:
	.string	"releaseFD"
.LC56:
	.string	"FileHandle"
.LC57:
	.string	"FileHandleCloseReq"
.LC58:
	.string	"use promises"
.LC59:
	.string	"kUsePromises"
	.section	.text.unlikely
.LCOLDB60:
	.text
.LHOTB60:
	.p2align 4
	.globl	_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L931
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %r15
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L931
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L931
	movq	271(%rax), %rbx
	leaq	_ZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	leaq	.LC15(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	352(%rbx), %r13
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC3(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC16(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC17(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC18(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC19(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC20(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC21(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC22(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC23(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC24(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC25(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC26(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC27(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC28(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC29(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC30(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC31(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC32(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC33(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC34(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC35(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC36(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC37(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC38(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC39(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC40(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC41(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC42(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC43(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC44(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC45(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC46(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC47(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC48(%rip), %rdx
	call	_ZN4node11Environment9SetMethodEN2v85LocalINS1_6ObjectEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	movl	$18, %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$20, %ecx
	movq	%r13, %rdi
	leaq	.LC49(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1004
.L932:
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1005
.L933:
	movq	2296(%rbx), %r14
	testq	%r14, %r14
	je	.L934
	movq	(%r14), %rsi
	movq	2264(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
.L934:
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC50(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1006
.L935:
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1007
.L936:
	movq	2336(%rbx), %r14
	testq	%r14, %r14
	je	.L937
	movq	(%r14), %rsi
	movq	2304(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
.L937:
	xorl	%edx, %edx
	movl	$16, %ecx
	leaq	.LC51(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1008
.L938:
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1009
.L939:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movl	$13, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	.LC52(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1010
.L940:
	movq	%rdx, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1011
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1012
.L942:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	xorl	%edx, %edx
	movl	$17, %ecx
	movq	%r13, %rdi
	leaq	.LC53(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L1013
.L943:
	movq	%r8, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	3112(%rbx), %rdi
	movq	352(%rbx), %r8
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L944
	movq	%r8, -56(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-56(%rbp), %r8
	movq	$0, 3112(%rbx)
.L944:
	testq	%r14, %r14
	je	.L945
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3112(%rbx)
.L945:
	subq	$8, %rsp
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movl	$12, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	.LC54(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1014
.L946:
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3120(%rbx), %rdi
	movq	352(%rbx), %r8
	testq	%rdi, %rdi
	je	.L947
	movq	%r8, -56(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-56(%rbp), %r8
	movq	$0, 3120(%rbx)
.L947:
	testq	%r14, %r14
	je	.L948
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3120(%rbx)
.L948:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node2fs10FileHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1015
.L949:
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node2fs10FileHandle9ReleaseFDERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC55(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1016
.L950:
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r13, %rdi
	leaq	.LC56(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1017
.L951:
	movq	%rdx, %rsi
	movq	%r14, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	-64(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1018
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1019
.L953:
	movq	3096(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L954
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3096(%rbx)
.L954:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L955
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3096(%rbx)
.L955:
	subq	$8, %rsp
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%r13, %rdi
	leaq	.LC57(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L1020
.L956:
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3104(%rbx), %rdi
	movq	352(%rbx), %r8
	testq	%rdi, %rdi
	je	.L957
	movq	%r8, -56(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-56(%rbp), %r8
	movq	$0, 3104(%rbx)
.L957:
	testq	%r14, %r14
	je	.L958
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3104(%rbx)
.L958:
	leaq	.LC58(%rip), %rsi
	movl	$12, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1021
.L959:
	movq	%r13, %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2784(%rbx), %rdi
	movq	352(%rbx), %r8
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L960
	movq	%r8, -56(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-56(%rbp), %r8
	movq	$0, 2784(%rbx)
.L960:
	testq	%r14, %r14
	je	.L961
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2784(%rbx)
.L961:
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	.LC59(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1022
.L962:
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1023
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1004:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1005:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L1007:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L1009:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L942
	.p2align 4,,10
	.p2align 3
.L1012:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%rdx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L953
	.p2align 4,,10
	.p2align 3
.L1019:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1023:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7945:
.L931:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7945:
	.text
	.size	_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE60:
	.text
.LHOTE60:
	.section	.text._ZN4node10BaseObject12pointer_dataEv,"axG",@progbits,_ZN4node10BaseObject12pointer_dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject12pointer_dataEv
	.type	_ZN4node10BaseObject12pointer_dataEv, @function
_ZN4node10BaseObject12pointer_dataEv:
.LFB7185:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L1032
	ret
	.p2align 4,,10
	.p2align 3
.L1032:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1027
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1026:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1027:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1026
	.cfi_endproc
.LFE7185:
	.size	_ZN4node10BaseObject12pointer_dataEv, .-_ZN4node10BaseObject12pointer_dataEv
	.section	.text._ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb,"axG",@progbits,_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb,comdat
	.p2align 4
	.weak	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	.type	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb, @function
_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb:
.LFB7646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1034
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1097
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1063
	cmpw	$1040, %cx
	jne	.L1036
.L1063:
	movq	23(%rdx), %r12
.L1033:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1098
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1034:
	.cfi_restore_state
	movq	2784(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L1048
	movq	3120(%rbx), %rdi
	movq	3280(%rbx), %rsi
	testb	%r13b, %r13b
	jne	.L1099
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1048
	movq	3280(%rbx), %rdi
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1048
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	1424(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1048
	movl	$696, %edi
	call	_Znwm@PLT
	movl	$8, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movsd	.LC7(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L1050
	movq	2112(%rbx), %rdx
	movdqa	.LC11(%rip), %xmm0
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r15
	movq	%r13, %rdi
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	584(%r12), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%rbx), %rdx
	movq	352(%rbx), %rbx
	movq	%rax, 576(%r12)
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rbx, %rsi
	movq	%rdx, 72(%r12)
	movq	%rax, 56(%r12)
	movups	%xmm0, 560(%r12)
	movdqa	.LC12(%rip), %xmm0
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 528(%r12)
	movl	$1, 536(%r12)
	movb	$0, 540(%r12)
	movq	$0, 544(%r12)
	movb	$0, 552(%r12)
	movb	$0, 584(%r12)
	movb	$0, 648(%r12)
	movq	%rbx, 656(%r12)
	movq	$0, 688(%r12)
	movups	%xmm0, 664(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	656(%r12), %rdi
	movl	$144, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movl	$18, %edx
	movq	%r14, %rdi
	movq	672(%r12), %rsi
	movq	%rax, 680(%r12)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1096
.L1051:
	movq	%rbx, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -112(%rbp)
	movq	688(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1054
.L1053:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 688(%r12)
	movq	-112(%rbp), %rax
.L1054:
	testq	%rax, %rax
	je	.L1052
	movq	%rax, 688(%r12)
	leaq	688(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L1052:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1048:
	xorl	%r12d, %r12d
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1099:
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1048
	movq	3280(%rbx), %rdi
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1048
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	1424(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1048
	movl	$696, %edi
	call	_Znwm@PLT
	movl	$8, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movsd	.LC7(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L1050
	movq	2112(%rbx), %rdx
	movdqa	.LC11(%rip), %xmm0
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r15
	movq	%r13, %rdi
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	584(%r12), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%rbx), %rdx
	movq	352(%rbx), %rbx
	movq	%rax, 576(%r12)
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rbx, %rsi
	movq	%rdx, 72(%r12)
	movq	%rax, 56(%r12)
	movups	%xmm0, 560(%r12)
	movdqa	.LC12(%rip), %xmm0
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 528(%r12)
	movl	$1, 536(%r12)
	movb	$0, 540(%r12)
	movq	$0, 544(%r12)
	movb	$1, 552(%r12)
	movb	$0, 584(%r12)
	movb	$0, 648(%r12)
	movq	%rbx, 656(%r12)
	movq	$0, 688(%r12)
	movups	%xmm0, 664(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	656(%r12), %rdi
	movl	$144, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movl	$18, %edx
	movq	%r14, %rdi
	movq	672(%r12), %rsi
	movq	%rax, 680(%r12)
	call	_ZN2v814BigUint64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L1051
.L1096:
	movq	$0, -112(%rbp)
	movq	688(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L1053
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1097:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1050:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1098:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7646:
	.size	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb, .-_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata.str1.1
.LC62:
	.string	"basic_string::substr"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.text
	.p2align 4
	.globl	_ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	.type	_ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_, @function
_ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_:
.LFB7751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	movq	(%rsi), %r15
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L1101
	testq	%r15, %r15
	je	.L1123
.L1101:
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L1191
	cmpq	$1, %r13
	jne	.L1104
	movzbl	(%r15), %eax
	movb	%al, 16(%r12)
	movq	%r14, %rax
.L1105:
	movq	%r13, 8(%r12)
	movq	$-1, %rdx
	movl	$47, %esi
	movq	%r12, %rdi
	movb	$0, (%rax,%r13)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5rfindEcm@PLT
	cmpq	$-1, %rax
	je	.L1106
	movq	8(%r12), %r13
	addq	$1, %rax
	cmpq	%r13, %rax
	ja	.L1192
	movq	(%r12), %r8
	leaq	-80(%rbp), %r15
	subq	%rax, %r13
	leaq	-96(%rbp), %rdi
	movq	%r15, -96(%rbp)
	movq	%r13, -136(%rbp)
	addq	%rax, %r8
	cmpq	$15, %r13
	ja	.L1193
	cmpq	$1, %r13
	jne	.L1110
	movzbl	(%r8), %eax
	movb	%al, -80(%rbp)
	movq	%r15, %rax
.L1111:
	movq	%r13, -88(%rbp)
	movb	$0, (%rax,%r13)
	movq	-96(%rbp), %rdx
	movq	(%r12), %rdi
	cmpq	%r15, %rdx
	je	.L1194
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	cmpq	%rdi, %r14
	je	.L1195
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	16(%r12), %rsi
	movq	%rdx, (%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	testq	%rdi, %rdi
	je	.L1117
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L1115:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1106
	call	_ZdlPv@PLT
.L1106:
	movq	8(%r12), %rax
	movq	8(%rbx), %r13
	cmpq	%r13, %rax
	jnb	.L1196
.L1100:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1197
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1198
	movq	%r14, %rax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	%r12, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L1103:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	(%r12), %rax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	(%r12), %rdx
	movq	%rax, %r8
	leaq	-112(%rbp), %r15
	subq	%r13, %r8
	movq	%r15, -128(%rbp)
	addq	%rdx, %r8
	addq	%rax, %rdx
	je	.L1152
	testq	%r8, %r8
	je	.L1123
.L1152:
	movq	%r13, -136(%rbp)
	movq	%r13, %rax
	movq	%r15, %rdi
	cmpq	$15, %r13
	ja	.L1199
.L1125:
	cmpq	$1, %r13
	je	.L1200
	testq	%r13, %r13
	jne	.L1201
.L1127:
	movq	%rax, -120(%rbp)
	movb	$0, (%rdi,%rax)
	movq	8(%rbx), %r13
	movq	-128(%rbp), %rdi
	cmpq	%r13, -120(%rbp)
	je	.L1202
.L1190:
	cmpq	%r15, %rdi
	je	.L1100
	call	_ZdlPv@PLT
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1110:
	testq	%r13, %r13
	jne	.L1203
	movq	%r15, %rax
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-128(%rbp), %rdi
	movq	-136(%rbp), %rax
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1109:
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-96(%rbp), %rax
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1113
	cmpq	$1, %rdx
	je	.L1204
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	(%r12), %rdi
.L1113:
	movq	%rdx, 8(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1202:
	testq	%r13, %r13
	je	.L1129
	movq	(%rbx), %rsi
	movq	%r13, %rdx
	movq	%rdi, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %rdi
	testl	%eax, %eax
	jne	.L1190
.L1129:
	cmpq	%r15, %rdi
	je	.L1132
	call	_ZdlPv@PLT
	movq	8(%rbx), %r13
.L1132:
	movq	8(%r12), %rax
	movq	(%r12), %rbx
	leaq	-80(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	%rax, %rcx
	subq	%r13, %rcx
	cmpq	%rax, %rcx
	movq	%rcx, %r13
	cmova	%rax, %r13
	movq	%rbx, %rax
	addq	%r13, %rax
	je	.L1153
	testq	%rbx, %rbx
	je	.L1123
.L1153:
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L1205
	cmpq	$1, %r13
	jne	.L1138
	movzbl	(%rbx), %eax
	movb	%al, -80(%rbp)
	movq	%r15, %rax
.L1139:
	movq	%r13, -88(%rbp)
	movb	$0, (%rax,%r13)
	movq	-96(%rbp), %rdx
	movq	(%r12), %rdi
	cmpq	%r15, %rdx
	je	.L1206
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	cmpq	%rdi, %r14
	je	.L1207
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	16(%r12), %rsi
	movq	%rdx, (%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r12)
	testq	%rdi, %rdi
	je	.L1145
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L1143:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1200:
	movzbl	(%r8), %eax
	movb	%al, (%rdi)
	movq	-128(%rbp), %rdi
	movq	-136(%rbp), %rax
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1138:
	testq	%r13, %r13
	jne	.L1208
	movq	%r15, %rax
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1205:
	leaq	-96(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1137:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-96(%rbp), %rax
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1199:
	leaq	-128(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, (%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r12)
.L1117:
	movq	%r15, -96(%rbp)
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1141
	cmpq	$1, %rdx
	je	.L1209
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	(%r12), %rdi
.L1141:
	movq	%rdx, 8(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, (%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r12)
.L1145:
	movq	%r15, -96(%rbp)
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1204:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	(%r12), %rdi
	jmp	.L1113
.L1123:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1209:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	(%r12), %rdi
	jmp	.L1141
.L1197:
	call	__stack_chk_fail@PLT
.L1192:
	movq	%rax, %rdx
	movq	%r13, %rcx
	leaq	.LC62(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC63(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1208:
	movq	%r15, %rdi
	jmp	.L1137
.L1203:
	movq	%r15, %rdi
	jmp	.L1109
.L1198:
	movq	%r14, %rdi
	jmp	.L1103
	.cfi_endproc
.LFE7751:
	.size	_ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_, .-_ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	.align 2
	.p2align 4
	.globl	_ZN4node2fs9FSReqBaseD2Ev
	.type	_ZN4node2fs9FSReqBaseD2Ev, @function
_ZN4node2fs9FSReqBaseD2Ev:
.LFB7759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$144, %rax
	movq	%rax, 56(%rdi)
	movq	576(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1211
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1211
	call	free@PLT
.L1211:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1212
	movq	(%rdi), %rax
	call	*8(%rax)
.L1212:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L1221
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1221:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7759:
	.size	_ZN4node2fs9FSReqBaseD2Ev, .-_ZN4node2fs9FSReqBaseD2Ev
	.globl	_ZN4node2fs9FSReqBaseD1Ev
	.set	_ZN4node2fs9FSReqBaseD1Ev,_ZN4node2fs9FSReqBaseD2Ev
	.p2align 4
	.globl	_ZThn56_N4node2fs9FSReqBaseD1Ev
	.type	_ZThn56_N4node2fs9FSReqBaseD1Ev, @function
_ZThn56_N4node2fs9FSReqBaseD1Ev:
.LFB11761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	movq	%rax, (%rdi)
	movq	520(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1223
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1223
	call	free@PLT
.L1223:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1224
	movq	(%rdi), %rax
	call	*8(%rax)
.L1224:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	leaq	-56(%rbx), %rdi
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L1233
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1233:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11761:
	.size	_ZThn56_N4node2fs9FSReqBaseD1Ev, .-_ZThn56_N4node2fs9FSReqBaseD1Ev
	.align 2
	.p2align 4
	.globl	_ZN4node2fs9FSReqBaseD0Ev
	.type	_ZN4node2fs9FSReqBaseD0Ev, @function
_ZN4node2fs9FSReqBaseD0Ev:
.LFB7761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$144, %rax
	movq	%rax, 56(%rdi)
	movq	576(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1235
	leaq	584(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1235
	call	free@PLT
.L1235:
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1236
	movq	(%rdi), %rax
	call	*8(%rax)
.L1236:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L1245
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$648, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1245:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7761:
	.size	_ZN4node2fs9FSReqBaseD0Ev, .-_ZN4node2fs9FSReqBaseD0Ev
	.p2align 4
	.globl	_ZThn56_N4node2fs9FSReqBaseD0Ev
	.type	_ZThn56_N4node2fs9FSReqBaseD0Ev, @function
_ZThn56_N4node2fs9FSReqBaseD0Ev:
.LFB11751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs9FSReqBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-56(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	movq	%rax, (%rdi)
	movq	520(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1247
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1247
	call	free@PLT
.L1247:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1248
	movq	(%rdi), %rax
	call	*8(%rax)
.L1248:
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, -56(%rbx)
	addq	$112, %rax
	cmpq	$0, -48(%rbx)
	movq	%rax, (%rbx)
	je	.L1257
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$648, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11751:
	.size	_ZThn56_N4node2fs9FSReqBaseD0Ev, .-_ZThn56_N4node2fs9FSReqBaseD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.type	_ZN4node2fs10FileHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi, @function
_ZN4node2fs10FileHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi:
.LFB7768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	.LC7(%rip), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	movl	$4, %ecx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	56(%r12), %r14
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%rbx, 88(%r12)
	leaq	16+_ZTVN4node22EmitToJSStreamListenerE(%rip), %rax
	movups	%xmm0, 72(%r12)
	pcmpeqd	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	leaq	96(%r12), %rax
	movq	%rax, 64(%r12)
	leaq	16+_ZTVN4node2fs10FileHandleE(%rip), %rax
	movq	%rax, (%r12)
	addq	$168, %rax
	movq	%rax, 56(%r12)
	movq	24(%r12), %rax
	movq	$0, 112(%r12)
	movq	%r14, 104(%r12)
	movl	%r13d, 120(%r12)
	movw	%dx, 124(%r12)
	movb	$0, 144(%r12)
	movq	$0, 152(%r12)
	movups	%xmm0, 128(%r12)
	testq	%rax, %rax
	je	.L1261
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1260
.L1261:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L1260:
	movq	%r14, %rdi
	call	_ZN4node10StreamBase9GetObjectEv@PLT
	popq	%rbx
	movq	%r14, %rdx
	popq	%r12
	movq	%rax, %rdi
	popq	%r13
	movl	$1, %esi
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7768:
	.size	_ZN4node2fs10FileHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi, .-_ZN4node2fs10FileHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.globl	_ZN4node2fs10FileHandleC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.set	_ZN4node2fs10FileHandleC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi,_ZN4node2fs10FileHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE
	.type	_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE, @function
_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE:
.LFB7770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%esi, %ebx
	testq	%rdx, %rdx
	je	.L1273
.L1264:
	movl	$160, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movl	$4, %ecx
	movq	%r14, %rsi
	movsd	.LC7(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	leaq	56(%r12), %r13
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r14, 88(%r12)
	leaq	16+_ZTVN4node22EmitToJSStreamListenerE(%rip), %rax
	movups	%xmm0, 72(%r12)
	pcmpeqd	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	leaq	96(%r12), %rax
	movq	%rax, 64(%r12)
	leaq	16+_ZTVN4node2fs10FileHandleE(%rip), %rax
	movq	%rax, (%r12)
	addq	$168, %rax
	movq	%rax, 56(%r12)
	movq	24(%r12), %rax
	movq	$0, 112(%r12)
	movq	%r13, 104(%r12)
	movl	%ebx, 120(%r12)
	movw	%dx, 124(%r12)
	movb	$0, 144(%r12)
	movq	$0, 152(%r12)
	movups	%xmm0, 128(%r12)
	testq	%rax, %rax
	je	.L1268
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1267
.L1268:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L1267:
	movq	%r13, %rdi
	call	_ZN4node10StreamBase9GetObjectEv@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
.L1263:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1273:
	.cfi_restore_state
	movq	3096(%rdi), %rdi
	movq	3280(%r14), %rsi
	xorl	%r12d, %r12d
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L1264
	jmp	.L1263
	.cfi_endproc
.LFE7770:
	.size	_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE, .-_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1297
	cmpw	$1040, %cx
	jne	.L1275
.L1297:
	movq	23(%rdx), %r13
.L1277:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1278
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1305
.L1278:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L1306
	movq	8(%rbx), %rdi
.L1280:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L1307
	movq	8(%rbx), %rdi
	movl	16(%rbx), %eax
	leaq	8(%rdi), %r12
	testl	%eax, %eax
	jg	.L1282
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1282:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1274
	cmpl	$1, 16(%rbx)
	jg	.L1285
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1286:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1287
	cmpl	$1, 16(%rbx)
	jle	.L1308
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L1289:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1309
.L1290:
	movq	%rdx, 128(%r12)
.L1287:
	cmpl	$2, 16(%rbx)
	jg	.L1291
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1292:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1274
	cmpl	$2, 16(%rbx)
	jle	.L1310
	movq	8(%rbx), %rdi
	subq	$16, %rdi
.L1295:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1311
.L1296:
	movq	%rdx, 136(%r12)
.L1274:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1306:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	addq	$88, %rdi
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1305:
	cmpl	$5, 43(%rax)
	jne	.L1278
	leaq	_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1291:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1275:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1307:
	leaq	_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	%rdx, -40(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	%rdx, -40(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L1290
	.cfi_endproc
.LFE7771:
	.size	_ZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node2fs10FileHandle5CloseEv,"axG",@progbits,_ZN4node2fs10FileHandle5CloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs10FileHandle5CloseEv
	.type	_ZN4node2fs10FileHandle5CloseEv, @function
_ZN4node2fs10FileHandle5CloseEv:
.LFB7778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$464, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 125(%rdi)
	je	.L1336
.L1312:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1337
	addq	$464, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1336:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movl	120(%rdi), %edx
	xorl	%ecx, %ecx
	leaq	-480(%rbp), %r13
	movq	360(%rax), %rax
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_close@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	uv_fs_req_cleanup@PLT
	movl	$256, %eax
	cmpb	$0, 144(%rbx)
	movl	120(%rbx), %r13d
	movw	%ax, 124(%rbx)
	movl	$-1, 120(%rbx)
	je	.L1314
	cmpq	$0, 8(%rbx)
	je	.L1314
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	64(%rbx), %rdi
	movq	$-4095, %rsi
	movq	%rax, -496(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -488(%rbp)
	leaq	-496(%rbp), %rdx
	call	*24(%rax)
.L1314:
	movq	16(%rbx), %rbx
	movl	$32, %edi
	leaq	2464(%rbx), %r14
	testl	%r12d, %r12d
	js	.L1338
	call	_Znwm@PLT
	movq	2480(%rbx), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_EE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, (%rax)
	movl	%r12d, 24(%rax)
	movl	%r13d, 28(%rax)
	lock addq	$1, (%r14)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L1322
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L1312
.L1335:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1338:
	call	_Znwm@PLT
	movq	2480(%rbx), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_EE(%rip), %rsi
	movb	$1, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rsi, (%rax)
	movl	%r12d, 24(%rax)
	movl	%r13d, 28(%rax)
	lock addq	$1, (%r14)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L1316
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L1318
.L1334:
	movq	(%rdi), %rax
	call	*8(%rax)
.L1318:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L1339
.L1320:
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L1335
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1339:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L1334
	jmp	.L1318
.L1337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7778:
	.size	_ZN4node2fs10FileHandle5CloseEv, .-_ZN4node2fs10FileHandle5CloseEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandleD2Ev
	.type	_ZN4node2fs10FileHandleD2Ev, @function
_ZN4node2fs10FileHandleD2Ev:
.LFB7773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs10FileHandleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	addq	$168, %rax
	cmpb	$0, 124(%rdi)
	movq	%rax, 56(%rdi)
	jne	.L1369
	movq	%rdi, %r12
	call	_ZN4node2fs10FileHandle5CloseEv
	cmpb	$0, 125(%r12)
	je	.L1370
	movq	152(%r12), %r13
	testq	%r13, %r13
	je	.L1343
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, 0(%r13)
	addq	$112, %rax
	cmpq	$0, 8(%r13)
	movq	%rax, 56(%r13)
	je	.L1371
	movq	64(%r13), %rdx
	movq	72(%r13), %rax
	movq	%r13, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movl	$552, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1343:
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	104(%r12), %rdx
	movq	%rax, 56(%r12)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 96(%r12)
	testq	%rdx, %rdx
	jne	.L1372
.L1348:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	64(%r12), %rbx
	movq	%rax, 56(%r12)
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	64(%r12), %rax
	cmpq	%rbx, %rax
	je	.L1373
	movq	%rax, %rbx
.L1352:
	testq	%rbx, %rbx
	jne	.L1374
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1372:
	.cfi_restore_state
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L1349
	leaq	96(%r12), %rcx
	cmpq	%rax, %rcx
	jne	.L1346
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1366:
	cmpq	%rax, %rcx
	je	.L1376
.L1346:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1366
.L1349:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1376:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1369:
	leaq	_ZZN4node2fs10FileHandleD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1370:
	leaq	_ZZN4node2fs10FileHandleD4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1371:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	112(%r12), %rax
	movq	%rax, 8(%rdx)
	jmp	.L1348
	.cfi_endproc
.LFE7773:
	.size	_ZN4node2fs10FileHandleD2Ev, .-_ZN4node2fs10FileHandleD2Ev
	.set	.LTHUNK8,_ZN4node2fs10FileHandleD2Ev
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandleD1Ev
	.type	_ZThn56_N4node2fs10FileHandleD1Ev, @function
_ZThn56_N4node2fs10FileHandleD1Ev:
.LFB11803:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK8
	.cfi_endproc
.LFE11803:
	.size	_ZThn56_N4node2fs10FileHandleD1Ev, .-_ZThn56_N4node2fs10FileHandleD1Ev
	.globl	_ZN4node2fs10FileHandleD1Ev
	.set	_ZN4node2fs10FileHandleD1Ev,_ZN4node2fs10FileHandleD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandleD0Ev
	.type	_ZN4node2fs10FileHandleD0Ev, @function
_ZN4node2fs10FileHandleD0Ev:
.LFB7775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node2fs10FileHandleD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7775:
	.size	_ZN4node2fs10FileHandleD0Ev, .-_ZN4node2fs10FileHandleD0Ev
	.p2align 4
	.globl	_ZThn56_N4node2fs10FileHandleD0Ev
	.type	_ZThn56_N4node2fs10FileHandleD0Ev, @function
_ZThn56_N4node2fs10FileHandleD0Ev:
.LFB11749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN4node2fs10FileHandleD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11749:
	.size	_ZThn56_N4node2fs10FileHandleD0Ev, .-_ZThn56_N4node2fs10FileHandleD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle8CloseReq7ResolveEv
	.type	_ZN4node2fs10FileHandle8CloseReq7ResolveEv, @function
_ZN4node2fs10FileHandle8CloseReq7ResolveEv:
.LFB7782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	528(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1382
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1382:
	movq	16(%rbx), %rax
	leaq	88(%r12), %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1389
.L1383:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1390
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1389:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1383
.L1390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7782:
	.size	_ZN4node2fs10FileHandle8CloseReq7ResolveEv, .-_ZN4node2fs10FileHandle8CloseReq7ResolveEv
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle8CloseReq6RejectEN2v85LocalINS3_5ValueEEE
	.type	_ZN4node2fs10FileHandle8CloseReq6RejectEN2v85LocalINS3_5ValueEEE, @function
_ZN4node2fs10FileHandle8CloseReq6RejectEN2v85LocalINS3_5ValueEEE:
.LFB7783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	352(%rax), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	528(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1392
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L1392:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1399
.L1393:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1400
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1399:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1393
.L1400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7783:
	.size	_ZN4node2fs10FileHandle8CloseReq6RejectEN2v85LocalINS3_5ValueEEE, .-_ZN4node2fs10FileHandle8CloseReq6RejectEN2v85LocalINS3_5ValueEEE
	.p2align 4
	.type	_ZZN4node2fs10FileHandle12ClosePromiseEvENUlP7uv_fs_sE_4_FUNES3_, @function
_ZZN4node2fs10FileHandle12ClosePromiseEvENUlP7uv_fs_sE_4_FUNES3_:
.LFB7800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$88, %rdi
	je	.L1417
	movq	-72(%rdi), %rax
	leaq	-80(%rbp), %r14
	leaq	-88(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r14, %rdi
	movq	352(%rax), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	536(%r12), %r13
	testq	%r13, %r13
	je	.L1403
	movq	0(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
.L1403:
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1418
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1412
	cmpw	$1040, %cx
	jne	.L1405
.L1412:
	movq	23(%rdx), %r13
.L1407:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$256, %eax
	cmpb	$0, 144(%r13)
	movl	$-1, 120(%r13)
	movw	%ax, 124(%r13)
	je	.L1408
	cmpq	$0, 8(%r13)
	je	.L1408
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	64(%r13), %rdi
	movq	$-4095, %rsi
	movq	%rax, -80(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -72(%rbp)
	movq	%r14, %rdx
	call	*24(%rax)
.L1408:
	cmpq	$0, 88(%rbx)
	js	.L1419
	movq	%r12, %rdi
	call	_ZN4node2fs10FileHandle8CloseReq7ResolveEv
.L1410:
	movq	%r12, %rdi
	call	_ZN4node2fs10FileHandle8CloseReqD1Ev
	movl	$544, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1420
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1419:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	%r14, %rdi
	movq	352(%rax), %r13
	movq	%r13, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	88(%rbx), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC3(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN4node2fs10FileHandle8CloseReq6RejectEN2v85LocalINS3_5ValueEEE
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1417:
	leaq	_ZZZN4node2fs10FileHandle12ClosePromiseEvENKUlP7uv_fs_sE_clES3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1418:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1420:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7800:
	.size	_ZZN4node2fs10FileHandle12ClosePromiseEvENUlP7uv_fs_sE_4_FUNES3_, .-_ZZN4node2fs10FileHandle12ClosePromiseEvENUlP7uv_fs_sE_4_FUNES3_
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle8CloseReq11file_handleEv
	.type	_ZN4node2fs10FileHandle8CloseReq11file_handleEv, @function
_ZN4node2fs10FileHandle8CloseReq11file_handleEv:
.LFB7784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	352(%rax), %r14
	movq	%r14, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	536(%r12), %r12
	testq	%r12, %r12
	je	.L1422
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L1422:
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1433
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1428
	cmpw	$1040, %cx
	jne	.L1424
.L1428:
	movq	23(%rdx), %r12
.L1426:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1434
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1424:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1433:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7784:
	.size	_ZN4node2fs10FileHandle8CloseReq11file_handleEv, .-_ZN4node2fs10FileHandle8CloseReq11file_handleEv
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle8CloseReqC2EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE
	.type	_ZN4node2fs10FileHandle8CloseReqC2EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE, @function
_ZN4node2fs10FileHandle8CloseReqC2EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE:
.LFB7786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movl	$5, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	.LC7(%rip), %xmm0
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r12)
	movq	%rax, 56(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	%rax, 72(%rbx)
	je	.L1449
	movq	2112(%r12), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r12)
	leaq	16+_ZTVN4node2fs10FileHandle8CloseReqE(%rip), %rax
	movq	%rdx, 64(%rbx)
	leaq	2112(%r12), %rdx
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	$0, 88(%rbx)
	movups	%xmm0, 528(%rbx)
	movq	352(%r12), %r15
	movq	%rdx, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	%rax, 56(%rbx)
	testq	%r14, %r14
	je	.L1437
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	536(%rbx), %rdi
	movq	352(%r12), %r15
	movq	%rax, 528(%rbx)
	testq	%rdi, %rdi
	je	.L1437
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 536(%rbx)
.L1437:
	testq	%r13, %r13
	je	.L1435
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 536(%rbx)
.L1435:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1449:
	.cfi_restore_state
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7786:
	.size	_ZN4node2fs10FileHandle8CloseReqC2EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE, .-_ZN4node2fs10FileHandle8CloseReqC2EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE
	.globl	_ZN4node2fs10FileHandle8CloseReqC1EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE
	.set	_ZN4node2fs10FileHandle8CloseReqC1EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE,_ZN4node2fs10FileHandle8CloseReqC2EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle12ClosePromiseEv
	.type	_ZN4node2fs10FileHandle12ClosePromiseEv, @function
_ZN4node2fs10FileHandle12ClosePromiseEv:
.LFB7793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	352(%rax), %r13
	movq	%r13, %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L1471
	cmpb	$0, 144(%rbx)
	jne	.L1472
	cmpb	$0, 125(%rbx)
	movq	%rax, %r15
	jne	.L1453
	cmpb	$0, 124(%rbx)
	je	.L1473
.L1453:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$-9, %esi
	leaq	.LC3(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1474
.L1459:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
.L1461:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1475
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1473:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movb	$1, 124(%rbx)
	movq	3104(%rax), %rdi
	movq	3280(%rax), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1476
	movq	8(%rbx), %r8
	movq	16(%rbx), %r9
	testq	%r8, %r8
	je	.L1456
	movzbl	11(%r8), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1477
.L1456:
	movl	$544, %edi
	movq	%rdx, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r9
	movq	-120(%rbp), %rdx
	movq	%r15, %rcx
	movq	-112(%rbp), %r8
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	_ZN4node2fs10FileHandle8CloseReqC1EPNS_11EnvironmentEN2v85LocalINS5_6ObjectEEENS6_INS5_7PromiseEEENS6_INS5_5ValueEEE
	cmpq	$0, 80(%r14)
	movl	120(%rbx), %edx
	movq	%r14, 88(%r14)
	jne	.L1478
	leaq	_ZZN4node2fs10FileHandle12ClosePromiseEvENUlP7uv_fs_sE_4_FUNES3_(%rip), %rax
	leaq	88(%r14), %rsi
	movq	%rax, 80(%r14)
	movq	16(%r14), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	js	.L1458
	movq	16(%r14), %rax
	addl	$1, 2156(%rax)
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1471:
	leaq	_ZZN4node2fs10FileHandle12ClosePromiseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1472:
	leaq	_ZZN4node2fs10FileHandle12ClosePromiseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1476:
	xorl	%r13d, %r13d
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1474:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	352(%r9), %rdi
	movq	(%r8), %rsi
	movq	%rdx, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %r9
	movq	%rax, %r8
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1458:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rdx
	movl	%eax, %esi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN4node2fs10FileHandle8CloseReq6RejectEN2v85LocalINS3_5ValueEEE
	movq	%r14, %rdi
	call	_ZN4node2fs10FileHandle8CloseReqD1Ev
	movl	$544, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1459
.L1478:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7793:
	.size	_ZN4node2fs10FileHandle12ClosePromiseEv, .-_ZN4node2fs10FileHandle12ClosePromiseEv
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node2fs10FileHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node2fs10FileHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1491
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1486
	cmpw	$1040, %cx
	jne	.L1481
.L1486:
	movq	23(%rdx), %rdi
.L1483:
	testq	%rdi, %rdi
	je	.L1479
	call	_ZN4node2fs10FileHandle12ClosePromiseEv
	testq	%rax, %rax
	je	.L1479
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L1479:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1481:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1491:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7802:
	.size	_ZN4node2fs10FileHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node2fs10FileHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node2fs10FileHandle10AfterCloseEv
	.type	_ZN4node2fs10FileHandle10AfterCloseEv, @function
_ZN4node2fs10FileHandle10AfterCloseEv:
.LFB7804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$256, %eax
	cmpb	$0, 144(%rdi)
	movl	$-1, 120(%rdi)
	movw	%ax, 124(%rdi)
	je	.L1492
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rbx
	je	.L1492
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	64(%rbx), %rdi
	movq	$-4095, %rsi
	movq	%rax, -48(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	call	*24(%rax)
.L1492:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1496
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1496:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7804:
	.size	_ZN4node2fs10FileHandle10AfterCloseEv, .-_ZN4node2fs10FileHandle10AfterCloseEv
	.align 2
	.p2align 4
	.globl	_ZN4node2fs18FileHandleReadWrapC2EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE
	.type	_ZN4node2fs18FileHandleReadWrapC2EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE, @function
_ZN4node2fs18FileHandleReadWrapC2EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE:
.LFB7807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rsi), %r12
	movsd	.LC7(%rip), %xmm0
	movq	%r12, %rsi
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r12)
	movq	%rax, 56(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	%rax, 72(%rbx)
	je	.L1500
	movq	2112(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r12)
	leaq	16+_ZTVN4node2fs18FileHandleReadWrapE(%rip), %rax
	movq	%rdx, 64(%rbx)
	leaq	2112(%r12), %rdx
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%r13, 528(%rbx)
	movq	%rdx, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	.cfi_restore_state
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7807:
	.size	_ZN4node2fs18FileHandleReadWrapC2EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE, .-_ZN4node2fs18FileHandleReadWrapC2EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE
	.globl	_ZN4node2fs18FileHandleReadWrapC1EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE
	.set	_ZN4node2fs18FileHandleReadWrapC1EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE,_ZN4node2fs18FileHandleReadWrapC2EPNS0_10FileHandleEN2v85LocalINS4_6ObjectEEE
	.p2align 4
	.globl	_ZN4node2fs18FromNamespacedPathEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node2fs18FromNamespacedPathEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node2fs18FromNamespacedPathEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB7864:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7864:
	.size	_ZN4node2fs18FromNamespacedPathEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node2fs18FromNamespacedPathEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_Z12_register_fsv
	.type	_Z12_register_fsv, @function
_Z12_register_fsv:
.LFB7946:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7946:
	.size	_Z12_register_fsv, .-_Z12_register_fsv
	.section	.text._ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm,"axG",@progbits,_ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm
	.type	_ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm, @function
_ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm:
.LFB8724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L1516
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpq	8(%rdi), %rsi
	jbe	.L1506
	leaq	24(%rdi), %r15
	movl	$1, %r14d
	cmpq	%r15, %r13
	je	.L1517
.L1507:
	testq	%r12, %r12
	je	.L1518
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1519
.L1509:
	movq	%rax, 16(%rbx)
	movq	%r12, 8(%rbx)
	testb	%r14b, %r14b
	jne	.L1506
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L1520
.L1506:
	movq	%r12, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1520:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1516:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1517:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	%r13, %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1519:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L1509
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8724:
	.size	_ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm, .-_ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm
	.section	.text._ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,"axG",@progbits,_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,comdat
	.p2align 4
	.weak	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.type	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, @function
_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm:
.LFB8727:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rsi), %r8
	leaq	0(,%rdx,8), %rcx
	movq	%r8, (%rax,%rdx,8)
	movq	8(%rsi), %rdx
	movq	%rdx, 8(%rax,%rcx)
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rax,%rcx)
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rax,%rcx)
	movq	32(%rsi), %rdx
	movq	%rdx, 32(%rax,%rcx)
	movq	40(%rsi), %rdx
	movq	%rdx, 40(%rax,%rcx)
	movq	64(%rsi), %rdx
	movq	%rdx, 48(%rax,%rcx)
	movq	48(%rsi), %rdx
	movq	%rdx, 56(%rax,%rcx)
	movq	56(%rsi), %rdx
	movq	%rdx, 64(%rax,%rcx)
	movq	72(%rsi), %rdx
	movq	%rdx, 72(%rax,%rcx)
	movq	96(%rsi), %rdx
	movq	%rdx, 80(%rax,%rcx)
	movq	104(%rsi), %rdx
	movq	%rdx, 88(%rax,%rcx)
	movq	112(%rsi), %rdx
	movq	%rdx, 96(%rax,%rcx)
	movq	120(%rsi), %rdx
	movq	%rdx, 104(%rax,%rcx)
	movq	128(%rsi), %rdx
	movq	%rdx, 112(%rax,%rcx)
	movq	136(%rsi), %rdx
	movq	%rdx, 120(%rax,%rcx)
	movq	144(%rsi), %rdx
	movq	%rdx, 128(%rax,%rcx)
	movq	152(%rsi), %rdx
	movq	%rdx, 136(%rax,%rcx)
	ret
	.cfi_endproc
.LFE8727:
	.size	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, .-_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.section	.text._ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,"axG",@progbits,_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,comdat
	.p2align 4
	.weak	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.type	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, @function
_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm:
.LFB8732:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rsi), %rdi
	leaq	0(,%rdx,8), %rcx
	testq	%rdi, %rdi
	js	.L1523
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdi, %xmm0
	movsd	%xmm0, (%rax,%rdx,8)
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	js	.L1525
.L1559:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	16(%rsi), %rdx
	movsd	%xmm0, 8(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1527
.L1560:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	24(%rsi), %rdx
	movsd	%xmm0, 16(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1529
.L1561:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	32(%rsi), %rdx
	movsd	%xmm0, 24(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1531
.L1562:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	40(%rsi), %rdx
	movsd	%xmm0, 32(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1533
.L1563:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	64(%rsi), %rdx
	movsd	%xmm0, 40(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1535
.L1564:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	48(%rsi), %rdx
	movsd	%xmm0, 48(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1537
.L1565:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	56(%rsi), %rdx
	movsd	%xmm0, 56(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1539
.L1566:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	72(%rsi), %rdx
	movsd	%xmm0, 64(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1541
.L1567:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	96(%rsi), %rdx
	movsd	%xmm0, 72(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1543
.L1568:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	104(%rsi), %rdx
	movsd	%xmm0, 80(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1545
.L1569:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	112(%rsi), %rdx
	movsd	%xmm0, 88(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1547
.L1570:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	120(%rsi), %rdx
	movsd	%xmm0, 96(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1549
.L1571:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L1550:
	movq	128(%rsi), %rdx
	movsd	%xmm0, 104(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1551
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L1552:
	movq	136(%rsi), %rdx
	movsd	%xmm0, 112(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1553
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L1554:
	movq	144(%rsi), %rdx
	movsd	%xmm0, 120(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L1555
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L1556:
	movsd	%xmm0, 128(%rax,%rcx)
	movq	152(%rsi), %rdx
	testq	%rdx, %rdx
	js	.L1557
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movsd	%xmm0, 136(%rax,%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1523:
	movq	%rdi, %r8
	andl	$1, %edi
	pxor	%xmm0, %xmm0
	shrq	%r8
	orq	%rdi, %r8
	cvtsi2sdq	%r8, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, (%rax,%rdx,8)
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	jns	.L1559
.L1525:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	16(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1560
.L1527:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	24(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1561
.L1529:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	32(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1562
.L1531:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	40(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1563
.L1533:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	64(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 40(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1564
.L1535:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	48(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 48(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1565
.L1537:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	56(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 56(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1566
.L1539:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	72(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 64(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1567
.L1541:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	96(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1568
.L1543:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	104(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 80(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1569
.L1545:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	112(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 88(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1570
.L1547:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	120(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 96(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L1571
.L1549:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	%rdx, %rsi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	orq	%rdx, %rsi
	cvtsi2sdq	%rsi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 136(%rax,%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1552
	.cfi_endproc
.LFE8732:
	.size	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, .-_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs13FSReqCallback11ResolveStatEPK9uv_stat_t
	.type	_ZN4node2fs13FSReqCallback11ResolveStatEPK9uv_stat_t, @function
_ZN4node2fs13FSReqCallback11ResolveStatEPK9uv_stat_t:
.LFB7846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 552(%rdi)
	movq	16(%rdi), %rbx
	je	.L1573
	leaq	2304(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2336(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1575
	movq	(%rsi), %rsi
	movq	2304(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L1575:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE
	.p2align 4,,10
	.p2align 3
.L1573:
	.cfi_restore_state
	leaq	2264(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2296(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1575
	movq	2264(%rbx), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE
	.cfi_endproc
.LFE7846:
	.size	_ZN4node2fs13FSReqCallback11ResolveStatEPK9uv_stat_t, .-_ZN4node2fs13FSReqCallback11ResolveStatEPK9uv_stat_t
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L1601
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L1602
.L1587:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L1591
.L1584:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1602:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1592
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1588:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L1587
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1593
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1589:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L1591:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1584
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L1601:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1592:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1593:
	xorl	%edx, %edx
	jmp	.L1589
	.cfi_endproc
.LFE8768:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1604
	leaq	40(%r12), %rsi
.L1605:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1606
	addq	$40, %r12
.L1607:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1608
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1624
.L1608:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1625
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1612
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L1626
.L1603:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1627
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1624:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1625:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1615
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1610:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1612:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1615:
	xorl	%edx, %edx
	jmp	.L1610
.L1627:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7746:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%r12, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	leaq	-32(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1629
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L1645
.L1629:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1646
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1633
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L1647
.L1628:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1648
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1645:
	.cfi_restore_state
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1646:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1636
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1631:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1633:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1636:
	xorl	%edx, %edx
	jmp	.L1631
.L1648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7744:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC5EPS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_
	.type	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_, @function
_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_:
.LFB8882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L1666
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L1667
.L1652:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L1656
.L1649:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1667:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1657
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1653:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L1652
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1658
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1654:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L1656:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1649
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L1666:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1657:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1658:
	xorl	%edx, %edx
	jmp	.L1654
	.cfi_endproc
.LFE8882:
	.size	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_, .-_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_
	.weak	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	.set	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_,_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node2fs15FSReqAfterScopeC2EPNS0_9FSReqBaseEP7uv_fs_s
	.type	_ZN4node2fs15FSReqAfterScopeC2EPNS0_9FSReqBaseEP7uv_fs_s, @function
_ZN4node2fs15FSReqAfterScopeC2EPNS0_9FSReqBaseEP7uv_fs_s:
.LFB7852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	movq	%r13, 8(%rbx)
	leaq	16(%rbx), %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, 40(%rbx)
	call	_ZN2v87Context5EnterEv@PLT
	movq	(%rbx), %rax
	addq	$88, %rax
	cmpq	%rax, %r13
	jne	.L1671
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1671:
	.cfi_restore_state
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7852:
	.size	_ZN4node2fs15FSReqAfterScopeC2EPNS0_9FSReqBaseEP7uv_fs_s, .-_ZN4node2fs15FSReqAfterScopeC2EPNS0_9FSReqBaseEP7uv_fs_s
	.globl	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	.set	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s,_ZN4node2fs15FSReqAfterScopeC2EPNS0_9FSReqBaseEP7uv_fs_s
	.align 2
	.p2align 4
	.globl	_ZN4node2fs15FSReqAfterScope5ClearEv
	.type	_ZN4node2fs15FSReqAfterScope5ClearEv, @function
_ZN4node2fs15FSReqAfterScope5ClearEv:
.LFB7857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1672
	addq	$88, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	(%rbx), %r12
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L1691
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L1682
	movb	$1, 9(%rax)
	leaq	-32(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1677
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1692
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1693
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1677
	cmpb	$0, 9(%rdx)
	jne	.L1694
	cmpb	$0, 8(%rdx)
	je	.L1677
	cmpq	$0, 8(%r12)
	je	.L1677
	movq	%r12, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r12, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1677
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1677:
	movq	-32(%rbp), %rax
	movq	%rax, (%rbx)
.L1672:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1695
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1691:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r12), %rdx
	xorl	%esi, %esi
	movq	$0, (%rax)
	movw	%si, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1685
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1675:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1682:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1692:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1685:
	xorl	%edx, %edx
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1693:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1695:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7857:
	.size	_ZN4node2fs15FSReqAfterScope5ClearEv, .-_ZN4node2fs15FSReqAfterScope5ClearEv
	.align 2
	.p2align 4
	.globl	_ZN4node2fs15FSReqAfterScopeD2Ev
	.type	_ZN4node2fs15FSReqAfterScopeD2Ev, @function
_ZN4node2fs15FSReqAfterScopeD2Ev:
.LFB7855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1697
	addq	$88, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	(%rbx), %r12
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L1727
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L1712
	movb	$1, 9(%rax)
	leaq	-32(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1701
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1708
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1709
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1701
	cmpb	$0, 9(%rdx)
	jne	.L1728
	cmpb	$0, 8(%rdx)
	je	.L1701
	cmpq	$0, 8(%r12)
	je	.L1701
	movq	%r12, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r12, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %esi
	testl	%esi, %esi
	jne	.L1701
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	-32(%rbp), %rax
	movq	%rax, (%rbx)
.L1697:
	movq	40(%rbx), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	leaq	16(%rbx), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1696
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1708
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1709
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1696
	cmpb	$0, 9(%rdx)
	je	.L1711
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1729
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1727:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r12), %rdx
	xorl	%edi, %edi
	movq	$0, (%rax)
	movw	%di, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1715
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1699:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1712:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1711:
	cmpb	$0, 8(%rdx)
	je	.L1696
	cmpq	$0, 8(%r12)
	je	.L1696
	movq	%r12, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r12, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1696
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1708:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1709:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1715:
	xorl	%edx, %edx
	jmp	.L1699
.L1729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7855:
	.size	_ZN4node2fs15FSReqAfterScopeD2Ev, .-_ZN4node2fs15FSReqAfterScopeD2Ev
	.globl	_ZN4node2fs15FSReqAfterScopeD1Ev
	.set	_ZN4node2fs15FSReqAfterScopeD1Ev,_ZN4node2fs15FSReqAfterScopeD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	.type	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s, @function
_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s:
.LFB7858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$40, %rsp
	movq	(%rbx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	(%rbx), %rax
	xorl	%r9d, %r9d
	cmpb	$0, 540(%rax)
	je	.L1731
	movq	576(%rax), %r9
.L1731:
	movq	544(%rax), %rdx
	movq	16(%rax), %rax
	xorl	%ecx, %ecx
	movq	88(%r12), %rsi
	movq	104(%r12), %r8
	movq	352(%rax), %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1732
	addq	$88, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	(%rbx), %r13
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1764
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L1747
	movb	$1, 9(%rax)
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L1736
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L1743
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1744
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1736
	cmpb	$0, 9(%rdx)
	jne	.L1765
	cmpb	$0, 8(%rdx)
	je	.L1736
	cmpq	$0, 8(%r13)
	je	.L1736
	movq	%r13, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r13, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %esi
	testl	%esi, %esi
	jne	.L1736
	movq	8(%r13), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	-48(%rbp), %rax
	movq	%rax, (%rbx)
.L1732:
	movq	-56(%rbp), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L1730
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1743
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1744
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1730
	cmpb	$0, 9(%rdx)
	je	.L1746
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1766
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1764:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%edi, %edi
	movq	$0, (%rax)
	movw	%di, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1751
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1734:
	movb	%dl, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
.L1747:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1746:
	cmpb	$0, 8(%rdx)
	je	.L1730
	cmpq	$0, 8(%r12)
	je	.L1730
	movq	%r12, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r12, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1730
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1743:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1744:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1751:
	xorl	%edx, %edx
	jmp	.L1734
.L1766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7858:
	.size	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s, .-_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	.align 2
	.p2align 4
	.globl	_ZN4node2fs15FSReqAfterScope7ProceedEv
	.type	_ZN4node2fs15FSReqAfterScope7ProceedEv, @function
_ZN4node2fs15FSReqAfterScope7ProceedEv:
.LFB7859:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	cmpq	$0, 88(%rsi)
	js	.L1775
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1775:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7859:
	.size	_ZN4node2fs15FSReqAfterScope7ProceedEv, .-_ZN4node2fs15FSReqAfterScope7ProceedEv
	.section	.rodata.str1.1
.LC64:
	.string	"fs.sync.fchmod"
	.text
	.p2align 4
	.type	_ZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1822
	cmpw	$1040, %cx
	jne	.L1777
.L1822:
	movq	23(%rdx), %rbx
.L1779:
	movl	16(%r12), %r15d
	cmpl	$1, %r15d
	jg	.L1835
	leaq	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1835:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L1836
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L1837
	movq	8(%r12), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r13d
	jg	.L1783
.L1841:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1784:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L1838
	cmpl	$1, 16(%r12)
	jg	.L1786
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r14d
	jg	.L1788
.L1842:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L1790
.L1843:
	leaq	.LC42(%rip), %rax
	cmpq	$0, 80(%r9)
	movq	%r9, 88(%r9)
	movq	%rax, 544(%r9)
	movl	$1, 536(%r9)
	jne	.L1839
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r9), %r15
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%rax, 80(%r9)
	movq	16(%r9), %rax
	movq	%r15, %rsi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r9, -568(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fchmod@PLT
	movq	-568(%rbp), %r9
	testl	%eax, %eax
	js	.L1792
	movq	16(%r9), %rax
	movq	%r12, %rsi
	movq	%r9, %rdi
	addl	$1, 2156(%rax)
	movq	(%r9), %rax
	call	*120(%rax)
.L1776:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1840
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1837:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r13d
	jle	.L1841
.L1783:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1786:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r14d
	jle	.L1842
.L1788:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L1843
.L1790:
	cmpl	$4, %r15d
	jne	.L1844
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1797
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L1845
.L1798:
	cmpb	$0, (%rax)
	jne	.L1846
.L1797:
	cmpl	$3, 16(%r12)
	jle	.L1847
	movq	8(%r12), %r15
	subq	$24, %r15
.L1803:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_fchmod@PLT
	testl	%eax, %eax
	js	.L1848
.L1804:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1809
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L1849
.L1810:
	cmpb	$0, (%rax)
	jne	.L1850
.L1809:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1776
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	(%r12), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1792:
	cltq
	leaq	-560(%rbp), %r12
	movq	%r9, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r9)
	movq	%r12, %rdi
	movq	$0, 192(%r9)
	movq	%r9, -568(%rbp)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	movq	-568(%rbp), %r9
	cmpq	$0, 88(%rsi)
	js	.L1851
	movq	16(%r9), %rdx
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L1794:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2087(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1852
.L1801:
	testb	$5, (%rsi)
	je	.L1797
	leaq	.LC64(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1777:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2090(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1853
.L1813:
	testb	$5, (%rsi)
	je	.L1809
	leaq	.LC64(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1836:
	leaq	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1838:
	leaq	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	352(%rbx), %r14
	movl	%eax, %esi
	movq	3280(%rbx), %r13
	movq	%r14, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1854
.L1805:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1855
.L1806:
	movq	360(%rbx), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1804
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1845:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1849:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1839:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1844:
	leaq	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1853:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2090(%rip)
	mfence
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1852:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2087(%rip)
	mfence
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L1794
.L1855:
	movq	%rax, -568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-568(%rbp), %rcx
	jmp	.L1806
.L1854:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1805
.L1840:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7938:
	.size	_ZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC65:
	.string	"fs.sync.ftruncate"
	.text
	.p2align 4
	.type	_ZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1902
	cmpw	$1040, %cx
	jne	.L1857
.L1902:
	movq	23(%rdx), %rbx
.L1859:
	movl	16(%r12), %r15d
	cmpl	$2, %r15d
	jg	.L1915
	leaq	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1915:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L1916
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L1917
	movq	8(%r12), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r13d
	jg	.L1863
.L1921:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1864:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1918
	cmpl	$1, 16(%r12)
	jg	.L1866
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	cmpl	$2, 16(%r12)
	movq	%rax, %r14
	jg	.L1868
.L1922:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L1870
.L1923:
	leaq	.LC23(%rip), %rax
	cmpq	$0, 80(%r9)
	movq	%r9, 88(%r9)
	movq	%rax, 544(%r9)
	movl	$1, 536(%r9)
	jne	.L1919
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r9), %r15
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rax, 80(%r9)
	movq	16(%r9), %rax
	movq	%r15, %rsi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r9, -568(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_ftruncate@PLT
	movq	-568(%rbp), %r9
	testl	%eax, %eax
	js	.L1872
	movq	16(%r9), %rax
	movq	%r12, %rsi
	movq	%r9, %rdi
	addl	$1, 2156(%rax)
	movq	(%r9), %rax
	call	*120(%rax)
.L1856:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1920
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r13d
	jle	.L1921
.L1863:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	cmpl	$2, 16(%r12)
	movq	%rax, %r14
	jle	.L1922
.L1868:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L1923
.L1870:
	cmpl	$4, %r15d
	jne	.L1924
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1877
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L1925
.L1878:
	cmpb	$0, (%rax)
	jne	.L1926
.L1877:
	cmpl	$3, 16(%r12)
	jle	.L1927
	movq	8(%r12), %r15
	subq	$24, %r15
.L1883:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_ftruncate@PLT
	testl	%eax, %eax
	js	.L1928
.L1884:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1889
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L1929
.L1890:
	cmpb	$0, (%rax)
	jne	.L1930
.L1889:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1856
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1927:
	movq	(%r12), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1872:
	cltq
	leaq	-560(%rbp), %r12
	movq	%r9, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r9)
	movq	%r12, %rdi
	movq	$0, 192(%r9)
	movq	%r9, -568(%rbp)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	movq	-568(%rbp), %r9
	cmpq	$0, 88(%rsi)
	js	.L1931
	movq	16(%r9), %rdx
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L1874:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1181(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1932
.L1881:
	testb	$5, (%rsi)
	je	.L1877
	leaq	.LC65(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1857:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1184(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1933
.L1893:
	testb	$5, (%rsi)
	je	.L1889
	leaq	.LC65(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L1916:
	leaq	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1918:
	leaq	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	352(%rbx), %r14
	movl	%eax, %esi
	movq	3280(%rbx), %r13
	movq	%r14, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1934
.L1885:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1935
.L1886:
	movq	360(%rbx), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1884
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1925:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L1929:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1919:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1924:
	leaq	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1933:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1184(%rip)
	mfence
	jmp	.L1893
	.p2align 4,,10
	.p2align 3
.L1932:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1181(%rip)
	mfence
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L1931:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L1874
.L1935:
	movq	%rax, -568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-568(%rbp), %rcx
	jmp	.L1886
.L1934:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1885
.L1920:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7909:
	.size	_ZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC66:
	.string	"fs.sync.link"
	.text
	.p2align 4
	.type	_ZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2648, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1996
	cmpw	$1040, %cx
	jne	.L1937
.L1996:
	movq	23(%rdx), %rbx
.L1939:
	movl	16(%r12), %r14d
	movq	352(%rbx), %r13
	cmpl	$2, %r14d
	jg	.L2025
	leaq	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	8(%r12), %rdx
	leaq	-2160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -2144(%rbp)
	je	.L2026
	cmpl	$1, 16(%r12)
	jle	.L2027
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdx
.L1942:
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2028
	cmpl	$2, 16(%r12)
	jg	.L1944
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1946
.L2033:
	movq	-1088(%rbp), %r15
	leaq	.LC32(%rip), %rax
	movl	$1, 536(%r13)
	movq	%rax, 544(%r13)
	movq	-2144(%rbp), %rbx
	movq	-1104(%rbp), %r14
	testq	%r15, %r15
	je	.L1947
	movzbl	540(%r13), %r8d
	testb	%r8b, %r8b
	jne	.L2029
	movq	576(%r13), %rdi
	leaq	1(%r14), %rcx
	testq	%rdi, %rdi
	je	.L2030
	movq	568(%r13), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2023
	leaq	584(%r13), %rax
	movq	%rax, -2688(%rbp)
	cmpq	%rax, %rdi
	je	.L1951
	movl	$1, %r8d
	testq	%rcx, %rcx
	je	.L2031
.L1952:
	movq	%rcx, %rsi
	movb	%r8b, -2680(%rbp)
	movq	%rcx, -2672(%rbp)
	movq	%rdi, -2664(%rbp)
	call	realloc@PLT
	movq	-2664(%rbp), %rdi
	movq	-2672(%rbp), %rcx
	testq	%rax, %rax
	movzbl	-2680(%rbp), %r8d
	je	.L2032
.L1955:
	movq	%rax, 576(%r13)
	movq	%rcx, %rdx
	movq	%rcx, 568(%r13)
	testb	%r8b, %r8b
	jne	.L2023
.L1954:
	movq	560(%r13), %r8
	testq	%r8, %r8
	je	.L2023
	movq	-2688(%rbp), %rsi
	movq	%r8, %rdx
	movq	%rax, %rdi
	movq	%rcx, -2664(%rbp)
	call	memcpy@PLT
	movq	-2664(%rbp), %rcx
	movq	568(%r13), %rdx
	movq	%rcx, 560(%r13)
	cmpq	%rdx, %rcx
	jbe	.L1956
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2027:
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2033
.L1946:
	cmpl	$4, %r14d
	jne	.L2034
	movq	$0, -2168(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1964
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2035
.L1965:
	cmpb	$0, (%rax)
	jne	.L2036
.L1964:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r15
	movq	-2144(%rbp), %r14
	jle	.L2037
	movq	8(%r12), %r13
	subq	$24, %r13
.L1970:
	movq	%rbx, %rdi
	leaq	-2608(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_link@PLT
	testl	%eax, %eax
	js	.L2038
.L1971:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1976
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2039
.L1977:
	cmpb	$0, (%rax)
	jne	.L2040
.L1976:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-2168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1960
	movq	(%rdi), %rax
	call	*8(%rax)
.L1960:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1982
	testq	%rdi, %rdi
	je	.L1982
	call	free@PLT
.L1982:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1936
	testq	%rdi, %rdi
	je	.L1936
	call	free@PLT
.L1936:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2041
	addq	$2648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2023:
	.cfi_restore_state
	movq	%rcx, 560(%r13)
.L1956:
	cmpq	%rdx, %r14
	ja	.L1953
	movq	576(%r13), %rax
	movq	%r14, 560(%r13)
	movq	%r14, %rdx
	movq	%r15, %rsi
	movb	$0, (%rax,%r14)
	movq	576(%r13), %rdi
	call	memcpy@PLT
	movb	$1, 540(%r13)
.L1947:
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	jne	.L2042
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_link@PLT
	testl	%eax, %eax
	js	.L1959
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L2037:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1085(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2043
.L1968:
	testb	$5, (%rsi)
	je	.L1964
	leaq	.LC66(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1937:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1959:
	cltq
	leaq	-2656(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-2648(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L2044
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L1961:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1088(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2045
.L1980:
	testb	$5, (%rsi)
	je	.L1976
	leaq	.LC66(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2026:
	leaq	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2028:
	leaq	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2038:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2046
.L1972:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2047
.L1973:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1971
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2039:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2035:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L2030:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1951:
	xorl	%edi, %edi
	testq	%rcx, %rcx
	jne	.L1952
	movq	$0, 576(%r13)
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	$0, 568(%r13)
	jmp	.L1954
.L2044:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L2034:
	leaq	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2045:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1088(%rip)
	mfence
	jmp	.L1980
	.p2align 4,,10
	.p2align 3
.L2043:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1085(%rip)
	mfence
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2042:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2029:
	leaq	_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2031:
	call	free@PLT
	movq	$0, 576(%r13)
	movq	$0, 568(%r13)
	movq	$0, 560(%r13)
.L1953:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2032:
	movb	%r8b, -2672(%rbp)
	movq	%rdi, -2680(%rbp)
	movq	%rcx, -2664(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2664(%rbp), %rcx
	movq	-2680(%rbp), %rdi
	movq	%rcx, %rsi
	call	realloc@PLT
	movq	-2664(%rbp), %rcx
	movzbl	-2672(%rbp), %r8d
	testq	%rax, %rax
	jne	.L1955
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2047:
	movq	%rax, -2664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2664(%rbp), %rcx
	jmp	.L1973
.L2046:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1972
.L2041:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7906:
	.size	_ZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC67:
	.string	"fs.sync.rename"
	.text
	.p2align 4
	.type	_ZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2648, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2108
	cmpw	$1040, %cx
	jne	.L2049
.L2108:
	movq	23(%rdx), %rbx
.L2051:
	movl	16(%r12), %r14d
	movq	352(%rbx), %r13
	cmpl	$2, %r14d
	jg	.L2137
	leaq	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	8(%r12), %rdx
	leaq	-2160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -2144(%rbp)
	je	.L2138
	cmpl	$1, 16(%r12)
	jle	.L2139
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdx
.L2054:
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2140
	cmpl	$2, 16(%r12)
	jg	.L2056
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2058
.L2145:
	movq	-1088(%rbp), %r15
	leaq	.LC22(%rip), %rax
	movl	$1, 536(%r13)
	movq	%rax, 544(%r13)
	movq	-2144(%rbp), %rbx
	movq	-1104(%rbp), %r14
	testq	%r15, %r15
	je	.L2059
	movzbl	540(%r13), %r8d
	testb	%r8b, %r8b
	jne	.L2141
	movq	576(%r13), %rdi
	leaq	1(%r14), %rcx
	testq	%rdi, %rdi
	je	.L2142
	movq	568(%r13), %rdx
	cmpq	%rdx, %rcx
	jbe	.L2135
	leaq	584(%r13), %rax
	movq	%rax, -2688(%rbp)
	cmpq	%rax, %rdi
	je	.L2063
	movl	$1, %r8d
	testq	%rcx, %rcx
	je	.L2143
.L2064:
	movq	%rcx, %rsi
	movb	%r8b, -2680(%rbp)
	movq	%rcx, -2672(%rbp)
	movq	%rdi, -2664(%rbp)
	call	realloc@PLT
	movq	-2664(%rbp), %rdi
	movq	-2672(%rbp), %rcx
	testq	%rax, %rax
	movzbl	-2680(%rbp), %r8d
	je	.L2144
.L2067:
	movq	%rax, 576(%r13)
	movq	%rcx, %rdx
	movq	%rcx, 568(%r13)
	testb	%r8b, %r8b
	jne	.L2135
.L2066:
	movq	560(%r13), %r8
	testq	%r8, %r8
	je	.L2135
	movq	-2688(%rbp), %rsi
	movq	%r8, %rdx
	movq	%rax, %rdi
	movq	%rcx, -2664(%rbp)
	call	memcpy@PLT
	movq	-2664(%rbp), %rcx
	movq	568(%r13), %rdx
	movq	%rcx, 560(%r13)
	cmpq	%rdx, %rcx
	jbe	.L2068
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2145
.L2058:
	cmpl	$4, %r14d
	jne	.L2146
	movq	$0, -2168(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2076
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2147
.L2077:
	cmpb	$0, (%rax)
	jne	.L2148
.L2076:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r15
	movq	-2144(%rbp), %r14
	jle	.L2149
	movq	8(%r12), %r13
	subq	$24, %r13
.L2082:
	movq	%rbx, %rdi
	leaq	-2608(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_rename@PLT
	testl	%eax, %eax
	js	.L2150
.L2083:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2088
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2151
.L2089:
	cmpb	$0, (%rax)
	jne	.L2152
.L2088:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-2168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2072
	movq	(%rdi), %rax
	call	*8(%rax)
.L2072:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2094
	testq	%rdi, %rdi
	je	.L2094
	call	free@PLT
.L2094:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2048
	testq	%rdi, %rdi
	je	.L2048
	call	free@PLT
.L2048:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2153
	addq	$2648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2135:
	.cfi_restore_state
	movq	%rcx, 560(%r13)
.L2068:
	cmpq	%rdx, %r14
	ja	.L2065
	movq	576(%r13), %rax
	movq	%r14, 560(%r13)
	movq	%r14, %rdx
	movq	%r15, %rsi
	movb	$0, (%rax,%r14)
	movq	576(%r13), %rdi
	call	memcpy@PLT
	movb	$1, 540(%r13)
.L2059:
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	jne	.L2154
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_rename@PLT
	testl	%eax, %eax
	js	.L2071
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
	jmp	.L2072
	.p2align 4,,10
	.p2align 3
.L2149:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1155(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2155
.L2080:
	testb	$5, (%rsi)
	je	.L2076
	leaq	.LC67(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2076
	.p2align 4,,10
	.p2align 3
.L2049:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2051
	.p2align 4,,10
	.p2align 3
.L2071:
	cltq
	leaq	-2656(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-2648(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L2156
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2073:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2072
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1158(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2157
.L2092:
	testb	$5, (%rsi)
	je	.L2088
	leaq	.LC67(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2138:
	leaq	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2140:
	leaq	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2158
.L2084:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2159
.L2085:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2083
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2151:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2147:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2142:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2063:
	xorl	%edi, %edi
	testq	%rcx, %rcx
	jne	.L2064
	movq	$0, 576(%r13)
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	$0, 568(%r13)
	jmp	.L2066
.L2156:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2146:
	leaq	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2157:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1158(%rip)
	mfence
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2155:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1155(%rip)
	mfence
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2154:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2141:
	leaq	_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2143:
	call	free@PLT
	movq	$0, 576(%r13)
	movq	$0, 568(%r13)
	movq	$0, 560(%r13)
.L2065:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2144:
	movb	%r8b, -2672(%rbp)
	movq	%rdi, -2680(%rbp)
	movq	%rcx, -2664(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2664(%rbp), %rcx
	movq	-2680(%rbp), %rdi
	movq	%rcx, %rsi
	call	realloc@PLT
	movq	-2664(%rbp), %rcx
	movzbl	-2672(%rbp), %r8d
	testq	%rax, %rax
	jne	.L2067
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2159:
	movq	%rax, -2664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2664(%rbp), %rcx
	jmp	.L2085
.L2158:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2084
.L2153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7908:
	.size	_ZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC68:
	.string	"fs.sync.fchown"
	.text
	.p2align 4
	.type	_ZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2210
	cmpw	$1040, %cx
	jne	.L2161
.L2210:
	movq	23(%rdx), %rbx
.L2163:
	movl	16(%r14), %r15d
	cmpl	$2, %r15d
	jg	.L2220
	leaq	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2220:
	movq	8(%r14), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L2221
	movl	16(%r14), %eax
	testl	%eax, %eax
	jle	.L2222
	movq	8(%r14), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r14)
	movl	%eax, %r12d
	jg	.L2167
.L2227:
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2168:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2223
	cmpl	$1, 16(%r14)
	jg	.L2170
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%r14)
	movl	%eax, %r13d
	jg	.L2172
.L2228:
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2173:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2224
	cmpl	$2, 16(%r14)
	jg	.L2175
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2176:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%r14)
	movl	%eax, -568(%rbp)
	jg	.L2177
	movq	(%r14), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L2179
.L2229:
	leaq	.LC44(%rip), %rax
	cmpq	$0, 80(%r11)
	movq	%r11, 88(%r11)
	movq	%rax, 544(%r11)
	movl	$1, 536(%r11)
	jne	.L2225
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r11), %r15
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movq	%rax, 80(%r11)
	movq	16(%r11), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r9
	movq	%r15, %rsi
	movl	-568(%rbp), %r8d
	movq	%r11, -576(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fchown@PLT
	movq	-576(%rbp), %r11
	testl	%eax, %eax
	js	.L2181
	movq	16(%r11), %rax
	movq	%r14, %rsi
	movq	%r11, %rdi
	addl	$1, 2156(%rax)
	movq	(%r11), %rax
	call	*120(%rax)
.L2160:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2226
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2222:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r14)
	movl	%eax, %r12d
	jle	.L2227
.L2167:
	movq	8(%r14), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2168
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	8(%r14), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%r14)
	movl	%eax, %r13d
	jle	.L2228
.L2172:
	movq	8(%r14), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2175:
	movq	8(%r14), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2177:
	movq	8(%r14), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r11
	testq	%rax, %rax
	jne	.L2229
.L2179:
	cmpl	$5, %r15d
	jne	.L2230
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2186
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2231
.L2187:
	cmpb	$0, (%rax)
	jne	.L2232
.L2186:
	cmpl	$4, 16(%r14)
	jle	.L2233
	movq	8(%r14), %rax
	leaq	-32(%rax), %r14
.L2192:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r15
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r9d, %r9d
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movq	360(%rbx), %rax
	movl	-568(%rbp), %r8d
	movq	%r15, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_fchown@PLT
	testl	%eax, %eax
	js	.L2234
.L2193:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2198
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2235
.L2199:
	cmpb	$0, (%rax)
	jne	.L2236
.L2198:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	(%r14), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r14
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2181:
	cltq
	leaq	-560(%rbp), %r12
	movq	%r11, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r11)
	movq	%r12, %rdi
	movq	$0, 192(%r11)
	movq	%r11, -568(%rbp)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	movq	-568(%rbp), %r11
	cmpq	$0, 88(%rsi)
	js	.L2237
	movq	16(%r11), %rdx
	movq	(%r11), %rax
	movq	%r11, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2183:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2161:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2163
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2153(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2238
.L2190:
	testb	$5, (%rsi)
	je	.L2186
	leaq	.LC68(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2156(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2239
.L2202:
	testb	$5, (%rsi)
	je	.L2198
	leaq	.LC68(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2221:
	leaq	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2223:
	leaq	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2224:
	leaq	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2234:
	movq	352(%rbx), %r13
	movl	%eax, %esi
	movq	3280(%rbx), %r12
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2240
.L2194:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2241
.L2195:
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2193
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2193
	.p2align 4,,10
	.p2align 3
.L2231:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2235:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L2225:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2230:
	leaq	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2238:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2153(%rip)
	mfence
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2239:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2156(%rip)
	mfence
	jmp	.L2202
.L2237:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2183
.L2241:
	movq	%rax, -568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-568(%rbp), %rcx
	jmp	.L2195
.L2240:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2194
.L2226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7940:
	.size	_ZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC69:
	.string	"futime"
.LC70:
	.string	"fs.sync.futimes"
	.text
	.p2align 4
	.type	_ZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2292
	cmpw	$1040, %cx
	jne	.L2243
.L2292:
	movq	23(%rdx), %rbx
.L2245:
	movl	16(%r12), %r15d
	cmpl	$2, %r15d
	jg	.L2302
	leaq	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2302:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L2303
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L2304
	movq	8(%r12), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jg	.L2249
.L2309:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2250:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L2305
	cmpl	$1, 16(%r12)
	jg	.L2252
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2253:
	call	_ZNK2v86Number5ValueEv@PLT
	cmpl	$2, 16(%r12)
	movsd	%xmm0, -568(%rbp)
	jg	.L2254
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2255:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L2306
	cmpl	$2, 16(%r12)
	jg	.L2257
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2258:
	call	_ZNK2v86Number5ValueEv@PLT
	cmpl	$3, 16(%r12)
	movsd	%xmm0, -576(%rbp)
	jg	.L2259
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2261
.L2310:
	leaq	.LC69(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movl	$1, 536(%r13)
	jne	.L2307
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r15
	movsd	-576(%rbp), %xmm1
	movsd	-568(%rbp), %xmm0
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	movl	%r14d, %edx
	movq	%r15, %rsi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_futime@PLT
	testl	%eax, %eax
	js	.L2263
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L2242:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2308
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2304:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jle	.L2309
.L2249:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2250
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2254:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2257:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2259:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2310
.L2261:
	cmpl	$5, %r15d
	jne	.L2311
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2268
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2312
.L2269:
	cmpb	$0, (%rax)
	jne	.L2313
.L2268:
	cmpl	$4, 16(%r12)
	jle	.L2314
	movq	8(%r12), %r13
	subq	$32, %r13
.L2274:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	360(%rbx), %rax
	movsd	-576(%rbp), %xmm1
	movsd	-568(%rbp), %xmm0
	movq	2360(%rax), %rdi
	call	uv_fs_futime@PLT
	testl	%eax, %eax
	js	.L2315
.L2275:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2280
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2316
.L2281:
	cmpb	$0, (%rax)
	jne	.L2317
.L2280:
	movq	%r12, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2314:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2263:
	cltq
	leaq	-560(%rbp), %r12
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L2318
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2265:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2243:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2242(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2319
.L2272:
	testb	$5, (%rsi)
	je	.L2268
	leaq	.LC70(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2317:
	movq	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2245(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2320
.L2284:
	testb	$5, (%rsi)
	je	.L2280
	leaq	.LC70(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2280
	.p2align 4,,10
	.p2align 3
.L2303:
	leaq	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2305:
	leaq	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2306:
	leaq	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2315:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2321
.L2276:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC69(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2322
.L2277:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2275
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2275
	.p2align 4,,10
	.p2align 3
.L2312:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2316:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2307:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2311:
	leaq	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2319:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2242(%rip)
	mfence
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2320:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2245(%rip)
	mfence
	jmp	.L2284
.L2318:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2265
.L2322:
	movq	%rax, -568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-568(%rbp), %rcx
	jmp	.L2277
.L2321:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2276
.L2308:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7943:
	.size	_ZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC71:
	.string	"fs.sync.chown"
	.text
	.p2align 4
	.type	_ZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2368
	cmpw	$1040, %cx
	jne	.L2324
.L2368:
	movq	23(%rdx), %rbx
.L2326:
	movl	16(%r12), %r15d
	cmpl	$2, %r15d
	jg	.L2384
	leaq	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2384:
	movq	8(%r12), %rdx
	movq	352(%rbx), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2385
	cmpl	$1, 16(%r12)
	jle	.L2386
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L2329:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2387
	cmpl	$1, 16(%r12)
	jg	.L2331
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2332:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r13d
	jg	.L2333
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2334:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2388
	cmpl	$2, 16(%r12)
	jg	.L2336
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2337:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %r14d
	jg	.L2338
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L2340
.L2391:
	leaq	.LC43(%rip), %rax
	cmpq	$0, 80(%r10)
	movq	%r10, 88(%r10)
	movq	%rax, 544(%r10)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r10)
	jne	.L2389
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r10), %r15
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	%rax, 80(%r10)
	movq	16(%r10), %rax
	movq	%r15, %rsi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r9
	movq	%r10, -1608(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_chown@PLT
	movq	-1608(%rbp), %r10
	testl	%eax, %eax
	js	.L2342
	movq	16(%r10), %rax
	movq	%r12, %rsi
	movq	%r10, %rdi
	addl	$1, 2156(%rax)
	movq	(%r10), %rax
	call	*120(%rax)
.L2343:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2323
	testq	%rdi, %rdi
	je	.L2323
	call	free@PLT
.L2323:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2390
	addq	$1576, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2386:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2333:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2331:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2338:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L2391
.L2340:
	cmpl	$5, %r15d
	jne	.L2392
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2393
.L2347:
	cmpl	$4, 16(%r12)
	movq	-1088(%rbp), %rdx
	jle	.L2394
	movq	8(%r12), %r12
	subq	$32, %r12
.L2352:
	movq	%rbx, %rdi
	movq	%rdx, -1608(%rbp)
	leaq	-1552(%rbp), %r15
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	360(%rbx), %rax
	movq	-1608(%rbp), %rdx
	movq	%r15, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_chown@PLT
	testl	%eax, %eax
	js	.L2395
.L2353:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2396
.L2358:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2336:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2394:
	movq	(%r12), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2342:
	cltq
	leaq	-1600(%rbp), %r12
	movq	%r10, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r10)
	movq	%r12, %rdi
	movq	$0, 192(%r10)
	movq	%r10, -1608(%rbp)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1592(%rbp), %rsi
	movq	-1608(%rbp), %r10
	cmpq	$0, 88(%rsi)
	js	.L2397
	movq	16(%r10), %rdx
	movq	(%r10), %rax
	movq	%r10, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2344:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2324:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2120(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2398
.L2349:
	testb	$5, (%rsi)
	je	.L2347
	leaq	.LC71(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2396:
	movq	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2123(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2399
.L2360:
	testb	$5, (%rsi)
	je	.L2358
	leaq	.LC71(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2385:
	leaq	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2387:
	leaq	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2388:
	leaq	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2395:
	movq	352(%rbx), %r14
	movl	%eax, %esi
	movq	3280(%rbx), %r13
	movq	%r14, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2400
.L2354:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC43(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2401
.L2355:
	movq	360(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2353
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2389:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2392:
	leaq	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2399:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2123(%rip)
	mfence
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2398:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2120(%rip)
	mfence
	jmp	.L2349
.L2397:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2344
.L2401:
	movq	%rax, -1608(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1608(%rbp), %rcx
	jmp	.L2355
.L2400:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2354
.L2390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7939:
	.size	_ZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC72:
	.string	"fs.sync.lchown"
	.text
	.p2align 4
	.type	_ZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2447
	cmpw	$1040, %cx
	jne	.L2403
.L2447:
	movq	23(%rdx), %rbx
.L2405:
	movl	16(%r12), %r15d
	cmpl	$2, %r15d
	jg	.L2463
	leaq	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	8(%r12), %rdx
	movq	352(%rbx), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2464
	cmpl	$1, 16(%r12)
	jle	.L2465
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L2408:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2466
	cmpl	$1, 16(%r12)
	jg	.L2410
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2411:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r13d
	jg	.L2412
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2413:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2467
	cmpl	$2, 16(%r12)
	jg	.L2415
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2416:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %r14d
	jg	.L2417
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L2419
.L2470:
	leaq	.LC45(%rip), %rax
	cmpq	$0, 80(%r10)
	movq	%r10, 88(%r10)
	movq	%rax, 544(%r10)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r10)
	jne	.L2468
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r10), %r15
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	%rax, 80(%r10)
	movq	16(%r10), %rax
	movq	%r15, %rsi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r9
	movq	%r10, -1608(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_lchown@PLT
	movq	-1608(%rbp), %r10
	testl	%eax, %eax
	js	.L2421
	movq	16(%r10), %rax
	movq	%r12, %rsi
	movq	%r10, %rdi
	addl	$1, 2156(%rax)
	movq	(%r10), %rax
	call	*120(%rax)
.L2422:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2402
	testq	%rdi, %rdi
	je	.L2402
	call	free@PLT
.L2402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2469
	addq	$1576, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2465:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2412:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2410:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2417:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L2470
.L2419:
	cmpl	$5, %r15d
	jne	.L2471
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2472
.L2426:
	cmpl	$4, 16(%r12)
	movq	-1088(%rbp), %rdx
	jle	.L2473
	movq	8(%r12), %r12
	subq	$32, %r12
.L2431:
	movq	%rbx, %rdi
	movq	%rdx, -1608(%rbp)
	leaq	-1552(%rbp), %r15
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	360(%rbx), %rax
	movq	-1608(%rbp), %rdx
	movq	%r15, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_lchown@PLT
	testl	%eax, %eax
	js	.L2474
.L2432:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2475
.L2437:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2415:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	(%r12), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L2431
	.p2align 4,,10
	.p2align 3
.L2421:
	cltq
	leaq	-1600(%rbp), %r12
	movq	%r10, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r10)
	movq	%r12, %rdi
	movq	$0, 192(%r10)
	movq	%r10, -1608(%rbp)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1592(%rbp), %rsi
	movq	-1608(%rbp), %r10
	cmpq	$0, 88(%rsi)
	js	.L2476
	movq	16(%r10), %rdx
	movq	(%r10), %rax
	movq	%r10, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2423:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2403:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2472:
	movq	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2183(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2477
.L2428:
	testb	$5, (%rsi)
	je	.L2426
	leaq	.LC72(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2475:
	movq	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2186(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2478
.L2439:
	testb	$5, (%rsi)
	je	.L2437
	leaq	.LC72(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2464:
	leaq	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2466:
	leaq	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2467:
	leaq	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2474:
	movq	352(%rbx), %r14
	movl	%eax, %esi
	movq	3280(%rbx), %r13
	movq	%r14, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2479
.L2433:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2480
.L2434:
	movq	360(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2432
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2468:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2471:
	leaq	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2478:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2186(%rip)
	mfence
	jmp	.L2439
	.p2align 4,,10
	.p2align 3
.L2477:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2183(%rip)
	mfence
	jmp	.L2428
.L2476:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2423
.L2480:
	movq	%rax, -1608(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1608(%rbp), %rcx
	jmp	.L2434
.L2479:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2433
.L2469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7941:
	.size	_ZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC73:
	.string	"utime"
.LC74:
	.string	"fs.sync.utimes"
	.text
	.p2align 4
	.type	_ZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2526
	cmpw	$1040, %cx
	jne	.L2482
.L2526:
	movq	23(%rdx), %rbx
.L2484:
	movl	16(%r12), %r14d
	cmpl	$2, %r14d
	jg	.L2542
	leaq	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2542:
	movq	8(%r12), %rdx
	movq	352(%rbx), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2543
	cmpl	$1, 16(%r12)
	jle	.L2544
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L2487:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L2545
	cmpl	$1, 16(%r12)
	jg	.L2489
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2490:
	call	_ZNK2v86Number5ValueEv@PLT
	cmpl	$2, 16(%r12)
	movsd	%xmm0, -1608(%rbp)
	jg	.L2491
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2492:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L2546
	cmpl	$2, 16(%r12)
	jg	.L2494
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2495:
	call	_ZNK2v86Number5ValueEv@PLT
	cmpl	$3, 16(%r12)
	movsd	%xmm0, -1616(%rbp)
	jg	.L2496
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2498
.L2549:
	leaq	.LC73(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r13)
	jne	.L2547
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movsd	-1616(%rbp), %xmm1
	movsd	-1608(%rbp), %xmm0
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_utime@PLT
	testl	%eax, %eax
	js	.L2500
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L2501:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2481
	testq	%rdi, %rdi
	je	.L2481
	call	free@PLT
.L2481:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2548
	addq	$1576, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2544:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2549
.L2498:
	cmpl	$5, %r14d
	jne	.L2550
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2551
.L2505:
	cmpl	$4, 16(%r12)
	movq	-1088(%rbp), %r14
	jle	.L2552
	movq	8(%r12), %r12
	subq	$32, %r12
.L2510:
	movq	%rbx, %rdi
	leaq	-1552(%rbp), %r13
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	360(%rbx), %rax
	movsd	-1616(%rbp), %xmm1
	movsd	-1608(%rbp), %xmm0
	movq	2360(%rax), %rdi
	call	uv_fs_utime@PLT
	testl	%eax, %eax
	js	.L2553
.L2511:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2554
.L2516:
	movq	%r13, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2494:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2552:
	movq	(%r12), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2500:
	cltq
	leaq	-1600(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1592(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L2555
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2502:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2482:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2213(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2556
.L2507:
	testb	$5, (%rsi)
	je	.L2505
	leaq	.LC74(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2554:
	movq	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2216(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2557
.L2518:
	testb	$5, (%rsi)
	je	.L2516
	leaq	.LC74(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2516
	.p2align 4,,10
	.p2align 3
.L2543:
	leaq	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2545:
	leaq	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2546:
	leaq	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2553:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2558
.L2512:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC73(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2559
.L2513:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2511
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2547:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2550:
	leaq	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2557:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2216(%rip)
	mfence
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2556:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2213(%rip)
	mfence
	jmp	.L2507
.L2555:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2502
.L2559:
	movq	%rax, -1608(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1608(%rbp), %rcx
	jmp	.L2513
.L2558:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2512
.L2548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7942:
	.size	_ZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC75:
	.string	"fs.sync.symlink"
	.text
	.p2align 4
	.type	_ZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2664, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2619
	cmpw	$1040, %cx
	jne	.L2561
.L2619:
	movq	23(%rdx), %rbx
.L2563:
	movl	16(%r12), %r15d
	movq	352(%rbx), %r13
	cmpl	$3, %r15d
	jg	.L2645
	leaq	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2645:
	movq	8(%r12), %rdx
	leaq	-2160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -2144(%rbp)
	je	.L2646
	cmpl	$1, 16(%r12)
	jle	.L2647
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdx
.L2566:
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2648
	cmpl	$2, 16(%r12)
	jg	.L2568
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2569:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2649
	cmpl	$2, 16(%r12)
	jg	.L2571
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %r14d
	jg	.L2573
.L2654:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L2575
.L2655:
	movq	-1088(%rbp), %r15
	leaq	.LC33(%rip), %rax
	movl	$1, 536(%r10)
	movq	%rax, 544(%r10)
	movq	-2144(%rbp), %r11
	movq	-1104(%rbp), %rbx
	testq	%r15, %r15
	je	.L2576
	cmpb	$0, 540(%r10)
	jne	.L2650
	movq	576(%r10), %rdi
	leaq	1(%rbx), %r8
	testq	%rdi, %rdi
	je	.L2651
	movq	568(%r10), %rdx
	cmpq	%rdx, %r8
	jbe	.L2643
	leaq	584(%r10), %rax
	movq	%rax, -2696(%rbp)
	cmpq	%rax, %rdi
	je	.L2580
	testq	%r8, %r8
	je	.L2652
.L2581:
	movq	%r8, %rsi
	movq	%r11, -2688(%rbp)
	movq	%r10, -2680(%rbp)
	movq	%r8, -2672(%rbp)
	movq	%rdi, -2664(%rbp)
	call	realloc@PLT
	movq	-2664(%rbp), %rdi
	movq	-2672(%rbp), %r8
	testq	%rax, %rax
	movq	-2680(%rbp), %r10
	movq	-2688(%rbp), %r11
	je	.L2653
.L2584:
	movq	%rax, 576(%r10)
	movq	%r8, %rdx
	movq	%r8, 568(%r10)
	testb	%r13b, %r13b
	jne	.L2643
.L2583:
	movq	560(%r10), %r9
	testq	%r9, %r9
	je	.L2643
	movq	-2696(%rbp), %rsi
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r8, -2680(%rbp)
	movq	%r11, -2672(%rbp)
	movq	%r10, -2664(%rbp)
	call	memcpy@PLT
	movq	-2664(%rbp), %r10
	movq	-2680(%rbp), %r8
	movq	-2672(%rbp), %r11
	movq	568(%r10), %rdx
	movq	%r8, 560(%r10)
	cmpq	%rdx, %r8
	jbe	.L2585
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2647:
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L2568:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2571:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %r14d
	jle	.L2654
.L2573:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L2655
.L2575:
	cmpl	$5, %r15d
	jne	.L2656
	movq	$0, -2168(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2657
.L2593:
	cmpl	$4, 16(%r12)
	movq	-1088(%rbp), %rcx
	movq	-2144(%rbp), %r15
	jle	.L2658
	movq	8(%r12), %r12
	subq	$32, %r12
.L2598:
	movq	%rbx, %rdi
	movq	%rcx, -2664(%rbp)
	leaq	-2608(%rbp), %r13
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movq	%r15, %rdx
	movq	360(%rbx), %rax
	movq	-2664(%rbp), %rcx
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_symlink@PLT
	testl	%eax, %eax
	js	.L2659
.L2599:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2660
.L2604:
	movq	%r13, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
.L2589:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2608
	testq	%rdi, %rdi
	je	.L2608
	call	free@PLT
.L2608:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2560
	testq	%rdi, %rdi
	je	.L2560
	call	free@PLT
.L2560:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2661
	addq	$2664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2643:
	.cfi_restore_state
	movq	%r8, 560(%r10)
.L2585:
	cmpq	%rbx, %rdx
	jb	.L2582
	movq	576(%r10), %rax
	movq	%rbx, 560(%r10)
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r11, -2672(%rbp)
	movb	$0, (%rax,%rbx)
	movq	576(%r10), %rdi
	movq	%r10, -2664(%rbp)
	call	memcpy@PLT
	movq	-2664(%rbp), %r10
	movq	-2672(%rbp), %r11
	movb	$1, 540(%r10)
.L2576:
	cmpq	$0, 80(%r10)
	movq	%r10, 88(%r10)
	jne	.L2662
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	movl	%r14d, %r8d
	movq	%r15, %rcx
	movq	%r11, %rdx
	movq	%rax, 80(%r10)
	movq	16(%r10), %rax
	leaq	88(%r10), %r13
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r9
	movq	%r13, %rsi
	movq	%r10, -2664(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_symlink@PLT
	movq	-2664(%rbp), %r10
	testl	%eax, %eax
	js	.L2588
	movq	16(%r10), %rax
	movq	%r12, %rsi
	movq	%r10, %rdi
	addl	$1, 2156(%rax)
	movq	(%r10), %rax
	call	*120(%rax)
	jmp	.L2589
	.p2align 4,,10
	.p2align 3
.L2658:
	movq	(%r12), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L2598
	.p2align 4,,10
	.p2align 3
.L2561:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L2657:
	movq	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1058(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2663
.L2595:
	testb	$5, (%rsi)
	je	.L2593
	leaq	.LC75(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2660:
	movq	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1061(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2664
.L2606:
	testb	$5, (%rsi)
	je	.L2604
	leaq	.LC75(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L2646:
	leaq	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2648:
	leaq	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2649:
	leaq	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2665
.L2600:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2666
.L2601:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2599
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2599
	.p2align 4,,10
	.p2align 3
.L2588:
	cltq
	leaq	-2656(%rbp), %r12
	movq	%r10, %rsi
	movq	%r13, %rdx
	movq	%rax, 176(%r10)
	movq	%r12, %rdi
	movq	$0, 192(%r10)
	movq	%r10, -2664(%rbp)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-2648(%rbp), %rsi
	movq	-2664(%rbp), %r10
	cmpq	$0, 88(%rsi)
	js	.L2667
	movq	16(%r10), %rdx
	movq	(%r10), %rax
	movq	%r10, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2590:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2589
	.p2align 4,,10
	.p2align 3
.L2650:
	leaq	_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2580:
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	testq	%r8, %r8
	jne	.L2581
	movq	$0, 576(%r10)
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	$0, 568(%r10)
	jmp	.L2583
	.p2align 4,,10
	.p2align 3
.L2656:
	leaq	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2664:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1061(%rip)
	mfence
	jmp	.L2606
	.p2align 4,,10
	.p2align 3
.L2663:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1058(%rip)
	mfence
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2662:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2651:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2652:
	movq	%r10, -2664(%rbp)
	call	free@PLT
	movq	-2664(%rbp), %r10
	movq	$0, 576(%r10)
	movq	$0, 568(%r10)
	movq	$0, 560(%r10)
.L2582:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2653:
	movq	%r11, -2680(%rbp)
	movq	%r10, -2672(%rbp)
	movq	%rdi, -2688(%rbp)
	movq	%r8, -2664(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2664(%rbp), %r8
	movq	-2688(%rbp), %rdi
	movq	%r8, %rsi
	call	realloc@PLT
	movq	-2664(%rbp), %r8
	movq	-2672(%rbp), %r10
	testq	%rax, %rax
	movq	-2680(%rbp), %r11
	jne	.L2584
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2666:
	movq	%rax, -2664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2664(%rbp), %rcx
	jmp	.L2601
.L2665:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2600
.L2667:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2590
.L2661:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7905:
	.size	_ZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC76:
	.string	"copyfile"
.LC77:
	.string	"fs.sync.copyfile"
	.text
	.p2align 4
	.type	_ZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2664, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2727
	cmpw	$1040, %cx
	jne	.L2669
.L2727:
	movq	23(%rdx), %rbx
.L2671:
	movl	16(%r12), %r15d
	movq	352(%rbx), %r13
	cmpl	$2, %r15d
	jg	.L2753
	leaq	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2753:
	movq	8(%r12), %rdx
	leaq	-2160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -2144(%rbp)
	je	.L2754
	cmpl	$1, 16(%r12)
	jle	.L2755
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdx
.L2674:
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2756
	cmpl	$2, 16(%r12)
	jg	.L2676
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2677:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L2757
	cmpl	$2, 16(%r12)
	jg	.L2679
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %r14d
	jg	.L2681
.L2762:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L2683
.L2763:
	movq	-1088(%rbp), %r15
	leaq	.LC76(%rip), %rax
	movl	$1, 536(%r10)
	movq	%rax, 544(%r10)
	movq	-2144(%rbp), %r11
	movq	-1104(%rbp), %rbx
	testq	%r15, %r15
	je	.L2684
	cmpb	$0, 540(%r10)
	jne	.L2758
	movq	576(%r10), %rdi
	leaq	1(%rbx), %r8
	testq	%rdi, %rdi
	je	.L2759
	movq	568(%r10), %rdx
	cmpq	%rdx, %r8
	jbe	.L2751
	leaq	584(%r10), %rax
	movq	%rax, -2696(%rbp)
	cmpq	%rax, %rdi
	je	.L2688
	testq	%r8, %r8
	je	.L2760
.L2689:
	movq	%r8, %rsi
	movq	%r11, -2688(%rbp)
	movq	%r10, -2680(%rbp)
	movq	%r8, -2672(%rbp)
	movq	%rdi, -2664(%rbp)
	call	realloc@PLT
	movq	-2664(%rbp), %rdi
	movq	-2672(%rbp), %r8
	testq	%rax, %rax
	movq	-2680(%rbp), %r10
	movq	-2688(%rbp), %r11
	je	.L2761
.L2692:
	movq	%rax, 576(%r10)
	movq	%r8, %rdx
	movq	%r8, 568(%r10)
	testb	%r13b, %r13b
	jne	.L2751
.L2691:
	movq	560(%r10), %r9
	testq	%r9, %r9
	je	.L2751
	movq	-2696(%rbp), %rsi
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r8, -2680(%rbp)
	movq	%r11, -2672(%rbp)
	movq	%r10, -2664(%rbp)
	call	memcpy@PLT
	movq	-2664(%rbp), %r10
	movq	-2680(%rbp), %r8
	movq	-2672(%rbp), %r11
	movq	568(%r10), %rdx
	movq	%r8, 560(%r10)
	cmpq	%rdx, %r8
	jbe	.L2693
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L2674
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2677
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %r14d
	jle	.L2762
.L2681:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L2763
.L2683:
	cmpl	$5, %r15d
	jne	.L2764
	movq	$0, -2168(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2765
.L2701:
	cmpl	$4, 16(%r12)
	movq	-1088(%rbp), %rcx
	movq	-2144(%rbp), %r15
	jle	.L2766
	movq	8(%r12), %r12
	subq	$32, %r12
.L2706:
	movq	%rbx, %rdi
	movq	%rcx, -2664(%rbp)
	leaq	-2608(%rbp), %r13
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movq	%r15, %rdx
	movq	360(%rbx), %rax
	movq	-2664(%rbp), %rcx
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	call	uv_fs_copyfile@PLT
	testl	%eax, %eax
	js	.L2767
.L2707:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L2768
.L2712:
	movq	%r13, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
.L2697:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2716
	testq	%rdi, %rdi
	je	.L2716
	call	free@PLT
.L2716:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2668
	testq	%rdi, %rdi
	je	.L2668
	call	free@PLT
.L2668:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2769
	addq	$2664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2751:
	.cfi_restore_state
	movq	%r8, 560(%r10)
.L2693:
	cmpq	%rbx, %rdx
	jb	.L2690
	movq	576(%r10), %rax
	movq	%rbx, 560(%r10)
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r11, -2672(%rbp)
	movb	$0, (%rax,%rbx)
	movq	576(%r10), %rdi
	movq	%r10, -2664(%rbp)
	call	memcpy@PLT
	movq	-2664(%rbp), %r10
	movq	-2672(%rbp), %r11
	movb	$1, 540(%r10)
.L2684:
	cmpq	$0, 80(%r10)
	movq	%r10, 88(%r10)
	jne	.L2770
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	movl	%r14d, %r8d
	movq	%r15, %rcx
	movq	%r11, %rdx
	movq	%rax, 80(%r10)
	movq	16(%r10), %rax
	leaq	88(%r10), %r13
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r9
	movq	%r13, %rsi
	movq	%r10, -2664(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_copyfile@PLT
	movq	-2664(%rbp), %r10
	testl	%eax, %eax
	js	.L2696
	movq	16(%r10), %rax
	movq	%r12, %rsi
	movq	%r10, %rdi
	addl	$1, 2156(%rax)
	movq	(%r10), %rax
	call	*120(%rax)
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2766:
	movq	(%r12), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2669:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2671
	.p2align 4,,10
	.p2align 3
.L2765:
	movq	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1728(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2771
.L2703:
	testb	$5, (%rsi)
	je	.L2701
	leaq	.LC77(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2768:
	movq	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1731(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2772
.L2714:
	testb	$5, (%rsi)
	je	.L2712
	leaq	.LC77(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L2754:
	leaq	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2756:
	leaq	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2757:
	leaq	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2767:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2773
.L2708:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC76(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2774
.L2709:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2707
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2696:
	cltq
	leaq	-2656(%rbp), %r12
	movq	%r10, %rsi
	movq	%r13, %rdx
	movq	%rax, 176(%r10)
	movq	%r12, %rdi
	movq	$0, 192(%r10)
	movq	%r10, -2664(%rbp)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-2648(%rbp), %rsi
	movq	-2664(%rbp), %r10
	cmpq	$0, 88(%rsi)
	js	.L2775
	movq	16(%r10), %rdx
	movq	(%r10), %rax
	movq	%r10, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2698:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2758:
	leaq	_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2688:
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	testq	%r8, %r8
	jne	.L2689
	movq	$0, 576(%r10)
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	$0, 568(%r10)
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2764:
	leaq	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2772:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1731(%rip)
	mfence
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L2771:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1728(%rip)
	mfence
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2770:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2759:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2760:
	movq	%r10, -2664(%rbp)
	call	free@PLT
	movq	-2664(%rbp), %r10
	movq	$0, 576(%r10)
	movq	$0, 568(%r10)
	movq	$0, 560(%r10)
.L2690:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2761:
	movq	%r11, -2680(%rbp)
	movq	%r10, -2672(%rbp)
	movq	%rdi, -2688(%rbp)
	movq	%r8, -2664(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2664(%rbp), %r8
	movq	-2688(%rbp), %rdi
	movq	%r8, %rsi
	call	realloc@PLT
	movq	-2664(%rbp), %r8
	movq	-2672(%rbp), %r10
	testq	%rax, %rax
	movq	-2680(%rbp), %r11
	jne	.L2692
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2774:
	movq	%rax, -2664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2664(%rbp), %rcx
	jmp	.L2709
.L2773:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2708
.L2775:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2698
.L2769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7931:
	.size	_ZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC78:
	.string	"fs.sync.fdatasync"
	.text
	.p2align 4
	.type	_ZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2817
	cmpw	$1040, %cx
	jne	.L2777
.L2817:
	movq	23(%rdx), %rbx
.L2779:
	movl	16(%r12), %r15d
	cmpl	$1, %r15d
	jg	.L2830
	leaq	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2830:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L2831
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L2832
	movq	8(%r12), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jg	.L2783
.L2835:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2785
.L2836:
	leaq	.LC20(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movl	$1, 536(%r13)
	jne	.L2833
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r15
	movl	%r14d, %edx
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fdatasync@PLT
	testl	%eax, %eax
	js	.L2787
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L2776:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2834
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2832:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jle	.L2835
.L2783:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-8(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2836
.L2785:
	cmpl	$3, %r15d
	jne	.L2837
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2792
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2838
.L2793:
	cmpb	$0, (%rax)
	jne	.L2839
.L2792:
	cmpl	$2, 16(%r12)
	jle	.L2840
	movq	8(%r12), %r13
	subq	$16, %r13
.L2798:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fdatasync@PLT
	testl	%eax, %eax
	js	.L2841
.L2799:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2804
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2842
.L2805:
	cmpb	$0, (%rax)
	jne	.L2843
.L2804:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2776
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2840:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L2798
	.p2align 4,,10
	.p2align 3
.L2839:
	movq	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1204(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2844
.L2796:
	testb	$5, (%rsi)
	je	.L2792
	leaq	.LC78(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2777:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2779
	.p2align 4,,10
	.p2align 3
.L2843:
	movq	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1206(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2845
.L2808:
	testb	$5, (%rsi)
	je	.L2804
	leaq	.LC78(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L2831:
	leaq	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2841:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2846
.L2800:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2847
.L2801:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2799
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2787:
	cltq
	leaq	-560(%rbp), %r12
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L2848
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2789:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2838:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2793
	.p2align 4,,10
	.p2align 3
.L2842:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2805
	.p2align 4,,10
	.p2align 3
.L2833:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2837:
	leaq	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2844:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1204(%rip)
	mfence
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2845:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1206(%rip)
	mfence
	jmp	.L2808
	.p2align 4,,10
	.p2align 3
.L2848:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2789
.L2847:
	movq	%rax, -568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-568(%rbp), %rcx
	jmp	.L2801
.L2846:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2800
.L2834:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7910:
	.size	_ZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC79:
	.string	"fs.sync.close"
	.text
	.p2align 4
	.globl	_ZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2890
	cmpw	$1040, %cx
	jne	.L2850
.L2890:
	movq	23(%rdx), %rbx
.L2852:
	movl	16(%r12), %r15d
	cmpl	$1, %r15d
	jg	.L2903
	leaq	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2903:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L2904
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L2905
	movq	8(%r12), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jg	.L2856
.L2908:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2858
.L2909:
	leaq	.LC3(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movl	$1, 536(%r13)
	jne	.L2906
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r15
	movl	%r14d, %edx
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	js	.L2860
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L2849:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2907
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2905:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jle	.L2908
.L2856:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-8(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2909
.L2858:
	cmpl	$3, %r15d
	jne	.L2910
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2865
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2911
.L2866:
	cmpb	$0, (%rax)
	jne	.L2912
.L2865:
	cmpl	$2, 16(%r12)
	jle	.L2913
	movq	8(%r12), %r13
	subq	$16, %r13
.L2871:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	js	.L2914
.L2872:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2877
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2915
.L2878:
	cmpb	$0, (%rax)
	jne	.L2916
.L2877:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2849
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2849
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L2871
	.p2align 4,,10
	.p2align 3
.L2912:
	movq	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic823(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2917
.L2869:
	testb	$5, (%rsi)
	je	.L2865
	leaq	.LC79(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2865
	.p2align 4,,10
	.p2align 3
.L2850:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2852
	.p2align 4,,10
	.p2align 3
.L2916:
	movq	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic825(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2918
.L2881:
	testb	$5, (%rsi)
	je	.L2877
	leaq	.LC79(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2877
	.p2align 4,,10
	.p2align 3
.L2904:
	leaq	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2914:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2919
.L2873:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2920
.L2874:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2872
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2872
	.p2align 4,,10
	.p2align 3
.L2860:
	cltq
	leaq	-560(%rbp), %r12
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L2921
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2862:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2849
	.p2align 4,,10
	.p2align 3
.L2911:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2915:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2878
	.p2align 4,,10
	.p2align 3
.L2906:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2910:
	leaq	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2917:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic823(%rip)
	mfence
	jmp	.L2869
	.p2align 4,,10
	.p2align 3
.L2918:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic825(%rip)
	mfence
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2921:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2862
.L2920:
	movq	%rax, -568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-568(%rbp), %rcx
	jmp	.L2874
.L2919:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2873
.L2907:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7886:
	.size	_ZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC80:
	.string	"fs.sync.fsync"
	.text
	.p2align 4
	.type	_ZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2963
	cmpw	$1040, %cx
	jne	.L2923
.L2963:
	movq	23(%rdx), %rbx
.L2925:
	movl	16(%r12), %r15d
	cmpl	$1, %r15d
	jg	.L2976
	leaq	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2976:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L2977
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L2978
	movq	8(%r12), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jg	.L2929
.L2981:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2931
.L2982:
	leaq	.LC21(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movl	$1, 536(%r13)
	jne	.L2979
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r15
	movl	%r14d, %edx
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fsync@PLT
	testl	%eax, %eax
	js	.L2933
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L2922:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2980
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2978:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r14d
	jle	.L2981
.L2929:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-8(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2982
.L2931:
	cmpl	$3, %r15d
	jne	.L2983
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2938
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2984
.L2939:
	cmpb	$0, (%rax)
	jne	.L2985
.L2938:
	cmpl	$2, 16(%r12)
	jle	.L2986
	movq	8(%r12), %r13
	subq	$16, %r13
.L2944:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fsync@PLT
	testl	%eax, %eax
	js	.L2987
.L2945:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2950
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L2988
.L2951:
	cmpb	$0, (%rax)
	jne	.L2989
.L2950:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2922
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2986:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L2944
	.p2align 4,,10
	.p2align 3
.L2985:
	movq	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1226(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2990
.L2942:
	testb	$5, (%rsi)
	je	.L2938
	leaq	.LC80(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2938
	.p2align 4,,10
	.p2align 3
.L2923:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2989:
	movq	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1228(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2991
.L2954:
	testb	$5, (%rsi)
	je	.L2950
	leaq	.LC80(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L2950
	.p2align 4,,10
	.p2align 3
.L2977:
	leaq	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2987:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2992
.L2946:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2993
.L2947:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L2945
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2945
	.p2align 4,,10
	.p2align 3
.L2933:
	cltq
	leaq	-560(%rbp), %r12
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L2994
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L2935:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2984:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2939
	.p2align 4,,10
	.p2align 3
.L2988:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L2951
	.p2align 4,,10
	.p2align 3
.L2979:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2983:
	leaq	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2990:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1226(%rip)
	mfence
	jmp	.L2942
	.p2align 4,,10
	.p2align 3
.L2991:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1228(%rip)
	mfence
	jmp	.L2954
	.p2align 4,,10
	.p2align 3
.L2994:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L2935
.L2993:
	movq	%rax, -568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-568(%rbp), %rcx
	jmp	.L2947
.L2992:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2946
.L2980:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7911:
	.size	_ZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC81:
	.string	"fs.sync.fstat"
	.text
	.p2align 4
	.type	_ZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3045
	cmpw	$1040, %cx
	jne	.L2996
.L3045:
	movq	23(%rdx), %rbx
.L2998:
	movl	16(%r12), %r14d
	cmpl	$1, %r14d
	jg	.L3057
	leaq	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3057:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3058
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L3059
	movq	8(%r12), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r13d
	jg	.L3002
.L3062:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3003:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$2, 16(%r12)
	movb	%al, -561(%rbp)
	movzbl	%al, %edx
	jg	.L3004
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3006
.L3063:
	leaq	.LC31(%rip), %rax
	cmpq	$0, 80(%r15)
	movq	%r15, 88(%r15)
	movq	%rax, 544(%r15)
	movl	$1, 536(%r15)
	jne	.L3060
	leaq	_ZN4node2fs9AfterStatEP7uv_fs_s(%rip), %rax
	leaq	88(%r15), %r14
	movl	%r13d, %edx
	movq	%rax, 80(%r15)
	movq	16(%r15), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fstat@PLT
	testl	%eax, %eax
	js	.L3008
	movq	16(%r15), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	addl	$1, 2156(%rax)
	movq	(%r15), %rax
	call	*120(%rax)
.L2995:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3061
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3059:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, %r13d
	jle	.L3062
.L3002:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3003
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L3063
.L3006:
	cmpl	$4, %r14d
	jne	.L3064
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3013
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L3065
.L3014:
	cmpb	$0, (%rax)
	jne	.L3066
.L3013:
	cmpl	$3, 16(%r12)
	jle	.L3067
	movq	8(%r12), %rax
	leaq	-24(%rax), %r14
.L3019:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r15
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movl	%r13d, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_fstat@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L3068
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3025
.L3024:
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L3069
.L3026:
	cmpb	$0, (%rax)
	jne	.L3070
.L3025:
	testl	%r13d, %r13d
	jne	.L3056
	cmpb	$0, -561(%rbp)
	movq	-416(%rbp), %rsi
	je	.L3031
	leaq	2304(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2336(%rbx), %rax
	testq	%rax, %rax
	je	.L3055
	movq	2304(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
.L3034:
	movq	(%r12), %rdx
	testq	%rax, %rax
	je	.L3033
	movq	(%rax), %rax
.L3037:
	movq	%rax, 24(%rdx)
.L3056:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L3067:
	movq	(%r12), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L3019
	.p2align 4,,10
	.p2align 3
.L3066:
	movq	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1023(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3071
.L3017:
	testb	$5, (%rsi)
	je	.L3013
	leaq	.LC81(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L2996:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2998
	.p2align 4,,10
	.p2align 3
.L3031:
	leaq	2264(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2296(%rbx), %rax
	testq	%rax, %rax
	je	.L3055
	movq	2264(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3070:
	movq	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1025(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3072
.L3029:
	testb	$5, (%rsi)
	je	.L3025
	leaq	.LC81(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3055:
	movq	(%r12), %rdx
.L3033:
	movq	16(%rdx), %rax
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3058:
	leaq	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3068:
	movq	352(%rbx), %r9
	movq	3280(%rbx), %rax
	movl	%r13d, %esi
	movq	%r9, %rdi
	movq	%r9, -584(%rbp)
	movq	%rax, -576(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-576(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-584(%rbp), %r9
	testb	%al, %al
	je	.L3073
.L3021:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r9, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3074
.L3022:
	movq	360(%rbx), %rax
	movq	-576(%rbp), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L3075
.L3023:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L3024
	jmp	.L3056
	.p2align 4,,10
	.p2align 3
.L3008:
	cltq
	leaq	-560(%rbp), %r13
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r15)
	movq	%r13, %rdi
	movq	$0, 192(%r15)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-552(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L3076
	movq	(%r15), %rax
	leaq	200(%r15), %rsi
	movq	%r15, %rdi
	call	*112(%rax)
.L3010:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L3065:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3069:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3060:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3064:
	leaq	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3071:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1023(%rip)
	mfence
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3072:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1025(%rip)
	mfence
	jmp	.L3029
	.p2align 4,,10
	.p2align 3
.L3076:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3010
.L3075:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3023
.L3074:
	movq	%rax, -584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-584(%rbp), %rcx
	jmp	.L3022
.L3073:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-584(%rbp), %r9
	jmp	.L3021
.L3061:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7904:
	.size	_ZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node2fs9AfterStatEP7uv_fs_s
	.type	_ZN4node2fs9AfterStatEP7uv_fs_s, @function
_ZN4node2fs9AfterStatEP7uv_fs_s:
.LFB7861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	leaq	-88(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	leaq	-80(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-96(%rbp), %rsi
	leaq	88(%rsi), %rax
	cmpq	%rax, %rbx
	jne	.L3083
	movq	-88(%rbp), %r8
	cmpq	$0, 88(%r8)
	js	.L3079
	movq	-88(%rbx), %rax
	addq	$200, %rsi
	movq	%r12, %rdi
	call	*112(%rax)
.L3080:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3084
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3079:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3083:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3084:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7861:
	.size	_ZN4node2fs9AfterStatEP7uv_fs_s, .-_ZN4node2fs9AfterStatEP7uv_fs_s
	.p2align 4
	.globl	_ZN4node2fs11AfterNoArgsEP7uv_fs_s
	.type	_ZN4node2fs11AfterNoArgsEP7uv_fs_s, @function
_ZN4node2fs11AfterNoArgsEP7uv_fs_s:
.LFB7860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	leaq	-88(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	leaq	-80(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-96(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %rbx
	jne	.L3091
	movq	-88(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L3087
	movq	16(%r12), %rdx
	movq	-88(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L3088:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3092
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3087:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3088
	.p2align 4,,10
	.p2align 3
.L3091:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3092:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7860:
	.size	_ZN4node2fs11AfterNoArgsEP7uv_fs_s, .-_ZN4node2fs11AfterNoArgsEP7uv_fs_s
	.p2align 4
	.globl	_ZN4node2fs12AfterIntegerEP7uv_fs_s
	.type	_ZN4node2fs12AfterIntegerEP7uv_fs_s, @function
_ZN4node2fs12AfterIntegerEP7uv_fs_s:
.LFB7862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-88(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	leaq	-80(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-96(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %rbx
	jne	.L3099
	movq	-88(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L3095
	movq	-88(%rbx), %rax
	movq	88(%rbx), %rsi
	movq	104(%rax), %r14
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%r14
.L3096:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3100
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3095:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3096
	.p2align 4,,10
	.p2align 3
.L3099:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7862:
	.size	_ZN4node2fs12AfterIntegerEP7uv_fs_s, .-_ZN4node2fs12AfterIntegerEP7uv_fs_s
	.section	.rodata.str1.1
.LC82:
	.string	"write"
.LC83:
	.string	"fs.sync.write"
.LC84:
	.string	"bytesWritten"
	.text
	.p2align 4
	.type	_ZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$664, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3178
	cmpw	$1040, %cx
	jne	.L3102
.L3178:
	movq	23(%rdx), %rbx
.L3104:
	movl	16(%r13), %r15d
	movq	352(%rbx), %r14
	cmpl	$3, %r15d
	jg	.L3208
	leaq	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3208:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3209
	movl	16(%r13), %edi
	testl	%edi, %edi
	jle	.L3210
	movq	8(%r13), %rdi
.L3107:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%r13)
	movl	%eax, -672(%rbp)
	jg	.L3108
	movq	0(%r13), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L3109:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L3176
	movq	%r12, %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	movq	%rax, -680(%rbp)
.L3110:
	cmpl	$3, 16(%r13)
	jg	.L3111
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L3112:
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	movl	%eax, %r12d
	movl	16(%r13), %eax
	cmpl	$1, %eax
	jg	.L3113
	movq	0(%r13), %rax
	movq	8(%rax), %r8
	addq	$88, %r8
	movq	%r8, %rsi
.L3116:
	movq	%rbx, %rdi
	movq	%r8, -664(%rbp)
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	-664(%rbp), %r8
	testq	%rax, %rax
	je	.L3211
	movl	%r12d, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%rax, -664(%rbp)
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movq	-664(%rbp), %r11
	testb	%al, %al
	movq	%rdx, %r15
	movl	%eax, %ebx
	je	.L3101
	movq	576(%r11), %r10
	leaq	.LC82(%rip), %rax
	movl	%r12d, 536(%r11)
	movq	%rax, 544(%r11)
	testq	%r10, %r10
	je	.L3212
	leaq	1(%r15), %rcx
	cmpq	568(%r11), %rcx
	ja	.L3213
.L3126:
	cmpl	$1, 16(%r13)
	movq	%rcx, 560(%r11)
	movb	$0, 540(%r11)
	jg	.L3130
	movq	0(%r13), %rax
	movq	8(%rax), %rcx
	addq	$88, %rcx
.L3131:
	movq	%r15, %rdx
	movq	%r10, %rsi
	xorl	%r9d, %r9d
	movl	%r12d, %r8d
	movq	%r14, %rdi
	movq	%r11, -664(%rbp)
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	-664(%rbp), %r11
	movq	%rax, %rsi
	movq	568(%r11), %rax
	leaq	1(%rsi), %rdx
	cmpq	%rax, %rdx
	ja	.L3142
	cmpq	%rax, %rsi
	ja	.L3143
	movq	576(%r11), %rax
	movq	%rsi, 560(%r11)
	movq	%r11, -664(%rbp)
	movb	$0, (%rax,%rsi)
	movq	576(%r11), %rdi
	call	uv_buf_init@PLT
	movq	-664(%rbp), %r11
	movq	%rax, -624(%rbp)
	cmpq	$0, 80(%r11)
	movq	%rdx, -616(%rbp)
	movq	%r11, 88(%r11)
	jne	.L3214
	leaq	_ZN4node2fs12AfterIntegerEP7uv_fs_s(%rip), %rax
	subq	$8, %rsp
	leaq	88(%r11), %r12
	movq	-680(%rbp), %r9
	movq	%rax, 80(%r11)
	movq	16(%r11), %rax
	movq	%r12, %rsi
	movl	$1, %r8d
	movl	-672(%rbp), %edx
	leaq	-624(%rbp), %rcx
	movq	%r11, -664(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rax
	pushq	%rax
	call	uv_fs_write@PLT
	popq	%rcx
	movq	-664(%rbp), %r11
	testl	%eax, %eax
	popq	%rsi
	js	.L3135
	movq	16(%r11), %rax
	movq	%r13, %rsi
	movq	%r11, %rdi
	addl	$1, 2156(%rax)
	movq	(%r11), %rax
	call	*120(%rax)
	.p2align 4,,10
	.p2align 3
.L3101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3215
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3210:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3107
	.p2align 4,,10
	.p2align 3
.L3113:
	movq	8(%r13), %rsi
	leaq	-8(%rsi), %r8
	cmpl	$4, %eax
	jle	.L3216
	subq	$32, %rsi
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3111:
	movq	8(%r13), %rax
	leaq	-24(%rax), %rsi
	jmp	.L3112
	.p2align 4,,10
	.p2align 3
.L3108:
	movq	8(%r13), %rax
	leaq	-16(%rax), %r12
	jmp	.L3109
	.p2align 4,,10
	.p2align 3
.L3211:
	movq	(%r8), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L3118
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3118
	testl	$-5, %r12d
	je	.L3119
.L3121:
	cmpl	$3, %r12d
	je	.L3217
	.p2align 4,,10
	.p2align 3
.L3118:
	cmpl	$6, %r15d
	je	.L3218
.L3169:
	leaq	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3218:
	leaq	-584(%rbp), %r15
	movdqa	.LC11(%rip), %xmm0
	movq	$0, -72(%rbp)
	movq	%r15, -592(%rbp)
	movb	$0, -584(%rbp)
	movaps	%xmm0, -608(%rbp)
.L3173:
	movl	%r12d, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	testb	%al, %al
	je	.L3138
	leaq	1(%rdx), %rsi
	leaq	-608(%rbp), %rdi
	movq	%rdx, -664(%rbp)
	call	_ZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEm
	cmpl	$1, 16(%r13)
	movq	-664(%rbp), %rdx
	jle	.L3139
	movq	8(%r13), %rax
	leaq	-8(%rax), %rcx
.L3141:
	movq	-592(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	%r12d, %r8d
	movq	%r14, %rdi
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	%rax, %rsi
	movq	-600(%rbp), %rax
	leaq	1(%rsi), %rdx
	cmpq	%rax, %rdx
	ja	.L3142
	cmpq	%rax, %rsi
	ja	.L3143
	movq	-592(%rbp), %rax
	movq	%rsi, -608(%rbp)
	movb	$0, (%rax,%rsi)
	movq	-592(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L3137:
	movq	%r9, %rdi
	call	uv_buf_init@PLT
	movq	%rax, -624(%rbp)
	movq	%rdx, -616(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3219
.L3145:
	cmpl	$5, 16(%r13)
	jg	.L3149
	movq	0(%r13), %rax
	movq	8(%rax), %r14
	leaq	88(%r14), %rax
	movq	%rax, -664(%rbp)
.L3150:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	360(%rbx), %rax
	subq	$8, %rsp
	movl	-672(%rbp), %edx
	movq	-680(%rbp), %r9
	leaq	-624(%rbp), %rcx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	2360(%rax), %rdi
	pushq	$0
	call	uv_fs_write@PLT
	movl	%eax, %r14d
	popq	%rax
	popq	%rdx
	testl	%r14d, %r14d
	js	.L3220
.L3151:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movslq	%r14d, %rbx
	cmpb	$0, (%rax)
	jne	.L3221
.L3156:
	movq	0(%r13), %rax
	movq	-592(%rbp), %rdi
	salq	$32, %rbx
	movq	%rbx, 24(%rax)
	cmpq	%r15, %rdi
	je	.L3162
.L3207:
	testq	%rdi, %rdi
	je	.L3162
	call	free@PLT
.L3162:
	movq	%r12, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L3101
	.p2align 4,,10
	.p2align 3
.L3102:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L3104
	.p2align 4,,10
	.p2align 3
.L3213:
	leaq	584(%r11), %rdx
	movl	$0, %r8d
	movl	$0, %eax
	cmpq	%rdx, %r10
	movq	%r8, %rdi
	movq	%rdx, -704(%rbp)
	cmove	%eax, %ebx
	cmovne	%r10, %rdi
	testq	%rcx, %rcx
	je	.L3222
	movq	%rcx, %rsi
	movq	%r11, -696(%rbp)
	movq	%rcx, -688(%rbp)
	movq	%rdi, -664(%rbp)
	call	realloc@PLT
	movq	-664(%rbp), %rdi
	movq	-688(%rbp), %rcx
	testq	%rax, %rax
	movq	-696(%rbp), %r11
	movq	%rax, %r10
	je	.L3223
.L3129:
	movq	%r10, 576(%r11)
	movq	%rcx, 568(%r11)
	testb	%bl, %bl
	jne	.L3126
	movq	560(%r11), %rdx
	testq	%rdx, %rdx
	je	.L3126
	movq	-704(%rbp), %rsi
	movq	%r10, %rdi
	movq	%rcx, -688(%rbp)
	movq	%r11, -664(%rbp)
	call	memcpy@PLT
	movq	-664(%rbp), %r11
	movq	-688(%rbp), %rcx
	movq	576(%r11), %r10
	jmp	.L3126
	.p2align 4,,10
	.p2align 3
.L3130:
	movq	8(%r13), %rax
	leaq	-8(%rax), %rcx
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3209:
	leaq	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3138:
	movq	-592(%rbp), %rdi
	leaq	-512(%rbp), %r12
	cmpq	%r15, %rdi
	jne	.L3207
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3139:
	movq	0(%r13), %rax
	movq	8(%rax), %rcx
	addq	$88, %rcx
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L3119:
	movq	%r8, %rdi
	movq	%r8, -664(%rbp)
	call	_ZNK2v86String17IsExternalOneByteEv@PLT
	movq	-664(%rbp), %r8
	testb	%al, %al
	je	.L3121
	movq	%r8, %rdi
	movq	%r8, -696(%rbp)
	call	_ZNK2v86String32GetExternalOneByteStringResourceEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rdi, -688(%rbp)
	call	*48(%rax)
	movq	-688(%rbp), %rdi
	movq	%rax, -664(%rbp)
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-664(%rbp), %r9
	movq	-696(%rbp), %r8
	movq	%rax, %rsi
.L3122:
	cmpl	$6, %r15d
	jne	.L3169
	movdqa	.LC11(%rip), %xmm0
	leaq	-584(%rbp), %r15
	movq	$0, -72(%rbp)
	movq	%r15, -592(%rbp)
	movb	$0, -584(%rbp)
	movaps	%xmm0, -608(%rbp)
	testq	%r9, %r9
	jne	.L3137
	jmp	.L3173
	.p2align 4,,10
	.p2align 3
.L3217:
	movq	%r8, %rdi
	movq	%r8, -664(%rbp)
	call	_ZNK2v86String10IsExternalEv@PLT
	movq	-664(%rbp), %r8
	testb	%al, %al
	je	.L3118
	movq	(%r8), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$2, %ax
	jne	.L3123
	movq	15(%rdx), %rdi
.L3124:
	movq	(%rdi), %rax
	movq	%r8, -696(%rbp)
	movq	%rdi, -688(%rbp)
	call	*48(%rax)
	movq	-688(%rbp), %rdi
	movq	%rax, -664(%rbp)
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	-664(%rbp), %r9
	movq	-696(%rbp), %r8
	leaq	(%rax,%rax), %rsi
	jmp	.L3122
	.p2align 4,,10
	.p2align 3
.L3149:
	movq	8(%r13), %rax
	subq	$40, %rax
	movq	%rax, -664(%rbp)
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1928(%rip), %rdi
	testq	%rdi, %rdi
	je	.L3224
.L3158:
	testb	$5, (%rdi)
	je	.L3156
	leaq	.LC84(%rip), %rax
	leaq	-641(%rbp), %rcx
	movb	$3, -641(%rbp)
	leaq	-640(%rbp), %rdx
	leaq	-632(%rbp), %r8
	movq	%rax, -640(%rbp)
	leaq	.LC83(%rip), %rsi
	movq	%rbx, -632(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	jmp	.L3156
	.p2align 4,,10
	.p2align 3
.L3219:
	movq	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1925(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3225
.L3147:
	testb	$5, (%rsi)
	je	.L3145
	leaq	.LC83(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3145
	.p2align 4,,10
	.p2align 3
.L3220:
	movq	352(%rbx), %r8
	movq	3280(%rbx), %rax
	movl	%r14d, %esi
	movq	%r8, %rdi
	movq	%r8, -680(%rbp)
	movq	%rax, -672(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-672(%rbp), %rsi
	movq	-664(%rbp), %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-680(%rbp), %r8
	testb	%al, %al
	je	.L3226
.L3152:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC82(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3227
.L3153:
	movq	360(%rbx), %rax
	movq	-672(%rbp), %rsi
	movq	-664(%rbp), %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L3151
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3151
	.p2align 4,,10
	.p2align 3
.L3135:
	movq	$0, 192(%r11)
	cltq
	movq	%r12, %rdi
	movq	%rax, 176(%r11)
	call	_ZN4node2fs12AfterIntegerEP7uv_fs_s
	jmp	.L3101
	.p2align 4,,10
	.p2align 3
.L3176:
	movq	$-1, -680(%rbp)
	jmp	.L3110
	.p2align 4,,10
	.p2align 3
.L3225:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1925(%rip)
	mfence
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3224:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rdi
	movq	%rax, _ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1928(%rip)
	mfence
	jmp	.L3158
	.p2align 4,,10
	.p2align 3
.L3142:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3212:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3143:
	leaq	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3222:
	movq	%rcx, -688(%rbp)
	movq	%r11, -664(%rbp)
	call	free@PLT
	movq	-664(%rbp), %r11
	movq	-688(%rbp), %rcx
	xorl	%r10d, %r10d
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3214:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3223:
	movq	%r11, -688(%rbp)
	movq	%rdi, -696(%rbp)
	movq	%rcx, -664(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-664(%rbp), %rcx
	movq	-696(%rbp), %rdi
	movq	%rcx, %rsi
	call	realloc@PLT
	movq	-664(%rbp), %rcx
	movq	-688(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L3129
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3227:
	movq	%rax, -680(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-680(%rbp), %rcx
	jmp	.L3153
.L3226:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-680(%rbp), %r8
	jmp	.L3152
.L3123:
	movq	%r8, %rdi
	movq	%r8, -664(%rbp)
	call	_ZNK2v86String29GetExternalStringResourceSlowEv@PLT
	movq	-664(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L3124
.L3215:
	call	__stack_chk_fail@PLT
.L3216:
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L3116
	.cfi_endproc
.LFE7934:
	.size	_ZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC85:
	.string	"fs.sync.read"
.LC86:
	.string	"bytesRead"
	.text
	.p2align 4
	.type	_ZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3285
	cmpw	$1040, %cx
	jne	.L3229
.L3285:
	movq	23(%rdx), %r15
.L3231:
	movl	16(%r13), %r14d
	cmpl	$4, %r14d
	jg	.L3294
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3294:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3295
	movl	16(%r13), %edi
	testl	%edi, %edi
	jle	.L3296
	movq	8(%r13), %rdi
.L3234:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r13)
	movl	%eax, -568(%rbp)
	jg	.L3235
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3236:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L3297
	cmpl	$1, 16(%r13)
	jg	.L3238
	movq	0(%r13), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L3239:
	movq	%r12, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	cmpl	$2, 16(%r13)
	movq	%rax, %rbx
	jg	.L3240
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3241:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3298
	cmpl	$2, 16(%r13)
	jg	.L3243
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3244:
	call	_ZNK2v85Int325ValueEv@PLT
	movslq	%eax, %r12
	cmpq	%r12, %rbx
	jbe	.L3299
	cmpl	$3, 16(%r13)
	jg	.L3246
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3247:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3300
	cmpl	$3, 16(%r13)
	jg	.L3249
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3250:
	call	_ZNK2v85Int325ValueEv@PLT
	subq	%r12, %rbx
	movl	%eax, %esi
	cltq
	cmpq	%rbx, %rax
	ja	.L3301
	cmpl	$4, 16(%r13)
	jg	.L3252
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3253:
	movl	%esi, -580(%rbp)
	call	_ZNK2v85Value8IsNumberEv@PLT
	movl	-580(%rbp), %esi
	testb	%al, %al
	je	.L3302
	cmpl	$4, 16(%r13)
	jg	.L3255
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3256:
	movl	%esi, -580(%rbp)
	call	_ZNK2v87Integer5ValueEv@PLT
	movq	-576(%rbp), %rdi
	movl	-580(%rbp), %esi
	movq	%rax, %rbx
	addq	%r12, %rdi
	call	uv_buf_init@PLT
	cmpl	$5, 16(%r13)
	movq	%rax, -528(%rbp)
	movq	%rdx, -520(%rbp)
	jg	.L3257
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3259
.L3305:
	leaq	.LC18(%rip), %rax
	cmpq	$0, 80(%r12)
	movq	%r12, 88(%r12)
	movq	%rax, 544(%r12)
	movl	$1, 536(%r12)
	jne	.L3303
	leaq	_ZN4node2fs12AfterIntegerEP7uv_fs_s(%rip), %rax
	subq	$8, %rsp
	movq	%rbx, %r9
	movl	-568(%rbp), %edx
	movq	%rax, 80(%r12)
	movq	16(%r12), %rax
	leaq	88(%r12), %r14
	leaq	-528(%rbp), %rcx
	movq	%r14, %rsi
	movl	$1, %r8d
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rax
	pushq	%rax
	call	uv_fs_read@PLT
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	js	.L3261
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	addl	$1, 2156(%rax)
	movq	(%r12), %rax
	call	*120(%rax)
.L3228:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3304
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3296:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3235:
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3236
	.p2align 4,,10
	.p2align 3
.L3238:
	movq	8(%r13), %rax
	leaq	-8(%rax), %r12
	jmp	.L3239
	.p2align 4,,10
	.p2align 3
.L3240:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3243:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3244
	.p2align 4,,10
	.p2align 3
.L3246:
	movq	8(%r13), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3247
	.p2align 4,,10
	.p2align 3
.L3249:
	movq	8(%r13), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3250
	.p2align 4,,10
	.p2align 3
.L3252:
	movq	8(%r13), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3255:
	movq	8(%r13), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3256
	.p2align 4,,10
	.p2align 3
.L3257:
	movq	8(%r13), %rax
	movq	%r15, %rdi
	leaq	-40(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L3305
.L3259:
	cmpl	$7, %r14d
	jne	.L3306
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3265
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L3307
.L3266:
	cmpb	$0, (%rax)
	jne	.L3308
.L3265:
	cmpl	$6, 16(%r13)
	jg	.L3270
	movq	0(%r13), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L3271:
	movq	%r15, %rdi
	leaq	-512(%rbp), %r14
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	subq	$8, %rsp
	movq	%rbx, %r9
	movq	%r14, %rsi
	movq	360(%r15), %rax
	movl	-568(%rbp), %edx
	leaq	-528(%rbp), %rcx
	movl	$1, %r8d
	movq	2360(%rax), %rdi
	pushq	$0
	call	uv_fs_read@PLT
	movslq	%eax, %rbx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	js	.L3309
.L3272:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3310
.L3277:
	movq	0(%r13), %rax
	salq	$32, %rbx
	movq	%r14, %rdi
	movq	%rbx, 24(%rax)
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L3228
	.p2align 4,,10
	.p2align 3
.L3229:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L3231
	.p2align 4,,10
	.p2align 3
.L3301:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3270:
	movq	8(%r13), %rax
	leaq	-48(%rax), %r12
	jmp	.L3271
	.p2align 4,,10
	.p2align 3
.L3295:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3297:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3298:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3299:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3310:
	movq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1983(%rip), %rdi
	testq	%rdi, %rdi
	je	.L3311
.L3279:
	testb	$5, (%rdi)
	je	.L3277
	leaq	.LC86(%rip), %rax
	leaq	-545(%rbp), %rcx
	movb	$3, -545(%rbp)
	leaq	-544(%rbp), %rdx
	leaq	-536(%rbp), %r8
	movq	%rax, -544(%rbp)
	leaq	.LC85(%rip), %rsi
	movq	%rbx, -536(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	jmp	.L3277
	.p2align 4,,10
	.p2align 3
.L3308:
	movq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1980(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3312
.L3269:
	testb	$5, (%rsi)
	je	.L3265
	leaq	.LC85(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3300:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3303:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3306:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3311:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rdi
	movq	%rax, _ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1983(%rip)
	mfence
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3312:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1980(%rip)
	mfence
	jmp	.L3269
	.p2align 4,,10
	.p2align 3
.L3302:
	leaq	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3309:
	movq	352(%r15), %r8
	movq	3280(%r15), %rax
	movl	%ebx, %esi
	movq	%r8, %rdi
	movq	%r8, -576(%rbp)
	movq	%rax, -568(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-568(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%r15), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-576(%rbp), %r8
	testb	%al, %al
	je	.L3313
.L3273:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3314
.L3274:
	movq	360(%r15), %rax
	movq	-568(%rbp), %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L3272
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3272
	.p2align 4,,10
	.p2align 3
.L3261:
	cltq
	movq	%r14, %rdi
	movq	$0, 192(%r12)
	movq	%rax, 176(%r12)
	call	_ZN4node2fs12AfterIntegerEP7uv_fs_s
	jmp	.L3228
	.p2align 4,,10
	.p2align 3
.L3307:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L3266
.L3314:
	movq	%rax, -576(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-576(%rbp), %rcx
	jmp	.L3274
.L3313:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-576(%rbp), %r8
	jmp	.L3273
.L3304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7935:
	.size	_ZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3373
	cmpw	$1040, %cx
	jne	.L3316
.L3373:
	movq	23(%rdx), %r15
.L3318:
	movl	16(%r13), %r14d
	cmpl	$3, %r14d
	jg	.L3384
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3384:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3385
	movl	16(%r13), %edi
	testl	%edi, %edi
	jle	.L3386
	movq	8(%r13), %rdi
.L3321:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r13)
	movl	%eax, -568(%rbp)
	jg	.L3322
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3323:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L3387
	cmpl	$1, 16(%r13)
	jg	.L3325
	movq	0(%r13), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L3326:
	movq	%r12, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -576(%rbp)
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	cmpl	$2, 16(%r13)
	movq	%rax, %r12
	jg	.L3327
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3328:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3388
	cmpl	$2, 16(%r13)
	jg	.L3330
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3331:
	call	_ZNK2v85Int325ValueEv@PLT
	movslq	%eax, %rbx
	cmpq	%rbx, %r12
	jb	.L3389
	cmpl	$3, 16(%r13)
	jg	.L3333
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3334:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3390
	cmpl	$3, 16(%r13)
	jg	.L3336
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3337:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	%r12, %rdx
	movl	%eax, -580(%rbp)
	subq	%rbx, %rdx
	cltq
	cmpq	%rax, %rdx
	jb	.L3391
	cmpq	%rax, %r12
	jb	.L3392
	addq	%rbx, %rax
	jc	.L3393
	cmpl	$4, 16(%r13)
	jle	.L3394
	movq	8(%r13), %rax
	leaq	-32(%rax), %r12
.L3342:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L3372
	movq	%r12, %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	movq	%rax, %r12
.L3343:
	movq	-576(%rbp), %rdi
	movl	-580(%rbp), %esi
	addq	%rbx, %rdi
	call	uv_buf_init@PLT
	cmpl	$5, 16(%r13)
	movq	%rax, -528(%rbp)
	movq	%rdx, -520(%rbp)
	jg	.L3344
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3346
.L3397:
	leaq	.LC82(%rip), %rax
	cmpq	$0, 80(%rbx)
	movq	%rbx, 88(%rbx)
	movq	%rax, 544(%rbx)
	movl	$1, 536(%rbx)
	jne	.L3395
	leaq	_ZN4node2fs12AfterIntegerEP7uv_fs_s(%rip), %rax
	subq	$8, %rsp
	leaq	88(%rbx), %r14
	movq	%r12, %r9
	movq	%rax, 80(%rbx)
	movq	16(%rbx), %rax
	movq	%r14, %rsi
	movl	$1, %r8d
	movl	-568(%rbp), %edx
	leaq	-528(%rbp), %rcx
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rax
	pushq	%rax
	call	uv_fs_write@PLT
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	js	.L3348
	movq	16(%rbx), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addl	$1, 2156(%rax)
	movq	(%rbx), %rax
	call	*120(%rax)
.L3315:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3396
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3386:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3321
	.p2align 4,,10
	.p2align 3
.L3322:
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3323
	.p2align 4,,10
	.p2align 3
.L3325:
	movq	8(%r13), %rax
	leaq	-8(%rax), %r12
	jmp	.L3326
	.p2align 4,,10
	.p2align 3
.L3327:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3328
	.p2align 4,,10
	.p2align 3
.L3330:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3331
	.p2align 4,,10
	.p2align 3
.L3333:
	movq	8(%r13), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3334
	.p2align 4,,10
	.p2align 3
.L3336:
	movq	8(%r13), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3337
	.p2align 4,,10
	.p2align 3
.L3394:
	movq	0(%r13), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3316:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L3318
	.p2align 4,,10
	.p2align 3
.L3344:
	movq	8(%r13), %rax
	movq	%r15, %rdi
	leaq	-40(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L3397
.L3346:
	cmpl	$7, %r14d
	jne	.L3398
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3399
.L3352:
	cmpl	$6, 16(%r13)
	jg	.L3356
	movq	0(%r13), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L3357:
	movq	%r15, %rdi
	leaq	-512(%rbp), %rbx
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	subq	$8, %rsp
	movq	%r12, %r9
	movq	%rbx, %rsi
	movq	360(%r15), %rax
	movl	-568(%rbp), %edx
	leaq	-528(%rbp), %rcx
	movl	$1, %r8d
	movq	2360(%rax), %rdi
	pushq	$0
	call	uv_fs_write@PLT
	movslq	%eax, %r12
	popq	%rax
	popq	%rdx
	testl	%r12d, %r12d
	js	.L3400
.L3358:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3401
.L3363:
	movq	0(%r13), %rax
	salq	$32, %r12
	movq	%rbx, %rdi
	movq	%r12, 24(%rax)
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L3315
	.p2align 4,,10
	.p2align 3
.L3385:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3356:
	movq	8(%r13), %rax
	leaq	-48(%rax), %r14
	jmp	.L3357
	.p2align 4,,10
	.p2align 3
.L3387:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3388:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3389:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3390:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3401:
	movq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1784(%rip), %rdi
	testq	%rdi, %rdi
	je	.L3402
.L3365:
	testb	$5, (%rdi)
	je	.L3363
	leaq	.LC84(%rip), %rax
	leaq	-545(%rbp), %rcx
	movb	$3, -545(%rbp)
	leaq	-544(%rbp), %rdx
	leaq	-536(%rbp), %r8
	movq	%rax, -544(%rbp)
	leaq	.LC83(%rip), %rsi
	movq	%r12, -536(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3399:
	movq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1781(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3403
.L3354:
	testb	$5, (%rsi)
	je	.L3352
	leaq	.LC83(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3352
	.p2align 4,,10
	.p2align 3
.L3391:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3403:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1781(%rip)
	mfence
	jmp	.L3354
	.p2align 4,,10
	.p2align 3
.L3402:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rdi
	movq	%rax, _ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1784(%rip)
	mfence
	jmp	.L3365
	.p2align 4,,10
	.p2align 3
.L3392:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3393:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3400:
	movq	352(%r15), %r8
	movq	3280(%r15), %rax
	movl	%r12d, %esi
	movq	%r8, %rdi
	movq	%r8, -576(%rbp)
	movq	%rax, -568(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-568(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%r15), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-576(%rbp), %r8
	testb	%al, %al
	je	.L3404
.L3359:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC82(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3405
.L3360:
	movq	360(%r15), %rax
	movq	-568(%rbp), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L3358
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3358
	.p2align 4,,10
	.p2align 3
.L3348:
	movq	$0, 192(%rbx)
	cltq
	movq	%r14, %rdi
	movq	%rax, 176(%rbx)
	call	_ZN4node2fs12AfterIntegerEP7uv_fs_s
	jmp	.L3315
	.p2align 4,,10
	.p2align 3
.L3395:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3398:
	leaq	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_8(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3372:
	movq	$-1, %r12
	jmp	.L3343
.L3405:
	movq	%rax, -576(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-576(%rbp), %rcx
	jmp	.L3360
.L3404:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-576(%rbp), %r8
	jmp	.L3359
.L3396:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7932:
	.size	_ZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC87:
	.string	"fs.sync.open"
	.text
	.p2align 4
	.type	_ZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7929:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3448
	cmpw	$1040, %cx
	jne	.L3407
.L3448:
	movq	23(%rdx), %rbx
.L3409:
	movl	16(%r13), %r14d
	cmpl	$2, %r14d
	jg	.L3463
	leaq	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3463:
	movq	8(%r13), %rdx
	movq	352(%rbx), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L3464
	cmpl	$1, 16(%r13)
	jle	.L3465
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
.L3412:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3466
	cmpl	$1, 16(%r13)
	jg	.L3414
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3415:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%r13)
	movl	%eax, %r12d
	jg	.L3416
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3417:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3467
	cmpl	$2, 16(%r13)
	jg	.L3419
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3420:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$3, 16(%r13)
	movl	%eax, -1560(%rbp)
	jg	.L3421
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3423
.L3470:
	leaq	.LC16(%rip), %rax
	cmpq	$0, 80(%r15)
	movq	%r15, 88(%r15)
	movq	%rax, 544(%r15)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r15)
	jne	.L3468
	leaq	_ZN4node2fs12AfterIntegerEP7uv_fs_s(%rip), %rax
	leaq	88(%r15), %r14
	movl	%r12d, %ecx
	movl	-1560(%rbp), %r8d
	movq	%rax, 80(%r15)
	movq	16(%r15), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r9
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_open@PLT
	testl	%eax, %eax
	js	.L3425
	movq	16(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	addl	$1, 2156(%rax)
	movq	(%r15), %rax
	call	*120(%rax)
.L3426:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3406
	testq	%rdi, %rdi
	je	.L3406
	call	free@PLT
.L3406:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3469
	addq	$1528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3465:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3412
	.p2align 4,,10
	.p2align 3
.L3416:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3417
	.p2align 4,,10
	.p2align 3
.L3414:
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3415
	.p2align 4,,10
	.p2align 3
.L3421:
	movq	8(%r13), %rax
	movq	%rbx, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L3470
.L3423:
	cmpl	$5, %r14d
	jne	.L3471
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3472
.L3429:
	cmpl	$4, 16(%r13)
	movq	-1088(%rbp), %rdx
	jle	.L3473
	movq	8(%r13), %rax
	leaq	-32(%rax), %r14
.L3434:
	movq	%rbx, %rdi
	movq	%rdx, -1568(%rbp)
	leaq	-1552(%rbp), %r15
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movl	%r12d, %ecx
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movq	360(%rbx), %rax
	movl	-1560(%rbp), %r8d
	movq	-1568(%rbp), %rdx
	movq	2360(%rax), %rdi
	call	uv_fs_open@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L3474
.L3435:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3475
.L3440:
	movq	0(%r13), %rax
	salq	$32, %r12
	movq	%r15, %rdi
	movq	%r12, 24(%rax)
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L3426
	.p2align 4,,10
	.p2align 3
.L3419:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3420
	.p2align 4,,10
	.p2align 3
.L3473:
	movq	0(%r13), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L3434
	.p2align 4,,10
	.p2align 3
.L3425:
	movq	$0, 192(%r15)
	cltq
	movq	%r14, %rdi
	movq	%rax, 176(%r15)
	call	_ZN4node2fs12AfterIntegerEP7uv_fs_s
	jmp	.L3426
	.p2align 4,,10
	.p2align 3
.L3407:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L3409
	.p2align 4,,10
	.p2align 3
.L3472:
	movq	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1660(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3476
.L3431:
	testb	$5, (%rsi)
	je	.L3429
	leaq	.LC87(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3429
	.p2align 4,,10
	.p2align 3
.L3475:
	movq	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1663(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3477
.L3442:
	testb	$5, (%rsi)
	je	.L3440
	leaq	.LC87(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3440
	.p2align 4,,10
	.p2align 3
.L3464:
	leaq	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3466:
	leaq	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3467:
	leaq	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3474:
	movq	352(%rbx), %r8
	movq	3280(%rbx), %rax
	movl	%r12d, %esi
	movq	%r8, %rdi
	movq	%r8, -1568(%rbp)
	movq	%rax, -1560(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-1560(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-1568(%rbp), %r8
	testb	%al, %al
	je	.L3478
.L3436:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3479
.L3437:
	movq	360(%rbx), %rax
	movq	-1560(%rbp), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L3435
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3435
	.p2align 4,,10
	.p2align 3
.L3468:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3471:
	leaq	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3477:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1663(%rip)
	mfence
	jmp	.L3442
	.p2align 4,,10
	.p2align 3
.L3476:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1660(%rip)
	mfence
	jmp	.L3431
.L3479:
	movq	%rax, -1568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1568(%rbp), %rcx
	jmp	.L3437
.L3478:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-1568(%rbp), %r8
	jmp	.L3436
.L3469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7929:
	.size	_ZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL0:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL0
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3536
	cmpw	$1040, %cx
	jne	.L3481
.L3536:
	movq	23(%rdx), %r15
.L3483:
	movl	16(%r12), %eax
	movl	%eax, -16964(%rbp)
	cmpl	$2, %eax
	jg	.L3554
	leaq	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3554:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3555
	movl	16(%r12), %edi
	testl	%edi, %edi
	jle	.L3556
	movq	8(%r12), %rdi
.L3486:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, -16968(%rbp)
	jg	.L3487
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3488:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L3557
	movl	16(%r12), %eax
	cmpl	$1, %eax
	jg	.L3490
	movq	(%r12), %rax
	movq	8(%rax), %r14
	leaq	88(%r14), %rax
	movq	%rax, -16960(%rbp)
	movq	%rax, %r13
.L3493:
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L3535
	movq	%r13, %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	movq	%rax, -16984(%rbp)
.L3494:
	movq	-16960(%rbp), %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movdqa	.LC88(%rip), %xmm0
	leaq	-16440(%rbp), %rcx
	movq	$0, -16440(%rbp)
	movq	%rcx, -16976(%rbp)
	movl	%eax, %ebx
	movq	%rcx, -16448(%rbp)
	movq	$0, -16432(%rbp)
	movaps	%xmm0, -16464(%rbp)
	cmpl	$1024, %eax
	ja	.L3558
	movq	%rbx, -16464(%rbp)
	testq	%rbx, %rbx
	je	.L3499
.L3498:
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
.L3505:
	movq	3280(%r15), %rsi
	movq	-16960(%rbp), %rdi
	movl	%r13d, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3559
	movq	%r14, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L3560
.L3503:
	movq	%r14, %rdi
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -16952(%rbp)
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpq	-16464(%rbp), %rbx
	movq	-16952(%rbp), %rsi
	movq	%rax, %rdi
	jnb	.L3561
	call	uv_buf_init@PLT
	salq	$4, %rbx
	addq	-16448(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
	leal	1(%r13), %ebx
	movq	%rbx, %r13
	cmpq	-16464(%rbp), %rbx
	jb	.L3505
.L3499:
	cmpl	$3, 16(%r12)
	jle	.L3500
	movq	8(%r12), %rax
	leaq	-24(%rax), %rsi
.L3506:
	movq	%r15, %rdi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3507
	leaq	.LC82(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-16464(%rbp), %r8
	movl	$1, 536(%r13)
	movq	-16448(%rbp), %rcx
	jne	.L3562
	leaq	_ZN4node2fs12AfterIntegerEP7uv_fs_s(%rip), %rax
	subq	$8, %rsp
	movl	-16968(%rbp), %edx
	movq	-16984(%rbp), %r9
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	88(%r13), %r14
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rax
	pushq	%rax
	call	uv_fs_write@PLT
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	js	.L3509
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L3510:
	movq	-16448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3480
	cmpq	-16976(%rbp), %rdi
	je	.L3480
	call	free@PLT
.L3480:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3563
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3556:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3486
	.p2align 4,,10
	.p2align 3
.L3487:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3488
	.p2align 4,,10
	.p2align 3
.L3490:
	movq	8(%r12), %r13
	leaq	-8(%r13), %rdx
	movq	%rdx, -16960(%rbp)
	cmpl	$2, %eax
	je	.L3564
	subq	$16, %r13
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3558:
	movq	%rbx, %r13
	salq	$4, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L3529
	movq	%rax, -16448(%rbp)
	movq	%rbx, -16456(%rbp)
.L3497:
	movq	%rbx, -16464(%rbp)
	jmp	.L3498
	.p2align 4,,10
	.p2align 3
.L3559:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	%r14, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	jne	.L3503
	.p2align 4,,10
	.p2align 3
.L3560:
	leaq	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3561:
	leaq	_ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3481:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L3483
	.p2align 4,,10
	.p2align 3
.L3555:
	leaq	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3557:
	leaq	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3500:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L3506
	.p2align 4,,10
	.p2align 3
.L3507:
	cmpl	$5, -16964(%rbp)
	jne	.L3565
	movq	$0, -16472(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3566
.L3513:
	cmpl	$4, 16(%r12)
	movq	-16464(%rbp), %rbx
	movq	-16448(%rbp), %rcx
	jg	.L3517
	movq	(%r12), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L3518:
	movq	%r15, %rdi
	movq	%rcx, -16952(%rbp)
	leaq	-16912(%rbp), %r13
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	360(%r15), %rax
	subq	$8, %rsp
	movl	-16968(%rbp), %edx
	movq	-16984(%rbp), %r9
	movq	-16952(%rbp), %rcx
	movl	%ebx, %r8d
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	pushq	$0
	call	uv_fs_write@PLT
	movslq	%eax, %rbx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	js	.L3567
.L3519:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3568
.L3524:
	movq	(%r12), %rax
	salq	$32, %rbx
	movq	%r13, %rdi
	movq	%rbx, 24(%rax)
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L3510
.L3529:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3569
	movq	-16464(%rbp), %rdx
	movq	%rax, -16448(%rbp)
	movq	%rbx, -16456(%rbp)
	testq	%rdx, %rdx
	je	.L3497
	movq	-16976(%rbp), %rsi
	salq	$4, %rdx
	call	memcpy@PLT
	movq	%rbx, -16464(%rbp)
	jmp	.L3498
.L3569:
	leaq	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3535:
	movq	$-1, -16984(%rbp)
	jmp	.L3494
.L3517:
	movq	8(%r12), %rax
	leaq	-32(%rax), %r14
	jmp	.L3518
.L3568:
	movq	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1829(%rip), %rdi
	testq	%rdi, %rdi
	je	.L3570
.L3526:
	testb	$5, (%rdi)
	je	.L3524
	leaq	.LC84(%rip), %rax
	leaq	-16929(%rbp), %rcx
	movb	$3, -16929(%rbp)
	leaq	-16928(%rbp), %rdx
	leaq	-16920(%rbp), %r8
	movq	%rax, -16928(%rbp)
	leaq	.LC83(%rip), %rsi
	movq	%rbx, -16920(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	jmp	.L3524
.L3566:
	movq	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1826(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3571
.L3515:
	testb	$5, (%rsi)
	je	.L3513
	leaq	.LC83(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3513
.L3567:
	movq	352(%r15), %r8
	movq	3280(%r15), %rax
	movl	%ebx, %esi
	movq	%r8, %rdi
	movq	%r8, -16960(%rbp)
	movq	%rax, -16952(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-16952(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%r15), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-16960(%rbp), %r8
	testb	%al, %al
	je	.L3572
.L3520:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC82(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3573
.L3521:
	movq	360(%r15), %rax
	movq	-16952(%rbp), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L3519
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3519
.L3509:
	movq	$0, 192(%r13)
	cltq
	movq	%r14, %rdi
	movq	%rax, 176(%r13)
	call	_ZN4node2fs12AfterIntegerEP7uv_fs_s
	jmp	.L3510
.L3565:
	leaq	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3570:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rdi
	movq	%rax, _ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1829(%rip)
	mfence
	jmp	.L3526
.L3571:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1826(%rip)
	mfence
	jmp	.L3515
.L3562:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3573:
	movq	%rax, -16960(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-16960(%rbp), %rcx
	jmp	.L3521
.L3572:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-16960(%rbp), %r8
	jmp	.L3520
.L3564:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L3493
.L3563:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7933:
	.size	_ZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL1:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL1
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3630
	cmpw	$1040, %cx
	jne	.L3575
.L3630:
	movq	23(%rdx), %r15
.L3577:
	movl	16(%r12), %eax
	movl	%eax, -16964(%rbp)
	cmpl	$2, %eax
	jg	.L3648
	leaq	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3648:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3649
	movl	16(%r12), %edi
	testl	%edi, %edi
	jle	.L3650
	movq	8(%r12), %rdi
.L3580:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%r12)
	movl	%eax, -16968(%rbp)
	jg	.L3581
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3582:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L3651
	movl	16(%r12), %eax
	cmpl	$1, %eax
	jg	.L3584
	movq	(%r12), %rax
	movq	8(%rax), %r14
	leaq	88(%r14), %rax
	movq	%rax, -16960(%rbp)
	movq	%rax, %r13
.L3587:
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L3629
	movq	%r13, %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	movq	%rax, -16984(%rbp)
.L3588:
	movq	-16960(%rbp), %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movdqa	.LC88(%rip), %xmm0
	leaq	-16440(%rbp), %rcx
	movq	$0, -16440(%rbp)
	movq	%rcx, -16976(%rbp)
	movl	%eax, %ebx
	movq	%rcx, -16448(%rbp)
	movq	$0, -16432(%rbp)
	movaps	%xmm0, -16464(%rbp)
	cmpl	$1024, %eax
	ja	.L3652
	movq	%rbx, -16464(%rbp)
	testq	%rbx, %rbx
	je	.L3593
.L3592:
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
.L3599:
	movq	3280(%r15), %rsi
	movq	-16960(%rbp), %rdi
	movl	%r13d, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3653
	movq	%r14, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L3654
.L3597:
	movq	%r14, %rdi
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -16952(%rbp)
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpq	-16464(%rbp), %rbx
	movq	-16952(%rbp), %rsi
	movq	%rax, %rdi
	jnb	.L3655
	call	uv_buf_init@PLT
	salq	$4, %rbx
	addq	-16448(%rbp), %rbx
	movq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
	leal	1(%r13), %ebx
	movq	%rbx, %r13
	cmpq	-16464(%rbp), %rbx
	jb	.L3599
.L3593:
	cmpl	$3, 16(%r12)
	jle	.L3594
	movq	8(%r12), %rax
	leaq	-24(%rax), %rsi
.L3600:
	movq	%r15, %rdi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3601
	leaq	.LC18(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-16464(%rbp), %r8
	movl	$1, 536(%r13)
	movq	-16448(%rbp), %rcx
	jne	.L3656
	leaq	_ZN4node2fs12AfterIntegerEP7uv_fs_s(%rip), %rax
	subq	$8, %rsp
	movl	-16968(%rbp), %edx
	movq	-16984(%rbp), %r9
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	88(%r13), %r14
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rax
	pushq	%rax
	call	uv_fs_read@PLT
	popq	%rcx
	popq	%rsi
	testl	%eax, %eax
	js	.L3603
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L3604:
	movq	-16448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3574
	cmpq	-16976(%rbp), %rdi
	je	.L3574
	call	free@PLT
.L3574:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3657
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3650:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3580
	.p2align 4,,10
	.p2align 3
.L3581:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3582
	.p2align 4,,10
	.p2align 3
.L3584:
	movq	8(%r12), %r13
	leaq	-8(%r13), %rdx
	movq	%rdx, -16960(%rbp)
	cmpl	$2, %eax
	je	.L3658
	subq	$16, %r13
	jmp	.L3587
	.p2align 4,,10
	.p2align 3
.L3652:
	movq	%rbx, %r13
	salq	$4, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L3623
	movq	%rax, -16448(%rbp)
	movq	%rbx, -16456(%rbp)
.L3591:
	movq	%rbx, -16464(%rbp)
	jmp	.L3592
	.p2align 4,,10
	.p2align 3
.L3653:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	%r14, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	jne	.L3597
	.p2align 4,,10
	.p2align 3
.L3654:
	leaq	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3655:
	leaq	_ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3575:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L3577
	.p2align 4,,10
	.p2align 3
.L3649:
	leaq	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3651:
	leaq	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3594:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L3600
	.p2align 4,,10
	.p2align 3
.L3601:
	cmpl	$5, -16964(%rbp)
	jne	.L3659
	movq	$0, -16472(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3660
.L3607:
	cmpl	$4, 16(%r12)
	movq	-16464(%rbp), %rbx
	movq	-16448(%rbp), %rcx
	jg	.L3611
	movq	(%r12), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L3612:
	movq	%r15, %rdi
	movq	%rcx, -16952(%rbp)
	leaq	-16912(%rbp), %r13
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	360(%r15), %rax
	subq	$8, %rsp
	movl	-16968(%rbp), %edx
	movq	-16984(%rbp), %r9
	movq	-16952(%rbp), %rcx
	movl	%ebx, %r8d
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	pushq	$0
	call	uv_fs_read@PLT
	movslq	%eax, %rbx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	js	.L3661
.L3613:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3662
.L3618:
	movq	(%r12), %rax
	salq	$32, %rbx
	movq	%r13, %rdi
	movq	%rbx, 24(%rax)
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L3604
.L3623:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3663
	movq	-16464(%rbp), %rdx
	movq	%rax, -16448(%rbp)
	movq	%rbx, -16456(%rbp)
	testq	%rdx, %rdx
	je	.L3591
	movq	-16976(%rbp), %rsi
	salq	$4, %rdx
	call	memcpy@PLT
	movq	%rbx, -16464(%rbp)
	jmp	.L3592
.L3663:
	leaq	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3629:
	movq	$-1, -16984(%rbp)
	jmp	.L3588
.L3611:
	movq	8(%r12), %rax
	leaq	-32(%rax), %r14
	jmp	.L3612
.L3662:
	movq	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2029(%rip), %rdi
	testq	%rdi, %rdi
	je	.L3664
.L3620:
	testb	$5, (%rdi)
	je	.L3618
	leaq	.LC86(%rip), %rax
	leaq	-16929(%rbp), %rcx
	movb	$3, -16929(%rbp)
	leaq	-16928(%rbp), %rdx
	leaq	-16920(%rbp), %r8
	movq	%rax, -16928(%rbp)
	leaq	.LC85(%rip), %rsi
	movq	%rbx, -16920(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	jmp	.L3618
.L3660:
	movq	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2026(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3665
.L3609:
	testb	$5, (%rsi)
	je	.L3607
	leaq	.LC85(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3607
.L3661:
	movq	352(%r15), %r8
	movq	3280(%r15), %rax
	movl	%ebx, %esi
	movq	%r8, %rdi
	movq	%r8, -16960(%rbp)
	movq	%rax, -16952(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-16952(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%r15), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-16960(%rbp), %r8
	testb	%al, %al
	je	.L3666
.L3614:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3667
.L3615:
	movq	360(%r15), %rax
	movq	-16952(%rbp), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L3613
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3613
.L3603:
	movq	$0, 192(%r13)
	cltq
	movq	%r14, %rdi
	movq	%rax, 176(%r13)
	call	_ZN4node2fs12AfterIntegerEP7uv_fs_s
	jmp	.L3604
.L3659:
	leaq	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3664:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rdi
	movq	%rax, _ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2029(%rip)
	mfence
	jmp	.L3620
.L3665:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2026(%rip)
	mfence
	jmp	.L3609
.L3656:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3667:
	movq	%rax, -16960(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-16960(%rbp), %rcx
	jmp	.L3615
.L3666:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-16960(%rbp), %r8
	jmp	.L3614
.L3658:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L3587
.L3657:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7936:
	.size	_ZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node2fs19AfterOpenFileHandleEP7uv_fs_s
	.type	_ZN4node2fs19AfterOpenFileHandleEP7uv_fs_s, @function
_ZN4node2fs19AfterOpenFileHandleEP7uv_fs_s:
.LFB7863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	leaq	-88(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	leaq	-80(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-96(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %rbx
	jne	.L3684
	movq	-88(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L3670
	movq	16(%r12), %rdi
	movl	88(%rbx), %esi
	xorl	%edx, %edx
	call	_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE
	testq	%rax, %rax
	je	.L3671
	movq	-88(%rbx), %rdx
	movq	8(%rax), %rsi
	movq	104(%rdx), %rbx
	testq	%rsi, %rsi
	je	.L3673
	movzbl	11(%rsi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L3685
.L3673:
	movq	%r12, %rdi
	call	*%rbx
.L3671:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3686
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3685:
	.cfi_restore_state
	movq	16(%rax), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L3673
	.p2align 4,,10
	.p2align 3
.L3670:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3671
	.p2align 4,,10
	.p2align 3
.L3684:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3686:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7863:
	.size	_ZN4node2fs19AfterOpenFileHandleEP7uv_fs_s, .-_ZN4node2fs19AfterOpenFileHandleEP7uv_fs_s
	.p2align 4
	.type	_ZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7930:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1528, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3736
	cmpw	$1040, %cx
	jne	.L3688
.L3736:
	movq	23(%rdx), %r12
.L3690:
	movl	16(%rbx), %r14d
	movq	352(%r12), %rsi
	cmpl	$2, %r14d
	jg	.L3763
	leaq	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3763:
	movq	8(%rbx), %rdx
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L3764
	cmpl	$1, 16(%rbx)
	jle	.L3765
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L3693:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3766
	cmpl	$1, 16(%rbx)
	jg	.L3695
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3696:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, %r13d
	jg	.L3697
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3698:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3767
	cmpl	$2, 16(%rbx)
	jg	.L3700
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3701:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -1560(%rbp)
	jg	.L3702
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3704
.L3770:
	leaq	.LC16(%rip), %rax
	cmpq	$0, 80(%r15)
	movq	%r15, 88(%r15)
	movq	%rax, 544(%r15)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r15)
	jne	.L3768
	leaq	_ZN4node2fs19AfterOpenFileHandleEP7uv_fs_s(%rip), %rax
	leaq	88(%r15), %r12
	movl	%r13d, %ecx
	movl	-1560(%rbp), %r8d
	movq	%rax, 80(%r15)
	movq	16(%r15), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r9
	movq	%r12, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_open@PLT
	testl	%eax, %eax
	js	.L3706
	movq	16(%r15), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	addl	$1, 2156(%rax)
	movq	(%r15), %rax
	call	*120(%rax)
.L3707:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3687
.L3762:
	testq	%rdi, %rdi
	je	.L3687
	call	free@PLT
.L3687:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3769
	addq	$1528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3765:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3693
	.p2align 4,,10
	.p2align 3
.L3697:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3698
	.p2align 4,,10
	.p2align 3
.L3695:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3696
	.p2align 4,,10
	.p2align 3
.L3702:
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L3770
.L3704:
	cmpl	$5, %r14d
	jne	.L3771
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3772
.L3710:
	cmpl	$4, 16(%rbx)
	movq	-1088(%rbp), %rdx
	jle	.L3773
	movq	8(%rbx), %rax
	leaq	-32(%rax), %r14
.L3715:
	movq	%r12, %rdi
	movq	%rdx, -1568(%rbp)
	leaq	-1552(%rbp), %r15
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movl	%r13d, %ecx
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	movq	360(%r12), %rax
	movl	-1560(%rbp), %r8d
	movq	-1568(%rbp), %rdx
	movq	2360(%rax), %rdi
	call	uv_fs_open@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	js	.L3774
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L3731
.L3732:
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN4node2fs10FileHandle3NewEPNS_11EnvironmentEiN2v85LocalINS4_6ObjectEEE
	testq	%rax, %rax
	je	.L3720
	movq	8(%rax), %rdx
	movq	(%rbx), %rbx
	testq	%rdx, %rdx
	je	.L3724
	movzbl	11(%rdx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L3775
.L3725:
	movq	(%rdx), %rax
.L3726:
	movq	%rax, 24(%rbx)
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3700:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3701
	.p2align 4,,10
	.p2align 3
.L3773:
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3706:
	movq	$0, 192(%r15)
	cltq
	movq	%r12, %rdi
	movq	%rax, 176(%r15)
	call	_ZN4node2fs19AfterOpenFileHandleEP7uv_fs_s
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3688:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L3690
	.p2align 4,,10
	.p2align 3
.L3772:
	movq	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1691(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3776
.L3712:
	testb	$5, (%rsi)
	je	.L3710
	leaq	.LC87(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3710
	.p2align 4,,10
	.p2align 3
.L3774:
	movq	352(%r12), %r8
	movq	3280(%r12), %rax
	movl	%r13d, %esi
	movq	%r8, %rdi
	movq	%r8, -1568(%rbp)
	movq	%rax, -1560(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-1560(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%r12), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-1568(%rbp), %r8
	testb	%al, %al
	je	.L3777
.L3717:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3778
.L3718:
	movq	360(%r12), %rax
	movq	-1560(%rbp), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L3779
.L3719:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	je	.L3720
.L3731:
	movq	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1694(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3780
.L3722:
	testb	$5, (%rsi)
	jne	.L3781
.L3723:
	testl	%r13d, %r13d
	jns	.L3732
.L3720:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3762
	jmp	.L3687
	.p2align 4,,10
	.p2align 3
.L3764:
	leaq	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3766:
	leaq	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3767:
	leaq	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3775:
	movq	16(%rax), %rax
	movq	(%rdx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L3725
	.p2align 4,,10
	.p2align 3
.L3724:
	movq	16(%rbx), %rax
	jmp	.L3726
	.p2align 4,,10
	.p2align 3
.L3768:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3771:
	leaq	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3776:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1691(%rip)
	mfence
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3781:
	leaq	.LC87(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3723
	.p2align 4,,10
	.p2align 3
.L3780:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1694(%rip)
	mfence
	jmp	.L3722
.L3779:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3719
.L3778:
	movq	%rax, -1568(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1568(%rbp), %rcx
	jmp	.L3718
.L3777:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-1568(%rbp), %r8
	jmp	.L3717
.L3769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7930:
	.size	_ZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node2fs15AfterStringPathEP7uv_fs_s
	.type	_ZN4node2fs15AfterStringPathEP7uv_fs_s, @function
_ZN4node2fs15AfterStringPathEP7uv_fs_s:
.LFB7866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	leaq	-88(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	leaq	-80(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-96(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %rbx
	jne	.L3791
	movq	-88(%rbp), %rsi
	movq	$0, -104(%rbp)
	cmpq	$0, 88(%rsi)
	js	.L3784
	movq	16(%r12), %rax
	movq	104(%rbx), %rsi
	leaq	-104(%rbp), %rcx
	movl	536(%r12), %edx
	movq	352(%rax), %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3792
	movq	-88(%rbx), %rax
	movq	%r12, %rdi
	call	*104(%rax)
.L3787:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3793
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3792:
	.cfi_restore_state
	movq	-88(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	*96(%rax)
	jmp	.L3787
	.p2align 4,,10
	.p2align 3
.L3784:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3787
	.p2align 4,,10
	.p2align 3
.L3791:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3793:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7866:
	.size	_ZN4node2fs15AfterStringPathEP7uv_fs_s, .-_ZN4node2fs15AfterStringPathEP7uv_fs_s
	.p2align 4
	.globl	_ZN4node2fs14AfterStringPtrEP7uv_fs_s
	.type	_ZN4node2fs14AfterStringPtrEP7uv_fs_s, @function
_ZN4node2fs14AfterStringPtrEP7uv_fs_s:
.LFB7867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	leaq	-88(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	leaq	-80(%rbp), %rdi
	movq	%rbx, -88(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-96(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %rbx
	jne	.L3803
	movq	-88(%rbp), %rsi
	movq	$0, -104(%rbp)
	cmpq	$0, 88(%rsi)
	js	.L3796
	movq	16(%r12), %rax
	movq	96(%rbx), %rsi
	leaq	-104(%rbp), %rcx
	movl	536(%r12), %edx
	movq	352(%rax), %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3804
	movq	-88(%rbx), %rax
	movq	%r12, %rdi
	call	*104(%rax)
.L3799:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3805
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3804:
	.cfi_restore_state
	movq	-88(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	*96(%rax)
	jmp	.L3799
	.p2align 4,,10
	.p2align 3
.L3796:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3799
	.p2align 4,,10
	.p2align 3
.L3803:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3805:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7867:
	.size	_ZN4node2fs14AfterStringPtrEP7uv_fs_s, .-_ZN4node2fs14AfterStringPtrEP7uv_fs_s
	.p2align 4
	.globl	_ZN4node2fs11AfterMkdirpEP7uv_fs_s
	.type	_ZN4node2fs11AfterMkdirpEP7uv_fs_s, @function
_ZN4node2fs11AfterMkdirpEP7uv_fs_s:
.LFB7865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-88(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r12), %rax
	leaq	-128(%rbp), %rdi
	movq	%rbx, -136(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -104(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-144(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %rbx
	jne	.L3823
	movq	-136(%rbp), %rsi
	movq	$0, -160(%rbp)
	cmpq	$0, 88(%rsi)
	js	.L3808
	movq	528(%r12), %rax
	movq	64(%rax), %r13
	testq	%r13, %r13
	jne	.L3824
	movq	16(%r12), %rdx
	movq	-88(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L3811:
	movq	%r14, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3825
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3824:
	.cfi_restore_state
	movq	56(%rax), %r8
	leaq	-80(%rbp), %r15
	leaq	-96(%rbp), %rdi
	movq	%r15, -96(%rbp)
	testq	%r8, %r8
	je	.L3826
	movq	%r13, -152(%rbp)
	cmpq	$15, %r13
	ja	.L3827
	cmpq	$1, %r13
	jne	.L3820
	movzbl	(%r8), %eax
	movb	%al, -80(%rbp)
	movq	%r15, %rax
.L3815:
	movq	%r13, -88(%rbp)
	leaq	-160(%rbp), %rcx
	movb	$0, (%rax,%r13)
	movq	16(%r12), %rax
	movq	-96(%rbp), %rsi
	movl	536(%r12), %edx
	movq	352(%rax), %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %rsi
	movq	-88(%rbx), %rax
	testq	%rsi, %rsi
	je	.L3828
	movq	%r12, %rdi
	call	*104(%rax)
.L3817:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3811
	call	_ZdlPv@PLT
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3827:
	leaq	-152(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	%rax, -80(%rbp)
.L3814:
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %r13
	movq	-96(%rbp), %rax
	jmp	.L3815
	.p2align 4,,10
	.p2align 3
.L3808:
	movq	%r14, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3823:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3820:
	movq	%r15, %rdi
	jmp	.L3814
	.p2align 4,,10
	.p2align 3
.L3828:
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	*96(%rax)
	jmp	.L3817
.L3826:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L3825:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7865:
	.size	_ZN4node2fs11AfterMkdirpEP7uv_fs_s, .-_ZN4node2fs11AfterMkdirpEP7uv_fs_s
	.section	.rodata.str1.1
.LC89:
	.string	"vector::_M_realloc_insert"
	.text
	.p2align 4
	.globl	_ZN4node2fs12AfterScanDirEP7uv_fs_s
	.type	_ZN4node2fs12AfterScanDirEP7uv_fs_s, @function
_ZN4node2fs12AfterScanDirEP7uv_fs_s:
.LFB7868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-88(%rdi), %r14
	pushq	%r13
	movq	%r14, %rsi
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r14), %rax
	leaq	-96(%rbp), %rdi
	movq	%r13, -104(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r14), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -72(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-112(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %r13
	jne	.L3877
	movq	-104(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L3831
	movq	16(%r14), %rax
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	leaq	-128(%rbp), %r15
	movq	$0, -136(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-136(%rbp), %rax
	movq	$0, -152(%rbp)
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L3832:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	uv_fs_scandir_next@PLT
	cmpl	$-4095, %eax
	je	.L3834
	movq	-160(%rbp), %rcx
	movq	352(%rcx), %rdi
	testl	%eax, %eax
	jne	.L3878
	movl	536(%r14), %edx
	movq	-168(%rbp), %rcx
	movq	-128(%rbp), %rsi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3879
	cmpq	-152(%rbp), %rbx
	je	.L3838
	movq	%rax, (%rbx)
	addq	$8, %rbx
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L3838:
	movabsq	$1152921504606846975, %rsi
	movq	%rbx, %rcx
	subq	%r12, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L3880
	testq	%rax, %rax
	je	.L3851
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3881
.L3841:
	movq	%rsi, %rdi
	movq	%rcx, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rsi, -152(%rbp)
	call	_Znwm@PLT
	movq	-152(%rbp), %rsi
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rcx
	addq	%rax, %rsi
	movq	%rsi, -152(%rbp)
	leaq	8(%rax), %rsi
	movq	%rdx, (%rax,%rcx)
	cmpq	%r12, %rbx
	je	.L3854
.L3883:
	leaq	-8(%rbx), %rsi
	leaq	15(%rax), %rdx
	subq	%r12, %rsi
	subq	%r12, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L3855
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L3855
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L3845:
	movdqu	(%r12,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L3845
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%r12,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r8, %rdi
	je	.L3847
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L3847:
	leaq	16(%rax,%rsi), %rbx
.L3843:
	testq	%r12, %r12
	je	.L3848
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rax
.L3848:
	movq	%rax, %r12
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L3881:
	testq	%rdi, %rdi
	jne	.L3882
	xorl	%eax, %eax
	movl	$8, %esi
	movq	$0, -152(%rbp)
	movq	%rdx, (%rax,%rcx)
	cmpq	%r12, %rbx
	jne	.L3883
.L3854:
	movq	%rsi, %rbx
	jmp	.L3843
	.p2align 4,,10
	.p2align 3
.L3851:
	movl	$8, %esi
	jmp	.L3841
	.p2align 4,,10
	.p2align 3
.L3834:
	movq	-88(%r13), %rax
	subq	%r12, %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	104(%rax), %r13
	movq	-160(%rbp), %rax
	sarq	$3, %rdx
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	*%r13
.L3876:
	testq	%r12, %r12
	je	.L3833
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3833:
	movq	-176(%rbp), %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3884
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3878:
	.cfi_restore_state
	movq	-88(%r13), %rdx
	movq	544(%r14), %rcx
	xorl	%r9d, %r9d
	movl	%eax, %esi
	movq	104(%r13), %r8
	movq	96(%rdx), %rbx
	xorl	%edx, %edx
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	*%rbx
	jmp	.L3876
	.p2align 4,,10
	.p2align 3
.L3879:
	movq	-88(%r13), %rax
	movq	-136(%rbp), %rsi
	movq	%r14, %rdi
	call	*96(%rax)
	jmp	.L3876
	.p2align 4,,10
	.p2align 3
.L3855:
	movq	%rax, %rcx
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L3844:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%rbx, %rdx
	jne	.L3844
	jmp	.L3847
	.p2align 4,,10
	.p2align 3
.L3831:
	movq	-176(%rbp), %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3833
	.p2align 4,,10
	.p2align 3
.L3877:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3880:
	leaq	.LC89(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3882:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %rsi
	cmovbe	%rdi, %rsi
	salq	$3, %rsi
	jmp	.L3841
.L3884:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7868:
	.size	_ZN4node2fs12AfterScanDirEP7uv_fs_s, .-_ZN4node2fs12AfterScanDirEP7uv_fs_s
	.section	.text._ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_,"axG",@progbits,_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_,comdat
	.p2align 4
	.weak	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	.type	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_, @function
_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_:
.LFB8923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	%r12, %rdx
	movl	16(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	2360(%rax), %rdi
	call	*%r15
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L3891
.L3885:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3891:
	.cfi_restore_state
	movq	352(%rbx), %r8
	movl	%eax, %esi
	movq	3280(%rbx), %r15
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r8
	testb	%al, %al
	je	.L3892
.L3887:
	movq	-56(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3893
.L3888:
	movq	360(%rbx), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L3885
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3885
	.p2align 4,,10
	.p2align 3
.L3892:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-64(%rbp), %r8
	jmp	.L3887
	.p2align 4,,10
	.p2align 3
.L3893:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L3888
	.cfi_endproc
.LFE8923:
	.size	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_, .-_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	.section	.rodata.str1.1
.LC90:
	.string	"fs.sync.access"
	.text
	.p2align 4
	.globl	_ZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1608, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3935
	cmpw	$1040, %cx
	jne	.L3895
.L3935:
	movq	23(%rdx), %r12
.L3897:
	movq	352(%r12), %r14
	leaq	-1632(%rbp), %r15
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	16(%r13), %ebx
	cmpl	$1, %ebx
	jg	.L3955
	leaq	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3955:
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3956
	cmpl	$1, 16(%r13)
	jle	.L3957
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
.L3900:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	16(%r13), %ecx
	movl	%eax, -1636(%rbp)
	testl	%ecx, %ecx
	jg	.L3901
	movq	0(%r13), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L3902:
	leaq	-1104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L3958
	cmpl	$2, 16(%r13)
	jg	.L3904
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3906
.L3961:
	leaq	.LC15(%rip), %rax
	cmpq	$0, 80(%r14)
	movq	%r14, 88(%r14)
	movq	%rax, 544(%r14)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r14)
	jne	.L3959
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r14), %r12
	movl	-1636(%rbp), %ecx
	movq	%rax, 80(%r14)
	movq	16(%r14), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r12, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_access@PLT
	testl	%eax, %eax
	js	.L3908
	movq	16(%r14), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	addl	$1, 2156(%rax)
	movq	(%r14), %rax
	call	*120(%rax)
.L3909:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3927
	testq	%rdi, %rdi
	je	.L3927
	call	free@PLT
.L3927:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3960
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3957:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3900
	.p2align 4,,10
	.p2align 3
.L3901:
	movq	8(%r13), %rdx
	jmp	.L3902
	.p2align 4,,10
	.p2align 3
.L3904:
	movq	8(%r13), %rax
	movq	%r12, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L3961
.L3906:
	cmpl	$4, %ebx
	jne	.L3962
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3913
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L3963
.L3914:
	cmpb	$0, (%rax)
	jne	.L3964
.L3913:
	cmpl	$3, 16(%r13)
	movq	-1088(%rbp), %r9
	jle	.L3965
	movq	8(%r13), %rsi
	subq	$24, %rsi
.L3919:
	movl	-1636(%rbp), %eax
	subq	$8, %rsp
	movq	%r12, %rdi
	movq	uv_fs_access@GOTPCREL(%rip), %r8
	leaq	-1552(%rbp), %r14
	leaq	.LC15(%rip), %rcx
	pushq	%rax
	movq	%r14, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L3921
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L3966
.L3922:
	cmpb	$0, (%rax)
	jne	.L3967
.L3921:
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-1112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3909
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3909
	.p2align 4,,10
	.p2align 3
.L3965:
	movq	0(%r13), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L3919
	.p2align 4,,10
	.p2align 3
.L3908:
	cltq
	leaq	-1600(%rbp), %r13
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%rax, 176(%r14)
	movq	%r13, %rdi
	movq	$0, 192(%r14)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1592(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L3968
	movq	16(%r14), %rdx
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L3910:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L3909
	.p2align 4,,10
	.p2align 3
.L3964:
	movq	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic800(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3969
.L3917:
	testb	$5, (%rsi)
	je	.L3913
	leaq	.LC90(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3913
	.p2align 4,,10
	.p2align 3
.L3895:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L3897
	.p2align 4,,10
	.p2align 3
.L3967:
	movq	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic802(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3970
.L3925:
	testb	$5, (%rsi)
	je	.L3921
	leaq	.LC90(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3921
	.p2align 4,,10
	.p2align 3
.L3956:
	leaq	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3958:
	leaq	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3966:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L3922
	.p2align 4,,10
	.p2align 3
.L3963:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3959:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3962:
	leaq	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3970:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic802(%rip)
	mfence
	jmp	.L3925
	.p2align 4,,10
	.p2align 3
.L3969:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic800(%rip)
	mfence
	jmp	.L3917
	.p2align 4,,10
	.p2align 3
.L3968:
	movq	%r13, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3910
.L3960:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7879:
	.size	_ZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC91:
	.string	"fs.sync.chmod"
	.text
	.p2align 4
	.type	_ZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1560, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4012
	cmpw	$1040, %cx
	jne	.L3972
.L4012:
	movq	23(%rdx), %r14
.L3974:
	movl	16(%r12), %ebx
	cmpl	$1, %ebx
	jg	.L4031
	leaq	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4031:
	movq	8(%r12), %rdx
	movq	352(%r14), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L4032
	cmpl	$1, 16(%r12)
	jle	.L4033
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L3977:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L4034
	cmpl	$1, 16(%r12)
	jg	.L3979
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3980:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r15d
	jg	.L3981
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3983
.L4037:
	leaq	.LC41(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r13)
	jne	.L4035
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movl	%r15d, %ecx
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_chmod@PLT
	testl	%eax, %eax
	js	.L3985
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L3986:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3971
	testq	%rdi, %rdi
	je	.L3971
	call	free@PLT
.L3971:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4036
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4033:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3977
	.p2align 4,,10
	.p2align 3
.L3981:
	movq	8(%r12), %rax
	movq	%r14, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L4037
.L3983:
	cmpl	$4, %ebx
	jne	.L4038
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3990
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4039
.L3991:
	cmpb	$0, (%rax)
	jne	.L4040
.L3990:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r9
	jle	.L4041
	movq	8(%r12), %rsi
	subq	$24, %rsi
.L3996:
	subq	$8, %rsp
	movq	uv_fs_chmod@GOTPCREL(%rip), %r8
	movq	%r14, %rdi
	leaq	-1552(%rbp), %r12
	pushq	%r15
	movq	%r12, %rdx
	leaq	.LC41(%rip), %rcx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L3998
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4042
.L3999:
	cmpb	$0, (%rax)
	jne	.L4043
.L3998:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-1112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3986
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3986
	.p2align 4,,10
	.p2align 3
.L3979:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3980
	.p2align 4,,10
	.p2align 3
.L4041:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L3996
	.p2align 4,,10
	.p2align 3
.L3985:
	cltq
	leaq	-1600(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1592(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L4044
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L3987:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L3986
	.p2align 4,,10
	.p2align 3
.L4040:
	movq	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2057(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4045
.L3994:
	testb	$5, (%rsi)
	je	.L3990
	leaq	.LC91(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3990
	.p2align 4,,10
	.p2align 3
.L3972:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L3974
	.p2align 4,,10
	.p2align 3
.L4043:
	movq	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2060(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4046
.L4002:
	testb	$5, (%rsi)
	je	.L3998
	leaq	.LC91(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L3998
	.p2align 4,,10
	.p2align 3
.L4032:
	leaq	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4034:
	leaq	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4042:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L3999
	.p2align 4,,10
	.p2align 3
.L4039:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L3991
	.p2align 4,,10
	.p2align 3
.L4035:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4038:
	leaq	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4046:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2060(%rip)
	mfence
	jmp	.L4002
	.p2align 4,,10
	.p2align 3
.L4045:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2057(%rip)
	mfence
	jmp	.L3994
	.p2align 4,,10
	.p2align 3
.L4044:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L3987
.L4036:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7937:
	.size	_ZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_,"axG",@progbits,_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_,comdat
	.p2align 4
	.weak	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	.type	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_, @function
_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_:
.LFB8952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rcx, -56(%rbp)
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	*%r12
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L4053
.L4047:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4053:
	.cfi_restore_state
	movq	352(%rbx), %r8
	movl	%eax, %esi
	movq	3280(%rbx), %r15
	movq	%r8, %rdi
	movq	%r8, -64(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r8
	testb	%al, %al
	je	.L4054
.L4049:
	movq	-56(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L4055
.L4050:
	movq	360(%rbx), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L4047
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L4047
	.p2align 4,,10
	.p2align 3
.L4054:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-64(%rbp), %r8
	jmp	.L4049
	.p2align 4,,10
	.p2align 3
.L4055:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L4050
	.cfi_endproc
.LFE8952:
	.size	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_, .-_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	.section	.rodata.str1.1
.LC92:
	.string	"fs.sync.mkdtemp"
	.text
	.p2align 4
	.type	_ZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4097
	cmpw	$1040, %cx
	jne	.L4057
.L4097:
	movq	23(%rdx), %rbx
.L4059:
	movl	16(%r12), %r15d
	movq	352(%rbx), %r14
	cmpl	$1, %r15d
	jg	.L4120
	leaq	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4120:
	movq	8(%r12), %rdx
	leaq	-1104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L4121
	cmpl	$1, 16(%r12)
	jle	.L4122
	movq	8(%r12), %rax
	leaq	-8(%rax), %rsi
.L4062:
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r13d
	jg	.L4063
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L4065
.L4125:
	leaq	.LC48(%rip), %rax
	cmpq	$0, 80(%r8)
	movq	%r8, 88(%r8)
	movq	%rax, 544(%r8)
	movq	-1088(%rbp), %rdx
	movl	%r13d, 536(%r8)
	jne	.L4123
	leaq	_ZN4node2fs15AfterStringPathEP7uv_fs_s(%rip), %rax
	leaq	88(%r8), %r13
	movq	%r8, -1576(%rbp)
	movq	%rax, 80(%r8)
	movq	16(%r8), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_mkdtemp@PLT
	movq	-1576(%rbp), %r8
	testl	%eax, %eax
	js	.L4067
	movq	16(%r8), %rax
	movq	%r12, %rsi
	movq	%r8, %rdi
	addl	$1, 2156(%rax)
	movq	(%r8), %rax
	call	*120(%rax)
.L4068:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4056
	testq	%rdi, %rdi
	je	.L4056
	call	free@PLT
.L4056:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4124
	addq	$1544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4122:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4062
	.p2align 4,,10
	.p2align 3
.L4063:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L4125
.L4065:
	cmpl	$4, %r15d
	jne	.L4126
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4071
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4127
.L4072:
	cmpb	$0, (%rax)
	jne	.L4128
.L4071:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r9
	jle	.L4129
	movq	8(%r12), %rax
	leaq	-24(%rax), %rsi
.L4077:
	movq	uv_fs_mkdtemp@GOTPCREL(%rip), %r8
	leaq	-1552(%rbp), %r15
	movq	%rbx, %rdi
	leaq	.LC48(%rip), %rcx
	movq	%r15, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4079
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4130
.L4080:
	cmpb	$0, (%rax)
	jne	.L4131
.L4079:
	movq	-1448(%rbp), %rsi
	leaq	-1560(%rbp), %rcx
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	$0, -1560(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L4132
	movq	(%rax), %rdx
	movq	(%r12), %rax
	movq	%rdx, 24(%rax)
.L4119:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L4068
	.p2align 4,,10
	.p2align 3
.L4129:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4077
	.p2align 4,,10
	.p2align 3
.L4128:
	movq	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2268(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4133
.L4075:
	testb	$5, (%rsi)
	je	.L4071
	leaq	.LC92(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4071
	.p2align 4,,10
	.p2align 3
.L4057:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L4059
	.p2align 4,,10
	.p2align 3
.L4131:
	movq	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2271(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4134
.L4083:
	testb	$5, (%rsi)
	je	.L4079
	leaq	.LC92(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4079
	.p2align 4,,10
	.p2align 3
.L4132:
	cmpl	$3, 16(%r12)
	jg	.L4085
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4086:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	-1560(%rbp), %rcx
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L4119
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L4119
	.p2align 4,,10
	.p2align 3
.L4121:
	leaq	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4067:
	movq	$0, 192(%r8)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r8)
	call	_ZN4node2fs15AfterStringPathEP7uv_fs_s
	jmp	.L4068
	.p2align 4,,10
	.p2align 3
.L4130:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4080
	.p2align 4,,10
	.p2align 3
.L4127:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4072
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	8(%r12), %rdi
	subq	$24, %rdi
	jmp	.L4086
	.p2align 4,,10
	.p2align 3
.L4123:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4126:
	leaq	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4134:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2271(%rip)
	mfence
	jmp	.L4083
	.p2align 4,,10
	.p2align 3
.L4133:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2268(%rip)
	mfence
	jmp	.L4075
.L4124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7944:
	.size	_ZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC93:
	.string	"fs.sync.realpath"
	.text
	.p2align 4
	.type	_ZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4177
	cmpw	$1040, %cx
	jne	.L4136
.L4177:
	movq	23(%rdx), %rbx
.L4138:
	movl	16(%r12), %r14d
	movq	352(%rbx), %r13
	cmpl	$2, %r14d
	jg	.L4198
	leaq	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4198:
	movq	8(%r12), %rdx
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L4199
	cmpl	$1, 16(%r12)
	jle	.L4200
	movq	8(%r12), %rax
	leaq	-8(%rax), %rsi
.L4141:
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, -1572(%rbp)
	jg	.L4142
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4144
.L4203:
	leaq	.LC39(%rip), %rax
	cmpq	$0, 80(%r15)
	movq	%r15, 88(%r15)
	movq	%rax, 544(%r15)
	movl	-1572(%rbp), %eax
	movq	-1088(%rbp), %rdx
	movl	%eax, 536(%r15)
	jne	.L4201
	leaq	_ZN4node2fs14AfterStringPtrEP7uv_fs_s(%rip), %rax
	leaq	88(%r15), %r13
	movq	%rax, 80(%r15)
	movq	16(%r15), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_realpath@PLT
	testl	%eax, %eax
	js	.L4146
	movq	16(%r15), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	addl	$1, 2156(%rax)
	movq	(%r15), %rax
	call	*120(%rax)
.L4147:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4135
.L4197:
	testq	%rdi, %rdi
	je	.L4135
	call	free@PLT
.L4135:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4202
	addq	$1544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4200:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4141
	.p2align 4,,10
	.p2align 3
.L4142:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L4203
.L4144:
	cmpl	$4, %r14d
	jne	.L4204
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4150
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4205
.L4151:
	cmpb	$0, (%rax)
	jne	.L4206
.L4150:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r9
	jle	.L4207
	movq	8(%r12), %rax
	leaq	-24(%rax), %rsi
.L4156:
	movq	uv_fs_realpath@GOTPCREL(%rip), %r8
	leaq	-1552(%rbp), %r15
	movq	%rbx, %rdi
	leaq	.LC39(%rip), %rcx
	movq	%r15, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	movl	%eax, %r14d
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4158
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4208
.L4159:
	cmpb	$0, (%rax)
	jne	.L4209
.L4158:
	testl	%r14d, %r14d
	js	.L4163
	movl	-1572(%rbp), %edx
	movq	-1456(%rbp), %rsi
	leaq	-1560(%rbp), %rcx
	movq	%r13, %rdi
	movq	$0, -1560(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L4210
	movq	(%rax), %rdx
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rdx, 24(%rax)
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L4147
	.p2align 4,,10
	.p2align 3
.L4207:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4156
	.p2align 4,,10
	.p2align 3
.L4206:
	movq	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1526(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4211
.L4154:
	testb	$5, (%rsi)
	je	.L4150
	leaq	.LC93(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4150
	.p2align 4,,10
	.p2align 3
.L4136:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L4138
	.p2align 4,,10
	.p2align 3
.L4210:
	cmpl	$3, 16(%r12)
	jle	.L4212
	movq	8(%r12), %rdi
	subq	$24, %rdi
.L4166:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	-1560(%rbp), %rcx
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L4163
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L4163:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4197
	jmp	.L4135
	.p2align 4,,10
	.p2align 3
.L4209:
	movq	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1529(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4213
.L4162:
	testb	$5, (%rsi)
	je	.L4158
	leaq	.LC93(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4158
	.p2align 4,,10
	.p2align 3
.L4199:
	leaq	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4146:
	movq	$0, 192(%r15)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r15)
	call	_ZN4node2fs14AfterStringPtrEP7uv_fs_s
	jmp	.L4147
	.p2align 4,,10
	.p2align 3
.L4208:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4159
	.p2align 4,,10
	.p2align 3
.L4205:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4151
	.p2align 4,,10
	.p2align 3
.L4212:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4166
	.p2align 4,,10
	.p2align 3
.L4201:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4204:
	leaq	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4213:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1529(%rip)
	mfence
	jmp	.L4162
	.p2align 4,,10
	.p2align 3
.L4211:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1526(%rip)
	mfence
	jmp	.L4154
.L4202:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7927:
	.size	_ZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC94:
	.string	"fs.sync.unlink"
	.text
	.p2align 4
	.type	_ZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7912:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1552, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4250
	cmpw	$1040, %cx
	jne	.L4215
.L4250:
	movq	23(%rdx), %r14
.L4217:
	movl	16(%r12), %ebx
	cmpl	$1, %ebx
	jg	.L4269
	leaq	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4269:
	movq	8(%r12), %rdx
	movq	352(%r14), %rsi
	leaq	-1088(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L4270
	cmpl	$1, 16(%r12)
	jle	.L4271
	movq	8(%r12), %rax
	movq	%r14, %rdi
	leaq	-8(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4221
.L4274:
	leaq	.LC35(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-1072(%rbp), %rdx
	movl	$1, 536(%r13)
	jne	.L4272
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_unlink@PLT
	testl	%eax, %eax
	js	.L4223
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L4224:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4214
	testq	%rdi, %rdi
	je	.L4214
	call	free@PLT
.L4214:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4273
	addq	$1552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4271:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L4274
.L4221:
	cmpl	$3, %ebx
	jne	.L4275
	movq	$0, -1096(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4228
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4276
.L4229:
	cmpb	$0, (%rax)
	jne	.L4277
.L4228:
	cmpl	$2, 16(%r12)
	movq	-1072(%rbp), %r9
	jle	.L4278
	movq	8(%r12), %rsi
	subq	$16, %rsi
.L4234:
	movq	uv_fs_unlink@GOTPCREL(%rip), %r8
	leaq	-1536(%rbp), %r12
	movq	%r14, %rdi
	leaq	.LC35(%rip), %rcx
	movq	%r12, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4236
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4279
.L4237:
	cmpb	$0, (%rax)
	jne	.L4280
.L4236:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-1096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4224
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4278:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4234
	.p2align 4,,10
	.p2align 3
.L4277:
	movq	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1248(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4281
.L4232:
	testb	$5, (%rsi)
	je	.L4228
	leaq	.LC94(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4228
	.p2align 4,,10
	.p2align 3
.L4215:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L4217
	.p2align 4,,10
	.p2align 3
.L4280:
	movq	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1250(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4282
.L4240:
	testb	$5, (%rsi)
	je	.L4236
	leaq	.LC94(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4236
	.p2align 4,,10
	.p2align 3
.L4270:
	leaq	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4223:
	cltq
	leaq	-1584(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1576(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L4283
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L4225:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4276:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4229
	.p2align 4,,10
	.p2align 3
.L4279:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4237
	.p2align 4,,10
	.p2align 3
.L4272:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4275:
	leaq	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4282:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1250(%rip)
	mfence
	jmp	.L4240
	.p2align 4,,10
	.p2align 3
.L4281:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1248(%rip)
	mfence
	jmp	.L4232
	.p2align 4,,10
	.p2align 3
.L4283:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L4225
.L4273:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7912:
	.size	_ZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC95:
	.string	"fs.sync.readlink"
	.text
	.p2align 4
	.type	_ZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4326
	cmpw	$1040, %cx
	jne	.L4285
.L4326:
	movq	23(%rdx), %rbx
.L4287:
	movl	16(%r12), %r14d
	movq	352(%rbx), %r13
	cmpl	$2, %r14d
	jg	.L4347
	leaq	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4347:
	movq	8(%r12), %rdx
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L4348
	cmpl	$1, 16(%r12)
	jle	.L4349
	movq	8(%r12), %rax
	leaq	-8(%rax), %rsi
.L4290:
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, -1572(%rbp)
	jg	.L4291
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4293
.L4352:
	leaq	.LC34(%rip), %rax
	cmpq	$0, 80(%r15)
	movq	%r15, 88(%r15)
	movq	%rax, 544(%r15)
	movl	-1572(%rbp), %eax
	movq	-1088(%rbp), %rdx
	movl	%eax, 536(%r15)
	jne	.L4350
	leaq	_ZN4node2fs14AfterStringPtrEP7uv_fs_s(%rip), %rax
	leaq	88(%r15), %r13
	movq	%rax, 80(%r15)
	movq	16(%r15), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_readlink@PLT
	testl	%eax, %eax
	js	.L4295
	movq	16(%r15), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	addl	$1, 2156(%rax)
	movq	(%r15), %rax
	call	*120(%rax)
.L4296:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4284
.L4346:
	testq	%rdi, %rdi
	je	.L4284
	call	free@PLT
.L4284:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4351
	addq	$1544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4349:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4290
	.p2align 4,,10
	.p2align 3
.L4291:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L4352
.L4293:
	cmpl	$4, %r14d
	jne	.L4353
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4299
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4354
.L4300:
	cmpb	$0, (%rax)
	jne	.L4355
.L4299:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r9
	jle	.L4356
	movq	8(%r12), %rax
	leaq	-24(%rax), %rsi
.L4305:
	movq	uv_fs_readlink@GOTPCREL(%rip), %r8
	leaq	-1552(%rbp), %r15
	movq	%rbx, %rdi
	leaq	.LC34(%rip), %rcx
	movq	%r15, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	movl	%eax, %r14d
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4307
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4357
.L4308:
	cmpb	$0, (%rax)
	jne	.L4358
.L4307:
	testl	%r14d, %r14d
	js	.L4312
	movl	-1572(%rbp), %edx
	movq	-1456(%rbp), %rsi
	leaq	-1560(%rbp), %rcx
	movq	%r13, %rdi
	movq	$0, -1560(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L4359
	movq	(%rax), %rdx
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rdx, 24(%rax)
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L4296
	.p2align 4,,10
	.p2align 3
.L4356:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4305
	.p2align 4,,10
	.p2align 3
.L4355:
	movq	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1111(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4360
.L4303:
	testb	$5, (%rsi)
	je	.L4299
	leaq	.LC95(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4299
	.p2align 4,,10
	.p2align 3
.L4285:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L4287
	.p2align 4,,10
	.p2align 3
.L4359:
	cmpl	$3, 16(%r12)
	jle	.L4361
	movq	8(%r12), %rdi
	subq	$24, %rdi
.L4315:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	-1560(%rbp), %rcx
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L4312
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L4312:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4346
	jmp	.L4284
	.p2align 4,,10
	.p2align 3
.L4358:
	movq	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1114(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4362
.L4311:
	testb	$5, (%rsi)
	je	.L4307
	leaq	.LC95(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4307
	.p2align 4,,10
	.p2align 3
.L4348:
	leaq	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4295:
	movq	$0, 192(%r15)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r15)
	call	_ZN4node2fs14AfterStringPtrEP7uv_fs_s
	jmp	.L4296
	.p2align 4,,10
	.p2align 3
.L4357:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4308
	.p2align 4,,10
	.p2align 3
.L4354:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4300
	.p2align 4,,10
	.p2align 3
.L4361:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4315
	.p2align 4,,10
	.p2align 3
.L4350:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4353:
	leaq	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4362:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1114(%rip)
	mfence
	jmp	.L4311
	.p2align 4,,10
	.p2align 3
.L4360:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1111(%rip)
	mfence
	jmp	.L4303
.L4351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7907:
	.size	_ZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC96:
	.string	"fs.sync.lstat"
	.text
	.p2align 4
	.type	_ZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1560, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4411
	cmpw	$1040, %cx
	jne	.L4364
.L4411:
	movq	23(%rdx), %rbx
.L4366:
	movl	16(%r12), %r15d
	cmpl	$2, %r15d
	jg	.L4436
	leaq	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4436:
	movq	8(%r12), %rdx
	movq	352(%rbx), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L4437
	cmpl	$1, 16(%r12)
	jle	.L4438
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L4369:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$2, 16(%r12)
	movzbl	%al, %edx
	movl	%edx, %r14d
	jg	.L4370
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4372
.L4441:
	leaq	.LC30(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r13)
	jne	.L4439
	leaq	_ZN4node2fs9AfterStatEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_lstat@PLT
	testl	%eax, %eax
	js	.L4374
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L4375:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4363
	testq	%rdi, %rdi
	je	.L4363
	call	free@PLT
.L4363:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4440
	addq	$1560, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4438:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4369
	.p2align 4,,10
	.p2align 3
.L4370:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L4441
.L4372:
	cmpl	$4, %r15d
	jne	.L4442
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4379
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4443
.L4380:
	cmpb	$0, (%rax)
	jne	.L4444
.L4379:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r9
	jle	.L4445
	movq	8(%r12), %rax
	leaq	-24(%rax), %rsi
.L4385:
	movq	uv_fs_lstat@GOTPCREL(%rip), %r8
	leaq	-1552(%rbp), %r15
	movq	%rbx, %rdi
	leaq	.LC30(%rip), %rcx
	movq	%r15, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	movl	%eax, %r13d
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4387
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4446
.L4388:
	cmpb	$0, (%rax)
	jne	.L4447
.L4387:
	testl	%r13d, %r13d
	jne	.L4435
	movq	-1456(%rbp), %rsi
	testb	%r14b, %r14b
	je	.L4396
	leaq	2304(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2336(%rbx), %rax
	testq	%rax, %rax
	je	.L4430
	movq	2304(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
.L4399:
	movq	(%r12), %rdx
	testq	%rax, %rax
	je	.L4398
	movq	(%rax), %rax
.L4402:
	movq	%rax, 24(%rdx)
.L4435:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L4375
	.p2align 4,,10
	.p2align 3
.L4445:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4385
	.p2align 4,,10
	.p2align 3
.L4444:
	movq	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic992(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4448
.L4383:
	testb	$5, (%rsi)
	je	.L4379
	leaq	.LC96(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4379
	.p2align 4,,10
	.p2align 3
.L4364:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L4366
	.p2align 4,,10
	.p2align 3
.L4447:
	movq	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic995(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4449
.L4391:
	testb	$5, (%rsi)
	je	.L4387
	leaq	.LC96(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4387
	.p2align 4,,10
	.p2align 3
.L4396:
	leaq	2264(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2296(%rbx), %rax
	testq	%rax, %rax
	je	.L4430
	movq	2264(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L4399
	.p2align 4,,10
	.p2align 3
.L4430:
	movq	(%r12), %rdx
.L4398:
	movq	16(%rdx), %rax
	jmp	.L4402
	.p2align 4,,10
	.p2align 3
.L4437:
	leaq	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4374:
	cltq
	leaq	-1600(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1592(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L4450
	movq	0(%r13), %rax
	leaq	200(%r13), %rsi
	movq	%r13, %rdi
	call	*112(%rax)
.L4376:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L4375
	.p2align 4,,10
	.p2align 3
.L4443:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4380
	.p2align 4,,10
	.p2align 3
.L4446:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4388
	.p2align 4,,10
	.p2align 3
.L4439:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4442:
	leaq	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4449:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic995(%rip)
	mfence
	jmp	.L4391
	.p2align 4,,10
	.p2align 3
.L4448:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic992(%rip)
	mfence
	jmp	.L4383
	.p2align 4,,10
	.p2align 3
.L4450:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L4376
.L4440:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7903:
	.size	_ZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC97:
	.string	"fs.sync.stat"
	.text
	.p2align 4
	.type	_ZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1560, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4499
	cmpw	$1040, %cx
	jne	.L4452
.L4499:
	movq	23(%rdx), %rbx
.L4454:
	movl	16(%r12), %r15d
	cmpl	$1, %r15d
	jg	.L4524
	leaq	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4524:
	movq	8(%r12), %rdx
	movq	352(%rbx), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L4525
	cmpl	$1, 16(%r12)
	jle	.L4526
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L4457:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$2, 16(%r12)
	movzbl	%al, %edx
	movl	%edx, %r14d
	jg	.L4458
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4460
.L4529:
	leaq	.LC29(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-1088(%rbp), %rdx
	movl	$1, 536(%r13)
	jne	.L4527
	leaq	_ZN4node2fs9AfterStatEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_stat@PLT
	testl	%eax, %eax
	js	.L4462
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L4463:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4451
	testq	%rdi, %rdi
	je	.L4451
	call	free@PLT
.L4451:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4528
	addq	$1560, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4526:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4457
	.p2align 4,,10
	.p2align 3
.L4458:
	movq	8(%r12), %rax
	movq	%rbx, %rdi
	leaq	-16(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L4529
.L4460:
	cmpl	$4, %r15d
	jne	.L4530
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4467
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4531
.L4468:
	cmpb	$0, (%rax)
	jne	.L4532
.L4467:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %r9
	jle	.L4533
	movq	8(%r12), %rax
	leaq	-24(%rax), %rsi
.L4473:
	movq	uv_fs_stat@GOTPCREL(%rip), %r8
	leaq	-1552(%rbp), %r15
	movq	%rbx, %rdi
	leaq	.LC29(%rip), %rcx
	movq	%r15, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	movl	%eax, %r13d
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4475
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4534
.L4476:
	cmpb	$0, (%rax)
	jne	.L4535
.L4475:
	testl	%r13d, %r13d
	jne	.L4523
	movq	-1456(%rbp), %rsi
	testb	%r14b, %r14b
	je	.L4484
	leaq	2304(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2336(%rbx), %rax
	testq	%rax, %rax
	je	.L4518
	movq	2304(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
.L4487:
	movq	(%r12), %rdx
	testq	%rax, %rax
	je	.L4486
	movq	(%rax), %rax
.L4490:
	movq	%rax, 24(%rdx)
.L4523:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L4463
	.p2align 4,,10
	.p2align 3
.L4533:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4473
	.p2align 4,,10
	.p2align 3
.L4532:
	movq	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic962(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4536
.L4471:
	testb	$5, (%rsi)
	je	.L4467
	leaq	.LC97(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4467
	.p2align 4,,10
	.p2align 3
.L4452:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L4454
	.p2align 4,,10
	.p2align 3
.L4535:
	movq	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic964(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4537
.L4479:
	testb	$5, (%rsi)
	je	.L4475
	leaq	.LC97(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4475
	.p2align 4,,10
	.p2align 3
.L4484:
	leaq	2264(%rbx), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2296(%rbx), %rax
	testq	%rax, %rax
	je	.L4518
	movq	2264(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L4487
	.p2align 4,,10
	.p2align 3
.L4518:
	movq	(%r12), %rdx
.L4486:
	movq	16(%rdx), %rax
	jmp	.L4490
	.p2align 4,,10
	.p2align 3
.L4525:
	leaq	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4462:
	cltq
	leaq	-1600(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1592(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L4538
	movq	0(%r13), %rax
	leaq	200(%r13), %rsi
	movq	%r13, %rdi
	call	*112(%rax)
.L4464:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L4463
	.p2align 4,,10
	.p2align 3
.L4531:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4468
	.p2align 4,,10
	.p2align 3
.L4534:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4476
	.p2align 4,,10
	.p2align 3
.L4527:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4530:
	leaq	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4537:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic964(%rip)
	mfence
	jmp	.L4479
	.p2align 4,,10
	.p2align 3
.L4536:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic962(%rip)
	mfence
	jmp	.L4471
	.p2align 4,,10
	.p2align 3
.L4538:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L4464
.L4528:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7902:
	.size	_ZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC98:
	.string	"fs.sync.rmdir"
	.text
	.p2align 4
	.type	_ZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1552, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4575
	cmpw	$1040, %cx
	jne	.L4540
.L4575:
	movq	23(%rdx), %r14
.L4542:
	movl	16(%r12), %ebx
	cmpl	$1, %ebx
	jg	.L4594
	leaq	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4594:
	movq	8(%r12), %rdx
	movq	352(%r14), %rsi
	leaq	-1088(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L4595
	cmpl	$1, 16(%r12)
	jle	.L4596
	movq	8(%r12), %rax
	movq	%r14, %rdi
	leaq	-8(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4546
.L4599:
	leaq	.LC24(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	%r13, 88(%r13)
	movq	%rax, 544(%r13)
	movq	-1072(%rbp), %rdx
	movl	$1, 536(%r13)
	jne	.L4597
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_rmdir@PLT
	testl	%eax, %eax
	js	.L4548
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L4549:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4539
	testq	%rdi, %rdi
	je	.L4539
	call	free@PLT
.L4539:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4598
	addq	$1552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4596:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L4599
.L4546:
	cmpl	$3, %ebx
	jne	.L4600
	movq	$0, -1096(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4553
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4601
.L4554:
	cmpb	$0, (%rax)
	jne	.L4602
.L4553:
	cmpl	$2, 16(%r12)
	movq	-1072(%rbp), %r9
	jle	.L4603
	movq	8(%r12), %rsi
	subq	$16, %rsi
.L4559:
	movq	uv_fs_rmdir@GOTPCREL(%rip), %r8
	leaq	-1536(%rbp), %r12
	movq	%r14, %rdi
	leaq	.LC24(%rip), %rcx
	movq	%r12, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKcPFvS5_EEJPcEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4561
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4604
.L4562:
	cmpb	$0, (%rax)
	jne	.L4605
.L4561:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-1096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4549
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4549
	.p2align 4,,10
	.p2align 3
.L4603:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4559
	.p2align 4,,10
	.p2align 3
.L4602:
	movq	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1270(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4606
.L4557:
	testb	$5, (%rsi)
	je	.L4553
	leaq	.LC98(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4553
	.p2align 4,,10
	.p2align 3
.L4540:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L4542
	.p2align 4,,10
	.p2align 3
.L4605:
	movq	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1273(%rip), %rsi
	testq	%rsi, %rsi
	je	.L4607
.L4565:
	testb	$5, (%rsi)
	je	.L4561
	leaq	.LC98(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L4561
	.p2align 4,,10
	.p2align 3
.L4595:
	leaq	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4548:
	cltq
	leaq	-1584(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s
	movq	-1576(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L4608
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L4550:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	jmp	.L4549
	.p2align 4,,10
	.p2align 3
.L4601:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4554
	.p2align 4,,10
	.p2align 3
.L4604:
	leaq	.LC6(%rip), %rsi
	call	*%rdx
	jmp	.L4562
	.p2align 4,,10
	.p2align 3
.L4597:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4600:
	leaq	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4607:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1273(%rip)
	mfence
	jmp	.L4565
	.p2align 4,,10
	.p2align 3
.L4606:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1270(%rip)
	mfence
	jmp	.L4557
	.p2align 4,,10
	.p2align 3
.L4608:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L4550
.L4598:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7913:
	.size	_ZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L4636
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L4627
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L4637
.L4611:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L4626:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L4638
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L4614:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L4615
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L4619
	.p2align 4,,10
	.p2align 3
.L4616:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L4635:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L4639
.L4619:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L4616
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L4635
	.p2align 4,,10
	.p2align 3
.L4639:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L4615:
	cmpq	%r12, %rbx
	je	.L4620
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L4624:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L4640
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L4624
.L4622:
	subq	%rbx, %r12
	addq	%r12, %r8
.L4620:
	testq	%r15, %r15
	je	.L4625
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L4625:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4640:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L4624
	jmp	.L4622
	.p2align 4,,10
	.p2align 3
.L4637:
	testq	%r8, %r8
	jne	.L4612
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L4626
	.p2align 4,,10
	.p2align 3
.L4627:
	movl	$32, %esi
	jmp	.L4611
	.p2align 4,,10
	.p2align 3
.L4638:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L4614
.L4612:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L4611
.L4636:
	leaq	.LC89(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9409:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.text
	.p2align 4
	.globl	_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E
	.type	_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E, @function
_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E:
.LFB7917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-88(%rsi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	528(%r15), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L4668
.L4642:
	movq	40(%rdx), %rax
	cmpq	%rax, 32(%rdx)
	je	.L4669
	leaq	-80(%rbp), %rbx
	leaq	-16(%rax), %rcx
	movq	%rbx, -96(%rbp)
	movq	-32(%rax), %rsi
	cmpq	%rcx, %rsi
	je	.L4670
	movq	%rsi, -96(%rbp)
	movq	-16(%rax), %rsi
	movq	%rsi, -80(%rbp)
.L4653:
	movq	-24(%rax), %rsi
	movq	%rsi, -88(%rbp)
	movq	%rcx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	40(%rdx), %rax
	movq	-32(%rax), %rdi
	leaq	-32(%rax), %rcx
	subq	$16, %rax
	movq	%rcx, 40(%rdx)
	cmpq	%rax, %rdi
	je	.L4654
	call	_ZdlPv@PLT
.L4654:
	movq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%r13d, %ecx
	leaq	_ZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENUlS4_E_4_FUNES4_(%rip), %r8
	call	uv_fs_mkdir@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %r12d
	cmpq	%rbx, %rdi
	je	.L4641
	call	_ZdlPv@PLT
.L4641:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4671
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4670:
	.cfi_restore_state
	movdqu	-16(%rax), %xmm1
	movaps	%xmm1, -80(%rbp)
	jmp	.L4653
	.p2align 4,,10
	.p2align 3
.L4668:
	movl	$88, %edi
	movq	%r9, -120(%rbp)
	movq	%r8, %rbx
	call	_Znwm@PLT
	movq	528(%r15), %rdi
	pxor	%xmm0, %xmm0
	movq	-120(%rbp), %r9
	movq	%rax, %rcx
	leaq	16+_ZTVN4node2fs18FSContinuationDataE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rdi, %rdi
	leaq	72(%rcx), %rax
	movq	%rbx, 8(%rcx)
	movq	%r12, 16(%rcx)
	movl	%r13d, 24(%rcx)
	movq	$0, 48(%rcx)
	movq	%rax, 56(%rcx)
	movq	$0, 64(%rcx)
	movb	$0, 72(%rcx)
	movq	%rcx, 528(%r15)
	movups	%xmm0, 32(%rcx)
	je	.L4643
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	528(%r15), %rcx
	movq	-120(%rbp), %r9
.L4643:
	leaq	-96(%rbp), %rax
	leaq	-80(%rbp), %rbx
	movq	%rax, -136(%rbp)
	movq	%rbx, -96(%rbp)
	testq	%r9, %r9
	jne	.L4672
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L4672:
	movq	%r9, %rdi
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	strlen@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %rcx
	cmpq	$15, %rax
	movq	%rax, -104(%rbp)
	movq	%rax, %r8
	ja	.L4673
	cmpq	$1, %rax
	jne	.L4645
	movzbl	(%r9), %edx
	movb	%dl, -80(%rbp)
	movq	%rbx, %rdx
.L4646:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	40(%rcx), %rsi
	cmpq	48(%rcx), %rsi
	je	.L4647
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4674
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
.L4649:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$32, 40(%rcx)
.L4650:
	movq	528(%r15), %rdx
	jmp	.L4642
	.p2align 4,,10
	.p2align 3
.L4669:
	leaq	_ZZN4node2fs18FSContinuationData7PopPathB5cxx11EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4647:
	movq	-136(%rbp), %rdx
	leaq	32(%rcx), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4650
	call	_ZdlPv@PLT
	jmp	.L4650
	.p2align 4,,10
	.p2align 3
.L4645:
	testq	%r8, %r8
	jne	.L4675
	movq	%rbx, %rdx
	jmp	.L4646
	.p2align 4,,10
	.p2align 3
.L4673:
	movq	-136(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -144(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	-144(%rbp), %r8
	movq	%rax, -80(%rbp)
.L4644:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%rcx, -120(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	-120(%rbp), %rcx
	jmp	.L4646
	.p2align 4,,10
	.p2align 3
.L4674:
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L4649
.L4671:
	call	__stack_chk_fail@PLT
.L4675:
	movq	%rbx, %rdi
	jmp	.L4644
	.cfi_endproc
.LFE7917:
	.size	_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E, .-_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E
	.p2align 4
	.type	_ZZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENKUlS4_E_clES4_ENUlS4_E_4_FUNES4_, @function
_ZZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENKUlS4_E_clES4_ENUlS4_E_4_FUNES4_:
.LFB7921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-88(%rdi), %rbx
	subq	$88, %rsp
	movq	528(%rbx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$-17, (%rdi)
	movq	88(%rdi), %rax
	je	.L4693
.L4677:
	testl	%eax, %eax
	je	.L4680
	cltq
.L4679:
	movq	16(%rdx), %rdi
	movq	8(%rdx), %rcx
	movq	%rax, 88(%rdi)
	call	*%rcx
.L4676:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4694
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4680:
	.cfi_restore_state
	movq	120(%r12), %rax
	movq	$-17, %rcx
	andl	$61440, %eax
	cmpq	$16384, %rax
	movl	$0, %eax
	cmovne	%rcx, %rax
	jmp	.L4679
	.p2align 4,,10
	.p2align 3
.L4693:
	movq	40(%rdx), %rsi
	cmpq	%rsi, 32(%rdx)
	je	.L4677
	testl	%eax, %eax
	jne	.L4695
	movq	120(%rdi), %rcx
	movq	$-20, %rax
	andl	$61440, %ecx
	cmpq	$16384, %rcx
	jne	.L4679
	movq	16(%rbx), %rax
	movq	104(%rdi), %r15
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %r14
	leaq	-96(%rbp), %rax
	movq	%rax, -120(%rbp)
	testq	%r15, %r15
	je	.L4696
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L4697
	cmpq	$1, %rax
	jne	.L4684
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L4685:
	movq	%rax, -88(%rbp)
	movq	%r12, %rdi
	movb	$0, (%rdx,%rax)
	call	uv_fs_req_cleanup@PLT
	movq	-96(%rbp), %rdx
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movq	528(%rbx), %rax
	movq	%r12, %rsi
	movl	24(%rax), %ecx
	call	_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4676
	call	_ZdlPv@PLT
	jmp	.L4676
	.p2align 4,,10
	.p2align 3
.L4695:
	movq	$-20, %rax
	jmp	.L4679
.L4684:
	testq	%rax, %rax
	jne	.L4698
	movq	%r13, %rdx
	jmp	.L4685
.L4697:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L4683:
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L4685
.L4696:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4694:
	call	__stack_chk_fail@PLT
.L4698:
	movq	%r13, %rdi
	jmp	.L4683
	.cfi_endproc
.LFE7921:
	.size	_ZZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENKUlS4_E_clES4_ENUlS4_E_4_FUNES4_, .-_ZZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENKUlS4_E_clES4_ENUlS4_E_4_FUNES4_
	.p2align 4
	.type	_ZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENUlS4_E_4_FUNES4_, @function
_ZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENUlS4_E_4_FUNES4_:
.LFB7923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-88(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	subq	$136, %rsp
	movq	104(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r13), %rax
	movq	%rbx, -128(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rax
	movq	%rax, -152(%rbp)
	testq	%r15, %r15
	je	.L4714
	movq	%rdi, %r12
	movq	%r15, %rdi
	leaq	-128(%rbp), %r14
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L4748
	cmpq	$1, %rax
	jne	.L4703
	movzbl	(%r15), %edx
	movb	%dl, -112(%rbp)
	movq	%rbx, %rdx
.L4704:
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	88(%r12), %rdx
	leal	20(%rdx), %eax
	cmpl	$20, %eax
	ja	.L4705
	leaq	.L4707(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4707:
	.long	.L4708-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4708-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4705-.L4707
	.long	.L4709-.L4707
	.long	.L4708-.L4707
	.long	.L4706-.L4707
	.text
	.p2align 4,,10
	.p2align 3
.L4705:
	movslq	%edx, %r14
.L4731:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	%r14, (%r12)
	movq	-128(%rbp), %rdx
	movq	%r12, %rsi
	movq	-152(%rbp), %rdi
	leaq	_ZZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENKUlS4_E_clES4_ENUlS4_E_4_FUNES4_(%rip), %rcx
	call	uv_fs_stat@PLT
	testl	%eax, %eax
	js	.L4749
	.p2align 4,,10
	.p2align 3
.L4712:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4699
	call	_ZdlPv@PLT
.L4699:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4750
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4703:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L4751
	movq	%rbx, %rdx
	jmp	.L4704
	.p2align 4,,10
	.p2align 3
.L4708:
	movq	528(%r13), %rax
	movslq	%edx, %rdx
	movq	16(%rax), %rdi
	movq	8(%rax), %rcx
	movq	%rdx, 88(%rdi)
	call	*%rcx
	jmp	.L4712
	.p2align 4,,10
	.p2align 3
.L4709:
	movq	$-1, %rdx
	movl	$47, %esi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5rfindEcm@PLT
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %r10
	leaq	-80(%rbp), %r9
	movq	%r9, -96(%rbp)
	cmpq	%r8, %rax
	cmovbe	%rax, %r8
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L4737
	testq	%r10, %r10
	je	.L4714
.L4737:
	movq	%r8, -136(%rbp)
	cmpq	$15, %r8
	ja	.L4752
	cmpq	$1, %r8
	jne	.L4718
	movzbl	(%r10), %eax
	movb	%al, -80(%rbp)
	movq	%r9, %rax
.L4719:
	movq	%r8, -88(%rbp)
	movb	$0, (%rax,%r8)
	movq	-88(%rbp), %rdx
	cmpq	-120(%rbp), %rdx
	je	.L4753
.L4720:
	movq	528(%r13), %rax
	movq	40(%rax), %rsi
	movq	%rax, %rdx
	cmpq	48(%rax), %rsi
	je	.L4722
	leaq	16(%rsi), %rcx
	movq	%rcx, (%rsi)
	movq	-128(%rbp), %rcx
	cmpq	%rbx, %rcx
	je	.L4754
	movq	%rcx, (%rsi)
	movq	-112(%rbp), %rcx
	movq	%rcx, 16(%rsi)
.L4724:
	movq	-120(%rbp), %rcx
	movq	%rbx, -128(%rbp)
	movq	%rcx, 8(%rsi)
	addq	$32, 40(%rdx)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
.L4725:
	movq	40(%rax), %rsi
	cmpq	48(%rax), %rsi
	je	.L4726
	leaq	16(%rsi), %rdx
	movq	%rdx, (%rsi)
	movq	-96(%rbp), %rdx
	cmpq	%r9, %rdx
	je	.L4755
	movq	%rdx, (%rsi)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rsi)
.L4728:
	movq	-88(%rbp), %rdx
	movq	%r9, -96(%rbp)
	movq	%rdx, 8(%rsi)
	addq	$32, 40(%rax)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	jmp	.L4729
	.p2align 4,,10
	.p2align 3
.L4706:
	movq	528(%r13), %rdi
	movq	64(%rdi), %rax
	movq	40(%rdi), %rsi
	cmpq	%rsi, 32(%rdi)
	je	.L4756
	testq	%rax, %rax
	je	.L4757
.L4713:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-128(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	528(%r13), %rax
	movq	-152(%rbp), %rdi
	movl	24(%rax), %ecx
	call	_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E
	jmp	.L4712
	.p2align 4,,10
	.p2align 3
.L4748:
	movq	%r14, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L4702:
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L4704
	.p2align 4,,10
	.p2align 3
.L4714:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L4749:
	movq	528(%r13), %rdx
	cltq
	movq	16(%rdx), %rdi
	movq	8(%rdx), %rcx
	movq	%rax, 88(%rdi)
	call	*%rcx
	jmp	.L4712
	.p2align 4,,10
	.p2align 3
.L4752:
	leaq	-96(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%r10, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r10
	movq	-168(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	-176(%rbp), %r9
	movq	%rax, -80(%rbp)
.L4717:
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r9, -160(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r8
	movq	-96(%rbp), %rax
	movq	-160(%rbp), %r9
	jmp	.L4719
	.p2align 4,,10
	.p2align 3
.L4756:
	testq	%rax, %rax
	je	.L4758
.L4711:
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdi
	movq	$0, 88(%rdi)
	call	*%rax
	jmp	.L4712
	.p2align 4,,10
	.p2align 3
.L4753:
	movq	-96(%rbp), %rdi
	testq	%rdx, %rdx
	je	.L4721
	movq	-128(%rbp), %rsi
	movq	%r9, -168(%rbp)
	movq	%rdi, -160(%rbp)
	call	memcmp@PLT
	movq	-160(%rbp), %rdi
	movq	-168(%rbp), %r9
	testl	%eax, %eax
	jne	.L4720
.L4721:
	movq	528(%r13), %rax
	movq	40(%rax), %rsi
	cmpq	%rsi, 32(%rax)
	je	.L4759
.L4729:
	movq	%r12, %rdi
	movq	%r9, -160(%rbp)
	call	uv_fs_req_cleanup@PLT
	movq	-128(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	528(%r13), %rax
	movq	-152(%rbp), %rdi
	movl	24(%rax), %ecx
	call	_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E
	movq	-96(%rbp), %rdi
	movq	-160(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L4712
	call	_ZdlPv@PLT
	jmp	.L4712
	.p2align 4,,10
	.p2align 3
.L4718:
	testq	%r8, %r8
	jne	.L4760
	movq	%r9, %rax
	jmp	.L4719
	.p2align 4,,10
	.p2align 3
.L4757:
	addq	$56, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L4713
	.p2align 4,,10
	.p2align 3
.L4754:
	movdqa	-112(%rbp), %xmm0
	movups	%xmm0, 16(%rsi)
	movq	528(%r13), %rax
	jmp	.L4724
	.p2align 4,,10
	.p2align 3
.L4759:
	cmpq	%r9, %rdi
	je	.L4730
	call	_ZdlPv@PLT
.L4730:
	movq	$-17, %r14
	jmp	.L4731
	.p2align 4,,10
	.p2align 3
.L4758:
	addq	$56, %rdi
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	528(%r13), %rdi
	jmp	.L4711
	.p2align 4,,10
	.p2align 3
.L4755:
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L4728
	.p2align 4,,10
	.p2align 3
.L4726:
	leaq	-96(%rbp), %rdx
	leaq	32(%rax), %rdi
	movq	%r9, -160(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-160(%rbp), %r9
	jmp	.L4729
	.p2align 4,,10
	.p2align 3
.L4722:
	leaq	32(%rax), %rdi
	movq	%r14, %rdx
	movq	%r9, -160(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	528(%r13), %rax
	movq	-160(%rbp), %r9
	jmp	.L4725
.L4750:
	call	__stack_chk_fail@PLT
.L4751:
	movq	%rbx, %rdi
	jmp	.L4702
.L4760:
	movq	%r9, %rdi
	jmp	.L4717
	.cfi_endproc
.LFE7923:
	.size	_ZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENUlS4_E_4_FUNES4_, .-_ZZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_EENUlS4_E_4_FUNES4_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L4803
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L4783
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L4804
	movabsq	$9223372036854775776, %rcx
.L4763:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L4781:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L4765
	testq	%r10, %r10
	je	.L4805
.L4765:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L4806
	cmpq	$1, %r9
	jne	.L4768
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L4769:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L4785
.L4810:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L4774
	.p2align 4,,10
	.p2align 3
.L4771:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L4801:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L4807
.L4774:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L4771
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L4801
	.p2align 4,,10
	.p2align 3
.L4804:
	testq	%rcx, %rcx
	jne	.L4764
	xorl	%r13d, %r13d
	jmp	.L4781
	.p2align 4,,10
	.p2align 3
.L4807:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L4770:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L4775
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L4779:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L4808
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L4779
.L4777:
	subq	%rbx, %r12
	addq	%r12, %r8
.L4775:
	testq	%r14, %r14
	je	.L4780
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L4780:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4809
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4808:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L4779
	jmp	.L4777
	.p2align 4,,10
	.p2align 3
.L4783:
	movl	$32, %ecx
	jmp	.L4763
	.p2align 4,,10
	.p2align 3
.L4768:
	testq	%r9, %r9
	jne	.L4767
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L4810
.L4785:
	movq	%r13, %r8
	jmp	.L4770
	.p2align 4,,10
	.p2align 3
.L4806:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %rdi
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L4767:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L4769
.L4805:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4764:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L4763
.L4809:
	call	__stack_chk_fail@PLT
.L4803:
	leaq	.LC89(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9414:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB9557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movabsq	$1152921504606846975, %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -88(%rbp)
	movq	%rax, -80(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L4839
	movq	%rsi, %r15
	movq	%rsi, %rbx
	movq	%rsi, %r13
	subq	%r12, %r15
	testq	%rax, %rax
	je	.L4825
	leaq	(%rax,%rax), %rcx
	movq	%rcx, -72(%rbp)
	cmpq	%rcx, %rax
	jbe	.L4840
	movabsq	$9223372036854775800, %rax
	movq	%rax, -72(%rbp)
.L4813:
	movq	-72(%rbp), %rdi
	call	_Znwm@PLT
	movq	%rax, -64(%rbp)
.L4824:
	movq	(%r14), %rax
	movq	-64(%rbp), %rcx
	movq	$0, (%r14)
	movq	%rax, (%rcx,%r15)
	movq	%rcx, %r15
	cmpq	%r12, %rbx
	je	.L4815
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L4818:
	movq	(%r14), %rsi
	movq	$0, (%r14)
	movq	%rsi, (%r15)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4816
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L4841
	movq	72(%rdi), %rsi
	movq	64(%rdi), %r9
	movq	%rdi, -56(%rbp)
	movq	%rsi, 8(%r9)
	movq	%r9, (%rsi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movq	-56(%rbp), %rdi
	movl	$552, %esi
	call	_ZdlPvm@PLT
.L4816:
	addq	$8, %r14
	addq	$8, %r15
	cmpq	%r14, %rbx
	jne	.L4818
.L4815:
	movq	-80(%rbp), %rax
	leaq	8(%r15), %r14
	cmpq	%rax, %rbx
	je	.L4819
	subq	%rbx, %rax
	leaq	-8(%rax), %rdi
	movq	%rdi, %r9
	shrq	$3, %r9
	addq	$1, %r9
	testq	%rdi, %rdi
	je	.L4828
	movq	%r9, %rsi
	xorl	%eax, %eax
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L4821:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, 8(%r15,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L4821
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%r14,%r8), %rdx
	leaq	(%rbx,%r8), %r13
	cmpq	%rax, %r9
	je	.L4822
.L4820:
	movq	0(%r13), %rax
	movq	%rax, (%rdx)
.L4822:
	leaq	8(%r14,%rdi), %r14
.L4819:
	testq	%r12, %r12
	je	.L4823
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4823:
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r14, %xmm2
	movq	%rax, %xmm0
	addq	-72(%rbp), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%rdx)
	movups	%xmm0, (%rdx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4840:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L4814
	movq	$0, -64(%rbp)
	jmp	.L4824
	.p2align 4,,10
	.p2align 3
.L4825:
	movq	$8, -72(%rbp)
	jmp	.L4813
	.p2align 4,,10
	.p2align 3
.L4841:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4828:
	movq	%r14, %rdx
	jmp	.L4820
.L4839:
	leaq	.LC89(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4814:
	movq	-72(%rbp), %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	salq	$3, %rax
	movq	%rax, -72(%rbp)
	jmp	.L4813
	.cfi_endproc
.LFE9557:
	.size	_ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.text
	.p2align 4
	.type	_ZZN4node2fs10FileHandle9ReadStartEvENUlP7uv_fs_sE_4_FUNES3_, @function
_ZZN4node2fs10FileHandle9ReadStartEvENUlP7uv_fs_sE_4_FUNES3_:
.LFB7837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-88(%rdi), %rax
	movq	528(%rax), %r12
	cmpq	152(%r12), %rax
	jne	.L4861
	movdqu	536(%rax), %xmm0
	movq	88(%rdi), %rbx
	movq	%rax, -72(%rbp)
	movq	$0, 152(%r12)
	movaps	%xmm0, -64(%rbp)
	movl	%ebx, %r13d
	call	uv_fs_req_cleanup@PLT
	movq	16(%r12), %rdi
	movq	2352(%rdi), %rax
	subq	2344(%rdi), %rax
	cmpq	$799, %rax
	jbe	.L4862
.L4844:
	movq	64(%r12), %rdi
	testl	%ebx, %ebx
	js	.L4846
	movq	136(%r12), %rax
	testq	%rax, %rax
	js	.L4847
	movslq	%ebx, %rbx
	cmpq	%rbx, %rax
	jge	.L4848
	movl	%eax, %r13d
	movq	%rax, %rbx
.L4848:
	subq	%rbx, %rax
	movq	%rax, 136(%r12)
.L4847:
	movq	128(%r12), %rax
	testq	%rax, %rax
	js	.L4849
	movslq	%r13d, %rdx
	addq	%rdx, %rax
	movq	%rax, 128(%r12)
.L4849:
	movq	$-4095, %rsi
	testl	%r13d, %r13d
	jne	.L4846
.L4850:
	movq	(%rdi), %rax
	leaq	-64(%rbp), %rdx
	call	*24(%rax)
	cmpb	$0, 144(%r12)
	jne	.L4863
.L4851:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L4842
	leaq	16+_ZTVN4node7ReqWrapI7uv_fs_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L4864
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movl	$552, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4842:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4865
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4846:
	.cfi_restore_state
	movslq	%r13d, %rsi
	testq	%rsi, %rsi
	jle	.L4850
	addq	%rsi, 72(%r12)
	jmp	.L4850
	.p2align 4,,10
	.p2align 3
.L4863:
	movq	%r12, %rdi
	call	_ZN4node2fs10FileHandle9ReadStartEv
	jmp	.L4851
	.p2align 4,,10
	.p2align 3
.L4862:
	movq	-72(%rbp), %rax
	movq	$0, 88(%rax)
	movq	2352(%rdi), %rsi
	movq	$0, 80(%rax)
	cmpq	2360(%rdi), %rsi
	je	.L4845
	movq	$0, -72(%rbp)
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 2352(%rdi)
	jmp	.L4844
	.p2align 4,,10
	.p2align 3
.L4861:
	leaq	_ZZZN4node2fs10FileHandle9ReadStartEvENKUlP7uv_fs_sE_clES3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4845:
	leaq	-72(%rbp), %rdx
	addq	$2344, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN4node2fs18FileHandleReadWrapESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L4844
	.p2align 4,,10
	.p2align 3
.L4864:
	leaq	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7837:
	.size	_ZZN4node2fs10FileHandle9ReadStartEvENUlP7uv_fs_sE_4_FUNES3_, .-_ZZN4node2fs10FileHandle9ReadStartEvENUlP7uv_fs_sE_4_FUNES3_
	.section	.text._ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_:
.LFB9585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L4867
	movq	(%rsi), %rax
	addq	$8, %r12
	movq	%rax, -8(%r12)
	movq	%r12, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4867:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L4893
	testq	%rax, %rax
	je	.L4878
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4894
.L4870:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L4871:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L4872
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L4881
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L4881
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L4874:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L4874
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L4876
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L4876:
	leaq	16(%r13,%rsi), %rax
.L4872:
	testq	%r14, %r14
	je	.L4877
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L4877:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4894:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L4895
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L4871
	.p2align 4,,10
	.p2align 3
.L4878:
	movl	$8, %r15d
	jmp	.L4870
	.p2align 4,,10
	.p2align 3
.L4881:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L4873:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L4873
	jmp	.L4876
.L4893:
	leaq	.LC89(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4895:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L4870
	.cfi_endproc
.LFE9585:
	.size	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB9591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L4923
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L4908
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4924
.L4898:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L4907:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L4900
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L4910
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L4910
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L4902:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L4902
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L4904
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L4904:
	leaq	16(%rbx,%rsi), %r14
.L4900:
	cmpq	%rcx, %r13
	je	.L4905
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L4905:
	testq	%r12, %r12
	je	.L4906
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4906:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4924:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L4899
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L4907
	.p2align 4,,10
	.p2align 3
.L4908:
	movl	$8, %r14d
	jmp	.L4898
	.p2align 4,,10
	.p2align 3
.L4910:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L4901:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L4901
	jmp	.L4904
.L4899:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L4898
.L4923:
	leaq	.LC89(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9591:
	.size	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.text
	.p2align 4
	.globl	_ZN4node2fs21AfterScanDirWithTypesEP7uv_fs_s
	.type	_ZN4node2fs21AfterScanDirWithTypesEP7uv_fs_s, @function
_ZN4node2fs21AfterScanDirWithTypesEP7uv_fs_s:
.LFB7878:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-88(%rdi), %r15
	pushq	%r14
	movq	%r15, %rsi
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	16(%r15), %rax
	leaq	-112(%rbp), %rdi
	movq	%r14, -120(%rbp)
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r15), %rax
	movq	3280(%rax), %rdi
	movq	%rdi, -88(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-128(%rbp), %rax
	addq	$88, %rax
	cmpq	%rax, %r14
	jne	.L4985
	movq	-120(%rbp), %rsi
	cmpq	$0, 88(%rsi)
	js	.L4927
	movq	16(%r15), %rax
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	movq	$0, -192(%rbp)
	movq	352(%rax), %r13
	leaq	-176(%rbp), %rax
	movq	$0, -144(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-192(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	%rax, -216(%rbp)
	movaps	%xmm0, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L4928:
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	uv_fs_scandir_next@PLT
	cmpl	$-4095, %eax
	je	.L4930
	testl	%eax, %eax
	jne	.L4986
	movl	536(%r15), %edx
	movq	-216(%rbp), %rcx
	movq	%r13, %rdi
	movq	-176(%rbp), %rsi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4987
	cmpq	-200(%rbp), %rbx
	je	.L4934
	movq	%rax, (%rbx)
	addq	$8, %rbx
.L4935:
	movl	-168(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, -184(%rbp)
	cmpq	-144(%rbp), %rsi
	je	.L4945
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, -152(%rbp)
	jmp	.L4928
	.p2align 4,,10
	.p2align 3
.L4934:
	movabsq	$1152921504606846975, %rdi
	movq	%rbx, %rcx
	subq	%r12, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L4988
	testq	%rax, %rax
	je	.L4951
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4989
.L4937:
	movq	%rsi, %rdi
	movq	%rcx, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rsi, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %rsi
	movq	-232(%rbp), %rdx
	leaq	(%rax,%rsi), %rcx
	leaq	8(%rax), %rsi
	movq	%rcx, -200(%rbp)
	movq	-240(%rbp), %rcx
	movq	%rdx, (%rax,%rcx)
	cmpq	%r12, %rbx
	je	.L4954
.L4991:
	leaq	-8(%rbx), %rsi
	leaq	15(%rax), %rdx
	subq	%r12, %rsi
	subq	%r12, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L4955
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L4955
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L4941:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L4941
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%r12,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %r8
	je	.L4943
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L4943:
	leaq	16(%rax,%rsi), %rbx
.L4939:
	testq	%r12, %r12
	je	.L4944
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rax
.L4944:
	movq	%rax, %r12
	jmp	.L4935
	.p2align 4,,10
	.p2align 3
.L4945:
	leaq	-184(%rbp), %rdx
	leaq	-160(%rbp), %rdi
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L4928
	.p2align 4,,10
	.p2align 3
.L4989:
	testq	%rdi, %rdi
	jne	.L4990
	xorl	%eax, %eax
	movl	$8, %esi
	movq	$0, -200(%rbp)
	movq	%rdx, (%rax,%rcx)
	cmpq	%r12, %rbx
	jne	.L4991
.L4954:
	movq	%rsi, %rbx
	jmp	.L4939
	.p2align 4,,10
	.p2align 3
.L4951:
	movl	$8, %esi
	jmp	.L4937
	.p2align 4,,10
	.p2align 3
.L4930:
	subq	%r12, %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	sarq	$3, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-160(%rbp), %rsi
	movq	-152(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	subq	%rsi, %rdx
	sarq	$3, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdi
	movl	$2, %edx
	movq	%rax, -72(%rbp)
	movq	-88(%r14), %rax
	movq	104(%rax), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	*%rbx
.L4984:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4947
	call	_ZdlPv@PLT
.L4947:
	testq	%r12, %r12
	je	.L4929
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4929:
	movq	-224(%rbp), %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4992
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4986:
	.cfi_restore_state
	movq	-88(%r14), %rdx
	movq	544(%r15), %rcx
	movq	%r13, %rdi
	movl	%eax, %esi
	movq	104(%r14), %r8
	xorl	%r9d, %r9d
	movq	96(%rdx), %rbx
	xorl	%edx, %edx
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	*%rbx
	jmp	.L4984
	.p2align 4,,10
	.p2align 3
.L4987:
	movq	-88(%r14), %rax
	movq	-192(%rbp), %rsi
	movq	%r15, %rdi
	call	*96(%rax)
	jmp	.L4984
	.p2align 4,,10
	.p2align 3
.L4955:
	movq	%rax, %rcx
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L4940:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%rbx, %rdx
	jne	.L4940
	jmp	.L4943
	.p2align 4,,10
	.p2align 3
.L4927:
	movq	-224(%rbp), %rdi
	call	_ZN4node2fs15FSReqAfterScope6RejectEP7uv_fs_s
	jmp	.L4929
	.p2align 4,,10
	.p2align 3
.L4985:
	leaq	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4988:
	leaq	.LC89(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4990:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %rsi
	cmovbe	%rdi, %rsi
	salq	$3, %rsi
	jmp	.L4937
.L4992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7878:
	.size	_ZN4node2fs21AfterScanDirWithTypesEP7uv_fs_s, .-_ZN4node2fs21AfterScanDirWithTypesEP7uv_fs_s
	.section	.rodata.str1.1
.LC99:
	.string	"scandir"
.LC100:
	.string	"fs.sync.readdir"
	.text
	.p2align 4
	.type	_ZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1656, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L5062
	cmpw	$1040, %cx
	jne	.L4994
.L5062:
	movq	23(%rdx), %r14
.L4996:
	movl	16(%r12), %r15d
	movq	352(%r14), %r13
	cmpl	$2, %r15d
	jg	.L5102
	leaq	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5102:
	movq	8(%r12), %rdx
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L5103
	cmpl	$1, 16(%r12)
	jle	.L5104
	movq	8(%r12), %rax
	leaq	-8(%rax), %rsi
.L4999:
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %ebx
	jg	.L5000
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L5001:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$3, 16(%r12)
	movb	%al, -1672(%rbp)
	jg	.L5002
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L5004
.L5106:
	leaq	.LC99(%rip), %rcx
	cmpb	$0, -1672(%rbp)
	movq	80(%rax), %rax
	movl	%ebx, 536(%r9)
	movq	%rcx, 544(%r9)
	movq	-1088(%rbp), %rdx
	movq	%r9, 88(%r9)
	je	.L5005
	testq	%rax, %rax
	jne	.L5009
	leaq	_ZN4node2fs21AfterScanDirWithTypesEP7uv_fs_s(%rip), %rax
	leaq	88(%r9), %r13
	xorl	%ecx, %ecx
	movq	%r9, -1672(%rbp)
	movq	%rax, 80(%r9)
	movq	16(%r9), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_scandir@PLT
	movq	-1672(%rbp), %r9
	testl	%eax, %eax
	js	.L5007
.L5100:
	movq	16(%r9), %rax
	movq	%r12, %rsi
	movq	%r9, %rdi
	addl	$1, 2156(%rax)
	movq	(%r9), %rax
	call	*120(%rax)
.L5008:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4993
	testq	%rdi, %rdi
	je	.L4993
.L5101:
	call	free@PLT
.L4993:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5105
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5104:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L4999
	.p2align 4,,10
	.p2align 3
.L5002:
	movq	8(%r12), %rax
	movq	%r14, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L5106
.L5004:
	cmpl	$5, %r15d
	jne	.L5107
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L5108
.L5013:
	cmpl	$4, 16(%r12)
	movq	-1088(%rbp), %r9
	jle	.L5109
	movq	8(%r12), %rax
	leaq	-32(%rax), %rsi
.L5018:
	subq	$8, %rsp
	movq	uv_fs_scandir@GOTPCREL(%rip), %r8
	movq	%r14, %rdi
	leaq	-1552(%rbp), %r15
	pushq	$0
	leaq	.LC99(%rip), %rcx
	movq	%r15, %rdx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	movl	%eax, -1680(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	popq	%rdx
	popq	%rcx
	cmpb	$0, (%rax)
	jne	.L5110
.L5020:
	movl	-1680(%rbp), %eax
	testl	%eax, %eax
	js	.L5024
	cmpq	$0, -1464(%rbp)
	js	.L5111
	pxor	%xmm0, %xmm0
	leaq	-1648(%rbp), %rax
	cmpb	$0, -1672(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1672(%rbp)
	leaq	-1664(%rbp), %rax
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1632(%rbp)
	movaps	%xmm0, -1600(%rbp)
	jne	.L5112
	movq	%rax, -1680(%rbp)
	jmp	.L5030
	.p2align 4,,10
	.p2align 3
.L5113:
	testl	%eax, %eax
	jne	.L5028
	movq	-1680(%rbp), %rcx
	movq	-1648(%rbp), %rsi
	movl	%ebx, %edx
	movq	%r13, %rdi
	movq	$0, -1664(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L5029
	leaq	-1656(%rbp), %rsi
	leaq	-1632(%rbp), %rdi
	movq	%rax, -1656(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
.L5030:
	movq	-1672(%rbp), %rsi
	movq	%r15, %rdi
	call	uv_fs_scandir_next@PLT
	cmpl	$-4095, %eax
	jne	.L5113
	movq	-1632(%rbp), %rsi
	movq	-1624(%rbp), %rdx
	movq	%r13, %rdi
	subq	%rsi, %rdx
	sarq	$3, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	(%r12), %rdx
	testq	%rax, %rax
	je	.L5114
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
.L5048:
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5049
	call	_ZdlPv@PLT
.L5049:
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5050
	call	_ZdlPv@PLT
.L5050:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L5008
	.p2align 4,,10
	.p2align 3
.L5000:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L5001
	.p2align 4,,10
	.p2align 3
.L5005:
	testq	%rax, %rax
	jne	.L5009
	leaq	_ZN4node2fs12AfterScanDirEP7uv_fs_s(%rip), %rax
	leaq	88(%r9), %r13
	xorl	%ecx, %ecx
	movq	%r9, -1672(%rbp)
	movq	%rax, 80(%r9)
	movq	16(%r9), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_scandir@PLT
	movq	-1672(%rbp), %r9
	testl	%eax, %eax
	jns	.L5100
	movq	$0, 192(%r9)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r9)
	call	_ZN4node2fs12AfterScanDirEP7uv_fs_s
	jmp	.L5008
	.p2align 4,,10
	.p2align 3
.L5109:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L5018
	.p2align 4,,10
	.p2align 3
.L4994:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L4996
	.p2align 4,,10
	.p2align 3
.L5007:
	movq	$0, 192(%r9)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r9)
	call	_ZN4node2fs21AfterScanDirWithTypesEP7uv_fs_s
	jmp	.L5008
	.p2align 4,,10
	.p2align 3
.L5108:
	movq	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1577(%rip), %rsi
	testq	%rsi, %rsi
	je	.L5115
.L5015:
	testb	$5, (%rsi)
	je	.L5013
	leaq	.LC100(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L5013
	.p2align 4,,10
	.p2align 3
.L5028:
	cmpl	$4, 16(%r12)
	jg	.L5033
	movq	(%r12), %rdx
	movq	8(%rdx), %r12
	addq	$88, %r12
.L5034:
	movl	%eax, %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%r14), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L5116
.L5035:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L5117
.L5036:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L5099
.L5038:
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5045
	call	_ZdlPv@PLT
.L5045:
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5024
	call	_ZdlPv@PLT
.L5024:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4993
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L5101
	jmp	.L4993
	.p2align 4,,10
	.p2align 3
.L5103:
	leaq	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5110:
	movq	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1580(%rip), %rsi
	testq	%rsi, %rsi
	je	.L5118
.L5022:
	testb	$5, (%rsi)
	je	.L5020
	leaq	.LC100(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L5020
	.p2align 4,,10
	.p2align 3
.L5009:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5118:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1580(%rip)
	mfence
	jmp	.L5022
	.p2align 4,,10
	.p2align 3
.L5115:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1577(%rip)
	mfence
	jmp	.L5015
	.p2align 4,,10
	.p2align 3
.L5112:
	movq	%rax, -1688(%rbp)
.L5026:
	movq	-1672(%rbp), %rsi
	movq	%r15, %rdi
	call	uv_fs_scandir_next@PLT
	cmpl	$-4095, %eax
	je	.L5031
	testl	%eax, %eax
	jne	.L5028
	movq	-1688(%rbp), %rcx
	movq	-1648(%rbp), %rsi
	movl	%ebx, %edx
	movq	%r13, %rdi
	movq	$0, -1664(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L5029
	leaq	-1656(%rbp), %rdx
	leaq	-1632(%rbp), %rdi
	movq	%rax, -1656(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, -1680(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movl	-1640(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-1592(%rbp), %rsi
	cmpq	-1584(%rbp), %rsi
	movq	%rax, -1656(%rbp)
	movq	-1680(%rbp), %rdx
	je	.L5043
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, -1592(%rbp)
	jmp	.L5026
	.p2align 4,,10
	.p2align 3
.L5029:
	cmpl	$4, 16(%r12)
	jg	.L5040
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L5041:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	-1664(%rbp), %rcx
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L5038
.L5099:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5107:
	leaq	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5033:
	movq	8(%r12), %r12
	subq	$32, %r12
	jmp	.L5034
	.p2align 4,,10
	.p2align 3
.L5043:
	leaq	-1600(%rbp), %rdi
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE17_M_realloc_insertIJNS1_INS0_7IntegerEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L5026
	.p2align 4,,10
	.p2align 3
.L5040:
	movq	8(%r12), %rdi
	subq	$32, %rdi
	jmp	.L5041
	.p2align 4,,10
	.p2align 3
.L5031:
	movq	-1632(%rbp), %rsi
	movq	-1624(%rbp), %rdx
	movq	%r13, %rdi
	subq	%rsi, %rdx
	sarq	$3, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-1600(%rbp), %rsi
	movq	-1592(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -1568(%rbp)
	subq	%rsi, %rdx
	sarq	$3, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	leaq	-1568(%rbp), %rsi
	movq	%r13, %rdi
	movq	(%r12), %rbx
	movl	$2, %edx
	movq	%rax, -1560(%rbp)
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L5119
	movq	(%rax), %rax
.L5047:
	movq	%rax, 24(%rbx)
	jmp	.L5048
	.p2align 4,,10
	.p2align 3
.L5111:
	leaq	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L5117:
	movq	%rax, -1672(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1672(%rbp), %rcx
	jmp	.L5036
.L5116:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L5035
.L5114:
	movq	16(%rdx), %rax
	movq	%rax, 24(%rdx)
	jmp	.L5048
.L5119:
	movq	16(%rbx), %rax
	jmp	.L5047
.L5105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7928:
	.size	_ZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata._ZNSt6vectorIcSaIcEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC101:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIcSaIcEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIcSaIcEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE17_M_default_appendEm
	.type	_ZNSt6vectorIcSaIcEE17_M_default_appendEm, @function
_ZNSt6vectorIcSaIcEE17_M_default_appendEm:
.LFB9606:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L5140
	movabsq	$9223372036854775807, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %r13
	subq	%rcx, %rax
	subq	(%rdi), %r13
	subq	%r13, %rsi
	cmpq	%rbx, %rax
	jb	.L5122
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	call	memset@PLT
	addq	%rax, %rbx
	movq	%rbx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5122:
	.cfi_restore_state
	cmpq	%rbx, %rsi
	jb	.L5143
	cmpq	%r13, %rbx
	movq	%r13, %rax
	cmovnb	%rbx, %rax
	addq	%r13, %rax
	movq	%rax, %r15
	jc	.L5132
	testq	%rax, %rax
	js	.L5132
	jne	.L5126
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L5130:
	movq	%rbx, %rdx
	leaq	(%r14,%r13), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L5144
	testq	%r8, %r8
	jne	.L5128
.L5129:
	addq	%r13, %rbx
	movq	%r14, (%r12)
	addq	%rbx, %r14
	movq	%r15, 16(%r12)
	movq	%r14, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5140:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L5132:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L5126:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
	addq	%rax, %r15
	jmp	.L5130
	.p2align 4,,10
	.p2align 3
.L5144:
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L5128:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L5129
.L5143:
	leaq	.LC101(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9606:
	.size	_ZNSt6vectorIcSaIcEE17_M_default_appendEm, .-_ZNSt6vectorIcSaIcEE17_M_default_appendEm
	.section	.rodata.str1.1
.LC102:
	.string	"\357\273\277"
.LC103:
	.string	"exports"
	.text
	.p2align 4
	.type	_ZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2520, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -2544(%rbp)
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L5200
	cmpw	$1040, %cx
	jne	.L5146
.L5200:
	movq	23(%rdx), %rax
.L5148:
	movq	352(%rax), %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %r14
	movq	-2544(%rbp), %rax
	movq	%rsi, -2552(%rbp)
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L5231
	movq	8(%rax), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L5232
.L5151:
	leaq	_ZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5231:
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rax
	addq	$88, %rdx
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L5151
.L5232:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L5151
	movq	-2552(%rbp), %r15
	leaq	-1104(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	cmpq	-1104(%rbp), %rax
	je	.L5153
	movq	-2544(%rbp), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	testq	%rax, %rax
	je	.L5227
.L5157:
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L5155:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5145
.L5230:
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5145
	call	free@PLT
.L5145:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5233
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5153:
	.cfi_restore_state
	leaq	-2448(%rbp), %r15
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	uv_fs_open@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	uv_fs_req_cleanup@PLT
	testl	%r13d, %r13d
	js	.L5234
	leaq	-2528(%rbp), %rsi
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movq	%rsi, -2536(%rbp)
	leaq	-2512(%rbp), %rsi
	xorl	%ebx, %ebx
	leaq	-1552(%rbp), %r12
	movq	$0, -2496(%rbp)
	movq	%rsi, -2560(%rbp)
	movaps	%xmm0, -2512(%rbp)
	jmp	.L5166
	.p2align 4,,10
	.p2align 3
.L5158:
	addq	%rcx, %r8
	cmpq	%rax, %r8
	je	.L5159
	movq	%r8, -2504(%rbp)
.L5159:
	subq	$8, %rsp
	movl	%r13d, %edx
	movq	%rbx, %r9
	movl	$1, %r8d
	pushq	$0
	movq	-2536(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -2528(%rbp)
	movq	$32768, -2520(%rbp)
	call	uv_fs_read@PLT
	movq	%r12, %rdi
	movslq	%eax, %r15
	call	uv_fs_req_cleanup@PLT
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	js	.L5235
	movq	-2512(%rbp), %r8
	addq	%r15, %rbx
	cmpq	$32768, %r15
	jne	.L5165
	movq	-2504(%rbp), %rax
.L5166:
	movq	%rax, %r15
	subq	%r8, %r15
	leaq	32768(%r15), %rcx
	cmpq	%r15, %rcx
	jbe	.L5158
	movq	-2560(%rbp), %rdi
	movl	$32768, %esi
	call	_ZNSt6vectorIcSaIcEE17_M_default_appendEm
	movq	-2512(%rbp), %rax
	addq	%r15, %rax
	jmp	.L5159
	.p2align 4,,10
	.p2align 3
.L5146:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L5148
	.p2align 4,,10
	.p2align 3
.L5235:
	movq	-2544(%rbp), %rax
	movq	-2552(%rbp), %rdi
	xorl	%esi, %esi
	movq	(%rax), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	testq	%rax, %rax
	je	.L5236
	movq	(%rax), %rax
.L5162:
	movq	-2512(%rbp), %rdi
	movq	%rax, 24(%rbx)
	testq	%rdi, %rdi
	je	.L5163
	call	_ZdlPv@PLT
.L5163:
	leaq	-2000(%rbp), %r12
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	jne	.L5185
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	jmp	.L5155
	.p2align 4,,10
	.p2align 3
.L5165:
	cmpq	$2, %rbx
	jle	.L5193
	cmpw	$-17425, (%r8)
	je	.L5237
.L5193:
	movq	%r8, %rsi
	xorl	%eax, %eax
.L5167:
	subq	%rax, %rbx
	addq	%rbx, %r8
	movq	%rbx, %r9
	cmpq	%rsi, %r8
	jbe	.L5195
	leaq	-2480(%rbp), %rcx
	movzbl	(%rsi), %eax
	movq	%rsi, %rdx
	leaq	-2464(%rbp), %r10
	movq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L5171:
	leaq	1(%rdx), %r15
	cmpb	$92, %al
	jne	.L5172
.L5239:
	cmpq	%r8, %r15
	jnb	.L5172
	movzbl	1(%rdx), %eax
	cmpb	$34, %al
	je	.L5238
	movq	%r15, %rdx
	leaq	1(%rdx), %r15
	cmpb	$92, %al
	je	.L5239
	.p2align 4,,10
	.p2align 3
.L5172:
	cmpb	$34, %al
	jne	.L5174
	leaq	8(%rdi), %rax
	movq	%r15, (%rdi)
	cmpq	%r10, %rax
	jb	.L5197
	movq	-2480(%rbp), %rdx
	movq	%rdx, %rax
	notq	%rax
	addq	-2472(%rbp), %rax
	cmpq	$4, %rax
	je	.L5240
	movq	%rcx, %rdi
	cmpq	$7, %rax
	je	.L5241
.L5174:
	cmpq	%r15, %r8
	jbe	.L5170
.L5242:
	movzbl	(%r15), %eax
	movq	%r15, %rdx
	jmp	.L5171
	.p2align 4,,10
	.p2align 3
.L5238:
	leaq	2(%rdx), %r15
	cmpq	%r15, %r8
	ja	.L5242
.L5170:
	movq	-2552(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r9d, %ecx
	movq	%r10, -2560(%rbp)
	movq	%r8, -2536(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-2536(%rbp), %r8
	movq	-2560(%rbp), %r10
	testq	%rax, %rax
	je	.L5190
	movq	%rax, -2464(%rbp)
.L5180:
	movq	-2552(%rbp), %rax
	addq	$120, %rax
	jmp	.L5181
	.p2align 4,,10
	.p2align 3
.L5197:
	movq	%rax, %rdi
	jmp	.L5174
	.p2align 4,,10
	.p2align 3
.L5240:
	cmpl	$1852399981, (%rdx)
	je	.L5176
	cmpl	$1701667182, (%rdx)
	je	.L5176
	cmpl	$1701869940, (%rdx)
	je	.L5176
.L5177:
	movq	%rcx, %rdi
	jmp	.L5174
	.p2align 4,,10
	.p2align 3
.L5241:
	cmpl	$1869641829, (%rdx)
	jne	.L5177
	cmpw	$29810, 4(%rdx)
	jne	.L5177
	cmpb	$115, 6(%rdx)
	jne	.L5177
.L5176:
	movq	-2552(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r9d, %ecx
	movq	%r10, -2560(%rbp)
	movq	%r8, -2536(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-2536(%rbp), %r8
	movq	-2560(%rbp), %r10
	testq	%rax, %rax
	je	.L5190
.L5179:
	movq	%rax, -2464(%rbp)
	cmpq	%r8, %r15
	jnb	.L5180
	movq	-2552(%rbp), %rax
	addq	$112, %rax
.L5181:
	movq	%rax, -2456(%rbp)
	movq	-2552(%rbp), %rdi
	movl	$2, %edx
	movq	%r10, %rsi
	movq	-2544(%rbp), %rax
	movq	(%rax), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L5243
	movq	(%rax), %rax
.L5183:
	movq	-2512(%rbp), %rdi
	movq	%rax, 24(%rbx)
	testq	%rdi, %rdi
	je	.L5184
	call	_ZdlPv@PLT
.L5184:
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uv_fs_close@PLT
	testl	%eax, %eax
	jne	.L5185
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5230
	jmp	.L5145
	.p2align 4,,10
	.p2align 3
.L5234:
	movq	-2544(%rbp), %rax
	movq	-2552(%rbp), %rdi
	xorl	%esi, %esi
	movq	(%rax), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	testq	%rax, %rax
	jne	.L5157
.L5227:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L5155
.L5237:
	cmpb	$-65, 2(%r8)
	leaq	3(%r8), %rsi
	movl	$3, %eax
	jne	.L5193
	jmp	.L5167
.L5236:
	movq	16(%rbx), %rax
	jmp	.L5162
.L5243:
	movq	16(%rbx), %rax
	jmp	.L5183
.L5190:
	movq	%r10, -2560(%rbp)
	movq	%r8, -2536(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2560(%rbp), %r10
	movq	-2536(%rbp), %r8
	xorl	%eax, %eax
	jmp	.L5179
.L5185:
	leaq	_ZZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlvE_clEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L5195:
	movq	%rsi, %r15
	leaq	-2464(%rbp), %r10
	jmp	.L5170
.L5233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7887:
	.size	_ZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB10017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L5245
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L5255
.L5271:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L5256:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5245:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L5269
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L5270
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L5248:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L5250
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L5251
	.p2align 4,,10
	.p2align 3
.L5252:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L5253:
	testq	%rsi, %rsi
	je	.L5250
.L5251:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L5252
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L5258
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L5251
	.p2align 4,,10
	.p2align 3
.L5250:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L5254
	call	_ZdlPv@PLT
.L5254:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L5271
.L5255:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L5257
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L5257:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L5256
	.p2align 4,,10
	.p2align 3
.L5258:
	movq	%rdx, %rdi
	jmp	.L5253
	.p2align 4,,10
	.p2align 3
.L5269:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L5248
.L5270:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10017:
	.size	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC104:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB10021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L5281
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L5282
.L5274:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5282:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L5283
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L5284
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L5279
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L5279:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L5277:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L5274
	.p2align 4,,10
	.p2align 3
.L5283:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L5276
	cmpq	%r14, %rsi
	je	.L5277
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L5277
	.p2align 4,,10
	.p2align 3
.L5276:
	cmpq	%r14, %rsi
	je	.L5277
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L5277
.L5281:
	leaq	.LC104(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5284:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10021:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata.str1.1
.LC105:
	.string	"paths"
.LC106:
	.string	"std::basic_string"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node2fs18FSContinuationData10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs18FSContinuationData10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs18FSContinuationData10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	cmpq	%rax, 40(%rdi)
	je	.L5285
	movq	64(%rsi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r15
	cmpq	32(%rsi), %rax
	je	.L5287
	cmpq	72(%rsi), %rax
	je	.L5329
.L5288:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L5287
	subq	$24, 64(%rax)
.L5287:
	movl	$72, %edi
	leaq	-64(%rbp), %r13
	call	_Znwm@PLT
	movl	$5, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC105(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r15), %rdi
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	$24, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5289
	movq	(%rdi), %rax
	call	*8(%rax)
.L5289:
	movq	64(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L5290
	movq	%rax, %rdx
	cmpq	72(%r15), %rax
	je	.L5330
.L5291:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L5290
	movq	8(%r15), %rdi
	leaq	.LC105(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%r15), %rax
.L5290:
	movq	80(%r15), %rcx
	movq	%r12, -64(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L5292
	movq	%r12, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%r15)
.L5293:
	movq	32(%rbx), %r12
	movq	40(%rbx), %rax
	cmpq	%rax, %r12
	jne	.L5294
	jmp	.L5302
	.p2align 4,,10
	.p2align 3
.L5300:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L5328
	movq	8(%r15), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L5328:
	movq	40(%rbx), %rax
.L5297:
	addq	$32, %r12
	cmpq	%rax, %r12
	je	.L5302
.L5294:
	movq	8(%r12), %r11
	testq	%r11, %r11
	movq	%r11, -72(%rbp)
	je	.L5297
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$17, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r14
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC106(%rip), %rcx
	movq	%rax, (%r14)
	leaq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	$0, 8(%r14)
	movq	$0, 16(%r14)
	movb	$0, 24(%r14)
	movq	%rax, 32(%r14)
	movq	$0, 40(%r14)
	movb	$0, 48(%r14)
	movq	$0, 64(%r14)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r15), %rdi
	movq	-72(%rbp), %r11
	movq	%r13, %rsi
	movb	$0, 24(%r14)
	movq	(%rdi), %rax
	movq	%r11, 64(%r14)
	movq	8(%rax), %rax
	movq	%r14, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5298
	movq	(%rdi), %rax
	call	*8(%rax)
.L5298:
	movq	64(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L5328
	cmpq	72(%r15), %rax
	jne	.L5300
	movq	88(%r15), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L5300
	.p2align 4,,10
	.p2align 3
.L5302:
	movq	64(%r15), %rdi
	cmpq	72(%r15), %rdi
	je	.L5331
	subq	$8, %rdi
	movq	%rdi, 64(%r15)
.L5285:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5332
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5329:
	.cfi_restore_state
	movq	88(%rsi), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L5288
	.p2align 4,,10
	.p2align 3
.L5330:
	movq	88(%r15), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L5291
	.p2align 4,,10
	.p2align 3
.L5292:
	leaq	16(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L5293
	.p2align 4,,10
	.p2align 3
.L5331:
	call	_ZdlPv@PLT
	movq	88(%r15), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%r15)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%r15)
	jmp	.L5285
.L5332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7753:
	.size	_ZNK4node2fs18FSContinuationData10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs18FSContinuationData10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata._ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_.str1.1,"aMS",@progbits,1
.LC107:
	.string	"wrapped"
.LC108:
	.string	"wrapper"
	.section	.text._ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_,"axG",@progbits,_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_
	.type	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_, @function
_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_:
.LFB7672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L5333
	movq	%rdi, %rbx
	movq	104(%rdi), %rdi
	movq	%rdx, %rax
	movq	%rdx, %r12
	xorl	%edx, %edx
	movq	%rsi, %r13
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L5335
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L5337
	.p2align 4,,10
	.p2align 3
.L5412:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5335
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L5335
.L5337:
	cmpq	%rsi, %r12
	jne	.L5412
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L5413
	cmpq	72(%rbx), %rax
	je	.L5414
.L5338:
	movq	-8(%rax), %rsi
.L5364:
	movq	%r13, %rcx
	call	*%r8
.L5333:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5415
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5414:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L5338
	.p2align 4,,10
	.p2align 3
.L5335:
	movq	(%rbx), %rsi
	leaq	-160(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L5339
	movq	(%rax), %r8
	movq	8(%r8), %r10
	movq	%r8, %rcx
	movq	%r10, %rsi
	jmp	.L5342
	.p2align 4,,10
	.p2align 3
.L5416:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5346
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L5346
.L5342:
	cmpq	%r12, %rsi
	jne	.L5416
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L5344
	cmpq	72(%rbx), %rax
	je	.L5417
.L5343:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L5344
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	%r13, %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L5344:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L5333
	.p2align 4,,10
	.p2align 3
.L5418:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L5339
	movq	8(%r8), %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L5339
.L5346:
	cmpq	%r10, %r12
	jne	.L5418
	movq	16(%r8), %r15
.L5354:
	movq	80(%rbx), %rcx
	movq	64(%rbx), %rax
	movq	%r15, -128(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L5355
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L5356:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-128(%rbp), %r13
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L5369
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L5419
.L5358:
	movq	-8(%rax), %rax
.L5357:
	cmpq	%r13, %rax
	jne	.L5420
	cmpq	$0, 64(%rax)
	je	.L5421
	cmpq	72(%rbx), %rdi
	je	.L5361
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L5344
	.p2align 4,,10
	.p2align 3
.L5413:
	xorl	%esi, %esi
	jmp	.L5364
	.p2align 4,,10
	.p2align 3
.L5339:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rax, (%r15)
	leaq	48(%r15), %rax
	movq	%rax, 32(%r15)
	leaq	-128(%rbp), %rax
	movq	%r12, 8(%r15)
	movq	%rax, %rdi
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	movq	%rax, -184(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testq	%rax, %rax
	je	.L5367
	movq	8(%rbx), %rdi
	leaq	-168(%rbp), %rsi
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	%rax, -168(%rbp)
	call	*%rdx
	movq	%rax, 16(%r15)
.L5367:
	movq	8(%r15), %rsi
	leaq	-96(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-192(%rbp), %r8
	leaq	32(%r15), %rdi
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5347
	call	_ZdlPv@PLT
.L5347:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-184(%rbp), %rdi
	movq	%rax, 64(%r15)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	-184(%rbp), %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5348
	movq	(%rdi), %rax
	call	*8(%rax)
.L5348:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L5349
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L5351
	.p2align 4,,10
	.p2align 3
.L5422:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5349
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L5349
.L5351:
	cmpq	%r12, %rsi
	jne	.L5422
	addq	$16, %rcx
.L5362:
	movq	%r15, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L5352
	cmpq	72(%rbx), %rax
	je	.L5423
.L5353:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L5352
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L5352:
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L5354
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC107(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r15), %rsi
	movq	%r15, %rdx
	leaq	.LC108(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L5354
	.p2align 4,,10
	.p2align 3
.L5417:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L5343
.L5419:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L5358
.L5355:
	leaq	-128(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L5356
.L5361:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L5344
.L5420:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L5421:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L5369:
	xorl	%eax, %eax
	jmp	.L5357
.L5423:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L5353
.L5349:
	movl	$24, %edi
	movq	%r9, -184(%rbp)
	call	_Znwm@PLT
	movq	-184(%rbp), %r9
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	%r9, %rsi
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L5362
.L5415:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7672:
	.size	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_, .-_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_
	.section	.rodata.str1.1
.LC109:
	.string	"continuation_data"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7762:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	528(%r8), %rdx
	testq	%rdx, %rdx
	je	.L5424
	xorl	%ecx, %ecx
	leaq	.LC109(%rip), %rsi
	jmp	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_
	.p2align 4,,10
	.p2align 3
.L5424:
	ret
	.cfi_endproc
.LFE7762:
	.size	_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1
.LC110:
	.string	"current_read"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node2fs10FileHandle10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs10FileHandle10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs10FileHandle10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7777:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	152(%r8), %rdx
	testq	%rdx, %rdx
	je	.L5426
	xorl	%ecx, %ecx
	leaq	.LC110(%rip), %rsi
	jmp	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_
	.p2align 4,,10
	.p2align 3
.L5426:
	ret
	.cfi_endproc
.LFE7777:
	.size	_ZNK4node2fs10FileHandle10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs10FileHandle10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1
.LC111:
	.string	"uv_buf_t"
.LC112:
	.string	"buffer"
.LC113:
	.string	"file_handle"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node2fs18FileHandleReadWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs18FileHandleReadWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs18FileHandleReadWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	544(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L5430
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$8, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC111(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r13), %rdi
	movq	%rbx, 64(%r12)
	leaq	-48(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	%r12, -48(%rbp)
	call	*8(%rax)
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5431
	movq	(%rdi), %rax
	call	*8(%rax)
.L5431:
	movq	64(%r13), %rax
	cmpq	32(%r13), %rax
	je	.L5430
	cmpq	72(%r13), %rax
	je	.L5442
.L5433:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L5430
	movq	8(%r13), %rdi
	leaq	.LC112(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L5430:
	movq	528(%r14), %rdx
	xorl	%ecx, %ecx
	leaq	.LC113(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5443
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5442:
	.cfi_restore_state
	movq	88(%r13), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L5433
.L5443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7805:
	.size	_ZNK4node2fs18FileHandleReadWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs18FileHandleReadWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE.str1.1,"aMS",@progbits,1
.LC114:
	.string	"stats_field_array"
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	528(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L5445
	xorl	%ecx, %ecx
	leaq	.LC109(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_
.L5445:
	movq	688(%r12), %rax
	testq	%rax, %rax
	je	.L5444
	movq	656(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L5444
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L5452
	cmpq	72(%rbx), %rcx
	je	.L5460
.L5450:
	movq	-8(%rcx), %rsi
.L5449:
	leaq	.LC114(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L5444:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5461
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5460:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L5450
	.p2align 4,,10
	.p2align 3
.L5452:
	xorl	%esi, %esi
	jmp	.L5449
.L5461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11387:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	528(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L5463
	xorl	%ecx, %ecx
	leaq	.LC109(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker10TrackFieldEPKcPKNS_14MemoryRetainerES2_
.L5463:
	movq	688(%r12), %rax
	testq	%rax, %rax
	je	.L5462
	movq	656(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L5462
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L5470
	cmpq	72(%rbx), %rcx
	je	.L5478
.L5468:
	movq	-8(%rcx), %rsi
.L5467:
	leaq	.LC114(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L5462:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5479
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5478:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L5468
	.p2align 4,,10
	.p2align 3
.L5470:
	xorl	%esi, %esi
	jmp	.L5467
.L5479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11398:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB10222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L5481
	testq	%rsi, %rsi
	je	.L5497
.L5481:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L5498
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L5484
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L5485:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5499
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5484:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L5485
	jmp	.L5483
	.p2align 4,,10
	.p2align 3
.L5498:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L5483:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L5485
.L5497:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L5499:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10222:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.text
	.p2align 4
	.globl	_ZN4node2fs10MKDirpSyncEP9uv_loop_sP7uv_fs_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiPFvS4_E
	.type	_ZN4node2fs10MKDirpSyncEP9uv_loop_sP7uv_fs_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiPFvS4_E, @function
_ZN4node2fs10MKDirpSyncEP9uv_loop_sP7uv_fs_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiPFvS4_E:
.LFB7914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	440(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L5564
.L5501:
	movq	40(%rcx), %rax
	leaq	-112(%rbp), %r12
	leaq	.L5511(%rip), %r15
	cmpq	%rax, 32(%rcx)
	je	.L5537
.L5504:
	movq	%r12, -128(%rbp)
	movq	-32(%rax), %rsi
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L5565
	movq	%rsi, -128(%rbp)
	movq	-16(%rax), %rsi
	movq	%rsi, -112(%rbp)
.L5507:
	movq	-24(%rax), %rsi
	movq	%rsi, -120(%rbp)
	movq	%rdx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	movq	40(%rcx), %rax
	movq	-32(%rax), %rdi
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 40(%rcx)
	cmpq	%rax, %rdi
	je	.L5508
	call	_ZdlPv@PLT
.L5508:
	movq	-128(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	uv_fs_mkdir@PLT
	movl	%eax, %r8d
	leal	20(%rax), %eax
	cmpl	$20, %eax
	ja	.L5509
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5511:
	.long	.L5512-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5512-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5509-.L5511
	.long	.L5513-.L5511
	.long	.L5512-.L5511
	.long	.L5510-.L5511
	.text
	.p2align 4,,10
	.p2align 3
.L5573:
	cmpq	%rcx, %rdi
	je	.L5532
	call	_ZdlPv@PLT
.L5532:
	movl	$-17, %r8d
	.p2align 4,,10
	.p2align 3
.L5509:
	movq	%rbx, %rdi
	movl	%r8d, -152(%rbp)
	call	uv_fs_req_cleanup@PLT
	movq	-128(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	uv_fs_stat@PLT
	testl	%eax, %eax
	jne	.L5533
	movq	120(%rbx), %rax
	andl	$61440, %eax
	cmpq	$16384, %rax
	je	.L5515
	movq	%rbx, %rdi
	call	uv_fs_req_cleanup@PLT
	movl	-152(%rbp), %r8d
	cmpl	$-17, %r8d
	jne	.L5541
	movq	440(%rbx), %rax
	movq	40(%rax), %rbx
	cmpq	%rbx, 32(%rax)
	movl	$-20, %eax
	cmovne	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L5512:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L5500
	movl	%r8d, -152(%rbp)
	call	_ZdlPv@PLT
	movl	-152(%rbp), %r8d
.L5500:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5566
	addq	$136, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5513:
	.cfi_restore_state
	leaq	-128(%rbp), %rax
	movq	$-1, %rdx
	movl	$47, %esi
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5rfindEcm@PLT
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %r9
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	cmpq	%r8, %rax
	cmovbe	%rax, %r8
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L5516
	testq	%r9, %r9
	je	.L5567
.L5516:
	movq	%r8, -136(%rbp)
	cmpq	$15, %r8
	ja	.L5568
	cmpq	$1, %r8
	jne	.L5519
	movzbl	(%r9), %eax
	movb	%al, -80(%rbp)
	movq	%rcx, %rax
.L5520:
	movq	%r8, -88(%rbp)
	movb	$0, (%rax,%r8)
	movq	-88(%rbp), %rdx
	cmpq	-120(%rbp), %rdx
	je	.L5569
.L5521:
	movq	440(%rbx), %rax
	movq	40(%rax), %rsi
	movq	%rax, %rdx
	cmpq	48(%rax), %rsi
	je	.L5523
	leaq	16(%rsi), %rdi
	movq	%rdi, (%rsi)
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L5570
	movq	%rdi, (%rsi)
	movq	-112(%rbp), %rdi
	movq	%rdi, 16(%rsi)
.L5525:
	movq	-120(%rbp), %rdi
	movq	%r12, -128(%rbp)
	movq	%rdi, 8(%rsi)
	addq	$32, 40(%rdx)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
.L5526:
	movq	40(%rax), %rsi
	cmpq	48(%rax), %rsi
	je	.L5527
	leaq	16(%rsi), %rdx
	movq	%rdx, (%rsi)
	movq	-96(%rbp), %rdx
	cmpq	%rcx, %rdx
	je	.L5571
	movq	%rdx, (%rsi)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rsi)
.L5529:
	movq	-88(%rbp), %rdx
	movq	%rdx, 8(%rsi)
	addq	$32, 40(%rax)
	jmp	.L5515
	.p2align 4,,10
	.p2align 3
.L5510:
	movq	440(%rbx), %rdi
	cmpq	$0, 64(%rdi)
	je	.L5572
.L5514:
	movq	40(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L5512
.L5515:
	movq	%rbx, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L5535
	call	_ZdlPv@PLT
	movq	440(%rbx), %rcx
	movq	40(%rcx), %rax
	cmpq	32(%rcx), %rax
	jne	.L5504
.L5537:
	xorl	%r8d, %r8d
	jmp	.L5500
	.p2align 4,,10
	.p2align 3
.L5535:
	movq	440(%rbx), %rcx
	movq	40(%rcx), %rax
	cmpq	%rax, 32(%rcx)
	jne	.L5504
	jmp	.L5537
	.p2align 4,,10
	.p2align 3
.L5565:
	movdqu	-16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L5507
	.p2align 4,,10
	.p2align 3
.L5533:
	jns	.L5515
	movl	%eax, %r8d
	jmp	.L5512
	.p2align 4,,10
	.p2align 3
.L5568:
	leaq	-96(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	-176(%rbp), %rcx
	movq	%rax, -80(%rbp)
.L5518:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%rcx, -160(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r8
	movq	-96(%rbp), %rax
	movq	-160(%rbp), %rcx
	jmp	.L5520
	.p2align 4,,10
	.p2align 3
.L5572:
	addq	$56, %rdi
	leaq	-128(%rbp), %rsi
	movl	%r8d, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	440(%rbx), %rdi
	movl	-152(%rbp), %r8d
	jmp	.L5514
	.p2align 4,,10
	.p2align 3
.L5569:
	movq	-96(%rbp), %rdi
	testq	%rdx, %rdx
	je	.L5522
	movq	-128(%rbp), %rsi
	movq	%rcx, -168(%rbp)
	movq	%rdi, -160(%rbp)
	call	memcmp@PLT
	movq	-160(%rbp), %rdi
	movq	-168(%rbp), %rcx
	testl	%eax, %eax
	jne	.L5521
.L5522:
	movq	440(%rbx), %rax
	movq	40(%rax), %rdx
	cmpq	%rdx, 32(%rax)
	je	.L5573
.L5531:
	cmpq	%rcx, %rdi
	je	.L5515
	call	_ZdlPv@PLT
	jmp	.L5515
	.p2align 4,,10
	.p2align 3
.L5519:
	testq	%r8, %r8
	jne	.L5574
	movq	%rcx, %rax
	jmp	.L5520
	.p2align 4,,10
	.p2align 3
.L5564:
	movl	$88, %edi
	movq	%r8, -152(%rbp)
	movq	%rdx, %r15
	call	_Znwm@PLT
	movq	-152(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	leaq	16+_ZTVN4node2fs18FSContinuationDataE(%rip), %rax
	movq	%r8, 8(%r12)
	movq	440(%rbx), %r8
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	movq	%rbx, 16(%r12)
	movl	%r14d, 24(%r12)
	movq	$0, 48(%r12)
	movq	%rax, 56(%r12)
	movq	$0, 64(%r12)
	movb	$0, 72(%r12)
	movq	%r12, 440(%rbx)
	movups	%xmm0, 32(%r12)
	testq	%r8, %r8
	je	.L5503
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	movq	440(%rbx), %r12
	movq	40(%r12), %r8
	cmpq	48(%r12), %r8
	je	.L5503
	leaq	16(%r8), %rax
	movq	8(%r15), %rdx
	movq	%r8, %rdi
	movq	%rax, (%r8)
	movq	(%r15), %rsi
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	addq	$32, 40(%r12)
	movq	440(%rbx), %rcx
	jmp	.L5501
	.p2align 4,,10
	.p2align 3
.L5503:
	leaq	32(%r12), %rdi
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	440(%rbx), %rcx
	jmp	.L5501
	.p2align 4,,10
	.p2align 3
.L5571:
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 16(%rsi)
	jmp	.L5529
	.p2align 4,,10
	.p2align 3
.L5570:
	movdqa	-112(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	movq	440(%rbx), %rax
	jmp	.L5525
	.p2align 4,,10
	.p2align 3
.L5523:
	movq	-152(%rbp), %rdx
	leaq	32(%rax), %rdi
	movq	%rcx, -160(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	440(%rbx), %rax
	movq	-160(%rbp), %rcx
	jmp	.L5526
	.p2align 4,,10
	.p2align 3
.L5527:
	leaq	32(%rax), %rdi
	leaq	-96(%rbp), %rdx
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-96(%rbp), %rdi
	movq	-152(%rbp), %rcx
	jmp	.L5531
.L5541:
	movl	$-17, %r8d
	jmp	.L5512
.L5566:
	call	__stack_chk_fail@PLT
.L5567:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L5574:
	movq	%rcx, %rdi
	jmp	.L5518
	.cfi_endproc
.LFE7914:
	.size	_ZN4node2fs10MKDirpSyncEP9uv_loop_sP7uv_fs_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiPFvS4_E, .-_ZN4node2fs10MKDirpSyncEP9uv_loop_sP7uv_fs_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiPFvS4_E
	.p2align 4
	.globl	_ZN4node2fs14CallMKDirpSyncEPNS_11EnvironmentERKN2v820FunctionCallbackInfoINS3_5ValueEEEPNS0_13FSReqWrapSyncEPKci
	.type	_ZN4node2fs14CallMKDirpSyncEPNS_11EnvironmentERKN2v820FunctionCallbackInfoINS3_5ValueEEEPNS0_13FSReqWrapSyncEPKci, @function
_ZN4node2fs14CallMKDirpSyncEPNS_11EnvironmentERKN2v820FunctionCallbackInfoINS3_5ValueEEEPNS0_13FSReqWrapSyncEPKci:
.LFB7925:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	%r14, -96(%rbp)
	testq	%r12, %r12
	je	.L5591
	movq	%r12, %rdi
	leaq	-96(%rbp), %r15
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L5592
	cmpq	$1, %rax
	jne	.L5579
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%r14, %rdx
.L5580:
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	movb	$0, (%rdx,%rax)
	movq	360(%rbx), %rax
	movq	%r15, %rdx
	movq	2360(%rax), %rdi
	call	_ZN4node2fs10MKDirpSyncEP9uv_loop_sP7uv_fs_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiPFvS4_E
	movq	-96(%rbp), %rdi
	movl	%eax, %r15d
	cmpq	%r14, %rdi
	je	.L5581
	call	_ZdlPv@PLT
.L5581:
	testl	%r15d, %r15d
	js	.L5593
.L5575:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5594
	addq	$104, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5579:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L5595
	movq	%r14, %rdx
	jmp	.L5580
	.p2align 4,,10
	.p2align 3
.L5593:
	movq	-128(%rbp), %rax
	movq	3280(%rbx), %r12
	cmpl	$4, 16(%rax)
	jg	.L5583
	movq	(%rax), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L5584:
	movq	352(%rbx), %r14
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L5596
.L5585:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L5597
.L5586:
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L5575
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L5575
	.p2align 4,,10
	.p2align 3
.L5592:
	movq	%r15, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L5578:
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L5580
	.p2align 4,,10
	.p2align 3
.L5591:
	leaq	.LC61(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L5583:
	movq	8(%rax), %r13
	subq	$32, %r13
	jmp	.L5584
	.p2align 4,,10
	.p2align 3
.L5596:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L5585
	.p2align 4,,10
	.p2align 3
.L5597:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rcx
	jmp	.L5586
.L5594:
	call	__stack_chk_fail@PLT
.L5595:
	movq	%r14, %rdi
	jmp	.L5578
	.cfi_endproc
.LFE7925:
	.size	_ZN4node2fs14CallMKDirpSyncEPNS_11EnvironmentERKN2v820FunctionCallbackInfoINS3_5ValueEEEPNS0_13FSReqWrapSyncEPKci, .-_ZN4node2fs14CallMKDirpSyncEPNS_11EnvironmentERKN2v820FunctionCallbackInfoINS3_5ValueEEEPNS0_13FSReqWrapSyncEPKci
	.section	.rodata.str1.1
.LC115:
	.string	"fs.sync.mkdir"
	.text
	.p2align 4
	.type	_ZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L5649
	cmpw	$1040, %cx
	jne	.L5599
.L5649:
	movq	23(%rdx), %r13
.L5601:
	movl	16(%r12), %r15d
	cmpl	$3, %r15d
	jg	.L5671
	leaq	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5671:
	movq	8(%r12), %rdx
	movq	352(%r13), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L5672
	cmpl	$1, 16(%r12)
	jle	.L5673
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L5604:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L5674
	cmpl	$1, 16(%r12)
	jg	.L5606
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r14d
	jg	.L5608
.L5678:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L5609:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L5675
	cmpl	$2, 16(%r12)
	jg	.L5611
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %ebx
	jg	.L5613
.L5679:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L5615
.L5680:
	movq	-1088(%rbp), %rdx
	testb	%bl, %bl
	je	.L5648
	leaq	_ZN4node2fs11MKDirpAsyncEP9uv_loop_sP7uv_fs_sPKciPFvS4_E(%rip), %rax
	leaq	_ZN4node2fs11AfterMkdirpEP7uv_fs_s(%rip), %rbx
.L5616:
	leaq	.LC25(%rip), %rcx
	cmpq	$0, 80(%r9)
	movq	%r9, 88(%r9)
	movq	%rcx, 544(%r9)
	movl	$1, 536(%r9)
	jne	.L5676
	movq	16(%r9), %rcx
	leaq	88(%r9), %r13
	movq	%rbx, 80(%r9)
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %r8
	movq	%r9, -1608(%rbp)
	movq	%r13, %rsi
	movq	360(%rcx), %rcx
	movq	2360(%rcx), %rdi
	movl	%r14d, %ecx
	call	*%rax
	movq	-1608(%rbp), %r9
	testl	%eax, %eax
	js	.L5618
	movq	16(%r9), %rax
	movq	%r12, %rsi
	movq	%r9, %rdi
	addl	$1, 2156(%rax)
	movq	(%r9), %rax
	call	*120(%rax)
.L5619:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5598
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5598
.L5670:
	call	free@PLT
.L5598:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5677
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5673:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L5604
	.p2align 4,,10
	.p2align 3
.L5606:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %r14d
	jle	.L5678
.L5608:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L5609
	.p2align 4,,10
	.p2align 3
.L5611:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$3, 16(%r12)
	movl	%eax, %ebx
	jle	.L5679
.L5613:
	movq	8(%r12), %rax
	movq	%r13, %rdi
	leaq	-24(%rax), %rsi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.constprop.0
	movq	%rax, %r9
	testq	%rax, %rax
	jne	.L5680
.L5615:
	cmpl	$5, %r15d
	jne	.L5681
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L5682
.L5622:
	movq	-1088(%rbp), %r9
	testb	%bl, %bl
	jne	.L5683
	cmpl	$4, 16(%r12)
	jg	.L5637
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L5638:
	subq	$8, %rsp
	movq	uv_fs_mkdir@GOTPCREL(%rip), %r8
	movq	%r13, %rdi
	leaq	-1552(%rbp), %r15
	pushq	%r14
	movq	%r15, %rdx
	leaq	.LC25(%rip), %rcx
	call	_ZN4node2fs8SyncCallIPFiP9uv_loop_sP7uv_fs_sPKciPFvS5_EEJPciEEEiPNS_11EnvironmentEN2v85LocalINSF_5ValueEEEPNS0_13FSReqWrapSyncES7_T_DpT0_
	popq	%rax
	popq	%rdx
.L5627:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	cmpb	$0, (%rax)
	jne	.L5684
.L5640:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	jmp	.L5619
	.p2align 4,,10
	.p2align 3
.L5648:
	movq	uv_fs_mkdir@GOTPCREL(%rip), %rax
	leaq	_ZN4node2fs11AfterNoArgsEP7uv_fs_s(%rip), %rbx
	jmp	.L5616
	.p2align 4,,10
	.p2align 3
.L5599:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L5601
	.p2align 4,,10
	.p2align 3
.L5672:
	leaq	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5618:
	movq	$0, 192(%r9)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r9)
	call	*%rbx
	jmp	.L5619
	.p2align 4,,10
	.p2align 3
.L5674:
	leaq	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5675:
	leaq	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5637:
	movq	8(%r12), %rsi
	subq	$32, %rsi
	jmp	.L5638
	.p2align 4,,10
	.p2align 3
.L5683:
	leaq	-1552(%rbp), %r15
	movl	%r14d, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN4node2fs14CallMKDirpSyncEPNS_11EnvironmentERKN2v820FunctionCallbackInfoINS3_5ValueEEEPNS0_13FSReqWrapSyncEPKci
	testl	%eax, %eax
	jne	.L5627
	movq	-1112(%rbp), %rdx
	movq	64(%rdx), %rax
	testq	%rax, %rax
	je	.L5627
	movq	56(%rdx), %rsi
	leaq	-1584(%rbp), %rdi
	leaq	-1568(%rbp), %rbx
	movq	$0, -1592(%rbp)
	movq	%rbx, -1584(%rbp)
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	352(%r13), %rdi
	movl	$1, %edx
	movq	-1584(%rbp), %rsi
	leaq	-1592(%rbp), %rcx
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L5685
	movq	(%rax), %rdx
	movq	-1584(%rbp), %rdi
	movq	(%r12), %rax
	movq	%rdx, 24(%rax)
	cmpq	%rbx, %rdi
	je	.L5627
	call	_ZdlPv@PLT
	jmp	.L5627
	.p2align 4,,10
	.p2align 3
.L5682:
	movq	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1481(%rip), %rsi
	testq	%rsi, %rsi
	je	.L5686
.L5624:
	testb	$5, (%rsi)
	je	.L5622
	leaq	.LC115(%rip), %rdx
	movl	$66, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L5622
	.p2align 4,,10
	.p2align 3
.L5684:
	movq	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1503(%rip), %rsi
	testq	%rsi, %rsi
	je	.L5687
.L5642:
	testb	$5, (%rsi)
	je	.L5640
	leaq	.LC115(%rip), %rdx
	movl	$69, %edi
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L5640
	.p2align 4,,10
	.p2align 3
.L5686:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1481(%rip)
	mfence
	jmp	.L5624
	.p2align 4,,10
	.p2align 3
.L5687:
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc.constprop.0
	movq	%rax, %rsi
	movq	%rax, _ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1503(%rip)
	mfence
	jmp	.L5642
	.p2align 4,,10
	.p2align 3
.L5676:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5681:
	leaq	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L5685:
	cmpl	$4, 16(%r12)
	jg	.L5629
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L5630:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	-1592(%rbp), %rcx
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L5688
.L5631:
	movq	-1584(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5632
	call	_ZdlPv@PLT
.L5632:
	movq	%r15, %rdi
	call	_ZN4node2fs13FSReqWrapSyncD1Ev
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5598
	testq	%rdi, %rdi
	jne	.L5670
	jmp	.L5598
.L5629:
	movq	8(%r12), %rdi
	subq	$32, %rdi
	jmp	.L5630
.L5688:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L5631
.L5677:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7926:
	.size	_ZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_, @function
_GLOBAL__sub_I__ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_:
.LFB11415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE11415:
	.size	_GLOBAL__sub_I__ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_, .-_GLOBAL__sub_I__ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node2fs8BasenameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_
	.weak	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args
	.section	.rodata.str1.1
.LC116:
	.string	"../src/node_file-inl.h:152"
.LC117:
	.string	"finished_"
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"node::fs::FSReqPromise<AliasedBufferT>::~FSReqPromise() [with AliasedBufferT = node::AliasedBufferBase<long unsigned int, v8::BigUint64Array>]"
	.section	.data.rel.ro.local._ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args,"awG",@progbits,_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args,comdat
	.align 16
	.type	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args, @gnu_unique_object
	.size	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args, 24
_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args:
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.weak	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"node::fs::FSReqPromise<AliasedBufferT>::~FSReqPromise() [with AliasedBufferT = node::AliasedBufferBase<double, v8::Float64Array>]"
	.section	.data.rel.ro.local._ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args,"awG",@progbits,_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args,comdat
	.align 16
	.type	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args, @gnu_unique_object
	.size	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args, 24
_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args:
	.quad	.LC116
	.quad	.LC117
	.quad	.LC119
	.weak	_ZTVN4node11ReqWrapBaseE
	.section	.data.rel.ro._ZTVN4node11ReqWrapBaseE,"awG",@progbits,_ZTVN4node11ReqWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11ReqWrapBaseE, @object
	.size	_ZTVN4node11ReqWrapBaseE, 48
_ZTVN4node11ReqWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node9StreamReqE
	.section	.data.rel.ro._ZTVN4node9StreamReqE,"awG",@progbits,_ZTVN4node9StreamReqE,comdat
	.align 8
	.type	_ZTVN4node9StreamReqE, @object
	.size	_ZTVN4node9StreamReqE, 48
_ZTVN4node9StreamReqE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node7ReqWrapI7uv_fs_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI7uv_fs_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI7uv_fs_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI7uv_fs_sEE, @object
	.size	_ZTVN4node7ReqWrapI7uv_fs_sEE, 160
_ZTVN4node7ReqWrapI7uv_fs_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE
	.section	.data.rel.ro._ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE,"awG",@progbits,_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE,comdat
	.align 8
	.type	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE, @object
	.size	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE, 192
_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE
	.section	.data.rel.ro._ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE,"awG",@progbits,_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE,comdat
	.align 8
	.type	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE, @object
	.size	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE, 192
_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, 168
_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-40
	.quad	0
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node2fs18FSContinuationDataE
	.section	.data.rel.ro.local._ZTVN4node2fs18FSContinuationDataE,"awG",@progbits,_ZTVN4node2fs18FSContinuationDataE,comdat
	.align 8
	.type	_ZTVN4node2fs18FSContinuationDataE, @object
	.size	_ZTVN4node2fs18FSContinuationDataE, 72
_ZTVN4node2fs18FSContinuationDataE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs18FSContinuationDataD1Ev
	.quad	_ZN4node2fs18FSContinuationDataD0Ev
	.quad	_ZNK4node2fs18FSContinuationData10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs18FSContinuationData14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs18FSContinuationData8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node2fs18FileHandleReadWrapE
	.section	.data.rel.ro._ZTVN4node2fs18FileHandleReadWrapE,"awG",@progbits,_ZTVN4node2fs18FileHandleReadWrapE,comdat
	.align 8
	.type	_ZTVN4node2fs18FileHandleReadWrapE, @object
	.size	_ZTVN4node2fs18FileHandleReadWrapE, 160
_ZTVN4node2fs18FileHandleReadWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs18FileHandleReadWrapD1Ev
	.quad	_ZN4node2fs18FileHandleReadWrapD0Ev
	.quad	_ZNK4node2fs18FileHandleReadWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs18FileHandleReadWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs18FileHandleReadWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs18FileHandleReadWrapD1Ev
	.quad	_ZThn56_N4node2fs18FileHandleReadWrapD0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node2fs9FSReqBaseE
	.section	.data.rel.ro._ZTVN4node2fs9FSReqBaseE,"awG",@progbits,_ZTVN4node2fs9FSReqBaseE,comdat
	.align 8
	.type	_ZTVN4node2fs9FSReqBaseE, @object
	.size	_ZTVN4node2fs9FSReqBaseE, 192
_ZTVN4node2fs9FSReqBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node2fs10FileHandleE
	.section	.data.rel.ro._ZTVN4node2fs10FileHandleE,"awG",@progbits,_ZTVN4node2fs10FileHandleE,comdat
	.align 8
	.type	_ZTVN4node2fs10FileHandleE, @object
	.size	_ZTVN4node2fs10FileHandleE, 328
_ZTVN4node2fs10FileHandleE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs10FileHandleD1Ev
	.quad	_ZN4node2fs10FileHandleD0Ev
	.quad	_ZNK4node2fs10FileHandle10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs10FileHandle14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs10FileHandle8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node2fs10FileHandle5GetFDEv
	.quad	_ZN4node2fs10FileHandle9ReadStartEv
	.quad	_ZN4node2fs10FileHandle8ReadStopEv
	.quad	_ZN4node2fs10FileHandle7IsAliveEv
	.quad	_ZN4node2fs10FileHandle9IsClosingEv
	.quad	_ZN4node2fs10FileHandle12GetAsyncWrapEv
	.quad	_ZN4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE
	.quad	_ZN4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs10FileHandleD1Ev
	.quad	_ZThn56_N4node2fs10FileHandleD0Ev
	.quad	_ZThn56_N4node2fs10FileHandle9ReadStartEv
	.quad	_ZThn56_N4node2fs10FileHandle8ReadStopEv
	.quad	_ZThn56_N4node2fs10FileHandle10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn56_N4node2fs10FileHandle7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn56_N4node2fs10FileHandle7IsAliveEv
	.quad	_ZThn56_N4node2fs10FileHandle9IsClosingEv
	.quad	_ZN4node10StreamBase9IsIPCPipeEv
	.quad	_ZThn56_N4node2fs10FileHandle5GetFDEv
	.quad	_ZThn56_N4node2fs10FileHandle18CreateShutdownWrapEN2v85LocalINS2_6ObjectEEE
	.quad	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn56_N4node2fs10FileHandle12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZTVN4node2fs10FileHandle8CloseReqE
	.section	.data.rel.ro._ZTVN4node2fs10FileHandle8CloseReqE,"awG",@progbits,_ZTVN4node2fs10FileHandle8CloseReqE,comdat
	.align 8
	.type	_ZTVN4node2fs10FileHandle8CloseReqE, @object
	.size	_ZTVN4node2fs10FileHandle8CloseReqE, 160
_ZTVN4node2fs10FileHandle8CloseReqE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs10FileHandle8CloseReqD1Ev
	.quad	_ZN4node2fs10FileHandle8CloseReqD0Ev
	.quad	_ZNK4node2fs10FileHandle8CloseReq10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs10FileHandle8CloseReq14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs10FileHandle8CloseReq8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs10FileHandle8CloseReqD1Ev
	.quad	_ZThn56_N4node2fs10FileHandle8CloseReqD0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE
	.section	.data.rel.ro._ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE,"awG",@progbits,_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE,comdat
	.align 8
	.type	_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE, @object
	.size	_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE, 232
_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.quad	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv
	.quad	-16
	.quad	0
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.quad	-72
	.quad	0
	.quad	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED1Ev
	.quad	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEED0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI7uv_fs_sEEE12GetAsyncWrapEv
	.weak	_ZTVN4node2fs13FSReqCallbackE
	.section	.data.rel.ro._ZTVN4node2fs13FSReqCallbackE,"awG",@progbits,_ZTVN4node2fs13FSReqCallbackE,comdat
	.align 8
	.type	_ZTVN4node2fs13FSReqCallbackE, @object
	.size	_ZTVN4node2fs13FSReqCallbackE, 192
_ZTVN4node2fs13FSReqCallbackE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs13FSReqCallbackD1Ev
	.quad	_ZN4node2fs13FSReqCallbackD0Ev
	.quad	_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs13FSReqCallback14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs13FSReqCallback8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	_ZN4node2fs13FSReqCallback6RejectEN2v85LocalINS2_5ValueEEE
	.quad	_ZN4node2fs13FSReqCallback7ResolveEN2v85LocalINS2_5ValueEEE
	.quad	_ZN4node2fs13FSReqCallback11ResolveStatEPK9uv_stat_t
	.quad	_ZN4node2fs13FSReqCallback14SetReturnValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs13FSReqCallbackD1Ev
	.quad	_ZThn56_N4node2fs13FSReqCallbackD0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_EE
	.section	.data.rel.ro.local._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_EE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_EE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E_E4CallES2_
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_EE
	.section	.data.rel.ro.local._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_EE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_EE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_2fs10FileHandle5CloseEvEUlS2_E0_E4CallES2_
	.weak	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args
	.section	.rodata.str1.1
.LC120:
	.string	"../src/util-inl.h:374"
.LC121:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"T* node::Realloc(T*, size_t) [with T = uv_buf_t; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args,"awG",@progbits,_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args, 24
_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args:
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.weak	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args
	.section	.rodata.str1.1
.LC123:
	.string	"../src/req_wrap-inl.h:130"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"(req_wrap->original_callback_) == nullptr"
	.align 8
.LC125:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, vo"
	.string	"id (*)(ReqT*, Args ...)>::For(node::ReqWrap<T>*, node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_fs_s; Args = {}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_fs_s*)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.weak	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC126:
	.string	"../src/util.h:391"
.LC127:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = char; long unsigned int kStackStorageSize = 64; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm64EE9SetLengthEmE4args:
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.weak	_ZZN4node7ReallocIcEEPT_S2_mE4args
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"T* node::Realloc(T*, size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIcEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIcEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIcEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIcEEPT_S2_mE4args, 24
_ZZN4node7ReallocIcEEPT_S2_mE4args:
	.quad	.LC120
	.quad	.LC121
	.quad	.LC129
	.weak	_ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args
	.section	.rodata.str1.1
.LC130:
	.string	"../src/util.h:352"
.LC131:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = uv_buf_t; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args, 24
_ZZN4node16MaybeStackBufferI8uv_buf_tLm1024EEixEmE4args:
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.weak	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args
	.section	.rodata.str1.1
.LC133:
	.string	"../src/util.h:397"
.LC134:
	.string	"(length + 1) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLengthAndZeroTerminate(size_t) [with T = char; long unsigned int kStackStorageSize = 64; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm64EE25SetLengthAndZeroTerminateEmE4args:
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.weak	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.1
.LC136:
	.string	"../src/util.h:376"
.LC137:
	.string	"!IsInvalidated()"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = char; long unsigned int kStackStorageSize = 64; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm64EE25AllocateSufficientStorageEmE4args:
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.weak	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args
	.section	.rodata.str1.1
.LC139:
	.string	"../src/req_wrap-inl.h:28"
	.section	.rodata.str1.8
	.align 8
.LC140:
	.string	"(false) == (persistent().IsEmpty())"
	.align 8
.LC141:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_fs_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI7uv_fs_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args, 24
_ZZN4node7ReqWrapI7uv_fs_sED4EvE4args:
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.section	.rodata.str1.1
.LC142:
	.string	"../src/node_file.cc"
.LC143:
	.string	"fs"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC142
	.quad	0
	.quad	_ZN4node2fs10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC143
	.quad	0
	.quad	0
	.local	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2271
	.comm	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2271,8,8
	.local	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2268
	.comm	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2268,8,8
	.section	.rodata.str1.1
.LC144:
	.string	"../src/node_file.cc:2266"
.LC145:
	.string	"(argc) == (4)"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"void node::fs::Mkdtemp(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.section	.rodata.str1.1
.LC147:
	.string	"../src/node_file.cc:2257"
.LC148:
	.string	"(*tmpl) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC147
	.quad	.LC148
	.quad	.LC146
	.section	.rodata.str1.1
.LC149:
	.string	"../src/node_file.cc:2254"
.LC150:
	.string	"(argc) >= (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL7MkdtempERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC149
	.quad	.LC150
	.quad	.LC146
	.local	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2245
	.comm	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2245,8,8
	.local	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2242
	.comm	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2242,8,8
	.section	.rodata.str1.1
.LC151:
	.string	"../src/node_file.cc:2240"
.LC152:
	.string	"(argc) == (5)"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"void node::fs::FUTimes(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.section	.rodata.str1.1
.LC154:
	.string	"../src/node_file.cc:2232"
.LC155:
	.string	"args[2]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC154
	.quad	.LC155
	.quad	.LC153
	.section	.rodata.str1.1
.LC156:
	.string	"../src/node_file.cc:2229"
.LC157:
	.string	"args[1]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC153
	.section	.rodata.str1.1
.LC158:
	.string	"../src/node_file.cc:2226"
.LC159:
	.string	"args[0]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC158
	.quad	.LC159
	.quad	.LC153
	.section	.rodata.str1.1
.LC160:
	.string	"../src/node_file.cc:2224"
.LC161:
	.string	"(argc) >= (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL7FUTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC160
	.quad	.LC161
	.quad	.LC153
	.local	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2216
	.comm	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2216,8,8
	.local	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2213
	.comm	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2213,8,8
	.section	.rodata.str1.1
.LC162:
	.string	"../src/node_file.cc:2211"
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"void node::fs::UTimes(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC162
	.quad	.LC152
	.quad	.LC163
	.section	.rodata.str1.1
.LC164:
	.string	"../src/node_file.cc:2203"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC164
	.quad	.LC155
	.quad	.LC163
	.section	.rodata.str1.1
.LC165:
	.string	"../src/node_file.cc:2200"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC165
	.quad	.LC157
	.quad	.LC163
	.section	.rodata.str1.1
.LC166:
	.string	"../src/node_file.cc:2198"
.LC167:
	.string	"(*path) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC166
	.quad	.LC167
	.quad	.LC163
	.section	.rodata.str1.1
.LC168:
	.string	"../src/node_file.cc:2195"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL6UTimesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC168
	.quad	.LC161
	.quad	.LC163
	.local	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2186
	.comm	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2186,8,8
	.local	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2183
	.comm	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2183,8,8
	.section	.rodata.str1.1
.LC169:
	.string	"../src/node_file.cc:2181"
	.section	.rodata.str1.8
	.align 8
.LC170:
	.string	"void node::fs::LChown(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC169
	.quad	.LC152
	.quad	.LC170
	.section	.rodata.str1.1
.LC171:
	.string	"../src/node_file.cc:2173"
.LC172:
	.string	"args[2]->IsUint32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC171
	.quad	.LC172
	.quad	.LC170
	.section	.rodata.str1.1
.LC173:
	.string	"../src/node_file.cc:2170"
.LC174:
	.string	"args[1]->IsUint32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC173
	.quad	.LC174
	.quad	.LC170
	.section	.rodata.str1.1
.LC175:
	.string	"../src/node_file.cc:2168"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC175
	.quad	.LC167
	.quad	.LC170
	.section	.rodata.str1.1
.LC176:
	.string	"../src/node_file.cc:2165"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL6LChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC176
	.quad	.LC161
	.quad	.LC170
	.local	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2156
	.comm	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2156,8,8
	.local	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2153
	.comm	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2153,8,8
	.section	.rodata.str1.1
.LC177:
	.string	"../src/node_file.cc:2151"
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"void node::fs::FChown(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC177
	.quad	.LC152
	.quad	.LC178
	.section	.rodata.str1.1
.LC179:
	.string	"../src/node_file.cc:2143"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC179
	.quad	.LC172
	.quad	.LC178
	.section	.rodata.str1.1
.LC180:
	.string	"../src/node_file.cc:2140"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC180
	.quad	.LC174
	.quad	.LC178
	.section	.rodata.str1.1
.LC181:
	.string	"../src/node_file.cc:2137"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC181
	.quad	.LC159
	.quad	.LC178
	.section	.rodata.str1.1
.LC182:
	.string	"../src/node_file.cc:2135"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL6FChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC182
	.quad	.LC161
	.quad	.LC178
	.local	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2123
	.comm	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2123,8,8
	.local	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2120
	.comm	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2120,8,8
	.section	.rodata.str1.1
.LC183:
	.string	"../src/node_file.cc:2118"
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"void node::fs::Chown(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC183
	.quad	.LC152
	.quad	.LC184
	.section	.rodata.str1.1
.LC185:
	.string	"../src/node_file.cc:2110"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC185
	.quad	.LC172
	.quad	.LC184
	.section	.rodata.str1.1
.LC186:
	.string	"../src/node_file.cc:2107"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC186
	.quad	.LC174
	.quad	.LC184
	.section	.rodata.str1.1
.LC187:
	.string	"../src/node_file.cc:2105"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC187
	.quad	.LC167
	.quad	.LC184
	.section	.rodata.str1.1
.LC188:
	.string	"../src/node_file.cc:2102"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL5ChownERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC188
	.quad	.LC161
	.quad	.LC184
	.local	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2090
	.comm	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2090,8,8
	.local	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2087
	.comm	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2087,8,8
	.section	.rodata.str1.1
.LC189:
	.string	"../src/node_file.cc:2085"
	.section	.rodata.str1.8
	.align 8
.LC190:
	.string	"void node::fs::FChmod(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC189
	.quad	.LC145
	.quad	.LC190
	.section	.rodata.str1.1
.LC191:
	.string	"../src/node_file.cc:2077"
.LC192:
	.string	"args[1]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC191
	.quad	.LC192
	.quad	.LC190
	.section	.rodata.str1.1
.LC193:
	.string	"../src/node_file.cc:2074"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC193
	.quad	.LC159
	.quad	.LC190
	.section	.rodata.str1.1
.LC194:
	.string	"../src/node_file.cc:2072"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL6FChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC194
	.quad	.LC150
	.quad	.LC190
	.local	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2060
	.comm	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2060,8,8
	.local	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2057
	.comm	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2057,8,8
	.section	.rodata.str1.1
.LC195:
	.string	"../src/node_file.cc:2055"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"void node::fs::Chmod(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC195
	.quad	.LC145
	.quad	.LC196
	.section	.rodata.str1.1
.LC197:
	.string	"../src/node_file.cc:2047"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC197
	.quad	.LC192
	.quad	.LC196
	.section	.rodata.str1.1
.LC198:
	.string	"../src/node_file.cc:2045"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC198
	.quad	.LC167
	.quad	.LC196
	.section	.rodata.str1.1
.LC199:
	.string	"../src/node_file.cc:2042"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL5ChmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC199
	.quad	.LC150
	.quad	.LC196
	.local	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2029
	.comm	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2029,8,8
	.local	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2026
	.comm	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic2026,8,8
	.section	.rodata.str1.1
.LC200:
	.string	"../src/node_file.cc:2024"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"void node::fs::ReadBuffers(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC200
	.quad	.LC152
	.quad	.LC201
	.section	.rodata.str1.1
.LC202:
	.string	"../src/node_file.cc:2015"
.LC203:
	.string	"Buffer::HasInstance(buffer)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC202
	.quad	.LC203
	.quad	.LC201
	.section	.rodata.str1.1
.LC204:
	.string	"../src/node_file.cc:2005"
.LC205:
	.string	"args[1]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC204
	.quad	.LC205
	.quad	.LC201
	.section	.rodata.str1.1
.LC206:
	.string	"../src/node_file.cc:2002"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC206
	.quad	.LC159
	.quad	.LC201
	.section	.rodata.str1.1
.LC207:
	.string	"../src/node_file.cc:2000"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL11ReadBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC207
	.quad	.LC161
	.quad	.LC201
	.local	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1983
	.comm	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1983,8,8
	.local	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1980
	.comm	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1980,8,8
	.section	.rodata.str1.1
.LC208:
	.string	"../src/node_file.cc:1978"
.LC209:
	.string	"(argc) == (7)"
	.section	.rodata.str1.8
	.align 8
.LC210:
	.string	"void node::fs::Read(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7:
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.section	.rodata.str1.1
.LC211:
	.string	"../src/node_file.cc:1967"
.LC212:
	.string	"args[4]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6:
	.quad	.LC211
	.quad	.LC212
	.quad	.LC210
	.section	.rodata.str1.1
.LC213:
	.string	"../src/node_file.cc:1965"
	.section	.rodata.str1.8
	.align 8
.LC214:
	.string	"Buffer::IsWithinBounds(off, len, buffer_length)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5:
	.quad	.LC213
	.quad	.LC214
	.quad	.LC210
	.section	.rodata.str1.1
.LC215:
	.string	"../src/node_file.cc:1963"
.LC216:
	.string	"args[3]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4:
	.quad	.LC215
	.quad	.LC216
	.quad	.LC210
	.section	.rodata.str1.1
.LC217:
	.string	"../src/node_file.cc:1961"
.LC218:
	.string	"(off) < (buffer_length)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC217
	.quad	.LC218
	.quad	.LC210
	.section	.rodata.str1.1
.LC219:
	.string	"../src/node_file.cc:1959"
.LC220:
	.string	"args[2]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC219
	.quad	.LC220
	.quad	.LC210
	.section	.rodata.str1.1
.LC221:
	.string	"../src/node_file.cc:1954"
.LC222:
	.string	"Buffer::HasInstance(args[1])"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC221
	.quad	.LC222
	.quad	.LC210
	.section	.rodata.str1.1
.LC223:
	.string	"../src/node_file.cc:1951"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC223
	.quad	.LC159
	.quad	.LC210
	.section	.rodata.str1.1
.LC224:
	.string	"../src/node_file.cc:1949"
.LC225:
	.string	"(argc) >= (5)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL4ReadERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC224
	.quad	.LC225
	.quad	.LC210
	.local	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1928
	.comm	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1928,8,8
	.local	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1925
	.comm	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1925,8,8
	.section	.rodata.str1.1
.LC226:
	.string	"../src/node_file.cc:1910"
.LC227:
	.string	"(argc) == (6)"
	.section	.rodata.str1.8
	.align 8
.LC228:
	.string	"void node::fs::WriteString(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.section	.rodata.str1.1
.LC229:
	.string	"../src/node_file.cc:1850"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC229
	.quad	.LC159
	.quad	.LC228
	.section	.rodata.str1.1
.LC230:
	.string	"../src/node_file.cc:1848"
.LC231:
	.string	"(argc) >= (4)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL11WriteStringERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC228
	.local	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1829
	.comm	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1829,8,8
	.local	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1826
	.comm	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1826,8,8
	.section	.rodata.str1.1
.LC232:
	.string	"../src/node_file.cc:1824"
	.section	.rodata.str1.8
	.align 8
.LC233:
	.string	"void node::fs::WriteBuffers(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC232
	.quad	.LC152
	.quad	.LC233
	.section	.rodata.str1.1
.LC234:
	.string	"../src/node_file.cc:1815"
.LC235:
	.string	"Buffer::HasInstance(chunk)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC234
	.quad	.LC235
	.quad	.LC233
	.section	.rodata.str1.1
.LC236:
	.string	"../src/node_file.cc:1806"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC236
	.quad	.LC205
	.quad	.LC233
	.section	.rodata.str1.1
.LC237:
	.string	"../src/node_file.cc:1803"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC237
	.quad	.LC159
	.quad	.LC233
	.section	.rodata.str1.1
.LC238:
	.string	"../src/node_file.cc:1801"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL12WriteBuffersERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC238
	.quad	.LC161
	.quad	.LC233
	.local	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1784
	.comm	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1784,8,8
	.local	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1781
	.comm	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1781,8,8
	.section	.rodata.str1.1
.LC239:
	.string	"../src/node_file.cc:1779"
	.section	.rodata.str1.8
	.align 8
.LC240:
	.string	"void node::fs::WriteBuffer(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_8, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_8, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_8:
	.quad	.LC239
	.quad	.LC209
	.quad	.LC240
	.section	.rodata.str1.1
.LC241:
	.string	"../src/node_file.cc:1767"
.LC242:
	.string	"(off + len) >= (off)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_7:
	.quad	.LC241
	.quad	.LC242
	.quad	.LC240
	.section	.rodata.str1.1
.LC243:
	.string	"../src/node_file.cc:1766"
.LC244:
	.string	"(len) <= (buffer_length)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_6:
	.quad	.LC243
	.quad	.LC244
	.quad	.LC240
	.section	.rodata.str1.1
.LC245:
	.string	"../src/node_file.cc:1765"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_5:
	.quad	.LC245
	.quad	.LC214
	.quad	.LC240
	.section	.rodata.str1.1
.LC246:
	.string	"../src/node_file.cc:1763"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_4:
	.quad	.LC246
	.quad	.LC216
	.quad	.LC240
	.section	.rodata.str1.1
.LC247:
	.string	"../src/node_file.cc:1761"
.LC248:
	.string	"(off) <= (buffer_length)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC247
	.quad	.LC248
	.quad	.LC240
	.section	.rodata.str1.1
.LC249:
	.string	"../src/node_file.cc:1759"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC249
	.quad	.LC220
	.quad	.LC240
	.section	.rodata.str1.1
.LC250:
	.string	"../src/node_file.cc:1754"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC250
	.quad	.LC222
	.quad	.LC240
	.section	.rodata.str1.1
.LC251:
	.string	"../src/node_file.cc:1751"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC251
	.quad	.LC159
	.quad	.LC240
	.section	.rodata.str1.1
.LC252:
	.string	"../src/node_file.cc:1749"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL11WriteBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC252
	.quad	.LC231
	.quad	.LC240
	.local	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1731
	.comm	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1731,8,8
	.local	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1728
	.comm	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1728,8,8
	.section	.rodata.str1.1
.LC253:
	.string	"../src/node_file.cc:1726"
	.section	.rodata.str1.8
	.align 8
.LC254:
	.string	"void node::fs::CopyFile(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC253
	.quad	.LC152
	.quad	.LC254
	.section	.rodata.str1.1
.LC255:
	.string	"../src/node_file.cc:1717"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC255
	.quad	.LC220
	.quad	.LC254
	.section	.rodata.str1.1
.LC256:
	.string	"../src/node_file.cc:1715"
.LC257:
	.string	"(*dest) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC256
	.quad	.LC257
	.quad	.LC254
	.section	.rodata.str1.1
.LC258:
	.string	"../src/node_file.cc:1712"
.LC259:
	.string	"(*src) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC258
	.quad	.LC259
	.quad	.LC254
	.section	.rodata.str1.1
.LC260:
	.string	"../src/node_file.cc:1709"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL8CopyFileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC260
	.quad	.LC161
	.quad	.LC254
	.local	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1694
	.comm	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1694,8,8
	.local	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1691
	.comm	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1691,8,8
	.section	.rodata.str1.1
.LC261:
	.string	"../src/node_file.cc:1689"
	.section	.rodata.str1.8
	.align 8
.LC262:
	.string	"void node::fs::OpenFileHandle(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC261
	.quad	.LC152
	.quad	.LC262
	.section	.rodata.str1.1
.LC263:
	.string	"../src/node_file.cc:1681"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC263
	.quad	.LC220
	.quad	.LC262
	.section	.rodata.str1.1
.LC264:
	.string	"../src/node_file.cc:1678"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC264
	.quad	.LC192
	.quad	.LC262
	.section	.rodata.str1.1
.LC265:
	.string	"../src/node_file.cc:1676"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC265
	.quad	.LC167
	.quad	.LC262
	.section	.rodata.str1.1
.LC266:
	.string	"../src/node_file.cc:1673"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL14OpenFileHandleERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC266
	.quad	.LC161
	.quad	.LC262
	.local	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1663
	.comm	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1663,8,8
	.local	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1660
	.comm	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1660,8,8
	.section	.rodata.str1.1
.LC267:
	.string	"../src/node_file.cc:1658"
	.section	.rodata.str1.8
	.align 8
.LC268:
	.string	"void node::fs::Open(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC267
	.quad	.LC152
	.quad	.LC268
	.section	.rodata.str1.1
.LC269:
	.string	"../src/node_file.cc:1650"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC269
	.quad	.LC220
	.quad	.LC268
	.section	.rodata.str1.1
.LC270:
	.string	"../src/node_file.cc:1647"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC270
	.quad	.LC192
	.quad	.LC268
	.section	.rodata.str1.1
.LC271:
	.string	"../src/node_file.cc:1645"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC271
	.quad	.LC167
	.quad	.LC268
	.section	.rodata.str1.1
.LC272:
	.string	"../src/node_file.cc:1642"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC272
	.quad	.LC161
	.quad	.LC268
	.section	.rodata.str1.1
.LC273:
	.string	"../src/node_file.cc:1585"
	.section	.rodata.str1.8
	.align 8
.LC274:
	.string	"(req_wrap_sync.req.result) >= (0)"
	.align 8
.LC275:
	.string	"void node::fs::ReadDir(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.local	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1580
	.comm	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1580,8,8
	.local	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1577
	.comm	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1577,8,8
	.section	.rodata.str1.1
.LC276:
	.string	"../src/node_file.cc:1575"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC276
	.quad	.LC152
	.quad	.LC275
	.section	.rodata.str1.1
.LC277:
	.string	"../src/node_file.cc:1559"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC277
	.quad	.LC167
	.quad	.LC275
	.section	.rodata.str1.1
.LC278:
	.string	"../src/node_file.cc:1556"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL7ReadDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC278
	.quad	.LC161
	.quad	.LC275
	.local	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1529
	.comm	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1529,8,8
	.local	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1526
	.comm	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1526,8,8
	.section	.rodata.str1.1
.LC279:
	.string	"../src/node_file.cc:1524"
	.section	.rodata.str1.8
	.align 8
.LC280:
	.string	"void node::fs::RealPath(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC279
	.quad	.LC145
	.quad	.LC280
	.section	.rodata.str1.1
.LC281:
	.string	"../src/node_file.cc:1515"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC281
	.quad	.LC167
	.quad	.LC280
	.section	.rodata.str1.1
.LC282:
	.string	"../src/node_file.cc:1512"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL8RealPathERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC282
	.quad	.LC161
	.quad	.LC280
	.local	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1503
	.comm	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1503,8,8
	.local	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1481
	.comm	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1481,8,8
	.section	.rodata.str1.1
.LC283:
	.string	"../src/node_file.cc:1479"
	.section	.rodata.str1.8
	.align 8
.LC284:
	.string	"void node::fs::MKDir(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC283
	.quad	.LC152
	.quad	.LC284
	.section	.rodata.str1.1
.LC285:
	.string	"../src/node_file.cc:1470"
.LC286:
	.string	"args[2]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC285
	.quad	.LC286
	.quad	.LC284
	.section	.rodata.str1.1
.LC287:
	.string	"../src/node_file.cc:1467"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC287
	.quad	.LC192
	.quad	.LC284
	.section	.rodata.str1.1
.LC288:
	.string	"../src/node_file.cc:1465"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC288
	.quad	.LC167
	.quad	.LC284
	.section	.rodata.str1.1
.LC289:
	.string	"../src/node_file.cc:1462"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL5MKDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC289
	.quad	.LC231
	.quad	.LC284
	.local	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1273
	.comm	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1273,8,8
	.local	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1270
	.comm	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1270,8,8
	.section	.rodata.str1.1
.LC290:
	.string	"../src/node_file.cc:1268"
.LC291:
	.string	"(argc) == (3)"
	.section	.rodata.str1.8
	.align 8
.LC292:
	.string	"void node::fs::RMDir(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC290
	.quad	.LC291
	.quad	.LC292
	.section	.rodata.str1.1
.LC293:
	.string	"../src/node_file.cc:1261"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC293
	.quad	.LC167
	.quad	.LC292
	.section	.rodata.str1.1
.LC294:
	.string	"../src/node_file.cc:1258"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL5RMDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC294
	.quad	.LC150
	.quad	.LC292
	.local	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1250
	.comm	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1250,8,8
	.local	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1248
	.comm	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1248,8,8
	.section	.rodata.str1.1
.LC295:
	.string	"../src/node_file.cc:1246"
	.section	.rodata.str1.8
	.align 8
.LC296:
	.string	"void node::fs::Unlink(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC295
	.quad	.LC291
	.quad	.LC296
	.section	.rodata.str1.1
.LC297:
	.string	"../src/node_file.cc:1239"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC297
	.quad	.LC167
	.quad	.LC296
	.section	.rodata.str1.1
.LC298:
	.string	"../src/node_file.cc:1236"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL6UnlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC298
	.quad	.LC150
	.quad	.LC296
	.local	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1228
	.comm	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1228,8,8
	.local	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1226
	.comm	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1226,8,8
	.section	.rodata.str1.1
.LC299:
	.string	"../src/node_file.cc:1224"
	.section	.rodata.str1.8
	.align 8
.LC300:
	.string	"void node::fs::Fsync(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC299
	.quad	.LC291
	.quad	.LC300
	.section	.rodata.str1.1
.LC301:
	.string	"../src/node_file.cc:1216"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC301
	.quad	.LC159
	.quad	.LC300
	.section	.rodata.str1.1
.LC302:
	.string	"../src/node_file.cc:1214"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL5FsyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC302
	.quad	.LC150
	.quad	.LC300
	.local	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1206
	.comm	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1206,8,8
	.local	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1204
	.comm	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1204,8,8
	.section	.rodata.str1.1
.LC303:
	.string	"../src/node_file.cc:1202"
	.section	.rodata.str1.8
	.align 8
.LC304:
	.string	"void node::fs::Fdatasync(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC303
	.quad	.LC291
	.quad	.LC304
	.section	.rodata.str1.1
.LC305:
	.string	"../src/node_file.cc:1194"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC305
	.quad	.LC159
	.quad	.LC304
	.section	.rodata.str1.1
.LC306:
	.string	"../src/node_file.cc:1192"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL9FdatasyncERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC306
	.quad	.LC150
	.quad	.LC304
	.local	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1184
	.comm	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1184,8,8
	.local	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1181
	.comm	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1181,8,8
	.section	.rodata.str1.1
.LC307:
	.string	"../src/node_file.cc:1179"
	.section	.rodata.str1.8
	.align 8
.LC308:
	.string	"void node::fs::FTruncate(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC307
	.quad	.LC145
	.quad	.LC308
	.section	.rodata.str1.1
.LC309:
	.string	"../src/node_file.cc:1171"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC309
	.quad	.LC157
	.quad	.LC308
	.section	.rodata.str1.1
.LC310:
	.string	"../src/node_file.cc:1168"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC310
	.quad	.LC159
	.quad	.LC308
	.section	.rodata.str1.1
.LC311:
	.string	"../src/node_file.cc:1166"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL9FTruncateERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC311
	.quad	.LC161
	.quad	.LC308
	.local	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1158
	.comm	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1158,8,8
	.local	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1155
	.comm	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1155,8,8
	.section	.rodata.str1.1
.LC312:
	.string	"../src/node_file.cc:1153"
	.section	.rodata.str1.8
	.align 8
.LC313:
	.string	"void node::fs::Rename(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC312
	.quad	.LC145
	.quad	.LC313
	.section	.rodata.str1.1
.LC314:
	.string	"../src/node_file.cc:1145"
.LC315:
	.string	"(*new_path) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC314
	.quad	.LC315
	.quad	.LC313
	.section	.rodata.str1.1
.LC316:
	.string	"../src/node_file.cc:1143"
.LC317:
	.string	"(*old_path) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC316
	.quad	.LC317
	.quad	.LC313
	.section	.rodata.str1.1
.LC318:
	.string	"../src/node_file.cc:1140"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL6RenameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC318
	.quad	.LC161
	.quad	.LC313
	.local	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1114
	.comm	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1114,8,8
	.local	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1111
	.comm	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1111,8,8
	.section	.rodata.str1.1
.LC319:
	.string	"../src/node_file.cc:1109"
	.section	.rodata.str1.8
	.align 8
.LC320:
	.string	"void node::fs::ReadLink(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC319
	.quad	.LC145
	.quad	.LC320
	.section	.rodata.str1.1
.LC321:
	.string	"../src/node_file.cc:1100"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC321
	.quad	.LC167
	.quad	.LC320
	.section	.rodata.str1.1
.LC322:
	.string	"../src/node_file.cc:1097"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL8ReadLinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC322
	.quad	.LC161
	.quad	.LC320
	.local	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1088
	.comm	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1088,8,8
	.local	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1085
	.comm	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1085,8,8
	.section	.rodata.str1.1
.LC323:
	.string	"../src/node_file.cc:1083"
	.section	.rodata.str1.8
	.align 8
.LC324:
	.string	"void node::fs::Link(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC323
	.quad	.LC145
	.quad	.LC324
	.section	.rodata.str1.1
.LC325:
	.string	"../src/node_file.cc:1076"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC325
	.quad	.LC257
	.quad	.LC324
	.section	.rodata.str1.1
.LC326:
	.string	"../src/node_file.cc:1073"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC326
	.quad	.LC259
	.quad	.LC324
	.section	.rodata.str1.1
.LC327:
	.string	"../src/node_file.cc:1070"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL4LinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC327
	.quad	.LC161
	.quad	.LC324
	.local	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1061
	.comm	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1061,8,8
	.local	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1058
	.comm	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1058,8,8
	.section	.rodata.str1.1
.LC328:
	.string	"../src/node_file.cc:1056"
	.section	.rodata.str1.8
	.align 8
.LC329:
	.string	"void node::fs::Symlink(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC328
	.quad	.LC152
	.quad	.LC329
	.section	.rodata.str1.1
.LC330:
	.string	"../src/node_file.cc:1048"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC330
	.quad	.LC220
	.quad	.LC329
	.section	.rodata.str1.1
.LC331:
	.string	"../src/node_file.cc:1046"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC331
	.quad	.LC167
	.quad	.LC329
	.section	.rodata.str1.1
.LC332:
	.string	"../src/node_file.cc:1044"
.LC333:
	.string	"(*target) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC332
	.quad	.LC333
	.quad	.LC329
	.section	.rodata.str1.1
.LC334:
	.string	"../src/node_file.cc:1041"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL7SymlinkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC334
	.quad	.LC231
	.quad	.LC329
	.local	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1025
	.comm	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1025,8,8
	.local	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1023
	.comm	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE29trace_event_unique_atomic1023,8,8
	.section	.rodata.str1.1
.LC335:
	.string	"../src/node_file.cc:1021"
	.section	.rodata.str1.8
	.align 8
.LC336:
	.string	"void node::fs::FStat(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC335
	.quad	.LC145
	.quad	.LC336
	.section	.rodata.str1.1
.LC337:
	.string	"../src/node_file.cc:1012"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC337
	.quad	.LC159
	.quad	.LC336
	.section	.rodata.str1.1
.LC338:
	.string	"../src/node_file.cc:1010"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL5FStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC338
	.quad	.LC150
	.quad	.LC336
	.local	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic995
	.comm	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic995,8,8
	.local	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic992
	.comm	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic992,8,8
	.section	.rodata.str1.1
.LC339:
	.string	"../src/node_file.cc:990"
	.section	.rodata.str1.8
	.align 8
.LC340:
	.string	"void node::fs::LStat(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC339
	.quad	.LC145
	.quad	.LC340
	.section	.rodata.str1.1
.LC341:
	.string	"../src/node_file.cc:982"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC341
	.quad	.LC167
	.quad	.LC340
	.section	.rodata.str1.1
.LC342:
	.string	"../src/node_file.cc:979"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL5LStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC342
	.quad	.LC161
	.quad	.LC340
	.local	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic964
	.comm	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic964,8,8
	.local	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic962
	.comm	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic962,8,8
	.section	.rodata.str1.1
.LC343:
	.string	"../src/node_file.cc:960"
	.section	.rodata.str1.8
	.align 8
.LC344:
	.string	"void node::fs::Stat(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC343
	.quad	.LC145
	.quad	.LC344
	.section	.rodata.str1.1
.LC345:
	.string	"../src/node_file.cc:952"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC345
	.quad	.LC167
	.quad	.LC344
	.section	.rodata.str1.1
.LC346:
	.string	"../src/node_file.cc:949"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL4StatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC346
	.quad	.LC150
	.quad	.LC344
	.section	.rodata.str1.1
.LC347:
	.string	"../src/node_file.cc:931"
.LC348:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC349:
	.string	"void node::fs::InternalModuleStat(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL18InternalModuleStatERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC347
	.quad	.LC348
	.quad	.LC349
	.section	.rodata.str1.1
.LC350:
	.string	"../src/node_file.cc:854"
	.section	.rodata.str1.8
	.align 8
.LC351:
	.string	"(0) == (uv_fs_close(loop, &close_req, fd, nullptr))"
	.align 8
.LC352:
	.string	"node::fs::InternalModuleReadJSON(const v8::FunctionCallbackInfo<v8::Value>&)::<lambda()>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlvE_clEvE4args, @object
	.size	_ZZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlvE_clEvE4args, 24
_ZZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlvE_clEvE4args:
	.quad	.LC350
	.quad	.LC351
	.quad	.LC352
	.section	.rodata.str1.1
.LC353:
	.string	"../src/node_file.cc:836"
	.section	.rodata.str1.8
	.align 8
.LC354:
	.string	"void node::fs::InternalModuleReadJSON(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fsL22InternalModuleReadJSONERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC353
	.quad	.LC348
	.quad	.LC354
	.local	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic825
	.comm	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic825,8,8
	.local	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic823
	.comm	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic823,8,8
	.section	.rodata.str1.1
.LC355:
	.string	"../src/node_file.cc:821"
	.section	.rodata.str1.8
	.align 8
.LC356:
	.string	"void node::fs::Close(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC355
	.quad	.LC291
	.quad	.LC356
	.section	.rodata.str1.1
.LC357:
	.string	"../src/node_file.cc:813"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC357
	.quad	.LC159
	.quad	.LC356
	.section	.rodata.str1.1
.LC358:
	.string	"../src/node_file.cc:811"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fs5CloseERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC358
	.quad	.LC150
	.quad	.LC356
	.local	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic802
	.comm	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic802,8,8
	.local	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic800
	.comm	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic800,8,8
	.section	.rodata.str1.1
.LC359:
	.string	"../src/node_file.cc:798"
	.section	.rodata.str1.8
	.align 8
.LC360:
	.string	"void node::fs::Access(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC359
	.quad	.LC145
	.quad	.LC360
	.section	.rodata.str1.1
.LC361:
	.string	"../src/node_file.cc:791"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC361
	.quad	.LC167
	.quad	.LC360
	.section	.rodata.str1.1
.LC362:
	.string	"../src/node_file.cc:787"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC362
	.quad	.LC192
	.quad	.LC360
	.section	.rodata.str1.1
.LC363:
	.string	"../src/node_file.cc:785"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fs6AccessERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC363
	.quad	.LC150
	.quad	.LC360
	.section	.rodata.str1.1
.LC364:
	.string	"../src/node_file.cc:540"
.LC365:
	.string	"(wrap_->req()) == (req)"
	.section	.rodata.str1.8
	.align 8
.LC366:
	.string	"node::fs::FSReqAfterScope::FSReqAfterScope(node::fs::FSReqBase*, uv_fs_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args, @object
	.size	_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args, 24
_ZZN4node2fs15FSReqAfterScopeC4EPNS0_9FSReqBaseEP7uv_fs_sE4args:
	.quad	.LC364
	.quad	.LC365
	.quad	.LC366
	.section	.rodata.str1.1
.LC367:
	.string	"../src/node_file.cc:530"
.LC368:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC369:
	.string	"void node::fs::NewFSReqCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2fs16NewFSReqCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC367
	.quad	.LC368
	.quad	.LC369
	.section	.rodata.str1.1
.LC370:
	.string	"../src/node_file.cc:425"
	.section	.rodata.str1.8
	.align 8
.LC371:
	.string	"(handle->current_read_.get()) == (req_wrap)"
	.align 8
.LC372:
	.string	"node::fs::FileHandle::ReadStart()::<lambda(uv_fs_t*)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN4node2fs10FileHandle9ReadStartEvENKUlP7uv_fs_sE_clES3_E4args, @object
	.size	_ZZZN4node2fs10FileHandle9ReadStartEvENKUlP7uv_fs_sE_clES3_E4args, 24
_ZZZN4node2fs10FileHandle9ReadStartEvENKUlP7uv_fs_sE_clES3_E4args:
	.quad	.LC370
	.quad	.LC371
	.quad	.LC372
	.section	.rodata.str1.1
.LC373:
	.string	"../src/node_file.cc:305"
.LC374:
	.string	"(close) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC375:
	.string	"node::fs::FileHandle::ClosePromise()::<lambda(uv_fs_t*)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN4node2fs10FileHandle12ClosePromiseEvENKUlP7uv_fs_sE_clES3_E4args, @object
	.size	_ZZZN4node2fs10FileHandle12ClosePromiseEvENKUlP7uv_fs_sE_clES3_E4args, 24
_ZZZN4node2fs10FileHandle12ClosePromiseEvENKUlP7uv_fs_sE_clES3_E4args:
	.quad	.LC373
	.quad	.LC374
	.quad	.LC375
	.section	.rodata.str1.1
.LC376:
	.string	"../src/node_file.cc:292"
.LC377:
	.string	"!reading_"
	.section	.rodata.str1.8
	.align 8
.LC378:
	.string	"v8::MaybeLocal<v8::Promise> node::fs::FileHandle::ClosePromise()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs10FileHandle12ClosePromiseEvE4args_0, @object
	.size	_ZZN4node2fs10FileHandle12ClosePromiseEvE4args_0, 24
_ZZN4node2fs10FileHandle12ClosePromiseEvE4args_0:
	.quad	.LC376
	.quad	.LC377
	.quad	.LC378
	.section	.rodata.str1.1
.LC379:
	.string	"../src/node_file.cc:289"
.LC380:
	.string	"!maybe_resolver.IsEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs10FileHandle12ClosePromiseEvE4args, @object
	.size	_ZZN4node2fs10FileHandle12ClosePromiseEvE4args, 24
_ZZN4node2fs10FileHandle12ClosePromiseEvE4args:
	.quad	.LC379
	.quad	.LC380
	.quad	.LC378
	.section	.rodata.str1.1
.LC381:
	.string	"../src/node_file.cc:174"
.LC382:
	.string	"closed_"
	.section	.rodata.str1.8
	.align 8
.LC383:
	.string	"virtual node::fs::FileHandle::~FileHandle()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs10FileHandleD4EvE4args_0, @object
	.size	_ZZN4node2fs10FileHandleD4EvE4args_0, 24
_ZZN4node2fs10FileHandleD4EvE4args_0:
	.quad	.LC381
	.quad	.LC382
	.quad	.LC383
	.section	.rodata.str1.1
.LC384:
	.string	"../src/node_file.cc:172"
.LC385:
	.string	"!closing_"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs10FileHandleD4EvE4args, @object
	.size	_ZZN4node2fs10FileHandleD4EvE4args, 24
_ZZN4node2fs10FileHandleD4EvE4args:
	.quad	.LC384
	.quad	.LC385
	.quad	.LC383
	.section	.rodata.str1.1
.LC386:
	.string	"../src/node_file.cc:160"
	.section	.rodata.str1.8
	.align 8
.LC387:
	.string	"static void node::fs::FileHandle::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC386
	.quad	.LC159
	.quad	.LC387
	.section	.rodata.str1.1
.LC388:
	.string	"../src/node_file.cc:159"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node2fs10FileHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC388
	.quad	.LC368
	.quad	.LC387
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC389:
	.string	"../src/stream_base-inl.h:103"
.LC390:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC391:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC389
	.quad	.LC390
	.quad	.LC391
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC392:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC393:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC394:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC392
	.quad	.LC393
	.quad	.LC394
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC395:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC396:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC395
	.quad	.LC393
	.quad	.LC396
	.weak	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC397:
	.string	"../src/stream_base-inl.h:26"
	.section	.rodata.str1.8
	.align 8
.LC398:
	.string	"(req_wrap_obj->GetAlignedPointerFromInternalField( StreamReq::kStreamReqField)) == (nullptr)"
	.align 8
.LC399:
	.string	"void node::StreamReq::AttachToObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC397
	.quad	.LC398
	.quad	.LC399
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0
	.section	.rodata.str1.8
	.align 8
.LC400:
	.string	"../src/memory_tracker-inl.h:269"
	.section	.rodata.str1.1
.LC401:
	.string	"(n->size_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC402:
	.string	"void node::MemoryTracker::Track(const node::MemoryRetainer*, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0:
	.quad	.LC400
	.quad	.LC401
	.quad	.LC402
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args
	.section	.rodata.str1.8
	.align 8
.LC403:
	.string	"../src/memory_tracker-inl.h:268"
	.section	.rodata.str1.1
.LC404:
	.string	"(CurrentNode()) == (n)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args:
	.quad	.LC403
	.quad	.LC404
	.quad	.LC402
	.weak	_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args
	.section	.rodata.str1.1
.LC405:
	.string	"../src/node_file-inl.h:57"
.LC406:
	.string	"!has_data_"
	.section	.rodata.str1.8
	.align 8
.LC407:
	.string	"void node::fs::FSReqBase::Init(const char*, const char*, size_t, node::encoding)"
	.section	.data.rel.ro.local._ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args,"awG",@progbits,_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args,comdat
	.align 16
	.type	_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args, @gnu_unique_object
	.size	_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args, 24
_ZZN4node2fs9FSReqBase4InitEPKcS3_mNS_8encodingEE4args:
	.quad	.LC405
	.quad	.LC406
	.quad	.LC407
	.weak	_ZZN4node2fs18FSContinuationData7PopPathB5cxx11EvE4args
	.section	.rodata.str1.1
.LC408:
	.string	"../src/node_file-inl.h:31"
.LC409:
	.string	"(paths_.size()) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC410:
	.string	"std::string node::fs::FSContinuationData::PopPath()"
	.section	.data.rel.ro.local._ZZN4node2fs18FSContinuationData7PopPathB5cxx11EvE4args,"awG",@progbits,_ZZN4node2fs18FSContinuationData7PopPathB5cxx11EvE4args,comdat
	.align 16
	.type	_ZZN4node2fs18FSContinuationData7PopPathB5cxx11EvE4args, @gnu_unique_object
	.size	_ZZN4node2fs18FSContinuationData7PopPathB5cxx11EvE4args, 24
_ZZN4node2fs18FSContinuationData7PopPathB5cxx11EvE4args:
	.quad	.LC408
	.quad	.LC409
	.quad	.LC410
	.weak	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC411:
	.string	"../src/req_wrap-inl.h:13"
	.section	.rodata.str1.8
	.align 8
.LC412:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC413:
	.string	"node::ReqWrapBase::ReqWrapBase(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, 24
_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args:
	.quad	.LC411
	.quad	.LC412
	.quad	.LC413
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC414:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC415:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC416:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC414
	.quad	.LC415
	.quad	.LC416
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC417:
	.string	"../src/base_object-inl.h:195"
.LC418:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC417
	.quad	.LC418
	.quad	.LC416
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC419:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC420:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC421:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC419
	.quad	.LC420
	.quad	.LC421
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC422:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC423:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC424:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC422
	.quad	.LC423
	.quad	.LC424
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC425:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC426:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC427:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC425
	.quad	.LC426
	.quad	.LC427
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC428:
	.string	"../src/env-inl.h:955"
.LC429:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC430:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC428
	.quad	.LC429
	.quad	.LC430
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC431:
	.string	"../src/env-inl.h:399"
.LC432:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC433:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC431
	.quad	.LC432
	.quad	.LC433
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC434:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC435:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC436:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC434
	.quad	.LC435
	.quad	.LC436
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC8:
	.quad	7517463719428581715
	.quad	8239175502546629749
	.align 16
.LC9:
	.quad	7956018234388599622
	.quad	7008848344884994421
	.align 16
.LC10:
	.quad	7236828614798108998
	.quad	8239164412772115820
	.align 16
.LC11:
	.quad	0
	.quad	64
	.align 16
.LC12:
	.quad	18
	.quad	0
	.section	.rodata.cst8
	.align 8
.LC14:
	.long	0
	.long	0
	.section	.rodata.cst16
	.align 16
.LC88:
	.quad	0
	.quad	1024
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
