	.file	"node_stat_watcher.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB4709:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4709:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE:
.LFB4711:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4711:
	.size	_ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node11StatWatcher8SelfSizeEv,"axG",@progbits,_ZNK4node11StatWatcher8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11StatWatcher8SelfSizeEv
	.type	_ZNK4node11StatWatcher8SelfSizeEv, @function
_ZNK4node11StatWatcher8SelfSizeEv:
.LFB4713:
	.cfi_startproc
	endbr64
	movl	$200, %eax
	ret
	.cfi_endproc
.LFE4713:
	.size	_ZNK4node11StatWatcher8SelfSizeEv, .-_ZNK4node11StatWatcher8SelfSizeEv
	.section	.text._ZN4node11StatWatcherD2Ev,"axG",@progbits,_ZN4node11StatWatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11StatWatcherD2Ev
	.type	_ZN4node11StatWatcherD2Ev, @function
_ZN4node11StatWatcherD2Ev:
.LFB10200:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE10200:
	.size	_ZN4node11StatWatcherD2Ev, .-_ZN4node11StatWatcherD2Ev
	.weak	_ZN4node11StatWatcherD1Ev
	.set	_ZN4node11StatWatcherD1Ev,_ZN4node11StatWatcherD2Ev
	.section	.text._ZN4node11StatWatcherD0Ev,"axG",@progbits,_ZN4node11StatWatcherD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11StatWatcherD0Ev
	.type	_ZN4node11StatWatcherD0Ev, @function
_ZN4node11StatWatcherD0Ev:
.LFB10202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdx
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$200, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10202:
	.size	_ZN4node11StatWatcherD0Ev, .-_ZN4node11StatWatcherD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$1056, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	jne	.L38
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L39
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L27
	cmpw	$1040, %cx
	jne	.L11
.L27:
	movq	23(%rdx), %r12
.L13:
	testq	%r12, %r12
	je	.L8
	movq	80(%r12), %rdi
	call	uv_is_active@PLT
	testl	%eax, %eax
	jne	.L40
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movl	16(%rbx), %eax
	leaq	88(%rsi), %rdx
	testl	%eax, %eax
	jle	.L17
	movq	8(%rbx), %rdx
.L17:
	leaq	-1072(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1056(%rbp)
	je	.L41
	cmpl	$1, 16(%rbx)
	jg	.L19
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L20:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L42
	cmpl	$1, 16(%rbx)
	jg	.L22
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L23:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	-1056(%rbp), %rdx
	leaq	88(%r12), %rdi
	leaq	_ZN4node11StatWatcher8CallbackEP12uv_fs_poll_siPK9uv_stat_tS5_(%rip), %rsi
	movl	%eax, %ecx
	call	uv_fs_poll_start@PLT
	testl	%eax, %eax
	je	.L24
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L24:
	movq	-1056(%rbp), %rdi
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L8
	testq	%rdi, %rdi
	je	.L8
	call	free@PLT
.L8:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$1056, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7715:
	.size	_ZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev:
.LFB4712:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$114, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$7166459935295894611, %rcx
	movq	%rdx, (%rdi)
	movl	$25960, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE4712:
	.size	_ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev, .-_ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L46
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L54
.L46:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L52
	cmpw	$1040, %cx
	jne	.L47
.L52:
	movq	23(%rdx), %r12
.L49:
	movq	8(%rbx), %rdi
	movl	16(%rbx), %eax
	leaq	8(%rdi), %r15
	testl	%eax, %eax
	jle	.L55
.L50:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	$200, %edi
	movl	%eax, %r13d
	call	_Znwm@PLT
	movq	%r12, %rsi
	movl	$28, %r8d
	movq	%r15, %rdx
	leaq	88(%rax), %r14
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%r14, %rcx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node11StatWatcherE(%rip), %rax
	movb	%r13b, 192(%rbx)
	movq	%r14, %rsi
	movq	%rax, (%rbx)
	movq	360(%r12), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_poll_init@PLT
	testl	%eax, %eax
	jne	.L56
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L54:
	cmpl	$5, 43(%rax)
	jne	.L46
	leaq	_ZZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	_ZZN4node11StatWatcherC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7714:
	.size	_ZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"StatWatcher"
.LC1:
	.string	"start"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	352(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movq	2680(%rbx), %rdx
	movl	$1, %r9d
	leaq	_ZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movl	$11, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L64
.L58:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L65
.L59:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-96(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L66
.L60:
	movq	3280(%rbx), %rsi
	movq	-88(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L67
.L61:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rsi, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rcx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L67:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L61
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7703:
	.size	_ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11StatWatcherC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.type	_ZN4node11StatWatcherC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb, @function
_ZN4node11StatWatcherC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb:
.LFB7709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$28, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	88(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	movq	%r14, %rcx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node11StatWatcherE(%rip), %rax
	movb	%r13b, 192(%rbx)
	movq	%r14, %rsi
	movq	%rax, (%rbx)
	movq	360(%r12), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_poll_init@PLT
	testl	%eax, %eax
	jne	.L72
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	leaq	_ZZN4node11StatWatcherC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7709:
	.size	_ZN4node11StatWatcherC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb, .-_ZN4node11StatWatcherC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.globl	_ZN4node11StatWatcherC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.set	_ZN4node11StatWatcherC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb,_ZN4node11StatWatcherC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.section	.text._ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,"axG",@progbits,_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,comdat
	.p2align 4
	.weak	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.type	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, @function
_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm:
.LFB8545:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rsi), %r8
	leaq	0(,%rdx,8), %rcx
	movq	%r8, (%rax,%rdx,8)
	movq	8(%rsi), %rdx
	movq	%rdx, 8(%rax,%rcx)
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rax,%rcx)
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rax,%rcx)
	movq	32(%rsi), %rdx
	movq	%rdx, 32(%rax,%rcx)
	movq	40(%rsi), %rdx
	movq	%rdx, 40(%rax,%rcx)
	movq	64(%rsi), %rdx
	movq	%rdx, 48(%rax,%rcx)
	movq	48(%rsi), %rdx
	movq	%rdx, 56(%rax,%rcx)
	movq	56(%rsi), %rdx
	movq	%rdx, 64(%rax,%rcx)
	movq	72(%rsi), %rdx
	movq	%rdx, 72(%rax,%rcx)
	movq	96(%rsi), %rdx
	movq	%rdx, 80(%rax,%rcx)
	movq	104(%rsi), %rdx
	movq	%rdx, 88(%rax,%rcx)
	movq	112(%rsi), %rdx
	movq	%rdx, 96(%rax,%rcx)
	movq	120(%rsi), %rdx
	movq	%rdx, 104(%rax,%rcx)
	movq	128(%rsi), %rdx
	movq	%rdx, 112(%rax,%rcx)
	movq	136(%rsi), %rdx
	movq	%rdx, 120(%rax,%rcx)
	movq	144(%rsi), %rdx
	movq	%rdx, 128(%rax,%rcx)
	movq	152(%rsi), %rdx
	movq	%rdx, 136(%rax,%rcx)
	ret
	.cfi_endproc
.LFE8545:
	.size	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, .-_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.section	.text._ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,"axG",@progbits,_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm,comdat
	.p2align 4
	.weak	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.type	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, @function
_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm:
.LFB8550:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rsi), %rdi
	leaq	0(,%rdx,8), %rcx
	testq	%rdi, %rdi
	js	.L75
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdi, %xmm0
	movsd	%xmm0, (%rax,%rdx,8)
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	js	.L77
.L111:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	16(%rsi), %rdx
	movsd	%xmm0, 8(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L79
.L112:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	24(%rsi), %rdx
	movsd	%xmm0, 16(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L81
.L113:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	32(%rsi), %rdx
	movsd	%xmm0, 24(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L83
.L114:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	40(%rsi), %rdx
	movsd	%xmm0, 32(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L85
.L115:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	64(%rsi), %rdx
	movsd	%xmm0, 40(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L87
.L116:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	48(%rsi), %rdx
	movsd	%xmm0, 48(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L89
.L117:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	56(%rsi), %rdx
	movsd	%xmm0, 56(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L91
.L118:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	72(%rsi), %rdx
	movsd	%xmm0, 64(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L93
.L119:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	96(%rsi), %rdx
	movsd	%xmm0, 72(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L95
.L120:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	104(%rsi), %rdx
	movsd	%xmm0, 80(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L97
.L121:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	112(%rsi), %rdx
	movsd	%xmm0, 88(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L99
.L122:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	120(%rsi), %rdx
	movsd	%xmm0, 96(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L101
.L123:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L102:
	movq	128(%rsi), %rdx
	movsd	%xmm0, 104(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L103
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L104:
	movq	136(%rsi), %rdx
	movsd	%xmm0, 112(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L105
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L106:
	movq	144(%rsi), %rdx
	movsd	%xmm0, 120(%rax,%rcx)
	testq	%rdx, %rdx
	js	.L107
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L108:
	movsd	%xmm0, 128(%rax,%rcx)
	movq	152(%rsi), %rdx
	testq	%rdx, %rdx
	js	.L109
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movsd	%xmm0, 136(%rax,%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rdi, %r8
	andl	$1, %edi
	pxor	%xmm0, %xmm0
	shrq	%r8
	orq	%rdi, %r8
	cvtsi2sdq	%r8, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, (%rax,%rdx,8)
	movq	8(%rsi), %rdx
	testq	%rdx, %rdx
	jns	.L111
.L77:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	16(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L112
.L79:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	24(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L113
.L81:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	32(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L114
.L83:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	40(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L115
.L85:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	64(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 40(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L116
.L87:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	48(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 48(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L117
.L89:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	56(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 56(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L118
.L91:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	72(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 64(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L119
.L93:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	96(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L120
.L95:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	104(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 80(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L121
.L97:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	112(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 88(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L122
.L99:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	movq	120(%rsi), %rdx
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 96(%rax,%rcx)
	testq	%rdx, %rdx
	jns	.L123
.L101:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rdx, %rsi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	orq	%rdx, %rsi
	cvtsi2sdq	%rsi, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 136(%rax,%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%rdx, %rdi
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rdi
	orq	%rdx, %rdi
	cvtsi2sdq	%rdi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L104
	.cfi_endproc
.LFE8550:
	.size	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm, .-_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11StatWatcher8CallbackEP12uv_fs_poll_siPK9uv_stat_tS5_
	.type	_ZN4node11StatWatcher8CallbackEP12uv_fs_poll_siPK9uv_stat_tS5_, @function
_ZN4node11StatWatcher8CallbackEP12uv_fs_poll_siPK9uv_stat_tS5_:
.LFB7711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-88(%rdi), %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	16(%r12), %r13
	movl	%esi, -116(%rbp)
	movq	%rdx, -128(%rbp)
	movq	352(%r13), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpb	$0, 192(%r12)
	je	.L125
	movq	%rbx, %rsi
	leaq	2304(%r13), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2336(%r13), %rbx
	testq	%rbx, %rbx
	je	.L127
	movq	(%rbx), %rsi
	movq	2304(%r13), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rbx
.L127:
	cmpb	$0, 192(%r12)
	je	.L129
	movq	-128(%rbp), %rsi
	leaq	2304(%r13), %rdi
	movl	$18, %edx
	call	_ZN4node2fs14FillStatsArrayImN2v814BigUint64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2336(%r13), %rax
	testq	%rax, %rax
	je	.L131
	movq	2304(%r13), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
.L131:
	movq	352(%r13), %rdi
	movl	-116(%rbp), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%r12), %rdi
	movq	%rbx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	360(%r13), %rax
	movq	16(%r12), %rdx
	movq	1120(%rax), %r13
	testq	%rdi, %rdi
	je	.L133
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L157
.L133:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L136
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L158
.L136:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	-128(%rbp), %rsi
	leaq	2264(%r13), %rdi
	movl	$18, %edx
	call	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2296(%r13), %rax
	testq	%rax, %rax
	je	.L131
	movq	2264(%r13), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%rbx, %rsi
	leaq	2264(%r13), %rdi
	xorl	%edx, %edx
	call	_ZN4node2fs14FillStatsArrayIdN2v812Float64ArrayEEEvPNS_17AliasedBufferBaseIT_T0_NSt9enable_ifIXsrSt9is_scalarIS5_E5valueEvE4typeEEEPK9uv_stat_tm
	movq	2296(%r13), %rbx
	testq	%rbx, %rbx
	je	.L127
	movq	(%rbx), %rsi
	movq	2264(%r13), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rbx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L157:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L136
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7711:
	.size	_ZN4node11StatWatcher8CallbackEP12uv_fs_poll_siPK9uv_stat_tS5_, .-_ZN4node11StatWatcher8CallbackEP12uv_fs_poll_siPK9uv_stat_tS5_
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_GLOBAL__sub_I__ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB10275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10275:
	.size	_GLOBAL__sub_I__ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_GLOBAL__sub_I__ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11StatWatcher10InitializeEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.weak	_ZTVN4node11StatWatcherE
	.section	.data.rel.ro._ZTVN4node11StatWatcherE,"awG",@progbits,_ZTVN4node11StatWatcherE,comdat
	.align 8
	.type	_ZTVN4node11StatWatcherE, @object
	.size	_ZTVN4node11StatWatcherE, 112
_ZTVN4node11StatWatcherE:
	.quad	0
	.quad	0
	.quad	_ZN4node11StatWatcherD1Ev
	.quad	_ZN4node11StatWatcherD0Ev
	.quad	_ZNK4node11StatWatcher10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node11StatWatcher14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node11StatWatcher8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../src/node_stat_watcher.cc:110"
	.section	.rodata.str1.1
.LC3:
	.string	"args[1]->IsUint32()"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"static void node::StatWatcher::Start(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"../src/node_stat_watcher.cc:108"
	.section	.rodata.str1.1
.LC6:
	.string	"(*path) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC4
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"../src/node_stat_watcher.cc:105"
	.align 8
.LC8:
	.string	"!uv_is_active(wrap->GetHandle())"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC4
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"../src/node_stat_watcher.cc:101"
	.section	.rodata.str1.1
.LC10:
	.string	"(args.Length()) == (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11StatWatcher5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC4
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"../src/node_stat_watcher.cc:94"
	.section	.rodata.str1.1
.LC12:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"static void node::StatWatcher::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11StatWatcher3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"../src/node_stat_watcher.cc:72"
	.align 8
.LC15:
	.string	"(0) == (uv_fs_poll_init(env->event_loop(), &watcher_))"
	.align 8
.LC16:
	.string	"node::StatWatcher::StatWatcher(node::Environment*, v8::Local<v8::Object>, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StatWatcherC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEbE4args, @object
	.size	_ZZN4node11StatWatcherC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEbE4args, 24
_ZZN4node11StatWatcherC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEbE4args:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC19:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
