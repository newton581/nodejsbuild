	.file	"node_serdes.cc"
	.text
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7629:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7629:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_117SerializerContext10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_117SerializerContext10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7662:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7662:
	.size	_ZNK4node12_GLOBAL__N_117SerializerContext10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_117SerializerContext10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node12_GLOBAL__N_119DeserializerContext10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node12_GLOBAL__N_117SerializerContext10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_117SerializerContext8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_117SerializerContext8SelfSizeEv:
.LFB7664:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE7664:
	.size	_ZNK4node12_GLOBAL__N_117SerializerContext8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_117SerializerContext8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_119DeserializerContext8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_119DeserializerContext8SelfSizeEv:
.LFB7667:
	.cfi_startproc
	endbr64
	movl	$64, %eax
	ret
	.cfi_endproc
.LFE7667:
	.size	_ZNK4node12_GLOBAL__N_119DeserializerContext8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_119DeserializerContext8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_117SerializerContext14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_117SerializerContext14MemoryInfoNameEv:
.LFB7663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$17, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movb	$116, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7663:
	.size	_ZNK4node12_GLOBAL__N_117SerializerContext14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_117SerializerContext14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_119DeserializerContext14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_119DeserializerContext14MemoryInfoNameEv:
.LFB7666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$19, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$30821, %edx
	movw	%dx, 16(%rax)
	movb	$116, 18(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7666:
	.size	_ZNK4node12_GLOBAL__N_119DeserializerContext14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_119DeserializerContext14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE:
.LFB7678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	%rsi, -32(%rbp)
	testq	%rdi, %rdi
	je	.L15
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L29
.L15:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	792(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
.L16:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L31
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	testq	%rdx, %rdx
	je	.L18
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L32
.L18:
	movq	3280(%rcx), %rsi
	leaq	-32(%rbp), %r8
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L14
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L14:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rcx
	movq	%rax, %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L29:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	_ZZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L16
.L33:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7678:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE
	.set	.LTHUNK0,_ZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE, @function
_ZThn32_N4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE:
.LFB9849:
	.cfi_startproc
	endbr64
	subq	$32, %rdi
	jmp	.LTHUNK0
	.cfi_endproc
.LFE9849:
	.size	_ZThn32_N4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE, .-_ZThn32_N4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"readHostObject must return an object"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE:
.LFB7706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L35
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L52
.L35:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1464(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L53
.L36:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L37
	leaq	32(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v817ValueDeserializer8Delegate14ReadHostObjectEPNS_7IsolateE@PLT
	movq	%rax, %r12
.L38:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leaq	-96(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v87Isolate29AllowJavascriptExecutionScopeC1EPS0_@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	testq	%rdx, %rdx
	je	.L39
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L55
.L39:
	movq	3280(%rcx), %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L41
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L56
.L41:
	movq	%r13, %rdi
	call	_ZN2v87Isolate29AllowJavascriptExecutionScopeD1Ev@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L52:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L56:
	movq	16(%rbx), %rbx
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r14
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L57
.L43:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L55:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rcx
	movq	%rax, %rdx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L53:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L36
.L57:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rdi
	jmp	.L43
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7706:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE, .-_ZN4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE
	.set	.LTHUNK3,_ZN4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE, @function
_ZThn32_N4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE:
.LFB9850:
	.cfi_startproc
	endbr64
	subq	$32, %rdi
	jmp	.LTHUNK3
	.cfi_endproc
.LFE9850:
	.size	_ZThn32_N4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE, .-_ZThn32_N4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE:
.LFB7681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	%rdx, -48(%rbp)
	testq	%rdi, %rdi
	je	.L59
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L73
.L59:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1944(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L74
.L60:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L61
	leaq	32(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v815ValueSerializer8Delegate15WriteHostObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE@PLT
.L62:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L75
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	testq	%rdx, %rdx
	je	.L63
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L76
.L63:
	movq	3280(%rcx), %rsi
	leaq	-48(%rbp), %r8
	movq	%r12, %rdi
	movl	$1, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r8
	movl	$257, %eax
	testq	%r8, %r8
	jne	.L62
	xorb	%al, %al
	movb	$0, %ah
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L73:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L76:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rcx
	movq	%rax, %rdx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L74:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L60
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7681:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE:
.LFB7680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	%rdx, -48(%rbp)
	testq	%rdi, %rdi
	je	.L78
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L92
.L78:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	800(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
.L79:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L80
	leaq	32(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v815ValueSerializer8Delegate22GetSharedArrayBufferIdEPNS_7IsolateENS_5LocalINS_17SharedArrayBufferEEE@PLT
.L81:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L94
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	testq	%rdx, %rdx
	je	.L82
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L95
.L82:
	movq	3280(%rcx), %rsi
	movq	%r12, %rdi
	leaq	-48(%rbp), %r8
	movl	$1, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L81
	movq	16(%rbx), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L92:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L95:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rcx
	movq	%rax, %rdx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L93:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L79
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7680:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ERR_INVALID_ARG_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"buffer must be a TypedArray or a DataView"
	.section	.rodata.str1.1
.LC5:
	.string	"code"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L134
	cmpw	$1040, %cx
	jne	.L97
.L134:
	movq	23(%rdx), %rbx
.L99:
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L156
	movq	8(%r12), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L157
.L102:
	movq	8(%r12), %r14
	movl	16(%r12), %edx
	leaq	8(%r14), %r13
	testl	%edx, %edx
	jg	.L108
	movq	(%r12), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L108:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L158
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %r15
	movl	$40, %edi
	leaq	1(%r15), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r10
	xorl	%edx, %edx
	movq	%r15, 24(%rax)
	movq	%rax, %r13
	movq	%r10, 8(%rax)
	movq	%r12, 16(%rax)
	movq	2592(%rbx), %rsi
	movq	$0, (%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%r8, %r8
	je	.L109
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L110:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L109
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L109
	movq	%rdi, %rax
.L112:
	cmpq	%rcx, %r12
	jne	.L110
	cmpq	%r10, 8(%rax)
	jne	.L110
	cmpq	16(%rax), %r12
	jne	.L110
	cmpq	$0, (%r8)
	je	.L109
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L102
.L157:
	movq	352(%rbx), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L159
.L103:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L160
.L104:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L161
.L105:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L162
.L106:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L163
.L107:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L113
	movq	2584(%rbx), %r9
	movq	%r12, 32(%r13)
	addq	%r9, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L123
.L169:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r15), %rax
	movq	%r13, (%rax)
.L124:
	leaq	16+_ZTVN4node12_GLOBAL__N_119DeserializerContextE(%rip), %rax
	addq	$1, 2608(%rbx)
	leaq	56(%r12), %r13
	movq	%r14, %rdi
	addq	$1, 2656(%rbx)
	movq	%rax, (%r12)
	addq	$96, %rax
	movq	%rax, 32(%r12)
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r14, %rdi
	movq	%rax, 40(%r12)
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	40(%r12), %rdx
	movq	%r13, %rdi
	movq	352(%rbx), %rsi
	movq	%rax, 48(%r12)
	movq	%rax, %rcx
	leaq	32(%r12), %r8
	call	_ZN2v817ValueDeserializerC1EPNS_7IsolateEPKhmPNS0_8DelegateE@PLT
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L126
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L164
.L126:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r14, %rcx
	movq	232(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L165
.L127:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v817ValueDeserializer19SetExpectInlineWasmEb@PLT
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L129
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L166
.L129:
	movq	8(%r12), %rdi
	addq	$24, %rsp
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	popq	%rbx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L164:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L113:
	cmpq	$1, %rdx
	je	.L167
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L168
	leaq	0(,%rdx,8), %r15
	movq	%rdx, -56(%rbp)
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-56(%rbp), %r8
	leaq	2632(%rbx), %r11
	movq	%rax, %r9
.L116:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L118
	xorl	%edi, %edi
	leaq	2600(%rbx), %r10
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L120:
	movq	(%r15), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L121:
	testq	%rsi, %rsi
	je	.L118
.L119:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	jne	.L120
	movq	2600(%rbx), %r15
	movq	%r15, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L133
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L119
	.p2align 4,,10
	.p2align 3
.L118:
	movq	2584(%rbx), %rdi
	cmpq	%rdi, %r11
	je	.L122
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
.L122:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r8, 2592(%rbx)
	divq	%r8
	movq	%r9, 2584(%rbx)
	movq	%r12, 32(%r13)
	leaq	0(,%rdx,8), %r15
	addq	%r9, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L169
.L123:
	movq	2600(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 2600(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L125
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r9,%rdx,8)
.L125:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%rdx, %rdi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L163:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L159:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L161:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L162:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	2632(%rbx), %r9
	movq	$0, 2632(%rbx)
	movq	%r9, %r11
	jmp	.L116
.L168:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7707:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"arrayBuffer must be an ArrayBuffer or SharedArrayBuffer"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L197
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L195
	cmpw	$1040, %cx
	jne	.L172
.L195:
	movq	23(%rdx), %r13
.L174:
	testq	%r13, %r13
	je	.L170
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L198
	movq	8(%rbx), %rdi
.L178:
	movq	16(%r13), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L170
	cmpl	$1, 16(%rbx)
	jg	.L180
	movq	(%rbx), %rdx
	shrq	$32, %rax
	movq	%rax, %r12
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L199
.L182:
	cmpl	$1, 16(%rbx)
	jle	.L200
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value19IsSharedArrayBufferEv@PLT
	testb	%al, %al
	je	.L187
.L201:
	cmpl	$1, 16(%rbx)
	jg	.L188
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L189:
	addq	$24, %rsp
	leaq	56(%r13), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v817ValueDeserializer25TransferSharedArrayBufferEjNS_5LocalINS_17SharedArrayBufferEEE@PLT
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L170:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	8(%rbx), %rcx
	shrq	$32, %rax
	movq	%rax, %r12
	leaq	-8(%rcx), %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	je	.L182
.L199:
	cmpl	$1, 16(%rbx)
	jg	.L183
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L184:
	addq	$24, %rsp
	leaq	56(%r13), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v817ValueDeserializer19TransferArrayBufferEjNS_5LocalINS_11ArrayBufferEEE@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value19IsSharedArrayBufferEv@PLT
	testb	%al, %al
	jne	.L201
.L187:
	movq	16(%r13), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	352(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L202
.L190:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L203
.L191:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L204
.L192:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L205
.L193:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L206
.L194:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	movq	8(%rbx), %rdx
	subq	$8, %rdx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L188:
	movq	8(%rbx), %rdx
	subq	$8, %rdx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L202:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L206:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L205:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L204:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L191
	.cfi_endproc
.LFE7710:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L234
	cmpw	$1040, %cx
	jne	.L208
.L234:
	movq	23(%rdx), %rbx
.L210:
	movl	$48, %edi
	movq	8(%r12), %r13
	call	_Znwm@PLT
	movq	352(%rbx), %rdi
	movq	%rax, %r12
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	addq	$8, %r13
	movq	%rax, (%r12)
	movq	%r13, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L253
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %r14
	movl	$40, %edi
	leaq	1(%r14), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r10
	xorl	%edx, %edx
	movq	%r12, 16(%rax)
	movq	%rax, %r13
	movq	%r10, 8(%rax)
	movq	%r14, 24(%rax)
	movq	2592(%rbx), %rsi
	movq	$0, (%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%r8, %r8
	je	.L211
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L212:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L211
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L211
	movq	%rdi, %rax
.L214:
	cmpq	%rcx, %r12
	jne	.L212
	cmpq	%r10, 8(%rax)
	jne	.L212
	cmpq	%r12, 16(%rax)
	jne	.L212
	cmpq	$0, (%r8)
	je	.L211
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L215
	movq	2584(%rbx), %r8
	movq	%r12, 32(%r13)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L225
.L257:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r15), %rax
	movq	%r13, (%rax)
.L226:
	leaq	16+_ZTVN4node12_GLOBAL__N_117SerializerContextE(%rip), %rax
	addq	$1, 2608(%rbx)
	movq	352(%rbx), %rsi
	leaq	32(%r12), %rdx
	addq	$1, 2656(%rbx)
	leaq	40(%r12), %rdi
	movq	%rax, (%r12)
	addq	$112, %rax
	movq	%rax, 32(%r12)
	call	_ZN2v815ValueSerializerC1EPNS_7IsolateEPNS0_8DelegateE@PLT
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L229
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L254
.L229:
	movq	8(%r12), %rdi
	addq	$24, %rsp
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	popq	%rbx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L215:
	cmpq	$1, %rdx
	je	.L255
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L256
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%rbx), %r10
	movq	%rax, %r8
.L218:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L220
	xorl	%edi, %edi
	leaq	2600(%rbx), %r9
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L222:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L223:
	testq	%rsi, %rsi
	je	.L220
.L221:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L222
	movq	2600(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L233
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L221
	.p2align 4,,10
	.p2align 3
.L220:
	movq	2584(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L224
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L224:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r14, 2592(%rbx)
	divq	%r14
	movq	%r8, 2584(%rbx)
	movq	%r12, 32(%r13)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L257
.L225:
	movq	2600(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 2600(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L227
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r8,%rdx,8)
.L227:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%rdx, %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	leaq	2632(%rbx), %r8
	movq	$0, 2632(%rbx)
	movq	%r8, %r10
	jmp	.L218
.L256:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7682:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L259
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L259:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L269
.L260:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L261
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L260
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7627:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext11WriteHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext11WriteHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L278
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L276
	cmpw	$1040, %cx
	jne	.L272
.L276:
	movq	23(%rdx), %rax
.L274:
	testq	%rax, %rax
	je	.L270
	addq	$8, %rsp
	leaq	40(%rax), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v815ValueSerializer11WriteHeaderEv@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7683:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext11WriteHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext11WriteHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext37SetTreatArrayBufferViewsAsHostObjectsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext37SetTreatArrayBufferViewsAsHostObjectsERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L289
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L287
	cmpw	$1040, %cx
	jne	.L281
.L287:
	movq	23(%rdx), %r12
.L283:
	testq	%r12, %r12
	je	.L279
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L290
	movq	8(%rbx), %rdi
.L286:
	movq	16(%r12), %rax
	movq	352(%rax), %rsi
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	popq	%rbx
	leaq	40(%r12), %rdi
	popq	%r12
	movzbl	%al, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v815ValueSerializer37SetTreatArrayBufferViewsAsHostObjectsEb@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L279:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7685:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext37SetTreatArrayBufferViewsAsHostObjectsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext37SetTreatArrayBufferViewsAsHostObjectsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext20GetWireFormatVersionERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext20GetWireFormatVersionERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L304
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L299
	cmpw	$1040, %cx
	jne	.L293
.L299:
	movq	23(%rdx), %rax
.L295:
	testq	%rax, %rax
	je	.L291
	leaq	56(%rax), %rdi
	movq	(%rbx), %rbx
	call	_ZNK2v817ValueDeserializer20GetWireFormatVersionEv@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L297
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L291:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L305
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L291
.L305:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L291
	.cfi_endproc
.LFE7711:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext20GetWireFormatVersionERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext20GetWireFormatVersionERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext10ReadHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L320
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L315
	cmpw	$1040, %cx
	jne	.L308
.L315:
	movq	23(%rdx), %rax
.L310:
	testq	%rax, %rax
	je	.L306
	movq	16(%rax), %rdx
	leaq	56(%rax), %rdi
	movq	3280(%rdx), %rsi
	call	_ZN2v817ValueDeserializer10ReadHeaderENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L306
	movq	(%rbx), %rdx
	movzbl	%ah, %eax
	cmpb	$1, %al
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
.L306:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7708:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext10ReadHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext9ReadValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext9ReadValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L334
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L329
	cmpw	$1040, %cx
	jne	.L323
.L329:
	movq	23(%rdx), %rax
.L325:
	testq	%rax, %rax
	je	.L321
	movq	16(%rax), %rdx
	leaq	56(%rax), %rdi
	movq	3280(%rdx), %rsi
	call	_ZN2v817ValueDeserializer9ReadValueENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L321
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L321:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7709:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext9ReadValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext9ReadValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L348
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L343
	cmpw	$1040, %cx
	jne	.L337
.L343:
	movq	23(%rdx), %r12
.L339:
	testq	%r12, %r12
	je	.L335
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L349
	movq	8(%rbx), %rdi
.L342:
	movq	16(%r12), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L335
	shrq	$32, %rax
	popq	%rbx
	leaq	40(%r12), %rdi
	popq	%r12
	movl	%eax, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v815ValueSerializer11WriteUint32Ej@PLT
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L335:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7692:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext11WriteDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext11WriteDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L363
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L358
	cmpw	$1040, %cx
	jne	.L352
.L358:
	movq	23(%rdx), %r12
.L354:
	testq	%r12, %r12
	je	.L355
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L364
	movq	8(%rbx), %rdi
.L357:
	movq	16(%r12), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11NumberValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L355
	popq	%rbx
	leaq	40(%r12), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v815ValueSerializer11WriteDoubleEd@PLT
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L355:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7694:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext11WriteDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext11WriteDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC7:
	.string	"ReadDouble() failed"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext10ReadDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L380
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L375
	cmpw	$1040, %cx
	jne	.L367
.L375:
	movq	23(%rdx), %r12
.L369:
	testq	%r12, %r12
	je	.L365
	leaq	-72(%rbp), %rsi
	leaq	56(%r12), %rdi
	call	_ZN2v817ValueDeserializer10ReadDoubleEPd@PLT
	testb	%al, %al
	je	.L381
	movq	(%rbx), %rbx
	movsd	-72(%rbp), %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L382
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L365:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L383
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movq	16(%r12), %rbx
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r13
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L384
.L372:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L380:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L384:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L382:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L365
.L383:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7715:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext10ReadDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext13ReleaseBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext13ReleaseBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L398
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L392
	cmpw	$1040, %cx
	jne	.L387
.L392:
	movq	23(%rdx), %r12
.L389:
	testq	%r12, %r12
	je	.L393
	leaq	40(%r12), %rdi
	call	_ZN2v815ValueSerializer7ReleaseEv@PLT
	movq	16(%r12), %rdi
	movl	$1, %ecx
	movq	%rax, %rsi
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb@PLT
	testq	%rax, %rax
	je	.L393
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L393:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7686:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext13ReleaseBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext13ReleaseBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC8:
	.string	"ReadUint32() failed"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L415
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L410
	cmpw	$1040, %cx
	jne	.L401
.L410:
	movq	23(%rdx), %r12
.L403:
	testq	%r12, %r12
	je	.L399
	leaq	-68(%rbp), %rsi
	leaq	56(%r12), %rdi
	call	_ZN2v817ValueDeserializer10ReadUint32EPj@PLT
	testb	%al, %al
	je	.L416
	movl	-68(%rbp), %esi
	movq	(%rbx), %rbx
	testl	%esi, %esi
	js	.L407
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L399:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L417
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movq	16(%r12), %rbx
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r13
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L418
.L406:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L415:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L407:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L419
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L406
.L419:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L399
.L417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7712:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext10WriteValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext10WriteValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L436
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L431
	cmpw	$1040, %cx
	jne	.L422
.L431:
	movq	23(%rdx), %rax
.L424:
	testq	%rax, %rax
	je	.L420
	movl	16(%rbx), %edx
	leaq	40(%rax), %rdi
	testl	%edx, %edx
	jle	.L437
	movq	8(%rbx), %rdx
.L427:
	movq	16(%rax), %rax
	movq	3280(%rax), %rsi
	call	_ZN2v815ValueSerializer10WriteValueENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L420
	movq	(%rbx), %rdx
	movzbl	%ah, %eax
	cmpb	$1, %al
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
.L420:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L422:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7684:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext10WriteValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext10WriteValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"source must be a TypedArray or a DataView"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext13WriteRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext13WriteRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L465
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L460
	cmpw	$1040, %cx
	jne	.L440
.L460:
	movq	23(%rdx), %r12
.L442:
	testq	%r12, %r12
	je	.L438
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L466
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L467
.L446:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L452
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L453:
	movq	%r13, %rdi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L468
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	ja	.L457
	movq	%r13, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L469
.L457:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	leaq	(%rbx,%rax), %rsi
	movq	%rsi, -80(%rbp)
.L456:
	movq	-72(%rbp), %rdx
	leaq	40(%r12), %rdi
	call	_ZN2v815ValueSerializer13WriteRawBytesEPKvm@PLT
.L438:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L470
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L446
.L467:
	movq	16(%r12), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	352(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L471
.L447:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L472
.L448:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L473
.L449:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L474
.L450:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L475
.L451:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L452:
	movq	8(%rbx), %r13
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	-144(%rbp), %rbx
	movl	$64, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%rbx, -80(%rbp)
	movq	%rbx, %rsi
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L468:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L475:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L474:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L473:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%rax, -216(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-216(%rbp), %rdi
	jmp	.L448
.L470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7695:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext13WriteRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext13WriteRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC10:
	.string	"ReadUint64() failed"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L491
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L486
	cmpw	$1040, %cx
	jne	.L478
.L486:
	movq	23(%rdx), %r12
.L480:
	testq	%r12, %r12
	je	.L476
	leaq	-104(%rbp), %rsi
	leaq	56(%r12), %rdi
	call	_ZN2v817ValueDeserializer10ReadUint64EPm@PLT
	testb	%al, %al
	je	.L492
	movq	16(%r12), %rax
	movq	-104(%rbp), %r13
	movq	352(%rax), %r12
	movq	%r13, %rsi
	shrq	$32, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rbx), %rbx
	movl	$2, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L493
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L476:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L494
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	16(%r12), %rbx
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r13
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L495
.L483:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L491:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L495:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L493:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L476
.L494:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7713:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L511
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L506
	cmpw	$1040, %cx
	jne	.L498
.L506:
	movq	23(%rdx), %r13
.L500:
	testq	%r13, %r13
	je	.L496
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L512
	movq	8(%rbx), %rdi
.L503:
	movq	16(%r13), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	cmpl	$1, 16(%rbx)
	movq	%rax, %r12
	jg	.L504
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L505:
	movq	16(%r13), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%r12b, %r12b
	je	.L496
	testb	%al, %al
	je	.L496
	movq	%r12, %rsi
	addq	$8, %rsp
	shrq	$32, %rax
	leaq	40(%r13), %rdi
	shrq	$32, %rsi
	popq	%rbx
	popq	%r12
	salq	$32, %rsi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orq	%rax, %rsi
	jmp	_ZN2v815ValueSerializer11WriteUint64Em@PLT
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L504:
	movq	8(%rbx), %rdi
	subq	$8, %rdi
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L498:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7693:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"arrayBuffer must be an ArrayBuffer"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117SerializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L535
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L533
	cmpw	$1040, %cx
	jne	.L515
.L533:
	movq	23(%rdx), %r13
.L517:
	testq	%r13, %r13
	je	.L513
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L536
	movq	8(%rbx), %rdi
.L521:
	movq	16(%r13), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L513
	cmpl	$1, 16(%rbx)
	jg	.L523
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L524:
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	je	.L537
	cmpl	$1, 16(%rbx)
	jle	.L538
	movq	8(%rbx), %rdx
	subq	$8, %rdx
.L532:
	addq	$24, %rsp
	movq	%r12, %rsi
	leaq	40(%r13), %rdi
	popq	%rbx
	shrq	$32, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v815ValueSerializer19TransferArrayBufferEjNS_5LocalINS_11ArrayBufferEEE@PLT
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L513:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L538:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L515:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L537:
	movq	16(%r13), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	352(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L539
.L526:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L540
.L527:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L541
.L528:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L542
.L529:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L543
.L530:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L539:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L526
.L543:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L530
.L542:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L529
.L541:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L528
.L540:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L527
	.cfi_endproc
.LFE7691:
	.size	_ZN4node12_GLOBAL__N_117SerializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_117SerializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC12:
	.string	"ReadRawBytes() failed"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L566
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L561
	cmpw	$1040, %cx
	jne	.L546
.L561:
	movq	23(%rdx), %r12
.L548:
	testq	%r12, %r12
	je	.L554
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L567
	movq	8(%rbx), %rdi
.L551:
	movq	16(%r12), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %r13
	testb	%al, %al
	je	.L554
	leaq	-72(%rbp), %rdx
	leaq	56(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v817ValueDeserializer12ReadRawBytesEmPPKv@PLT
	testb	%al, %al
	je	.L568
	movq	-72(%rbp), %rdx
	movq	40(%r12), %rax
	cmpq	%rdx, %rax
	ja	.L569
	movq	48(%r12), %rcx
	addq	%rdx, %r13
	addq	%rax, %rcx
	cmpq	%rcx, %r13
	ja	.L570
	movq	%rdx, %rsi
	subq	%rax, %rsi
	movl	%esi, %ecx
	addq	%rcx, %rax
	cmpq	%rax, %rdx
	jne	.L571
	movq	(%rbx), %rbx
	testl	%esi, %esi
	js	.L558
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L554:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L572
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L568:
	movq	16(%r12), %rbx
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r13
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L573
.L553:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L570:
	leaq	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L558:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L574
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L554
.L573:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L553
.L574:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L554
.L572:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7716:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE, @function
_ZThn32_N4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE:
.LFB9845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-16(%rdi), %rax
	movq	-24(%rdi), %rdi
	movq	%rdx, -48(%rbp)
	testq	%rdi, %rdi
	je	.L576
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L590
.L576:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1944(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L591
.L577:
	movq	%r13, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L578
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v815ValueSerializer8Delegate15WriteHostObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE@PLT
	movl	%eax, %ecx
	movzbl	%ah, %eax
.L579:
	xorl	%edx, %edx
	movb	%cl, %dl
	movb	%al, %dh
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L592
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L578:
	.cfi_restore_state
	movq	-24(%r12), %rdx
	movq	-16(%r12), %rcx
	testq	%rdx, %rdx
	je	.L580
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L593
.L580:
	movq	3280(%rcx), %rsi
	leaq	-48(%rbp), %r8
	movq	%r13, %rdi
	movl	$1, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	cmpq	$1, %rax
	sbbl	%ecx, %ecx
	addl	$1, %ecx
	testq	%rax, %rax
	setne	%al
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L590:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	-16(%r12), %rax
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L593:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-16(%r12), %rcx
	movq	%rax, %rdx
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L591:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L577
.L592:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9845:
	.size	_ZThn32_N4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE, .-_ZThn32_N4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE, @function
_ZThn32_N4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE:
.LFB9846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-16(%rdi), %rax
	movq	-24(%rdi), %rdi
	movq	%rdx, -64(%rbp)
	testq	%rdi, %rdi
	je	.L595
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L611
.L595:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	800(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L612
.L596:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L597
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v815ValueSerializer8Delegate22GetSharedArrayBufferIdEPNS_7IsolateENS_5LocalINS_17SharedArrayBufferEEE@PLT
.L610:
	movq	%rax, %rbx
	movl	%eax, %edx
	shrq	$32, %rax
	movq	%rax, -72(%rbp)
.L598:
	movq	-72(%rbp), %r14
	movb	%dl, %bl
	movl	%ebx, %eax
	salq	$32, %r14
	orq	%r14, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L613
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L597:
	.cfi_restore_state
	movq	-24(%r12), %rdx
	movq	-16(%r12), %rcx
	testq	%rdx, %rdx
	je	.L599
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L614
.L599:
	movq	3280(%rcx), %rsi
	movq	%r14, %rdi
	leaq	-64(%rbp), %r8
	movl	$1, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	xorl	%edx, %edx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L598
	movq	-16(%r12), %rax
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L611:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	-16(%r12), %rax
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L614:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-16(%r12), %rcx
	movq	%rax, %rdx
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L612:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L596
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9846:
	.size	_ZThn32_N4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE, .-_ZThn32_N4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE
	.section	.rodata.str1.1
.LC13:
	.string	"writeHeader"
.LC14:
	.string	"writeValue"
.LC15:
	.string	"releaseBuffer"
.LC16:
	.string	"transferArrayBuffer"
.LC17:
	.string	"writeUint32"
.LC18:
	.string	"writeUint64"
.LC19:
	.string	"writeDouble"
.LC20:
	.string	"writeRawBytes"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"_setTreatArrayBufferViewsAsHostObjects"
	.section	.rodata.str1.1
.LC22:
	.string	"Serializer"
.LC23:
	.string	"readHeader"
.LC24:
	.string	"readValue"
.LC25:
	.string	"getWireFormatVersion"
.LC26:
	.string	"readUint32"
.LC27:
	.string	"readUint64"
.LC28:
	.string	"readDouble"
.LC29:
	.string	"_readRawBytes"
.LC30:
	.string	"Deserializer"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB31:
	.text
.LHOTB31:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L616
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L616
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L616
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext11WriteHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L644
.L617:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext10WriteValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L645
.L618:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext13ReleaseBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r13
	popq	%rax
	testq	%r13, %r13
	je	.L646
.L619:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L647
.L620:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L648
.L621:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext11WriteUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L649
.L622:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext11WriteDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L650
.L623:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext13WriteRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r13
	popq	%rax
	testq	%r13, %r13
	je	.L651
.L624:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117SerializerContext37SetTreatArrayBufferViewsAsHostObjectsERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L652
.L625:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L653
.L626:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L654
.L627:
	movq	3280(%rbx), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L655
.L628:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadHeaderERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L656
.L629:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext9ReadValueERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC24(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L657
.L630:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext20GetWireFormatVersionERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC25(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L658
.L631:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext19TransferArrayBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r13
	popq	%rax
	testq	%r13, %r13
	je	.L659
.L632:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L660
.L633:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadUint64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L661
.L634:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext10ReadDoubleERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC28(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L662
.L635:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC29(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L663
.L636:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	.LC30(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L664
.L637:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L665
.L638:
	movq	3280(%rbx), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L666
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L645:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L646:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L647:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L648:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L649:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L650:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L651:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L652:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L653:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L654:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L655:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L656:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L657:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L658:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L659:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L660:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L661:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L662:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L663:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L664:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7717:
.L616:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7717:
	.text
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE31:
	.text
.LHOTE31:
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L668
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L668
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L668
	movq	%r9, %rdi
.L671:
	cmpq	%rsi, %rbx
	jne	.L669
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L669
	cmpq	16(%rdi), %rbx
	jne	.L669
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L703
	testq	%rsi, %rsi
	je	.L673
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L673
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L673:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L668:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L704
.L676:
	cmpq	$0, 8(%rbx)
	je	.L667
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L680
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L705
.L680:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L667
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L667:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L706
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L685
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L673
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L672:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L707
.L674:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L705:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L685:
	movq	%r10, %rax
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L704:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L708
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L676
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%rsi, 2600(%r12)
	jmp	.L674
.L708:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7615:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContextD2Ev, @function
_ZN4node12_GLOBAL__N_119DeserializerContextD2Ev:
.LFB9736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_119DeserializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	56(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	addq	$96, %rax
	movq	%rax, -24(%rdi)
	call	_ZN2v817ValueDeserializerD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE9736:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContextD2Ev, .-_ZN4node12_GLOBAL__N_119DeserializerContextD2Ev
	.set	_ZN4node12_GLOBAL__N_119DeserializerContextD1Ev,_ZN4node12_GLOBAL__N_119DeserializerContextD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_119DeserializerContextD0Ev, @function
_ZN4node12_GLOBAL__N_119DeserializerContextD0Ev:
.LFB9738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_119DeserializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	56(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	addq	$96, %rax
	movq	%rax, -24(%rdi)
	call	_ZN2v817ValueDeserializerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9738:
	.size	_ZN4node12_GLOBAL__N_119DeserializerContextD0Ev, .-_ZN4node12_GLOBAL__N_119DeserializerContextD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContextD2Ev, @function
_ZN4node12_GLOBAL__N_117SerializerContextD2Ev:
.LFB9740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117SerializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$112, %rax
	movq	%rax, -8(%rdi)
	call	_ZN2v815ValueSerializerD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE9740:
	.size	_ZN4node12_GLOBAL__N_117SerializerContextD2Ev, .-_ZN4node12_GLOBAL__N_117SerializerContextD2Ev
	.set	_ZN4node12_GLOBAL__N_117SerializerContextD1Ev,_ZN4node12_GLOBAL__N_117SerializerContextD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117SerializerContextD0Ev, @function
_ZN4node12_GLOBAL__N_117SerializerContextD0Ev:
.LFB9742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117SerializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	40(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$112, %rax
	movq	%rax, -8(%rdi)
	call	_ZN2v815ValueSerializerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9742:
	.size	_ZN4node12_GLOBAL__N_117SerializerContextD0Ev, .-_ZN4node12_GLOBAL__N_117SerializerContextD0Ev
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD1Ev, @function
_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD1Ev:
.LFB9841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_119DeserializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	addq	$24, %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	addq	$96, %rax
	movq	%rax, -24(%rdi)
	call	_ZN2v817ValueDeserializerD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE9841:
	.size	_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD1Ev, .-_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD1Ev
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_117SerializerContextD1Ev, @function
_ZThn32_N4node12_GLOBAL__N_117SerializerContextD1Ev:
.LFB9842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117SerializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$112, %rax
	movq	%rax, -8(%rdi)
	call	_ZN2v815ValueSerializerD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE9842:
	.size	_ZThn32_N4node12_GLOBAL__N_117SerializerContextD1Ev, .-_ZThn32_N4node12_GLOBAL__N_117SerializerContextD1Ev
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD0Ev, @function
_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD0Ev:
.LFB9843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_119DeserializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	addq	$24, %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	addq	$96, %rax
	movq	%rax, -24(%rdi)
	call	_ZN2v817ValueDeserializerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9843:
	.size	_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD0Ev, .-_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD0Ev
	.p2align 4
	.type	_ZThn32_N4node12_GLOBAL__N_117SerializerContextD0Ev, @function
_ZThn32_N4node12_GLOBAL__N_117SerializerContextD0Ev:
.LFB9844:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117SerializerContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$112, %rax
	movq	%rax, -8(%rdi)
	call	_ZN2v815ValueSerializerD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9844:
	.size	_ZThn32_N4node12_GLOBAL__N_117SerializerContextD0Ev, .-_ZThn32_N4node12_GLOBAL__N_117SerializerContextD0Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7617:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.text
	.p2align 4
	.globl	_Z16_register_serdesv
	.type	_Z16_register_serdesv, @function
_Z16_register_serdesv:
.LFB7718:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7718:
	.size	_Z16_register_serdesv, .-_Z16_register_serdesv
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_117SerializerContextE, @object
	.size	_ZTVN4node12_GLOBAL__N_117SerializerContextE, 192
_ZTVN4node12_GLOBAL__N_117SerializerContextE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_117SerializerContextD1Ev
	.quad	_ZN4node12_GLOBAL__N_117SerializerContextD0Ev
	.quad	_ZNK4node12_GLOBAL__N_117SerializerContext10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_117SerializerContext14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_117SerializerContext8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE
	.quad	_ZN4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE
	.quad	_ZN4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE
	.quad	-32
	.quad	0
	.quad	_ZThn32_N4node12_GLOBAL__N_117SerializerContextD1Ev
	.quad	_ZThn32_N4node12_GLOBAL__N_117SerializerContextD0Ev
	.quad	_ZThn32_N4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEE
	.quad	_ZThn32_N4node12_GLOBAL__N_117SerializerContext15WriteHostObjectEPN2v87IsolateENS2_5LocalINS2_6ObjectEEE
	.quad	_ZThn32_N4node12_GLOBAL__N_117SerializerContext22GetSharedArrayBufferIdEPN2v87IsolateENS2_5LocalINS2_17SharedArrayBufferEEE
	.quad	_ZN2v815ValueSerializer8Delegate23GetWasmModuleTransferIdEPNS_7IsolateENS_5LocalINS_16WasmModuleObjectEEE
	.quad	_ZN2v815ValueSerializer8Delegate22ReallocateBufferMemoryEPvmPm
	.quad	_ZN2v815ValueSerializer8Delegate16FreeBufferMemoryEPv
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_119DeserializerContextE, @object
	.size	_ZTVN4node12_GLOBAL__N_119DeserializerContextE, 152
_ZTVN4node12_GLOBAL__N_119DeserializerContextE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_119DeserializerContextD1Ev
	.quad	_ZN4node12_GLOBAL__N_119DeserializerContextD0Ev
	.quad	_ZNK4node12_GLOBAL__N_119DeserializerContext10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_119DeserializerContext14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_119DeserializerContext8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZN4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE
	.quad	-32
	.quad	0
	.quad	_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD1Ev
	.quad	_ZThn32_N4node12_GLOBAL__N_119DeserializerContextD0Ev
	.quad	_ZThn32_N4node12_GLOBAL__N_119DeserializerContext14ReadHostObjectEPN2v87IsolateE
	.quad	_ZN2v817ValueDeserializer8Delegate19GetWasmModuleFromIdEPNS_7IsolateEj
	.quad	_ZN2v817ValueDeserializer8Delegate26GetSharedArrayBufferFromIdEPNS_7IsolateEj
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC32:
	.string	"../src/util-inl.h:495"
.LC33:
	.string	"value->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Value>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"../src/node_serdes.cc"
.LC36:
	.string	"serdes"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC35
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC36
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC37:
	.string	"../src/node_serdes.cc:441"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"(ctx->data_ + offset) == (position)"
	.align 8
.LC39:
	.string	"static void node::{anonymous}::DeserializerContext::ReadRawBytes(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.section	.rodata.str1.1
.LC40:
	.string	"../src/node_serdes.cc:438"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"(position + length) <= (ctx->data_ + ctx->length_)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC39
	.section	.rodata.str1.1
.LC42:
	.string	"../src/node_serdes.cc:437"
.LC43:
	.string	"(position) >= (ctx->data_)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_119DeserializerContext12ReadRawBytesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC39
	.section	.rodata.str1.1
.LC44:
	.string	"../src/node_serdes.cc:110"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"get_data_clone_error->IsFunction()"
	.align 8
.LC46:
	.string	"virtual void node::{anonymous}::SerializerContext::ThrowDataCloneError(v8::Local<v8::String>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEEE4args, 24
_ZZN4node12_GLOBAL__N_117SerializerContext19ThrowDataCloneErrorEN2v85LocalINS2_6StringEEEE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC49:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC50:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC52:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC53:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC55:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC56:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC58:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC59:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC61:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	8820700510818559315
	.quad	8675468274861044325
	.align 16
.LC1:
	.quad	7809639168886400324
	.quad	8389765491310557801
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
