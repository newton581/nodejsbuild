	.file	"node_http_parser_traditional.cc"
	.text
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7149:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7149:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB7589:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7589:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB7590:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7590:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB7606:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7606:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv:
.LFB7677:
	.cfi_startproc
	endbr64
	movl	$1752, %eax
	ret
	.cfi_endproc
.LFE7677:
	.size	_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"current_buffer"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1728(%rdi), %rax
	testq	%rax, %rax
	je	.L7
	movq	8(%rsi), %r12
	movq	%rsi, %rbx
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L12
	cmpq	72(%rbx), %rcx
	je	.L17
.L10:
	movq	-8(%rcx), %rsi
.L9:
	leaq	.LC0(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L7:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%esi, %esi
	jmp	.L9
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7675:
	.size	_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB10015:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE10015:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB10019:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE10019:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_125InitMaxHttpHeaderSizeOnceEv, @function
_ZN4node12_GLOBAL__N_125InitMaxHttpHeaderSizeOnceEv:
.LFB7713:
	.cfi_startproc
	endbr64
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	120(%rax), %rdi
	jmp	http_parser_set_max_header_size@PLT
	.cfi_endproc
.LFE7713:
	.size	_ZN4node12_GLOBAL__N_125InitMaxHttpHeaderSizeOnceEv, .-_ZN4node12_GLOBAL__N_125InitMaxHttpHeaderSizeOnceEv
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7624:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7624:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7625:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L33
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L33:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7625:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L63
	cmpw	$1040, %cx
	jne	.L35
.L63:
	movq	23(%rdx), %r15
.L37:
	cmpl	$2, 16(%rbx)
	jg	.L38
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	16(%rbx), %ecx
	movl	%eax, %r13d
	testl	%ecx, %ecx
	jg	.L40
.L81:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L75
.L42:
	cmpl	$1, 16(%rbx)
	jle	.L76
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L44:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L77
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L46
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L47:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, %r14d
	cmpl	$1, %eax
	ja	.L78
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L79
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L64
	cmpw	$1040, %cx
	jne	.L50
.L64:
	movq	23(%rdx), %r12
.L52:
	testq	%r12, %r12
	je	.L34
	cmpq	%r15, 16(%r12)
	jne	.L80
	xorl	%eax, %eax
	testl	%r14d, %r14d
	setne	%al
	addl	$16, %eax
	cmpl	$1, 16(%rbx)
	movl	%eax, 32(%r12)
	jg	.L56
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L57:
	movsd	.LC1(%rip), %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	sall	$7, %r13d
	call	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb@PLT
	leaq	80(%r12), %rdi
	movl	%r14d, %esi
	call	http_parser_init@PLT
	movzbl	82(%r12), %edx
	andl	$127, %edx
	orl	%edx, %r13d
	cmpb	$0, 1664(%r12)
	movb	%r13b, 82(%r12)
	je	.L58
	movq	1656(%r12), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdaPv@PLT
.L59:
	movb	$0, 1664(%r12)
.L58:
	cmpb	$0, 1688(%r12)
	movq	$0, 1656(%r12)
	movq	$0, 1672(%r12)
	je	.L60
	movq	1680(%r12), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdaPv@PLT
.L61:
	movb	$0, 1688(%r12)
.L60:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	$0, 1680(%r12)
	movq	$0, 1712(%r12)
	movw	%ax, 1720(%r12)
	movups	%xmm0, 1696(%r12)
.L34:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L38:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	16(%rbx), %ecx
	movl	%eax, %r13d
	testl	%ecx, %ecx
	jle	.L81
.L40:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L42
.L75:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	movq	8(%rbx), %rdi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L56:
	movq	8(%rbx), %rsi
	subq	$8, %rsi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7697:
	.size	_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP11http_parser, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP11http_parser:
.LFB8499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-80(%rdi), %rbx
	subq	$8, %rsp
	cmpb	$0, 1664(%rbx)
	movups	%xmm0, 1704(%rbx)
	je	.L83
	movq	1656(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdaPv@PLT
.L84:
	movb	$0, 1664(%rbx)
.L83:
	cmpb	$0, 1688(%rbx)
	movq	$0, 1656(%rbx)
	movq	$0, 1672(%rbx)
	je	.L85
	movq	1680(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdaPv@PLT
.L86:
	movb	$0, 1688(%rbx)
.L85:
	movq	$0, 1680(%rbx)
	xorl	%eax, %eax
	movq	$0, 1696(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8499:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP11http_parser, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP11http_parser
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm, @function
_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm:
.LFB7702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	cmpb	$0, 2192(%r13)
	jne	.L107
	movq	%rdi, %rbx
	movq	2184(%r13), %rdi
	movb	$1, 2192(%r13)
	testq	%rdi, %rdi
	je	.L108
.L97:
	addq	$8, %rsp
	movl	$65536, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	$65536, %edi
	call	_Znam@PLT
	cmpq	$0, 2184(%r13)
	jne	.L109
	movq	%rax, 2184(%r13)
	movq	16(%rbx), %rax
	movq	2184(%rax), %rdi
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L107:
	testq	%rsi, %rsi
	movl	$1, %r13d
	movq	%rsi, %r12
	cmovne	%rsi, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L110
.L96:
	addq	$8, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	leaq	_ZZN4node11Environment22set_http_parser_bufferEPcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L110:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%r12, %r12
	je	.L96
	testq	%rax, %rax
	jne	.L96
	leaq	_ZZN4node6MallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7702:
	.size	_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm, .-_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16ParserD2Ev, @function
_ZN4node12_GLOBAL__N_16ParserD2Ev:
.LFB9962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 1688(%rdi)
	movq	%rax, 56(%rdi)
	je	.L112
	movq	1680(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdaPv@PLT
.L112:
	cmpb	$0, 1664(%r13)
	je	.L113
	movq	1656(%r13), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdaPv@PLT
.L113:
	leaq	1632(%r13), %r12
	leaq	864(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L115:
	cmpb	$0, 8(%r12)
	je	.L114
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdaPv@PLT
.L114:
	subq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L115
	leaq	96(%r13), %r12
	.p2align 4,,10
	.p2align 3
.L117:
	cmpb	$0, 8(%rbx)
	je	.L116
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdaPv@PLT
.L116:
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L117
	movq	64(%r13), %rcx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 56(%r13)
	testq	%rcx, %rcx
	je	.L118
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L119
	leaq	56(%r13), %rdx
	cmpq	%rax, %rdx
	jne	.L121
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L147:
	cmpq	%rax, %rdx
	je	.L150
.L121:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L147
.L119:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L149:
	movq	72(%r13), %rax
	movq	%rax, 8(%rcx)
.L118:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
.L150:
	.cfi_restore_state
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L118
	.cfi_endproc
.LFE9962:
	.size	_ZN4node12_GLOBAL__N_16ParserD2Ev, .-_ZN4node12_GLOBAL__N_16ParserD2Ev
	.set	_ZN4node12_GLOBAL__N_16ParserD1Ev,_ZN4node12_GLOBAL__N_16ParserD2Ev
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv:
.LFB7676:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1936875856, 16(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$29285, %edx
	movw	%dx, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	ret
	.cfi_endproc
.LFE7676:
	.size	_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L158
	cmpw	$1040, %cx
	jne	.L153
.L158:
	movq	23(%rdx), %r13
.L155:
	movq	8(%rbx), %r12
	movl	$1752, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	addq	$8, %r12
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%r12, %rdx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE@PLT
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	leaq	120(%rbx), %rdx
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	leaq	888(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L156:
	movb	$0, 8(%rdx)
	addq	$24, %rdx
	movq	$0, -24(%rdx)
	movq	$0, -8(%rdx)
	cmpq	%rdx, %rax
	jne	.L156
	leaq	1656(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L157:
	movb	$0, 8(%rax)
	addq	$24, %rax
	movq	$0, -24(%rax)
	movq	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L157
	movb	$0, 1664(%rbx)
	movq	$0, 1656(%rbx)
	movq	$0, 1672(%rbx)
	movb	$0, 1688(%rbx)
	movq	$0, 1680(%rbx)
	movq	$0, 1696(%rbx)
	movq	$0, 1728(%rbx)
	movq	$0, 1736(%rbx)
	movq	$0, 1744(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L155
	.cfi_endproc
.LFE7691:
	.size	_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP11http_parserS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP11http_parserS4_m:
.LFB8507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	movq	%r15, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L163
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L175
.L163:
	movq	3280(%rdx), %rsi
	movl	$2, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L176
.L164:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L169
	movq	16(%rbx), %rdx
	movq	1728(%rbx), %rax
	movq	1744(%rbx), %rsi
	movq	352(%rdx), %rdi
	testq	%rax, %rax
	je	.L177
.L167:
	subq	%rsi, %r13
	movq	%rax, -80(%rbp)
	movq	%r13, %rsi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movl	%r14d, %esi
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	leaq	-80(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	$3, %edx
	movq	%rax, -64(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L178
.L169:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	xorl	%eax, %eax
.L162:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L179
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L177:
	movq	1736(%rbx), %rdx
	call	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L180
.L168:
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	16(%rbx), %rdx
	movq	1744(%rbx), %rsi
	movq	%rax, 1728(%rbx)
	movq	352(%rdx), %rdi
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L176:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L178:
	movb	$1, 1721(%rbx)
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movl	$33, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rsi
	jmp	.L168
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8507:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP11http_parserS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP11http_parserS4_m
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm, @function
_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm:
.LFB10085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	-40(%rdi), %r13
	cmpb	$0, 2192(%r13)
	jne	.L194
	movq	%rdi, %rbx
	movq	2184(%r13), %rdi
	movb	$1, 2192(%r13)
	testq	%rdi, %rdi
	je	.L195
.L184:
	addq	$8, %rsp
	movl	$65536, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movl	$65536, %edi
	call	_Znam@PLT
	cmpq	$0, 2184(%r13)
	jne	.L196
	movq	%rax, 2184(%r13)
	movq	-40(%rbx), %rax
	movq	2184(%rax), %rdi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L194:
	testq	%rsi, %rsi
	movl	$1, %r13d
	movq	%rsi, %r12
	cmovne	%rsi, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L197
.L183:
	addq	$8, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	leaq	_ZZN4node11Environment22set_http_parser_bufferEPcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%r12, %r12
	je	.L183
	testq	%rax, %rax
	jne	.L183
	leaq	_ZZN4node6MallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10085:
	.size	_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm, .-_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm, @function
_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm:
.LFB7705:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	leaq	80(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r13, 1736(%rbx)
	leaq	_ZN4node12_GLOBAL__N_16Parser8settingsE(%rip), %rsi
	movq	%r12, 1744(%rbx)
	movb	$0, 1721(%rbx)
	call	http_parser_execute@PLT
	movq	%rax, -104(%rbp)
	movzbl	111(%rbx), %eax
	andl	$127, %eax
	movb	%al, -112(%rbp)
	testq	%r12, %r12
	je	.L252
	cmpb	$0, 1664(%rbx)
	jne	.L203
	movq	1672(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L253
.L203:
	cmpb	$0, 1688(%rbx)
	jne	.L204
	movq	1696(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L254
.L204:
	movq	1704(%rbx), %rax
	leaq	120(%rbx), %r14
	xorl	%r13d, %r13d
	testq	%rax, %rax
	jne	.L209
	.p2align 4,,10
	.p2align 3
.L208:
	movq	1712(%rbx), %rax
	leaq	888(%rbx), %r13
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jne	.L211
	.p2align 4,,10
	.p2align 3
.L202:
	cmpb	$0, 1721(%rbx)
	movq	$0, 1728(%rbx)
	movq	$0, 1736(%rbx)
	movq	$0, 1744(%rbx)
	je	.L212
.L220:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L213:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	call	_Znam@PLT
	movq	(%r14), %rsi
	movq	16(%r14), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$1, 8(%r14)
	movq	%rax, (%r14)
	movq	1704(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L207:
	addq	$1, %r13
	addq	$24, %r14
	cmpq	%r13, %rax
	jbe	.L208
.L209:
	cmpb	$0, 8(%r14)
	jne	.L207
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L207
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L257:
	call	_Znam@PLT
	movq	0(%r13), %rsi
	movq	16(%r13), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$1, 8(%r13)
	movq	%rax, 0(%r13)
	movq	1712(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L210:
	addq	$1, %r14
	addq	$24, %r13
	cmpq	%rax, %r14
	jnb	.L202
.L211:
	cmpb	$0, 8(%r13)
	jne	.L210
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L210
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L212:
	movq	16(%rbx), %rax
	movl	-104(%rbp), %esi
	movzbl	-112(%rbp), %r14d
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	cmpb	$0, 111(%rbx)
	movq	%rax, %r13
	js	.L214
	testl	%r14d, %r14d
	jne	.L258
.L214:
	testq	%r12, %r12
	je	.L220
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L252:
	testq	%r13, %r13
	jne	.L259
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	jne	.L260
	cmpb	$0, 1721(%rbx)
	movq	$0, 1728(%rbx)
	movq	$0, 1736(%rbx)
	movq	$0, 1744(%rbx)
	jne	.L220
	movq	16(%rbx), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L260:
	cmpq	$1, %rax
	jne	.L261
	movq	$0, -104(%rbp)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L258:
	movq	16(%rbx), %rax
	movq	360(%rax), %rax
	movq	1304(%rax), %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r12
	movq	%rax, -104(%rbp)
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L262
.L215:
	movq	16(%rbx), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	240(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L263
.L216:
	movl	%r14d, %edi
	call	http_errno_name@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L264
.L217:
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	328(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L265
.L218:
	movq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L254:
	call	_Znam@PLT
	movq	1680(%rbx), %rsi
	movq	1696(%rbx), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$1, 1688(%rbx)
	movq	%rax, 1680(%rbx)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L253:
	call	_Znam@PLT
	movq	1656(%rbx), %rsi
	movq	1672(%rbx), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$1, 1664(%rbx)
	movq	%rax, 1656(%rbx)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L265:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L218
.L264:
	movq	%rax, -112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %rcx
	jmp	.L217
.L263:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L216
.L262:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L215
.L255:
	call	__stack_chk_fail@PLT
.L261:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7705:
	.size	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm, .-_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, @function
_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t:
.LFB7703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	js	.L291
	movq	0(%r13), %rsi
	jne	.L292
	movq	16(%rbx), %rax
	cmpq	2184(%rax), %rsi
	je	.L288
.L278:
	movq	%rsi, %rdi
	call	free@PLT
.L277:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L293
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movq	$0, 1728(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L290
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L273
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L294
.L273:
	movq	3280(%rdx), %rsi
	movl	$4, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L295
.L274:
	movq	%r15, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L275
.L290:
	movq	0(%r13), %rsi
.L297:
	movq	16(%rbx), %rax
	cmpq	2184(%rax), %rsi
	jne	.L278
.L288:
	movb	$0, 2192(%rax)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L275:
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	leaq	-96(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, 1736(%rbx)
	movq	%r15, %rsi
	movq	%rax, 1744(%rbx)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	16(%rbx), %rax
	movq	0(%r13), %rdi
	movq	$0, 1736(%rbx)
	movq	$0, 1744(%rbx)
	cmpq	2184(%rax), %rdi
	je	.L288
	call	free@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L294:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L291:
	movq	72(%rbx), %r15
	testq	%r15, %r15
	je	.L296
	movq	(%r15), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rax), %rcx
	movq	%rcx, -104(%rbp)
	call	uv_buf_init@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	leaq	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t(%rip), %rax
	movq	%rdx, -88(%rbp)
	leaq	-96(%rbp), %rdx
	cmpq	%rax, %rcx
	jne	.L269
	movq	%r12, %rsi
	leaq	-56(%r15), %rdi
	call	.LTHUNK1
	movq	0(%r13), %rsi
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L295:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L296:
	leaq	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7703:
	.size	_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, .-_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.set	.LTHUNK1,_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP11http_parserS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP11http_parserS4_m:
.LFB8501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	subq	$24, %rsp
	movq	1656(%rbx), %rax
	movq	1672(%rbx), %r12
	testq	%rax, %rax
	je	.L305
	cmpb	$0, 1664(%rbx)
	jne	.L301
	addq	%r12, %rax
	cmpq	%rax, %rsi
	je	.L300
.L301:
	leaq	(%r14,%r12), %rdi
	call	_Znam@PLT
	movq	1656(%rbx), %r8
	movq	1672(%rbx), %r12
	movq	%rax, %r13
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	leaq	0(%r13,%r12), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	cmpb	$0, 1664(%rbx)
	movq	-56(%rbp), %r8
	jne	.L306
	movb	$1, 1664(%rbx)
.L303:
	movq	%r13, 1656(%rbx)
.L300:
	addq	%r14, %r12
	xorl	%eax, %eax
	movq	%r12, 1672(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movq	%rsi, 1656(%rbx)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	1672(%rbx), %r12
	jmp	.L303
	.cfi_endproc
.LFE8501:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP11http_parserS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP11http_parserS4_m
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP11http_parserS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP11http_parserS4_m:
.LFB8503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	subq	$24, %rsp
	movq	1680(%rbx), %rax
	movq	1696(%rbx), %r12
	testq	%rax, %rax
	je	.L314
	cmpb	$0, 1688(%rbx)
	jne	.L310
	addq	%r12, %rax
	cmpq	%rax, %rsi
	je	.L309
.L310:
	leaq	(%r14,%r12), %rdi
	call	_Znam@PLT
	movq	1680(%rbx), %r8
	movq	1696(%rbx), %r12
	movq	%rax, %r13
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r12, %rdx
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	leaq	0(%r13,%r12), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	cmpb	$0, 1688(%rbx)
	movq	-56(%rbp), %r8
	jne	.L315
	movb	$1, 1688(%rbx)
.L312:
	movq	%r13, 1680(%rbx)
.L309:
	addq	%r14, %r12
	xorl	%eax, %eax
	movq	%r12, 1696(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	%rsi, 1680(%rbx)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	1696(%rbx), %r12
	jmp	.L312
	.cfi_endproc
.LFE8503:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP11http_parserS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP11http_parserS4_m
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP11http_parserS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP11http_parserS4_m:
.LFB8505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	1712(%r12), %rbx
	cmpq	1704(%r12), %rbx
	je	.L317
	leaq	(%rbx,%rbx,2), %rdx
	leaq	1(%rbx), %rax
	leaq	(%r12,%rdx,8), %rdx
	movq	%rax, 1712(%r12)
	cmpb	$0, 896(%rdx)
	jne	.L333
.L318:
	leaq	(%rbx,%rbx,2), %rdx
	leaq	(%r12,%rdx,8), %rdx
	movq	$0, 888(%rdx)
	movq	$0, 904(%rdx)
	cmpq	$31, %rax
	ja	.L327
	movq	1704(%r12), %rbx
	cmpq	%rbx, %rax
	jne	.L334
.L321:
	subq	$1, %rbx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r12,%rax,8), %rax
	movq	888(%rax), %rdx
	testq	%rdx, %rdx
	je	.L335
	cmpb	$0, 896(%rax)
	movq	904(%rax), %rdi
	jne	.L324
	addq	%rdi, %rdx
	cmpq	%rdx, %r14
	je	.L323
.L324:
	addq	%r13, %rdi
	call	_Znam@PLT
	movq	%rax, %r15
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r12,%rax,8), %rcx
	movq	%r15, %rdi
	movq	888(%rcx), %r8
	movq	904(%rcx), %rdx
	movq	%rcx, -72(%rbp)
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	movq	%r14, %rsi
	leaq	(%r15,%rdx), %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %r8
	cmpb	$0, 896(%rcx)
	jne	.L336
	movb	$1, 896(%rcx)
.L326:
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r12,%rax,8), %rax
	movq	%r15, 888(%rax)
	movq	904(%rax), %rdi
.L323:
	leaq	(%rbx,%rbx,2), %rax
	addq	%r13, %rdi
	movq	%rdi, 904(%r12,%rax,8)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	888(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZdaPv@PLT
	movq	1712(%r12), %rax
.L319:
	leaq	(%rbx,%rbx,2), %rdx
	movb	$0, 896(%r12,%rdx,8)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L317:
	cmpq	$31, %rbx
	jbe	.L321
.L327:
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r14, 888(%rax)
	movq	904(%rax), %rdi
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8505:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP11http_parserS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP11http_parserS4_m
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv, @function
_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv:
.LFB7706:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %rdx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-576(%rbp), %r12
	pushq	%rbx
	movq	%r12, %r13
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L338:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L338
	movq	16(%r14), %rax
	movq	1712(%r14), %rdx
	movq	352(%rax), %rdi
	testq	%rdx, %rdx
	je	.L339
	leaq	120(%r14), %rbx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L347:
	movq	16(%rbx), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L364
.L342:
	movq	%rax, 0(%r13)
	movq	784(%rbx), %rax
	testq	%rax, %rax
	je	.L343
	movq	768(%rbx), %rsi
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%rax, 784(%rbx)
	testq	%rax, %rax
	je	.L343
.L346:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L351
	cmpb	$9, %dl
	je	.L351
	movq	768(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L348
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
.L349:
	movq	%rax, 8(%r13)
	addq	$1, %r15
	addq	$24, %rbx
	addq	$16, %r13
	movq	1712(%r14), %rax
	cmpq	%r15, %rax
	ja	.L347
	leaq	(%rax,%rax), %rdx
.L339:
	movq	%r12, %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L365
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	leaq	128(%rdi), %rax
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L341
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L342
.L348:
	movq	%rax, -584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-584(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L349
.L341:
	movq	%rax, -584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-584(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L342
.L365:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7706:
	.size	_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv, .-_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L374
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L372
	cmpw	$1040, %cx
	jne	.L368
.L372:
	movq	23(%rdx), %r12
.L370:
	testq	%r12, %r12
	je	.L366
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrap11EmitDestroyEb@PLT
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7693:
	.size	_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L402
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L387
	cmpw	$1040, %cx
	jne	.L377
.L387:
	movq	23(%rdx), %rax
.L379:
	testq	%rax, %rax
	je	.L375
	movq	64(%rax), %rsi
	testq	%rsi, %rsi
	je	.L375
	movq	8(%rsi), %rdx
	leaq	56(%rax), %rcx
	testq	%rdx, %rdx
	je	.L381
	cmpq	%rdx, %rcx
	jne	.L383
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L400:
	cmpq	%rdx, %rcx
	je	.L404
.L383:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L400
.L381:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L404:
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rsi)
.L385:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 64(%rax)
.L375:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L402:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L403:
	movq	72(%rax), %rdx
	movq	%rdx, 8(%rsi)
	jmp	.L385
	.cfi_endproc
.LFE7700:
	.size	_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev, @function
_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev:
.LFB10081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 1632(%rdi)
	movq	%rax, (%rdi)
	je	.L406
	movq	1624(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	_ZdaPv@PLT
.L406:
	cmpb	$0, 1608(%r13)
	je	.L407
	movq	1600(%r13), %rdi
	testq	%rdi, %rdi
	je	.L407
	call	_ZdaPv@PLT
.L407:
	leaq	1576(%r13), %r12
	leaq	808(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L409:
	cmpb	$0, 8(%r12)
	je	.L408
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L408
	call	_ZdaPv@PLT
.L408:
	subq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L409
	leaq	40(%r13), %r12
	.p2align 4,,10
	.p2align 3
.L411:
	cmpb	$0, 8(%rbx)
	je	.L410
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L410
	call	_ZdaPv@PLT
.L410:
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L411
	movq	8(%r13), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdx, %rdx
	je	.L412
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L413
	cmpq	%rax, %r13
	jne	.L415
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L441:
	cmpq	%rax, %r13
	je	.L444
.L415:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L441
.L413:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L443:
	movq	16(%r13), %rax
	movq	%rax, 8(%rdx)
.L412:
	subq	$56, %r13
	movq	%r13, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$1752, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L444:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	%rax, 16(%rdx)
	jmp	.L412
	.cfi_endproc
.LFE10081:
	.size	_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev, .-_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16ParserD0Ev, @function
_ZN4node12_GLOBAL__N_16ParserD0Ev:
.LFB9964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 1688(%rdi)
	movq	%rax, 56(%rdi)
	je	.L446
	movq	1680(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L446
	call	_ZdaPv@PLT
.L446:
	cmpb	$0, 1664(%r12)
	je	.L447
	movq	1656(%r12), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZdaPv@PLT
.L447:
	leaq	1632(%r12), %r13
	leaq	864(%r12), %rbx
	.p2align 4,,10
	.p2align 3
.L449:
	cmpb	$0, 8(%r13)
	je	.L448
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L448
	call	_ZdaPv@PLT
.L448:
	subq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L449
	leaq	96(%r12), %r13
	.p2align 4,,10
	.p2align 3
.L451:
	cmpb	$0, 8(%rbx)
	je	.L450
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L450
	call	_ZdaPv@PLT
.L450:
	subq	$24, %rbx
	cmpq	%r13, %rbx
	jne	.L451
	movq	64(%r12), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 56(%r12)
	testq	%rdx, %rdx
	je	.L452
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L453
	leaq	56(%r12), %rcx
	cmpq	%rax, %rcx
	jne	.L455
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L481:
	cmpq	%rax, %rcx
	je	.L484
.L455:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L481
.L453:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L483:
	movq	72(%r12), %rax
	movq	%rax, 8(%rdx)
.L452:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1752, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L452
	.cfi_endproc
.LFE9964:
	.size	_ZN4node12_GLOBAL__N_16ParserD0Ev, .-_ZN4node12_GLOBAL__N_16ParserD0Ev
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L495
	cmpw	$1040, %cx
	jne	.L486
.L495:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L498
.L489:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L496
	cmpw	$1040, %cx
	jne	.L490
.L496:
	movq	23(%rdx), %rax
.L492:
	testq	%rax, %rax
	je	.L485
	cmpq	%r13, 16(%rax)
	jne	.L499
	addq	$8, %rsp
	leaq	80(%rax), %rdi
	movl	$1, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	http_parser_pause@PLT
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	%rdi, %rbx
	xorl	%esi, %esi
	leaq	32(%r12), %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L489
	.p2align 4,,10
	.p2align 3
.L498:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L490:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L485:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8523:
	.size	_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L510
	cmpw	$1040, %cx
	jne	.L501
.L510:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L513
.L504:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L511
	cmpw	$1040, %cx
	jne	.L505
.L511:
	movq	23(%rdx), %rax
.L507:
	testq	%rax, %rax
	je	.L500
	cmpq	%r13, 16(%rax)
	jne	.L514
	addq	$8, %rsp
	leaq	80(%rax), %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	http_parser_pause@PLT
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	movq	%rdi, %rbx
	xorl	%esi, %esi
	leaq	32(%r12), %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L504
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L505:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L500:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8524:
	.size	_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev, @function
_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev:
.LFB10086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 1632(%rdi)
	movq	%rax, (%rdi)
	je	.L516
	movq	1624(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L516
	call	_ZdaPv@PLT
.L516:
	cmpb	$0, 1608(%r13)
	je	.L517
	movq	1600(%r13), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZdaPv@PLT
.L517:
	leaq	1576(%r13), %r12
	leaq	808(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L519:
	cmpb	$0, 8(%r12)
	je	.L518
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L518
	call	_ZdaPv@PLT
.L518:
	subq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L519
	leaq	40(%r13), %r12
	.p2align 4,,10
	.p2align 3
.L521:
	cmpb	$0, 8(%rbx)
	je	.L520
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L520
	call	_ZdaPv@PLT
.L520:
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L521
	movq	8(%r13), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdx, %rdx
	je	.L522
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L523
	cmpq	%rax, %r13
	jne	.L525
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L551:
	cmpq	%rax, %r13
	je	.L554
.L525:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L551
.L523:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L553:
	movq	16(%r13), %rax
	movq	%rax, 8(%rdx)
.L522:
	addq	$8, %rsp
	leaq	-56(%r13), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
.L554:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	%rax, 16(%rdx)
	jmp	.L522
	.cfi_endproc
.LFE10086:
	.size	_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev, .-_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L567
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L562
	cmpw	$1040, %cx
	jne	.L557
.L562:
	movq	23(%rdx), %rax
.L559:
	testq	%rax, %rax
	je	.L555
	movq	1736(%rax), %rdx
	movq	1744(%rax), %rsi
	movq	16(%rax), %rdi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	testq	%rax, %rax
	je	.L568
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L555:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L568:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L555
	.cfi_endproc
.LFE7701:
	.size	_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L605
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L591
	cmpw	$1040, %cx
	jne	.L571
.L591:
	movq	23(%rdx), %r13
.L573:
	testq	%r13, %r13
	je	.L569
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L606
	movq	8(%rbx), %rdi
.L576:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L607
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L578
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L579:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L592
	cmpw	$1040, %cx
	jne	.L580
.L592:
	cmpq	$0, 23(%rdx)
	je	.L583
.L582:
	movq	31(%rdx), %rax
.L587:
	testq	%rax, %rax
	je	.L583
	cmpq	$0, 64(%r13)
	leaq	56(%r13), %rdx
	jne	.L608
	movq	8(%rax), %rcx
	movq	%rax, 64(%r13)
	movq	%rcx, 72(%r13)
	movq	%rdx, 8(%rax)
.L569:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L578:
	movq	8(%rbx), %r12
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L571:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L605:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L607:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L580:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L583
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L582
	cmpw	$1040, %cx
	je	.L582
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L608:
	leaq	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7699:
	.size	_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L653
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L627
	cmpw	$1040, %cx
	jne	.L611
.L627:
	movq	23(%rdx), %r12
.L613:
	testq	%r12, %r12
	je	.L609
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpb	$0, 1688(%r12)
	movq	%rax, 56(%r12)
	je	.L615
	movq	1680(%r12), %rdi
	testq	%rdi, %rdi
	je	.L615
	call	_ZdaPv@PLT
.L615:
	cmpb	$0, 1664(%r12)
	leaq	1656(%r12), %r13
	jne	.L654
.L616:
	leaq	888(%r12), %rbx
	.p2align 4,,10
	.p2align 3
.L618:
	subq	$24, %r13
	cmpb	$0, 8(%r13)
	je	.L617
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L617
	call	_ZdaPv@PLT
.L617:
	cmpq	%rbx, %r13
	jne	.L618
	leaq	120(%r12), %r13
	.p2align 4,,10
	.p2align 3
.L620:
	subq	$24, %rbx
	cmpb	$0, 8(%rbx)
	je	.L619
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L619
	call	_ZdaPv@PLT
.L619:
	cmpq	%rbx, %r13
	jne	.L620
	movq	64(%r12), %rcx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 56(%r12)
	testq	%rcx, %rcx
	je	.L621
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L622
	leaq	56(%r12), %rdx
	cmpq	%rax, %rdx
	jne	.L624
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L651:
	cmpq	%rax, %rdx
	je	.L656
.L624:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L651
.L622:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L654:
	movq	1656(%r12), %rdi
	testq	%rdi, %rdi
	je	.L616
	call	_ZdaPv@PLT
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L655:
	movq	72(%r12), %rax
	movq	%rax, 8(%rcx)
.L621:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1752, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L653:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L656:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L621
	.cfi_endproc
.LFE7692:
	.size	_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L679
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L677
	cmpw	$1040, %cx
	jne	.L659
.L677:
	movq	23(%rdx), %r12
.L661:
	testq	%r12, %r12
	je	.L657
	cmpq	$0, 1728(%r12)
	jne	.L680
	cmpq	$0, 1736(%r12)
	jne	.L681
	cmpq	$0, 1744(%r12)
	jne	.L682
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L683
	movq	8(%rbx), %r13
.L667:
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L684
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -56(%rbp)
	cmpq	$64, %rax
	ja	.L671
	movq	%r13, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L685
.L671:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-192(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	leaq	(%r14,%rax), %rsi
	movq	%rsi, -64(%rbp)
.L670:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L672
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L673:
	movq	%rax, 1728(%r12)
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	testq	%rax, %rax
	je	.L657
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L657:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L686
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L672:
	movq	8(%rbx), %rax
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L659:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	-128(%rbp), %r14
	movl	$64, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -64(%rbp)
	movq	%r14, %rsi
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L681:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L682:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L684:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L686:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7695:
	.size	_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L688
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L688:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L698
.L689:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L690
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L689
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7147:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP11http_parserS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP11http_parserS4_m:
.LFB8504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	subq	$104, %rsp
	movq	1704(%rbx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	1712(%rbx), %r13
	je	.L732
	cmpq	$31, %r13
	ja	.L733
.L713:
	movq	1712(%rbx), %r12
	leaq	1(%r12), %rax
	cmpq	%r13, %rax
	jne	.L734
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L735
	cmpb	$0, 128(%rax)
	movq	136(%rax), %rdi
	jne	.L717
	addq	%rdi, %rdx
	cmpq	%rdx, %r15
	je	.L716
.L717:
	addq	%r14, %rdi
	call	_Znam@PLT
	movq	%rax, %r13
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %rcx
	movq	%r13, %rdi
	movq	120(%rcx), %r8
	movq	136(%rcx), %rdx
	movq	%rcx, -136(%rbp)
	movq	%r8, %rsi
	movq	%r8, -120(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	movq	%r15, %rsi
	leaq	0(%r13,%rdx), %rdi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	-136(%rbp), %rcx
	movq	-120(%rbp), %r8
	cmpb	$0, 128(%rcx)
	jne	.L736
	movb	$1, 128(%rcx)
.L719:
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%r13, 120(%rax)
	movq	136(%rax), %rdi
.L716:
	leaq	(%r12,%r12,2), %rax
	addq	%rdi, %r14
	movq	%r14, 136(%rbx,%rax,8)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L737
	addq	$104, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	leaq	1(%r13), %rax
	movq	%rax, 1704(%rbx)
	cmpq	$32, %rax
	je	.L738
.L701:
	leaq	0(%r13,%r13,2), %rdx
	leaq	(%rbx,%rdx,8), %rdx
	cmpb	$0, 128(%rdx)
	je	.L711
	movq	120(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L712
	call	_ZdaPv@PLT
	movq	1704(%rbx), %rax
.L712:
	leaq	0(%r13,%r13,2), %rdx
	movb	$0, 128(%rbx,%rdx,8)
.L711:
	leaq	0(%r13,%r13,2), %rdx
	movq	%rax, %r13
	leaq	(%rbx,%rdx,8), %rdx
	movq	$0, 120(%rdx)
	movq	$0, 136(%rdx)
	cmpq	$31, %r13
	jbe	.L713
.L733:
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L735:
	movq	%r15, 120(%rax)
	movq	136(%rax), %rdi
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L736:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L734:
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L738:
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L702
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L739
.L702:
	movq	3280(%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L740
.L703:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L731
	movq	%rbx, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv
	movq	1672(%rbx), %rcx
	movq	%rax, -80(%rbp)
	movq	16(%rbx), %rax
	testq	%rcx, %rcx
	jne	.L741
	movq	352(%rax), %rax
	subq	$-128, %rax
.L707:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L742
.L708:
	cmpb	$0, 1664(%rbx)
	je	.L709
	movq	1656(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L710
	call	_ZdaPv@PLT
.L710:
	movb	$0, 1664(%rbx)
.L709:
	movq	$0, 1656(%rbx)
	movq	$0, 1672(%rbx)
	movb	$1, 1720(%rbx)
.L731:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v811HandleScopeD1Ev@PLT
	movdqa	.LC2(%rip), %xmm0
	movl	$1, %eax
	movups	%xmm0, 1704(%rbx)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L739:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L741:
	movq	1656(%rbx), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L707
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rax
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L742:
	movb	$1, 1721(%rbx)
	jmp	.L708
.L740:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L703
.L737:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8504:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP11http_parserS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP11http_parserS4_m
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP11http_parser, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP11http_parser:
.LFB8506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	8(%r14), %rdi
	movq	16(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -656(%rbp)
	movq	$0, -592(%rbp)
	movaps	%xmm0, -640(%rbp)
	movaps	%xmm0, -624(%rbp)
	movaps	%xmm0, -608(%rbp)
	testq	%rdi, %rdi
	je	.L744
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L837
.L744:
	movq	3280(%rdx), %rsi
	movl	$1, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L838
.L745:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L839
	movq	16(%r14), %rax
	cmpb	$0, 1720(%r14)
	movq	352(%rax), %rdi
	leaq	88(%rdi), %rax
	movq	%rax, %xmm0
	movq	%rax, -592(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -656(%rbp)
	movaps	%xmm0, -640(%rbp)
	movaps	%xmm0, -624(%rbp)
	movaps	%xmm0, -608(%rbp)
	je	.L748
	leaq	-704(%rbp), %r13
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r14), %rdi
	movq	16(%r14), %rdx
	testq	%rdi, %rdi
	je	.L749
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L840
.L749:
	movq	3280(%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, -736(%rbp)
	testq	%rax, %rax
	je	.L841
.L750:
	movq	-736(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L836
	leaq	-576(%rbp), %rax
	leaq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, -720(%rbp)
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L753:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L753
	movq	16(%r14), %rax
	movq	1712(%r14), %rdx
	movq	352(%rax), %rdi
	testq	%rdx, %rdx
	je	.L754
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L762:
	movq	56(%rbx,%r8), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L842
.L757:
	movq	%rax, (%r10)
	movq	904(%r14,%r8), %rax
	testq	%rax, %rax
	je	.L758
	movq	888(%r14,%r8), %rsi
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L801:
	movq	%rax, 904(%r14,%r8)
	testq	%rax, %rax
	je	.L758
.L761:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L801
	cmpb	$9, %dl
	je	.L801
	movq	808(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -728(%rbp)
	movq	%r8, -712(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-712(%rbp), %r8
	movq	-728(%rbp), %r10
	testq	%rax, %rax
	je	.L796
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
.L797:
	addq	$1, %r15
	movq	%rax, 8(%r10)
	addq	$24, %r8
	addq	$16, %r10
	movq	1712(%r14), %rdx
	cmpq	%r15, %rdx
	ja	.L762
	addq	%rdx, %rdx
.L754:
	movq	-720(%rbp), %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	1672(%r14), %rcx
	movq	%rax, -576(%rbp)
	movq	16(%r14), %rax
	testq	%rcx, %rcx
	jne	.L843
	movq	352(%rax), %rbx
	leaq	128(%rbx), %rax
.L764:
	movq	-720(%rbp), %rcx
	movq	-736(%rbp), %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L844
.L765:
	cmpb	$0, 1664(%r14)
	je	.L766
	movq	1656(%r14), %rdi
	testq	%rdi, %rdi
	je	.L767
	call	_ZdaPv@PLT
.L767:
	movb	$0, 1664(%r14)
.L766:
	movq	$0, 1656(%r14)
	movq	$0, 1672(%r14)
	movb	$1, 1720(%r14)
.L836:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	16(%r14), %rax
	movzbl	80(%r14), %edx
	movq	352(%rax), %rdi
	andl	$3, %edx
.L752:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 1704(%r14)
	testb	%dl, %dl
	je	.L783
.L779:
	cmpb	$1, %dl
	je	.L845
.L784:
	movzwl	104(%r14), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movzwl	106(%r14), %esi
	movq	%rax, -656(%rbp)
	movq	16(%r14), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	leaq	80(%r14), %rdi
	movq	%rax, -648(%rbp)
	call	http_should_keep_alive@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %r8d
	movq	16(%r14), %rax
	movq	352(%rax), %rax
	leaq	112(%rax), %rcx
	addq	$120, %rax
	testl	%r8d, %r8d
	movq	%rcx, %rdx
	cmove	%rax, %rdx
	cmpb	$0, 111(%r14)
	cmovns	%rax, %rcx
	movq	%rdx, -592(%rbp)
	movl	$2, %edx
	movq	%rcx, -600(%rbp)
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%r14), %rdx
	movq	16(%r14), %rcx
	testq	%rdx, %rdx
	je	.L792
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L846
.L792:
	movq	3280(%rcx), %rsi
	movq	%r12, %rdi
	leaq	-656(%rbp), %r8
	movl	$9, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L847
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	16(%r14), %rax
	movq	%r12, %rdi
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movl	%eax, %r8d
	movl	%edx, %eax
	testb	%r8b, %r8b
	jne	.L743
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L839:
	xorl	%eax, %eax
.L743:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L848
	addq	$712, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L748:
	.cfi_restore_state
	leaq	-576(%rbp), %rax
	leaq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, -720(%rbp)
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L768:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L768
	movq	1712(%r14), %rdx
	testq	%rdx, %rdx
	je	.L769
	movq	$0, -712(%rbp)
	addq	$40, %rbx
	leaq	888(%r14), %r15
	.p2align 4,,10
	.p2align 3
.L777:
	movq	16(%rbx), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L849
.L772:
	movq	%rax, 0(%r13)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L773
	movq	(%r15), %rsi
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L802:
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L773
.L776:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L802
	cmpb	$9, %dl
	je	.L802
	movq	768(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L799
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
.L798:
	movq	%rax, 8(%r13)
	addq	$24, %rbx
	addq	$16, %r13
	addq	$24, %r15
	addq	$1, -712(%rbp)
	movq	1712(%r14), %rdx
	movq	-712(%rbp), %rax
	cmpq	%rax, %rdx
	ja	.L777
	addq	%rdx, %rdx
.L769:
	movq	-720(%rbp), %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movzbl	80(%r14), %edx
	movq	%rax, -640(%rbp)
	andl	$3, %edx
	jne	.L850
	movq	16(%r14), %rax
	movq	1672(%r14), %rcx
	movq	352(%rax), %rdi
	testq	%rcx, %rcx
	jne	.L851
	leaq	128(%rdi), %rax
	pxor	%xmm0, %xmm0
	leaq	-704(%rbp), %r13
	movq	%rax, -624(%rbp)
	movups	%xmm0, 1704(%r14)
.L783:
	movzbl	110(%r14), %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movzbl	80(%r14), %edx
	movq	%rax, -632(%rbp)
	movq	16(%r14), %rax
	andl	$3, %edx
	movq	352(%rax), %rdi
	cmpb	$1, %dl
	jne	.L784
.L845:
	movzwl	108(%r14), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	1696(%r14), %rcx
	movq	%rax, -616(%rbp)
	movq	16(%r14), %rax
	testq	%rcx, %rcx
	jne	.L852
	movq	352(%rax), %rdi
	leaq	128(%rdi), %rax
.L787:
	movq	%rax, -608(%rbp)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L773:
	leaq	128(%rdi), %rax
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L849:
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L771
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L837:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %rdi
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L850:
	movq	16(%r14), %rax
	pxor	%xmm0, %xmm0
	leaq	-704(%rbp), %r13
	movq	352(%rax), %rdi
	movups	%xmm0, 1704(%r14)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L840:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %rdi
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L843:
	movq	1656(%r14), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L764
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-712(%rbp), %rax
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L758:
	leaq	128(%rdi), %rax
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L842:
	movq	40(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -728(%rbp)
	movq	%r8, -712(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-712(%rbp), %r8
	movq	-728(%rbp), %r10
	testq	%rax, %rax
	je	.L756
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L847:
	movq	%r13, %rdi
	movb	$1, -670(%rbp)
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
.L794:
	movb	$1, 1721(%r14)
	movl	$-1, %eax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L846:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rcx
	movq	%rax, %rdx
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L838:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%rax, -728(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-728(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L799:
	movq	%rax, -728(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-728(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L844:
	movb	$1, 1721(%r14)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L796:
	movq	%r8, -744(%rbp)
	movq	%r10, -728(%rbp)
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-744(%rbp), %r8
	movq	-728(%rbp), %r10
	movq	-712(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%r8, -744(%rbp)
	movq	%r10, -728(%rbp)
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-744(%rbp), %r8
	movq	-728(%rbp), %r10
	movq	-712(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L851:
	movq	1656(%r14), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L781
	movq	16(%r14), %rcx
	movzbl	80(%r14), %edx
	movq	352(%rcx), %rdi
	andl	$3, %edx
.L782:
	movq	%rax, -624(%rbp)
	leaq	-704(%rbp), %r13
	jmp	.L752
.L781:
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movzbl	80(%r14), %edx
	movq	16(%r14), %rcx
	movq	-712(%rbp), %rax
	movq	352(%rcx), %rdi
	andl	$3, %edx
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L852:
	movq	1680(%r14), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L786
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L841:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L750
.L786:
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-712(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L787
.L848:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8506:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP11http_parser, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP11http_parser
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP11http_parser, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP11http_parser:
.LFB8508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-80(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-656(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r15), %rax
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpq	$0, 1704(%r15)
	jne	.L908
.L854:
	movq	8(%r15), %rdi
	movq	16(%r15), %rdx
	testq	%rdi, %rdi
	je	.L873
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L909
.L873:
	movq	3280(%rdx), %rsi
	movl	$3, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L910
.L874:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L875
.L907:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	xorl	%eax, %eax
.L853:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L911
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L875:
	.cfi_restore_state
	leaq	-624(%rbp), %r13
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%r15), %rdx
	movq	16(%r15), %rcx
	testq	%rdx, %rdx
	je	.L877
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L912
.L877:
	movq	3280(%rcx), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	je	.L913
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L908:
	movq	16(%r15), %rax
	leaq	-624(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	16(%r15), %rdx
	testq	%rdi, %rdi
	je	.L855
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L914
.L855:
	movq	3280(%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, -680(%rbp)
	testq	%rax, %rax
	je	.L915
.L856:
	movq	-680(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L906
	leaq	-576(%rbp), %rax
	leaq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, -688(%rbp)
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L858:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L858
	movq	16(%r15), %rax
	movq	1712(%r15), %rdx
	movq	352(%rax), %rdi
	testq	%rdx, %rdx
	je	.L859
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L867:
	movq	56(%rbx,%r8), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L916
.L862:
	movq	%rax, (%r10)
	movq	904(%r15,%r8), %rax
	testq	%rax, %rax
	je	.L863
	movq	888(%r15,%r8), %rsi
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L882:
	movq	%rax, 904(%r15,%r8)
	testq	%rax, %rax
	je	.L863
.L866:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L882
	cmpb	$9, %dl
	je	.L882
	movq	808(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-664(%rbp), %r8
	movq	-672(%rbp), %r10
	testq	%rax, %rax
	je	.L879
	movq	16(%r15), %rdx
	movq	352(%rdx), %rdi
.L880:
	addq	$1, %r14
	movq	%rax, 8(%r10)
	addq	$24, %r8
	addq	$16, %r10
	movq	1712(%r15), %rdx
	cmpq	%r14, %rdx
	ja	.L867
	addq	%rdx, %rdx
.L859:
	movq	-688(%rbp), %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	1672(%r15), %rcx
	movq	%rax, -576(%rbp)
	movq	16(%r15), %rax
	testq	%rcx, %rcx
	jne	.L917
	movq	352(%rax), %rbx
	leaq	128(%rbx), %rax
.L869:
	movq	-688(%rbp), %rcx
	movq	-680(%rbp), %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L918
.L870:
	cmpb	$0, 1664(%r15)
	je	.L871
	movq	1656(%r15), %rdi
	testq	%rdi, %rdi
	je	.L872
	call	_ZdaPv@PLT
.L872:
	movb	$0, 1664(%r15)
.L871:
	movq	$0, 1656(%r15)
	movq	$0, 1672(%r15)
	movb	$1, 1720(%r15)
.L906:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L909:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r15), %rdx
	movq	%rax, %rdi
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L914:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r15), %rdx
	movq	%rax, %rdi
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L917:
	movq	1656(%r15), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L869
	movq	%rax, -664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-664(%rbp), %rax
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L863:
	leaq	128(%rdi), %rax
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L916:
	movq	40(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-664(%rbp), %r8
	movq	-672(%rbp), %r10
	testq	%rax, %rax
	je	.L861
	movq	16(%r15), %rdx
	movq	352(%rdx), %rdi
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r13, %rdi
	movb	$1, -590(%rbp)
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movb	$1, 1721(%r15)
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$-1, %eax
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L912:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r15), %rcx
	movq	%rax, %rdx
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L910:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L918:
	movb	$1, 1721(%r15)
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L879:
	movq	%r10, -696(%rbp)
	movq	%rax, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r15), %rdx
	movq	-696(%rbp), %r10
	movq	-672(%rbp), %rax
	movq	-664(%rbp), %r8
	movq	352(%rdx), %rdi
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r10, -696(%rbp)
	movq	%rax, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r15), %rdx
	movq	-696(%rbp), %r10
	movq	-672(%rbp), %rax
	movq	-664(%rbp), %r8
	movq	352(%rdx), %rdi
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L915:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L856
.L911:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8508:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP11http_parser, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP11http_parser
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, @function
_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t:
.LFB10087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-40(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	js	.L944
	movq	0(%r13), %rsi
	jne	.L945
	movq	-40(%rbx), %rax
	cmpq	2184(%rax), %rsi
	je	.L941
.L931:
	movq	%rsi, %rdi
	call	free@PLT
.L930:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L946
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	movq	$0, 1672(%rbx)
	leaq	-56(%rbx), %r15
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L943
	movq	-48(%rbx), %rdi
	movq	-40(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L926
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L947
.L926:
	movq	3280(%rdx), %rsi
	movl	$4, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L948
.L927:
	movq	%rsi, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-104(%rbp), %rsi
	testb	%al, %al
	jne	.L928
.L943:
	movq	0(%r13), %rsi
.L951:
	movq	-40(%rbx), %rax
	cmpq	2184(%rax), %rsi
	jne	.L931
.L941:
	movb	$0, 2192(%rax)
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L928:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	leaq	-96(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, 1680(%rbx)
	movq	%rax, 1688(%rbx)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	-40(%rbx), %rax
	movq	0(%r13), %rdi
	movq	$0, 1680(%rbx)
	movq	$0, 1688(%rbx)
	cmpq	2184(%rax), %rdi
	je	.L941
	call	free@PLT
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L947:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-40(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L944:
	movq	16(%rbx), %r15
	testq	%r15, %r15
	je	.L949
	movq	(%r15), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rax), %rcx
	movq	%rcx, -104(%rbp)
	call	uv_buf_init@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	leaq	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t(%rip), %rax
	movq	%rdx, -88(%rbp)
	leaq	-96(%rbp), %rdx
	cmpq	%rax, %rcx
	je	.L950
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rcx
	movq	0(%r13), %rsi
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rsi
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L949:
	leaq	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L946:
	call	__stack_chk_fail@PLT
.L950:
	leaq	-56(%r15), %rdi
	movq	%r12, %rsi
	call	.LTHUNK1
	jmp	.L943
	.cfi_endproc
.LFE10087:
	.size	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, .-_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L984
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L972
	cmpw	$1040, %cx
	jne	.L954
.L972:
	movq	23(%rdx), %rbx
.L956:
	testq	%rbx, %rbx
	je	.L952
	cmpq	$0, 1728(%rbx)
	jne	.L985
	movq	16(%rbx), %rax
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movb	$0, 1721(%rbx)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 1736(%rbx)
	leaq	80(%rbx), %rdi
	leaq	_ZN4node12_GLOBAL__N_16Parser8settingsE(%rip), %rsi
	movq	$0, 1744(%rbx)
	call	http_parser_execute@PLT
	testq	%rax, %rax
	je	.L959
	cmpq	$1, %rax
	jne	.L986
	movq	$0, 1728(%rbx)
	movzbl	111(%rbx), %eax
	movq	$0, 1736(%rbx)
	movq	$0, 1744(%rbx)
	andl	$127, %eax
	cmpb	$0, 1721(%rbx)
	jne	.L963
	movzbl	%al, %r14d
	movq	16(%rbx), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	cmpb	$0, 111(%rbx)
	movq	%rax, -104(%rbp)
	js	.L963
	testl	%r14d, %r14d
	je	.L963
	movq	16(%rbx), %rax
	movq	360(%rax), %rax
	movq	1304(%rax), %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r15
	movq	%rax, -112(%rbp)
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L987
.L964:
	movq	16(%rbx), %rax
	movq	%r15, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	240(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L988
.L965:
	movl	%r14d, %edi
	call	http_errno_name@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L989
.L966:
	movq	16(%rbx), %rax
	movq	%r15, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	328(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L990
.L967:
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %rbx
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L959:
	cmpb	$0, 1721(%rbx)
	movq	$0, 1728(%rbx)
	movq	$0, 1736(%rbx)
	movq	$0, 1744(%rbx)
	jne	.L963
	movq	16(%rbx), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
.L963:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %rbx
.L962:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	testq	%rbx, %rbx
	je	.L952
	movq	(%rbx), %rdx
	movq	(%r12), %rax
	movq	%rdx, 24(%rax)
.L952:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L991
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L984:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L985:
	leaq	_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L987:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rcx
	jmp	.L964
.L990:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L967
.L989:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rcx
	jmp	.L966
.L988:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L965
.L991:
	call	__stack_chk_fail@PLT
.L986:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7696:
	.size	_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC3:
	.string	"HTTPParser"
.LC4:
	.string	"REQUEST"
.LC5:
	.string	"RESPONSE"
.LC6:
	.string	"kOnHeaders"
.LC7:
	.string	"kOnHeadersComplete"
.LC8:
	.string	"kOnBody"
.LC9:
	.string	"kOnMessageComplete"
.LC10:
	.string	"kOnExecute"
.LC11:
	.string	"DELETE"
.LC12:
	.string	"GET"
.LC13:
	.string	"HEAD"
.LC14:
	.string	"POST"
.LC15:
	.string	"PUT"
.LC16:
	.string	"CONNECT"
.LC17:
	.string	"OPTIONS"
.LC18:
	.string	"TRACE"
.LC19:
	.string	"COPY"
.LC20:
	.string	"LOCK"
.LC21:
	.string	"MKCOL"
.LC22:
	.string	"MOVE"
.LC23:
	.string	"PROPFIND"
.LC24:
	.string	"PROPPATCH"
.LC25:
	.string	"SEARCH"
.LC26:
	.string	"UNLOCK"
.LC27:
	.string	"BIND"
.LC28:
	.string	"REBIND"
.LC29:
	.string	"UNBIND"
.LC30:
	.string	"ACL"
.LC31:
	.string	"REPORT"
.LC32:
	.string	"MKACTIVITY"
.LC33:
	.string	"CHECKOUT"
.LC34:
	.string	"MERGE"
.LC35:
	.string	"M-SEARCH"
.LC36:
	.string	"NOTIFY"
.LC37:
	.string	"SUBSCRIBE"
.LC38:
	.string	"UNSUBSCRIBE"
.LC39:
	.string	"PATCH"
.LC40:
	.string	"PURGE"
.LC41:
	.string	"MKCALENDAR"
.LC42:
	.string	"LINK"
.LC43:
	.string	"UNLINK"
.LC44:
	.string	"SOURCE"
.LC45:
	.string	"methods"
.LC46:
	.string	"close"
.LC47:
	.string	"free"
.LC48:
	.string	"execute"
.LC49:
	.string	"finish"
.LC50:
	.string	"initialize"
.LC51:
	.string	"pause"
.LC52:
	.string	"resume"
.LC53:
	.string	"consume"
.LC54:
	.string	"unconsume"
.LC55:
	.string	"getCurrentBuffer"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB56:
	.text
.LHOTB56:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L993
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L993
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L993
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movl	$10, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L1089
.L994:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1090
.L995:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	xorl	%edx, %edx
	movl	$8, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1091
.L996:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1092
.L997:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1093
.L998:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1094
.L999:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1095
.L1000:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1096
.L1001:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1097
.L1002:
	movq	3280(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1098
.L1003:
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1099
.L1004:
	movq	3280(%rbx), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1100
.L1005:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1101
.L1006:
	movq	3280(%rbx), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1102
.L1007:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1103
.L1008:
	movq	3280(%rbx), %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1104
.L1009:
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1105
.L1010:
	movq	3280(%rbx), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1106
.L1011:
	movq	352(%rbx), %rdi
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1107
.L1012:
	movq	3280(%rbx), %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1108
.L1013:
	movq	352(%rbx), %rdi
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1109
.L1014:
	movq	3280(%rbx), %rsi
	movl	$6, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1110
.L1015:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1111
.L1016:
	movq	3280(%rbx), %rsi
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1112
.L1017:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1113
.L1018:
	movq	3280(%rbx), %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1114
.L1019:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1115
.L1020:
	movq	3280(%rbx), %rsi
	movl	$9, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1116
.L1021:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1117
.L1022:
	movq	3280(%rbx), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1118
.L1023:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1119
.L1024:
	movq	3280(%rbx), %rsi
	movl	$11, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1120
.L1025:
	movq	352(%rbx), %rdi
	movl	$8, %ecx
	xorl	%edx, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1121
.L1026:
	movq	3280(%rbx), %rsi
	movl	$12, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1122
.L1027:
	movq	352(%rbx), %rdi
	movl	$9, %ecx
	xorl	%edx, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1123
.L1028:
	movq	3280(%rbx), %rsi
	movl	$13, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1124
.L1029:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1125
.L1030:
	movq	3280(%rbx), %rsi
	movl	$14, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1126
.L1031:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1127
.L1032:
	movq	3280(%rbx), %rsi
	movl	$15, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1128
.L1033:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1129
.L1034:
	movq	3280(%rbx), %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1130
.L1035:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1131
.L1036:
	movq	3280(%rbx), %rsi
	movl	$17, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1132
.L1037:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1133
.L1038:
	movq	3280(%rbx), %rsi
	movl	$18, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1134
.L1039:
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1135
.L1040:
	movq	3280(%rbx), %rsi
	movl	$19, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1136
.L1041:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1137
.L1042:
	movq	3280(%rbx), %rsi
	movl	$20, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1138
.L1043:
	movq	352(%rbx), %rdi
	movl	$10, %ecx
	xorl	%edx, %edx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1139
.L1044:
	movq	3280(%rbx), %rsi
	movl	$21, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1140
.L1045:
	movq	352(%rbx), %rdi
	movl	$8, %ecx
	xorl	%edx, %edx
	leaq	.LC33(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1141
.L1046:
	movq	3280(%rbx), %rsi
	movl	$22, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1142
.L1047:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC34(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1143
.L1048:
	movq	3280(%rbx), %rsi
	movl	$23, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1144
.L1049:
	movq	352(%rbx), %rdi
	movl	$8, %ecx
	xorl	%edx, %edx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1145
.L1050:
	movq	3280(%rbx), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1146
.L1051:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC36(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1147
.L1052:
	movq	3280(%rbx), %rsi
	movl	$25, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1148
.L1053:
	movq	352(%rbx), %rdi
	movl	$9, %ecx
	xorl	%edx, %edx
	leaq	.LC37(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1149
.L1054:
	movq	3280(%rbx), %rsi
	movl	$26, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1150
.L1055:
	movq	352(%rbx), %rdi
	movl	$11, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1151
.L1056:
	movq	3280(%rbx), %rsi
	movl	$27, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1152
.L1057:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1153
.L1058:
	movq	3280(%rbx), %rsi
	movl	$28, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1154
.L1059:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1155
.L1060:
	movq	3280(%rbx), %rsi
	movl	$29, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1156
.L1061:
	movq	352(%rbx), %rdi
	movl	$10, %ecx
	xorl	%edx, %edx
	leaq	.LC41(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1157
.L1062:
	movq	3280(%rbx), %rsi
	movl	$30, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1158
.L1063:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1159
.L1064:
	movq	3280(%rbx), %rsi
	movl	$31, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1160
.L1065:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC43(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1161
.L1066:
	movq	3280(%rbx), %rsi
	movl	$32, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1162
.L1067:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC44(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1163
.L1068:
	movq	3280(%rbx), %rsi
	movl	$33, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1164
.L1069:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC45(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1165
.L1070:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1166
.L1071:
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC46(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r12
	popq	%rax
	testq	%r12, %r12
	je	.L1167
.L1072:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC47(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1168
.L1073:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC48(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1169
.L1074:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC49(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1170
.L1075:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC50(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L1171
.L1076:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC51(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r12
	popq	%rax
	testq	%r12, %r12
	je	.L1172
.L1077:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC52(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1173
.L1078:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC53(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1174
.L1079:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC54(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1175
.L1080:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC55(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L1176
.L1081:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1177
.L1082:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1178
.L1083:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1179
.L1084:
	leaq	-40(%rbp), %rsp
	leaq	_ZN4node12_GLOBAL__N_125InitMaxHttpHeaderSizeOnceEv(%rip), %rsi
	popq	%rbx
	leaq	_ZZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvE9init_once(%rip), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_once@PLT
	.p2align 4,,10
	.p2align 3
.L1089:
	.cfi_restore_state
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1097:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1098:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1100:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1102:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1104:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1106:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1108:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1110:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1112:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1114:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1116:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1118:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1120:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1122:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1124:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1126:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1128:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1130:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1132:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1134:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1136:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1138:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1139:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1140:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1142:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1144:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1146:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1147:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1148:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1150:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1152:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1154:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1156:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1158:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1160:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1162:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1164:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1166:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1167:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1168:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1169:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1170:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1171:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1172:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1173:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1174:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1175:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1176:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1177:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1179:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1084
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7714:
.L993:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7714:
	.text
	.size	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE56:
	.text
.LHOTE56:
	.p2align 4
	.globl	_Z21_register_http_parserv
	.type	_Z21_register_http_parserv, @function
_Z21_register_http_parserv:
.LFB7715:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7715:
	.size	_Z21_register_http_parserv, .-_Z21_register_http_parserv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L1198
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L1199
.L1184:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L1188
.L1181:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1199:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1189
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1185:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L1184
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1190
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1186:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L1188:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1181
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L1198:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1189:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1190:
	xorl	%edx, %edx
	jmp	.L1186
	.cfi_endproc
.LFE8437:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1201
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1203
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1201
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1203
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1201
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	%r12, %rsi
	call	*%rax
.L1208:
	movq	(%r12), %rax
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1209
	leaq	16(%r12), %rsi
.L1210:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1211
	addq	$16, %r12
.L1212:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1213
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1241
.L1213:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1242
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1217
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L1200
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L1200:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1243
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1204:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1242:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1220
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1215:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1217:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1220:
	xorl	%edx, %edx
	jmp	.L1215
.L1243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7655:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1245
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1248
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1247
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1248
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1245
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1248
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1247
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1248
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1245
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1248
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*32(%rax)
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	%r12, %rsi
	call	*%rax
.L1252:
	movq	(%r12), %rax
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1253
	leaq	40(%r12), %rsi
.L1254:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1255
	addq	$40, %r12
.L1256:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1257
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1285
.L1257:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1286
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1261
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L1244
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L1244:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1287
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1286:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1264
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1259:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1261:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1264:
	xorl	%edx, %edx
	jmp	.L1259
.L1287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7657:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_16ParserE, @object
	.size	_ZTVN4node12_GLOBAL__N_16ParserE, 192
_ZTVN4node12_GLOBAL__N_16ParserE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_16ParserD1Ev
	.quad	_ZN4node12_GLOBAL__N_16ParserD0Ev
	.quad	_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.quad	_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.quad	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC57:
	.string	"../src/node_http_parser_impl.h:549"
	.section	.rodata.str1.1
.LC58:
	.string	"(env) == (parser->env())"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"static void node::{anonymous}::Parser::Pause(const v8::FunctionCallbackInfo<v8::Value>&) [with bool should_pause = false]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"static void node::{anonymous}::Parser::Pause(const v8::FunctionCallbackInfo<v8::Value>&) [with bool should_pause = true]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC60
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC61:
	.string	"../src/util-inl.h:495"
.LC62:
	.string	"value->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Value>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.weak	_ZZN4node6MallocIcEEPT_mE4args
	.section	.rodata.str1.1
.LC64:
	.string	"../src/util-inl.h:381"
.LC65:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"T* node::Malloc(size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIcEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIcEEPT_mE4args, 24
_ZZN4node6MallocIcEEPT_mE4args:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"../src/node_http_parser_traditional.cc"
	.section	.rodata.str1.1
.LC68:
	.string	"http_parser"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC67
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC68
	.quad	0
	.quad	0
	.globl	_ZN4node11per_process19http_parser_versionE
	.section	.rodata.str1.1
.LC69:
	.string	"2.9.3"
	.section	.data.rel.ro.local
	.align 8
	.type	_ZN4node11per_process19http_parser_versionE, @object
	.size	_ZN4node11per_process19http_parser_versionE, 8
_ZN4node11per_process19http_parser_versionE:
	.quad	.LC69
	.local	_ZZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvE9init_once
	.comm	_ZZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvE9init_once,4,4
	.align 32
	.type	_ZN4node12_GLOBAL__N_16Parser8settingsE, @object
	.size	_ZN4node12_GLOBAL__N_16Parser8settingsE, 80
_ZN4node12_GLOBAL__N_16Parser8settingsE:
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP11http_parser
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP11http_parserS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP11http_parserS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP11http_parserS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP11http_parserS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP11http_parser
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP11http_parserS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP11http_parser
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"../src/node_http_parser_impl.h:721"
	.section	.rodata.str1.1
.LC71:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"v8::Local<v8::Value> node::{anonymous}::Parser::Execute(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"../src/node_http_parser_impl.h:712"
	.section	.rodata.str1.1
.LC74:
	.string	"(len) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args:
	.quad	.LC73
	.quad	.LC74
	.quad	.LC72
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"../src/node_http_parser_impl.h:573"
	.section	.rodata.str1.1
.LC76:
	.string	"(stream) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"static void node::{anonymous}::Parser::Consume(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"../src/node_http_parser_impl.h:571"
	.section	.rodata.str1.1
.LC79:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC77
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"../src/node_http_parser_impl.h:531"
	.align 8
.LC81:
	.string	"static void node::{anonymous}::Parser::Initialize(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC80
	.quad	.LC58
	.quad	.LC81
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"../src/node_http_parser_impl.h:527"
	.align 8
.LC83:
	.string	"type == HTTP_REQUEST || type == HTTP_RESPONSE"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC82
	.quad	.LC83
	.quad	.LC81
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"../src/node_http_parser_impl.h:522"
	.section	.rodata.str1.1
.LC85:
	.string	"args[1]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC84
	.quad	.LC85
	.quad	.LC81
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"../src/node_http_parser_impl.h:521"
	.section	.rodata.str1.1
.LC87:
	.string	"args[0]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC86
	.quad	.LC87
	.quad	.LC81
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"../src/node_http_parser_impl.h:509"
	.align 8
.LC89:
	.string	"parser->current_buffer_.IsEmpty()"
	.align 8
.LC90:
	.string	"static void node::{anonymous}::Parser::Finish(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"../src/node_http_parser_impl.h:489"
	.align 8
.LC92:
	.string	"(parser->current_buffer_data_) == nullptr"
	.align 8
.LC93:
	.string	"static void node::{anonymous}::Parser::Execute(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"../src/node_http_parser_impl.h:488"
	.align 8
.LC95:
	.string	"(parser->current_buffer_len_) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC93
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"../src/node_http_parser_impl.h:487"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC96
	.quad	.LC89
	.quad	.LC93
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"../src/node_http_parser_impl.h:254"
	.align 8
.LC98:
	.string	"(num_values_) == (num_fields_)"
	.align 8
.LC99:
	.string	"int node::{anonymous}::Parser::on_header_value(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"../src/node_http_parser_impl.h:253"
	.align 8
.LC101:
	.string	"(num_values_) < (arraysize(values_))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args:
	.quad	.LC100
	.quad	.LC101
	.quad	.LC99
	.section	.rodata.str1.8
	.align 8
.LC102:
	.string	"../src/node_http_parser_impl.h:233"
	.align 8
.LC103:
	.string	"(num_fields_) == (num_values_ + 1)"
	.align 8
.LC104:
	.string	"int node::{anonymous}::Parser::on_header_field(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0:
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"../src/node_http_parser_impl.h:232"
	.align 8
.LC106:
	.string	"(num_fields_) < (kMaxHeaderFieldsCount)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args:
	.quad	.LC105
	.quad	.LC106
	.quad	.LC104
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC107:
	.string	"../src/stream_base-inl.h:103"
.LC108:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.weak	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC110:
	.string	"../src/stream_base-inl.h:85"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"(listener->stream_) == nullptr"
	.align 8
.LC112:
	.string	"void node::StreamResource::PushStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC113:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC115:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC116:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC116
	.quad	.LC114
	.quad	.LC117
	.weak	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args
	.section	.rodata.str1.1
.LC118:
	.string	"../src/stream_base-inl.h:56"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"void node::StreamListener::PassReadErrorToPreviousListener(ssize_t)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args,"awG",@progbits,_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args, 24
_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args:
	.quad	.LC118
	.quad	.LC114
	.quad	.LC119
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC120:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC122:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC123:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC125:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC126:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC128:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.weak	_ZZN4node11Environment22set_http_parser_bufferEPcE4args
	.section	.rodata.str1.1
.LC129:
	.string	"../src/env-inl.h:578"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"(http_parser_buffer_) == nullptr"
	.align 8
.LC131:
	.string	"void node::Environment::set_http_parser_buffer(char*)"
	.section	.data.rel.ro.local._ZZN4node11Environment22set_http_parser_bufferEPcE4args,"awG",@progbits,_ZZN4node11Environment22set_http_parser_bufferEPcE4args,comdat
	.align 16
	.type	_ZZN4node11Environment22set_http_parser_bufferEPcE4args, @gnu_unique_object
	.size	_ZZN4node11Environment22set_http_parser_bufferEPcE4args, 24
_ZZN4node11Environment22set_http_parser_bufferEPcE4args:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	1
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
