	.file	"node_os.cc"
	.text
	.p2align 4
	.type	_ZN4node2osL13GetFreeMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL13GetFreeMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	uv_get_free_memory@PLT
	movq	(%rbx), %rbx
	testq	%rax, %rax
	js	.L2
	pxor	%xmm0, %xmm0
	movq	8(%rbx), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L8
.L4:
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	movq	8(%rbx), %rdi
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	jne	.L4
.L8:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7099:
	.size	_ZN4node2osL13GetFreeMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL13GetFreeMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node2osL14GetTotalMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL14GetTotalMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	uv_get_total_memory@PLT
	movq	(%rbx), %rbx
	testq	%rax, %rax
	js	.L10
	pxor	%xmm0, %xmm0
	movq	8(%rbx), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L15
.L12:
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	movq	8(%rbx), %rdi
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	jne	.L12
.L15:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7100:
	.size	_ZN4node2osL14GetTotalMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL14GetTotalMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node2osL9GetUptimeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL9GetUptimeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_uptime@PLT
	testl	%eax, %eax
	je	.L21
.L16:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	(%rbx), %rbx
	movsd	-32(%rbp), %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L23
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L23:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L16
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7101:
	.size	_ZN4node2osL9GetUptimeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL9GetUptimeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$64, %rsp
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L25
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value14IsFloat64ArrayEv@PLT
	testb	%al, %al
	je	.L34
.L27:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L35
	movq	8(%rbx), %r12
.L29:
	movq	%r12, %rdi
	call	_ZN2v810TypedArray6LengthEv@PLT
	cmpq	$3, %rax
	jne	.L36
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rdi
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_loadavg@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value14IsFloat64ArrayEv@PLT
	testb	%al, %al
	jne	.L27
.L34:
	leaq	_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L37:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7102:
	.size	_ZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"uv_os_get_passwd"
	.text
	.p2align 4
	.type	_ZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L69
	cmpw	$1040, %cx
	jne	.L39
.L69:
	movq	23(%rdx), %r14
.L41:
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L42
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L68
.L81:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L78
	movq	8(%rbx), %rdi
.L46:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	600(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L38
	movq	352(%r14), %rdi
	movl	$1, %edx
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	movl	%eax, %r12d
.L44:
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	uv_os_get_passwd@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L49
	movl	16(%rbx), %ecx
	cmpl	$1, %ecx
	jle	.L79
	subq	$8, %rsp
	subl	$1, %ecx
	movq	8(%rbx), %rsi
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%ecx, %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	salq	$3, %rcx
	subq	%rcx, %rsi
	leaq	.LC0(%rip), %rcx
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	popq	%rax
	popq	%rdx
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L42:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L81
.L68:
	movl	$1, %r12d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	pxor	%xmm0, %xmm0
	movq	352(%r14), %rdi
	movq	$0, -104(%rbp)
	leaq	-104(%rbp), %r15
	cvtsi2sdq	-88(%rbp), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	pxor	%xmm0, %xmm0
	movq	352(%r14), %rdi
	cvtsi2sdq	-80(%rbp), %xmm0
	movq	%rax, -144(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-96(%rbp), %rsi
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	352(%r14), %rdi
	movq	%rax, -136(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	352(%r14), %rdi
	movq	%rax, -128(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, -120(%rbp)
	testq	%rsi, %rsi
	je	.L82
	movq	352(%r14), %rdi
	movq	%r15, %rcx
	movl	%r12d, %edx
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %r15
.L53:
	cmpq	$0, -128(%rbp)
	sete	%dl
	cmpq	$0, -120(%rbp)
	sete	%al
	orb	%al, %dl
	jne	.L70
	testq	%r15, %r15
	jne	.L54
.L70:
	cmpq	$0, -104(%rbp)
	je	.L83
	movq	352(%r14), %rdi
	movq	-104(%rbp), %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	uv_os_free_passwd@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L82:
	movq	352(%r14), %rax
	leaq	104(%rax), %r15
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L54:
	movq	352(%r14), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	3280(%r14), %rsi
	movq	-144(%rbp), %rcx
	movq	%rax, %r12
	movq	360(%r14), %rax
	movq	%r12, %rdi
	movq	1776(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L84
.L58:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	-136(%rbp), %rcx
	movq	808(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L85
.L60:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	-128(%rbp), %rcx
	movq	1856(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L86
.L61:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	-120(%rbp), %rcx
	movq	840(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L87
.L62:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	1592(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L88
.L63:
	movq	(%rbx), %rax
	testq	%r12, %r12
	je	.L89
	movq	(%r12), %rdx
.L65:
	movq	%rdx, 24(%rax)
	movq	%r13, %rdi
	call	uv_os_free_passwd@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L89:
	movq	16(%rax), %rdx
	jmp	.L65
.L88:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L63
.L87:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L62
.L86:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L61
.L85:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L60
.L84:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L58
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7106:
	.size	_ZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC1:
	.string	"uv_os_gethostname"
	.text
	.p2align 4
	.type	_ZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L100
	cmpw	$1040, %cx
	jne	.L91
.L100:
	movq	23(%rdx), %r12
.L93:
	leaq	-112(%rbp), %r13
	leaq	-120(%rbp), %rsi
	movq	$65, -120(%rbp)
	movq	%r13, %rdi
	call	uv_os_gethostname@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L94
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L102
	subq	$8, %rsp
	subl	$1, %ecx
	movq	8(%rbx), %rsi
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%ecx, %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	salq	$3, %rcx
	subq	%rcx, %rsi
	leaq	.LC1(%rip), %rcx
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	popq	%rax
	popq	%rdx
.L90:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	(%rbx), %rbx
	movl	$-1, %ecx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L104
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	_ZZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L90
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7095:
	.size	_ZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC2:
	.string	"uv_os_homedir"
	.text
	.p2align 4
	.type	_ZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L115
	cmpw	$1040, %cx
	jne	.L106
.L115:
	movq	23(%rdx), %r12
.L108:
	leaq	-4144(%rbp), %r13
	leaq	-4152(%rbp), %rsi
	movq	$4096, -4152(%rbp)
	movq	%r13, %rdi
	call	uv_os_homedir@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L109
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L117
	subq	$8, %rsp
	subl	$1, %ecx
	movq	8(%rbx), %rsi
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%ecx, %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	salq	$3, %rcx
	subq	%rcx, %rsi
	leaq	.LC2(%rip), %rcx
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	popq	%rax
	popq	%rdx
.L105:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	352(%r12), %rdi
	movl	-4152(%rbp), %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L119
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	_ZZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L119:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L105
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7105:
	.size	_ZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC3:
	.string	"uv_os_getpriority"
	.text
	.p2align 4
	.type	_ZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L137
	cmpw	$1040, %cx
	jne	.L121
.L137:
	movq	23(%rdx), %r13
.L123:
	cmpl	$2, 16(%rbx)
	je	.L140
	leaq	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L141
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L142
	movq	8(%rbx), %rdi
.L126:
	call	_ZNK2v85Int325ValueEv@PLT
	leaq	-44(%rbp), %rsi
	movl	%eax, %edi
	call	uv_os_getpriority@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L127
	cmpl	$1, 16(%rbx)
	jg	.L128
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L129:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L143
	cmpl	$1, 16(%rbx)
	jle	.L144
	movq	8(%rbx), %rsi
	subq	$8, %rsi
.L132:
	subq	$8, %rsp
	movl	%r12d, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	popq	%rax
	popq	%rdx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L142:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L127:
	movslq	-44(%rbp), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L120:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7109:
	.size	_ZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC4:
	.string	"uv_os_uname"
	.text
	.p2align 4
	.type	_ZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$1096, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L159
	cmpw	$1040, %cx
	jne	.L147
.L159:
	movq	23(%rdx), %r12
.L149:
	leaq	-1072(%rbp), %r13
	movq	%r13, %rdi
	call	uv_os_uname@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	je	.L150
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L161
	subq	$8, %rsp
	subl	$1, %ecx
	movq	8(%rbx), %rsi
	xorl	%r9d, %r9d
	pushq	$0
	movslq	%ecx, %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	salq	$3, %rcx
	subq	%rcx, %rsi
	leaq	.LC4(%rip), %rcx
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	popq	%rax
	popq	%rdx
.L146:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r13, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L163
.L153:
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	leaq	-560(%rbp), %rsi
	movl	$-1, %ecx
	movq	%rax, -1104(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L164
.L154:
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	leaq	-816(%rbp), %rsi
	movl	$-1, %ecx
	movq	%rax, -1096(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L165
.L155:
	movq	352(%r12), %rdi
	movl	$3, %edx
	movq	(%rbx), %rbx
	leaq	-1104(%rbp), %rsi
	movq	%rax, -1088(%rbp)
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L166
	movq	(%rax), %rax
.L157:
	movq	%rax, 24(%rbx)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L147:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	_ZZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	movq	16(%rbx), %rax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%rax, -1112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1112(%rbp), %rax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%rax, -1112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1112(%rbp), %rax
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%rax, -1112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1112(%rbp), %rax
	jmp	.L155
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC5:
	.string	"uv_os_setpriority"
	.text
	.p2align 4
	.type	_ZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L187
	cmpw	$1040, %cx
	jne	.L168
.L187:
	movq	23(%rdx), %r13
.L170:
	cmpl	$3, 16(%rbx)
	je	.L194
	leaq	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L195
	cmpl	$1, 16(%rbx)
	jle	.L196
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L173:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L197
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L175
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L176:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, %r12d
	jg	.L177
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L178:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%r12d, %edi
	movl	%eax, %esi
	call	uv_os_setpriority@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L179
	cmpl	$2, 16(%rbx)
	jle	.L198
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L181:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L199
	cmpl	$2, 16(%rbx)
	jg	.L183
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L184:
	subq	$8, %rsp
	movl	%r12d, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	popq	%rax
	popq	%rdx
.L179:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L177:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L175:
	movq	8(%rbx), %rdi
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L198:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L168:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L183:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L199:
	leaq	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7108:
	.size	_ZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"cannot create std::vector larger than max_size()"
	.text
	.p2align 4
	.type	_ZN4node2osL10GetCPUInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL10GetCPUInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L231
	cmpw	$1040, %cx
	jne	.L201
.L231:
	movq	23(%rdx), %rax
.L203:
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	352(%rax), %r13
	call	uv_cpu_info@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L239
.L200:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L240
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movl	-68(%rbp), %esi
	leal	0(,%rsi,8), %ebx
	subl	%esi, %ebx
	movslq	%ebx, %rax
	movq	%rax, -96(%rbp)
	movq	%rax, %rcx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L241
	movq	$0, -88(%rbp)
	movq	%rcx, %r15
	testq	%rcx, %rcx
	je	.L206
	leaq	0(,%rcx,8), %r12
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
	cmpl	$1, %ebx
	je	.L207
	movq	%r15, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L208:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L208
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rax
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rdx, %rcx
	je	.L211
.L207:
	movq	$0, (%rax)
.L211:
	sarq	$3, %r12
	movl	-68(%rbp), %esi
	movq	%r12, -96(%rbp)
.L206:
	movq	-88(%rbp), %rbx
	xorl	%r15d, %r15d
	testl	%esi, %esi
	jg	.L224
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L243:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L215:
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, 16(%rbx)
	movq	24(%r12), %rax
	testq	%rax, %rax
	js	.L216
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L217:
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, 24(%rbx)
	movq	32(%r12), %rax
	testq	%rax, %rax
	js	.L218
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L219:
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, 32(%rbx)
	movq	40(%r12), %rax
	testq	%rax, %rax
	js	.L220
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L221:
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, 40(%rbx)
	movq	48(%r12), %rax
	testq	%rax, %rax
	js	.L222
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L223:
	movq	%r13, %rdi
	addl	$1, %r14d
	addq	$56, %r15
	addq	$56, %rbx
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	-68(%rbp), %esi
	movq	%rax, -8(%rbx)
	cmpl	%r14d, %esi
	jle	.L209
.L224:
	movq	-64(%rbp), %r12
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	addq	%r15, %r12
	movq	(%r12), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L242
.L213:
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movq	%r13, %rdi
	cvtsi2sdl	8(%r12), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, 8(%rbx)
	movq	16(%r12), %rax
	testq	%rax, %rax
	jns	.L243
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L209:
	movq	-64(%rbp), %rdi
	call	uv_free_cpu_info@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-88(%rbp), %rsi
	movq	(%rax), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L244
	movq	(%rax), %rax
.L226:
	movq	%rax, 24(%rbx)
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L200
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%rax, -112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %rax
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L201:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L244:
	movq	16(%rbx), %rax
	jmp	.L226
.L240:
	call	__stack_chk_fail@PLT
.L241:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7098:
	.size	_ZN4node2osL10GetCPUInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL10GetCPUInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC7:
	.string	"uv_interface_addresses"
.LC8:
	.string	"%02x:%02x:%02x:%02x:%02x:%02x"
	.text
	.p2align 4
	.type	_ZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -256(%rbp)
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L278
	cmpw	$1040, %cx
	jne	.L246
.L278:
	movq	23(%rdx), %rax
	movq	%rax, -224(%rbp)
.L248:
	leaq	-204(%rbp), %rsi
	leaq	-200(%rbp), %rdi
	movq	352(%rax), %r13
	call	uv_interface_addresses@PLT
	cmpl	$-38, %eax
	je	.L287
	testl	%eax, %eax
	je	.L251
	movq	-256(%rbp), %rax
	movl	16(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L288
	movq	%rax, %r14
	subl	$1, %ebx
	call	__errno_location@PLT
	subq	$8, %rsp
	movslq	%ebx, %rbx
	xorl	%r9d, %r9d
	movl	(%rax), %edx
	movq	8(%r14), %rax
	pushq	$0
	salq	$3, %rbx
	movq	-224(%rbp), %rdi
	xorl	%r8d, %r8d
	leaq	.LC7(%rip), %rcx
	movq	%rax, %rsi
	movq	%rax, -216(%rbp)
	subq	%rbx, %rsi
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	movq	(%r14), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	popq	%rsi
	popq	%rdi
.L245:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movl	$-1, %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movl	-204(%rbp), %esi
	movq	%rax, -272(%rbp)
	leal	0(,%rsi,8), %ebx
	subl	%esi, %ebx
	movslq	%ebx, %rax
	movq	%rax, -288(%rbp)
	movq	%rax, %rcx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	ja	.L290
	movq	$0, -280(%rbp)
	movq	%rcx, %r15
	testq	%rcx, %rcx
	je	.L254
	leaq	0(,%rcx,8), %r12
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%rax, -280(%rbp)
	cmpl	$1, %ebx
	je	.L255
	movq	%r15, %rdx
	pxor	%xmm0, %xmm0
	shrq	%rdx
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L256:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L256
	movq	-288(%rbp), %rcx
	movq	-280(%rbp), %rax
	movq	%rcx, %rdx
	andq	$-2, %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rdx, %rcx
	je	.L259
.L255:
	movq	$0, (%rax)
.L259:
	sarq	$3, %r12
	movl	-204(%rbp), %esi
	movq	%r12, -288(%rbp)
.L254:
	movq	-200(%rbp), %rdi
	testl	%esi, %esi
	jle	.L257
	leaq	-192(%rbp), %rax
	movq	-280(%rbp), %r12
	movq	$0, -216(%rbp)
	leaq	-160(%rbp), %r15
	movq	%rax, -232(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -248(%rbp)
	leaq	120(%r13), %rax
	movq	%rax, -264(%rbp)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-224(%rbp), %rax
	xorl	%ecx, %ecx
	movdqa	.LC9(%rip), %xmm0
	movq	$4094316, 16(%r15)
	movq	$0, 24(%r15)
	movq	360(%rax), %rax
	movq	$0, 32(%r15)
	movl	$0, 40(%r15)
	movq	1784(%rax), %rax
	movw	%cx, 44(%r15)
	movaps	%xmm0, (%r15)
	movq	%rax, -240(%rbp)
.L263:
	movq	%r14, (%r12)
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L291
.L265:
	movq	%rax, 8(%r12)
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	movq	-248(%rbp), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L292
.L266:
	movq	%rax, %xmm0
	xorl	%edx, %edx
	movl	$17, %ecx
	movq	%r13, %rdi
	movhps	-240(%rbp), %xmm0
	movq	-232(%rbp), %rsi
	movups	%xmm0, 16(%r12)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L293
.L267:
	movq	-200(%rbp), %rdi
	movq	%rax, 32(%r12)
	movq	-264(%rbp), %rax
	addq	%rdi, %rbx
	movl	16(%rbx), %edx
	testl	%edx, %edx
	je	.L269
	leaq	112(%r13), %rax
.L269:
	movq	%rax, 40(%r12)
	cmpw	$10, 20(%rbx)
	je	.L294
	movq	-272(%rbp), %rax
	movq	%rax, 48(%r12)
.L286:
	addq	$1, -216(%rbp)
	movl	-204(%rbp), %esi
	addq	$56, %r12
	movq	-216(%rbp), %rax
	cmpl	%eax, %esi
	jle	.L257
.L272:
	movq	-216(%rbp), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	(%rax,%rax,4), %rbx
	salq	$4, %rbx
	movq	(%rdi,%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L295
.L261:
	movq	-200(%rbp), %rax
	subq	$8, %rsp
	movq	-232(%rbp), %rdi
	leaq	.LC8(%rip), %r8
	movl	$18, %ecx
	movl	$18, %esi
	addq	%rbx, %rax
	movzbl	13(%rax), %edx
	movzbl	8(%rax), %r9d
	pushq	%rdx
	movzbl	12(%rax), %edx
	pushq	%rdx
	movzbl	11(%rax), %edx
	pushq	%rdx
	movzbl	10(%rax), %edx
	pushq	%rdx
	movzbl	9(%rax), %eax
	movl	$1, %edx
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %rdi
	addq	$48, %rsp
	addq	%rbx, %rdi
	movzwl	20(%rdi), %eax
	cmpw	$2, %ax
	je	.L296
	cmpw	$10, %ax
	jne	.L264
	addq	$20, %rdi
	movl	$46, %edx
	movq	%r15, %rsi
	call	uv_ip6_name@PLT
	movq	-200(%rbp), %rdi
	movl	$46, %edx
	movq	-248(%rbp), %rsi
	addq	%rbx, %rdi
	addq	$48, %rdi
	call	uv_ip6_name@PLT
	movq	-224(%rbp), %rax
	movq	360(%rax), %rax
	movq	936(%rax), %rax
	movq	%rax, -240(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L296:
	addq	$20, %rdi
	movl	$46, %edx
	movq	%r15, %rsi
	call	uv_ip4_name@PLT
	movq	-200(%rbp), %rdi
	movl	$46, %edx
	movq	-248(%rbp), %rsi
	addq	%rbx, %rdi
	addq	$48, %rdi
	call	uv_ip4_name@PLT
	movq	-224(%rbp), %rax
	movq	360(%rax), %rax
	movq	928(%rax), %rax
	movq	%rax, -240(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L294:
	movl	44(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-200(%rbp), %rdi
	movq	%rax, 48(%r12)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L257:
	call	uv_free_interface_addresses@PLT
	movq	-256(%rbp), %rax
	movq	-288(%rbp), %rdx
	movq	%r13, %rdi
	movq	-280(%rbp), %rsi
	movq	(%rax), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L297
	movq	(%rax), %rax
.L274:
	movq	%rax, 24(%rbx)
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L245
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L295:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rax, -296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-296(%rbp), %rax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L293:
	movq	%rax, -240(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-240(%rbp), %rax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%rax, -296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-296(%rbp), %rax
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L287:
	movq	-256(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L246:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -224(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	_ZZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	movq	16(%rbx), %rax
	jmp	.L274
.L289:
	call	__stack_chk_fail@PLT
.L290:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7103:
	.size	_ZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC10:
	.string	"getHostname"
.LC11:
	.string	"getLoadAvg"
.LC12:
	.string	"getUptime"
.LC13:
	.string	"getTotalMem"
.LC14:
	.string	"getFreeMem"
.LC15:
	.string	"getCPUs"
.LC16:
	.string	"getOSInformation"
.LC17:
	.string	"getInterfaceAddresses"
.LC18:
	.string	"getHomeDirectory"
.LC19:
	.string	"getUserInfo"
.LC20:
	.string	"setPriority"
.LC21:
	.string	"getPriority"
.LC22:
	.string	"isBigEndian"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB23:
	.text
.LHOTB23:
	.p2align 4
	.globl	_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L299
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L299
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L299
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L347
.L300:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L348
.L301:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L349
.L302:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L350
.L303:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L351
.L304:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L352
.L305:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL9GetUptimeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L353
.L306:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L354
.L307:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L355
.L308:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL14GetTotalMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L356
.L309:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L357
.L310:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L358
.L311:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL13GetFreeMemoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L359
.L312:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L360
.L313:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L361
.L314:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node2osL10GetCPUInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L362
.L315:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L363
.L316:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L364
.L317:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L365
.L318:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L366
.L319:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L367
.L320:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L368
.L321:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L369
.L322:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L370
.L323:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L371
.L324:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L372
.L325:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L373
.L326:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L374
.L327:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L375
.L328:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L376
.L329:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L377
.L330:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L378
.L331:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L379
.L332:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L380
.L333:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L381
.L334:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L382
.L335:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC22(%rip), %rsi
	leaq	120(%rdi), %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L383
.L338:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L384
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L348:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L349:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L350:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L351:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L352:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L353:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L354:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L355:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L356:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L357:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L358:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L359:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L360:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L362:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L364:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L366:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L367:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L368:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L369:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L370:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L371:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L372:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L373:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L374:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L375:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L376:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L377:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L379:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L380:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L382:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L384:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
.L346:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7110:
.L299:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7110:
	.text
	.size	_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE23:
	.text
.LHOTE23:
	.p2align 4
	.globl	_Z12_register_osv
	.type	_Z12_register_osv, @function
_Z12_register_osv:
.LFB7111:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7111:
	.size	_Z12_register_osv, .-_Z12_register_osv
	.section	.rodata.str1.1
.LC24:
	.string	"../src/node_os.cc"
.LC25:
	.string	"os"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC24
	.quad	0
	.quad	_ZN4node2os10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC25
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC26:
	.string	"../src/node_os.cc:370"
.LC27:
	.string	"args[1]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"void node::os::GetPriority(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"../src/node_os.cc:363"
.LC30:
	.string	"args[0]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC28
	.section	.rodata.str1.1
.LC31:
	.string	"../src/node_os.cc:362"
.LC32:
	.string	"(args.Length()) == (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL11GetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC28
	.section	.rodata.str1.1
.LC33:
	.string	"../src/node_os.cc:351"
.LC34:
	.string	"args[2]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"void node::os::SetPriority(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"../src/node_os.cc:344"
.LC37:
	.string	"args[1]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC35
	.section	.rodata.str1.1
.LC38:
	.string	"../src/node_os.cc:343"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC38
	.quad	.LC30
	.quad	.LC35
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_os.cc:342"
.LC40:
	.string	"(args.Length()) == (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL11SetPriorityERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC35
	.section	.rodata.str1.1
.LC41:
	.string	"../src/node_os.cc:316"
.LC42:
	.string	"!error.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::os::GetUserInfo(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/node_os.cc:288"
.LC45:
	.string	"(args.Length()) >= (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL11GetUserInfoERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC43
	.section	.rodata.str1.1
.LC46:
	.string	"../src/node_os.cc:254"
.LC47:
	.string	"(args.Length()) >= (1)"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"void node::os::GetHomeDirectory(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL16GetHomeDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.section	.rodata.str1.1
.LC49:
	.string	"../src/node_os.cc:184"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"void node::os::GetInterfaceAddresses(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL21GetInterfaceAddressesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC49
	.quad	.LC47
	.quad	.LC50
	.section	.rodata.str1.1
.LC51:
	.string	"../src/node_os.cc:161"
.LC52:
	.string	"(array->Length()) == (3)"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"void node::os::GetLoadAvg(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.section	.rodata.str1.1
.LC54:
	.string	"../src/node_os.cc:159"
.LC55:
	.string	"args[0]->IsFloat64Array()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL10GetLoadAvgERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC53
	.section	.rodata.str1.1
.LC56:
	.string	"../src/node_os.cc:85"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"void node::os::GetOSInformation(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL16GetOSInformationERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC56
	.quad	.LC47
	.quad	.LC57
	.section	.rodata.str1.1
.LC58:
	.string	"../src/node_os.cc:67"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"void node::os::GetHostname(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node2osL11GetHostnameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC58
	.quad	.LC47
	.quad	.LC59
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.quad	7959953386440127804
	.quad	7596835237701710624
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
