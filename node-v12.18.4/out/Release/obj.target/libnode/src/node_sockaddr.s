	.file	"node_sockaddr.cc"
	.text
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4971:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4971:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4972:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4972:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6406:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6406:
	.size	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node13SocketAddress8SelfSizeEv,"axG",@progbits,_ZNK4node13SocketAddress8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13SocketAddress8SelfSizeEv
	.type	_ZNK4node13SocketAddress8SelfSizeEv, @function
_ZNK4node13SocketAddress8SelfSizeEv:
.LFB6408:
	.cfi_startproc
	endbr64
	movl	$136, %eax
	ret
	.cfi_endproc
.LFE6408:
	.size	_ZNK4node13SocketAddress8SelfSizeEv, .-_ZNK4node13SocketAddress8SelfSizeEv
	.section	.text._ZN4node13SocketAddressD2Ev,"axG",@progbits,_ZN4node13SocketAddressD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13SocketAddressD2Ev
	.type	_ZN4node13SocketAddressD2Ev, @function
_ZN4node13SocketAddressD2Ev:
.LFB7715:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7715:
	.size	_ZN4node13SocketAddressD2Ev, .-_ZN4node13SocketAddressD2Ev
	.weak	_ZN4node13SocketAddressD1Ev
	.set	_ZN4node13SocketAddressD1Ev,_ZN4node13SocketAddressD2Ev
	.section	.text._ZN4node13SocketAddressD0Ev,"axG",@progbits,_ZN4node13SocketAddressD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13SocketAddressD0Ev
	.type	_ZN4node13SocketAddressD0Ev, @function
_ZN4node13SocketAddressD0Ev:
.LFB7717:
	.cfi_startproc
	endbr64
	movl	$136, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7717:
	.size	_ZN4node13SocketAddressD0Ev, .-_ZN4node13SocketAddressD0Ev
	.section	.text._ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev:
.LFB6407:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1936028260, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7224183256221183827, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$115, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE6407:
	.size	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev, .-_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage
	.type	_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage, @function
_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage:
.LFB6451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	%edx, %esi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$2, %edi
	je	.L10
	cmpl	$10, %edi
	jne	.L16
	movq	%r8, %rdi
	call	uv_ip6_addr@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r8, %rdi
	call	uv_ip4_addr@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
.L16:
	.cfi_restore_state
	leaq	_ZZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storageE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE6451:
	.size	_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage, .-_ZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storage
	.align 2
	.p2align 4
	.globl	_ZN4node13SocketAddress3NewEPKcjPS0_
	.type	_ZN4node13SocketAddress3NewEPKcjPS0_, @function
_ZN4node13SocketAddress3NewEPKcjPS0_:
.LFB6452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdx), %r14
	pushq	%r13
	movq	%r14, %rdx
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	uv_ip4_addr@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L23
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	uv_ip6_addr@PLT
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6452:
	.size	_ZN4node13SocketAddress3NewEPKcjPS0_, .-_ZN4node13SocketAddress3NewEPKcjPS0_
	.align 2
	.p2align 4
	.globl	_ZN4node13SocketAddress3NewEiPKcjPS0_
	.type	_ZN4node13SocketAddress3NewEiPKcjPS0_, @function
_ZN4node13SocketAddress3NewEiPKcjPS0_:
.LFB6453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	%edx, %esi
	leaq	8(%rcx), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$2, %edi
	je	.L25
	cmpl	$10, %edi
	jne	.L31
	movq	%r8, %rdi
	call	uv_ip6_addr@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%r8, %rdi
	call	uv_ip4_addr@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
.L31:
	.cfi_restore_state
	leaq	_ZZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storageE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE6453:
	.size	_ZN4node13SocketAddress3NewEiPKcjPS0_, .-_ZN4node13SocketAddress3NewEiPKcjPS0_
	.align 2
	.p2align 4
	.globl	_ZNK4node13SocketAddress4HashclERKS0_
	.type	_ZNK4node13SocketAddress4HashclERKS0_, @function
_ZNK4node13SocketAddress4HashclERKS0_:
.LFB6454:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %eax
	cmpw	$2, %ax
	je	.L33
	cmpw	$10, %ax
	jne	.L42
	movzwl	10(%rsi), %ecx
	movl	$2654435769, %edx
	movq	16(%rsi), %rdi
	addq	%rdx, %rcx
	addq	%rdx, %rdi
	addq	24(%rsi), %rdx
	movq	%rcx, %rax
	salq	$6, %rax
	addq	%rax, %rdi
	movq	%rcx, %rax
	shrq	$2, %rax
	addq	%rdi, %rax
	xorq	%rax, %rcx
.L41:
	movq	%rcx, %rax
	salq	$6, %rax
	addq	%rax, %rdx
	movq	%rcx, %rax
	shrq	$2, %rax
	addq	%rdx, %rax
	xorq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movzwl	10(%rsi), %eax
	movl	12(%rsi), %edx
	movl	$2654435769, %edi
	leaq	(%rax,%rdi), %rcx
	addq	%rdi, %rdx
	jmp	.L41
.L42:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node13SocketAddress4HashclERKS0_E4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE6454:
	.size	_ZNK4node13SocketAddress4HashclERKS0_, .-_ZNK4node13SocketAddress4HashclERKS0_
	.align 2
	.p2align 4
	.globl	_ZN4node13SocketAddress12FromSockNameERK8uv_tcp_s
	.type	_ZN4node13SocketAddress12FromSockNameERK8uv_tcp_s, @function
_ZN4node13SocketAddress12FromSockNameERK8uv_tcp_s:
.LFB6455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-28(%rbp), %rdx
	leaq	8(%r12), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node13SocketAddressE(%rip), %rax
	movl	$128, -28(%rbp)
	movq	%rax, (%r12)
	call	uv_tcp_getsockname@PLT
	testl	%eax, %eax
	jne	.L44
	cmpw	$2, 8(%r12)
	movl	$16, %eax
	movl	$28, %ecx
	movslq	-28(%rbp), %rdx
	cmovne	%rcx, %rax
	cmpq	%rax, %rdx
	jne	.L50
.L43:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 8(%r12)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_tcp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6455:
	.size	_ZN4node13SocketAddress12FromSockNameERK8uv_tcp_s, .-_ZN4node13SocketAddress12FromSockNameERK8uv_tcp_s
	.align 2
	.p2align 4
	.globl	_ZN4node13SocketAddress12FromSockNameERK8uv_udp_s
	.type	_ZN4node13SocketAddress12FromSockNameERK8uv_udp_s, @function
_ZN4node13SocketAddress12FromSockNameERK8uv_udp_s:
.LFB6456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-28(%rbp), %rdx
	leaq	8(%r12), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node13SocketAddressE(%rip), %rax
	movl	$128, -28(%rbp)
	movq	%rax, (%r12)
	call	uv_udp_getsockname@PLT
	testl	%eax, %eax
	jne	.L53
	cmpw	$2, 8(%r12)
	movl	$16, %eax
	movl	$28, %ecx
	movslq	-28(%rbp), %rdx
	cmovne	%rcx, %rax
	cmpq	%rax, %rdx
	jne	.L59
.L52:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 8(%r12)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_udp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6456:
	.size	_ZN4node13SocketAddress12FromSockNameERK8uv_udp_s, .-_ZN4node13SocketAddress12FromSockNameERK8uv_udp_s
	.align 2
	.p2align 4
	.globl	_ZN4node13SocketAddress12FromPeerNameERK8uv_tcp_s
	.type	_ZN4node13SocketAddress12FromPeerNameERK8uv_tcp_s, @function
_ZN4node13SocketAddress12FromPeerNameERK8uv_tcp_s:
.LFB6457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-28(%rbp), %rdx
	leaq	8(%r12), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node13SocketAddressE(%rip), %rax
	movl	$128, -28(%rbp)
	movq	%rax, (%r12)
	call	uv_tcp_getpeername@PLT
	testl	%eax, %eax
	jne	.L62
	cmpw	$2, 8(%r12)
	movl	$16, %eax
	movl	$28, %ecx
	movslq	-28(%rbp), %rdx
	cmovne	%rcx, %rax
	cmpq	%rax, %rdx
	jne	.L68
.L61:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 8(%r12)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_tcp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6457:
	.size	_ZN4node13SocketAddress12FromPeerNameERK8uv_tcp_s, .-_ZN4node13SocketAddress12FromPeerNameERK8uv_tcp_s
	.align 2
	.p2align 4
	.globl	_ZN4node13SocketAddress12FromPeerNameERK8uv_udp_s
	.type	_ZN4node13SocketAddress12FromPeerNameERK8uv_udp_s, @function
_ZN4node13SocketAddress12FromPeerNameERK8uv_udp_s:
.LFB6458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-28(%rbp), %rdx
	leaq	8(%r12), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node13SocketAddressE(%rip), %rax
	movl	$128, -28(%rbp)
	movq	%rax, (%r12)
	call	uv_udp_getpeername@PLT
	testl	%eax, %eax
	jne	.L71
	cmpw	$2, 8(%r12)
	movl	$16, %eax
	movl	$28, %ecx
	movslq	-28(%rbp), %rdx
	cmovne	%rcx, %rax
	cmpq	%rax, %rdx
	jne	.L77
.L70:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, 8(%r12)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_udp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6458:
	.size	_ZN4node13SocketAddress12FromPeerNameERK8uv_udp_s, .-_ZN4node13SocketAddress12FromPeerNameERK8uv_udp_s
	.weak	_ZTVN4node13SocketAddressE
	.section	.data.rel.ro.local._ZTVN4node13SocketAddressE,"awG",@progbits,_ZTVN4node13SocketAddressE,comdat
	.align 8
	.type	_ZTVN4node13SocketAddressE, @object
	.size	_ZTVN4node13SocketAddressE, 72
_ZTVN4node13SocketAddressE:
	.quad	0
	.quad	0
	.quad	_ZN4node13SocketAddressD1Ev
	.quad	_ZN4node13SocketAddressD0Ev
	.quad	_ZNK4node13SocketAddress10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node13SocketAddress14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node13SocketAddress8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../src/node_sockaddr.cc:12"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"(static_cast<size_t>(len)) == (addr.length())"
	.align 8
.LC2:
	.string	"node::SocketAddress node::{anonymous}::FromUVHandle(F, const T&) [with T = uv_udp_s; F = int (*)(const uv_udp_s*, sockaddr*, int*)]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_udp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args, @object
	.size	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_udp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args, 24
_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_udp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"node::SocketAddress node::{anonymous}::FromUVHandle(F, const T&) [with T = uv_tcp_s; F = int (*)(const uv_tcp_s*, sockaddr*, int*)]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_tcp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args, @object
	.size	_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_tcp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args, 24
_ZZN4node12_GLOBAL__N_112FromUVHandleI8uv_tcp_sPFiPKS2_P8sockaddrPiEEENS_13SocketAddressET0_RKT_E4args:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC3
	.section	.rodata.str1.1
.LC4:
	.string	"../src/node_sockaddr.cc:74"
.LC5:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"size_t node::SocketAddress::Hash::operator()(const node::SocketAddress&) const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node13SocketAddress4HashclERKS0_E4args, @object
	.size	_ZZNK4node13SocketAddress4HashclERKS0_E4args, 24
_ZZNK4node13SocketAddress4HashclERKS0_E4args:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.section	.rodata.str1.1
.LC7:
	.string	"../src/node_sockaddr.cc:36"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"static bool node::SocketAddress::ToSockAddr(int32_t, const char*, uint32_t, sockaddr_storage*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storageE4args, @object
	.size	_ZZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storageE4args, 24
_ZZN4node13SocketAddress10ToSockAddrEiPKcjP16sockaddr_storageE4args:
	.quad	.LC7
	.quad	.LC5
	.quad	.LC8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
