	.file	"node_platform.cc"
	.text
	.section	.text._ZN2v88Platform16GetPageAllocatorEv,"axG",@progbits,_ZN2v88Platform16GetPageAllocatorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform16GetPageAllocatorEv
	.type	_ZN2v88Platform16GetPageAllocatorEv, @function
_ZN2v88Platform16GetPageAllocatorEv:
.LFB4181:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4181:
	.size	_ZN2v88Platform16GetPageAllocatorEv, .-_ZN2v88Platform16GetPageAllocatorEv
	.section	.text._ZN2v88Platform24OnCriticalMemoryPressureEv,"axG",@progbits,_ZN2v88Platform24OnCriticalMemoryPressureEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform24OnCriticalMemoryPressureEv
	.type	_ZN2v88Platform24OnCriticalMemoryPressureEv, @function
_ZN2v88Platform24OnCriticalMemoryPressureEv:
.LFB4182:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4182:
	.size	_ZN2v88Platform24OnCriticalMemoryPressureEv, .-_ZN2v88Platform24OnCriticalMemoryPressureEv
	.section	.text._ZN2v88Platform24OnCriticalMemoryPressureEm,"axG",@progbits,_ZN2v88Platform24OnCriticalMemoryPressureEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform24OnCriticalMemoryPressureEm
	.type	_ZN2v88Platform24OnCriticalMemoryPressureEm, @function
_ZN2v88Platform24OnCriticalMemoryPressureEm:
.LFB4183:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4183:
	.size	_ZN2v88Platform24OnCriticalMemoryPressureEm, .-_ZN2v88Platform24OnCriticalMemoryPressureEm
	.section	.text._ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE,"axG",@progbits,_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE
	.type	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE, @function
_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE:
.LFB4188:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4188:
	.size	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE, .-_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE
	.section	.text._ZN2v88Platform20GetStackTracePrinterEv,"axG",@progbits,_ZN2v88Platform20GetStackTracePrinterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform20GetStackTracePrinterEv
	.type	_ZN2v88Platform20GetStackTracePrinterEv, @function
_ZN2v88Platform20GetStackTracePrinterEv:
.LFB4189:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4189:
	.size	_ZN2v88Platform20GetStackTracePrinterEv, .-_ZN2v88Platform20GetStackTracePrinterEv
	.section	.text._ZN2v88Platform19DumpWithoutCrashingEv,"axG",@progbits,_ZN2v88Platform19DumpWithoutCrashingEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform19DumpWithoutCrashingEv
	.type	_ZN2v88Platform19DumpWithoutCrashingEv, @function
_ZN2v88Platform19DumpWithoutCrashingEv:
.LFB4190:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4190:
	.size	_ZN2v88Platform19DumpWithoutCrashingEv, .-_ZN2v88Platform19DumpWithoutCrashingEv
	.section	.text._ZN2v88Platform11AddCrashKeyEiPKcm,"axG",@progbits,_ZN2v88Platform11AddCrashKeyEiPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform11AddCrashKeyEiPKcm
	.type	_ZN2v88Platform11AddCrashKeyEiPKcm, @function
_ZN2v88Platform11AddCrashKeyEiPKcm:
.LFB4191:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4191:
	.size	_ZN2v88Platform11AddCrashKeyEiPKcm, .-_ZN2v88Platform11AddCrashKeyEiPKcm
	.section	.text._ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv,"axG",@progbits,_ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv
	.type	_ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv, @function
_ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv:
.LFB5389:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5389:
	.size	_ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv, .-_ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv
	.section	.text._ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv,"axG",@progbits,_ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv
	.type	_ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv, @function
_ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv:
.LFB5390:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5390:
	.size	_ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv, .-_ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv
	.section	.text._ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv,"axG",@progbits,_ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv
	.type	_ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv, @function
_ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv:
.LFB5391:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5391:
	.size	_ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv, .-_ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv
	.section	.text._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_,"axG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_,comdat
	.p2align 4
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_, @function
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_:
.LFB7556:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7556:
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_, .-_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform21NumberOfWorkerThreadsEv
	.type	_ZN4node12NodePlatform21NumberOfWorkerThreadsEv, @function
_ZN4node12NodePlatform21NumberOfWorkerThreadsEv:
.LFB7722:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rdx
	movq	240(%rdx), %rax
	subq	232(%rdx), %rax
	sarq	$3, %rax
	ret
	.cfi_endproc
.LFE7722:
	.size	_ZN4node12NodePlatform21NumberOfWorkerThreadsEv, .-_ZN4node12NodePlatform21NumberOfWorkerThreadsEv
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform16IdleTasksEnabledEPN2v87IsolateE
	.type	_ZN4node12NodePlatform16IdleTasksEnabledEPN2v87IsolateE, @function
_ZN4node12NodePlatform16IdleTasksEnabledEPN2v87IsolateE:
.LFB7745:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7745:
	.size	_ZN4node12NodePlatform16IdleTasksEnabledEPN2v87IsolateE, .-_ZN4node12NodePlatform16IdleTasksEnabledEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform20GetStackTracePrinterEv
	.type	_ZN4node12NodePlatform20GetStackTracePrinterEv, @function
_ZN4node12NodePlatform20GetStackTracePrinterEv:
.LFB7756:
	.cfi_startproc
	endbr64
	leaq	_ZZN4node12NodePlatform20GetStackTracePrinterEvENUlvE_4_FUNEv(%rip), %rax
	ret
	.cfi_endproc
.LFE7756:
	.size	_ZN4node12NodePlatform20GetStackTracePrinterEv, .-_ZN4node12NodePlatform20GetStackTracePrinterEv
	.align 2
	.p2align 4
	.globl	_ZN4node20MultiIsolatePlatform25CancelPendingDelayedTasksEPN2v87IsolateE
	.type	_ZN4node20MultiIsolatePlatform25CancelPendingDelayedTasksEPN2v87IsolateE, @function
_ZN4node20MultiIsolatePlatform25CancelPendingDelayedTasksEPN2v87IsolateE:
.LFB7768:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7768:
	.size	_ZN4node20MultiIsolatePlatform25CancelPendingDelayedTasksEPN2v87IsolateE, .-_ZN4node20MultiIsolatePlatform25CancelPendingDelayedTasksEPN2v87IsolateE
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD2Ev,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD2Ev
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD2Ev, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD2Ev:
.LFB9230:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L17:
	ret
	.cfi_endproc
.LFE9230:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD2Ev, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD2Ev
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD1Ev
	.set	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD1Ev,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD2Ev
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD2Ev,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD2Ev
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD2Ev, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD2Ev:
.LFB9255:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9255:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD2Ev, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD2Ev
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD1Ev
	.set	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD1Ev,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11614:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11614:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11617:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r8
	movq	%r8, %rdi
	movq	64(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE11617:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11621:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11621:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EE
	.type	_ZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EE, @function
_ZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EE:
.LFB7645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7645:
	.size	_ZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EE, .-_ZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform20GetTracingControllerEv
	.type	_ZN4node12NodePlatform20GetTracingControllerEv, @function
_ZN4node12NodePlatform20GetTracingControllerEv:
.LFB7755:
	.cfi_startproc
	endbr64
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L30
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node12NodePlatform20GetTracingControllerEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7755:
	.size	_ZN4node12NodePlatform20GetTracingControllerEv, .-_ZN4node12NodePlatform20GetTracingControllerEv
	.section	.text._ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd,"axG",@progbits,_ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd
	.type	_ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd, @function
_ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd:
.LFB5397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEdE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5397:
	.size	_ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd, .-_ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd
	.section	.text._ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE,"axG",@progbits,_ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE
	.type	_ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE, @function
_ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE:
.LFB5396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5396:
	.size	_ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE, .-_ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE
	.text
	.p2align 4
	.type	_ZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENUlPNS_11DelayedTaskEE_4_FUNES2_, @function
_ZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENUlPNS_11DelayedTaskEE_4_FUNES2_:
.LFB7736:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	leaq	_ZZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENKUlPNS_11DelayedTaskEE_clES2_ENUlP11uv_handle_sE_4_FUNES5_(%rip), %rsi
	jmp	uv_close@PLT
	.cfi_endproc
.LFE7736:
	.size	_ZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENUlPNS_11DelayedTaskEE_4_FUNES2_, .-_ZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENUlPNS_11DelayedTaskEE_4_FUNES2_
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD0Ev,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD0Ev
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD0Ev, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD0Ev:
.LFB9257:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9257:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD0Ev, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD0Ev
	.section	.text._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_,"axG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_, @function
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_:
.LFB7567:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L37
	movl	$152, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	ret
	.cfi_endproc
.LFE7567:
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_, .-_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD0Ev,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD0Ev
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD0Ev, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD0Ev:
.LFB9232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*8(%rax)
.L40:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9232:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD0Ev, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11623:
	.cfi_startproc
	endbr64
	movl	$272, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11623:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11616:
	.cfi_startproc
	endbr64
	movl	$584, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11616:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11618:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE11618:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11625:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE11625:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"__metadata"
.LC1:
	.string	"name"
.LC2:
	.string	"PlatformWorkerThread"
.LC3:
	.string	"thread_name"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPv, @function
_ZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPv:
.LFB7486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPvE27trace_event_unique_atomic33(%rip), %r14
	testq	%r14, %r14
	je	.L82
	testb	$5, (%r14)
	jne	.L83
.L53:
	movq	8(%r13), %r14
	leaq	40(%r12), %rbx
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	24(%r13), %rax
	movq	16(%r13), %rdi
	subl	$1, (%rax)
	call	uv_cond_signal@PLT
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	160(%r12), %rax
	cmpq	%rax, 192(%r12)
	je	.L59
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	uv_cond_wait@PLT
	movq	160(%r12), %rax
	cmpq	%rax, 192(%r12)
	jne	.L56
.L59:
	cmpb	$0, 140(%r12)
	je	.L57
.L60:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
.L58:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	cmpb	$0, 140(%r12)
	jne	.L60
	movq	176(%r12), %rcx
	movq	(%rax), %r14
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L61
	addq	$8, %rax
	movq	%rax, 160(%r12)
.L62:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	testq	%r14, %r14
	je	.L58
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	subl	$1, 136(%r12)
	je	.L85
.L63:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	88(%r12), %rdi
	call	uv_cond_broadcast@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L61:
	movq	168(%r12), %rdi
	call	_ZdlPv@PLT
	movq	184(%r12), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 160(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 176(%r12)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L82:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L52
	movq	(%rax), %rax
	leaq	.LC0(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r14
.L52:
	movq	%r14, _ZZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPvE27trace_event_unique_atomic33(%rip)
	mfence
	testb	$5, (%r14)
	je	.L53
.L83:
	leaq	.LC1(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$6, -81(%rbp)
	movq	%rax, -80(%rbp)
	leaq	.LC2(%rip), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	leaq	-81(%rbp), %r9
	movq	%r14, %rsi
	movq	%rax, %rdi
	leaq	-64(%rbp), %rax
	pushq	$0
	leaq	-80(%rbp), %r8
	pushq	%rax
	leaq	-72(%rbp), %rax
	movl	$1, %ecx
	leaq	.LC3(%rip), %rdx
	pushq	%rax
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	movq	-56(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rax
	call	*8(%rax)
.L54:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L53
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7486:
	.size	_ZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPv, .-_ZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L86
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L86:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11626:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L90
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L90:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11619:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text.unlikely._ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE,"axG",@progbits,_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE,comdat
	.align 2
	.weak	_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE
	.type	_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE, @function
_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE:
.LFB4187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE4187:
	.size	_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE, .-_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE
	.section	.text._ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv,"axG",@progbits,_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.type	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv, @function
_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv:
.LFB5364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_hrtime@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movabsq	$2361183241434822607, %rdx
	shrq	$3, %rax
	mulq	%rdx
	movq	%rdx, %rax
	shrq	$4, %rax
	ret
	.cfi_endproc
.LFE5364:
	.size	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv, .-_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform27MonotonicallyIncreasingTimeEv
	.type	_ZN4node12NodePlatform27MonotonicallyIncreasingTimeEv, @function
_ZN4node12NodePlatform27MonotonicallyIncreasingTimeEv:
.LFB7753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_hrtime@PLT
	testq	%rax, %rax
	js	.L99
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cvtsi2sdq	%rax, %xmm0
	divsd	.LC4(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	divsd	.LC4(%rip), %xmm0
	ret
	.cfi_endproc
.LFE7753:
	.size	_ZN4node12NodePlatform27MonotonicallyIncreasingTimeEv, .-_ZN4node12NodePlatform27MonotonicallyIncreasingTimeEv
	.section	.text._ZN4node7tracing17TracingControllerD2Ev,"axG",@progbits,_ZN4node7tracing17TracingControllerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing17TracingControllerD2Ev
	.type	_ZN4node7tracing17TracingControllerD2Ev, @function
_ZN4node7tracing17TracingControllerD2Ev:
.LFB11594:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7tracing17TracingControllerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88platform7tracing17TracingControllerD2Ev@PLT
	.cfi_endproc
.LFE11594:
	.size	_ZN4node7tracing17TracingControllerD2Ev, .-_ZN4node7tracing17TracingControllerD2Ev
	.weak	_ZN4node7tracing17TracingControllerD1Ev
	.set	_ZN4node7tracing17TracingControllerD1Ev,_ZN4node7tracing17TracingControllerD2Ev
	.section	.text._ZN4node7tracing17TracingControllerD0Ev,"axG",@progbits,_ZN4node7tracing17TracingControllerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing17TracingControllerD0Ev
	.type	_ZN4node7tracing17TracingControllerD0Ev, @function
_ZN4node7tracing17TracingControllerD0Ev:
.LFB11596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing17TracingControllerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88platform7tracing17TracingControllerD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11596:
	.size	_ZN4node7tracing17TracingControllerD0Ev, .-_ZN4node7tracing17TracingControllerD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform22CurrentClockTimeMillisEv
	.type	_ZN4node12NodePlatform22CurrentClockTimeMillisEv, @function
_ZN4node12NodePlatform22CurrentClockTimeMillisEv:
.LFB7754:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88Platform21SystemClockTimeMillisEv@PLT
	.cfi_endproc
.LFE7754:
	.size	_ZN4node12NodePlatform22CurrentClockTimeMillisEv, .-_ZN4node12NodePlatform22CurrentClockTimeMillisEv
	.p2align 4
	.type	_ZZN4node12NodePlatform20GetStackTracePrinterEvENUlvE_4_FUNEv, @function
_ZZN4node12NodePlatform20GetStackTracePrinterEvENUlvE_4_FUNEv:
.LFB7758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	fputc@PLT
	movq	stderr(%rip), %rdi
	call	_ZN4node13DumpBacktraceEP8_IO_FILE@PLT
	movq	stderr(%rip), %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	fflush@PLT
	.cfi_endproc
.LFE7758:
	.size	_ZZN4node12NodePlatform20GetStackTracePrinterEvENUlvE_4_FUNEv, .-_ZZN4node12NodePlatform20GetStackTracePrinterEvENUlvE_4_FUNEv
	.section	.rodata._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"WorkerThreadsTaskRunner::DelayedTaskScheduler"
	.section	.text._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_,"axG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_,comdat
	.p2align 4
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_, @function
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_:
.LFB7524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81(%rip), %r13
	testq	%r13, %r13
	je	.L130
	testb	$5, 0(%r13)
	jne	.L131
.L112:
	movq	%r12, 264(%r12)
	leaq	264(%r12), %r13
	movq	%r13, %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L132
	movq	%r12, 1112(%r12)
	leaq	1112(%r12), %rsi
	leaq	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s(%rip), %rdx
	movq	%r13, %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L133
	movq	%r12, %rdi
	call	uv_sem_post@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	movq	%r13, %rdi
	call	_ZN4node18CheckedUvLoopCloseEP9uv_loop_s@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L111
	movq	(%rax), %rax
	leaq	.LC0(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r13
.L111:
	movq	%r13, _ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81(%rip)
	mfence
	testb	$5, 0(%r13)
	je	.L112
.L131:
	leaq	.LC1(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$6, -65(%rbp)
	movq	%rax, -64(%rbp)
	leaq	.LC5(%rip), %rax
	movaps	%xmm0, -48(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	leaq	-65(%rbp), %r9
	movq	%r13, %rsi
	movq	%rax, %rdi
	leaq	-48(%rbp), %rax
	pushq	$0
	leaq	-64(%rbp), %r8
	pushq	%rax
	leaq	-56(%rbp), %rax
	movl	$1, %ecx
	leaq	.LC3(%rip), %rdx
	pushq	%rax
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	movq	-40(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L113
	movq	(%rdi), %rax
	call	*8(%rax)
.L113:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7524:
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_, .-_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s,comdat
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s:
.LFB7533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	leaq	-264(%r13), %rbx
	subq	$224, %r13
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L136:
	movq	216(%rbx), %rcx
	movq	(%rax), %r12
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L137
	addq	$8, %rax
	movq	%r13, %rdi
	movq	%rax, 200(%rbx)
	call	uv_mutex_unlock@PLT
	testq	%r12, %r12
	je	.L135
.L139:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L140:
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	200(%rbx), %rax
	cmpq	%rax, 232(%rbx)
	jne	.L136
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	208(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	224(%rbx), %rdx
	movq	%r13, %rdi
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 200(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 216(%rbx)
	call	uv_mutex_unlock@PLT
	testq	%r12, %r12
	jne	.L139
.L135:
	popq	%rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7533:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler10FlushTasksEP10uv_async_s
	.text
	.p2align 4
	.type	_ZZN4node22PerIsolatePlatformData8ShutdownEvENUlP11uv_handle_sE_4_FUNES2_, @function
_ZZN4node22PerIsolatePlatformData8ShutdownEvENUlP11uv_handle_sE_4_FUNES2_:
.LFB7665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r12
	movl	64(%r12), %eax
	testl	%eax, %eax
	je	.L161
	subl	$1, %eax
	movq	%rdi, %r13
	movl	%eax, 64(%r12)
	jne	.L150
	movq	24(%r12), %rbx
	movq	32(%r12), %r14
	cmpq	%r14, %rbx
	je	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	movq	8(%rbx), %rdi
	call	*(%rbx)
	addq	$16, %rbx
	cmpq	%rbx, %r14
	jne	.L151
.L150:
	movq	56(%r12), %r14
	pxor	%xmm0, %xmm0
	movups	%xmm0, 48(%r12)
	testq	%r14, %r14
	je	.L153
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L154
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L162
.L153:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$128, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L153
.L162:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L157
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L158:
	cmpl	$1, %eax
	jne	.L153
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	_ZZN4node22PerIsolatePlatformData19DecreaseHandleCountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L158
	.cfi_endproc
.LFE7665:
	.size	_ZZN4node22PerIsolatePlatformData8ShutdownEvENUlP11uv_handle_sE_4_FUNES2_, .-_ZZN4node22PerIsolatePlatformData8ShutdownEvENUlP11uv_handle_sE_4_FUNES2_
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB11588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L165
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L166
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L192
	.p2align 4,,10
	.p2align 3
.L165:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L171
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L177
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L193
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L171
.L172:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L179
.L193:
	lock subl	$1, 8(%r13)
	jne	.L179
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L179
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L172
	.p2align 4,,10
	.p2align 3
.L171:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L175
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L171
.L177:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L175
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L175
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L166:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L165
.L192:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L169
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L170:
	cmpl	$1, %eax
	jne	.L165
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L169:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L170
	.cfi_endproc
.LFE11588:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB11586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L196
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L197
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L223
	.p2align 4,,10
	.p2align 3
.L196:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L202
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L208
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L224
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L202
.L203:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L210
.L224:
	lock subl	$1, 8(%r13)
	jne	.L210
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L210
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L203
	.p2align 4,,10
	.p2align 3
.L202:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L206
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L202
.L208:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L206
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L206
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L197:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L196
.L223:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L200
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L201:
	cmpl	$1, %eax
	jne	.L196
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L200:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L201
	.cfi_endproc
.LFE11586:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.text
	.p2align 4
	.type	_ZZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENKUlPNS_11DelayedTaskEE_clES2_ENUlP11uv_handle_sE_4_FUNES5_, @function
_ZZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENKUlPNS_11DelayedTaskEE_clES2_ENUlP11uv_handle_sE_4_FUNES5_:
.LFB7734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	168(%r12), %rdx
	movl	64(%rdx), %eax
	testl	%eax, %eax
	je	.L243
	subl	$1, %eax
	movl	%eax, 64(%rdx)
	jne	.L227
	movq	24(%rdx), %rbx
	movq	32(%rdx), %r13
	cmpq	%r13, %rbx
	je	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	movq	8(%rbx), %rdi
	call	*(%rbx)
	addq	$16, %rbx
	cmpq	%rbx, %r13
	jne	.L228
.L227:
	movq	176(%r12), %r13
	testq	%r13, %r13
	je	.L232
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L229
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L230:
	cmpl	$1, %eax
	jne	.L232
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L233
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L234:
	cmpl	$1, %eax
	jne	.L232
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L236
	movq	(%rdi), %rax
	call	*8(%rax)
.L236:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	_ZZN4node22PerIsolatePlatformData19DecreaseHandleCountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L234
	.cfi_endproc
.LFE7734:
	.size	_ZZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENKUlPNS_11DelayedTaskEE_clES2_ENUlP11uv_handle_sE_4_FUNES5_, .-_ZZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENKUlPNS_11DelayedTaskEE_clES2_ENUlP11uv_handle_sE_4_FUNES5_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB7:
	.text
.LHOTB7:
	.align 2
	.p2align 4
	.type	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0, @function
_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0:
.LFB11968:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$184, %edi
	subq	$24, %rsp
	movsd	%xmm0, -56(%rbp)
	call	_Znwm@PLT
	movl	$23, %ecx
	movq	16(%rbx), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	rep stosq
	movq	0(%r13), %rax
	movq	$0, 0(%r13)
	movups	%xmm0, 168(%r12)
	movq	%rax, (%r12)
	testq	%rdx, %rdx
	je	.L245
	movl	8(%rdx), %eax
	leaq	8(%rdx), %rcx
.L247:
	testl	%eax, %eax
	je	.L245
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rcx)
	jne	.L247
	movq	8(%rbx), %rax
	movq	176(%r12), %r13
	movq	%rdx, 176(%r12)
	movq	%rax, 168(%r12)
	testq	%r13, %r13
	je	.L249
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L250
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L270
	.p2align 4,,10
	.p2align 3
.L249:
	movsd	-56(%rbp), %xmm1
	leaq	320(%rbx), %r13
	movq	%r13, %rdi
	movsd	%xmm1, 160(%r12)
	call	uv_mutex_lock@PLT
	movq	528(%rbx), %rcx
	movq	512(%rbx), %rax
	addl	$1, 456(%rbx)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L255
	movq	%r12, (%rax)
	addq	$8, %rax
	movq	%rax, 512(%rbx)
.L256:
	leaq	360(%rbx), %rdi
	call	uv_cond_signal@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	88(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_async_send@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L249
.L270:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L253
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L254:
	cmpl	$1, %eax
	jne	.L249
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L255:
	movq	536(%rbx), %r14
	movq	504(%rbx), %rsi
	movabsq	$1152921504606846975, %r8
	subq	520(%rbx), %rax
	movq	%r14, %r15
	sarq	$3, %rax
	subq	%rsi, %r15
	movq	%r15, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	496(%rbx), %rax
	subq	480(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%r8, %rax
	je	.L271
	movq	464(%rbx), %r9
	movq	472(%rbx), %rdx
	movq	%r14, %rax
	subq	%r9, %rax
	movq	%rdx, %rcx
	sarq	$3, %rax
	subq	%rax, %rcx
	cmpq	$1, %rcx
	jbe	.L272
.L258:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	536(%rbx), %rdx
	movq	%rax, 8(%r14)
	movq	512(%rbx), %rax
	addq	$8, %rdx
	movq	%r12, (%rax)
	movq	(%rdx), %rax
	movq	%rdx, %xmm2
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 512(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 528(%rbx)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L253:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	2(%rdi), %r10
	leaq	(%r10,%r10), %rax
	cmpq	%rax, %rdx
	ja	.L273
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%r8, %r14
	ja	.L274
	leaq	0(,%r14,8), %rdi
	movq	%r10, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r10
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r10, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r8
	movq	536(%rbx), %rax
	movq	504(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L263
	movq	%r8, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	%rax, %r8
.L263:
	movq	464(%rbx), %rdi
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 472(%rbx)
	movq	-64(%rbp), %r8
	movq	%rax, 464(%rbx)
.L261:
	movq	(%r8), %rax
	movq	(%r8), %xmm0
	leaq	(%r8,%r15), %r14
	movq	%r8, 504(%rbx)
	movq	%r14, 536(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 488(%rbx)
	movq	(%r14), %rax
	movq	%rax, 520(%rbx)
	addq	$512, %rax
	movq	%rax, 528(%rbx)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L273:
	subq	%r10, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r9,%rdx,8), %r8
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L260
	cmpq	%r14, %rsi
	je	.L261
	movq	%r8, %rdi
	call	memmove@PLT
	movq	%rax, %r8
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L260:
	cmpq	%r14, %rsi
	je	.L261
	leaq	8(%r15), %rdi
	movq	%r8, -56(%rbp)
	subq	%rdx, %rdi
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
	jmp	.L261
.L271:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L274:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0.cold, @function
_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0.cold:
.LFSB11968:
.L245:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE11968:
	.text
	.size	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0, .-_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0
	.section	.text.unlikely
	.size	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0.cold, .-_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0.cold
.LCOLDE7:
	.text
.LHOTE7:
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.type	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, @function
_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd:
.LFB7647:
	.cfi_startproc
	endbr64
	cmpq	$0, 88(%rdi)
	je	.L275
	jmp	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0
	.p2align 4,,10
	.p2align 3
.L275:
	ret
	.cfi_endproc
.LFE7647:
	.size	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, .-_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData26PostNonNestableDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.type	_ZN4node22PerIsolatePlatformData26PostNonNestableDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, @function
_ZN4node22PerIsolatePlatformData26PostNonNestableDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r8, -16(%rbp)
	movq	16(%rax), %rax
	movq	$0, (%rsi)
	cmpq	%rdx, %rax
	jne	.L278
	cmpq	$0, 88(%rdi)
	je	.L279
	leaq	-16(%rbp), %rsi
	call	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd.part.0
	movq	-16(%rbp), %r8
.L279:
	testq	%r8, %r8
	je	.L277
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L277:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	leaq	-16(%rbp), %rsi
	call	*%rax
	movq	-16(%rbp), %r8
	jmp	.L279
.L286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7655:
	.size	_ZN4node22PerIsolatePlatformData26PostNonNestableDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, .-_ZN4node22PerIsolatePlatformData26PostNonNestableDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.align 2
	.p2align 4
	.globl	_ZN4node23WorkerThreadsTaskRunner8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.type	_ZN4node23WorkerThreadsTaskRunner8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, @function
_ZN4node23WorkerThreadsTaskRunner8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE:
.LFB7582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	$0, (%rsi)
	call	uv_mutex_lock@PLT
	movq	208(%r12), %rcx
	movq	192(%r12), %rax
	addl	$1, 136(%r12)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L288
	movq	%rbx, (%rax)
	addq	$8, %rax
	movq	%rax, 192(%r12)
.L289:
	leaq	40(%r12), %rdi
	call	uv_cond_signal@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	216(%r12), %r14
	movq	184(%r12), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	200(%r12), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	176(%r12), %rax
	subq	160(%r12), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L298
	movq	144(%r12), %r8
	movq	152(%r12), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L299
.L291:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	216(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	192(%r12), %rax
	addq	$8, %rdx
	movq	%rbx, (%rax)
	movq	(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 192(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 208(%r12)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L300
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rdi, %r14
	ja	.L301
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	184(%r12), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r15
	movq	216(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L296
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L296:
	movq	144(%r12), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 152(%r12)
	movq	%rax, 144(%r12)
.L294:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 184(%r12)
	movq	%r14, 216(%r12)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 168(%r12)
	movq	(%r14), %rax
	movq	%rax, 200(%r12)
	addq	$512, %rax
	movq	%rax, 208(%r12)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L300:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L293
	cmpq	%r14, %rsi
	je	.L294
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L293:
	cmpq	%r14, %rsi
	je	.L294
	movq	%r15, %rax
	subq	%rdx, %rax
	leaq	8(%rax,%r13), %rdi
	call	memmove@PLT
	jmp	.L294
.L298:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L301:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7582:
	.size	_ZN4node23WorkerThreadsTaskRunner8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, .-_ZN4node23WorkerThreadsTaskRunner8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node23WorkerThreadsTaskRunner13BlockingDrainEv
	.type	_ZN4node23WorkerThreadsTaskRunner13BlockingDrainEv, @function
_ZN4node23WorkerThreadsTaskRunner13BlockingDrainEv:
.LFB7584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	88(%r12), %rbx
	call	uv_mutex_lock@PLT
	movl	136(%r12), %edx
	testl	%edx, %edx
	jle	.L304
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	uv_cond_wait@PLT
	movl	136(%r12), %eax
	testl	%eax, %eax
	jg	.L305
.L304:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE7584:
	.size	_ZN4node23WorkerThreadsTaskRunner13BlockingDrainEv, .-_ZN4node23WorkerThreadsTaskRunner13BlockingDrainEv
	.align 2
	.p2align 4
	.globl	_ZNK4node23WorkerThreadsTaskRunner21NumberOfWorkerThreadsEv
	.type	_ZNK4node23WorkerThreadsTaskRunner21NumberOfWorkerThreadsEv, @function
_ZNK4node23WorkerThreadsTaskRunner21NumberOfWorkerThreadsEv:
.LFB7586:
	.cfi_startproc
	endbr64
	movq	240(%rdi), %rax
	subq	232(%rdi), %rax
	sarq	$3, %rax
	ret
	.cfi_endproc
.LFE7586:
	.size	_ZNK4node23WorkerThreadsTaskRunner21NumberOfWorkerThreadsEv, .-_ZNK4node23WorkerThreadsTaskRunner21NumberOfWorkerThreadsEv
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData19DecreaseHandleCountEv
	.type	_ZN4node22PerIsolatePlatformData19DecreaseHandleCountEv, @function
_ZN4node22PerIsolatePlatformData19DecreaseHandleCountEv:
.LFB7667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	64(%rdi), %eax
	testl	%eax, %eax
	je	.L316
	subl	$1, %eax
	movl	%eax, 64(%rdi)
	jne	.L310
	movq	24(%rdi), %rbx
	movq	32(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L310
	.p2align 4,,10
	.p2align 3
.L313:
	movq	8(%rbx), %rdi
	call	*(%rbx)
	addq	$16, %rbx
	cmpq	%rbx, %r12
	jne	.L313
.L310:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	leaq	_ZZN4node22PerIsolatePlatformData19DecreaseHandleCountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7667:
	.size	_ZN4node22PerIsolatePlatformData19DecreaseHandleCountEv, .-_ZN4node22PerIsolatePlatformData19DecreaseHandleCountEv
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData17RunForegroundTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.type	_ZN4node22PerIsolatePlatformData17RunForegroundTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, @function
_ZN4node22PerIsolatePlatformData17RunForegroundTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE:
.LFB7723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	72(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L319
	leaq	-96(%rbp), %r14
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L321
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L321
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L321
	movq	271(%rax), %r15
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r15, %r15
	je	.L319
	movq	72(%r12), %rsi
	leaq	-128(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	72(%r12), %rdi
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	leaq	-144(%rbp), %rcx
	call	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi@PLT
	movq	0(%r13), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L317:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L328
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L319:
	movq	0(%r13), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L317
.L328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7723:
	.size	_ZN4node22PerIsolatePlatformData17RunForegroundTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, .-_ZN4node22PerIsolatePlatformData17RunForegroundTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskE
	.type	_ZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskE, @function
_ZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskE:
.LFB7724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	552(%rdi), %rdx
	movq	544(%rdi), %rbx
	movq	%rdx, %rax
	subq	%rbx, %rax
	movq	%rax, %rcx
	sarq	$6, %rax
	sarq	$4, %rcx
	testq	%rax, %rax
	jle	.L330
	salq	$6, %rax
	addq	%rbx, %rax
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L367:
	cmpq	24(%rbx), %rsi
	je	.L363
	cmpq	40(%rbx), %rsi
	je	.L364
	cmpq	56(%rbx), %rsi
	je	.L365
	addq	$64, %rbx
	cmpq	%rbx, %rax
	je	.L366
.L335:
	cmpq	8(%rbx), %rsi
	jne	.L367
.L331:
	cmpq	%rbx, %rdx
	je	.L340
	leaq	16(%rbx), %r14
	movq	%r14, %rax
	cmpq	%r14, %rdx
	je	.L342
	movq	%rdx, %rcx
	subq	%r14, %rcx
	movq	%rcx, %r12
	sarq	$4, %r12
	testq	%rcx, %rcx
	jg	.L346
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L368:
	addq	$16, %r14
.L346:
	movq	24(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	$0, 24(%rbx)
	movq	%rax, 8(%rbx)
	testq	%rdi, %rdi
	je	.L343
	call	*(%rbx)
.L343:
	movq	16(%rbx), %rax
	movq	%rax, (%rbx)
	movq	%r14, %rbx
	subq	$1, %r12
	jne	.L368
	movq	552(%r13), %rax
.L342:
	movq	-8(%rax), %rdi
	leaq	-16(%rax), %rdx
	movq	%rdx, 552(%r13)
	testq	%rdi, %rdi
	je	.L329
	popq	%rbx
	movq	-16(%rax), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	movq	%rdx, %rcx
	subq	%rbx, %rcx
	sarq	$4, %rcx
.L330:
	cmpq	$2, %rcx
	je	.L336
	cmpq	$3, %rcx
	je	.L337
	cmpq	$1, %rcx
	je	.L338
.L340:
	leaq	_ZZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	cmpq	8(%rbx), %rsi
	je	.L331
	addq	$16, %rbx
.L336:
	cmpq	8(%rbx), %rsi
	je	.L331
	addq	$16, %rbx
.L338:
	cmpq	8(%rbx), %rsi
	jne	.L340
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L363:
	addq	$16, %rbx
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L364:
	addq	$32, %rbx
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L365:
	addq	$48, %rbx
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%rdx, %rax
	jmp	.L342
	.cfi_endproc
.LFE7724:
	.size	_ZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskE, .-_ZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskE
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData17RunForegroundTaskEP10uv_timer_s
	.type	_ZN4node22PerIsolatePlatformData17RunForegroundTaskEP10uv_timer_s, @function
_ZN4node22PerIsolatePlatformData17RunForegroundTaskEP10uv_timer_s:
.LFB7728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	168(%r12), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-8(%rdi), %rax
	movq	$0, -8(%rdi)
	movq	%r8, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN4node22PerIsolatePlatformData17RunForegroundTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L370
	movq	(%rdi), %rax
	call	*8(%rax)
.L370:
	movq	168(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L376:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7728:
	.size	_ZN4node22PerIsolatePlatformData17RunForegroundTaskEP10uv_timer_s, .-_ZN4node22PerIsolatePlatformData17RunForegroundTaskEP10uv_timer_s
	.section	.text._ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE,"axG",@progbits,_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	.type	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE, @function
_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE:
.LFB8515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	uv_mutex_lock@PLT
	movq	208(%r12), %rcx
	movq	192(%r12), %rax
	addl	$1, 136(%r12)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L378
	movq	(%rbx), %rdx
	addq	$8, %rax
	movq	$0, (%rbx)
	movq	%rdx, -8(%rax)
	movq	%rax, 192(%r12)
.L379:
	leaq	40(%r12), %rdi
	call	uv_cond_signal@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	movq	216(%r12), %r14
	movq	184(%r12), %rsi
	movabsq	$1152921504606846975, %rdi
	subq	200(%r12), %rax
	movq	%r14, %r13
	sarq	$3, %rax
	subq	%rsi, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	leaq	-1(%rcx), %rdx
	salq	$6, %rdx
	addq	%rax, %rdx
	movq	176(%r12), %rax
	subq	160(%r12), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L388
	movq	144(%r12), %r8
	movq	152(%r12), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L389
.L381:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	%rax, 8(%r14)
	movq	192(%r12), %rax
	movq	$0, (%rbx)
	movq	%rdx, (%rax)
	movq	216(%r12), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 192(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 208(%r12)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L389:
	leaq	2(%rcx), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L390
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rdi, %r14
	ja	.L391
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	184(%r12), %rsi
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r15
	movq	216(%r12), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L386
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L386:
	movq	144(%r12), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 152(%r12)
	movq	%rax, 144(%r12)
.L384:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 184(%r12)
	movq	%r14, 216(%r12)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 168(%r12)
	movq	(%r14), %rax
	movq	%rax, 200(%r12)
	addq	$512, %rax
	movq	%rax, 208(%r12)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L390:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L383
	cmpq	%r14, %rsi
	je	.L384
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L383:
	cmpq	%r14, %rsi
	je	.L384
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L384
.L388:
	leaq	.LC6(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L391:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8515:
	.size	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE, .-_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s,comdat
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s:
.LFB7564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	leaq	-264(%rax), %rbx
	movq	(%rdi), %rax
	movq	32(%rbx), %r13
	movq	%rax, -64(%rbp)
	call	uv_timer_stop@PLT
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	movq	1248(%rbx), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	1240(%rbx), %r14
	divq	%rcx
	leaq	(%r14,%rdx,8), %r15
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L393
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	8(%rdi), %rsi
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L418:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L393
	movq	8(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L393
	movq	%r9, %rdi
.L395:
	cmpq	%rsi, %r12
	jne	.L418
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L419
	testq	%rsi, %rsi
	je	.L397
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L397
	movq	%r10, (%r14,%rdx,8)
	movq	(%rdi), %rsi
.L397:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 1264(%rbx)
.L393:
	movq	%r13, %rdi
	leaq	-64(%rbp), %rsi
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L392
	movq	(%rdi), %rax
	call	*8(%rax)
.L392:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L420
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L403
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L397
	movq	%r10, (%r14,%rdx,8)
	movq	(%r15), %rax
.L396:
	leaq	1256(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L421
.L398:
	movq	$0, (%r15)
	movq	(%rdi), %rsi
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%r10, %rax
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%rsi, 1256(%rbx)
	jmp	.L398
.L420:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7564:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node23WorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.type	_ZN4node23WorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, @function
_ZN4node23WorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd:
.LFB7583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movsd	%xmm0, -40(%rbp)
	movq	224(%rdi), %rbx
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rsi)
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE(%rip), %rdx
	movsd	-40(%rbp), %xmm0
	leaq	40(%rbx), %rdi
	movq	%rdx, (%rax)
	leaq	-32(%rbp), %rsi
	movq	%rbx, 8(%rax)
	movq	%r12, 16(%rax)
	movsd	%xmm0, 24(%rax)
	movq	%rax, -32(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L423
	movq	(%rdi), %rax
	call	*8(%rax)
.L423:
	leaq	1112(%rbx), %rdi
	call	uv_async_send@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L429
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L429:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7583:
	.size	_ZN4node23WorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, .-_ZN4node23WorkerThreadsTaskRunner15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.align 2
	.p2align 4
	.globl	_ZN4node23WorkerThreadsTaskRunner8ShutdownEv
	.type	_ZN4node23WorkerThreadsTaskRunner8ShutdownEv, @function
_ZN4node23WorkerThreadsTaskRunner8ShutdownEv:
.LFB7585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	40(%r12), %rdi
	movb	$1, 140(%r12)
	call	uv_cond_broadcast@PLT
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	224(%r12), %rbx
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskE(%rip), %rcx
	leaq	-32(%rbp), %rsi
	movq	%rcx, (%rax)
	leaq	40(%rbx), %rdi
	movq	%rbx, 8(%rax)
	movq	%rax, -32(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L431
	movq	(%rdi), %rax
	call	*8(%rax)
.L431:
	leaq	1112(%rbx), %rdi
	call	uv_async_send@PLT
	movq	232(%r12), %rdx
	cmpq	%rdx, 240(%r12)
	je	.L430
	xorl	%ebx, %ebx
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L433:
	movq	232(%r12), %rdx
	movq	240(%r12), %rax
	addq	$1, %rbx
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L430
.L434:
	movq	(%rdx,%rbx,8), %rdi
	call	uv_thread_join@PLT
	testl	%eax, %eax
	je	.L433
	leaq	_ZZN4node23WorkerThreadsTaskRunner8ShutdownEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L441
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L441:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7585:
	.size	_ZN4node23WorkerThreadsTaskRunner8ShutdownEv, .-_ZN4node23WorkerThreadsTaskRunner8ShutdownEv
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform8ShutdownEv
	.type	_ZN4node12NodePlatform8ShutdownEv, @function
_ZN4node12NodePlatform8ShutdownEv:
.LFB7721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	subq	$8, %rsp
	movq	112(%rdi), %rdi
	call	_ZN4node23WorkerThreadsTaskRunner8ShutdownEv
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L443
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L449
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	jne	.L463
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L443
.L444:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L451
.L463:
	lock subl	$1, 8(%r13)
	jne	.L451
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L451
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L444
.L443:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 72(%rbx)
	movq	%r14, %rdi
	movq	$0, 64(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L447
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L443
.L449:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L447
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L447
	jmp	.L448
	.cfi_endproc
.LFE7721:
	.size	_ZN4node12NodePlatform8ShutdownEv, .-_ZN4node12NodePlatform8ShutdownEv
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.type	_ZN4node22PerIsolatePlatformData8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, @function
_ZN4node22PerIsolatePlatformData8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE:
.LFB7646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 88(%rdi)
	je	.L464
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rsi
	leaq	96(%rdi), %rdi
	movq	%rax, -32(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L466
	movq	(%rdi), %rax
	call	*8(%rax)
.L466:
	movq	88(%rbx), %rdi
	call	uv_async_send@PLT
.L464:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L472
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L472:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7646:
	.size	_ZN4node22PerIsolatePlatformData8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, .-_ZN4node22PerIsolatePlatformData8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData19PostNonNestableTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.type	_ZN4node22PerIsolatePlatformData19PostNonNestableTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, @function
_ZN4node22PerIsolatePlatformData19PostNonNestableTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE:
.LFB7654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node22PerIsolatePlatformData8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	(%rsi), %rdi
	movq	(%rax), %rax
	movq	$0, (%rsi)
	movq	%rdi, -40(%rbp)
	cmpq	%rdx, %rax
	jne	.L474
	cmpq	$0, 88(%r12)
	je	.L475
	movq	%rdi, -32(%rbp)
	leaq	-32(%rbp), %rsi
	leaq	96(%r12), %rdi
	movq	$0, -40(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L476
	movq	(%rdi), %rax
	call	*8(%rax)
.L476:
	movq	88(%r12), %rdi
	call	uv_async_send@PLT
	movq	-40(%rbp), %rdi
.L475:
	testq	%rdi, %rdi
	je	.L473
	movq	(%rdi), %rax
	call	*8(%rax)
.L473:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-40(%rbp), %rsi
	call	*%rax
	movq	-40(%rbp), %rdi
	jmp	.L475
.L486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7654:
	.size	_ZN4node22PerIsolatePlatformData19PostNonNestableTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, .-_ZN4node22PerIsolatePlatformData19PostNonNestableTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.type	_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, @function
_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE:
.LFB7741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-16(%rbp), %rsi
	movq	%rax, -16(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L487
	movq	(%rdi), %rax
	call	*8(%rax)
.L487:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L494
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L494:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7741:
	.size	_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE, .-_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform25CallDelayedOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.type	_ZN4node12NodePlatform25CallDelayedOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, @function
_ZN4node12NodePlatform25CallDelayedOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd:
.LFB7742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movsd	%xmm0, -40(%rbp)
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	112(%rdi), %rax
	movq	$0, (%rsi)
	movl	$32, %edi
	movq	224(%rax), %rbx
	call	_Znwm@PLT
	leaq	16+_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE(%rip), %rdx
	movsd	-40(%rbp), %xmm0
	leaq	-32(%rbp), %rsi
	movq	%rdx, (%rax)
	leaq	40(%rbx), %rdi
	movq	%rbx, 8(%rax)
	movq	%r12, 16(%rax)
	movsd	%xmm0, 24(%rax)
	movq	%rax, -32(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L496
	movq	(%rdi), %rax
	call	*8(%rax)
.L496:
	leaq	1112(%rbx), %rdi
	call	uv_async_send@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L502
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L502:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7742:
	.size	_ZN4node12NodePlatform25CallDelayedOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd, .-_ZN4node12NodePlatform25CallDelayedOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.section	.text._ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB4186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	56(%rax), %rdx
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, -24(%rbp)
	cmpq	%rcx, %rdx
	jne	.L504
	movq	112(%rdi), %rdi
	leaq	-16(%rbp), %rsi
	movq	$0, -24(%rbp)
	movq	%rax, -16(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	movq	(%rdi), %rax
	call	*8(%rax)
.L506:
	movq	-24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L503
	movq	(%rdi), %rax
	call	*8(%rax)
.L503:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L516
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	leaq	-24(%rbp), %rsi
	call	*%rdx
	jmp	.L506
.L516:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4186:
	.size	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,"axG",@progbits,_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.type	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, @function
_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE:
.LFB4184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	56(%rax), %rdx
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, -24(%rbp)
	cmpq	%rcx, %rdx
	jne	.L518
	movq	112(%rdi), %rdi
	leaq	-16(%rbp), %rsi
	movq	$0, -24(%rbp)
	movq	%rax, -16(%rbp)
	call	_ZN4node9TaskQueueIN2v84TaskEE4PushESt10unique_ptrIS2_St14default_deleteIS2_EE
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L520
	movq	(%rdi), %rax
	call	*8(%rax)
.L520:
	movq	-24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L517
	movq	(%rdi), %rax
	call	*8(%rax)
.L517:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L530
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	leaq	-24(%rbp), %rsi
	call	*%rdx
	jmp	.L520
.L530:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4184:
	.size	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE, .-_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.section	.text._ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED2Ev,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED2Ev
	.type	_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED2Ev, @function
_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED2Ev:
.LFB8662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	48(%rdi), %rax
	movq	40(%rdi), %rdx
	movq	32(%rdi), %rcx
	movq	16(%rdi), %r12
	movq	%rax, -56(%rbp)
	movq	56(%rdi), %rax
	leaq	8(%rdx), %r15
	movq	%rcx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	72(%rdi), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%r15, %rax
	jbe	.L545
	movq	%r12, -104(%rbp)
	movq	%rdi, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L532:
	movq	(%r15), %r13
	leaq	512(%r13), %rbx
	movq	%rbx, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L544:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L535
	movq	176(%r14), %r12
	testq	%r12, %r12
	je	.L537
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L538
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L539:
	cmpl	$1, %eax
	je	.L617
.L537:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L543
	movq	(%rdi), %rax
	call	*8(%rax)
.L543:
	movl	$184, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L535:
	addq	$8, %r13
	cmpq	%r13, -96(%rbp)
	jne	.L544
	addq	$8, %r15
	cmpq	%r15, -64(%rbp)
	ja	.L532
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %rbx
.L545:
	movq	-64(%rbp), %rsi
	cmpq	%rsi, -88(%rbp)
	je	.L618
	cmpq	-72(%rbp), %r12
	je	.L559
	.p2align 4,,10
	.p2align 3
.L546:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L550
	movq	176(%r14), %r13
	testq	%r13, %r13
	je	.L552
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L553
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L554:
	cmpl	$1, %eax
	jne	.L552
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L556
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L557:
	cmpl	$1, %eax
	jne	.L552
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L552:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L558
	movq	(%rdi), %rax
	call	*8(%rax)
.L558:
	movl	$184, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L550:
	addq	$8, %r12
	cmpq	%r12, -72(%rbp)
	jne	.L546
.L559:
	movq	-80(%rbp), %rcx
	movq	%rcx, %r14
	cmpq	%rcx, -56(%rbp)
	je	.L569
	.p2align 4,,10
	.p2align 3
.L547:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L560
	movq	176(%r13), %r12
	testq	%r12, %r12
	je	.L562
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L563
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L564:
	cmpl	$1, %eax
	jne	.L562
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L566
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L567:
	cmpl	$1, %eax
	jne	.L562
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L562:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L568
	movq	(%rdi), %rax
	call	*8(%rax)
.L568:
	movl	$184, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L560:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L547
.L569:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L619
.L548:
	movq	72(%rbx), %rax
	movq	40(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L580
	.p2align 4,,10
	.p2align 3
.L581:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L581
	movq	(%rbx), %rdi
.L580:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L541
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L542:
	cmpl	$1, %eax
	jne	.L537
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L541:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L538:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L553:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L563:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L618:
	movq	-56(%rbp), %rax
	cmpq	%rax, %r12
	je	.L569
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L579:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L570
	movq	176(%r13), %r14
	testq	%r14, %r14
	je	.L572
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L573
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L574:
	cmpl	$1, %eax
	jne	.L572
	movq	(%r14), %rax
	movq	%rdx, -56(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L576
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L577:
	cmpl	$1, %eax
	je	.L620
	.p2align 4,,10
	.p2align 3
.L572:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L578
	movq	(%rdi), %rax
	call	*8(%rax)
.L578:
	movl	$184, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L570:
	addq	$8, %r12
	cmpq	%r12, %r15
	jne	.L579
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L548
.L619:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L556:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L566:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L576:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L620:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L572
	.cfi_endproc
.LFE8662:
	.size	_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED2Ev, .-_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED2Ev
	.weak	_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED1Ev
	.set	_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED1Ev,_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED2Ev
	.section	.rodata._ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB9282:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L635
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L631
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L636
.L623:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L630:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L637
	testq	%r13, %r13
	jg	.L626
	testq	%r9, %r9
	jne	.L629
.L627:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L626
.L629:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L636:
	testq	%rsi, %rsi
	jne	.L624
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L626:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L627
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$8, %r14d
	jmp	.L623
.L635:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L624:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L623
	.cfi_endproc
.LFE9282:
	.size	_ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv:
.LFB7545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	%rdi, -104(%rbp)
	movq	1256(%r15), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rbx, %rbx
	je	.L644
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	leaq	-88(%rbp), %r12
	movq	%r13, %rsi
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L681:
	movq	(%rbx), %rbx
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, -72(%rbp)
	testq	%rbx, %rbx
	je	.L680
.L641:
	movq	-64(%rbp), %rdx
.L645:
	movq	8(%rbx), %rax
	movq	%rax, -88(%rbp)
	cmpq	%rsi, %rdx
	jne	.L681
	leaq	-80(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIP10uv_timer_sSaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	movq	(%rbx), %rbx
	movq	-72(%rbp), %rsi
	testq	%rbx, %rbx
	jne	.L641
	.p2align 4,,10
	.p2align 3
.L680:
	movq	-104(%rbp), %rcx
	movq	-80(%rbp), %rax
	movq	8(%rcx), %r15
	cmpq	%rsi, %rax
	je	.L644
	movq	%rsi, -120(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L655:
	movq	(%rbx), %r12
	movq	%r12, %rdi
	movq	(%r12), %r14
	call	uv_timer_stop@PLT
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler13TakeTimerTaskEP10uv_timer_sENUlP11uv_handle_sE_4_FUNES5_(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	movq	1248(%r15), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	1240(%r15), %r13
	divq	%rcx
	leaq	0(%r13,%rdx,8), %rax
	movq	%rdx, %r11
	movq	(%rax), %r9
	movq	%rax, -112(%rbp)
	testq	%r9, %r9
	je	.L646
	movq	(%r9), %rdi
	movq	%r9, %r10
	movq	8(%rdi), %rsi
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L682:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L646
	movq	8(%r8), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L646
	movq	%r8, %rdi
.L648:
	cmpq	%rsi, %r12
	jne	.L682
	movq	(%rdi), %rsi
	cmpq	%r10, %r9
	je	.L683
	testq	%rsi, %rsi
	je	.L650
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L650
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L650:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 1264(%r15)
.L646:
	testq	%r14, %r14
	je	.L652
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L652:
	movq	-104(%rbp), %rax
	addq	$8, %rbx
	movq	8(%rax), %r15
	cmpq	%rbx, -120(%rbp)
	jne	.L655
.L644:
	leaq	1112(%r15), %rdi
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEvENUlP11uv_handle_sE_4_FUNES4_(%rip), %rsi
	call	uv_close@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L638
	call	_ZdlPv@PLT
.L638:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L684
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L660
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L650
	movq	-112(%rbp), %rax
	movq	%r10, 0(%r13,%rdx,8)
	leaq	1256(%r15), %rdx
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L685
.L651:
	movq	-112(%rbp), %rax
	movq	$0, (%rax)
	movq	(%rdi), %rsi
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%r10, %rax
	leaq	1256(%r15), %rdx
	cmpq	%rdx, %rax
	jne	.L651
.L685:
	movq	%rsi, 1256(%r15)
	jmp	.L651
.L684:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7545:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv
	.section	.text._ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB9395:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L700
	movq	%rdx, %r13
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L696
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L701
.L688:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L695:
	movq	8(%r13), %rcx
	movq	0(%r13), %rsi
	leaq	(%rbx,%rdx), %rax
	subq	%r8, %r9
	leaq	16(%rbx,%rdx), %r10
	movq	%r9, %r13
	movq	%rsi, (%rax)
	movq	%rcx, 8(%rax)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L702
	testq	%r9, %r9
	jg	.L691
	testq	%r15, %r15
	jne	.L694
.L692:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L691
.L694:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L701:
	testq	%rsi, %rsi
	jne	.L689
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L691:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L692
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L696:
	movl	$16, %r14d
	jmp	.L688
.L700:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L689:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$4, %r14
	jmp	.L688
	.cfi_endproc
.LFE9395:
	.size	_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform26AddIsolateFinishedCallbackEPN2v87IsolateEPFvPvES4_
	.type	_ZN4node12NodePlatform26AddIsolateFinishedCallbackEPN2v87IsolateEPFvPvES4_, @function
_ZN4node12NodePlatform26AddIsolateFinishedCallbackEPN2v87IsolateEPFvPvES4_:
.LFB7720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	56(%r12), %rsi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	48(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L704
	movq	(%rax), %rdi
	movq	%rdx, %rcx
	movq	8(%rdi), %r8
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L721:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L704
	movq	8(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %rcx
	jne	.L704
.L706:
	cmpq	%rbx, %r8
	jne	.L721
	movq	16(%rdi), %rdi
	movq	%r14, -80(%rbp)
	movq	%r13, -72(%rbp)
	movq	32(%rdi), %rsi
	cmpq	40(%rdi), %rsi
	je	.L722
	movq	%r14, (%rsi)
	addq	$16, %rsi
	movq	%r13, -8(%rsi)
	movq	%rsi, 32(%rdi)
.L707:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L723
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L704:
	.cfi_restore_state
	movq	16, %rax
	ud2
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	-80(%rbp), %rdx
	addq	$24, %rdi
	call	_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L707
.L723:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7720:
	.size	_ZN4node12NodePlatform26AddIsolateFinishedCallbackEPN2v87IsolateEPFvPvES4_, .-_ZN4node12NodePlatform26AddIsolateFinishedCallbackEPN2v87IsolateEPFvPvES4_
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData19AddShutdownCallbackEPFvPvES1_
	.type	_ZN4node22PerIsolatePlatformData19AddShutdownCallbackEPFvPvES1_, @function
_ZN4node22PerIsolatePlatformData19AddShutdownCallbackEPFvPvES1_:
.LFB7660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -32(%rbp)
	movq	32(%rdi), %rsi
	movq	%rdx, -24(%rbp)
	cmpq	40(%rdi), %rsi
	je	.L725
	movq	%rax, (%rsi)
	addq	$16, %rsi
	movq	%rdx, -8(%rsi)
	movq	%rsi, 32(%rdi)
.L724:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L729
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	addq	$24, %rdi
	call	_ZNSt6vectorIN4node22PerIsolatePlatformData16ShutdownCallbackESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L724
.L729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7660:
	.size	_ZN4node22PerIsolatePlatformData19AddShutdownCallbackEPFvPvES1_, .-_ZN4node22PerIsolatePlatformData19AddShutdownCallbackEPFvPvES1_
	.section	.text._ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.type	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, @function
_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_:
.LFB9792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L731
	.p2align 4,,10
	.p2align 3
.L736:
	movq	0(%r13), %rbx
	leaq	512(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L735:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L732
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L735
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L736
.L753:
	movq	24(%r15), %rdx
.L731:
	movq	(%r15), %rbx
	cmpq	%rax, %rdx
	je	.L737
	movq	16(%r15), %r12
	cmpq	%r12, %rbx
	je	.L742
	.p2align 4,,10
	.p2align 3
.L738:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L741
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L738
.L742:
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	je	.L730
	.p2align 4,,10
	.p2align 3
.L740:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L744
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L740
.L730:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L735
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L736
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L744:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L740
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L738
	jmp	.L742
.L737:
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L730
.L748:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L746
.L754:
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r12
	je	.L730
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L754
.L746:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L748
	jmp	.L730
	.cfi_endproc
.LFE9792:
	.size	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_, .-_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	256(%rdi), %rbx
	movq	248(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, -136(%rbp)
	cmpq	%r12, %rbx
	je	.L756
	.p2align 4,,10
	.p2align 3
.L760:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L757
	movl	$8, %esi
	addq	$8, %r12
	call	_ZdlPvm@PLT
	cmpq	%r12, %rbx
	jne	.L760
.L758:
	movq	248(%r15), %r12
.L756:
	testq	%r12, %r12
	je	.L761
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L761:
	movq	240(%r15), %r14
	testq	%r14, %r14
	je	.L794
	movq	1256(%r14), %rbx
	testq	%rbx, %rbx
	je	.L766
	.p2align 4,,10
	.p2align 3
.L763:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L763
.L766:
	movq	1248(%r14), %rax
	movq	1240(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	1240(%r14), %rdi
	leaq	1288(%r14), %rax
	movq	$0, 1264(%r14)
	movq	$0, 1256(%r14)
	cmpq	%rax, %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movdqu	248(%r14), %xmm2
	leaq	-128(%rbp), %r13
	movdqu	200(%r14), %xmm1
	leaq	-96(%rbp), %r12
	movdqu	216(%r14), %xmm0
	leaq	40(%r14), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movdqu	232(%r14), %xmm7
	leaq	184(%r14), %rdi
	movaps	%xmm2, -112(%rbp)
	movq	%rax, -152(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	184(%r14), %rdi
	testq	%rdi, %rdi
	je	.L767
	movq	256(%r14), %rax
	movq	224(%r14), %rbx
	addq	$8, %rax
	movq	%rax, -144(%rbp)
	cmpq	%rbx, %rax
	jbe	.L768
	.p2align 4,,10
	.p2align 3
.L769:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, -144(%rbp)
	ja	.L769
	movq	184(%r14), %rdi
.L768:
	call	_ZdlPv@PLT
.L767:
	leaq	128(%r14), %rdi
	call	uv_cond_destroy@PLT
	leaq	80(%r14), %rdi
	call	uv_cond_destroy@PLT
	movq	-152(%rbp), %rdi
	call	uv_mutex_destroy@PLT
	movl	$1296, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L762:
	movdqu	208(%r15), %xmm3
	movq	%r13, %rdx
	movq	%r12, %rsi
	movdqu	224(%r15), %xmm4
	movdqu	176(%r15), %xmm5
	leaq	160(%r15), %rdi
	movdqu	192(%r15), %xmm6
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	160(%r15), %rdi
	testq	%rdi, %rdi
	je	.L770
	movq	232(%r15), %rax
	movq	200(%r15), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L771
	.p2align 4,,10
	.p2align 3
.L772:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L772
	movq	160(%r15), %rdi
.L771:
	call	_ZdlPv@PLT
.L770:
	leaq	104(%r15), %rdi
	call	uv_cond_destroy@PLT
	leaq	56(%r15), %rdi
	call	uv_cond_destroy@PLT
	movq	-136(%rbp), %rdi
	call	uv_mutex_destroy@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L795
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L760
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	-128(%rbp), %r13
	leaq	-96(%rbp), %r12
	jmp	.L762
.L795:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11624:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB9967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r8
	movabsq	$1152921504606846975, %rdi
	movq	%r14, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L820
	movq	%rsi, %rbx
	movq	%rsi, %r9
	subq	%r8, %rsi
	testq	%rax, %rax
	je	.L811
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r10
	cmpq	%r10, %rax
	jbe	.L821
.L798:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	%rax, %r13
	addq	%rax, %r15
	movq	-88(%rbp), %rdx
	leaq	8(%rax), %r12
.L810:
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	movq	%rax, 0(%r13,%rsi)
	cmpq	%r8, %rbx
	je	.L800
	movq	%r13, %rax
	movq	%r8, %r12
	.p2align 4,,10
	.p2align 3
.L804:
	movq	(%r12), %rdx
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L801
	movl	$8, %esi
	movq	%rcx, -80(%rbp)
	addq	$8, %r12
	movq	%r9, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %rcx
	addq	$8, %rax
	cmpq	%r12, %rbx
	jne	.L804
.L802:
	movq	%rbx, %rax
	subq	%r8, %rax
	leaq	8(%r13,%rax), %r12
.L800:
	cmpq	%r14, %rbx
	je	.L805
	subq	%rbx, %r14
	leaq	-8(%r14), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	addq	$1, %rdx
	testq	%rsi, %rsi
	je	.L813
	movq	%rdx, %rdi
	xorl	%eax, %eax
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L807:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%r12,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L807
	movq	%rdx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rax
	leaq	(%rbx,%rax), %r9
	addq	%r12, %rax
	cmpq	%rdi, %rdx
	je	.L808
.L806:
	movq	(%r9), %rdx
	movq	%rdx, (%rax)
.L808:
	leaq	8(%r12,%rsi), %r12
.L805:
	testq	%r8, %r8
	je	.L809
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
.L809:
	movq	%r13, %xmm0
	movq	%r12, %xmm2
	movq	%r15, 16(%rcx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rcx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	addq	$8, %r12
	addq	$8, %rax
	cmpq	%r12, %rbx
	jne	.L804
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L821:
	testq	%r10, %r10
	jne	.L799
	movl	$8, %r12d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L811:
	movl	$8, %r15d
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%r12, %rax
	jmp	.L806
.L820:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L799:
	cmpq	%rdi, %r10
	cmovbe	%r10, %rdi
	movq	%rdi, %r15
	salq	$3, %r15
	jmp	.L798
	.cfi_endproc
.LFE9967:
	.size	_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB10100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L823
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L833
.L849:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L834:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L823:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L847
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L848
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L826:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L828
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L830:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L831:
	testq	%rsi, %rsi
	je	.L828
.L829:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L830
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L836
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L829
	.p2align 4,,10
	.p2align 3
.L828:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L832
	call	_ZdlPv@PLT
.L832:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L849
.L833:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L835
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L835:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%rdx, %rdi
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L847:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L826
.L848:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10100:
	.size	_ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	.type	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_, @function
_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_:
.LFB9437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L851
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L863:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L851
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L851
.L853:
	cmpq	%r8, %r13
	jne	.L863
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	movq	$0, 24(%rcx)
	call	_ZNSt10_HashtableIPN2v87IsolateESt4pairIKS2_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9437:
	.size	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_, .-_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform10ForIsolateEPN2v87IsolateE
	.type	_ZN4node12NodePlatform10ForIsolateEPN2v87IsolateE, @function
_ZN4node12NodePlatform10ForIsolateEPN2v87IsolateE:
.LFB7743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdx, -40(%rbp)
	call	uv_mutex_lock@PLT
	leaq	-40(%rbp), %rsi
	leaq	48(%rbx), %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rdx, (%r12)
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L865
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L866
	lock addl	$1, 8(%rax)
	movq	(%r12), %rdx
.L865:
	testq	%rdx, %rdx
	je	.L872
.L867:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	addl	$1, 8(%rax)
	testq	%rdx, %rdx
	jne	.L867
.L872:
	leaq	_ZZN4node12NodePlatform10ForIsolateEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7743:
	.size	_ZN4node12NodePlatform10ForIsolateEPN2v87IsolateE, .-_ZN4node12NodePlatform10ForIsolateEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform23GetForegroundTaskRunnerEPN2v87IsolateE
	.type	_ZN4node12NodePlatform23GetForegroundTaskRunnerEPN2v87IsolateE, @function
_ZN4node12NodePlatform23GetForegroundTaskRunnerEPN2v87IsolateE:
.LFB7746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -48(%rbp)
	call	uv_mutex_lock@PLT
	leaq	-48(%rbp), %rsi
	leaq	48(%rbx), %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L874
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L875
	lock addl	$1, 8(%rax)
.L874:
	testq	%rdx, %rdx
	je	.L882
.L876:
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r13, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	uv_mutex_unlock@PLT
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, (%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L883
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L875:
	.cfi_restore_state
	addl	$1, 8(%rax)
	testq	%rdx, %rdx
	jne	.L876
.L882:
	leaq	_ZZN4node12NodePlatform10ForIsolateEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7746:
	.size	_ZN4node12NodePlatform23GetForegroundTaskRunnerEPN2v87IsolateE, .-_ZN4node12NodePlatform23GetForegroundTaskRunnerEPN2v87IsolateE
	.section	.text._ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm
	.type	_ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm, @function
_ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm:
.LFB10511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L885
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L895
.L911:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L896:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L909
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L910
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L888:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L890
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L892:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L893:
	testq	%rsi, %rsi
	je	.L890
.L891:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L892
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L898
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L891
	.p2align 4,,10
	.p2align 3
.L890:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L894
	call	_ZdlPv@PLT
.L894:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L911
.L895:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L897
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L897:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%rdx, %rdi
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L909:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L888
.L910:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10511:
	.size	_ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm, .-_ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm
	.section	.text._ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv,"axG",@progbits,_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv
	.type	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv, @function
_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv:
.LFB7561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	.LC9(%rip), %xmm0
	mulsd	24(%rdi), %xmm0
	call	llround@PLT
	movl	$152, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	movl	$19, %ecx
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r12, %rsi
	rep stosq
	movq	8(%rbx), %rax
	leaq	264(%rax), %rdi
	call	uv_timer_init@PLT
	testl	%eax, %eax
	jne	.L926
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	leaq	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler7RunTaskEP10uv_timer_s(%rip), %rsi
	movq	$0, 16(%rbx)
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	uv_timer_start@PLT
	testl	%eax, %eax
	jne	.L927
	movq	8(%rbx), %rbx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	1248(%rbx), %rdi
	divq	%rdi
	movq	1240(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L915
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L928:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L915
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L915
.L917:
	cmpq	%r12, %rsi
	jne	.L928
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	1240(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIP10uv_timer_sS1_SaIS1_ENSt8__detail9_IdentityESt8equal_toIS1_ESt4hashIS1_ENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS3_10_Hash_nodeIS1_Lb0EEEm
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L927:
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7561:
	.size	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv, .-_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv
	.section	.text._ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm:
.LFB10665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	shrq	$6, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	1(%rdi), %rbx
	addq	$3, %rdi
	subq	$8, %rsp
	cmpq	$8, %rdi
	ja	.L930
	movq	$8, 8(%r13)
	movl	$64, %edi
.L931:
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	subq	%rbx, %rdx
	shrq	%rdx
	leaq	(%rax,%rdx,8), %r15
	leaq	(%r15,%rbx,8), %r12
	movq	%r15, %rbx
	cmpq	%r12, %r15
	jnb	.L933
	.p2align 4,,10
	.p2align 3
.L932:
	movl	$512, %edi
	addq	$8, %rbx
	call	_Znwm@PLT
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r12
	ja	.L932
.L933:
	movq	(%r15), %rdx
	andl	$63, %r14d
	subq	$8, %r12
	movq	%r15, 40(%r13)
	leaq	512(%rdx), %rax
	movq	%rdx, %xmm0
	movq	%rax, 32(%r13)
	movq	(%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, %xmm2
	movups	%xmm0, 16(%r13)
	leaq	(%rax,%r14,8), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L930:
	.cfi_restore_state
	movq	%rdi, 8(%r13)
	salq	$3, %rdi
	jmp	.L931
	.cfi_endproc
.LFE10665:
	.size	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv
	.type	_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv, @function
_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv:
.LFB7731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	320(%rdi), %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L939:
	movq	496(%r13), %rcx
	movq	(%rax), %rbx
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L941
	addq	$8, %rax
	movq	%rax, 480(%r13)
.L942:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	testq	%rbx, %rbx
	je	.L940
	movsd	.LC9(%rip), %xmm0
	mulsd	160(%rbx), %xmm0
	leaq	8(%rbx), %r14
	call	llround@PLT
	movq	%rbx, 8(%rbx)
	movq	80(%r13), %rdi
	movq	%r14, %rsi
	movq	%rax, %r15
	call	uv_timer_init@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN4node22PerIsolatePlatformData17RunForegroundTaskEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
	movq	%r14, %rdi
	call	uv_unref@PLT
	addl	$1, 64(%r13)
	movq	552(%r13), %r15
	cmpq	560(%r13), %r15
	je	.L943
	leaq	_ZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENUlPNS_11DelayedTaskEE_4_FUNES2_(%rip), %rax
	leaq	16(%r15), %rdx
	movq	%rbx, 8(%r15)
	movq	%rax, (%r15)
	movq	%rdx, 552(%r13)
.L944:
	movl	$1, %r14d
.L954:
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	480(%r13), %rax
	cmpq	%rax, 512(%r13)
	jne	.L939
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
.L940:
	leaq	96(%r13), %r12
	leaq	-144(%rbp), %r15
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r15, %rdi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	movq	-104(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	-112(%rbp), %xmm0
	movq	-128(%rbp), %xmm1
	leaq	-176(%rbp), %r12
	movq	%rax, %xmm7
	movq	-72(%rbp), %rax
	movq	%rdx, %xmm6
	movq	-88(%rbp), %rdx
	punpcklqdq	%xmm7, %xmm0
	punpcklqdq	%xmm6, %xmm1
	movdqu	256(%r13), %xmm4
	movdqu	272(%r13), %xmm5
	movq	%rax, %xmm7
	movq	-144(%rbp), %rax
	movq	%rdx, %xmm6
	movq	240(%r13), %rdx
	movups	%xmm1, 256(%r13)
	movq	-96(%rbp), %xmm1
	movq	%rax, 240(%r13)
	movq	-136(%rbp), %rax
	movups	%xmm0, 272(%r13)
	movq	-80(%rbp), %xmm0
	punpcklqdq	%xmm6, %xmm1
	movq	%rdx, -144(%rbp)
	movq	248(%r13), %rdx
	movq	%rax, 248(%r13)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm4, -128(%rbp)
	movdqu	288(%r13), %xmm4
	movaps	%xmm5, -112(%rbp)
	movdqu	304(%r13), %xmm5
	movups	%xmm1, 288(%r13)
	movups	%xmm0, 304(%r13)
	movq	%rdx, -136(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	uv_mutex_unlock@PLT
	movq	-128(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	je	.L955
	.p2align 4,,10
	.p2align 3
.L960:
	movq	-112(%rbp), %rcx
	movq	(%rax), %rbx
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L956
.L985:
	addq	$8, %rax
	movq	%rax, -128(%rbp)
.L957:
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbx, -176(%rbp)
	call	_ZN4node22PerIsolatePlatformData17RunForegroundTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L958
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-128(%rbp), %rax
	cmpq	-96(%rbp), %rax
	jne	.L960
.L982:
	movl	$1, %r14d
.L955:
	movq	-88(%rbp), %rdx
	movq	%rax, -208(%rbp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rax, -176(%rbp)
	movq	-120(%rbp), %rax
	movq	%rdx, -200(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movq	-112(%rbp), %rax
	movq	%rdx, -192(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	-104(%rbp), %rax
	movq	%rdx, -184(%rbp)
	leaq	-208(%rbp), %rdx
	movq	%rax, -152(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L938
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L962
	.p2align 4,,10
	.p2align 3
.L963:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L963
	movq	-144(%rbp), %rdi
.L962:
	call	_ZdlPv@PLT
.L938:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L984
	addq	$200, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	.cfi_restore_state
	movq	-128(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	je	.L982
	movq	-112(%rbp), %rcx
	movq	(%rax), %rbx
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L985
.L956:
	movq	-120(%rbp), %rdi
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm2
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L943:
	movq	544(%r13), %rax
	movq	%r15, %rcx
	movabsq	$576460752303423487, %rsi
	subq	%rax, %rcx
	movq	%rax, -224(%rbp)
	movq	%rcx, %rax
	sarq	$4, %rax
	cmpq	%rsi, %rax
	je	.L986
	testq	%rax, %rax
	je	.L965
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L987
.L946:
	movq	%r14, %rdi
	movq	%rcx, -240(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %rcx
	addq	%rax, %r14
	movq	%rax, -216(%rbp)
	addq	$16, %rax
	movq	%r14, -232(%rbp)
.L947:
	movq	-216(%rbp), %r14
	leaq	_ZZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEvENUlPNS_11DelayedTaskEE_4_FUNES2_(%rip), %rsi
	addq	%r14, %rcx
	movq	%rbx, 8(%rcx)
	movq	-224(%rbp), %rbx
	movq	%rsi, (%rcx)
	cmpq	%rbx, %r15
	je	.L948
	.p2align 4,,10
	.p2align 3
.L952:
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	$0, 8(%rbx)
	movq	%rdi, (%r14)
	movq	%rsi, 8(%r14)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L949
	call	*(%rbx)
	addq	$16, %rbx
	addq	$16, %r14
	cmpq	%rbx, %r15
	jne	.L952
.L950:
	movq	-216(%rbp), %rax
	movq	%r15, %rdx
	subq	-224(%rbp), %rdx
	leaq	16(%rax,%rdx), %rax
.L948:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L953
	movq	%rax, -224(%rbp)
	call	_ZdlPv@PLT
	movq	-224(%rbp), %rax
.L953:
	movq	-216(%rbp), %xmm0
	movq	%rax, %xmm3
	movq	-232(%rbp), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 560(%r13)
	movups	%xmm0, 544(%r13)
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L987:
	testq	%rsi, %rsi
	jne	.L988
	movq	$0, -232(%rbp)
	movl	$16, %eax
	movq	$0, -216(%rbp)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L949:
	addq	$16, %rbx
	addq	$16, %r14
	cmpq	%rbx, %r15
	jne	.L952
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L941:
	movq	488(%r13), %rdi
	call	_ZdlPv@PLT
	movq	504(%r13), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm3
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 480(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 496(%r13)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L965:
	movl	$16, %r14d
	jmp	.L946
.L986:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L984:
	call	__stack_chk_fail@PLT
.L988:
	movabsq	$576460752303423487, %rax
	cmpq	%rax, %rsi
	cmovbe	%rsi, %rax
	salq	$4, %rax
	movq	%rax, %r14
	jmp	.L946
	.cfi_endproc
.LFE7731:
	.size	_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv, .-_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData10FlushTasksEP10uv_async_s
	.type	_ZN4node22PerIsolatePlatformData10FlushTasksEP10uv_async_s, @function
_ZN4node22PerIsolatePlatformData10FlushTasksEP10uv_async_s:
.LFB7642:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv
	.cfi_endproc
.LFE7642:
	.size	_ZN4node22PerIsolatePlatformData10FlushTasksEP10uv_async_s, .-_ZN4node22PerIsolatePlatformData10FlushTasksEP10uv_async_s
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform10DrainTasksEPN2v87IsolateE
	.type	_ZN4node12NodePlatform10DrainTasksEPN2v87IsolateE, @function
_ZN4node12NodePlatform10DrainTasksEPN2v87IsolateE:
.LFB7730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -64(%rbp)
	call	uv_mutex_lock@PLT
	leaq	-64(%rbp), %rsi
	leaq	48(%r13), %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	movq	8(%rax), %r15
	movq	(%rax), %r14
	testq	%r15, %r15
	je	.L991
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L992
	lock addl	$1, 8(%r15)
.L991:
	testq	%r14, %r14
	je	.L1014
.L993:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L995:
	movq	112(%r13), %r12
	movq	%r12, %rdi
	leaq	88(%r12), %rbx
	call	uv_mutex_lock@PLT
	movl	136(%r12), %edx
	testl	%edx, %edx
	jle	.L997
	.p2align 4,,10
	.p2align 3
.L998:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	uv_cond_wait@PLT
	movl	136(%r12), %eax
	testl	%eax, %eax
	jg	.L998
.L997:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv
	testb	%al, %al
	jne	.L995
	testq	%r15, %r15
	je	.L990
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1001
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L1015
	.p2align 4,,10
	.p2align 3
.L990:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L992:
	.cfi_restore_state
	addl	$1, 8(%r15)
	testq	%r14, %r14
	jne	.L993
.L1014:
	leaq	_ZZN4node12NodePlatform10ForIsolateEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1001:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L990
.L1015:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1004
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1005:
	cmpl	$1, %eax
	jne	.L990
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L990
.L1004:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1005
.L1016:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7730:
	.size	_ZN4node12NodePlatform10DrainTasksEPN2v87IsolateE, .-_ZN4node12NodePlatform10DrainTasksEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform20FlushForegroundTasksEPN2v87IsolateE
	.type	_ZN4node12NodePlatform20FlushForegroundTasksEPN2v87IsolateE, @function
_ZN4node12NodePlatform20FlushForegroundTasksEPN2v87IsolateE:
.LFB7744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -64(%rbp)
	call	uv_mutex_lock@PLT
	leaq	-64(%rbp), %rsi
	leaq	48(%rbx), %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	movq	8(%rax), %r12
	movq	(%rax), %r13
	testq	%r12, %r12
	je	.L1018
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	leaq	8(%r12), %r15
	testq	%rbx, %rbx
	je	.L1019
	lock addl	$1, (%r15)
	testq	%r13, %r13
	je	.L1027
.L1038:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r13, %rdi
	call	_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv
	testq	%rbx, %rbx
	je	.L1041
	movl	$-1, %edx
	lock xaddl	%edx, (%r15)
	cmpl	$1, %edx
	je	.L1042
.L1017:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1043
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1019:
	.cfi_restore_state
	addl	$1, 8(%r12)
	testq	%r13, %r13
	jne	.L1038
.L1027:
	leaq	_ZZN4node12NodePlatform10ForIsolateEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1018:
	testq	%r13, %r13
	je	.L1027
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r13, %rdi
	call	_ZN4node22PerIsolatePlatformData28FlushForegroundTasksInternalEv
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1041:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %edx
	jne	.L1017
.L1042:
	movq	(%r12), %rdx
	movb	%al, -65(%rbp)
	movq	%r12, %rdi
	call	*16(%rdx)
	testq	%rbx, %rbx
	movzbl	-65(%rbp), %eax
	je	.L1023
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r12)
.L1024:
	cmpl	$1, %edx
	jne	.L1017
	movq	(%r12), %rdx
	movb	%al, -65(%rbp)
	movq	%r12, %rdi
	call	*24(%rdx)
	movzbl	-65(%rbp), %eax
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1023:
	movl	12(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r12)
	jmp	.L1024
.L1043:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7744:
	.size	_ZN4node12NodePlatform20FlushForegroundTasksEPN2v87IsolateE, .-_ZN4node12NodePlatform20FlushForegroundTasksEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZN4node23WorkerThreadsTaskRunnerC2Ei
	.type	_ZN4node23WorkerThreadsTaskRunnerC2Ei, @function
_ZN4node23WorkerThreadsTaskRunnerC2Ei:
.LFB7580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movl	%esi, -244(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1048
	leaq	40(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1047
	leaq	88(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1047
	movl	$0, 136(%rbx)
	pxor	%xmm1, %xmm1
	xorl	%esi, %esi
	leaq	144(%rbx), %rdi
	movb	$0, 140(%rbx)
	leaq	-160(%rbp), %r13
	movq	$0, 144(%rbx)
	movq	$0, 152(%rbx)
	movups	%xmm1, 160(%rbx)
	movups	%xmm1, 176(%rbx)
	movups	%xmm1, 192(%rbx)
	movups	%xmm1, 208(%rbx)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	pxor	%xmm1, %xmm1
	movq	%r13, %rdi
	movups	%xmm1, 224(%rbx)
	movups	%xmm1, 240(%rbx)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1048
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1047
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movl	-244(%rbp), %eax
	movl	$1296, %edi
	movl	%eax, -228(%rbp)
	call	_Znwm@PLT
	movq	%rbx, 32(%rax)
	leaq	40(%rax), %rdi
	movq	%rax, %r15
	movq	%rax, -256(%rbp)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1048
	movq	-256(%rbp), %rdx
	leaq	80(%rdx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1047
	movq	-256(%rbp), %rdx
	leaq	128(%rdx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1047
	movq	-256(%rbp), %rdx
	pxor	%xmm1, %xmm1
	xorl	%esi, %esi
	movb	$0, 180(%rdx)
	leaq	184(%rdx), %rdi
	movl	$0, 176(%rdx)
	movq	$0, 184(%rdx)
	movq	$0, 192(%rdx)
	movups	%xmm1, 200(%rdx)
	movups	%xmm1, 216(%rdx)
	movups	%xmm1, 232(%rdx)
	movups	%xmm1, 248(%rdx)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	movq	-256(%rbp), %rdx
	movq	224(%rbx), %r12
	leaq	1288(%rdx), %rax
	movq	$1, 1248(%rdx)
	movq	%rax, 1240(%rdx)
	movq	$0, 1256(%rdx)
	movq	$0, 1264(%rdx)
	movl	$0x3f800000, 1272(%rdx)
	movq	$0, 1280(%rdx)
	movq	$0, 1288(%rdx)
	movq	%rdx, 224(%rbx)
	testq	%r12, %r12
	je	.L1049
	movq	1256(%r12), %rax
	testq	%rax, %rax
	je	.L1053
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rax, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %rax
	testq	%rax, %rax
	jne	.L1050
.L1053:
	movq	1248(%r12), %rax
	movq	1240(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	1240(%r12), %rdi
	leaq	1288(%r12), %rax
	movq	$0, 1264(%r12)
	movq	$0, 1256(%r12)
	cmpq	%rax, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movdqu	232(%r12), %xmm2
	leaq	40(%r12), %rax
	movdqu	248(%r12), %xmm3
	leaq	184(%r12), %rdi
	leaq	-224(%rbp), %rdx
	leaq	-192(%rbp), %rsi
	movdqu	216(%r12), %xmm0
	movdqu	200(%r12), %xmm4
	movq	%rax, -264(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1054
	movq	256(%r12), %rax
	addq	$8, %rax
	movq	%rax, %rcx
	movq	%rax, %r15
	movq	224(%r12), %rax
	cmpq	%rax, %rcx
	jbe	.L1055
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	(%rax), %rdi
	movq	%rax, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %rax
	addq	$8, %rax
	cmpq	%rax, %r15
	ja	.L1056
	movq	184(%r12), %rdi
.L1055:
	call	_ZdlPv@PLT
.L1054:
	leaq	128(%r12), %rdi
	call	uv_cond_destroy@PLT
	leaq	80(%r12), %rdi
	call	uv_cond_destroy@PLT
	movq	-264(%rbp), %rdi
	call	uv_mutex_destroy@PLT
	movl	$1296, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	224(%rbx), %rax
	movq	%rax, %r15
.L1049:
	movl	$8, %edi
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	$0, (%rax)
	movq	%rax, -192(%rbp)
	call	uv_sem_init@PLT
	movq	-192(%rbp), %rdi
	movq	%r15, %rdx
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvENUlPvE_4_FUNES2_(%rip), %rsi
	call	uv_thread_create@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L1096
	leaq	232(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, -264(%rbp)
	call	uv_sem_wait@PLT
	movq	%r15, %rdi
	call	uv_sem_destroy@PLT
	movq	240(%rbx), %rsi
	cmpq	248(%rbx), %rsi
	je	.L1058
	movq	-192(%rbp), %rax
	addq	$8, %rsi
	movq	$0, -192(%rbp)
	movq	%rax, -8(%rsi)
	movq	%rsi, 240(%rbx)
.L1059:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1060
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L1060:
	leaq	-228(%rbp), %rax
	movl	-244(%rbp), %ecx
	movq	%rax, -256(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -272(%rbp)
	testl	%ecx, %ecx
	jle	.L1062
	.p2align 4,,10
	.p2align 3
.L1061:
	movl	$40, %edi
	call	_Znwm@PLT
	movl	$8, %edi
	movq	%rbx, (%rax)
	movq	%rax, %r15
	movq	%r13, 8(%rax)
	movq	%r14, 16(%rax)
	movq	-256(%rbp), %rax
	movl	%r12d, 32(%r15)
	movq	%rax, 24(%r15)
	call	_Znwm@PLT
	movq	%r15, %rdx
	leaq	_ZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPv(%rip), %rsi
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	uv_thread_create@PLT
	testl	%eax, %eax
	jne	.L1063
	movq	240(%rbx), %rsi
	cmpq	248(%rbx), %rsi
	je	.L1064
	movq	-192(%rbp), %rax
	addq	$8, %rsi
	movq	$0, -192(%rbp)
	movq	%rax, -8(%rsi)
	movq	%rsi, 240(%rbx)
.L1065:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1066
	movl	$8, %esi
	addl	$1, %r12d
	call	_ZdlPvm@PLT
	cmpl	%r12d, -244(%rbp)
	jne	.L1061
.L1062:
	movl	-228(%rbp), %edx
	testl	%edx, %edx
	jle	.L1071
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	uv_cond_wait@PLT
	movl	-228(%rbp), %eax
	testl	%eax, %eax
	jg	.L1070
.L1071:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	uv_cond_destroy@PLT
	movq	%r13, %rdi
	call	uv_mutex_destroy@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1097
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1047:
	.cfi_restore_state
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1066:
	addl	$1, %r12d
	cmpl	%r12d, -244(%rbp)
	jne	.L1061
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rdi
	call	_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1048:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1062
	movl	$8, %esi
	call	_ZdlPvm@PLT
	jmp	.L1062
.L1058:
	leaq	-192(%rbp), %rdx
	leaq	232(%rbx), %rdi
	call	_ZNSt6vectorISt10unique_ptrImSt14default_deleteImEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1059
.L1096:
	leaq	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1097:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7580:
	.size	_ZN4node23WorkerThreadsTaskRunnerC2Ei, .-_ZN4node23WorkerThreadsTaskRunnerC2Ei
	.globl	_ZN4node23WorkerThreadsTaskRunnerC1Ei
	.set	_ZN4node23WorkerThreadsTaskRunnerC1Ei,_ZN4node23WorkerThreadsTaskRunnerC2Ei
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatformC2EiPNS_7tracing17TracingControllerE
	.type	_ZN4node12NodePlatformC2EiPNS_7tracing17TracingControllerE, @function
_ZN4node12NodePlatformC2EiPNS_7tracing17TracingControllerE:
.LFB7710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$8, %rdi
	movq	%rax, -8(%rdi)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1111
	leaq	96(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	$1, 56(%rbx)
	movq	%rax, 48(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movl	$0x3f800000, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movups	%xmm0, 112(%rbx)
	testq	%r12, %r12
	je	.L1100
.L1110:
	movq	%r12, 104(%rbx)
	movl	$272, %edi
	call	_Znwm@PLT
	movl	%r13d, %esi
	movq	%rax, %r12
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r12)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r12), %r14
	movq	%rax, (%r12)
	movq	%r14, %rdi
	call	_ZN4node23WorkerThreadsTaskRunnerC1Ei
	movq	120(%rbx), %r13
	movq	%r14, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 112(%rbx)
	testq	%r13, %r13
	je	.L1098
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1104
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1112
.L1098:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1098
.L1112:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1107
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1108:
	cmpl	$1, %eax
	jne	.L1098
	movq	0(%r13), %rax
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	movq	24(%rax), %rax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88platform7tracing17TracingControllerC2Ev@PLT
	leaq	16+_ZTVN4node7tracing17TracingControllerE(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1111:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1107:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1108
	.cfi_endproc
.LFE7710:
	.size	_ZN4node12NodePlatformC2EiPNS_7tracing17TracingControllerE, .-_ZN4node12NodePlatformC2EiPNS_7tracing17TracingControllerE
	.globl	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE
	.set	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE,_ZN4node12NodePlatformC2EiPNS_7tracing17TracingControllerE
	.section	.text._ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm:
.LFB10919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	shrq	$6, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	1(%rdi), %rbx
	addq	$3, %rdi
	subq	$8, %rsp
	cmpq	$8, %rdi
	ja	.L1114
	movq	$8, 8(%r13)
	movl	$64, %edi
.L1115:
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	subq	%rbx, %rdx
	shrq	%rdx
	leaq	(%rax,%rdx,8), %r15
	leaq	(%r15,%rbx,8), %r12
	movq	%r15, %rbx
	cmpq	%r12, %r15
	jnb	.L1117
	.p2align 4,,10
	.p2align 3
.L1116:
	movl	$512, %edi
	addq	$8, %rbx
	call	_Znwm@PLT
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r12
	ja	.L1116
.L1117:
	movq	(%r15), %rdx
	andl	$63, %r14d
	subq	$8, %r12
	movq	%r15, 40(%r13)
	leaq	512(%rdx), %rax
	movq	%rdx, %xmm0
	movq	%rax, 32(%r13)
	movq	(%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, %xmm2
	movups	%xmm0, 16(%r13)
	leaq	(%rax,%r14,8), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1114:
	.cfi_restore_state
	movq	%rdi, 8(%r13)
	salq	$3, %rdi
	jmp	.L1115
	.cfi_endproc
.LFE10919:
	.size	_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformDataC2EPN2v87IsolateEP9uv_loop_s
	.type	_ZN4node22PerIsolatePlatformDataC2EPN2v87IsolateEP9uv_loop_s, @function
_ZN4node22PerIsolatePlatformDataC2EPN2v87IsolateEP9uv_loop_s:
.LFB7640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node22PerIsolatePlatformDataE(%rip), %rax
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$96, %rdi
	subq	$24, %rsp
	movq	%rdx, -24(%rbp)
	movq	$0, -88(%rdi)
	movq	$0, -80(%rdi)
	movq	%rax, -96(%rdi)
	movhps	-24(%rbp), %xmm0
	movq	$0, -72(%rdi)
	movq	$0, -64(%rdi)
	movq	$0, -56(%rdi)
	movq	$0, -48(%rdi)
	movq	$0, -40(%rdi)
	movl	$1, -32(%rdi)
	movq	$0, -8(%rdi)
	movups	%xmm0, -24(%rdi)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1126
	leaq	136(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1125
	leaq	184(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1125
	movl	$0, 232(%rbx)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	240(%rbx), %rdi
	movb	$0, 236(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movups	%xmm0, 256(%rbx)
	movups	%xmm0, 272(%rbx)
	movups	%xmm0, 288(%rbx)
	movups	%xmm0, 304(%rbx)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	leaq	320(%rbx), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1126
	leaq	360(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1125
	leaq	408(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1125
	movl	$0, 456(%rbx)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	464(%rbx), %rdi
	movb	$0, 460(%rbx)
	movq	$0, 464(%rbx)
	movq	$0, 472(%rbx)
	movups	%xmm0, 480(%rbx)
	movups	%xmm0, 496(%rbx)
	movups	%xmm0, 512(%rbx)
	movups	%xmm0, 528(%rbx)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	pxor	%xmm0, %xmm0
	movl	$128, %edi
	movq	$0, 560(%rbx)
	movups	%xmm0, 544(%rbx)
	call	_Znwm@PLT
	movl	$16, %ecx
	leaq	_ZN4node22PerIsolatePlatformData10FlushTasksEP10uv_async_s(%rip), %rdx
	movq	%rax, %rsi
	xorl	%eax, %eax
	movq	%rsi, %rdi
	movq	%rsi, 88(%rbx)
	rep stosq
	movq	-24(%rbp), %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L1129
	movq	88(%rbx), %rdi
	movq	%rbx, (%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_restore_state
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1129:
	leaq	_ZZN4node22PerIsolatePlatformDataC4EPN2v87IsolateEP9uv_loop_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1126:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7640:
	.size	_ZN4node22PerIsolatePlatformDataC2EPN2v87IsolateEP9uv_loop_s, .-_ZN4node22PerIsolatePlatformDataC2EPN2v87IsolateEP9uv_loop_s
	.globl	_ZN4node22PerIsolatePlatformDataC1EPN2v87IsolateEP9uv_loop_s
	.set	_ZN4node22PerIsolatePlatformDataC1EPN2v87IsolateEP9uv_loop_s,_ZN4node22PerIsolatePlatformDataC2EPN2v87IsolateEP9uv_loop_s
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_s
	.type	_ZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_s, @function
_ZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_s:
.LFB7712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	addq	$48, %r15
	pushq	%r13
	movq	%r14, %rdi
	.cfi_offset 13, -40
	leaq	-56(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movq	%rsi, -56(%rbp)
	call	uv_mutex_lock@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	movq	8(%rax), %r12
	movq	(%rax), %rdx
	testq	%r12, %r12
	je	.L1131
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1132
	lock addl	$1, 8(%r12)
.L1131:
	testq	%rdx, %rdx
	jne	.L1170
.L1133:
	movl	$584, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	movabsq	$4294967297, %rax
	leaq	16(%rbx), %rcx
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rcx, %rdi
	movq	%rax, (%rbx)
	movq	%rcx, -80(%rbp)
	call	_ZN4node22PerIsolatePlatformDataC1EPN2v87IsolateEP9uv_loop_s
	movq	32(%rbx), %rax
	movq	-80(%rbp), %rcx
	testq	%rax, %rax
	je	.L1134
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L1134
.L1135:
	movq	%rcx, %xmm0
	movq	%rbx, %xmm1
	movq	%r13, %rsi
	movq	%r15, %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	movdqa	-80(%rbp), %xmm0
	movq	8(%rax), %r13
	movups	%xmm0, (%rax)
	testq	%r13, %r13
	je	.L1143
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1144
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
	cmpl	$1, %edx
	je	.L1171
	.p2align 4,,10
	.p2align 3
.L1143:
	testq	%r12, %r12
	je	.L1150
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1151
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
	cmpl	$1, %edx
	je	.L1172
.L1150:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1134:
	.cfi_restore_state
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	movq	%rcx, 24(%rbx)
	testq	%rax, %rax
	je	.L1173
	lock addl	$1, 12(%rbx)
.L1136:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1138
	testq	%rax, %rax
	je	.L1139
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1140:
	cmpl	$1, %eax
	jne	.L1138
	movq	(%rdi), %rax
	movq	%rcx, -80(%rbp)
	call	*24(%rax)
	movq	-80(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	%rbx, 32(%rbx)
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1132:
	addl	$1, 8(%r12)
	testq	%rdx, %rdx
	je	.L1133
.L1170:
	leaq	_ZZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1151:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %edx
	jne	.L1150
.L1172:
	movq	(%r12), %rdx
	movq	%rax, -80(%rbp)
	movq	%r12, %rdi
	call	*16(%rdx)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1154
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1155:
	cmpl	$1, %eax
	jne	.L1150
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1144:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %edx
	jne	.L1143
.L1171:
	movq	0(%r13), %rdx
	movq	%rax, -80(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1147
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1148:
	cmpl	$1, %eax
	jne	.L1143
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1173:
	addl	$1, 12(%rbx)
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1147:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1154:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1155
	.cfi_endproc
.LFE7712:
	.size	_ZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_s, .-_ZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_s
	.section	.text.unlikely
	.align 2
.LCOLDB11:
	.text
.LHOTB11:
	.align 2
	.p2align 4
	.type	_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0, @function
_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0:
.LFB11929:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	320(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	-112(%rbp), %xmm2
	movdqu	496(%rbx), %xmm4
	leaq	96(%rbx), %r13
	movq	%rax, %xmm6
	movq	-56(%rbp), %rax
	movq	%rdx, %xmm5
	movq	-96(%rbp), %xmm1
	movq	-72(%rbp), %rdx
	punpcklqdq	%xmm5, %xmm2
	movaps	%xmm4, -96(%rbp)
	movdqu	480(%rbx), %xmm3
	movq	%rax, %xmm5
	movq	-128(%rbp), %rax
	punpcklqdq	%xmm6, %xmm1
	movups	%xmm2, 480(%rbx)
	movq	%rdx, %xmm4
	movq	464(%rbx), %rdx
	movq	-80(%rbp), %xmm2
	movups	%xmm1, 496(%rbx)
	movq	%rax, 464(%rbx)
	movq	-120(%rbp), %rax
	movq	-64(%rbp), %xmm1
	movq	%rdx, -128(%rbp)
	punpcklqdq	%xmm4, %xmm2
	movq	472(%rbx), %rdx
	movdqu	512(%rbx), %xmm7
	movq	%rax, 472(%rbx)
	punpcklqdq	%xmm5, %xmm1
	movaps	%xmm3, -112(%rbp)
	movdqu	528(%rbx), %xmm3
	movups	%xmm2, 512(%rbx)
	movups	%xmm1, 528(%rbx)
	movq	%rdx, -120(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm3, -64(%rbp)
	call	uv_mutex_unlock@PLT
	movq	%r12, %rdi
	call	_ZNSt5dequeISt10unique_ptrIN4node11DelayedTaskESt14default_deleteIS2_EESaIS5_EED1Ev
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE17_M_initialize_mapEm
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	-112(%rbp), %xmm1
	movdqu	272(%rbx), %xmm7
	movq	%rax, %xmm4
	movq	-56(%rbp), %rax
	movq	%rdx, %xmm3
	movq	-96(%rbp), %xmm0
	movq	-72(%rbp), %rdx
	punpcklqdq	%xmm3, %xmm1
	movaps	%xmm7, -96(%rbp)
	movdqu	256(%rbx), %xmm6
	movq	%rax, %xmm3
	movq	-128(%rbp), %rax
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm1, 256(%rbx)
	movq	%rdx, %xmm7
	movq	240(%rbx), %rdx
	movq	-80(%rbp), %xmm1
	movups	%xmm0, 272(%rbx)
	movq	%rax, 240(%rbx)
	movq	-120(%rbp), %rax
	movq	-64(%rbp), %xmm0
	movq	%rdx, -128(%rbp)
	punpcklqdq	%xmm7, %xmm1
	movq	248(%rbx), %rdx
	movdqu	288(%rbx), %xmm5
	movq	%rax, 248(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm6, -112(%rbp)
	movdqu	304(%rbx), %xmm6
	movups	%xmm1, 288(%rbx)
	movups	%xmm0, 304(%rbx)
	movq	%rdx, -120(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	call	uv_mutex_unlock@PLT
	movdqa	-80(%rbp), %xmm4
	movdqa	-64(%rbp), %xmm5
	movq	%r12, %rdi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	-192(%rbp), %rdx
	leaq	-160(%rbp), %rsi
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1175
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L1176
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L1177
	movq	-128(%rbp), %rdi
.L1176:
	call	_ZdlPv@PLT
.L1175:
	movq	544(%rbx), %r14
	movq	552(%rbx), %r13
	cmpq	%r13, %r14
	je	.L1178
	movq	%r14, %r12
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1179
	call	*(%r12)
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L1182
.L1180:
	movq	%r14, 552(%rbx)
.L1178:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1183
	movl	8(%rdx), %eax
	leaq	8(%rdx), %rcx
.L1185:
	testl	%eax, %eax
	je	.L1183
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rcx)
	jne	.L1185
	movq	8(%rbx), %rax
	movq	56(%rbx), %r12
	movq	%rdx, 56(%rbx)
	movq	%rax, 48(%rbx)
	testq	%r12, %r12
	je	.L1187
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1188
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1204
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	88(%rbx), %rdi
	leaq	_ZZN4node22PerIsolatePlatformData8ShutdownEvENUlP11uv_handle_sE_4_FUNES2_(%rip), %rsi
	call	uv_close@PLT
	movq	$0, 88(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1205
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L1182
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1188:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1187
.L1204:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1191
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1192:
	cmpl	$1, %eax
	jne	.L1187
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1191:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1192
.L1205:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0.cold, @function
_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0.cold:
.LFSB11929:
.L1183:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE11929:
	.text
	.size	_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0, .-_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0
	.section	.text.unlikely
	.size	_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0.cold, .-_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0.cold
.LCOLDE11:
	.text
.LHOTE11:
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformData8ShutdownEv
	.type	_ZN4node22PerIsolatePlatformData8ShutdownEv, @function
_ZN4node22PerIsolatePlatformData8ShutdownEv:
.LFB7661:
	.cfi_startproc
	endbr64
	cmpq	$0, 88(%rdi)
	je	.L1206
	jmp	_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0
	.p2align 4,,10
	.p2align 3
.L1206:
	ret
	.cfi_endproc
.LFE7661:
	.size	_ZN4node22PerIsolatePlatformData8ShutdownEv, .-_ZN4node22PerIsolatePlatformData8ShutdownEv
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformDataD2Ev
	.type	_ZN4node22PerIsolatePlatformDataD2Ev, @function
_ZN4node22PerIsolatePlatformDataD2Ev:
.LFB7657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node22PerIsolatePlatformDataE(%rip), %rax
	cmpq	$0, 88(%rdi)
	movq	%rax, (%rdi)
	je	.L1209
	call	_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0
.L1209:
	movq	552(%rbx), %r13
	movq	544(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1210
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1211
	call	*(%r12)
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L1214
.L1212:
	movq	544(%rbx), %r12
.L1210:
	testq	%r12, %r12
	je	.L1215
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1215:
	leaq	320(%rbx), %rax
	movq	504(%rbx), %rcx
	movq	496(%rbx), %rsi
	movq	%rax, -168(%rbp)
	movq	512(%rbx), %rax
	leaq	8(%rcx), %r15
	movq	%rsi, -152(%rbp)
	movq	480(%rbx), %r12
	movq	%rax, -136(%rbp)
	movq	520(%rbx), %rax
	movq	%rcx, -176(%rbp)
	movq	%rax, -160(%rbp)
	movq	536(%rbx), %rax
	movq	%rax, -144(%rbp)
	cmpq	%r15, %rax
	jbe	.L1229
	movq	%r12, -192(%rbp)
	movq	%rbx, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	(%r15), %r13
	leaq	512(%r13), %rbx
	movq	%rbx, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L1219
	movq	176(%r14), %r12
	testq	%r12, %r12
	je	.L1221
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1222
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1223:
	cmpl	$1, %eax
	je	.L1327
.L1221:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1227
	movq	(%rdi), %rax
	call	*8(%rax)
.L1227:
	movl	$184, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1219:
	addq	$8, %r13
	cmpq	-184(%rbp), %r13
	jne	.L1228
	addq	$8, %r15
	cmpq	%r15, -144(%rbp)
	ja	.L1216
	movq	-192(%rbp), %r12
	movq	-200(%rbp), %rbx
.L1229:
	movq	-176(%rbp), %rdx
	cmpq	%rdx, -144(%rbp)
	je	.L1328
	cmpq	-152(%rbp), %r12
	je	.L1243
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1234
	movq	176(%r14), %r13
	testq	%r13, %r13
	je	.L1236
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1237
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1238:
	cmpl	$1, %eax
	jne	.L1236
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1240
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1241:
	cmpl	$1, %eax
	jne	.L1236
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1242
	movq	(%rdi), %rax
	call	*8(%rax)
.L1242:
	movl	$184, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1234:
	addq	$8, %r12
	cmpq	%r12, -152(%rbp)
	jne	.L1230
.L1243:
	movq	-160(%rbp), %rsi
	movq	%rsi, %r14
	cmpq	%rsi, -136(%rbp)
	je	.L1253
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1244
	movq	176(%r13), %r12
	testq	%r12, %r12
	je	.L1246
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1247
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1248:
	cmpl	$1, %eax
	jne	.L1246
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1250
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1251:
	cmpl	$1, %eax
	jne	.L1246
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1252
	movq	(%rdi), %rax
	call	*8(%rax)
.L1252:
	movl	$184, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1244:
	addq	$8, %r14
	cmpq	%r14, -136(%rbp)
	jne	.L1231
.L1253:
	movq	464(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1233
	movq	536(%rbx), %rax
	movq	504(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L1264
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L1265
	movq	464(%rbx), %rdi
.L1264:
	call	_ZdlPv@PLT
.L1233:
	leaq	408(%rbx), %rdi
	leaq	96(%rbx), %r14
	call	uv_cond_destroy@PLT
	leaq	360(%rbx), %rdi
	call	uv_cond_destroy@PLT
	movq	-168(%rbp), %rdi
	call	uv_mutex_destroy@PLT
	movdqu	288(%rbx), %xmm0
	movdqu	304(%rbx), %xmm1
	leaq	240(%rbx), %rdi
	movdqu	256(%rbx), %xmm2
	movdqu	272(%rbx), %xmm3
	leaq	-128(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN2v84TaskESt14default_deleteIS2_EESaIS5_EE19_M_destroy_data_auxESt15_Deque_iteratorIS5_RS5_PS5_ESB_
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1266
	movq	312(%rbx), %rax
	movq	280(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L1267
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L1268
	movq	240(%rbx), %rdi
.L1267:
	call	_ZdlPv@PLT
.L1266:
	leaq	184(%rbx), %rdi
	call	uv_cond_destroy@PLT
	leaq	136(%rbx), %rdi
	call	uv_cond_destroy@PLT
	movq	%r14, %rdi
	call	uv_mutex_destroy@PLT
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L1270
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1271
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1329
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1276
	call	_ZdlPv@PLT
.L1276:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1208
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1279
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L1330
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1331
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1211:
	.cfi_restore_state
	addq	$16, %r12
	cmpq	%r12, %r13
	jne	.L1214
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1327:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1225
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1226:
	cmpl	$1, %eax
	jne	.L1221
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1225:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1222:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1237:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1247:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1271:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1270
.L1329:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1274
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1275:
	cmpl	$1, %eax
	jne	.L1270
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1279:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L1208
.L1330:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	-136(%rbp), %rax
	cmpq	%r12, %rax
	je	.L1253
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L1254
	movq	176(%r13), %r14
	testq	%r14, %r14
	je	.L1256
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1257
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L1258:
	cmpl	$1, %eax
	jne	.L1256
	movq	(%r14), %rax
	movq	%rdx, -136(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-136(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1260
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1261:
	cmpl	$1, %eax
	je	.L1332
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1262
	movq	(%rdi), %rax
	call	*8(%rax)
.L1262:
	movl	$184, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1254:
	addq	$8, %r12
	cmpq	%r12, %r15
	jne	.L1263
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1257:
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1240:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1260:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1332:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1256
.L1331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7657:
	.size	_ZN4node22PerIsolatePlatformDataD2Ev, .-_ZN4node22PerIsolatePlatformDataD2Ev
	.globl	_ZN4node22PerIsolatePlatformDataD1Ev
	.set	_ZN4node22PerIsolatePlatformDataD1Ev,_ZN4node22PerIsolatePlatformDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node22PerIsolatePlatformDataD0Ev
	.type	_ZN4node22PerIsolatePlatformDataD0Ev, @function
_ZN4node22PerIsolatePlatformDataD0Ev:
.LFB7659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node22PerIsolatePlatformDataD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$568, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7659:
	.size	_ZN4node22PerIsolatePlatformDataD0Ev, .-_ZN4node22PerIsolatePlatformDataD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateE
	.type	_ZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateE, @function
_ZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateE:
.LFB7719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	call	uv_mutex_lock@PLT
	leaq	48(%rbx), %rdi
	leaq	-56(%rbp), %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v87IsolateESt4pairIKS3_St10shared_ptrIN4node22PerIsolatePlatformDataEEESaISA_ENS_10_Select1stESt8equal_toIS3_ESt4hashIS3_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS5_
	movq	8(%rax), %r13
	movq	(%rax), %rdi
	testq	%r13, %r13
	je	.L1336
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1337
	lock addl	$1, 8(%r13)
.L1336:
	testq	%rdi, %rdi
	je	.L1378
.L1338:
	cmpq	$0, 88(%rdi)
	je	.L1339
	call	_ZN4node22PerIsolatePlatformData8ShutdownEv.part.0
.L1339:
	movq	-56(%rbp), %r9
	movq	56(%rbx), %rcx
	xorl	%edx, %edx
	movq	48(%rbx), %r15
	movq	%r9, %rax
	divq	%rcx
	leaq	(%r15,%rdx,8), %rax
	movq	%rdx, %r11
	movq	(%rax), %rdi
	movq	%rax, -64(%rbp)
	testq	%rdi, %rdi
	je	.L1340
	movq	(%rdi), %r12
	movq	%rdi, %r10
	movq	8(%r12), %rsi
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L1340
	movq	8(%r8), %rsi
	xorl	%edx, %edx
	movq	%r12, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L1340
	movq	%r8, %r12
.L1342:
	cmpq	%rsi, %r9
	jne	.L1379
	movq	(%r12), %rsi
	cmpq	%r10, %rdi
	je	.L1380
	testq	%rsi, %rsi
	je	.L1344
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L1344
	movq	%r10, (%r15,%rdx,8)
	movq	(%r12), %rsi
.L1344:
	movq	24(%r12), %r15
	movq	%rsi, (%r10)
	testq	%r15, %r15
	je	.L1347
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1348
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r15)
.L1349:
	cmpl	$1, %edx
	jne	.L1347
	movq	(%r15), %rdx
	movq	%rax, -64(%rbp)
	movq	%r15, %rdi
	call	*16(%rdx)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1351
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1352:
	cmpl	$1, %eax
	je	.L1381
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 72(%rbx)
.L1340:
	testq	%r13, %r13
	je	.L1354
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1355
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
	cmpl	$1, %edx
	je	.L1382
.L1354:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1337:
	.cfi_restore_state
	addl	$1, 8(%r13)
	testq	%rdi, %rdi
	jne	.L1338
.L1378:
	leaq	_ZZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1355:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %edx
	jne	.L1354
.L1382:
	movq	0(%r13), %rdx
	movq	%rax, -64(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1358
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1359:
	cmpl	$1, %eax
	jne	.L1354
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1380:
	testq	%rsi, %rsi
	je	.L1362
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L1344
	movq	-64(%rbp), %rax
	movq	%r10, (%r15,%rdx,8)
	movq	(%rax), %rax
.L1343:
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1383
.L1345:
	movq	-64(%rbp), %rax
	movq	$0, (%rax)
	movq	(%r12), %rsi
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1348:
	movl	8(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1358:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	%r10, %rax
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1351:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	%rsi, 64(%rbx)
	jmp	.L1345
	.cfi_endproc
.LFE7719:
	.size	_ZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateE, .-_ZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateE
	.weak	_ZTVN4node7tracing17TracingControllerE
	.section	.data.rel.ro._ZTVN4node7tracing17TracingControllerE,"awG",@progbits,_ZTVN4node7tracing17TracingControllerE,comdat
	.align 8
	.type	_ZTVN4node7tracing17TracingControllerE, @object
	.size	_ZTVN4node7tracing17TracingControllerE, 96
_ZTVN4node7tracing17TracingControllerE:
	.quad	0
	.quad	0
	.quad	_ZN4node7tracing17TracingControllerD1Ev
	.quad	_ZN4node7tracing17TracingControllerD0Ev
	.quad	_ZN2v88platform7tracing17TracingController23GetCategoryGroupEnabledEPKc
	.quad	_ZN2v88platform7tracing17TracingController13AddTraceEventEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEj
	.quad	_ZN2v88platform7tracing17TracingController26AddTraceEventWithTimestampEcPKhPKcS6_mmiPS6_S4_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteISB_EEjl
	.quad	_ZN2v88platform7tracing17TracingController24UpdateTraceEventDurationEPKhPKcm
	.quad	_ZN2v88platform7tracing17TracingController21AddTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.quad	_ZN2v88platform7tracing17TracingController24RemoveTraceStateObserverEPNS_17TracingController18TraceStateObserverE
	.quad	_ZN4node7tracing17TracingController28CurrentTimestampMicrosecondsEv
	.quad	_ZN2v88platform7tracing17TracingController31CurrentCpuTimestampMicrosecondsEv
	.weak	_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskE
	.section	.data.rel.ro.local._ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskE,"awG",@progbits,_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskE,comdat
	.align 8
	.type	_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskE, @object
	.size	_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskE, 40
_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskE:
	.quad	0
	.quad	0
	.quad	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD1Ev
	.quad	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTaskD0Ev
	.quad	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler8StopTask3RunEv
	.weak	_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE
	.section	.data.rel.ro.local._ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE,"awG",@progbits,_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE,comdat
	.align 8
	.type	_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE, @object
	.size	_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE, 40
_ZTVN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskE:
	.quad	0
	.quad	0
	.quad	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD1Ev
	.quad	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTaskD0Ev
	.quad	_ZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEv
	.weak	_ZTVN4node22PerIsolatePlatformDataE
	.section	.data.rel.ro.local._ZTVN4node22PerIsolatePlatformDataE,"awG",@progbits,_ZTVN4node22PerIsolatePlatformDataE,comdat
	.align 8
	.type	_ZTVN4node22PerIsolatePlatformDataE, @object
	.size	_ZTVN4node22PerIsolatePlatformDataE, 96
_ZTVN4node22PerIsolatePlatformDataE:
	.quad	0
	.quad	0
	.quad	_ZN4node22PerIsolatePlatformData8PostTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.quad	_ZN4node22PerIsolatePlatformData19PostNonNestableTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.quad	_ZN4node22PerIsolatePlatformData15PostDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.quad	_ZN4node22PerIsolatePlatformData26PostNonNestableDelayedTaskESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.quad	_ZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EE
	.quad	_ZN4node22PerIsolatePlatformData16IdleTasksEnabledEv
	.quad	_ZNK4node22PerIsolatePlatformData23NonNestableTasksEnabledEv
	.quad	_ZNK4node22PerIsolatePlatformData30NonNestableDelayedTasksEnabledEv
	.quad	_ZN4node22PerIsolatePlatformDataD1Ev
	.quad	_ZN4node22PerIsolatePlatformDataD0Ev
	.weak	_ZTVN4node12NodePlatformE
	.section	.data.rel.ro.local._ZTVN4node12NodePlatformE,"awG",@progbits,_ZTVN4node12NodePlatformE,comdat
	.align 8
	.type	_ZTVN4node12NodePlatformE, @object
	.size	_ZTVN4node12NodePlatformE, 232
_ZTVN4node12NodePlatformE:
	.quad	0
	.quad	0
	.quad	_ZN4node12NodePlatformD1Ev
	.quad	_ZN4node12NodePlatformD0Ev
	.quad	_ZN2v88Platform16GetPageAllocatorEv
	.quad	_ZN2v88Platform24OnCriticalMemoryPressureEv
	.quad	_ZN2v88Platform24OnCriticalMemoryPressureEm
	.quad	_ZN4node12NodePlatform21NumberOfWorkerThreadsEv
	.quad	_ZN4node12NodePlatform23GetForegroundTaskRunnerEPN2v87IsolateE
	.quad	_ZN4node12NodePlatform18CallOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EE
	.quad	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	_ZN4node12NodePlatform25CallDelayedOnWorkerThreadESt10unique_ptrIN2v84TaskESt14default_deleteIS3_EEd
	.quad	_ZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskE
	.quad	_ZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEd
	.quad	_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE
	.quad	_ZN4node12NodePlatform16IdleTasksEnabledEPN2v87IsolateE
	.quad	_ZN4node12NodePlatform27MonotonicallyIncreasingTimeEv
	.quad	_ZN4node12NodePlatform22CurrentClockTimeMillisEv
	.quad	_ZN4node12NodePlatform20GetStackTracePrinterEv
	.quad	_ZN4node12NodePlatform20GetTracingControllerEv
	.quad	_ZN2v88Platform19DumpWithoutCrashingEv
	.quad	_ZN2v88Platform11AddCrashKeyEiPKcm
	.quad	_ZN4node12NodePlatform20FlushForegroundTasksEPN2v87IsolateE
	.quad	_ZN4node12NodePlatform10DrainTasksEPN2v87IsolateE
	.quad	_ZN4node20MultiIsolatePlatform25CancelPendingDelayedTasksEPN2v87IsolateE
	.quad	_ZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_s
	.quad	_ZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateE
	.quad	_ZN4node12NodePlatform26AddIsolateFinishedCallbackEPN2v87IsolateEPFvPvES4_
	.weak	_ZTVN4node20MultiIsolatePlatformE
	.section	.data.rel.ro._ZTVN4node20MultiIsolatePlatformE,"awG",@progbits,_ZTVN4node20MultiIsolatePlatformE,comdat
	.align 8
	.type	_ZTVN4node20MultiIsolatePlatformE, @object
	.size	_ZTVN4node20MultiIsolatePlatformE, 232
_ZTVN4node20MultiIsolatePlatformE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88Platform16GetPageAllocatorEv
	.quad	_ZN2v88Platform24OnCriticalMemoryPressureEv
	.quad	_ZN2v88Platform24OnCriticalMemoryPressureEm
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88Platform30CallBlockingTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	_ZN2v88Platform33CallLowPriorityTaskOnWorkerThreadESt10unique_ptrINS_4TaskESt14default_deleteIS2_EE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88Platform26CallIdleOnForegroundThreadEPNS_7IsolateEPNS_8IdleTaskE
	.quad	_ZN2v88Platform16IdleTasksEnabledEPNS_7IsolateE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88Platform20GetStackTracePrinterEv
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88Platform19DumpWithoutCrashingEv
	.quad	_ZN2v88Platform11AddCrashKeyEiPKcm
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node20MultiIsolatePlatform25CancelPendingDelayedTasksEPN2v87IsolateE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node23WorkerThreadsTaskRunnerESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node22PerIsolatePlatformDataESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC12:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC14:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC15:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC17:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.section	.rodata.str1.1
.LC18:
	.string	"../src/node_platform.cc:491"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"(tracing_controller_) != nullptr"
	.align 8
.LC20:
	.string	"virtual node::tracing::TracingController* node::NodePlatform::GetTracingController()"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12NodePlatform20GetTracingControllerEvE4args, @object
	.size	_ZZN4node12NodePlatform20GetTracingControllerEvE4args, 24
_ZZN4node12NodePlatform20GetTracingControllerEvE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"../src/node_platform.cc:466"
.LC22:
	.string	"data"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"std::shared_ptr<node::PerIsolatePlatformData> node::NodePlatform::ForIsolate(v8::Isolate*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12NodePlatform10ForIsolateEPN2v87IsolateEE4args, @object
	.size	_ZZN4node12NodePlatform10ForIsolateEPN2v87IsolateEE4args, 24
_ZZN4node12NodePlatform10ForIsolateEPN2v87IsolateEE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"../src/node_platform.cc:393"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"(it) != (scheduled_delayed_tasks_.end())"
	.align 8
.LC26:
	.string	"void node::PerIsolatePlatformData::DeleteFromScheduledTasks(node::DelayedTask*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskEE4args, @object
	.size	_ZZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskEE4args, 24
_ZZN4node22PerIsolatePlatformData24DeleteFromScheduledTasksEPNS_11DelayedTaskEE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"../src/node_platform.cc:344"
.LC28:
	.string	"existing"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"virtual void node::NodePlatform::UnregisterIsolate(v8::Isolate*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateEE4args, @object
	.size	_ZZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateEE4args, 24
_ZZN4node12NodePlatform17UnregisterIsolateEPN2v87IsolateEE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/node_platform.cc:336"
.LC31:
	.string	"!existing"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"virtual void node::NodePlatform::RegisterIsolate(v8::Isolate*, uv_loop_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_sE4args, @object
	.size	_ZZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_sE4args, 24
_ZZN4node12NodePlatform15RegisterIsolateEPN2v87IsolateEP9uv_loop_sE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/node_platform.cc:315"
.LC34:
	.string	"(uv_handle_count_) >= (1)"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"void node::PerIsolatePlatformData::DecreaseHandleCount()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22PerIsolatePlatformData19DecreaseHandleCountEvE4args, @object
	.size	_ZZN4node22PerIsolatePlatformData19DecreaseHandleCountEvE4args, 24
_ZZN4node22PerIsolatePlatformData19DecreaseHandleCountEvE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"../src/node_platform.cc:238"
.LC37:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"virtual void node::PerIsolatePlatformData::PostIdleTask(std::unique_ptr<v8::IdleTask>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EEE4args, @object
	.size	_ZZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EEE4args, 24
_ZZN4node22PerIsolatePlatformData12PostIdleTaskESt10unique_ptrIN2v88IdleTaskESt14default_deleteIS3_EEE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_platform.cc:227"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"(0) == (uv_async_init(loop, flush_tasks_, FlushTasks))"
	.align 8
.LC41:
	.string	"node::PerIsolatePlatformData::PerIsolatePlatformData(v8::Isolate*, uv_loop_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node22PerIsolatePlatformDataC4EPN2v87IsolateEP9uv_loop_sE4args, @object
	.size	_ZZN4node22PerIsolatePlatformDataC4EPN2v87IsolateEP9uv_loop_sE4args, 24
_ZZN4node22PerIsolatePlatformDataC4EPN2v87IsolateEP9uv_loop_sE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"../src/node_platform.cc:215"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"(0) == (uv_thread_join(threads_[i].get()))"
	.align 8
.LC44:
	.string	"void node::WorkerThreadsTaskRunner::Shutdown()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node23WorkerThreadsTaskRunner8ShutdownEvE4args, @object
	.size	_ZZN4node23WorkerThreadsTaskRunner8ShutdownEvE4args, 24
_ZZN4node23WorkerThreadsTaskRunner8ShutdownEvE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args_0
	.section	.rodata.str1.1
.LC45:
	.string	"../src/node_platform.cc:132"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"(0) == (uv_timer_start(timer.get(), RunTask, delay_millis, 0))"
	.align 8
.LC47:
	.string	"virtual void node::WorkerThreadsTaskRunner::DelayedTaskScheduler::ScheduleTask::Run()"
	.section	.data.rel.ro.local._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args_0,"awG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args_0,comdat
	.align 16
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args_0, @gnu_unique_object
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args_0, 24
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args_0:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args
	.section	.rodata.str1.1
.LC48:
	.string	"../src/node_platform.cc:130"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"(0) == (uv_timer_init(&scheduler_->loop_, timer.get()))"
	.section	.data.rel.ro.local._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args,"awG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args,comdat
	.align 16
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args, @gnu_unique_object
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args, 24
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler12ScheduleTask3RunEvE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC47
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args_0
	.section	.rodata.str1.1
.LC50:
	.string	"../src/node_platform.cc:86"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"(0) == (uv_async_init(&loop_, &flush_tasks_, FlushTasks))"
	.align 8
.LC52:
	.string	"void node::WorkerThreadsTaskRunner::DelayedTaskScheduler::Run()"
	.section	.data.rel.ro.local._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args_0,"awG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args_0,comdat
	.align 16
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args_0, @gnu_unique_object
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args_0, 24
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args_0:
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args
	.section	.rodata.str1.1
.LC53:
	.string	"../src/node_platform.cc:84"
.LC54:
	.string	"(0) == (uv_loop_init(&loop_))"
	.section	.data.rel.ro.local._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args,"awG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args,comdat
	.align 16
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args, @gnu_unique_object
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args, 24
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC52
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81
	.section	.bss._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81,"awG",@nobits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81,comdat
	.align 8
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81, @gnu_unique_object
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81, 8
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler3RunEvE27trace_event_unique_atomic81:
	.zero	8
	.weak	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvE4args
	.section	.rodata.str1.1
.LC55:
	.string	"../src/node_platform.cc:62"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"(0) == (uv_thread_create(t.get(), start_thread, this))"
	.align 8
.LC57:
	.string	"std::unique_ptr<long unsigned int> node::WorkerThreadsTaskRunner::DelayedTaskScheduler::Start()"
	.section	.data.rel.ro.local._ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvE4args,"awG",@progbits,_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvE4args,comdat
	.align 16
	.type	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvE4args, @gnu_unique_object
	.size	_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvE4args, 24
_ZZN4node23WorkerThreadsTaskRunner20DelayedTaskScheduler5StartEvE4args:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.local	_ZZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPvE27trace_event_unique_atomic33
	.comm	_ZZN4node12_GLOBAL__N_1L20PlatformWorkerThreadEPvE27trace_event_unique_atomic33,8,8
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEdE4args
	.section	.rodata.str1.1
.LC58:
	.string	"../src/node_platform.h:157"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"virtual void node::NodePlatform::CallDelayedOnForegroundThread(v8::Isolate*, v8::Task*, double)"
	.section	.data.rel.ro.local._ZZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEdE4args,"awG",@progbits,_ZZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEdE4args,comdat
	.align 16
	.type	_ZZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEdE4args, @gnu_unique_object
	.size	_ZZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEdE4args, 24
_ZZN4node12NodePlatform29CallDelayedOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEdE4args:
	.quad	.LC58
	.quad	.LC37
	.quad	.LC59
	.weak	_ZZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEE4args
	.section	.rodata.str1.1
.LC60:
	.string	"../src/node_platform.h:152"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"virtual void node::NodePlatform::CallOnForegroundThread(v8::Isolate*, v8::Task*)"
	.section	.data.rel.ro.local._ZZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEE4args,"awG",@progbits,_ZZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEE4args,comdat
	.align 16
	.type	_ZZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEE4args, @gnu_unique_object
	.size	_ZZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEE4args, 24
_ZZN4node12NodePlatform22CallOnForegroundThreadEPN2v87IsolateEPNS1_4TaskEE4args:
	.quad	.LC60
	.quad	.LC37
	.quad	.LC61
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1104006501
	.align 8
.LC9:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
