	.file	"node_metadata.cc"
	.text
	.section	.text._ZN4node8MetadataD2Ev,"axG",@progbits,_ZN4node8MetadataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8MetadataD2Ev
	.type	_ZN4node8MetadataD2Ev, @function
_ZN4node8MetadataD2Ev:
.LFB5374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	688(%rbx), %rax
	subq	$8, %rsp
	movq	672(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	640(%rbx), %rdi
	leaq	656(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3
	call	_ZdlPv@PLT
.L3:
	movq	608(%rbx), %rdi
	leaq	624(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	576(%rbx), %rdi
	leaq	592(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	544(%rbx), %rdi
	leaq	560(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	movq	512(%rbx), %rdi
	leaq	528(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	480(%rbx), %rdi
	leaq	496(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	448(%rbx), %rdi
	leaq	464(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	416(%rbx), %rdi
	leaq	432(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	384(%rbx), %rdi
	leaq	400(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	352(%rbx), %rdi
	leaq	368(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	320(%rbx), %rdi
	leaq	336(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	288(%rbx), %rdi
	leaq	304(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	256(%rbx), %rdi
	leaq	272(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	192(%rbx), %rdi
	leaq	208(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L1
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5374:
	.size	_ZN4node8MetadataD2Ev, .-_ZN4node8MetadataD2Ev
	.weak	_ZN4node8MetadataD1Ev
	.set	_ZN4node8MetadataD1Ev,_ZN4node8MetadataD2Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%u"
	.text
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB5411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L27
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L27:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L29
.L48:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L48
.L29:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L31
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L32:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L33
	testb	$4, %al
	jne	.L50
	testl	%eax, %eax
	je	.L34
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L51
.L34:
	movq	(%r12), %rcx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L34
	andl	$-8, %eax
	xorl	%edx, %edx
.L37:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L37
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L50:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L34
.L51:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L34
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5411:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.1
.LC1:
	.string	"OpenSSL 1.1.1g  21 Apr 2020"
.LC2:
	.string	"%.*s"
	.text
	.p2align 4
	.globl	_ZN4node17GetOpenSSLVersionB5cxx11Ev
	.type	_ZN4node17GetOpenSSLVersionB5cxx11Ev, @function
_ZN4node17GetOpenSSLVersionB5cxx11Ev:
.LFB4284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L53:
	movl	%eax, %ecx
	movq	%rax, %rsi
	addq	$1, %rax
	cmpb	$32, (%rdx,%rax)
	jne	.L53
	addl	$2, %ecx
	movslq	%ecx, %rax
	addq	%rax, %rdx
	cmpb	$32, (%rdx)
	je	.L67
	addl	$3, %esi
	movq	%rdx, %rax
	subl	%edx, %esi
	.p2align 4,,10
	.p2align 3
.L55:
	leal	(%rsi,%rax), %r9d
	addq	$1, %rax
	cmpb	$32, (%rax)
	jne	.L55
	subl	%ecx, %r9d
.L54:
	subq	$8, %rsp
	movl	$128, %ecx
	movl	$128, %esi
	xorl	%eax, %eax
	pushq	%rdx
	leaq	-176(%rbp), %r13
	leaq	.LC2(%rip), %r8
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r13, %rbx
	call	__snprintf_chk@PLT
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
.L56:
	movl	(%rbx), %ecx
	addq	$4, %rbx
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L56
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rbx), %rcx
	cmove	%rcx, %rbx
	movl	%edx, %edi
	addb	%dl, %dil
	popq	%rdx
	popq	%rcx
	sbbq	$3, %rbx
	subq	%r13, %rbx
	movq	%rbx, -184(%rbp)
	cmpq	$15, %rbx
	ja	.L82
	cmpq	$1, %rbx
	jne	.L60
	movzbl	-176(%rbp), %edx
	movb	%dl, 16(%r12)
.L61:
	movq	%rbx, 8(%r12)
	movb	$0, (%rax,%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L61
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%edx, %edx
	leaq	-184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-184(%rbp), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
.L59:
	movl	%ebx, %ecx
	cmpl	$8, %ebx
	jnb	.L62
	andl	$4, %ebx
	jne	.L84
	testl	%ecx, %ecx
	je	.L63
	movzbl	0(%r13), %edx
	movb	%dl, (%rax)
	testb	$2, %cl
	jne	.L85
.L63:
	movq	-184(%rbp), %rbx
	movq	(%r12), %rax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-176(%rbp), %rdx
	leaq	8(%rax), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rbx,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%r9d, %r9d
	jmp	.L54
.L84:
	movl	0(%r13), %edx
	movl	%edx, (%rax)
	movl	-4(%r13,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L63
.L85:
	movzwl	-2(%r13,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L63
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4284:
	.size	_ZN4node17GetOpenSSLVersionB5cxx11Ev, .-_ZN4node17GetOpenSSLVersionB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node8Metadata8Versions22InitializeIntlVersionsEv
	.type	_ZN4node8Metadata8Versions22InitializeIntlVersionsEv, @function
_ZN4node8Metadata8Versions22InitializeIntlVersionsEv:
.LFB4285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-72(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	call	_ZN6icu_678TimeZone16getTZDataVersionER10UErrorCode@PLT
	movl	-72(%rbp), %edx
	testl	%edx, %edx
	jle	.L95
.L87:
	leaq	-68(%rbp), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ulocdata_getCLDRVersion_67@PLT
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jle	.L96
.L86:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movq	456(%rbx), %rdx
	leaq	448(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r12, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	-64(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	u_versionToString_67@PLT
	movq	%r13, %r8
.L89:
	movl	(%r8), %edx
	addq	$4, %r8
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L89
	movl	%eax, %edx
	leaq	384(%rbx), %rdi
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r8), %rdx
	cmove	%rdx, %r8
	movq	392(%rbx), %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movq	%r13, %rcx
	sbbq	$3, %r8
	xorl	%esi, %esi
	subq	%r13, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L86
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4285:
	.size	_ZN4node8Metadata8Versions22InitializeIntlVersionsEv, .-_ZN4node8Metadata8Versions22InitializeIntlVersionsEv
	.section	.rodata.str1.1
.LC3:
	.string	"12.18.4"
.LC4:
	.string	"1.2.11"
.LC5:
	.string	"1.16.0"
.LC6:
	.string	"72"
.LC7:
	.string	"1.41.0"
.LC8:
	.string	"6"
.LC9:
	.string	"basic_string::append"
.LC10:
	.string	"."
.LC11:
	.string	"67.1"
.LC12:
	.string	"13.0"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8Metadata8VersionsC2Ev
	.type	_ZN4node8Metadata8VersionsC2Ev, @function
_ZN4node8Metadata8VersionsC2Ev:
.LFB4290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	96(%rdi), %r9
	leaq	64(%rdi), %r10
	xorl	%edx, %edx
	leaq	32(%rdi), %r11
	leaq	480(%rdi), %rcx
	xorl	%esi, %esi
	movl	$7, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	160(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	192(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	224(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	256(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%r9, -304(%rbp)
	movq	%rax, (%rdi)
	leaq	48(%rdi), %rax
	movq	%rax, 32(%rdi)
	leaq	80(%rdi), %rax
	movq	%rax, 64(%rdi)
	leaq	112(%rdi), %rax
	movq	%rax, 96(%rdi)
	leaq	144(%rdi), %rax
	movq	%rax, 128(%rdi)
	movq	%rax, -344(%rbp)
	leaq	176(%rdi), %rax
	movq	%rax, 160(%rdi)
	leaq	208(%rdi), %rax
	movq	%rax, 192(%rdi)
	leaq	240(%rdi), %rax
	movq	%r10, -312(%rbp)
	movq	%r11, -320(%rbp)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movq	$0, 72(%rdi)
	movb	$0, 80(%rdi)
	movq	$0, 104(%rdi)
	movb	$0, 112(%rdi)
	movq	$0, 136(%rdi)
	movb	$0, 144(%rdi)
	movq	$0, 168(%rdi)
	movb	$0, 176(%rdi)
	movq	$0, 200(%rdi)
	movq	%rax, 224(%rdi)
	leaq	272(%rdi), %rax
	movq	%rax, 256(%rdi)
	leaq	304(%rdi), %rax
	movq	%rax, 288(%rdi)
	leaq	336(%rdi), %rax
	movq	%rax, 320(%rdi)
	leaq	368(%rdi), %rax
	movq	%rax, -336(%rbp)
	movq	%rax, 352(%rdi)
	leaq	400(%rdi), %rax
	movq	%rax, 384(%rdi)
	leaq	432(%rdi), %rax
	movq	%rax, 416(%rdi)
	leaq	464(%rdi), %rax
	movq	%rax, 448(%rdi)
	leaq	496(%rdi), %rax
	movb	$0, 208(%rdi)
	movq	$0, 232(%rdi)
	movb	$0, 240(%rdi)
	movq	$0, 264(%rdi)
	movb	$0, 272(%rdi)
	movq	$0, 296(%rdi)
	movb	$0, 304(%rdi)
	movq	$0, 328(%rdi)
	movb	$0, 336(%rdi)
	movq	$0, 360(%rdi)
	movb	$0, 368(%rdi)
	movq	$0, 392(%rdi)
	movb	$0, 400(%rdi)
	movq	$0, 424(%rdi)
	movb	$0, 432(%rdi)
	movq	$0, 456(%rdi)
	movb	$0, 464(%rdi)
	movq	%rax, 480(%rdi)
	movq	$0, 488(%rdi)
	movb	$0, 496(%rdi)
	movq	%rcx, -328(%rbp)
	leaq	.LC3(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	call	_ZN2v82V810GetVersionEv@PLT
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	strlen@PLT
	movq	-320(%rbp), %r11
	movq	40(%rbx), %rdx
	xorl	%esi, %esi
	movq	-296(%rbp), %rcx
	movq	%rax, %r8
	movq	%r11, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	call	uv_version_string@PLT
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	strlen@PLT
	movq	-312(%rbp), %r10
	movq	72(%rbx), %rdx
	xorl	%esi, %esi
	movq	-296(%rbp), %rcx
	movq	%rax, %r8
	movq	%r10, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-304(%rbp), %r9
	movq	104(%rbx), %rdx
	xorl	%esi, %esi
	movl	$6, %r8d
	leaq	.LC4(%rip), %rcx
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	$6, %r8d
	movq	168(%rbx), %rdx
	leaq	.LC5(%rip), %rcx
	leaq	-160(%rbp), %r15
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$2, %r8d
	movq	200(%rbx), %rdx
	leaq	.LC6(%rip), %rcx
	leaq	-96(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$6, %r8d
	movq	232(%rbx), %rdx
	leaq	.LC7(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%esi, %esi
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	264(%rbx), %rdx
	leaq	.LC8(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	_ZN4node11per_process14llhttp_versionE(%rip), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	296(%rbx), %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	288(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	_ZN4node11per_process19http_parser_versionE(%rip), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	movq	328(%rbx), %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	320(%rbx), %rdi
	leaq	-128(%rbp), %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	call	BrotliEncoderVersion@PLT
	movq	vsnprintf@GOTPCREL(%rip), %r13
	movl	$16, %edx
	movq	%r14, %rdi
	andl	$4095, %eax
	leaq	.LC0(%rip), %rcx
	movl	%eax, %r8d
	movq	%r13, %rsi
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	call	BrotliEncoderVersion@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	shrl	$12, %eax
	leaq	.LC0(%rip), %rcx
	movl	%eax, %r8d
	xorl	%eax, %eax
	andl	$4095, %r8d
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	call	BrotliEncoderVersion@PLT
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	shrl	$24, %eax
	leaq	.LC0(%rip), %rcx
	movl	%eax, %r8d
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -152(%rbp)
	je	.L108
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-176(%rbp), %r13
	movq	%r13, -192(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L157
	movq	%rcx, -192(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L101:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -184(%rbp)
	movq	%rdx, (%rax)
	movq	-192(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-120(%rbp), %rdx
	movq	-184(%rbp), %r8
	cmpq	%r13, %r9
	movq	%rax, %rdi
	movq	-128(%rbp), %rsi
	cmovne	-176(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L103
	leaq	-112(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-112(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L158
.L103:
	leaq	-192(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L105:
	leaq	-208(%rbp), %rsi
	leaq	16(%rax), %rdx
	movq	%rsi, -224(%rbp)
	movq	(%rax), %rcx
	movq	%rsi, -296(%rbp)
	cmpq	%rdx, %rcx
	je	.L159
	movq	%rcx, -224(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -208(%rbp)
.L107:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -216(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -216(%rbp)
	je	.L108
	movl	$1, %edx
	leaq	-224(%rbp), %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-240(%rbp), %r15
	movq	%r15, -256(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L160
	movq	%rcx, -256(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -240(%rbp)
.L110:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-80(%rbp), %r12
	movq	%rcx, -248(%rbp)
	movq	%rdx, (%rax)
	movq	-256(%rbp), %r10
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-88(%rbp), %rdx
	movq	-248(%rbp), %r8
	cmpq	%r15, %r10
	movq	%rax, %rdi
	movq	-96(%rbp), %rsi
	cmovne	-240(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L112
	cmpq	%r12, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L161
.L112:
	leaq	-256(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L114:
	leaq	-272(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -288(%rbp)
	movq	(%rax), %rsi
	cmpq	%rdx, %rsi
	je	.L162
	movq	%rsi, -288(%rbp)
	movq	16(%rax), %rsi
	movq	%rsi, -272(%rbp)
.L116:
	movq	8(%rax), %rsi
	movb	$0, 16(%rax)
	movq	%rsi, -280(%rbp)
	movq	%rdx, (%rax)
	movq	-288(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	128(%rbx), %rdi
	cmpq	%rcx, %rdx
	je	.L163
	movq	-272(%rbp), %rsi
	movq	-280(%rbp), %rax
	cmpq	%rdi, -344(%rbp)
	je	.L164
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	144(%rbx), %r8
	movq	%rdx, 128(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 136(%rbx)
	testq	%rdi, %rdi
	je	.L122
	movq	%rdi, -288(%rbp)
	movq	%r8, -272(%rbp)
.L120:
	movq	$0, -280(%rbp)
	movb	$0, (%rdi)
	movq	-288(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L123
	call	_ZdlPv@PLT
.L123:
	movq	-256(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L124
	call	_ZdlPv@PLT
.L124:
	movq	-224(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	-192(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L128
	call	_ZdlPv@PLT
.L128:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	%r14, %rdi
	call	_ZN4node17GetOpenSSLVersionB5cxx11Ev
	movq	-96(%rbp), %rdx
	movq	352(%rbx), %rdi
	cmpq	%r12, %rdx
	je	.L165
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -336(%rbp)
	je	.L166
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	368(%rbx), %rsi
	movq	%rdx, 352(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 360(%rbx)
	testq	%rdi, %rdi
	je	.L135
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L133:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	movq	424(%rbx), %rdx
	xorl	%esi, %esi
	movl	$4, %r8d
	leaq	.LC11(%rip), %rcx
	leaq	416(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	488(%rbx), %rdx
	xorl	%esi, %esi
	movq	-328(%rbp), %rdi
	movl	$4, %r8d
	leaq	.LC12(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	%rdx, 128(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 136(%rbx)
.L122:
	movq	%rcx, -288(%rbp)
	leaq	-272(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L159:
	movdqu	16(%rax), %xmm4
	movaps	%xmm4, -208(%rbp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L157:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -176(%rbp)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L160:
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -240(%rbp)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L162:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -272(%rbp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L165:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L131
	cmpq	$1, %rdx
	je	.L168
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	352(%rbx), %rdi
.L131:
	movq	%rdx, 360(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-280(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L118
	cmpq	$1, %rdx
	je	.L169
	movq	%rcx, %rsi
	movq	%rcx, -304(%rbp)
	call	memcpy@PLT
	movq	-280(%rbp), %rdx
	movq	128(%rbx), %rdi
	movq	-304(%rbp), %rcx
.L118:
	movq	%rdx, 136(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-288(%rbp), %rdi
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdx, 352(%rbx)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 360(%rbx)
.L135:
	movq	%r12, -96(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L169:
	movzbl	-272(%rbp), %eax
	movb	%al, (%rdi)
	movq	-280(%rbp), %rdx
	movq	128(%rbx), %rdi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L168:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	352(%rbx), %rdi
	jmp	.L131
.L108:
	leaq	.LC9(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4290:
	.size	_ZN4node8Metadata8VersionsC2Ev, .-_ZN4node8Metadata8VersionsC2Ev
	.globl	_ZN4node8Metadata8VersionsC1Ev
	.set	_ZN4node8Metadata8VersionsC1Ev,_ZN4node8Metadata8VersionsC2Ev
	.section	.rodata.str1.1
.LC13:
	.string	"Erbium"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"https://nodejs.org/download/release/v12.18.4/node-v12.18.4.tar.gz"
	.align 8
.LC15:
	.string	"https://nodejs.org/download/release/v12.18.4/node-v12.18.4-headers.tar.gz"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8Metadata7ReleaseC2Ev
	.type	_ZN4node8Metadata7ReleaseC2Ev, @function
_ZN4node8Metadata7ReleaseC2Ev:
.LFB4293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movl	$6, %r8d
	xorl	%edx, %edx
	leaq	.LC13(%rip), %rcx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	32(%rdi), %rdi
	leaq	64(%rbx), %r13
	leaq	96(%rbx), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	leaq	48(%rbx), %rax
	movl	$1701080942, -16(%rdi)
	movq	$4, -24(%rdi)
	movb	$0, -12(%rdi)
	movq	%rax, 32(%rbx)
	leaq	80(%rbx), %rax
	movq	%rax, 64(%rbx)
	leaq	112(%rbx), %rax
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	$0, 72(%rbx)
	movb	$0, 80(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 104(%rbx)
	movb	$0, 112(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	72(%rbx), %rdx
	movq	%r13, %rdi
	xorl	%esi, %esi
	movl	$65, %r8d
	leaq	.LC14(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	104(%rbx), %rdx
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movl	$73, %r8d
	popq	%r12
	xorl	%esi, %esi
	popq	%r13
	leaq	.LC15(%rip), %rcx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	.cfi_endproc
.LFE4293:
	.size	_ZN4node8Metadata7ReleaseC2Ev, .-_ZN4node8Metadata7ReleaseC2Ev
	.globl	_ZN4node8Metadata7ReleaseC1Ev
	.set	_ZN4node8Metadata7ReleaseC1Ev,_ZN4node8Metadata7ReleaseC2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node8MetadataC2Ev
	.type	_ZN4node8MetadataC2Ev, @function
_ZN4node8MetadataC2Ev:
.LFB4302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node8Metadata8VersionsC1Ev
	leaq	512(%rbx), %rdi
	call	_ZN4node8Metadata7ReleaseC1Ev
	leaq	656(%rbx), %rax
	movb	$52, 658(%rbx)
	movq	%rax, 640(%rbx)
	movl	$13944, %eax
	movw	%ax, 656(%rbx)
	leaq	688(%rbx), %rax
	movq	$3, 648(%rbx)
	movb	$0, 659(%rbx)
	movq	%rax, 672(%rbx)
	movl	$1970170220, 688(%rbx)
	movb	$120, 692(%rbx)
	movq	$5, 680(%rbx)
	movb	$0, 693(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4302:
	.size	_ZN4node8MetadataC2Ev, .-_ZN4node8MetadataC2Ev
	.globl	_ZN4node8MetadataC1Ev
	.set	_ZN4node8MetadataC1Ev,_ZN4node8MetadataC2Ev
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11per_process8metadataE, @function
_GLOBAL__sub_I__ZN4node11per_process8metadataE:
.LFB5384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_process8metadataE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node8MetadataC1Ev
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node11per_process8metadataE(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZN4node8MetadataD1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5384:
	.size	_GLOBAL__sub_I__ZN4node11per_process8metadataE, .-_GLOBAL__sub_I__ZN4node11per_process8metadataE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11per_process8metadataE
	.globl	_ZN4node11per_process8metadataE
	.bss
	.align 32
	.type	_ZN4node11per_process8metadataE, @object
	.size	_ZN4node11per_process8metadataE, 704
_ZN4node11per_process8metadataE:
	.zero	704
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
