	.file	"inspector_profiler.cc"
	.text
	.section	.rodata._ZNK4node8profiler20V8CoverageConnection4typeEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"coverage"
	.section	.text._ZNK4node8profiler20V8CoverageConnection4typeEv,"axG",@progbits,_ZNK4node8profiler20V8CoverageConnection4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8profiler20V8CoverageConnection4typeEv
	.type	_ZNK4node8profiler20V8CoverageConnection4typeEv, @function
_ZNK4node8profiler20V8CoverageConnection4typeEv:
.LFB4015:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE4015:
	.size	_ZNK4node8profiler20V8CoverageConnection4typeEv, .-_ZNK4node8profiler20V8CoverageConnection4typeEv
	.section	.text._ZNK4node8profiler20V8CoverageConnection6endingEv,"axG",@progbits,_ZNK4node8profiler20V8CoverageConnection6endingEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8profiler20V8CoverageConnection6endingEv
	.type	_ZNK4node8profiler20V8CoverageConnection6endingEv, @function
_ZNK4node8profiler20V8CoverageConnection6endingEv:
.LFB4016:
	.cfi_startproc
	endbr64
	movzbl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE4016:
	.size	_ZNK4node8profiler20V8CoverageConnection6endingEv, .-_ZNK4node8profiler20V8CoverageConnection6endingEv
	.section	.rodata._ZNK4node8profiler23V8CpuProfilerConnection4typeEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"CPU"
	.section	.text._ZNK4node8profiler23V8CpuProfilerConnection4typeEv,"axG",@progbits,_ZNK4node8profiler23V8CpuProfilerConnection4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8profiler23V8CpuProfilerConnection4typeEv
	.type	_ZNK4node8profiler23V8CpuProfilerConnection4typeEv, @function
_ZNK4node8profiler23V8CpuProfilerConnection4typeEv:
.LFB4020:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE4020:
	.size	_ZNK4node8profiler23V8CpuProfilerConnection4typeEv, .-_ZNK4node8profiler23V8CpuProfilerConnection4typeEv
	.section	.text._ZNK4node8profiler23V8CpuProfilerConnection6endingEv,"axG",@progbits,_ZNK4node8profiler23V8CpuProfilerConnection6endingEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8profiler23V8CpuProfilerConnection6endingEv
	.type	_ZNK4node8profiler23V8CpuProfilerConnection6endingEv, @function
_ZNK4node8profiler23V8CpuProfilerConnection6endingEv:
.LFB4021:
	.cfi_startproc
	endbr64
	movzbl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE4021:
	.size	_ZNK4node8profiler23V8CpuProfilerConnection6endingEv, .-_ZNK4node8profiler23V8CpuProfilerConnection6endingEv
	.section	.rodata._ZNK4node8profiler24V8HeapProfilerConnection4typeEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"heap"
	.section	.text._ZNK4node8profiler24V8HeapProfilerConnection4typeEv,"axG",@progbits,_ZNK4node8profiler24V8HeapProfilerConnection4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8profiler24V8HeapProfilerConnection4typeEv
	.type	_ZNK4node8profiler24V8HeapProfilerConnection4typeEv, @function
_ZNK4node8profiler24V8HeapProfilerConnection4typeEv:
.LFB4025:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE4025:
	.size	_ZNK4node8profiler24V8HeapProfilerConnection4typeEv, .-_ZNK4node8profiler24V8HeapProfilerConnection4typeEv
	.section	.text._ZNK4node8profiler24V8HeapProfilerConnection6endingEv,"axG",@progbits,_ZNK4node8profiler24V8HeapProfilerConnection6endingEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8profiler24V8HeapProfilerConnection6endingEv
	.type	_ZNK4node8profiler24V8HeapProfilerConnection6endingEv, @function
_ZNK4node8profiler24V8HeapProfilerConnection6endingEv:
.LFB4026:
	.cfi_startproc
	endbr64
	movzbl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE4026:
	.size	_ZNK4node8profiler24V8HeapProfilerConnection6endingEv, .-_ZNK4node8profiler24V8HeapProfilerConnection6endingEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8CoverageConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.type	_ZN4node8profiler20V8CoverageConnection10GetProfileEN2v85LocalINS2_6ObjectEEE, @function
_ZN4node8profiler20V8CoverageConnection10GetProfileEN2v85LocalINS2_6ObjectEEE:
.LFB7881:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE7881:
	.size	_ZN4node8profiler20V8CoverageConnection10GetProfileEN2v85LocalINS2_6ObjectEEE, .-_ZN4node8profiler20V8CoverageConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.section	.text._ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD2Ev,"axG",@progbits,_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD2Ev
	.type	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD2Ev, @function
_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD2Ev:
.LFB9315:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9315:
	.size	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD2Ev, .-_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD2Ev
	.weak	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD1Ev
	.set	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD1Ev,_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD2Ev
	.section	.text._ZN4node8profiler20V8CoverageConnectionD2Ev,"axG",@progbits,_ZN4node8profiler20V8CoverageConnectionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler20V8CoverageConnectionD2Ev
	.type	_ZN4node8profiler20V8CoverageConnectionD2Ev, @function
_ZN4node8profiler20V8CoverageConnectionD2Ev:
.LFB9368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8profiler20V8CoverageConnectionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	call	*8(%rax)
.L11:
	movq	8(%rbx), %rdi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9368:
	.size	_ZN4node8profiler20V8CoverageConnectionD2Ev, .-_ZN4node8profiler20V8CoverageConnectionD2Ev
	.weak	_ZN4node8profiler20V8CoverageConnectionD1Ev
	.set	_ZN4node8profiler20V8CoverageConnectionD1Ev,_ZN4node8profiler20V8CoverageConnectionD2Ev
	.section	.text._ZN4node8profiler23V8CpuProfilerConnectionD2Ev,"axG",@progbits,_ZN4node8profiler23V8CpuProfilerConnectionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler23V8CpuProfilerConnectionD2Ev
	.type	_ZN4node8profiler23V8CpuProfilerConnectionD2Ev, @function
_ZN4node8profiler23V8CpuProfilerConnectionD2Ev:
.LFB9385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8profiler23V8CpuProfilerConnectionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L18
	movq	(%rdi), %rax
	call	*8(%rax)
.L18:
	movq	8(%rbx), %rdi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L17
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9385:
	.size	_ZN4node8profiler23V8CpuProfilerConnectionD2Ev, .-_ZN4node8profiler23V8CpuProfilerConnectionD2Ev
	.weak	_ZN4node8profiler23V8CpuProfilerConnectionD1Ev
	.set	_ZN4node8profiler23V8CpuProfilerConnectionD1Ev,_ZN4node8profiler23V8CpuProfilerConnectionD2Ev
	.section	.text._ZN4node8profiler24V8HeapProfilerConnectionD2Ev,"axG",@progbits,_ZN4node8profiler24V8HeapProfilerConnectionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler24V8HeapProfilerConnectionD2Ev
	.type	_ZN4node8profiler24V8HeapProfilerConnectionD2Ev, @function
_ZN4node8profiler24V8HeapProfilerConnectionD2Ev:
.LFB9402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8profiler24V8HeapProfilerConnectionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L25
	movq	(%rdi), %rax
	call	*8(%rax)
.L25:
	movq	8(%rbx), %rdi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L24
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9402:
	.size	_ZN4node8profiler24V8HeapProfilerConnectionD2Ev, .-_ZN4node8profiler24V8HeapProfilerConnectionD2Ev
	.weak	_ZN4node8profiler24V8HeapProfilerConnectionD1Ev
	.set	_ZN4node8profiler24V8HeapProfilerConnectionD1Ev,_ZN4node8profiler24V8HeapProfilerConnectionD2Ev
	.section	.text._ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD0Ev,"axG",@progbits,_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD0Ev
	.type	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD0Ev, @function
_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD0Ev:
.LFB9317:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9317:
	.size	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD0Ev, .-_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD0Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s: Failed to create %s profile directory %s\n"
	.text
	.p2align 4
	.type	_ZN4node8profilerL15EnsureDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc, @function
_ZN4node8profilerL15EnsureDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc:
.LFB7873:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rdi, %rdx
	movl	$511, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-624(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$592, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -184(%rbp)
	call	_ZN4node2fs10MKDirpSyncEP9uv_loop_sP7uv_fs_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiPFvS4_E@PLT
	testl	%eax, %eax
	jns	.L36
	cmpl	$-17, %eax
	jne	.L44
.L36:
	movl	$1, %r13d
.L33:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*8(%rax)
.L32:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$592, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	-176(%rbp), %r14
	movl	$128, %edx
	movl	%eax, %edi
	movq	%r14, %rsi
	call	uv_err_name_r@PLT
	movq	(%rbx), %r9
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	stderr(%rip), %rdi
	leaq	.LC3(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	__fprintf_chk@PLT
	jmp	.L33
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7873:
	.size	_ZN4node8profilerL15EnsureDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc, .-_ZN4node8profilerL15EnsureDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc
	.p2align 4
	.type	_ZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	16(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jg	.L47
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L65
.L49:
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L57
	cmpw	$1040, %cx
	jne	.L50
.L57:
	movq	23(%rdx), %r12
.L52:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L66
	movq	8(%rbx), %r13
.L54:
	movq	3000(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L55
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3000(%r12)
.L55:
	testq	%r13, %r13
	je	.L46
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3000(%r12)
.L46:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L49
.L65:
	leaq	_ZZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L52
	.cfi_endproc
.LFE7908:
	.size	_ZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"profile"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"'profile' from CPU profile result is not an Object\n"
	.align 8
.LC6:
	.string	"'profile' from CPU profile result is undefined\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler23V8CpuProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.type	_ZN4node8profiler23V8CpuProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE, @function
_ZN4node8profiler23V8CpuProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE:
.LFB7887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC4(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L78
.L68:
	movq	24(%rbx), %rax
	movq	%r12, %rdi
	movq	3280(%rax), %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L79
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movl	%eax, %r8d
	movq	%rbx, %rax
	testb	%r8b, %r8b
	je	.L76
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	stderr(%rip), %rcx
	movl	$51, %edx
	movl	$1, %esi
	leaq	.LC5(%rip), %rdi
	call	fwrite@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	stderr(%rip), %rcx
	movl	$47, %edx
	movl	$1, %esi
	leaq	.LC6(%rip), %rdi
	call	fwrite@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rdx
	jmp	.L68
	.cfi_endproc
.LFE7887:
	.size	_ZN4node8profiler23V8CpuProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE, .-_ZN4node8profiler23V8CpuProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"'profile' from heap profile result is not an Object\n"
	.align 8
.LC8:
	.string	"'profile' from heap profile result is undefined\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler24V8HeapProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.type	_ZN4node8profiler24V8HeapProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE, @function
_ZN4node8profiler24V8HeapProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE:
.LFB7892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC4(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L91
.L81:
	movq	24(%rbx), %rax
	movq	%r12, %rdi
	movq	3280(%rax), %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L92
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movl	%eax, %r8d
	movq	%rbx, %rax
	testb	%r8b, %r8b
	je	.L89
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	stderr(%rip), %rcx
	movl	$52, %edx
	movl	$1, %esi
	leaq	.LC7(%rip), %rdi
	call	fwrite@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	stderr(%rip), %rcx
	movl	$48, %edx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdi
	call	fwrite@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rdx
	jmp	.L81
	.cfi_endproc
.LFE7892:
	.size	_ZN4node8profiler24V8HeapProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE, .-_ZN4node8profiler24V8HeapProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.section	.text._ZN4node8profiler20V8CoverageConnectionD0Ev,"axG",@progbits,_ZN4node8profiler20V8CoverageConnectionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler20V8CoverageConnectionD0Ev
	.type	_ZN4node8profiler20V8CoverageConnectionD0Ev, @function
_ZN4node8profiler20V8CoverageConnectionD0Ev:
.LFB9370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8profiler20V8CoverageConnectionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L94
	movq	(%rdi), %rax
	call	*8(%rax)
.L94:
	movq	8(%r12), %rdi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L95
	movq	(%rdi), %rax
	call	*8(%rax)
.L95:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9370:
	.size	_ZN4node8profiler20V8CoverageConnectionD0Ev, .-_ZN4node8profiler20V8CoverageConnectionD0Ev
	.section	.text._ZN4node8profiler23V8CpuProfilerConnectionD0Ev,"axG",@progbits,_ZN4node8profiler23V8CpuProfilerConnectionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler23V8CpuProfilerConnectionD0Ev
	.type	_ZN4node8profiler23V8CpuProfilerConnectionD0Ev, @function
_ZN4node8profiler23V8CpuProfilerConnectionD0Ev:
.LFB9387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8profiler23V8CpuProfilerConnectionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L104
	movq	(%rdi), %rax
	call	*8(%rax)
.L104:
	movq	8(%r12), %rdi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L105
	movq	(%rdi), %rax
	call	*8(%rax)
.L105:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9387:
	.size	_ZN4node8profiler23V8CpuProfilerConnectionD0Ev, .-_ZN4node8profiler23V8CpuProfilerConnectionD0Ev
	.section	.text._ZN4node8profiler24V8HeapProfilerConnectionD0Ev,"axG",@progbits,_ZN4node8profiler24V8HeapProfilerConnectionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8profiler24V8HeapProfilerConnectionD0Ev
	.type	_ZN4node8profiler24V8HeapProfilerConnectionD0Ev, @function
_ZN4node8profiler24V8HeapProfilerConnectionD0Ev:
.LFB9404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8profiler24V8HeapProfilerConnectionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*8(%rax)
.L114:
	movq	8(%r12), %rdi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L115
	movq	(%rdi), %rax
	call	*8(%rax)
.L115:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9404:
	.size	_ZN4node8profiler24V8HeapProfilerConnectionD0Ev, .-_ZN4node8profiler24V8HeapProfilerConnectionD0Ev
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"Failed to parse %s profile result as JSON object\n"
	.section	.rodata.str1.1
.LC10:
	.string	"result"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"'result' from %s profile message is not an object\n"
	.align 8
.LC12:
	.string	"Failed to get 'result' from %s profile message\n"
	.text
	.p2align 4
	.type	_ZN4node8profilerL12ParseProfileEPNS_11EnvironmentEN2v85LocalINS3_6StringEEEPKc, @function
_ZN4node8profilerL12ParseProfileEPNS_11EnvironmentEN2v85LocalINS3_6StringEEEPKc:
.LFB7878:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	3280(%rdi), %r14
	movq	352(%rdi), %r15
	movq	%r14, %rdi
	call	_ZN2v84JSON5ParseENS_5LocalINS_7ContextEEENS1_INS_6StringEEE@PLT
	testq	%rax, %rax
	je	.L132
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L138
.L132:
	movq	%r13, %rcx
	leaq	.LC9(%rip), %rdx
.L137:
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	xorl	%eax, %eax
.L133:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L139
.L126:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L140
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movl	%eax, %r8d
	movq	%r12, %rax
	testb	%r8b, %r8b
	jne	.L133
	movq	%r13, %rcx
	leaq	.LC11(%rip), %rdx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r13, %rcx
	leaq	.LC12(%rip), %rdx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L126
	.cfi_endproc
.LFE7878:
	.size	_ZN4node8profilerL12ParseProfileEPNS_11EnvironmentEN2v85LocalINS3_6StringEEEPKc, .-_ZN4node8profilerL12ParseProfileEPNS_11EnvironmentEN2v85LocalINS3_6StringEEEPKc
	.section	.rodata.str1.1
.LC13:
	.string	"setCoverageDirectory"
.LC14:
	.string	"setSourceMapCacheGetter"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB15:
	.text
.LHOTB15:
	.p2align 4
	.type	_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L142
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L142
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L142
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L153
.L143:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L154
.L144:
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L155
.L145:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L156
.L146:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L157
.L147:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L158
.L148:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L154:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L155:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L156:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L157:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L158:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L148
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7909:
.L142:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7909:
	.text
	.size	_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE15:
	.text
.LHOTE15:
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB10867:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L160
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L161
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L161:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L162
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L163
.L162:
	testq	%rbx, %rbx
	je	.L163
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L163:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L164
	call	__stack_chk_fail@PLT
.L160:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L164:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10867:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.text
	.p2align 4
	.type	_ZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1112, %rsp
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L171
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L209
.L173:
	leaq	_ZZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L173
.L209:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L173
	movq	(%rbx), %rdi
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L195
	cmpw	$1040, %si
	jne	.L175
.L195:
	movq	23(%rcx), %r13
.L177:
	testl	%edx, %edx
	jle	.L210
	movq	8(%rbx), %rdx
.L179:
	movq	352(%r13), %rsi
	leaq	-1104(%rbp), %rdi
	leaq	-1120(%rbp), %rbx
	leaq	-1136(%rbp), %r14
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r15
	movq	%rbx, -1136(%rbp)
	testq	%r15, %r15
	je	.L211
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -1144(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L212
	cmpq	$1, %rax
	jne	.L183
	movzbl	(%r15), %edx
	movb	%dl, -1120(%rbp)
	movq	%rbx, %rdx
.L184:
	movq	%rax, -1128(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-1136(%rbp), %rdx
	movq	1456(%r13), %rdi
	cmpq	%rbx, %rdx
	je	.L213
	leaq	1472(%r13), %rsi
	movq	-1128(%rbp), %rax
	movq	-1120(%rbp), %rcx
	cmpq	%rsi, %rdi
	je	.L214
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	1472(%r13), %rsi
	movq	%rdx, 1456(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 1464(%r13)
	testq	%rdi, %rdi
	je	.L190
	movq	%rdi, -1136(%rbp)
	movq	%rsi, -1120(%rbp)
.L188:
	movq	$0, -1128(%rbp)
	movb	$0, (%rdi)
	movq	-1136(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L170
	testq	%rdi, %rdi
	je	.L170
	call	free@PLT
.L170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L215
	addq	$1112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L183:
	testq	%rax, %rax
	jne	.L216
	movq	%rbx, %rdx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r14, %rdi
	leaq	-1144(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1136(%rbp)
	movq	%rax, %rdi
	movq	-1144(%rbp), %rax
	movq	%rax, -1120(%rbp)
.L182:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-1144(%rbp), %rax
	movq	-1136(%rbp), %rdx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-1128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L186
	cmpq	$1, %rdx
	je	.L217
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1128(%rbp), %rdx
	movq	1456(%r13), %rdi
.L186:
	movq	%rdx, 1464(%r13)
	movb	$0, (%rdi,%rdx)
	movq	-1136(%rbp), %rdi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movl	16(%rbx), %edx
	movq	%rax, %r13
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 1456(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 1464(%r13)
.L190:
	movq	%rbx, -1136(%rbp)
	leaq	-1120(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L217:
	movzbl	-1120(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1128(%rbp), %rdx
	movq	1456(%r13), %rdi
	jmp	.L186
.L215:
	call	__stack_chk_fail@PLT
.L216:
	movq	%rbx, %rdi
	jmp	.L182
	.cfi_endproc
.LFE7904:
	.size	_ZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node8profiler24V8HeapProfilerConnection11GetFilenameB5cxx11Ev
	.type	_ZNK4node8profiler24V8HeapProfilerConnection11GetFilenameB5cxx11Ev, @function
_ZNK4node8profiler24V8HeapProfilerConnection11GetFilenameB5cxx11Ev:
.LFB7891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	%rdi, (%r12)
	movq	1600(%rax), %r14
	movq	1608(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L219
	testq	%r14, %r14
	je	.L235
.L219:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L236
	cmpq	$1, %r13
	jne	.L222
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L223:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L223
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L221:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L223
.L235:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7891:
	.size	_ZNK4node8profiler24V8HeapProfilerConnection11GetFilenameB5cxx11Ev, .-_ZNK4node8profiler24V8HeapProfilerConnection11GetFilenameB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node8profiler20V8CoverageConnection12GetDirectoryB5cxx11Ev
	.type	_ZNK4node8profiler20V8CoverageConnection12GetDirectoryB5cxx11Ev, @function
_ZNK4node8profiler20V8CoverageConnection12GetDirectoryB5cxx11Ev:
.LFB7882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	%rdi, (%r12)
	movq	1456(%rax), %r14
	movq	1464(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L239
	testq	%r14, %r14
	je	.L255
.L239:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L256
	cmpq	$1, %r13
	jne	.L242
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L243:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L243
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L241:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L243
.L255:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7882:
	.size	_ZNK4node8profiler20V8CoverageConnection12GetDirectoryB5cxx11Ev, .-_ZNK4node8profiler20V8CoverageConnection12GetDirectoryB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node8profiler23V8CpuProfilerConnection12GetDirectoryB5cxx11Ev
	.type	_ZNK4node8profiler23V8CpuProfilerConnection12GetDirectoryB5cxx11Ev, @function
_ZNK4node8profiler23V8CpuProfilerConnection12GetDirectoryB5cxx11Ev:
.LFB7885:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	%rdi, (%r12)
	movq	1488(%rax), %r14
	movq	1496(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L259
	testq	%r14, %r14
	je	.L275
.L259:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L276
	cmpq	$1, %r13
	jne	.L262
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L263:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L263
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L261:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L263
.L275:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7885:
	.size	_ZNK4node8profiler23V8CpuProfilerConnection12GetDirectoryB5cxx11Ev, .-_ZNK4node8profiler23V8CpuProfilerConnection12GetDirectoryB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node8profiler24V8HeapProfilerConnection12GetDirectoryB5cxx11Ev
	.type	_ZNK4node8profiler24V8HeapProfilerConnection12GetDirectoryB5cxx11Ev, @function
_ZNK4node8profiler24V8HeapProfilerConnection12GetDirectoryB5cxx11Ev:
.LFB7890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	%rdi, (%r12)
	movq	1568(%rax), %r14
	movq	1576(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L279
	testq	%r14, %r14
	je	.L295
.L279:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L296
	cmpq	$1, %r13
	jne	.L282
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L283:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L297
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L283
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L281:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L283
.L295:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L297:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7890:
	.size	_ZNK4node8profiler24V8HeapProfilerConnection12GetDirectoryB5cxx11Ev, .-_ZNK4node8profiler24V8HeapProfilerConnection12GetDirectoryB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node8profiler23V8CpuProfilerConnection11GetFilenameB5cxx11Ev
	.type	_ZNK4node8profiler23V8CpuProfilerConnection11GetFilenameB5cxx11Ev, @function
_ZNK4node8profiler23V8CpuProfilerConnection11GetFilenameB5cxx11Ev:
.LFB7886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	%rdi, (%r12)
	movq	1520(%rax), %r14
	movq	1528(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L299
	testq	%r14, %r14
	je	.L315
.L299:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L316
	cmpq	$1, %r13
	jne	.L302
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L303:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L317
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L303
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L301:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L303
.L315:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7886:
	.size	_ZNK4node8profiler23V8CpuProfilerConnection11GetFilenameB5cxx11Ev, .-_ZNK4node8profiler23V8CpuProfilerConnection11GetFilenameB5cxx11Ev
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB10880:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L319
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L319:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rdx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rdx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L321
.L335:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L335
.L321:
	andl	$4095, %edx
	subq	%rdx, %rsp
	testq	%rdx, %rdx
	jne	.L336
.L322:
	leaq	15(%rsp), %r14
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movl	$1, %edx
	andq	$-16, %r14
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	leaq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	$-1, %rcx
	movl	$32, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L337
	cmpq	$1, %r13
	jne	.L325
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L326:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L326
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L324:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L336:
	orq	$0, -8(%rsp,%rdx)
	jmp	.L322
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10880:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.1
.LC17:
	.string	"%lu"
.LC18:
	.string	"%d"
.LC21:
	.string	"coverage-%s-%s-%s.json"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node8profiler20V8CoverageConnection11GetFilenameB5cxx11Ev
	.type	_ZNK4node8profiler20V8CoverageConnection11GetFilenameB5cxx11Ev, @function
_ZNK4node8profiler20V8CoverageConnection11GetFilenameB5cxx11Ev:
.LFB7877:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC17(%rip), %rcx
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-1168(%rbp), %rdi
	pushq	%rbx
	subq	$1160, %rsp
	.cfi_offset 3, -40
	movq	vsnprintf@GOTPCREL(%rip), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rsi), %rax
	movq	%r13, %rsi
	movq	1936(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	call	uv_os_getpid@PLT
	leaq	-1136(%rbp), %rdi
	movl	$16, %edx
	movq	%r13, %rsi
	movl	%eax, %r8d
	leaq	.LC18(%rip), %rcx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	call	_ZN4node28GetCurrentTimeInMicrosecondsEv@PLT
	movsd	.LC20(%rip), %xmm1
	divsd	.LC19(%rip), %xmm0
	leaq	-1104(%rbp), %rdi
	comisd	%xmm1, %xmm0
	jnb	.L340
	cvttsd2siq	%xmm0, %r8
.L341:
	movq	%r13, %rsi
	leaq	.LC17(%rip), %rcx
	movl	$32, %edx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	pushq	-1168(%rbp)
	xorl	%eax, %eax
	leaq	-1072(%rbp), %r13
	movl	$1024, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r13, %rbx
	pushq	-1104(%rbp)
	movq	-1136(%rbp), %r9
	movl	$1024, %esi
	leaq	.LC21(%rip), %r8
	call	__snprintf_chk@PLT
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
.L342:
	movl	(%rbx), %ecx
	addq	$4, %rbx
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L342
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rbx), %rcx
	cmove	%rcx, %rbx
	movl	%edx, %esi
	addb	%dl, %sil
	popq	%rdx
	popq	%rcx
	sbbq	$3, %rbx
	subq	%r13, %rbx
	movq	%rbx, -1176(%rbp)
	cmpq	$15, %rbx
	ja	.L368
	cmpq	$1, %rbx
	jne	.L346
	movzbl	-1072(%rbp), %edx
	movb	%dl, 16(%r12)
.L347:
	movq	%rbx, 8(%r12)
	movb	$0, (%rax,%rbx)
	movq	-1104(%rbp), %rdi
	leaq	-1088(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movq	-1168(%rbp), %rdi
	leaq	-1152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %r8
	btcq	$63, %r8
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L346:
	testq	%rbx, %rbx
	je	.L347
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L368:
	xorl	%edx, %edx
	leaq	-1176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1176(%rbp), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
.L345:
	movl	%ebx, %ecx
	cmpl	$8, %ebx
	jnb	.L348
	andl	$4, %ebx
	jne	.L370
	testl	%ecx, %ecx
	je	.L349
	movzbl	0(%r13), %edx
	movb	%dl, (%rax)
	testb	$2, %cl
	jne	.L371
.L349:
	movq	-1176(%rbp), %rbx
	movq	(%r12), %rax
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L348:
	movq	-1072(%rbp), %rdx
	leaq	8(%rax), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rbx,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L370:
	movl	0(%r13), %edx
	movl	%edx, (%rax)
	movl	-4(%r13,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L349
.L371:
	movzwl	-2(%r13,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L349
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7877:
	.size	_ZNK4node8profiler20V8CoverageConnection11GetFilenameB5cxx11Ev, .-_ZNK4node8profiler20V8CoverageConnection11GetFilenameB5cxx11Ev
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L372
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L376:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L374
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L376
.L372:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L376
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB7140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L379
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L408
	cmpq	$1, %rax
	jne	.L382
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L383:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L378:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L383
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L381:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L379:
	cmpb	$37, 1(%rax)
	jne	.L410
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L411
	cmpq	$1, %r13
	jne	.L388
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L389:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L391
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L412
.L391:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L393:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L413
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L395:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L378
	call	_ZdlPv@PLT
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L388:
	testq	%r13, %r13
	jne	.L414
	movq	%rbx, %rax
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L411:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L387:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L413:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L393
.L410:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L409:
	call	__stack_chk_fail@PLT
.L414:
	movq	%rbx, %rdi
	jmp	.L387
	.cfi_endproc
.LFE7140:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE
	.type	_ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE, @function
_ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE:
.LFB7868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	2080(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	8(%rbx), %rdi
	leaq	-48(%rbp), %rdx
	movq	%r13, %rsi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE(%rip), %rcx
	movq	%rbx, 8(%rax)
	movq	%rcx, (%rax)
	xorl	%ecx, %ecx
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L416
	movq	(%rdi), %rax
	call	*8(%rax)
.L416:
	movq	$1, 16(%rbx)
	movq	%r12, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L422
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L422:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7868:
	.size	_ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE, .-_ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE
	.globl	_ZN4node8profiler20V8ProfilerConnectionC1EPNS_11EnvironmentE
	.set	_ZN4node8profiler20V8ProfilerConnectionC1EPNS_11EnvironmentE,_ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE
	.p2align 4
	.globl	_ZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentE
	.type	_ZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentE, @function
_ZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentE:
.LFB7896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-4144(%rbp), %r13
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%r13, %rdi
	leaq	-4160(%rbp), %rsi
	movq	$4096, -4160(%rbp)
	call	uv_cwd@PLT
	testl	%eax, %eax
	jne	.L424
	cmpq	$0, -4160(%rbp)
	je	.L464
	leaq	16(%r12), %rax
	movq	%r13, %rbx
	movq	%rax, (%r12)
.L426:
	movl	(%rbx), %ecx
	addq	$4, %rbx
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L426
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rbx), %rcx
	cmove	%rcx, %rbx
	movl	%edx, %edi
	addb	%dl, %dil
	sbbq	$3, %rbx
	subq	%r13, %rbx
	movq	%rbx, -4152(%rbp)
	cmpq	$15, %rbx
	ja	.L465
	cmpq	$1, %rbx
	jne	.L430
	movzbl	-4144(%rbp), %eax
	movb	%al, 16(%r12)
	movq	(%r12), %rax
.L431:
	movq	%rbx, 8(%r12)
	movb	$0, (%rax,%rbx)
.L423:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L466
	addq	$4128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L431
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	1720(%rbx), %rdi
	movq	$-1, %rdx
	movl	$47, %esi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5rfindEcm@PLT
	movq	1728(%rbx), %r13
	leaq	16(%r12), %rdi
	movq	%rdi, (%r12)
	movq	1720(%rbx), %r14
	cmpq	%r13, %rax
	cmovbe	%rax, %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L437
	testq	%r14, %r14
	je	.L467
.L437:
	movq	%r13, -4152(%rbp)
	cmpq	$15, %r13
	ja	.L468
	cmpq	$1, %r13
	jne	.L440
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L441:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L440:
	testq	%r13, %r13
	je	.L441
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L465:
	xorl	%edx, %edx
	leaq	-4152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-4152(%rbp), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
.L429:
	movl	%ebx, %ecx
	cmpl	$8, %ebx
	jnb	.L432
	andl	$4, %ebx
	jne	.L469
	testl	%ecx, %ecx
	je	.L433
	movzbl	0(%r13), %edx
	movb	%dl, (%rax)
	testb	$2, %cl
	je	.L433
	movzwl	-2(%r13,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	.p2align 4,,10
	.p2align 3
.L433:
	movq	-4152(%rbp), %rbx
	movq	(%r12), %rax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%r12, %rdi
	leaq	-4152(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-4152(%rbp), %rax
	movq	%rax, 16(%r12)
.L439:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-4152(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L432:
	movq	-4144(%rbp), %rdx
	leaq	8(%rax), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rbx,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	_ZZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L469:
	movl	0(%r13), %edx
	movl	%edx, (%rax)
	movl	-4(%r13,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L433
.L466:
	call	__stack_chk_fail@PLT
.L467:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7896:
	.size	_ZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentE, .-_ZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentE
	.p2align 4
	.globl	_Z18_register_profilerv
	.type	_Z18_register_profilerv, @function
_Z18_register_profilerv:
.LFB7910:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7910:
	.size	_Z18_register_profilerv, .-_Z18_register_profilerv
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L478
	movq	16(%rdi), %r10
.L472:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L473
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L479
	movq	16(%r9), %r10
.L474:
	cmpq	%r10, %rax
	jbe	.L481
.L473:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L475:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L482
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L477:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L482:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L478:
	movl	$15, %r10d
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L479:
	movl	$15, %r10d
	jmp	.L474
	.cfi_endproc
.LFE8419:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB8793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L484
	testq	%r14, %r14
	je	.L500
.L484:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L501
	cmpq	$1, %r13
	jne	.L487
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L488:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L502
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L488
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L501:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L486:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L488
.L500:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L502:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8793:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB9970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L504
	testq	%rsi, %rsi
	je	.L520
.L504:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L521
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L507
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L508:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L522
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L508
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L521:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L506:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L508
.L520:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L522:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9970:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB10160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L527
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L525:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L525
	.cfi_endproc
.LFE10160:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC22:
	.string	"(null)"
.LC23:
	.string	"lz"
.LC24:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB9647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L529
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L529:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC23(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-184(%rbp), %r9
.L530:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L530
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L531
	cmpb	$99, %dl
	jg	.L532
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L533
	cmpb	$88, %dl
	je	.L534
	jmp	.L531
.L532:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L531
	leaq	.L536(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L536:
	.long	.L535-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L535-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L535-.L536
	.long	.L538-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L535-.L536
	.long	.L531-.L536
	.long	.L535-.L536
	.long	.L531-.L536
	.long	.L531-.L536
	.long	.L535-.L536
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L533:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L570
	jmp	.L567
.L531:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L567
.L570:
	call	_ZdlPv@PLT
	jmp	.L567
.L535:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L550
	leaq	.LC22(%rip), %rsi
.L550:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L562
	jmp	.L547
.L534:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L552
	leaq	.LC22(%rip), %rsi
.L552:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L553
	call	_ZdlPv@PLT
.L553:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L547
.L562:
	call	_ZdlPv@PLT
	jmp	.L547
.L538:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC24(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L555
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L555:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L547:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L567:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L558
	call	__stack_chk_fail@PLT
.L558:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9647:
	.size	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB9198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L572
	call	__stack_chk_fail@PLT
.L572:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9198:
	.size	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_:
.LFB8570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L576
	call	__stack_chk_fail@PLT
.L576:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8570:
	.size	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.section	.rodata.str1.1
.LC25:
	.string	"source-map-cache"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"Failed to stringify %s profile result\n"
	.section	.rodata.str1.1
.LC27:
	.string	"%s: Failed to write file %s\n"
.LC28:
	.string	"Written result to %s\n"
	.section	.text.unlikely
	.align 2
.LCOLDB29:
	.text
.LHOTB29:
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.type	_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE, @function
_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE:
.LFB7880:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	%rbx, %rdi
	movq	352(%rax), %r15
	movq	3280(%rax), %r14
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	24(%r12), %rdi
	cmpq	$0, 3000(%rdi)
	je	.L592
	movq	(%r12), %rax
	leaq	_ZNK4node8profiler20V8CoverageConnection4typeEv(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L662
.L580:
	movq	%r13, %rsi
	call	_ZN4node8profilerL12ParseProfileEPNS_11EnvironmentEN2v85LocalINS3_6StringEEEPKc
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L592
	movq	(%r12), %rax
	leaq	_ZN4node8profiler20V8CoverageConnection10GetProfileEN2v85LocalINS2_6ObjectEEE(%rip), %rdx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L663
.L583:
	movq	24(%r12), %rax
	movq	352(%rax), %rsi
	movq	%rax, -464(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	leaq	-416(%rbp), %r9
	movq	-464(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r9, -472(%rbp)
	movq	%rax, -336(%rbp)
	movl	$0, -328(%rbp)
	call	_ZN2v87Isolate29AllowJavascriptExecutionScopeC1EPS0_@PLT
	movq	24(%r12), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	88(%r15), %rdx
	movq	%r14, %rsi
	movq	3000(%rax), %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	-472(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -464(%rbp)
	je	.L664
	movq	%r9, %rdi
	call	_ZN2v87Isolate29AllowJavascriptExecutionScopeD1Ev@PLT
	movq	-456(%rbp), %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L665
.L585:
	movq	-456(%rbp), %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-464(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L666
.L586:
	xorl	%edx, %edx
	movl	$16, %ecx
	leaq	.LC25(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L667
.L588:
	movq	-464(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L668
.L587:
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v84JSON9StringifyENS_5LocalINS_7ContextEEENS1_INS_5ValueEEENS1_INS_6StringEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L669
	movq	(%r12), %rax
	leaq	_ZNK4node8profiler20V8CoverageConnection12GetDirectoryB5cxx11Ev(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L670
	movq	24(%r12), %rdx
	leaq	-304(%rbp), %rax
	movq	%rax, -320(%rbp)
	movq	1456(%rdx), %r8
	movq	1464(%rdx), %r15
	movq	%r8, %rcx
	addq	%r15, %rcx
	je	.L593
	testq	%r8, %r8
	je	.L602
.L593:
	movq	%r15, -384(%rbp)
	cmpq	$15, %r15
	ja	.L671
	cmpq	$1, %r15
	jne	.L596
	movzbl	(%r8), %edx
	leaq	-320(%rbp), %r9
	movb	%dl, -304(%rbp)
.L597:
	movq	%r15, -312(%rbp)
	movb	$0, (%rax,%r15)
.L598:
	movq	(%r12), %rax
	leaq	_ZNK4node8profiler20V8CoverageConnection4typeEv(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L672
.L599:
	movq	%r9, %rdi
	call	_ZN4node8profilerL15EnsureDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc
	testb	%al, %al
	je	.L614
	movq	(%r12), %rax
	leaq	-288(%rbp), %rdi
	movq	%r12, %rsi
	call	*56(%rax)
	movq	-320(%rbp), %r8
	movq	-312(%rbp), %r15
	leaq	-208(%rbp), %r9
	movq	%r9, -224(%rbp)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L633
	testq	%r8, %r8
	je	.L602
.L633:
	movq	%r15, -384(%rbp)
	cmpq	$15, %r15
	ja	.L673
	cmpq	$1, %r15
	jne	.L606
	movzbl	(%r8), %eax
	leaq	-224(%rbp), %r10
	movb	%al, -208(%rbp)
	movq	%r9, %rax
.L607:
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$47, %r8d
	movq	%r15, -216(%rbp)
	movb	$0, (%rax,%r15)
	movq	-216(%rbp), %rsi
	leaq	-240(%rbp), %r15
	movq	%r9, -472(%rbp)
	movq	%r10, -464(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-464(%rbp), %r10
	movq	-280(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r15, -256(%rbp)
	movq	-472(%rbp), %r9
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L674
	movq	%rcx, -256(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -240(%rbp)
.L609:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -248(%rbp)
	movq	%rdx, (%rax)
	movq	-224(%rbp), %rdi
	movq	$0, 8(%rax)
	cmpq	%r9, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movq	24(%r12), %r12
	movq	-256(%rbp), %rsi
	movq	%r13, %rdx
	movq	352(%r12), %rdi
	movq	%rsi, -384(%rbp)
	call	_ZN4node13WriteFileSyncEPN2v87IsolateEPKcNS0_5LocalINS0_6StringEEE@PLT
	testl	%eax, %eax
	jne	.L675
	cmpb	$0, 2257(%r12)
	jne	.L652
.L612:
	movq	-256(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L613
	call	_ZdlPv@PLT
.L613:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L614
	call	_ZdlPv@PLT
.L614:
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L592
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L676
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L592
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L666:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L586
	cmpl	$5, 43(%rax)
	je	.L587
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%r9, %rdi
	call	_ZN2v87Isolate29AllowJavascriptExecutionScopeD1Ev@PLT
	movq	-456(%rbp), %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L671:
	leaq	-320(%rbp), %r9
	movq	-456(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -472(%rbp)
	movq	%r9, %rdi
	movq	%r9, -464(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-464(%rbp), %r9
	movq	-472(%rbp), %r8
	movq	%rax, -320(%rbp)
	movq	%rax, %rdi
	movq	-384(%rbp), %rax
	movq	%rax, -304(%rbp)
.L595:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r9, -464(%rbp)
	call	memcpy@PLT
	movq	-384(%rbp), %r15
	movq	-320(%rbp), %rax
	movq	-464(%rbp), %r9
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%r12, %rdi
	call	*%rax
	movq	24(%r12), %rdi
	movq	%rax, %rdx
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L665:
	movq	-456(%rbp), %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L585
	movq	-456(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE@PLT
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L596:
	testq	%r15, %r15
	jne	.L677
	leaq	-320(%rbp), %r9
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L606:
	testq	%r15, %r15
	jne	.L678
	movq	%r9, %rax
	leaq	-224(%rbp), %r10
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%r12), %rax
	leaq	_ZNK4node8profiler20V8CoverageConnection4typeEv(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movq	32(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L679
.L619:
	movq	stderr(%rip), %rdi
	leaq	.LC26(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L673:
	leaq	-224(%rbp), %r10
	movq	-456(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -480(%rbp)
	movq	%r10, %rdi
	movq	%r8, -472(%rbp)
	movq	%r10, -464(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-464(%rbp), %r10
	movq	-472(%rbp), %r8
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-384(%rbp), %rax
	movq	-480(%rbp), %r9
	movq	%rax, -208(%rbp)
.L605:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r9, -472(%rbp)
	movq	%r10, -464(%rbp)
	call	memcpy@PLT
	movq	-384(%rbp), %r15
	movq	-224(%rbp), %rax
	movq	-464(%rbp), %r10
	movq	-472(%rbp), %r9
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L670:
	leaq	-320(%rbp), %r9
	movq	%r12, %rsi
	movq	%r9, -464(%rbp)
	movq	%r9, %rdi
	call	*%rax
	movq	-464(%rbp), %r9
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L672:
	movq	%r9, -464(%rbp)
	movq	%r12, %rdi
	call	*%rax
	movq	-464(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L674:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -240(%rbp)
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L675:
	leaq	-192(%rbp), %r12
	movl	$128, %edx
	movl	%eax, %edi
	movq	%r12, %rsi
	call	uv_err_name_r@PLT
	movq	%r12, %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	-384(%rbp), %r8
	movq	stderr(%rip), %rdi
	leaq	.LC27(%rip), %rdx
	call	__fprintf_chk@PLT
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%rax, -472(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-472(%rbp), %rdx
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L668:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L619
.L676:
	call	__stack_chk_fail@PLT
.L602:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L678:
	movq	%r9, %rdi
	leaq	-224(%rbp), %r10
	jmp	.L605
.L677:
	movq	%rax, %rdi
	leaq	-320(%rbp), %r9
	jmp	.L595
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold, @function
_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold:
.LFSB7880:
.L652:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-456(%rbp), %rdx
	movq	stderr(%rip), %rdi
	leaq	.LC28(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	jmp	.L612
	.cfi_endproc
.LFE7880:
	.text
	.size	_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE, .-_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.section	.text.unlikely
	.size	_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold, .-_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold
.LCOLDE29:
	.text
.LHOTE29:
	.section	.text.unlikely
	.align 2
.LCOLDB30:
	.text
.LHOTB30:
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.type	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE, @function
_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE:
.LFB7879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	3280(%rax), %r14
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	24(%r12), %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	call	_ZN4node8profilerL12ParseProfileEPNS_11EnvironmentEN2v85LocalINS3_6StringEEEPKc
	testq	%rax, %rax
	je	.L680
	movq	%rax, %rsi
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*64(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L680
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v84JSON9StringifyENS_5LocalINS_7ContextEEENS1_INS_5ValueEEENS1_INS_6StringEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L725
	movq	(%r12), %rax
	leaq	-320(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*48(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN4node8profilerL15EnsureDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc
	testb	%al, %al
	je	.L696
	movq	(%r12), %rax
	leaq	-288(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-208(%rbp), %rbx
	call	*56(%rax)
	movq	-320(%rbp), %r8
	movq	-312(%rbp), %r14
	movq	%rbx, -224(%rbp)
	movq	%r8, %rax
	addq	%r14, %rax
	je	.L685
	testq	%r8, %r8
	je	.L726
.L685:
	movq	%r14, -328(%rbp)
	cmpq	$15, %r14
	ja	.L727
	cmpq	$1, %r14
	jne	.L688
	movzbl	(%r8), %eax
	leaq	-224(%rbp), %r15
	movb	%al, -208(%rbp)
	movq	%rbx, %rax
.L689:
	movl	$1, %ecx
	movl	$47, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r14, -216(%rbp)
	movb	$0, (%rax,%r14)
	movq	-216(%rbp), %rsi
	leaq	-240(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-280(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r14, -256(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L728
	movq	%rcx, -256(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -240(%rbp)
.L691:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -248(%rbp)
	movq	%rdx, (%rax)
	movq	-224(%rbp), %rdi
	movq	$0, 8(%rax)
	cmpq	%rbx, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	24(%r12), %rbx
	movq	-256(%rbp), %rsi
	movq	%r13, %rdx
	movq	352(%rbx), %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN4node13WriteFileSyncEPN2v87IsolateEPKcNS0_5LocalINS0_6StringEEE@PLT
	testl	%eax, %eax
	jne	.L729
	cmpb	$0, 2257(%rbx)
	jne	.L717
.L694:
	movq	-256(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L695
	call	_ZdlPv@PLT
.L695:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L696
	call	_ZdlPv@PLT
.L696:
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L680
	call	_ZdlPv@PLT
.L680:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L730
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	movq	stderr(%rip), %rdi
	leaq	.LC26(%rip), %rdx
	movl	$1, %esi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L688:
	testq	%r14, %r14
	jne	.L731
	movq	%rbx, %rax
	leaq	-224(%rbp), %r15
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L727:
	leaq	-224(%rbp), %r15
	leaq	-328(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -344(%rbp)
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-344(%rbp), %r8
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	%rax, -208(%rbp)
.L687:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-328(%rbp), %r14
	movq	-224(%rbp), %rax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L728:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -240(%rbp)
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L729:
	leaq	-192(%rbp), %r12
	movl	$128, %edx
	movl	%eax, %edi
	movq	%r12, %rsi
	call	uv_err_name_r@PLT
	movq	%r12, %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	-328(%rbp), %r8
	movq	stderr(%rip), %rdi
	leaq	.LC27(%rip), %rdx
	call	__fprintf_chk@PLT
	jmp	.L694
.L730:
	call	__stack_chk_fail@PLT
.L731:
	movq	%rbx, %rdi
	leaq	-224(%rbp), %r15
	jmp	.L687
.L726:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold, @function
_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold:
.LFSB7879:
.L717:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-328(%rbp), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	jmp	.L694
	.cfi_endproc
.LFE7879:
	.text
	.size	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE, .-_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.section	.text.unlikely
	.size	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold, .-_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE.cold
.LCOLDE30:
	.text
.LHOTE30:
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB10537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L733
	leaq	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L733:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC23(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-184(%rbp), %r9
.L734:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L734
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L735
	cmpb	$99, %dl
	jg	.L736
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L737
	cmpb	$88, %dl
	je	.L738
	jmp	.L735
.L736:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L735
	leaq	.L740(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L740:
	.long	.L739-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L739-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L739-.L740
	.long	.L742-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L739-.L740
	.long	.L735-.L740
	.long	.L739-.L740
	.long	.L735-.L740
	.long	.L735-.L740
	.long	.L739-.L740
	.section	.text.unlikely._ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L737:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L774
	jmp	.L771
.L735:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L771
.L774:
	call	_ZdlPv@PLT
	jmp	.L771
.L739:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L754
	leaq	.LC22(%rip), %rsi
.L754:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L766
	jmp	.L751
.L738:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L756
	leaq	.LC22(%rip), %rsi
.L756:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L757
	call	_ZdlPv@PLT
.L757:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L751
.L766:
	call	_ZdlPv@PLT
	jmp	.L751
.L742:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC24(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L759
	leaq	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L759:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L751:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L771:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L746
	call	_ZdlPv@PLT
.L746:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L732
	call	_ZdlPv@PLT
.L732:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L762
	call	__stack_chk_fail@PLT
.L762:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10537:
	.size	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB10265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L776
	call	__stack_chk_fail@PLT
.L776:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10265:
	.size	_ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_:
.LFB9881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L778
	call	_ZdlPv@PLT
.L778:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L780
	call	__stack_chk_fail@PLT
.L780:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9881:
	.size	_ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_
	.section	.rodata.str1.1
.LC31:
	.string	"{ \"id\": "
.LC32:
	.string	", \"method\": \""
.LC33:
	.string	", \"params\": "
.LC34:
	.string	" }"
.LC35:
	.string	"Dispatching message %s\n"
	.section	.text.unlikely
	.align 2
.LCOLDB39:
	.text
.LHOTB39:
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	.type	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_, @function
_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_:
.LFB7870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-448(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$504, %rsp
	movq	.LC36(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	.LC37(%rip), %xmm1
	movaps	%xmm1, -528(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	leaq	-432(%rbp), %r12
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r12, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-528(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -536(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	16(%rbx), %rax
	movl	$8, %edx
	movq	%r12, %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, -528(%rbp)
	addq	$1, %rax
	movq	%rax, 16(%rbx)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$13, %edx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r15, %r15
	je	.L798
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L784:
	leaq	-512(%rbp), %r15
	movl	$1, %edx
	movq	%r12, %rdi
	movb	$34, -512(%rbp)
	movq	%r15, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r13, %r13
	je	.L785
	movl	$12, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L785:
	movq	%r12, %rdi
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	leaq	-464(%rbp), %r12
	movq	$0, -472(%rbp)
	movq	%r12, -480(%rbp)
	leaq	-480(%rbp), %rdi
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L786
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L799
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L788:
	movq	24(%rbx), %rax
	movq	-480(%rbp), %r13
	cmpb	$0, 2257(%rax)
	movq	%r13, -512(%rbp)
	jne	.L796
.L789:
	movq	8(%rbx), %rdi
	movq	-472(%rbp), %rdx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movb	$1, -512(%rbp)
	movq	%rdx, -504(%rbp)
	movq	%r13, -496(%rbp)
	call	*%rax
	movq	-480(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L790
	call	_ZdlPv@PLT
.L790:
	movq	.LC36(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC38(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L791
	call	_ZdlPv@PLT
.L791:
	movq	-536(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r14, %rdi
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L800
	movq	-528(%rbp), %rax
	addq	$504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L786:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L788
.L800:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_.cold, @function
_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_.cold:
.LFSB7870:
.L796:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	movq	%r15, %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN4node7FPrintFIJPKcEEEvP8_IO_FILES2_DpOT_
	jmp	.L789
	.cfi_endproc
.LFE7870:
	.text
	.size	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_, .-_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	.section	.text.unlikely
	.size	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_.cold, .-_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_.cold
.LCOLDE39:
	.text
.LHOTE39:
	.section	.rodata.str1.1
.LC40:
	.string	"Profiler.enable"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"{ \"callCount\": true, \"detailed\": true }"
	.section	.rodata.str1.1
.LC42:
	.string	"Profiler.startPreciseCoverage"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8CoverageConnection5StartEv
	.type	_ZN4node8profiler20V8CoverageConnection5StartEv, @function
_ZN4node8profiler20V8CoverageConnection5StartEv:
.LFB7883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	addq	$8, %rsp
	movq	%r12, %rdi
	leaq	.LC41(%rip), %rdx
	popq	%r12
	leaq	.LC42(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	.cfi_endproc
.LFE7883:
	.size	_ZN4node8profiler20V8CoverageConnection5StartEv, .-_ZN4node8profiler20V8CoverageConnection5StartEv
	.section	.rodata.str1.1
.LC43:
	.string	"Profiler.takePreciseCoverage"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8CoverageConnection3EndEv
	.type	_ZN4node8profiler20V8CoverageConnection3EndEv, @function
_ZN4node8profiler20V8CoverageConnection3EndEv:
.LFB7884:
	.cfi_startproc
	endbr64
	cmpb	$0, 40(%rdi)
	jne	.L808
	movb	$1, 40(%rdi)
	xorl	%edx, %edx
	leaq	.LC43(%rip), %rsi
	jmp	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	.p2align 4,,10
	.p2align 3
.L808:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node8profiler20V8CoverageConnection3EndEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7884:
	.size	_ZN4node8profiler20V8CoverageConnection3EndEv, .-_ZN4node8profiler20V8CoverageConnection3EndEv
	.section	.rodata.str1.1
.LC44:
	.string	"Profiler.start"
.LC45:
	.string	"basic_string::append"
.LC46:
	.string	"Profiler.setSamplingInterval"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler23V8CpuProfilerConnection5StartEv
	.type	_ZN4node8profiler23V8CpuProfilerConnection5StartEv, @function
_ZN4node8profiler23V8CpuProfilerConnection5StartEv:
.LFB7888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-112(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-96(%rbp), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	xorl	%edx, %edx
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-80(%rbp), %rdi
	movabsq	$8243122710530629755, %rax
	movq	%rax, -96(%rbp)
	movl	$8250, %eax
	leaq	.LC17(%rip), %rcx
	movl	$32, %edx
	movw	%ax, -84(%rbp)
	movq	24(%r12), %rax
	movq	%rbx, -112(%rbp)
	movq	1552(%rax), %r8
	xorl	%eax, %eax
	movl	$577528182, -88(%rbp)
	movq	$14, -104(%rbp)
	movb	$0, -82(%rbp)
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L810
	call	_ZdlPv@PLT
.L810:
	movabsq	$4611686018427387903, %rax
	subq	-104(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L815
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	leaq	.LC46(%rip), %rsi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L809
	call	_ZdlPv@PLT
.L809:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L816
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L815:
	.cfi_restore_state
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L816:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7888:
	.size	_ZN4node8profiler23V8CpuProfilerConnection5StartEv, .-_ZN4node8profiler23V8CpuProfilerConnection5StartEv
	.section	.rodata.str1.1
.LC47:
	.string	"Profiler.stop"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler23V8CpuProfilerConnection3EndEv
	.type	_ZN4node8profiler23V8CpuProfilerConnection3EndEv, @function
_ZN4node8profiler23V8CpuProfilerConnection3EndEv:
.LFB7889:
	.cfi_startproc
	endbr64
	cmpb	$0, 40(%rdi)
	jne	.L822
	movb	$1, 40(%rdi)
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rsi
	jmp	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	.p2align 4,,10
	.p2align 3
.L822:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node8profiler23V8CpuProfilerConnection3EndEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7889:
	.size	_ZN4node8profiler23V8CpuProfilerConnection3EndEv, .-_ZN4node8profiler23V8CpuProfilerConnection3EndEv
	.section	.rodata.str1.1
.LC48:
	.string	"HeapProfiler.enable"
.LC49:
	.string	"HeapProfiler.startSampling"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler24V8HeapProfilerConnection5StartEv
	.type	_ZN4node8profiler24V8HeapProfilerConnection5StartEv, @function
_ZN4node8profiler24V8HeapProfilerConnection5StartEv:
.LFB7893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	leaq	.LC48(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-96(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	leaq	-120(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, -112(%rbp)
	movq	$22, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %rdx
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-80(%rbp), %rdi
	movdqa	.LC50(%rip), %xmm0
	movq	%rax, -112(%rbp)
	leaq	.LC17(%rip), %rcx
	movq	%rdx, -96(%rbp)
	movl	$8250, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 20(%rax)
	movq	-112(%rbp), %rdx
	movl	$577528182, 16(%rax)
	movq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movb	$0, (%rdx,%rax)
	movq	24(%r12), %rax
	movl	$32, %edx
	movq	1632(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L824
	call	_ZdlPv@PLT
.L824:
	movabsq	$4611686018427387903, %rax
	subq	-104(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L829
	movl	$2, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	leaq	.LC49(%rip), %rsi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L823
	call	_ZdlPv@PLT
.L823:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L830
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L829:
	.cfi_restore_state
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7893:
	.size	_ZN4node8profiler24V8HeapProfilerConnection5StartEv, .-_ZN4node8profiler24V8HeapProfilerConnection5StartEv
	.section	.rodata.str1.1
.LC51:
	.string	"HeapProfiler.stopSampling"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler24V8HeapProfilerConnection3EndEv
	.type	_ZN4node8profiler24V8HeapProfilerConnection3EndEv, @function
_ZN4node8profiler24V8HeapProfilerConnection3EndEv:
.LFB7894:
	.cfi_startproc
	endbr64
	cmpb	$0, 40(%rdi)
	jne	.L836
	movb	$1, 40(%rdi)
	xorl	%edx, %edx
	leaq	.LC51(%rip), %rsi
	jmp	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	.p2align 4,,10
	.p2align 3
.L836:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node8profiler24V8HeapProfilerConnection3EndEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7894:
	.size	_ZN4node8profiler24V8HeapProfilerConnection3EndEv, .-_ZN4node8profiler24V8HeapProfilerConnection3EndEv
	.section	.rodata.str1.1
.LC52:
	.string	"EndStartedProfilers\n"
.LC53:
	.string	"%s"
.LC54:
	.string	"Ending cpu profiling\n"
.LC55:
	.string	"Ending heap profiling\n"
.LC56:
	.string	"Ending coverage collection\n"
	.section	.text.unlikely
.LCOLDB57:
	.text
.LHOTB57:
	.p2align 4
	.type	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_, @function
_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_:
.LFB7899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	.LC52(%rip), %rax
	cmpb	$0, 2257(%rdi)
	movq	%rax, -32(%rbp)
	jne	.L864
.L838:
	movq	1448(%rbx), %r12
	testq	%r12, %r12
	je	.L840
	movq	(%r12), %rax
	leaq	_ZNK4node8profiler23V8CpuProfilerConnection6endingEv(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L841
	movzbl	40(%r12), %eax
.L842:
	testb	%al, %al
	jne	.L840
	leaq	.LC54(%rip), %rax
	cmpb	$0, 2257(%rbx)
	movq	%rax, -32(%rbp)
	jne	.L865
.L844:
	movq	(%r12), %rax
	leaq	_ZN4node8profiler23V8CpuProfilerConnection3EndEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L845
	cmpb	$0, 40(%r12)
	jne	.L869
	movb	$1, 40(%r12)
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
.L840:
	movq	1560(%rbx), %r12
	testq	%r12, %r12
	je	.L848
	movq	(%r12), %rax
	leaq	_ZNK4node8profiler24V8HeapProfilerConnection6endingEv(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L849
	movzbl	40(%r12), %eax
.L850:
	testb	%al, %al
	jne	.L848
	leaq	.LC55(%rip), %rax
	cmpb	$0, 2257(%rbx)
	movq	%rax, -32(%rbp)
	jne	.L866
.L852:
	movq	(%r12), %rax
	leaq	_ZN4node8profiler24V8HeapProfilerConnection3EndEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L853
	cmpb	$0, 40(%r12)
	jne	.L870
	movb	$1, 40(%r12)
	xorl	%edx, %edx
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
.L848:
	movq	1440(%rbx), %r12
	testq	%r12, %r12
	je	.L837
	movq	(%r12), %rax
	leaq	_ZNK4node8profiler20V8CoverageConnection6endingEv(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L857
	movzbl	40(%r12), %eax
.L858:
	testb	%al, %al
	jne	.L837
	leaq	.LC56(%rip), %rax
	cmpb	$0, 2257(%rbx)
	movq	%rax, -32(%rbp)
	jne	.L867
.L860:
	movq	(%r12), %rax
	leaq	_ZN4node8profiler20V8CoverageConnection3EndEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L861
	cmpb	$0, 40(%r12)
	jne	.L871
	movb	$1, 40(%r12)
	xorl	%edx, %edx
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
.L837:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L872
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L857:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L845:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L853:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L869:
	leaq	_ZZN4node8profiler23V8CpuProfilerConnection3EndEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L870:
	leaq	_ZZN4node8profiler24V8HeapProfilerConnection3EndEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L871:
	leaq	_ZZN4node8profiler20V8CoverageConnection3EndEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L872:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_.cold, @function
_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_.cold:
.LFSB7899:
.L867:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	stderr(%rip), %rdi
	leaq	-32(%rbp), %rdx
	leaq	.LC53(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	jmp	.L860
.L865:
	movq	stderr(%rip), %rdi
	leaq	-32(%rbp), %rdx
	leaq	.LC53(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	jmp	.L844
.L866:
	movq	stderr(%rip), %rdi
	leaq	-32(%rbp), %rdx
	leaq	.LC53(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	jmp	.L852
.L864:
	movq	stderr(%rip), %rdi
	leaq	-32(%rbp), %rdx
	leaq	.LC53(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	jmp	.L838
	.cfi_endproc
.LFE7899:
	.text
	.size	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_, .-_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_
	.section	.text.unlikely
	.size	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_.cold, .-_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_.cold
.LCOLDE57:
	.text
.LHOTE57:
	.section	.rodata.str1.1
.LC58:
	.string	"NODE_V8_COVERAGE"
.LC59:
	.string	"cpuprofile"
.LC60:
	.string	"heapprofile"
.LC61:
	.string	"Heap"
	.text
	.p2align 4
	.globl	_ZN4node8profiler14StartProfilersEPNS_11EnvironmentE
	.type	_ZN4node8profiler14StartProfilersEPNS_11EnvironmentE, @function
_ZN4node8profiler14StartProfilersEPNS_11EnvironmentE:
.LFB7897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	leaq	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEENUlPvE_4_FUNES3_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_@PLT
	movq	1392(%rbx), %r13
	movq	352(%rbx), %r15
	movq	1384(%rbx), %r14
	testq	%r13, %r13
	je	.L874
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L875
	lock addl	$1, 8(%r13)
.L874:
	movq	(%r14), %rax
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r15, %rdi
	leaq	.LC58(%rip), %rsi
	movq	16(%rax), %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1096
.L876:
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	*%r12
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1097
	testq	%r13, %r13
	je	.L886
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	testq	%r12, %r12
	je	.L879
.L1113:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1098
.L882:
	testq	%r14, %r14
	je	.L887
.L886:
	movq	%r14, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	jle	.L887
	cmpq	$0, 1440(%rbx)
	jne	.L1099
	movl	$48, %edi
	leaq	-264(%rbp), %r13
	call	_Znwm@PLT
	movl	$16, %edi
	movq	2080(%rbx), %r14
	movq	%rax, %r12
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%r12)
	call	_Znwm@PLT
	leaq	8(%r12), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rcx, (%rax)
	xorl	%ecx, %ecx
	movq	%rax, -264(%rbp)
	call	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L890
	movq	(%rdi), %rax
	call	*8(%rax)
.L890:
	leaq	16+_ZTVN4node8profiler20V8CoverageConnectionE(%rip), %rax
	cmpq	$0, 1440(%rbx)
	movq	%rbx, 24(%r12)
	movq	$1, 16(%r12)
	movq	%rax, (%r12)
	movq	$0, 32(%r12)
	movb	$0, 40(%r12)
	jne	.L1100
	movq	%r12, 1440(%rbx)
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
	leaq	.LC41(%rip), %rdx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node8profiler20V8ProfilerConnection15DispatchMessageEPKcS3_
.L887:
	movq	1648(%rbx), %r13
	movq	1640(%rbx), %r14
	testq	%r13, %r13
	je	.L892
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L893
	lock addl	$1, (%rax)
	movzbl	344(%r14), %r15d
	testq	%r12, %r12
	je	.L1101
.L1031:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L896
.L1091:
	movq	1640(%rbx), %r14
	movq	1648(%rbx), %r13
	testb	%r15b, %r15b
	je	.L901
	testq	%r13, %r13
	je	.L902
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L903
	lock addl	$1, (%rax)
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L905:
	cmpl	$1, %eax
	je	.L906
.L1092:
	movq	1648(%rbx), %r13
	movq	1640(%rbx), %rdx
	testq	%r13, %r13
	je	.L911
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L912
	lock addl	$1, (%rax)
.L913:
	movq	304(%rdx), %rdx
	movq	%rdx, 1552(%rbx)
	testq	%r12, %r12
	je	.L1102
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L914:
	cmpl	$1, %eax
	je	.L1103
.L916:
	movq	280(%r14), %r12
	leaq	-256(%rbp), %r15
	testq	%r12, %r12
	jne	.L920
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentE
.L921:
	leaq	1488(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-256(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L926
	call	_ZdlPv@PLT
.L926:
	movq	1648(%rbx), %r13
	movq	1640(%rbx), %rdx
	testq	%r13, %r13
	je	.L927
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L928
	lock addl	$1, (%rax)
	movq	320(%rdx), %r14
	testq	%r12, %r12
	je	.L1104
.L1025:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L930:
	cmpl	$1, %eax
	je	.L1105
.L931:
	testq	%r14, %r14
	jne	.L934
	movq	1936(%rbx), %rsi
	leaq	-224(%rbp), %rdi
	leaq	.LC59(%rip), %rcx
	leaq	.LC1(%rip), %rdx
	leaq	-176(%rbp), %r13
	call	_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_@PLT
	movq	-224(%rbp), %r15
	movq	%r13, -192(%rbp)
	leaq	-192(%rbp), %r14
	testq	%r15, %r15
	je	.L935
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -264(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L1106
	cmpq	$1, %rax
	jne	.L938
	movzbl	(%r15), %edx
	movb	%dl, -176(%rbp)
	movq	%r13, %rdx
.L939:
	movq	%rax, -184(%rbp)
	leaq	1520(%rbx), %rdi
	movq	%r14, %rsi
	movb	$0, (%rdx,%rax)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-192(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L940
	call	_ZdlPv@PLT
.L940:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L942
	call	_ZdlPv@PLT
.L942:
	cmpq	$0, 1448(%rbx)
	jne	.L1107
	movl	$48, %edi
	leaq	-264(%rbp), %r13
	call	_Znwm@PLT
	movl	$16, %edi
	movq	2080(%rbx), %r14
	movq	%rax, %r12
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%r12)
	call	_Znwm@PLT
	leaq	8(%r12), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rcx, (%rax)
	xorl	%ecx, %ecx
	movq	%rax, -264(%rbp)
	call	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L953
	movq	(%rdi), %rax
	call	*8(%rax)
.L953:
	leaq	16+_ZTVN4node8profiler23V8CpuProfilerConnectionE(%rip), %rax
	cmpq	$0, 1448(%rbx)
	movq	%rbx, 24(%r12)
	movq	$1, 16(%r12)
	movq	%rax, (%r12)
	movq	$0, 32(%r12)
	movb	$0, 40(%r12)
	jne	.L1108
	movq	%r12, 1448(%rbx)
	movq	%r12, %rdi
	call	_ZN4node8profiler23V8CpuProfilerConnection5StartEv
	movq	1640(%rbx), %r14
	movq	1648(%rbx), %r13
.L901:
	testq	%r13, %r13
	je	.L955
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L956
	lock addl	$1, (%rax)
	movzbl	424(%r14), %r14d
	testq	%r12, %r12
	je	.L1109
.L1021:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L1110
.L959:
	testb	%r14b, %r14b
	jne	.L1111
.L873:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1112
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L875:
	.cfi_restore_state
	addl	$1, 8(%r13)
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L893:
	addl	$1, 8(%r13)
	movzbl	344(%r14), %r15d
	testq	%r12, %r12
	jne	.L1031
.L1101:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1091
.L896:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L898
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L899:
	cmpl	$1, %eax
	jne	.L1091
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L879:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L882
.L1098:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L883
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L884:
	cmpl	$1, %eax
	jne	.L882
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L1097:
	testq	%r13, %r13
	je	.L887
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	testq	%r12, %r12
	jne	.L1113
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L892:
	cmpb	$0, 344(%r14)
	je	.L955
.L902:
	movq	%r14, %rdx
.L911:
	movq	304(%rdx), %rax
	movq	%rax, 1552(%rbx)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L955:
	movzbl	424(%r14), %r14d
	testb	%r14b, %r14b
	je	.L873
.L1111:
	movq	1648(%rbx), %r13
	movq	1640(%rbx), %r15
	testq	%r13, %r13
	je	.L963
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L964
	lock addl	$1, (%rax)
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L966:
	cmpl	$1, %eax
	je	.L967
.L1093:
	movq	1648(%rbx), %r13
	movq	1640(%rbx), %rdx
	testq	%r13, %r13
	je	.L972
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L973
	lock addl	$1, (%rax)
.L974:
	movq	416(%rdx), %rdx
	movq	%rdx, 1632(%rbx)
	testq	%r12, %r12
	je	.L1114
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L975:
	cmpl	$1, %eax
	je	.L1115
.L977:
	movq	360(%r15), %r12
	leaq	-160(%rbp), %r8
	testq	%r12, %r12
	jne	.L981
	movq	%r8, %rdi
	movq	%rbx, %rsi
	movq	%r8, -280(%rbp)
	leaq	-144(%rbp), %r14
	call	_ZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentE
	movq	-280(%rbp), %r8
.L982:
	leaq	1568(%rbx), %rdi
	movq	%r8, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L986
	call	_ZdlPv@PLT
.L986:
	movq	1648(%rbx), %r13
	movq	1640(%rbx), %rdx
	testq	%r13, %r13
	je	.L987
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	leaq	8(%r13), %rax
	testq	%r12, %r12
	je	.L988
	lock addl	$1, (%rax)
.L989:
	movq	392(%rdx), %r14
	testq	%r12, %r12
	je	.L1116
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L990:
	cmpl	$1, %eax
	je	.L1117
.L991:
	testq	%r14, %r14
	jne	.L994
	movq	1936(%rbx), %rsi
	leaq	-128(%rbp), %rdi
	leaq	-80(%rbp), %r14
	leaq	.LC60(%rip), %rcx
	leaq	.LC61(%rip), %rdx
	call	_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_@PLT
	movq	-128(%rbp), %r8
	movq	%r14, -96(%rbp)
	leaq	-96(%rbp), %r15
	testq	%r8, %r8
	je	.L935
	movq	%r8, %rdi
	movq	%r8, -280(%rbp)
	call	strlen@PLT
	movq	-280(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -264(%rbp)
	movq	%rax, %r12
	ja	.L1118
	cmpq	$1, %rax
	jne	.L997
	movzbl	(%r8), %edx
	leaq	-264(%rbp), %r13
	movb	%dl, -80(%rbp)
	movq	%r14, %rdx
.L998:
	movq	%rax, -88(%rbp)
	leaq	1600(%rbx), %rdi
	movq	%r15, %rsi
	movb	$0, (%rdx,%rax)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L999
	call	_ZdlPv@PLT
.L999:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1001
	call	_ZdlPv@PLT
.L1001:
	movl	$48, %edi
	call	_Znwm@PLT
	movl	$16, %edi
	movq	2080(%rbx), %r14
	movq	%rax, %r12
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnectionE(%rip), %rax
	movq	%rax, (%r12)
	call	_Znwm@PLT
	leaq	8(%r12), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	leaq	16+_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE(%rip), %rcx
	movq	%r12, 8(%rax)
	movq	%rcx, (%rax)
	xorl	%ecx, %ecx
	movq	%rax, -264(%rbp)
	call	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1011
	movq	(%rdi), %rax
	call	*8(%rax)
.L1011:
	leaq	16+_ZTVN4node8profiler24V8HeapProfilerConnectionE(%rip), %rax
	cmpq	$0, 1560(%rbx)
	movq	%rbx, 24(%r12)
	movq	$1, 16(%r12)
	movq	%rax, (%r12)
	movq	$0, 32(%r12)
	movb	$0, 40(%r12)
	jne	.L1119
	movq	%r12, 1560(%rbx)
	movq	%r12, %rdi
	call	_ZN4node8profiler24V8HeapProfilerConnection5StartEv
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L956:
	addl	$1, 8(%r13)
	movzbl	424(%r14), %r14d
	testq	%r12, %r12
	jne	.L1021
.L1109:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L959
.L1110:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L960
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L961:
	cmpl	$1, %eax
	jne	.L959
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L903:
	addl	$1, 8(%r13)
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L963:
	movq	%r15, %rdx
.L972:
	movq	416(%rdx), %rax
	movq	%rax, 1632(%rbx)
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L934:
	movq	1640(%rbx), %rax
	movq	1648(%rbx), %r13
	leaq	1520(%rbx), %rdi
	leaq	312(%rax), %rsi
	testq	%r13, %r13
	je	.L943
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	leaq	8(%r13), %r14
	testq	%r12, %r12
	je	.L944
	lock addl	$1, (%r14)
.L945:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	testq	%r12, %r12
	je	.L1120
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
.L946:
	cmpl	$1, %eax
	jne	.L942
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L949
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L950:
	cmpl	$1, %eax
	jne	.L942
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L920:
	movq	272(%r14), %r14
	leaq	-240(%rbp), %r13
	movq	%r13, -256(%rbp)
	testq	%r14, %r14
	je	.L935
	movq	%r12, -264(%rbp)
	cmpq	$15, %r12
	ja	.L1121
	cmpq	$1, %r12
	jne	.L1038
	movzbl	(%r14), %eax
	movb	%al, -240(%rbp)
	movq	%r13, %rax
.L925:
	movq	%r12, -248(%rbp)
	movb	$0, (%rax,%r12)
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L912:
	addl	$1, 8(%r13)
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L928:
	addl	$1, 8(%r13)
	movq	320(%rdx), %r14
	testq	%r12, %r12
	jne	.L1025
.L1104:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L927:
	movq	320(%rdx), %r14
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L981:
	movq	352(%r15), %r15
	leaq	-144(%rbp), %r14
	movq	%r14, -160(%rbp)
	testq	%r15, %r15
	je	.L935
	movq	%r12, -264(%rbp)
	cmpq	$15, %r12
	ja	.L1122
	cmpq	$1, %r12
	jne	.L1040
	movzbl	(%r15), %eax
	movb	%al, -144(%rbp)
	movq	%r14, %rax
.L985:
	movq	%r12, -152(%rbp)
	movb	$0, (%rax,%r12)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L994:
	movq	1640(%rbx), %rax
	movq	1648(%rbx), %r14
	leaq	1600(%rbx), %rdi
	leaq	384(%rax), %rsi
	testq	%r14, %r14
	je	.L1002
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	leaq	8(%r14), %r13
	testq	%r12, %r12
	je	.L1003
	lock addl	$1, 0(%r13)
.L1004:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	testq	%r12, %r12
	je	.L1123
	movl	$-1, %eax
	lock xaddl	%eax, 0(%r13)
.L1005:
	cmpl	$1, %eax
	je	.L1006
.L1094:
	leaq	-264(%rbp), %r13
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L935:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	%rax, -280(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-280(%rbp), %rdx
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L988:
	addl	$1, 8(%r13)
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L964:
	addl	$1, 8(%r13)
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L973:
	addl	$1, 8(%r13)
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L883:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L906:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L908
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L909:
	cmpl	$1, %eax
	jne	.L1092
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L932
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L933:
	cmpl	$1, %eax
	jne	.L931
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L917
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L918:
	cmpl	$1, %eax
	jne	.L916
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L987:
	movq	392(%rdx), %r14
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L898:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L938:
	testq	%rax, %rax
	jne	.L1124
	movq	%r13, %rdx
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%r14, %rdi
	leaq	-264(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, -176(%rbp)
.L937:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-264(%rbp), %rax
	movq	-192(%rbp), %rdx
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	%r13, %rdi
.L924:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-264(%rbp), %r12
	movq	-256(%rbp), %rax
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	%r15, %rdi
	leaq	-264(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -256(%rbp)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, -240(%rbp)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L944:
	addl	$1, 8(%r13)
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L960:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L943:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1114:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1116:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1120:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L967:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L969
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L970:
	cmpl	$1, %eax
	jne	.L1093
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L992
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L993:
	cmpl	$1, %eax
	jne	.L991
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L978
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L979:
	cmpl	$1, %eax
	jne	.L977
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L997:
	testq	%rax, %rax
	jne	.L1125
	movq	%r14, %rdx
	leaq	-264(%rbp), %r13
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1040:
	movq	%r14, %rdi
.L984:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -280(%rbp)
	call	memcpy@PLT
	movq	-264(%rbp), %r12
	movq	-160(%rbp), %rax
	movq	-280(%rbp), %r8
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L932:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L908:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L917:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L1123:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L992:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L978:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L969:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1107:
	leaq	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1122:
	leaq	-264(%rbp), %r13
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r8, -280(%rbp)
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-280(%rbp), %r8
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, -144(%rbp)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	-264(%rbp), %r13
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-280(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, -80(%rbp)
.L996:
	movq	%r12, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-264(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1003:
	addl	$1, 8(%r14)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1108:
	leaq	_ZZN4node11Environment27set_cpu_profiler_connectionESt10unique_ptrINS_8profiler23V8CpuProfilerConnectionESt14default_deleteIS3_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1002:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1099:
	leaq	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1100:
	leaq	_ZZN4node11Environment23set_coverage_connectionESt10unique_ptrINS_8profiler20V8CoverageConnectionESt14default_deleteIS3_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1119:
	leaq	_ZZN4node11Environment28set_heap_profiler_connectionESt10unique_ptrINS_8profiler24V8HeapProfilerConnectionESt14default_deleteIS3_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L1008
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1009:
	leaq	-264(%rbp), %r13
	cmpl	$1, %eax
	jne	.L1001
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L949:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1009
.L1112:
	call	__stack_chk_fail@PLT
.L1124:
	movq	%r13, %rdi
	jmp	.L937
.L1125:
	movq	%r14, %rdi
	leaq	-264(%rbp), %r13
	jmp	.L996
	.cfi_endproc
.LFE7897:
	.size	_ZN4node8profiler14StartProfilersEPNS_11EnvironmentE, .-_ZN4node8profiler14StartProfilersEPNS_11EnvironmentE
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB10538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1127
	leaq	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1127:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC23(%rip), %rbx
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %r9
.L1128:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -216(%rbp)
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	movq	-216(%rbp), %r8
	jne	.L1128
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L1129
	cmpb	$99, %dl
	jg	.L1130
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L1131
	cmpb	$88, %dl
	je	.L1132
	jmp	.L1129
.L1130:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1129
	leaq	.L1134(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L1134:
	.long	.L1133-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1133-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1133-.L1134
	.long	.L1136-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1133-.L1134
	.long	.L1129-.L1134
	.long	.L1133-.L1134
	.long	.L1129-.L1134
	.long	.L1129-.L1134
	.long	.L1133-.L1134
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L1131:
	movq	-184(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1165
	jmp	.L1141
.L1129:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	leaq	-144(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1141
.L1165:
	call	_ZdlPv@PLT
.L1141:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L1161
	jmp	.L1140
.L1133:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L1148
	leaq	.LC22(%rip), %rsi
.L1148:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L1160
	jmp	.L1145
.L1132:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L1150
	leaq	.LC22(%rip), %rsi
.L1150:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1151
	call	_ZdlPv@PLT
.L1151:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1145
.L1160:
	call	_ZdlPv@PLT
	jmp	.L1145
.L1136:
	movq	(%r8), %r9
	leaq	-80(%rbp), %rbx
	xorl	%eax, %eax
	leaq	.LC24(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%rbx, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L1153
	leaq	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1153:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L1145:
	movq	-184(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1140
.L1161:
	call	_ZdlPv@PLT
.L1140:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1126
	call	_ZdlPv@PLT
.L1126:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1156
	call	__stack_chk_fail@PLT
.L1156:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10538:
	.size	_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB10270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1168
	call	__stack_chk_fail@PLT
.L1168:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10270:
	.size	_ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_:
.LFB9883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPKcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1170
	call	_ZdlPv@PLT
.L1170:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1172
	call	__stack_chk_fail@PLT
.L1172:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9883:
	.size	_ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_
	.section	.rodata.str1.1
.LC62:
	.string	"true"
.LC63:
	.string	"false"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"Receive %s profile message, ending = %s\n"
	.align 8
.LC65:
	.string	"Failed to convert %s profile message\n"
	.section	.text.unlikely
	.align 2
.LCOLDB66:
	.text
.LHOTB66:
	.align 2
	.p2align 4
	.globl	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.type	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, @function
_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE:
.LFB7872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r13, %rdi
	movq	24(%rax), %r12
	movq	352(%r12), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r12), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	8(%rbx), %rdi
	movq	%rax, -96(%rbp)
	movq	(%rdi), %rax
	call	*40(%rax)
	leaq	.LC63(%rip), %rcx
	testb	%al, %al
	leaq	.LC62(%rip), %rax
	cmove	%rcx, %rax
	cmpb	$0, 2257(%r12)
	movq	%rax, -88(%rbp)
	jne	.L1186
.L1176:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	jne	.L1190
.L1189:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1191
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1190:
	.cfi_restore_state
	movq	-104(%rbp), %rax
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1192
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*72(%rax)
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	-96(%rbp), %rcx
	movq	stderr(%rip), %rdi
	leaq	.LC65(%rip), %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
	jmp	.L1189
.L1191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE.cold, @function
_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE.cold:
.LFSB7872:
.L1186:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-88(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	leaq	.LC64(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcS2_EEEvP8_IO_FILES2_DpOT_
	jmp	.L1176
	.cfi_endproc
.LFE7872:
	.text
	.size	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, .-_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.section	.text.unlikely
	.size	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE.cold, .-_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE.cold
.LCOLDE66:
	.text
.LHOTE66:
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE, @function
_GLOBAL__sub_I__ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE:
.LFB10798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10798:
	.size	_GLOBAL__sub_I__ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE, .-_GLOBAL__sub_I__ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node8profiler20V8ProfilerConnectionC2EPNS_11EnvironmentE
	.weak	_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE
	.section	.data.rel.ro.local._ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE,"awG",@progbits,_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE,comdat
	.align 8
	.type	_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE, @object
	.size	_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE, 40
_ZTVN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD1Ev
	.quad	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegateD0Ev
	.quad	_ZN4node8profiler20V8ProfilerConnection25V8ProfilerSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.weak	_ZTVN4node8profiler20V8ProfilerConnectionE
	.section	.data.rel.ro._ZTVN4node8profiler20V8ProfilerConnectionE,"awG",@progbits,_ZTVN4node8profiler20V8ProfilerConnectionE,comdat
	.align 8
	.type	_ZTVN4node8profiler20V8ProfilerConnectionE, @object
	.size	_ZTVN4node8profiler20V8ProfilerConnectionE, 96
_ZTVN4node8profiler20V8ProfilerConnectionE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.weak	_ZTVN4node8profiler20V8CoverageConnectionE
	.section	.data.rel.ro.local._ZTVN4node8profiler20V8CoverageConnectionE,"awG",@progbits,_ZTVN4node8profiler20V8CoverageConnectionE,comdat
	.align 8
	.type	_ZTVN4node8profiler20V8CoverageConnectionE, @object
	.size	_ZTVN4node8profiler20V8CoverageConnectionE, 96
_ZTVN4node8profiler20V8CoverageConnectionE:
	.quad	0
	.quad	0
	.quad	_ZN4node8profiler20V8CoverageConnectionD1Ev
	.quad	_ZN4node8profiler20V8CoverageConnectionD0Ev
	.quad	_ZN4node8profiler20V8CoverageConnection5StartEv
	.quad	_ZN4node8profiler20V8CoverageConnection3EndEv
	.quad	_ZNK4node8profiler20V8CoverageConnection4typeEv
	.quad	_ZNK4node8profiler20V8CoverageConnection6endingEv
	.quad	_ZNK4node8profiler20V8CoverageConnection12GetDirectoryB5cxx11Ev
	.quad	_ZNK4node8profiler20V8CoverageConnection11GetFilenameB5cxx11Ev
	.quad	_ZN4node8profiler20V8CoverageConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.quad	_ZN4node8profiler20V8CoverageConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.weak	_ZTVN4node8profiler23V8CpuProfilerConnectionE
	.section	.data.rel.ro.local._ZTVN4node8profiler23V8CpuProfilerConnectionE,"awG",@progbits,_ZTVN4node8profiler23V8CpuProfilerConnectionE,comdat
	.align 8
	.type	_ZTVN4node8profiler23V8CpuProfilerConnectionE, @object
	.size	_ZTVN4node8profiler23V8CpuProfilerConnectionE, 96
_ZTVN4node8profiler23V8CpuProfilerConnectionE:
	.quad	0
	.quad	0
	.quad	_ZN4node8profiler23V8CpuProfilerConnectionD1Ev
	.quad	_ZN4node8profiler23V8CpuProfilerConnectionD0Ev
	.quad	_ZN4node8profiler23V8CpuProfilerConnection5StartEv
	.quad	_ZN4node8profiler23V8CpuProfilerConnection3EndEv
	.quad	_ZNK4node8profiler23V8CpuProfilerConnection4typeEv
	.quad	_ZNK4node8profiler23V8CpuProfilerConnection6endingEv
	.quad	_ZNK4node8profiler23V8CpuProfilerConnection12GetDirectoryB5cxx11Ev
	.quad	_ZNK4node8profiler23V8CpuProfilerConnection11GetFilenameB5cxx11Ev
	.quad	_ZN4node8profiler23V8CpuProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.quad	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.weak	_ZTVN4node8profiler24V8HeapProfilerConnectionE
	.section	.data.rel.ro.local._ZTVN4node8profiler24V8HeapProfilerConnectionE,"awG",@progbits,_ZTVN4node8profiler24V8HeapProfilerConnectionE,comdat
	.align 8
	.type	_ZTVN4node8profiler24V8HeapProfilerConnectionE, @object
	.size	_ZTVN4node8profiler24V8HeapProfilerConnectionE, 96
_ZTVN4node8profiler24V8HeapProfilerConnectionE:
	.quad	0
	.quad	0
	.quad	_ZN4node8profiler24V8HeapProfilerConnectionD1Ev
	.quad	_ZN4node8profiler24V8HeapProfilerConnectionD0Ev
	.quad	_ZN4node8profiler24V8HeapProfilerConnection5StartEv
	.quad	_ZN4node8profiler24V8HeapProfilerConnection3EndEv
	.quad	_ZNK4node8profiler24V8HeapProfilerConnection4typeEv
	.quad	_ZNK4node8profiler24V8HeapProfilerConnection6endingEv
	.quad	_ZNK4node8profiler24V8HeapProfilerConnection12GetDirectoryB5cxx11Ev
	.quad	_ZNK4node8profiler24V8HeapProfilerConnection11GetFilenameB5cxx11Ev
	.quad	_ZN4node8profiler24V8HeapProfilerConnection10GetProfileEN2v85LocalINS2_6ObjectEEE
	.quad	_ZN4node8profiler20V8ProfilerConnection12WriteProfileEN2v85LocalINS2_6StringEEE
	.weak	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC67:
	.string	"../src/debug_utils-inl.h:113"
.LC68:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {const char*}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.weak	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.rodata.str1.1
.LC70:
	.string	"../src/debug_utils-inl.h:76"
.LC71:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC69
	.weak	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC72
	.weak	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC73
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC73
	.section	.rodata.str1.1
.LC74:
	.string	"../src/inspector_profiler.cc"
.LC75:
	.string	"profiler"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC74
	.quad	0
	.quad	_ZN4node8profilerL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC75
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"../src/inspector_profiler.cc:467"
	.section	.rodata.str1.1
.LC77:
	.string	"args[0]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"void node::profiler::SetSourceMapCacheGetter(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node8profilerL23SetSourceMapCacheGetterERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"../src/inspector_profiler.cc:459"
	.section	.rodata.str1.1
.LC80:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"void node::profiler::SetCoverageDirectory(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node8profilerL20SetCoverageDirectoryERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"../src/inspector_profiler.cc:437"
	.align 8
.LC83:
	.string	"(env->cpu_profiler_connection()) == nullptr"
	.align 8
.LC84:
	.string	"void node::profiler::StartProfilers(node::Environment*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args_0, @object
	.size	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args_0, 24
_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args_0:
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"../src/inspector_profiler.cc:423"
	.align 8
.LC86:
	.string	"(env->coverage_connection()) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args, @object
	.size	_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args, 24
_ZZN4node8profiler14StartProfilersEPNS_11EnvironmentEE4args:
	.quad	.LC85
	.quad	.LC86
	.quad	.LC84
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"../src/inspector_profiler.cc:403"
	.section	.rodata.str1.1
.LC88:
	.string	"(size) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"std::string node::profiler::GetCwd(node::Environment*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentEE4args, @object
	.size	_ZZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentEE4args, 24
_ZZN4node8profiler6GetCwdB5cxx11EPNS_11EnvironmentEE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"../src/inspector_profiler.cc:368"
	.section	.rodata.str1.1
.LC91:
	.string	"(ending_) == (false)"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"virtual void node::profiler::V8HeapProfilerConnection::End()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8profiler24V8HeapProfilerConnection3EndEvE4args, @object
	.size	_ZZN4node8profiler24V8HeapProfilerConnection3EndEvE4args, 24
_ZZN4node8profiler24V8HeapProfilerConnection3EndEvE4args:
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"../src/inspector_profiler.cc:330"
	.align 8
.LC94:
	.string	"virtual void node::profiler::V8CpuProfilerConnection::End()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8profiler23V8CpuProfilerConnection3EndEvE4args, @object
	.size	_ZZN4node8profiler23V8CpuProfilerConnection3EndEvE4args, 24
_ZZN4node8profiler23V8CpuProfilerConnection3EndEvE4args:
	.quad	.LC93
	.quad	.LC91
	.quad	.LC94
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"../src/inspector_profiler.cc:291"
	.align 8
.LC96:
	.string	"virtual void node::profiler::V8CoverageConnection::End()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8profiler20V8CoverageConnection3EndEvE4args, @object
	.size	_ZZN4node8profiler20V8CoverageConnection3EndEvE4args, 24
_ZZN4node8profiler20V8CoverageConnection3EndEvE4args:
	.quad	.LC95
	.quad	.LC91
	.quad	.LC96
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC97:
	.string	"../src/debug_utils-inl.h:67"
.LC98:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.weak	_ZZN4node11Environment28set_heap_profiler_connectionESt10unique_ptrINS_8profiler24V8HeapProfilerConnectionESt14default_deleteIS3_EEE4args
	.section	.rodata.str1.1
.LC100:
	.string	"../src/env-inl.h:685"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"(heap_profiler_connection_) == nullptr"
	.align 8
.LC102:
	.string	"void node::Environment::set_heap_profiler_connection(std::unique_ptr<node::profiler::V8HeapProfilerConnection>)"
	.section	.data.rel.ro.local._ZZN4node11Environment28set_heap_profiler_connectionESt10unique_ptrINS_8profiler24V8HeapProfilerConnectionESt14default_deleteIS3_EEE4args,"awG",@progbits,_ZZN4node11Environment28set_heap_profiler_connectionESt10unique_ptrINS_8profiler24V8HeapProfilerConnectionESt14default_deleteIS3_EEE4args,comdat
	.align 16
	.type	_ZZN4node11Environment28set_heap_profiler_connectionESt10unique_ptrINS_8profiler24V8HeapProfilerConnectionESt14default_deleteIS3_EEE4args, @gnu_unique_object
	.size	_ZZN4node11Environment28set_heap_profiler_connectionESt10unique_ptrINS_8profiler24V8HeapProfilerConnectionESt14default_deleteIS3_EEE4args, 24
_ZZN4node11Environment28set_heap_profiler_connectionESt10unique_ptrINS_8profiler24V8HeapProfilerConnectionESt14default_deleteIS3_EEE4args:
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.weak	_ZZN4node11Environment27set_cpu_profiler_connectionESt10unique_ptrINS_8profiler23V8CpuProfilerConnectionESt14default_deleteIS3_EEE4args
	.section	.rodata.str1.1
.LC103:
	.string	"../src/env-inl.h:650"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"(cpu_profiler_connection_) == nullptr"
	.align 8
.LC105:
	.string	"void node::Environment::set_cpu_profiler_connection(std::unique_ptr<node::profiler::V8CpuProfilerConnection>)"
	.section	.data.rel.ro.local._ZZN4node11Environment27set_cpu_profiler_connectionESt10unique_ptrINS_8profiler23V8CpuProfilerConnectionESt14default_deleteIS3_EEE4args,"awG",@progbits,_ZZN4node11Environment27set_cpu_profiler_connectionESt10unique_ptrINS_8profiler23V8CpuProfilerConnectionESt14default_deleteIS3_EEE4args,comdat
	.align 16
	.type	_ZZN4node11Environment27set_cpu_profiler_connectionESt10unique_ptrINS_8profiler23V8CpuProfilerConnectionESt14default_deleteIS3_EEE4args, @gnu_unique_object
	.size	_ZZN4node11Environment27set_cpu_profiler_connectionESt10unique_ptrINS_8profiler23V8CpuProfilerConnectionESt14default_deleteIS3_EEE4args, 24
_ZZN4node11Environment27set_cpu_profiler_connectionESt10unique_ptrINS_8profiler23V8CpuProfilerConnectionESt14default_deleteIS3_EEE4args:
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.weak	_ZZN4node11Environment23set_coverage_connectionESt10unique_ptrINS_8profiler20V8CoverageConnectionESt14default_deleteIS3_EEE4args
	.section	.rodata.str1.1
.LC106:
	.string	"../src/env-inl.h:636"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"(coverage_connection_) == nullptr"
	.align 8
.LC108:
	.string	"void node::Environment::set_coverage_connection(std::unique_ptr<node::profiler::V8CoverageConnection>)"
	.section	.data.rel.ro.local._ZZN4node11Environment23set_coverage_connectionESt10unique_ptrINS_8profiler20V8CoverageConnectionESt14default_deleteIS3_EEE4args,"awG",@progbits,_ZZN4node11Environment23set_coverage_connectionESt10unique_ptrINS_8profiler20V8CoverageConnectionESt14default_deleteIS3_EEE4args,comdat
	.align 16
	.type	_ZZN4node11Environment23set_coverage_connectionESt10unique_ptrINS_8profiler20V8CoverageConnectionESt14default_deleteIS3_EEE4args, @gnu_unique_object
	.size	_ZZN4node11Environment23set_coverage_connectionESt10unique_ptrINS_8profiler20V8CoverageConnectionESt14default_deleteIS3_EEE4args, 24
_ZZN4node11Environment23set_coverage_connectionESt10unique_ptrINS_8profiler20V8CoverageConnectionESt14default_deleteIS3_EEE4args:
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC19:
	.long	0
	.long	1083129856
	.align 8
.LC20:
	.long	0
	.long	1138753536
	.section	.data.rel.ro,"aw"
	.align 8
.LC36:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC37:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC38:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC50:
	.quad	7813865618798682235
	.quad	8243122709998300777
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
