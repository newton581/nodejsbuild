	.file	"node_crypto_clienthello.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto17ClientHelloParser17ParseRecordHeaderEPKhm
	.type	_ZN4node6crypto17ClientHelloParser17ParseRecordHeaderEPKhm, @function
_ZN4node6crypto17ClientHelloParser17ParseRecordHeaderEPKhm:
.LFB3860:
	.cfi_startproc
	endbr64
	cmpq	$4, %rdx
	jbe	.L20
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	(%rsi), %eax
	subl	$20, %eax
	cmpb	$3, %al
	ja	.L4
	movzbl	3(%rsi), %eax
	movzbl	4(%rsi), %edx
	movl	$1, (%rdi)
	movq	$5, 40(%rdi)
	sall	$8, %eax
	addl	%edx, %eax
	movslq	%eax, %rdx
	movq	%rdx, 32(%rdi)
	cmpl	$16388, %eax
	jg	.L5
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	cmpl	$3, (%rdi)
	je	.L16
.L5:
	movq	16(%rbx), %rax
	movl	$3, (%rbx)
	testq	%rax, %rax
	je	.L16
	movq	24(%rbx), %rdi
	call	*%rax
	movq	$0, 16(%rbx)
.L16:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3860:
	.size	_ZN4node6crypto17ClientHelloParser17ParseRecordHeaderEPKhm, .-_ZN4node6crypto17ClientHelloParser17ParseRecordHeaderEPKhm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto17ClientHelloParser14ParseExtensionEtPKhm
	.type	_ZN4node6crypto17ClientHelloParser14ParseExtensionEtPKhm, @function
_ZN4node6crypto17ClientHelloParser14ParseExtensionEtPKhm:
.LFB3862:
	.cfi_startproc
	endbr64
	testw	%si, %si
	je	.L22
	cmpw	$35, %si
	jne	.L34
	addq	%rcx, %rdx
	movw	%cx, 88(%rdi)
	movq	%rdx, 96(%rdi)
.L21:
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	$1, %rcx
	jbe	.L21
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %esi
	sall	$8, %eax
	leal	2(%rax,%rsi), %r9d
	cmpq	%r9, %rcx
	jb	.L21
	cmpq	$2, %r9
	je	.L21
	cmpq	$4, %rcx
	jbe	.L21
	movl	$5, %r8d
	movl	$2, %eax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	movzbl	1(%rdx,%rax), %esi
	movzbl	2(%rdx,%rax), %eax
	sall	$8, %esi
	addl	%eax, %esi
	movzwl	%si, %eax
	addq	%r8, %rax
	cmpq	%rax, %rcx
	jb	.L21
	addq	%rdx, %r8
	movw	%si, 72(%rdi)
	movq	%r8, 80(%rdi)
	cmpq	%rax, %r9
	jbe	.L35
	leaq	3(%rax), %r8
	cmpq	%r8, %rcx
	jb	.L21
.L27:
	cmpb	$0, (%rdx,%rax)
	je	.L36
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	ret
	.cfi_endproc
.LFE3862:
	.size	_ZN4node6crypto17ClientHelloParser14ParseExtensionEtPKhm, .-_ZN4node6crypto17ClientHelloParser14ParseExtensionEtPKhm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto17ClientHelloParser19ParseTLSClientHelloEPKhm
	.type	_ZN4node6crypto17ClientHelloParser19ParseTLSClientHelloEPKhm, @function
_ZN4node6crypto17ClientHelloParser19ParseTLSClientHelloEPKhm:
.LFB3863:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	leaq	39(%rax), %rcx
	cmpq	%rdx, %rcx
	jnb	.L57
	leaq	38(%rax), %rcx
	leaq	(%rsi,%rcx), %r8
	movzbl	(%r8), %eax
	addq	$1, %r8
	movq	%r8, 64(%rdi)
	movb	%al, 56(%rdi)
	addq	%rcx, %rax
	leaq	2(%rax), %rcx
	cmpq	%rdx, %rcx
	jnb	.L57
	movzbl	1(%rsi,%rax), %ecx
	movzbl	2(%rsi,%rax), %r8d
	sall	$8, %ecx
	addl	%r8d, %ecx
	movzwl	%cx, %ecx
	leaq	3(%rax,%rcx), %rcx
	cmpq	%rcx, %rdx
	jbe	.L57
	movzbl	(%rsi,%rcx), %eax
	addq	%rcx, %rax
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L57
	movl	$1, %r8d
	je	.L58
	leaq	3(%rax), %r8
	cmpq	%r8, %rdx
	jbe	.L59
	addq	$7, %rax
	cmpq	%rax, %rdx
	jb	.L57
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L61:
	cmpw	$35, %cx
	jne	.L45
	addq	%r10, %rax
	movw	%r9w, 88(%rdi)
	movq	%rax, 96(%rdi)
.L45:
	cmpq	%rdx, %r8
	je	.L41
	leaq	4(%r8), %rax
	cmpq	%rdx, %rax
	ja	.L40
.L42:
	movzbl	(%rsi,%r8), %ecx
	movzbl	1(%rsi,%r8), %r9d
	sall	$8, %ecx
	addl	%r9d, %ecx
	movzbl	2(%rsi,%r8), %r9d
	movzbl	3(%rsi,%r8), %r8d
	sall	$8, %r9d
	addl	%r8d, %r9d
	movzwl	%r9w, %r10d
	leaq	(%r10,%rax), %r8
	cmpq	%rdx, %r8
	ja	.L40
	addq	%rsi, %rax
	testw	%cx, %cx
	jne	.L61
	cmpw	$1, %r9w
	jbe	.L45
	movzbl	(%rax), %ecx
	movzbl	1(%rax), %r9d
	sall	$8, %ecx
	leal	2(%rcx,%r9), %ebx
	cmpq	%rbx, %r10
	jb	.L45
	cmpq	$2, %rbx
	je	.L45
	cmpq	$4, %r10
	jbe	.L45
	movl	$5, %r11d
	movl	$2, %ecx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L62:
	movzbl	1(%rax,%rcx), %r9d
	movzbl	2(%rax,%rcx), %ecx
	sall	$8, %r9d
	addl	%ecx, %r9d
	movzwl	%r9w, %ecx
	addq	%r11, %rcx
	cmpq	%rcx, %r10
	jb	.L45
	addq	%rax, %r11
	movw	%r9w, 72(%rdi)
	movq	%r11, 80(%rdi)
	cmpq	%rcx, %rbx
	jbe	.L45
	leaq	3(%rcx), %r11
	cmpq	%r11, %r10
	jb	.L45
.L48:
	cmpb	$0, (%rax,%rcx)
	je	.L62
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r8d, %r8d
.L37:
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%r8d, %r8d
.L58:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpq	%r8, %rdx
	setnb	%r8b
	jmp	.L37
.L59:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	cmpq	%r8, %rdx
	setnb	%r8b
	jmp	.L58
	.cfi_endproc
.LFE3863:
	.size	_ZN4node6crypto17ClientHelloParser19ParseTLSClientHelloEPKhm, .-_ZN4node6crypto17ClientHelloParser19ParseTLSClientHelloEPKhm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto17ClientHelloParser11ParseHeaderEPKhm
	.type	_ZN4node6crypto17ClientHelloParser11ParseHeaderEPKhm, @function
_ZN4node6crypto17ClientHelloParser11ParseHeaderEPKhm:
.LFB3861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movq	32(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	addq	%rax, %rcx
	cmpq	%rdx, %rcx
	ja	.L63
	cmpb	$3, 4(%rsi,%rax)
	movq	%rdi, %rbx
	je	.L103
.L71:
	cmpl	$3, (%rbx)
	je	.L63
.L98:
	movq	16(%rbx), %rax
	movl	$3, (%rbx)
	testq	%rax, %rax
	je	.L63
	movq	24(%rbx), %rdi
	call	*%rax
	movq	$0, 16(%rbx)
.L63:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movzbl	5(%rsi,%rax), %edi
	leal	-1(%rdi), %ecx
	cmpb	$2, %cl
	ja	.L71
	cmpb	$1, (%rsi,%rax)
	je	.L105
	movq	64(%rbx), %rax
	movzbl	56(%rbx), %ecx
.L72:
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 96(%rbx)
	movl	$2, (%rbx)
	movb	%cl, -64(%rbp)
	je	.L73
	cmpw	$0, 88(%rbx)
	setne	%al
.L73:
	movb	%al, -48(%rbp)
	movq	80(%rbx), %rax
	leaq	-64(%rbp), %rsi
	movq	24(%rbx), %rdi
	movq	%rax, -40(%rbp)
	movzwl	72(%rbx), %eax
	movb	%al, -47(%rbp)
	call	*8(%rbx)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L105:
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L106
	cmpl	$3, %eax
	jne	.L98
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rbx, %rdi
	call	_ZN4node6crypto17ClientHelloParser19ParseTLSClientHelloEPKhm
	testb	%al, %al
	je	.L71
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.L71
	movzbl	56(%rbx), %ecx
	cmpb	$32, %cl
	ja	.L71
	movzbl	%cl, %edi
	addq	%rsi, %rdx
	addq	%rax, %rdi
	cmpq	%rdx, %rdi
	jbe	.L72
	jmp	.L71
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3861:
	.size	_ZN4node6crypto17ClientHelloParser11ParseHeaderEPKhm, .-_ZN4node6crypto17ClientHelloParser11ParseHeaderEPKhm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto17ClientHelloParser5ParseEPKhm
	.type	_ZN4node6crypto17ClientHelloParser5ParseEPKhm, @function
_ZN4node6crypto17ClientHelloParser5ParseEPKhm:
.LFB3859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L108
	cmpl	$1, %eax
	jne	.L107
	movq	40(%rdi), %rcx
	movq	32(%rdi), %rax
.L114:
	addq	%rcx, %rax
	cmpq	%rax, %rdx
	jb	.L107
	cmpb	$3, 4(%rsi,%rcx)
	je	.L153
.L148:
	movq	16(%rbx), %rax
	movl	$3, (%rbx)
	testq	%rax, %rax
	je	.L107
	movq	24(%rbx), %rdi
	call	*%rax
	movq	$0, 16(%rbx)
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	cmpq	$4, %rdx
	jbe	.L107
	movzbl	(%rsi), %eax
	subl	$20, %eax
	cmpb	$3, %al
	ja	.L148
	movzbl	3(%rsi), %ecx
	movzbl	4(%rsi), %eax
	movl	$1, (%rdi)
	movq	$5, 40(%rdi)
	sall	$8, %ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	movq	%rax, 32(%rdi)
	cmpl	$16388, %ecx
	jg	.L148
	movl	$5, %ecx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L153:
	movzbl	5(%rsi,%rcx), %eax
	subl	$1, %eax
	cmpb	$2, %al
	ja	.L148
	cmpb	$1, (%rsi,%rcx)
	je	.L155
	movq	64(%rbx), %rax
	movzbl	56(%rbx), %ecx
.L123:
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 96(%rbx)
	movl	$2, (%rbx)
	movb	%cl, -64(%rbp)
	je	.L124
	cmpw	$0, 88(%rbx)
	setne	%al
.L124:
	movb	%al, -48(%rbp)
	movq	80(%rbx), %rax
	leaq	-64(%rbp), %rsi
	movq	24(%rbx), %rdi
	movq	%rax, -40(%rbp)
	movzwl	72(%rbx), %eax
	movb	%al, -47(%rbp)
	call	*8(%rbx)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%rbx, %rdi
	call	_ZN4node6crypto17ClientHelloParser19ParseTLSClientHelloEPKhm
	testb	%al, %al
	je	.L122
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.L122
	movzbl	56(%rbx), %ecx
	cmpb	$32, %cl
	ja	.L122
	movzbl	%cl, %edi
	addq	%rdx, %rsi
	addq	%rax, %rdi
	cmpq	%rsi, %rdi
	jbe	.L123
.L122:
	cmpl	$3, (%rbx)
	jne	.L148
	jmp	.L107
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3859:
	.size	_ZN4node6crypto17ClientHelloParser5ParseEPKhm, .-_ZN4node6crypto17ClientHelloParser5ParseEPKhm
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
