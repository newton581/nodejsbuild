	.file	"node_i18n.cc"
	.text
	.section	.text._ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6105:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6105:
	.size	_ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node4i18n15ConverterObject8SelfSizeEv,"axG",@progbits,_ZNK4node4i18n15ConverterObject8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4i18n15ConverterObject8SelfSizeEv
	.type	_ZNK4node4i18n15ConverterObject8SelfSizeEv, @function
_ZNK4node4i18n15ConverterObject8SelfSizeEv:
.LFB6107:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE6107:
	.size	_ZNK4node4i18n15ConverterObject8SelfSizeEv, .-_ZNK4node4i18n15ConverterObject8SelfSizeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7187:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7187:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.text
	.p2align 4
	.type	_ZN4node6BufferL3NewIcEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE, @function
_ZN4node6BufferL3NewIcEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE:
.LFB9607:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L10
	leaq	24(%rbx), %r12
	movq	(%rbx), %rdx
	cmpq	%rsi, %r12
	je	.L8
	movl	$1, %ecx
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb@PLT
.L9:
	testq	%rax, %rax
	je	.L10
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L7
	cmpq	%rdx, %r12
	je	.L7
	movdqa	.LC0(%rip), %xmm0
	movq	%r12, 16(%rbx)
	movaps	%xmm0, (%rbx)
.L7:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rsi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9607:
	.size	_ZN4node6BufferL3NewIcEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE, .-_ZN4node6BufferL3NewIcEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE, @function
_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE:
.LFB9066:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L24
	movq	(%rbx), %rax
	leaq	24(%rbx), %r12
	leaq	(%rax,%rax), %rdx
	cmpq	%r12, %rsi
	je	.L22
	movl	$1, %ecx
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb@PLT
.L23:
	testq	%rax, %rax
	je	.L24
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L21
	cmpq	%rdx, %r12
	je	.L21
	movdqa	.LC0(%rip), %xmm0
	movq	%r12, 16(%rbx)
	movaps	%xmm0, (%rbx)
.L21:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L49
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rsi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%eax, %eax
	jmp	.L21
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9066:
	.size	_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE, .-_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	.section	.text._ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev:
.LFB6106:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1784827762, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7310593875233369923, %rcx
	movq	%rdx, (%rdi)
	movl	$25445, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$116, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE6106:
	.size	_ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev, .-_ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2296, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L105
	cmpw	$1040, %cx
	jne	.L52
.L105:
	movq	23(%rdx), %r14
.L54:
	cmpl	$2, 16(%rbx)
	jg	.L144
	leaq	_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	movq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L145
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L106
	cmpw	$1040, %cx
	jne	.L56
.L106:
	movq	23(%rdx), %r12
.L58:
	testq	%r12, %r12
	je	.L51
	cmpl	$1, 16(%rbx)
	jle	.L146
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r15
.L61:
	movq	$0, -2144(%rbp)
	movq	%r15, %rdi
	movq	$0, -2136(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	movl	%eax, %r13d
	testb	%al, %al
	je	.L147
	movq	%r15, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -2136(%rbp)
	cmpq	$64, %rax
	ja	.L65
	movq	%r15, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L148
.L65:
	movq	%r15, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-2272(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-2272(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -2296(%rbp)
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	-2296(%rbp), %rsi
	leaq	(%rsi,%rax), %r15
	movq	%r15, -2144(%rbp)
.L64:
	cmpl	$2, 16(%rbx)
	jg	.L66
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L67:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L149
.L68:
	shrq	$32, %rax
	movq	32(%r12), %rdi
	xorl	%r9d, %r9d
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, %r8
	leaq	-2104(%rbp), %r15
	leaq	-2128(%rbp), %rax
	movl	$0, -2284(%rbp)
	movq	%rax, -2304(%rbp)
	movq	%r15, -2112(%rbp)
	movw	%r9w, -2104(%rbp)
	movaps	%xmm0, -2128(%rbp)
	testq	%rdi, %rdi
	je	.L150
	movq	%r8, -2296(%rbp)
	call	ucnv_getMinCharSize_67@PLT
	movq	-2136(%rbp), %rdx
	movq	-2296(%rbp), %r8
	movsbq	%al, %rcx
	imulq	%rdx, %rcx
	testq	%rcx, %rcx
	jne	.L70
	leaq	-2284(%rbp), %rax
	movl	%r8d, %r13d
	andl	$1, %r8d
	xorl	%r9d, %r9d
	movq	-2144(%rbp), %rcx
	movq	32(%r12), %rdi
	pushq	%rax
	leaq	-2280(%rbp), %r11
	pushq	%r8
	movq	-2112(%rbp), %r10
	leaq	-2272(%rbp), %rsi
	andl	$1, %r13d
	leaq	(%rcx,%rdx), %r8
	movq	%rcx, -2280(%rbp)
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%r10, -2272(%rbp)
	call	ucnv_toUnicode_67@PLT
	movl	-2284(%rbp), %eax
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	jle	.L135
.L72:
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L143:
	testb	%r13b, %r13b
	jne	.L151
.L96:
	movq	-2112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L51
	cmpq	%r15, %rdi
	je	.L51
	call	free@PLT
.L51:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L152
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r8
	leaq	88(%r8), %r15
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L66:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L52:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-2112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L153
	cmpq	-2120(%rbp), %rcx
	jbe	.L74
	cmpq	%r15, %rdi
	je	.L154
.L75:
	leaq	(%rcx,%rcx), %rsi
	testq	%rcx, %rcx
	js	.L155
	testq	%rsi, %rsi
	je	.L156
	movq	%r8, -2328(%rbp)
	movq	%rcx, -2320(%rbp)
	movq	%rsi, -2312(%rbp)
	movq	%rdi, -2296(%rbp)
	call	realloc@PLT
	movq	-2296(%rbp), %rdi
	movq	-2312(%rbp), %rsi
	testq	%rax, %rax
	movq	-2320(%rbp), %rcx
	movq	-2328(%rbp), %r8
	je	.L157
.L79:
	movq	%rax, -2112(%rbp)
	movq	%rcx, -2120(%rbp)
	testb	%r13b, %r13b
	je	.L80
.L134:
	movq	-2136(%rbp), %rdx
	movq	%rax, %rdi
.L74:
	leaq	(%rdi,%rcx,4), %r10
	movl	%r8d, %r13d
	movq	%rcx, -2128(%rbp)
	andl	$1, %r8d
	leaq	-2284(%rbp), %rcx
	movq	-2144(%rbp), %rax
	movq	%rdi, -2272(%rbp)
	leaq	-2280(%rbp), %r11
	movq	32(%r12), %rdi
	pushq	%rcx
	leaq	-2272(%rbp), %rsi
	movq	%r11, %rcx
	pushq	%r8
	xorl	%r9d, %r9d
	leaq	(%rax,%rdx), %r8
	movq	%r10, %rdx
	movq	%rax, -2280(%rbp)
	andl	$1, %r13d
	call	ucnv_toUnicode_67@PLT
	movl	-2284(%rbp), %eax
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jg	.L72
	cmpq	$0, -2128(%rbp)
	movq	-2272(%rbp), %rax
	je	.L158
	movq	-2112(%rbp), %rdx
	subq	%rdx, %rax
	sarq	%rax
	cmpq	-2120(%rbp), %rax
	ja	.L159
	movq	%rax, -2128(%rbp)
	testq	%rax, %rax
	je	.L135
	movl	40(%r12), %eax
	testb	$8, %al
	je	.L135
	testb	$4, %al
	jne	.L135
	testb	$16, %al
	jne	.L135
	orl	$16, %eax
	cmpw	$-257, (%rdx)
	movq	-2304(%rbp), %rsi
	movq	%r14, %rdi
	movl	%eax, 40(%r12)
	je	.L90
	call	_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L135:
	movq	-2304(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
.L137:
	testq	%rax, %rax
	je	.L143
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
	testb	%r13b, %r13b
	je	.L96
.L151:
	andl	$-17, 40(%r12)
	movq	32(%r12), %rdi
	call	ucnv_reset_67@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	-2208(%rbp), %rcx
	movl	$64, %edx
	movq	%r15, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -2296(%rbp)
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	-2296(%rbp), %rcx
	movq	%rcx, -2144(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	_ZZNK4node4i18n9Converter13min_char_sizeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%rax, -2296(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-2296(%rbp), %rax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-2128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L134
	addq	%rdx, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r8, -2312(%rbp)
	movq	%rcx, -2296(%rbp)
	call	memcpy@PLT
	movq	-2136(%rbp), %rdx
	movq	-2112(%rbp), %rdi
	movq	-2312(%rbp), %r8
	movq	-2296(%rbp), %rcx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	jmp	.L75
.L90:
	call	_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L143
	movq	%rax, -2296(%rbp)
	call	_ZNK2v85Value12IsUint8ArrayEv@PLT
	movq	-2296(%rbp), %rdi
	testb	%al, %al
	je	.L160
	movq	%rdi, -2304(%rbp)
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	-2304(%rbp), %rdi
	movq	%rax, -2296(%rbp)
	movq	%rdi, -2312(%rbp)
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	-2312(%rbp), %rdi
	movq	%rax, -2304(%rbp)
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	-2296(%rbp), %rcx
	movq	-2304(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	subq	$2, %rcx
	addq	$2, %rdx
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmm@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	_ZZN4node16MaybeStackBufferIDsLm1024EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	call	free@PLT
.L78:
	leaq	_ZZN4node7ReallocIDsEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	_ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L157:
	movq	%rcx, -2296(%rbp)
	movq	%rsi, -2320(%rbp)
	movq	%rdi, -2312(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2320(%rbp), %rsi
	movq	-2312(%rbp), %rdi
	call	realloc@PLT
	movq	-2296(%rbp), %rcx
	movq	-2328(%rbp), %r8
	testq	%rax, %rax
	jne	.L79
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L155:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8136:
	.size	_ZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L172
	cmpw	$1040, %cx
	jne	.L162
.L172:
	movq	23(%rdx), %rax
.L164:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L184
	leaq	_ZZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	movq	8(%rbx), %rdx
	movq	352(%rax), %rsi
	leaq	-1072(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %rdi
	leaq	-1076(%rbp), %rsi
	movl	$0, -1076(%rbp)
	call	ucnv_open_67@PLT
	movl	-1076(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	(%rbx), %rax
	testl	%esi, %esi
	movq	8(%rax), %rdx
	setg	%cl
	movq	112(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rax)
	testq	%rdi, %rdi
	je	.L166
	call	ucnv_close_67@PLT
.L166:
	movq	-1056(%rbp), %rdi
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L161
	testq	%rdi, %rdi
	je	.L161
	call	free@PLT
.L161:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$1080, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L164
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8131:
	.size	_ZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L197
	cmpw	$1040, %cx
	jne	.L187
.L197:
	movq	23(%rdx), %r12
.L189:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L190
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L199
.L192:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L200
	movq	8(%rbx), %rdi
.L194:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	(%rbx), %rbx
	movl	%eax, %edi
	call	u_errorName_67@PLT
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L201
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L186:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L190:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L192
.L199:
	leaq	_ZZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L201:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L186
	.cfi_endproc
.LFE8120:
	.size	_ZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L203
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L203:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L213
.L204:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L205
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L204
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7185:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"utf-8"
.LC2:
	.string	"iso8859-1"
.LC3:
	.string	"utf16le"
.LC4:
	.string	"us-ascii"
	.text
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_19TranscodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4i18n12_GLOBAL__N_19TranscodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L265
	cmpw	$1040, %cx
	jne	.L215
.L265:
	movq	23(%rdx), %r14
.L217:
	movl	16(%rbx), %eax
	movq	352(%r14), %r13
	movl	$0, -212(%rbp)
	testl	%eax, %eax
	jg	.L218
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L219:
	movq	%r12, %rdi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L287
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	jbe	.L221
.L223:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%r15, %rax
	movq	%rax, -80(%rbp)
.L222:
	cmpl	$1, 16(%rbx)
	jle	.L288
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
.L225:
	movl	$6, %edx
	movq	%r13, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, %r12d
	jg	.L226
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L227:
	movl	$6, %edx
	movq	%r13, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	cmpl	$1, %r12d
	ja	.L289
.L228:
	cmpl	$1, %eax
	ja	.L290
	cmpl	$3, %r12d
	je	.L232
	.p2align 4,,10
	.p2align 3
.L252:
	cmpl	$3, %r12d
	ja	.L233
	testl	%r12d, %r12d
	je	.L234
	cmpl	$1, %r12d
	jne	.L236
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	cmpl	$3, %eax
	je	.L291
	cmpl	$1, %eax
	je	.L242
	leaq	_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
	ja	.L286
	leaq	.LC4(%rip), %rdx
.L246:
	leaq	.LC1(%rip), %rsi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L288:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L223
	leaq	-144(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r15, -80(%rbp)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L289:
	leal	-3(%r12), %edx
	cmpl	$1, %edx
	jbe	.L228
.L256:
	movabsq	$4294967296, %rax
.L229:
	movq	(%rbx), %rdx
	movq	%rax, 24(%rdx)
	.p2align 4,,10
	.p2align 3
.L214:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movq	8(%rbx), %r12
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L226:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L290:
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	ja	.L256
	cmpl	$3, %r12d
	jne	.L252
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	cmpl	$3, %eax
	je	.L293
.L241:
	leaq	_ZN4node4i18n12_GLOBAL__N_117TranscodeFromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L233:
	cmpl	$4, %r12d
	jne	.L236
.L234:
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	cmpl	$3, %eax
	je	.L294
	leaq	_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
	cmpl	$1, %eax
	je	.L242
.L243:
	leaq	.LC4(%rip), %rdx
	cmpl	$1, %eax
	jbe	.L245
.L286:
	leaq	.LC2(%rip), %rdx
	cmpl	$4, %eax
	je	.L245
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L245:
	cmpl	$1, %r12d
	je	.L246
	leaq	.LC4(%rip), %rsi
	jbe	.L247
	cmpl	$3, %r12d
	je	.L240
	cmpl	$4, %r12d
	leaq	.LC2(%rip), %rsi
	movl	$0, %eax
	cmovne	%rax, %rsi
.L247:
	leaq	-212(%rbp), %r9
	movq	%r14, %rdi
	call	*%r10
	testq	%rax, %rax
	je	.L295
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L215:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	leaq	_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
.L237:
	leaq	.LC3(%rip), %rdx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	_ZN4node4i18n12_GLOBAL__N_115TranscodeToUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	_ZN4node4i18n12_GLOBAL__N_121TranscodeUcs2FromUtf8EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
	jmp	.L237
.L232:
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	cmpl	$1, %eax
	jne	.L241
	leaq	_ZN4node4i18n12_GLOBAL__N_121TranscodeUtf8FromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
	leaq	.LC1(%rip), %rdx
.L240:
	leaq	.LC3(%rip), %rsi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode(%rip), %r10
	leaq	.LC1(%rip), %rdx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L295:
	movslq	-212(%rbp), %rax
	salq	$32, %rax
	jmp	.L229
.L292:
	call	__stack_chk_fail@PLT
.L236:
	call	_ZN4node5AbortEv@PLT
	.cfi_endproc
.LFE8119:
	.size	_ZN4node4i18n12_GLOBAL__N_19TranscodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4i18n12_GLOBAL__N_19TranscodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L335
	cmpw	$1040, %cx
	jne	.L297
.L335:
	movq	23(%rdx), %rbx
.L299:
	movl	16(%r14), %edx
	testl	%edx, %edx
	jg	.L300
	movq	(%r14), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L301:
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L364
.L302:
	leaq	_ZZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L364:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L302
	cmpl	$1, %edx
	jg	.L304
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L305:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$2, 16(%r14)
	movb	%al, -2130(%rbp)
	jg	.L306
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L307:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	movl	$1, %r15d
	testb	%al, %al
	je	.L308
	cmpl	$2, 16(%r14)
	jg	.L309
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L310:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	%eax, %r15d
.L308:
	movl	16(%r14), %eax
	testl	%eax, %eax
	jg	.L311
	movq	(%r14), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L312:
	movq	352(%rbx), %rsi
	leaq	-2128(%rbp), %rdi
	call	_ZN4node12TwoByteValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2128(%rbp), %rcx
	movq	-2112(%rbp), %r13
	testq	%rcx, %rcx
	je	.L313
	xorl	$1, %r15d
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	xorl	%edi, %edi
	movb	%r15b, -2129(%rbp)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L366:
	testl	%eax, %eax
	jne	.L322
.L321:
	movl	$58, %esi
	movl	%r15d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	jne	.L363
.L322:
	cmpl	$173, %r15d
	jne	.L327
	addl	$1, %r12d
.L324:
	movq	-2128(%rbp), %rcx
	cmpq	%rcx, %rbx
	jnb	.L315
.L367:
	movq	%rbx, %rax
	movl	%r15d, %edi
.L314:
	movzwl	0(%r13,%rax,2), %r15d
	leaq	1(%rax), %rbx
	leaq	(%rax,%rax), %r8
	movl	%r15d, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L316
	cmpq	%rcx, %rbx
	jne	.L365
.L316:
	cmpl	$8205, %edi
	sete	%al
	testb	%al, -2129(%rbp)
	je	.L325
	testq	%rbx, %rbx
	jne	.L317
.L325:
	movl	$4100, %esi
	movl	%r15d, %edi
	call	u_getIntPropertyValue_67@PLT
	cmpl	$1, %eax
	je	.L319
	jle	.L366
	subl	$3, %eax
	andl	$-3, %eax
	jne	.L322
.L363:
	movq	-2128(%rbp), %rcx
	addl	$2, %r12d
	cmpq	%rcx, %rbx
	jb	.L367
.L315:
	movq	(%r14), %rbx
	testl	%r12d, %r12d
	js	.L328
	movq	%r12, %rcx
	movq	-2112(%rbp), %r13
	salq	$32, %rcx
.L332:
	movq	%rcx, 24(%rbx)
.L329:
	testq	%r13, %r13
	je	.L296
	leaq	-2104(%rbp), %rax
	cmpq	%rax, %r13
	je	.L296
	movq	%r13, %rdi
	call	free@PLT
.L296:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L368
	addq	$2104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movl	%r15d, %edi
	call	u_charType_67@PLT
	movl	$98496, %edx
	btl	%eax, %edx
	jc	.L324
	movl	$59, %esi
	movl	%r15d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	sete	%al
	movzbl	%al, %eax
	addl	%eax, %r12d
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L319:
	cmpb	$0, -2130(%rbp)
	je	.L321
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L365:
	movzwl	2(%r13,%r8), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L316
	movl	%r15d, %edx
	leaq	2(%rax), %rbx
	sall	$10, %edx
	leal	-56613888(%rcx,%rdx), %r15d
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$58, %esi
	movl	%r15d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	jne	.L324
	movl	$59, %esi
	movl	%r15d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	je	.L325
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L300:
	movq	8(%r14), %rax
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L311:
	movq	8(%r14), %rdx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L306:
	movq	8(%r14), %rax
	leaq	-16(%rax), %rdi
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L304:
	movq	8(%r14), %rax
	leaq	-8(%rax), %rdi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L309:
	movq	8(%r14), %rax
	leaq	-16(%rax), %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L313:
	movq	(%r14), %rbx
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L328:
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L369
	movq	(%rax), %rax
	movq	-2112(%rbp), %r13
	movq	%rax, 24(%rbx)
	jmp	.L329
.L369:
	movq	16(%rbx), %rax
	movq	-2112(%rbp), %r13
	movq	%rax, 24(%rbx)
	jmp	.L329
.L368:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8147:
	.size	_ZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_115TranscodeToUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, @function
_ZN4node4i18n12_GLOBAL__N_115TranscodeToUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode:
.LFB8113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-2104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	xorl	%ecx, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$2120, %rsp
	movq	%rdi, -2152(%rbp)
	movdqa	.LC0(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%r9)
	movq	%r14, -2112(%rbp)
	movw	%cx, -2104(%rbp)
	movaps	%xmm0, -2128(%rbp)
	cmpq	$1024, %r8
	jbe	.L372
	leaq	(%r8,%r8), %rdi
	testq	%r8, %r8
	js	.L397
	testq	%rdi, %rdi
	je	.L374
	movq	%rdi, -2160(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	je	.L398
	movq	%rax, -2112(%rbp)
	movq	%rbx, -2120(%rbp)
.L372:
	movq	%r15, %rdi
	leaq	-2132(%rbp), %rsi
	movq	%rbx, -2128(%rbp)
	movl	$0, -2132(%rbp)
	call	ucnv_open_67@PLT
	movl	-2132(%rbp), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jg	.L399
	testq	%rax, %rax
	je	.L400
	movq	-2112(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %r9
	movl	%ebx, %r8d
	leaq	(%rbx,%rbx), %rdx
	movq	%rax, %rdi
	xorl	%r12d, %r12d
	call	ucnv_toUChars_67@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L401
.L380:
	movq	%r15, %rdi
	call	ucnv_close_67@PLT
	movq	-2112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L381
	cmpq	%r14, %rdi
	je	.L381
	call	free@PLT
.L381:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L402
	addq	$2120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	-2152(%rbp), %rdi
	leaq	-2128(%rbp), %rsi
	call	_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	movq	%rax, %r12
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	_ZZN4node4i18n9ConverterC4EPKcS3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	_ZZN4node7ReallocIDsEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L398:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2160(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L374
	movq	-2128(%rbp), %rdx
	movq	%rax, -2112(%rbp)
	movq	%rbx, -2120(%rbp)
	testq	%rdx, %rdx
	je	.L372
	addq	%rdx, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8113:
	.size	_ZN4node4i18n12_GLOBAL__N_115TranscodeToUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, .-_ZN4node4i18n12_GLOBAL__N_115TranscodeToUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_121TranscodeUtf8FromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, @function
_ZN4node4i18n12_GLOBAL__N_121TranscodeUtf8FromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode:
.LFB8116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-3160(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2104(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	shrq	%rbx
	subq	$3192, %rsp
	movq	%rdi, -3216(%rbp)
	movdqa	.LC0(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%r9)
	movq	%r13, -2112(%rbp)
	movw	%dx, -2104(%rbp)
	movq	%r14, -3168(%rbp)
	movb	$0, -3160(%rbp)
	movaps	%xmm0, -2128(%rbp)
	movaps	%xmm0, -3184(%rbp)
	cmpq	$2049, %r8
	jbe	.L432
	leaq	(%rbx,%rbx), %r8
	movq	%rcx, -3224(%rbp)
	movq	%r8, %rdi
	movq	%r8, -3208(%rbp)
	call	malloc@PLT
	movq	-3224(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L455
	movq	%rax, -2112(%rbp)
	movq	%rbx, -2120(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r13, %rdi
.L404:
	movq	%rcx, %rsi
	movq	%r15, %rdx
	movq	%rbx, -2128(%rbp)
	call	memcpy@PLT
	leaq	-3188(%rbp), %r10
	movq	%r12, %r9
	movq	-2112(%rbp), %rcx
	movl	-3176(%rbp), %esi
	movq	-3168(%rbp), %rdi
	movq	%r10, %rdx
	movl	%ebx, %r8d
	movq	%r10, -3208(%rbp)
	call	u_strToUTF8_67@PLT
	movl	(%r12), %eax
	movq	-3208(%rbp), %r10
	testl	%eax, %eax
	jle	.L454
	movq	-3168(%rbp), %rdi
	xorl	%r15d, %r15d
	cmpl	$15, %eax
	je	.L456
.L419:
	testq	%rdi, %rdi
	je	.L429
	cmpq	%r14, %rdi
	je	.L429
	call	free@PLT
.L429:
	movq	-2112(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L430
	testq	%rdi, %rdi
	je	.L430
	call	free@PLT
.L430:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$3192, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	movslq	-3188(%rbp), %r15
	movl	$0, (%r12)
	movq	%r15, %rsi
	testq	%rdi, %rdi
	je	.L458
	cmpq	-3176(%rbp), %r15
	jbe	.L421
	movb	$1, -3208(%rbp)
	cmpq	%r14, %rdi
	je	.L459
.L422:
	testq	%r15, %r15
	je	.L460
	movq	%r15, %rsi
	movq	%r10, -3232(%rbp)
	movq	%rdi, -3224(%rbp)
	call	realloc@PLT
	movq	-3232(%rbp), %r10
	testq	%rax, %rax
	je	.L461
.L424:
	cmpb	$0, -3208(%rbp)
	movq	%rax, -3168(%rbp)
	movq	%r15, -3176(%rbp)
	jne	.L452
	movq	-3184(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L426
.L452:
	movl	-3188(%rbp), %esi
	movq	%rax, %rdi
.L421:
	movq	-2112(%rbp), %rcx
	movq	%r12, %r9
	movl	%ebx, %r8d
	movq	%r10, %rdx
	movq	%r15, -3184(%rbp)
	call	u_strToUTF8_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L462
	.p2align 4,,10
	.p2align 3
.L454:
	movslq	-3188(%rbp), %rax
	cmpq	-3176(%rbp), %rax
	ja	.L463
	movq	-3216(%rbp), %rdi
	leaq	-3184(%rbp), %rsi
	movq	%rax, -3184(%rbp)
	call	_ZN4node6BufferL3NewIcEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	movq	-3168(%rbp), %rdi
	movq	%rax, %r15
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L455:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-3208(%rbp), %r8
	movq	%r8, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L406
	movq	-2128(%rbp), %rdx
	movq	%rax, -2112(%rbp)
	movq	%rbx, -2120(%rbp)
	movq	-3224(%rbp), %rcx
	testq	%rdx, %rdx
	je	.L404
	addq	%rdx, %rdx
	movq	%r13, %rsi
	movq	%rcx, -3208(%rbp)
	call	memcpy@PLT
	movq	-3208(%rbp), %rcx
	movq	%rax, %rdi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L462:
	movq	-3168(%rbp), %rdi
	xorl	%r15d, %r15d
	jmp	.L419
.L406:
	leaq	_ZZN4node7ReallocIDsEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L458:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L459:
	movb	$0, -3208(%rbp)
	xorl	%edi, %edi
	jmp	.L422
.L460:
	movq	%r10, -3224(%rbp)
	call	free@PLT
	movq	-3224(%rbp), %r10
	xorl	%eax, %eax
	jmp	.L424
.L426:
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%r10, -3208(%rbp)
	call	memcpy@PLT
	movl	-3188(%rbp), %esi
	movq	-3168(%rbp), %rdi
	movq	-3208(%rbp), %r10
	jmp	.L421
.L461:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-3224(%rbp), %rdi
	movq	%r15, %rsi
	call	realloc@PLT
	movq	-3232(%rbp), %r10
	testq	%rax, %rax
	jne	.L424
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8116:
	.size	_ZN4node4i18n12_GLOBAL__N_121TranscodeUtf8FromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, .-_ZN4node4i18n12_GLOBAL__N_121TranscodeUtf8FromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_121TranscodeUcs2FromUtf8EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, @function
_ZN4node4i18n12_GLOBAL__N_121TranscodeUcs2FromUtf8EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode:
.LFB8115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$1024, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2132(%rbp), %r15
	leaq	-2104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$2152, %rsp
	movq	%rdi, -2152(%rbp)
	movdqa	.LC0(%rip), %xmm0
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%r9)
	leaq	-2128(%rbp), %rax
	movw	%dx, -2104(%rbp)
	movq	%r15, %rdx
	movq	%rax, -2160(%rbp)
	movq	%r14, -2112(%rbp)
	movaps	%xmm0, -2128(%rbp)
	call	u_strFromUTF8_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L498
	movq	-2112(%rbp), %rdi
	xorl	%eax, %eax
	cmpl	$15, %edx
	je	.L499
.L467:
	testq	%rdi, %rdi
	je	.L479
	cmpq	%r14, %rdi
	je	.L479
	movq	%rax, -2152(%rbp)
	call	free@PLT
	movq	-2152(%rbp), %rax
.L479:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L500
	addq	$2152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movslq	-2132(%rbp), %rax
	cmpq	-2120(%rbp), %rax
	ja	.L501
	movq	-2152(%rbp), %rdi
	movq	-2160(%rbp), %rsi
	movq	%rax, -2128(%rbp)
	call	_ZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	movq	-2112(%rbp), %rdi
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L499:
	movslq	-2132(%rbp), %rcx
	movl	$0, (%rbx)
	movq	%rcx, %rsi
	testq	%rdi, %rdi
	je	.L502
	cmpq	-2120(%rbp), %rcx
	jbe	.L469
	movb	$1, -2168(%rbp)
	cmpq	%r14, %rdi
	je	.L503
.L470:
	leaq	(%rcx,%rcx), %rsi
	testq	%rcx, %rcx
	js	.L504
	testq	%rsi, %rsi
	je	.L505
	movq	%rcx, -2192(%rbp)
	movq	%rsi, -2184(%rbp)
	movq	%rdi, -2176(%rbp)
	call	realloc@PLT
	movq	-2176(%rbp), %rdi
	movq	-2184(%rbp), %rsi
	testq	%rax, %rax
	movq	-2192(%rbp), %rcx
	je	.L506
.L474:
	cmpb	$0, -2168(%rbp)
	movq	%rax, -2112(%rbp)
	movq	%rcx, -2120(%rbp)
	jne	.L496
	movq	-2128(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L476
.L496:
	movl	-2132(%rbp), %esi
	movq	%rax, %rdi
.L469:
	movq	%rcx, -2128(%rbp)
	movq	%rbx, %r9
	movl	%r13d, %r8d
	movq	%r12, %rcx
	movq	%r15, %rdx
	call	u_strFromUTF8_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L498
	movq	-2112(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L503:
	movb	$0, -2168(%rbp)
	xorl	%edi, %edi
	jmp	.L470
.L504:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L501:
	leaq	_ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L505:
	movq	%rcx, -2176(%rbp)
	call	free@PLT
	movq	-2176(%rbp), %rcx
	movl	$1, %edx
	xorl	%eax, %eax
.L473:
	testq	%rcx, %rcx
	je	.L474
	testb	%dl, %dl
	je	.L474
	leaq	_ZZN4node7ReallocIDsEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L476:
	movq	%r14, %rsi
	movq	%rax, %rdi
	addq	%rdx, %rdx
	movq	%rcx, -2168(%rbp)
	call	memcpy@PLT
	movl	-2132(%rbp), %esi
	movq	-2112(%rbp), %rdi
	movq	-2168(%rbp), %rcx
	jmp	.L469
.L506:
	movq	%rcx, -2176(%rbp)
	movq	%rsi, -2192(%rbp)
	movq	%rdi, -2184(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2192(%rbp), %rsi
	movq	-2184(%rbp), %rdi
	call	realloc@PLT
	movq	-2176(%rbp), %rcx
	testq	%rax, %rax
	sete	%dl
	jmp	.L473
.L500:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8115:
	.size	_ZN4node4i18n12_GLOBAL__N_121TranscodeUcs2FromUtf8EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, .-_ZN4node4i18n12_GLOBAL__N_121TranscodeUcs2FromUtf8EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode
	.section	.rodata.str1.1
.LC5:
	.string	"toUnicode"
.LC6:
	.string	"toASCII"
.LC7:
	.string	"getStringWidth"
.LC8:
	.string	"icuErrName"
.LC9:
	.string	"transcode"
.LC10:
	.string	"Converter"
.LC11:
	.string	"getConverter"
.LC12:
	.string	"decode"
.LC13:
	.string	"hasConverter"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB14:
	.text
.LHOTB14:
	.p2align 4
	.globl	_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB8151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L508
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L508
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L508
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L546
.L509:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L547
.L510:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L548
.L511:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L549
.L512:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L550
.L513:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L551
.L514:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L552
.L515:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L553
.L516:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L554
.L517:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L555
.L518:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L556
.L519:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L557
.L520:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4i18n12_GLOBAL__N_19TranscodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L558
.L521:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L559
.L522:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L560
.L523:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%edx, %edx
	movl	$9, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L561
.L524:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	3168(%rbx), %rdi
	movq	352(%rbx), %r14
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L525
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3168(%rbx)
.L525:
	testq	%r13, %r13
	je	.L526
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3168(%rbx)
.L526:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L562
.L527:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L563
.L528:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L564
.L529:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L565
.L530:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L566
.L531:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L567
.L532:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L568
.L533:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L569
.L534:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L570
.L535:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L547:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L548:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L549:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L550:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L551:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L552:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L553:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L554:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L555:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L556:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L557:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L558:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L559:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L560:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L561:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L562:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L563:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L564:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L565:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L566:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L567:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L568:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L569:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L570:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L535
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB8151:
.L508:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE8151:
	.text
	.size	_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE14:
	.text
.LHOTE14:
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L572
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L573:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L572
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L572
	movq	%r9, %rdi
.L575:
	cmpq	%rsi, %rbx
	jne	.L573
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L573
	cmpq	16(%rdi), %rbx
	jne	.L573
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L607
	testq	%rsi, %rsi
	je	.L577
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L577
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L577:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L572:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L608
.L580:
	cmpq	$0, 8(%rbx)
	je	.L571
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L584
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L609
.L584:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L571
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L571:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L610
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L589
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L577
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L576:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L611
.L578:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L609:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L589:
	movq	%r10, %rax
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L608:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L612
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L580
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%rsi, 2600(%r12)
	jmp	.L578
.L612:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L610:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7173:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN4node4i18n15ConverterObjectD2Ev,"axG",@progbits,_ZN4node4i18n15ConverterObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4i18n15ConverterObjectD2Ev
	.type	_ZN4node4i18n15ConverterObjectD2Ev, @function
_ZN4node4i18n15ConverterObjectD2Ev:
.LFB10754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4i18n15ConverterObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L614
	call	ucnv_close_67@PLT
.L614:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE10754:
	.size	_ZN4node4i18n15ConverterObjectD2Ev, .-_ZN4node4i18n15ConverterObjectD2Ev
	.weak	_ZN4node4i18n15ConverterObjectD1Ev
	.set	_ZN4node4i18n15ConverterObjectD1Ev,_ZN4node4i18n15ConverterObjectD2Ev
	.section	.text._ZN4node4i18n15ConverterObjectD0Ev,"axG",@progbits,_ZN4node4i18n15ConverterObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4i18n15ConverterObjectD0Ev
	.type	_ZN4node4i18n15ConverterObjectD0Ev, @function
_ZN4node4i18n15ConverterObjectD0Ev:
.LFB10756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4i18n15ConverterObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L620
	call	ucnv_close_67@PLT
.L620:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10756:
	.size	_ZN4node4i18n15ConverterObjectD0Ev, .-_ZN4node4i18n15ConverterObjectD0Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7175:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n9ConverterC2EPKcS3_
	.type	_ZN4node4i18n9ConverterC2EPKcS3_, @function
_ZN4node4i18n9ConverterC2EPKcS3_:
.LFB8122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	leaq	-48(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rbx)
	movl	$0, -48(%rbp)
	call	ucnv_open_67@PLT
	movl	-48(%rbp), %edx
	testl	%edx, %edx
	jg	.L640
	movq	(%rbx), %rdi
	movq	%rax, %r12
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L629
	call	ucnv_close_67@PLT
	movq	(%rbx), %r12
.L629:
	testq	%r12, %r12
	je	.L641
	movl	$0, -44(%rbp)
	testq	%r13, %r13
	je	.L627
	movq	%r13, %rdi
	call	strlen@PLT
	leaq	-44(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsbl	%al, %edx
	call	ucnv_setSubstChars_67@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L642
.L627:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L643
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	leaq	_ZZN4node4i18n9ConverterC4EPKcS3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L642:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L643:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8122:
	.size	_ZN4node4i18n9ConverterC2EPKcS3_, .-_ZN4node4i18n9ConverterC2EPKcS3_
	.globl	_ZN4node4i18n9ConverterC1EPKcS3_
	.set	_ZN4node4i18n9ConverterC1EPKcS3_,_ZN4node4i18n9ConverterC2EPKcS3_
	.section	.rodata.str1.1
.LC15:
	.string	"?"
	.text
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_117TranscodeFromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, @function
_ZN4node4i18n12_GLOBAL__N_117TranscodeFromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode:
.LFB8114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	leaq	.LC15(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2104(%rbp), %r13
	movq	%r14, %r15
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	xorl	%ecx, %ecx
	shrq	%r15
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$3192, %rsp
	movq	%rdi, -3208(%rbp)
	movdqa	.LC0(%rip), %xmm0
	leaq	-3192(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%r9)
	movq	%r13, -2112(%rbp)
	movw	%cx, -2104(%rbp)
	movaps	%xmm0, -2128(%rbp)
	call	_ZN4node4i18n9ConverterC1EPKcS3_
	movq	-2112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L703
	cmpq	-2120(%rbp), %r15
	jbe	.L646
	cmpq	%r13, %rdi
	movl	$0, %r9d
	movq	%r15, %rsi
	cmovne	%rdi, %r9
	setne	-3209(%rbp)
	addq	%rsi, %rsi
	je	.L704
	movq	%r9, %rdi
	movq	%rsi, -3232(%rbp)
	movq	%r9, -3224(%rbp)
	call	realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L705
.L650:
	cmpb	$0, -3209(%rbp)
	movq	%rdi, -2112(%rbp)
	movq	%r15, -2120(%rbp)
	jne	.L646
	movq	-2128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L646
	addq	%rdx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-2112(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L646:
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, -2128(%rbp)
	leaq	-3160(%rbp), %r12
	call	memcpy@PLT
	movdqa	.LC0(%rip), %xmm1
	movq	%r12, -3168(%rbp)
	movq	%r12, %r11
	movb	$0, -3160(%rbp)
	movaps	%xmm1, -3184(%rbp)
	cmpq	$1024, %r15
	ja	.L706
.L662:
	movq	-2112(%rbp), %rcx
	movl	%r15d, %edx
	movl	%r15d, %r8d
	movq	%rbx, %r9
	movq	-3192(%rbp), %rdi
	movq	%r11, %rsi
	movq	%r15, -3184(%rbp)
	xorl	%r15d, %r15d
	call	ucnv_fromUChars_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L707
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L665
.L711:
	cmpq	%r12, %rdi
	je	.L665
	call	free@PLT
.L665:
	movq	-3192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L666
	call	ucnv_close_67@PLT
.L666:
	movq	-2112(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L667
	testq	%rdi, %rdi
	je	.L667
	call	free@PLT
.L667:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L708
	addq	$3192, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L709
	movq	%rax, -3168(%rbp)
	movq	%r15, -3176(%rbp)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L707:
	movl	%eax, %eax
	cmpq	-3176(%rbp), %rax
	ja	.L710
	movq	-3208(%rbp), %rdi
	leaq	-3184(%rbp), %rsi
	movq	%rax, -3184(%rbp)
	call	_ZN4node6BufferL3NewIcEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	movq	-3168(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	jne	.L711
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%r9, %rdi
	call	free@PLT
	movl	$1, %eax
	xorl	%edi, %edi
.L649:
	testq	%r15, %r15
	je	.L650
	testb	%al, %al
	je	.L650
	leaq	_ZZN4node7ReallocIDsEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L709:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L660
	movq	-3184(%rbp), %rdx
	movq	%rax, -3168(%rbp)
	movq	%r15, -3176(%rbp)
	testq	%rdx, %rdx
	je	.L662
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r11
	jmp	.L662
.L660:
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L705:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-3224(%rbp), %r9
	movq	-3232(%rbp), %rsi
	movq	%r9, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	sete	%al
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8114:
	.size	_ZN4node4i18n12_GLOBAL__N_117TranscodeFromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, .-_ZN4node4i18n12_GLOBAL__N_117TranscodeFromUcs2EPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode
	.p2align 4
	.type	_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, @function
_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode:
.LFB8109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1112(%rbp), %r15
	leaq	-1080(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	leaq	.LC15(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$1128, %rsp
	movq	%rdi, -1136(%rbp)
	movdqa	.LC0(%rip), %xmm0
	leaq	-1120(%rbp), %rdi
	movq	%rcx, -1128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%r9)
	movaps	%xmm0, -1104(%rbp)
	movq	%r14, -1088(%rbp)
	movb	$0, -1080(%rbp)
	call	_ZN4node4i18n9ConverterC1EPKcS3_
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$0, -1112(%rbp)
	call	ucnv_open_67@PLT
	movl	-1112(%rbp), %edx
	testl	%edx, %edx
	jg	.L742
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L743
	movq	-1120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L744
	call	ucnv_getMaxCharSize_67@PLT
	movsbq	%al, %rcx
	movq	-1088(%rbp), %rax
	imull	%r13d, %ecx
	testq	%rax, %rax
	je	.L745
	cmpq	-1096(%rbp), %rcx
	jbe	.L717
	movl	$0, %r8d
	cmpq	%r14, %rax
	movq	%r8, %rdi
	setne	-1153(%rbp)
	cmovne	%rax, %rdi
	testq	%rcx, %rcx
	je	.L746
	movq	%rcx, %rsi
	movq	%rcx, -1152(%rbp)
	movq	%rdi, -1144(%rbp)
	call	realloc@PLT
	movq	-1144(%rbp), %rdi
	movq	-1152(%rbp), %rcx
	testq	%rax, %rax
	je	.L747
.L720:
	cmpb	$0, -1153(%rbp)
	movq	%rax, -1088(%rbp)
	movq	%rcx, -1096(%rbp)
	jne	.L717
	movq	-1104(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L748
	.p2align 4,,10
	.p2align 3
.L717:
	subq	$8, %rsp
	movq	-1128(%rbp), %r9
	movq	-1120(%rbp), %rdi
	movq	%rcx, -1104(%rbp)
	pushq	%rbx
	addq	%rax, %rcx
	leaq	-1128(%rbp), %r8
	movq	%r15, %rdx
	pushq	$1
	addq	%r13, %r9
	movq	%r12, %rsi
	xorl	%r13d, %r13d
	pushq	$1
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	movq	%rax, -1112(%rbp)
	call	ucnv_convertEx_67@PLT
	movl	(%rbx), %eax
	addq	$64, %rsp
	testl	%eax, %eax
	jle	.L749
.L721:
	movq	%r12, %rdi
	call	ucnv_close_67@PLT
	movq	-1120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L724
	call	ucnv_close_67@PLT
.L724:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L725
	cmpq	%r14, %rdi
	je	.L725
	call	free@PLT
.L725:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L750
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	cmpq	$0, -1104(%rbp)
	movq	-1112(%rbp), %rax
	je	.L751
	subq	-1088(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	ja	.L752
	movq	-1136(%rbp), %rdi
	leaq	-1104(%rbp), %rsi
	movq	%rax, -1104(%rbp)
	call	_ZN4node6BufferL3NewIcEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEE
	movq	%rax, %r13
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%rcx, -1144(%rbp)
	call	free@PLT
	movq	-1144(%rbp), %rcx
	xorl	%eax, %eax
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L742:
	leaq	_ZZN4node4i18n9ConverterC4EPKcS3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L743:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	_ZZNK4node4i18n9Converter13max_char_sizeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L745:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rcx, -1144(%rbp)
	call	memcpy@PLT
	movq	-1088(%rbp), %rax
	movq	-1144(%rbp), %rcx
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%rdi, -1152(%rbp)
	movq	%rcx, -1144(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-1144(%rbp), %rcx
	movq	-1152(%rbp), %rdi
	movq	%rcx, %rsi
	call	realloc@PLT
	movq	-1144(%rbp), %rcx
	testq	%rax, %rax
	jne	.L720
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L751:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L752:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L750:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8109:
	.size	_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode, .-_ZN4node4i18n12_GLOBAL__N_19TranscodeEPNS_11EnvironmentEPKcS5_S5_mP10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n9ConverterC2EP10UConverterPKc
	.type	_ZN4node4i18n9ConverterC2EP10UConverterPKc, @function
_ZN4node4i18n9ConverterC2EP10UConverterPKc:
.LFB8125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	testq	%rsi, %rsi
	je	.L761
	movl	$0, -28(%rbp)
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L753
	movq	%rdx, %rdi
	movq	%rsi, %r12
	call	strlen@PLT
	leaq	-28(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsbl	%al, %edx
	call	ucnv_setSubstChars_67@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L762
.L753:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L763
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L762:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L763:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8125:
	.size	_ZN4node4i18n9ConverterC2EP10UConverterPKc, .-_ZN4node4i18n9ConverterC2EP10UConverterPKc
	.globl	_ZN4node4i18n9ConverterC1EP10UConverterPKc
	.set	_ZN4node4i18n9ConverterC1EP10UConverterPKc,_ZN4node4i18n9ConverterC2EP10UConverterPKc
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n9Converter15set_subst_charsEPKc
	.type	_ZN4node4i18n9Converter15set_subst_charsEPKc, @function
_ZN4node4i18n9Converter15set_subst_charsEPKc:
.LFB8127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L772
	movl	$0, -28(%rbp)
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L764
	movq	%rsi, %rdi
	call	strlen@PLT
	leaq	-28(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movsbl	%al, %edx
	call	ucnv_setSubstChars_67@PLT
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L773
.L764:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L774
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	.cfi_restore_state
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L773:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L774:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8127:
	.size	_ZN4node4i18n9Converter15set_subst_charsEPKc, .-_ZN4node4i18n9Converter15set_subst_charsEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n9Converter5resetEv
	.type	_ZN4node4i18n9Converter5resetEv, @function
_ZN4node4i18n9Converter5resetEv:
.LFB8128:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	ucnv_reset_67@PLT
	.cfi_endproc
.LFE8128:
	.size	_ZN4node4i18n9Converter5resetEv, .-_ZN4node4i18n9Converter5resetEv
	.align 2
	.p2align 4
	.globl	_ZNK4node4i18n9Converter13min_char_sizeEv
	.type	_ZNK4node4i18n9Converter13min_char_sizeEv, @function
_ZNK4node4i18n9Converter13min_char_sizeEv:
.LFB8129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L779
	call	ucnv_getMinCharSize_67@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movsbq	%al, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	leaq	_ZZNK4node4i18n9Converter13min_char_sizeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8129:
	.size	_ZNK4node4i18n9Converter13min_char_sizeEv, .-_ZNK4node4i18n9Converter13min_char_sizeEv
	.align 2
	.p2align 4
	.globl	_ZNK4node4i18n9Converter13max_char_sizeEv
	.type	_ZNK4node4i18n9Converter13max_char_sizeEv, @function
_ZNK4node4i18n9Converter13max_char_sizeEv:
.LFB8130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L783
	call	ucnv_getMaxCharSize_67@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movsbq	%al, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	leaq	_ZZNK4node4i18n9Converter13max_char_sizeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8130:
	.size	_ZNK4node4i18n9Converter13max_char_sizeEv, .-_ZNK4node4i18n9Converter13max_char_sizeEv
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n15ConverterObjectC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc
	.type	_ZN4node4i18n15ConverterObjectC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc, @function
_ZN4node4i18n15ConverterObjectC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc:
.LFB8139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movl	%r8d, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L785
	movq	%rdx, %rsi
	movq	%rdx, %r13
	movq	%rcx, %r14
	movq	%r9, %r15
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L835
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	2592(%rbx), %rsi
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r8
	testq	%r9, %r9
	je	.L787
	movq	(%r9), %rax
	movq	32(%rax), %rcx
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L788:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L787
	movq	32(%rdi), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L787
	movq	%rdi, %rax
.L790:
	cmpq	%rcx, %r12
	jne	.L788
	cmpq	%r11, 8(%rax)
	jne	.L788
	cmpq	16(%rax), %r12
	jne	.L788
	cmpq	$0, (%r9)
	je	.L787
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L787:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	movq	%r8, -80(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r9
	testb	%al, %al
	jne	.L791
	movq	2584(%rbx), %r10
	movq	-80(%rbp), %r8
	movq	%r12, 32(%r13)
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L801
.L841:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r8), %rax
	movq	%r13, (%rax)
.L802:
	addq	$1, 2608(%rbx)
	addq	$1, 2656(%rbx)
	movq	%r14, 32(%r12)
	testq	%r14, %r14
	je	.L836
	movl	$0, -60(%rbp)
	testq	%r15, %r15
	je	.L805
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	-60(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movsbl	%al, %edx
	call	ucnv_setSubstChars_67@PLT
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L837
.L805:
	leaq	16+_ZTVN4node4i18n15ConverterObjectE(%rip), %rax
	movq	%rax, (%r12)
	movl	-68(%rbp), %eax
	movl	%eax, 40(%r12)
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L808
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L807
.L808:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L807:
	movq	%r14, %rdi
	call	ucnv_getType_67@PLT
	subl	$4, %eax
	cmpl	$2, %eax
	ja	.L784
	orl	$8, 40(%r12)
.L784:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L838
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L839
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L840
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -88(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %r10
	leaq	2632(%rbx), %rax
	movq	%rax, -80(%rbp)
.L794:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L796
	xorl	%r8d, %r8d
	leaq	2600(%rbx), %r11
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L798:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L799:
	testq	%rsi, %rsi
	je	.L796
.L797:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r9
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L798
	movq	2600(%rbx), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L812
	movq	%rcx, (%r10,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L797
	.p2align 4,,10
	.p2align 3
.L796:
	movq	2584(%rbx), %rdi
	cmpq	-80(%rbp), %rdi
	je	.L800
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r10
.L800:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r13)
	divq	%r9
	movq	%r9, 2592(%rbx)
	movq	%r10, 2584(%rbx)
	leaq	0(,%rdx,8), %r8
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L841
.L801:
	movq	2600(%rbx), %rax
	movq	%r13, 2600(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L803
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r10,%rdx,8)
.L803:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r8)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%rdx, %r8
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L785:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L835:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L836:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L837:
	leaq	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L839:
	leaq	2632(%rbx), %r10
	movq	$0, 2632(%rbx)
	movq	%r10, -80(%rbp)
	jmp	.L794
.L838:
	call	__stack_chk_fail@PLT
.L840:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8139:
	.size	_ZN4node4i18n15ConverterObjectC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc, .-_ZN4node4i18n15ConverterObjectC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc
	.globl	_ZN4node4i18n15ConverterObjectC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc
	.set	_ZN4node4i18n15ConverterObjectC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc,_ZN4node4i18n15ConverterObjectC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc
	.align 2
	.p2align 4
	.globl	_ZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1096, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L858
	cmpw	$1040, %cx
	jne	.L843
.L858:
	movq	23(%rdx), %r14
.L845:
	movq	3168(%r14), %rdi
	movq	3280(%r14), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L842
	cmpl	$1, 16(%rbx)
	jle	.L881
	movq	8(%rbx), %rdx
	movq	352(%r14), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jle	.L882
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L849:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L883
.L850:
	leaq	-1108(%rbp), %r9
	movq	-1088(%rbp), %rdi
	shrq	$32, %r12
	movl	$0, -1108(%rbp)
	movq	%r9, %rsi
	movq	%r9, -1128(%rbp)
	call	ucnv_open_67@PLT
	movq	-1128(%rbp), %r9
	movq	%rax, %r15
	movl	-1108(%rbp), %eax
	testl	%eax, %eax
	jg	.L884
	testb	$2, %r12b
	jne	.L885
.L854:
	movl	$48, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%r9d, %r9d
	movl	%r12d, %r8d
	movq	%rax, %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	call	_ZN4node4i18n15ConverterObjectC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP10UConverteriPKc
	movq	(%rbx), %rax
	movq	0(%r13), %rdx
	movq	-1088(%rbp), %rdi
	movq	%rdx, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L842
	testq	%rdi, %rdi
	je	.L842
.L880:
	call	free@PLT
.L842:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L886
	addq	$1096, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L884:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L842
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L880
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L843:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L885:
	movq	UCNV_TO_U_CALLBACK_STOP_67@GOTPCREL(%rip), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$0, -1108(%rbp)
	movq	%r15, %rdi
	call	ucnv_setToUCallBack_67@PLT
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L881:
	leaq	_ZZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L883:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L850
.L886:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8135:
	.size	_ZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.globl	_ZN4node4i18n22InitializeICUDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node4i18n22InitializeICUDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node4i18n22InitializeICUDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	movl	$0, -12(%rbp)
	jne	.L888
	leaq	-12(%rbp), %rsi
	leaq	icusmdt67_dat(%rip), %rdi
	call	udata_setCommonData_67@PLT
.L889:
	movl	-12(%rbp), %eax
	testl	%eax, %eax
	sete	%al
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L892
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	u_setDataDirectory_67@PLT
	leaq	-12(%rbp), %rdi
	call	u_init_67@PLT
	jmp	.L889
.L892:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8141:
	.size	_ZN4node4i18n22InitializeICUDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node4i18n22InitializeICUDirectoryERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node4i18n9ToUnicodeEPNS_16MaybeStackBufferIcLm1024EEEPKcm
	.type	_ZN4node4i18n9ToUnicodeEPNS_16MaybeStackBufferIcLm1024EEEPKcm, @function
_ZN4node4i18n9ToUnicodeEPNS_16MaybeStackBufferIcLm1024EEEPKcm:
.LFB8142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-84(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$104, %rsp
	movq	%rdx, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -84(%rbp)
	call	uidna_openUTS46_67@PLT
	movl	-84(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L906
	subq	$8, %rsp
	movq	16(%rbx), %rcx
	movl	8(%rbx), %r8d
	leaq	-80(%rbp), %r9
	pushq	%r14
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%r9, -104(%rbp)
	movq	%rax, %r13
	movq	$16, -80(%rbp)
	movq	$0, -72(%rbp)
	call	uidna_nameToUnicodeUTF8_67@PLT
	popq	%rsi
	movq	-104(%rbp), %r9
	movl	%eax, %r15d
	movl	-84(%rbp), %eax
	popq	%rdi
	cmpl	$15, %eax
	je	.L912
.L895:
	testl	%eax, %eax
	jle	.L902
	movq	$0, (%rbx)
	movl	$-1, %r15d
.L903:
	movq	%r13, %rdi
	call	uidna_close_67@PLT
.L893:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L913
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_restore_state
	movslq	%r15d, %rax
	cmpq	8(%rbx), %rax
	ja	.L914
	movq	%rax, (%rbx)
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L912:
	movq	16(%rbx), %rcx
	movl	$0, -84(%rbp)
	movslq	%r15d, %r11
	testq	%rcx, %rcx
	je	.L915
	movq	8(%rbx), %rax
	cmpq	%rax, %r11
	jbe	.L916
	leaq	24(%rbx), %rax
	movl	$0, %edi
	cmpq	%rax, %rcx
	movq	%rax, -144(%rbp)
	cmovne	%rcx, %rdi
	setne	-129(%rbp)
	testq	%r11, %r11
	je	.L917
	movq	%r11, %rsi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%rdi, -104(%rbp)
	call	realloc@PLT
	movq	-104(%rbp), %rdi
	movq	-120(%rbp), %r11
	testq	%rax, %rax
	movq	-128(%rbp), %r9
	movq	%rax, %rcx
	je	.L918
.L901:
	cmpb	$0, -129(%rbp)
	movq	%rcx, 16(%rbx)
	movq	%r11, 8(%rbx)
	jne	.L898
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L898
	movq	-144(%rbp), %rsi
	movq	%rcx, %rdi
	movq	%r9, -120(%rbp)
	movq	%r11, -104(%rbp)
	call	memcpy@PLT
	movq	16(%rbx), %rcx
	movl	8(%rbx), %r15d
	movq	-120(%rbp), %r9
	movq	-104(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L898:
	subq	$8, %rsp
	movq	%r11, (%rbx)
	movl	-112(%rbp), %edx
	movl	%r15d, %r8d
	pushq	%r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	uidna_nameToUnicodeUTF8_67@PLT
	popq	%rdx
	popq	%rcx
	movl	%eax, %r15d
	movl	-84(%rbp), %eax
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L916:
	movl	%eax, %r15d
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L914:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L906:
	movl	$-1, %r15d
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L915:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L917:
	movq	%r9, -120(%rbp)
	movq	%r11, -104(%rbp)
	call	free@PLT
	movq	-104(%rbp), %r11
	movq	-120(%rbp), %r9
	xorl	%ecx, %ecx
	jmp	.L901
.L918:
	movq	%rdi, -120(%rbp)
	movq	%r11, -104(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-104(%rbp), %r11
	movq	-120(%rbp), %rdi
	movq	%r11, %rsi
	call	realloc@PLT
	movq	-104(%rbp), %r11
	movq	-128(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L901
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L913:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8142:
	.size	_ZN4node4i18n9ToUnicodeEPNS_16MaybeStackBufferIcLm1024EEEPKcm, .-_ZN4node4i18n9ToUnicodeEPNS_16MaybeStackBufferIcLm1024EEEPKcm
	.section	.rodata.str1.1
.LC16:
	.string	"ERR_INVALID_ARG_VALUE"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"Cannot convert name to Unicode"
	.section	.rodata.str1.1
.LC18:
	.string	"code"
	.text
	.p2align 4
	.type	_ZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2136, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L942
	cmpw	$1040, %cx
	jne	.L920
.L942:
	movq	23(%rdx), %r13
.L922:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L973
	leaq	_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L973:
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L923
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L923
	movq	352(%r13), %rsi
	leaq	-2160(%rbp), %rdi
	leaq	-1080(%rbp), %r12
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movdqa	.LC0(%rip), %xmm0
	movq	-2160(%rbp), %rdx
	leaq	-1104(%rbp), %rdi
	movq	-2144(%rbp), %rsi
	movq	%r12, -1088(%rbp)
	movb	$0, -1080(%rbp)
	movaps	%xmm0, -1104(%rbp)
	call	_ZN4node4i18n9ToUnicodeEPNS_16MaybeStackBufferIcLm1024EEEPKcm
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L974
	movq	352(%r13), %rdi
	movq	-1088(%rbp), %rsi
	xorl	%edx, %edx
	movq	(%rbx), %rbx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L975
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L938:
	movq	-1088(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L936
.L972:
	testq	%rdi, %rdi
	je	.L936
	call	free@PLT
.L936:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L919
	testq	%rdi, %rdi
	je	.L919
	call	free@PLT
.L919:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L976
	addq	$2136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	movq	352(%r13), %r13
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L977
.L926:
	movq	%r13, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L978
.L927:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L979
.L928:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L980
.L929:
	movq	%r13, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L981
.L930:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-1088(%rbp), %rdi
	cmpq	%r12, %rdi
	jne	.L972
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L923:
	leaq	_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L920:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L975:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L977:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L980:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L981:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L978:
	movq	%rax, -2168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2168(%rbp), %rdi
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L979:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L928
.L976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8144:
	.size	_ZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node4i18n7ToASCIIEPNS_16MaybeStackBufferIcLm1024EEEPKcmNS0_9idna_modeE
	.type	_ZN4node4i18n7ToASCIIEPNS_16MaybeStackBufferIcLm1024EEEPKcmNS0_9idna_modeE, @function
_ZN4node4i18n7ToASCIIEPNS_16MaybeStackBufferIcLm1024EEEPKcmNS0_9idna_modeE:
.LFB8143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-84(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%r15, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$120, %rsp
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %ecx
	movl	$0, -84(%rbp)
	sete	%dil
	leal	28(%rdi,%rdi), %edi
	call	uidna_openUTS46_67@PLT
	movl	-84(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L1001
	subq	$8, %rsp
	movl	8(%rbx), %r8d
	movl	-104(%rbp), %edx
	leaq	-80(%rbp), %r9
	pushq	%r15
	movq	16(%rbx), %rcx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r9, -112(%rbp)
	movq	%rax, %r14
	movq	$16, -80(%rbp)
	movq	$0, -72(%rbp)
	call	uidna_nameToASCII_UTF8_67@PLT
	movl	-84(%rbp), %edx
	popq	%rsi
	movq	-112(%rbp), %r9
	movl	%eax, %r8d
	popq	%rdi
	cmpl	$15, %edx
	je	.L1011
.L985:
	movl	-76(%rbp), %ecx
	movl	%ecx, %eax
	andl	$-57, %eax
	movl	%eax, -76(%rbp)
	cmpl	$2, %r12d
	je	.L992
	movl	%ecx, %eax
	andl	$-64, %eax
	movl	%eax, -76(%rbp)
	testl	%edx, %edx
	jle	.L1012
.L993:
	movq	$0, (%rbx)
	movl	$-1, %r8d
.L998:
	movq	%r14, %rdi
	movl	%r8d, -104(%rbp)
	call	uidna_close_67@PLT
	movl	-104(%rbp), %r8d
.L982:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1013
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1012:
	.cfi_restore_state
	cmpl	$1, %r12d
	jne	.L994
.L997:
	movslq	%r8d, %rax
	cmpq	8(%rbx), %rax
	ja	.L1014
	movq	%rax, (%rbx)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L992:
	testl	%edx, %edx
	jg	.L993
.L994:
	testl	%eax, %eax
	je	.L997
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	16(%rbx), %rcx
	movl	$0, -84(%rbp)
	movslq	%eax, %r11
	testq	%rcx, %rcx
	je	.L1015
	movq	8(%rbx), %rax
	cmpq	%rax, %r11
	jbe	.L1016
	leaq	24(%rbx), %rax
	movl	$0, %edi
	cmpq	%rax, %rcx
	movq	%rax, -152(%rbp)
	cmovne	%rcx, %rdi
	setne	-137(%rbp)
	testq	%r11, %r11
	je	.L1017
	movq	%r11, %rsi
	movq	%r9, -136(%rbp)
	movl	%r8d, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	realloc@PLT
	movq	-112(%rbp), %rdi
	movq	-120(%rbp), %r11
	testq	%rax, %rax
	movl	-128(%rbp), %r8d
	movq	-136(%rbp), %r9
	movq	%rax, %rcx
	je	.L1018
.L991:
	cmpb	$0, -137(%rbp)
	movq	%rcx, 16(%rbx)
	movq	%r11, 8(%rbx)
	jne	.L988
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L988
	movq	-152(%rbp), %rsi
	movq	%rcx, %rdi
	movq	%r9, -120(%rbp)
	movq	%r11, -112(%rbp)
	call	memcpy@PLT
	movq	16(%rbx), %rcx
	movl	8(%rbx), %r8d
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L988:
	subq	$8, %rsp
	movq	%r11, (%rbx)
	movl	-104(%rbp), %edx
	movq	%r13, %rsi
	pushq	%r15
	movq	%r14, %rdi
	call	uidna_nameToASCII_UTF8_67@PLT
	movl	-84(%rbp), %edx
	movl	%eax, %r8d
	popq	%rax
	popq	%rcx
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1016:
	movl	%eax, %r8d
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1001:
	movl	$-1, %r8d
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1014:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1015:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%r9, -128(%rbp)
	movl	%r8d, -120(%rbp)
	movq	%r11, -112(%rbp)
	call	free@PLT
	movq	-112(%rbp), %r11
	movl	-120(%rbp), %r8d
	xorl	%ecx, %ecx
	movq	-128(%rbp), %r9
	jmp	.L991
.L1018:
	movq	%rdi, -120(%rbp)
	movq	%r11, -112(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %rdi
	movq	%r11, %rsi
	call	realloc@PLT
	movq	-112(%rbp), %r11
	movl	-128(%rbp), %r8d
	testq	%rax, %rax
	movq	-136(%rbp), %r9
	movq	%rax, %rcx
	jne	.L991
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8143:
	.size	_ZN4node4i18n7ToASCIIEPNS_16MaybeStackBufferIcLm1024EEEPKcmNS0_9idna_modeE, .-_ZN4node4i18n7ToASCIIEPNS_16MaybeStackBufferIcLm1024EEEPKcmNS0_9idna_modeE
	.section	.rodata.str1.1
.LC19:
	.string	"Cannot convert name to ASCII"
	.text
	.p2align 4
	.type	_ZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2136, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1044
	cmpw	$1040, %cx
	jne	.L1020
.L1044:
	movq	23(%rdx), %r12
.L1022:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1075
	leaq	_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1023
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1023
	movq	352(%r12), %rsi
	leaq	-2160(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jg	.L1025
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1026:
	movq	352(%r12), %rsi
	leaq	-1080(%rbp), %r13
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	movdqa	.LC0(%rip), %xmm0
	movq	-2160(%rbp), %rdx
	leaq	-1104(%rbp), %rdi
	movq	-2144(%rbp), %rsi
	movzbl	%al, %ecx
	movq	%r13, -1088(%rbp)
	movb	$0, -1080(%rbp)
	movaps	%xmm0, -1104(%rbp)
	call	_ZN4node4i18n7ToASCIIEPNS_16MaybeStackBufferIcLm1024EEEPKcmNS0_9idna_modeE
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L1076
	movq	352(%r12), %rdi
	movq	-1088(%rbp), %rsi
	xorl	%edx, %edx
	movq	(%rbx), %rbx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1077
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L1040:
	movq	-1088(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1038
.L1074:
	testq	%rdi, %rdi
	je	.L1038
	call	free@PLT
.L1038:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1019
	testq	%rdi, %rdi
	je	.L1019
	call	free@PLT
.L1019:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1078
	addq	$2136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1079
.L1028:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1080
.L1029:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1081
.L1030:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1082
.L1031:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1083
.L1032:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-1088(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L1074
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1023:
	leaq	_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1020:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1077:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1079:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1082:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1083:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	%rax, -2168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2168(%rbp), %rdi
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1081:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1030
.L1078:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8145:
	.size	_ZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_Z13_register_icuv
	.type	_Z13_register_icuv, @function
_Z13_register_icuv:
.LFB8152:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE8152:
	.size	_Z13_register_icuv, .-_Z13_register_icuv
	.weak	_ZTVN4node4i18n15ConverterObjectE
	.section	.data.rel.ro._ZTVN4node4i18n15ConverterObjectE,"awG",@progbits,_ZTVN4node4i18n15ConverterObjectE,comdat
	.align 8
	.type	_ZTVN4node4i18n15ConverterObjectE, @object
	.size	_ZTVN4node4i18n15ConverterObjectE, 88
_ZTVN4node4i18n15ConverterObjectE:
	.quad	0
	.quad	0
	.quad	_ZN4node4i18n15ConverterObjectD1Ev
	.quad	_ZN4node4i18n15ConverterObjectD0Ev
	.quad	_ZNK4node4i18n15ConverterObject10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node4i18n15ConverterObject14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node4i18n15ConverterObject8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.weak	_ZZN4node7ReallocIcEEPT_S2_mE4args
	.section	.rodata.str1.1
.LC20:
	.string	"../src/util-inl.h:374"
.LC21:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"T* node::Realloc(T*, size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIcEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIcEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIcEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIcEEPT_S2_mE4args, 24
_ZZN4node7ReallocIcEEPT_S2_mE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.weak	_ZZN4node7ReallocIDsEEPT_S2_mE4args
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"T* node::Realloc(T*, size_t) [with T = char16_t; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIDsEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIDsEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIDsEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIDsEEPT_S2_mE4args, 24
_ZZN4node7ReallocIDsEEPT_S2_mE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC23
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC24:
	.string	"../src/util-inl.h:325"
.LC25:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.weak	_ZZN4node16MaybeStackBufferIDsLm1024EEixEmE4args
	.section	.rodata.str1.1
.LC27:
	.string	"../src/util.h:352"
.LC28:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = char16_t; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIDsLm1024EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIDsLm1024EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIDsLm1024EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIDsLm1024EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIDsLm1024EEixEmE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC30:
	.string	"../src/util-inl.h:495"
.LC31:
	.string	"value->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Value>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.weak	_ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC33:
	.string	"../src/util.h:391"
.LC34:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = char16_t; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIDsLm1024EE9SetLengthEmE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"../src/node_i18n.cc:113"
.LC37:
	.string	"(retbuf_data) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"v8::MaybeLocal<v8::Object> node::i18n::{anonymous}::ToBufferEndian(node::Environment*, node::MaybeStackBuffer<T>*) [with T = char16_t]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEEE4args_0, @object
	.size	_ZZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEEE4args_0, 24
_ZZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEEE4args_0:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"(ret.ToLocalChecked())->IsArrayBufferView()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEEE4args, @object
	.size	_ZZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEEE4args, 24
_ZZN4node4i18n12_GLOBAL__N_114ToBufferEndianIDsEEN2v810MaybeLocalINS3_6ObjectEEEPNS_11EnvironmentEPNS_16MaybeStackBufferIT_Lm1024EEEE4args:
	.quad	.LC36
	.quad	.LC39
	.quad	.LC38
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC40
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EEixEmE4args
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EEixEmE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC41
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.1
.LC42:
	.string	"../src/util.h:376"
.LC43:
	.string	"!IsInvalidated()"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.weak	_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = char16_t; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIDsLm1024EE25AllocateSufficientStorageEmE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC45
	.section	.rodata.str1.1
.LC46:
	.string	"../src/node_i18n.cc"
.LC47:
	.string	"icu"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC46
	.quad	0
	.quad	_ZN4node4i18n10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC47
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC48:
	.string	"../src/node_i18n.cc:758"
.LC49:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"void node::i18n::GetStringWidth(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4i18nL14GetStringWidthERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.section	.rodata.str1.1
.LC51:
	.string	"../src/node_i18n.cc:676"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"void node::i18n::ToASCII(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC51
	.quad	.LC49
	.quad	.LC52
	.section	.rodata.str1.1
.LC53:
	.string	"../src/node_i18n.cc:675"
.LC54:
	.string	"(args.Length()) >= (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4i18nL7ToASCIIERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC52
	.section	.rodata.str1.1
.LC55:
	.string	"../src/node_i18n.cc:656"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"void node::i18n::ToUnicode(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC55
	.quad	.LC49
	.quad	.LC56
	.section	.rodata.str1.1
.LC57:
	.string	"../src/node_i18n.cc:655"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4i18nL9ToUnicodeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC57
	.quad	.LC54
	.quad	.LC56
	.section	.rodata.str1.1
.LC58:
	.string	"../src/node_i18n.cc:479"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"ret.ToLocalChecked()->IsUint8Array()"
	.align 8
.LC60:
	.string	"static void node::i18n::ConverterObject::Decode(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.section	.rodata.str1.1
.LC61:
	.string	"../src/node_i18n.cc:425"
.LC62:
	.string	"(args.Length()) >= (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4i18n15ConverterObject6DecodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC60
	.section	.rodata.str1.1
.LC63:
	.string	"../src/node_i18n.cc:401"
.LC64:
	.string	"(args.Length()) >= (2)"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"static void node::i18n::ConverterObject::Create(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4i18n15ConverterObject6CreateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.section	.rodata.str1.1
.LC66:
	.string	"../src/node_i18n.cc:386"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"static void node::i18n::ConverterObject::Has(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4i18n15ConverterObject3HasERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC66
	.quad	.LC54
	.quad	.LC67
	.section	.rodata.str1.1
.LC68:
	.string	"../src/node_i18n.cc:379"
.LC69:
	.string	"conv_"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"size_t node::i18n::Converter::max_char_size() const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node4i18n9Converter13max_char_sizeEvE4args, @object
	.size	_ZZNK4node4i18n9Converter13max_char_sizeEvE4args, 24
_ZZNK4node4i18n9Converter13max_char_sizeEvE4args:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.section	.rodata.str1.1
.LC71:
	.string	"../src/node_i18n.cc:374"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"size_t node::i18n::Converter::min_char_size() const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node4i18n9Converter13min_char_sizeEvE4args, @object
	.size	_ZZNK4node4i18n9Converter13min_char_sizeEvE4args, 24
_ZZNK4node4i18n9Converter13min_char_sizeEvE4args:
	.quad	.LC71
	.quad	.LC69
	.quad	.LC72
	.section	.rodata.str1.1
.LC73:
	.string	"../src/node_i18n.cc:365"
.LC74:
	.string	"U_SUCCESS(status)"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"void node::i18n::Converter::set_subst_chars(const char*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args_0, @object
	.size	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args_0, 24
_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args_0:
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.section	.rodata.str1.1
.LC76:
	.string	"../src/node_i18n.cc:361"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args, @object
	.size	_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args, 24
_ZZN4node4i18n9Converter15set_subst_charsEPKcE4args:
	.quad	.LC76
	.quad	.LC69
	.quad	.LC75
	.section	.rodata.str1.1
.LC77:
	.string	"../src/node_i18n.cc:350"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"node::i18n::Converter::Converter(const char*, const char*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n9ConverterC4EPKcS3_E4args, @object
	.size	_ZZN4node4i18n9ConverterC4EPKcS3_E4args, 24
_ZZN4node4i18n9ConverterC4EPKcS3_E4args:
	.quad	.LC77
	.quad	.LC74
	.quad	.LC78
	.section	.rodata.str1.1
.LC79:
	.string	"../src/node_i18n.cc:337"
.LC80:
	.string	"args[0]->IsInt32()"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"void node::i18n::{anonymous}::ICUErrorName(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4i18n12_GLOBAL__N_112ICUErrorNameERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC82:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC84:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC85:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC87:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC88:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC90:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC91:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC93:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC94:
	.string	"../src/base_object-inl.h:44"
.LC95:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC93
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC96:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC98:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.weak	_ZZN4node11SwapBytes16EPcmE4args
	.section	.rodata.str1.1
.LC99:
	.string	"../src/util-inl.h:208"
.LC100:
	.string	"(nbytes % 2) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"void node::SwapBytes16(char*, size_t)"
	.section	.data.rel.ro.local._ZZN4node11SwapBytes16EPcmE4args,"awG",@progbits,_ZZN4node11SwapBytes16EPcmE4args,comdat
	.align 16
	.type	_ZZN4node11SwapBytes16EPcmE4args, @gnu_unique_object
	.size	_ZZN4node11SwapBytes16EPcmE4args, 24
_ZZN4node11SwapBytes16EPcmE4args:
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	0
	.quad	1024
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
