	.file	"node_zlib.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4286:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4288:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4588:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4588:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4589:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4589:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB4637:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE4637:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB4638:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE4638:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB4639:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE4639:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB4641:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L10
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE4641:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7149:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7149:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_,"axG",@progbits,_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_,comdat
	.p2align 4
	.weak	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_
	.type	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_, @function
_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_:
.LFB7578:
	.cfi_startproc
	endbr64
	movq	-16(%rdi), %rax
	leaq	-16(%rdi), %r8
	movq	%r8, %rdi
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7578:
	.size	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_, .-_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ZlibContext8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_111ZlibContext8SelfSizeEv:
.LFB7604:
	.cfi_startproc
	endbr64
	movl	$176, %eax
	ret
	.cfi_endproc
.LFE7604:
	.size	_ZNK4node12_GLOBAL__N_111ZlibContext8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_111ZlibContext8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_120BrotliEncoderContext8SelfSizeEv:
.LFB7610:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE7610:
	.size	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_120BrotliEncoderContext8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_120BrotliEncoderContext10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7611:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7611:
	.size	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_120BrotliEncoderContext10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node12_GLOBAL__N_120BrotliEncoderContext10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_120BrotliDecoderContext8SelfSizeEv:
.LFB7615:
	.cfi_startproc
	endbr64
	movl	$128, %eax
	ret
	.cfi_endproc
.LFE7615:
	.size	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_120BrotliDecoderContext8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_110ZlibStream8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_110ZlibStream8SelfSizeEv:
.LFB7661:
	.cfi_startproc
	endbr64
	movl	$416, %eax
	ret
	.cfi_endproc
.LFE7661:
	.size	_ZNK4node12_GLOBAL__N_110ZlibStream8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_110ZlibStream8SelfSizeEv
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8967:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8967:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE8SelfSizeEv:
.LFB10260:
	.cfi_startproc
	endbr64
	movl	$368, %eax
	ret
	.cfi_endproc
.LFE10260:
	.size	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE8SelfSizeEv:
.LFB10265:
	.cfi_startproc
	endbr64
	movl	$336, %eax
	ret
	.cfi_endproc
.LFE10265:
	.size	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv:
.LFB8935:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	xchgq	224(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L21
	movq	232(%rdi), %rax
	js	.L29
.L24:
	addq	%rbx, %rax
	movq	%rax, 232(%rdi)
	movq	16(%rdi), %rax
	movq	352(%rax), %r12
	movq	32(%r12), %r13
	addq	%rbx, %r13
	movq	%r13, %rax
	subq	48(%r12), %rax
	movq	%r13, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L30
	movq	40(%r12), %rax
	testq	%rbx, %rbx
	js	.L31
.L26:
	cmpq	%rax, %r13
	jg	.L32
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	addq	%rax, %rbx
	cmpq	$67108864, %rbx
	jle	.L21
	movq	%rbx, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	40(%r12), %rax
	testq	%rbx, %rbx
	jns	.L26
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%rbx, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L24
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8935:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv:
.LFB9722:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	xchgq	224(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L33
	movq	232(%rdi), %rax
	js	.L41
.L36:
	addq	%rbx, %rax
	movq	%rax, 232(%rdi)
	movq	16(%rdi), %rax
	movq	352(%rax), %r12
	movq	32(%r12), %r13
	addq	%rbx, %r13
	movq	%r13, %rax
	subq	48(%r12), %rax
	movq	%r13, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L42
	movq	40(%r12), %rax
	testq	%rbx, %rbx
	js	.L43
.L38:
	cmpq	%rax, %r13
	jg	.L44
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	%rax, %rbx
	cmpq	$67108864, %rbx
	jle	.L33
	movq	%rbx, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	40(%r12), %rax
	testq	%rbx, %rbx
	jns	.L38
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%rbx, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L36
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9722:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv:
.LFB9742:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	xchgq	224(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L45
	movq	232(%rdi), %rax
	js	.L53
.L48:
	addq	%rbx, %rax
	movq	%rax, 232(%rdi)
	movq	16(%rdi), %rax
	movq	352(%rax), %r12
	movq	32(%r12), %r13
	addq	%rbx, %r13
	movq	%r13, %rax
	subq	48(%r12), %rax
	movq	%r13, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L54
	movq	40(%r12), %rax
	testq	%rbx, %rbx
	js	.L55
.L50:
	cmpq	%rax, %r13
	jg	.L56
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	addq	%rax, %rbx
	cmpq	$67108864, %rbx
	jle	.L45
	movq	%rbx, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	40(%r12), %rax
	testq	%rbx, %rbx
	jns	.L50
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%rbx, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L48
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9742:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE11FreeForZlibEPvS4_, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE11FreeForZlibEPvS4_:
.LFB8485:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L57
	movq	-8(%rsi), %rax
	lock subq	%rax, 224(%rdi)
	leaq	-8(%rsi), %rdi
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	ret
	.cfi_endproc
.LFE8485:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE11FreeForZlibEPvS4_, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE11FreeForZlibEPvS4_
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE14AllocForBrotliEPvm, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE14AllocForBrotliEPvm:
.LFB8936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	addq	$8, %rsi
	cmovne	%rsi, %r13
	movq	%rsi, %rbx
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L67
.L60:
	movq	%rbx, (%rax)
	lock addq	%rbx, 224(%r12)
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L60
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8936:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE14AllocForBrotliEPvm, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE14AllocForBrotliEPvm
	.set	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE14AllocForBrotliEPvm,_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE14AllocForBrotliEPvm
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ZlibContextD2Ev, @function
_ZN4node12_GLOBAL__N_111ZlibContextD2Ev:
.LFB8438:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node12_GLOBAL__N_111ZlibContextE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L68
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	ret
	.cfi_endproc
.LFE8438:
	.size	_ZN4node12_GLOBAL__N_111ZlibContextD2Ev, .-_ZN4node12_GLOBAL__N_111ZlibContextD2Ev
	.set	_ZN4node12_GLOBAL__N_111ZlibContextD1Ev,_ZN4node12_GLOBAL__N_111ZlibContextD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10237:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L70
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	ret
	.cfi_endproc
.LFE10237:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ZlibContextD0Ev, @function
_ZN4node12_GLOBAL__N_111ZlibContextD0Ev:
.LFB8440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_111ZlibContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$176, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8440:
	.size	_ZN4node12_GLOBAL__N_111ZlibContextD0Ev, .-_ZN4node12_GLOBAL__N_111ZlibContextD0Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10239:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120BrotliEncoderContextD2Ev, @function
_ZN4node12_GLOBAL__N_120BrotliEncoderContextD2Ev:
.LFB9708:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE(%rip), %rax
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L81
	jmp	BrotliEncoderDestroyInstance@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	ret
	.cfi_endproc
.LFE9708:
	.size	_ZN4node12_GLOBAL__N_120BrotliEncoderContextD2Ev, .-_ZN4node12_GLOBAL__N_120BrotliEncoderContextD2Ev
	.set	_ZN4node12_GLOBAL__N_120BrotliEncoderContextD1Ev,_ZN4node12_GLOBAL__N_120BrotliEncoderContextD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120BrotliEncoderContextD0Ev, @function
_ZN4node12_GLOBAL__N_120BrotliEncoderContextD0Ev:
.LFB9710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	BrotliEncoderDestroyInstance@PLT
.L84:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9710:
	.size	_ZN4node12_GLOBAL__N_120BrotliEncoderContextD0Ev, .-_ZN4node12_GLOBAL__N_120BrotliEncoderContextD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120BrotliDecoderContextD2Ev, @function
_ZN4node12_GLOBAL__N_120BrotliDecoderContextD2Ev:
.LFB9728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	BrotliDecoderDestroyInstance@PLT
.L90:
	movq	88(%rbx), %rdi
	addq	$104, %rbx
	cmpq	%rbx, %rdi
	je	.L89
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9728:
	.size	_ZN4node12_GLOBAL__N_120BrotliDecoderContextD2Ev, .-_ZN4node12_GLOBAL__N_120BrotliDecoderContextD2Ev
	.set	_ZN4node12_GLOBAL__N_120BrotliDecoderContextD1Ev,_ZN4node12_GLOBAL__N_120BrotliDecoderContextD2Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Z_MEM_ERROR"
.LC2:
	.string	"Z_STREAM_ERROR"
.LC3:
	.string	"Z_ERRNO"
.LC4:
	.string	"Z_NEED_DICT"
.LC5:
	.string	"Z_STREAM_END"
.LC6:
	.string	"Z_OK"
.LC7:
	.string	"Z_UNKNOWN_ERROR"
.LC8:
	.string	"Z_BUF_ERROR"
.LC9:
	.string	"Z_DATA_ERROR"
.LC10:
	.string	"Z_VERSION_ERROR"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ZlibContext15ErrorForMessageEPKc.isra.0, @function
_ZNK4node12_GLOBAL__N_111ZlibContext15ErrorForMessageEPKc.isra.0:
.LFB10325:
	.cfi_startproc
	testq	%rdx, %rdx
	movq	%rdi, %rax
	cmove	%rcx, %rdx
	leaq	.LC6(%rip), %rcx
	testl	%esi, %esi
	je	.L98
	leaq	.LC5(%rip), %rcx
	cmpl	$1, %esi
	je	.L98
	leaq	.LC4(%rip), %rcx
	cmpl	$2, %esi
	je	.L98
	leaq	.LC3(%rip), %rcx
	cmpl	$-1, %esi
	je	.L98
	leaq	.LC2(%rip), %rcx
	cmpl	$-2, %esi
	je	.L98
	leaq	.LC9(%rip), %rcx
	cmpl	$-3, %esi
	je	.L98
	leaq	.LC1(%rip), %rcx
	cmpl	$-4, %esi
	je	.L98
	leaq	.LC8(%rip), %rcx
	cmpl	$-5, %esi
	je	.L98
	cmpl	$-6, %esi
	leaq	.LC7(%rip), %rcx
	leaq	.LC10(%rip), %rdi
	cmove	%rdi, %rcx
.L98:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movl	%esi, 16(%rax)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	ret
	.cfi_endproc
.LFE10325:
	.size	_ZNK4node12_GLOBAL__N_111ZlibContext15ErrorForMessageEPKc.isra.0, .-_ZNK4node12_GLOBAL__N_111ZlibContext15ErrorForMessageEPKc.isra.0
	.section	.text._ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i,"axG",@progbits,_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i,comdat
	.p2align 4
	.weak	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i
	.type	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i, @function
_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i:
.LFB7581:
	.cfi_startproc
	endbr64
	movq	-8(%rdi), %rdx
	leaq	-16(%rdi), %r8
	subl	$1, 2156(%rdx)
	js	.L122
	movq	-16(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L122:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7581:
	.size	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i, .-_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB10485:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10485:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L125:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L135
.L126:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L127
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L126
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7147:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE11FreeForZlibEPvS4_, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE11FreeForZlibEPvS4_:
.LFB10483:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L136
	movq	-8(%rsi), %rax
	lock subq	%rax, 224(%rdi)
	leaq	-8(%rsi), %rdi
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L136:
	ret
	.cfi_endproc
.LFE10483:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE11FreeForZlibEPvS4_, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE11FreeForZlibEPvS4_
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE11FreeForZlibEPvS4_, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE11FreeForZlibEPvS4_:
.LFB10481:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L138
	movq	-8(%rsi), %rax
	lock subq	%rax, 224(%rdi)
	leaq	-8(%rsi), %rdi
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	ret
	.cfi_endproc
.LFE10481:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE11FreeForZlibEPvS4_, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE11FreeForZlibEPvS4_
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120BrotliDecoderContextD0Ev, @function
_ZN4node12_GLOBAL__N_120BrotliDecoderContextD0Ev:
.LFB9730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	BrotliDecoderDestroyInstance@PLT
.L141:
	movq	88(%r12), %rdi
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9730:
	.size	_ZN4node12_GLOBAL__N_120BrotliDecoderContextD0Ev, .-_ZN4node12_GLOBAL__N_120BrotliDecoderContextD0Ev
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE14AllocForBrotliEPvm, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE14AllocForBrotliEPvm:
.LFB10479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	addq	$8, %rsi
	cmovne	%rsi, %r13
	movq	%rsi, %rbx
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L155
.L148:
	movq	%rbx, (%rax)
	lock addq	%rbx, 224(%r12)
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L148
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10479:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE14AllocForBrotliEPvm, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE14AllocForBrotliEPvm
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE12AllocForZlibEPvjj, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE12AllocForZlibEPvjj:
.LFB8484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	imulq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L157
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rax, %rcx
	jne	.L167
.L157:
	addq	$8, %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L168
.L158:
	movq	%rbx, (%rax)
	lock addq	%rbx, 224(%r12)
	addq	$8, %rax
.L156:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L156
	jmp	.L158
	.cfi_endproc
.LFE8484:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE12AllocForZlibEPvjj, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE12AllocForZlibEPvjj
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110ZlibStreamD2Ev, @function
_ZN4node12_GLOBAL__N_110ZlibStreamD2Ev:
.LFB10225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 201(%rdi)
	movq	%rax, 56(%rdi)
	jne	.L201
	movl	$256, %eax
	cmpb	$0, 200(%rdi)
	movq	%rdi, %r12
	movw	%ax, 202(%rdi)
	je	.L202
	movl	264(%rdi), %eax
	cmpl	$7, %eax
	jg	.L203
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L183
	cmpl	$5, %eax
	je	.L183
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L184
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L176
.L184:
	leaq	304(%r12), %rdi
	call	inflateEnd@PLT
	testl	%eax, %eax
	jne	.L204
	.p2align 4,,10
	.p2align 3
.L176:
	movl	$0, 264(%r12)
	movq	280(%r12), %rax
	cmpq	288(%r12), %rax
	je	.L178
	movq	%rax, 288(%r12)
.L178:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	cmpq	$0, 232(%r12)
	jne	.L205
	movq	224(%r12), %rax
	testq	%rax, %rax
	jne	.L206
	movq	280(%r12), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_111ZlibContextE(%rip), %rax
	movq	%rax, 240(%r12)
	testq	%rdi, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L182
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L182:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	leaq	304(%r12), %rdi
	call	deflateEnd@PLT
	testl	%eax, %eax
	je	.L176
.L204:
	cmpl	$-3, %eax
	je	.L176
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L201:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10225:
	.size	_ZN4node12_GLOBAL__N_110ZlibStreamD2Ev, .-_ZN4node12_GLOBAL__N_110ZlibStreamD2Ev
	.set	_ZN4node12_GLOBAL__N_110ZlibStreamD1Ev,_ZN4node12_GLOBAL__N_110ZlibStreamD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED2Ev, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED2Ev:
.LFB10221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 201(%rdi)
	movq	%rax, 56(%rdi)
	jne	.L225
	movl	$256, %eax
	cmpb	$0, 200(%rdi)
	movq	%rdi, %r12
	movw	%ax, 202(%rdi)
	je	.L226
	movq	328(%rdi), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L210
	call	BrotliEncoderDestroyInstance@PLT
.L210:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	cmpq	$0, 232(%r12)
	jne	.L227
	movq	224(%r12), %rax
	testq	%rax, %rax
	jne	.L228
	movq	328(%r12), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE(%rip), %rax
	movq	%rax, 240(%r12)
	testq	%rdi, %rdi
	je	.L213
	call	BrotliEncoderDestroyInstance@PLT
.L213:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L214:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10221:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED2Ev, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED2Ev
	.set	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED1Ev,_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED2Ev
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_120BrotliEncoderContext14MemoryInfoNameEv:
.LFB7609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$20, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC11(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$1954047348, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L232
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L232:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7609:
	.size	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_120BrotliEncoderContext14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_110ZlibStream14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_110ZlibStream14MemoryInfoNameEv:
.LFB7660:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$10, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$7310033046657920090, %rcx
	movq	%rdx, (%rdi)
	movl	$28001, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE7660:
	.size	_ZNK4node12_GLOBAL__N_110ZlibStream14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_110ZlibStream14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ZlibContext14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_111ZlibContext14MemoryInfoNameEv:
.LFB7603:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$116, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$8389765491042380890, %rcx
	movq	%rdx, (%rdi)
	movl	$30821, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7603:
	.size	_ZNK4node12_GLOBAL__N_111ZlibContext14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_111ZlibContext14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE14MemoryInfoNameEv:
.LFB10264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$23, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$24933, %edx
	movl	$1920226158, 16(%rax)
	movw	%dx, 20(%rax)
	movb	$109, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L238:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10264:
	.size	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE14MemoryInfoNameEv
	.set	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE14MemoryInfoNameEv,_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_120BrotliDecoderContext14MemoryInfoNameEv:
.LFB7614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$20, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC13(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$1954047348, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L242:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7614:
	.size	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_120BrotliDecoderContext14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev:
.LFB10223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 201(%rdi)
	movq	%rax, 56(%rdi)
	jne	.L261
	movl	$256, %eax
	cmpb	$0, 200(%rdi)
	movq	%rdi, %r12
	movw	%ax, 202(%rdi)
	je	.L262
	movq	328(%rdi), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L246
	call	BrotliEncoderDestroyInstance@PLT
.L246:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	cmpq	$0, 232(%r12)
	jne	.L263
	movq	224(%r12), %rax
	testq	%rax, %rax
	jne	.L264
	movq	328(%r12), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE(%rip), %rax
	movq	%rax, 240(%r12)
	testq	%rdi, %rdi
	je	.L249
	call	BrotliEncoderDestroyInstance@PLT
.L249:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L250
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L250:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$336, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10223:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110ZlibStreamD0Ev, @function
_ZN4node12_GLOBAL__N_110ZlibStreamD0Ev:
.LFB10227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 201(%rdi)
	movq	%rax, 56(%rdi)
	jne	.L297
	movl	$256, %eax
	cmpb	$0, 200(%rdi)
	movq	%rdi, %r12
	movw	%ax, 202(%rdi)
	je	.L298
	movl	264(%rdi), %eax
	cmpl	$7, %eax
	jg	.L299
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L279
	cmpl	$5, %eax
	je	.L279
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L280
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L272
.L280:
	leaq	304(%r12), %rdi
	call	inflateEnd@PLT
	testl	%eax, %eax
	jne	.L300
	.p2align 4,,10
	.p2align 3
.L272:
	movl	$0, 264(%r12)
	movq	280(%r12), %rax
	cmpq	288(%r12), %rax
	je	.L274
	movq	%rax, 288(%r12)
.L274:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	cmpq	$0, 232(%r12)
	jne	.L301
	movq	224(%r12), %rax
	testq	%rax, %rax
	jne	.L302
	movq	280(%r12), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_111ZlibContextE(%rip), %rax
	movq	%rax, 240(%r12)
	testq	%rdi, %rdi
	je	.L277
	call	_ZdlPv@PLT
.L277:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L278
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L278:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$416, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	leaq	304(%r12), %rdi
	call	deflateEnd@PLT
	testl	%eax, %eax
	je	.L272
.L300:
	cmpl	$-3, %eax
	je	.L272
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L302:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L298:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10227:
	.size	_ZN4node12_GLOBAL__N_110ZlibStreamD0Ev, .-_ZN4node12_GLOBAL__N_110ZlibStreamD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED2Ev, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED2Ev:
.LFB10217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 201(%rdi)
	movq	%rax, 56(%rdi)
	jne	.L322
	movl	$256, %eax
	cmpb	$0, 200(%rdi)
	movq	%rdi, %r12
	movw	%ax, 202(%rdi)
	je	.L323
	movq	360(%rdi), %rdi
	movq	$0, 360(%r12)
	testq	%rdi, %rdi
	je	.L306
	call	BrotliDecoderDestroyInstance@PLT
.L306:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	cmpq	$0, 232(%r12)
	jne	.L324
	movq	224(%r12), %rax
	testq	%rax, %rax
	jne	.L325
	movq	360(%r12), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE(%rip), %rax
	movq	%rax, 240(%r12)
	testq	%rdi, %rdi
	je	.L309
	call	BrotliDecoderDestroyInstance@PLT
.L309:
	movq	328(%r12), %rdi
	leaq	344(%r12), %rax
	cmpq	%rax, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L311
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L311:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L325:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10217:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED2Ev, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED2Ev
	.set	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED1Ev,_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev:
.LFB10219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 201(%rdi)
	movq	%rax, 56(%rdi)
	jne	.L345
	movl	$256, %eax
	cmpb	$0, 200(%rdi)
	movq	%rdi, %r12
	movw	%ax, 202(%rdi)
	je	.L346
	movq	360(%rdi), %rdi
	movq	$0, 360(%r12)
	testq	%rdi, %rdi
	je	.L329
	call	BrotliDecoderDestroyInstance@PLT
.L329:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	cmpq	$0, 232(%r12)
	jne	.L347
	movq	224(%r12), %rax
	testq	%rax, %rax
	jne	.L348
	movq	360(%r12), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE(%rip), %rax
	movq	%rax, 240(%r12)
	testq	%rdi, %rdi
	je	.L332
	call	BrotliDecoderDestroyInstance@PLT
.L332:
	movq	328(%r12), %rdi
	leaq	344(%r12), %rax
	cmpq	%rax, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L334
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L334:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$368, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10219:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv:
.LFB9718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$9, 248(%rdi)
	jne	.L354
	movq	%rdi, %rbx
	movq	328(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L355
	subq	$8, %rsp
	movq	256(%rbx), %rax
	leaq	-32(%rbp), %rcx
	movl	288(%rbx), %esi
	pushq	$0
	leaq	272(%rbx), %rdx
	leaq	264(%rbx), %r9
	leaq	280(%rbx), %r8
	movq	%rax, -32(%rbp)
	call	BrotliEncoderCompressStream@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	movq	-32(%rbp), %rax
	setne	320(%rbx)
	movq	%rax, 256(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L356:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9718:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv, @function
_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv:
.LFB10486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$9, 192(%rdi)
	jne	.L362
	movq	%rdi, %rbx
	movq	272(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L363
	subq	$8, %rsp
	movq	200(%rbx), %rax
	leaq	-32(%rbp), %rcx
	movl	232(%rbx), %esi
	pushq	$0
	leaq	216(%rbx), %rdx
	leaq	208(%rbx), %r9
	leaq	224(%rbx), %r8
	movq	%rax, -32(%rbp)
	call	BrotliEncoderCompressStream@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	movq	-32(%rbp), %rax
	setne	264(%rbx)
	movq	%rax, 200(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10486:
	.size	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv, .-_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE:
.LFB9395:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	3280(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L394
	testq	%rax, %rax
	je	.L369
	movq	(%rdx), %rcx
	cmpq	%rcx, (%rax)
	sete	%al
.L367:
	testb	%al, %al
	je	.L369
	movq	352(%rbx), %rsi
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	0(%r13), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L395
.L371:
	movl	16(%r13), %esi
	movq	352(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%r13), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L396
.L372:
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -48(%rbp)
	movq	360(%rbx), %rax
	movq	1160(%rax), %r13
	testq	%rdi, %rdi
	je	.L373
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L397
.L373:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L380
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L398
.L380:
	cmpb	$0, 202(%r12)
	movb	$0, 201(%r12)
	jne	.L399
.L376:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L400
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	-64(%rbp), %rcx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L399:
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L401
	movq	360(%r12), %rdi
	movq	$0, 360(%r12)
	testq	%rdi, %rdi
	je	.L378
	call	BrotliDecoderDestroyInstance@PLT
.L378:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L394:
	testq	%rax, %rax
	sete	%al
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L369:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L401:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9395:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE:
.LFB9377:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	3280(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L431
	testq	%rax, %rax
	je	.L406
	movq	(%rdx), %rcx
	cmpq	%rcx, (%rax)
	sete	%al
.L404:
	testb	%al, %al
	je	.L406
	movq	352(%rbx), %rsi
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	0(%r13), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L432
.L408:
	movl	16(%r13), %esi
	movq	352(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%r13), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L433
.L409:
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -48(%rbp)
	movq	360(%rbx), %rax
	movq	1160(%rax), %r13
	testq	%rdi, %rdi
	je	.L410
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L434
.L410:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L417
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L435
.L417:
	cmpb	$0, 202(%r12)
	movb	$0, 201(%r12)
	jne	.L436
.L413:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	-64(%rbp), %rcx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L436:
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L438
	movq	328(%r12), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L415
	call	BrotliEncoderDestroyInstance@PLT
.L415:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L431:
	testq	%rax, %rax
	sete	%al
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L433:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L438:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9377:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	.section	.rodata.str1.1
.LC14:
	.string	"basic_string::append"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEv, @function
_ZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEv:
.LFB7694:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$8, 8(%rdi)
	jne	.L462
	movq	%rdi, %rbx
	movq	120(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L463
	movq	16(%rbx), %rax
	leaq	-120(%rbp), %rdx
	leaq	40(%rbx), %rcx
	xorl	%r9d, %r9d
	leaq	32(%rbx), %rsi
	leaq	24(%rbx), %r8
	movq	%rax, -120(%rbp)
	call	BrotliDecoderDecompressStream@PLT
	movq	-120(%rbp), %rdx
	movl	%eax, 80(%rbx)
	movq	%rdx, 16(%rbx)
	testl	%eax, %eax
	je	.L464
.L439:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	120(%rbx), %rdi
	leaq	-96(%rbp), %r13
	leaq	-112(%rbp), %r14
	call	BrotliDecoderGetErrorCode@PLT
	movl	%eax, 84(%rbx)
	movl	%eax, %edi
	call	BrotliDecoderErrorString@PLT
	movq	%r13, -112(%rbp)
	movq	%rax, %rdi
	movl	$1599230533, -96(%rbp)
	movq	%rax, %r12
	movq	$4, -104(%rbp)
	movb	$0, -92(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387899, %rax
	cmpq	%rax, %rdx
	ja	.L466
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-64(%rbp), %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r12, -80(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L467
	movq	%rcx, -80(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -64(%rbp)
.L445:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -72(%rbp)
	movq	%rdx, (%rax)
	movq	-80(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	88(%rbx), %rdi
	cmpq	%r12, %rdx
	je	.L468
	leaq	104(%rbx), %rcx
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L469
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	104(%rbx), %rcx
	movq	%rdx, 88(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 96(%rbx)
	testq	%rdi, %rdi
	je	.L451
	movq	%rdi, -80(%rbp)
	movq	%rcx, -64(%rbp)
.L449:
	movq	$0, -72(%rbp)
	movb	$0, (%rdi)
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L452
	call	_ZdlPv@PLT
.L452:
	movq	-112(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L439
	call	_ZdlPv@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L467:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -64(%rbp)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L462:
	leaq	_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L468:
	movq	-72(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L447
	cmpq	$1, %rdx
	je	.L470
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-72(%rbp), %rdx
	movq	88(%rbx), %rdi
.L447:
	movq	%rdx, 96(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-80(%rbp), %rdi
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	%rdx, 88(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 96(%rbx)
.L451:
	movq	%r12, -80(%rbp)
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L470:
	movzbl	-64(%rbp), %eax
	movb	%al, (%rdi)
	movq	-72(%rbp), %rdx
	movq	88(%rbx), %rdi
	jmp	.L447
.L465:
	call	__stack_chk_fail@PLT
.L466:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7694:
	.size	_ZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEv, .-_ZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv:
.LFB9738:
	.cfi_startproc
	endbr64
	addq	$240, %rdi
	jmp	_ZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEv
	.cfi_endproc
.LFE9738:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv, @function
_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv:
.LFB10487:
	.cfi_startproc
	endbr64
	addq	$184, %rdi
	jmp	_ZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEv
	.cfi_endproc
.LFE10487:
	.size	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv, .-_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv
	.section	.rodata.str1.1
.LC15:
	.string	"Failed to set dictionary"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ZlibContext13SetDictionaryEv, @function
_ZN4node12_GLOBAL__N_111ZlibContext13SetDictionaryEv:
.LFB7682:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	48(%rsi), %rdx
	movq	40(%rsi), %rsi
	cmpq	%rdx, %rsi
	je	.L478
	movl	24(%rbx), %eax
	movl	$0, 8(%rbx)
	cmpl	$5, %eax
	je	.L476
	cmpl	$6, %eax
	je	.L477
	cmpl	$1, %eax
	je	.L476
.L478:
	pxor	%xmm0, %xmm0
	movq	%r12, %rax
	popq	%rbx
	movl	$0, 16(%r12)
	movups	%xmm0, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	subq	%rsi, %rdx
	leaq	64(%rbx), %rdi
	call	inflateSetDictionary@PLT
	movl	%eax, 8(%rbx)
.L479:
	testl	%eax, %eax
	je	.L478
	movq	112(%rbx), %rdx
	leaq	.LC15(%rip), %rcx
	testq	%rdx, %rdx
	cmove	%rcx, %rdx
	leaq	.LC5(%rip), %rcx
	cmpl	$1, %eax
	je	.L481
	leaq	.LC4(%rip), %rcx
	cmpl	$2, %eax
	je	.L481
	leaq	.LC3(%rip), %rcx
	cmpl	$-1, %eax
	je	.L481
	leaq	.LC2(%rip), %rcx
	cmpl	$-2, %eax
	je	.L481
	leaq	.LC9(%rip), %rcx
	cmpl	$-3, %eax
	je	.L481
	leaq	.LC1(%rip), %rcx
	cmpl	$-4, %eax
	je	.L481
	leaq	.LC8(%rip), %rcx
	cmpl	$-5, %eax
	je	.L481
	cmpl	$-6, %eax
	leaq	.LC7(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	cmove	%rsi, %rcx
.L481:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movl	%eax, 16(%r12)
	popq	%rbx
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, %rax
	movups	%xmm0, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	subq	%rsi, %rdx
	leaq	64(%rbx), %rdi
	call	deflateSetDictionary@PLT
	movl	%eax, 8(%rbx)
	jmp	.L479
	.cfi_endproc
.LFE7682:
	.size	_ZN4node12_GLOBAL__N_111ZlibContext13SetDictionaryEv, .-_ZN4node12_GLOBAL__N_111ZlibContext13SetDictionaryEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEv, @function
_ZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEv:
.LFB7670:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$7, 24(%rdi)
	ja	.L506
	movl	24(%rdi), %eax
	leaq	.L508(%rip), %rdx
	movq	%rdi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L508:
	.long	.L506-.L508
	.long	.L510-.L508
	.long	.L509-.L508
	.long	.L510-.L508
	.long	.L509-.L508
	.long	.L510-.L508
	.long	.L509-.L508
	.long	.L507-.L508
	.text
	.p2align 4,,10
	.p2align 3
.L507:
	movl	72(%rdi), %edx
	movl	36(%rdi), %eax
	testl	%edx, %edx
	je	.L512
	movq	64(%rdi), %rcx
	testl	%eax, %eax
	jne	.L541
	testq	%rcx, %rcx
	je	.L509
	cmpb	$31, (%rcx)
	je	.L542
.L517:
	movl	$2, 24(%rbx)
.L509:
	movl	12(%rbx), %esi
	leaq	64(%rbx), %r12
	movq	%r12, %rdi
	call	inflate@PLT
	cmpl	$6, 24(%rbx)
	movl	%eax, 8(%rbx)
	je	.L522
	cmpl	$2, %eax
	je	.L543
.L522:
	movl	72(%rbx), %edx
	testl	%edx, %edx
	je	.L505
	leaq	-64(%rbp), %r13
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L544:
	cmpl	$1, 8(%rbx)
	jne	.L505
	movq	64(%rbx), %rax
	cmpb	$0, (%rax)
	je	.L505
	movl	$0, 8(%rbx)
	movq	%r12, %rdi
	call	inflateReset@PLT
	movl	%eax, 8(%rbx)
	testl	%eax, %eax
	jne	.L527
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN4node12_GLOBAL__N_111ZlibContext13SetDictionaryEv
.L527:
	movl	12(%rbx), %esi
	movq	%r12, %rdi
	call	inflate@PLT
	movl	%eax, 8(%rbx)
	movl	72(%rbx), %eax
	testl	%eax, %eax
	je	.L505
.L526:
	cmpl	$4, 24(%rbx)
	je	.L544
	.p2align 4,,10
	.p2align 3
.L505:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L545
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movl	12(%rdi), %esi
	leaq	64(%rdi), %rdi
	call	deflate@PLT
	movl	%eax, 8(%rbx)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L512:
	cmpl	$1, %eax
	jbe	.L509
.L515:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L543:
	movq	48(%rbx), %rdx
	movq	40(%rbx), %rsi
	cmpq	%rdx, %rsi
	je	.L522
	subq	%rsi, %rdx
	movq	%r12, %rdi
	call	inflateSetDictionary@PLT
	movl	%eax, 8(%rbx)
	testl	%eax, %eax
	je	.L546
	cmpl	$-3, %eax
	jne	.L522
	movl	$2, 8(%rbx)
	jmp	.L522
.L541:
	cmpl	$1, %eax
	jne	.L515
	testq	%rcx, %rcx
	je	.L509
.L519:
	cmpb	$-117, (%rcx)
	jne	.L517
	movl	$2, 36(%rbx)
	movl	$4, 24(%rbx)
	jmp	.L509
.L542:
	movl	$1, 36(%rdi)
	addq	$1, %rcx
	cmpl	$1, %edx
	jne	.L519
	jmp	.L509
.L546:
	movl	12(%rbx), %esi
	movq	%r12, %rdi
	call	inflate@PLT
	movl	%eax, 8(%rbx)
	jmp	.L522
.L506:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L545:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7670:
	.size	_ZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEv, .-_ZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv:
.LFB9667:
	.cfi_startproc
	endbr64
	addq	$240, %rdi
	jmp	_ZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEv
	.cfi_endproc
.LFE9667:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv, @function
_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv:
.LFB10488:
	.cfi_startproc
	endbr64
	addq	$184, %rdi
	jmp	_ZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEv
	.cfi_endproc
.LFE10488:
	.size	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv, .-_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE:
.LFB8486:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	3280(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L592
	testq	%rax, %rax
	je	.L553
	movq	(%rdx), %rcx
	cmpq	%rcx, (%rax)
	sete	%al
.L551:
	testb	%al, %al
	je	.L553
	movq	352(%rbx), %rsi
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	0(%r13), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L593
.L555:
	movl	16(%r13), %esi
	movq	352(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%r13), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L594
.L556:
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -48(%rbp)
	movq	360(%rbx), %rax
	movq	1160(%rax), %r13
	testq	%rdi, %rdi
	je	.L557
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L595
.L557:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L570
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L596
.L570:
	cmpb	$0, 202(%r12)
	movb	$0, 201(%r12)
	jne	.L597
.L560:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L598
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L596:
	leaq	-64(%rbp), %rcx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L597:
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L599
	movl	264(%r12), %eax
	cmpl	$7, %eax
	jg	.L600
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L572
	cmpl	$5, %eax
	je	.L572
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L573
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L566
.L573:
	leaq	304(%r12), %rdi
	call	inflateEnd@PLT
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L592:
	testq	%rax, %rax
	sete	%al
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L553:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	304(%r12), %rdi
	call	deflateEnd@PLT
.L565:
	testl	%eax, %eax
	je	.L566
	cmpl	$-3, %eax
	jne	.L601
.L566:
	movl	$0, 264(%r12)
	movq	280(%r12), %rax
	cmpq	288(%r12), %rax
	je	.L568
	movq	%rax, 288(%r12)
.L568:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L599:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L601:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L598:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8486:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L633
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L617
	cmpw	$1040, %cx
	jne	.L604
.L617:
	movq	23(%rdx), %r12
.L606:
	testq	%r12, %r12
	je	.L602
	cmpb	$0, 201(%r12)
	jne	.L634
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L635
	movl	264(%r12), %eax
	cmpl	$7, %eax
	jg	.L636
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L618
	cmpl	$5, %eax
	je	.L618
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L619
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L614
.L619:
	leaq	304(%r12), %rdi
	call	inflateEnd@PLT
.L613:
	testl	%eax, %eax
	je	.L614
	cmpl	$-3, %eax
	jne	.L637
.L614:
	movl	$0, 264(%r12)
	movq	280(%r12), %rax
	cmpq	288(%r12), %rax
	je	.L616
	movq	%rax, 288(%r12)
.L616:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	movb	$1, 202(%r12)
.L602:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	.cfi_restore_state
	leaq	304(%r12), %rdi
	call	deflateEnd@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L636:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L637:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8960:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L655
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L647
	cmpw	$1040, %cx
	jne	.L640
.L647:
	movq	23(%rdx), %r12
.L642:
	testq	%r12, %r12
	je	.L638
	cmpb	$0, 201(%r12)
	jne	.L656
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L657
	movq	360(%r12), %rdi
	movq	$0, 360(%r12)
	testq	%rdi, %rdi
	je	.L646
	call	BrotliDecoderDestroyInstance@PLT
.L646:
	movl	$0, 248(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movb	$1, 202(%r12)
.L638:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L655:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L657:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8972:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L675
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L667
	cmpw	$1040, %cx
	jne	.L660
.L667:
	movq	23(%rdx), %r12
.L662:
	testq	%r12, %r12
	je	.L658
	cmpb	$0, 201(%r12)
	jne	.L676
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L677
	movq	328(%r12), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L666
	call	BrotliEncoderDestroyInstance@PLT
.L666:
	movl	$0, 248(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_restore_state
	movb	$1, 202(%r12)
.L658:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L675:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L677:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8965:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L691
	cmpw	$1040, %cx
	jne	.L679
.L691:
	movq	23(%rdx), %r12
.L681:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L682
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L693
.L684:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L694
	movq	8(%rbx), %rdi
.L686:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	8(%rbx), %r14
	movl	$368, %edi
	movl	%eax, %r13d
	call	_Znwm@PLT
	addq	$8, %r14
	movl	$40, %ecx
	movq	%r12, %rsi
	movsd	.LC16(%rip), %xmm0
	movq	%rax, %rdi
	movq	%r14, %rdx
	movq	%rax, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node14ThreadPoolWorkE(%rip), %rax
	movq	%r12, 64(%rbx)
	movq	%rax, 56(%rbx)
	testq	%r12, %r12
	je	.L695
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 200(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE(%rip), %rax
	movq	%rax, 240(%rbx)
	leaq	344(%rbx), %rax
	movq	%rax, 328(%rbx)
	movq	24(%rbx), %rax
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movl	$0, 248(%rbx)
	movl	$0, 288(%rbx)
	movq	$0, 312(%rbx)
	movq	$1, 320(%rbx)
	movq	$0, 336(%rbx)
	movb	$0, 344(%rbx)
	movq	$0, 360(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 256(%rbx)
	movups	%xmm0, 272(%rbx)
	movups	%xmm0, 296(%rbx)
	testq	%rax, %rax
	je	.L690
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L689
.L690:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L689:
	leaq	16+_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEEE(%rip), %rax
	movl	%r13d, 248(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L682:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L684
.L693:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L679:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L695:
	leaq	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8969:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L709
	cmpw	$1040, %cx
	jne	.L697
.L709:
	movq	23(%rdx), %r12
.L699:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L700
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L711
.L702:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L712
	movq	8(%rbx), %rdi
.L704:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	8(%rbx), %r14
	movl	$336, %edi
	movl	%eax, %r13d
	call	_Znwm@PLT
	addq	$8, %r14
	movl	$40, %ecx
	movq	%r12, %rsi
	movsd	.LC16(%rip), %xmm0
	movq	%rax, %rdi
	movq	%r14, %rdx
	movq	%rax, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node14ThreadPoolWorkE(%rip), %rax
	movq	%r12, 64(%rbx)
	movq	%rax, 56(%rbx)
	testq	%r12, %r12
	je	.L713
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 200(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE(%rip), %rax
	movq	%rax, 240(%rbx)
	movq	24(%rbx), %rax
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movl	$0, 248(%rbx)
	movl	$0, 288(%rbx)
	movq	$0, 312(%rbx)
	movb	$0, 320(%rbx)
	movq	$0, 328(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 256(%rbx)
	movups	%xmm0, 272(%rbx)
	movups	%xmm0, 296(%rbx)
	testq	%rax, %rax
	je	.L708
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L707
.L708:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L707:
	leaq	16+_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEEE(%rip), %rax
	movl	%r13d, 248(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L712:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L700:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L702
.L711:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L697:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8962:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L727
	cmpw	$1040, %cx
	jne	.L715
.L727:
	movq	23(%rdx), %r12
.L717:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L718
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L729
.L720:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L730
	movq	8(%rbx), %rdi
.L722:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	8(%rbx), %r14
	movl	$416, %edi
	movl	%eax, %r13d
	call	_Znwm@PLT
	addq	$8, %r14
	movl	$40, %ecx
	movq	%r12, %rsi
	movsd	.LC16(%rip), %xmm0
	movq	%rax, %rdi
	movq	%r14, %rdx
	movq	%rax, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node14ThreadPoolWorkE(%rip), %rax
	movq	%r12, 64(%rbx)
	movq	%rax, 56(%rbx)
	testq	%r12, %r12
	je	.L731
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	$0, 200(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	leaq	16+_ZTVN4node12_GLOBAL__N_111ZlibContextE(%rip), %rax
	movq	%rax, 240(%rbx)
	movq	24(%rbx), %rax
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 296(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm1, 248(%rbx)
	movups	%xmm0, 280(%rbx)
	testq	%rax, %rax
	je	.L726
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L725
.L726:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L725:
	leaq	16+_ZTVN4node12_GLOBAL__N_110ZlibStreamE(%rip), %rax
	movl	%r13d, 264(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L718:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L720
.L729:
	leaq	_ZZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L715:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L731:
	leaq	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7644:
	.size	_ZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC17:
	.string	"Failed to reset stream"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L770
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L758
	cmpw	$1040, %cx
	jne	.L734
.L758:
	movq	23(%rdx), %r12
.L736:
	testq	%r12, %r12
	je	.L732
	cmpl	$6, 264(%r12)
	movl	$0, 248(%r12)
	ja	.L739
	movl	264(%r12), %eax
	leaq	.L741(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L741:
	.long	.L739-.L741
	.long	.L742-.L741
	.long	.L740-.L741
	.long	.L742-.L741
	.long	.L740-.L741
	.long	.L742-.L741
	.long	.L740-.L741
	.text
	.p2align 4,,10
	.p2align 3
.L740:
	leaq	304(%r12), %rdi
	call	inflateReset@PLT
	movl	%eax, 248(%r12)
	testl	%eax, %eax
	je	.L739
.L772:
	movq	352(%r12), %rdx
	leaq	.LC17(%rip), %rcx
	testq	%rdx, %rdx
	cmove	%rcx, %rdx
	leaq	.LC5(%rip), %rcx
	cmpl	$1, %eax
	je	.L745
	leaq	.LC4(%rip), %rcx
	cmpl	$2, %eax
	je	.L745
	leaq	.LC3(%rip), %rcx
	cmpl	$-1, %eax
	je	.L745
	leaq	.LC2(%rip), %rcx
	cmpl	$-2, %eax
	je	.L745
	leaq	.LC9(%rip), %rcx
	cmpl	$-3, %eax
	je	.L745
	leaq	.LC1(%rip), %rcx
	cmpl	$-4, %eax
	je	.L745
	leaq	.LC8(%rip), %rcx
	cmpl	$-5, %eax
	je	.L745
	cmpl	$-6, %eax
	leaq	.LC7(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	cmove	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L745:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movl	%eax, -32(%rbp)
	punpcklqdq	%xmm1, %xmm0
	leaq	-48(%rbp), %r13
	movaps	%xmm0, -48(%rbp)
.L747:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE
.L746:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
.L732:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L771
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	leaq	304(%r12), %rdi
	call	deflateReset@PLT
	movl	%eax, 248(%r12)
	testl	%eax, %eax
	jne	.L772
.L739:
	leaq	-48(%rbp), %r13
	leaq	240(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN4node12_GLOBAL__N_111ZlibContext13SetDictionaryEv
	cmpq	$0, -40(%rbp)
	je	.L746
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L734:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L770:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L771:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8961:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"Could not initialize Brotli instance"
	.align 8
.LC19:
	.string	"ERR_ZLIB_INITIALIZATION_FAILED"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L789
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L784
	cmpw	$1040, %cx
	jne	.L775
.L784:
	movq	23(%rdx), %r12
.L777:
	testq	%r12, %r12
	je	.L773
	movq	296(%r12), %rdi
	movq	312(%r12), %rdx
	movq	304(%r12), %rsi
	call	BrotliEncoderCreateInstance@PLT
	movq	328(%r12), %rdi
	movq	%rax, 328(%r12)
	testq	%rdi, %rdi
	je	.L780
	call	BrotliEncoderDestroyInstance@PLT
	movq	328(%r12), %rax
.L780:
	testq	%rax, %rax
	je	.L790
	movl	$0, -32(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
.L782:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
.L773:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L791
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	.cfi_restore_state
	leaq	.LC19(%rip), %rax
	leaq	.LC18(%rip), %rcx
	movq	%r12, %rdi
	movl	$-1, -32(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	leaq	-48(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L775:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L791:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8968:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L808
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L803
	cmpw	$1040, %cx
	jne	.L794
.L803:
	movq	23(%rdx), %r12
.L796:
	testq	%r12, %r12
	je	.L792
	movq	296(%r12), %rdi
	movq	312(%r12), %rdx
	movq	304(%r12), %rsi
	call	BrotliDecoderCreateInstance@PLT
	movq	360(%r12), %rdi
	movq	%rax, 360(%r12)
	testq	%rdi, %rdi
	je	.L799
	call	BrotliDecoderDestroyInstance@PLT
	movq	360(%r12), %rax
.L799:
	testq	%rax, %rax
	je	.L809
	movl	$0, -32(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
.L801:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
.L792:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L810
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	leaq	.LC19(%rip), %rax
	leaq	.LC18(%rip), %rcx
	movq	%r12, %rdi
	movl	$-1, -32(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	leaq	-48(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L794:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L808:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L810:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8975:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.section	.rodata.str1.1
.LC20:
	.string	"Zlib error"
.LC21:
	.string	"Missing dictionary"
.LC22:
	.string	"Bad dictionary"
.LC23:
	.string	"unexpected end of file"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ZlibContext12GetErrorInfoEv, @function
_ZNK4node12_GLOBAL__N_111ZlibContext12GetErrorInfoEv:
.LFB7675:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	8(%rsi), %r8d
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	cmpl	$1, %r8d
	je	.L812
	jg	.L813
	cmpl	$-5, %r8d
	je	.L814
	testl	%r8d, %r8d
	jne	.L853
.L814:
	movl	96(%rsi), %edx
	testl	%edx, %edx
	jne	.L854
.L812:
	movl	$0, 16(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.L811:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L855
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	cmpl	$2, %r8d
	jne	.L856
	movq	112(%rsi), %rdx
	movq	40(%rsi), %rcx
	cmpq	%rcx, 48(%rsi)
	je	.L857
	leaq	.LC22(%rip), %rcx
	testq	%rdx, %rdx
	movl	$2, 16(%rdi)
	cmove	%rcx, %rdx
	leaq	.LC4(%rip), %rcx
	movq	%rcx, 8(%rdi)
	movq	%rdx, (%rdi)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L856:
	movq	112(%rsi), %rdx
	leaq	.LC7(%rip), %rcx
	testq	%rdx, %rdx
	jne	.L826
.L852:
	leaq	.LC20(%rip), %rdx
.L825:
	leaq	.LC3(%rip), %rcx
	cmpl	$-1, %r8d
	je	.L826
	leaq	.LC2(%rip), %rcx
	cmpl	$-2, %r8d
	je	.L826
	leaq	.LC9(%rip), %rcx
	cmpl	$-3, %r8d
	je	.L826
	leaq	.LC1(%rip), %rcx
	cmpl	$-4, %r8d
	je	.L826
	cmpl	$-6, %r8d
	leaq	.LC7(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	cmove	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L826:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movl	%r8d, 16(%rax)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L854:
	cmpl	$4, 12(%rsi)
	jne	.L812
	movq	112(%rsi), %rdx
	movq	%rax, %rdi
	movl	%r8d, %esi
	movq	%rax, -24(%rbp)
	leaq	.LC23(%rip), %rcx
	call	_ZNK4node12_GLOBAL__N_111ZlibContext15ErrorForMessageEPKc.isra.0
	movq	-24(%rbp), %rax
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L857:
	testq	%rdx, %rdx
	leaq	.LC21(%rip), %rcx
	movl	$2, 16(%rdi)
	cmove	%rcx, %rdx
	movq	%rdx, (%rdi)
	leaq	.LC4(%rip), %rdx
	movq	%rdx, 8(%rdi)
	jmp	.L811
.L855:
	call	__stack_chk_fail@PLT
.L853:
	movq	112(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L825
	jmp	.L852
	.cfi_endproc
.LFE7675:
	.size	_ZNK4node12_GLOBAL__N_111ZlibContext12GetErrorInfoEv, .-_ZNK4node12_GLOBAL__N_111ZlibContext12GetErrorInfoEv
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED1Ev, @function
_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED1Ev:
.LFB10491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 145(%rdi)
	movq	%rax, (%rdi)
	jne	.L882
	movl	$256, %eax
	cmpb	$0, 144(%rdi)
	movq	%rdi, %rbx
	movw	%ax, 146(%rdi)
	je	.L883
	movq	272(%rdi), %rdi
	movq	$0, 272(%rbx)
	testq	%rdi, %rdi
	je	.L861
	call	BrotliEncoderDestroyInstance@PLT
.L861:
	leaq	168(%rbx), %rax
	xorl	%r12d, %r12d
	movl	$0, 192(%rbx)
	xchgq	(%rax), %r12
	testq	%r12, %r12
	je	.L863
	movq	176(%rbx), %rax
	js	.L884
.L864:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L885
.L865:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L886
	cmpq	%r14, %rax
	jl	.L887
.L863:
	cmpq	$0, 176(%rbx)
	jne	.L888
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L889
	movq	272(%rbx), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE(%rip), %rax
	movq	%rax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L870
	call	BrotliEncoderDestroyInstance@PLT
.L870:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L871
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L871:
	leaq	-56(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L886:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L863
	movq	%r12, 40(%r13)
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L885:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L882:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L888:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L889:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L883:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L884:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L864
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10491:
	.size	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED1Ev, .-_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED1Ev
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD0Ev, @function
_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD0Ev:
.LFB10494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 145(%rdi)
	movq	%rax, (%rdi)
	jne	.L928
	movl	$256, %eax
	cmpb	$0, 144(%rdi)
	movq	%rdi, %rbx
	movw	%ax, 146(%rdi)
	je	.L929
	movl	208(%rdi), %eax
	cmpl	$7, %eax
	jg	.L930
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L910
	cmpl	$5, %eax
	je	.L910
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L911
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L897
.L911:
	leaq	248(%rbx), %rdi
	call	inflateEnd@PLT
	testl	%eax, %eax
	jne	.L931
	.p2align 4,,10
	.p2align 3
.L897:
	movl	$0, 208(%rbx)
	movq	224(%rbx), %rax
	cmpq	232(%rbx), %rax
	je	.L899
	movq	%rax, 232(%rbx)
.L899:
	leaq	168(%rbx), %rax
	xorl	%r12d, %r12d
	xchgq	(%rax), %r12
	testq	%r12, %r12
	je	.L901
	movq	176(%rbx), %rax
	js	.L932
.L902:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L933
.L903:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L934
	cmpq	%r14, %rax
	jge	.L901
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.p2align 4,,10
	.p2align 3
.L901:
	cmpq	$0, 176(%rbx)
	jne	.L935
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L936
	movq	224(%rbx), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_111ZlibContextE(%rip), %rax
	movq	%rax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L908
	call	_ZdlPv@PLT
.L908:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L909
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L909:
	leaq	-56(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$416, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	leaq	248(%rbx), %rdi
	call	deflateEnd@PLT
	testl	%eax, %eax
	je	.L897
.L931:
	cmpl	$-3, %eax
	je	.L897
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L934:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L901
	movq	%r12, 40(%r13)
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L933:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L930:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L928:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L935:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L936:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L929:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L932:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L902
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10494:
	.size	_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD0Ev, .-_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD0Ev
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev, @function
_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev:
.LFB10492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 145(%rdi)
	movq	%rax, (%rdi)
	jne	.L962
	movl	$256, %eax
	cmpb	$0, 144(%rdi)
	movq	%rdi, %rbx
	movw	%ax, 146(%rdi)
	je	.L963
	movq	304(%rdi), %rdi
	movq	$0, 304(%rbx)
	testq	%rdi, %rdi
	je	.L940
	call	BrotliDecoderDestroyInstance@PLT
.L940:
	leaq	168(%rbx), %rax
	xorl	%r12d, %r12d
	movl	$0, 192(%rbx)
	xchgq	(%rax), %r12
	testq	%r12, %r12
	je	.L942
	movq	176(%rbx), %rax
	js	.L964
.L943:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L965
.L944:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L966
	cmpq	%r14, %rax
	jl	.L967
.L942:
	cmpq	$0, 176(%rbx)
	jne	.L968
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L969
	movq	304(%rbx), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE(%rip), %rax
	movq	%rax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L949
	call	BrotliDecoderDestroyInstance@PLT
.L949:
	movq	272(%rbx), %rdi
	leaq	288(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L950
	call	_ZdlPv@PLT
.L950:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L951
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L951:
	leaq	-56(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$368, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L967:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L966:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L942
	movq	%r12, 40(%r13)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L965:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L962:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L968:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L969:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L963:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L964:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L943
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10492:
	.size	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev, .-_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev, @function
_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev:
.LFB10493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 145(%rdi)
	movq	%rax, (%rdi)
	jne	.L994
	movl	$256, %eax
	cmpb	$0, 144(%rdi)
	movq	%rdi, %rbx
	movw	%ax, 146(%rdi)
	je	.L995
	movq	272(%rdi), %rdi
	movq	$0, 272(%rbx)
	testq	%rdi, %rdi
	je	.L973
	call	BrotliEncoderDestroyInstance@PLT
.L973:
	leaq	168(%rbx), %rax
	xorl	%r12d, %r12d
	movl	$0, 192(%rbx)
	xchgq	(%rax), %r12
	testq	%r12, %r12
	je	.L975
	movq	176(%rbx), %rax
	js	.L996
.L976:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L997
.L977:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L998
	cmpq	%r14, %rax
	jl	.L999
.L975:
	cmpq	$0, 176(%rbx)
	jne	.L1000
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L1001
	movq	272(%rbx), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE(%rip), %rax
	movq	%rax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L982
	call	BrotliEncoderDestroyInstance@PLT
.L982:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L983
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L983:
	leaq	-56(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$336, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L998:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L975
	movq	%r12, 40(%r13)
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L997:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L994:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1000:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1001:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L995:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L996:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L976
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10493:
	.size	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev, .-_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED1Ev, @function
_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED1Ev:
.LFB10490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 145(%rdi)
	movq	%rax, (%rdi)
	jne	.L1027
	movl	$256, %eax
	cmpb	$0, 144(%rdi)
	movq	%rdi, %rbx
	movw	%ax, 146(%rdi)
	je	.L1028
	movq	304(%rdi), %rdi
	movq	$0, 304(%rbx)
	testq	%rdi, %rdi
	je	.L1005
	call	BrotliDecoderDestroyInstance@PLT
.L1005:
	leaq	168(%rbx), %rax
	xorl	%r12d, %r12d
	movl	$0, 192(%rbx)
	xchgq	(%rax), %r12
	testq	%r12, %r12
	je	.L1007
	movq	176(%rbx), %rax
	js	.L1029
.L1008:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1030
.L1009:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1031
	cmpq	%r14, %rax
	jl	.L1032
.L1007:
	cmpq	$0, 176(%rbx)
	jne	.L1033
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L1034
	movq	304(%rbx), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE(%rip), %rax
	movq	%rax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L1014
	call	BrotliDecoderDestroyInstance@PLT
.L1014:
	movq	272(%rbx), %rdi
	leaq	288(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1016
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1016:
	leaq	-56(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1032:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1031:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L1007
	movq	%r12, 40(%r13)
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1027:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1033:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1034:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1008
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10490:
	.size	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED1Ev, .-_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED1Ev
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD1Ev, @function
_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD1Ev:
.LFB10489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 145(%rdi)
	movq	%rax, (%rdi)
	jne	.L1073
	movl	$256, %eax
	cmpb	$0, 144(%rdi)
	movq	%rdi, %rbx
	movw	%ax, 146(%rdi)
	je	.L1074
	movl	208(%rdi), %eax
	cmpl	$7, %eax
	jg	.L1075
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L1055
	cmpl	$5, %eax
	je	.L1055
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L1056
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L1042
.L1056:
	leaq	248(%rbx), %rdi
	call	inflateEnd@PLT
	testl	%eax, %eax
	jne	.L1076
	.p2align 4,,10
	.p2align 3
.L1042:
	movl	$0, 208(%rbx)
	movq	224(%rbx), %rax
	cmpq	232(%rbx), %rax
	je	.L1044
	movq	%rax, 232(%rbx)
.L1044:
	leaq	168(%rbx), %rax
	xorl	%r12d, %r12d
	xchgq	(%rax), %r12
	testq	%r12, %r12
	je	.L1046
	movq	176(%rbx), %rax
	js	.L1077
.L1047:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1078
.L1048:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1079
	cmpq	%r14, %rax
	jge	.L1046
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.p2align 4,,10
	.p2align 3
.L1046:
	cmpq	$0, 176(%rbx)
	jne	.L1080
	movq	168(%rbx), %rax
	testq	%rax, %rax
	jne	.L1081
	movq	224(%rbx), %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_111ZlibContextE(%rip), %rax
	movq	%rax, 184(%rbx)
	testq	%rdi, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1054
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1054:
	leaq	-56(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1055:
	.cfi_restore_state
	leaq	248(%rbx), %rdi
	call	deflateEnd@PLT
	testl	%eax, %eax
	je	.L1042
.L1076:
	cmpl	$-3, %eax
	je	.L1042
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1079:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L1046
	movq	%r12, 40(%r13)
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1075:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1073:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1080:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1081:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1074:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L1047
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10489:
	.size	_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD1Ev, .-_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD1Ev
	.section	.rodata.str1.1
.LC24:
	.string	"Compression failed"
.LC25:
	.string	"ERR_BROTLI_COMPRESSION_FAILED"
	.text
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi, @function
_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi:
.LFB10496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 145(%rdi)
	cmpl	$-125, %esi
	je	.L1151
	testl	%esi, %esi
	jne	.L1152
	movq	-40(%rdi), %r13
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%r13), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpb	$0, 264(%rbx)
	jne	.L1092
	leaq	-80(%rbp), %rsi
	leaq	-56(%rbx), %rdi
	movl	$-1, -64(%rbp)
	leaq	.LC25(%rip), %rax
	leaq	.LC24(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1090:
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.L1116
	subl	$1, %eax
	movl	%eax, 148(%rbx)
	je	.L1153
.L1118:
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1082
	movq	176(%rbx), %rax
	js	.L1154
.L1123:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %rbx
	addq	%r12, %rbx
	movq	%rbx, %rax
	subq	48(%r13), %rax
	movq	%rbx, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1155
.L1124:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1150
	cmpq	%rax, %rbx
	jg	.L1148
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1156
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1092:
	.cfi_restore_state
	movq	152(%rbx), %rax
	movq	224(%rbx), %rdx
	movq	216(%rbx), %rcx
	movq	160(%rbx), %rsi
	movl	%edx, (%rax)
	movl	%ecx, 4(%rax)
	testq	%rsi, %rsi
	je	.L1093
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1157
.L1093:
	leaq	-56(%rbx), %r13
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	cmpb	$0, 146(%rbx)
	jne	.L1158
.L1095:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.L1116
	subl	$1, %eax
	movl	%eax, 148(%rbx)
	je	.L1159
.L1105:
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1082
	movq	176(%rbx), %rax
	js	.L1160
.L1111:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %rbx
	addq	%r12, %rbx
	movq	%rbx, %rax
	subq	48(%r13), %rax
	movq	%rbx, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1161
.L1112:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1150
	cmpq	%rbx, %rax
	jge	.L1082
.L1148:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1150:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L1082
	movq	%r12, 40(%r13)
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1158:
	cmpb	$0, 145(%rbx)
	jne	.L1095
	movl	$256, %ecx
	cmpb	$0, 144(%rbx)
	movw	%cx, 146(%rbx)
	je	.L1097
	movq	272(%rbx), %rdi
	movq	$0, 272(%rbx)
	testq	%rdi, %rdi
	je	.L1098
	call	BrotliEncoderDestroyInstance@PLT
.L1098:
	movl	$0, 192(%rbx)
	xorl	%r15d, %r15d
	xchgq	168(%rbx), %r15
	testq	%r15, %r15
	je	.L1095
	movq	176(%rbx), %rax
	js	.L1162
.L1099:
	addq	%r15, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %rdi
	movq	32(%rdi), %rax
	addq	%r15, %rax
	movq	%rax, %rdx
	subq	48(%rdi), %rdx
	movq	%rax, 32(%rdi)
	cmpq	$33554432, %rdx
	jg	.L1163
.L1101:
	movq	40(%rdi), %rdx
	testq	%r15, %r15
	js	.L1164
	cmpq	%rax, %rdx
	jge	.L1095
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	(%rsi), %rsi
	movq	352(%r13), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1116:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	-32(%rbx), %rax
	testq	%rax, %rax
	je	.L1108
	movl	(%rax), %edx
	movb	$1, 8(%rax)
	testl	%edx, %edx
	jne	.L1105
.L1108:
	movq	-48(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	-32(%rbx), %rax
	leaq	-56(%rbx), %rsi
	testq	%rax, %rax
	je	.L1121
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1118
.L1121:
	movq	-48(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1152:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1151:
	movl	$256, %esi
	cmpb	$0, 144(%rdi)
	movw	%si, 146(%rdi)
	je	.L1097
	movq	272(%rdi), %rdi
	movq	$0, 272(%rbx)
	testq	%rdi, %rdi
	je	.L1085
	call	BrotliEncoderDestroyInstance@PLT
.L1085:
	movl	$0, 192(%rbx)
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1090
	movq	176(%rbx), %rax
	js	.L1165
.L1087:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1166
.L1088:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1167
	cmpq	%r14, %rax
	jge	.L1090
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1164:
	addq	%rdx, %r15
	cmpq	$67108864, %r15
	jle	.L1095
	movq	%r15, 40(%rdi)
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	%rax, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdi
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1123
.L1100:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L1111
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1097:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1167:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L1090
	movq	%r12, 40(%r13)
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1088
.L1162:
	movq	%r15, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1099
	jmp	.L1100
.L1165:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L1087
	jmp	.L1100
.L1156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10496:
	.size	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi, .-_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi, @function
_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi:
.LFB10495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 145(%rdi)
	cmpl	$-125, %esi
	je	.L1275
	testl	%esi, %esi
	jne	.L1276
	movq	-40(%rdi), %r15
	leaq	-112(%rbp), %r12
	leaq	-56(%rbx), %r13
	movq	%r12, %rdi
	movq	352(%r15), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r15), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	leaq	-80(%rbp), %r8
	leaq	184(%rbx), %rsi
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK4node12_GLOBAL__N_111ZlibContext12GetErrorInfoEv
	cmpq	$0, -72(%rbp)
	movq	-120(%rbp), %r8
	je	.L1277
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1182:
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.L1217
	subl	$1, %eax
	movl	%eax, 148(%rbx)
	jne	.L1219
	movq	-32(%rbx), %rax
	leaq	-56(%rbx), %rsi
	testq	%rax, %rax
	je	.L1222
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1219
.L1222:
	movq	-48(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	256(%rbx), %edx
	movq	152(%rbx), %rax
	movq	160(%rbx), %rsi
	movl	%edx, 4(%rax)
	movl	280(%rbx), %edx
	movl	%edx, (%rax)
	testq	%rsi, %rsi
	je	.L1185
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1278
.L1185:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	cmpb	$0, 146(%rbx)
	jne	.L1279
.L1188:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.L1217
	subl	$1, %eax
	movl	%eax, 148(%rbx)
	je	.L1280
.L1219:
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1168
	movq	176(%rbx), %rax
	js	.L1281
.L1224:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %rbx
	addq	%r12, %rbx
	movq	%rbx, %rax
	subq	48(%r13), %rax
	movq	%rbx, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1282
.L1225:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1283
	cmpq	%rbx, %rax
	jl	.L1284
.L1168:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1285
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1279:
	.cfi_restore_state
	cmpb	$0, 145(%rbx)
	jne	.L1188
	movl	$256, %ecx
	cmpb	$0, 144(%rbx)
	movw	%cx, 146(%rbx)
	je	.L1190
	movl	208(%rbx), %eax
	cmpl	$7, %eax
	jg	.L1191
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L1231
	cmpl	$5, %eax
	je	.L1231
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L1232
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L1195
.L1232:
	leaq	248(%rbx), %rdi
	call	inflateEnd@PLT
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	(%rsi), %rsi
	movq	352(%r15), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1217:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1231:
	leaq	248(%rbx), %rdi
	call	deflateEnd@PLT
.L1194:
	testl	%eax, %eax
	je	.L1195
	cmpl	$-3, %eax
	je	.L1195
.L1197:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1195:
	movl	$0, 208(%rbx)
	movq	224(%rbx), %rax
	cmpq	232(%rbx), %rax
	je	.L1199
	movq	%rax, 232(%rbx)
.L1199:
	xorl	%r15d, %r15d
	xchgq	168(%rbx), %r15
	testq	%r15, %r15
	je	.L1188
	movq	176(%rbx), %rax
	js	.L1286
.L1200:
	addq	%r15, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %rdi
	movq	32(%rdi), %rax
	addq	%r15, %rax
	movq	%rax, %rdx
	subq	48(%rdi), %rdx
	movq	%rax, 32(%rdi)
	cmpq	$33554432, %rdx
	jg	.L1287
.L1202:
	movq	40(%rdi), %rdx
	testq	%r15, %r15
	js	.L1288
	cmpq	%rdx, %rax
	jle	.L1188
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1283:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L1168
	movq	%r12, 40(%r13)
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	-32(%rbx), %rax
	testq	%rax, %rax
	je	.L1209
	movl	(%rax), %edx
	movb	$1, 8(%rax)
	testl	%edx, %edx
	jne	.L1219
.L1209:
	movq	-48(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1276:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1275:
	movl	$256, %esi
	cmpb	$0, 144(%rdi)
	movw	%si, 146(%rdi)
	je	.L1190
	movl	208(%rdi), %eax
	cmpl	$7, %eax
	jg	.L1191
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L1229
	cmpl	$5, %eax
	je	.L1229
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L1230
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L1175
.L1230:
	leaq	248(%rbx), %rdi
	call	inflateEnd@PLT
.L1174:
	testl	%eax, %eax
	je	.L1175
	cmpl	$-3, %eax
	jne	.L1197
.L1175:
	movl	$0, 208(%rbx)
	movq	224(%rbx), %rax
	cmpq	232(%rbx), %rax
	je	.L1177
	movq	%rax, 232(%rbx)
.L1177:
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1182
	movq	176(%rbx), %rax
	js	.L1289
.L1179:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1290
.L1180:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1291
	cmpq	%rax, %r14
	jle	.L1182
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1288:
	addq	%rdx, %r15
	cmpq	$67108864, %r15
	jle	.L1188
	movq	%r15, 40(%rdi)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	%rax, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdi
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1229:
	leaq	248(%rbx), %rdi
	call	deflateEnd@PLT
	jmp	.L1174
.L1286:
	movq	%r15, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1200
.L1201:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rdx, %rax
	jnb	.L1224
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1190:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1191:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1291:
	addq	%r12, %rax
	cmpq	$67108864, %rax
	jle	.L1182
	movq	%rax, 40(%r13)
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1180
.L1289:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1179
	jmp	.L1201
.L1285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10495:
	.size	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi, .-_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi
	.section	.rodata.str1.1
.LC26:
	.string	"Decompression failed"
	.text
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi, @function
_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi:
.LFB10497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 145(%rdi)
	cmpl	$-125, %esi
	je	.L1368
	testl	%esi, %esi
	jne	.L1369
	movq	-40(%rdi), %r13
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%r13), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movl	268(%rbx), %eax
	testl	%eax, %eax
	je	.L1302
	movq	272(%rbx), %rdx
	leaq	.LC26(%rip), %rcx
	movl	%eax, -64(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L1303
.L1304:
	leaq	-80(%rbp), %rsi
	leaq	-56(%rbx), %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1300:
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.L1329
	subl	$1, %eax
	movl	%eax, 148(%rbx)
	je	.L1370
.L1331:
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1292
	movq	176(%rbx), %rax
	js	.L1371
.L1336:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %rbx
	addq	%r12, %rbx
	movq	%rbx, %rax
	subq	48(%r13), %rax
	movq	%rbx, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1372
.L1337:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1367
	cmpq	%rbx, %rax
	jl	.L1365
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1373
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore_state
	cmpl	$2, 232(%rbx)
	jne	.L1303
	cmpl	$2, 264(%rbx)
	je	.L1374
.L1303:
	movq	152(%rbx), %rax
	movq	224(%rbx), %rdx
	movq	216(%rbx), %rcx
	movq	160(%rbx), %rsi
	movl	%edx, (%rax)
	movl	%ecx, 4(%rax)
	testq	%rsi, %rsi
	je	.L1305
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1375
.L1305:
	leaq	-56(%rbx), %r13
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	cmpb	$0, 146(%rbx)
	jne	.L1376
.L1308:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.L1329
	subl	$1, %eax
	movl	%eax, 148(%rbx)
	je	.L1377
.L1318:
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1292
	movq	176(%rbx), %rax
	js	.L1378
.L1324:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %rbx
	addq	%r12, %rbx
	movq	%rbx, %rax
	subq	48(%r13), %rax
	movq	%rbx, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1379
.L1325:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1367
	cmpq	%rax, %rbx
	jle	.L1292
.L1365:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1367:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L1292
	movq	%r12, 40(%r13)
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1376:
	cmpb	$0, 145(%rbx)
	jne	.L1308
	movl	$256, %ecx
	cmpb	$0, 144(%rbx)
	movw	%cx, 146(%rbx)
	je	.L1310
	movq	304(%rbx), %rdi
	movq	$0, 304(%rbx)
	testq	%rdi, %rdi
	je	.L1311
	call	BrotliDecoderDestroyInstance@PLT
.L1311:
	movl	$0, 192(%rbx)
	xorl	%r15d, %r15d
	xchgq	168(%rbx), %r15
	testq	%r15, %r15
	je	.L1308
	movq	176(%rbx), %rax
	js	.L1380
.L1312:
	addq	%r15, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %rdi
	movq	32(%rdi), %rax
	addq	%r15, %rax
	movq	%rax, %rdx
	subq	48(%rdi), %rdx
	movq	%rax, 32(%rdi)
	cmpq	$33554432, %rdx
	jg	.L1381
.L1314:
	movq	40(%rdi), %rdx
	testq	%r15, %r15
	js	.L1382
	cmpq	%rdx, %rax
	jle	.L1308
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	(%rsi), %rsi
	movq	352(%r13), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1329:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	-32(%rbx), %rax
	testq	%rax, %rax
	je	.L1321
	movl	(%rax), %edx
	movb	$1, 8(%rax)
	testl	%edx, %edx
	jne	.L1318
.L1321:
	movq	-48(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	-32(%rbx), %rax
	leaq	-56(%rbx), %rsi
	testq	%rax, %rax
	je	.L1334
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1331
.L1334:
	movq	-48(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1369:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1368:
	movl	$256, %esi
	cmpb	$0, 144(%rdi)
	movw	%si, 146(%rdi)
	je	.L1310
	movq	304(%rdi), %rdi
	movq	$0, 304(%rbx)
	testq	%rdi, %rdi
	je	.L1295
	call	BrotliDecoderDestroyInstance@PLT
.L1295:
	movl	$0, 192(%rbx)
	xorl	%r12d, %r12d
	xchgq	168(%rbx), %r12
	testq	%r12, %r12
	je	.L1300
	movq	176(%rbx), %rax
	js	.L1383
.L1297:
	addq	%r12, %rax
	movq	%rax, 176(%rbx)
	movq	-40(%rbx), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%r12, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L1384
.L1298:
	movq	40(%r13), %rax
	testq	%r12, %r12
	js	.L1385
	cmpq	%rax, %r14
	jle	.L1300
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1374:
	leaq	.LC8(%rip), %rax
	leaq	.LC23(%rip), %rcx
	movl	$-5, -64(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1382:
	addq	%rdx, %r15
	cmpq	$67108864, %r15
	jle	.L1308
	movq	%r15, 40(%rdi)
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	%rax, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rdi
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1336
.L1313:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1324
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1310:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1385:
	addq	%rax, %r12
	cmpq	$67108864, %r12
	jle	.L1300
	movq	%r12, 40(%r13)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L1298
.L1380:
	movq	%r15, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1312
	jmp	.L1313
.L1383:
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	jbe	.L1297
	jmp	.L1313
.L1373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10497:
	.size	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi, .-_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi
	.section	.rodata.str1.1
.LC27:
	.string	"Failed to set parameters"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	jne	.L1431
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1432
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1414
	cmpw	$1040, %cx
	jne	.L1389
.L1414:
	movq	23(%rdx), %r13
.L1391:
	testq	%r13, %r13
	je	.L1386
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r14
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1433
	movq	8(%rbx), %rdi
.L1394:
	movq	%r14, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1386
	sarq	$32, %rax
	cmpl	$1, 16(%rbx)
	movq	%rax, %r12
	jg	.L1396
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1397:
	movq	%r14, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testb	%al, %al
	je	.L1386
	movl	264(%r13), %eax
	sarq	$32, %rdx
	movl	$0, 248(%r13)
	andl	$-5, %eax
	cmpl	$1, %eax
	je	.L1434
.L1399:
	movl	$0, -48(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
.L1404:
	movq	%r13, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
.L1386:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1435
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1433:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1389:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	8(%rbx), %rdi
	subq	$8, %rdi
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1431:
	leaq	_ZZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1432:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1434:
	leaq	304(%r13), %rdi
	movl	%r12d, %esi
	call	deflateParams@PLT
	movl	%eax, 248(%r13)
	testl	%eax, %eax
	je	.L1399
	cmpl	$-5, %eax
	je	.L1399
	movq	352(%r13), %rdx
	leaq	.LC27(%rip), %rcx
	testq	%rdx, %rdx
	cmove	%rcx, %rdx
	leaq	.LC5(%rip), %rcx
	cmpl	$1, %eax
	je	.L1402
	leaq	.LC4(%rip), %rcx
	cmpl	$2, %eax
	je	.L1402
	leaq	.LC3(%rip), %rcx
	cmpl	$-1, %eax
	je	.L1402
	leaq	.LC2(%rip), %rcx
	cmpl	$-2, %eax
	je	.L1402
	leaq	.LC9(%rip), %rcx
	cmpl	$-3, %eax
	je	.L1402
	leaq	.LC1(%rip), %rcx
	cmpl	$-4, %eax
	je	.L1402
	cmpl	$-6, %eax
	leaq	.LC7(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	cmove	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	%r13, %rdi
	movl	%eax, -48(%rbp)
	punpcklqdq	%xmm1, %xmm0
	leaq	-64(%rbp), %rsi
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE
	jmp	.L1404
.L1435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7659:
	.size	_ZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE,"axG",@progbits,_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	.type	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE, @function
_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE:
.LFB6486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	352(%rdi), %rdi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	xorl	%r9d, %r9d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movq	%r12, %rsi
	movl	$1, %edx
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1439
.L1437:
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	leaq	-32(%rbp), %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1439:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1437
	.cfi_endproc
.LFE6486:
	.size	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE, .-_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	.section	.rodata.str1.1
.LC28:
	.string	"write"
.LC29:
	.string	"writeSync"
.LC30:
	.string	"close"
.LC31:
	.string	"init"
.LC32:
	.string	"params"
.LC33:
	.string	"reset"
.LC34:
	.string	"Zlib"
.LC35:
	.string	"BrotliEncoder"
.LC36:
	.string	"BrotliDecoder"
.LC37:
	.string	"1.2.11"
.LC38:
	.string	"ZLIB_VERSION"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB39:
	.text
.LHOTB39:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L1441
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L1441
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L1441
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1471
.L1442:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC29(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1472
.L1443:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC30(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L1473
.L1444:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC31(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r14
	popq	%rax
	testq	%r14, %r14
	je	.L1474
.L1445:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1475
.L1446:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1476
.L1447:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC34(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1477
.L1448:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1478
.L1449:
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1479
.L1450:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC28(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1480
.L1451:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC29(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC30(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L1481
.L1452:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC31(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r14
	popq	%rax
	testq	%r14, %r14
	je	.L1482
.L1453:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1483
.L1454:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1484
.L1455:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1485
.L1456:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1486
.L1457:
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1487
.L1458:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC28(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1488
.L1459:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC29(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC30(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC31(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE6ParamsERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L1489
.L1460:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5ResetERKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC33(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC36(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1490
.L1461:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1491
.L1462:
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1492
.L1463:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC37(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1493
.L1464:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1494
.L1465:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1495
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1471:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1472:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1473:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1474:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1475:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1476:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1477:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1479:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1480:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1481:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1482:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1483:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1484:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1485:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1487:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1488:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1489:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1490:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1492:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1493:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1495:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7700:
.L1441:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7700:
	.text
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE39:
	.text
.LHOTE39:
	.section	.text._ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE:
.LFB7143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1504
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1501
	cmpw	$1040, %cx
	jne	.L1498
.L1501:
	movq	23(%rdx), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	.p2align 4,,10
	.p2align 3
.L1504:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7143:
	.size	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	.section	.rodata.str1.1
.LC40:
	.string	"Setting parameter failed"
.LC41:
	.string	"ERR_BROTLI_PARAM_SET_FAILED"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	testq	%rax, %rax
	je	.L1505
	cmpl	$3, 16(%r13)
	jne	.L1557
	movq	%rax, %r12
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value13IsUint32ArrayEv@PLT
	testb	%al, %al
	je	.L1558
	cmpl	$1, 16(%r13)
	jle	.L1559
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpl	$2, 16(%r13)
	movq	%rax, %rbx
	jg	.L1511
.L1566:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1512:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L1560
	movq	16(%r12), %rax
	cmpl	$2, 16(%r13)
	movq	216(%r12), %rdi
	movq	352(%rax), %r15
	jg	.L1514
	movq	0(%r13), %rax
	movq	8(%rax), %r14
	movq	%rbx, 208(%r12)
	addq	$88, %r14
	testq	%rdi, %rdi
	je	.L1515
.L1516:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 216(%r12)
.L1515:
	testq	%r14, %r14
	je	.L1518
.L1517:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 216(%r12)
.L1518:
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE14AllocForBrotliEPvm(%rip), %rdi
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE11FreeForZlibEPvS4_(%rip), %rsi
	movq	%r12, %rdx
	movb	$1, 200(%r12)
	movq	%rdi, %xmm0
	movq	%rsi, %xmm3
	movq	%r12, 312(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 296(%r12)
	call	BrotliEncoderCreateInstance@PLT
	movq	328(%r12), %rdi
	movq	%rax, 328(%r12)
	testq	%rdi, %rdi
	je	.L1519
	call	BrotliEncoderDestroyInstance@PLT
	movq	328(%r12), %rax
.L1519:
	testq	%rax, %rax
	je	.L1561
	movl	16(%r13), %ecx
	pxor	%xmm0, %xmm0
	movl	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%ecx, %ecx
	jg	.L1522
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1523:
	call	_ZNK2v85Value13IsUint32ArrayEv@PLT
	testb	%al, %al
	je	.L1562
	movl	16(%r13), %edx
	testl	%edx, %edx
	jle	.L1563
	movq	8(%r13), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r15
	movl	16(%r13), %eax
	testl	%eax, %eax
	jg	.L1527
.L1567:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1528:
	call	_ZN2v810TypedArray6LengthEv@PLT
	xorl	%ebx, %ebx
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L1529
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1564:
	movl	$0, -64(%rbp)
	pxor	%xmm1, %xmm1
	movaps	%xmm1, -80(%rbp)
.L1531:
	addq	$1, %rbx
	cmpq	%r14, %rbx
	je	.L1533
.L1529:
	movl	(%r15,%rbx,4), %edx
	cmpl	$-1, %edx
	je	.L1531
	movq	328(%r12), %rdi
	movl	%ebx, %esi
	call	BrotliEncoderSetParameter@PLT
	testl	%eax, %eax
	jne	.L1564
	leaq	.LC41(%rip), %rax
	leaq	.LC40(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1561:
	leaq	.LC19(%rip), %rax
	leaq	.LC18(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	.p2align 4,,10
	.p2align 3
.L1556:
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$-1, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 24(%rax)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
.L1505:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1565
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1559:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpl	$2, 16(%r13)
	movq	%rax, %rbx
	jle	.L1566
.L1511:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	8(%r13), %rax
	movq	%rbx, 208(%r12)
	leaq	-16(%rax), %r14
	testq	%rdi, %rdi
	jne	.L1516
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r15
	movl	16(%r13), %eax
	testl	%eax, %eax
	jle	.L1567
.L1527:
	movq	8(%r13), %rdi
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	112(%rdx), %rdx
	movq	%rdx, 24(%rax)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1522:
	movq	8(%r13), %rdi
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1557:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1558:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1560:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1562:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8966:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	testq	%rax, %rax
	je	.L1568
	cmpl	$3, 16(%r13)
	jne	.L1620
	movq	%rax, %r12
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value13IsUint32ArrayEv@PLT
	testb	%al, %al
	je	.L1621
	cmpl	$1, 16(%r13)
	jle	.L1622
	movq	8(%r13), %rax
	leaq	-8(%rax), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpl	$2, 16(%r13)
	movq	%rax, %rbx
	jg	.L1574
.L1629:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1575:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L1623
	movq	16(%r12), %rax
	cmpl	$2, 16(%r13)
	movq	216(%r12), %rdi
	movq	352(%rax), %r15
	jg	.L1577
	movq	0(%r13), %rax
	movq	8(%rax), %r14
	movq	%rbx, 208(%r12)
	addq	$88, %r14
	testq	%rdi, %rdi
	je	.L1578
.L1579:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 216(%r12)
.L1578:
	testq	%r14, %r14
	je	.L1581
.L1580:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 216(%r12)
.L1581:
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE14AllocForBrotliEPvm(%rip), %rdi
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE11FreeForZlibEPvS4_(%rip), %rsi
	movq	%r12, %rdx
	movb	$1, 200(%r12)
	movq	%rdi, %xmm0
	movq	%rsi, %xmm3
	movq	%r12, 312(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 296(%r12)
	call	BrotliDecoderCreateInstance@PLT
	movq	360(%r12), %rdi
	movq	%rax, 360(%r12)
	testq	%rdi, %rdi
	je	.L1582
	call	BrotliDecoderDestroyInstance@PLT
	movq	360(%r12), %rax
.L1582:
	testq	%rax, %rax
	je	.L1624
	movl	16(%r13), %ecx
	pxor	%xmm0, %xmm0
	movl	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%ecx, %ecx
	jg	.L1585
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1586:
	call	_ZNK2v85Value13IsUint32ArrayEv@PLT
	testb	%al, %al
	je	.L1625
	movl	16(%r13), %edx
	testl	%edx, %edx
	jle	.L1626
	movq	8(%r13), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r15
	movl	16(%r13), %eax
	testl	%eax, %eax
	jg	.L1590
.L1630:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1591:
	call	_ZN2v810TypedArray6LengthEv@PLT
	xorl	%ebx, %ebx
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L1592
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1627:
	movl	$0, -64(%rbp)
	pxor	%xmm1, %xmm1
	movaps	%xmm1, -80(%rbp)
.L1594:
	addq	$1, %rbx
	cmpq	%r14, %rbx
	je	.L1596
.L1592:
	movl	(%r15,%rbx,4), %edx
	cmpl	$-1, %edx
	je	.L1594
	movq	360(%r12), %rdi
	movl	%ebx, %esi
	call	BrotliDecoderSetParameter@PLT
	testl	%eax, %eax
	jne	.L1627
	leaq	.LC41(%rip), %rax
	leaq	.LC40(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1624:
	leaq	.LC19(%rip), %rax
	leaq	.LC18(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$-1, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 24(%rax)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
.L1568:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1628
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1622:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpl	$2, 16(%r13)
	movq	%rax, %rbx
	jle	.L1629
.L1574:
	movq	8(%r13), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	8(%r13), %rax
	movq	%rbx, 208(%r12)
	leaq	-16(%rax), %r14
	testq	%rdi, %rdi
	jne	.L1579
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	0(%r13), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r15
	movl	16(%r13), %eax
	testl	%eax, %eax
	jle	.L1630
.L1590:
	movq	8(%r13), %rdi
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rdx
	movq	112(%rdx), %rdx
	movq	%rdx, 24(%rax)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	8(%r13), %rdi
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1620:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1621:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1623:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1625:
	leaq	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1628:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8973:
	.size	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC42:
	.ascii	"WARNING: You are likely using"
	.string	" a version of node-tar or npm that is incompatible with this version of Node.js.\nPlease use either the version of npm that is bundled with Node.js, or a version of npm (> 5.5.1 or < 5.4.0) or node-tar (> 4.0.1) that is compatible with Node.js 9 and above.\n"
	.align 8
.LC43:
	.string	"cannot create std::vector larger than max_size()"
	.section	.rodata.str1.1
.LC44:
	.string	"zlib error"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	cmpl	$5, %eax
	je	.L1710
	cmpl	$7, %eax
	jne	.L1711
.L1633:
	movq	(%r14), %rdi
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1631
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	16(%r14), %ecx
	movq	%rax, %r15
	testl	%ecx, %ecx
	jle	.L1712
	movq	8(%r14), %rdi
.L1636:
	movq	%r15, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L1631
	shrq	$32, %rbx
	cmpl	$1, 16(%r14)
	jg	.L1638
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1639:
	movq	%r15, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L1631
	sarq	$32, %r13
	cmpl	$2, 16(%r14)
	jle	.L1713
	movq	8(%r14), %rax
	leaq	-16(%rax), %rdi
.L1642:
	movq	%r15, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1631
	shrq	$32, %rax
	cmpl	$3, 16(%r14)
	movq	%rax, -120(%rbp)
	jg	.L1644
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1645:
	movq	%r15, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1631
	shrq	$32, %rax
	cmpl	$4, 16(%r14)
	movq	%rax, -128(%rbp)
	jg	.L1647
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1648:
	call	_ZNK2v85Value13IsUint32ArrayEv@PLT
	testb	%al, %al
	je	.L1714
	cmpl	$4, 16(%r14)
	jg	.L1650
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1651:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %r15
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	cmpl	$5, 16(%r14)
	movq	%rax, -136(%rbp)
	jg	.L1652
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1653:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L1715
	movl	16(%r14), %eax
	cmpl	$5, %eax
	jle	.L1716
	movq	8(%r14), %rdi
	leaq	-40(%rdi), %rcx
	movq	%rcx, -144(%rbp)
	cmpl	$6, %eax
	je	.L1717
	subq	$48, %rdi
.L1658:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L1691
	cmpl	$6, 16(%r14)
	jg	.L1660
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1661:
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpl	$6, 16(%r14)
	movq	%rax, -168(%rbp)
	jg	.L1662
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1663:
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	js	.L1718
	je	.L1692
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_Znwm@PLT
	movq	-152(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	leaq	(%rax,%rdx), %rax
	movq	%rax, -152(%rbp)
	call	memcpy@PLT
.L1659:
	movq	-136(%rbp), %rax
	movq	216(%r12), %rdi
	movq	%rax, 208(%r12)
	movq	16(%r12), %rax
	movq	352(%rax), %r8
	testq	%rdi, %rdi
	je	.L1666
	movq	%r8, -136(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-136(%rbp), %r8
	movq	$0, 216(%r12)
.L1666:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L1667
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 216(%r12)
.L1667:
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE11FreeForZlibEPvS4_(%rip), %rax
	leaq	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE12AllocForZlibEPvjj(%rip), %rcx
	movb	$1, 200(%r12)
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	movq	%r12, 384(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 368(%r12)
	testl	%ebx, %ebx
	jne	.L1668
	movl	264(%r12), %eax
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L1671
	cmpl	$7, %eax
	je	.L1671
.L1669:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1668:
	leal	-8(%rbx), %eax
	cmpl	$7, %eax
	ja	.L1669
.L1671:
	leal	1(%r13), %eax
	cmpl	$10, %eax
	ja	.L1719
	movl	-120(%rbp), %eax
	subl	$1, %eax
	cmpl	$8, %eax
	ja	.L1720
	cmpl	$4, -128(%rbp)
	ja	.L1721
	movl	-120(%rbp), %eax
	movl	%r13d, 256(%r12)
	movl	%ebx, 272(%r12)
	movl	%eax, 260(%r12)
	movl	-128(%rbp), %eax
	movq	$0, 248(%r12)
	movl	%eax, 268(%r12)
	movl	264(%r12), %eax
	leal	-3(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1722
	cmpl	$7, %eax
	jne	.L1677
	addl	$32, %ebx
	movl	%ebx, 272(%r12)
.L1678:
	movl	272(%r12), %esi
	leaq	304(%r12), %rdi
	movl	$112, %ecx
	leaq	.LC37(%rip), %rdx
	call	inflateInit2_@PLT
	movl	%eax, 248(%r12)
	movl	%eax, %esi
.L1682:
	movq	-152(%rbp), %rax
	movq	280(%r12), %rdi
	movq	-160(%rbp), %xmm0
	movq	%rax, %xmm2
	movq	%rax, 296(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 280(%r12)
	testq	%rdi, %rdi
	je	.L1683
	call	_ZdlPv@PLT
	movl	248(%r12), %esi
.L1683:
	testl	%esi, %esi
	je	.L1684
	movq	280(%r12), %rax
	cmpq	288(%r12), %rax
	je	.L1685
	movq	%rax, 288(%r12)
.L1685:
	movl	$0, 264(%r12)
	movq	352(%r12), %rdx
	leaq	.LC44(%rip), %rcx
	movq	%r15, %rdi
	call	_ZNK4node12_GLOBAL__N_111ZlibContext15ErrorForMessageEPKc.isra.0
.L1686:
	cmpq	$0, -104(%rbp)
	je	.L1687
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE
	cmpq	$1, -104(%rbp)
	movq	(%r14), %rdx
	sbbq	%rax, %rax
	andq	$-8, %rax
	addq	$64, %rax
.L1688:
	movq	8(%rdx), %rcx
	movq	%r12, %rdi
	movq	56(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L1631:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1723
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1712:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	8(%r14), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	stderr(%rip), %rcx
	movl	$285, %edx
	movl	$1, %esi
	leaq	.LC42(%rip), %rdi
	call	fwrite@PLT
	movl	16(%r14), %eax
	cmpl	$7, %eax
	je	.L1633
.L1711:
	leaq	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	8(%r14), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	8(%r14), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	8(%r14), %rax
	leaq	-40(%rax), %rdi
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1650:
	movq	8(%r14), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	(%r14), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -144(%rbp)
	movq	%rax, %rdi
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1691:
	movq	$0, -152(%rbp)
	movq	$0, -160(%rbp)
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	(%r14), %rdx
	movl	$56, %eax
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1677:
	leal	-5(%rax), %edx
	cmpl	$1, %edx
	ja	.L1689
	negl	%ebx
	movl	%ebx, 272(%r12)
.L1689:
	cmpl	$7, %eax
	ja	.L1679
	leaq	.L1680(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1680:
	.long	.L1679-.L1680
	.long	.L1681-.L1680
	.long	.L1678-.L1680
	.long	.L1681-.L1680
	.long	.L1678-.L1680
	.long	.L1681-.L1680
	.long	.L1678-.L1680
	.long	.L1678-.L1680
	.text
	.p2align 4,,10
	.p2align 3
.L1681:
	movl	-128(%rbp), %r9d
	movl	-120(%rbp), %r8d
	movl	$8, %edx
	movl	%r13d, %esi
	leaq	.LC37(%rip), %rax
	movl	272(%r12), %ecx
	pushq	$112
	leaq	304(%r12), %rdi
	pushq	%rax
	call	deflateInit2_@PLT
	movl	%eax, 248(%r12)
	movl	%eax, %esi
	popq	%rax
	popq	%rdx
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1722:
	addl	$16, %ebx
	movl	%ebx, 272(%r12)
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	8(%r14), %rax
	leaq	-48(%rax), %rdi
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	8(%r14), %rax
	leaq	-48(%rax), %rdi
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1684:
	leaq	240(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN4node12_GLOBAL__N_111ZlibContext13SetDictionaryEv
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1692:
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1714:
	leaq	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1715:
	leaq	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1719:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1720:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1721:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1723:
	call	__stack_chk_fail@PLT
.L1718:
	leaq	.LC43(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1717:
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1658
.L1679:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7645:
	.size	_ZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node10BaseObject8MakeWeakEv,"axG",@progbits,_ZN4node10BaseObject8MakeWeakEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject8MakeWeakEv
	.type	_ZN4node10BaseObject8MakeWeakEv, @function
_ZN4node10BaseObject8MakeWeakEv:
.LFB7145:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rdi, %rsi
	testq	%rax, %rax
	je	.L1726
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1727
.L1726:
	movq	8(%rsi), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	jmp	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1727:
	ret
	.cfi_endproc
.LFE7145:
	.size	_ZN4node10BaseObject8MakeWeakEv, .-_ZN4node10BaseObject8MakeWeakEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi:
.LFB10266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, 201(%rdi)
	cmpl	$-125, %esi
	je	.L1762
	testl	%esi, %esi
	jne	.L1763
	movq	16(%rdi), %rbx
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpb	$0, 320(%r12)
	jne	.L1734
	leaq	.LC25(%rip), %rax
	leaq	.LC24(%rip), %rdx
	movq	%r12, %rdi
	movl	$-1, -48(%rbp)
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	leaq	-64(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1761:
	movl	204(%r12), %eax
	testl	%eax, %eax
	je	.L1764
	subl	$1, %eax
	movl	%eax, 204(%r12)
	je	.L1765
.L1743:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1766
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1734:
	.cfi_restore_state
	movq	208(%r12), %rax
	movq	280(%r12), %rdx
	movq	272(%r12), %rcx
	movq	216(%r12), %rsi
	movl	%edx, (%rax)
	movl	%ecx, 4(%rax)
	testq	%rsi, %rsi
	je	.L1735
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1767
.L1735:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	cmpb	$0, 202(%r12)
	jne	.L1768
.L1736:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1765:
	movq	%r12, %rdi
	call	_ZN4node10BaseObject8MakeWeakEv
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1768:
	cmpb	$0, 201(%r12)
	jne	.L1736
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L1737
	movq	328(%r12), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L1738
	call	BrotliEncoderDestroyInstance@PLT
.L1738:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	(%rsi), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1764:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1763:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1762:
	movl	$256, %edx
	cmpb	$0, 200(%rdi)
	movw	%dx, 202(%rdi)
	je	.L1737
	movq	328(%rdi), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L1731
	call	BrotliEncoderDestroyInstance@PLT
.L1731:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1737:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1766:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10266:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi:
.LFB10269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, 201(%rdi)
	cmpl	$-125, %esi
	je	.L1832
	testl	%esi, %esi
	jne	.L1833
	movq	16(%rdi), %rbx
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r15
	movq	%r13, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	leaq	240(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNK4node12_GLOBAL__N_111ZlibContext12GetErrorInfoEv
	cmpq	$0, -72(%rbp)
	je	.L1834
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1831:
	movl	204(%r12), %eax
	testl	%eax, %eax
	je	.L1835
	subl	$1, %eax
	movl	%eax, 204(%r12)
	je	.L1836
.L1799:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1837
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1834:
	.cfi_restore_state
	movl	312(%r12), %edx
	movq	208(%r12), %rax
	movq	216(%r12), %rsi
	movl	%edx, 4(%rax)
	movl	336(%r12), %edx
	movl	%edx, (%rax)
	testq	%rsi, %rsi
	je	.L1782
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1838
.L1782:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	cmpb	$0, 202(%r12)
	jne	.L1839
.L1784:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	%r12, %rdi
	call	_ZN4node10BaseObject8MakeWeakEv
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1839:
	cmpb	$0, 201(%r12)
	jne	.L1784
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L1785
	movl	264(%r12), %eax
	cmpl	$7, %eax
	jg	.L1786
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L1803
	cmpl	$5, %eax
	je	.L1803
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L1804
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L1790
.L1804:
	leaq	304(%r12), %rdi
	call	inflateEnd@PLT
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	(%rsi), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1803:
	leaq	304(%r12), %rdi
	call	deflateEnd@PLT
.L1789:
	testl	%eax, %eax
	je	.L1790
	cmpl	$-3, %eax
	je	.L1790
.L1792:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1790:
	movl	$0, 264(%r12)
	movq	280(%r12), %rax
	cmpq	288(%r12), %rax
	je	.L1794
	movq	%rax, 288(%r12)
.L1794:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1833:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1835:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1832:
	movl	$256, %edx
	cmpb	$0, 200(%rdi)
	movw	%dx, 202(%rdi)
	je	.L1785
	movl	264(%rdi), %eax
	cmpl	$7, %eax
	jg	.L1786
	movl	%eax, %edx
	andl	$-3, %edx
	cmpl	$1, %edx
	je	.L1801
	cmpl	$5, %eax
	je	.L1801
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L1802
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L1776
.L1802:
	leaq	304(%r12), %rdi
	call	inflateEnd@PLT
.L1775:
	testl	%eax, %eax
	je	.L1776
	cmpl	$-3, %eax
	jne	.L1792
.L1776:
	movl	$0, 264(%r12)
	movq	280(%r12), %rax
	cmpq	288(%r12), %rax
	je	.L1778
	movq	%rax, 288(%r12)
.L1778:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1801:
	leaq	304(%r12), %rdi
	call	deflateEnd@PLT
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1785:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1786:
	leaq	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1837:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10269:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi:
.LFB10261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$0, 201(%rdi)
	cmpl	$-125, %esi
	je	.L1881
	testl	%esi, %esi
	jne	.L1882
	movq	16(%rdi), %rbx
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movl	324(%r12), %eax
	testl	%eax, %eax
	je	.L1846
	movq	328(%r12), %rdx
	leaq	.LC26(%rip), %rcx
	movl	%eax, -48(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1847
.L1848:
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1880:
	movl	204(%r12), %eax
	testl	%eax, %eax
	je	.L1883
	subl	$1, %eax
	movl	%eax, 204(%r12)
	je	.L1884
.L1858:
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1885
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1846:
	.cfi_restore_state
	cmpl	$2, 288(%r12)
	jne	.L1847
	cmpl	$2, 320(%r12)
	je	.L1886
.L1847:
	movq	208(%r12), %rax
	movq	280(%r12), %rdx
	movq	272(%r12), %rcx
	movq	216(%r12), %rsi
	movl	%edx, (%rax)
	movl	%ecx, 4(%rax)
	testq	%rsi, %rsi
	je	.L1849
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1887
.L1849:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	cmpb	$0, 202(%r12)
	jne	.L1888
.L1851:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	%r12, %rdi
	call	_ZN4node10BaseObject8MakeWeakEv
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1888:
	cmpb	$0, 201(%r12)
	jne	.L1851
	cmpb	$0, 200(%r12)
	movl	$256, %eax
	movw	%ax, 202(%r12)
	je	.L1852
	movq	360(%r12), %rdi
	movq	$0, 360(%r12)
	testq	%rdi, %rdi
	je	.L1853
	call	BrotliDecoderDestroyInstance@PLT
.L1853:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1887:
	movq	(%rsi), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1883:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1882:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1881:
	movl	$256, %edx
	cmpb	$0, 200(%rdi)
	movw	%dx, 202(%rdi)
	je	.L1852
	movq	360(%rdi), %rdi
	movq	$0, 360(%r12)
	testq	%rdi, %rdi
	je	.L1843
	call	BrotliDecoderDestroyInstance@PLT
.L1843:
	movl	$0, 248(%r12)
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1886:
	leaq	.LC8(%rip), %rax
	leaq	.LC23(%rip), %rbx
	movl	$-5, -48(%rbp)
	movq	%rbx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1852:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1885:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10261:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi
	.section	.text._ZN4node10BaseObject9ClearWeakEv,"axG",@progbits,_ZN4node10BaseObject9ClearWeakEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject9ClearWeakEv
	.type	_ZN4node10BaseObject9ClearWeakEv, @function
_ZN4node10BaseObject9ClearWeakEv:
.LFB7150:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L1890
	movb	$0, 8(%rax)
.L1890:
	movq	8(%rdi), %rdi
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.cfi_endproc
.LFE7150:
	.size	_ZN4node10BaseObject9ClearWeakEv, .-_ZN4node10BaseObject9ClearWeakEv
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1939
	cmpw	$1040, %si
	jne	.L1895
.L1939:
	movq	23(%rcx), %rax
.L1897:
	cmpl	$7, 16(%r15)
	movq	3280(%rax), %r13
	je	.L1946
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1947
.L1898:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L1894
	shrq	$32, %rbx
	cmpl	$5, %ebx
	ja	.L1948
	movl	16(%r15), %ecx
	cmpl	$1, %ecx
	jle	.L1949
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
.L1902:
	movq	(%rdi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L1903
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L1950
.L1903:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L1951
	movl	16(%r15), %eax
	cmpl	$1, %eax
	jg	.L1906
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, -56(%rbp)
.L1909:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L1894
	shrq	$32, %r12
	cmpl	$3, 16(%r15)
	jle	.L1952
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
.L1912:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testb	%al, %al
	je	.L1894
	movq	-56(%rbp), %rdi
	shrq	$32, %r14
	movl	%r14d, -60(%rbp)
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r12d, %ecx
	cmpq	%r12, %rax
	jb	.L1914
	subq	%rcx, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rax, %r14
	ja	.L1914
	movq	-56(%rbp), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	-72(%rbp), %rcx
	addq	%rcx, %rax
	movl	16(%r15), %ecx
	movq	%rax, -56(%rbp)
.L1904:
	cmpl	$4, %ecx
	jg	.L1916
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1917:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L1953
	movl	16(%r15), %eax
	cmpl	$4, %eax
	jg	.L1919
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r14
.L1922:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L1894
	shrq	$32, %r12
	cmpl	$6, 16(%r15)
	jg	.L1924
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1925:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L1894
	movq	%r14, %rdi
	shrq	$32, %r13
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r12d, %ecx
	cmpq	%r12, %rax
	jb	.L1927
	subq	%rcx, %rax
	movq	%rcx, -72(%rbp)
	cmpq	%rax, %r13
	ja	.L1927
	movq	%r14, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	movq	-72(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1894
	cmpb	$0, 200(%rax)
	je	.L1954
	cmpb	$0, 203(%rax)
	jne	.L1955
	cmpb	$0, 201(%rax)
	jne	.L1956
	cmpb	$0, 202(%rax)
	jne	.L1957
	movb	$1, 201(%rax)
	movl	204(%rax), %eax
	addl	$1, %eax
	movl	%eax, 204(%r14)
	cmpl	$1, %eax
	je	.L1958
.L1933:
	movl	-60(%rbp), %eax
	addq	%rcx, %r12
	leaq	72(%r14), %rsi
	movl	%r13d, 336(%r14)
	movq	%r12, 328(%r14)
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i(%rip), %rcx
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_(%rip), %rdx
	movl	%eax, 312(%r14)
	movq	-56(%rbp), %rax
	movl	%ebx, 252(%r14)
	movq	%rax, 304(%r14)
	movq	64(%r14), %rax
	addl	$1, 2156(%rax)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_queue_work@PLT
	testl	%eax, %eax
	jne	.L1959
	addq	$40, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L1894:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1947:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1898
	cmpl	$5, 43(%rax)
	jne	.L1898
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	8(%r15), %rdi
	leaq	-8(%rdi), %rdx
	movq	%rdx, -56(%rbp)
	cmpl	$2, %eax
	je	.L1960
	subq	$16, %rdi
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L1950:
	cmpl	$3, 43(%rax)
	jne	.L1903
	movl	$0, -60(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1895:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1897
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1919:
	movq	8(%r15), %rdi
	leaq	-32(%rdi), %r14
	cmpl	$5, %eax
	je	.L1961
	subq	$40, %rdi
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1924:
	movq	8(%r15), %rax
	leaq	-48(%rax), %rdi
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1927:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1951:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1953:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1914:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1958:
	movq	%r14, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN4node10BaseObject9ClearWeakEv
	movq	-72(%rbp), %rcx
	jmp	.L1933
.L1954:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1955:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1956:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1957:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1959:
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1948:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1961:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1922
.L1960:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1909
	.cfi_endproc
.LFE8958:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2007
	cmpw	$1040, %si
	jne	.L1963
.L2007:
	movq	23(%rcx), %rax
.L1965:
	cmpl	$7, 16(%r15)
	movq	3280(%rax), %r12
	je	.L2014
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2014:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2015
.L1966:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L1962
	shrq	$32, %rbx
	cmpl	$5, %ebx
	ja	.L2016
	movl	16(%r15), %ecx
	cmpl	$1, %ecx
	jle	.L2017
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
.L1970:
	movq	(%rdi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L1971
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L2018
.L1971:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2019
	movl	16(%r15), %eax
	cmpl	$1, %eax
	jg	.L1974
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, -56(%rbp)
.L1977:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testb	%al, %al
	je	.L1962
	shrq	$32, %r14
	cmpl	$3, 16(%r15)
	jle	.L2020
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
.L1980:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L1962
	movq	-56(%rbp), %rdi
	shrq	$32, %r13
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r14d, %ecx
	cmpq	%r14, %rax
	jb	.L1982
	movl	%r13d, %edx
	subq	%rcx, %rax
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	cmpq	%rax, %r13
	ja	.L1982
	movq	-56(%rbp), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %rax
	movl	16(%r15), %ecx
	movq	%rax, -56(%rbp)
.L1972:
	cmpl	$4, %ecx
	jg	.L1984
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1985:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2021
	movl	16(%r15), %eax
	cmpl	$4, %eax
	jg	.L1987
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r14
.L1990:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L1962
	shrq	$32, %r13
	cmpl	$6, 16(%r15)
	jg	.L1992
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1993:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L1962
	movq	%r14, %rdi
	shrq	$32, %r12
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r13d, %ecx
	cmpq	%r13, %rax
	jb	.L1995
	movl	%r12d, %edx
	subq	%rcx, %rax
	movq	%rcx, -64(%rbp)
	movq	%rdx, -80(%rbp)
	cmpq	%rax, %r12
	ja	.L1995
	movq	%r14, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1962
	cmpb	$0, 200(%rax)
	je	.L2022
	cmpb	$0, 203(%rax)
	jne	.L2023
	cmpb	$0, 201(%rax)
	jne	.L2024
	cmpb	$0, 202(%rax)
	jne	.L2025
	movb	$1, 201(%rax)
	movl	204(%rax), %eax
	addl	$1, %eax
	movl	%eax, 204(%r13)
	cmpl	$1, %eax
	je	.L2026
.L2001:
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %xmm0
	addq	%rcx, %r12
	movl	%ebx, 288(%r13)
	movq	%r12, 264(%r13)
	leaq	72(%r13), %rsi
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i(%rip), %rcx
	movq	%rax, 256(%r13)
	movq	64(%r13), %rax
	movhps	-80(%rbp), %xmm0
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_(%rip), %rdx
	movups	%xmm0, 272(%r13)
	addl	$1, 2156(%rax)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_queue_work@PLT
	testl	%eax, %eax
	jne	.L2027
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L1962:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2015:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1966
	cmpl	$5, 43(%rax)
	jne	.L1966
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1974:
	movq	8(%r15), %rdi
	leaq	-8(%rdi), %rdx
	movq	%rdx, -56(%rbp)
	cmpl	$2, %eax
	je	.L2028
	subq	$16, %rdi
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2020:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1980
	.p2align 4,,10
	.p2align 3
.L2018:
	cmpl	$3, 43(%rax)
	jne	.L1971
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L1963:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1987:
	movq	8(%r15), %rdi
	leaq	-32(%rdi), %r14
	cmpl	$5, %eax
	je	.L2029
	subq	$40, %rdi
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	8(%r15), %rax
	leaq	-48(%rax), %rdi
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L1995:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2019:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2021:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1982:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2026:
	movq	%r13, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN4node10BaseObject9ClearWeakEv
	movq	-64(%rbp), %rcx
	jmp	.L2001
.L2022:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2023:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2024:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2025:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2027:
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2016:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2029:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1990
.L2028:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1977
	.cfi_endproc
.LFE8963:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2075
	cmpw	$1040, %si
	jne	.L2031
.L2075:
	movq	23(%rcx), %rax
.L2033:
	cmpl	$7, 16(%r15)
	movq	3280(%rax), %r12
	je	.L2082
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2083
.L2034:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L2030
	shrq	$32, %rbx
	cmpl	$5, %ebx
	ja	.L2084
	movl	16(%r15), %ecx
	cmpl	$1, %ecx
	jle	.L2085
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
.L2038:
	movq	(%rdi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L2039
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L2086
.L2039:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2087
	movl	16(%r15), %eax
	cmpl	$1, %eax
	jg	.L2042
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, -56(%rbp)
.L2045:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testb	%al, %al
	je	.L2030
	shrq	$32, %r14
	cmpl	$3, 16(%r15)
	jle	.L2088
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
.L2048:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L2030
	movq	-56(%rbp), %rdi
	shrq	$32, %r13
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r14d, %ecx
	cmpq	%r14, %rax
	jb	.L2050
	movl	%r13d, %edx
	subq	%rcx, %rax
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	cmpq	%rax, %r13
	ja	.L2050
	movq	-56(%rbp), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %rax
	movl	16(%r15), %ecx
	movq	%rax, -56(%rbp)
.L2040:
	cmpl	$4, %ecx
	jg	.L2052
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2053:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2089
	movl	16(%r15), %eax
	cmpl	$4, %eax
	jg	.L2055
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r14
.L2058:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L2030
	shrq	$32, %r13
	cmpl	$6, 16(%r15)
	jg	.L2060
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2061:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L2030
	movq	%r14, %rdi
	shrq	$32, %r12
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r13d, %ecx
	cmpq	%r13, %rax
	jb	.L2063
	movl	%r12d, %edx
	subq	%rcx, %rax
	movq	%rcx, -64(%rbp)
	movq	%rdx, -80(%rbp)
	cmpq	%rax, %r12
	ja	.L2063
	movq	%r14, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L2030
	cmpb	$0, 200(%rax)
	je	.L2090
	cmpb	$0, 203(%rax)
	jne	.L2091
	cmpb	$0, 201(%rax)
	jne	.L2092
	cmpb	$0, 202(%rax)
	jne	.L2093
	movb	$1, 201(%rax)
	movl	204(%rax), %eax
	addl	$1, %eax
	movl	%eax, 204(%r13)
	cmpl	$1, %eax
	je	.L2094
.L2069:
	movq	-56(%rbp), %rax
	movq	-72(%rbp), %xmm0
	addq	%rcx, %r12
	movl	%ebx, 288(%r13)
	movq	%r12, 264(%r13)
	leaq	72(%r13), %rsi
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i(%rip), %rcx
	movq	%rax, 256(%r13)
	movq	64(%r13), %rax
	movhps	-80(%rbp), %xmm0
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_(%rip), %rdx
	movups	%xmm0, 272(%r13)
	addl	$1, 2156(%rax)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_queue_work@PLT
	testl	%eax, %eax
	jne	.L2095
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L2030:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2083:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2034
	cmpl	$5, 43(%rax)
	jne	.L2034
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2085:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	8(%r15), %rdi
	leaq	-8(%rdi), %rdx
	movq	%rdx, -56(%rbp)
	cmpl	$2, %eax
	je	.L2096
	subq	$16, %rdi
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2086:
	cmpl	$3, 43(%rax)
	jne	.L2039
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L2040
	.p2align 4,,10
	.p2align 3
.L2031:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2052:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L2055:
	movq	8(%r15), %rdi
	leaq	-32(%rdi), %r14
	cmpl	$5, %eax
	je	.L2097
	subq	$40, %rdi
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	8(%r15), %rax
	leaq	-48(%rax), %rdi
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2063:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2087:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2089:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2050:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2094:
	movq	%r13, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN4node10BaseObject9ClearWeakEv
	movq	-64(%rbp), %rcx
	jmp	.L2069
.L2090:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2091:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2092:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2093:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2095:
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2084:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2097:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2058
.L2096:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2045
	.cfi_endproc
.LFE8970:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2148
	cmpw	$1040, %si
	jne	.L2099
.L2148:
	movq	23(%rcx), %rax
.L2101:
	cmpl	$7, 16(%r15)
	movq	3280(%rax), %r13
	je	.L2155
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2156
.L2102:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L2098
	shrq	$32, %rbx
	cmpl	$5, %ebx
	ja	.L2157
	movl	16(%r15), %ecx
	cmpl	$1, %ecx
	jle	.L2158
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
.L2106:
	movq	(%rdi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L2107
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L2159
.L2107:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2160
	movl	16(%r15), %eax
	cmpl	$1, %eax
	jg	.L2110
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, -88(%rbp)
.L2113:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L2098
	shrq	$32, %r12
	cmpl	$3, 16(%r15)
	jle	.L2161
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
.L2116:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testb	%al, %al
	je	.L2098
	movq	-88(%rbp), %rdi
	shrq	$32, %r14
	movl	%r14d, -92(%rbp)
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r12d, %ecx
	cmpq	%r12, %rax
	jb	.L2118
	subq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	cmpq	%rax, %r14
	ja	.L2118
	movq	-88(%rbp), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	-104(%rbp), %rcx
	addq	%rcx, %rax
	movl	16(%r15), %ecx
	movq	%rax, -88(%rbp)
.L2108:
	cmpl	$4, %ecx
	jg	.L2120
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2121:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2162
	movl	16(%r15), %eax
	cmpl	$4, %eax
	jg	.L2123
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r14
.L2126:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L2098
	shrq	$32, %r12
	cmpl	$6, 16(%r15)
	jg	.L2128
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2129:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L2098
	movq	%r14, %rdi
	shrq	$32, %r13
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r12d, %ecx
	cmpq	%r12, %rax
	jb	.L2131
	subq	%rcx, %rax
	movq	%rcx, -104(%rbp)
	cmpq	%rax, %r13
	ja	.L2131
	movq	%r14, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L2098
	cmpb	$0, 200(%rax)
	je	.L2163
	cmpb	$0, 203(%rax)
	jne	.L2164
	cmpb	$0, 201(%rax)
	jne	.L2165
	cmpb	$0, 202(%rax)
	jne	.L2166
	movb	$1, 201(%rax)
	movl	204(%rax), %eax
	addl	$1, %eax
	movl	%eax, 204(%r14)
	cmpl	$1, %eax
	je	.L2167
.L2137:
	movl	-92(%rbp), %eax
	addq	%rcx, %r12
	movq	16(%r14), %rdi
	movl	%r13d, 336(%r14)
	movq	%r12, 328(%r14)
	leaq	240(%r14), %r15
	leaq	-80(%rbp), %r12
	movl	%eax, 312(%r14)
	movq	-88(%rbp), %rax
	movl	%ebx, 252(%r14)
	movq	%rax, 304(%r14)
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	%r15, %rdi
	call	_ZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEv
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK4node12_GLOBAL__N_111ZlibContext12GetErrorInfoEv
	cmpq	$0, -72(%rbp)
	je	.L2138
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorE
.L2139:
	movl	204(%r14), %eax
	testl	%eax, %eax
	je	.L2168
	subl	$1, %eax
	movl	%eax, 204(%r14)
	je	.L2169
.L2141:
	movq	%r14, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2170
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2156:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2102
	cmpl	$5, 43(%rax)
	jne	.L2102
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2106
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	8(%r15), %rdi
	leaq	-8(%rdi), %rdx
	movq	%rdx, -88(%rbp)
	cmpl	$2, %eax
	je	.L2171
	subq	$16, %rdi
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2161:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2159:
	cmpl	$3, 43(%rax)
	jne	.L2107
	movl	$0, -92(%rbp)
	movq	$0, -88(%rbp)
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2099:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2120:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2123:
	movq	8(%r15), %rdi
	leaq	-32(%rdi), %r14
	cmpl	$5, %eax
	je	.L2172
	subq	$40, %rdi
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	8(%r15), %rax
	leaq	-48(%rax), %rdi
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2131:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2160:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2162:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2118:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2138:
	movl	312(%r14), %edx
	movq	208(%r14), %rax
	movl	%edx, 4(%rax)
	movl	336(%r14), %edx
	movl	%edx, (%rax)
	movb	$0, 201(%r14)
	jmp	.L2139
.L2169:
	movq	%r14, %rdi
	call	_ZN4node10BaseObject8MakeWeakEv
	jmp	.L2141
.L2167:
	movq	%r14, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN4node10BaseObject9ClearWeakEv
	movq	-104(%rbp), %rcx
	jmp	.L2137
.L2163:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2164:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2165:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2166:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2168:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2170:
	call	__stack_chk_fail@PLT
.L2157:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2172:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2126
.L2171:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2113
	.cfi_endproc
.LFE8959:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2225
	cmpw	$1040, %si
	jne	.L2174
.L2225:
	movq	23(%rcx), %rax
.L2176:
	cmpl	$7, 16(%r15)
	movq	3280(%rax), %r12
	je	.L2232
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2233
.L2177:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L2173
	shrq	$32, %rbx
	cmpl	$5, %ebx
	ja	.L2234
	movl	16(%r15), %ecx
	cmpl	$1, %ecx
	jle	.L2235
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
.L2181:
	movq	(%rdi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L2182
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L2236
.L2182:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2237
	movl	16(%r15), %eax
	cmpl	$1, %eax
	jg	.L2185
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, -88(%rbp)
.L2188:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testb	%al, %al
	je	.L2173
	shrq	$32, %r14
	cmpl	$3, 16(%r15)
	jle	.L2238
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
.L2191:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L2173
	movq	-88(%rbp), %rdi
	shrq	$32, %r13
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r14d, %ecx
	cmpq	%r14, %rax
	jb	.L2193
	movl	%r13d, %edx
	subq	%rcx, %rax
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	cmpq	%rax, %r13
	ja	.L2193
	movq	-88(%rbp), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	-96(%rbp), %rcx
	addq	%rcx, %rax
	movl	16(%r15), %ecx
	movq	%rax, -88(%rbp)
.L2183:
	cmpl	$4, %ecx
	jg	.L2195
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2196:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2239
	movl	16(%r15), %eax
	cmpl	$4, %eax
	jg	.L2198
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r14
.L2201:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L2173
	shrq	$32, %r13
	cmpl	$6, 16(%r15)
	jg	.L2203
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2204:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L2173
	movq	%r14, %rdi
	shrq	$32, %r12
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r13d, %ecx
	cmpq	%r13, %rax
	jb	.L2206
	movl	%r12d, %edx
	subq	%rcx, %rax
	movq	%rcx, -96(%rbp)
	movq	%rdx, -112(%rbp)
	cmpq	%rax, %r12
	ja	.L2206
	movq	%r14, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	movq	-96(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L2173
	cmpb	$0, 200(%rax)
	je	.L2240
	cmpb	$0, 203(%rax)
	jne	.L2241
	cmpb	$0, 201(%rax)
	jne	.L2242
	cmpb	$0, 202(%rax)
	jne	.L2243
	movb	$1, 201(%rax)
	movl	204(%rax), %eax
	addl	$1, %eax
	movl	%eax, 204(%r13)
	cmpl	$1, %eax
	je	.L2244
.L2212:
	movq	-88(%rbp), %rax
	addq	%rcx, %r12
	movq	16(%r13), %rdi
	movl	%ebx, 288(%r13)
	movq	-104(%rbp), %xmm0
	movq	%r12, 264(%r13)
	movq	%rax, 256(%r13)
	movhps	-112(%rbp), %xmm0
	movups	%xmm0, 272(%r13)
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	cmpl	$9, 248(%r13)
	jne	.L2245
	movq	328(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2246
	subq	$8, %rsp
	movq	256(%r13), %rax
	leaq	-80(%rbp), %r12
	movl	288(%r13), %esi
	pushq	$0
	leaq	272(%r13), %rdx
	movq	%r12, %rcx
	leaq	264(%r13), %r9
	leaq	280(%r13), %r8
	movq	%rax, -80(%rbp)
	call	BrotliEncoderCompressStream@PLT
	popq	%rdx
	movq	-80(%rbp), %rdx
	testl	%eax, %eax
	popq	%rcx
	movq	%rdx, 256(%r13)
	setne	320(%r13)
	je	.L2247
	movq	208(%r13), %rax
	movq	280(%r13), %rdx
	movq	272(%r13), %rcx
	movl	%edx, (%rax)
	movl	%ecx, 4(%rax)
	movb	$0, 201(%r13)
.L2216:
	movl	204(%r13), %eax
	testl	%eax, %eax
	je	.L2248
	subl	$1, %eax
	movl	%eax, 204(%r13)
	je	.L2249
.L2218:
	movq	%r13, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2250
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2233:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2177
	cmpl	$5, 43(%rax)
	jne	.L2177
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L2185:
	movq	8(%r15), %rdi
	leaq	-8(%rdi), %rdx
	movq	%rdx, -88(%rbp)
	cmpl	$2, %eax
	je	.L2251
	subq	$16, %rdi
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2238:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2236:
	cmpl	$3, 43(%rax)
	jne	.L2182
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2174:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2195:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	8(%r15), %rdi
	leaq	-32(%rdi), %r14
	cmpl	$5, %eax
	je	.L2252
	subq	$40, %rdi
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2203:
	movq	8(%r15), %rax
	leaq	-48(%rax), %rdi
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2206:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2237:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2239:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2193:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2247:
	leaq	.LC25(%rip), %rax
	leaq	.LC24(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	movl	$-1, -64(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	jmp	.L2216
.L2249:
	movq	%r13, %rdi
	call	_ZN4node10BaseObject8MakeWeakEv
	jmp	.L2218
.L2244:
	movq	%r13, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN4node10BaseObject9ClearWeakEv
	movq	-96(%rbp), %rcx
	jmp	.L2212
.L2240:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2241:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2242:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2243:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2245:
	leaq	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2246:
	leaq	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2248:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2250:
	call	__stack_chk_fail@PLT
.L2234:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2252:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2201
.L2251:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2188
	.cfi_endproc
.LFE8964:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2305
	cmpw	$1040, %si
	jne	.L2254
.L2305:
	movq	23(%rcx), %rax
.L2256:
	cmpl	$7, 16(%r15)
	movq	3280(%rax), %r12
	je	.L2315
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2315:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2316
.L2257:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L2253
	shrq	$32, %rbx
	cmpl	$5, %ebx
	ja	.L2317
	movl	16(%r15), %ecx
	cmpl	$1, %ecx
	jle	.L2318
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
.L2261:
	movq	(%rdi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L2262
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L2319
.L2262:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2320
	movl	16(%r15), %eax
	cmpl	$1, %eax
	jg	.L2265
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, -88(%rbp)
.L2268:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testb	%al, %al
	je	.L2253
	shrq	$32, %r14
	cmpl	$3, 16(%r15)
	jle	.L2321
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
.L2271:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L2253
	movq	-88(%rbp), %rdi
	shrq	$32, %r13
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r14d, %ecx
	cmpq	%r14, %rax
	jb	.L2273
	movl	%r13d, %edx
	subq	%rcx, %rax
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	cmpq	%rax, %r13
	ja	.L2273
	movq	-88(%rbp), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	-96(%rbp), %rcx
	addq	%rcx, %rax
	movl	16(%r15), %ecx
	movq	%rax, -88(%rbp)
.L2263:
	cmpl	$4, %ecx
	jg	.L2275
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2276:
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L2322
	movl	16(%r15), %eax
	cmpl	$4, %eax
	jg	.L2278
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r14
.L2281:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L2253
	shrq	$32, %r13
	cmpl	$6, 16(%r15)
	jg	.L2283
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2284:
	movq	%r12, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L2253
	movq	%r14, %rdi
	shrq	$32, %r12
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE@PLT
	movl	%r13d, %ecx
	cmpq	%r13, %rax
	jb	.L2286
	movl	%r12d, %edx
	subq	%rcx, %rax
	movq	%rcx, -96(%rbp)
	movq	%rdx, -112(%rbp)
	cmpq	%rax, %r12
	ja	.L2286
	movq	%r14, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	(%r15), %rdi
	movq	%rax, %r12
	call	_ZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEE
	movq	-96(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L2253
	cmpb	$0, 200(%rax)
	je	.L2323
	cmpb	$0, 203(%rax)
	jne	.L2324
	cmpb	$0, 201(%rax)
	jne	.L2325
	cmpb	$0, 202(%rax)
	jne	.L2326
	movb	$1, 201(%rax)
	movl	204(%rax), %eax
	addl	$1, %eax
	movl	%eax, 204(%r13)
	cmpl	$1, %eax
	je	.L2327
.L2292:
	movq	-88(%rbp), %rax
	addq	%rcx, %r12
	movq	16(%r13), %rdi
	movl	%ebx, 288(%r13)
	movq	-104(%rbp), %xmm0
	movq	%r12, 264(%r13)
	movq	%rax, 256(%r13)
	movhps	-112(%rbp), %xmm0
	movups	%xmm0, 272(%r13)
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	leaq	240(%r13), %rdi
	call	_ZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEv
	movl	324(%r13), %eax
	testl	%eax, %eax
	jne	.L2328
	cmpl	$2, 288(%r13)
	je	.L2329
.L2294:
	movq	208(%r13), %rax
	movq	280(%r13), %rdx
	movq	272(%r13), %rcx
	movl	%edx, (%rax)
	movl	%ecx, 4(%rax)
	movb	$0, 201(%r13)
.L2296:
	movl	204(%r13), %eax
	testl	%eax, %eax
	je	.L2330
	subl	$1, %eax
	movl	%eax, 204(%r13)
	je	.L2331
.L2298:
	movq	%r13, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEv
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2332
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2316:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2257
	cmpl	$5, 43(%rax)
	jne	.L2257
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2318:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2265:
	movq	8(%r15), %rdi
	leaq	-8(%rdi), %rdx
	movq	%rdx, -88(%rbp)
	cmpl	$2, %eax
	je	.L2333
	subq	$16, %rdi
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2319:
	cmpl	$3, 43(%rax)
	jne	.L2262
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2254:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	8(%r15), %rdi
	leaq	-32(%rdi), %r14
	cmpl	$5, %eax
	je	.L2334
	subq	$40, %rdi
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2283:
	movq	8(%r15), %rax
	leaq	-48(%rax), %rdi
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2286:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2320:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2322:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	328(%r13), %rdx
	leaq	.LC26(%rip), %rbx
	movl	%eax, -64(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L2294
.L2295:
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorE
	jmp	.L2296
	.p2align 4,,10
	.p2align 3
.L2273:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2331:
	movq	%r13, %rdi
	call	_ZN4node10BaseObject8MakeWeakEv
	jmp	.L2298
.L2327:
	movq	%r13, %rdi
	movq	%rcx, -96(%rbp)
	call	_ZN4node10BaseObject9ClearWeakEv
	movq	-96(%rbp), %rcx
	jmp	.L2292
.L2329:
	cmpl	$2, 320(%r13)
	jne	.L2294
	leaq	.LC8(%rip), %rax
	leaq	.LC23(%rip), %rdx
	movl	$-5, -64(%rbp)
	movq	%rdx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L2295
.L2323:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2324:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2325:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2326:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2330:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5UnrefEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2332:
	call	__stack_chk_fail@PLT
.L2317:
	leaq	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2334:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2281
.L2333:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2268
	.cfi_endproc
.LFE8971:
	.size	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.section	.rodata.str1.1
.LC45:
	.string	"Z_NO_FLUSH"
.LC47:
	.string	"Z_PARTIAL_FLUSH"
.LC49:
	.string	"Z_SYNC_FLUSH"
.LC51:
	.string	"Z_FULL_FLUSH"
.LC53:
	.string	"Z_FINISH"
.LC55:
	.string	"Z_BLOCK"
.LC62:
	.string	"Z_NO_COMPRESSION"
.LC63:
	.string	"Z_BEST_SPEED"
.LC64:
	.string	"Z_BEST_COMPRESSION"
.LC66:
	.string	"Z_DEFAULT_COMPRESSION"
.LC67:
	.string	"Z_FILTERED"
.LC68:
	.string	"Z_HUFFMAN_ONLY"
.LC69:
	.string	"Z_RLE"
.LC70:
	.string	"Z_FIXED"
.LC71:
	.string	"Z_DEFAULT_STRATEGY"
.LC72:
	.string	"ZLIB_VERNUM"
.LC74:
	.string	"DEFLATE"
.LC75:
	.string	"INFLATE"
.LC76:
	.string	"GZIP"
.LC77:
	.string	"GUNZIP"
.LC78:
	.string	"DEFLATERAW"
.LC79:
	.string	"INFLATERAW"
.LC81:
	.string	"UNZIP"
.LC83:
	.string	"BROTLI_DECODE"
.LC85:
	.string	"BROTLI_ENCODE"
.LC86:
	.string	"Z_MIN_WINDOWBITS"
.LC87:
	.string	"Z_MAX_WINDOWBITS"
.LC89:
	.string	"Z_DEFAULT_WINDOWBITS"
.LC90:
	.string	"Z_MIN_CHUNK"
.LC92:
	.string	"Z_MAX_CHUNK"
.LC94:
	.string	"Z_DEFAULT_CHUNK"
.LC96:
	.string	"Z_MIN_MEMLEVEL"
.LC97:
	.string	"Z_MAX_MEMLEVEL"
.LC98:
	.string	"Z_DEFAULT_MEMLEVEL"
.LC99:
	.string	"Z_MIN_LEVEL"
.LC100:
	.string	"Z_MAX_LEVEL"
.LC101:
	.string	"Z_DEFAULT_LEVEL"
.LC102:
	.string	"BROTLI_OPERATION_PROCESS"
.LC103:
	.string	"BROTLI_OPERATION_FLUSH"
.LC104:
	.string	"BROTLI_OPERATION_FINISH"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"BROTLI_OPERATION_EMIT_METADATA"
	.section	.rodata.str1.1
.LC106:
	.string	"BROTLI_PARAM_MODE"
.LC107:
	.string	"BROTLI_MODE_GENERIC"
.LC108:
	.string	"BROTLI_MODE_TEXT"
.LC109:
	.string	"BROTLI_MODE_FONT"
.LC110:
	.string	"BROTLI_DEFAULT_MODE"
.LC111:
	.string	"BROTLI_PARAM_QUALITY"
.LC112:
	.string	"BROTLI_MIN_QUALITY"
.LC113:
	.string	"BROTLI_MAX_QUALITY"
.LC115:
	.string	"BROTLI_DEFAULT_QUALITY"
.LC116:
	.string	"BROTLI_PARAM_LGWIN"
.LC117:
	.string	"BROTLI_MIN_WINDOW_BITS"
.LC119:
	.string	"BROTLI_MAX_WINDOW_BITS"
.LC121:
	.string	"BROTLI_LARGE_MAX_WINDOW_BITS"
.LC123:
	.string	"BROTLI_DEFAULT_WINDOW"
.LC125:
	.string	"BROTLI_PARAM_LGBLOCK"
.LC126:
	.string	"BROTLI_MIN_INPUT_BLOCK_BITS"
.LC128:
	.string	"BROTLI_MAX_INPUT_BLOCK_BITS"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"BROTLI_PARAM_DISABLE_LITERAL_CONTEXT_MODELING"
	.section	.rodata.str1.1
.LC130:
	.string	"BROTLI_PARAM_SIZE_HINT"
.LC131:
	.string	"BROTLI_PARAM_LARGE_WINDOW"
.LC132:
	.string	"BROTLI_PARAM_NPOSTFIX"
.LC133:
	.string	"BROTLI_PARAM_NDIRECT"
.LC134:
	.string	"BROTLI_DECODER_RESULT_ERROR"
.LC135:
	.string	"BROTLI_DECODER_RESULT_SUCCESS"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"BROTLI_DECODER_RESULT_NEEDS_MORE_INPUT"
	.align 8
.LC137:
	.string	"BROTLI_DECODER_RESULT_NEEDS_MORE_OUTPUT"
	.align 8
.LC138:
	.string	"BROTLI_DECODER_PARAM_DISABLE_RING_BUFFER_REALLOCATION"
	.align 8
.LC139:
	.string	"BROTLI_DECODER_PARAM_LARGE_WINDOW"
	.section	.rodata.str1.1
.LC140:
	.string	"BROTLI_DECODER_NO_ERROR"
.LC141:
	.string	"BROTLI_DECODER_SUCCESS"
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"BROTLI_DECODER_NEEDS_MORE_INPUT"
	.align 8
.LC143:
	.string	"BROTLI_DECODER_NEEDS_MORE_OUTPUT"
	.align 8
.LC144:
	.string	"BROTLI_DECODER_ERROR_FORMAT_EXUBERANT_NIBBLE"
	.align 8
.LC145:
	.string	"BROTLI_DECODER_ERROR_FORMAT_RESERVED"
	.align 8
.LC146:
	.string	"BROTLI_DECODER_ERROR_FORMAT_EXUBERANT_META_NIBBLE"
	.align 8
.LC147:
	.string	"BROTLI_DECODER_ERROR_FORMAT_SIMPLE_HUFFMAN_ALPHABET"
	.align 8
.LC148:
	.string	"BROTLI_DECODER_ERROR_FORMAT_SIMPLE_HUFFMAN_SAME"
	.align 8
.LC149:
	.string	"BROTLI_DECODER_ERROR_FORMAT_CL_SPACE"
	.align 8
.LC150:
	.string	"BROTLI_DECODER_ERROR_FORMAT_HUFFMAN_SPACE"
	.align 8
.LC152:
	.string	"BROTLI_DECODER_ERROR_FORMAT_CONTEXT_MAP_REPEAT"
	.align 8
.LC154:
	.string	"BROTLI_DECODER_ERROR_FORMAT_BLOCK_LENGTH_1"
	.align 8
.LC156:
	.string	"BROTLI_DECODER_ERROR_FORMAT_BLOCK_LENGTH_2"
	.align 8
.LC158:
	.string	"BROTLI_DECODER_ERROR_FORMAT_TRANSFORM"
	.align 8
.LC160:
	.string	"BROTLI_DECODER_ERROR_FORMAT_DICTIONARY"
	.align 8
.LC162:
	.string	"BROTLI_DECODER_ERROR_FORMAT_WINDOW_BITS"
	.align 8
.LC164:
	.string	"BROTLI_DECODER_ERROR_FORMAT_PADDING_1"
	.align 8
.LC166:
	.string	"BROTLI_DECODER_ERROR_FORMAT_PADDING_2"
	.align 8
.LC168:
	.string	"BROTLI_DECODER_ERROR_FORMAT_DISTANCE"
	.align 8
.LC170:
	.string	"BROTLI_DECODER_ERROR_DICTIONARY_NOT_SET"
	.align 8
.LC172:
	.string	"BROTLI_DECODER_ERROR_INVALID_ARGUMENTS"
	.align 8
.LC174:
	.string	"BROTLI_DECODER_ERROR_ALLOC_CONTEXT_MODES"
	.align 8
.LC176:
	.string	"BROTLI_DECODER_ERROR_ALLOC_TREE_GROUPS"
	.align 8
.LC178:
	.string	"BROTLI_DECODER_ERROR_ALLOC_CONTEXT_MAP"
	.align 8
.LC180:
	.string	"BROTLI_DECODER_ERROR_ALLOC_RING_BUFFER_1"
	.align 8
.LC182:
	.string	"BROTLI_DECODER_ERROR_ALLOC_RING_BUFFER_2"
	.align 8
.LC184:
	.string	"BROTLI_DECODER_ERROR_ALLOC_BLOCK_TYPE_TREES"
	.align 8
.LC186:
	.string	"BROTLI_DECODER_ERROR_UNREACHABLE"
	.text
	.p2align 4
	.globl	_ZN4node19DefineZlibConstantsEN2v85LocalINS0_6ObjectEEE
	.type	_ZN4node19DefineZlibConstantsEN2v85LocalINS0_6ObjectEEE, @function
_ZN4node19DefineZlibConstantsEN2v85LocalINS0_6ObjectEEE:
.LFB7701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC45(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2551
.L2336:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2552
.L2337:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC47(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2553
.L2338:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2554
.L2339:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC49(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2555
.L2340:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2556
.L2341:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC51(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2557
.L2342:
	movq	.LC52(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2558
.L2343:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC53(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2559
.L2344:
	movq	.LC54(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2560
.L2345:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC55(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2561
.L2346:
	movq	.LC56(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2562
.L2347:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2563
.L2348:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2564
.L2349:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2565
.L2350:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2566
.L2351:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2567
.L2352:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2568
.L2353:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2569
.L2354:
	movq	.LC16(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2570
.L2355:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2571
.L2356:
	movq	.LC57(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2572
.L2357:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2573
.L2358:
	movq	.LC58(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2574
.L2359:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2575
.L2360:
	movq	.LC59(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2576
.L2361:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2577
.L2362:
	movq	.LC60(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2578
.L2363:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2579
.L2364:
	movq	.LC61(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2580
.L2365:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC62(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2581
.L2366:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2582
.L2367:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC63(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2583
.L2368:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2584
.L2369:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC64(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2585
.L2370:
	movq	.LC65(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2586
.L2371:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC66(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2587
.L2372:
	movq	.LC16(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2588
.L2373:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC67(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2589
.L2374:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2590
.L2375:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC68(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2591
.L2376:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2592
.L2377:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC69(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2593
.L2378:
	movq	.LC52(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2594
.L2379:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC70(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2595
.L2380:
	movq	.LC54(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2596
.L2381:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC71(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2597
.L2382:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2598
.L2383:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC72(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2599
.L2384:
	movsd	.LC73(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2600
.L2385:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC74(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2601
.L2386:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2602
.L2387:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC75(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2603
.L2388:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2604
.L2389:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC76(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2605
.L2390:
	movq	.LC52(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2606
.L2391:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC77(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2607
.L2392:
	movq	.LC54(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2608
.L2393:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC78(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2609
.L2394:
	movq	.LC56(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2610
.L2395:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC79(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2611
.L2396:
	movq	.LC80(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2612
.L2397:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC81(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2613
.L2398:
	movq	.LC82(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2614
.L2399:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC83(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2615
.L2400:
	movq	.LC84(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2616
.L2401:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC85(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2617
.L2402:
	movq	.LC65(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2618
.L2403:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC86(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2619
.L2404:
	movq	.LC84(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2620
.L2405:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC87(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2621
.L2406:
	movq	.LC88(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2622
.L2407:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC89(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2623
.L2408:
	movq	.LC88(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2624
.L2409:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC90(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2625
.L2410:
	movsd	.LC91(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2626
.L2411:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC92(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2627
.L2412:
	movsd	.LC93(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2628
.L2413:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC94(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2629
.L2414:
	movsd	.LC95(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2630
.L2415:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC96(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2631
.L2416:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2632
.L2417:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC97(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2633
.L2418:
	movq	.LC65(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2634
.L2419:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC98(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2635
.L2420:
	movq	.LC84(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2636
.L2421:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC99(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2637
.L2422:
	movq	.LC16(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2638
.L2423:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC100(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2639
.L2424:
	movq	.LC65(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2640
.L2425:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC101(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2641
.L2426:
	movq	.LC16(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2642
.L2427:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC102(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2643
.L2428:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2644
.L2429:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC103(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2645
.L2430:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2646
.L2431:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC104(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2647
.L2432:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2648
.L2433:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC105(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2649
.L2434:
	movq	.LC52(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2650
.L2435:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC106(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2651
.L2436:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2652
.L2437:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC107(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2653
.L2438:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2654
.L2439:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC108(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2655
.L2440:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2656
.L2441:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC109(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2657
.L2442:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2658
.L2443:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC110(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2659
.L2444:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2660
.L2445:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC111(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2661
.L2446:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2662
.L2447:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC112(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2663
.L2448:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2664
.L2449:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC113(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2665
.L2450:
	movq	.LC114(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2666
.L2451:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC115(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2667
.L2452:
	movq	.LC114(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2668
.L2453:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC116(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2669
.L2454:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2670
.L2455:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC117(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2671
.L2456:
	movsd	.LC118(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2672
.L2457:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC119(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2673
.L2458:
	movq	.LC120(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2674
.L2459:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC121(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2675
.L2460:
	movsd	.LC122(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2676
.L2461:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC123(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2677
.L2462:
	movsd	.LC124(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2678
.L2463:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC125(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2679
.L2464:
	movq	.LC52(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2680
.L2465:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC126(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2681
.L2466:
	movsd	.LC127(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2682
.L2467:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC128(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2683
.L2468:
	movq	.LC120(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2684
.L2469:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC129(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2685
.L2470:
	movq	.LC54(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2686
.L2471:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC130(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2687
.L2472:
	movq	.LC56(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2688
.L2473:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC131(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2689
.L2474:
	movq	.LC80(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2690
.L2475:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC132(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2691
.L2476:
	movq	.LC82(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2692
.L2477:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC133(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2693
.L2478:
	movq	.LC84(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2694
.L2479:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC134(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2695
.L2480:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2696
.L2481:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC135(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2697
.L2482:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2698
.L2483:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC136(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2699
.L2484:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2700
.L2485:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC137(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2701
.L2486:
	movq	.LC52(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2702
.L2487:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC138(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2703
.L2488:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2704
.L2489:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC139(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2705
.L2490:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2706
.L2491:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC140(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2707
.L2492:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2708
.L2493:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC141(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2709
.L2494:
	movq	.LC48(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2710
.L2495:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC142(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2711
.L2496:
	movq	.LC50(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2712
.L2497:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC143(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2713
.L2498:
	movq	.LC52(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2714
.L2499:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC144(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2715
.L2500:
	movq	.LC16(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2716
.L2501:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC145(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2717
.L2502:
	movq	.LC57(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2718
.L2503:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC146(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2719
.L2504:
	movq	.LC58(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2720
.L2505:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC147(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2721
.L2506:
	movq	.LC59(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2722
.L2507:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC148(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2723
.L2508:
	movq	.LC60(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2724
.L2509:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC149(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2725
.L2510:
	movq	.LC61(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2726
.L2511:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC150(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2727
.L2512:
	movsd	.LC151(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2728
.L2513:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC152(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2729
.L2514:
	movsd	.LC153(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2730
.L2515:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC154(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2731
.L2516:
	movsd	.LC155(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2732
.L2517:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC156(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2733
.L2518:
	movsd	.LC157(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2734
.L2519:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC158(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2735
.L2520:
	movsd	.LC159(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2736
.L2521:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC160(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2737
.L2522:
	movsd	.LC161(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2738
.L2523:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC162(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2739
.L2524:
	movsd	.LC163(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2740
.L2525:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC164(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2741
.L2526:
	movsd	.LC165(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2742
.L2527:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC166(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2743
.L2528:
	movsd	.LC167(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2744
.L2529:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC168(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2745
.L2530:
	movsd	.LC169(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2746
.L2531:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC170(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2747
.L2532:
	movsd	.LC171(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2748
.L2533:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC172(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2749
.L2534:
	movsd	.LC173(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2750
.L2535:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC174(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2751
.L2536:
	movsd	.LC175(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2752
.L2537:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC176(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2753
.L2538:
	movsd	.LC177(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2754
.L2539:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC178(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2755
.L2540:
	movsd	.LC179(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2756
.L2541:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC180(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2757
.L2542:
	movsd	.LC181(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2758
.L2543:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC182(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2759
.L2544:
	movsd	.LC183(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2760
.L2545:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC184(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2761
.L2546:
	movsd	.LC185(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2762
.L2547:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC186(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2763
.L2548:
	movsd	.LC187(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2764
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2551:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2552:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2553:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2554:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2339
	.p2align 4,,10
	.p2align 3
.L2555:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2556:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2557:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2558:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2559:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2560:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2345
	.p2align 4,,10
	.p2align 3
.L2561:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2562:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2563:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2564:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2565:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2566:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2567:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2568:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2569:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2570:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2355
	.p2align 4,,10
	.p2align 3
.L2571:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2572:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2357
	.p2align 4,,10
	.p2align 3
.L2573:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2574:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2575:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2576:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2577:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2578:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2363
	.p2align 4,,10
	.p2align 3
.L2579:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2580:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2581:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2582:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2367
	.p2align 4,,10
	.p2align 3
.L2583:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2584:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2585:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2586:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2587:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2588:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2589:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2374
	.p2align 4,,10
	.p2align 3
.L2590:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2591:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2592:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2593:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2378
	.p2align 4,,10
	.p2align 3
.L2594:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2595:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2596:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2597:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2598:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2383
	.p2align 4,,10
	.p2align 3
.L2599:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2384
	.p2align 4,,10
	.p2align 3
.L2600:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2601:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2386
	.p2align 4,,10
	.p2align 3
.L2602:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2603:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2388
	.p2align 4,,10
	.p2align 3
.L2604:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2389
	.p2align 4,,10
	.p2align 3
.L2605:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2606:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2391
	.p2align 4,,10
	.p2align 3
.L2607:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2392
	.p2align 4,,10
	.p2align 3
.L2608:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2393
	.p2align 4,,10
	.p2align 3
.L2609:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2394
	.p2align 4,,10
	.p2align 3
.L2610:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2395
	.p2align 4,,10
	.p2align 3
.L2611:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2396
	.p2align 4,,10
	.p2align 3
.L2612:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2613:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2614:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2399
	.p2align 4,,10
	.p2align 3
.L2615:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2616:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2401
	.p2align 4,,10
	.p2align 3
.L2617:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2618:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2619:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2620:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2621:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2622:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2407
	.p2align 4,,10
	.p2align 3
.L2623:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2624:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2625:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2410
	.p2align 4,,10
	.p2align 3
.L2626:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2627:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2628:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2629:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2414
	.p2align 4,,10
	.p2align 3
.L2630:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2415
	.p2align 4,,10
	.p2align 3
.L2631:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2632:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2417
	.p2align 4,,10
	.p2align 3
.L2633:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2418
	.p2align 4,,10
	.p2align 3
.L2634:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2635:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2636:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2637:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2422
	.p2align 4,,10
	.p2align 3
.L2638:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2639:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2640:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2641:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2642:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2427
	.p2align 4,,10
	.p2align 3
.L2643:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2644:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2645:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2646:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2431
	.p2align 4,,10
	.p2align 3
.L2647:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2648:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2649:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2650:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2651:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2652:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2653:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2654:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2439
	.p2align 4,,10
	.p2align 3
.L2655:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2656:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2657:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2442
	.p2align 4,,10
	.p2align 3
.L2658:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2443
	.p2align 4,,10
	.p2align 3
.L2659:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2660:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2661:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2446
	.p2align 4,,10
	.p2align 3
.L2662:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2447
	.p2align 4,,10
	.p2align 3
.L2663:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2448
	.p2align 4,,10
	.p2align 3
.L2664:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2449
	.p2align 4,,10
	.p2align 3
.L2665:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2450
	.p2align 4,,10
	.p2align 3
.L2666:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2667:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2452
	.p2align 4,,10
	.p2align 3
.L2668:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2669:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2454
	.p2align 4,,10
	.p2align 3
.L2670:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2671:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2672:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L2673:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2674:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2675:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2676:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2461
	.p2align 4,,10
	.p2align 3
.L2677:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2462
	.p2align 4,,10
	.p2align 3
.L2678:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L2679:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2680:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L2681:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2682:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2467
	.p2align 4,,10
	.p2align 3
.L2683:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2684:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2685:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2470
	.p2align 4,,10
	.p2align 3
.L2686:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2687:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2688:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2473
	.p2align 4,,10
	.p2align 3
.L2689:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2690:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2691:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2692:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2693:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L2694:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2479
	.p2align 4,,10
	.p2align 3
.L2695:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2696:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L2697:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2698:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2699:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2484
	.p2align 4,,10
	.p2align 3
.L2700:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2485
	.p2align 4,,10
	.p2align 3
.L2701:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2702:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2487
	.p2align 4,,10
	.p2align 3
.L2703:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2704:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L2705:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2706:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2707:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2492
	.p2align 4,,10
	.p2align 3
.L2708:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2709:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2494
	.p2align 4,,10
	.p2align 3
.L2710:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2711:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2712:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2713:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2714:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2715:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2716:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2717:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2718:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2719:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2720:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2721:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L2722:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2507
	.p2align 4,,10
	.p2align 3
.L2723:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2724:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2509
	.p2align 4,,10
	.p2align 3
.L2725:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2510
	.p2align 4,,10
	.p2align 3
.L2726:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2727:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2512
	.p2align 4,,10
	.p2align 3
.L2728:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2513
	.p2align 4,,10
	.p2align 3
.L2729:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2730:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2515
	.p2align 4,,10
	.p2align 3
.L2731:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2516
	.p2align 4,,10
	.p2align 3
.L2732:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2733:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2734:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2519
	.p2align 4,,10
	.p2align 3
.L2735:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2520
	.p2align 4,,10
	.p2align 3
.L2736:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2521
	.p2align 4,,10
	.p2align 3
.L2737:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2522
	.p2align 4,,10
	.p2align 3
.L2738:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2523
	.p2align 4,,10
	.p2align 3
.L2739:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2740:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2525
	.p2align 4,,10
	.p2align 3
.L2741:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2526
	.p2align 4,,10
	.p2align 3
.L2742:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2743:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2744:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2529
	.p2align 4,,10
	.p2align 3
.L2745:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2746:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2747:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2748:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2749:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2534
	.p2align 4,,10
	.p2align 3
.L2750:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2751:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2752:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2753:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2538
	.p2align 4,,10
	.p2align 3
.L2754:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2755:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2540
	.p2align 4,,10
	.p2align 3
.L2756:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2757:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2758:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2759:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2760:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2761:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2546
	.p2align 4,,10
	.p2align 3
.L2762:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2763:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2764:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
.LFE7701:
	.size	_ZN4node19DefineZlibConstantsEN2v85LocalINS0_6ObjectEEE, .-_ZN4node19DefineZlibConstantsEN2v85LocalINS0_6ObjectEEE
	.p2align 4
	.globl	_Z14_register_zlibv
	.type	_Z14_register_zlibv, @function
_Z14_register_zlibv:
.LFB7702:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7702:
	.size	_Z14_register_zlibv, .-_Z14_register_zlibv
	.section	.text._ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB9128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2767
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L2777
.L2793:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L2778:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2767:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2791
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2792
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L2770:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2772
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L2773
	.p2align 4,,10
	.p2align 3
.L2774:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2775:
	testq	%rsi, %rsi
	je	.L2772
.L2773:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L2774
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L2780
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2773
	.p2align 4,,10
	.p2align 3
.L2772:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L2776
	call	_ZdlPv@PLT
.L2776:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L2793
.L2777:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2779
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L2779:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	%rdx, %rdi
	jmp	.L2775
	.p2align 4,,10
	.p2align 3
.L2791:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L2770
.L2792:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9128:
	.size	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc.str1.1,"aMS",@progbits,1
.LC188:
	.string	"wrapped"
.LC189:
	.string	"wrapper"
	.section	.text._ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc,"axG",@progbits,_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	.type	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc, @function
_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc:
.LFB4666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	104(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	divq	%rsi
	movq	96(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2795
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L2797
	.p2align 4,,10
	.p2align 3
.L2849:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2795
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L2795
.L2797:
	cmpq	%rdi, %r12
	jne	.L2849
	movq	16(%rcx), %r15
.L2794:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2850
	addq	$120, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2795:
	.cfi_restore_state
	movl	$72, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rax, (%r15)
	leaq	48(%r15), %rax
	movq	%r12, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rax, -152(%rbp)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	testq	%r12, %r12
	je	.L2851
	movq	(%rbx), %rsi
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testq	%rax, %rax
	je	.L2799
	movq	8(%rbx), %rdi
	leaq	-136(%rbp), %rsi
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	%rax, -136(%rbp)
	call	*%rdx
	movq	%rax, 16(%r15)
.L2799:
	movq	8(%r15), %rsi
	leaq	-96(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	32(%r15), %rdi
	cmpq	%rcx, %rdx
	je	.L2852
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rax
	cmpq	%rdi, -152(%rbp)
	je	.L2853
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	48(%r15), %r8
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r15)
	testq	%rdi, %rdi
	je	.L2805
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L2803:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L2806
	call	_ZdlPv@PLT
.L2806:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, 64(%r15)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2807
	movq	(%rdi), %rax
	call	*8(%rax)
.L2807:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L2808
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2854:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2808
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L2808
.L2810:
	cmpq	%rsi, %r12
	jne	.L2854
	addq	$16, %rcx
.L2814:
	movq	%r15, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2811
	cmpq	72(%rbx), %rax
	je	.L2855
.L2812:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2811
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L2811:
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L2794
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC188(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r15), %rsi
	movq	%r15, %rdx
	leaq	.LC189(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2852:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2801
	cmpq	$1, %rdx
	je	.L2856
	movq	%rcx, %rsi
	movq	%rcx, -152(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	movq	-152(%rbp), %rcx
.L2801:
	movq	%rdx, 40(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L2803
	.p2align 4,,10
	.p2align 3
.L2855:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2812
	.p2align 4,,10
	.p2align 3
.L2853:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%r15)
.L2805:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L2803
	.p2align 4,,10
	.p2align 3
.L2808:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L2814
	.p2align 4,,10
	.p2align 3
.L2851:
	leaq	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2856:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	jmp	.L2801
.L2850:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4666:
	.size	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc, .-_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC190:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB9132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L2866
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L2867
.L2859:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2867:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L2868
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L2869
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2864
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L2864:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L2862:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L2859
	.p2align 4,,10
	.p2align 3
.L2868:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L2861
	cmpq	%r14, %rsi
	je	.L2862
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2862
	.p2align 4,,10
	.p2align 3
.L2861:
	cmpq	%r14, %rsi
	je	.L2862
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2862
.L2866:
	leaq	.LC190(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2869:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9132:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata.str1.1
.LC191:
	.string	"dictionary"
	.section	.text.unlikely
	.align 2
.LCOLDB192:
	.text
.LHOTB192:
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	cmpq	%rax, 48(%rdi)
	je	.L2870
	movq	64(%rsi), %rax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	cmpq	32(%rsi), %rax
	je	.L2872
	cmpq	72(%rsi), %rax
	je	.L2900
.L2873:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L2872
	subq	$24, 64(%rax)
.L2872:
	movl	$72, %edi
	leaq	-48(%rbp), %r14
	call	_Znwm@PLT
	movl	$10, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC191(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r12)
	movq	%r14, %rsi
	movq	$24, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -48(%rbp)
	call	*%rax
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2874
	movq	(%rdi), %rax
	call	*8(%rax)
.L2874:
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L2875
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L2901
.L2876:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2875
	movq	8(%rbx), %rdi
	leaq	.LC191(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rdi
.L2875:
	movq	80(%rbx), %rax
	movq	%r12, -48(%rbp)
	subq	$8, %rax
	cmpq	%rax, %rdi
	je	.L2877
	movq	%r12, (%rdi)
	addq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L2878:
	movq	40(%r13), %rax
	movq	48(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L2879
	cmpq	%rdi, 32(%rbx)
	je	.L2880
	movq	72(%rbx), %rsi
	subq	%rax, %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2885:
	cmpq	%rdi, %rsi
	je	.L2902
	movq	-8(%rdi), %rcx
	addq	$1, %rax
	addq	$1, 64(%rcx)
	cmpq	%rax, %rdx
	jne	.L2885
.L2884:
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L2870:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2903
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2902:
	.cfi_restore_state
	movq	88(%rbx), %rcx
	addq	$1, %rax
	movq	-8(%rcx), %rcx
	movq	504(%rcx), %rcx
	addq	$1, 64(%rcx)
	cmpq	%rdx, %rax
	jne	.L2885
.L2883:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rsi
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L2870
	.p2align 4,,10
	.p2align 3
.L2901:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L2900:
	movq	88(%rsi), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2873
	.p2align 4,,10
	.p2align 3
.L2879:
	cmpq	%rdi, 72(%rbx)
	jne	.L2884
	jmp	.L2883
	.p2align 4,,10
	.p2align 3
.L2877:
	leaq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	64(%rbx), %rdi
	jmp	.L2878
.L2903:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE.cold, @function
_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE.cold:
.LFSB7605:
.L2880:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	64, %rax
	ud2
	.cfi_endproc
.LFE7605:
	.text
	.size	_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text.unlikely
	.size	_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE.cold, .-_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE.cold
.LCOLDE192:
	.text
.LHOTE192:
	.section	.rodata.str1.1
.LC193:
	.string	"compression context"
.LC194:
	.string	"zlib_memory"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB10268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	240(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	104(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rax
	divq	%rsi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2905
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L2960:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2905
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L2905
.L2907:
	cmpq	%rdi, %r13
	jne	.L2960
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2961
	cmpq	72(%rbx), %rax
	je	.L2962
.L2908:
	movq	-8(%rax), %rsi
.L2929:
	leaq	.LC193(%rip), %rcx
	call	*%r8
.L2909:
	movq	232(%r12), %rax
	movq	224(%r12), %r13
	addq	%rax, %r13
	je	.L2904
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$11, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC194(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 64(%r12)
	leaq	-80(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -80(%rbp)
	call	*%rax
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2924
	movq	(%rdi), %rax
	call	*8(%rax)
.L2924:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2904
	cmpq	72(%rbx), %rax
	je	.L2963
.L2926:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2904
	movq	8(%rbx), %rdi
	leaq	.LC194(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L2904:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2964
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2962:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2908
	.p2align 4,,10
	.p2align 3
.L2963:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L2905:
	movq	(%rbx), %rsi
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L2911
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2965:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2911
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%r8, %rdx
	jne	.L2911
.L2912:
	cmpq	%rsi, %r13
	jne	.L2965
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2914
	cmpq	72(%rbx), %rax
	je	.L2966
.L2913:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2914
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	leaq	.LC193(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L2914:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2909
	.p2align 4,,10
	.p2align 3
.L2961:
	xorl	%esi, %esi
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L2911:
	leaq	.LC193(%rip), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	movq	80(%rbx), %rcx
	movq	64(%rbx), %rdx
	movq	%rax, -88(%rbp)
	subq	$8, %rcx
	cmpq	%rcx, %rdx
	je	.L2915
	movq	%rax, (%rdx)
	addq	$8, %rdx
	movq	%rdx, 64(%rbx)
.L2916:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	-88(%rbp), %r15
	call	_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L2932
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L2967
.L2918:
	movq	-8(%rax), %rax
.L2917:
	cmpq	%r15, %rax
	jne	.L2968
	cmpq	$0, 64(%rax)
	je	.L2969
	cmpq	72(%rbx), %rdi
	je	.L2921
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L2914
	.p2align 4,,10
	.p2align 3
.L2966:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2913
.L2967:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2918
.L2915:
	leaq	-88(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L2916
.L2921:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L2914
.L2968:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2969:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2932:
	xorl	%eax, %eax
	jmp	.L2917
.L2964:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10268:
	.size	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB10263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	240(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	104(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rax
	divq	%rsi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2971
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L2973
	.p2align 4,,10
	.p2align 3
.L3026:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2971
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L2971
.L2973:
	cmpq	%rdi, %r13
	jne	.L3026
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3027
	cmpq	72(%rbx), %rax
	je	.L3028
.L2974:
	movq	-8(%rax), %rsi
.L2995:
	leaq	.LC193(%rip), %rcx
	call	*%r8
.L2975:
	movq	232(%r12), %rax
	movq	224(%r12), %r13
	addq	%rax, %r13
	je	.L2970
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$11, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC194(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 64(%r12)
	leaq	-64(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2990
	movq	(%rdi), %rax
	call	*8(%rax)
.L2990:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2970
	cmpq	72(%rbx), %rax
	je	.L3029
.L2992:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2970
	movq	8(%rbx), %rdi
	leaq	.LC194(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L2970:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3030
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3028:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2974
	.p2align 4,,10
	.p2align 3
.L3029:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L2971:
	movq	(%rbx), %rsi
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L2977
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2978
	.p2align 4,,10
	.p2align 3
.L3031:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2977
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%r8, %rdx
	jne	.L2977
.L2978:
	cmpq	%rsi, %r13
	jne	.L3031
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2980
	cmpq	72(%rbx), %rax
	je	.L3032
.L2979:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2980
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	leaq	.LC193(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L2980:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2975
	.p2align 4,,10
	.p2align 3
.L3027:
	xorl	%esi, %esi
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L2977:
	leaq	.LC193(%rip), %rdx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	movq	80(%rbx), %rdx
	movq	64(%rbx), %rdi
	movq	%rax, -72(%rbp)
	subq	$8, %rdx
	cmpq	%rdx, %rdi
	je	.L2981
	movq	%rax, (%rdi)
	addq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L2982:
	movq	-72(%rbp), %rdx
	cmpq	%rdi, 32(%rbx)
	je	.L2998
	movq	%rdi, %rax
	cmpq	%rdi, 72(%rbx)
	je	.L3033
.L2984:
	movq	-8(%rax), %rax
.L2983:
	cmpq	%rdx, %rax
	jne	.L3034
	cmpq	$0, 64(%rax)
	je	.L3035
	cmpq	%rdi, 72(%rbx)
	je	.L2987
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L2980
	.p2align 4,,10
	.p2align 3
.L3032:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2979
.L3033:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2984
.L2981:
	leaq	16(%rbx), %rdi
	leaq	-72(%rbp), %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	64(%rbx), %rdi
	jmp	.L2982
.L2987:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L2980
.L3034:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3035:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2998:
	xorl	%eax, %eax
	jmp	.L2983
.L3030:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10263:
	.size	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB10258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	240(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	104(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rax
	divq	%rsi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3037
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L3039
	.p2align 4,,10
	.p2align 3
.L3092:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3037
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L3037
.L3039:
	cmpq	%rdi, %r13
	jne	.L3092
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3093
	cmpq	72(%rbx), %rax
	je	.L3094
.L3040:
	movq	-8(%rax), %rsi
.L3061:
	leaq	.LC193(%rip), %rcx
	call	*%r8
.L3041:
	movq	232(%r12), %rax
	movq	224(%r12), %r13
	addq	%rax, %r13
	je	.L3036
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$11, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC194(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 64(%r12)
	leaq	-64(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3056
	movq	(%rdi), %rax
	call	*8(%rax)
.L3056:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3036
	cmpq	72(%rbx), %rax
	je	.L3095
.L3058:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3036
	movq	8(%rbx), %rdi
	leaq	.LC194(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3036:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3096
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3094:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3095:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3037:
	movq	(%rbx), %rsi
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L3043
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3044
	.p2align 4,,10
	.p2align 3
.L3097:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3043
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%r8, %rdx
	jne	.L3043
.L3044:
	cmpq	%rsi, %r13
	jne	.L3097
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3046
	cmpq	72(%rbx), %rax
	je	.L3098
.L3045:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3046
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	leaq	.LC193(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3046:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L3041
	.p2align 4,,10
	.p2align 3
.L3093:
	xorl	%esi, %esi
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3043:
	leaq	.LC193(%rip), %rdx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	movq	80(%rbx), %rdx
	movq	64(%rbx), %rdi
	movq	%rax, -72(%rbp)
	subq	$8, %rdx
	cmpq	%rdx, %rdi
	je	.L3047
	movq	%rax, (%rdi)
	addq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3048:
	movq	-72(%rbp), %rdx
	cmpq	%rdi, 32(%rbx)
	je	.L3064
	movq	%rdi, %rax
	cmpq	%rdi, 72(%rbx)
	je	.L3099
.L3050:
	movq	-8(%rax), %rax
.L3049:
	cmpq	%rdx, %rax
	jne	.L3100
	cmpq	$0, 64(%rax)
	je	.L3101
	cmpq	%rdi, 72(%rbx)
	je	.L3053
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L3046
	.p2align 4,,10
	.p2align 3
.L3098:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3045
.L3099:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3050
.L3047:
	leaq	16(%rbx), %rdi
	leaq	-72(%rbp), %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	64(%rbx), %rdi
	jmp	.L3048
.L3053:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3046
.L3100:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3101:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3064:
	xorl	%eax, %eax
	jmp	.L3049
.L3096:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10258:
	.size	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1
.LC195:
	.string	"../src/node_zlib.cc:401"
.LC196:
	.string	"(status) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC197:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::AfterThreadPoolWork(int) [with CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEiE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEiE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEiE4args:
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.section	.rodata.str1.8
	.align 8
.LC198:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::AfterThreadPoolWork(int) [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEiE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEiE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEiE4args:
	.quad	.LC195
	.quad	.LC196
	.quad	.LC198
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::AfterThreadPoolWork(int) [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEiE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEiE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEiE4args:
	.quad	.LC195
	.quad	.LC196
	.quad	.LC199
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node14ThreadPoolWorkE
	.section	.data.rel.ro._ZTVN4node14ThreadPoolWorkE,"awG",@progbits,_ZTVN4node14ThreadPoolWorkE,comdat
	.align 8
	.type	_ZTVN4node14ThreadPoolWorkE, @object
	.size	_ZTVN4node14ThreadPoolWorkE, 48
_ZTVN4node14ThreadPoolWorkE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_111ZlibContextE, @object
	.size	_ZTVN4node12_GLOBAL__N_111ZlibContextE, 72
_ZTVN4node12_GLOBAL__N_111ZlibContextE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_111ZlibContextD1Ev
	.quad	_ZN4node12_GLOBAL__N_111ZlibContextD0Ev
	.quad	_ZNK4node12_GLOBAL__N_111ZlibContext10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_111ZlibContext14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_111ZlibContext8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE, @object
	.size	_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE, 72
_ZTVN4node12_GLOBAL__N_120BrotliEncoderContextE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_120BrotliEncoderContextD1Ev
	.quad	_ZN4node12_GLOBAL__N_120BrotliEncoderContextD0Ev
	.quad	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_120BrotliEncoderContext8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE, @object
	.size	_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE, 72
_ZTVN4node12_GLOBAL__N_120BrotliDecoderContextE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_120BrotliDecoderContextD1Ev
	.quad	_ZN4node12_GLOBAL__N_120BrotliDecoderContextD0Ev
	.quad	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_120BrotliDecoderContext8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE, @object
	.size	_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE, 160
_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_110ZlibStreamE, @object
	.size	_ZTVN4node12_GLOBAL__N_110ZlibStreamE, 160
_ZTVN4node12_GLOBAL__N_110ZlibStreamE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_110ZlibStreamD1Ev
	.quad	_ZN4node12_GLOBAL__N_110ZlibStreamD0Ev
	.quad	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_110ZlibStream14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_110ZlibStream8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD1Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_110ZlibStreamD0Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE16DoThreadPoolWorkEv
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE19AfterThreadPoolWorkEi
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE, @object
	.size	_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE, 160
_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEEE, @object
	.size	_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEEE, 160
_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED1Ev
	.quad	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev
	.quad	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED1Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEED0Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE16DoThreadPoolWorkEv
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE19AfterThreadPoolWorkEi
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE, @object
	.size	_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE, 160
_ZTVN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEEE, @object
	.size	_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEEE, 160
_ZTVN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED1Ev
	.quad	_ZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev
	.quad	_ZNK4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv
	.quad	_ZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED1Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEED0Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE16DoThreadPoolWorkEv
	.quad	_ZThn56_N4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE19AfterThreadPoolWorkEi
	.section	.rodata.str1.1
.LC200:
	.string	"../src/node_zlib.cc:506"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"!(report < 0) || (zlib_memory_ >= static_cast<size_t>(-report))"
	.align 8
.LC202:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::AdjustAmountOfExternalAllocatedMemory() [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args:
	.quad	.LC200
	.quad	.LC201
	.quad	.LC202
	.section	.rodata.str1.1
.LC203:
	.string	"../src/node_zlib.cc:525"
.LC204:
	.string	"(refs_) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC205:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Unref() [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5UnrefEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5UnrefEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5UnrefEvE4args:
	.quad	.LC203
	.quad	.LC204
	.quad	.LC205
	.section	.rodata.str1.1
.LC206:
	.string	"../src/node_zlib.cc:259"
	.section	.rodata.str1.8
	.align 8
.LC207:
	.string	"(unreported_allocations_) == (0)"
	.align 8
.LC208:
	.string	"node::{anonymous}::CompressionStream<CompressionContext>::~CompressionStream() [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_1:
	.quad	.LC206
	.quad	.LC207
	.quad	.LC208
	.section	.rodata.str1.1
.LC209:
	.string	"../src/node_zlib.cc:258"
.LC210:
	.string	"(zlib_memory_) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args_0:
	.quad	.LC209
	.quad	.LC210
	.quad	.LC208
	.section	.rodata.str1.1
.LC211:
	.string	"../src/node_zlib.cc:256"
	.section	.rodata.str1.8
	.align 8
.LC212:
	.string	"(false) == (write_in_progress_ && \"write in progress\")"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEED4EvE4args:
	.quad	.LC211
	.quad	.LC212
	.quad	.LC208
	.section	.rodata.str1.8
	.align 8
.LC213:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::AdjustAmountOfExternalAllocatedMemory() [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args:
	.quad	.LC200
	.quad	.LC201
	.quad	.LC213
	.section	.rodata.str1.8
	.align 8
.LC214:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Unref() [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5UnrefEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5UnrefEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5UnrefEvE4args:
	.quad	.LC203
	.quad	.LC204
	.quad	.LC214
	.section	.rodata.str1.8
	.align 8
.LC215:
	.string	"node::{anonymous}::CompressionStream<CompressionContext>::~CompressionStream() [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_1:
	.quad	.LC206
	.quad	.LC207
	.quad	.LC215
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args_0:
	.quad	.LC209
	.quad	.LC210
	.quad	.LC215
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEED4EvE4args:
	.quad	.LC211
	.quad	.LC212
	.quad	.LC215
	.section	.rodata.str1.8
	.align 8
.LC216:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Unref() [with CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5UnrefEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5UnrefEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5UnrefEvE4args:
	.quad	.LC203
	.quad	.LC204
	.quad	.LC216
	.section	.rodata.str1.1
.LC217:
	.string	"../src/node_zlib.cc:425"
	.section	.rodata.str1.8
	.align 8
.LC218:
	.string	"(env->context()) == (env->isolate()->GetCurrentContext())"
	.align 8
.LC219:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::EmitError(const node::{anonymous}::CompressionError&) [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args:
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.section	.rodata.str1.1
.LC220:
	.string	"../src/node_zlib.cc:270"
	.section	.rodata.str1.8
	.align 8
.LC221:
	.string	"init_done_ && \"close before init\""
	.align 8
.LC222:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Close() [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5CloseEvE4args:
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.section	.rodata.str1.1
.LC223:
	.string	"../src/node_zlib.cc:345"
.LC224:
	.string	"(false) == (pending_close_)"
	.section	.rodata.str1.8
	.align 8
.LC225:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Write(uint32_t, char*, uint32_t, char*, uint32_t) [with bool async = false; CompressionContext = node::{anonymous}::BrotliDecoderContext; uint32_t = unsigned int]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.section	.rodata.str1.1
.LC226:
	.string	"../src/node_zlib.cc:344"
	.section	.rodata.str1.8
	.align 8
.LC227:
	.string	"(false) == (write_in_progress_)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC225
	.section	.rodata.str1.1
.LC228:
	.string	"../src/node_zlib.cc:342"
	.section	.rodata.str1.8
	.align 8
.LC229:
	.string	"!closed_ && \"already finalized\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC225
	.section	.rodata.str1.1
.LC230:
	.string	"../src/node_zlib.cc:341"
	.section	.rodata.str1.8
	.align 8
.LC231:
	.string	"init_done_ && \"write before init\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvjPcjS5_jE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC225
	.section	.rodata.str1.8
	.align 8
.LC232:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Write(uint32_t, char*, uint32_t, char*, uint32_t) [with bool async = true; CompressionContext = node::{anonymous}::BrotliDecoderContext; uint32_t = unsigned int]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC232
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC232
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC232
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvjPcjS5_jE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.section	.rodata.str1.8
	.align 8
.LC233:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::EmitError(const node::{anonymous}::CompressionError&) [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE9EmitErrorERKNS0_16CompressionErrorEE4args:
	.quad	.LC217
	.quad	.LC218
	.quad	.LC233
	.section	.rodata.str1.8
	.align 8
.LC234:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Close() [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5CloseEvE4args:
	.quad	.LC220
	.quad	.LC221
	.quad	.LC234
	.section	.rodata.str1.8
	.align 8
.LC235:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Write(uint32_t, char*, uint32_t, char*, uint32_t) [with bool async = false; CompressionContext = node::{anonymous}::BrotliEncoderContext; uint32_t = unsigned int]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_2:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC235
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_1:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC235
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args_0:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC235
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvjPcjS5_jE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC235
	.section	.rodata.str1.8
	.align 8
.LC236:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Write(uint32_t, char*, uint32_t, char*, uint32_t) [with bool async = true; CompressionContext = node::{anonymous}::BrotliEncoderContext; uint32_t = unsigned int]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_2:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC236
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_1:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC236
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args_0:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC236
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvjPcjS5_jE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC236
	.section	.rodata.str1.8
	.align 8
.LC237:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Write(uint32_t, char*, uint32_t, char*, uint32_t) [with bool async = false; CompressionContext = node::{anonymous}::ZlibContext; uint32_t = unsigned int]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_2:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC237
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_1:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC237
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args_0:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC237
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvjPcjS5_jE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC237
	.section	.rodata.str1.8
	.align 8
.LC238:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Write(uint32_t, char*, uint32_t, char*, uint32_t) [with bool async = true; CompressionContext = node::{anonymous}::ZlibContext; uint32_t = unsigned int]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_2:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC238
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_1:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC238
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args_0:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC238
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvjPcjS5_jE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC238
	.section	.rodata.str1.1
.LC239:
	.string	"../src/node_zlib.cc:693"
.LC240:
	.string	"args[0]->IsUint32Array()"
	.section	.rodata.str1.8
	.align 8
.LC241:
	.string	"static void node::{anonymous}::BrotliCompressionStream<CompressionContext>::Init(const v8::FunctionCallbackInfo<v8::Value>&) [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2:
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.section	.rodata.str1.1
.LC242:
	.string	"../src/node_zlib.cc:677"
.LC243:
	.string	"args[2]->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC242
	.quad	.LC243
	.quad	.LC241
	.section	.rodata.str1.1
.LC244:
	.string	"../src/node_zlib.cc:674"
.LC245:
	.string	"args[1]->IsUint32Array()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC244
	.quad	.LC245
	.quad	.LC241
	.section	.rodata.str1.1
.LC246:
	.string	"../src/node_zlib.cc:672"
	.section	.rodata.str1.8
	.align 8
.LC247:
	.string	"args.Length() == 3 && \"init(params, writeResult, writeCallback)\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC246
	.quad	.LC247
	.quad	.LC241
	.section	.rodata.str1.1
.LC248:
	.string	"../src/node_zlib.cc:326"
	.section	.rodata.str1.8
	.align 8
.LC249:
	.string	"Buffer::IsWithinBounds(out_off, out_len, Buffer::Length(out_buf))"
	.align 8
.LC250:
	.string	"static void node::{anonymous}::CompressionStream<CompressionContext>::Write(const v8::FunctionCallbackInfo<v8::Value>&) [with bool async = false; CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC250
	.section	.rodata.str1.1
.LC251:
	.string	"../src/node_zlib.cc:322"
.LC252:
	.string	"Buffer::HasInstance(args[4])"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4:
	.quad	.LC251
	.quad	.LC252
	.quad	.LC250
	.section	.rodata.str1.1
.LC253:
	.string	"../src/node_zlib.cc:318"
	.section	.rodata.str1.8
	.align 8
.LC254:
	.string	"Buffer::IsWithinBounds(in_off, in_len, Buffer::Length(in_buf))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3:
	.quad	.LC253
	.quad	.LC254
	.quad	.LC250
	.section	.rodata.str1.1
.LC255:
	.string	"../src/node_zlib.cc:313"
.LC256:
	.string	"Buffer::HasInstance(args[1])"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2:
	.quad	.LC255
	.quad	.LC256
	.quad	.LC250
	.section	.rodata.str1.1
.LC257:
	.string	"../src/node_zlib.cc:304"
.LC258:
	.string	"0 && \"Invalid flush value\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1:
	.quad	.LC257
	.quad	.LC258
	.quad	.LC250
	.section	.rodata.str1.1
.LC259:
	.string	"../src/node_zlib.cc:295"
	.section	.rodata.str1.8
	.align 8
.LC260:
	.string	"(false) == (args[0]->IsUndefined() && \"must provide flush value\")"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0:
	.quad	.LC259
	.quad	.LC260
	.quad	.LC250
	.section	.rodata.str1.1
.LC261:
	.string	"../src/node_zlib.cc:289"
.LC262:
	.string	"(args.Length()) == (7)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC250
	.section	.rodata.str1.8
	.align 8
.LC263:
	.string	"static void node::{anonymous}::CompressionStream<CompressionContext>::Write(const v8::FunctionCallbackInfo<v8::Value>&) [with bool async = true; CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC263
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4:
	.quad	.LC251
	.quad	.LC252
	.quad	.LC263
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3:
	.quad	.LC253
	.quad	.LC254
	.quad	.LC263
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2:
	.quad	.LC255
	.quad	.LC256
	.quad	.LC263
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1:
	.quad	.LC257
	.quad	.LC258
	.quad	.LC263
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0:
	.quad	.LC259
	.quad	.LC260
	.quad	.LC263
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliDecoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.section	.rodata.str1.1
.LC264:
	.string	"../src/node_zlib.cc:663"
.LC265:
	.string	"args[0]->IsInt32()"
	.section	.rodata.str1.8
	.align 8
.LC266:
	.string	"static void node::{anonymous}::BrotliCompressionStream<CompressionContext>::New(const v8::FunctionCallbackInfo<v8::Value>&) [with CompressionContext = node::{anonymous}::BrotliDecoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliDecoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.section	.rodata.str1.8
	.align 8
.LC267:
	.string	"static void node::{anonymous}::BrotliCompressionStream<CompressionContext>::Init(const v8::FunctionCallbackInfo<v8::Value>&) [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_2:
	.quad	.LC239
	.quad	.LC240
	.quad	.LC267
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC242
	.quad	.LC243
	.quad	.LC267
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC244
	.quad	.LC245
	.quad	.LC267
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE4InitERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC246
	.quad	.LC247
	.quad	.LC267
	.section	.rodata.str1.8
	.align 8
.LC268:
	.string	"static void node::{anonymous}::CompressionStream<CompressionContext>::Write(const v8::FunctionCallbackInfo<v8::Value>&) [with bool async = false; CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC268
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4:
	.quad	.LC251
	.quad	.LC252
	.quad	.LC268
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3:
	.quad	.LC253
	.quad	.LC254
	.quad	.LC268
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2:
	.quad	.LC255
	.quad	.LC256
	.quad	.LC268
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1:
	.quad	.LC257
	.quad	.LC258
	.quad	.LC268
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0:
	.quad	.LC259
	.quad	.LC260
	.quad	.LC268
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC268
	.section	.rodata.str1.8
	.align 8
.LC269:
	.string	"static void node::{anonymous}::CompressionStream<CompressionContext>::Write(const v8::FunctionCallbackInfo<v8::Value>&) [with bool async = true; CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC269
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4:
	.quad	.LC251
	.quad	.LC252
	.quad	.LC269
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3:
	.quad	.LC253
	.quad	.LC254
	.quad	.LC269
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2:
	.quad	.LC255
	.quad	.LC256
	.quad	.LC269
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1:
	.quad	.LC257
	.quad	.LC258
	.quad	.LC269
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0:
	.quad	.LC259
	.quad	.LC260
	.quad	.LC269
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_20BrotliEncoderContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC269
	.section	.rodata.str1.8
	.align 8
.LC270:
	.string	"static void node::{anonymous}::BrotliCompressionStream<CompressionContext>::New(const v8::FunctionCallbackInfo<v8::Value>&) [with CompressionContext = node::{anonymous}::BrotliEncoderContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_123BrotliCompressionStreamINS0_20BrotliEncoderContextEE3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC264
	.quad	.LC265
	.quad	.LC270
	.section	.rodata.str1.8
	.align 8
.LC271:
	.string	"static void node::{anonymous}::CompressionStream<CompressionContext>::Write(const v8::FunctionCallbackInfo<v8::Value>&) [with bool async = false; CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC271
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4:
	.quad	.LC251
	.quad	.LC252
	.quad	.LC271
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3:
	.quad	.LC253
	.quad	.LC254
	.quad	.LC271
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2:
	.quad	.LC255
	.quad	.LC256
	.quad	.LC271
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1:
	.quad	.LC257
	.quad	.LC258
	.quad	.LC271
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0:
	.quad	.LC259
	.quad	.LC260
	.quad	.LC271
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb0EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC271
	.section	.rodata.str1.8
	.align 8
.LC272:
	.string	"static void node::{anonymous}::CompressionStream<CompressionContext>::Write(const v8::FunctionCallbackInfo<v8::Value>&) [with bool async = true; CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_5:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC272
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_4:
	.quad	.LC251
	.quad	.LC252
	.quad	.LC272
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_3:
	.quad	.LC253
	.quad	.LC254
	.quad	.LC272
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_2:
	.quad	.LC255
	.quad	.LC256
	.quad	.LC272
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_1:
	.quad	.LC257
	.quad	.LC258
	.quad	.LC272
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args_0:
	.quad	.LC259
	.quad	.LC260
	.quad	.LC272
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5WriteILb1EEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC272
	.section	.rodata.str1.8
	.align 8
.LC273:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::AdjustAmountOfExternalAllocatedMemory() [with CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE37AdjustAmountOfExternalAllocatedMemoryEvE4args:
	.quad	.LC200
	.quad	.LC201
	.quad	.LC273
	.section	.rodata.str1.8
	.align 8
.LC274:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::Close() [with CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE5CloseEvE4args:
	.quad	.LC220
	.quad	.LC221
	.quad	.LC274
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC275:
	.string	"../src/util-inl.h:325"
.LC276:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC277:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC275
	.quad	.LC276
	.quad	.LC277
	.section	.rodata.str1.8
	.align 8
.LC278:
	.string	"void node::{anonymous}::CompressionStream<CompressionContext>::EmitError(const node::{anonymous}::CompressionError&) [with CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorEE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEE9EmitErrorERKNS0_16CompressionErrorEE4args:
	.quad	.LC217
	.quad	.LC218
	.quad	.LC278
	.section	.rodata.str1.8
	.align 8
.LC279:
	.string	"node::{anonymous}::CompressionStream<CompressionContext>::~CompressionStream() [with CompressionContext = node::{anonymous}::ZlibContext]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_1, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_1:
	.quad	.LC206
	.quad	.LC207
	.quad	.LC279
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_0, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args_0:
	.quad	.LC209
	.quad	.LC210
	.quad	.LC279
	.align 16
	.type	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args, 24
_ZZN4node12_GLOBAL__N_117CompressionStreamINS0_11ZlibContextEED4EvE4args:
	.quad	.LC211
	.quad	.LC212
	.quad	.LC279
	.section	.rodata.str1.1
.LC280:
	.string	"../src/node_zlib.cc"
.LC281:
	.string	"zlib"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC280
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC281
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC282:
	.string	"../src/node_zlib.cc:1152"
.LC283:
	.string	"state_"
	.section	.rodata.str1.8
	.align 8
.LC284:
	.string	"void node::{anonymous}::BrotliDecoderContext::DoThreadPoolWork()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args_0, 24
_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args_0:
	.quad	.LC282
	.quad	.LC283
	.quad	.LC284
	.section	.rodata.str1.1
.LC285:
	.string	"../src/node_zlib.cc:1151"
.LC286:
	.string	"(mode_) == (BROTLI_DECODE)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args, 24
_ZZN4node12_GLOBAL__N_120BrotliDecoderContext16DoThreadPoolWorkEvE4args:
	.quad	.LC285
	.quad	.LC286
	.quad	.LC284
	.section	.rodata.str1.1
.LC287:
	.string	"../src/node_zlib.cc:1084"
	.section	.rodata.str1.8
	.align 8
.LC288:
	.string	"void node::{anonymous}::BrotliEncoderContext::DoThreadPoolWork()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args_0, 24
_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args_0:
	.quad	.LC287
	.quad	.LC283
	.quad	.LC288
	.section	.rodata.str1.1
.LC289:
	.string	"../src/node_zlib.cc:1083"
.LC290:
	.string	"(mode_) == (BROTLI_ENCODE)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args, 24
_ZZN4node12_GLOBAL__N_120BrotliEncoderContext16DoThreadPoolWorkEvE4args:
	.quad	.LC289
	.quad	.LC290
	.quad	.LC288
	.section	.rodata.str1.1
.LC291:
	.string	"../src/node_zlib.cc:994"
.LC292:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC293:
	.string	"node::{anonymous}::CompressionError node::{anonymous}::ZlibContext::Init(int, int, int, int, std::vector<unsigned char>&&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_3, 24
_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_3:
	.quad	.LC291
	.quad	.LC292
	.quad	.LC293
	.section	.rodata.str1.1
.LC294:
	.string	"../src/node_zlib.cc:950"
	.section	.rodata.str1.8
	.align 8
.LC295:
	.string	"(strategy == 1 || strategy == 2 || strategy == 3 || strategy == 4 || strategy == 0) && \"invalid strategy\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_2, 24
_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_2:
	.quad	.LC294
	.quad	.LC295
	.quad	.LC293
	.section	.rodata.str1.1
.LC296:
	.string	"../src/node_zlib.cc:947"
	.section	.rodata.str1.8
	.align 8
.LC297:
	.string	"(mem_level >= 1 && mem_level <= 9) && \"invalid memlevel\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_1, 24
_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_1:
	.quad	.LC296
	.quad	.LC297
	.quad	.LC293
	.section	.rodata.str1.1
.LC298:
	.string	"../src/node_zlib.cc:944"
	.section	.rodata.str1.8
	.align 8
.LC299:
	.string	"(level >= -1 && level <= 9) && \"invalid compression level\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_0, 24
_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args_0:
	.quad	.LC298
	.quad	.LC299
	.quad	.LC293
	.section	.rodata.str1.1
.LC300:
	.string	"../src/node_zlib.cc:939"
	.section	.rodata.str1.8
	.align 8
.LC301:
	.string	"(window_bits >= 8 && window_bits <= 15) && \"invalid windowBits\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args, 24
_ZZN4node12_GLOBAL__N_111ZlibContext4InitEiiiiOSt6vectorIhSaIhEEE4args:
	.quad	.LC300
	.quad	.LC301
	.quad	.LC293
	.section	.rodata.str1.1
.LC302:
	.string	"../src/node_zlib.cc:839"
	.section	.rodata.str1.8
	.align 8
.LC303:
	.string	"void node::{anonymous}::ZlibContext::DoThreadPoolWork()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args_0, 24
_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args_0:
	.quad	.LC302
	.quad	.LC292
	.quad	.LC303
	.section	.rodata.str1.1
.LC304:
	.string	"../src/node_zlib.cc:796"
	.section	.rodata.str1.8
	.align 8
.LC305:
	.string	"0 && \"invalid number of gzip magic number bytes read\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args, 24
_ZZN4node12_GLOBAL__N_111ZlibContext16DoThreadPoolWorkEvE4args:
	.quad	.LC304
	.quad	.LC305
	.quad	.LC303
	.section	.rodata.str1.1
.LC306:
	.string	"../src/node_zlib.cc:736"
.LC307:
	.string	"status == 0 || status == (-3)"
	.section	.rodata.str1.8
	.align 8
.LC308:
	.string	"void node::{anonymous}::ZlibContext::Close()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0, 24
_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args_0:
	.quad	.LC306
	.quad	.LC307
	.quad	.LC308
	.section	.rodata.str1.1
.LC309:
	.string	"../src/node_zlib.cc:726"
.LC310:
	.string	"(mode_) <= (UNZIP)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args, 24
_ZZN4node12_GLOBAL__N_111ZlibContext5CloseEvE4args:
	.quad	.LC309
	.quad	.LC310
	.quad	.LC308
	.section	.rodata.str1.1
.LC311:
	.string	"../src/node_zlib.cc:627"
	.section	.rodata.str1.8
	.align 8
.LC312:
	.string	"args.Length() == 2 && \"params(level, strategy)\""
	.align 8
.LC313:
	.string	"static void node::{anonymous}::ZlibStream::Params(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_110ZlibStream6ParamsERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC311
	.quad	.LC312
	.quad	.LC313
	.section	.rodata.str1.1
.LC314:
	.string	"../src/node_zlib.cc:600"
.LC315:
	.string	"args[5]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC316:
	.string	"static void node::{anonymous}::ZlibStream::Init(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC314
	.quad	.LC315
	.quad	.LC316
	.section	.rodata.str1.1
.LC317:
	.string	"../src/node_zlib.cc:595"
.LC318:
	.string	"args[4]->IsUint32Array()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC317
	.quad	.LC318
	.quad	.LC316
	.section	.rodata.str1.1
.LC319:
	.string	"../src/node_zlib.cc:571"
	.section	.rodata.str1.8
	.align 8
.LC320:
	.string	"args.Length() == 7 && \"init(windowBits, level, memLevel, strategy, writeResult, writeCallback,\" \" dictionary)\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_110ZlibStream4InitERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC319
	.quad	.LC320
	.quad	.LC316
	.section	.rodata.str1.1
.LC321:
	.string	"../src/node_zlib.cc:553"
	.section	.rodata.str1.8
	.align 8
.LC322:
	.string	"static void node::{anonymous}::ZlibStream::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_110ZlibStream3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC321
	.quad	.LC265
	.quad	.LC322
	.weak	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args
	.section	.rodata.str1.8
	.align 8
.LC323:
	.string	"../src/threadpoolwork-inl.h:46"
	.align 8
.LC324:
	.string	"void node::ThreadPoolWork::ScheduleWork()"
	.section	.data.rel.ro.local._ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args,"awG",@progbits,_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args,comdat
	.align 16
	.type	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args, @gnu_unique_object
	.size	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args, 24
_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args:
	.quad	.LC323
	.quad	.LC196
	.quad	.LC324
	.weak	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC325:
	.string	"../src/node_internals.h:264"
.LC326:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC327:
	.string	"node::ThreadPoolWork::ThreadPoolWork(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args, 24
_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args:
	.quad	.LC325
	.quad	.LC326
	.quad	.LC327
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC328:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC329:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC330:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC328
	.quad	.LC329
	.quad	.LC330
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC331:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC332:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC333:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC331
	.quad	.LC332
	.quad	.LC333
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC334:
	.string	"../src/env-inl.h:399"
.LC335:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC336:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC334
	.quad	.LC335
	.quad	.LC336
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0
	.section	.rodata.str1.8
	.align 8
.LC337:
	.string	"../src/memory_tracker-inl.h:269"
	.section	.rodata.str1.1
.LC338:
	.string	"(n->size_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC339:
	.string	"void node::MemoryTracker::Track(const node::MemoryRetainer*, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0:
	.quad	.LC337
	.quad	.LC338
	.quad	.LC339
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args
	.section	.rodata.str1.8
	.align 8
.LC340:
	.string	"../src/memory_tracker-inl.h:268"
	.section	.rodata.str1.1
.LC341:
	.string	"(CurrentNode()) == (n)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args:
	.quad	.LC340
	.quad	.LC341
	.quad	.LC339
	.weak	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args
	.section	.rodata.str1.8
	.align 8
.LC342:
	.string	"../src/memory_tracker-inl.h:27"
	.section	.rodata.str1.1
.LC343:
	.string	"(retainer_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC344:
	.string	"node::MemoryRetainerNode::MemoryRetainerNode(node::MemoryTracker*, const node::MemoryRetainer*)"
	.section	.data.rel.ro.local._ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,"awG",@progbits,_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,comdat
	.align 16
	.type	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, @gnu_unique_object
	.size	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, 24
_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args:
	.quad	.LC342
	.quad	.LC343
	.quad	.LC344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.quad	7945873032095953474
	.quad	7957653225193369443
	.align 16
.LC12:
	.quad	8017367676180460098
	.quad	8028074750225051757
	.align 16
.LC13:
	.quad	7297073210777891394
	.quad	7957653225193369443
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC16:
	.long	0
	.long	-1074790400
	.align 8
.LC48:
	.long	0
	.long	1072693248
	.align 8
.LC50:
	.long	0
	.long	1073741824
	.align 8
.LC52:
	.long	0
	.long	1074266112
	.align 8
.LC54:
	.long	0
	.long	1074790400
	.align 8
.LC56:
	.long	0
	.long	1075052544
	.align 8
.LC57:
	.long	0
	.long	-1073741824
	.align 8
.LC58:
	.long	0
	.long	-1073217536
	.align 8
.LC59:
	.long	0
	.long	-1072693248
	.align 8
.LC60:
	.long	0
	.long	-1072431104
	.align 8
.LC61:
	.long	0
	.long	-1072168960
	.align 8
.LC65:
	.long	0
	.long	1075970048
	.align 8
.LC73:
	.long	0
	.long	1085452288
	.align 8
.LC80:
	.long	0
	.long	1075314688
	.align 8
.LC82:
	.long	0
	.long	1075576832
	.align 8
.LC84:
	.long	0
	.long	1075838976
	.align 8
.LC88:
	.long	0
	.long	1076756480
	.align 8
.LC91:
	.long	0
	.long	1078984704
	.align 8
.LC93:
	.long	0
	.long	2146435072
	.align 8
.LC95:
	.long	0
	.long	1087373312
	.align 8
.LC114:
	.long	0
	.long	1076232192
	.align 8
.LC118:
	.long	0
	.long	1076101120
	.align 8
.LC120:
	.long	0
	.long	1077411840
	.align 8
.LC122:
	.long	0
	.long	1077805056
	.align 8
.LC124:
	.long	0
	.long	1077280768
	.align 8
.LC127:
	.long	0
	.long	1076887552
	.align 8
.LC151:
	.long	0
	.long	-1071906816
	.align 8
.LC153:
	.long	0
	.long	-1071644672
	.align 8
.LC155:
	.long	0
	.long	-1071513600
	.align 8
.LC157:
	.long	0
	.long	-1071382528
	.align 8
.LC159:
	.long	0
	.long	-1071251456
	.align 8
.LC161:
	.long	0
	.long	-1071120384
	.align 8
.LC163:
	.long	0
	.long	-1070989312
	.align 8
.LC165:
	.long	0
	.long	-1070858240
	.align 8
.LC167:
	.long	0
	.long	-1070727168
	.align 8
.LC169:
	.long	0
	.long	-1070596096
	.align 8
.LC171:
	.long	0
	.long	-1070399488
	.align 8
.LC173:
	.long	0
	.long	-1070333952
	.align 8
.LC175:
	.long	0
	.long	-1070268416
	.align 8
.LC177:
	.long	0
	.long	-1070202880
	.align 8
.LC179:
	.long	0
	.long	-1070006272
	.align 8
.LC181:
	.long	0
	.long	-1069940736
	.align 8
.LC183:
	.long	0
	.long	-1069875200
	.align 8
.LC185:
	.long	0
	.long	-1069678592
	.align 8
.LC187:
	.long	0
	.long	-1069613056
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
