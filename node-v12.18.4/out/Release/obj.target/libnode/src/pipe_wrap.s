	.file	"pipe_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5790:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6068:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6068:
	.size	_ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node8PipeWrap8SelfSizeEv,"axG",@progbits,_ZNK4node8PipeWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8PipeWrap8SelfSizeEv
	.type	_ZNK4node8PipeWrap8SelfSizeEv, @function
_ZNK4node8PipeWrap8SelfSizeEv:
.LFB6070:
	.cfi_startproc
	endbr64
	movl	$424, %eax
	ret
	.cfi_endproc
.LFE6070:
	.size	_ZNK4node8PipeWrap8SelfSizeEv, .-_ZNK4node8PipeWrap8SelfSizeEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9711:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9711:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9715:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9715:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7603:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7603:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7602:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L21:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7602:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i
	.type	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i, @function
_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i:
.LFB9063:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rdx
	leaq	-88(%rdi), %rcx
	subl	$1, 2156(%rdx)
	js	.L27
	jmp	*80(%rcx)
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9063:
	.size	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i, .-_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i
	.section	.text._ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,"axG",@progbits,_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.type	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, @function
_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_:
.LFB7151:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %rdi
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7151:
	.size	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, .-_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1064, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L50
	cmpw	$1040, %cx
	jne	.L30
.L50:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L59
.L33:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L51
	cmpw	$1040, %cx
	jne	.L34
.L51:
	movq	23(%rdx), %r14
.L36:
	testq	%r14, %r14
	je	.L29
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L60
	movq	8(%rbx), %rdi
.L39:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L61
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L41
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
.L42:
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L62
.L43:
	leaq	_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L43
	testl	%eax, %eax
	jg	.L45
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
.L46:
	movq	352(%r13), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movl	$184, %edi
	call	_Znwm@PLT
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	$20, %ecx
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN4node11ConnectWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE@PLT
	cmpq	$0, 80(%r12)
	movq	%r12, 88(%r12)
	leaq	160(%r14), %rsi
	movq	-1088(%rbp), %rdx
	jne	.L63
	movq	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si@GOTPCREL(%rip), %rax
	leaq	88(%r12), %rdi
	leaq	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i(%rip), %rcx
	movq	%rax, 80(%r12)
	call	uv_pipe_connect@PLT
	movq	16(%r12), %rax
	movq	-1088(%rbp), %rdi
	addl	$1, 2156(%rax)
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L29
	testq	%rdi, %rdi
	je	.L29
	call	free@PLT
.L29:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$1064, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %rdx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L45:
	movq	8(%rbx), %r15
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L33
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7670:
	.size	_ZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev:
.LFB6069:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$8, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8097879324678449488, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE6069:
	.size	_ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L67
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L80
.L67:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L81
	movq	8(%rbx), %rdi
.L69:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L82
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L78
	cmpw	$1040, %cx
	jne	.L71
.L78:
	movq	23(%rdx), %r12
.L73:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L74
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L75:
	call	_ZNK2v85Int325ValueEv@PLT
	cmpl	$2, %eax
	ja	.L76
	movl	%eax, %eax
	leaq	CSWTCH.514(%rip), %rdx
	movq	8(%rbx), %r14
	movl	$424, %edi
	movzbl	(%rdx,%rax), %r13d
	leaq	CSWTCH.515(%rip), %rdx
	movl	(%rdx,%rax,4), %r15d
	addq	$8, %r14
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	%r15d, %ecx
	movq	%rax, %rbx
	call	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node8PipeWrapE(%rip), %rax
	movzbl	%r13b, %edx
	leaq	160(%rbx), %rsi
	movq	%rax, (%rbx)
	addq	$208, %rax
	movq	%rax, 88(%rbx)
	movq	360(%r12), %rax
	movq	2360(%rax), %rdi
	call	uv_pipe_init@PLT
	testl	%eax, %eax
	jne	.L83
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$5, 43(%rax)
	jne	.L67
	leaq	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	movq	8(%rbx), %rdi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	_ZZN4node8PipeWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L76:
	leaq	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7643:
	.size	_ZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node8PipeWrapD0Ev,"axG",@progbits,_ZN4node8PipeWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node8PipeWrapD0Ev
	.type	_ZThn88_N4node8PipeWrapD0Ev, @function
_ZThn88_N4node8PipeWrapD0Ev:
.LFB9778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L85
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L86
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L88
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L104:
	cmpq	%rax, %rdx
	je	.L107
.L88:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L104
.L86:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L106:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L108
	movq	%rax, %rbx
.L92:
	testq	%rbx, %rbx
	jne	.L109
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%r12), %rdx
	subq	$88, %r12
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$424, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L107:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L85
	.cfi_endproc
.LFE9778:
	.size	_ZThn88_N4node8PipeWrapD0Ev, .-_ZThn88_N4node8PipeWrapD0Ev
	.section	.text._ZN4node8PipeWrapD2Ev,"axG",@progbits,_ZN4node8PipeWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node8PipeWrapD1Ev
	.type	_ZThn88_N4node8PipeWrapD1Ev, @function
_ZThn88_N4node8PipeWrapD1Ev:
.LFB9771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L111
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L112
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L114
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L130:
	cmpq	%rax, %rdx
	je	.L133
.L114:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L130
.L112:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L132:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L134
	movq	%rax, %rbx
.L118:
	testq	%rbx, %rbx
	jne	.L135
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%r12), %rdx
	leaq	-88(%r12), %rdi
	movq	%rax, -88(%r12)
	movq	-24(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L133:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L111
	.cfi_endproc
.LFE9771:
	.size	_ZThn88_N4node8PipeWrapD1Ev, .-_ZThn88_N4node8PipeWrapD1Ev
	.align 2
	.p2align 4
	.weak	_ZN4node8PipeWrapD2Ev
	.type	_ZN4node8PipeWrapD2Ev, @function
_ZN4node8PipeWrapD2Ev:
.LFB9670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	136(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 128(%rdi)
	testq	%rdx, %rdx
	je	.L137
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L138
	leaq	128(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L140
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L156:
	cmpq	%rax, %rcx
	je	.L159
.L140:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L156
.L138:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	movq	144(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	96(%r12), %rbx
	movq	%rax, 88(%r12)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L161:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	96(%r12), %rax
	cmpq	%rbx, %rax
	je	.L160
	movq	%rax, %rbx
.L144:
	testq	%rbx, %rbx
	jne	.L161
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L159:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L137
	.cfi_endproc
.LFE9670:
	.size	_ZN4node8PipeWrapD2Ev, .-_ZN4node8PipeWrapD2Ev
	.weak	_ZN4node8PipeWrapD1Ev
	.set	_ZN4node8PipeWrapD1Ev,_ZN4node8PipeWrapD2Ev
	.section	.text._ZN4node8PipeWrapD0Ev,"axG",@progbits,_ZN4node8PipeWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8PipeWrapD0Ev
	.type	_ZN4node8PipeWrapD0Ev, @function
_ZN4node8PipeWrapD0Ev:
.LFB9672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	136(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 128(%rdi)
	testq	%rdx, %rdx
	je	.L163
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L164
	leaq	128(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L166
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L182:
	cmpq	%rax, %rcx
	je	.L185
.L166:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L182
.L164:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	movq	144(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	96(%r12), %rbx
	movq	%rax, 88(%r12)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L187:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	96(%r12), %rax
	cmpq	%rbx, %rax
	je	.L186
	movq	%rax, %rbx
.L170:
	testq	%rbx, %rbx
	jne	.L187
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$424, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L185:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L163
	.cfi_endproc
.LFE9672:
	.size	_ZN4node8PipeWrapD0Ev, .-_ZN4node8PipeWrapD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8PipeWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8PipeWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L202
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L197
	cmpw	$1040, %cx
	jne	.L190
.L197:
	movq	23(%rdx), %r12
.L192:
	testq	%r12, %r12
	je	.L188
	movl	16(%rbx), %edx
	movq	16(%r12), %rax
	testl	%edx, %edx
	jle	.L203
	movq	8(%rbx), %rdi
.L195:
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L188
	movq	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si@GOTPCREL(%rip), %rdx
	sarq	$32, %rsi
	leaq	160(%r12), %rdi
	call	uv_listen@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L188:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L202:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7668:
	.size	_ZN4node8PipeWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8PipeWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8PipeWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8PipeWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$1056, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L222
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L214
	cmpw	$1040, %cx
	jne	.L206
.L214:
	movq	23(%rdx), %r12
.L208:
	testq	%r12, %r12
	je	.L204
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movl	16(%rbx), %eax
	leaq	88(%rsi), %rdx
	testl	%eax, %eax
	jg	.L223
.L211:
	leaq	-1072(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %rsi
	leaq	160(%r12), %rdi
	call	uv_pipe_bind@PLT
	movq	(%rbx), %rdx
	movq	-1056(%rbp), %rdi
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L204
	testq	%rdi, %rdi
	je	.L204
	call	free@PLT
.L204:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$1056, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7663:
	.size	_ZN4node8PipeWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8PipeWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L241
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L236
	cmpw	$1040, %cx
	jne	.L227
.L236:
	movq	23(%rdx), %r12
.L229:
	testq	%r12, %r12
	je	.L225
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L242
	movq	8(%rbx), %rdi
.L232:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L243
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L234
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L235:
	call	_ZNK2v85Int325ValueEv@PLT
	leaq	160(%r12), %rdi
	movl	%eax, %esi
	call	uv_pipe_chmod@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L225:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L234:
	movq	8(%rbx), %rdi
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	_ZZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7667:
	.size	_ZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"uv_pipe_open"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node8PipeWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node8PipeWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r13
	movq	32(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L258
	cmpw	$1040, %cx
	jne	.L245
.L258:
	movq	%r13, %rdi
	movq	23(%rdx), %r12
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L264
.L248:
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L259
	cmpw	$1040, %cx
	jne	.L249
.L259:
	movq	23(%rdx), %r13
.L251:
	testq	%r13, %r13
	je	.L244
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L265
	movq	8(%rbx), %rdi
.L255:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L244
	sarq	$32, %rsi
	leaq	160(%r13), %rdi
	call	uv_pipe_open@PLT
	testl	%eax, %eax
	je	.L244
	movq	352(%r12), %r12
	movl	%eax, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	32(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r13
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L248
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L251
	.cfi_endproc
.LFE7669:
	.size	_ZN4node8PipeWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node8PipeWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L267
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L267:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L276
.L268:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L268
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7145:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.1
.LC1:
	.string	"Pipe"
.LC2:
	.string	"bind"
.LC3:
	.string	"listen"
.LC4:
	.string	"connect"
.LC5:
	.string	"open"
.LC6:
	.string	"fchmod"
.LC7:
	.string	"PipeConnectWrap"
.LC8:
	.string	"SOCKET"
.LC10:
	.string	"SERVER"
.LC12:
	.string	"IPC"
.LC14:
	.string	"UV_READABLE"
.LC15:
	.string	"UV_WRITABLE"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB16:
	.text
.LHOTB16:
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L278
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L278
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L278
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L313
.L279:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8PipeWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L314
.L280:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8PipeWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L315
.L281:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L316
.L282:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8PipeWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L317
.L283:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L318
.L284:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L319
.L285:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L320
.L286:
	movq	3192(%rbx), %rdi
	movq	352(%rbx), %r15
	testq	%rdi, %rdi
	je	.L287
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3192(%rbx)
.L287:
	testq	%r12, %r12
	je	.L288
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3192(%rbx)
.L288:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$15, %ecx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L321
.L289:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L322
.L290:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L323
.L291:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L324
.L292:
	movq	%r15, %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L325
.L293:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L326
.L294:
	movq	.LC11(%rip), %rax
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L327
.L295:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC12(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L328
.L296:
	movq	.LC13(%rip), %rax
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L329
.L297:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L330
.L298:
	movq	.LC11(%rip), %rax
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L331
.L299:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC15(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L332
.L300:
	movq	.LC13(%rip), %rax
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L333
.L301:
	movq	360(%rbx), %rax
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	352(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L334
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L320:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L321:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L323:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L325:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L327:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L329:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L331:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L333:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7642:
.L278:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7642:
	.text
	.size	_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE16:
	.text
.LHOTE16:
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE
	.type	_ZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE, @function
_ZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE:
.LFB7638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %r15
	movsd	40(%r12), %xmm0
	movq	1216(%r15), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	je	.L336
	comisd	.LC9(%rip), %xmm0
	jb	.L341
.L336:
	movq	1256(%r15), %rax
	movq	3192(%rbx), %rdi
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -120(%rbp)
	testq	%rdi, %rdi
	je	.L342
	movq	3280(%rbx), %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L343
	movq	352(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%rbx), %rsi
	leaq	-104(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, -104(%rbp)
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movsd	-120(%rbp), %xmm2
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	1256(%r15), %rax
	movsd	%xmm2, 24(%rax)
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L343:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	leaq	_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7638:
	.size	_ZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE, .-_ZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE
	.align 2
	.p2align 4
	.globl	_ZN4node8PipeWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb
	.type	_ZN4node8PipeWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb, @function
_ZN4node8PipeWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb:
.LFB7661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node8PipeWrapE(%rip), %rax
	movzbl	%r12b, %edx
	leaq	160(%rbx), %rsi
	movq	%rax, (%rbx)
	addq	$208, %rax
	movq	%rax, 88(%rbx)
	movq	360(%r13), %rax
	movq	2360(%rax), %rdi
	call	uv_pipe_init@PLT
	testl	%eax, %eax
	jne	.L348
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	leaq	_ZZN4node8PipeWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7661:
	.size	_ZN4node8PipeWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb, .-_ZN4node8PipeWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb
	.globl	_ZN4node8PipeWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb
	.set	_ZN4node8PipeWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb,_ZN4node8PipeWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEb
	.p2align 4
	.globl	_Z19_register_pipe_wrapv
	.type	_Z19_register_pipe_wrapv, @function
_Z19_register_pipe_wrapv:
.LFB7671:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7671:
	.size	_Z19_register_pipe_wrapv, .-_Z19_register_pipe_wrapv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L367
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L368
.L353:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L357
.L350:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L358
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L354:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L353
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L359
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L355:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L357:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L350
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L359:
	xorl	%edx, %edx
	jmp	.L355
	.cfi_endproc
.LFE8376:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L370
	leaq	16(%r12), %rsi
.L371:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L372
	addq	$16, %r12
.L373:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L374
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L390
.L374:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L391
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L378
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L392
.L369:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L393
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L381
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L376:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L378:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L381:
	xorl	%edx, %edx
	jmp	.L376
.L393:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7633:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L395
	leaq	40(%r12), %rsi
.L396:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L397
	addq	$40, %r12
.L398:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L399
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L415
.L399:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L416
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L403
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L417
.L394:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L417:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L416:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L406
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L401:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L403:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L406:
	xorl	%edx, %edx
	jmp	.L401
.L418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7635:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.section	.rodata
	.align 8
	.type	CSWTCH.515, @object
	.size	CSWTCH.515, 12
CSWTCH.515:
	.long	22
	.long	21
	.long	22
	.type	CSWTCH.514, @object
	.size	CSWTCH.514, 3
CSWTCH.514:
	.byte	0
	.byte	0
	.byte	1
	.weak	_ZTVN4node8PipeWrapE
	.section	.data.rel.ro._ZTVN4node8PipeWrapE,"awG",@progbits,_ZTVN4node8PipeWrapE,comdat
	.align 8
	.type	_ZTVN4node8PipeWrapE, @object
	.size	_ZTVN4node8PipeWrapE, 368
_ZTVN4node8PipeWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node8PipeWrapD1Ev
	.quad	_ZN4node8PipeWrapD0Ev
	.quad	_ZNK4node8PipeWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node8PipeWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node8PipeWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node15LibuvStreamWrap5GetFDEv
	.quad	_ZN4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZN4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZN4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZN4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	-88
	.quad	0
	.quad	_ZThn88_N4node8PipeWrapD1Ev
	.quad	_ZThn88_N4node8PipeWrapD0Ev
	.quad	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.quad	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/req_wrap-inl.h:130"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"(req_wrap->original_callback_) == nullptr"
	.align 8
.LC19:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, void (*)"
	.ascii	"(ReqT*, Args"
	.string	" ...)>::For(node::ReqWrap<T>*, node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_connect_s; Args = {int}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_connect_s*, int)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.section	.rodata.str1.1
.LC20:
	.string	"../src/pipe_wrap.cc"
.LC21:
	.string	"pipe_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC20
	.quad	0
	.quad	_ZN4node8PipeWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC21
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC22:
	.string	"../src/pipe_wrap.cc:232"
.LC23:
	.string	"args[1]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"static void node::PipeWrap::Connect(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.section	.rodata.str1.1
.LC25:
	.string	"../src/pipe_wrap.cc:231"
.LC26:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node8PipeWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC24
	.section	.rodata.str1.1
.LC27:
	.string	"../src/pipe_wrap.cc:187"
.LC28:
	.string	"args[0]->IsInt32()"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"static void node::PipeWrap::Fchmod(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node8PipeWrap6FchmodERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/pipe_wrap.cc:159"
.LC31:
	.string	"(r) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"node::PipeWrap::PipeWrap(node::Environment*, v8::Local<v8::Object>, node::AsyncWrap::ProviderType, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEbE4args, @object
	.size	_ZZN4node8PipeWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEbE4args, 24
_ZZN4node8PipeWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEbE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/pipe_wrap.cc:146"
.LC34:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"static void node::PipeWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"../src/pipe_wrap.cc:124"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC36
	.quad	.LC28
	.quad	.LC35
	.section	.rodata.str1.1
.LC37:
	.string	"../src/pipe_wrap.cc:123"
.LC38:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node8PipeWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC35
	.section	.rodata.str1.1
.LC39:
	.string	"../src/pipe_wrap.cc:59"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"(false) == (constructor.IsEmpty())"
	.align 8
.LC41:
	.string	"static v8::MaybeLocal<v8::Object> node::PipeWrap::Instantiate(node::Environment*, node::AsyncWrap*, node::PipeWrap::SocketType)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0, @object
	.size	_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0, 24
_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"../src/pipe_wrap.cc:55"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"(false) == (env->pipe_constructor_template().IsEmpty())"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args, @object
	.size	_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args, 24
_ZZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC41
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC44:
	.string	"../src/stream_base-inl.h:103"
.LC45:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC49:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC50:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC50
	.quad	.LC48
	.quad	.LC51
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC52:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC54:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC55:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC57:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC58:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC60:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC61:
	.string	"../src/env-inl.h:399"
.LC62:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC64:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC66:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC9:
	.long	0
	.long	0
	.align 8
.LC11:
	.long	0
	.long	1072693248
	.align 8
.LC13:
	.long	0
	.long	1073741824
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
