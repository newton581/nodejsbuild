	.file	"signal_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5801:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5801:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_110SignalWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_110SignalWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7534:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7534:
	.size	_ZNK4node12_GLOBAL__N_110SignalWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_110SignalWrap10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_110SignalWrap8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_110SignalWrap8SelfSizeEv:
.LFB7536:
	.cfi_startproc
	endbr64
	movl	$248, %eax
	ret
	.cfi_endproc
.LFE7536:
	.size	_ZNK4node12_GLOBAL__N_110SignalWrap8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_110SignalWrap8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110SignalWrapD2Ev, @function
_ZN4node12_GLOBAL__N_110SignalWrapD2Ev:
.LFB9671:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9671:
	.size	_ZN4node12_GLOBAL__N_110SignalWrapD2Ev, .-_ZN4node12_GLOBAL__N_110SignalWrapD2Ev
	.set	_ZN4node12_GLOBAL__N_110SignalWrapD1Ev,_ZN4node12_GLOBAL__N_110SignalWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110SignalWrapD0Ev, @function
_ZN4node12_GLOBAL__N_110SignalWrapD0Ev:
.LFB9673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdx
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$248, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9673:
	.size	_ZN4node12_GLOBAL__N_110SignalWrapD0Ev, .-_ZN4node12_GLOBAL__N_110SignalWrapD0Ev
	.section	.text._ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev,"axG",@progbits,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.type	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, @function
_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev:
.LFB9709:
	.cfi_startproc
	endbr64
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE9709:
	.size	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, .-_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev
	.set	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.text
	.p2align 4
	.type	_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlP11uv_signal_siE_4_FUNES9_i, @function
_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlP11uv_signal_siE_4_FUNES9_i:
.LFB7549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-88(%rdi), %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%r12), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -88(%rbp)
	movq	360(%rbx), %rax
	movq	1248(%rax), %r15
	testq	%rdi, %rdi
	je	.L10
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L25
.L10:
	movq	3280(%rdx), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L13
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L26
.L13:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	-88(%rbp), %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L13
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7549:
	.size	_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlP11uv_signal_siE_4_FUNES9_i, .-_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlP11uv_signal_siE_4_FUNES9_i
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_110SignalWrap14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_110SignalWrap14MemoryInfoNameEv:
.LFB7535:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$10, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8239173209017510227, %rcx
	movq	%rdx, (%rdi)
	movl	$28769, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE7535:
	.size	_ZNK4node12_GLOBAL__N_110SignalWrap14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_110SignalWrap14MemoryInfoNameEv
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L30
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L37
.L30:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L35
	cmpw	$1040, %cx
	jne	.L31
.L35:
	movq	23(%rdx), %r13
.L33:
	movq	8(%rbx), %r12
	movl	$248, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movl	$27, %r8d
	leaq	88(%rax), %r14
	addq	$8, %r12
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%r14, %rcx
	movq	%r12, %rdx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node12_GLOBAL__N_110SignalWrapE(%rip), %rax
	movb	$0, 240(%rbx)
	movq	%r14, %rsi
	movq	%rax, (%rbx)
	movq	360(%r13), %rax
	movq	2360(%rax), %rdi
	call	uv_signal_init@PLT
	testl	%eax, %eax
	jne	.L38
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L30
	leaq	_ZZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	_ZZN4node12_GLOBAL__N_110SignalWrapC4EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7537:
	.size	_ZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Signal"
.LC1:
	.string	"start"
.LC2:
	.string	"stop"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L40
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L40
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L40
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L50
.L41:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L51
.L42:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_110SignalWrap4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L52
.L43:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L53
.L44:
	movq	3280(%rbx), %rsi
	movq	-56(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L54
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L51:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L52:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7533:
.L40:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7533:
	.text
	.size	_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE3:
	.text
.LHOTE3:
	.p2align 4
	.globl	_ZN4node18HasSignalJSHandlerEi
	.type	_ZN4node18HasSignalJSHandlerEi, @function
_ZN4node18HasSignalJSHandlerEi:
.LFB7553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%edi, %ebx
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	16+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rax
	testq	%rax, %rax
	je	.L61
	leaq	8+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rcx
	movq	%rcx, %rdx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L58
.L57:
	cmpl	%ebx, 32(%rax)
	jge	.L64
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L57
.L58:
	xorl	%r12d, %r12d
	cmpq	%rcx, %rdx
	je	.L56
	cmpl	%ebx, 32(%rdx)
	setle	%r12b
.L56:
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L56
	.cfi_endproc
.LFE7553:
	.size	_ZN4node18HasSignalJSHandlerEi, .-_ZN4node18HasSignalJSHandlerEi
	.p2align 4
	.globl	_Z21_register_signal_wrapv
	.type	_Z21_register_signal_wrapv, @function
_Z21_register_signal_wrapv:
.LFB7554:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7554:
	.size	_Z21_register_signal_wrapv, .-_Z21_register_signal_wrapv
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB8667:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L74
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L68:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L68
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE8667:
	.size	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED2Ev,"axG",@progbits,_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED2Ev
	.type	_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED2Ev, @function
_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED2Ev:
.LFB9684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L77
	movq	%rdi, %r12
.L79:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L79
.L77:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9684:
	.size	_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED2Ev, .-_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED2Ev
	.weak	_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED1Ev
	.set	_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED1Ev,_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED2Ev
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	.type	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_, @function
_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_:
.LFB8690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	leaq	8(%rbx), %rcx
	movq	%rax, %r13
	movq	(%r14), %rax
	movq	$0, 40(%r13)
	movl	(%rax), %r14d
	movl	%r14d, 32(%r13)
	cmpq	%r15, %rcx
	je	.L145
	movq	%r15, %r12
	movl	32(%r15), %r15d
	cmpl	%r15d, %r14d
	jge	.L98
	movq	24(%rbx), %r15
	cmpq	%r12, %r15
	je	.L125
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpl	32(%rax), %r14d
	jle	.L100
	cmpq	$0, 24(%rax)
	je	.L88
.L125:
	movq	%r12, %rax
.L120:
	testq	%rax, %rax
	setne	%al
.L119:
	cmpq	%r12, %rcx
	je	.L133
	testb	%al, %al
	je	.L146
.L133:
	movl	$1, %edi
.L110:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	jle	.L96
	cmpq	%r12, 32(%rbx)
	je	.L142
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpl	32(%rax), %r14d
	jge	.L108
	cmpq	$0, 24(%r12)
	je	.L109
	movq	%rax, %r12
	movl	$1, %edi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rdx, %r12
	testb	%dil, %dil
	jne	.L89
.L94:
	cmpl	%esi, %r14d
	jg	.L97
	movq	%rdx, %r12
.L96:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	cmpq	$0, 40(%rbx)
	je	.L87
	movq	32(%rbx), %rax
	cmpl	32(%rax), %r14d
	jg	.L88
.L87:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L90
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L148:
	movq	16(%rdx), %rax
	movl	$1, %edi
.L93:
	testq	%rax, %rax
	je	.L91
	movq	%rax, %rdx
.L90:
	movl	32(%rdx), %esi
	cmpl	%esi, %r14d
	jl	.L148
	movq	24(%rdx), %rax
	xorl	%edi, %edi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rax, %r12
	xorl	%eax, %eax
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L100:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L102
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L150:
	movq	16(%r12), %rax
	movl	$1, %esi
.L105:
	testq	%rax, %rax
	je	.L103
	movq	%rax, %r12
.L102:
	movl	32(%r12), %edx
	cmpl	%edx, %r14d
	jl	.L150
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L105
.L149:
	movq	%rcx, %r12
.L101:
	cmpq	%r12, %r15
	je	.L128
.L144:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rdi
	movl	32(%rax), %edx
	movq	%rax, %r12
.L116:
	cmpl	%edx, %r14d
	jle	.L96
.L117:
	movq	%rdi, %r12
.L97:
	testq	%r12, %r12
	je	.L96
.L142:
	xorl	%eax, %eax
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L146:
	movl	32(%r12), %r15d
.L109:
	xorl	%edi, %edi
	cmpl	%r15d, %r14d
	setl	%dil
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r15, %rdx
.L89:
	cmpq	%rdx, 24(%rbx)
	je	.L123
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movl	32(%rax), %esi
	movq	%rdx, %r12
	movq	%rax, %rdx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L116
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L108:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L112
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L152:
	movq	16(%r12), %rax
	movl	$1, %esi
.L115:
	testq	%rax, %rax
	je	.L113
	movq	%rax, %r12
.L112:
	movl	32(%r12), %edx
	cmpl	%edx, %r14d
	jl	.L152
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L116
.L111:
	cmpq	%r12, 24(%rbx)
	jne	.L144
	movq	%r12, %rdi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%rdx, %r12
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r15, %rdi
	jmp	.L117
.L151:
	movq	%rcx, %r12
	jmp	.L111
	.cfi_endproc
.LFE8690:
	.size	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_, .-_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"process.on(SIGPROF) is reserved while debugging"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L184
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L178
	cmpw	$1040, %cx
	jne	.L155
.L178:
	movq	23(%rdx), %r12
.L157:
	testq	%r12, %r12
	je	.L153
	movl	16(%rbx), %edx
	movq	16(%r12), %rax
	testl	%edx, %edx
	jle	.L185
	movq	8(%rbx), %rdi
.L160:
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L153
	sarq	$32, %rax
	movl	%eax, -52(%rbp)
	movl	%eax, %edx
	cmpl	$27, %eax
	je	.L186
.L162:
	leaq	88(%r12), %rdi
	leaq	_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlP11uv_signal_siE_4_FUNES9_i(%rip), %rsi
	call	uv_signal_start@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L167
	cmpb	$0, 240(%r12)
	jne	.L187
	movb	$1, 240(%r12)
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	16+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rdx
	testq	%rdx, %rdx
	je	.L177
	leaq	8+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rcx
	movl	-52(%rbp), %eax
	movq	%rcx, %rsi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L171
.L170:
	cmpl	%eax, 32(%rdx)
	jge	.L188
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L170
.L171:
	cmpq	%rcx, %rsi
	je	.L169
	cmpl	32(%rsi), %eax
	jge	.L174
.L169:
	leaq	-52(%rbp), %rax
	leaq	-48(%rbp), %rcx
	leaq	-53(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -48(%rbp)
	leaq	_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	movq	%rax, %rsi
.L174:
	addq	$1, 40(%rsi)
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
.L167:
	movq	(%rbx), %rax
	salq	$32, %r13
	movq	%r13, 24(%rax)
.L153:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L179
	cmpw	$1040, %cx
	jne	.L163
.L179:
	movq	23(%rdx), %rdi
.L165:
	movq	2080(%rdi), %rax
	cmpq	$0, 24(%rax)
	jne	.L166
	movl	-52(%rbp), %edx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	8+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rsi
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L163:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L165
.L189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7546:
	.size	_ZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_,"axG",@progbits,_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_
	.type	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_, @function
_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_:
.LFB8703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	testq	%r15, %r15
	je	.L191
	movl	(%rsi), %ecx
	movq	%r13, %r14
	movq	%r15, %rbx
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L235:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L234
.L212:
	movq	%rax, %rbx
.L192:
	cmpl	%ecx, 32(%rbx)
	jl	.L235
	movq	16(%rbx), %rax
	jle	.L236
	movq	%rbx, %r14
	testq	%rax, %rax
	jne	.L212
.L234:
	movq	40(%r12), %r8
	cmpq	%r14, 24(%r12)
	jne	.L195
	cmpq	%r14, %r13
	je	.L209
.L195:
	xorl	%r8d, %r8d
.L190:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L199
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rdx, %r14
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L205
.L199:
	cmpl	32(%rdx), %ecx
	jl	.L237
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L199
	.p2align 4,,10
	.p2align 3
.L205:
	testq	%rax, %rax
	je	.L200
.L238:
	cmpl	32(%rax), %ecx
	jg	.L204
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L238
.L200:
	movq	40(%r12), %r8
	cmpq	%rbx, 24(%r12)
	jne	.L207
	cmpq	%r14, %r13
	je	.L209
.L207:
	cmpq	%rbx, %r14
	je	.L195
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%rbx, %rdi
	movq	%rbx, %r15
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	movq	40(%r12), %rax
	movq	-56(%rbp), %r8
	subq	$1, %rax
	cmpq	%r14, %rbx
	movq	%rax, 40(%r12)
	jne	.L211
	subq	%rax, %r8
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L204:
	movq	24(%rax), %rax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L209:
	movq	24(%r15), %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r15, %rdi
	movq	16(%r15), %r15
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
	testq	%r15, %r15
	jne	.L209
.L208:
	movq	$0, 16(%r12)
	movq	%r13, 24(%r12)
	movq	%r13, 32(%r12)
	movq	$0, 40(%r12)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L191:
	movq	40(%rdi), %r8
	cmpq	%r13, 24(%rdi)
	jne	.L195
	jmp	.L208
	.cfi_endproc
.LFE8703:
	.size	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_, .-_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_
	.text
	.p2align 4
	.globl	_ZN4node26DecreaseSignalHandlerCountEi
	.type	_ZN4node26DecreaseSignalHandlerCountEi, @function
_ZN4node26DecreaseSignalHandlerCountEi:
.LFB7552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%edi, -36(%rbp)
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	16+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rdx
	testq	%rdx, %rdx
	je	.L249
	leaq	8+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rcx
	movl	-36(%rbp), %eax
	movq	%rcx, %rsi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L242
.L241:
	cmpl	%eax, 32(%rdx)
	jge	.L251
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L241
.L242:
	cmpq	%rcx, %rsi
	je	.L240
	cmpl	32(%rsi), %eax
	jl	.L240
	movq	40(%rsi), %rax
	subq	$1, %rax
	movq	%rax, 40(%rsi)
	testl	%eax, %eax
	js	.L252
.L246:
	je	.L253
.L247:
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	leaq	8+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	-36(%rbp), %rax
	leaq	-16(%rbp), %rcx
	leaq	-17(%rbp), %r8
	leaq	_ZStL19piecewise_construct(%rip), %rdx
	movq	%rax, -16(%rbp)
	leaq	_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS1_EESD_IJEEEEESt17_Rb_tree_iteratorIS2_ESt23_Rb_tree_const_iteratorIS2_EDpOT_
	movq	%rax, %rsi
	movq	40(%rsi), %rax
	subq	$1, %rax
	movq	%rax, 40(%rsi)
	testl	%eax, %eax
	jns	.L246
.L252:
	leaq	_ZZN4node26DecreaseSignalHandlerCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	-36(%rbp), %rsi
	leaq	_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rdi
	call	_ZNSt8_Rb_treeIiSt4pairIKilESt10_Select1stIS2_ESt4lessIiESaIS2_EE5eraseERS1_
	jmp	.L247
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7552:
	.size	_ZN4node26DecreaseSignalHandlerCountEi, .-_ZN4node26DecreaseSignalHandlerCountEi
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110SignalWrap4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110SignalWrap4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L267
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L262
	cmpw	$1040, %cx
	jne	.L257
.L262:
	movq	23(%rdx), %rbx
.L259:
	testq	%rbx, %rbx
	je	.L255
	cmpb	$0, 240(%rbx)
	jne	.L268
.L261:
	leaq	88(%rbx), %rdi
	call	uv_signal_stop@PLT
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L255:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movb	$0, 240(%rbx)
	movl	192(%rbx), %edi
	call	_ZN4node26DecreaseSignalHandlerCountEi
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L257:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7551:
	.size	_ZN4node12_GLOBAL__N_110SignalWrap4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_110SignalWrap4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110SignalWrap5CloseEN2v85LocalINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110SignalWrap5CloseEN2v85LocalINS2_5ValueEEE:
.LFB7545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpb	$0, 240(%rdi)
	jne	.L272
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movl	192(%rdi), %edi
	movq	%rsi, -24(%rbp)
	call	_ZN4node26DecreaseSignalHandlerCountEi
	movq	-24(%rbp), %rsi
	movq	%r12, %rdi
	movb	$0, 240(%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	.cfi_endproc
.LFE7545:
	.size	_ZN4node12_GLOBAL__N_110SignalWrap5CloseEN2v85LocalINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_110SignalWrap5CloseEN2v85LocalINS2_5ValueEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node26DecreaseSignalHandlerCountEi, @function
_GLOBAL__sub_I__ZN4node26DecreaseSignalHandlerCountEi:
.LFB9711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L276
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE(%rip), %rsi
	leaq	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev(%rip), %rdi
	call	__cxa_atexit@PLT
	leaq	8+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip), %rax
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	-8(%rax), %rsi
	leaq	_ZNSt3mapIilSt4lessIiESaISt4pairIKilEEED1Ev(%rip), %rdi
	movl	$0, 8+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip)
	movq	$0, 16+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip)
	movq	%rax, 24+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip)
	movq	%rax, 32+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip)
	movq	$0, 40+_ZN4node12_GLOBAL__N_1L15handled_signalsE(%rip)
	jmp	__cxa_atexit@PLT
.L276:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9711:
	.size	_GLOBAL__sub_I__ZN4node26DecreaseSignalHandlerCountEi, .-_GLOBAL__sub_I__ZN4node26DecreaseSignalHandlerCountEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node26DecreaseSignalHandlerCountEi
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_110SignalWrapE, @object
	.size	_ZTVN4node12_GLOBAL__N_110SignalWrapE, 112
_ZTVN4node12_GLOBAL__N_110SignalWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_110SignalWrapD1Ev
	.quad	_ZN4node12_GLOBAL__N_110SignalWrapD0Ev
	.quad	_ZNK4node12_GLOBAL__N_110SignalWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_110SignalWrap14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_110SignalWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_110SignalWrap5CloseEN2v85LocalINS2_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC5:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC7:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"../src/signal_wrap.cc"
.LC9:
	.string	"signal_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC8
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_110SignalWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC9
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC10:
	.string	"../src/signal_wrap.cc:163"
.LC11:
	.string	"(new_handler_count) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"void node::DecreaseSignalHandlerCount(int)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node26DecreaseSignalHandlerCountEiE4args, @object
	.size	_ZZN4node26DecreaseSignalHandlerCountEiE4args, 24
_ZZN4node26DecreaseSignalHandlerCountEiE4args:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.section	.rodata.str1.1
.LC13:
	.string	"../src/signal_wrap.cc:131"
.LC14:
	.string	"!wrap->active_"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"static void node::{anonymous}::SignalWrap::Start(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_110SignalWrap5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/signal_wrap.cc:91"
.LC17:
	.string	"(r) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"node::{anonymous}::SignalWrap::SignalWrap(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110SignalWrapC4EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110SignalWrapC4EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args, 24
_ZZN4node12_GLOBAL__N_110SignalWrapC4EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"../src/signal_wrap.cc:80"
.LC20:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"static void node::{anonymous}::SignalWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_110SignalWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.local	_ZN4node12_GLOBAL__N_1L15handled_signalsE
	.comm	_ZN4node12_GLOBAL__N_1L15handled_signalsE,48,32
	.local	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE
	.comm	_ZN4node12_GLOBAL__N_1L21handled_signals_mutexE,40,32
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC22:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC24:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
