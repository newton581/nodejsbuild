	.file	"node_buffer.cc"
	.text
	.p2align 4
	.type	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbENUlS3_PvE_4_FUNES3_S4_, @function
_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbENUlS3_PvE_4_FUNES3_S4_:
.LFB7672:
	.cfi_startproc
	endbr64
	jmp	free@PLT
	.cfi_endproc
.LFE7672:
	.size	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbENUlS3_PvE_4_FUNES3_S4_, .-_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbENUlS3_PvE_4_FUNES3_S4_
	.section	.text._ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm,"axG",@progbits,_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm
	.type	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm, @function
_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm:
.LFB6533:
	.cfi_startproc
	endbr64
	lock addq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE6533:
	.size	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm, .-_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm
	.text
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm, @function
_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm:
.LFB7650:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L5
	movq	-1(%rcx), %rsi
	cmpw	$67, 11(%rsi)
	je	.L11
.L5:
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L7
	movl	$1, %eax
	testq	%rdx, %rdx
	js	.L9
	movq	%rdx, (%rbx)
	movl	$257, %eax
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	cmpl	$5, 43(%rcx)
	jne	.L5
	movq	%rdx, (%rbx)
	movl	$257, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$8, %rsp
	xorl	%eax, %eax
	movb	$0, %ah
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7650:
	.size	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm, .-_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	.align 2
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPv, @function
_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPv:
.LFB7647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L14
	movq	32(%r12), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L14
	movq	%rax, %rdi
	call	_ZNK2v811ArrayBuffer12IsDetachableEv@PLT
	testb	%al, %al
	jne	.L55
.L15:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	32(%r12), %rax
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	movq	352(%rax), %rbx
	call	*8(%r12)
	movq	32(%rbx), %rax
	subq	$40, %rax
	movq	%rax, 32(%rbx)
	subq	48(%rbx), %rax
	cmpq	$33554432, %rax
	jg	.L56
.L16:
	movq	40(%rbx), %rax
	subq	$40, %rax
	cmpq	$67108864, %rax
	jg	.L57
.L17:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, (%r12)
.L18:
	movq	32(%r12), %rbx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	2592(%rbx), %rcx
	movq	2584(%rbx), %r13
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	%rdx, %r11
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L26
	movq	(%r8), %rdi
	movq	%r8, %r10
	leaq	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPv(%rip), %r15
	movq	32(%rdi), %rsi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L26
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L26
	movq	%r9, %rdi
.L22:
	cmpq	%rsi, %r12
	jne	.L20
	cmpq	%r15, 8(%rdi)
	jne	.L20
	cmpq	16(%rdi), %r12
	jne	.L20
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L58
	testq	%rsi, %rsi
	je	.L24
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L24
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L24:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	movq	(%r12), %rdi
	subq	$1, 2608(%rbx)
	testq	%rdi, %rdi
	je	.L26
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L26:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	%rax, 40(%rbx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rbx, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r13, %rdi
	call	_ZN2v811ArrayBuffer6DetachEv@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L58:
	testq	%rsi, %rsi
	je	.L30
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L24
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L23:
	leaq	2600(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L60
.L25:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r10, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rsi, 2600(%rbx)
	jmp	.L25
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7647:
	.size	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPv, .-_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPv
	.align 2
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo12WeakCallbackERKN2v816WeakCallbackInfoIS2_EE, @function
_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo12WeakCallbackERKN2v816WeakCallbackInfoIS2_EE:
.LFB7648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	8(%rdi), %r12
	movq	24(%r12), %rsi
	movq	16(%r12), %rdi
	call	*8(%r12)
	movq	32(%rbx), %rax
	subq	$40, %rax
	movq	%rax, 32(%rbx)
	subq	48(%rbx), %rax
	cmpq	$33554432, %rax
	jg	.L93
.L62:
	movq	40(%rbx), %rax
	subq	$40, %rax
	cmpq	$67108864, %rax
	jle	.L63
	movq	%rax, 40(%rbx)
.L63:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, (%r12)
.L64:
	movq	32(%r12), %rbx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	2592(%rbx), %rcx
	movq	2584(%rbx), %r13
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	%rdx, %r11
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L72
	movq	(%r8), %rdi
	movq	%r8, %r10
	leaq	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPv(%rip), %r15
	movq	32(%rdi), %rsi
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L72
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L72
	movq	%r9, %rdi
.L68:
	cmpq	%rsi, %r12
	jne	.L66
	cmpq	%r15, 8(%rdi)
	jne	.L66
	cmpq	16(%rdi), %r12
	jne	.L66
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L94
	testq	%rsi, %rsi
	je	.L70
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L70
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L70:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	movq	(%r12), %rdi
	subq	$1, 2608(%rbx)
	testq	%rdi, %rdi
	je	.L72
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L72:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L75
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L70
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L69:
	leaq	2600(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L95
.L71:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rbx, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r10, %rax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rsi, 2600(%rbx)
	jmp	.L71
	.cfi_endproc
.LFE7648:
	.size	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo12WeakCallbackERKN2v816WeakCallbackInfoIS2_EE, .-_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo12WeakCallbackERKN2v816WeakCallbackInfoIS2_EE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ERR_INVALID_ARG_TYPE"
.LC1:
	.string	"argument must be a buffer"
.LC2:
	.string	"code"
	.text
	.p2align 4
	.type	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0, @function
_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0:
.LFB9959:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L103
.L97:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L104
.L98:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L105
.L99:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L106
.L100:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L107
.L101:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L105:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L106:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L107:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L101
	.cfi_endproc
.LFE9959:
	.size	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0, .-_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L119
	cmpw	$1040, %cx
	jne	.L109
.L119:
	movq	23(%rdx), %r12
.L111:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L112
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L127
.L114:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L128
	movq	8(%rbx), %r13
.L116:
	movq	2744(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L117
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2744(%r12)
.L117:
	testq	%r13, %r13
	je	.L108
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2744(%r12)
.L108:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L112:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L114
.L127:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L111
	.cfi_endproc
.LFE7704:
	.size	_ZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L137
	cmpw	$1040, %cx
	jne	.L130
.L137:
	movq	23(%rdx), %rax
.L132:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L133
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L134:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L139
.L135:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L135
	movq	352(%rax), %rsi
	movq	(%rbx), %rbx
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L130:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L132
	.cfi_endproc
.LFE7685:
	.size	_ZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L154
	cmpw	$1040, %cx
	jne	.L141
.L154:
	movq	23(%rdx), %r12
.L143:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L159
	leaq	_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	movq	8(%rbx), %r13
	movq	0(%r13), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L144
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L144
	movq	352(%r12), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	movslq	%eax, %r14
	movq	360(%r12), %rax
	movl	%r14d, -88(%rbp)
	movq	%r14, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	-88(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L160
	movl	%r8d, %esi
	call	uv_buf_init@PLT
	xorl	%r8d, %r8d
	movl	$-1, %ecx
	movq	%r15, %rsi
	movq	%rax, %r10
	movl	$10, %r9d
	movq	%rdx, -96(%rbp)
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r10, -88(%rbp)
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %r10
	xorl	%ecx, %ecx
	movq	352(%r12), %rdi
	movq	%rdx, -64(%rbp)
	movq	%r10, %rsi
	movq	%r11, %rdx
	movq	%rax, -72(%rbp)
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L161
	movq	(%rax), %rax
.L147:
	movq	%rax, 24(%rdx)
	movq	-72(%rbp), %r13
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-64(%rbp), %r14
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r13, %r13
	je	.L162
	movq	360(%r12), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L162:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	movq	16(%rdx), %rax
	jmp	.L147
	.cfi_endproc
.LFE7702:
	.size	_ZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L182
	cmpw	$1040, %cx
	jne	.L164
.L182:
	movq	23(%rdx), %rax
.L166:
	cmpl	$2, 16(%rbx)
	jg	.L186
	leaq	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L167
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L167
	movq	352(%rax), %rax
	subq	$8, %rdi
	movq	%rax, -120(%rbp)
	call	_ZNK2v85Value12IsUint8ArrayEv@PLT
	testb	%al, %al
	je	.L187
	cmpl	$2, 16(%rbx)
	jg	.L170
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L171:
	call	_ZNK2v85Value13IsUint32ArrayEv@PLT
	testb	%al, %al
	je	.L188
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L173
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	movq	%r13, %r15
.L176:
	movq	%r15, %rdi
	leaq	-112(%rbp), %r12
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %r14
	movq	%r15, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r15, %rdi
	addq	%rax, %r14
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, %rcx
	jg	.L177
	movq	(%rbx), %rax
	movq	8(%rax), %r9
	leaq	88(%r9), %r15
.L178:
	movq	%r15, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r15, %rdi
	movq	-112(%rbp), %rbx
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rsi
	movq	%r14, %rdx
	movl	$10, %r9d
	movq	%r12, %r8
	movq	%r13, %rdi
	addq	%rax, %rbx
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movl	-112(%rbp), %edx
	movl	%eax, 4(%rbx)
	movl	%edx, (%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L189
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L177:
	movq	8(%rbx), %r9
	leaq	-16(%r9), %r15
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L173:
	movq	8(%rbx), %r13
	cmpl	$1, %eax
	je	.L190
	leaq	-8(%r13), %r15
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L189:
	call	__stack_chk_fail@PLT
.L190:
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	jmp	.L176
	.cfi_endproc
.LFE7703:
	.size	_ZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC3:
	.string	"setBufferPrototype"
.LC4:
	.string	"createFromString"
.LC5:
	.string	"byteLengthUtf8"
.LC6:
	.string	"copy"
.LC7:
	.string	"compare"
.LC8:
	.string	"compareOffset"
.LC9:
	.string	"fill"
.LC10:
	.string	"indexOfBuffer"
.LC11:
	.string	"indexOfNumber"
.LC12:
	.string	"indexOfString"
.LC13:
	.string	"swap16"
.LC14:
	.string	"swap32"
.LC15:
	.string	"swap64"
.LC16:
	.string	"encodeInto"
.LC17:
	.string	"encodeUtf8String"
.LC18:
	.string	"kMaxLength"
.LC19:
	.string	"kStringMaxLength"
.LC20:
	.string	"asciiSlice"
.LC21:
	.string	"base64Slice"
.LC22:
	.string	"latin1Slice"
.LC23:
	.string	"hexSlice"
.LC24:
	.string	"ucs2Slice"
.LC25:
	.string	"utf8Slice"
.LC26:
	.string	"asciiWrite"
.LC27:
	.string	"base64Write"
.LC28:
	.string	"latin1Write"
.LC29:
	.string	"hexWrite"
.LC30:
	.string	"ucs2Write"
.LC31:
	.string	"utf8Write"
.LC32:
	.string	"zeroFill"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB33:
	.text
.LHOTB33:
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L192
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L192
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L192
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L289
.L193:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L290
.L194:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L291
.L195:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L292
.L196:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L293
.L197:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L294
.L198:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L295
.L199:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L296
.L200:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L297
.L201:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L298
.L202:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L299
.L203:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L300
.L204:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_17CompareERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L301
.L205:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L302
.L206:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L303
.L207:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L304
.L208:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L305
.L209:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L306
.L210:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L307
.L211:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L308
.L212:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L309
.L213:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L310
.L214:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L311
.L215:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L312
.L216:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L313
.L217:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L314
.L218:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L315
.L219:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L316
.L220:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L317
.L221:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L318
.L222:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L319
.L223:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L320
.L224:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L321
.L225:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L322
.L226:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L323
.L227:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L324
.L228:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L325
.L229:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L326
.L230:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L327
.L231:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L328
.L232:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L329
.L233:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L330
.L234:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L331
.L235:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L332
.L236:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L333
.L237:
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movl	$2147483647, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L334
.L238:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L335
.L239:
	movq	352(%rbx), %rdi
	movl	$1073741799, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16, %ecx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L336
.L240:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L337
.L241:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L338
.L242:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L339
.L243:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L340
.L244:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L341
.L245:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L342
.L246:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L343
.L247:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L344
.L248:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L345
.L249:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L346
.L250:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L347
.L251:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L348
.L252:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L349
.L253:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L350
.L254:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L351
.L255:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L352
.L256:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L353
.L257:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L354
.L258:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L355
.L259:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L356
.L260:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L357
.L261:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L358
.L262:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L359
.L263:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L360
.L264:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L361
.L265:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L362
.L266:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L363
.L267:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L364
.L268:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L365
.L269:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L366
.L270:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L367
.L271:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L368
.L272:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L369
.L273:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L370
.L274:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L371
.L275:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L372
.L276:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L373
.L277:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	360(%rbx), %rax
	movq	2376(%rax), %rsi
	testq	%rsi, %rsi
	je	.L191
	movq	352(%rbx), %rdi
	movl	$1, %ecx
	addq	$8, %rsi
	movl	$4, %edx
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC32(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L374
.L279:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L375
.L280:
	shrw	$8, %ax
	je	.L376
.L191:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L374:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L375:
	movl	%eax, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-56(%rbp), %eax
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L372:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L373:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L368:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L369:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L370:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L371:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L296:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L297:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L298:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L299:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L300:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L301:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L302:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L303:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L304:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L305:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L306:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L307:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L308:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L309:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L289:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L290:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L291:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L292:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L293:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L294:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L295:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L360:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L362:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L364:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L366:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L367:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L344:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L345:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L346:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L347:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L348:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L349:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L350:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L351:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L352:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L353:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L354:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L355:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L356:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L357:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L358:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L359:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L312:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L313:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L314:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L315:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L316:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L317:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L318:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L319:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L320:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L321:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L322:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L323:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L324:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L325:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L326:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L327:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L328:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L329:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L330:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L331:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L332:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L333:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L335:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L337:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L338:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L339:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L340:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L341:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L342:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L343:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L247
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7705:
.L192:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7705:
	.text
	.size	_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE33:
	.text
.LHOTE33:
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L410
	cmpw	$1040, %cx
	jne	.L378
.L410:
	movq	23(%rdx), %r12
.L380:
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L381
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L416
.L383:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L417
	movq	8(%rbx), %rdi
.L391:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L418
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L393
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L394:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	testq	%rax, %rax
	je	.L403
	movq	%r13, %rdx
	addq	-112(%rbp), %rdx
	je	.L419
	movq	%rax, %rcx
	andl	$1, %ecx
	jne	.L420
	leaq	-1(%rax), %rdi
	cmpq	$13, %rdi
	jbe	.L400
	shrq	%rdi
	xorl	%esi, %esi
	addq	$1, %rdi
	movq	%rdi, %r8
	shrq	$3, %r8
	.p2align 4,,10
	.p2align 3
.L402:
	movdqu	(%rdx,%rsi), %xmm0
	addq	$1, %rcx
	movdqa	%xmm0, %xmm1
	psrlw	$8, %xmm0
	psllw	$8, %xmm1
	por	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rsi)
	addq	$16, %rsi
	cmpq	%rcx, %r8
	ja	.L402
	movq	%rdi, %rsi
	andq	$-8, %rsi
	leaq	(%rsi,%rsi), %rcx
	cmpq	%rdi, %rsi
	je	.L403
	leaq	2(%rcx), %rsi
	rolw	$8, (%rdx,%rcx)
	cmpq	%rsi, %rax
	jbe	.L403
	rolw	$8, (%rdx,%rsi)
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %rax
	jbe	.L403
	rolw	$8, (%rdx,%rsi)
	leaq	6(%rcx), %rsi
	cmpq	%rsi, %rax
	jbe	.L403
	rolw	$8, (%rdx,%rsi)
	leaq	8(%rcx), %rsi
	cmpq	%rsi, %rax
	jbe	.L403
	rolw	$8, (%rdx,%rsi)
	leaq	10(%rcx), %rsi
	cmpq	%rsi, %rax
	jbe	.L403
	addq	$12, %rcx
	rolw	$8, (%rdx,%rsi)
	cmpq	%rcx, %rax
	jbe	.L403
	rolw	$8, (%rdx,%rcx)
	.p2align 4,,10
	.p2align 3
.L403:
	movl	16(%rbx), %eax
	movq	(%rbx), %rdx
	testl	%eax, %eax
	jg	.L421
	movq	8(%rdx), %rax
	addq	$88, %rax
.L406:
	testq	%rax, %rax
	je	.L422
	movq	(%rax), %rax
.L408:
	movq	%rax, 24(%rdx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L383
.L416:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L423
.L384:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L424
.L385:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L425
.L386:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L426
.L387:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L427
.L388:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	addq	$88, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	rolw	$8, (%rdx,%rcx)
	addq	$2, %rcx
	cmpq	%rcx, %rax
	jbe	.L403
	rolw	$8, (%rdx,%rcx)
	addq	$2, %rcx
	cmpq	%rcx, %rax
	ja	.L400
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L421:
	movq	8(%rbx), %rax
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L417:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L393:
	movq	8(%rbx), %r12
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L378:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L420:
	leaq	_ZZN4node11SwapBytes16EPcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L422:
	movq	16(%rdx), %rax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L423:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L425:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L426:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L427:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L419:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7699:
	.size	_ZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L456
	cmpw	$1040, %cx
	jne	.L429
.L456:
	movq	23(%rdx), %r12
.L431:
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L432
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L463
.L434:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L464
	movq	8(%rbx), %rdi
.L442:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L465
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L444
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L445:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	testq	%rax, %rax
	je	.L451
	movq	%r12, %rdx
	addq	-112(%rbp), %rdx
	je	.L466
	movq	%rax, %rcx
	andl	$7, %ecx
	jne	.L467
	.p2align 4,,10
	.p2align 3
.L450:
	movq	(%rdx,%rcx), %rsi
	bswap	%rsi
	movq	%rsi, (%rdx,%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	ja	.L450
.L451:
	movl	16(%rbx), %eax
	movq	(%rbx), %rdx
	testl	%eax, %eax
	jg	.L468
	movq	8(%rdx), %rax
	addq	$88, %rax
.L452:
	testq	%rax, %rax
	je	.L469
	movq	(%rax), %rax
.L454:
	movq	%rax, 24(%rdx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L462
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L434
.L463:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L470
.L435:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L471
.L436:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L472
.L437:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L473
.L438:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L474
.L439:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L462
	addq	$88, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L444:
	movq	8(%rbx), %r13
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L468:
	movq	8(%rbx), %rax
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L429:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L467:
	leaq	_ZZN4node11SwapBytes64EPcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L469:
	movq	16(%rdx), %rax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L474:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L470:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L472:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L473:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L462:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7701:
	.size	_ZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L503
	cmpw	$1040, %cx
	jne	.L476
.L503:
	movq	23(%rdx), %r12
.L478:
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L479
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L510
.L481:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L511
	movq	8(%rbx), %rdi
.L489:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L512
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L491
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L492:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	testq	%rax, %rax
	je	.L498
	movq	%r12, %rdx
	addq	-112(%rbp), %rdx
	je	.L513
	movq	%rax, %rcx
	andl	$3, %ecx
	jne	.L514
	.p2align 4,,10
	.p2align 3
.L497:
	movl	(%rdx,%rcx), %esi
	bswap	%esi
	movl	%esi, (%rdx,%rcx)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	ja	.L497
.L498:
	movl	16(%rbx), %eax
	movq	(%rbx), %rdx
	testl	%eax, %eax
	jg	.L515
	movq	8(%rdx), %rax
	addq	$88, %rax
.L499:
	testq	%rax, %rax
	je	.L516
	movq	(%rax), %rax
.L501:
	movq	%rax, 24(%rdx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L509
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L481
.L510:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L517
.L482:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L518
.L483:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L519
.L484:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L520
.L485:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L521
.L486:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L509
	addq	$88, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L491:
	movq	8(%rbx), %r13
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L515:
	movq	8(%rbx), %rax
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L476:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L514:
	leaq	_ZZN4node11SwapBytes32EPcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L516:
	movq	16(%rdx), %rax
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L521:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L517:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L519:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L520:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L509:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7700:
	.size	_ZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_17CompareERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_17CompareERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L566
	cmpw	$1040, %cx
	jne	.L523
.L566:
	movq	23(%rdx), %r12
.L525:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L526
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L527:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L586
	cmpl	$1, 16(%rbx)
	jle	.L587
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L586
.L537:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L543
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L544:
	movq	$0, -160(%rbp)
	movq	%r12, %rdi
	movq	$0, -152(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L551
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -152(%rbp)
	cmpq	$64, %rax
	ja	.L548
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L588
.L548:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-288(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-288(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r13
	movq	%r13, -160(%rbp)
.L547:
	cmpl	$1, 16(%rbx)
	jg	.L549
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L550:
	movq	%r12, %rdi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L551
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	ja	.L554
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L589
.L554:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-288(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-288(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	leaq	0(%r13,%rax), %rsi
	movq	%rsi, -80(%rbp)
.L553:
	movq	-72(%rbp), %r13
	movq	-152(%rbp), %r12
	cmpq	%r12, %r13
	jb	.L590
	testq	%r12, %r12
	jne	.L559
.L560:
	xorl	%edx, %edx
	cmpq	%r12, %r13
	seta	%dl
	negl	%edx
	movslq	%edx, %rdx
	salq	$32, %rdx
.L557:
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L522:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L591
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L537
.L586:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L592
.L538:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L593
.L539:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L594
.L540:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L595
.L541:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L596
.L542:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L526:
	movq	8(%rbx), %rdi
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L543:
	movq	8(%rbx), %r12
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L549:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r12
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-160(%rbp), %rdi
	movq	%r12, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L560
.L562:
	movabsq	$4294967296, %rdx
	testl	%eax, %eax
	movabsq	$-4294967296, %rax
	cmovle	%rax, %rdx
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L523:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	-224(%rbp), %r13
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r13, -160(%rbp)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L589:
	leaq	-144(%rbp), %r13
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r13, -80(%rbp)
	movq	%r13, %rsi
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L590:
	movabsq	$4294967296, %rdx
	testq	%r13, %r13
	je	.L557
	movq	-160(%rbp), %rdi
	movq	%r13, %rdx
	call	memcmp@PLT
	movabsq	$4294967296, %rdx
	testl	%eax, %eax
	je	.L557
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L551:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%rax, -296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-296(%rbp), %rdi
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L594:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L595:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L596:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L592:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L538
.L591:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7688:
	.size	_ZN4node6Buffer12_GLOBAL__N_17CompareERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_17CompareERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	jg	.L598
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L645
.L600:
	cmpl	$2, 16(%rbx)
	jle	.L646
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L602:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L647
	cmpl	$3, 16(%rbx)
	jg	.L604
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L605:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L648
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L607
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L649
.L609:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L614
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L615:
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L650
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -56(%rbp)
	cmpq	$64, %rax
	ja	.L619
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L651
.L619:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-192(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r13
	movq	%r13, -64(%rbp)
.L618:
	cmpl	$1, 16(%rbx)
	jg	.L620
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L621:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, %r13d
	jg	.L622
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L623:
	call	_ZNK2v87Integer5ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movq	%rax, %r12
	jg	.L624
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L625:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	-56(%rbp), %rdx
	testq	%r12, %r12
	js	.L652
	cmpq	%rdx, %r12
	jl	.L627
	testb	%al, %al
	jne	.L628
	movq	%rdx, %r12
	subq	$1, %r12
	jns	.L627
.L628:
	movabsq	$-4294967296, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
.L597:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L653
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movq	8(%rdi), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	jne	.L600
.L645:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L646:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L652:
	addq	%rdx, %r12
	jns	.L627
	testb	%al, %al
	je	.L628
	xorl	%r12d, %r12d
.L627:
	testq	%rdx, %rdx
	je	.L628
	cmpq	%rdx, %r12
	jnb	.L654
	movq	-64(%rbp), %r14
	testb	%al, %al
	jne	.L655
	leaq	1(%r12), %rdx
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	call	memrchr@PLT
.L632:
	movabsq	$-4294967296, %rdx
	movq	%rax, %rcx
	movq	(%rbx), %rsi
	subq	%r14, %rcx
	salq	$32, %rcx
	testq	%rax, %rax
	cmovne	%rcx, %rdx
	movq	%rdx, 24(%rsi)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L604:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L607:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L609
.L649:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L638
	cmpw	$1040, %cx
	jne	.L610
.L638:
	movq	23(%rdx), %rax
.L612:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L614:
	movq	8(%rbx), %r12
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L624:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L622:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L620:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L655:
	subq	%r12, %rdx
	leaq	(%r14,%r12), %rdi
	movl	%r13d, %esi
	call	memchr@PLT
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L651:
	leaq	-128(%rbp), %r13
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r13, -64(%rbp)
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L647:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L648:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L654:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L612
.L653:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7698:
	.size	_ZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata._ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc.str1.1,"aMS",@progbits,1
.LC34:
	.string	"ERR_OUT_OF_RANGE"
	.section	.text._ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc,"axG",@progbits,_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc,comdat
	.p2align 4
	.weak	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	.type	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc, @function
_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc:
.LFB6079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	.LC34(%rip), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L663
.L657:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L664
.L658:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L665
.L659:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L666
.L660:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L667
.L661:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L663:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L665:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L666:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L667:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L661
	.cfi_endproc
.LFE6079:
	.size	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc, .-_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	.section	.rodata.str1.1
.LC35:
	.string	"Index out of range"
	.text
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L712
	cmpw	$1040, %cx
	jne	.L669
.L712:
	movq	23(%rdx), %r13
.L671:
	movq	8(%rbx), %rax
	movq	352(%r13), %r14
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L720
	movq	8(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L721
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	jbe	.L680
.L682:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r15
	movq	%r15, -80(%rbp)
.L681:
	cmpq	$0, -72(%rbp)
	je	.L722
	movq	$0, -216(%rbp)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L723
	movq	8(%rbx), %rdi
.L685:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L686
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L686
	cmpl	$5, 43(%rdx)
	je	.L687
	.p2align 4,,10
	.p2align 3
.L686:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L668
	movl	16(%rbx), %eax
.L687:
	testl	%eax, %eax
	jg	.L724
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L689:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L690
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L690
	cmpl	$5, 43(%rax)
	je	.L691
	.p2align 4,,10
	.p2align 3
.L690:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L692
	testq	%rdx, %rdx
	js	.L719
	movq	%rdx, %r12
.L694:
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L695
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L696:
	leaq	-216(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L668
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L697
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L698:
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L725
.L699:
	shrw	$8, %ax
	je	.L719
	movq	-216(%rbp), %rdx
	cmpq	%r12, %rdx
	jnb	.L701
	movq	%r12, -216(%rbp)
	movq	%r12, %rdx
.L701:
	cmpq	%rdx, -72(%rbp)
	jb	.L719
	movq	-80(%rbp), %rsi
	subq	%r12, %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	leaq	-208(%rbp), %r8
	movq	$0, -208(%rbp)
	addq	%r12, %rsi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L726
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L682
	leaq	-144(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r15, -80(%rbp)
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L720:
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L727
.L673:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L728
.L674:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L729
.L675:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L730
.L676:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L731
.L677:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L668:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L732
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	128(%rdx), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L723:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L669:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L724:
	movq	8(%rbx), %rdi
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L692:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L719:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L695:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L697:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L721:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L691:
	xorl	%r12d, %r12d
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L729:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L730:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L731:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L727:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%rax, -232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %rdi
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L726:
	cmpq	$0, -208(%rbp)
	je	.L733
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L725:
	movl	%eax, -232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-232(%rbp), %eax
	jmp	.L699
.L733:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L732:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8500:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L778
	cmpw	$1040, %cx
	jne	.L735
.L778:
	movq	23(%rdx), %r13
.L737:
	movq	8(%rbx), %rax
	movq	352(%r13), %r14
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L786
	movq	8(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L787
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	jbe	.L746
.L748:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r15
	movq	%r15, -80(%rbp)
.L747:
	cmpq	$0, -72(%rbp)
	je	.L788
	movq	$0, -216(%rbp)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L789
	movq	8(%rbx), %rdi
.L751:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L752
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L752
	cmpl	$5, 43(%rdx)
	je	.L753
	.p2align 4,,10
	.p2align 3
.L752:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L734
	movl	16(%rbx), %eax
.L753:
	testl	%eax, %eax
	jg	.L790
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L755:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L756
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L756
	cmpl	$5, 43(%rax)
	je	.L757
	.p2align 4,,10
	.p2align 3
.L756:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L758
	testq	%rdx, %rdx
	js	.L785
	movq	%rdx, %r12
.L760:
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L761
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L762:
	leaq	-216(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L734
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L763
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L764:
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L791
.L765:
	shrw	$8, %ax
	je	.L785
	movq	-216(%rbp), %rdx
	cmpq	%r12, %rdx
	jnb	.L767
	movq	%r12, -216(%rbp)
	movq	%r12, %rdx
.L767:
	cmpq	%rdx, -72(%rbp)
	jb	.L785
	movq	-80(%rbp), %rsi
	subq	%r12, %rdx
	movl	$3, %ecx
	movq	%r14, %rdi
	leaq	-208(%rbp), %r8
	movq	$0, -208(%rbp)
	addq	%r12, %rsi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L792
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L748
	leaq	-144(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r15, -80(%rbp)
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L786:
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L793
.L739:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L794
.L740:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L795
.L741:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L796
.L742:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L797
.L743:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L734:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L798
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L788:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	128(%rdx), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L789:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L735:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L790:
	movq	8(%rbx), %rdi
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L758:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L785:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L761:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L763:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L787:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L757:
	xorl	%r12d, %r12d
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L795:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L796:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L797:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L793:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L794:
	movq	%rax, -232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %rdi
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L792:
	cmpq	$0, -208(%rbp)
	je	.L799
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L791:
	movl	%eax, -232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-232(%rbp), %eax
	jmp	.L765
.L799:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L798:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8499:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L844
	cmpw	$1040, %cx
	jne	.L801
.L844:
	movq	23(%rdx), %r13
.L803:
	movq	8(%rbx), %rax
	movq	352(%r13), %r14
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L852
	movq	8(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L853
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	jbe	.L812
.L814:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r15
	movq	%r15, -80(%rbp)
.L813:
	cmpq	$0, -72(%rbp)
	je	.L854
	movq	$0, -216(%rbp)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L855
	movq	8(%rbx), %rdi
.L817:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L818
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L818
	cmpl	$5, 43(%rdx)
	je	.L819
	.p2align 4,,10
	.p2align 3
.L818:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L800
	movl	16(%rbx), %eax
.L819:
	testl	%eax, %eax
	jg	.L856
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L821:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L822
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L822
	cmpl	$5, 43(%rax)
	je	.L823
	.p2align 4,,10
	.p2align 3
.L822:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L824
	testq	%rdx, %rdx
	js	.L851
	movq	%rdx, %r12
.L826:
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L827
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L828:
	leaq	-216(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L800
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L829
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L830:
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L857
.L831:
	shrw	$8, %ax
	je	.L851
	movq	-216(%rbp), %rdx
	cmpq	%r12, %rdx
	jnb	.L833
	movq	%r12, -216(%rbp)
	movq	%r12, %rdx
.L833:
	cmpq	%rdx, -72(%rbp)
	jb	.L851
	movq	-80(%rbp), %rsi
	subq	%r12, %rdx
	movl	$5, %ecx
	movq	%r14, %rdi
	leaq	-208(%rbp), %r8
	movq	$0, -208(%rbp)
	addq	%r12, %rsi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L858
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L814
	leaq	-144(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r15, -80(%rbp)
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L852:
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L859
.L805:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L860
.L806:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L861
.L807:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L862
.L808:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L863
.L809:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L800:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L864
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	128(%rdx), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L855:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L801:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L856:
	movq	8(%rbx), %rdi
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L824:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L851:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L827:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L829:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L853:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L823:
	xorl	%r12d, %r12d
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L861:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L862:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L863:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L859:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L860:
	movq	%rax, -232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %rdi
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L858:
	cmpq	$0, -208(%rbp)
	je	.L865
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L857:
	movl	%eax, -232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-232(%rbp), %eax
	jmp	.L831
.L865:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8498:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L910
	cmpw	$1040, %cx
	jne	.L867
.L910:
	movq	23(%rdx), %r13
.L869:
	movq	8(%rbx), %rax
	movq	352(%r13), %r14
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L918
	movq	8(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L919
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	jbe	.L878
.L880:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r15
	movq	%r15, -80(%rbp)
.L879:
	cmpq	$0, -72(%rbp)
	je	.L920
	movq	$0, -216(%rbp)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L921
	movq	8(%rbx), %rdi
.L883:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L884
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L884
	cmpl	$5, 43(%rdx)
	je	.L885
	.p2align 4,,10
	.p2align 3
.L884:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L866
	movl	16(%rbx), %eax
.L885:
	testl	%eax, %eax
	jg	.L922
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L887:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L888
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L888
	cmpl	$5, 43(%rax)
	je	.L889
	.p2align 4,,10
	.p2align 3
.L888:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L890
	testq	%rdx, %rdx
	js	.L917
	movq	%rdx, %r12
.L892:
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L893
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L894:
	leaq	-216(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L866
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L895
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L896:
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L923
.L897:
	shrw	$8, %ax
	je	.L917
	movq	-216(%rbp), %rdx
	cmpq	%r12, %rdx
	jnb	.L899
	movq	%r12, -216(%rbp)
	movq	%r12, %rdx
.L899:
	cmpq	%rdx, -72(%rbp)
	jb	.L917
	movq	-80(%rbp), %rsi
	subq	%r12, %rdx
	movl	$4, %ecx
	movq	%r14, %rdi
	leaq	-208(%rbp), %r8
	movq	$0, -208(%rbp)
	addq	%r12, %rsi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L924
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L878:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L880
	leaq	-144(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r15, -80(%rbp)
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L918:
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L925
.L871:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L926
.L872:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L927
.L873:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L928
.L874:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L929
.L875:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L866:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L930
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	128(%rdx), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L921:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L867:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L922:
	movq	8(%rbx), %rdi
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L890:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L917:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L893:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L895:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L919:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L889:
	xorl	%r12d, %r12d
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L927:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L928:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L929:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L925:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L926:
	movq	%rax, -232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %rdi
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L924:
	cmpq	$0, -208(%rbp)
	je	.L931
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L923:
	movl	%eax, -232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-232(%rbp), %eax
	jmp	.L897
.L931:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L930:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8497:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L970
	cmpw	$1040, %cx
	jne	.L933
.L970:
	movq	23(%rdx), %r13
.L935:
	movq	8(%rbx), %rax
	movq	352(%r13), %r14
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L981
	movq	8(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L982
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	jbe	.L944
.L946:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r15
	movq	%r15, -80(%rbp)
.L945:
	cmpq	$0, -72(%rbp)
	je	.L983
	movq	$0, -224(%rbp)
	movl	16(%rbx), %eax
	movq	$0, -216(%rbp)
	testl	%eax, %eax
	jle	.L984
	movq	8(%rbx), %rdi
.L949:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L950
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L950
	cmpl	$5, 43(%rdx)
	je	.L951
	.p2align 4,,10
	.p2align 3
.L950:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L932
	movl	16(%rbx), %eax
	testq	%rdx, %rdx
	js	.L951
	movq	%rdx, -224(%rbp)
.L951:
	testl	%eax, %eax
	jg	.L985
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L953:
	xorl	%edx, %edx
	leaq	-224(%rbp), %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L986
.L954:
	shrw	$8, %ax
	je	.L980
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L956
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L957:
	leaq	-216(%rbp), %r12
	movq	%r13, %rdi
	movq	%r12, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L932
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L958
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L959:
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L987
.L960:
	shrw	$8, %ax
	je	.L980
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	cmpq	%rsi, %rdx
	jnb	.L962
	movq	%rsi, -216(%rbp)
	movq	%rsi, %rdx
.L962:
	cmpq	%rdx, -72(%rbp)
	jb	.L980
	subq	%rsi, %rdx
	movl	$2, %ecx
	addq	-80(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %r8
	movq	$0, -208(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L988
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L944:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L946
	leaq	-144(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r15, -80(%rbp)
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L981:
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L989
.L937:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L990
.L938:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L991
.L939:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L992
.L940:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L993
.L941:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L932:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L994
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	128(%rdx), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L984:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L933:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L985:
	movq	8(%rbx), %rsi
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L980:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L956:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L958:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L982:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L990:
	movq	%rax, -232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %rdi
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L991:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L992:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L993:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L989:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L986:
	movl	%eax, -232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-232(%rbp), %eax
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L988:
	cmpq	$0, -208(%rbp)
	je	.L995
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L987:
	movl	%eax, -232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-232(%rbp), %eax
	jmp	.L960
.L995:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L994:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8496:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1040
	cmpw	$1040, %cx
	jne	.L997
.L1040:
	movq	23(%rdx), %r13
.L999:
	movq	8(%rbx), %rax
	movq	352(%r13), %r14
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1048
	movq	8(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1049
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	jbe	.L1008
.L1010:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r15
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r15
	movq	%r15, -80(%rbp)
.L1009:
	cmpq	$0, -72(%rbp)
	je	.L1050
	movq	$0, -216(%rbp)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1051
	movq	8(%rbx), %rdi
.L1013:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1014
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1014
	cmpl	$5, 43(%rdx)
	je	.L1015
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L996
	movl	16(%rbx), %eax
.L1015:
	testl	%eax, %eax
	jg	.L1052
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1017:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1018
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1018
	cmpl	$5, 43(%rax)
	je	.L1019
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1020
	testq	%rdx, %rdx
	js	.L1047
	movq	%rdx, %r12
.L1022:
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L1023
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1024:
	leaq	-216(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L996
	cmpl	$1, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L1025
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1026:
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1053
.L1027:
	shrw	$8, %ax
	je	.L1047
	movq	-216(%rbp), %rdx
	cmpq	%r12, %rdx
	jnb	.L1029
	movq	%r12, -216(%rbp)
	movq	%r12, %rdx
.L1029:
	cmpq	%rdx, -72(%rbp)
	jb	.L1047
	movq	-80(%rbp), %rsi
	subq	%r12, %rdx
	xorl	%ecx, %ecx
	leaq	-208(%rbp), %r8
	movq	$0, -208(%rbp)
	movq	%r14, %rdi
	addq	%r12, %rsi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L1054
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L1010
	leaq	-144(%rbp), %r15
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r15, -80(%rbp)
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1055
.L1001:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1056
.L1002:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1057
.L1003:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1058
.L1004:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1059
.L1005:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L996:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1060
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	128(%rdx), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L997:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	8(%rbx), %rdi
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1020:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
.L1047:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1049:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1019:
	xorl	%r12d, %r12d
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1057:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1058:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1059:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1055:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	%rax, -232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %rdi
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1054:
	cmpq	$0, -208(%rbp)
	je	.L1061
	movq	-208(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1053:
	movl	%eax, -232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-232(%rbp), %eax
	jmp	.L1027
.L1061:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8495:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"The value of \"sourceStart\" is out of range."
	.align 8
.LC37:
	.string	"The value of \"targetStart\" is out of range."
	.text
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$288, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1125
	cmpw	$1040, %cx
	jne	.L1063
.L1125:
	movq	23(%rdx), %r13
.L1065:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L1066
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1067:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1146
	cmpl	$1, 16(%rbx)
	jle	.L1150
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1146
.L1072:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1073
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r14
.L1074:
	movq	$0, -144(%rbp)
	movq	%r14, %rdi
	movq	$0, -136(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1081
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -136(%rbp)
	cmpq	$64, %rax
	ja	.L1078
	movq	%r14, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L1151
.L1078:
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-272(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-272(%rbp), %r12
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r12
	movq	%r12, -144(%rbp)
.L1077:
	cmpl	$1, 16(%rbx)
	jg	.L1079
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L1080:
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1081
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -56(%rbp)
	cmpq	$64, %rax
	ja	.L1084
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L1152
.L1084:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-272(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-272(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
.L1083:
	movq	$0, -296(%rbp)
	movl	16(%rbx), %eax
	movq	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	cmpl	$2, %eax
	jg	.L1085
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1086:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1087
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L1153
.L1087:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1062
	movl	16(%rbx), %eax
	testq	%rdx, %rdx
	js	.L1088
	movq	%rdx, -296(%rbp)
.L1088:
	cmpl	$2, %eax
	jle	.L1120
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
.L1090:
	xorl	%edx, %edx
	leaq	-296(%rbp), %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1154
.L1091:
	shrw	$8, %ax
	je	.L1149
	cmpl	$3, 16(%rbx)
	jg	.L1093
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1094:
	leaq	-288(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1062
	cmpl	$3, 16(%rbx)
	jg	.L1096
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1097:
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1155
.L1098:
	shrw	$8, %ax
	je	.L1149
	cmpl	$4, 16(%rbx)
	movq	-56(%rbp), %rdx
	jg	.L1100
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1101:
	leaq	-272(%rbp), %r12
	movq	%r13, %rdi
	movq	%r12, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1062
	cmpl	$4, 16(%rbx)
	movq	-56(%rbp), %rdx
	jle	.L1156
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rsi
.L1103:
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1157
.L1104:
	shrw	$8, %ax
	je	.L1149
	cmpl	$5, 16(%rbx)
	movq	-136(%rbp), %rdx
	jg	.L1106
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1107:
	leaq	-280(%rbp), %r12
	movq	%r13, %rdi
	movq	%r12, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1062
	cmpl	$5, 16(%rbx)
	movq	-136(%rbp), %rdx
	jg	.L1108
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1109:
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1158
.L1110:
	shrw	$8, %ax
	je	.L1149
	movq	-136(%rbp), %rax
	movq	-288(%rbp), %rcx
	cmpq	%rax, %rcx
	ja	.L1159
	movq	-296(%rbp), %rsi
	cmpq	-56(%rbp), %rsi
	ja	.L1160
	movq	-280(%rbp), %r12
	cmpq	%r12, %rcx
	ja	.L1161
	movq	-272(%rbp), %r13
	cmpq	%r13, %rsi
	ja	.L1162
	subq	%rsi, %r13
	subq	%rcx, %rax
	subq	%rcx, %r12
	cmpq	%r13, %rax
	cmova	%r13, %rax
	cmpq	%r12, %rax
	cmova	%r12, %rax
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1116
	addq	-144(%rbp), %rcx
	addq	-64(%rbp), %rsi
	movq	%rcx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L1116
	movabsq	$4294967296, %rax
	movabsq	$-4294967296, %rdx
	cmovle	%rdx, %rax
.L1118:
	movq	(%rbx), %rdx
	movq	%rax, 24(%rdx)
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1163
	addq	$288, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1150:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L1072
.L1146:
	movq	352(%r13), %rdi
	call	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	8(%rbx), %rdi
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	8(%rbx), %r14
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r12
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	8(%rbx), %rcx
	leaq	-16(%rcx), %rdi
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1120:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1153:
	cmpl	$5, 43(%rdx)
	je	.L1088
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1063:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1151:
	leaq	-208(%rbp), %r12
	movl	$64, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r12, -144(%rbp)
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1152:
	leaq	-128(%rbp), %r14
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -64(%rbp)
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1093:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rsi
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1081:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rsi
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rsi
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rsi
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1154:
	movl	%eax, -308(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-308(%rbp), %eax
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rsi
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1155:
	movl	%eax, -308(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-308(%rbp), %eax
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1116:
	movabsq	$4294967296, %rax
	cmpq	%r12, %r13
	jb	.L1118
	seta	%al
	movzbl	%al, %eax
	negl	%eax
	cltq
	salq	$32, %rax
	jmp	.L1118
.L1157:
	movl	%eax, -308(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-308(%rbp), %eax
	jmp	.L1104
.L1159:
	movq	352(%r13), %rdi
	leaq	.LC36(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1062
.L1160:
	movq	352(%r13), %rdi
	leaq	.LC37(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1062
.L1158:
	movl	%eax, -308(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-308(%rbp), %eax
	jmp	.L1110
.L1161:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1162:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7687:
	.size	_ZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC38:
	.string	"argument must be a string"
.LC39:
	.string	"ERR_BUFFER_OUT_OF_BOUNDS"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"\"offset\" is outside of buffer bounds"
	.text
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1210
	cmpw	$1040, %cx
	jne	.L1165
.L1210:
	movq	23(%rdx), %r14
.L1167:
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1238
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1239
	movq	8(%rbx), %rax
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r15
	addq	-112(%rbp), %r13
	jne	.L1176
	testq	%rax, %rax
	jne	.L1240
.L1176:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1241
	movq	8(%rbx), %rdi
.L1178:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1179
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1180
.L1179:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1242
.L1181:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
.L1237:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1243
.L1182:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
.L1233:
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1244
.L1197:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1245
.L1198:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1246
.L1199:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L1164:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1247
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1241:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1248
.L1169:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1249
.L1186:
	cmpl	$1, 16(%rbx)
	jg	.L1187
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1188:
	leaq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1164
	cmpl	$1, 16(%rbx)
	movq	-136(%rbp), %rcx
	jle	.L1250
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
.L1191:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1251
.L1192:
	shrw	$8, %ax
	je	.L1224
	movq	-128(%rbp), %rax
	cmpq	%r15, %rax
	ja	.L1252
	movq	%r15, %rdx
	subq	%rax, %rdx
	cmpl	$2, 16(%rbx)
	jg	.L1200
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1201:
	leaq	-120(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1164
	movq	%r15, %rdx
	subq	-128(%rbp), %rdx
	cmpl	$2, 16(%rbx)
	movq	-136(%rbp), %rcx
	jg	.L1202
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1203:
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1253
.L1204:
	shrw	$8, %ax
	je	.L1224
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%r15, %rdx
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L1206
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1165:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1245:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1244:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1246:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1239:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1240:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1242:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1248:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	352(%r14), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	352(%r14), %rdi
	addq	%r13, %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movl	$1, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	(%rbx), %rbx
	testl	%eax, %eax
	js	.L1207
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1254
.L1195:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1255
.L1196:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1249:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1186
.L1251:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1192
.L1253:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1204
.L1255:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1196
.L1254:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1195
.L1207:
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L1256
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1164
.L1247:
	call	__stack_chk_fail@PLT
.L1256:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1164
	.cfi_endproc
.LFE8506:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1303
	cmpw	$1040, %cx
	jne	.L1258
.L1303:
	movq	23(%rdx), %r14
.L1260:
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1331
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1332
	movq	8(%rbx), %rax
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r15
	addq	-112(%rbp), %r13
	jne	.L1269
	testq	%rax, %rax
	jne	.L1333
.L1269:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1334
	movq	8(%rbx), %rdi
.L1271:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1272
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1273
.L1272:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1335
.L1274:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
.L1330:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1336
.L1275:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
.L1326:
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1337
.L1290:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1338
.L1291:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1339
.L1292:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L1257:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1340
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1334:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1331:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1341
.L1262:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1342
.L1279:
	cmpl	$1, 16(%rbx)
	jg	.L1280
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1281:
	leaq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1257
	cmpl	$1, 16(%rbx)
	movq	-136(%rbp), %rcx
	jle	.L1343
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
.L1284:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1344
.L1285:
	shrw	$8, %ax
	je	.L1317
	movq	-128(%rbp), %rax
	cmpq	%r15, %rax
	ja	.L1345
	movq	%r15, %rdx
	subq	%rax, %rdx
	cmpl	$2, 16(%rbx)
	jg	.L1293
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1294:
	leaq	-120(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1257
	movq	%r15, %rdx
	subq	-128(%rbp), %rdx
	cmpl	$2, 16(%rbx)
	movq	-136(%rbp), %rcx
	jg	.L1295
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1296:
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1346
.L1297:
	shrw	$8, %ax
	je	.L1317
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%r15, %rdx
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L1299
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1258:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1338:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1337:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1339:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1332:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1343:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1333:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1335:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1341:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	352(%r14), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	352(%r14), %rdi
	addq	%r13, %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movl	$3, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	(%rbx), %rbx
	testl	%eax, %eax
	js	.L1300
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1345:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1347
.L1288:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1348
.L1289:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1342:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1279
.L1344:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1285
.L1346:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1297
.L1348:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1289
.L1347:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1288
.L1300:
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L1349
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1257
.L1340:
	call	__stack_chk_fail@PLT
.L1349:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1257
	.cfi_endproc
.LFE8505:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1396
	cmpw	$1040, %cx
	jne	.L1351
.L1396:
	movq	23(%rdx), %r14
.L1353:
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1424
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1425
	movq	8(%rbx), %rax
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r15
	addq	-112(%rbp), %r13
	jne	.L1362
	testq	%rax, %rax
	jne	.L1426
.L1362:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1427
	movq	8(%rbx), %rdi
.L1364:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1365
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1366
.L1365:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1428
.L1367:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
.L1423:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1429
.L1368:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
.L1419:
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1430
.L1383:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1431
.L1384:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1432
.L1385:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L1350:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1433
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1427:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1434
.L1355:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1435
.L1372:
	cmpl	$1, 16(%rbx)
	jg	.L1373
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1374:
	leaq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1350
	cmpl	$1, 16(%rbx)
	movq	-136(%rbp), %rcx
	jle	.L1436
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
.L1377:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1437
.L1378:
	shrw	$8, %ax
	je	.L1410
	movq	-128(%rbp), %rax
	cmpq	%r15, %rax
	ja	.L1438
	movq	%r15, %rdx
	subq	%rax, %rdx
	cmpl	$2, 16(%rbx)
	jg	.L1386
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1387:
	leaq	-120(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1350
	movq	%r15, %rdx
	subq	-128(%rbp), %rdx
	cmpl	$2, 16(%rbx)
	movq	-136(%rbp), %rcx
	jg	.L1388
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1389:
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1439
.L1390:
	shrw	$8, %ax
	je	.L1410
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%r15, %rdx
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L1392
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1351:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1431:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1384
	.p2align 4,,10
	.p2align 3
.L1430:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1432:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1425:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1426:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1428:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1434:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	352(%r14), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	352(%r14), %rdi
	addq	%r13, %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movl	$5, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	(%rbx), %rbx
	testl	%eax, %eax
	js	.L1393
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1440
.L1381:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1441
.L1382:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1435:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1372
.L1437:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1378
.L1439:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1390
.L1441:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1382
.L1440:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1381
.L1393:
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L1442
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1350
.L1433:
	call	__stack_chk_fail@PLT
.L1442:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1350
	.cfi_endproc
.LFE8504:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1489
	cmpw	$1040, %cx
	jne	.L1444
.L1489:
	movq	23(%rdx), %r14
.L1446:
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1517
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1518
	movq	8(%rbx), %rax
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r15
	addq	-112(%rbp), %r13
	jne	.L1455
	testq	%rax, %rax
	jne	.L1519
.L1455:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1520
	movq	8(%rbx), %rdi
.L1457:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1458
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1459
.L1458:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1521
.L1460:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
.L1516:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1522
.L1461:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
.L1512:
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1523
.L1476:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1524
.L1477:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1525
.L1478:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L1443:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1526
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1520:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1527
.L1448:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1528
.L1465:
	cmpl	$1, 16(%rbx)
	jg	.L1466
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1467:
	leaq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1443
	cmpl	$1, 16(%rbx)
	movq	-136(%rbp), %rcx
	jle	.L1529
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
.L1470:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1530
.L1471:
	shrw	$8, %ax
	je	.L1503
	movq	-128(%rbp), %rax
	cmpq	%r15, %rax
	ja	.L1531
	movq	%r15, %rdx
	subq	%rax, %rdx
	cmpl	$2, 16(%rbx)
	jg	.L1479
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1480:
	leaq	-120(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1443
	movq	%r15, %rdx
	subq	-128(%rbp), %rdx
	cmpl	$2, 16(%rbx)
	movq	-136(%rbp), %rcx
	jg	.L1481
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1482:
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1532
.L1483:
	shrw	$8, %ax
	je	.L1503
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%r15, %rdx
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L1485
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1444:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1524:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1523:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1525:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1522:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1518:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1519:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1521:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1527:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	352(%r14), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	352(%r14), %rdi
	addq	%r13, %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movl	$4, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	(%rbx), %rbx
	testl	%eax, %eax
	js	.L1486
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1531:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1533
.L1474:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1534
.L1475:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1528:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1465
.L1530:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1471
.L1532:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1483
.L1534:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1475
.L1533:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1474
.L1486:
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L1535
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1443
.L1526:
	call	__stack_chk_fail@PLT
.L1535:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1443
	.cfi_endproc
.LFE8503:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1582
	cmpw	$1040, %cx
	jne	.L1537
.L1582:
	movq	23(%rdx), %r14
.L1539:
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1610
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1611
	movq	8(%rbx), %rax
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r15
	addq	-112(%rbp), %r13
	jne	.L1548
	testq	%rax, %rax
	jne	.L1612
.L1548:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1613
	movq	8(%rbx), %rdi
.L1550:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1551
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1552
.L1551:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1614
.L1553:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
.L1609:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1615
.L1554:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
.L1605:
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1616
.L1569:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1617
.L1570:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1618
.L1571:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L1536:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1619
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1613:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1610:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1620
.L1541:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1621
.L1558:
	cmpl	$1, 16(%rbx)
	jg	.L1559
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1560:
	leaq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1536
	cmpl	$1, 16(%rbx)
	movq	-136(%rbp), %rcx
	jle	.L1622
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
.L1563:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1623
.L1564:
	shrw	$8, %ax
	je	.L1596
	movq	-128(%rbp), %rax
	cmpq	%r15, %rax
	ja	.L1624
	movq	%r15, %rdx
	subq	%rax, %rdx
	cmpl	$2, 16(%rbx)
	jg	.L1572
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1573:
	leaq	-120(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1536
	movq	%r15, %rdx
	subq	-128(%rbp), %rdx
	cmpl	$2, 16(%rbx)
	movq	-136(%rbp), %rcx
	jg	.L1574
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1575:
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1625
.L1576:
	shrw	$8, %ax
	je	.L1596
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%r15, %rdx
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L1578
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1537:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1617:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1616:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1618:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1611:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1612:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1614:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1620:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	352(%r14), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	352(%r14), %rdi
	addq	%r13, %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movl	$2, %r8d
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	(%rbx), %rbx
	testl	%eax, %eax
	js	.L1579
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1626
.L1567:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1627
.L1568:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1621:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1558
.L1623:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1564
.L1625:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1576
.L1627:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1568
.L1626:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1567
.L1579:
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L1628
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1536
.L1619:
	call	__stack_chk_fail@PLT
.L1628:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1536
	.cfi_endproc
.LFE8502:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1675
	cmpw	$1040, %cx
	jne	.L1630
.L1675:
	movq	23(%rdx), %r14
.L1632:
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1703
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1704
	movq	8(%rbx), %rax
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r15
	addq	-112(%rbp), %r13
	jne	.L1641
	testq	%rax, %rax
	jne	.L1705
.L1641:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1706
	movq	8(%rbx), %rdi
.L1643:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1644
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1645
.L1644:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1707
.L1646:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
.L1702:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1708
.L1647:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
.L1698:
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1709
.L1662:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1710
.L1663:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1711
.L1664:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L1629:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1712
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1706:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1713
.L1634:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1714
.L1651:
	cmpl	$1, 16(%rbx)
	jg	.L1652
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1653:
	leaq	-128(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1629
	cmpl	$1, 16(%rbx)
	movq	-136(%rbp), %rcx
	jle	.L1715
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
.L1656:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1716
.L1657:
	shrw	$8, %ax
	je	.L1689
	movq	-128(%rbp), %rax
	cmpq	%r15, %rax
	ja	.L1717
	movq	%r15, %rdx
	subq	%rax, %rdx
	cmpl	$2, 16(%rbx)
	jg	.L1665
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1666:
	leaq	-120(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1629
	movq	%r15, %rdx
	subq	-128(%rbp), %rdx
	cmpl	$2, 16(%rbx)
	movq	-136(%rbp), %rcx
	jg	.L1667
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1668:
	movq	%r14, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1718
.L1669:
	shrw	$8, %ax
	je	.L1689
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	movq	%r15, %rdx
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -120(%rbp)
	testq	%rdx, %rdx
	jne	.L1671
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1630:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1710:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1709:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1662
	.p2align 4,,10
	.p2align 3
.L1711:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1704:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1705:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1707:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1713:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1689:
	movq	352(%r14), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1671:
	movq	352(%r14), %rdi
	addq	%r13, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	(%rbx), %rbx
	testl	%eax, %eax
	js	.L1672
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1717:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1719
.L1660:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1720
.L1661:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1714:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1651
.L1716:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1657
.L1718:
	movl	%eax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-136(%rbp), %eax
	jmp	.L1669
.L1720:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L1661
.L1719:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1660
.L1672:
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L1721
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1629
.L1712:
	call	__stack_chk_fail@PLT
.L1721:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1629
	.cfi_endproc
.LFE8501:
	.size	_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1773
	cmpw	$1040, %cx
	jne	.L1723
.L1773:
	movq	23(%rdx), %r13
.L1725:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L1726
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1727:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1791
	cmpl	$1, 16(%rbx)
	jle	.L1794
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1791
.L1732:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1733
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L1734:
	movq	%r12, %rdi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1795
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	ja	.L1738
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L1796
.L1738:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r14
	movq	%r14, -80(%rbp)
.L1737:
	cmpl	$1, 16(%rbx)
	jg	.L1739
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L1740:
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1797
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r12
	addq	-208(%rbp), %r14
	jne	.L1742
	testq	%rax, %rax
	jne	.L1798
.L1742:
	movq	$0, -232(%rbp)
	movl	16(%rbx), %eax
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	cmpl	$2, %eax
	jg	.L1743
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1744:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1745
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L1799
.L1745:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1722
	movl	16(%rbx), %eax
	testq	%rdx, %rdx
	js	.L1746
	movq	%rdx, -232(%rbp)
.L1746:
	cmpl	$2, %eax
	jle	.L1770
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
.L1748:
	xorl	%edx, %edx
	leaq	-232(%rbp), %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1800
.L1749:
	shrw	$8, %ax
	je	.L1793
	cmpl	$3, 16(%rbx)
	jg	.L1751
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1752:
	leaq	-224(%rbp), %r15
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r15, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1722
	cmpl	$3, 16(%rbx)
	jg	.L1754
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1755:
	xorl	%edx, %edx
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1801
.L1756:
	shrw	$8, %ax
	je	.L1793
	cmpl	$4, 16(%rbx)
	movq	-72(%rbp), %rdx
	jg	.L1758
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1759:
	leaq	-216(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1722
	cmpl	$4, 16(%rbx)
	movq	-72(%rbp), %rdx
	jle	.L1802
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rsi
.L1761:
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1803
.L1762:
	shrw	$8, %ax
	je	.L1793
	movq	-232(%rbp), %rdi
	cmpq	%r12, %rdi
	jnb	.L1764
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdx
	cmpq	%rdx, %rsi
	jnb	.L1764
	movq	-72(%rbp), %rax
	cmpq	%rax, %rsi
	ja	.L1804
	movq	%r12, %rcx
	subq	%rsi, %rdx
	subq	%rdi, %rcx
	cmpq	%rcx, %rdx
	jbe	.L1767
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	addq	%r12, %rdx
	movq	%rdx, -216(%rbp)
	subq	%rsi, %rdx
.L1767:
	subq	%rsi, %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	addq	-80(%rbp), %rsi
	addq	%r14, %rdi
	movq	%rdx, %r12
	movl	%edx, %edx
	call	memmove@PLT
	movq	(%rbx), %rbx
	testl	%r12d, %r12d
	js	.L1768
	salq	$32, %r12
	movq	%r12, 24(%rbx)
	.p2align 4,,10
	.p2align 3
.L1722:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1805
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1794:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L1732
.L1791:
	movq	352(%r13), %rdi
	call	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1726:
	movq	8(%rbx), %rdi
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	8(%rbx), %r12
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1739:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r12
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1743:
	movq	8(%rbx), %rcx
	leaq	-16(%rcx), %rdi
	jmp	.L1744
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1799:
	cmpl	$5, 43(%rdx)
	je	.L1746
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1723:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1796:
	leaq	-144(%rbp), %r14
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -80(%rbp)
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rsi
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rsi
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rsi
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1795:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1797:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1802:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1800:
	movl	%eax, -244(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-244(%rbp), %eax
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L1798:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1801:
	movl	%eax, -244(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-244(%rbp), %eax
	jmp	.L1756
.L1803:
	movl	%eax, -244(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-244(%rbp), %eax
	jmp	.L1762
.L1804:
	movq	352(%r13), %rdi
	leaq	.LC36(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1722
.L1768:
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L1806
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1722
.L1805:
	call	__stack_chk_fail@PLT
.L1806:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1722
	.cfi_endproc
.LFE7676:
	.size	_ZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2248, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1876
	cmpw	$1040, %cx
	jne	.L1808
.L1876:
	movq	23(%rdx), %r13
.L1810:
	movl	16(%rbx), %ecx
	movq	3280(%r13), %r14
	testl	%ecx, %ecx
	jg	.L1811
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1918
.L1813:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L1919
	movq	8(%rbx), %rdi
.L1816:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1920
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1818
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
.L1819:
	movq	%r15, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-2256(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r15, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	addq	-2256(%rbp), %r12
	movq	%r12, -2280(%rbp)
	movq	%rax, %r15
	jne	.L1820
	testq	%rax, %rax
	jne	.L1921
.L1820:
	cmpl	$2, 16(%rbx)
	jg	.L1821
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1822:
	leaq	-2272(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rcx
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1807
	cmpl	$2, 16(%rbx)
	jg	.L1824
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1825:
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1922
.L1826:
	shrw	$8, %ax
	je	.L1913
	cmpl	$3, 16(%rbx)
	jle	.L1923
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rsi
.L1829:
	leaq	-2264(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rcx, -2288(%rbp)
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1807
	cmpl	$3, 16(%rbx)
	movq	-2288(%rbp), %rcx
	jg	.L1830
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1831:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN4node6Buffer12_GLOBAL__N_115ParseArrayIndexEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEEmPm
	testb	%al, %al
	je	.L1924
.L1832:
	shrw	$8, %ax
	je	.L1913
	movq	-2264(%rbp), %rax
	movq	-2272(%rbp), %rdx
	cmpq	%r15, %rax
	ja	.L1877
	cmpq	%rdx, %rax
	jb	.L1877
	cmpl	$1, 16(%rbx)
	jle	.L1925
	movq	8(%rbx), %rcx
	leaq	-8(%rcx), %rdi
.L1837:
	subq	%rdx, %rax
	movq	%rax, %r15
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1838
	cmpl	$1, 16(%rbx)
	jg	.L1839
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1840:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1926
	cmpl	$1, 16(%rbx)
	jg	.L1842
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L1843:
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-2192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	-2192(%rbp), %rsi
	movq	%rax, %r14
	addq	%r13, %rsi
	testq	%rax, %rax
	je	.L1844
	testq	%rsi, %rsi
	je	.L1927
.L1844:
	movq	-2280(%rbp), %rdi
	movq	%r14, %rdx
	addq	-2272(%rbp), %rdi
	cmpq	%r14, %r15
	cmovbe	%r15, %rdx
	call	memcpy@PLT
.L1845:
	cmpq	%r14, %r15
	jbe	.L1807
	testq	%r14, %r14
	je	.L1928
	movq	-2272(%rbp), %rsi
	movq	%r15, %rdx
	subq	%r14, %rdx
	leaq	(%rsi,%r14), %rdi
	addq	-2280(%rbp), %rdi
	cmpq	%rdx, %r14
	jb	.L1873
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	-2272(%rbp), %rsi
.L1873:
	movq	%r14, %rdx
	addq	-2280(%rbp), %rsi
	call	memcpy@PLT
	movq	%r15, %rdx
	movq	%rax, %rdi
	addq	%r14, %rdi
	addq	%r14, %r14
	subq	%r14, %rdx
	cmpq	%r14, %rdx
	ja	.L1929
	cmpq	%r14, %r15
	jbe	.L1807
	movq	-2272(%rbp), %rsi
.L1871:
	addq	-2280(%rbp), %rsi
	call	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L1807:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1930
	addq	$2248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1919:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1811:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L1813
.L1918:
	movq	352(%r13), %rdi
	call	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	8(%rbx), %r15
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L1821:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1824:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L1829
	.p2align 4,,10
	.p2align 3
.L1808:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1810
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	352(%r13), %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN4node22THROW_ERR_OUT_OF_RANGEEPN2v87IsolateEPKc
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rsi
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1877:
	movabsq	$-8589934592, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1920:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	(%rbx), %rcx
	movq	8(%rcx), %rdi
	addq	$88, %rdi
	jmp	.L1837
	.p2align 4,,10
	.p2align 3
.L1922:
	movl	%eax, -2288(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-2288(%rbp), %eax
	jmp	.L1826
	.p2align 4,,10
	.p2align 3
.L1838:
	cmpl	$1, 16(%rbx)
	jg	.L1846
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1847:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1848
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1849
.L1848:
	movq	%r14, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1807
	shrq	$32, %rax
	movq	-2280(%rbp), %rdi
	movq	%r15, %rdx
	addq	-2272(%rbp), %rdi
	movzbl	%al, %esi
	call	memset@PLT
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1921:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1924:
	movl	%eax, -2288(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-2288(%rbp), %eax
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1839:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r14
	jmp	.L1843
.L1928:
	movabsq	$-4294967296, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1807
.L1849:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1931
.L1851:
	cmpl	$4, 16(%rbx)
	jg	.L1852
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L1853:
	movq	352(%r13), %rdi
	movl	$1, %edx
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	movl	%eax, %r8d
	cmpl	$1, %eax
	je	.L1932
	cmpl	$3, %eax
	je	.L1933
	movq	352(%r13), %rdi
	movq	%r14, %rcx
	xorl	%r9d, %r9d
	movq	%r15, %rdx
	movq	-2280(%rbp), %rsi
	addq	-2272(%rbp), %rsi
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	%rax, %r14
	jmp	.L1845
.L1926:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1852:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rsi
	jmp	.L1853
.L1927:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1933:
	movq	%r14, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	movslq	%eax, %r14
	addq	%r14, %r14
	cmpl	$1, 16(%rbx)
	jg	.L1859
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L1860:
	movq	352(%r13), %rsi
	leaq	-2128(%rbp), %rdi
	call	_ZN4node12TwoByteValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
.L1917:
	movq	-2280(%rbp), %rdi
	movq	%r14, %rdx
	addq	-2272(%rbp), %rdi
	cmpq	%r14, %r15
	cmovbe	%r15, %rdx
	movq	-2112(%rbp), %rsi
	call	memcpy@PLT
	movq	-2112(%rbp), %rdi
	leaq	-2104(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1845
	testq	%rdi, %rdi
	je	.L1845
	call	free@PLT
	jmp	.L1845
.L1932:
	movq	352(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	cmpl	$1, 16(%rbx)
	movslq	%eax, %r14
	jg	.L1855
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L1856:
	movq	352(%r13), %rsi
	leaq	-2128(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	jmp	.L1917
.L1859:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdx
	jmp	.L1860
.L1855:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdx
	jmp	.L1856
.L1931:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1851
.L1930:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7677:
	.size	_ZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.globl	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE, @function
_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE:
.LFB7651:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	.cfi_endproc
.LFE7651:
	.size	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE, .-_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_6ObjectEEE:
.LFB7652:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	.cfi_endproc
.LFE7652:
	.size	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_6ObjectEEE, .-_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_6ObjectEEE
	.p2align 4
	.globl	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE, @function
_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1940
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rbx, %rax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1941
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1940:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1941:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7653:
	.size	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE, .-_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE:
.LFB7654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1946
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rbx, %rax
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1947
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1946:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1947:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7654:
	.size	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE, .-_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE
	.p2align 4
	.globl	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE, @function
_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1951
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	.p2align 4,,10
	.p2align 3
.L1951:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7655:
	.size	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE, .-_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE:
.LFB7656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1955
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	.p2align 4,,10
	.p2align 3
.L1955:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7656:
	.size	_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE, .-_ZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEE
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmm
	.type	_ZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmm, @function
_ZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmm:
.LFB7657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 2744(%rdi)
	movq	%rdi, %rbx
	je	.L1961
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	2744(%rbx), %rdx
	movq	3280(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	popq	%rbx
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1961:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7657:
	.size	_ZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmm, .-_ZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmm
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"ERR_BUFFER_CONTEXT_NOT_AVAILABLE"
	.align 8
.LC42:
	.string	"Buffer is not available for the current Context"
	.text
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_11ArrayBufferEEEmm
	.type	_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_11ArrayBufferEEEmm, @function
_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_11ArrayBufferEEEmm:
.LFB7658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L1964
	leaq	-80(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1966
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L1966
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L1966
	movq	271(%rax), %r13
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r13, %r13
	je	.L1964
	cmpq	$0, 2744(%r13)
	je	.L1979
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	2744(%r13), %rdx
	movq	3280(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
.L1973:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1980
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1966:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1964:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1981
.L1968:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1982
.L1969:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1983
.L1970:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1984
.L1971:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1985
.L1972:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%eax, %eax
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1979:
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1985:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L1981:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L1982:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L1983:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1984:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1971
.L1980:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7658:
	.size	_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_11ArrayBufferEEEmm, .-_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_11ArrayBufferEEEmm
	.p2align 4
	.globl	_Z16_register_bufferv
	.type	_Z16_register_bufferv, @function
_Z16_register_bufferv:
.LFB7706:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7706:
	.size	_Z16_register_bufferv, .-_Z16_register_bufferv
	.section	.text._ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB9183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movdqu	0(%r13), %xmm0
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	$0, (%rax)
	movups	%xmm0, 8(%rax)
	movq	16(%r13), %rax
	movq	16(%r12), %r13
	movq	%rax, 24(%r12)
	movq	%r13, %rax
	divq	%rsi
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	leaq	0(,%rdx,8), %r15
	testq	%rdi, %rdi
	je	.L1988
	movq	(%rdi), %rax
	movq	%rdx, %r8
	movq	32(%rax), %rcx
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L1988
	movq	32(%r9), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L1988
	movq	%r9, %rax
.L1991:
	cmpq	%rcx, %r13
	jne	.L1989
	movq	8(%rax), %rcx
	cmpq	%rcx, 8(%r12)
	jne	.L1989
	cmpq	16(%rax), %r13
	jne	.L1989
	movq	(%rdi), %r14
	testq	%r14, %r14
	je	.L1988
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r14, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1988:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L1993
	movq	(%rbx), %r8
	movq	%r13, 32(%r12)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L2003
.L2027:
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%r15), %rax
	movq	%r12, (%rax)
.L2004:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1993:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L2025
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2026
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1996:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1998
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2000:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2001:
	testq	%rsi, %rsi
	je	.L1998
.L1999:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2000
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2007
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1999
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2002
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2002:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%rbx)
	divq	%r14
	movq	%r8, (%rbx)
	movq	%r13, 32(%r12)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L2027
.L2003:
	movq	16(%rbx), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L2005
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r12, (%r8,%rdx,8)
.L2005:
	leaq	16(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%rdx, %rdi
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2025:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1996
.L2026:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9183:
	.size	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"Cannot create a Buffer larger than 0x%zx bytes"
	.section	.rodata.str1.1
.LC44:
	.string	"ERR_BUFFER_TOO_LARGE"
	.text
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmPFvS3_PvES4_
	.type	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmPFvS3_PvES4_, @function
_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmPFvS3_PvES4_:
.LFB7668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-288(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%rcx, -328(%rbp)
	movq	352(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	cmpq	$2147483647, %r14
	jbe	.L2029
	movq	352(%rbx), %r14
	leaq	-192(%rbp), %rbx
	movl	$2147483647, %r9d
	leaq	.LC43(%rip), %r8
	movl	$128, %ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	$128, %esi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	.LC44(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -336(%rbp)
	testq	%rax, %rax
	je	.L2054
.L2030:
	movq	%r14, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2055
.L2031:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2056
.L2032:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2057
.L2033:
	movq	%r14, %rdi
	movq	%rdx, -344(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-336(%rbp), %rcx
	movq	-344(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2058
.L2034:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L2053:
	movq	%r12, %rdi
	movq	-328(%rbp), %rax
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	*%rax
.L2035:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2059
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2029:
	.cfi_restore_state
	movq	352(%rbx), %rdi
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movq	3280(%rbx), %rsi
	movq	%rax, %r8
	movq	352(%rbx), %rax
	movq	%r8, %rdi
	movq	%r8, -336(%rbp)
	leaq	112(%rax), %rcx
	movq	360(%rbx), %rax
	movq	72(%rax), %rdx
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	movq	-336(%rbp), %r8
	testb	%al, %al
	je	.L2053
	cmpq	$0, 2744(%rbx)
	je	.L2060
	movq	%r8, %rdi
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r8, -344(%rbp)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	2744(%rbx), %rdx
	movq	3280(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movl	$40, %edi
	testb	%al, %al
	movl	$0, %eax
	cmovne	%r14, %rax
	movq	%rax, -336(%rbp)
	call	_Znwm@PLT
	movq	-344(%rbp), %r8
	movq	352(%rbx), %rdi
	movq	%rax, %r14
	testq	%r8, %r8
	je	.L2048
	movq	%r8, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-344(%rbp), %r8
.L2039:
	movq	%rax, (%r14)
	movq	%r13, %xmm1
	movq	%r12, %xmm0
	movq	%r8, %rsi
	movq	-328(%rbp), %rax
	movq	%rbx, 32(%r14)
	punpcklqdq	%xmm1, %xmm0
	leaq	-256(%rbp), %rdi
	movups	%xmm0, 16(%r14)
	movq	%rax, 8(%r14)
	movq	%r8, -328(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-256(%rbp), %rax
	cmpq	%rax, 16(%r14)
	movq	-328(%rbp), %r8
	jne	.L2061
	movq	%r8, %rdi
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	testq	%rax, %rax
	je	.L2041
	cmpq	$0, 16(%r14)
	je	.L2062
.L2041:
	movq	(%r14), %rdi
	xorl	%ecx, %ecx
	leaq	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo12WeakCallbackERKN2v816WeakCallbackInfoIS2_EE(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	2640(%rbx), %rax
	leaq	_ZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPv(%rip), %rcx
	leaq	-320(%rbp), %rsi
	leaq	2584(%rbx), %rdi
	movq	%rcx, -320(%rbp)
	leaq	1(%rax), %rdx
	movq	%r14, -312(%rbp)
	movq	%rdx, 2640(%rbx)
	movq	%rax, -304(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	testb	%dl, %dl
	je	.L2063
	movq	352(%rbx), %r12
	movq	32(%r12), %rax
	leaq	40(%rax), %rbx
	movq	%rbx, %rax
	subq	48(%r12), %rax
	movq	%rbx, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L2064
.L2043:
	cmpq	40(%r12), %rbx
	jg	.L2065
.L2044:
	xorl	%r12d, %r12d
	cmpq	$0, -336(%rbp)
	je	.L2035
	movq	-336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L2035
	.p2align 4,,10
	.p2align 3
.L2054:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2055:
	movq	%rax, -344(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-344(%rbp), %rdi
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2058:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2034
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	%rax, -344(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-344(%rbp), %rdx
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2056:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2032
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2048:
	xorl	%eax, %eax
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2060:
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2061:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2063:
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2062:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2059:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7668:
	.size	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmPFvS3_PvES4_, .-_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmPFvS3_PvES4_
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPN2v87IsolateEPcmPFvS4_PvES5_
	.type	_ZN4node6Buffer3NewEPN2v87IsolateEPcmPFvS4_PvES5_, @function
_ZN4node6Buffer3NewEPN2v87IsolateEPcmPFvS4_PvES5_:
.LFB7667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-96(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L2068
	leaq	-128(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	testq	%rax, %rax
	je	.L2070
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2070
	movq	-144(%rbp), %rdx
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	(%rdx), %rax
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L2070
	movq	271(%rax), %r9
	movq	%r14, %rdi
	movq	%r9, -144(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-144(%rbp), %r9
	testq	%r9, %r9
	je	.L2068
	movq	-136(%rbp), %rcx
	movq	%r9, %rdi
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	-152(%rbp), %rdx
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmPFvS3_PvES4_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L2077:
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2080
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2070:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2068:
	movq	%r15, %rsi
	movq	-136(%rbp), %rax
	movq	%r13, %rdi
	call	*%rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2081
.L2072:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2082
.L2073:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2083
.L2074:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2084
.L2075:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2085
.L2076:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2077
	.p2align 4,,10
	.p2align 3
.L2081:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2072
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2083:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2084:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L2085:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2076
.L2080:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7667:
	.size	_ZN4node6Buffer3NewEPN2v87IsolateEPcmPFvS4_PvES5_, .-_ZN4node6Buffer3NewEPN2v87IsolateEPcmPFvS4_PvES5_
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb
	.type	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb, @function
_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb:
.LFB7670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	testq	%rdx, %rdx
	je	.L2087
	testq	%rsi, %rsi
	je	.L2100
	cmpq	$2147483647, %rdx
	ja	.L2101
.L2087:
	testb	%cl, %cl
	je	.L2089
	movq	360(%r12), %rax
	cmpb	$0, 2384(%rax)
	je	.L2102
	movq	2376(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2103
	movq	(%rdi), %rax
	leaq	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm(%rip), %rdx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2092
	lock addq	%r13, 16(%rdi)
.L2089:
	movq	352(%r12), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	cmpq	$0, 2744(%r12)
	movq	%rax, %rdi
	je	.L2104
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	2744(%r12), %rdx
	movq	3280(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r13
	addq	$16, %rsp
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2102:
	.cfi_restore_state
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	popq	%r12
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbENUlS3_PvE_4_FUNES3_S4_(%rip), %rcx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmPFvS3_PvES4_
	.p2align 4,,10
	.p2align 3
.L2100:
	.cfi_restore_state
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2092:
	movq	%rsi, -24(%rbp)
	movq	%r13, %rdx
	call	*%rax
	movq	-24(%rbp), %rsi
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2104:
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2101:
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2103:
	leaq	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7670:
	.size	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb, .-_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb
	.section	.rodata.str1.1
.LC45:
	.string	"ERR_MEMORY_ALLOCATION_FAILED"
.LC46:
	.string	"Failed to allocate memory"
	.text
	.p2align 4
	.globl	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm
	.type	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm, @function
_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm:
.LFB7666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	%rsi, -296(%rbp)
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	cmpq	$2147483647, %r12
	jbe	.L2106
	movq	352(%rbx), %r12
	leaq	-192(%rbp), %r13
	xorl	%eax, %eax
	movl	$2147483647, %r9d
	leaq	.LC43(%rip), %r8
	movl	$128, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movl	$128, %esi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2142
.L2107:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2143
.L2108:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2144
.L2109:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2145
.L2110:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2146
.L2111:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L2112:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2147
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2106:
	.cfi_restore_state
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	uv_buf_init@PLT
	movq	%rdx, %rdi
	movq	%rax, -280(%rbp)
	movq	%rax, %r15
	movq	%rdi, -272(%rbp)
	movq	%rdi, %r13
	testq	%r12, %r12
	je	.L2113
	cmpq	$0, -296(%rbp)
	je	.L2148
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	$0, %esi
	testq	%rax, %rax
	movq	%rax, %rdi
	cmovne	%r12, %rsi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -304(%rbp)
	movq	%rdx, -328(%rbp)
	call	uv_buf_init@PLT
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	testq	%r15, %r15
	je	.L2117
	movq	360(%rbx), %rax
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2117:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -320(%rbp)
	movq	%rdi, -312(%rbp)
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-320(%rbp), %r8
	movq	-312(%rbp), %r9
	movq	%rax, -248(%rbp)
	testq	%r8, %r8
	movq	%rdx, -240(%rbp)
	je	.L2128
	movq	360(%rbx), %rax
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2128:
	cmpq	$0, -304(%rbp)
	je	.L2149
	movq	-304(%rbp), %r15
	movq	-296(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	-328(%rbp), %r13
.L2113:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2126
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	uv_buf_init@PLT
	movq	%rdx, %rdi
	movq	%rax, %r15
	movq	%rdi, %r13
.L2126:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r12
	call	uv_buf_init@PLT
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	testq	%r15, %r15
	je	.L2112
	movq	360(%rbx), %rax
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2149:
	movq	352(%rbx), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2150
.L2119:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC46(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2151
.L2120:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2152
.L2121:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2153
.L2122:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2154
.L2123:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	jmp	.L2112
	.p2align 4,,10
	.p2align 3
.L2142:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	%rax, -296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-296(%rbp), %rdi
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2146:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2145:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2144:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2148:
	leaq	_ZZN4node6Buffer4CopyEPNS_11EnvironmentEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2154:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2123
.L2153:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2122
.L2152:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2121
.L2151:
	movq	%rax, -296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-296(%rbp), %rdi
	jmp	.L2120
.L2150:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2119
.L2147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7666:
	.size	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm, .-_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm
	.p2align 4
	.globl	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm
	.type	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm, @function
_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm:
.LFB7665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L2157
	leaq	-128(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2159
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2159
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L2159
	movq	271(%rax), %rbx
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%rbx, %rbx
	je	.L2157
	movq	-136(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2166
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L2166:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2174
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2159:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2157:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2175
.L2161:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2176
.L2162:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2177
.L2163:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2178
.L2164:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2179
.L2165:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2175:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2176:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2177:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2163
	.p2align 4,,10
	.p2align 3
.L2178:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2179:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2165
.L2174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7665:
	.size	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm, .-_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPN2v87IsolateEPcm
	.type	_ZN4node6Buffer3NewEPN2v87IsolateEPcm, @function
_ZN4node6Buffer3NewEPN2v87IsolateEPcm:
.LFB7669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L2182
	leaq	-128(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2184
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2184
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L2184
	movq	271(%rax), %rbx
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%rbx, %rbx
	je	.L2182
	movq	-136(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	movl	$1, %ecx
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2191
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L2191:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2199
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2184:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2182:
	movq	%r13, %rdi
	call	free@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC41(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2200
.L2186:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2201
.L2187:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2202
.L2188:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2203
.L2189:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2204
.L2190:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2200:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2201:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2202:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2203:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2189
	.p2align 4,,10
	.p2align 3
.L2204:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2190
.L2199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7669:
	.size	_ZN4node6Buffer3NewEPN2v87IsolateEPcm, .-_ZN4node6Buffer3NewEPN2v87IsolateEPcm
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingE
	.type	_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingE, @function
_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingE:
.LFB7659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	testb	%al, %al
	je	.L2206
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	jne	.L2224
	xorl	%r10d, %r10d
.L2207:
	movq	%r10, %rsi
	movq	%r12, %rdi
	call	_ZN4node6Buffer3NewEPN2v87IsolateEPcm
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L2214:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2225
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2224:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	malloc@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L2226
.L2208:
	xorl	%r9d, %r9d
	movq	%r13, %rcx
	movq	%r10, %rsi
	movl	%r14d, %r8d
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	-104(%rbp), %r10
	cmpq	%rbx, %rax
	movq	%rax, %r13
	ja	.L2227
	testq	%rax, %rax
	je	.L2228
	movq	%rax, %rdx
	cmpq	%rbx, %rax
	jnb	.L2207
	movq	%r10, %rdi
	movq	%rax, %rsi
	movq	%r10, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	realloc@PLT
	movq	-112(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L2207
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-104(%rbp), %r10
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	realloc@PLT
	movq	%r13, %rdx
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L2207
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2206:
	xorl	%r12d, %r12d
	jmp	.L2214
	.p2align 4,,10
	.p2align 3
.L2226:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L2208
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2229
.L2209:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC46(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2230
.L2210:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2231
.L2211:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2232
.L2212:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2233
.L2213:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2214
	.p2align 4,,10
	.p2align 3
.L2228:
	movq	%r10, %rdi
	call	free@PLT
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2227:
	leaq	_ZZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2233:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2213
.L2232:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2212
.L2229:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2209
.L2231:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2211
.L2230:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rdi
	jmp	.L2210
.L2225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7659:
	.size	_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingE, .-_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jg	.L2235
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2249
.L2237:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L2237
.L2249:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2237
	cmpl	$1, %edx
	jle	.L2250
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L2240:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L2251
	cmpl	$1, 16(%rbx)
	jg	.L2242
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2243:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, %edx
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L2244
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	leaq	88(%rdi), %rsi
.L2245:
	call	_ZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingE
	testq	%rax, %rax
	je	.L2234
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L2234:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2250:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2244:
	movq	(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	8(%rax), %rdi
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2242:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2251:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7674:
	.size	_ZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPNS_11EnvironmentEm
	.type	_ZN4node6Buffer3NewEPNS_11EnvironmentEm, @function
_ZN4node6Buffer3NewEPNS_11EnvironmentEm:
.LFB7664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	cmpq	$2147483647, %r12
	jbe	.L2253
	movq	352(%rbx), %r12
	leaq	-192(%rbp), %r13
	xorl	%eax, %eax
	movl	$2147483647, %r9d
	leaq	.LC43(%rip), %r8
	movl	$128, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movl	$128, %esi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC44(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2288
.L2254:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2289
.L2255:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2290
.L2256:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2291
.L2257:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2292
.L2258:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L2259:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2293
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2253:
	.cfi_restore_state
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	uv_buf_init@PLT
	movq	%rdx, %rdi
	movq	%rax, -280(%rbp)
	movq	%rax, %r13
	movq	%rdi, -272(%rbp)
	movq	%rdi, %r15
	testq	%r12, %r12
	jne	.L2294
.L2260:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2271
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	uv_buf_init@PLT
	movq	%rdx, %rdi
	movq	%rax, %r13
	movq	%rdi, %r15
.L2271:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r12
	call	uv_buf_init@PLT
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	testq	%r13, %r13
	je	.L2259
	movq	360(%rbx), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rax, %rax
	movq	%rax, %rdi
	movl	$0, %eax
	cmove	%rax, %r12
	movl	%r12d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -296(%rbp)
	movq	%rdx, -304(%rbp)
	call	uv_buf_init@PLT
	movq	%rax, -280(%rbp)
	movq	%rdx, -272(%rbp)
	testq	%r13, %r13
	je	.L2263
	movq	360(%rbx), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2263:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, %r13
	movq	%rdi, %r12
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -248(%rbp)
	movq	%rdx, -240(%rbp)
	testq	%r13, %r13
	je	.L2273
	movq	360(%rbx), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2273:
	movq	-296(%rbp), %r13
	movq	-304(%rbp), %r15
	testq	%r13, %r13
	jne	.L2260
	movq	352(%rbx), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2295
.L2264:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC46(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2296
.L2265:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2297
.L2266:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2298
.L2267:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2299
.L2268:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2288:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	%rax, -296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-296(%rbp), %rdi
	jmp	.L2255
	.p2align 4,,10
	.p2align 3
.L2292:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2291:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2290:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2256
.L2295:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2264
.L2299:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2268
.L2298:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2267
.L2297:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2266
.L2296:
	movq	%rax, -296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-296(%rbp), %rdi
	jmp	.L2265
.L2293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7664:
	.size	_ZN4node6Buffer3NewEPNS_11EnvironmentEm, .-_ZN4node6Buffer3NewEPNS_11EnvironmentEm
	.p2align 4
	.globl	_ZN4node6Buffer3NewEPN2v87IsolateEm
	.type	_ZN4node6Buffer3NewEPN2v87IsolateEm, @function
_ZN4node6Buffer3NewEPN2v87IsolateEm:
.LFB7663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L2302
	leaq	-128(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2304
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2304
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L2304
	movq	271(%rax), %rbx
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%rbx, %rbx
	je	.L2302
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEm
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2311
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L2311:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2319
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2304:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2302:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2320
.L2306:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2321
.L2307:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2322
.L2308:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2323
.L2309:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2324
.L2310:
	movq	%r12, %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2320:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2321:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rdi
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2322:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2323:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2324:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2310
.L2319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7663:
	.size	_ZN4node6Buffer3NewEPN2v87IsolateEm, .-_ZN4node6Buffer3NewEPN2v87IsolateEm
	.section	.text._ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm,"axG",@progbits,_ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm
	.type	_ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm, @function
_ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm:
.LFB9452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	1024(%rdi), %rbx
	movq	%rbx, -64(%rbp)
	movzbl	32(%rbp), %eax
	movq	3032(%rdi), %r11
	movzbl	3048(%rdi), %r14d
	movq	24(%rbp), %r13
	movq	3064(%rdi), %r15
	movq	16(%rbp), %r9
	movb	%al, -41(%rbp)
	movq	3040(%rdi), %rax
	testb	%r14b, %r14b
	movq	%r15, -56(%rbp)
	leaq	-1(%rax), %rdx
	leaq	(%r11,%rdx,2), %rcx
	cmove	%r11, %rcx
	movzwl	(%rcx), %r8d
	movq	%r13, %rcx
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L2325
	movq	%rcx, %rax
	movzbl	%r8b, %r10d
	addq	%rax, %rax
	leaq	(%rdi,%r10,4), %rbx
	movq	%rax, -88(%rbp)
	movl	$1, %eax
	subq	%r15, %rax
	movq	%rbx, -72(%rbp)
	movq	%rcx, %rbx
	movq	%rax, -80(%rbp)
.L2328:
	cmpb	$0, -41(%rbp)
	jne	.L2329
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2360:
	movzbl	%al, %eax
	movl	%edx, %r15d
	subl	(%rdi,%rax,4), %r15d
	movslq	%r15d, %rax
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	jb	.L2325
.L2331:
	movq	%rbx, %rax
	subq	%rsi, %rax
	movzwl	(%r9,%rax,2), %eax
	cmpw	%ax, %r8w
	jne	.L2360
	testb	%r14b, %r14b
	jne	.L2341
	movq	-88(%rbp), %r15
	movq	%rsi, %rax
	xorl	%r12d, %r12d
	negq	%rax
	leaq	(%r15,%rax,2), %r15
	movq	%rdx, %rax
	addq	%r9, %r15
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2361:
	addq	$2, %r12
	testq	%rax, %rax
	je	.L2349
	subq	$1, %rax
.L2335:
	movzwl	(%r15,%r12), %r10d
	cmpw	(%r11,%r12), %r10w
	je	.L2361
.L2334:
	cmpq	%rax, -56(%rbp)
	jbe	.L2339
	movq	-72(%rbp), %rax
	movq	%rdx, %r15
	movslq	(%rax), %rax
	subq	%rax, %r15
	addq	%r15, %rsi
	cmpq	%rsi, %rcx
	jnb	.L2328
.L2325:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2332:
	.cfi_restore_state
	movzbl	%al, %eax
	movl	%edx, %r10d
	subl	(%rdi,%rax,4), %r10d
	movslq	%r10d, %rax
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	jb	.L2325
.L2329:
	leaq	(%rdx,%rsi), %rax
	movzwl	(%r9,%rax,2), %eax
	cmpw	%r8w, %ax
	jne	.L2332
	testb	%r14b, %r14b
	jne	.L2343
	movq	%r11, %r12
	leaq	(%r9,%rsi,2), %r15
	movq	%rdx, %rax
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2362:
	addq	$2, %r12
	testq	%rax, %rax
	je	.L2349
	subq	$1, %rax
.L2336:
	movzwl	(%r15,%rax,2), %r10d
	cmpw	(%r12), %r10w
	je	.L2362
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	%rcx, %rax
	subq	%rsi, %rax
	leaq	(%r9,%rax,2), %r12
	movq	%rdx, %rax
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2363:
	addq	$2, %r12
	testq	%rax, %rax
	je	.L2349
	subq	$1, %rax
.L2337:
	movzwl	(%r12), %r10d
	cmpw	%r10w, (%r11,%rax,2)
	je	.L2363
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2343:
	leaq	(%r9,%rsi,2), %r12
	movq	%rdx, %rax
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2364:
	testq	%rax, %rax
	je	.L2349
	subq	$1, %rax
.L2338:
	movzwl	(%r12,%rax,2), %r10d
	cmpw	%r10w, (%r11,%rax,2)
	je	.L2364
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2349:
	movq	%rsi, %r13
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2339:
	.cfi_restore_state
	movzbl	%r10b, %r10d
	movl	%eax, %r15d
	addq	-80(%rbp), %rax
	subl	(%rdi,%r10,4), %r15d
	movl	%r15d, %r10d
	movq	-64(%rbp), %r15
	cmpl	%r10d, (%r15,%rax,4)
	cmovge	(%r15,%rax,4), %r10d
	movslq	%r10d, %rax
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	jnb	.L2328
	jmp	.L2325
	.cfi_endproc
.LFE9452:
	.size	_ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm, .-_ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm
	.section	.text._ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm,"axG",@progbits,_ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm
	.type	_ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm, @function
_ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm:
.LFB9457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	32(%rbp), %eax
	movq	3040(%rdi), %rcx
	movq	3032(%rdi), %r11
	movq	24(%rbp), %r14
	movb	%al, -41(%rbp)
	leaq	1024(%rdi), %rax
	leaq	-1(%rcx), %rdx
	movq	16(%rbp), %r10
	movq	%rax, -64(%rbp)
	movzbl	3048(%rdi), %eax
	movq	3064(%rdi), %r15
	movl	%eax, %ebx
	movb	%al, -42(%rbp)
	leaq	(%r11,%rdx), %rax
	testb	%bl, %bl
	movq	%r15, -56(%rbp)
	cmove	%r11, %rax
	movzbl	(%rax), %r8d
	movq	%r14, %rax
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L2365
	movq	%rax, %rcx
	movzbl	%r8b, %eax
	leaq	(%r10,%rdx), %rbx
	leaq	(%rdi,%rax,4), %rax
	movq	%rcx, %r12
	movq	%rax, -72(%rbp)
	movl	$1, %eax
	subq	%r15, %rax
	movq	%rax, -80(%rbp)
.L2368:
	cmpb	$0, -41(%rbp)
	jne	.L2369
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2398:
	movl	%edx, %r15d
	subl	(%rdi,%rax,4), %r15d
	movslq	%r15d, %rax
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	jb	.L2365
.L2371:
	movq	%r12, %rax
	subq	%rsi, %rax
	movzbl	(%r10,%rax), %eax
	cmpb	%al, %r8b
	jne	.L2398
	cmpb	$0, -42(%rbp)
	movq	%rdx, %rax
	jne	.L2387
	movq	%rcx, %r15
	xorl	%r13d, %r13d
	subq	%rsi, %r15
	addq	%r10, %r15
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2399:
	addq	$1, %r13
	testq	%rax, %rax
	je	.L2386
	subq	$1, %rax
.L2375:
	movzbl	(%r15,%r13), %r9d
	cmpb	(%r11,%r13), %r9b
	je	.L2399
.L2374:
	cmpq	%rax, -56(%rbp)
	jbe	.L2379
	movq	-72(%rbp), %rax
	movq	%rdx, %r15
	movslq	(%rax), %rax
	subq	%rax, %r15
	addq	%r15, %rsi
	cmpq	%rsi, %rcx
	jnb	.L2368
.L2365:
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2372:
	.cfi_restore_state
	movl	%edx, %r9d
	subl	(%rdi,%rax,4), %r9d
	movslq	%r9d, %rax
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	jb	.L2365
.L2369:
	movzbl	(%rbx,%rsi), %eax
	cmpb	%r8b, %al
	jne	.L2372
	cmpb	$0, -42(%rbp)
	jne	.L2388
	movq	%r11, %r13
	movq	%rdx, %rax
	leaq	(%r10,%rsi), %r15
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2400:
	addq	$1, %r13
	testq	%rax, %rax
	je	.L2386
	subq	$1, %rax
.L2376:
	movzbl	(%r15,%rax), %r9d
	cmpb	0(%r13), %r9b
	je	.L2400
	jmp	.L2374
	.p2align 4,,10
	.p2align 3
.L2387:
	movq	%r14, %r13
	subq	%rsi, %r13
	addq	%r10, %r13
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2401:
	testq	%rax, %rax
	je	.L2386
	subq	$1, %rax
.L2377:
	movq	%rax, %r9
	negq	%r9
	movzbl	-1(%r13,%r9), %r9d
	cmpb	(%r11,%rax), %r9b
	je	.L2401
	jmp	.L2374
	.p2align 4,,10
	.p2align 3
.L2388:
	movq	%rdx, %rax
	leaq	(%r10,%rsi), %r15
	jmp	.L2378
	.p2align 4,,10
	.p2align 3
.L2402:
	testq	%rax, %rax
	je	.L2386
	subq	$1, %rax
.L2378:
	movzbl	(%r15,%rax), %r9d
	cmpb	%r9b, (%r11,%rax)
	je	.L2402
	jmp	.L2374
	.p2align 4,,10
	.p2align 3
.L2386:
	movq	%rsi, %r14
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2379:
	.cfi_restore_state
	movl	%eax, %r15d
	subl	(%rdi,%r9,4), %r15d
	addq	-80(%rbp), %rax
	movl	%r15d, %r9d
	movq	-64(%rbp), %r15
	cmpl	%r9d, (%r15,%rax,4)
	cmovge	(%r15,%rax,4), %r9d
	movslq	%r9d, %rax
	addq	%rax, %rsi
	cmpq	%rsi, %rcx
	jnb	.L2368
	jmp	.L2365
	.cfi_endproc
.LFE9457:
	.size	_ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm, .-_ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm
	.section	.text._ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv
	.type	_ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv, @function
_ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv:
.LFB9652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1024(%rdi), %rax
	leaq	2028(%rdi), %rsi
	movq	%rax, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	3064(%rdi), %r11
	movq	3040(%rdi), %rdx
	leaq	0(,%r11,4), %rcx
	leaq	0(,%rdx,4), %r9
	movq	%rdx, %r8
	subq	%rcx, %r10
	subq	%rcx, %rsi
	leal	1(%rdx), %r13d
	subq	%r11, %r8
	leaq	(%r10,%r9), %r12
	addq	%rsi, %r9
	cmpq	%rdx, %r11
	jnb	.L2404
	leaq	-1(%rdx), %rbx
	movd	%r8d, %xmm0
	movq	%rbx, %rcx
	subq	%r11, %rcx
	cmpq	$2, %rcx
	jbe	.L2441
	movq	%r8, %rcx
	pshufd	$0, %xmm0, %xmm0
	shrq	$2, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2406:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L2406
	movq	%r8, %rcx
	andq	$-4, %rcx
	leaq	(%r11,%rcx), %rax
	cmpq	%rcx, %r8
	je	.L2407
.L2405:
	leaq	1(%rax), %rcx
	movl	%r8d, (%r10,%rax,4)
	cmpq	%rcx, %rdx
	jbe	.L2407
	addq	$2, %rax
	movl	%r8d, (%r10,%rcx,4)
	cmpq	%rax, %rdx
	jbe	.L2407
	movl	%r8d, (%r10,%rax,4)
.L2407:
	movzbl	3048(%rdi), %ecx
	movl	$1, (%r12)
	movl	%r13d, (%r9)
	movq	3032(%rdi), %r9
	testb	%cl, %cl
	movb	%cl, -49(%rbp)
	movq	%rdx, %rcx
	leaq	(%r9,%rbx,2), %rax
	cmove	%r9, %rax
	movzwl	(%rax), %r15d
	leaq	1(%rdx), %rax
	.p2align 4,,10
	.p2align 3
.L2408:
	cmpb	$0, -49(%rbp)
	leaq	-1(%rcx), %rbx
	je	.L2410
	movzwl	(%r9,%rbx,2), %r14d
	cmpq	%rax, %rdx
	jb	.L2412
	.p2align 4,,10
	.p2align 3
.L2411:
	leaq	-1(%rax), %rdi
	cmpw	%r14w, -2(%r9,%rax,2)
	je	.L2444
.L2462:
	leaq	(%r10,%rax,4), %rdi
	movslq	(%rdi), %r13
	cmpq	%r8, %r13
	je	.L2460
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rdx, %rax
	jbe	.L2411
.L2445:
	movq	%rax, %r13
	subq	$1, %rax
.L2413:
	movl	%eax, (%rsi,%rbx,4)
	movl	%eax, %edi
	cmpq	%rax, %rdx
	je	.L2420
.L2446:
	movq	%rbx, %rcx
.L2421:
	cmpq	%r11, %rcx
	ja	.L2408
.L2409:
	cmpq	%rdx, %rax
	jnb	.L2403
	movq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L2434:
	movslq	(%r10,%rcx,4), %rdi
	cmpq	%r8, %rdi
	jne	.L2436
	movl	%eax, %edi
	subl	%r11d, %edi
	movl	%edi, (%r10,%rcx,4)
.L2436:
	cmpq	%rax, %rcx
	je	.L2461
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2434
.L2403:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2460:
	.cfi_restore_state
	movl	%eax, %r13d
	subl	%ecx, %r13d
	movl	%r13d, (%rdi)
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rax, %rdx
	jb	.L2445
	leaq	-1(%rax), %rdi
	cmpw	%r14w, -2(%r9,%rax,2)
	jne	.L2462
.L2444:
	movq	%rax, %r13
	movq	%rdi, %rax
	movl	%eax, (%rsi,%rbx,4)
	movl	%eax, %edi
	cmpq	%rax, %rdx
	jne	.L2446
.L2420:
	cmpq	%r11, %rbx
	jbe	.L2446
	cmpb	$0, -49(%rbp)
	leal	-1(%rdx), %r14d
	jne	.L2424
	movq	%rdx, %r14
	subq	%rcx, %r14
	leal	-1(%rdx), %ecx
	movl	%ecx, -56(%rbp)
	leaq	(%r9,%r14,2), %r14
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2426:
	movl	%edi, (%rsi,%rcx,4)
	addq	$2, %r14
	cmpq	%rcx, %r11
	jnb	.L2421
.L2427:
	movq	%rcx, %rbx
.L2428:
	leaq	-1(%rbx), %rcx
	cmpw	2(%r14), %r15w
	je	.L2425
	movslq	(%r12), %rbx
	cmpq	%rbx, %r8
	jne	.L2426
	movl	-56(%rbp), %ebx
	addq	$2, %r14
	subl	%ecx, %ebx
	movl	%ebx, (%r12)
	movl	%edi, (%rsi,%rcx,4)
	cmpq	%rcx, %r11
	jb	.L2427
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2410:
	movq	%rdx, %rdi
	subq	%rcx, %rdi
	movzwl	(%r9,%rdi,2), %r13d
	cmpq	%rax, %rdx
	jb	.L2412
	.p2align 4,,10
	.p2align 3
.L2417:
	movq	%rdx, %rdi
	leaq	-1(%rax), %r14
	subq	%rax, %rdi
	cmpw	%r13w, (%r9,%rdi,2)
	je	.L2442
	leaq	(%r10,%rax,4), %rdi
	movslq	(%rdi), %r14
	cmpq	%r14, %r8
	je	.L2463
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rdx, %rax
	jbe	.L2417
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2463:
	movl	%eax, %r14d
	subl	%ecx, %r14d
	movl	%r14d, (%rdi)
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rax, %rdx
	jnb	.L2417
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	%rax, %r13
	movq	%r14, %rax
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2432:
	movl	%edi, (%rsi,%rcx,4)
	cmpq	%r11, %rcx
	jbe	.L2409
.L2433:
	movq	%rcx, %rbx
.L2424:
	leaq	-1(%rbx), %rcx
	cmpw	%r15w, -2(%r9,%rbx,2)
	je	.L2425
	movslq	(%r12), %rbx
	cmpq	%r8, %rbx
	jne	.L2432
	movl	%r14d, %ebx
	subl	%ecx, %ebx
	movl	%ebx, (%r12)
	movl	%edi, (%rsi,%rcx,4)
	cmpq	%r11, %rcx
	ja	.L2433
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2461:
	movslq	(%rsi,%rcx,4), %rax
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	jnb	.L2434
	jmp	.L2403
.L2425:
	cmpq	%rbx, %r11
	jnb	.L2403
	leaq	-2(%r13), %rax
	movl	%eax, (%rsi,%rcx,4)
	jmp	.L2421
.L2412:
	movq	%rax, %r13
	subq	$1, %rax
	jmp	.L2413
.L2404:
	movl	$1, (%r12)
	movl	%r13d, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2441:
	.cfi_restore_state
	movq	%r11, %rax
	jmp	.L2405
	.cfi_endproc
.LFE9652:
	.size	_ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv, .-_ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv
	.section	.text._ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm,"axG",@progbits,_ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm
	.type	_ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm, @function
_ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm:
.LFB9451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	3040(%rdi), %r12
	movzbl	32(%rbp), %eax
	movzbl	3048(%rdi), %edx
	movq	3032(%rdi), %rbx
	leaq	-1(%r12), %rcx
	movq	24(%rbp), %r14
	movq	16(%rbp), %r10
	movb	%al, -56(%rbp)
	movl	%edx, %r11d
	movq	%r12, %rax
	movb	%dl, -65(%rbp)
	leaq	(%rbx,%rcx,2), %rdx
	negq	%rax
	testb	%r11b, %r11b
	movl	$1, %r11d
	cmove	%rbx, %rdx
	leaq	(%rax,%rax), %r13
	movzwl	(%rdx), %r9d
	leal	-1(%r12), %edx
	movzbl	%r9b, %r8d
	subl	(%rdi,%r8,4), %edx
	movq	%r14, %r8
	movl	%edx, -64(%rbp)
	leaq	1(%r14), %rdx
	subq	%r12, %r8
	subq	%r12, %rdx
	movq	%r8, %r15
	movq	%rdx, -96(%rbp)
	leaq	0(%r13,%r14,2), %rdx
	movq	%r12, %r13
	movq	%rdx, -104(%rbp)
	movslq	-64(%rbp), %rdx
	movq	%r12, -64(%rbp)
	subq	%rdx, %r13
	movq	%rdx, -80(%rbp)
	movq	%r13, -88(%rbp)
.L2476:
	cmpq	%r8, %rsi
	ja	.L2464
	cmpb	$0, -56(%rbp)
	jne	.L2467
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2497:
	movzbl	%dl, %edx
	movl	%ecx, %r13d
	subl	(%rdi,%rdx,4), %r13d
	movslq	%r13d, %r12
	movl	%r11d, %r13d
	subl	%r12d, %r13d
	addq	%r12, %rsi
	movslq	%r13d, %rdx
	addq	%rdx, %rax
	cmpq	%rsi, %r8
	jb	.L2464
.L2469:
	movq	%r15, %rdx
	subq	%rsi, %rdx
	movzwl	(%r10,%rdx,2), %edx
	cmpw	%dx, %r9w
	jne	.L2497
	movq	-64(%rbp), %rdx
	subq	$2, %rdx
	cmpb	$0, -65(%rbp)
	jne	.L2477
	movq	-104(%rbp), %r13
	movq	%rsi, %r12
	movq	%rax, -112(%rbp)
	negq	%r12
	leaq	0(%r13,%r12,2), %r13
	xorl	%r12d, %r12d
	addq	%r10, %r13
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2498:
	addq	$2, %r12
	testq	%rdx, %rdx
	je	.L2485
	subq	$1, %rdx
.L2472:
	movzwl	2(%rbx,%r12), %eax
	cmpw	%ax, 2(%r13,%r12)
	je	.L2498
.L2493:
	movq	-112(%rbp), %rax
.L2471:
	addq	-88(%rbp), %rax
	addq	-80(%rbp), %rsi
	subq	%rdx, %rax
	testq	%rax, %rax
	jle	.L2476
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN4node12stringsearch12StringSearchItE23PopulateBoyerMooreTableEv
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movl	$1, 3056(%rdi)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12stringsearch12StringSearchItE16BoyerMooreSearchENS0_6VectorIKtEEm
	.p2align 4,,10
	.p2align 3
.L2499:
	.cfi_restore_state
	movzbl	%dl, %edx
	movl	%ecx, %r13d
	subl	(%rdi,%rdx,4), %r13d
	movslq	%r13d, %r12
	movq	%r12, %rdx
	addq	%r12, %rsi
	movl	%r11d, %r12d
	subl	%edx, %r12d
	movslq	%r12d, %rdx
	addq	%rdx, %rax
	cmpq	%r8, %rsi
	ja	.L2464
.L2467:
	leaq	(%rcx,%rsi), %rdx
	movzwl	(%r10,%rdx,2), %edx
	cmpw	%r9w, %dx
	jne	.L2499
	movq	-64(%rbp), %rdx
	subq	$2, %rdx
	cmpb	$0, -65(%rbp)
	jne	.L2479
	movq	%rax, -112(%rbp)
	leaq	2(%rbx), %r12
	leaq	(%r10,%rsi,2), %r13
	jmp	.L2473
	.p2align 4,,10
	.p2align 3
.L2500:
	addq	$2, %r12
	testq	%rdx, %rdx
	je	.L2485
	subq	$1, %rdx
.L2473:
	movzwl	(%r12), %eax
	cmpw	%ax, 0(%r13,%rdx,2)
	je	.L2500
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	%rsi, %r14
.L2464:
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2477:
	.cfi_restore_state
	movq	-96(%rbp), %r12
	subq	%rsi, %r12
	leaq	(%r10,%r12,2), %r12
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2501:
	addq	$2, %r12
	testq	%rdx, %rdx
	je	.L2485
	subq	$1, %rdx
.L2474:
	movzwl	(%rbx,%rdx,2), %r13d
	cmpw	%r13w, (%r12)
	je	.L2501
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2479:
	leaq	(%r10,%rsi,2), %r12
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2502:
	testq	%rdx, %rdx
	je	.L2485
	subq	$1, %rdx
.L2475:
	movzwl	(%r12,%rdx,2), %r13d
	cmpw	%r13w, (%rbx,%rdx,2)
	je	.L2502
	jmp	.L2471
	.cfi_endproc
.LFE9451:
	.size	_ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm, .-_ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm
	.section	.text._ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m,"axG",@progbits,_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m,comdat
	.p2align 4
	.weak	_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m
	.type	_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m, @function
_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m:
.LFB8928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3256, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rbp), %r13
	movq	48(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	56(%rbp), %ecx
	movq	$0, -72(%rbp)
	movq	%r13, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movb	%cl, -88(%rbp)
	cmpq	$249, %rbx
	jbe	.L2504
	movdqu	16(%rbp), %xmm0
	movq	32(%rbp), %r15
	leaq	-250(%rbx), %rax
	leaq	0(,%rbx,4), %rsi
	movq	%rax, -72(%rbp)
	movq	$-10, %rax
	movaps	%xmm0, -3200(%rbp)
	movq	%xmm0, %r8
	subq	%rsi, %rax
	movq	-3192(%rbp), %r12
	movl	$2, -80(%rbp)
	movq	%r12, %r14
	movq	%r15, -3184(%rbp)
	subq	%rbx, %r14
	movq	%r15, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	cmpq	%r14, %rdi
	ja	.L2503
	addq	$1, %rax
	leaq	-1(%rbx), %rdi
	movq	%rax, -3248(%rbp)
	movq	%rdi, -3224(%rbp)
	testq	%rax, %rax
	jg	.L2514
.L2564:
	leaq	(%rbx,%rbx), %rsi
	movq	%r14, -3240(%rbp)
	leaq	-2(%r8,%rsi), %rax
	movb	%r15b, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	movq	-3224(%rbp), %rax
	movq	%rbx, -3256(%rbp)
	movq	%r12, %rbx
	addq	%rax, %rax
	testb	%cl, %cl
	movl	$0, %ecx
	movq	%r13, -3264(%rbp)
	cmovne	%rcx, %rax
	leaq	1(%r14), %rcx
	movq	%rcx, %r14
	movzwl	0(%r13,%rax), %r9d
	movl	%r9d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r9b
	movl	%eax, %r11d
	leaq	-4(%r13,%rsi), %rax
	movq	%rax, -3280(%rbp)
	leaq	0(%r13,%rsi), %rax
	cmova	%r9d, %r11d
	movq	%r8, %r13
	movq	%rax, -3272(%rbp)
	leaq	(%r12,%r12), %rax
	movq	%rax, -3288(%rbp)
	leaq	-1(%r12), %rax
	movzbl	%r11b, %r11d
	movl	%r9d, %r12d
	movq	%rax, -3208(%rbp)
	movl	%r11d, %r15d
.L2515:
	movzbl	-3216(%rbp), %eax
	movq	%r13, -3168(%rbp)
	movq	%rbx, -3160(%rbp)
	movb	%al, -3152(%rbp)
	testb	%al, %al
	jne	.L2516
	movl	%r15d, %eax
	movq	%r13, %r15
	movl	%eax, %r13d
.L2521:
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	(%rax,%rax), %r8
	cmpq	%rdx, %rbx
	jb	.L2517
	cmpq	%rdx, %rbx
	js	.L2518
	movq	-3232(%rbp), %rdi
	movq	%r8, %rdx
	movl	%r13d, %esi
	call	memrchr@PLT
	testq	%rax, %rax
	je	.L2622
	andq	$-2, %rax
	movq	-3208(%rbp), %rsi
	movq	%rax, %rdx
	subq	%r15, %rdx
	sarq	%rdx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	cmpw	%r12w, (%rax)
	je	.L2621
	addq	$1, %rdx
	cmpq	%r14, %rdx
	jb	.L2521
.L2622:
	movq	%rbx, %r12
.L2503:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2642
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2504:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L2643
	cmpq	$7, %rbx
	ja	.L2508
	cmpq	$1, %rbx
	je	.L2644
	movdqu	16(%rbp), %xmm1
	movq	32(%rbp), %r14
	movl	$3, -80(%rbp)
	movq	16(%rbp), %r8
	movaps	%xmm1, -3200(%rbp)
	movq	-3192(%rbp), %r15
	movq	%r14, -3184(%rbp)
	movq	%r15, %r11
	movb	%r14b, -3208(%rbp)
	subq	%rbx, %r11
	cmpq	%r11, %rdi
	ja	.L2641
	leaq	(%rbx,%rbx), %rax
	testb	%cl, %cl
	movl	$0, %ecx
	movq	%r8, %r12
	leaq	-2(%r8,%rax), %rsi
	movq	%r11, -3232(%rbp)
	leaq	1(%r11), %rdi
	movq	%rsi, -3224(%rbp)
	leaq	-2(%rbx,%rbx), %rsi
	cmove	%rsi, %rcx
	movq	%rbx, -3240(%rbp)
	movq	%r13, -3248(%rbp)
	movzwl	0(%r13,%rcx), %r10d
	movl	%r10d, %ecx
	movzbl	%ch, %esi
	leaq	-4(%r13,%rax), %rcx
	cmpb	%sil, %r10b
	movq	%rcx, -3264(%rbp)
	cmova	%r10d, %esi
	addq	%r13, %rax
	movq	%r15, %r13
	movq	%rax, -3256(%rbp)
	leaq	(%r15,%r15), %rax
	movq	%rax, -3272(%rbp)
	leaq	-1(%r15), %rax
	movzbl	%sil, %r14d
	movl	%r10d, %r15d
	movq	%rax, -3216(%rbp)
	movl	%r14d, %ebx
	movq	%rdi, %r14
.L2561:
	cmpb	$0, -3208(%rbp)
	jne	.L2542
	movl	%ebx, %eax
	movl	%r15d, %ebx
	movq	%r12, %r15
	movl	%eax, %r12d
.L2545:
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	(%rax,%rax), %r8
	cmpq	%rdx, %r13
	jb	.L2517
	cmpq	%rdx, %r13
	js	.L2518
	movq	-3224(%rbp), %rdi
	movq	%r8, %rdx
	movl	%r12d, %esi
	call	memrchr@PLT
	testq	%rax, %rax
	je	.L2629
	andq	$-2, %rax
	movq	-3216(%rbp), %rcx
	movq	%rax, %rdx
	subq	%r15, %rdx
	sarq	%rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	cmpw	(%rax), %bx
	je	.L2630
	addq	$1, %rdx
	cmpq	%rdx, %r14
	ja	.L2545
.L2629:
	movq	%r13, %r15
.L2641:
	movq	%r15, %r12
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	(%rax,%rax), %r8
	cmpq	%r14, %rdx
	ja	.L2546
	testq	%rax, %rax
	js	.L2547
	leaq	0(%r13,%rdx,2), %rdi
	movl	%r15d, %esi
	movq	%r8, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L2622
	andq	$-2, %rax
	movq	%rax, %rdx
	subq	%r13, %rdx
	sarq	%rdx
	movq	%rdx, %rsi
	cmpw	(%rax), %r12w
	je	.L2520
	addq	$1, %rdx
	cmpq	%r14, %rdx
	jb	.L2516
	jmp	.L2622
	.p2align 4,,10
	.p2align 3
.L2542:
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	(%rax,%rax), %r8
	cmpq	%rdx, %r14
	jb	.L2546
	testq	%rax, %rax
	js	.L2547
	leaq	(%r12,%rdx,2), %rdi
	movl	%ebx, %esi
	movq	%r8, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L2629
	andq	$-2, %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	sarq	%rdx
	movq	%rdx, %rdi
	cmpw	(%rax), %r15w
	je	.L2544
	addq	$1, %rdx
	cmpq	%rdx, %r14
	ja	.L2542
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2643:
	leaq	_ZZN4node12stringsearch12StringSearchItEC4ENS0_6VectorIKtEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2644:
	movzwl	0(%r13), %r13d
	movdqu	16(%rbp), %xmm2
	movl	$4, -80(%rbp)
	movq	32(%rbp), %rcx
	movq	16(%rbp), %rbx
	movl	%r13d, %eax
	movaps	%xmm2, -3200(%rbp)
	movq	-3192(%rbp), %r12
	movzbl	%ah, %eax
	movq	%rcx, -3184(%rbp)
	cmpb	%al, %r13b
	cmova	%r13d, %eax
	movzbl	%al, %r14d
	testb	%cl, %cl
	jne	.L2510
	leaq	-1(%r12), %r15
.L2511:
	movq	%r12, %rax
	subq	%rdx, %rax
	leaq	(%rax,%rax), %r8
	cmpq	%rdx, %r12
	jb	.L2517
	testq	%rax, %rax
	js	.L2518
	movq	%r8, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	memrchr@PLT
	testq	%rax, %rax
	je	.L2503
	andq	$-2, %rax
	movq	%r15, %rsi
	movq	%rax, %rdx
	subq	%rbx, %rdx
	sarq	%rdx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	cmpw	(%rax), %r13w
	je	.L2572
	addq	$1, %rdx
	cmpq	%rdx, %r12
	ja	.L2511
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	%r12, %rax
	subq	%rdx, %rax
	leaq	(%rax,%rax), %r8
	cmpq	%r12, %rdx
	ja	.L2546
	testq	%rax, %rax
	js	.L2547
	leaq	(%rbx,%rdx,2), %rdi
	movl	%r14d, %esi
	movq	%r8, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L2503
	andq	$-2, %rax
	movq	%rax, %rdx
	subq	%rbx, %rdx
	sarq	%rdx
	cmpw	(%rax), %r13w
	je	.L2572
	addq	$1, %rdx
	cmpq	%r12, %rdx
	jb	.L2510
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2546:
	leaq	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2517:
	leaq	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2518:
	leaq	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2547:
	leaq	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2621:
	movl	%r13d, %eax
	movq	%r15, %r13
	movl	%eax, %r15d
.L2520:
	cmpq	%rsi, %rbx
	je	.L2622
	cmpq	-3240(%rbp), %rsi
	ja	.L2645
	cmpb	$0, -88(%rbp)
	jne	.L2526
	cmpb	$0, -3216(%rbp)
	jne	.L2527
	movq	-3288(%rbp), %rcx
	movq	%rsi, %rax
	movq	-3256(%rbp), %r8
	xorl	%edx, %edx
	negq	%rax
	leaq	(%rcx,%rax,2), %rdi
	movq	-3272(%rbp), %rcx
	movl	$1, %eax
	addq	%r13, %rdi
	jmp	.L2529
	.p2align 4,,10
	.p2align 3
.L2646:
	addq	$1, %rax
	subq	$2, %rdx
	cmpq	%r8, %rax
	jnb	.L2528
.L2529:
	movzwl	-4(%rcx,%rdx), %r10d
	cmpw	%r10w, -4(%rdi,%rdx)
	je	.L2646
	.p2align 4,,10
	.p2align 3
.L2528:
	cmpq	-3256(%rbp), %rax
	je	.L2569
	addq	-3248(%rbp), %rax
	leaq	1(%rsi), %rdx
	cmpq	-3240(%rbp), %rdx
	ja	.L2622
	addq	$1, %rax
	movq	%rax, -3248(%rbp)
	testq	%rax, %rax
	jle	.L2515
	movq	%r13, %r8
	movq	%rbx, %r12
	movzbl	-3216(%rbp), %r15d
	movq	-3264(%rbp), %r13
	movq	-3256(%rbp), %rbx
.L2514:
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2647
	leal	-1(%rsi), %eax
	leaq	-3136(%rbp), %r9
	movd	%eax, %xmm0
	leaq	-2112(%rbp), %rcx
	movq	%r9, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L2537:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L2537
	cmpq	-3224(%rbp), %rsi
	jnb	.L2539
.L2536:
	cmpb	$0, -88(%rbp)
	jne	.L2627
	subq	%rsi, %rbx
	movq	-3224(%rbp), %rdi
	leaq	0(%r13,%rbx,2), %rax
	.p2align 4,,10
	.p2align 3
.L2540:
	movzbl	-2(%rax), %ecx
	subq	$2, %rax
	movl	%esi, -3136(%rbp,%rcx,4)
	addq	$1, %rsi
	cmpq	%rsi, %rdi
	jne	.L2540
.L2539:
	subq	$8, %rsp
	movq	%r12, -3160(%rbp)
	movq	%rdx, %rsi
	movq	%r9, %rdi
	movb	%r15b, -3152(%rbp)
	pushq	-3152(%rbp)
	pushq	-3160(%rbp)
	pushq	%r8
	movl	$0, -80(%rbp)
	call	_ZN4node12stringsearch12StringSearchItE24BoyerMooreHorspoolSearchENS0_6VectorIKtEEm
	addq	$32, %rsp
	movq	%rax, %r12
	jmp	.L2503
.L2630:
	movl	%r12d, %eax
	movq	%rcx, %rdi
	movq	%r15, %r12
	movl	%ebx, %r15d
	movl	%eax, %ebx
.L2544:
	cmpq	%rdi, %r13
	je	.L2631
	cmpq	%rdi, -3232(%rbp)
	jb	.L2648
	cmpb	$0, -88(%rbp)
	jne	.L2550
	cmpb	$0, -3208(%rbp)
	jne	.L2551
	movq	-3272(%rbp), %rcx
	movq	%rdi, %rax
	movl	$1, %edx
	movq	-3240(%rbp), %rsi
	negq	%rax
	leaq	(%rcx,%rax,2), %r11
	movq	-3256(%rbp), %rcx
	xorl	%eax, %eax
	addq	%r12, %r11
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2649:
	addq	$1, %rdx
	subq	$2, %rax
	cmpq	%rsi, %rdx
	je	.L2631
.L2553:
	movzwl	-4(%rcx,%rax), %r10d
	cmpw	%r10w, -4(%r11,%rax)
	je	.L2649
.L2552:
	leaq	1(%rdi), %rdx
	cmpq	%rdx, -3232(%rbp)
	jnb	.L2561
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2526:
	cmpb	$0, -3216(%rbp)
	jne	.L2531
	movq	%rbx, %rax
	movq	-3256(%rbp), %rdi
	movq	-3264(%rbp), %rcx
	subq	%rsi, %rax
	leaq	0(%r13,%rax,2), %rdx
	movl	$1, %eax
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2650:
	addq	$1, %rax
	subq	$2, %rdx
	cmpq	%rdi, %rax
	jnb	.L2528
.L2532:
	movzwl	(%rcx,%rax,2), %r10d
	cmpw	%r10w, -4(%rdx)
	je	.L2650
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2550:
	cmpb	$0, -3208(%rbp)
	jne	.L2555
	movq	%r13, %rax
	movq	-3240(%rbp), %rsi
	movq	-3248(%rbp), %rcx
	subq	%rdi, %rax
	leaq	(%r12,%rax,2), %rdx
	movl	$1, %eax
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2651:
	addq	$1, %rax
	subq	$2, %rdx
	cmpq	%rsi, %rax
	je	.L2631
.L2556:
	movzwl	(%rcx,%rax,2), %r9d
	cmpw	%r9w, -4(%rdx)
	je	.L2651
	jmp	.L2552
	.p2align 4,,10
	.p2align 3
.L2631:
	movq	%rdi, %r12
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	%rdx, %r12
	jmp	.L2503
.L2531:
	movq	-3256(%rbp), %rdi
	leaq	0(%r13,%rsi,2), %rdx
	movl	$1, %eax
	movq	-3264(%rbp), %rcx
	jmp	.L2533
	.p2align 4,,10
	.p2align 3
.L2652:
	addq	$1, %rax
	cmpq	%rdi, %rax
	jnb	.L2528
.L2533:
	movzwl	(%rdx,%rax,2), %r9d
	cmpw	%r9w, (%rcx,%rax,2)
	je	.L2652
	jmp	.L2528
.L2527:
	movq	-3280(%rbp), %rdx
	leaq	0(%r13,%rsi,2), %rdi
	movl	$1, %eax
	movq	-3256(%rbp), %rcx
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2653:
	addq	$1, %rax
	subq	$2, %rdx
	cmpq	%rax, %rcx
	jbe	.L2528
.L2530:
	movzwl	(%rdx), %r9d
	cmpw	%r9w, (%rdi,%rax,2)
	je	.L2653
	jmp	.L2528
.L2551:
	movq	-3264(%rbp), %rdx
	movq	-3240(%rbp), %rcx
	leaq	(%r12,%rdi,2), %r11
	movl	$1, %eax
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2654:
	addq	$1, %rax
	subq	$2, %rdx
	cmpq	%rax, %rcx
	je	.L2631
.L2554:
	movzwl	(%r11,%rax,2), %esi
	cmpw	%si, (%rdx)
	je	.L2654
	jmp	.L2552
.L2555:
	movq	-3240(%rbp), %rsi
	movq	-3248(%rbp), %rcx
	leaq	(%r12,%rdi,2), %rdx
	movl	$1, %eax
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2655:
	addq	$1, %rax
	cmpq	%rsi, %rax
	je	.L2631
.L2557:
	movzwl	(%rdx,%rax,2), %r10d
	cmpw	%r10w, (%rcx,%rax,2)
	je	.L2655
	jmp	.L2552
.L2645:
	leaq	_ZZN4node12stringsearch12StringSearchItE13InitialSearchENS0_6VectorIKtEEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2648:
	leaq	_ZZN4node12stringsearch12StringSearchItE12LinearSearchENS0_6VectorIKtEEmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2627:
	movq	-3224(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L2538:
	movzbl	0(%r13,%rsi,2), %eax
	movl	%esi, -3136(%rbp,%rax,4)
	addq	$1, %rsi
	cmpq	%rcx, %rsi
	jne	.L2538
	jmp	.L2539
.L2647:
	leaq	-3136(%rbp), %r9
	movl	$128, %ecx
	movq	$-1, %rax
	movq	%r9, %rdi
	rep stosq
	jmp	.L2536
.L2569:
	movq	%rsi, %r12
	jmp	.L2503
.L2642:
	call	__stack_chk_fail@PLT
.L2508:
	movdqu	16(%rbp), %xmm0
	movq	32(%rbp), %r15
	leaq	0(,%rbx,4), %rsi
	movq	$-10, %rax
	movl	$2, -80(%rbp)
	subq	%rsi, %rax
	movaps	%xmm0, -3200(%rbp)
	movq	-3192(%rbp), %r12
	movq	%xmm0, %r8
	movq	%r15, -3184(%rbp)
	movq	%r12, %r14
	movq	%r15, -3152(%rbp)
	subq	%rbx, %r14
	movaps	%xmm0, -3168(%rbp)
	cmpq	%r14, %rdi
	ja	.L2503
	addq	$1, %rax
	movq	%rax, -3248(%rbp)
	leaq	-1(%rbx), %rax
	movq	%rax, -3224(%rbp)
	jmp	.L2564
	.cfi_endproc
.LFE8928:
	.size	_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m, .-_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m
	.text
	.p2align 4
	.type	_ZN4node12SearchStringItEEmPKT_mS3_mmb.part.0, @function
_ZN4node12SearchStringItEEmPKT_mS3_mmb.part.0:
.LFB9935:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rcx, -72(%rbp)
	movb	%r9b, -64(%rbp)
	testq	%rcx, %rcx
	je	.L2659
	testq	%rdx, %rdx
	je	.L2659
	movq	%rsi, -40(%rbp)
	movq	%rsi, %rbx
	movb	%r9b, -32(%rbp)
	testq	%rdi, %rdi
	je	.L2659
	testb	%r9b, %r9b
	jne	.L2660
	movq	%rsi, %r12
	pushq	-64(%rbp)
	subq	%rcx, %r12
	pushq	-72(%rbp)
	movq	%r12, %rax
	pushq	%rdx
	subq	%r8, %rax
	cmpq	%r8, %r12
	movl	$0, %r8d
	pushq	-32(%rbp)
	cmovnb	%rax, %r8
	pushq	-40(%rbp)
	pushq	%rdi
	movq	%r8, %rdi
	call	_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m
	addq	$48, %rsp
	subq	%rax, %r12
	cmpq	%rax, %rbx
	cmovne	%r12, %rax
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2660:
	.cfi_restore_state
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	%rdx
	pushq	-32(%rbp)
	pushq	-40(%rbp)
	pushq	%rdi
	movq	%r8, %rdi
	call	_ZN4node12stringsearch12SearchStringItEEmNS0_6VectorIKT_EES5_m
	addq	$48, %rsp
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2659:
	.cfi_restore_state
	leaq	_ZZN4node12stringsearch6VectorIKtEC4EPS2_mbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9935:
	.size	_ZN4node12SearchStringItEEmPKT_mS3_mmb.part.0, .-_ZN4node12SearchStringItEEmPKT_mS3_mmb.part.0
	.section	.text._ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv
	.type	_ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv, @function
_ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv:
.LFB9656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1024(%rdi), %rax
	leaq	2028(%rdi), %rsi
	movq	%rax, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	3064(%rdi), %r11
	movq	3040(%rdi), %rdx
	leaq	0(,%r11,4), %rcx
	leaq	0(,%rdx,4), %r9
	movq	%rdx, %r8
	subq	%rcx, %r10
	subq	%rcx, %rsi
	leal	1(%rdx), %r12d
	subq	%r11, %r8
	leaq	(%r10,%r9), %r13
	addq	%rsi, %r9
	cmpq	%rdx, %r11
	jnb	.L2670
	leaq	-1(%rdx), %rbx
	movd	%r8d, %xmm0
	movq	%rbx, %rcx
	subq	%r11, %rcx
	cmpq	$2, %rcx
	jbe	.L2707
	movq	%r8, %rcx
	pshufd	$0, %xmm0, %xmm0
	shrq	$2, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L2672:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L2672
	movq	%r8, %rcx
	andq	$-4, %rcx
	leaq	(%r11,%rcx), %rax
	cmpq	%rcx, %r8
	je	.L2673
.L2671:
	leaq	1(%rax), %rcx
	movl	%r8d, (%r10,%rax,4)
	cmpq	%rcx, %rdx
	jbe	.L2673
	addq	$2, %rax
	movl	%r8d, (%r10,%rcx,4)
	cmpq	%rdx, %rax
	jnb	.L2673
	movl	%r8d, (%r10,%rax,4)
.L2673:
	movzbl	3048(%rdi), %eax
	movl	$1, 0(%r13)
	movq	%rdx, %rcx
	movl	%r12d, (%r9)
	movq	3032(%rdi), %r9
	movb	%al, -49(%rbp)
	addq	%r9, %rbx
	testb	%al, %al
	leaq	1(%rdx), %rax
	cmove	%r9, %rbx
	movzbl	(%rbx), %r15d
	.p2align 4,,10
	.p2align 3
.L2674:
	cmpb	$0, -49(%rbp)
	leaq	-1(%rcx), %rbx
	jne	.L2676
	movq	%rdx, %rdi
	subq	%rcx, %rdi
	movzbl	(%r9,%rdi), %r12d
	cmpq	%rax, %rdx
	jb	.L2677
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	%rdx, %rdi
	leaq	-1(%rax), %r14
	subq	%rax, %rdi
	cmpb	%r12b, (%r9,%rdi)
	je	.L2708
.L2729:
	leaq	(%r10,%rax,4), %rdi
	movslq	(%rdi), %r14
	cmpq	%r14, %r8
	je	.L2727
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rdx, %rax
	jbe	.L2682
.L2711:
	movq	%rax, %r12
	subq	$1, %rax
.L2678:
	movl	%eax, (%rsi,%rbx,4)
	movl	%eax, %edi
	cmpq	%rax, %rdx
	je	.L2685
.L2712:
	movq	%rbx, %rcx
.L2686:
	cmpq	%r11, %rcx
	ja	.L2674
.L2675:
	cmpq	%rdx, %rax
	jnb	.L2669
	movq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L2700:
	movslq	(%r10,%rcx,4), %rdi
	cmpq	%r8, %rdi
	jne	.L2702
	movl	%eax, %edi
	subl	%r11d, %edi
	movl	%edi, (%r10,%rcx,4)
.L2702:
	cmpq	%rax, %rcx
	je	.L2728
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	jbe	.L2700
.L2669:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2727:
	.cfi_restore_state
	movl	%eax, %r14d
	subl	%ecx, %r14d
	movl	%r14d, (%rdi)
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rax, %rdx
	jb	.L2711
	movq	%rdx, %rdi
	leaq	-1(%rax), %r14
	subq	%rax, %rdi
	cmpb	%r12b, (%r9,%rdi)
	jne	.L2729
.L2708:
	movq	%rax, %r12
	movq	%r14, %rax
	movl	%eax, (%rsi,%rbx,4)
	movl	%eax, %edi
	cmpq	%rax, %rdx
	jne	.L2712
.L2685:
	cmpq	%r11, %rbx
	jbe	.L2712
	cmpb	$0, -49(%rbp)
	leal	-1(%rdx), %r14d
	jne	.L2690
	movq	%rdx, %r14
	subq	%rcx, %r14
	leal	-1(%rdx), %ecx
	movl	%ecx, -56(%rbp)
	addq	%r9, %r14
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2692:
	movl	%edi, (%rsi,%rcx,4)
	addq	$1, %r14
	cmpq	%rcx, %r11
	jnb	.L2686
.L2693:
	movq	%rcx, %rbx
.L2694:
	leaq	-1(%rbx), %rcx
	cmpb	1(%r14), %r15b
	je	.L2691
	movslq	0(%r13), %rbx
	cmpq	%rbx, %r8
	jne	.L2692
	movl	-56(%rbp), %ebx
	addq	$1, %r14
	subl	%ecx, %ebx
	movl	%ebx, 0(%r13)
	movl	%edi, (%rsi,%rcx,4)
	cmpq	%rcx, %r11
	jb	.L2693
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2676:
	movzbl	-1(%r9,%rcx), %r14d
	cmpq	%rax, %rdx
	jb	.L2677
.L2688:
	leaq	-1(%rax), %rdi
	cmpb	%r14b, -1(%r9,%rax)
	je	.L2710
.L2731:
	leaq	(%r10,%rax,4), %rdi
	movslq	(%rdi), %r12
	cmpq	%r8, %r12
	je	.L2730
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rdx, %rax
	ja	.L2711
	leaq	-1(%rax), %rdi
	cmpb	%r14b, -1(%r9,%rax)
	jne	.L2731
.L2710:
	movq	%rax, %r12
	movq	%rdi, %rax
	jmp	.L2678
	.p2align 4,,10
	.p2align 3
.L2730:
	movl	%eax, %r12d
	subl	%ecx, %r12d
	movl	%r12d, (%rdi)
	movslq	(%rsi,%rax,4), %rax
	cmpq	%rax, %rdx
	jnb	.L2688
	jmp	.L2711
	.p2align 4,,10
	.p2align 3
.L2698:
	movl	%edi, (%rsi,%rcx,4)
	cmpq	%r11, %rcx
	jbe	.L2675
.L2699:
	movq	%rcx, %rbx
.L2690:
	leaq	-1(%rbx), %rcx
	cmpb	%r15b, -1(%r9,%rbx)
	je	.L2691
	movslq	0(%r13), %rbx
	cmpq	%r8, %rbx
	jne	.L2698
	movl	%r14d, %ebx
	subl	%ecx, %ebx
	movl	%ebx, 0(%r13)
	movl	%edi, (%rsi,%rcx,4)
	cmpq	%r11, %rcx
	ja	.L2699
	jmp	.L2675
	.p2align 4,,10
	.p2align 3
.L2728:
	movslq	(%rsi,%rcx,4), %rax
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	jnb	.L2700
	jmp	.L2669
.L2691:
	cmpq	%rbx, %r11
	jnb	.L2669
	leaq	-2(%r12), %rax
	movl	%eax, (%rsi,%rcx,4)
	jmp	.L2686
.L2677:
	movq	%rax, %r12
	subq	$1, %rax
	jmp	.L2678
.L2670:
	movl	$1, 0(%r13)
	movl	%r12d, (%r9)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2707:
	.cfi_restore_state
	movq	%r11, %rax
	jmp	.L2671
	.cfi_endproc
.LFE9656:
	.size	_ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv, .-_ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv
	.section	.text._ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm,"axG",@progbits,_ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm
	.type	_ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm, @function
_ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm:
.LFB9456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	32(%rbp), %eax
	movq	3040(%rdi), %r13
	movq	3032(%rdi), %rbx
	movq	24(%rbp), %r10
	movb	%al, -64(%rbp)
	movzbl	3048(%rdi), %eax
	movq	%r13, %rdx
	movq	%r13, %r15
	leaq	-1(%r13), %r8
	negq	%rdx
	movq	16(%rbp), %r11
	movq	%r13, -80(%rbp)
	movl	%eax, %ecx
	movb	%al, -65(%rbp)
	leaq	(%rbx,%r8), %rax
	testb	%cl, %cl
	movq	%r10, -56(%rbp)
	leaq	(%r11,%r8), %r12
	cmove	%rbx, %rax
	movzbl	(%rax), %ecx
	leal	-1(%r13), %eax
	subl	(%rdi,%rcx,4), %eax
	movq	%rcx, %r9
	movq	%r10, %rcx
	movl	$1, %r10d
	cltq
	subq	%r13, %rcx
	subq	%rax, %r15
	movq	%rax, -88(%rbp)
	movq	%rcx, %r14
	movq	%r15, -96(%rbp)
.L2744:
	cmpq	%rcx, %rsi
	ja	.L2732
	cmpb	$0, -64(%rbp)
	jne	.L2735
	jmp	.L2737
	.p2align 4,,10
	.p2align 3
.L2763:
	movl	%r8d, %r15d
	subl	(%rdi,%rax,4), %r15d
	movslq	%r15d, %r13
	movl	%r10d, %r15d
	subl	%r13d, %r15d
	addq	%r13, %rsi
	movslq	%r15d, %rax
	addq	%rax, %rdx
	cmpq	%rsi, %rcx
	jb	.L2732
.L2737:
	movq	%r14, %rax
	subq	%rsi, %rax
	movzbl	(%r11,%rax), %eax
	cmpb	%al, %r9b
	jne	.L2763
	movq	-80(%rbp), %rax
	subq	$2, %rax
	cmpb	$0, -65(%rbp)
	jne	.L2764
	movq	%rcx, %r15
	movq	%rdx, -104(%rbp)
	xorl	%r13d, %r13d
	subq	%rsi, %r15
	addq	%r11, %r15
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2765:
	addq	$1, %r13
	testq	%rax, %rax
	je	.L2750
	subq	$1, %rax
.L2740:
	movzbl	1(%rbx,%r13), %edx
	cmpb	%dl, 1(%r13,%r15)
	je	.L2765
.L2760:
	movq	-104(%rbp), %rdx
.L2739:
	addq	-96(%rbp), %rdx
	addq	-88(%rbp), %rsi
	subq	%rax, %rdx
	testq	%rdx, %rdx
	jle	.L2744
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN4node12stringsearch12StringSearchIhE23PopulateBoyerMooreTableEv
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movl	$1, 3056(%rdi)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12stringsearch12StringSearchIhE16BoyerMooreSearchENS0_6VectorIKhEEm
	.p2align 4,,10
	.p2align 3
.L2766:
	.cfi_restore_state
	movl	%r8d, %r15d
	subl	(%rdi,%rax,4), %r15d
	movslq	%r15d, %r13
	movl	%r10d, %r15d
	subl	%r13d, %r15d
	addq	%r13, %rsi
	movslq	%r15d, %rax
	addq	%rax, %rdx
	cmpq	%rcx, %rsi
	ja	.L2732
.L2735:
	movzbl	(%r12,%rsi), %eax
	cmpb	%r9b, %al
	jne	.L2766
	movq	-80(%rbp), %rax
	leaq	(%r11,%rsi), %r15
	subq	$2, %rax
	cmpb	$0, -65(%rbp)
	jne	.L2743
	movq	%rdx, -104(%rbp)
	leaq	1(%rbx), %r13
	leaq	(%r11,%rsi), %r15
	jmp	.L2741
	.p2align 4,,10
	.p2align 3
.L2767:
	addq	$1, %r13
	testq	%rax, %rax
	je	.L2750
	subq	$1, %rax
.L2741:
	movzbl	0(%r13), %edx
	cmpb	%dl, (%r15,%rax)
	je	.L2767
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2750:
	movq	%rsi, -56(%rbp)
.L2732:
	movq	-56(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2768:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L2750
	subq	$1, %rax
.L2743:
	movzbl	(%r15,%rax), %r13d
	cmpb	%r13b, (%rbx,%rax)
	je	.L2768
	jmp	.L2739
	.p2align 4,,10
	.p2align 3
.L2764:
	movq	-56(%rbp), %r15
	movq	%rdx, -104(%rbp)
	subq	%rsi, %r15
	addq	%r11, %r15
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2769:
	testq	%rax, %rax
	je	.L2750
	subq	$1, %rax
.L2742:
	movq	%rax, %r13
	movzbl	(%rbx,%rax), %edx
	negq	%r13
	cmpb	%dl, -1(%r15,%r13)
	je	.L2769
	jmp	.L2760
	.cfi_endproc
.LFE9456:
	.size	_ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm, .-_ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm
	.section	.text._ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m,"axG",@progbits,_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m,comdat
	.p2align 4
	.weak	_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m
	.type	_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m, @function
_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m:
.LFB8932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3240, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rbp), %r14
	movq	48(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	56(%rbp), %eax
	movq	$0, -72(%rbp)
	movq	%r14, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movb	%al, -88(%rbp)
	cmpq	$249, %rbx
	jbe	.L2771
	movdqu	16(%rbp), %xmm0
	movq	32(%rbp), %r9
	leaq	-250(%rbx), %rdx
	movq	$-10, %rcx
	movq	%rdx, -72(%rbp)
	leaq	0(,%rbx,4), %rdx
	movaps	%xmm0, -3200(%rbp)
	movq	%xmm0, %r10
	movl	%r9d, %r13d
	subq	%rdx, %rcx
	movq	-3192(%rbp), %r12
	movl	$2, -80(%rbp)
	movq	%r9, -3184(%rbp)
	movq	%r12, %r15
	movq	%r9, -3152(%rbp)
	subq	%rbx, %r15
	movaps	%xmm0, -3168(%rbp)
	cmpq	%r15, %rdi
	ja	.L2770
	leaq	-1(%rbx), %rdi
	addq	$1, %rcx
	movq	%rdi, -3272(%rbp)
	testq	%rcx, %rcx
	jg	.L2781
.L2824:
	leaq	1(%r15), %rdi
	testb	%al, %al
	movl	$0, %eax
	movq	%r12, %r9
	movq	%rdi, -3216(%rbp)
	movq	-3272(%rbp), %rdi
	movq	%r15, -3208(%rbp)
	movq	%r14, %r15
	cmove	%rdi, %rax
	movzbl	(%r14,%rax), %eax
	movl	%eax, -3220(%rbp)
	leaq	(%r10,%rdi), %rax
	movq	%rax, -3240(%rbp)
	leaq	-1(%r12), %rax
	movl	%r13d, %r12d
	movq	%rcx, %r13
	movq	%rax, -3248(%rbp)
	leaq	-2(%rbx), %rax
	movq	%rax, -3256(%rbp)
	leaq	(%r14,%rbx), %rax
	movq	%r10, %r14
	movq	%rax, -3264(%rbp)
	.p2align 4,,10
	.p2align 3
.L2782:
	movq	-3216(%rbp), %rdx
	movq	%r14, -3168(%rbp)
	movq	%r9, -3160(%rbp)
	movb	%r12b, -3152(%rbp)
	subq	%rsi, %rdx
	movq	%r9, -3232(%rbp)
	testb	%r12b, %r12b
	je	.L2783
	leaq	(%r14,%rsi), %rdi
	movl	-3220(%rbp), %esi
	call	memchr@PLT
	movq	-3232(%rbp), %r9
	testq	%rax, %rax
	je	.L2875
	subq	%r14, %rax
.L2821:
	cmpq	%rax, %r9
	je	.L2875
	cmpq	%rax, -3208(%rbp)
	jb	.L2886
	cmpb	$0, -88(%rbp)
	jne	.L2788
	testb	%r12b, %r12b
	jne	.L2789
	movq	%r9, %rdi
	movq	-3264(%rbp), %rcx
	xorl	%esi, %esi
	movl	$1, %edx
	subq	%rax, %rdi
	addq	%r14, %rdi
	jmp	.L2792
	.p2align 4,,10
	.p2align 3
.L2887:
	addq	$1, %rdx
	subq	$1, %rsi
	cmpq	%rbx, %rdx
	jnb	.L2796
.L2792:
	movzbl	-2(%rsi,%rdi), %r11d
	cmpb	%r11b, -2(%rsi,%rcx)
	je	.L2887
	.p2align 4,,10
	.p2align 3
.L2796:
	cmpq	%rbx, %rdx
	je	.L2829
	leaq	0(%r13,%rdx), %rcx
	leaq	1(%rax), %rsi
	cmpq	%rsi, -3208(%rbp)
	jb	.L2875
	leaq	1(%rcx), %r13
	testq	%r13, %r13
	jle	.L2782
	movl	%r12d, %r13d
	movq	%r14, %r10
	movq	%r9, %r12
	movq	%r15, %r14
.L2781:
	movq	-72(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2888
	leal	-1(%rdx), %eax
	leaq	-3136(%rbp), %r11
	movd	%eax, %xmm0
	leaq	-2112(%rbp), %rcx
	movq	%r11, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L2800:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L2800
	cmpq	%rdx, -3272(%rbp)
	jbe	.L2801
.L2799:
	cmpb	$0, -88(%rbp)
	jne	.L2880
	movq	%rbx, %rax
	leaq	1(%r14), %r8
	leal	(%rbx,%r14), %edi
	subq	%rdx, %rax
	addq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L2804:
	movzbl	-1(%rax), %edx
	movl	%edi, %ecx
	subl	%eax, %ecx
	subq	$1, %rax
	movl	%ecx, -3136(%rbp,%rdx,4)
	cmpq	%rax, %r8
	jne	.L2804
.L2801:
	subq	$8, %rsp
	movq	%r12, -3160(%rbp)
	movq	%r11, %rdi
	movb	%r13b, -3152(%rbp)
	pushq	-3152(%rbp)
	pushq	-3160(%rbp)
	pushq	%r10
	movl	$0, -80(%rbp)
	call	_ZN4node12stringsearch12StringSearchIhE24BoyerMooreHorspoolSearchENS0_6VectorIKhEEm
	addq	$32, %rsp
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2770:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2889
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2771:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L2890
	cmpq	$7, %rbx
	ja	.L2775
	cmpq	$1, %rbx
	je	.L2891
	movdqu	16(%rbp), %xmm1
	movq	32(%rbp), %r9
	movl	$3, -80(%rbp)
	movq	16(%rbp), %rcx
	movaps	%xmm1, -3200(%rbp)
	movq	-3192(%rbp), %rdi
	movq	%r9, -3184(%rbp)
	movq	%rdi, -3208(%rbp)
	subq	%rbx, %rdi
	movq	%rdi, %r13
	cmpq	%rdi, %rsi
	ja	.L2807
	testb	%al, %al
	leaq	-1(%rbx), %rdx
	movl	$0, %eax
	movq	%r14, %r12
	cmove	%rdx, %rax
	leaq	1(%rdi), %rdi
	leaq	(%r14,%rbx), %r15
	movq	%rbx, -3264(%rbp)
	movq	%rdi, -3216(%rbp)
	movzbl	(%r14,%rax), %eax
	movl	%eax, -3220(%rbp)
	leaq	(%rcx,%rdx), %rax
	movq	%rax, -3232(%rbp)
	movq	-3208(%rbp), %rax
	subq	$1, %rax
	movq	%rax, -3240(%rbp)
	leaq	-2(%r14,%rbx), %rax
	movl	%r9d, %r14d
	movq	%rax, -3248(%rbp)
	movl	$1, %eax
	subq	%rbx, %rax
	movq	%rcx, %rbx
	movq	%rax, -3256(%rbp)
	.p2align 4,,10
	.p2align 3
.L2822:
	movq	-3216(%rbp), %rdx
	subq	%rsi, %rdx
	testb	%r14b, %r14b
	je	.L2806
	leaq	(%rbx,%rsi), %rdi
	movl	-3220(%rbp), %esi
	call	memchr@PLT
	testq	%rax, %rax
	je	.L2807
	subq	%rbx, %rax
.L2823:
	cmpq	-3208(%rbp), %rax
	je	.L2881
	cmpq	%rax, %r13
	jb	.L2892
	cmpb	$0, -88(%rbp)
	jne	.L2811
	testb	%r14b, %r14b
	jne	.L2812
	movq	-3208(%rbp), %rsi
	movq	-3256(%rbp), %rcx
	xorl	%edx, %edx
	subq	%rax, %rsi
	addq	%rbx, %rsi
	jmp	.L2814
	.p2align 4,,10
	.p2align 3
.L2869:
	subq	$1, %rdx
	cmpq	%rcx, %rdx
	je	.L2881
.L2814:
	movzbl	-2(%rdx,%rsi), %edi
	cmpb	%dil, -2(%rdx,%r15)
	je	.L2869
.L2885:
	leaq	1(%rax), %rsi
.L2813:
	cmpq	%rsi, %r13
	jnb	.L2822
	.p2align 4,,10
	.p2align 3
.L2807:
	movq	-3208(%rbp), %r12
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2806:
	movl	-3220(%rbp), %esi
	movq	-3232(%rbp), %rdi
	call	memrchr@PLT
	testq	%rax, %rax
	je	.L2807
	movq	-3240(%rbp), %rcx
	subq	%rbx, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	jmp	.L2823
	.p2align 4,,10
	.p2align 3
.L2811:
	testb	%r14b, %r14b
	jne	.L2832
	movq	-3208(%rbp), %rsi
	leaq	1(%r12), %rdx
	subq	%rax, %rsi
	addq	%rbx, %rsi
	jmp	.L2817
	.p2align 4,,10
	.p2align 3
.L2870:
	addq	$1, %rdx
	subq	$1, %rsi
	cmpq	%r15, %rdx
	je	.L2881
.L2817:
	movzbl	-2(%rsi), %edi
	cmpb	%dil, (%rdx)
	je	.L2870
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2812:
	leaq	1(%rax), %rsi
	movq	-3248(%rbp), %rdx
	leaq	(%rbx,%rsi), %rdi
	jmp	.L2815
	.p2align 4,,10
	.p2align 3
.L2831:
	movq	%rcx, %rdx
.L2815:
	movzbl	(%rdi), %ecx
	cmpb	%cl, (%rdx)
	jne	.L2813
	leaq	-1(%rdx), %rcx
	addq	$1, %rdi
	cmpq	%r12, %rdx
	jne	.L2831
.L2881:
	movq	%rax, %r12
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2832:
	movq	-3264(%rbp), %rcx
	movl	$1, %edx
	leaq	(%rbx,%rax), %rsi
	jmp	.L2816
	.p2align 4,,10
	.p2align 3
.L2871:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L2881
.L2816:
	movzbl	(%rsi,%rdx), %edi
	cmpb	%dil, (%r12,%rdx)
	je	.L2871
	jmp	.L2885
	.p2align 4,,10
	.p2align 3
.L2890:
	leaq	_ZZN4node12stringsearch12StringSearchIhEC4ENS0_6VectorIKhEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2783:
	movl	-3220(%rbp), %esi
	movq	-3240(%rbp), %rdi
	call	memrchr@PLT
	movq	-3232(%rbp), %r9
	testq	%rax, %rax
	je	.L2875
	movq	-3248(%rbp), %rdi
	subq	%r14, %rax
	subq	%rax, %rdi
	movq	%rdi, %rax
	jmp	.L2821
	.p2align 4,,10
	.p2align 3
.L2788:
	testb	%r12b, %r12b
	jne	.L2828
	movq	%r9, %rsi
	movl	$1, %edx
	subq	%rax, %rsi
	addq	%r14, %rsi
	jmp	.L2795
	.p2align 4,,10
	.p2align 3
.L2893:
	addq	$1, %rdx
	subq	$1, %rsi
	cmpq	%rdx, %rbx
	jbe	.L2796
.L2795:
	movzbl	-2(%rsi), %ecx
	cmpb	%cl, (%r15,%rdx)
	je	.L2893
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2789:
	movq	-3256(%rbp), %rcx
	movl	$1, %edx
	leaq	(%r14,%rax), %rdi
	leaq	(%r15,%rcx), %rsi
	jmp	.L2793
	.p2align 4,,10
	.p2align 3
.L2894:
	addq	$1, %rdx
	subq	$1, %rsi
	cmpq	%rdx, %rbx
	jbe	.L2796
.L2793:
	movzbl	(%rsi), %ecx
	cmpb	%cl, (%rdi,%rdx)
	je	.L2894
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2828:
	movl	$1, %edx
	leaq	(%r14,%rax), %rdi
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2895:
	addq	$1, %rdx
	cmpq	%rbx, %rdx
	jnb	.L2796
.L2794:
	movzbl	(%rdi,%rdx), %ecx
	cmpb	%cl, (%r15,%rdx)
	je	.L2895
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2875:
	movq	%r9, %r12
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2891:
	movdqu	16(%rbp), %xmm2
	movq	32(%rbp), %rax
	movl	$4, -80(%rbp)
	movq	16(%rbp), %rbx
	movzbl	(%r14), %r8d
	movaps	%xmm2, -3200(%rbp)
	movq	-3192(%rbp), %r12
	movq	%rax, -3184(%rbp)
	movq	%r12, %rdx
	subq	%rdi, %rdx
	testb	%al, %al
	je	.L2896
	leaq	(%rbx,%rdi), %rdi
	movl	%r8d, %esi
	call	memchr@PLT
	movq	%rax, %rdx
	subq	%rbx, %rdx
	testq	%rax, %rax
	cmovne	%rdx, %r12
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2886:
	leaq	_ZZN4node12stringsearch12StringSearchIhE13InitialSearchENS0_6VectorIKhEEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2892:
	leaq	_ZZN4node12stringsearch12StringSearchIhE12LinearSearchENS0_6VectorIKhEEmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2896:
	movl	%r8d, %esi
	movq	%rbx, %rdi
	call	memrchr@PLT
	testq	%rax, %rax
	je	.L2770
	subq	$1, %r12
	subq	%rbx, %rax
	subq	%rax, %r12
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2829:
	movq	%rax, %r12
	jmp	.L2770
.L2880:
	movq	-3272(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L2802:
	movzbl	(%r14,%rdx), %eax
	movl	%edx, -3136(%rbp,%rax,4)
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L2802
	jmp	.L2801
.L2888:
	leaq	-3136(%rbp), %r11
	movl	$128, %ecx
	movq	$-1, %rax
	movq	%r11, %rdi
	rep stosq
	jmp	.L2799
.L2889:
	call	__stack_chk_fail@PLT
.L2775:
	movdqu	16(%rbp), %xmm0
	movq	32(%rbp), %r9
	leaq	0(,%rbx,4), %rdx
	movq	$-10, %rcx
	movl	$2, -80(%rbp)
	subq	%rdx, %rcx
	movaps	%xmm0, -3200(%rbp)
	movq	-3192(%rbp), %r12
	movq	%xmm0, %r10
	movl	%r9d, %r13d
	movq	%r9, -3184(%rbp)
	movq	%r12, %r15
	movq	%r9, -3152(%rbp)
	subq	%rbx, %r15
	movaps	%xmm0, -3168(%rbp)
	cmpq	%r15, %rdi
	ja	.L2770
	leaq	-1(%rbx), %rdi
	addq	$1, %rcx
	movq	%rdi, -3272(%rbp)
	jmp	.L2824
	.cfi_endproc
.LFE8932:
	.size	_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m, .-_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m
	.text
	.p2align 4
	.type	_ZN4node12SearchStringIhEEmPKT_mS3_mmb.part.0, @function
_ZN4node12SearchStringIhEEmPKT_mS3_mmb.part.0:
.LFB9936:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rcx, -72(%rbp)
	movb	%r9b, -64(%rbp)
	testq	%rcx, %rcx
	je	.L2900
	testq	%rdx, %rdx
	je	.L2900
	movq	%rsi, -40(%rbp)
	movq	%rsi, %rbx
	movb	%r9b, -32(%rbp)
	testq	%rsi, %rsi
	je	.L2900
	testq	%rdi, %rdi
	je	.L2900
	testb	%r9b, %r9b
	jne	.L2901
	movq	%rsi, %r12
	pushq	-64(%rbp)
	subq	%rcx, %r12
	pushq	-72(%rbp)
	movq	%r12, %rax
	pushq	%rdx
	subq	%r8, %rax
	cmpq	%r8, %r12
	movl	$0, %r8d
	pushq	-32(%rbp)
	cmovnb	%rax, %r8
	pushq	-40(%rbp)
	pushq	%rdi
	movq	%r8, %rdi
	call	_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m
	addq	$48, %rsp
	subq	%rax, %r12
	cmpq	%rax, %rbx
	cmovne	%r12, %rax
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2901:
	.cfi_restore_state
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	%rdx
	pushq	-32(%rbp)
	pushq	-40(%rbp)
	pushq	%rdi
	movq	%r8, %rdi
	call	_ZN4node12stringsearch12SearchStringIhEEmNS0_6VectorIKT_EES5_m
	addq	$48, %rsp
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2900:
	.cfi_restore_state
	leaq	_ZZN4node12stringsearch6VectorIKhEC4EPS2_mbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9936:
	.size	_ZN4node12SearchStringIhEEmPKT_mS3_mmb.part.0, .-_ZN4node12SearchStringIhEEmPKT_mS3_mmb.part.0
	.section	.rodata.str1.1
.LC50:
	.string	"Bad input string"
	.text
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1272, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3001
	cmpw	$1040, %cx
	jne	.L2908
.L3001:
	movq	23(%rdx), %r13
.L2910:
	movl	16(%rbx), %edx
	movq	352(%r13), %r15
	cmpl	$1, %edx
	jg	.L2911
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L2912:
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L3031
.L2913:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3031:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2913
	cmpl	$2, %edx
	jg	.L2915
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2916:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L3032
	cmpl	$3, 16(%rbx)
	jg	.L2918
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2919:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3033
	cmpl	$4, 16(%rbx)
	jg	.L2921
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2922:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L3034
	cmpl	$3, 16(%rbx)
	jg	.L2924
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2925:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	16(%rbx), %edx
	movl	%eax, %r12d
	testl	%edx, %edx
	jg	.L2926
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L3035
.L2928:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L2930
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L2931:
	movq	$0, -1120(%rbp)
	movq	%r13, %rdi
	movq	$0, -1112(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L3036
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -1112(%rbp)
	cmpq	$64, %rax
	ja	.L2935
	movq	%r13, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L3037
.L2935:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-1248(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-1248(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r14
	movq	%r14, -1120(%rbp)
.L2934:
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2936
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	movq	%r14, %rdi
.L2939:
	call	_ZNK2v87Integer5ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movq	%rax, %r13
	jg	.L2940
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2941:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	-1112(%rbp), %r10
	movl	%r12d, %edx
	movq	%r15, %rdi
	movq	-1120(%rbp), %rsi
	movb	%al, -1280(%rbp)
	movq	%r10, %rax
	andq	$-2, %r10
	cmpl	$3, %r12d
	cmovne	%rax, %r10
	movq	%rsi, -1288(%rbp)
	movq	%r14, %rsi
	movq	%r10, -1272(%rbp)
	call	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movq	%rdx, %r11
	testb	%al, %al
	je	.L2907
	testq	%r13, %r13
	movq	-1272(%rbp), %r10
	js	.L3038
	leaq	(%rdx,%r13), %rax
	cmpq	%rax, %r10
	jge	.L2946
	testq	%rdx, %rdx
	je	.L3039
	cmpb	$0, -1280(%rbp)
	jne	.L3030
	leaq	-1(%r10), %r13
.L2946:
	testq	%r11, %r11
	je	.L3040
	testq	%r10, %r10
	je	.L3030
	testq	%r13, %r13
	js	.L3030
	movq	%r13, -1272(%rbp)
.L2951:
	cmpq	-1272(%rbp), %r10
	jbe	.L3041
	cmpb	$0, -1280(%rbp)
	je	.L2959
	movq	-1272(%rbp), %rax
	addq	%r11, %rax
	cmpq	%r10, %rax
	ja	.L3030
.L2959:
	cmpq	%r11, %r10
	jb	.L3030
	movzbl	-1280(%rbp), %r13d
	cmpl	$3, %r12d
	je	.L3042
	cmpl	$1, %r12d
	je	.L3043
	cmpl	$4, %r12d
	je	.L3044
	movabsq	$-4294967296, %rax
	movq	(%rbx), %rdx
.L2992:
	movq	%rax, 24(%rdx)
	.p2align 4,,10
	.p2align 3
.L2907:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3045
	addq	$1272, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2911:
	.cfi_restore_state
	movq	8(%rbx), %rax
	subq	$8, %rax
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2915:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2916
	.p2align 4,,10
	.p2align 3
.L2918:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2921:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2926:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L2928
.L3035:
	movq	352(%r13), %rdi
	call	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L2924:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	8(%rbx), %r13
	jmp	.L2931
	.p2align 4,,10
	.p2align 3
.L2940:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2936:
	movq	8(%rbx), %rdi
	leaq	-8(%rdi), %r14
	cmpl	$2, %eax
	je	.L3046
	subq	$16, %rdi
	jmp	.L2939
	.p2align 4,,10
	.p2align 3
.L2908:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L3037:
	leaq	-1184(%rbp), %r14
	movl	$64, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -1120(%rbp)
	jmp	.L2934
.L3049:
	movq	%r11, -1304(%rbp)
	movq	%r10, -1280(%rbp)
	movq	%rdi, -1296(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-1296(%rbp), %rdi
	call	malloc@PLT
	movq	-1280(%rbp), %r10
	movq	-1304(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L2991
	.p2align 4,,10
	.p2align 3
.L3030:
	movabsq	$-4294967296, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L3038:
	addq	%r10, %r13
	jns	.L2946
	testq	%rdx, %rdx
	je	.L2997
	cmpb	$0, -1280(%rbp)
	je	.L2947
	testq	%rdx, %rdx
	je	.L2997
	testq	%r10, %r10
	je	.L3030
	movq	$0, -1272(%rbp)
	jmp	.L2951
	.p2align 4,,10
	.p2align 3
.L3032:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3033:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3034:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3036:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2997:
	pxor	%xmm0, %xmm0
.L2949:
	movq	(%rbx), %rbx
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L3047
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L3042:
	leaq	-1264(%rbp), %r12
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r10, -1280(%rbp)
	movq	%r12, %rdi
	call	_ZN2v86String5ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-1264(%rbp), %rdx
	movq	-1280(%rbp), %r10
	testq	%rdx, %rdx
	je	.L2965
	cmpq	$1, %r10
	jbe	.L2965
	movl	-1256(%rbp), %eax
	testl	%eax, %eax
	jle	.L2965
	movq	%r10, %rsi
	movslq	%eax, %rcx
	shrq	%rsi
	cmpq	%rsi, %rcx
	ja	.L2986
	movq	-1272(%rbp), %r8
	movq	-1288(%rbp), %rdi
	movl	%r13d, %r9d
	movq	%r10, -1280(%rbp)
	shrq	%r8
	call	_ZN4node12SearchStringItEEmPKT_mS3_mmb.part.0
	movq	-1280(%rbp), %r10
	movq	%rax, %rsi
.L2986:
	movq	%r12, %rdi
	movq	%r10, -1272(%rbp)
	leaq	(%rsi,%rsi), %r13
	call	_ZN2v86String5ValueD1Ev@PLT
	movq	-1272(%rbp), %r10
.L2987:
	movq	(%rbx), %rdx
	cmpq	%r10, %r13
	je	.L3000
	movq	%r13, %rax
	salq	$32, %rax
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3039:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r10, %xmm0
	jmp	.L2949
	.p2align 4,,10
	.p2align 3
.L3041:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3043:
	leaq	-1248(%rbp), %r12
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r11, -1296(%rbp)
	movq	%r12, %rdi
	movq	%r10, -1280(%rbp)
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-1248(%rbp), %rdx
	movq	-1280(%rbp), %r10
	movq	-1296(%rbp), %r11
	testq	%rdx, %rdx
	je	.L3048
	movq	-1272(%rbp), %r8
	movl	%r13d, %r9d
	movq	%r10, %rsi
	movq	%r11, %rcx
	movq	-1288(%rbp), %rdi
	movq	%r10, -1272(%rbp)
	call	_ZN4node12SearchStringIhEEmPKT_mS3_mmb.part.0
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	-1272(%rbp), %r10
	jmp	.L2987
.L3044:
	testq	%r11, %r11
	movl	$1, %edi
	movq	%r10, -1304(%rbp)
	cmovne	%r11, %rdi
	movq	%r11, -1296(%rbp)
	movq	%rdi, -1280(%rbp)
	call	malloc@PLT
	movq	-1280(%rbp), %rdi
	movq	-1296(%rbp), %r11
	testq	%rax, %rax
	movq	-1304(%rbp), %r10
	movq	%rax, %r12
	je	.L3049
.L2991:
	movl	%r11d, %r8d
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r10, -1296(%rbp)
	movq	%r11, -1280(%rbp)
	call	_ZNK2v86String12WriteOneByteEPNS_7IsolateEPhiii@PLT
	movq	-1296(%rbp), %r10
	movl	%r13d, %r9d
	movq	-1280(%rbp), %r11
	movq	-1272(%rbp), %r8
	movq	-1288(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	movq	%r11, %rcx
	movq	%r10, -1272(%rbp)
	call	_ZN4node12SearchStringIhEEmPKT_mS3_mmb.part.0
	movq	%r12, %rdi
	movq	%rax, %r13
	call	free@PLT
	movq	-1272(%rbp), %r10
	jmp	.L2987
.L2965:
	movabsq	$-4294967296, %rsi
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rsi, 24(%rax)
	call	_ZN2v86String5ValueD1Ev@PLT
	jmp	.L2907
.L3040:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r13, %xmm0
	jmp	.L2949
.L3047:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L2907
.L3048:
	movabsq	$-4294967296, %rsi
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rsi, 24(%rax)
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	jmp	.L2907
.L2947:
	testq	%rdx, %rdx
	jne	.L3030
	movsd	.LC48(%rip), %xmm0
	jmp	.L2949
.L3045:
	call	__stack_chk_fail@PLT
.L3000:
	movl	$4294967295, %eax
	salq	$32, %rax
	jmp	.L2992
.L3046:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2939
	.cfi_endproc
.LFE7690:
	.size	_ZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	jg	.L3051
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3148
.L3053:
	cmpl	$2, 16(%rbx)
	jle	.L3149
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3055:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L3150
	cmpl	$3, 16(%rbx)
	jg	.L3057
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3058:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L3151
	cmpl	$4, 16(%rbx)
	jg	.L3060
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3061:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L3152
	cmpl	$3, 16(%rbx)
	jg	.L3063
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3064:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	16(%rbx), %edx
	movl	%eax, %r13d
	testl	%edx, %edx
	jg	.L3065
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L3147
.L3067:
	cmpl	$1, 16(%rbx)
	jg	.L3072
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3073:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L3147
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L3078
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r14
.L3079:
	movq	$0, -144(%rbp)
	movq	%r14, %rdi
	movq	$0, -136(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L3086
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -136(%rbp)
	cmpq	$64, %rax
	jbe	.L3081
.L3083:
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-272(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-272(%rbp), %r12
	movq	%r14, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r12
	movq	%r12, -144(%rbp)
.L3082:
	cmpl	$1, 16(%rbx)
	jle	.L3153
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r12
.L3085:
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L3086
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -56(%rbp)
	cmpq	$64, %rax
	ja	.L3089
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L3154
.L3089:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-272(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-272(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %r14
	movq	%r14, -64(%rbp)
.L3088:
	cmpl	$2, 16(%rbx)
	jg	.L3090
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movq	%rax, %r12
	jg	.L3092
.L3160:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3093:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	-136(%rbp), %r14
	movq	-56(%rbp), %rcx
	testq	%r12, %r12
	js	.L3155
	leaq	(%rcx,%r12), %rdx
	cmpq	%rdx, %r14
	jge	.L3095
	testq	%rcx, %rcx
	je	.L3156
	testb	%al, %al
	jne	.L3126
	leaq	-1(%r14), %r12
.L3095:
	testq	%rcx, %rcx
	je	.L3157
	testq	%r14, %r14
	je	.L3126
	testq	%r12, %r12
	js	.L3126
.L3100:
	cmpq	%r12, %r14
	jbe	.L3158
	testb	%al, %al
	je	.L3108
	leaq	(%rcx,%r12), %rdx
	cmpq	%r14, %rdx
	ja	.L3126
.L3108:
	cmpq	%rcx, %r14
	jb	.L3126
	movq	-144(%rbp), %rdi
	movq	-64(%rbp), %rdx
	movzbl	%al, %r9d
	cmpl	$3, %r13d
	jne	.L3111
	cmpq	$1, %r14
	jbe	.L3126
	cmpq	$1, %rcx
	jbe	.L3126
	movq	%r14, %rsi
	shrq	%rcx
	shrq	%rsi
	cmpq	%rsi, %rcx
	ja	.L3114
	movq	%r12, %r8
	shrq	%r8
	call	_ZN4node12SearchStringItEEmPKT_mS3_mmb.part.0
	movq	%rax, %rsi
.L3114:
	leaq	(%rsi,%rsi), %rax
.L3115:
	movabsq	$-4294967296, %rdx
	movq	%rax, %rsi
	movq	(%rbx), %rcx
	salq	$32, %rsi
	cmpq	%r14, %rax
	cmovne	%rsi, %rdx
	movq	%rdx, 24(%rcx)
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L3051:
	movq	8(%rdi), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L3053
.L3148:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3149:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3057:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3060:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3065:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L3067
.L3147:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3124
	cmpw	$1040, %cx
	jne	.L3075
.L3124:
	movq	23(%rdx), %rax
.L3077:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_INVALID_ARG_TYPEEPN2v87IsolateEPKc.constprop.0
.L3050:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3159
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3063:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3064
	.p2align 4,,10
	.p2align 3
.L3072:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3078:
	movq	8(%rbx), %r14
	jmp	.L3079
	.p2align 4,,10
	.p2align 3
.L3153:
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3081:
	movq	%r14, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L3083
	leaq	-208(%rbp), %r12
	movl	$64, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r12, -144(%rbp)
	jmp	.L3082
	.p2align 4,,10
	.p2align 3
.L3155:
	addq	%r14, %r12
	jns	.L3095
	testq	%rcx, %rcx
	je	.L3119
	testb	%al, %al
	je	.L3096
	testq	%rcx, %rcx
	je	.L3119
	xorl	%r12d, %r12d
	testq	%r14, %r14
	jne	.L3100
	.p2align 4,,10
	.p2align 3
.L3126:
	movabsq	$-4294967296, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L3090:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movq	%rax, %r12
	jle	.L3160
.L3092:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3150:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3151:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3086:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3152:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3154:
	leaq	-128(%rbp), %r14
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -64(%rbp)
	jmp	.L3088
	.p2align 4,,10
	.p2align 3
.L3119:
	pxor	%xmm0, %xmm0
.L3098:
	movq	(%rbx), %rbx
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L3161
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L3111:
	movq	%r12, %r8
	movq	%r14, %rsi
	call	_ZN4node12SearchStringIhEEmPKT_mS3_mmb.part.0
	jmp	.L3115
	.p2align 4,,10
	.p2align 3
.L3075:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3077
	.p2align 4,,10
	.p2align 3
.L3156:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r14, %xmm0
	jmp	.L3098
	.p2align 4,,10
	.p2align 3
.L3158:
	leaq	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3157:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L3098
.L3161:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L3050
.L3096:
	testq	%rcx, %rcx
	jne	.L3126
	movsd	.LC48(%rip), %xmm0
	jmp	.L3098
.L3159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7697:
	.size	_ZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.weak	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_2
	.section	.rodata.str1.1
.LC51:
	.string	"../src/string_search.h:237"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"(subject.length() - pos) <= ((18446744073709551615UL) / sizeof(Char))"
	.align 8
.LC53:
	.string	"size_t node::stringsearch::FindFirstCharacter(node::stringsearch::Vector<const Char>, node::stringsearch::Vector<const Char>, size_t) [with Char = short unsigned int; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_2,"awG",@progbits,_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_2,comdat
	.align 16
	.type	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_2, @gnu_unique_object
	.size	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_2, 24
_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_2:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.weak	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_1
	.section	.rodata.str1.1
.LC54:
	.string	"../src/string_search.h:236"
.LC55:
	.string	"(pos) <= (subject.length())"
	.section	.data.rel.ro.local._ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_1,"awG",@progbits,_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_1,comdat
	.align 16
	.type	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_1, @gnu_unique_object
	.size	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_1, 24
_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_1:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC53
	.weak	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_0
	.section	.rodata.str1.1
.LC56:
	.string	"../src/string_search.h:233"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"(max_n - pos) <= ((18446744073709551615UL) / sizeof(Char))"
	.section	.data.rel.ro.local._ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_0,"awG",@progbits,_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_0,comdat
	.align 16
	.type	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_0, @gnu_unique_object
	.size	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_0, 24
_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args_0:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC53
	.weak	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args
	.section	.rodata.str1.1
.LC58:
	.string	"../src/string_search.h:232"
.LC59:
	.string	"(pos) <= (max_n)"
	.section	.data.rel.ro.local._ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args,"awG",@progbits,_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args,comdat
	.align 16
	.type	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args, @gnu_unique_object
	.size	_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args, 24
_ZZN4node12stringsearch18FindFirstCharacterItEEmNS0_6VectorIKT_EES5_mE4args:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC53
	.weak	_ZZN4node12stringsearch12StringSearchIhE12LinearSearchENS0_6VectorIKhEEmE4args_0
	.section	.rodata.str1.1
.LC60:
	.string	"../src/string_search.h:315"
.LC61:
	.string	"(i) <= (n)"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.ascii	"size_t node::"
	.string	"stringsearch::StringSearch<Char>::LinearSearch(node::stringsearch::StringSearch<Char>::Vector, size_t) [with Char = unsigned char; size_t = long unsigned int; node::stringsearch::StringSearch<Char>::Vector = node::stringsearch::Vector<const unsigned char>]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch12StringSearchIhE12LinearSearchENS0_6VectorIKhEEmE4args_0,"awG",@progbits,_ZZN4node12stringsearch12StringSearchIhE12LinearSearchENS0_6VectorIKhEEmE4args_0,comdat
	.align 16
	.type	_ZZN4node12stringsearch12StringSearchIhE12LinearSearchENS0_6VectorIKhEEmE4args_0, @gnu_unique_object
	.size	_ZZN4node12stringsearch12StringSearchIhE12LinearSearchENS0_6VectorIKhEEmE4args_0, 24
_ZZN4node12stringsearch12StringSearchIhE12LinearSearchENS0_6VectorIKhEEmE4args_0:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.weak	_ZZN4node12stringsearch12StringSearchIhE13InitialSearchENS0_6VectorIKhEEmE4args
	.section	.rodata.str1.1
.LC63:
	.string	"../src/string_search.h:556"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.ascii	"size_t node::s"
	.string	"tringsearch::StringSearch<Char>::InitialSearch(node::stringsearch::StringSearch<Char>::Vector, size_t) [with Char = unsigned char; size_t = long unsigned int; node::stringsearch::StringSearch<Char>::Vector = node::stringsearch::Vector<const unsigned char>]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch12StringSearchIhE13InitialSearchENS0_6VectorIKhEEmE4args,"awG",@progbits,_ZZN4node12stringsearch12StringSearchIhE13InitialSearchENS0_6VectorIKhEEmE4args,comdat
	.align 16
	.type	_ZZN4node12stringsearch12StringSearchIhE13InitialSearchENS0_6VectorIKhEEmE4args, @gnu_unique_object
	.size	_ZZN4node12stringsearch12StringSearchIhE13InitialSearchENS0_6VectorIKhEEmE4args, 24
_ZZN4node12stringsearch12StringSearchIhE13InitialSearchENS0_6VectorIKhEEmE4args:
	.quad	.LC63
	.quad	.LC61
	.quad	.LC64
	.weak	_ZZN4node12stringsearch12StringSearchItE12LinearSearchENS0_6VectorIKtEEmE4args_0
	.section	.rodata.str1.8
	.align 8
.LC65:
	.ascii	"size_t node::stringsear"
	.string	"ch::StringSearch<Char>::LinearSearch(node::stringsearch::StringSearch<Char>::Vector, size_t) [with Char = short unsigned int; size_t = long unsigned int; node::stringsearch::StringSearch<Char>::Vector = node::stringsearch::Vector<const short unsigned int>]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch12StringSearchItE12LinearSearchENS0_6VectorIKtEEmE4args_0,"awG",@progbits,_ZZN4node12stringsearch12StringSearchItE12LinearSearchENS0_6VectorIKtEEmE4args_0,comdat
	.align 16
	.type	_ZZN4node12stringsearch12StringSearchItE12LinearSearchENS0_6VectorIKtEEmE4args_0, @gnu_unique_object
	.size	_ZZN4node12stringsearch12StringSearchItE12LinearSearchENS0_6VectorIKtEEmE4args_0, 24
_ZZN4node12stringsearch12StringSearchItE12LinearSearchENS0_6VectorIKtEEmE4args_0:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC65
	.weak	_ZZN4node12stringsearch12StringSearchItE13InitialSearchENS0_6VectorIKtEEmE4args
	.section	.rodata.str1.8
	.align 8
.LC66:
	.ascii	"size_t node::stringsearc"
	.string	"h::StringSearch<Char>::InitialSearch(node::stringsearch::StringSearch<Char>::Vector, size_t) [with Char = short unsigned int; size_t = long unsigned int; node::stringsearch::StringSearch<Char>::Vector = node::stringsearch::Vector<const short unsigned int>]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch12StringSearchItE13InitialSearchENS0_6VectorIKtEEmE4args,"awG",@progbits,_ZZN4node12stringsearch12StringSearchItE13InitialSearchENS0_6VectorIKtEEmE4args,comdat
	.align 16
	.type	_ZZN4node12stringsearch12StringSearchItE13InitialSearchENS0_6VectorIKtEEmE4args, @gnu_unique_object
	.size	_ZZN4node12stringsearch12StringSearchItE13InitialSearchENS0_6VectorIKtEEmE4args, 24
_ZZN4node12stringsearch12StringSearchItE13InitialSearchENS0_6VectorIKtEEmE4args:
	.quad	.LC63
	.quad	.LC61
	.quad	.LC66
	.weak	_ZZN4node12stringsearch12StringSearchIhEC4ENS0_6VectorIKhEEE4args
	.section	.rodata.str1.1
.LC67:
	.string	"../src/string_search.h:100"
.LC68:
	.string	"(pattern_length) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"node::stringsearch::StringSearch<Char>::StringSearch(node::stringsearch::StringSearch<Char>::Vector) [with Char = unsigned char; node::stringsearch::StringSearch<Char>::Vector = node::stringsearch::Vector<const unsigned char>]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch12StringSearchIhEC4ENS0_6VectorIKhEEE4args,"awG",@progbits,_ZZN4node12stringsearch12StringSearchIhEC4ENS0_6VectorIKhEEE4args,comdat
	.align 16
	.type	_ZZN4node12stringsearch12StringSearchIhEC4ENS0_6VectorIKhEEE4args, @gnu_unique_object
	.size	_ZZN4node12stringsearch12StringSearchIhEC4ENS0_6VectorIKhEEE4args, 24
_ZZN4node12stringsearch12StringSearchIhEC4ENS0_6VectorIKhEEE4args:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.weak	_ZZN4node12stringsearch12StringSearchItEC4ENS0_6VectorIKtEEE4args
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"node::stringsearch::StringSearch<Char>::StringSearch(node::stringsearch::StringSearch<Char>::Vector) [with Char = short unsigned int; node::stringsearch::StringSearch<Char>::Vector = node::stringsearch::Vector<const short unsigned int>]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch12StringSearchItEC4ENS0_6VectorIKtEEE4args,"awG",@progbits,_ZZN4node12stringsearch12StringSearchItEC4ENS0_6VectorIKtEEE4args,comdat
	.align 16
	.type	_ZZN4node12stringsearch12StringSearchItEC4ENS0_6VectorIKtEEE4args, @gnu_unique_object
	.size	_ZZN4node12stringsearch12StringSearchItEC4ENS0_6VectorIKtEEE4args, 24
_ZZN4node12stringsearch12StringSearchItEC4ENS0_6VectorIKtEEE4args:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC70
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC71:
	.string	"../src/util-inl.h:502"
.LC72:
	.string	"value->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Object>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_6ObjectEEEE4args:
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.weak	_ZZN4node12stringsearch6VectorIKhEC4EPS2_mbE4args
	.section	.rodata.str1.1
.LC74:
	.string	"../src/string_search.h:23"
.LC75:
	.string	"length > 0 && data != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"node::stringsearch::Vector<T>::Vector(T*, size_t, bool) [with T = const unsigned char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch6VectorIKhEC4EPS2_mbE4args,"awG",@progbits,_ZZN4node12stringsearch6VectorIKhEC4EPS2_mbE4args,comdat
	.align 16
	.type	_ZZN4node12stringsearch6VectorIKhEC4EPS2_mbE4args, @gnu_unique_object
	.size	_ZZN4node12stringsearch6VectorIKhEC4EPS2_mbE4args, 24
_ZZN4node12stringsearch6VectorIKhEC4EPS2_mbE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.weak	_ZZN4node12stringsearch6VectorIKtEC4EPS2_mbE4args
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"node::stringsearch::Vector<T>::Vector(T*, size_t, bool) [with T = const short unsigned int; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node12stringsearch6VectorIKtEC4EPS2_mbE4args,"awG",@progbits,_ZZN4node12stringsearch6VectorIKtEC4EPS2_mbE4args,comdat
	.align 16
	.type	_ZZN4node12stringsearch6VectorIKtEC4EPS2_mbE4args, @gnu_unique_object
	.size	_ZZN4node12stringsearch6VectorIKtEC4EPS2_mbE4args, 24
_ZZN4node12stringsearch6VectorIKtEC4EPS2_mbE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC77
	.section	.rodata.str1.1
.LC78:
	.string	"../src/node_buffer.cc:669"
.LC79:
	.string	"(ts_obj_data) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"void node::Buffer::{anonymous}::StringWrite(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::UTF8]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"(args.This())->IsArrayBufferView()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC81
	.quad	.LC80
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"void node::Buffer::{anonymous}::StringWrite(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::UCS2]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC82
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC81
	.quad	.LC82
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"void node::Buffer::{anonymous}::StringWrite(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::HEX]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC83
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC81
	.quad	.LC83
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"void node::Buffer::{anonymous}::StringWrite(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::BINARY]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC84
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC81
	.quad	.LC84
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"void node::Buffer::{anonymous}::StringWrite(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::BASE64]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC85
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC81
	.quad	.LC85
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"void node::Buffer::{anonymous}::StringWrite(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::ASCII]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC86
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringWriteILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC81
	.quad	.LC86
	.section	.rodata.str1.1
.LC87:
	.string	"../src/node_buffer.cc:520"
.LC88:
	.string	"!error.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"void node::Buffer::{anonymous}::StringSlice(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::UTF8]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE1EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"void node::Buffer::{anonymous}::StringSlice(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::UCS2]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE3EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC90
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"void node::Buffer::{anonymous}::StringSlice(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::HEX]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE5EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC91
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"void node::Buffer::{anonymous}::StringSlice(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::BINARY]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE4EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC92
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"void node::Buffer::{anonymous}::StringSlice(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::BASE64]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE2EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC93
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"void node::Buffer::{anonymous}::StringSlice(const v8::FunctionCallbackInfo<v8::Value>&) [with node::encoding encoding = node::ASCII]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_111StringSliceILNS_8encodingE0EEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC94
	.weak	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args
	.section	.rodata.str1.1
.LC95:
	.string	"../src/util.h:352"
.LC96:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args:
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC98:
	.string	"../src/util-inl.h:495"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Value>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args:
	.quad	.LC98
	.quad	.LC72
	.quad	.LC99
	.weak	_ZZN4node7ReallocIcEEPT_S2_mE4args
	.section	.rodata.str1.1
.LC100:
	.string	"../src/util-inl.h:374"
.LC101:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC102:
	.string	"T* node::Realloc(T*, size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIcEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIcEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIcEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIcEEPT_S2_mE4args, 24
_ZZN4node7ReallocIcEEPT_S2_mE4args:
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC103:
	.string	"../src/util.h:391"
.LC104:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args:
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.1
.LC106:
	.string	"../src/util.h:376"
.LC107:
	.string	"!IsInvalidated()"
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args:
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.section	.rodata.str1.1
.LC109:
	.string	"../src/node_buffer.cc"
.LC110:
	.string	"buffer"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC109
	.quad	0
	.quad	_ZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC110
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC111:
	.string	"../src/node_buffer.cc:1191"
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"target ->Set(env->context(), FIXED_ONE_BYTE_STRING(env->isolate(), \"zeroFill\"), Uint32Array::New(array_buffer, 0, 1)) .FromJust()"
	.align 8
.LC113:
	.string	"void node::Buffer::{anonymous}::Initialize(v8::Local<v8::Object>, v8::Local<v8::Value>, v8::Local<v8::Context>, void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPvE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPvE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPvE4args:
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.section	.rodata.str1.1
.LC114:
	.string	"../src/node_buffer.cc:1131"
.LC115:
	.string	"args[0]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"void node::Buffer::{anonymous}::SetBufferPrototype(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_118SetBufferPrototypeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.section	.rodata.str1.1
.LC117:
	.string	"../src/node_buffer.cc:1100"
.LC118:
	.string	"args[2]->IsUint32Array()"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"void node::Buffer::{anonymous}::EncodeInto(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.section	.rodata.str1.1
.LC120:
	.string	"../src/node_buffer.cc:1099"
.LC121:
	.string	"args[1]->IsUint8Array()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC120
	.quad	.LC121
	.quad	.LC119
	.section	.rodata.str1.1
.LC122:
	.string	"../src/node_buffer.cc:1098"
.LC123:
	.string	"args[0]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC122
	.quad	.LC123
	.quad	.LC119
	.section	.rodata.str1.1
.LC124:
	.string	"../src/node_buffer.cc:1097"
.LC125:
	.string	"(args.Length()) >= (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_1L10EncodeIntoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC124
	.quad	.LC125
	.quad	.LC119
	.section	.rodata.str1.1
.LC126:
	.string	"../src/node_buffer.cc:1079"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"void node::Buffer::{anonymous}::EncodeUtf8String(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC126
	.quad	.LC123
	.quad	.LC127
	.section	.rodata.str1.1
.LC128:
	.string	"../src/node_buffer.cc:1078"
.LC129:
	.string	"(args.Length()) >= (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_1L16EncodeUtf8StringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC128
	.quad	.LC129
	.quad	.LC127
	.section	.rodata.str1.1
.LC130:
	.string	"../src/node_buffer.cc:1067"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"void node::Buffer::{anonymous}::Swap64(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC130
	.quad	.LC79
	.quad	.LC131
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"(args[0])->IsArrayBufferView()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_16Swap64ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC130
	.quad	.LC132
	.quad	.LC131
	.section	.rodata.str1.1
.LC133:
	.string	"../src/node_buffer.cc:1058"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"void node::Buffer::{anonymous}::Swap32(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC133
	.quad	.LC79
	.quad	.LC134
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_16Swap32ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC133
	.quad	.LC132
	.quad	.LC134
	.section	.rodata.str1.1
.LC135:
	.string	"../src/node_buffer.cc:1049"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"void node::Buffer::{anonymous}::Swap16(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC135
	.quad	.LC79
	.quad	.LC136
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_16Swap16ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC135
	.quad	.LC132
	.quad	.LC136
	.section	.rodata.str1.1
.LC137:
	.string	"../src/node_buffer.cc:1032"
.LC138:
	.string	"(offset) < (buffer.length())"
	.section	.rodata.str1.8
	.align 8
.LC139:
	.string	"void node::Buffer::{anonymous}::IndexOfNumber(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.section	.rodata.str1.1
.LC140:
	.string	"../src/node_buffer.cc:1017"
.LC141:
	.string	"args[3]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC140
	.quad	.LC141
	.quad	.LC139
	.section	.rodata.str1.1
.LC142:
	.string	"../src/node_buffer.cc:1016"
.LC143:
	.string	"args[2]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC142
	.quad	.LC143
	.quad	.LC139
	.section	.rodata.str1.1
.LC144:
	.string	"../src/node_buffer.cc:1015"
.LC145:
	.string	"args[1]->IsUint32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfNumberERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC144
	.quad	.LC145
	.quad	.LC139
	.section	.rodata.str1.1
.LC146:
	.string	"../src/node_buffer.cc:980"
.LC147:
	.string	"(offset) < (haystack_length)"
	.section	.rodata.str1.8
	.align 8
.LC148:
	.string	"void node::Buffer::{anonymous}::IndexOfBuffer(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.section	.rodata.str1.1
.LC149:
	.string	"../src/node_buffer.cc:945"
.LC150:
	.string	"args[4]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC149
	.quad	.LC150
	.quad	.LC148
	.section	.rodata.str1.1
.LC151:
	.string	"../src/node_buffer.cc:944"
.LC152:
	.string	"args[3]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC151
	.quad	.LC152
	.quad	.LC148
	.section	.rodata.str1.1
.LC153:
	.string	"../src/node_buffer.cc:943"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC153
	.quad	.LC143
	.quad	.LC148
	.section	.rodata.str1.1
.LC154:
	.string	"../src/node_buffer.cc:942"
.LC155:
	.string	"args[1]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC154
	.quad	.LC155
	.quad	.LC148
	.section	.rodata.str1.1
.LC156:
	.string	"../src/node_buffer.cc:868"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"void node::Buffer::{anonymous}::IndexOfString(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC156
	.quad	.LC147
	.quad	.LC157
	.section	.rodata.str1.1
.LC158:
	.string	"../src/node_buffer.cc:830"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC158
	.quad	.LC150
	.quad	.LC157
	.section	.rodata.str1.1
.LC159:
	.string	"../src/node_buffer.cc:829"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC159
	.quad	.LC152
	.quad	.LC157
	.section	.rodata.str1.1
.LC160:
	.string	"../src/node_buffer.cc:828"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC160
	.quad	.LC143
	.quad	.LC157
	.section	.rodata.str1.1
.LC161:
	.string	"../src/node_buffer.cc:827"
.LC162:
	.string	"args[1]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_113IndexOfStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC161
	.quad	.LC162
	.quad	.LC157
	.section	.rodata.str1.1
.LC163:
	.string	"../src/node_buffer.cc:754"
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"(target_start) <= (target_end)"
	.align 8
.LC165:
	.string	"void node::Buffer::{anonymous}::CompareOffset(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.section	.rodata.str1.1
.LC166:
	.string	"../src/node_buffer.cc:753"
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"(source_start) <= (source_end)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_113CompareOffsetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC166
	.quad	.LC167
	.quad	.LC165
	.section	.rodata.str1.1
.LC168:
	.string	"../src/node_buffer.cc:703"
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"void node::Buffer::{anonymous}::ByteLengthUtf8(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_114ByteLengthUtf8ERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC168
	.quad	.LC123
	.quad	.LC169
	.section	.rodata.str1.1
.LC170:
	.string	"../src/node_buffer.cc:590"
.LC171:
	.string	"(fill_obj_data) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"void node::Buffer::{anonymous}::Fill(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.section	.rodata.str1.8
	.align 8
.LC173:
	.string	"(args[1])->IsArrayBufferView()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC170
	.quad	.LC173
	.quad	.LC172
	.section	.rodata.str1.1
.LC174:
	.string	"../src/node_buffer.cc:572"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC174
	.quad	.LC79
	.quad	.LC172
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_14FillERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC174
	.quad	.LC132
	.quad	.LC172
	.section	.rodata.str1.1
.LC175:
	.string	"../src/node_buffer.cc:536"
.LC176:
	.string	"(target_data) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"void node::Buffer::{anonymous}::Copy(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC175
	.quad	.LC176
	.quad	.LC177
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"(target_obj)->IsArrayBufferView()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_14CopyERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC175
	.quad	.LC178
	.quad	.LC177
	.section	.rodata.str1.1
.LC179:
	.string	"../src/node_buffer.cc:484"
.LC180:
	.string	"args[1]->IsInt32()"
	.section	.rodata.str1.8
	.align 8
.LC181:
	.string	"void node::Buffer::{anonymous}::CreateFromString(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.section	.rodata.str1.1
.LC182:
	.string	"../src/node_buffer.cc:483"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_116CreateFromStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC182
	.quad	.LC123
	.quad	.LC181
	.section	.rodata.str1.1
.LC183:
	.string	"../src/node_buffer.cc:467"
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"(env->isolate_data()->node_allocator()) != nullptr"
	.align 8
.LC185:
	.string	"v8::MaybeLocal<v8::Object> node::Buffer::New(node::Environment*, char*, size_t, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_1, @object
	.size	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_1, 24
_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_1:
	.quad	.LC183
	.quad	.LC184
	.quad	.LC185
	.section	.rodata.str1.1
.LC186:
	.string	"../src/node_buffer.cc:455"
.LC187:
	.string	"length <= kMaxLength"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_0, @object
	.size	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_0, 24
_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args_0:
	.quad	.LC186
	.quad	.LC187
	.quad	.LC185
	.section	.rodata.str1.1
.LC188:
	.string	"../src/node_buffer.cc:454"
.LC189:
	.string	"(data) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args, @object
	.size	_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args, 24
_ZZN4node6Buffer3NewEPNS_11EnvironmentEPcmbE4args:
	.quad	.LC188
	.quad	.LC189
	.quad	.LC185
	.section	.rodata.str1.1
.LC190:
	.string	"../src/node_buffer.cc:370"
	.section	.rodata.str1.8
	.align 8
.LC191:
	.string	"v8::MaybeLocal<v8::Object> node::Buffer::Copy(node::Environment*, const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer4CopyEPNS_11EnvironmentEPKcmE4args, @object
	.size	_ZZN4node6Buffer4CopyEPNS_11EnvironmentEPKcmE4args, 24
_ZZN4node6Buffer4CopyEPNS_11EnvironmentEPKcmE4args:
	.quad	.LC190
	.quad	.LC189
	.quad	.LC191
	.section	.rodata.str1.1
.LC192:
	.string	"../src/node_buffer.cc:295"
.LC193:
	.string	"actual <= length"
	.section	.rodata.str1.8
	.align 8
.LC194:
	.string	"v8::MaybeLocal<v8::Object> node::Buffer::New(v8::Isolate*, v8::Local<v8::String>, node::encoding)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingEE4args, @object
	.size	_ZZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingEE4args, 24
_ZZN4node6Buffer3NewEPN2v87IsolateENS1_5LocalINS1_6StringEEENS_8encodingEE4args:
	.quad	.LC192
	.quad	.LC193
	.quad	.LC194
	.section	.rodata.str1.1
.LC195:
	.string	"../src/node_buffer.cc:253"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"!env->buffer_prototype_object().IsEmpty()"
	.align 8
.LC197:
	.string	"v8::MaybeLocal<v8::Uint8Array> node::Buffer::New(node::Environment*, v8::Local<v8::ArrayBuffer>, size_t, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmmE4args, @object
	.size	_ZZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmmE4args, 24
_ZZN4node6Buffer3NewEPNS_11EnvironmentEN2v85LocalINS3_11ArrayBufferEEEmmE4args:
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.section	.rodata.str1.1
.LC198:
	.string	"../src/node_buffer.cc:243"
.LC199:
	.string	"obj->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC200:
	.string	"size_t node::Buffer::Length(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEEE4args, @object
	.size	_ZZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node6Buffer6LengthEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC198
	.quad	.LC199
	.quad	.LC200
	.section	.rodata.str1.1
.LC201:
	.string	"../src/node_buffer.cc:236"
.LC202:
	.string	"val->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC203:
	.string	"size_t node::Buffer::Length(v8::Local<v8::Value>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEEE4args, 24
_ZZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEEE4args:
	.quad	.LC201
	.quad	.LC202
	.quad	.LC203
	.section	.rodata.str1.1
.LC204:
	.string	"../src/node_buffer.cc:228"
	.section	.rodata.str1.8
	.align 8
.LC205:
	.string	"char* node::Buffer::Data(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEEE4args, @object
	.size	_ZZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC204
	.quad	.LC199
	.quad	.LC205
	.section	.rodata.str1.1
.LC206:
	.string	"../src/node_buffer.cc:220"
	.section	.rodata.str1.8
	.align 8
.LC207:
	.string	"char* node::Buffer::Data(v8::Local<v8::Value>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEEE4args, @object
	.size	_ZZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEEE4args, 24
_ZZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEEE4args:
	.quad	.LC206
	.quad	.LC202
	.quad	.LC207
	.section	.rodata.str1.1
.LC208:
	.string	"../src/node_buffer.cc:153"
.LC209:
	.string	"!ab.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC210:
	.string	"static void node::Buffer::{anonymous}::CallbackInfo::CleanupHook(void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPvE4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPvE4args, 24
_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfo11CleanupHookEPvE4args:
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.section	.rodata.str1.1
.LC211:
	.string	"../src/node_buffer.cc:133"
.LC212:
	.string	"(data_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC213:
	.string	"node::Buffer::{anonymous}::CallbackInfo::CallbackInfo(node::Environment*, v8::Local<v8::ArrayBuffer>, node::Buffer::FreeCallback, char*, void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args_0, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args_0, 24
_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args_0:
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.section	.rodata.str1.1
.LC214:
	.string	"../src/node_buffer.cc:131"
	.section	.rodata.str1.8
	.align 8
.LC215:
	.string	"(data_) == (static_cast<char*>(obj_c.Data()))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args, @object
	.size	_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args, 24
_ZZN4node6Buffer12_GLOBAL__N_112CallbackInfoC4EPNS_11EnvironmentEN2v85LocalINS5_11ArrayBufferEEEPFvPcPvES9_SA_E4args:
	.quad	.LC214
	.quad	.LC215
	.quad	.LC213
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC216:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC217:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC218:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.weak	_ZZN4node11Environment8AllocateEmE4args
	.section	.rodata.str1.1
.LC219:
	.string	"../src/env-inl.h:889"
.LC220:
	.string	"(ret) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC221:
	.string	"char* node::Environment::Allocate(size_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8AllocateEmE4args,"awG",@progbits,_ZZN4node11Environment8AllocateEmE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8AllocateEmE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8AllocateEmE4args, 24
_ZZN4node11Environment8AllocateEmE4args:
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.weak	_ZZN4node11SwapBytes64EPcmE4args
	.section	.rodata.str1.1
.LC222:
	.string	"../src/util-inl.h:256"
.LC223:
	.string	"(nbytes % 8) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC224:
	.string	"void node::SwapBytes64(char*, size_t)"
	.section	.data.rel.ro.local._ZZN4node11SwapBytes64EPcmE4args,"awG",@progbits,_ZZN4node11SwapBytes64EPcmE4args,comdat
	.align 16
	.type	_ZZN4node11SwapBytes64EPcmE4args, @gnu_unique_object
	.size	_ZZN4node11SwapBytes64EPcmE4args, 24
_ZZN4node11SwapBytes64EPcmE4args:
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.weak	_ZZN4node11SwapBytes32EPcmE4args
	.section	.rodata.str1.1
.LC225:
	.string	"../src/util-inl.h:232"
.LC226:
	.string	"(nbytes % 4) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC227:
	.string	"void node::SwapBytes32(char*, size_t)"
	.section	.data.rel.ro.local._ZZN4node11SwapBytes32EPcmE4args,"awG",@progbits,_ZZN4node11SwapBytes32EPcmE4args,comdat
	.align 16
	.type	_ZZN4node11SwapBytes32EPcmE4args, @gnu_unique_object
	.size	_ZZN4node11SwapBytes32EPcmE4args, 24
_ZZN4node11SwapBytes32EPcmE4args:
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.weak	_ZZN4node11SwapBytes16EPcmE4args
	.section	.rodata.str1.1
.LC228:
	.string	"../src/util-inl.h:208"
.LC229:
	.string	"(nbytes % 2) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC230:
	.string	"void node::SwapBytes16(char*, size_t)"
	.section	.data.rel.ro.local._ZZN4node11SwapBytes16EPcmE4args,"awG",@progbits,_ZZN4node11SwapBytes16EPcmE4args,comdat
	.align 16
	.type	_ZZN4node11SwapBytes16EPcmE4args, @gnu_unique_object
	.size	_ZZN4node11SwapBytes16EPcmE4args, 24
_ZZN4node11SwapBytes16EPcmE4args:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC48:
	.long	0
	.long	-1074790400
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
