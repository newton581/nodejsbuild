	.file	"tty_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5790:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6068:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6068:
	.size	_ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node7TTYWrap8SelfSizeEv,"axG",@progbits,_ZNK4node7TTYWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TTYWrap8SelfSizeEv
	.type	_ZNK4node7TTYWrap8SelfSizeEv, @function
_ZNK4node7TTYWrap8SelfSizeEv:
.LFB6070:
	.cfi_startproc
	endbr64
	movl	$472, %eax
	ret
	.cfi_endproc
.LFE6070:
	.size	_ZNK4node7TTYWrap8SelfSizeEv, .-_ZNK4node7TTYWrap8SelfSizeEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9637:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9637:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9641:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9641:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TTY"
.LC1:
	.string	"getWindowSize"
.LC2:
	.string	"setRawMode"
.LC3:
	.string	"isTTY"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L11
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L11
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L11
	movq	271(%rax), %rbx
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L38
.L12:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movl	$1, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L39
.L13:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TTYWrap10SetRawModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L40
.L14:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L41
.L15:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L42
.L16:
	movq	-56(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L43
.L17:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L10
	movq	3280(%rbx), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L44
.L10:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	3256(%rbx), %rdi
	movq	352(%rbx), %r13
	testq	%rdi, %rdi
	je	.L20
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3256(%rbx)
.L20:
	testq	%r12, %r12
	je	.L10
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3256(%rbx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L39:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L40:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L41:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L43:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L17
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7618:
.L11:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7618:
	.text
	.size	_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE4:
	.text
.LHOTE4:
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7583:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L50:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7583:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7582:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L56:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7582:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev:
.LFB6069:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 22(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movl	$1465472084, 16(%rdi)
	movw	%dx, 20(%rdi)
	movq	$7, 8(%rdi)
	movb	$0, 23(%rdi)
	ret
	.cfi_endproc
.LFE6069:
	.size	_ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TTYWrap10SetRawModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TTYWrap10SetRawModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TTYWrap10SetRawModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L69
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L67
	cmpw	$1040, %cx
	jne	.L60
.L67:
	movq	23(%rdx), %r12
.L62:
	testq	%r12, %r12
	je	.L70
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L71
	movq	8(%rbx), %rdi
.L66:
	call	_ZNK2v85Value6IsTrueEv@PLT
	leaq	160(%r12), %rdi
	movzbl	%al, %esi
	call	uv_tty_set_mode@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L70:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7621:
	.size	_ZN4node7TTYWrap10SetRawModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TTYWrap10SetRawModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L83
	cmpw	$1040, %cx
	jne	.L73
.L83:
	movq	23(%rdx), %rax
.L75:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L76
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L77:
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testb	%al, %al
	je	.L72
	sarq	$32, %rdi
	testl	%edi, %edi
	js	.L85
	call	uv_guess_handle@PLT
	xorl	%ecx, %ecx
	movl	%eax, %r8d
	movq	(%rbx), %rax
	cmpl	$14, %r8d
	movq	8(%rax), %rdx
	setne	%cl
	movq	112(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%rax)
.L72:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	_ZZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7619:
	.size	_ZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L105
	cmpw	$1040, %cx
	jne	.L87
.L105:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L108
.L90:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L106
	cmpw	$1040, %cx
	jne	.L91
.L106:
	movq	23(%rdx), %r12
.L93:
	testq	%r12, %r12
	je	.L109
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L110
	movq	8(%rbx), %rdi
.L97:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L111
	leaq	160(%r12), %rdi
	leaq	-44(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	call	uv_tty_get_winsize@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L99
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L112
	movq	8(%rbx), %r14
.L101:
	movq	352(%r13), %rdi
	movl	-48(%rbp), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%r13), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L113
.L102:
	movq	352(%r13), %rdi
	movl	-44(%rbp), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%r13), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L114
.L99:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
.L86:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L90
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L109:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	_ZZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L102
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7620:
	.size	_ZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node7TTYWrapD2Ev,"axG",@progbits,_ZN4node7TTYWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node7TTYWrapD1Ev
	.type	_ZThn88_N4node7TTYWrapD1Ev, @function
_ZThn88_N4node7TTYWrapD1Ev:
.LFB9679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L117
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L118
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L120
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L136:
	cmpq	%rax, %rdx
	je	.L139
.L120:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L136
.L118:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L138:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L140
	movq	%rax, %rbx
.L124:
	testq	%rbx, %rbx
	jne	.L141
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%r12), %rdx
	leaq	-88(%r12), %rdi
	movq	%rax, -88(%r12)
	movq	-24(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L139:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L117
	.cfi_endproc
.LFE9679:
	.size	_ZThn88_N4node7TTYWrapD1Ev, .-_ZThn88_N4node7TTYWrapD1Ev
	.section	.text._ZN4node7TTYWrapD0Ev,"axG",@progbits,_ZN4node7TTYWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node7TTYWrapD0Ev
	.type	_ZThn88_N4node7TTYWrapD0Ev, @function
_ZThn88_N4node7TTYWrapD0Ev:
.LFB9686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L143
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L144
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L146
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L162:
	cmpq	%rax, %rdx
	je	.L165
.L146:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L162
.L144:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L164:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L166
	movq	%rax, %rbx
.L150:
	testq	%rbx, %rbx
	jne	.L167
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%r12), %rdx
	subq	$88, %r12
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$472, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L165:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L143
	.cfi_endproc
.LFE9686:
	.size	_ZThn88_N4node7TTYWrapD0Ev, .-_ZThn88_N4node7TTYWrapD0Ev
	.section	.text._ZN4node7TTYWrapD2Ev,"axG",@progbits,_ZN4node7TTYWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7TTYWrapD2Ev
	.type	_ZN4node7TTYWrapD2Ev, @function
_ZN4node7TTYWrapD2Ev:
.LFB9600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	136(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 128(%rdi)
	testq	%rdx, %rdx
	je	.L169
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L170
	leaq	128(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L172
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L188:
	cmpq	%rax, %rcx
	je	.L191
.L172:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L188
.L170:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	movq	144(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	96(%r12), %rbx
	movq	%rax, 88(%r12)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L193:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	96(%r12), %rax
	cmpq	%rbx, %rax
	je	.L192
	movq	%rax, %rbx
.L176:
	testq	%rbx, %rbx
	jne	.L193
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L191:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L169
	.cfi_endproc
.LFE9600:
	.size	_ZN4node7TTYWrapD2Ev, .-_ZN4node7TTYWrapD2Ev
	.weak	_ZN4node7TTYWrapD1Ev
	.set	_ZN4node7TTYWrapD1Ev,_ZN4node7TTYWrapD2Ev
	.section	.text._ZN4node7TTYWrapD0Ev,"axG",@progbits,_ZN4node7TTYWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7TTYWrapD0Ev
	.type	_ZN4node7TTYWrapD0Ev, @function
_ZN4node7TTYWrapD0Ev:
.LFB9602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	136(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 128(%rdi)
	testq	%rdx, %rdx
	je	.L195
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L196
	leaq	128(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L198
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L214:
	cmpq	%rax, %rcx
	je	.L217
.L198:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L214
.L196:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	movq	144(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	96(%r12), %rbx
	movq	%rax, 88(%r12)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L219:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	96(%r12), %rax
	cmpq	%rbx, %rax
	je	.L218
	movq	%rax, %rbx
.L202:
	testq	%rbx, %rbx
	jne	.L219
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$472, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L217:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L195
	.cfi_endproc
.LFE9602:
	.size	_ZN4node7TTYWrapD0Ev, .-_ZN4node7TTYWrapD0Ev
	.section	.rodata.str1.1
.LC5:
	.string	"uv_tty_init"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L235
	cmpw	$1040, %cx
	jne	.L221
.L235:
	movq	23(%rdx), %r13
.L223:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L224
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L240
.L224:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L241
	movq	8(%rbx), %rdi
.L226:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L220
	sarq	$32, %rax
	movq	%rax, %r12
	testl	%eax, %eax
	js	.L242
	movq	8(%rbx), %rdi
	leaq	8(%rdi), %rdx
	subq	$8, %rdi
	cmpl	$1, 16(%rbx)
	jg	.L230
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L230:
	movq	%rdx, -64(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	$472, %edi
	movb	%al, -49(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	movl	$33, %r8d
	movq	%r13, %rsi
	leaq	160(%rax), %r15
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	%r15, %rcx
	call	_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node7TTYWrapE(%rip), %rsi
	movq	360(%r13), %rax
	movl	%r12d, %edx
	leaq	208(%rsi), %rcx
	movq	%rsi, (%r14)
	movq	%r15, %rsi
	movq	%rcx, 88(%r14)
	movq	2360(%rax), %rdi
	movzbl	-49(%rbp), %ecx
	call	uv_tty_init@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L243
.L220:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	addq	$88, %rdi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L240:
	cmpl	$5, 43(%rax)
	jne	.L224
	leaq	_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%r14, %rdi
	call	_ZN4node10HandleWrap19MarkAsUninitializedEv@PLT
	cmpl	$2, 16(%rbx)
	jle	.L244
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
.L233:
	subq	$8, %rsp
	movl	%r12d, %edx
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	leaq	.LC5(%rip), %rcx
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L221:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7622:
	.size	_ZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L246
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L246:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L255
.L247:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L247
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7145:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TTYWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi
	.type	_ZN4node7TTYWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi, @function
_ZN4node7TTYWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi:
.LFB7636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	leaq	160(%rdi), %r9
	pushq	%r13
	movq	%r9, %rcx
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	movl	$33, %r8d
	subq	$24, %rsp
	movq	%r9, -56(%rbp)
	call	_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE@PLT
	movq	-56(%rbp), %r9
	movzbl	%bl, %ecx
	movl	%r15d, %edx
	leaq	16+_ZTVN4node7TTYWrapE(%rip), %rax
	movq	%rax, (%r12)
	addq	$208, %rax
	movq	%r9, %rsi
	movq	%rax, 88(%r12)
	movq	360(%r13), %rax
	movq	2360(%rax), %rdi
	call	uv_tty_init@PLT
	movl	%eax, (%r14)
	testl	%eax, %eax
	jne	.L259
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10HandleWrap19MarkAsUninitializedEv@PLT
	.cfi_endproc
.LFE7636:
	.size	_ZN4node7TTYWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi, .-_ZN4node7TTYWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi
	.globl	_ZN4node7TTYWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi
	.set	_ZN4node7TTYWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi,_ZN4node7TTYWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEibPi
	.p2align 4
	.globl	_Z18_register_tty_wrapv
	.type	_Z18_register_tty_wrapv, @function
_Z18_register_tty_wrapv:
.LFB7638:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7638:
	.size	_Z18_register_tty_wrapv, .-_Z18_register_tty_wrapv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L278
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L279
.L264:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L268
.L261:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L269
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L265:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L264
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L270
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L266:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L268:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L261
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L270:
	xorl	%edx, %edx
	jmp	.L266
	.cfi_endproc
.LFE8336:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L281
	leaq	16(%r12), %rsi
.L282:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L283
	addq	$16, %r12
.L284:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L285
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L301
.L285:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L302
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L289
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L303
.L280:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L292
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L287:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L289:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L292:
	xorl	%edx, %edx
	jmp	.L287
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7613:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L306
	leaq	40(%r12), %rsi
.L307:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L308
	addq	$40, %r12
.L309:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L310
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L326
.L310:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L327
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L314
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L328
.L305:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L329
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L328:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L327:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L317
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L312:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L314:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L317:
	xorl	%edx, %edx
	jmp	.L312
.L329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7615:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node7TTYWrapE
	.section	.data.rel.ro._ZTVN4node7TTYWrapE,"awG",@progbits,_ZTVN4node7TTYWrapE,comdat
	.align 8
	.type	_ZTVN4node7TTYWrapE, @object
	.size	_ZTVN4node7TTYWrapE, 368
_ZTVN4node7TTYWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node7TTYWrapD1Ev
	.quad	_ZN4node7TTYWrapD0Ev
	.quad	_ZNK4node7TTYWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node7TTYWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node7TTYWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node15LibuvStreamWrap5GetFDEv
	.quad	_ZN4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZN4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZN4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZN4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	-88
	.quad	0
	.quad	_ZThn88_N4node7TTYWrapD1Ev
	.quad	_ZThn88_N4node7TTYWrapD0Ev
	.quad	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.quad	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.section	.rodata.str1.1
.LC6:
	.string	"../src/tty_wrap.cc"
.LC7:
	.string	"tty_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC6
	.quad	0
	.quad	_ZN4node7TTYWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC7
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC8:
	.string	"../src/tty_wrap.cc:121"
.LC9:
	.string	"(fd) >= (0)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"static void node::TTYWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"../src/tty_wrap.cc:117"
.LC12:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TTYWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC10
	.section	.rodata.str1.1
.LC13:
	.string	"../src/tty_wrap.cc:86"
.LC14:
	.string	"args[0]->IsArray()"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"static void node::TTYWrap::GetWindowSize(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TTYWrap13GetWindowSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/tty_wrap.cc:73"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"static void node::TTYWrap::IsTTY(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TTYWrap5IsTTYERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC16
	.quad	.LC9
	.quad	.LC17
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC18:
	.string	"../src/stream_base-inl.h:103"
.LC19:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC21:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC23:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC24:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC24
	.quad	.LC22
	.quad	.LC25
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC26:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC28:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC29:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC31:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC32:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC34:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
