	.file	"node_process_object.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4391:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4391:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.text
	.p2align 4
	.type	_ZN4nodeL18GetParentProcessIdEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL18GetParentProcessIdEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE:
.LFB7511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rsi), %rbx
	call	uv_os_getppid@PLT
	salq	$32, %rax
	movq	%rax, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7511:
	.size	_ZN4nodeL18GetParentProcessIdEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL18GetParentProcessIdEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"node"
	.text
	.p2align 4
	.type	_ZN4nodeL18ProcessTitleGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL18ProcessTitleGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE:
.LFB7501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node15GetProcessTitleB5cxx11EPKc@PLT
	movq	(%rbx), %rbx
	movl	-56(%rbp), %ecx
	xorl	%edx, %edx
	movq	-64(%rbp), %rsi
	movq	16(%rbx), %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L11
	movq	(%rax), %rax
	movq	%rax, 32(%rbx)
.L8:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	24(%rbx), %rax
	movq	%rax, 32(%rbx)
	jmp	.L8
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7501:
	.size	_ZN4nodeL18ProcessTitleGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL18ProcessTitleGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC1:
	.string	"__metadata"
.LC2:
	.string	"name"
.LC3:
	.string	"process_name"
	.text
	.p2align 4
	.type	_ZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE, @function
_ZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE:
.LFB7502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	leaq	-1072(%rbp), %rdi
	subq	$1112, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%r8, %rdx
	movq	16(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	_ZZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEEE27trace_event_unique_atomic44(%rip), %r12
	testq	%r12, %r12
	je	.L41
.L15:
	movq	-1056(%rbp), %rdi
	testb	$5, (%r12)
	jne	.L42
.L17:
	call	uv_set_process_title@PLT
	movq	-1056(%rbp), %rdi
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L13
	testq	%rdi, %rdi
	je	.L13
	call	free@PLT
.L13:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	.LC2(%rip), %rax
	movq	%rdi, -1096(%rbp)
	movaps	%xmm0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movb	$7, -1105(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	-1088(%rbp), %rax
	pushq	$0
	leaq	-1105(%rbp), %r9
	pushq	%rax
	leaq	-1096(%rbp), %rax
	leaq	-1104(%rbp), %r8
	pushq	%rax
	leaq	.LC3(%rip), %rdx
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	movq	-1080(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L18
	movq	(%rdi), %rax
	call	*8(%rax)
.L18:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	call	*8(%rax)
.L19:
	movq	-1056(%rbp), %rdi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L41:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L16
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L44
.L16:
	movq	%r12, _ZZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEEE27trace_event_unique_atomic44(%rip)
	mfence
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L16
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7502:
	.size	_ZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE, .-_ZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE
	.p2align 4
	.type	_ZN4nodeL15DebugPortGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL15DebugPortGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE:
.LFB7506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L73
	cmpw	$1040, %cx
	jne	.L46
.L73:
	movq	23(%rdx), %rax
.L48:
	movq	1664(%rax), %r12
	movq	1656(%rax), %r13
	testq	%r12, %r12
	je	.L49
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r12), %r14
	testq	%r15, %r15
	je	.L50
	lock addl	$1, (%r14)
	lock addl	$1, (%r14)
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	testq	%r15, %r15
	je	.L79
.L68:
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
	cmpl	$1, %eax
	je	.L80
.L57:
	movl	72(%r13), %eax
	testl	%eax, %eax
	js	.L66
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%r13, %rdi
	movq	%rax, 32(%rdx)
	call	uv_mutex_unlock@PLT
	testq	%r15, %r15
	je	.L81
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
	cmpl	$1, %eax
	je	.L82
.L45:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	addl	$1, 8(%r12)
	movq	%r13, %rdi
	addl	$1, 8(%r12)
	call	uv_mutex_lock@PLT
	testq	%r15, %r15
	jne	.L68
.L79:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L57
.L80:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L55
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L56:
	cmpl	$1, %eax
	jne	.L57
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movl	72(%r13), %eax
	testl	%eax, %eax
	js	.L66
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%r13, %rdi
	movq	%rax, 32(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L45
.L82:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L61
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L62:
	cmpl	$1, %eax
	jne	.L45
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	_ZZNK4node8HostPort4portEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L61:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L62
	.cfi_endproc
.LFE7506:
	.size	_ZN4nodeL15DebugPortGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL15DebugPortGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL15DebugPortSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE, @function
_ZN4nodeL15DebugPortSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE:
.LFB7510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L110
	cmpw	$1040, %cx
	jne	.L84
.L110:
	movq	23(%rdx), %r12
.L86:
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movl	$0, %r13d
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	1656(%r12), %r14
	movq	1664(%r12), %r12
	movq	%rax, %rdx
	sarq	$32, %rdx
	testb	%al, %al
	cmovne	%edx, %r13d
	testq	%r12, %r12
	je	.L88
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r12), %rbx
	testq	%r15, %r15
	je	.L89
	lock addl	$1, (%rbx)
	lock addl	$1, (%rbx)
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	testq	%r15, %r15
	je	.L114
.L105:
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L115
.L96:
	movl	%r13d, 72(%r14)
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testq	%r15, %r15
	je	.L116
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L117
.L83:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	addl	$1, 8(%r12)
	movq	%r14, %rdi
	addl	$1, 8(%r12)
	call	uv_mutex_lock@PLT
	testq	%r15, %r15
	jne	.L105
.L114:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L96
.L115:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L94
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L95:
	cmpl	$1, %eax
	jne	.L96
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movl	%r13d, 72(%r14)
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L83
.L117:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L100
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L101:
	cmpl	$1, %eax
	jne	.L83
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L100:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L94:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L95
	.cfi_endproc
.LFE7510:
	.size	_ZN4nodeL15DebugPortSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE, .-_ZN4nodeL15DebugPortSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE
	.section	.rodata.str1.1
.LC4:
	.string	"version"
.LC5:
	.string	"versions"
.LC6:
	.string	"v8"
.LC7:
	.string	"uv"
.LC8:
	.string	"zlib"
.LC9:
	.string	"brotli"
.LC10:
	.string	"ares"
.LC11:
	.string	"modules"
.LC12:
	.string	"nghttp2"
.LC13:
	.string	"napi"
.LC14:
	.string	"llhttp"
.LC15:
	.string	"http_parser"
.LC16:
	.string	"openssl"
.LC17:
	.string	"cldr"
.LC18:
	.string	"icu"
.LC19:
	.string	"tz"
.LC20:
	.string	"unicode"
.LC21:
	.string	"arch"
.LC22:
	.string	"platform"
.LC23:
	.string	"release"
.LC24:
	.string	"lts"
.LC25:
	.string	"sourceUrl"
.LC26:
	.string	"headersUrl"
.LC27:
	.string	"_rawDebug"
.LC28:
	.string	"v12.18.4"
	.text
	.p2align 4
	.globl	_ZN4node19CreateProcessObjectEPNS_11EnvironmentE
	.type	_ZN4node19CreateProcessObjectEPNS_11EnvironmentE, @function
_ZN4node19CreateProcessObjectEPNS_11EnvironmentE:
.LFB7512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	352(%rdi), %r13
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	3280(%rbx), %r12
	pushq	$0
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r14
	movq	360(%rbx), %rax
	movq	%r14, %rdi
	movq	1416(%rax), %rsi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	testq	%rax, %rax
	je	.L258
	movq	%rax, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L258
	movq	352(%rbx), %rdi
	movl	$8, %ecx
	xorl	%edx, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L267
.L257:
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r13, %rdi
	movq	%r9, -104(%rbp)
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L268
.L121:
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L269
.L122:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	xorl	%edx, %edx
	movl	$8, %ecx
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L270
.L123:
	movq	-104(%rbp), %rcx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L271
.L124:
	cmpq	$0, 8+_ZN4node11per_process8metadataE(%rip)
	jne	.L272
.L125:
	cmpq	$0, 40+_ZN4node11per_process8metadataE(%rip)
	jne	.L273
.L131:
	cmpq	$0, 72+_ZN4node11per_process8metadataE(%rip)
	jne	.L274
.L137:
	cmpq	$0, 104+_ZN4node11per_process8metadataE(%rip)
	jne	.L275
.L143:
	cmpq	$0, 136+_ZN4node11per_process8metadataE(%rip)
	jne	.L276
.L149:
	cmpq	$0, 168+_ZN4node11per_process8metadataE(%rip)
	jne	.L277
.L155:
	cmpq	$0, 200+_ZN4node11per_process8metadataE(%rip)
	jne	.L278
.L161:
	cmpq	$0, 232+_ZN4node11per_process8metadataE(%rip)
	jne	.L279
.L167:
	cmpq	$0, 264+_ZN4node11per_process8metadataE(%rip)
	jne	.L280
.L173:
	cmpq	$0, 296+_ZN4node11per_process8metadataE(%rip)
	jne	.L281
.L179:
	cmpq	$0, 328+_ZN4node11per_process8metadataE(%rip)
	jne	.L282
.L185:
	cmpq	$0, 360+_ZN4node11per_process8metadataE(%rip)
	jne	.L283
.L191:
	cmpq	$0, 392+_ZN4node11per_process8metadataE(%rip)
	jne	.L284
.L197:
	cmpq	$0, 424+_ZN4node11per_process8metadataE(%rip)
	jne	.L285
.L203:
	cmpq	$0, 456+_ZN4node11per_process8metadataE(%rip)
	jne	.L286
.L209:
	cmpq	$0, 488+_ZN4node11per_process8metadataE(%rip)
	jne	.L287
.L215:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	648+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L288
	movq	640+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L222
.L223:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	movq	%r9, -104(%rbp)
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L289
.L224:
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L290
.L225:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	680+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L291
	movq	672+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L227
.L228:
	xorl	%edx, %edx
	movl	$8, %ecx
	movq	%r13, %rdi
	movq	%r9, -104(%rbp)
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L292
.L229:
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L293
.L230:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r13, %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L294
.L231:
	movq	-104(%rbp), %rcx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L295
.L232:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	520+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L296
	movq	512+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L234
.L235:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L297
.L236:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L298
.L237:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	552+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L299
	movq	544+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L239
.L240:
	xorl	%edx, %edx
	movl	$3, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC24(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L300
.L241:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L301
.L242:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	584+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L302
	movq	576+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L244
.L245:
	xorl	%edx, %edx
	movl	$9, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L303
.L246:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L304
.L247:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	616+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L305
	movq	608+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L249
.L250:
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC26(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L306
.L251:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L307
.L252:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	movq	_ZN4node8RawDebugERKN2v820FunctionCallbackInfoINS0_5ValueEEE@GOTPCREL(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L308
.L253:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L309
.L254:
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L310
.L255:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L258:
	xorl	%r12d, %r12d
.L120:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L311
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	488+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L312
	movq	480+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L217
.L218:
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L313
.L219:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L215
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	456+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L314
	movq	448+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L211
.L212:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L315
.L213:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L209
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	424+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L316
	movq	416+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L205
.L206:
	xorl	%edx, %edx
	movl	$3, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L317
.L207:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L203
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	392+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L318
	movq	384+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L199
.L200:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L319
.L201:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L197
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	360+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L320
	movq	352+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L193
.L194:
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L321
.L195:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L191
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	328+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L322
	movq	320+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L187
.L188:
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L323
.L189:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L185
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	296+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L324
	movq	288+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L181
.L182:
	xorl	%edx, %edx
	movl	$6, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L325
.L183:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L179
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	264+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L326
	movq	256+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L175
.L176:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L327
.L177:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L173
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	232+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L328
	movq	224+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L169
.L170:
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L329
.L171:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L167
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	200+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L330
	movq	192+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L163
.L164:
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L331
.L165:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L161
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	168+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L332
	movq	160+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L157
.L158:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L333
.L159:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L155
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	136+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L334
	movq	128+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L151
.L152:
	xorl	%edx, %edx
	movl	$6, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L335
.L153:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L149
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	104+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L336
	movq	96+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L145
.L146:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L337
.L147:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L143
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	72+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L338
	movq	64+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L139
.L140:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L339
.L141:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L137
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	40+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L340
	movq	32+_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L133
.L134:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L341
.L135:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L131
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	8+_ZN4node11per_process8metadataE(%rip), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L342
	movq	_ZN4node11per_process8metadataE(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L127
.L128:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	movq	%r9, -112(%rbp)
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L343
.L129:
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L125
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L288:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L222:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%rax, -112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %r9
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L290:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L291:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L227:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%rax, -112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %r9
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L293:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%rax, -112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %rdx
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L295:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L296:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L234:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L298:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L299:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L239:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L301:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L302:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L244:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L304:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L305:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L249:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L307:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L308:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L309:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%r9, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %r9
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%r9, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rdx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L269:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%rax, -112(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %rdx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L271:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L124
.L314:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L211:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L212
.L330:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L163:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L164
.L328:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L169:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L170
.L327:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L177
.L326:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L175:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L176
.L338:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L139:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L140
.L336:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L145:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L146
.L335:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L153
.L334:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L151:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L152
.L337:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L147
.L331:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L165
.L332:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L157:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L158
.L333:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L159
.L313:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L219
.L312:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L217:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L218
.L322:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L187:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L188
.L320:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L193:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L194
.L319:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L201
.L318:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L199:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L200
.L321:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L195
.L315:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L213
.L316:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L205:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L206
.L317:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L207
.L342:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L127:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L128
.L343:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L129
.L339:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L141
.L340:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L133:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L134
.L341:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L135
.L329:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L171
.L323:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L189
.L324:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L181:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%r9d, %r9d
	jmp	.L182
.L325:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	movq	-112(%rbp), %r9
	jmp	.L183
.L311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7512:
	.size	_ZN4node19CreateProcessObjectEPNS_11EnvironmentE, .-_ZN4node19CreateProcessObjectEPNS_11EnvironmentE
	.section	.text._ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE,"axG",@progbits,_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE,comdat
	.p2align 4
	.weak	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	.type	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE, @function
_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE:
.LFB8252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1128, %rsp
	movq	%rdi, -1144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L398
.L345:
	leaq	-1136(%rbp), %r14
	movq	%r12, %rsi
	leaq	-1080(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	(%rbx), %rcx
	movq	8(%rbx), %r15
	movq	%r13, %rax
	movdqa	.LC29(%rip), %xmm0
	movq	%r13, -1088(%rbp)
	leaq	-56(%rbp), %rdx
	subq	%rcx, %r15
	sarq	$5, %r15
	movaps	%xmm0, -1104(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L346:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L346
	movq	$0, -1080(%rbp)
	cmpq	$128, %r15
	jbe	.L353
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r15,8), %rdi
	andq	%r15, %rax
	cmpq	%rax, %r15
	jne	.L399
	movq	%rcx, -1160(%rbp)
	movq	%r9, -1152(%rbp)
	testq	%rdi, %rdi
	je	.L349
	movq	%rdi, -1168(%rbp)
	call	malloc@PLT
	movq	-1168(%rbp), %rdi
	movq	-1152(%rbp), %r9
	testq	%rax, %rax
	movq	-1160(%rbp), %rcx
	je	.L400
	movq	%rax, -1088(%rbp)
	movq	%r15, -1096(%rbp)
.L353:
	movq	%r9, -1104(%rbp)
	testq	%r9, %r9
	je	.L354
	xorl	%r15d, %r15d
	testq	%r12, %r12
	jne	.L359
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%rax, (%rdx)
	movq	(%rbx), %rcx
	addq	$1, %r15
	movq	8(%rbx), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%r15, %rax
	jbe	.L368
.L359:
	movq	%r15, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movq	8(%rax), %rcx
	cmpq	$1073741798, %rcx
	ja	.L356
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r9
	cmpq	%r15, %r9
	jbe	.L364
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	testq	%rax, %rax
	jne	.L401
	jmp	.L369
.L394:
	movq	%rax, (%rdx)
	movq	(%rbx), %rcx
	addq	$1, %r15
	movq	8(%rbx), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%rax, %r15
	jnb	.L368
.L355:
	movq	%r15, %rsi
	movq	-1144(%rbp), %rdi
	salq	$5, %rsi
	addq	%rcx, %rsi
	movq	%rsi, -1152(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-1152(%rbp), %rsi
	movq	%rax, %rdi
	movq	8(%rsi), %rcx
	cmpq	$1073741798, %rcx
	ja	.L402
	movq	(%rsi), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r9
	cmpq	%r15, %r9
	jbe	.L364
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	testq	%rax, %rax
	jne	.L394
	jmp	.L369
.L364:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L402:
	movq	%rax, %r12
.L356:
	movq	%r12, %rdi
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
	cmpq	-1104(%rbp), %r15
	jnb	.L364
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
.L369:
	movq	$0, (%rdx)
	xorl	%r12d, %r12d
.L367:
	testq	%rsi, %rsi
	je	.L366
	cmpq	%r13, %rsi
	je	.L366
	movq	%rsi, %rdi
	call	free@PLT
.L366:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$1128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L354:
	.cfi_restore_state
	movq	-1088(%rbp), %rsi
.L368:
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-1088(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L367
.L398:
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r12
	jmp	.L345
.L349:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L400:
	movq	%rdi, -1152(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-1152(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L349
	movq	(%rbx), %rcx
	movq	8(%rbx), %r9
	movq	%rdi, -1088(%rbp)
	movq	-1104(%rbp), %rax
	movq	%r15, -1096(%rbp)
	subq	%rcx, %r9
	sarq	$5, %r9
	testq	%rax, %rax
	je	.L352
	leaq	0(,%rax,8), %rdx
	movq	%r13, %rsi
	movq	%r9, -1160(%rbp)
	movq	%rcx, -1152(%rbp)
	call	memcpy@PLT
	movq	-1160(%rbp), %r9
	movq	-1152(%rbp), %rcx
.L352:
	movq	%r15, -1104(%rbp)
	cmpq	%r9, %r15
	jnb	.L353
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L399:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8252:
	.size	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE, .-_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	.section	.rodata.str1.1
.LC30:
	.string	"title"
.LC31:
	.string	"argv"
.LC32:
	.string	"execArgv"
.LC33:
	.string	"pid"
.LC34:
	.string	"ppid"
.LC35:
	.string	"REVERT_CVE_2019_9512"
.LC36:
	.string	"REVERT_CVE_2019_9514"
.LC37:
	.string	"REVERT_CVE_2019_9516"
.LC38:
	.string	"REVERT_CVE_2019_9518"
.LC39:
	.string	"execPath"
.LC40:
	.string	"debugPort"
	.text
	.p2align 4
	.globl	_ZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	8(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L407
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L407
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L407
	movq	271(%rax), %rbx
.L406:
	movl	16(%r14), %edx
	testl	%edx, %edx
	jg	.L408
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L462
.L410:
	movl	16(%r14), %eax
	testl	%eax, %eax
	jle	.L463
	movq	8(%r14), %r14
.L412:
	testb	$2, 1932(%rbx)
	movl	$0, %r8d
	movq	%r13, %rdi
	movq	2680(%rbx), %r9
	leaq	_ZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE(%rip), %r15
	movl	$5, %ecx
	leaq	.LC30(%rip), %rsi
	cmove	%r8, %r15
	xorl	%edx, %edx
	movq	%r9, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L464
.L414:
	pushq	$0
	movq	%r15, %r8
	leaq	_ZN4nodeL18ProcessTitleGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	pushq	$1
	movq	%r14, %rdi
	pushq	$0
	pushq	$0
	call	_ZN2v86Object11SetAccessorENS_5LocalINS_7ContextEEENS1_INS_4NameEEEPFvS5_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS5_NS1_IS7_EERKNS6_IvEEENS_10MaybeLocalIS7_EENS_13AccessControlENS_17PropertyAttributeENS_14SideEffectTypeESN_@PLT
	addq	$32, %rsp
	testb	%al, %al
	je	.L465
.L415:
	shrw	$8, %ax
	je	.L466
	leaq	1696(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L467
.L417:
	xorl	%edx, %edx
	movl	$4, %ecx
	leaq	.LC31(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L468
.L418:
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L469
.L419:
	leaq	1672(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L470
.L420:
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC32(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L471
.L421:
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L472
.L422:
	call	uv_os_getpid@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$3, %ecx
	movq	%r13, %rdi
	leaq	.LC33(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L473
.L423:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L474
.L424:
	xorl	%edx, %edx
	movl	$4, %ecx
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L475
.L425:
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	_ZN4nodeL18GetParentProcessIdEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE(%rip), %rcx
	pushq	$0
	movq	%r12, %rsi
	movq	%r14, %rdi
	pushq	$0
	pushq	$0
	call	_ZN2v86Object11SetAccessorENS_5LocalINS_7ContextEEENS1_INS_4NameEEEPFvS5_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS5_NS1_IS7_EERKNS6_IvEEENS_10MaybeLocalIS7_EENS_13AccessControlENS_17PropertyAttributeENS_14SideEffectTypeESN_@PLT
	addq	$32, %rsp
	testb	%al, %al
	je	.L476
.L426:
	shrw	$8, %ax
	je	.L477
	movl	_ZN4node11per_process12reverted_cveE(%rip), %eax
	testb	$1, %al
	jne	.L478
.L428:
	testb	$2, %al
	jne	.L479
.L431:
	testb	$4, %al
	jne	.L480
.L434:
	testb	$8, %al
	jne	.L481
.L437:
	movq	1720(%rbx), %rsi
	movl	1728(%rbx), %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L482
.L440:
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC39(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L483
.L441:
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L484
.L442:
	testb	$2, 1932(%rbx)
	movq	2680(%rbx), %r15
	movq	%r13, %rdi
	movl	$0, %r8d
	leaq	_ZN4nodeL15DebugPortSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEE(%rip), %rbx
	movl	$9, %ecx
	leaq	.LC40(%rip), %rsi
	cmove	%r8, %rbx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L485
.L444:
	pushq	$0
	movq	%r15, %r9
	movq	%rbx, %r8
	leaq	_ZN4nodeL15DebugPortGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE(%rip), %rcx
	pushq	$0
	movq	%r12, %rsi
	movq	%r14, %rdi
	pushq	$0
	pushq	$0
	call	_ZN2v86Object11SetAccessorENS_5LocalINS_7ContextEEENS1_INS_4NameEEEPFvS5_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS5_NS1_IS7_EERKNS6_IvEEENS_10MaybeLocalIS7_EENS_13AccessControlENS_17PropertyAttributeENS_14SideEffectTypeESN_@PLT
	addq	$32, %rsp
	testb	%al, %al
	je	.L486
.L445:
	shrw	$8, %ax
	je	.L487
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L408:
	movq	8(%r14), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L410
.L462:
	leaq	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L407:
	xorl	%ebx, %ebx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L481:
	xorl	%edx, %edx
	movl	$20, %ecx
	leaq	112(%r13), %r15
	movq	%r13, %rdi
	leaq	.LC38(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L488
.L438:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	jne	.L437
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L480:
	xorl	%edx, %edx
	movl	$20, %ecx
	leaq	112(%r13), %r15
	movq	%r13, %rdi
	leaq	.LC37(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L489
.L435:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L490
.L436:
	movl	_ZN4node11per_process12reverted_cveE(%rip), %eax
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L479:
	xorl	%edx, %edx
	movl	$20, %ecx
	leaq	112(%r13), %r15
	movq	%r13, %rdi
	leaq	.LC36(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L491
.L432:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L492
.L433:
	movl	_ZN4node11per_process12reverted_cveE(%rip), %eax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L478:
	xorl	%edx, %edx
	movl	$20, %ecx
	leaq	112(%r13), %r15
	movq	%r13, %rdi
	leaq	.LC35(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L493
.L429:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L494
.L430:
	movl	_ZN4node11per_process12reverted_cveE(%rip), %eax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L465:
	movl	%eax, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-56(%rbp), %eax
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L476:
	movl	%eax, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-56(%rbp), %eax
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L474:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L467:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L472:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L470:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L469:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L482:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L484:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L487:
	leaq	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L486:
	movl	%eax, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-56(%rbp), %eax
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L485:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L490:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L492:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L494:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L438
	.cfi_endproc
.LFE7516:
	.size	_ZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args
	.section	.rodata.str1.1
.LC41:
	.string	"../src/util.h:352"
.LC42:
	.string	"(index) < (length())"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC44:
	.string	"../src/util.h:391"
.LC45:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/util-inl.h:374"
.LC48:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC50:
	.string	"../src/util-inl.h:325"
.LC51:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"../src/node_process_object.cc:194"
	.align 8
.LC54:
	.string	"process ->SetAccessor(context, FIXED_ONE_BYTE_STRING(isolate, \"debugPort\"), DebugPortGetter, env->owns_process_state() ? DebugPortSetter : nullptr, env->as_callback_data()) .FromJust()"
	.align 8
.LC55:
	.string	"void node::PatchProcessObject(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2, @object
	.size	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2, 24
_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"../src/node_process_object.cc:168"
	.align 8
.LC57:
	.string	"process->SetAccessor(context, FIXED_ONE_BYTE_STRING(isolate, \"ppid\"), GetParentProcessId).FromJust()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, @object
	.size	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, 24
_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC55
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"../src/node_process_object.cc:142"
	.align 8
.LC59:
	.string	"process ->SetAccessor( context, FIXED_ONE_BYTE_STRING(isolate, \"title\"), ProcessTitleGetter, env->owns_process_state() ? ProcessTitleSetter : nullptr, env->as_callback_data(), DEFAULT, None, SideEffectType::kHasNoSideEffect) .FromJust()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC55
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"../src/node_process_object.cc:138"
	.section	.rodata.str1.1
.LC61:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4node18PatchProcessObjectERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC55
	.local	_ZZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEEE27trace_event_unique_atomic44
	.comm	_ZZN4nodeL18ProcessTitleSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIvEEE27trace_event_unique_atomic44,8,8
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZNK4node8HostPort4portEvE4args
	.section	.rodata.str1.1
.LC62:
	.string	"../src/node_options.h:33"
.LC63:
	.string	"(port_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"int node::HostPort::port() const"
	.section	.data.rel.ro.local._ZZNK4node8HostPort4portEvE4args,"awG",@progbits,_ZZNK4node8HostPort4portEvE4args,comdat
	.align 16
	.type	_ZZNK4node8HostPort4portEvE4args, @gnu_unique_object
	.size	_ZZNK4node8HostPort4portEvE4args, 24
_ZZNK4node8HostPort4portEvE4args:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC29:
	.quad	0
	.quad	128
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
