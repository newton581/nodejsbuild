	.file	"heap_utils.cc"
	.text
	.section	.text._ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi,"axG",@progbits,_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.type	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi, @function
_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi:
.LFB4686:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4686:
	.size	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi, .-_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4687:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4687:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node10IsRootNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node10IsRootNodeEv
	.type	_ZN2v813EmbedderGraph4Node10IsRootNodeEv, @function
_ZN2v813EmbedderGraph4Node10IsRootNodeEv:
.LFB4688:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4688:
	.size	_ZN2v813EmbedderGraph4Node10IsRootNodeEv, .-_ZN2v813EmbedderGraph4Node10IsRootNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4689:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4689:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node10NamePrefixEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node10NamePrefixEv
	.type	_ZN2v813EmbedderGraph4Node10NamePrefixEv, @function
_ZN2v813EmbedderGraph4Node10NamePrefixEv:
.LFB4690:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4690:
	.size	_ZN2v813EmbedderGraph4Node10NamePrefixEv, .-_ZN2v813EmbedderGraph4Node10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7484:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7484:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7485:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7485:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7486:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7486:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7488:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L11
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L11:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7488:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7538:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7538:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB7595:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7595:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB7596:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7596:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB7612:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7612:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.rodata._ZN4node4heap13JSGraphJSNode4NameEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"<JS Node>"
	.section	.text._ZN4node4heap13JSGraphJSNode4NameEv,"axG",@progbits,_ZN4node4heap13JSGraphJSNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap13JSGraphJSNode4NameEv
	.type	_ZN4node4heap13JSGraphJSNode4NameEv, @function
_ZN4node4heap13JSGraphJSNode4NameEv:
.LFB7666:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE7666:
	.size	_ZN4node4heap13JSGraphJSNode4NameEv, .-_ZN4node4heap13JSGraphJSNode4NameEv
	.section	.text._ZN4node4heap13JSGraphJSNode11SizeInBytesEv,"axG",@progbits,_ZN4node4heap13JSGraphJSNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap13JSGraphJSNode11SizeInBytesEv
	.type	_ZN4node4heap13JSGraphJSNode11SizeInBytesEv, @function
_ZN4node4heap13JSGraphJSNode11SizeInBytesEv:
.LFB7667:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7667:
	.size	_ZN4node4heap13JSGraphJSNode11SizeInBytesEv, .-_ZN4node4heap13JSGraphJSNode11SizeInBytesEv
	.section	.text._ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv,"axG",@progbits,_ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv
	.type	_ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv, @function
_ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv:
.LFB7668:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7668:
	.size	_ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv, .-_ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_116FileOutputStream12GetChunkSizeEv, @function
_ZN4node4heap12_GLOBAL__N_116FileOutputStream12GetChunkSizeEv:
.LFB7812:
	.cfi_startproc
	endbr64
	movl	$65536, %eax
	ret
	.cfi_endproc
.LFE7812:
	.size	_ZN4node4heap12_GLOBAL__N_116FileOutputStream12GetChunkSizeEv, .-_ZN4node4heap12_GLOBAL__N_116FileOutputStream12GetChunkSizeEv
	.set	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetChunkSizeEv,_ZN4node4heap12_GLOBAL__N_116FileOutputStream12GetChunkSizeEv
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_116FileOutputStream11EndOfStreamEv, @function
_ZN4node4heap12_GLOBAL__N_116FileOutputStream11EndOfStreamEv:
.LFB7813:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7813:
	.size	_ZN4node4heap12_GLOBAL__N_116FileOutputStream11EndOfStreamEv, .-_ZN4node4heap12_GLOBAL__N_116FileOutputStream11EndOfStreamEv
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv:
.LFB7833:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7833:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv:
.LFB7836:
	.cfi_startproc
	endbr64
	cmpq	$0, 128(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE7836:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv:
.LFB7837:
	.cfi_startproc
	endbr64
	cmpq	$0, 128(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE7837:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv:
.LFB7838:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE7838:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv
	.align 2
	.p2align 4
	.type	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream8SelfSizeEv, @function
_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream8SelfSizeEv:
.LFB7841:
	.cfi_startproc
	endbr64
	movl	$136, %eax
	ret
	.cfi_endproc
.LFE7841:
	.size	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream8SelfSizeEv, .-_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD2Ev, @function
_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD2Ev:
.LFB10940:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10940:
	.size	_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD2Ev, .-_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD2Ev
	.set	_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD1Ev,_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD2Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB10997:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE10997:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB10998:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10998:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB11000:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE11000:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB11001:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE11001:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11002:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11002:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB11004:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE11004:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD0Ev, @function
_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD0Ev:
.LFB10942:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10942:
	.size	_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD0Ev, .-_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD0Ev
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10956:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L34
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE10956:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10958:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZN4node4heap13JSGraphJSNodeD2Ev,"axG",@progbits,_ZN4node4heap13JSGraphJSNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap13JSGraphJSNodeD2Ev
	.type	_ZN4node4heap13JSGraphJSNodeD2Ev, @function
_ZN4node4heap13JSGraphJSNodeD2Ev:
.LFB9215:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node4heap13JSGraphJSNodeE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	ret
	.cfi_endproc
.LFE9215:
	.size	_ZN4node4heap13JSGraphJSNodeD2Ev, .-_ZN4node4heap13JSGraphJSNodeD2Ev
	.weak	_ZN4node4heap13JSGraphJSNodeD1Ev
	.set	_ZN4node4heap13JSGraphJSNodeD1Ev,_ZN4node4heap13JSGraphJSNodeD2Ev
	.section	.text._ZN4node4heap13JSGraphJSNodeD0Ev,"axG",@progbits,_ZN4node4heap13JSGraphJSNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap13JSGraphJSNodeD0Ev
	.type	_ZN4node4heap13JSGraphJSNodeD0Ev, @function
_ZN4node4heap13JSGraphJSNodeD0Ev:
.LFB9217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap13JSGraphJSNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L42:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9217:
	.size	_ZN4node4heap13JSGraphJSNodeD0Ev, .-_ZN4node4heap13JSGraphJSNodeD0Ev
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7631:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L52
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L52:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7631:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7630:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L58
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L58:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7630:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.text
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB7835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7835:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE:
.LFB7834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7834:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv:
.LFB7830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_buf_init@PLT
	movq	64(%rbx), %rdi
	movq	$-4095, %rsi
	movq	%rax, -48(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	call	*24(%rax)
	movq	128(%rbx), %rdi
	movq	$0, 128(%rbx)
	testq	%rdi, %rdi
	je	.L63
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L63:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7830:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev:
.LFB10952:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	addq	$16, %rdi
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE10952:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB10954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10954:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB10950:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	movq	%rax, (%r12)
	call	uv_buf_init@PLT
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	testq	%r13, %r13
	je	.L75
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L81
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L75:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10950:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$96, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L87
	cmpw	$1040, %cx
	jne	.L83
.L87:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L89
.L86:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	32(%rbx), %rsi
	leaq	40(%r12), %rdi
	movsd	.LC2(%rip), %xmm0
	movq	%rdx, 32(%r12)
	movl	$39, %ecx
	movq	%r13, %rdx
	movq	%rax, 24(%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L86
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7658:
	.size	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.text
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv:
.LFB7832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	128(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L93
	leaq	120(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
	call	_ZNK2v812HeapSnapshot9SerializeEPNS_12OutputStreamENS0_19SerializationFormatE@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	leaq	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7832:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci:
.LFB7831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L99
	movslq	%edx, %rbx
	leaq	-80(%rbp), %rax
	movq	%rdi, %r15
	movq	%rsi, %r13
	movq	%rax, -88(%rbp)
	movl	%edx, %r12d
	movq	%rbx, %r14
	testq	%rbx, %rbx
	jg	.L96
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L109:
	movslq	%r12d, %rbx
.L96:
	movq	64(%r15), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rsi
	cmpq	%rbx, %rdx
	movq	%rdx, -72(%rbp)
	movq	%rax, %rdi
	cmovle	%rdx, %rbx
	movq	%rax, -80(%rbp)
	movq	%rbx, %rdx
	subl	%ebx, %r12d
	addq	%rbx, %r13
	call	memcpy@PLT
	movq	64(%r15), %rdi
	addq	%r14, 72(%r15)
	movq	%r14, %rsi
	movq	-88(%rbp), %rdx
	movq	(%rdi), %rax
	call	*24(%rax)
	testl	%r12d, %r12d
	jne	.L109
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movslq	%r12d, %rbx
.L98:
	movq	64(%r15), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rsi
	cmpq	%rbx, %rdx
	movq	%rdx, -72(%rbp)
	movq	%rax, %rdi
	cmovle	%rdx, %rbx
	movq	%rax, -80(%rbp)
	movq	%rbx, %rdx
	subl	%ebx, %r12d
	addq	%rbx, %r13
	call	memcpy@PLT
	movq	64(%r15), %rdi
	movq	-88(%rbp), %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testl	%r12d, %r12d
	jne	.L111
	jmp	.L99
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7831:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_116FileOutputStream15WriteAsciiChunkEPci, @function
_ZN4node4heap12_GLOBAL__N_116FileOutputStream15WriteAsciiChunkEPci:
.LFB7814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movslq	%edx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%r13, %r13
	je	.L115
	movq	%rdi, %r12
	movq	%rsi, %r14
	xorl	%ebx, %ebx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L118:
	movq	8(%r12), %rdi
	call	ferror@PLT
	testl	%eax, %eax
	jne	.L113
	movq	8(%r12), %rcx
	movq	%r13, %rdx
	leaq	(%r14,%rbx), %rdi
	movl	$1, %esi
	subq	%rbx, %rdx
	call	fwrite@PLT
	addq	%rax, %rbx
	cmpq	%rbx, %r13
	jbe	.L113
.L114:
	movq	8(%r12), %rdi
	call	feof@PLT
	testl	%eax, %eax
	je	.L118
.L113:
	xorl	%eax, %eax
	cmpq	%rbx, %r13
	popq	%rbx
	popq	%r12
	setne	%al
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L113
	.cfi_endproc
.LFE7814:
	.size	_ZN4node4heap12_GLOBAL__N_116FileOutputStream15WriteAsciiChunkEPci, .-_ZN4node4heap12_GLOBAL__N_116FileOutputStream15WriteAsciiChunkEPci
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11249:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11249:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB11250:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE11250:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11251:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11251:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB11252:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE11252:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.text
	.p2align 4
	.type	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetChunkSizeEv, @function
_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetChunkSizeEv:
.LFB11253:
	.cfi_startproc
	endbr64
	movl	$65536, %eax
	ret
	.cfi_endproc
.LFE11253:
	.size	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetChunkSizeEv, .-_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetChunkSizeEv
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv:
.LFB11254:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11254:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv:
.LFB11255:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE11255:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv:
.LFB11256:
	.cfi_startproc
	endbr64
	cmpq	$0, 72(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE11256:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv:
.LFB11257:
	.cfi_startproc
	endbr64
	cmpq	$0, 72(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE11257:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE:
.LFB11258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11258:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB11259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11259:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev:
.LFB11260:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE11260:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream14MemoryInfoNameEv, @function
_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream14MemoryInfoNameEv:
.LFB7840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28001, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L136
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L136:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7840:
	.size	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream14MemoryInfoNameEv, .-_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream14MemoryInfoNameEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB11264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11264:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.text
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv:
.LFB11268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	72(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%r8, %r8
	je	.L142
	leaq	64(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
	call	_ZNK2v812HeapSnapshot9SerializeEPNS_12OutputStreamENS0_19SerializationFormatE@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	leaq	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11268:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"heapsnapshot"
.LC5:
	.string	"Heap"
.LC6:
	.string	"w"
	.text
	.p2align 4
	.globl	_ZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$1072, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L161
	cmpw	$1040, %cx
	jne	.L144
.L161:
	movq	23(%rdx), %rax
.L146:
	movq	8(%rdi), %r14
	movl	16(%rbx), %edx
	leaq	88(%r14), %r13
	testl	%edx, %edx
	jle	.L148
	movq	8(%rbx), %r13
.L148:
	movq	0(%r13), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L149
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L178
.L149:
	leaq	-1088(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	leaq	.LC6(%rip), %rsi
	call	fopen64@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L159
	movq	%r14, %rdi
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_116FileOutputStreamE(%rip), %rax
	movq	%r12, -1096(%rbp)
	movq	%rax, -1104(%rbp)
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler16TakeHeapSnapshotEPNS_15ActivityControlEPNS0_18ObjectNameResolverE@PLT
	xorl	%edx, %edx
	leaq	-1104(%rbp), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZNK2v812HeapSnapshot9SerializeEPNS_12OutputStreamENS0_19SerializationFormatE@PLT
	testq	%r14, %r14
	je	.L158
	movq	%r14, %rdi
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L158:
	movq	%r12, %rdi
	call	fclose@PLT
	movq	0(%r13), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L159:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L143
	testq	%rdi, %rdi
	je	.L143
	call	free@PLT
.L143:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$1072, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	cmpl	$5, 43(%rdx)
	jne	.L149
	movq	1936(%rax), %rsi
	leaq	-1088(%rbp), %rdi
	leaq	.LC4(%rip), %rcx
	leaq	.LC5(%rip), %rdx
	call	_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_@PLT
	movq	-1088(%rbp), %rdi
	leaq	.LC6(%rip), %rsi
	call	fopen64@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L151
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_116FileOutputStreamE(%rip), %rax
	movq	%r14, %rdi
	movq	%r12, -1096(%rbp)
	movq	%rax, -1104(%rbp)
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler16TakeHeapSnapshotEPNS_15ActivityControlEPNS0_18ObjectNameResolverE@PLT
	xorl	%edx, %edx
	leaq	-1104(%rbp), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZNK2v812HeapSnapshot9SerializeEPNS_12OutputStreamENS0_19SerializationFormatE@PLT
	testq	%r13, %r13
	je	.L152
	movq	%r13, %rdi
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L152:
	movq	%r12, %rdi
	call	fclose@PLT
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	movq	-1088(%rbp), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L151
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L151:
	movq	-1088(%rbp), %rdi
	leaq	-1072(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L143
	call	_ZdlPv@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L144:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	_ZZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7847:
	.size	_ZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB11261:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE11261:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB11262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC7(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L185:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11262:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB10999:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE10999:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB11003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC7(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11003:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD2Ev, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD2Ev:
.LFB7826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	addq	$176, %rax
	movq	%rax, 56(%rdi)
	addq	$160, %rax
	movq	%rax, 120(%rdi)
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L192
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L192:
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	104(%r12), %rdx
	movq	%rax, 56(%r12)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 96(%r12)
	testq	%rdx, %rdx
	jne	.L217
.L196:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	64(%r12), %rbx
	movq	%rax, 56(%r12)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L219:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	64(%r12), %rax
	cmpq	%rbx, %rax
	je	.L218
	movq	%rax, %rbx
.L200:
	testq	%rbx, %rbx
	jne	.L219
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L197
	leaq	96(%r12), %rcx
	cmpq	%rcx, %rax
	jne	.L194
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L214:
	cmpq	%rcx, %rax
	je	.L221
.L194:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L214
.L197:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L221:
	movq	112(%r12), %rax
	movq	%rax, 16(%rdx)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L220:
	movq	112(%r12), %rax
	movq	%rax, 8(%rdx)
	jmp	.L196
	.cfi_endproc
.LFE7826:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD2Ev, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD2Ev
	.set	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev,_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD2Ev
	.p2align 4
	.type	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv, @function
_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv:
.LFB11269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_buf_init@PLT
	movq	-56(%rbx), %rdi
	movq	$-4095, %rsi
	movq	%rax, -48(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	call	*24(%rax)
	movq	8(%rbx), %rdi
	movq	$0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L222
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L222:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L229:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11269:
	.size	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv, .-_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev:
.LFB11263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r13
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L230
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L234
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11263:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB11267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-40(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r14
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L236
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L242
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L236:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$96, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11267:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev:
.LFB10948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rbx), %r12
	movq	32(%rbx), %r13
	movq	%rax, (%rbx)
	call	uv_buf_init@PLT
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%r12, %r12
	je	.L243
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L247
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10948:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$72, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L253
	cmpw	$1040, %cx
	jne	.L249
.L253:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L255
.L252:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	32(%rbx), %rsi
	leaq	16(%r12), %rdi
	movsd	.LC2(%rip), %xmm0
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movq	%r13, %rdx
	movl	$26, %ecx
	movq	%rax, (%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L252
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7656:
	.size	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.text
	.p2align 4
	.type	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci, @function
_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci:
.LFB11270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L261
	movslq	%edx, %rbx
	leaq	-80(%rbp), %rax
	movq	%rdi, %r15
	movq	%rsi, %r13
	movq	%rax, -88(%rbp)
	movl	%edx, %r12d
	movq	%rbx, %r14
	testq	%rbx, %rbx
	jg	.L258
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L271:
	movslq	%r12d, %rbx
.L258:
	movq	-56(%r15), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rsi
	cmpq	%rbx, %rdx
	movq	%rdx, -72(%rbp)
	movq	%rax, %rdi
	cmovle	%rdx, %rbx
	movq	%rax, -80(%rbp)
	movq	%rbx, %rdx
	subl	%ebx, %r12d
	addq	%rbx, %r13
	call	memcpy@PLT
	movq	-56(%r15), %rdi
	addq	%r14, -48(%r15)
	movq	%r14, %rsi
	movq	-88(%rbp), %rdx
	movq	(%rdi), %rax
	call	*24(%rax)
	testl	%r12d, %r12d
	jne	.L271
.L261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movslq	%r12d, %rbx
.L260:
	movq	-56(%r15), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rsi
	cmpq	%rbx, %rdx
	movq	%rdx, -72(%rbp)
	movq	%rax, %rdi
	cmovle	%rdx, %rbx
	movq	%rax, -80(%rbp)
	movq	%rbx, %rdx
	subl	%ebx, %r12d
	addq	%rbx, %r13
	call	memcpy@PLT
	movq	-56(%r15), %rdi
	movq	-88(%rbp), %rdx
	movq	%r14, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testl	%r12d, %r12d
	jne	.L273
	jmp	.L261
.L272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11270:
	.size	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci, .-_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev:
.LFB11265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, -56(%rdi)
	addq	$176, %rax
	movq	%rax, (%rdi)
	addq	$160, %rax
	movq	%rax, 64(%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L275
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L275:
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	48(%r12), %rcx
	movq	%rax, (%r12)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%r12)
	testq	%rcx, %rcx
	jne	.L300
.L279:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L302:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L301
	movq	%rax, %rbx
.L283:
	testq	%rbx, %rbx
	jne	.L302
	subq	$56, %r12
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L280
	leaq	40(%r12), %rdx
	cmpq	%rdx, %rax
	jne	.L277
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L297:
	cmpq	%rdx, %rax
	je	.L304
.L277:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L297
.L280:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L304:
	movq	56(%r12), %rax
	movq	%rax, 16(%rcx)
	jmp	.L279
.L303:
	movq	56(%r12), %rax
	movq	%rax, 8(%rcx)
	jmp	.L279
	.cfi_endproc
.LFE11265:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev
	.p2align 4
	.type	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev, @function
_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev:
.LFB11266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, -120(%rdi)
	addq	$176, %rax
	movq	%rax, -64(%rdi)
	addq	$160, %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L306
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L306:
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	-16(%r12), %rcx
	movq	%rax, -64(%r12)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, -24(%r12)
	testq	%rcx, %rcx
	jne	.L331
.L310:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	-56(%r12), %rbx
	movq	%rax, -64(%r12)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L333:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	-56(%r12), %rax
	cmpq	%rbx, %rax
	je	.L332
	movq	%rax, %rbx
.L314:
	testq	%rbx, %rbx
	jne	.L333
	subq	$120, %r12
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L311
	leaq	-24(%r12), %rdx
	cmpq	%rdx, %rax
	jne	.L308
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L328:
	cmpq	%rdx, %rax
	je	.L335
.L308:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L328
.L311:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L332:
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -56(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-8(%r12), %rax
	movq	%rax, 16(%rcx)
	jmp	.L310
.L334:
	movq	-8(%r12), %rax
	movq	%rax, 8(%rcx)
	jmp	.L310
	.cfi_endproc
.LFE11266:
	.size	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev, .-_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev, @function
_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev:
.LFB7828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	addq	$176, %rax
	movq	%rax, 56(%rdi)
	addq	$160, %rax
	movq	%rax, 120(%rdi)
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L337
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L337:
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	104(%r12), %rdx
	movq	%rax, 56(%r12)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 96(%r12)
	testq	%rdx, %rdx
	jne	.L362
.L341:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	64(%r12), %rbx
	movq	%rax, 56(%r12)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	64(%r12), %rax
	cmpq	%rbx, %rax
	je	.L363
	movq	%rax, %rbx
.L345:
	testq	%rbx, %rbx
	jne	.L364
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L342
	leaq	96(%r12), %rcx
	cmpq	%rcx, %rax
	jne	.L339
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L359:
	cmpq	%rcx, %rax
	je	.L366
.L339:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L359
.L342:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L363:
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 64(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L366:
	movq	112(%r12), %rax
	movq	%rax, 16(%rdx)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L365:
	movq	112(%r12), %rax
	movq	%rax, 8(%rdx)
	jmp	.L341
	.cfi_endproc
.LFE7828:
	.size	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev, .-_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev
	.p2align 4
	.type	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev, @function
_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev:
.LFB11271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, -120(%rdi)
	addq	$176, %rax
	movq	%rax, -64(%rdi)
	addq	$160, %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L368
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L368:
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	-16(%r12), %rcx
	movq	%rax, -64(%r12)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, -24(%r12)
	testq	%rcx, %rcx
	jne	.L393
.L372:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	-56(%r12), %rbx
	movq	%rax, -64(%r12)
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L395:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	-56(%r12), %rax
	cmpq	%rbx, %rax
	je	.L394
	movq	%rax, %rbx
.L376:
	testq	%rbx, %rbx
	jne	.L395
	popq	%rbx
	leaq	-120(%r12), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L373
	leaq	-24(%r12), %rdx
	cmpq	%rdx, %rax
	jne	.L370
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L390:
	cmpq	%rdx, %rax
	je	.L397
.L370:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L390
.L373:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L394:
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -56(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L397:
	movq	-8(%r12), %rax
	movq	%rax, 16(%rcx)
	jmp	.L372
.L396:
	movq	-8(%r12), %rax
	movq	%rax, 8(%rcx)
	jmp	.L372
	.cfi_endproc
.LFE11271:
	.size	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev, .-_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev
	.p2align 4
	.type	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev, @function
_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev:
.LFB11272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, -56(%rdi)
	addq	$176, %rax
	movq	%rax, (%rdi)
	addq	$160, %rax
	movq	%rax, 64(%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L399:
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	48(%r12), %rcx
	movq	%rax, (%r12)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%r12)
	testq	%rcx, %rcx
	jne	.L424
.L403:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L426:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L425
	movq	%rax, %rbx
.L407:
	testq	%rbx, %rbx
	jne	.L426
	popq	%rbx
	leaq	-56(%r12), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L404
	leaq	40(%r12), %rdx
	cmpq	%rdx, %rax
	jne	.L401
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L421:
	cmpq	%rdx, %rax
	je	.L428
.L401:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L421
.L404:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L425:
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L428:
	movq	56(%r12), %rax
	movq	%rax, 16(%rcx)
	jmp	.L403
.L427:
	movq	56(%r12), %rax
	movq	%rax, 8(%rcx)
	jmp	.L403
	.cfi_endproc
.LFE11272:
	.size	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev, .-_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev
	.section	.text._ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE,"axG",@progbits,_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE
	.type	_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE, @function
_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE:
.LFB7755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$24, %rsp
	movq	(%rsi), %r15
	call	_Znwm@PLT
	movq	(%r14), %r12
	movq	24(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r12, 8(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	16(%rbx), %rax
	movq	$0, (%r14)
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L430
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L466:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L430
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L430
.L432:
	cmpq	%rdi, %r12
	jne	.L466
	testq	%r12, %r12
	je	.L448
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L448:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L429:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movq	40(%rbx), %rdx
	leaq	48(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L434
	movq	16(%rbx), %r9
.L435:
	addq	%r9, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L444
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r14), %rax
	movq	%r13, (%rax)
.L445:
	addq	$1, 40(%rbx)
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L434:
	cmpq	$1, %rdx
	je	.L467
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L468
	leaq	0(,%rdx,8), %r14
	movq	%rdx, -56(%rbp)
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-56(%rbp), %r8
	leaq	64(%rbx), %r11
	movq	%rax, %r9
.L437:
	movq	32(%rbx), %rsi
	movq	$0, 32(%rbx)
	testq	%rsi, %rsi
	je	.L439
	xorl	%edi, %edi
	leaq	32(%rbx), %r10
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L441:
	movq	(%r14), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L442:
	testq	%rsi, %rsi
	je	.L439
.L440:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	jne	.L441
	movq	32(%rbx), %r14
	movq	%r14, (%rcx)
	movq	%rcx, 32(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L449
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L439:
	movq	16(%rbx), %rdi
	cmpq	%r11, %rdi
	je	.L443
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
.L443:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r8, 24(%rbx)
	divq	%r8
	movq	%r9, 16(%rbx)
	leaq	0(,%rdx,8), %r14
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L444:
	movq	32(%rbx), %rax
	movq	%r13, 32(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L446
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	24(%rbx)
	movq	%r13, (%r9,%rdx,8)
.L446:
	leaq	32(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%rdx, %rdi
	jmp	.L442
.L467:
	leaq	64(%rbx), %r9
	movq	$0, 64(%rbx)
	movq	%r9, %r11
	jmp	.L437
.L468:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7755:
	.size	_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE, .-_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE
	.section	.rodata.str1.1
.LC8:
	.string	"buildEmbedderGraph"
.LC9:
	.string	"triggerHeapSnapshot"
.LC10:
	.string	"createHeapSnapshotStream"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB11:
	.text
.LHOTB11:
	.p2align 4
	.globl	_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L470
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L470
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L470
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4heap18BuildEmbedderGraphERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L484
.L471:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L485
.L472:
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L486
.L473:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L487
.L474:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L488
.L475:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L489
.L476:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L490
.L477:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L491
.L478:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L492
.L479:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L485:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L486:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L487:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L488:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L489:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L490:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L491:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L492:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L479
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7854:
.L470:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7854:
	.text
	.size	_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE11:
	.text
.LHOTE11:
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L494
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L494:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L504
.L495:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L496
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L495
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7536:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.globl	_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE
	.type	_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE, @function
_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE:
.LFB7844:
	.cfi_startproc
	endbr64
	jmp	_ZN2v812HeapSnapshot6DeleteEv@PLT
	.cfi_endproc
.LFE7844:
	.size	_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE, .-_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE
	.p2align 4
	.globl	_Z20_register_heap_utilsv
	.type	_Z20_register_heap_utilsv, @function
_Z20_register_heap_utilsv:
.LFB7855:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7855:
	.size	_Z20_register_heap_utilsv, .-_Z20_register_heap_utilsv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L524
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L525
.L510:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L514
.L507:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L515
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L511:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L510
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L516
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L512:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L514:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L507
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L516:
	xorl	%edx, %edx
	jmp	.L512
	.cfi_endproc
.LFE8576:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.rodata.str1.1
.LC12:
	.string	"HeapSnapshotStream"
	.text
	.p2align 4
	.globl	_ZN4node4heap24CreateHeapSnapshotStreamEPNS_11EnvironmentEOSt10unique_ptrIKN2v812HeapSnapshotENS_15FunctionDeleterIS6_XadL_ZNS0_18DeleteHeapSnapshotEPS6_EEEEE
	.type	_ZN4node4heap24CreateHeapSnapshotStreamEPNS_11EnvironmentEOSt10unique_ptrIKN2v812HeapSnapshotENS_15FunctionDeleterIS6_XadL_ZNS0_18DeleteHeapSnapshotEPS6_EEEEE, @function
_ZN4node4heap24CreateHeapSnapshotStreamEPNS_11EnvironmentEOSt10unique_ptrIKN2v812HeapSnapshotENS_15FunctionDeleterIS6_XadL_ZNS0_18DeleteHeapSnapshotEPS6_EEEEE:
.LFB7845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	352(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3240(%rbx), %r8
	testq	%r8, %r8
	je	.L561
.L527:
	movq	3280(%rbx), %rsi
	movq	%r8, %rdi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L562
	movl	$136, %edi
	movq	%rax, -88(%rbp)
	call	_Znwm@PLT
	movsd	.LC2(%rip), %xmm0
	movq	-88(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movl	$11, %ecx
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node22EmitToJSStreamListenerE(%rip), %rax
	leaq	56(%r12), %r8
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	leaq	96(%r12), %rax
	movq	%rax, 64(%r12)
	leaq	16+_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE(%rip), %rax
	movq	%rax, (%r12)
	addq	$176, %rax
	movq	%rax, 56(%r12)
	addq	$160, %rax
	movq	%rax, 120(%r12)
	movq	(%r14), %rax
	movq	%rbx, 88(%r12)
	movq	%rax, 128(%r12)
	movq	24(%r12), %rax
	movq	$0, 112(%r12)
	movq	%r8, 104(%r12)
	movq	$0, (%r14)
	movups	%xmm0, 72(%r12)
	testq	%rax, %rax
	je	.L544
	movl	(%rax), %ecx
	movb	$1, 8(%rax)
	testl	%ecx, %ecx
	jne	.L533
.L544:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r8, -88(%rbp)
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-88(%rbp), %r8
.L533:
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN4node10StreamBase9GetObjectEv@PLT
	movq	-88(%rbp), %r8
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%r8, %rdx
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L563
.L534:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L564
.L536:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L565
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L566
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L545
	cmpb	$0, 9(%rdx)
	jne	.L567
	cmpb	$0, 8(%rdx)
	je	.L545
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L545
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L562:
	movq	$0, 0(%r13)
.L545:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L568
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L536
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L561:
	subq	$8, %rsp
	movq	352(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	movl	$1, %r9d
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	movl	$18, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	testq	%rax, %rax
	movq	-88(%rbp), %r8
	movq	%rax, %rsi
	je	.L569
.L528:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE@PLT
	movq	3240(%rbx), %rdi
	movq	352(%rbx), %r12
	movq	-88(%rbp), %r8
	testq	%rdi, %rdi
	je	.L529
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-88(%rbp), %r8
	movq	$0, 3240(%rbx)
.L529:
	testq	%r8, %r8
	je	.L527
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3240(%rbx)
	movq	%rax, %r8
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L563:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L547
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L535:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L567:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L569:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %r8
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L565:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L547:
	xorl	%edx, %edx
	jmp	.L535
.L568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7845:
	.size	_ZN4node4heap24CreateHeapSnapshotStreamEPNS_11EnvironmentEOSt10unique_ptrIKN2v812HeapSnapshotENS_15FunctionDeleterIS6_XadL_ZNS0_18DeleteHeapSnapshotEPS6_EEEEE, .-_ZN4node4heap24CreateHeapSnapshotStreamEPNS_11EnvironmentEOSt10unique_ptrIKN2v812HeapSnapshotENS_15FunctionDeleterIS6_XadL_ZNS0_18DeleteHeapSnapshotEPS6_EEEEE
	.p2align 4
	.globl	_ZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L587
	cmpw	$1040, %cx
	jne	.L571
.L587:
	movq	23(%rdx), %r12
.L573:
	movq	352(%r12), %rdi
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler16TakeHeapSnapshotEPNS_15ActivityControlEPNS0_18ObjectNameResolverE@PLT
	movq	%rax, -40(%rbp)
	testq	%rax, %rax
	je	.L598
	leaq	-32(%rbp), %rdi
	leaq	-40(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN4node4heap24CreateHeapSnapshotStreamEPNS_11EnvironmentEOSt10unique_ptrIKN2v812HeapSnapshotENS_15FunctionDeleterIS6_XadL_ZNS0_18DeleteHeapSnapshotEPS6_EEEEE
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L576
	movq	8(%rdi), %rax
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L577
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L599
.L578:
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L579:
	testq	%rdi, %rdi
	je	.L576
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L600
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L601
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L576
	cmpb	$0, 9(%rdx)
	je	.L583
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L576:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L570
	call	_ZN2v812HeapSnapshot6DeleteEv@PLT
.L570:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L602
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L599:
	movq	16(%rdi), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-32(%rbp), %rdi
	testq	%rax, %rax
	jne	.L578
	.p2align 4,,10
	.p2align 3
.L577:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L598:
	leaq	_ZZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L583:
	cmpb	$0, 8(%rdx)
	je	.L576
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L576
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L601:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L602:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7846:
	.size	_ZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L604
	leaq	16(%r12), %rsi
.L605:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L606
	addq	$16, %r12
.L607:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L608
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L624
.L608:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L625
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L612
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L626
.L603:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L627
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L606:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L626:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L615
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L610:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L612:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L615:
	xorl	%edx, %edx
	jmp	.L610
.L627:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7661:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L629
	leaq	40(%r12), %rsi
.L630:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L631
	addq	$40, %r12
.L632:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L633
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L649
.L633:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L650
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L637
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L651
.L628:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L652
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L649:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L651:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L650:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L640
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L635:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L637:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L640:
	xorl	%edx, %edx
	jmp	.L635
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7663:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.section	.text._ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB9689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movq	8(%rax), %r14
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	%r14, %rdi
	testb	%al, %al
	je	.L654
	call	_ZN2v86Object15GetIdentityHashEv@PLT
	cltq
	movq	%rax, -56(%rbp)
.L655:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -80(%rbp)
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %r14
	testq	%r14, %r14
	je	.L657
	movq	(%r14), %rdx
	movq	16(%rdx), %rcx
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L658:
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L657
	movq	16(%rsi), %rcx
	movq	%rdx, %r14
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	8(%rbx)
	cmpq	%rdx, %r10
	jne	.L657
	movq	%rsi, %rdx
.L660:
	cmpq	%rcx, -56(%rbp)
	jne	.L658
	movq	(%r12), %rax
	movq	%r10, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	8(%rax), %rdi
	movq	8(%rdx), %rax
	movq	8(%rax), %rsi
	call	_ZNK2v85Value9SameValueENS_5LocalIS0_EE@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r10
	testb	%al, %al
	je	.L658
	movq	(%r14), %rax
	xorl	%r15d, %r15d
	testq	%rax, %rax
	je	.L657
.L661:
	addq	$40, %rsp
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	movq	%r13, %rcx
	movq	%rax, %r9
	movq	$0, (%rax)
	movq	(%r12), %rax
	leaq	32(%rbx), %rdi
	movq	%r9, -64(%rbp)
	movq	%rax, 8(%r9)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	movq	%rdx, %r12
	jne	.L662
	movq	(%rbx), %r13
.L663:
	movq	-80(%rbp), %r14
	movq	-56(%rbp), %rax
	addq	%r13, %r14
	movq	%rax, 16(%r9)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L672
	movq	(%rax), %rax
	movq	%rax, (%r9)
	movq	(%r14), %rax
	movq	%r9, (%rax)
.L673:
	addq	$1, 24(%rbx)
	movq	%r9, %rax
	movb	$1, %r15b
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L654:
	call	_ZNK2v85Value6IsNameEv@PLT
	movq	%r14, %rdi
	testb	%al, %al
	je	.L656
	call	_ZN2v84Name15GetIdentityHashEv@PLT
	cltq
	movq	%rax, -56(%rbp)
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L656:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movq	$0, -56(%rbp)
	testb	%al, %al
	je	.L655
	movq	%r14, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	cltq
	movq	%rax, -56(%rbp)
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L662:
	cmpq	$1, %rdx
	je	.L697
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L698
	leaq	0(,%rdx,8), %r14
	movq	%r9, -64(%rbp)
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	movq	-64(%rbp), %r9
	leaq	48(%rbx), %r11
.L665:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L667
	xorl	%edi, %edi
	leaq	16(%rbx), %r10
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%r14), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L670:
	testq	%rsi, %rsi
	je	.L667
.L668:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	jne	.L669
	movq	16(%rbx), %r14
	movq	%r14, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L676
	movq	%rcx, 0(%r13,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L668
	.p2align 4,,10
	.p2align 3
.L667:
	movq	(%rbx), %rdi
	cmpq	%r11, %rdi
	je	.L671
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r9
.L671:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	divq	%r12
	leaq	0(,%rdx,8), %rax
	movq	%rax, -80(%rbp)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L672:
	movq	16(%rbx), %rax
	movq	%r9, 16(%rbx)
	movq	%rax, (%r9)
	testq	%rax, %rax
	je	.L674
	movq	16(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r9, 0(%r13,%rdx,8)
.L674:
	leaq	16(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L676:
	movq	%rdx, %rdi
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L697:
	leaq	48(%rbx), %r13
	movq	$0, 48(%rbx)
	movq	%r13, %r11
	jmp	.L665
.L698:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9689:
	.size	_ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.text._ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE,"axG",@progbits,_ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE
	.type	_ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE, @function
_ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE:
.LFB7752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	8(%r12), %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN4node4heap13JSGraphJSNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L700
	movq	%r14, %rsi
	leaq	72(%r12), %r15
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	%rax, 8(%r13)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	%r14, %rdi
	testb	%al, %al
	jne	.L763
	call	_ZNK2v85Value6IsNameEv@PLT
	movq	%r14, %rdi
	testb	%al, %al
	je	.L703
	call	_ZN2v84Name15GetIdentityHashEv@PLT
	movslq	%eax, %rbx
.L702:
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	80(%r12)
	movq	72(%r12), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L704
	movq	(%r8), %r14
	movq	16(%r14), %rcx
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L705:
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L704
	movq	16(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%rcx, %rax
	divq	80(%r12)
	cmpq	%rdx, %r9
	jne	.L704
	movq	%rsi, %r14
.L707:
	cmpq	%rbx, %rcx
	jne	.L705
	movq	8(%r14), %rax
	movq	8(%r13), %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -96(%rbp)
	movq	8(%rax), %rsi
	call	_ZNK2v85Value9SameValueENS_5LocalIS0_EE@PLT
	movq	-88(%rbp), %r9
	testb	%al, %al
	je	.L705
	movq	-96(%rbp), %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L704
	movq	8(%rax), %r12
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L699:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L764
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_restore_state
	call	_ZN2v86Object15GetIdentityHashEv@PLT
	movslq	%eax, %rbx
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	-64(%rbp), %r14
	leaq	-72(%rbp), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, -72(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZNSt10_HashtableIPN4node4heap13JSGraphJSNodeES3_SaIS3_ENSt8__detail9_IdentityENS2_5EqualENS2_4HashENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS3_NS5_10_AllocNodeISaINS5_10_Hash_nodeIS3_Lb1EEEEEEEESt4pairINS5_14_Node_iteratorIS3_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	(%r12), %rax
	leaq	_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE(%rip), %rdx
	movq	8(%rax), %rax
	movq	%r13, -64(%rbp)
	cmpq	%rdx, %rax
	jne	.L709
	movl	$16, %edi
	call	_Znwm@PLT
	movq	-64(%rbp), %r14
	movq	24(%r12), %r8
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	%r14, 8(%rax)
	movq	%r14, %rax
	divq	%r8
	movq	16(%r12), %rax
	movq	$0, -64(%rbp)
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %rbx
	testq	%rax, %rax
	je	.L710
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L765:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L710
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %rdi
	jne	.L710
.L712:
	cmpq	%rsi, %r14
	jne	.L765
	testq	%r14, %r14
	je	.L730
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L730:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L713:
	movq	%r13, %r12
.L727:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L699
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L703:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	xorl	%ebx, %ebx
	testb	%al, %al
	je	.L702
	movq	%r14, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movslq	%eax, %rbx
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L700:
	leaq	_ZZN4node4heap13JSGraphJSNodeC4EPN2v87IsolateENS2_5LocalINS2_5ValueEEEE4args(%rip), %rdi
	movq	$0, 8(%r13)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L710:
	movq	40(%r12), %rdx
	movq	%r8, %rsi
	leaq	48(%r12), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L714
	movq	16(%r12), %r9
.L715:
	addq	%r9, %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L724
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%rbx), %rax
	movq	%r15, (%rax)
.L725:
	addq	$1, 40(%r12)
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L709:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L714:
	cmpq	$1, %rdx
	je	.L766
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L767
	leaq	0(,%rdx,8), %rbx
	movq	%rdx, -88(%rbp)
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-88(%rbp), %r8
	leaq	64(%r12), %r11
	movq	%rax, %r9
.L717:
	movq	32(%r12), %rsi
	movq	$0, 32(%r12)
	testq	%rsi, %rsi
	je	.L719
	xorl	%edi, %edi
	leaq	32(%r12), %r10
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L721:
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L722:
	testq	%rsi, %rsi
	je	.L719
.L720:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L721
	movq	32(%r12), %rbx
	movq	%rbx, (%rcx)
	movq	%rcx, 32(%r12)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L733
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L719:
	movq	16(%r12), %rdi
	cmpq	%r11, %rdi
	je	.L723
	movq	%r8, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %r9
.L723:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r8, 24(%r12)
	divq	%r8
	movq	%r9, 16(%r12)
	leaq	0(,%rdx,8), %rbx
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L724:
	movq	32(%r12), %rax
	movq	%r15, 32(%r12)
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L726
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	24(%r12)
	movq	%r15, (%r9,%rdx,8)
.L726:
	leaq	32(%r12), %rax
	movq	%rax, (%rbx)
	jmp	.L725
.L733:
	movq	%rdx, %rdi
	jmp	.L722
.L766:
	movq	$0, 64(%r12)
	leaq	64(%r12), %r9
	movq	%r9, %r11
	jmp	.L717
.L764:
	call	__stack_chk_fail@PLT
.L767:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7752:
	.size	_ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE, .-_ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE
	.section	.text._ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm:
.LFB9733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L769
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L779
.L795:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L780:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L793
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L794
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L772:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L774
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L776:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L777:
	testq	%rsi, %rsi
	je	.L774
.L775:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L776
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L782
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L775
	.p2align 4,,10
	.p2align 3
.L774:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L778
	call	_ZdlPv@PLT
.L778:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L795
.L779:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L781
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L781:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L782:
	movq	%rdx, %rdi
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L793:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L772
.L794:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9733:
	.size	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm, .-_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm
	.section	.text._ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc,"axG",@progbits,_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc
	.type	_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc, @function
_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc:
.LFB7757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r12, %rax
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	136(%rdi), %rsi
	divq	%rsi
	movq	128(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L797
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L831:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L797
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r15
	jne	.L797
.L799:
	cmpq	%rdi, %r12
	jne	.L831
	movq	32(%rcx), %r12
	leaq	16(%rcx), %rbx
	leaq	8(%rbx), %r15
	testq	%r12, %r12
	jne	.L801
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L833:
	jne	.L804
	cmpq	40(%r12), %r14
	jb	.L803
.L804:
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L802
.L834:
	movq	%rax, %r12
.L801:
	movq	32(%r12), %rdx
	cmpq	%rdx, %r13
	jnb	.L833
.L803:
	movq	16(%r12), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	jne	.L834
.L802:
	testb	%cl, %cl
	jne	.L835
	movq	%r12, %rcx
	cmpq	%rdx, %r13
	ja	.L812
.L813:
	cmpq	%rdx, %r13
	jne	.L796
	cmpq	40(%r12), %r14
	jbe	.L796
	movq	%rcx, %r12
.L808:
	testq	%r12, %r12
	je	.L796
.L812:
	movl	$1, %r8d
	cmpq	%r12, %r15
	jne	.L836
.L810:
	movq	%r14, %xmm1
	movq	%r13, %xmm0
	movl	$48, %edi
	movl	%r8d, -68(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	-64(%rbp), %xmm0
	movq	%r15, %rcx
	movq	%r12, %rdx
	movl	-68(%rbp), %r8d
	movq	%rax, %rsi
	movups	%xmm0, 32(%rax)
	movl	%r8d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
.L796:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	cmpq	%r12, 24(%rbx)
	je	.L812
.L814:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	32(%rax), %rdx
	cmpq	%r13, %rdx
	jb	.L808
	movq	%r12, %rcx
	movq	%rax, %r12
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L836:
	cmpq	32(%r12), %r13
	jb	.L810
	movl	$0, %r8d
	jne	.L810
	xorl	%r8d, %r8d
	cmpq	40(%r12), %r14
	setb	%r8b
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L797:
	movl	$64, %edi
	call	_Znwm@PLT
	leaq	128(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	leaq	24(%rax), %rax
	movl	$1, %r8d
	movq	$0, -24(%rax)
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 40(%rcx)
	movq	%rax, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm
	movq	32(%rax), %r12
	leaq	16(%rax), %rbx
	leaq	8(%rbx), %r15
	testq	%r12, %r12
	jne	.L801
	.p2align 4,,10
	.p2align 3
.L832:
	movq	%r15, %r12
	cmpq	%r15, 24(%rbx)
	jne	.L814
	movl	$1, %r8d
	jmp	.L810
	.cfi_endproc
.LFE7757:
	.size	_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc, .-_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc
	.section	.rodata.str1.1
.LC13:
	.string	"HeapSnapshot"
.LC14:
	.string	"snapshot"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 128(%rdi)
	je	.L837
	movl	$72, %edi
	movq	%rsi, %rbx
	call	_Znwm@PLT
	xorl	%edx, %edx
	movl	$12, %r8d
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC13(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %r14
	leaq	_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE(%rip), %rdx
	movq	$1, 64(%r12)
	movb	$0, 24(%r12)
	movq	(%r14), %rax
	movq	%r12, -64(%rbp)
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L840
	movl	$16, %edi
	call	_Znwm@PLT
	movq	-64(%rbp), %r13
	movq	24(%r14), %r8
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	%r13, 8(%rax)
	movq	%r13, %rax
	divq	%r8
	movq	16(%r14), %rax
	movq	$0, -64(%rbp)
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rdi
	leaq	0(,%rdx,8), %r10
	testq	%rax, %rax
	je	.L841
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L925:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L841
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	cmpq	%rdx, %rdi
	jne	.L841
.L843:
	cmpq	%rsi, %r13
	jne	.L925
	testq	%r13, %r13
	je	.L878
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L878:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L858:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L859
	movq	(%rdi), %rax
	call	*8(%rax)
.L859:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L837
	cmpq	72(%rbx), %rax
	je	.L926
.L861:
	movq	-8(%rax), %r13
	testq	%r13, %r13
	je	.L837
	movq	8(%rbx), %r15
	leaq	_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc(%rip), %rdx
	movq	(%r15), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L862
	movq	136(%r15), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	128(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L863
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L927:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L863
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L863
.L865:
	cmpq	%rsi, %r13
	jne	.L927
	leaq	16(%rcx), %rbx
.L876:
	movq	16(%rbx), %r13
	leaq	8(%rbx), %r15
	leaq	.LC14(%rip), %r14
	testq	%r13, %r13
	jne	.L867
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L929:
	jne	.L870
	cmpq	40(%r13), %r12
	jb	.L869
.L870:
	movq	24(%r13), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L868
.L930:
	movq	%rax, %r13
.L867:
	movq	32(%r13), %rdx
	cmpq	%r14, %rdx
	jbe	.L929
.L869:
	movq	16(%r13), %rax
	movl	$1, %ecx
	testq	%rax, %rax
	jne	.L930
.L868:
	testb	%cl, %cl
	jne	.L931
	movq	%r13, %rcx
	cmpq	%r14, %rdx
	jb	.L879
.L880:
	cmpq	%r14, %rdx
	jne	.L837
	cmpq	40(%r13), %r12
	jbe	.L837
	testq	%rcx, %rcx
	je	.L837
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L879:
	movl	$1, %r8d
	cmpq	%r13, %r15
	jne	.L932
.L875:
	movl	$48, %edi
	movl	%r8d, -72(%rbp)
	call	_Znwm@PLT
	movl	-72(%rbp), %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, 32(%rax)
	movq	%rax, %rsi
	movq	%r12, 40(%rax)
	movl	%r8d, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	.p2align 4,,10
	.p2align 3
.L837:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L933
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore_state
	leaq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L931:
	cmpq	24(%rbx), %r13
	je	.L879
.L881:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	32(%rax), %rdx
	cmpq	%r14, %rdx
	jb	.L879
	movq	%r13, %rcx
	movq	%rax, %r13
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L926:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L841:
	movq	40(%r14), %rdx
	movq	%r8, %rsi
	leaq	48(%r14), %rdi
	movl	$1, %ecx
	movq	%r10, -72(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L845
	movq	16(%r14), %r9
	movq	-72(%rbp), %r10
.L846:
	addq	%r9, %r10
	movq	(%r10), %rax
	testq	%rax, %rax
	je	.L855
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%r10), %rax
	movq	%r15, (%rax)
.L856:
	addq	$1, 40(%r14)
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L862:
	leaq	.LC14(%rip), %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L863:
	movl	$64, %edi
	call	_Znwm@PLT
	leaq	128(%r15), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	leaq	24(%rax), %rax
	movl	$1, %r8d
	movq	%r13, -16(%rax)
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 40(%rcx)
	movq	%rax, 48(%rcx)
	movq	$0, 56(%rcx)
	call	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_St3setIS4_IPKcS3_ESt4lessIS9_ESaIS9_EEESaISE_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSG_18_Mod_range_hashingENSG_20_Default_ranged_hashENSG_20_Prime_rehash_policyENSG_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSG_10_Hash_nodeISE_Lb0EEEm
	leaq	16(%rax), %rbx
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L845:
	cmpq	$1, %rdx
	je	.L934
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L935
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -80(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-80(%rbp), %r8
	movq	%rax, %r9
	leaq	64(%r14), %rax
	movq	%rax, -72(%rbp)
.L848:
	movq	32(%r14), %rsi
	movq	$0, 32(%r14)
	testq	%rsi, %rsi
	je	.L850
	xorl	%r10d, %r10d
	leaq	32(%r14), %r11
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L852:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L853:
	testq	%rsi, %rsi
	je	.L850
.L851:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L852
	movq	32(%r14), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 32(%r14)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L884
	movq	%rcx, (%r9,%r10,8)
	movq	%rdx, %r10
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L850:
	movq	16(%r14), %rdi
	cmpq	-72(%rbp), %rdi
	je	.L854
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r9
.L854:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r8, 24(%r14)
	divq	%r8
	movq	%r9, 16(%r14)
	leaq	0(,%rdx,8), %r10
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L855:
	movq	32(%r14), %rax
	movq	%rax, (%r15)
	movq	%r15, 32(%r14)
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L857
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	24(%r14)
	movq	%r15, (%r9,%rdx,8)
.L857:
	leaq	32(%r14), %rax
	movq	%rax, (%r10)
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L884:
	movq	%rdx, %r10
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L932:
	cmpq	%r14, 32(%r13)
	ja	.L875
	movl	$0, %r8d
	jne	.L875
	xorl	%r8d, %r8d
	cmpq	40(%r13), %r12
	setb	%r8b
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L928:
	movq	%r15, %r13
	cmpq	24(%rbx), %r15
	je	.L888
	leaq	.LC14(%rip), %r14
	jmp	.L881
.L888:
	movl	$1, %r8d
	leaq	.LC14(%rip), %r14
	jmp	.L875
.L934:
	leaq	64(%r14), %r9
	movq	$0, 64(%r14)
	movq	%r9, -72(%rbp)
	jmp	.L848
.L933:
	call	__stack_chk_fail@PLT
.L935:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7839:
	.size	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB9813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L937
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L947
.L963:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L948:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L961
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L962
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L940:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L942
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L944:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L945:
	testq	%rsi, %rsi
	je	.L942
.L943:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L944
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L950
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L943
	.p2align 4,,10
	.p2align 3
.L942:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L946
	call	_ZdlPv@PLT
.L946:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L963
.L947:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L949
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L949:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L950:
	movq	%rdx, %rdi
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L961:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L940
.L962:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9813:
	.size	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_
	.type	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_, @function
_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_:
.LFB9264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L965
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L977:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L965
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L965
.L967:
	cmpq	%r8, %r13
	jne	.L977
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L965:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9264:
	.size	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_, .-_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_
	.section	.text._ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	.type	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, @function
_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_:
.LFB9270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L979
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L991:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L979
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L979
.L981:
	cmpq	%r8, %r13
	jne	.L991
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L979:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIPN2v813EmbedderGraph4NodeESt4pairIKS3_NS0_5LocalINS0_6ObjectEEEESaIS9_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9270:
	.size	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_, .-_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	.section	.rodata._ZNK4node4heap7JSGraph12CreateObjectEv.str1.1,"aMS",@progbits,1
.LC16:
	.string	"edges"
.LC17:
	.string	"isRoot"
.LC18:
	.string	"wraps"
.LC19:
	.string	"to"
.LC20:
	.string	"basic_string::append"
.LC21:
	.string	" "
	.section	.text._ZNK4node4heap7JSGraph12CreateObjectEv,"axG",@progbits,_ZNK4node4heap7JSGraph12CreateObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4heap7JSGraph12CreateObjectEv
	.type	_ZNK4node4heap7JSGraph12CreateObjectEv, @function
_ZNK4node4heap7JSGraph12CreateObjectEv:
.LFB7776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L995
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L995
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L995
	movq	271(%rax), %rbx
.L994:
	movq	8(%r15), %rdi
	movl	40(%r15), %esi
	leaq	-112(%rbp), %rax
	movq	$1, -152(%rbp)
	movq	%rax, -352(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movl	$0x3f800000, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	8(%r15), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, -320(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L1107
.L996:
	movq	8(%r15), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -296(%rbp)
	testq	%rax, %rax
	je	.L1108
.L997:
	movq	360(%rbx), %rax
	movq	8(%r15), %rdi
	movl	$5, %ecx
	leaq	.LC18(%rip), %rsi
	movq	1056(%rax), %rdx
	movq	%rdx, -280(%rbp)
	movq	1616(%rax), %rdx
	movq	1880(%rax), %rax
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -336(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -368(%rbp)
	testq	%rax, %rax
	je	.L1109
.L998:
	movq	8(%r15), %rdi
	movl	$2, %ecx
	xorl	%edx, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -360(%rbp)
	testq	%rax, %rax
	je	.L1110
.L999:
	movq	32(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1111
	leaq	-224(%rbp), %r12
	movq	%r14, -256(%rbp)
	leaq	-160(%rbp), %r13
	movq	%r12, -328(%rbp)
	movq	%r15, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	-248(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %r15
	movq	8(%rbx), %rax
	movq	%rax, -224(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	(%rbx), %rbx
	movq	%rax, (%r14)
	testq	%rbx, %rbx
	jne	.L1001
	movq	-256(%rbp), %r14
	movq	-248(%rbp), %r15
.L1000:
	movq	8(%r15), %rsi
	movq	-328(%rbp), %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	32(%r15), %rbx
	testq	%rbx, %rbx
	je	.L1002
	leaq	-160(%rbp), %rax
	leaq	-96(%rbp), %r13
	movl	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, -288(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	8(%rbx), %rax
	movq	-288(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	%rax, -232(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_
	movq	(%rax), %r12
	movq	-248(%rbp), %rax
	movb	$0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	8(%rbx), %rdi
	testq	%rax, %rax
	movq	(%rdi), %rax
	je	.L1112
	call	*56(%rax)
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	strlen@PLT
	movq	-88(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	-264(%rbp), %rcx
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -88(%rbp)
	je	.L1006
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	%rax, %rdx
	ja	.L1006
	movq	-264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L1004:
	movq	8(%r15), %rdi
	movq	-96(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1016
	movq	-280(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1016
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	-296(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %r8d
	movq	8(%r15), %rax
	leaq	112(%rax), %rcx
	addq	$120, %rax
	testb	%r8b, %r8b
	cmove	%rax, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1016
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rax, %rax
	js	.L1012
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1013:
	movq	8(%r15), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-304(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1016
	movq	8(%r15), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	-312(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1016
	movl	-256(%rbp), %edx
	movq	-320(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1016
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	testb	%al, %al
	je	.L1113
.L1015:
	movq	-96(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L1017
	call	_ZdlPv@PLT
	movq	(%rbx), %rbx
	addl	$1, -256(%rbp)
	testq	%rbx, %rbx
	jne	.L1019
.L1002:
	movq	-328(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	32(%r15), %r13
	testq	%r13, %r13
	je	.L1022
	movq	%r15, -248(%rbp)
	movq	-368(%rbp), %r12
	movq	%r13, %r15
	leaq	-160(%rbp), %rbx
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L1114
.L1027:
	movq	8(%r15), %rdi
	leaq	_ZN2v813EmbedderGraph4Node11WrapperNodeEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1024
	call	*%rax
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L1024
	movq	8(%r15), %rax
	movq	-328(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixEOS4_
	leaq	-232(%rbp), %rsi
	movq	%rbx, %rdi
	movq	(%rax), %r13
	call	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	(%rax), %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1024
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	-144(%rbp), %rbx
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	jne	.L1042
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-160(%rbp), %rdi
	movq	$0, -136(%rbp)
	movq	$0, -144(%rbp)
	cmpq	-352(%rbp), %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movq	-344(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1115
	addq	$328, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1017:
	.cfi_restore_state
	movq	(%rbx), %rbx
	addl	$1, -256(%rbp)
	testq	%rbx, %rbx
	jne	.L1019
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	8(%rbx), %rax
	movq	-336(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1015
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	-96(%rbp), %rdi
	xorl	%r12d, %r12d
	cmpq	-248(%rbp), %rdi
	je	.L1020
	call	_ZdlPv@PLT
.L1020:
	movq	-328(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1021:
	movq	-144(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1041
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1042
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1112:
	call	*16(%rax)
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	strlen@PLT
	movq	-88(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	-264(%rbp), %rcx
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L995:
	xorl	%ebx, %ebx
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1111:
	leaq	-224(%rbp), %rax
	movq	%rax, -328(%rbp)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1107:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1109:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1110:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1108:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L997
.L1114:
	movq	-248(%rbp), %r15
.L1022:
	movq	144(%r15), %rax
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L1028
	leaq	-160(%rbp), %rax
	movq	%r15, %rbx
	movq	%rax, -272(%rbp)
.L1040:
	movq	-304(%rbp), %rax
	movq	-328(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	8(%rax), %rax
	movq	%rax, -224(%rbp)
	call	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	-312(%rbp), %rdx
	movq	%r14, %rsi
	movq	(%rax), %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -256(%rbp)
	testq	%rax, %rax
	je	.L1035
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L1035
	movq	-304(%rbp), %rax
	xorl	%r15d, %r15d
	movq	$0, -288(%rbp)
	movq	40(%rax), %r12
	addq	$24, %rax
	movq	%rax, -264(%rbp)
	cmpq	%r12, %rax
	jne	.L1031
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1035
	movq	%rax, %rcx
.L1046:
	movq	-280(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1035
	movq	-248(%rbp), %rcx
	movq	-360(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1035
	movq	-256(%rbp), %rdi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1035
	movq	%r12, %rdi
	addl	$1, %r15d
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -264(%rbp)
	je	.L1032
.L1031:
	movq	-272(%rbp), %rdi
	leaq	40(%r12), %rsi
	call	_ZNSt8__detail9_Map_baseIPN2v813EmbedderGraph4NodeESt4pairIKS4_NS1_5LocalINS1_6ObjectEEEESaISA_ENS_10_Select1stESt8equal_toIS4_ESt4hashIS4_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS6_
	movq	8(%rbx), %rdi
	movq	(%rax), %rax
	movq	%rax, -248(%rbp)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	32(%r12), %rsi
	movq	%rax, %r13
	testq	%rsi, %rsi
	jne	.L1116
	movq	-288(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, -296(%rbp)
	testq	%rax, %rax
	js	.L1037
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1038:
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, %rcx
	movq	-296(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	%rax, %rdx
	shrq	%rax
	pxor	%xmm0, %xmm0
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1038
.L1032:
	movq	-304(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	jne	.L1040
.L1028:
	movq	-320(%rbp), %rsi
	movq	-344(%rbp), %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L1021
.L1006:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7776:
	.size	_ZNK4node4heap7JSGraph12CreateObjectEv, .-_ZNK4node4heap7JSGraph12CreateObjectEv
	.section	.text._ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,"axG",@progbits,_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.type	_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, @function
_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E:
.LFB10868:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1125
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L1119:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1119
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE10868:
	.size	_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E, .-_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	.section	.text._ZN4node4heap7JSGraphD2Ev,"axG",@progbits,_ZN4node4heap7JSGraphD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap7JSGraphD2Ev
	.type	_ZN4node4heap7JSGraphD2Ev, @function
_ZN4node4heap7JSGraphD2Ev:
.LFB10944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap7JSGraphE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	144(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1129
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	%r13, %r14
	movq	0(%r13), %r13
	movq	32(%r14), %r12
	leaq	16(%r14), %r15
	testq	%r12, %r12
	je	.L1130
.L1131:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1131
.L1130:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1132
.L1129:
	movq	136(%rbx), %rax
	movq	128(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%rbx), %rdi
	leaq	176(%rbx), %rax
	movq	$0, 152(%rbx)
	movq	$0, 144(%rbx)
	cmpq	%rax, %rdi
	je	.L1133
	call	_ZdlPv@PLT
.L1133:
	movq	88(%rbx), %r12
	testq	%r12, %r12
	je	.L1134
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1135
.L1134:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%rbx), %rdi
	leaq	120(%rbx), %rax
	movq	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	cmpq	%rax, %rdi
	je	.L1136
	call	_ZdlPv@PLT
.L1136:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	jne	.L1140
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1137
.L1139:
	movq	%r13, %r12
.L1140:
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L1164
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1139
.L1137:
	movq	24(%rbx), %rax
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	addq	$64, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L1128
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1128:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10944:
	.size	_ZN4node4heap7JSGraphD2Ev, .-_ZN4node4heap7JSGraphD2Ev
	.weak	_ZN4node4heap7JSGraphD1Ev
	.set	_ZN4node4heap7JSGraphD1Ev,_ZN4node4heap7JSGraphD2Ev
	.section	.text._ZN4node4heap7JSGraphD0Ev,"axG",@progbits,_ZN4node4heap7JSGraphD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4heap7JSGraphD0Ev
	.type	_ZN4node4heap7JSGraphD0Ev, @function
_ZN4node4heap7JSGraphD0Ev:
.LFB10946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4heap7JSGraphE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	144(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L1166
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	32(%r14), %r13
	leaq	16(%r14), %r15
	testq	%r13, %r13
	je	.L1167
.L1168:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1168
.L1167:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1169
.L1166:
	movq	136(%r12), %rax
	movq	128(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	128(%r12), %rdi
	leaq	176(%r12), %rax
	movq	$0, 152(%r12)
	movq	$0, 144(%r12)
	cmpq	%rax, %rdi
	je	.L1170
	call	_ZdlPv@PLT
.L1170:
	movq	88(%r12), %r13
	testq	%r13, %r13
	je	.L1171
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	%r13, %rdi
	movq	0(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1172
.L1171:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	movq	32(%r12), %r13
	testq	%r13, %r13
	jne	.L1177
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1174
.L1176:
	movq	%rbx, %r13
.L1177:
	movq	8(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	jne	.L1201
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1176
.L1174:
	movq	24(%r12), %rax
	movq	16(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	16(%r12), %rdi
	leaq	64(%r12), %rax
	movq	$0, 40(%r12)
	movq	$0, 32(%r12)
	cmpq	%rax, %rdi
	je	.L1178
	call	_ZdlPv@PLT
.L1178:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10946:
	.size	_ZN4node4heap7JSGraphD0Ev, .-_ZN4node4heap7JSGraphD0Ev
	.text
	.p2align 4
	.globl	_ZN4node4heap18BuildEmbedderGraphERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node4heap18BuildEmbedderGraphERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4heap18BuildEmbedderGraphERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1222
	cmpw	$1040, %cx
	jne	.L1203
.L1222:
	movq	23(%rdx), %rdx
.L1205:
	movss	.LC15(%rip), %xmm0
	leaq	-240(%rbp), %r8
	movq	352(%rdx), %rdi
	leaq	16+_ZTVN4node4heap7JSGraphE(%rip), %r12
	movq	%r8, %rsi
	leaq	-176(%rbp), %r13
	leaq	-120(%rbp), %r14
	movq	%r8, -248(%rbp)
	leaq	-64(%rbp), %r15
	movq	%rdi, -232(%rbp)
	movq	%r12, -240(%rbp)
	movq	%r13, -224(%rbp)
	movq	$1, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	%r14, -168(%rbp)
	movq	$1, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r15, -112(%rbp)
	movq	$1, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movss	%xmm0, -192(%rbp)
	movss	%xmm0, -136(%rbp)
	movss	%xmm0, -80(%rbp)
	call	_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv@PLT
	movq	-248(%rbp), %r8
	movq	%r8, %rdi
	call	_ZNK4node4heap7JSGraph12CreateObjectEv
	testq	%rax, %rax
	je	.L1207
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L1207:
	movq	-96(%rbp), %rbx
	movq	%r12, -240(%rbp)
	testq	%rbx, %rbx
	je	.L1208
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	%rbx, %r8
	movq	(%rbx), %rbx
	movq	32(%r8), %r12
	leaq	16(%r8), %rax
	testq	%r12, %r12
	je	.L1209
.L1210:
	movq	24(%r12), %rsi
	movq	%rax, %rdi
	movq	%r8, -256(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZNSt8_Rb_treeISt4pairIPKcPN2v813EmbedderGraph4NodeEES7_St9_IdentityIS7_ESt4lessIS7_ESaIS7_EE8_M_eraseEPSt13_Rb_tree_nodeIS7_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %r8
	testq	%r12, %r12
	jne	.L1210
.L1209:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1211
.L1208:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-112(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	$0, -96(%rbp)
	cmpq	%r15, %rdi
	je	.L1212
	call	_ZdlPv@PLT
.L1212:
	movq	-152(%rbp), %r12
	testq	%r12, %r12
	je	.L1213
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1214
.L1213:
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-168(%rbp), %rdi
	movq	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	cmpq	%r14, %rdi
	je	.L1215
	call	_ZdlPv@PLT
.L1215:
	movq	-208(%rbp), %r12
	testq	%r12, %r12
	jne	.L1219
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1216
.L1218:
	movq	%rbx, %r12
.L1219:
	movq	8(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L1245
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1218
.L1216:
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-224(%rbp), %rdi
	movq	$0, -200(%rbp)
	movq	$0, -208(%rbp)
	cmpq	%r13, %rdi
	je	.L1202
	call	_ZdlPv@PLT
.L1202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1246
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1203:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdx
	jmp	.L1205
.L1246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7801:
	.size	_ZN4node4heap18BuildEmbedderGraphERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4heap18BuildEmbedderGraphERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node9StreamReqE
	.section	.data.rel.ro._ZTVN4node9StreamReqE,"awG",@progbits,_ZTVN4node9StreamReqE,comdat
	.align 8
	.type	_ZTVN4node9StreamReqE, @object
	.size	_ZTVN4node9StreamReqE, 48
_ZTVN4node9StreamReqE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, 168
_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-16
	.quad	0
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, 168
_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-40
	.quad	0
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node4heap13JSGraphJSNodeE
	.section	.data.rel.ro.local._ZTVN4node4heap13JSGraphJSNodeE,"awG",@progbits,_ZTVN4node4heap13JSGraphJSNodeE,comdat
	.align 8
	.type	_ZTVN4node4heap13JSGraphJSNodeE, @object
	.size	_ZTVN4node4heap13JSGraphJSNodeE, 80
_ZTVN4node4heap13JSGraphJSNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node4heap13JSGraphJSNodeD1Ev
	.quad	_ZN4node4heap13JSGraphJSNodeD0Ev
	.quad	_ZN4node4heap13JSGraphJSNode4NameEv
	.quad	_ZN4node4heap13JSGraphJSNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN2v813EmbedderGraph4Node10IsRootNodeEv
	.quad	_ZN4node4heap13JSGraphJSNode14IsEmbedderNodeEv
	.quad	_ZN2v813EmbedderGraph4Node10NamePrefixEv
	.weak	_ZTVN4node4heap7JSGraphE
	.section	.data.rel.ro.local._ZTVN4node4heap7JSGraphE,"awG",@progbits,_ZTVN4node4heap7JSGraphE,comdat
	.align 8
	.type	_ZTVN4node4heap7JSGraphE, @object
	.size	_ZTVN4node4heap7JSGraphE, 56
_ZTVN4node4heap7JSGraphE:
	.quad	0
	.quad	0
	.quad	_ZN4node4heap7JSGraph6V8NodeERKN2v85LocalINS2_5ValueEEE
	.quad	_ZN4node4heap7JSGraph7AddNodeESt10unique_ptrIN2v813EmbedderGraph4NodeESt14default_deleteIS5_EE
	.quad	_ZN4node4heap7JSGraph7AddEdgeEPN2v813EmbedderGraph4NodeES5_PKc
	.quad	_ZN4node4heap7JSGraphD1Ev
	.quad	_ZN4node4heap7JSGraphD0Ev
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node4heap12_GLOBAL__N_116FileOutputStreamE, @object
	.size	_ZTVN4node4heap12_GLOBAL__N_116FileOutputStreamE, 64
_ZTVN4node4heap12_GLOBAL__N_116FileOutputStreamE:
	.quad	0
	.quad	0
	.quad	_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD1Ev
	.quad	_ZN4node4heap12_GLOBAL__N_116FileOutputStreamD0Ev
	.quad	_ZN4node4heap12_GLOBAL__N_116FileOutputStream11EndOfStreamEv
	.quad	_ZN4node4heap12_GLOBAL__N_116FileOutputStream12GetChunkSizeEv
	.quad	_ZN4node4heap12_GLOBAL__N_116FileOutputStream15WriteAsciiChunkEPci
	.quad	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE, @object
	.size	_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE, 400
_ZTVN4node4heap12_GLOBAL__N_118HeapSnapshotStreamE:
	.quad	0
	.quad	0
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev
	.quad	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream14MemoryInfoNameEv
	.quad	_ZNK4node4heap12_GLOBAL__N_118HeapSnapshotStream8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetChunkSizeEv
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv
	.quad	_ZN4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEv
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream8ReadStopEv
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream7IsAliveEv
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream9IsClosingEv
	.quad	_ZN4node10StreamBase9IsIPCPipeEv
	.quad	_ZN4node10StreamBase5GetFDEv
	.quad	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn56_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.quad	-120
	.quad	0
	.quad	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD1Ev
	.quad	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStreamD0Ev
	.quad	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream11EndOfStreamEv
	.quad	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream12GetChunkSizeEv
	.quad	_ZThn120_N4node4heap12_GLOBAL__N_118HeapSnapshotStream15WriteAsciiChunkEPci
	.quad	_ZN2v812OutputStream19WriteHeapStatsChunkEPNS_15HeapStatsUpdateEi
	.section	.rodata.str1.1
.LC22:
	.string	"../src/heap_utils.cc"
.LC23:
	.string	"heap_utils"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC22
	.quad	0
	.quad	_ZN4node4heap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC23
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC24:
	.string	"../src/heap_utils.cc:388"
.LC25:
	.string	"(*path) != nullptr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"void node::heap::TriggerHeapSnapshot(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4heap19TriggerHeapSnapshotERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"../src/heap_utils.cc:363"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"void node::heap::CreateHeapSnapshotStream(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4heap24CreateHeapSnapshotStreamERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC27
	.quad	.LC14
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"../src/heap_utils.cc:291"
.LC30:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"virtual int node::heap::{anonymous}::HeapSnapshotStream::DoWrite(node::WriteWrap*, uv_buf_t*, size_t, uv_stream_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args, @object
	.size	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args, 24
_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"../src/heap_utils.cc:284"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"virtual int node::heap::{anonymous}::HeapSnapshotStream::DoShutdown(node::ShutdownWrap*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapEE4args, @object
	.size	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapEE4args, 24
_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream10DoShutdownEPNS_12ShutdownWrapEE4args:
	.quad	.LC32
	.quad	.LC30
	.quad	.LC33
	.section	.rodata.str1.1
.LC34:
	.string	"../src/heap_utils.cc:274"
.LC35:
	.string	"(snapshot_) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"virtual int node::heap::{anonymous}::HeapSnapshotStream::ReadStart()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEvE4args, @object
	.size	_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEvE4args, 24
_ZZN4node4heap12_GLOBAL__N_118HeapSnapshotStream9ReadStartEvE4args:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.weak	_ZZN4node4heap13JSGraphJSNodeC4EPN2v87IsolateENS2_5LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC37:
	.string	"../src/heap_utils.cc:46"
.LC38:
	.string	"!val.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"node::heap::JSGraphJSNode::JSGraphJSNode(v8::Isolate*, v8::Local<v8::Value>)"
	.section	.data.rel.ro.local._ZZN4node4heap13JSGraphJSNodeC4EPN2v87IsolateENS2_5LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node4heap13JSGraphJSNodeC4EPN2v87IsolateENS2_5LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node4heap13JSGraphJSNodeC4EPN2v87IsolateENS2_5LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node4heap13JSGraphJSNodeC4EPN2v87IsolateENS2_5LocalINS2_5ValueEEEE4args, 24
_ZZN4node4heap13JSGraphJSNodeC4EPN2v87IsolateENS2_5LocalINS2_5ValueEEEE4args:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC40:
	.string	"../src/stream_base-inl.h:103"
.LC41:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC43:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC45:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC46:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC46
	.quad	.LC44
	.quad	.LC47
	.weak	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC48:
	.string	"../src/stream_base-inl.h:26"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"(req_wrap_obj->GetAlignedPointerFromInternalField( StreamReq::kStreamReqField)) == (nullptr)"
	.align 8
.LC50:
	.string	"void node::StreamReq::AttachToObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC51:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC53:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC54:
	.string	"../src/base_object-inl.h:195"
.LC55:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC53
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC56:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC58:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC59:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC61:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC62:
	.string	"../src/env-inl.h:955"
.LC63:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	8097874909635634504
	.quad	7310033046960302195
	.align 16
.LC7:
	.quad	7517463719428581715
	.quad	8239175502546629749
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC15:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
