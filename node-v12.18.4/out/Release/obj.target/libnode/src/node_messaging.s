	.file	"node_messaging.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4687:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4687:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4689:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4689:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4971:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4971:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4972:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4972:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZNK4node6worker7Message8SelfSizeEv,"axG",@progbits,_ZNK4node6worker7Message8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker7Message8SelfSizeEv
	.type	_ZNK4node6worker7Message8SelfSizeEv, @function
_ZNK4node6worker7Message8SelfSizeEv:
.LFB6038:
	.cfi_startproc
	endbr64
	movl	$120, %eax
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZNK4node6worker7Message8SelfSizeEv, .-_ZNK4node6worker7Message8SelfSizeEv
	.section	.text._ZNK4node6worker15MessagePortData8SelfSizeEv,"axG",@progbits,_ZNK4node6worker15MessagePortData8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker15MessagePortData8SelfSizeEv
	.type	_ZNK4node6worker15MessagePortData8SelfSizeEv, @function
_ZNK4node6worker15MessagePortData8SelfSizeEv:
.LFB6046:
	.cfi_startproc
	endbr64
	movl	$104, %eax
	ret
	.cfi_endproc
.LFE6046:
	.size	_ZNK4node6worker15MessagePortData8SelfSizeEv, .-_ZNK4node6worker15MessagePortData8SelfSizeEv
	.section	.text._ZNK4node6worker11MessagePort8SelfSizeEv,"axG",@progbits,_ZNK4node6worker11MessagePort8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker11MessagePort8SelfSizeEv
	.type	_ZNK4node6worker11MessagePort8SelfSizeEv, @function
_ZNK4node6worker11MessagePort8SelfSizeEv:
.LFB6050:
	.cfi_startproc
	endbr64
	movl	$240, %eax
	ret
	.cfi_endproc
.LFE6050:
	.size	_ZNK4node6worker11MessagePort8SelfSizeEv, .-_ZNK4node6worker11MessagePort8SelfSizeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7589:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7589:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7590:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7590:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7591:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7591:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7593:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L13
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7593:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD2Ev, @function
_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD2Ev:
.LFB11671:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11671:
	.size	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD2Ev, .-_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD2Ev
	.set	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD1Ev,_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11703:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11703:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEj, @function
_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEj:
.LFB7836:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	ja	.L21
	movq	(%rcx,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEjE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7836:
	.size	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEj, .-_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEj
	.section	.text._ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm,"axG",@progbits,_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm
	.type	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm, @function
_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm:
.LFB7521:
	.cfi_startproc
	endbr64
	lock addq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE7521:
	.size	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm, .-_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB11675:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L23
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	ret
	.cfi_endproc
.LFE11675:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11707:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE11707:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11706:
	.cfi_startproc
	endbr64
	addq	$16, %rdi
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE11706:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD0Ev, @function
_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD0Ev:
.LFB11673:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11673:
	.size	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD0Ev, .-_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD0Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB11677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11677:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11705:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11705:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L32
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L32:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11708:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEj, @function
_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEj:
.LFB7837:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movl	%edx, %edx
	movq	32(%r8), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	subq	%rsi, %rax
	sarq	$4, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L41
	leaq	(%rdx,%rdx,2), %rax
	salq	$4, %rax
	addq	%rax, %rsi
	jmp	_ZN2v816WasmModuleObject23FromTransferrableModuleEPNS_7IsolateERKNS0_19TransferrableModuleE@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEjE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7837:
	.size	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEj, .-_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEj
	.section	.text._ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm,"axG",@progbits,_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm
	.type	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm, @function
_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm:
.LFB7522:
	.cfi_startproc
	endbr64
	lock subq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE7522:
	.size	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm, .-_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD2Ev, @function
_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD2Ev:
.LFB11667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SerializerDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	48(%rbx), %r13
	movq	40(%rbx), %r12
	cmpq	%r12, %r13
	je	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L49
.L47:
	movq	40(%rbx), %r12
.L45:
	testq	%r12, %r12
	je	.L43
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L49
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11667:
	.size	_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD2Ev, .-_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD2Ev
	.set	_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD1Ev,_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD0Ev, @function
_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD0Ev:
.LFB11669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SerializerDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	48(%r13), %rbx
	movq	40(%r13), %r12
	cmpq	%r12, %rbx
	je	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L61
.L59:
	movq	40(%r13), %r12
.L57:
	testq	%r12, %r12
	je	.L62
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L62:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$88, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L61
	jmp	.L59
	.cfi_endproc
.LFE11669:
	.size	_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD0Ev, .-_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"DOMException"
	.text
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEE, @function
_ZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEE:
.LFB7857:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L73
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%r14, %rdi
	movq	%rax, %r13
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L86
.L78:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L73
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movl	%eax, %r8d
	movq	%r12, %rax
	testb	%r8b, %r8b
	jne	.L81
	leaq	_ZZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
.L81:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	%rdx, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L78
	.cfi_endproc
.LFE7857:
	.size	_ZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEE, .-_ZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEE
	.section	.rodata.str1.1
.LC2:
	.string	"DataCloneError"
	.text
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_123ThrowDataCloneExceptionEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEE, @function
_ZN4node6worker12_GLOBAL__N_123ThrowDataCloneExceptionEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEE:
.LFB7858:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%rbx, -64(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L99
.L88:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEE
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L87
	movq	%r12, %rsi
	leaq	-64(%rbp), %rcx
	movl	$2, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L87
	movq	%r13, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L87:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rax
	jmp	.L88
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7858:
	.size	_ZN4node6worker12_GLOBAL__N_123ThrowDataCloneExceptionEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEE, .-_ZN4node6worker12_GLOBAL__N_123ThrowDataCloneExceptionEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEE
	.section	.rodata.str1.1
.LC3:
	.string	"ERR_CONSTRUCT_CALL_INVALID"
.LC4:
	.string	"Constructor cannot be called"
.LC5:
	.string	"code"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L110
	cmpw	$1040, %cx
	jne	.L102
.L110:
	movq	23(%rdx), %rax
.L104:
	movq	352(%rax), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L112
.L105:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L113
.L106:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L114
.L107:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L115
.L108:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L116
.L109:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L112:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L115:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L116:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L109
	.cfi_endproc
.LFE7969:
	.size	_ZN4node6worker11MessagePort3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker11MessagePort3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateE, @function
_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateE:
.LFB7835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-28(%rbp), %rsi
	subq	$24, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v817ValueDeserializer10ReadUint32EPj@PLT
	testb	%al, %al
	jne	.L118
	xorl	%eax, %eax
.L119:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L127
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movl	-28(%rbp), %ecx
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	ja	.L128
	movq	(%rdx,%rcx,8), %rcx
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L119
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	jne	.L119
	movq	16(%rcx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7835:
	.size	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateE, .-_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePortD2Ev
	.type	_ZN4node6worker11MessagePortD2Ev, @function
_ZN4node6worker11MessagePortD2Ev:
.LFB7955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker11MessagePortE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L130
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%r12), %r14
	movq	%r13, %rdi
	movq	$0, 72(%r14)
	movq	$0, 88(%r12)
	call	uv_mutex_unlock@PLT
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L130:
	movq	232(%r12), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L131:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L132
	movq	(%rdi), %rax
	call	*8(%rax)
.L132:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE7955:
	.size	_ZN4node6worker11MessagePortD2Ev, .-_ZN4node6worker11MessagePortD2Ev
	.globl	_ZN4node6worker11MessagePortD1Ev
	.set	_ZN4node6worker11MessagePortD1Ev,_ZN4node6worker11MessagePortD2Ev
	.section	.text._ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev:
.LFB6049:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$116, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$5793150163928442189, %rcx
	movq	%rdx, (%rdi)
	movl	$29295, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE6049:
	.size	_ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev:
.LFB6037:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$101, 22(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$26465, %edx
	movl	$1936942413, 16(%rdi)
	movw	%dx, 20(%rdi)
	movq	$7, 8(%rdi)
	movb	$0, 23(%rdi)
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev:
.LFB6045:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1148482159, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$5793150163928442189, %rcx
	movq	%rdx, (%rdi)
	movl	$29793, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$97, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE6045:
	.size	_ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePortD0Ev
	.type	_ZN4node6worker11MessagePortD0Ev, @function
_ZN4node6worker11MessagePortD0Ev:
.LFB7957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker11MessagePortE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	88(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L147
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%r12), %r14
	movq	%r13, %rdi
	movq	$0, 72(%r14)
	movq	$0, 88(%r12)
	call	uv_mutex_unlock@PLT
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L147:
	movq	232(%r12), %rdi
	testq	%rdi, %rdi
	je	.L148
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L148:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L149
	movq	(%rdi), %rax
	call	*8(%rax)
.L149:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7957:
	.size	_ZN4node6worker11MessagePortD0Ev, .-_ZN4node6worker11MessagePortD0Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB11928:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L161
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L162
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L162:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L163
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L164
.L163:
	testq	%rbx, %rbx
	je	.L164
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L164:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L165
	call	__stack_chk_fail@PLT
.L161:
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L165:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11928:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata._ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"0123456789abcdef"
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB12137:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC7(%rip), %rax
.L172:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L172
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L173
	call	__stack_chk_fail@PLT
.L173:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12137:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"ERR_MISSING_MESSAGE_PORT_IN_TRANSFER_LIST"
	.align 8
.LC9:
	.string	"MessagePort was found in message but not listed in transferList"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate15WriteHostObjectEPN2v87IsolateENS3_5LocalINS3_6ObjectEEE, @function
_ZN4node6worker12_GLOBAL__N_118SerializerDelegate15WriteHostObjectEPN2v87IsolateENS3_5LocalINS3_6ObjectEEE:
.LFB7889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	3184(%rax), %rdi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L177
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L198
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L192
	cmpw	$1040, %cx
	jne	.L179
.L192:
	movq	23(%rdx), %rax
.L181:
	movq	64(%rbx), %rdi
	movq	72(%rbx), %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	je	.L182
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L183:
	leal	1(%rsi), %ecx
	movq	%rcx, %rsi
	cmpq	%rdx, %rcx
	jnb	.L182
.L185:
	cmpq	%rax, (%rdi,%rcx,8)
	jne	.L183
	movq	8(%rbx), %rdi
	call	_ZN2v815ValueSerializer11WriteUint32Ej@PLT
	movl	$1, %edx
.L184:
	xorl	%eax, %eax
	addq	$24, %rsp
	movb	%dl, %al
	popq	%rbx
	popq	%r12
	movb	$1, %ah
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdi
	movq	360(%rax), %rax
	movq	320(%rax), %rsi
	call	_ZN4node6worker12_GLOBAL__N_123ThrowDataCloneExceptionEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEE
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	movb	$0, %ah
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	movq	352(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L199
.L186:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L200
.L187:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L201
.L188:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L202
.L189:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L203
.L190:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%edx, %edx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L179:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L199:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L200:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L201:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L202:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L203:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L190
	.cfi_endproc
.LFE7889:
	.size	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate15WriteHostObjectEPN2v87IsolateENS3_5LocalINS3_6ObjectEEE, .-_ZN4node6worker12_GLOBAL__N_118SerializerDelegate15WriteHostObjectEPN2v87IsolateENS3_5LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate19ThrowDataCloneErrorEN2v85LocalINS3_6StringEEE, @function
_ZN4node6worker12_GLOBAL__N_118SerializerDelegate19ThrowDataCloneErrorEN2v85LocalINS3_6StringEEE:
.LFB7888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%rbx, -64(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L216
.L205:
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEE
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L204
	movq	%r13, %rsi
	leaq	-64(%rbp), %rcx
	movl	$2, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L204
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L204:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rax
	jmp	.L205
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7888:
	.size	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate19ThrowDataCloneErrorEN2v85LocalINS3_6StringEEE, .-_ZN4node6worker12_GLOBAL__N_118SerializerDelegate19ThrowDataCloneErrorEN2v85LocalINS3_6StringEEE
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L218
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L222:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L220
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L222
.L218:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L222
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB7561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L225
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L254
	cmpq	$1, %rax
	jne	.L228
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L229:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L229
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L227:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L225:
	cmpb	$37, 1(%rax)
	jne	.L256
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L257
	cmpq	$1, %r13
	jne	.L234
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L235:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L237
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L258
.L237:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L239:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L259
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L241:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L242
	call	_ZdlPv@PLT
.L242:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L224
	call	_ZdlPv@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L234:
	testq	%r13, %r13
	jne	.L260
	movq	%rbx, %rax
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L233:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L259:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L239
.L256:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L255:
	call	__stack_chk_fail@PLT
.L260:
	movq	%rbx, %rdi
	jmp	.L233
	.cfi_endproc
.LFE7561:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker7MessageC2EONS_14MallocedBufferIcEE
	.type	_ZN4node6worker7MessageC2EONS_14MallocedBufferIcEE, @function
_ZN4node6worker7MessageC2EONS_14MallocedBufferIcEE:
.LFB7822:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, 8(%rdi)
	movq	8(%rsi), %rax
	movups	%xmm0, 24(%rdi)
	movq	%rax, 16(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 56(%rdi)
	movups	%xmm0, 72(%rdi)
	movups	%xmm0, 88(%rdi)
	movups	%xmm0, 104(%rdi)
	ret
	.cfi_endproc
.LFE7822:
	.size	_ZN4node6worker7MessageC2EONS_14MallocedBufferIcEE, .-_ZN4node6worker7MessageC2EONS_14MallocedBufferIcEE
	.globl	_ZN4node6worker7MessageC1EONS_14MallocedBufferIcEE
	.set	_ZN4node6worker7MessageC1EONS_14MallocedBufferIcEE,_ZN4node6worker7MessageC2EONS_14MallocedBufferIcEE
	.align 2
	.p2align 4
	.globl	_ZNK4node6worker7Message14IsCloseMessageEv
	.type	_ZNK4node6worker7Message14IsCloseMessageEv, @function
_ZNK4node6worker7Message14IsCloseMessageEv:
.LFB7824:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE7824:
	.size	_ZNK4node6worker7Message14IsCloseMessageEv, .-_ZNK4node6worker7Message14IsCloseMessageEv
	.align 2
	.p2align 4
	.globl	_ZN4node6worker15MessagePortDataC2EPNS0_11MessagePortE
	.type	_ZN4node6worker15MessagePortDataC2EPNS0_11MessagePortE, @function
_ZN4node6worker15MessagePortDataC2EPNS0_11MessagePortE:
.LFB7931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L265
	leaq	48(%rbx), %rax
	movq	%r12, 72(%rbx)
	movl	$56, %edi
	movq	%rax, %xmm0
	movq	$0, 64(%rbx)
	movq	$0, 80(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	call	_Znwm@PLT
	movq	%rax, %r12
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r12)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%r12), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L265
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movq	$0, 96(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7931:
	.size	_ZN4node6worker15MessagePortDataC2EPNS0_11MessagePortE, .-_ZN4node6worker15MessagePortDataC2EPNS0_11MessagePortE
	.globl	_ZN4node6worker15MessagePortDataC1EPNS0_11MessagePortE
	.set	_ZN4node6worker15MessagePortDataC1EPNS0_11MessagePortE,_ZN4node6worker15MessagePortDataC2EPNS0_11MessagePortE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker15MessagePortData8EntangleEPS1_S2_
	.type	_ZN4node6worker15MessagePortData8EntangleEPS1_S2_, @function
_ZN4node6worker15MessagePortData8EntangleEPS1_S2_:
.LFB7940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 96(%rdi)
	jne	.L284
	cmpq	$0, 96(%rsi)
	jne	.L285
	movq	80(%rsi), %rax
	movq	88(%rsi), %r13
	movq	%rsi, 96(%rdi)
	movq	%rdi, %rbx
	movq	88(%rdi), %r12
	movq	%rdi, 96(%rsi)
	movq	%rax, 80(%rdi)
	cmpq	%r12, %r13
	je	.L267
	testq	%r13, %r13
	je	.L271
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L272
	lock addl	$1, 8(%r13)
	movq	88(%rdi), %r12
.L271:
	testq	%r12, %r12
	je	.L274
.L287:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L275
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L276:
	cmpl	$1, %eax
	jne	.L274
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L278
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L279:
	cmpl	$1, %eax
	je	.L286
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%r13, 88(%rbx)
.L267:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L272:
	addl	$1, 8(%r13)
	testq	%r12, %r12
	jne	.L287
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L285:
	leaq	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L278:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L279
	.cfi_endproc
.LFE7940:
	.size	_ZN4node6worker15MessagePortData8EntangleEPS1_S2_, .-_ZN4node6worker15MessagePortData8EntangleEPS1_S2_
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort12TriggerAsyncEv
	.type	_ZN4node6worker11MessagePort12TriggerAsyncEv, @function
_ZN4node6worker11MessagePort12TriggerAsyncEv:
.LFB7967:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L294
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$104, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L297
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leaq	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7967:
	.size	_ZN4node6worker11MessagePort12TriggerAsyncEv, .-_ZN4node6worker11MessagePort12TriggerAsyncEv
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort6DetachEv
	.type	_ZN4node6worker11MessagePort6DetachEv, @function
_ZN4node6worker11MessagePort6DetachEv:
.LFB7988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	88(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L301
	leaq	8(%rdi), %r12
	movq	%rsi, %rbx
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%rbx), %rax
	movq	%r12, %rdi
	movq	$0, 72(%rax)
	movq	$0, 88(%rbx)
	movq	%rax, 0(%r13)
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	leaq	_ZZN4node6worker11MessagePort6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7988:
	.size	_ZN4node6worker11MessagePort6DetachEv, .-_ZN4node6worker11MessagePort6DetachEv
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort8EntangleEPS1_S2_
	.type	_ZN4node6worker11MessagePort8EntangleEPS1_S2_, @function
_ZN4node6worker11MessagePort8EntangleEPS1_S2_:
.LFB8033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	88(%rdi), %rbx
	movq	88(%rsi), %rax
	cmpq	$0, 96(%rbx)
	jne	.L319
	cmpq	$0, 96(%rax)
	jne	.L320
	movq	80(%rax), %rdx
	movq	88(%rax), %r13
	movq	%rax, 96(%rbx)
	movq	88(%rbx), %r12
	movq	%rbx, 96(%rax)
	movq	%rdx, 80(%rbx)
	cmpq	%r12, %r13
	je	.L302
	testq	%r13, %r13
	je	.L306
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L307
	lock addl	$1, 8(%r13)
	movq	88(%rbx), %r12
.L306:
	testq	%r12, %r12
	je	.L309
.L322:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L310
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L311:
	cmpl	$1, %eax
	jne	.L309
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L313
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L314:
	cmpl	$1, %eax
	je	.L321
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%r13, 88(%rbx)
.L302:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L307:
	addl	$1, 8(%r13)
	testq	%r12, %r12
	jne	.L322
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L321:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L313:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L314
	.cfi_endproc
.LFE8033:
	.size	_ZN4node6worker11MessagePort8EntangleEPS1_S2_, .-_ZN4node6worker11MessagePort8EntangleEPS1_S2_
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort8EntangleEPS1_PNS0_15MessagePortDataE
	.type	_ZN4node6worker11MessagePort8EntangleEPS1_PNS0_15MessagePortDataE, @function
_ZN4node6worker11MessagePort8EntangleEPS1_PNS0_15MessagePortDataE:
.LFB8034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	88(%rdi), %rbx
	cmpq	$0, 96(%rbx)
	jne	.L340
	cmpq	$0, 96(%rsi)
	jne	.L341
	movq	80(%rsi), %rax
	movq	88(%rsi), %r13
	movq	%rsi, 96(%rbx)
	movq	88(%rbx), %r12
	movq	%rbx, 96(%rsi)
	movq	%rax, 80(%rbx)
	cmpq	%r12, %r13
	je	.L323
	testq	%r13, %r13
	je	.L327
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L328
	lock addl	$1, 8(%r13)
	movq	88(%rbx), %r12
.L327:
	testq	%r12, %r12
	je	.L330
.L343:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L331
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L332:
	cmpl	$1, %eax
	jne	.L330
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L334
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L335:
	cmpl	$1, %eax
	je	.L342
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r13, 88(%rbx)
.L323:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L328:
	addl	$1, 8(%r13)
	testq	%r12, %r12
	jne	.L343
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L342:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L334:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L335
	.cfi_endproc
.LFE8034:
	.size	_ZN4node6worker11MessagePort8EntangleEPS1_PNS0_15MessagePortDataE, .-_ZN4node6worker11MessagePort8EntangleEPS1_PNS0_15MessagePortDataE
	.section	.rodata.str1.1
.LC10:
	.string	"postMessage"
.LC11:
	.string	"start"
	.text
	.p2align 4
	.globl	_ZN4node6worker33GetMessagePortConstructorTemplateEPNS_11EnvironmentE
	.type	_ZN4node6worker33GetMessagePortConstructorTemplateEPNS_11EnvironmentE, @function
_ZN4node6worker33GetMessagePortConstructorTemplateEPNS_11EnvironmentE:
.LFB8036:
	.cfi_startproc
	endbr64
	movq	3184(%rdi), %rax
	testq	%rax, %rax
	jne	.L359
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN4node6worker11MessagePort3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	352(%rdi), %r13
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3184(%rbx)
	testq	%r12, %r12
	je	.L362
.L349:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3184(%rbx)
	testq	%rax, %rax
	jne	.L355
.L362:
	movq	352(%rbx), %r13
.L345:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r12
	movq	360(%rbx), %rax
	movq	%r12, %rdi
	movq	1008(%rax), %rsi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node6worker11MessagePort11PostMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L363
.L346:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L364
.L347:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3184(%rbx), %rdi
	movq	352(%rbx), %r13
	testq	%rdi, %rdi
	jne	.L365
	testq	%r12, %r12
	jne	.L349
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE8036:
	.size	_ZN4node6worker33GetMessagePortConstructorTemplateEPNS_11EnvironmentE, .-_ZN4node6worker33GetMessagePortConstructorTemplateEPNS_11EnvironmentE
	.section	.rodata.str1.1
.LC12:
	.string	"MessageChannel"
.LC13:
	.string	"stopMessagePort"
.LC14:
	.string	"drainMessagePort"
.LC15:
	.string	"receiveMessageOnPort"
.LC16:
	.string	"moveMessagePortToContext"
	.section	.text.unlikely
.LCOLDB17:
	.text
.LHOTB17:
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB8038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L367
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L367
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L367
	movq	271(%rax), %rbx
	movl	$14, %ecx
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L392
.L368:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r14
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L393
.L369:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L394
.L370:
	movq	%rbx, %rdi
	call	_ZN4node6worker33GetMessagePortConstructorTemplateEPNS_11EnvironmentE
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L395
.L371:
	movq	360(%rbx), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	1008(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L396
.L372:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L397
.L373:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L398
.L374:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L399
.L375:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6worker11MessagePort5DrainERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L400
.L376:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L401
.L377:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L402
.L378:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node6worker11MessagePort14ReceiveMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L403
.L379:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L404
.L380:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L405
.L381:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L406
.L382:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L407
.L383:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L408
.L384:
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L409
.L385:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L410
.L386:
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L411
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L394:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L396:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L397:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L399:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L400:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L402:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L403:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L405:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L406:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L408:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L409:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB8038:
.L367:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE8038:
	.text
	.size	_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE17:
	.text
.LHOTE17:
	.p2align 4
	.globl	_Z19_register_messagingv
	.type	_Z19_register_messagingv, @function
_Z19_register_messagingv:
.LFB8039:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE8039:
	.size	_Z19_register_messagingv, .-_Z19_register_messagingv
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB8113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L414
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L414:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L416
.L430:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L430
.L416:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L431
.L417:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r14
	movl	$32, -240(%rbp)
	movq	%rax, -232(%rbp)
	andq	$-16, %r14
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$48, -236(%rbp)
	call	*%r10
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L432
	cmpq	$1, %r13
	jne	.L420
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L421:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L433
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L421
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L419:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L431:
	orq	$0, -8(%rsp,%rcx)
	jmp	.L417
.L433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8113:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L441
	movq	16(%rdi), %r10
.L435:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L436
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L442
	movq	16(%r9), %r10
.L437:
	cmpq	%r10, %rax
	jbe	.L444
.L436:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L438:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L445
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L440:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L445:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$15, %r10d
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L442:
	movl	$15, %r10d
	jmp	.L437
	.cfi_endproc
.LFE8548:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm,"axG",@progbits,_ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm
	.type	_ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm, @function
_ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm:
.LFB9106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L467
	movq	%rdi, %rbx
	movq	%rsi, %r12
	cmpq	8(%rdi), %rsi
	jbe	.L449
	leaq	24(%rdi), %rax
	movl	$1, %r14d
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r13
	je	.L468
.L450:
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r12,8), %r15
	andq	%r12, %rax
	cmpq	%rax, %r12
	jne	.L469
	testq	%r15, %r15
	je	.L470
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	realloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L471
.L454:
	movq	%rdi, 16(%rbx)
	movq	%r12, 8(%rbx)
	testb	%r14b, %r14b
	jne	.L449
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L449
	movq	-56(%rbp), %rsi
	salq	$3, %rdx
	call	memcpy@PLT
.L449:
	movq	%r12, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L468:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%r13, %rdi
	call	free@PLT
	movl	$1, %eax
	xorl	%edi, %edi
.L453:
	testq	%r12, %r12
	je	.L454
	testb	%al, %al
	je	.L454
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	sete	%al
	jmp	.L453
	.cfi_endproc
.LFE9106:
	.size	_ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm, .-_ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm
	.section	.rodata.str1.1
.LC18:
	.string	"vector::_M_realloc_insert"
	.text
	.p2align 4
	.type	_ZN4node6workerL12ReadIterableEPNS_11EnvironmentEN2v85LocalINS3_7ContextEEERNS_16MaybeStackBufferINS4_INS3_5ValueEEELm8EEES9_, @function
_ZN4node6workerL12ReadIterableEPNS_11EnvironmentEN2v85LocalINS3_7ContextEEERNS_16MaybeStackBufferINS4_INS3_5ValueEEELm8EEES9_:
.LFB7990:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rcx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L473
.L528:
	movl	$1, %eax
.L545:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	movl	%eax, %r14d
	testb	%al, %al
	je	.L475
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	16(%rbx), %rdi
	movl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L579
	cmpq	8(%rbx), %r13
	jbe	.L478
	leaq	24(%rbx), %rax
	movb	%r14b, -56(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rdi
	je	.L580
.L479:
	movq	%r13, %rsi
	salq	$3, %rsi
	je	.L581
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	je	.L582
.L482:
	cmpb	$0, -56(%rbp)
	movq	%rax, 16(%rbx)
	movq	%r13, 8(%rbx)
	je	.L583
.L478:
	movq	%r13, (%rbx)
	testq	%r13, %r13
	je	.L484
	xorl	%r14d, %r14d
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L564:
	addq	$1, %r14
	movq	%rax, (%rdx)
	cmpq	%r14, %r13
	je	.L484
.L519:
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	cmpq	%r14, (%rbx)
	jbe	.L517
	movq	16(%rbx), %rdx
	leaq	(%rdx,%r14,8), %rdx
	testq	%rax, %rax
	jne	.L564
	xorl	%eax, %eax
	movq	$0, (%rdx)
	movb	$0, %ah
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L475:
	movq	352(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Symbol11GetIteratorEPNS_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L488
	movq	%rax, -56(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-56(%rbp), %rdi
	testb	%al, %al
	je	.L528
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L488
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L528
	movq	360(%r13), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	1072(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L488
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L528
	leaq	1930(%r13), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L515:
	movq	-72(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L584
.L495:
	movq	-56(%rbp), %r15
	movq	-96(%rbp), %r14
	movq	%rbx, %rdi
	movq	%r15, %r12
	subq	%r14, %r12
	movq	%r12, %rsi
	sarq	$3, %rsi
	call	_ZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEm
	cmpq	$0, (%rbx)
	je	.L517
	movq	%r14, %rsi
	cmpq	%r14, %r15
	je	.L518
	movq	16(%rbx), %rdi
	movq	%r12, %rdx
	call	memmove@PLT
.L518:
	movl	$257, %eax
.L516:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	movl	%eax, -56(%rbp)
	call	_ZdlPv@PLT
	movl	-56(%rbp), %eax
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L488:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L517:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L583:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L478
	movq	-80(%rbp), %rsi
	salq	$3, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L580:
	movb	$0, -56(%rbp)
	xorl	%edi, %edi
	jmp	.L479
.L582:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	sete	%r14b
.L481:
	testq	%r13, %r13
	je	.L482
	testb	%r14b, %r14b
	je	.L482
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L484:
	movl	$257, %eax
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L579:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L481
.L584:
	movzbl	2664(%r13), %eax
	testb	%al, %al
	jne	.L495
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L497
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L585
	movq	360(%r13), %rax
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	560(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L497
	movq	%r14, %rsi
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	testb	%al, %al
	jne	.L495
	movq	360(%r13), %rax
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	1880(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L497
	movq	-56(%rbp), %rcx
	cmpq	%rcx, -80(%rbp)
	je	.L586
	movq	%rcx, %rax
	movq	%r12, (%rcx)
	addq	$8, %rax
	movq	%rax, -56(%rbp)
	jmp	.L515
.L586:
	movabsq	$1152921504606846975, %rcx
	movq	-80(%rbp), %r8
	subq	-96(%rbp), %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L587
	testq	%rax, %rax
	je	.L588
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L589
	xorl	%esi, %esi
	testq	%rcx, %rcx
	jne	.L590
.L508:
	movq	-56(%rbp), %rax
	movq	-96(%rbp), %r9
	movq	%r12, (%rsi,%r8)
	cmpq	%r9, %rax
	je	.L534
	leaq	-8(%rax), %rdi
	leaq	15(%rsi), %rax
	subq	%r9, %rdi
	subq	%r9, %rax
	movq	%rdi, %r8
	shrq	$3, %r8
	cmpq	$30, %rax
	jbe	.L535
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r8
	je	.L535
	addq	$1, %r8
	xorl	%eax, %eax
	movq	%r8, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L511:
	movdqu	(%r9,%rax), %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L511
	movq	%r8, %r9
	movq	-96(%rbp), %rax
	andq	$-2, %r9
	leaq	0(,%r9,8), %rdx
	addq	%rdx, %rax
	addq	%rsi, %rdx
	cmpq	%r9, %r8
	je	.L513
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L513:
	leaq	8(%rsi,%rdi), %rax
.L509:
	movq	-96(%rbp), %rdi
	addq	$8, %rax
	movq	%rax, -56(%rbp)
	testq	%rdi, %rdi
	je	.L514
	movq	%rsi, -96(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rsi
	movq	-80(%rbp), %rcx
.L514:
	leaq	(%rsi,%rcx), %rax
	movq	%rsi, -96(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L515
.L497:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L516
.L588:
	movl	$1, %ecx
.L506:
	salq	$3, %rcx
	movq	%r8, -104(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -80(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r8
	movq	-80(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L508
.L589:
	movabsq	$1152921504606846975, %rcx
	jmp	.L506
.L535:
	movq	-96(%rbp), %rax
	movq	-56(%rbp), %r9
	movq	%rsi, %rdx
.L510:
	movq	(%rax), %r8
	addq	$8, %rax
	addq	$8, %rdx
	movq	%r8, -8(%rdx)
	cmpq	%r9, %rax
	jne	.L510
	jmp	.L513
.L590:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	jmp	.L506
.L534:
	movq	%rsi, %rax
	jmp	.L509
.L585:
	movl	$1, %eax
	jmp	.L516
.L587:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7990:
	.size	_ZN4node6workerL12ReadIterableEPNS_11EnvironmentEN2v85LocalINS3_7ContextEEERNS_16MaybeStackBufferINS4_INS3_5ValueEEELm8EEES9_, .-_ZN4node6workerL12ReadIterableEPNS_11EnvironmentEN2v85LocalINS3_7ContextEEERNS_16MaybeStackBufferINS4_INS3_5ValueEEELm8EEES9_
	.section	.text._ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB9666:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L628
	movq	%rsi, %rbx
	movq	%rdi, %r13
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L614
	movabsq	$9223372036854775792, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L629
.L593:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rdx
	addq	%rax, %r15
	movq	%rax, -56(%rbp)
	addq	$16, %rax
	movq	%r15, -64(%rbp)
.L613:
	movdqu	(%rdx), %xmm4
	movq	8(%rdx), %rcx
	movq	-56(%rbp), %rdx
	movaps	%xmm4, -80(%rbp)
	movups	%xmm4, (%rdx,%rsi)
	testq	%rcx, %rcx
	je	.L595
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L596
	lock addl	$1, 8(%rcx)
.L595:
	cmpq	%r12, %rbx
	je	.L597
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-56(%rbp), %rdx
	movq	%r12, %r15
	jne	.L599
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L603:
	movq	(%rdi), %rcx
	movq	%rdx, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	*16(%rcx)
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %rdx
	movl	12(%rdi), %ecx
	leal	-1(%rcx), %esi
	cmpl	$1, %ecx
	movl	%esi, 12(%rdi)
	jne	.L602
	movq	(%rdi), %rcx
	movq	%rdx, -80(%rbp)
	call	*24(%rcx)
	movq	-80(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L602:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L604
.L605:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm1, %xmm1
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm1, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L602
	movl	8(%rdi), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %ecx
	je	.L603
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	jne	.L605
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%rbx, %rax
	subq	%r12, %rax
	addq	-56(%rbp), %rax
	addq	$16, %rax
.L597:
	cmpq	%r14, %rbx
	je	.L610
	subq	%rbx, %r14
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r14), %rdi
	movq	%rdi, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L611:
	movdqu	(%rbx,%rdx), %xmm3
	addq	$1, %rcx
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	ja	.L611
	leaq	16(%rax,%rdi), %rax
.L610:
	testq	%r12, %r12
	je	.L612
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rax
.L612:
	movq	-56(%rbp), %xmm0
	movq	%rax, %xmm6
	movq	-64(%rbp), %rax
	punpcklqdq	%xmm6, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movq	(%rdi), %rcx
	movq	%rdi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	*16(%rcx)
	movq	-80(%rbp), %rdi
	movl	$-1, %ecx
	lock xaddl	%ecx, 12(%rdi)
	movq	-88(%rbp), %rdx
	cmpl	$1, %ecx
	jne	.L607
	movq	(%rdi), %rcx
	movq	%rdx, -80(%rbp)
	call	*24(%rcx)
	movq	-80(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L607:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L604
.L599:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm2, %xmm2
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm2, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L607
	lock subl	$1, 8(%rdi)
	jne	.L607
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L596:
	addl	$1, 8(%rcx)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L629:
	testq	%rdi, %rdi
	jne	.L594
	movq	$0, -64(%rbp)
	movl	$16, %eax
	movq	$0, -56(%rbp)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L614:
	movl	$16, %r15d
	jmp	.L593
.L628:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L594:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$4, %r15
	jmp	.L593
	.cfi_endproc
.LFE9666:
	.size	_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker7Message20AddSharedArrayBufferERKSt10shared_ptrINS0_25SharedArrayBufferMetadataEE
	.type	_ZN4node6worker7Message20AddSharedArrayBufferERKSt10shared_ptrINS0_25SharedArrayBufferMetadataEE, @function
_ZN4node6worker7Message20AddSharedArrayBufferERKSt10shared_ptrINS0_25SharedArrayBufferMetadataEE:
.LFB7851:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	56(%rdi), %rsi
	cmpq	64(%rdi), %rsi
	je	.L632
	movq	(%rdx), %rax
	movq	%rax, (%rsi)
	movq	8(%rdx), %rax
	movq	%rax, 8(%rsi)
	testq	%rax, %rax
	je	.L633
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L634
	lock addl	$1, 8(%rax)
	movq	56(%rdi), %rsi
.L633:
	addq	$16, %rsi
	movq	%rsi, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	addq	$48, %rdi
	jmp	_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.p2align 4,,10
	.p2align 3
.L634:
	addq	$16, %rsi
	addl	$1, 8(%rax)
	movq	%rsi, 56(%rdi)
	ret
	.cfi_endproc
.LFE7851:
	.size	_ZN4node6worker7Message20AddSharedArrayBufferERKSt10shared_ptrINS0_25SharedArrayBufferMetadataEE, .-_ZN4node6worker7Message20AddSharedArrayBufferERKSt10shared_ptrINS0_25SharedArrayBufferMetadataEE
	.section	.text._ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB9679:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movabsq	$192153584101141162, %rdi
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	imulq	%rcx, %rax
	cmpq	%rdi, %rax
	je	.L674
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r14, %rcx
	testq	%rax, %rax
	je	.L659
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L675
.L642:
	movq	%rsi, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
	leaq	48(%rax), %rsi
.L658:
	movq	-56(%rbp), %r15
	pxor	%xmm0, %xmm0
	movdqu	(%rdx), %xmm3
	movups	%xmm0, (%rdx)
	leaq	(%r15,%rcx), %rax
	movq	16(%rdx), %rcx
	movq	$0, 16(%rdx)
	movups	%xmm3, (%rax)
	movq	%rcx, 16(%rax)
	movq	24(%rdx), %rcx
	movq	%rcx, 24(%rax)
	movq	32(%rdx), %rcx
	movq	$0, 32(%rdx)
	movq	40(%rdx), %rdx
	movq	%rcx, 32(%rax)
	movq	%rdx, 40(%rax)
	cmpq	%r14, %rbx
	je	.L644
	movq	%r14, %r13
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L678:
	movl	$-1, %ecx
	lock xaddl	%ecx, 8(%rdi)
	cmpl	$1, %ecx
	je	.L676
.L648:
	addq	$48, %r13
	addq	$48, %r15
	cmpq	%r13, %rbx
	je	.L677
.L654:
	movq	0(%r13), %rcx
	movq	$0, 8(%r15)
	pxor	%xmm1, %xmm1
	movq	%rcx, (%r15)
	movq	8(%r13), %rcx
	movq	%rcx, 8(%r15)
	movq	16(%r13), %rcx
	movups	%xmm1, 0(%r13)
	movq	$0, 16(%r13)
	movq	%rcx, 16(%r15)
	movq	24(%r13), %rcx
	movq	%rcx, 24(%r15)
	movq	32(%r13), %rcx
	movq	$0, 32(%r13)
	movq	%rcx, 32(%r15)
	movq	40(%r13), %rcx
	movq	32(%r13), %rdi
	movq	%rcx, 40(%r15)
	testq	%rdi, %rdi
	je	.L645
	call	_ZdaPv@PLT
.L645:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L646
	call	_ZdaPv@PLT
.L646:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L648
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L678
	movl	8(%rdi), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %ecx
	jne	.L648
.L676:
	movq	(%rdi), %rcx
	movq	%rdi, -64(%rbp)
	call	*16(%rcx)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-64(%rbp), %rdi
	je	.L652
	movl	$-1, %ecx
	lock xaddl	%ecx, 12(%rdi)
.L653:
	cmpl	$1, %ecx
	jne	.L648
	movq	(%rdi), %rcx
	addq	$48, %r13
	addq	$48, %r15
	call	*24(%rcx)
	cmpq	%r13, %rbx
	jne	.L654
	.p2align 4,,10
	.p2align 3
.L677:
	movabsq	$768614336404564651, %rdx
	leaq	-48(%rbx), %rax
	subq	%r14, %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	6(%rax,%rax,2), %rsi
	salq	$4, %rsi
	addq	-56(%rbp), %rsi
.L644:
	cmpq	%r12, %rbx
	je	.L655
	movq	%rbx, %rax
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L656:
	movq	16(%rax), %rcx
	movdqu	(%rax), %xmm2
	addq	$48, %rax
	addq	$48, %rdx
	movq	%rcx, -32(%rdx)
	movq	-24(%rax), %rcx
	movups	%xmm2, -48(%rdx)
	movq	%rcx, -24(%rdx)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%r12, %rax
	jne	.L656
	movabsq	$768614336404564651, %rdx
	subq	%rbx, %rax
	subq	$48, %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	3(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%rax, %rsi
.L655:
	testq	%r14, %r14
	je	.L657
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
.L657:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rsi, %xmm4
	movq	-72(%rbp), %rbx
	punpcklqdq	%xmm4, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	movl	12(%rdi), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 12(%rdi)
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L675:
	testq	%r8, %r8
	jne	.L643
	movq	$0, -72(%rbp)
	movl	$48, %esi
	movq	$0, -56(%rbp)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L659:
	movl	$48, %esi
	jmp	.L642
.L643:
	cmpq	%rdi, %r8
	movq	%rdi, %rsi
	cmovbe	%r8, %rsi
	imulq	$48, %rsi, %rsi
	jmp	.L642
.L674:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9679:
	.size	_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate23GetWasmModuleTransferIdEPN2v87IsolateENS3_5LocalINS3_16WasmModuleObjectEEE, @function
_ZN4node6worker12_GLOBAL__N_118SerializerDelegate23GetWasmModuleTransferIdEPN2v87IsolateENS3_5LocalINS3_16WasmModuleObjectEEE:
.LFB7897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %rbx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v816WasmModuleObject22GetTransferrableModuleEv@PLT
	movq	104(%rbx), %rsi
	cmpq	112(%rbx), %rsi
	je	.L680
	movq	-96(%rbp), %rax
	movq	$0, 8(%rsi)
	addq	$48, %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, -48(%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, -40(%rsi)
	movq	-80(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -32(%rsi)
	movq	-72(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -24(%rsi)
	movq	-64(%rbp), %rax
	movq	$0, -64(%rbp)
	movq	%rax, -16(%rsi)
	movq	-56(%rbp), %rax
	movq	%rax, -8(%rsi)
	movq	%rsi, 104(%rbx)
.L681:
	subq	96(%rbx), %rsi
	movq	-64(%rbp), %rdi
	sarq	$4, %rsi
	imull	$-1431655765, %esi, %esi
	leal	-1(%rsi), %eax
	salq	$32, %rax
	orq	$1, %rax
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L682
	call	_ZdaPv@PLT
.L682:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L683
	call	_ZdaPv@PLT
.L683:
	movq	-88(%rbp), %r12
	testq	%r12, %r12
	je	.L685
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L686
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L699
	.p2align 4,,10
	.p2align 3
.L685:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L700
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L685
.L699:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L689
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L690:
	cmpl	$1, %eax
	jne	.L685
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	104(%rbx), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L689:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L690
.L700:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7897:
	.size	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate23GetWasmModuleTransferIdEPN2v87IsolateENS3_5LocalINS3_16WasmModuleObjectEEE, .-_ZN4node6worker12_GLOBAL__N_118SerializerDelegate23GetWasmModuleTransferIdEPN2v87IsolateENS3_5LocalINS3_16WasmModuleObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker7Message13AddWASMModuleEON2v816WasmModuleObject19TransferrableModuleE
	.type	_ZN4node6worker7Message13AddWASMModuleEON2v816WasmModuleObject19TransferrableModuleE, @function
_ZN4node6worker7Message13AddWASMModuleEON2v816WasmModuleObject19TransferrableModuleE:
.LFB7854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	104(%rdi), %rsi
	cmpq	112(%rdi), %rsi
	je	.L702
	movq	(%rdx), %rax
	movq	$0, 8(%rsi)
	pxor	%xmm0, %xmm0
	addq	$48, %rsi
	movq	%rax, -48(%rsi)
	movq	8(%rdx), %rax
	movups	%xmm0, (%rdx)
	movq	%rax, -40(%rsi)
	movq	16(%rdx), %rax
	movq	$0, 16(%rdx)
	movq	%rax, -32(%rsi)
	movq	24(%rdx), %rax
	movq	%rax, -24(%rsi)
	movq	32(%rdx), %rax
	movq	$0, 32(%rdx)
	movq	%rax, -16(%rsi)
	movq	40(%rdx), %rax
	movq	%rax, -8(%rsi)
	movq	%rsi, 104(%rdi)
.L703:
	subq	96(%rbx), %rsi
	addq	$8, %rsp
	sarq	$4, %rsi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	imull	$-1431655765, %esi, %esi
	leal	-1(%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	leaq	96(%rdi), %rdi
	call	_ZNSt6vectorIN2v816WasmModuleObject19TransferrableModuleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	104(%rbx), %rsi
	jmp	.L703
	.cfi_endproc
.LFE7854:
	.size	_ZN4node6worker7Message13AddWASMModuleEON2v816WasmModuleObject19TransferrableModuleE, .-_ZN4node6worker7Message13AddWASMModuleEON2v816WasmModuleObject19TransferrableModuleE
	.section	.text._ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB9720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r14
	movq	%rdi, -80(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L737
	movq	%rsi, %r12
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L723
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L738
.L707:
	movq	%rcx, %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rdx
	addq	%rax, %rcx
	addq	$8, %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
.L722:
	movq	(%rdx), %rax
	addq	-56(%rbp), %rsi
	movq	%rax, (%rsi)
	testq	%rax, %rax
	je	.L709
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
	movq	-88(%rbp), %rdx
	movq	$0, (%rdx)
.L709:
	cmpq	%r14, %r12
	je	.L710
	movq	-56(%rbp), %r13
	movq	%r14, %r15
	.p2align 4,,10
	.p2align 3
.L712:
	movq	(%r15), %rcx
	movq	%rcx, 0(%r13)
	cmpq	$0, (%r15)
	je	.L711
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
	movq	$0, (%r15)
.L711:
	addq	$8, %r15
	addq	$8, %r13
	cmpq	%r15, %r12
	jne	.L712
	movq	-56(%rbp), %rdx
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	8(%rdx,%rax), %rax
	movq	%rax, -64(%rbp)
.L710:
	cmpq	%rbx, %r12
	je	.L713
	movq	-64(%rbp), %r13
	movq	%r12, %r15
	.p2align 4,,10
	.p2align 3
.L715:
	movq	(%r15), %rcx
	movq	%rcx, 0(%r13)
	testq	%rcx, %rcx
	je	.L714
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
	movq	$0, (%r15)
.L714:
	addq	$8, %r15
	addq	$8, %r13
	cmpq	%r15, %rbx
	jne	.L715
	movq	%rbx, %rax
	subq	%r12, %rax
	addq	%rax, -64(%rbp)
.L713:
	movq	%r14, %r12
	cmpq	%rbx, %r14
	je	.L720
	.p2align 4,,10
	.p2align 3
.L716:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L719
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L716
.L720:
	testq	%r14, %r14
	je	.L718
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L718:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	movhps	-64(%rbp), %xmm0
	movq	%rdx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%rbx, %r12
	jne	.L716
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L738:
	testq	%r8, %r8
	jne	.L708
	movq	$8, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L723:
	movl	$8, %ecx
	jmp	.L707
.L708:
	cmpq	%rdi, %r8
	movq	%rdi, %rcx
	cmovbe	%r8, %rcx
	salq	$3, %rcx
	jmp	.L707
.L737:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9720:
	.size	_ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate22GetSharedArrayBufferIdEPN2v87IsolateENS3_5LocalINS3_17SharedArrayBufferEEE, @function
_ZN4node6worker12_GLOBAL__N_118SerializerDelegate22GetSharedArrayBufferIdEPN2v87IsolateENS3_5LocalINS3_17SharedArrayBufferEEE:
.LFB7890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$72, %rsp
	movq	40(%rdi), %rsi
	movq	48(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subq	%rsi, %rcx
	sarq	$3, %rcx
	je	.L740
	testq	%rdx, %rdx
	je	.L766
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L744:
	movq	(%rsi,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L742
	movq	(%rdx), %rax
	cmpq	%rax, (%r8)
	je	.L743
.L742:
	leal	1(%rbx), %edx
	movq	%rdx, %rbx
	cmpq	%rcx, %rdx
	jb	.L744
.L740:
	movq	24(%r12), %rdx
	leaq	-80(%rbp), %r15
	movq	%r8, %rcx
	movq	%r8, -104(%rbp)
	movq	16(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN4node6worker25SharedArrayBufferMetadata20ForSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_17SharedArrayBufferEEE@PLT
	xorl	%eax, %eax
	cmpq	$0, -80(%rbp)
	movq	-104(%rbp), %r8
	je	.L749
	testq	%r8, %r8
	je	.L750
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %r14
.L750:
	movq	%r14, -88(%rbp)
	movq	48(%r12), %rsi
	cmpq	56(%r12), %rsi
	je	.L751
	movq	%r14, (%rsi)
	cmpq	$0, -88(%rbp)
	je	.L752
	leaq	-88(%rbp), %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
	addq	$8, 48(%r12)
.L753:
	movq	32(%r12), %rax
	movq	56(%rax), %rsi
	cmpq	64(%rax), %rsi
	je	.L754
.L784:
	movq	-72(%rbp), %rdx
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	testq	%rdx, %rdx
	je	.L755
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L756
	lock addl	$1, 8(%rdx)
	movq	56(%rax), %rsi
.L755:
	addq	$16, %rsi
	movq	%rsi, 56(%rax)
.L757:
	salq	$32, %rbx
	movq	%rbx, %rax
	orq	$1, %rax
.L749:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L747
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L759
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
.L760:
	cmpl	$1, %edx
	jne	.L747
	movq	(%r12), %rdx
	movq	%rax, -104(%rbp)
	movq	%r12, %rdi
	call	*16(%rdx)
	testq	%rbx, %rbx
	movq	-104(%rbp), %rax
	je	.L762
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r12)
.L763:
	cmpl	$1, %edx
	je	.L782
	.p2align 4,,10
	.p2align 3
.L747:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L783
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L766:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L746:
	leal	1(%rbx), %eax
	movq	%rax, %rbx
	cmpq	%rcx, %rax
	jnb	.L740
.L741:
	cmpq	$0, (%rsi,%rax,8)
	jne	.L746
.L743:
	salq	$32, %rbx
	movq	%rbx, %rax
	orq	$1, %rax
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L752:
	movq	32(%r12), %rax
	addq	$8, %rsi
	movq	%rsi, 48(%r12)
	movq	56(%rax), %rsi
	cmpq	64(%rax), %rsi
	jne	.L784
.L754:
	leaq	48(%rax), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorISt10shared_ptrIN4node6worker25SharedArrayBufferMetadataEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L759:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	jmp	.L760
.L751:
	leaq	40(%r12), %rdi
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIN2v86GlobalINS0_17SharedArrayBufferEEESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L753
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L753
.L756:
	addl	$1, 8(%rdx)
	jmp	.L755
.L782:
	movq	(%r12), %rdx
	movq	%rax, -104(%rbp)
	movq	%r12, %rdi
	call	*24(%rdx)
	movq	-104(%rbp), %rax
	jmp	.L747
.L762:
	movl	12(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r12)
	jmp	.L763
.L783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7890:
	.size	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate22GetSharedArrayBufferIdEPN2v87IsolateENS3_5LocalINS3_17SharedArrayBufferEEE, .-_ZN4node6worker12_GLOBAL__N_118SerializerDelegate22GetSharedArrayBufferIdEPN2v87IsolateENS3_5LocalINS3_17SharedArrayBufferEEE
	.section	.text._ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB9759:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L799
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L795
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L800
.L787:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L794:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L801
	testq	%r13, %r13
	jg	.L790
	testq	%r9, %r9
	jne	.L793
.L791:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L790
.L793:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L800:
	testq	%rsi, %rsi
	jne	.L788
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L790:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L791
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L795:
	movl	$8, %r14d
	jmp	.L787
.L799:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L788:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L787
	.cfi_endproc
.LFE9759:
	.size	_ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB9764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r12
	movabsq	$576460752303423487, %rdi
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L825
	movq	%rsi, %r8
	subq	%r12, %r8
	testq	%rax, %rax
	je	.L816
	movabsq	$9223372036854775792, %r15
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L826
.L804:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r15), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	leaq	16(%rbx), %rax
	movq	%rax, -56(%rbp)
.L815:
	leaq	(%rbx,%r8), %rax
	movq	8(%rdx), %rdi
	movq	(%rdx), %r8
	movq	$0, (%rdx)
	movq	%r8, (%rax)
	movq	%rdi, 8(%rax)
	cmpq	%r12, %rsi
	je	.L806
	movq	%r12, %rax
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L807:
	movq	(%rax), %r8
	movq	8(%rax), %rdi
	addq	$16, %rax
	addq	$16, %rdx
	movq	%r8, -16(%rdx)
	movq	%rdi, -8(%rdx)
	movq	$0, -16(%rax)
	cmpq	%rax, %rsi
	jne	.L807
	movq	%rsi, %rax
	subq	%r12, %rax
	leaq	16(%rbx,%rax), %rax
	movq	%rax, -56(%rbp)
.L806:
	movq	-56(%rbp), %rdx
	movq	%rsi, %rax
	cmpq	%r14, %rsi
	je	.L813
	.p2align 4,,10
	.p2align 3
.L812:
	movq	(%rax), %r8
	movq	8(%rax), %rdi
	addq	$16, %rax
	addq	$16, %rdx
	movq	$0, -16(%rax)
	movq	%r8, -16(%rdx)
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r14
	jne	.L812
	movq	%r14, %rax
	subq	%rsi, %rax
	addq	%rax, -56(%rbp)
.L813:
	movq	%r12, %r15
	cmpq	%r14, %r12
	je	.L814
	.p2align 4,,10
	.p2align 3
.L809:
	movq	(%r15), %rdi
	addq	$16, %r15
	call	free@PLT
	cmpq	%r14, %r15
	jne	.L809
.L814:
	testq	%r12, %r12
	je	.L811
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L811:
	movq	-64(%rbp), %rax
	movq	%rbx, %xmm0
	movhps	-56(%rbp), %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L826:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L805
	movq	$16, -56(%rbp)
	xorl	%ebx, %ebx
	movq	$0, -64(%rbp)
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L816:
	movl	$16, %r15d
	jmp	.L804
.L805:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	movq	%rdi, %r15
	salq	$4, %r15
	jmp	.L804
.L825:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9764:
	.size	_ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB10212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L828
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L838
.L854:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L839:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L828:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L852
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L853
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L831:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L833
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L835:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L836:
	testq	%rsi, %rsi
	je	.L833
.L834:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L835
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L841
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L834
	.p2align 4,,10
	.p2align 3
.L833:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L837
	call	_ZdlPv@PLT
.L837:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L854
.L838:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L840
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L840:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L841:
	movq	%rdx, %rdi
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L852:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L831
.L853:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10212:
	.size	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB10216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L864
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L865
.L857:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L866
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L867
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L862
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L862:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L860:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L866:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L859
	cmpq	%r14, %rsi
	je	.L860
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L859:
	cmpq	%r14, %rsi
	je	.L860
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L860
.L864:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L867:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10216:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata._ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc.str1.1,"aMS",@progbits,1
.LC20:
	.string	"wrapped"
.LC21:
	.string	"wrapper"
	.section	.text._ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc,"axG",@progbits,_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	.type	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc, @function
_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc:
.LFB7615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L869
	movq	(%rax), %rsi
	movq	%rdx, %r10
	movq	8(%rsi), %rdi
	movq	%rsi, %r8
	movq	%rdi, %r9
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L947:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L876
	movq	8(%r8), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L876
.L872:
	cmpq	%r9, %r12
	jne	.L947
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L874
	cmpq	72(%rbx), %rax
	je	.L948
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L874
.L959:
	movq	8(%rbx), %rdi
	movq	16(%r8), %rdx
	movq	%r13, %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L874:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L949
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L869
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L869
.L876:
	cmpq	%rdi, %r12
	jne	.L950
	movq	16(%rsi), %r15
.L891:
	movq	80(%rbx), %rcx
	movq	64(%rbx), %rax
	movq	%r15, -128(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L892
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L893:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-128(%rbp), %r13
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L904
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L951
.L895:
	movq	-8(%rax), %rax
.L894:
	cmpq	%r13, %rax
	jne	.L952
	cmpq	$0, 64(%rax)
	je	.L953
	cmpq	72(%rbx), %rdi
	je	.L898
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L869:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	48(%r15), %rdx
	movq	%rax, (%r15)
	movq	%r12, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rdx, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	testq	%r12, %r12
	je	.L954
	leaq	-128(%rbp), %rax
	movq	(%rbx), %rsi
	movq	%rdx, -192(%rbp)
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	-192(%rbp), %rdx
	testq	%rax, %rax
	je	.L877
	movq	8(%rbx), %rdi
	leaq	-168(%rbp), %rsi
	movq	(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	%rax, -168(%rbp)
	call	*%rcx
	movq	-192(%rbp), %rdx
	movq	%rax, 16(%r15)
.L877:
	movq	8(%r15), %rsi
	movq	%rdx, -192(%rbp)
	leaq	-96(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rcx
	movq	32(%r15), %rdi
	movq	-192(%rbp), %rdx
	cmpq	%rcx, %rsi
	je	.L955
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %r8
	cmpq	%rdi, %rdx
	je	.L956
	movq	%rax, %xmm0
	movq	%r8, %xmm3
	movq	48(%r15), %rdx
	movq	%rsi, 32(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 40(%r15)
	testq	%rdi, %rdi
	je	.L883
	movq	%rdi, -96(%rbp)
	movq	%rdx, -80(%rbp)
.L881:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-184(%rbp), %rdi
	movq	%rax, 64(%r15)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	-184(%rbp), %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L885
	movq	(%rdi), %rax
	call	*8(%rax)
.L885:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L886
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L957:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L886
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L886
.L888:
	cmpq	%rsi, %r12
	jne	.L957
	addq	$16, %rcx
.L899:
	movq	%r15, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L889
	cmpq	72(%rbx), %rax
	je	.L958
.L890:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L889
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L889:
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L891
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC20(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r15), %rsi
	movq	%r15, %rdx
	leaq	.LC21(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L948:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %rsi
	addq	$512, %rax
	testq	%rsi, %rsi
	jne	.L959
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L951:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L898:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L892:
	leaq	-128(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L952:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L953:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L904:
	xorl	%eax, %eax
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L955:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L879
	cmpq	$1, %rdx
	je	.L960
	movq	%rcx, %rsi
	movq	%rcx, -192(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	movq	-192(%rbp), %rcx
.L879:
	movq	%rdx, 40(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L958:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L890
.L956:
	movq	%rax, %xmm0
	movq	%r8, %xmm4
	movq	%rsi, 32(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 40(%r15)
.L883:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L886:
	movl	$24, %edi
	movq	%r9, -184(%rbp)
	call	_Znwm@PLT
	movq	-184(%rbp), %r9
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	%r9, %rsi
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L954:
	leaq	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L960:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	jmp	.L879
.L949:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7615:
	.size	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc, .-_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	.section	.rodata.str1.1
.LC22:
	.string	"data"
.LC23:
	.string	"emit_message_fn"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node6worker11MessagePort10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6worker11MessagePort10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6worker11MessagePort10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L962
	movq	104(%rsi), %rdi
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L963
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L991:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L963
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L963
.L965:
	cmpq	%rsi, %r8
	jne	.L991
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L992
	cmpq	72(%rbx), %rax
	je	.L993
.L966:
	movq	-8(%rax), %rsi
.L972:
	leaq	.LC22(%rip), %rcx
	call	*%r8
.L962:
	movq	232(%r12), %rax
	testq	%rax, %rax
	je	.L961
	movq	(%rax), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L961
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L975
	cmpq	72(%rbx), %rcx
	je	.L994
.L971:
	movq	-8(%rcx), %rsi
.L970:
	leaq	.LC23(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L961:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L995
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L994:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L963:
	leaq	.LC22(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L993:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L975:
	xorl	%esi, %esi
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L992:
	xorl	%esi, %esi
	jmp	.L972
.L995:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8035:
	.size	_ZNK4node6worker11MessagePort10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6worker11MessagePort10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1
.LC24:
	.string	"incoming_messages"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node6worker15MessagePortData10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6worker15MessagePortData10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6worker15MessagePortData10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	leaq	48(%r12), %r13
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	cmpq	48(%r12), %r13
	je	.L997
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L998
	cmpq	72(%rbx), %rax
	je	.L1041
.L999:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L998
	subq	$24, 64(%rax)
.L998:
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$17, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC24(%rip), %rcx
	movq	%rax, (%r15)
	leaq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	leaq	-64(%rbp), %rcx
	movq	$24, 64(%r15)
	movb	$0, 24(%r15)
	movq	%rcx, %rsi
	movq	(%rdi), %rax
	movq	%rcx, -72(%rbp)
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1000
	movq	(%rdi), %rax
	call	*8(%rax)
.L1000:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1001
	movq	%rax, %rdx
	cmpq	72(%rbx), %rax
	je	.L1042
.L1002:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1001
	movq	8(%rbx), %rdi
	leaq	.LC24(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rax
.L1001:
	movq	80(%rbx), %rcx
	movq	%r15, -64(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1003
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L1004:
	movq	48(%r12), %r12
	cmpq	%r12, %r13
	je	.L1013
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	104(%rbx), %r8
	leaq	16(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1008
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1008
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1008
.L1010:
	cmpq	%rdi, %rsi
	jne	.L1043
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1044
	cmpq	72(%rbx), %rax
	je	.L1045
.L1011:
	movq	-8(%rax), %rsi
.L1014:
	xorl	%ecx, %ecx
	call	*%r8
.L1012:
	movq	(%r12), %r12
	cmpq	%r12, %r13
	jne	.L1005
.L1013:
	movq	64(%rbx), %rdi
	cmpq	72(%rbx), %rdi
	je	.L1046
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L997:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1047
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1008:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1044:
	xorl	%esi, %esi
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	-72(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1046:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L997
.L1047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7937:
	.size	_ZNK4node6worker15MessagePortData10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6worker15MessagePortData10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1
.LC25:
	.string	"array_buffer_contents"
.LC26:
	.string	"MallocedBuffer"
.LC27:
	.string	"shared_array_buffers"
.LC28:
	.string	"message_ports"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node6worker7Message10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6worker7Message10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6worker7Message10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L1049
	movq	64(%rsi), %rax
	cmpq	32(%rsi), %rax
	je	.L1050
	cmpq	72(%rsi), %rax
	je	.L1154
.L1051:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L1050
	subq	$24, 64(%rax)
.L1050:
	movl	$72, %edi
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r14
	leaq	-64(%rbp), %r13
	call	_Znwm@PLT
	movl	$21, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC25(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r15), %rdi
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	$24, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1052
	movq	(%rdi), %rax
	call	*8(%rax)
.L1052:
	movq	64(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L1053
	movq	%rax, %rdx
	cmpq	72(%r15), %rax
	je	.L1155
.L1054:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1053
	movq	8(%r15), %rdi
	leaq	.LC25(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%r15), %rax
.L1053:
	movq	80(%r15), %rcx
	movq	%r12, -64(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L1055
	movq	%r12, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%r15)
.L1056:
	movq	24(%rbx), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rcx
	movq	32(%rbx), %rax
	cmpq	%rax, %rcx
	jne	.L1057
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1153
	movq	8(%r15), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L1153:
	movq	32(%rbx), %rax
.L1060:
	addq	$16, -72(%rbp)
	movq	-72(%rbp), %rcx
	cmpq	%rax, %rcx
	je	.L1065
.L1057:
	movq	-72(%rbp), %rdx
	movq	8(%rdx), %r11
	testq	%r11, %r11
	movq	%r11, -80(%rbp)
	je	.L1060
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$14, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC26(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r15), %rdi
	movq	-80(%rbp), %r11
	movq	%r13, %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	%r11, 64(%r12)
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1061
	movq	(%rdi), %rax
	call	*8(%rax)
.L1061:
	movq	64(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L1153
	cmpq	72(%r15), %rax
	jne	.L1063
	movq	88(%r15), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	64(%r15), %rdi
	cmpq	72(%r15), %rdi
	je	.L1156
	subq	$8, %rdi
	movq	%rdi, 64(%r15)
.L1049:
	movq	56(%rbx), %rax
	subq	48(%rbx), %rax
	movq	%rax, %r13
	je	.L1066
	movl	$72, %edi
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r14
	call	_Znwm@PLT
	movl	$20, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC27(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r15), %rdi
	movq	%r13, 64(%r12)
	leaq	-64(%rbp), %r13
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1067
	movq	(%rdi), %rax
	call	*8(%rax)
.L1067:
	movq	64(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L1068
	movq	72(%r15), %rdx
	movq	88(%r15), %rcx
	movq	%rax, %rsi
	cmpq	%rdx, %rax
	je	.L1157
.L1069:
	movq	-8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1070
	movq	8(%r15), %rdi
	leaq	.LC27(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L1066:
	movq	72(%rbx), %rax
	cmpq	%rax, 80(%rbx)
	je	.L1048
	movq	64(%r15), %rax
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r14
	leaq	-64(%rbp), %r13
	cmpq	32(%r15), %rax
	je	.L1074
	movq	72(%r15), %rdx
	movq	88(%r15), %rcx
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r14
	leaq	-64(%rbp), %r13
.L1095:
	cmpq	%rax, %rdx
	je	.L1158
.L1075:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L1074
	subq	$24, 64(%rax)
.L1074:
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$13, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC28(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r15), %rdi
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	$24, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1077
	movq	(%rdi), %rax
	call	*8(%rax)
.L1077:
	movq	64(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L1078
	movq	%rax, %rdx
	cmpq	72(%r15), %rax
	je	.L1159
.L1079:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L1078
	movq	8(%r15), %rdi
	leaq	.LC28(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%r15), %rax
.L1078:
	movq	80(%r15), %rdx
	movq	%r12, -64(%rbp)
	subq	$8, %rdx
	cmpq	%rdx, %rax
	je	.L1080
	movq	%r12, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%r15)
.L1081:
	movq	80(%rbx), %rax
	movq	72(%rbx), %r12
	cmpq	%r12, %rax
	je	.L1091
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1085
	movq	104(%r15), %r8
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	96(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1086
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1086
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L1086
.L1088:
	cmpq	%rsi, %rdi
	jne	.L1160
	movq	8(%r15), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%r15), %rax
	cmpq	32(%r15), %rax
	je	.L1161
	cmpq	72(%r15), %rax
	je	.L1162
.L1089:
	movq	-8(%rax), %rsi
.L1092:
	xorl	%ecx, %ecx
	call	*%r8
	movq	80(%rbx), %rax
.L1085:
	addq	$8, %r12
	cmpq	%r12, %rax
	jne	.L1090
.L1091:
	movq	64(%r15), %rdi
	cmpq	72(%r15), %rdi
	je	.L1163
	subq	$8, %rdi
	movq	%rdi, 64(%r15)
.L1048:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1164
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1162:
	.cfi_restore_state
	movq	88(%r15), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1086:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	movq	80(%rbx), %rax
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	88(%r15), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	88(%rsi), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	88(%r15), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	-8(%rcx), %rax
	addq	$512, %rax
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1161:
	xorl	%esi, %esi
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1163:
	call	_ZdlPv@PLT
	movq	88(%r15), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm4
	leaq	504(%rax), %rbx
	movq	%rax, %xmm3
	addq	$512, %rax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 64(%r15)
	movq	%rax, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 80(%r15)
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1156:
	call	_ZdlPv@PLT
	movq	88(%r15), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%r15)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%r15)
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1055:
	leaq	16(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1080:
	leaq	16(%r15), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	80(%rbx), %rsi
	cmpq	%rsi, 72(%rbx)
	jne	.L1095
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	-8(%rcx), %rsi
	addq	$512, %rsi
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	80(%rbx), %rax
	cmpq	%rax, 72(%rbx)
	jne	.L1074
	jmp	.L1048
.L1164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7917:
	.size	_ZNK4node6worker7Message10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6worker7Message10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag:
.LFB10356:
	.cfi_startproc
	endbr64
	movq	%rsi, %r9
	subq	%rdi, %r9
	movq	%r9, %rax
	sarq	$5, %r9
	sarq	$3, %rax
	testq	%r9, %r9
	jle	.L1166
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	sete	%r10b
	salq	$5, %r9
	addq	%rdi, %r9
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1237:
	testq	%rcx, %rcx
	je	.L1168
	movq	(%rcx), %rax
	cmpq	%rax, (%r8)
	sete	%r8b
	testb	%r8b, %r8b
	jne	.L1235
.L1169:
	movq	8(%rdi), %r8
	leaq	8(%rdi), %rax
	testq	%r8, %r8
	je	.L1200
	testq	%rcx, %rcx
	je	.L1172
	movq	(%rcx), %r11
	cmpq	%r11, (%r8)
	sete	%r8b
.L1171:
	testb	%r8b, %r8b
	jne	.L1232
	movq	16(%rdi), %r8
	leaq	16(%rdi), %rax
	testq	%r8, %r8
	je	.L1197
	testq	%rcx, %rcx
	je	.L1176
	movq	(%rcx), %r11
	cmpq	%r11, (%r8)
	sete	%r8b
	testb	%r8b, %r8b
	jne	.L1232
.L1238:
	movq	24(%rdi), %r8
	leaq	24(%rdi), %rax
	testq	%r8, %r8
	je	.L1196
	testq	%rcx, %rcx
	je	.L1180
	movq	(%rcx), %r11
	cmpq	%r11, (%r8)
	sete	%r8b
.L1179:
	testb	%r8b, %r8b
	jne	.L1232
.L1180:
	addq	$32, %rdi
	cmpq	%rdi, %r9
	je	.L1236
.L1181:
	movq	(%rdi), %r8
	testq	%r8, %r8
	jne	.L1237
	movl	%r10d, %r8d
	testb	%r8b, %r8b
	je	.L1169
.L1235:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	movl	%r10d, %r8d
	testb	%r8b, %r8b
	je	.L1238
.L1232:
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	cmpq	$0, 8(%rdi)
	je	.L1239
.L1172:
	cmpq	$0, 16(%rdi)
	leaq	16(%rdi), %rax
	je	.L1197
.L1176:
	cmpq	$0, 24(%rdi)
	leaq	24(%rdi), %rax
	jne	.L1180
	.p2align 4,,10
	.p2align 3
.L1196:
	movl	%r10d, %r8d
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1200:
	movl	%r10d, %r8d
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1239:
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
.L1166:
	cmpq	$2, %rax
	je	.L1182
	cmpq	$3, %rax
	je	.L1183
	cmpq	$1, %rax
	je	.L1240
.L1185:
	movq	%rsi, %rax
	ret
.L1183:
	movq	(%rdx), %rax
	movq	(%rdi), %rcx
	testq	%rax, %rax
	sete	%dl
	testq	%rcx, %rcx
	je	.L1189
	testq	%rax, %rax
	je	.L1190
	movq	(%rax), %rdx
	cmpq	%rdx, (%rcx)
	sete	%dl
.L1189:
	testb	%dl, %dl
	jne	.L1235
.L1190:
	addq	$8, %rdi
	jmp	.L1186
.L1240:
	movq	(%rdx), %rax
.L1187:
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1241
	testq	%rax, %rax
	je	.L1185
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	sete	%al
.L1195:
	testb	%al, %al
	je	.L1185
	jmp	.L1235
.L1182:
	movq	(%rdx), %rax
.L1186:
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1242
	testq	%rax, %rax
	je	.L1193
	movq	(%rax), %rcx
	cmpq	%rcx, (%rdx)
	sete	%dl
.L1192:
	testb	%dl, %dl
	jne	.L1235
.L1193:
	addq	$8, %rdi
	jmp	.L1187
.L1241:
	testq	%rax, %rax
	sete	%al
	jmp	.L1195
.L1242:
	testq	%rax, %rax
	sete	%dl
	jmp	.L1192
	.cfi_endproc
.LFE10356:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag
	.section	.rodata._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_.str1.1,"aMS",@progbits,1
.LC29:
	.string	"basic_string::append"
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_:
.LFB10407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	subq	$8, %rsp
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L1248
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1249
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L1246:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1249:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L1246
.L1248:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10407:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB10475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1251
	testq	%rsi, %rsi
	je	.L1267
.L1251:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L1268
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L1254
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L1255:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1269
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1254:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1255
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1253:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1255
.L1267:
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10475:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB10684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1271
	testq	%r14, %r14
	je	.L1287
.L1271:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L1288
	cmpq	$1, %r13
	jne	.L1274
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L1275:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1289
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1274:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L1275
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L1273:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L1275
.L1287:
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10684:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB10690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1294
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L1292:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1294:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L1292
	.cfi_endproc
.LFE10690:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC30:
	.string	"(null)"
.LC31:
	.string	"lz"
.LC32:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB10131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L1296
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1296:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC31(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-184(%rbp), %r9
.L1297:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L1297
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L1298
	cmpb	$99, %dl
	jg	.L1299
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L1300
	cmpb	$88, %dl
	je	.L1301
	jmp	.L1298
.L1299:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1298
	leaq	.L1303(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L1303:
	.long	.L1302-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1302-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1302-.L1303
	.long	.L1305-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1302-.L1303
	.long	.L1298-.L1303
	.long	.L1302-.L1303
	.long	.L1298-.L1303
	.long	.L1298-.L1303
	.long	.L1302-.L1303
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L1300:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1337
	jmp	.L1334
.L1298:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1334
.L1337:
	call	_ZdlPv@PLT
	jmp	.L1334
.L1302:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L1317
	leaq	.LC30(%rip), %rsi
.L1317:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L1329
	jmp	.L1314
.L1301:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L1319
	leaq	.LC30(%rip), %rsi
.L1319:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1320
	call	_ZdlPv@PLT
.L1320:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1314
.L1329:
	call	_ZdlPv@PLT
	jmp	.L1314
.L1305:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC32(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L1322
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1322:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L1314:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1334:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1309
	call	_ZdlPv@PLT
.L1309:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1295
	call	_ZdlPv@PLT
.L1295:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1325
	call	__stack_chk_fail@PLT
.L1325:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10131:
	.size	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB9517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1339
	call	__stack_chk_fail@PLT
.L1339:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9517:
	.size	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_:
.LFB8760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1341
	call	_ZdlPv@PLT
.L1341:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1343
	call	__stack_chk_fail@PLT
.L1343:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8760:
	.size	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.section	.rodata._ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_.str1.1,"aMS",@progbits,1
.LC33:
	.string	" "
.LC34:
	.string	"\n"
.LC35:
	.string	"%s"
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB9791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-144(%rbp), %r14
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdi, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	call	*72(%rax)
	movq	%r13, %rsi
	leaq	.LC33(%rip), %rdx
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	-112(%rbp), %r13
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC34(%rip), %rdx
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	-80(%rbp), %rax
	movq	16(%rbx), %rdx
	movq	%rax, -184(%rbp)
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1346
	movq	stderr(%rip), %rdi
	leaq	-184(%rbp), %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
.L1346:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1347
	call	_ZdlPv@PLT
.L1347:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1348
	call	_ZdlPv@PLT
.L1348:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1349
	call	_ZdlPv@PLT
.L1349:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1345
	call	_ZdlPv@PLT
.L1345:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1351
	call	__stack_chk_fail@PLT
.L1351:
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9791:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.1
.LC36:
	.string	"Stop receiving messages"
	.section	.text.unlikely
	.align 2
.LCOLDB37:
	.text
.LHOTB37:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort4StopEv
	.type	_ZN4node6worker11MessagePort4StopEv, @function
_ZN4node6worker11MessagePort4StopEv:
.LFB8002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1355
.L1354:
	movb	$0, 96(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort4StopEv.cold, @function
_ZN4node6worker11MessagePort4StopEv.cold:
.LFSB8002:
.L1355:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leaq	.LC36(%rip), %rsi
	movq	%rdi, -8(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-8(%rbp), %rdi
	jmp	.L1354
	.cfi_endproc
.LFE8002:
	.text
	.size	_ZN4node6worker11MessagePort4StopEv, .-_ZN4node6worker11MessagePort4StopEv
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort4StopEv.cold, .-_ZN4node6worker11MessagePort4StopEv.cold
.LCOLDE37:
	.text
.LHOTE37:
	.section	.text.unlikely
	.align 2
.LCOLDB38:
	.text
.LHOTB38:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jg	.L1358
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1375
.L1360:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1376
	movq	8(%rbx), %r12
.L1362:
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1377
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1369
	cmpw	$1040, %cx
	jne	.L1364
.L1369:
	movq	23(%rdx), %rax
.L1366:
	testq	%rax, %rax
	je	.L1357
	cmpq	$0, 88(%rax)
	je	.L1357
	movq	16(%rax), %rcx
	movslq	32(%rax), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L1373
.L1368:
	movb	$0, 96(%rax)
.L1357:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1358:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1360
.L1375:
	leaq	_ZZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1376:
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1377:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1364:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1366
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB8004:
.L1373:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%rax, %rdi
	leaq	.LC36(%rip), %rsi
	movq	%rax, -24(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-24(%rbp), %rax
	jmp	.L1368
	.cfi_endproc
.LFE8004:
	.text
	.size	_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE38:
	.text
.LHOTE38:
	.section	.rodata.str1.1
.LC39:
	.string	"Start receiving messages"
	.section	.text.unlikely
	.align 2
.LCOLDB40:
	.text
.LHOTB40:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort5StartEv
	.type	_ZN4node6worker11MessagePort5StartEv, @function
_ZN4node6worker11MessagePort5StartEv:
.LFB8001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %rdx
	movq	%rdi, %rbx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1384
.L1379:
	movq	88(%rbx), %rax
	movb	$1, 96(%rbx)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%rbx), %rax
	leaq	48(%rax), %rdx
	cmpq	%rdx, 48(%rax)
	je	.L1380
	movl	72(%rbx), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1380
	leaq	104(%rbx), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L1386
.L1380:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L1386:
	.cfi_restore_state
	leaq	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort5StartEv.cold, @function
_ZN4node6worker11MessagePort5StartEv.cold:
.LFSB8001:
.L1384:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	.LC39(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1379
	.cfi_endproc
.LFE8001:
	.text
	.size	_ZN4node6worker11MessagePort5StartEv, .-_ZN4node6worker11MessagePort5StartEv
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort5StartEv.cold, .-_ZN4node6worker11MessagePort5StartEv.cold
.LCOLDE40:
	.text
.LHOTE40:
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"Adding message to incoming queue"
	.section	.text.unlikely
	.align 2
.LCOLDB42:
	.text
.LHOTB42:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE
	.type	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE, @function
_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE:
.LFB7938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movl	$136, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	48(%r12), %rsi
	movq	%rax, %rdi
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %rax
	movq	%rax, 16(%rdi)
	movq	8(%rbx), %rax
	movq	$0, 8(%rbx)
	movq	%rax, 24(%rdi)
	movq	16(%rbx), %rax
	movq	%rax, 32(%rdi)
	movq	24(%rbx), %rax
	movq	%rax, 40(%rdi)
	movq	32(%rbx), %rax
	movups	%xmm0, 24(%rbx)
	movq	%rax, 48(%rdi)
	movq	40(%rbx), %rax
	movq	%rax, 56(%rdi)
	movq	48(%rbx), %rax
	movups	%xmm0, 40(%rbx)
	movq	%rax, 64(%rdi)
	movq	56(%rbx), %rax
	movq	%rax, 72(%rdi)
	movq	64(%rbx), %rax
	movq	%rax, 80(%rdi)
	movq	72(%rbx), %rax
	movq	%rax, 88(%rdi)
	movq	80(%rbx), %rax
	movq	%rax, 96(%rdi)
	movq	88(%rbx), %rax
	movq	%rax, 104(%rdi)
	movq	96(%rbx), %rax
	movq	%rax, 112(%rdi)
	movq	104(%rbx), %rax
	movq	%rax, 120(%rdi)
	movq	112(%rbx), %rax
	movq	%rax, 128(%rdi)
	movups	%xmm0, 56(%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	72(%r12), %rdi
	addq	$1, 64(%r12)
	testq	%rdi, %rdi
	je	.L1388
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1396
.L1389:
	movl	72(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1388
	addq	$104, %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L1398
.L1388:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L1398:
	.cfi_restore_state
	leaq	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE.cold, @function
_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE.cold:
.LFSB7938:
.L1396:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	.LC41(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	72(%r12), %rdi
	jmp	.L1389
	.cfi_endproc
.LFE7938:
	.text
	.size	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE, .-_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE
	.section	.text.unlikely
	.size	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE.cold, .-_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE.cold
.LCOLDE42:
	.text
.LHOTE42:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker15MessagePortData11DisentangleEv
	.type	_ZN4node6worker15MessagePortData11DisentangleEv, @function
_ZN4node6worker15MessagePortData11DisentangleEv:
.LFB7943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movq	%rax, -312(%rbp)
	testq	%r13, %r13
	je	.L1400
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1401
	lock addl	$1, 8(%r13)
.L1400:
	movq	-312(%rbp), %rdi
	call	uv_mutex_lock@PLT
	movl	$56, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	movabsq	$4294967297, %rax
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%rbx), %r14
	movq	%rax, (%rbx)
	movq	%r14, %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1570
	movq	88(%r12), %rdi
	movq	%r14, %xmm0
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%r12)
	testq	%rdi, %rdi
	je	.L1404
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1405
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
	cmpl	$1, %edx
	je	.L1571
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	96(%r12), %rax
	movq	%rax, -344(%rbp)
	testq	%rax, %rax
	je	.L1410
	movq	$0, 96(%rax)
	movq	$0, 96(%r12)
.L1410:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %rbx
	movq	%r12, %rdi
	movq	$0, -296(%rbp)
	leaq	-304(%rbp), %rsi
	movq	%rbx, -304(%rbp)
	movq	$0, -288(%rbp)
	movups	%xmm0, -280(%rbp)
	movups	%xmm0, -264(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movups	%xmm0, -216(%rbp)
	movups	%xmm0, -200(%rbp)
	call	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %r12
	movq	%rbx, -304(%rbp)
	movq	%rax, -320(%rbp)
	cmpq	%r12, %rax
	jne	.L1421
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1574:
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r14)
	cmpl	$1, %edx
	je	.L1572
.L1415:
	addq	$48, %r12
	cmpq	%r12, -320(%rbp)
	je	.L1573
.L1421:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1412
	call	_ZdaPv@PLT
.L1412:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1413
	call	_ZdaPv@PLT
.L1413:
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1415
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	jne	.L1574
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %edx
	jne	.L1415
.L1572:
	movq	(%r14), %rdx
	movq	%r14, %rdi
	call	*16(%rdx)
	testq	%r15, %r15
	je	.L1419
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1420:
	cmpl	$1, %eax
	jne	.L1415
	movq	(%r14), %rax
	movq	%r14, %rdi
	addq	$48, %r12
	call	*24(%rax)
	cmpq	%r12, -320(%rbp)
	jne	.L1421
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	-208(%rbp), %r12
.L1411:
	testq	%r12, %r12
	je	.L1422
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1422:
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r12
	movq	%rax, -336(%rbp)
	cmpq	%r12, %rax
	je	.L1423
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1424
	movq	(%r14), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1425
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r14)
	movq	%rax, (%r14)
	jne	.L1469
	movq	%r14, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1428
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1429
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
	cmpl	$1, %edx
	je	.L1575
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	48(%r14), %rax
	leaq	48(%r14), %r15
	cmpq	%r15, %rax
	je	.L1435
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	%rax, %r9
	movq	(%rax), %rax
	movq	16(%r9), %rcx
	leaq	16(%r9), %rdi
	movq	%r9, -320(%rbp)
	movq	%rax, -328(%rbp)
	call	*(%rcx)
	movq	-320(%rbp), %r9
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	movq	-328(%rbp), %rax
	cmpq	%r15, %rax
	jne	.L1434
.L1435:
	leaq	8(%r14), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1424:
	addq	$8, %r12
	cmpq	%r12, -336(%rbp)
	jne	.L1436
	movq	-232(%rbp), %r12
.L1423:
	testq	%r12, %r12
	je	.L1437
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1437:
	movq	-248(%rbp), %r15
	movq	-256(%rbp), %r12
	cmpq	%r12, %r15
	je	.L1438
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1445
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L1576
	.p2align 4,,10
	.p2align 3
.L1447:
	addq	$16, %r12
	cmpq	%r12, %r15
	je	.L1444
.L1439:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1447
.L1576:
	lock subl	$1, 8(%rdi)
	jne	.L1447
	movq	(%rdi), %rdx
	movq	%rdi, -320(%rbp)
	call	*16(%rdx)
	movq	-320(%rbp), %rdi
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
	cmpl	$1, %edx
	jne	.L1447
	movq	(%rdi), %rdx
	addq	$16, %r12
	call	*24(%rdx)
	cmpq	%r12, %r15
	jne	.L1439
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	-256(%rbp), %r12
.L1438:
	testq	%r12, %r12
	je	.L1449
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1449:
	movq	-272(%rbp), %r12
	movq	-280(%rbp), %r15
	cmpq	%r15, %r12
	je	.L1450
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	(%r15), %rdi
	addq	$16, %r15
	call	free@PLT
	cmpq	%r15, %r12
	jne	.L1451
	movq	-280(%rbp), %r15
.L1450:
	testq	%r15, %r15
	je	.L1452
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1452:
	movq	-296(%rbp), %rdi
	call	free@PLT
	movq	-344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1453
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE
	movq	%rbx, -176(%rbp)
	movq	-80(%rbp), %r14
	movq	-72(%rbp), %rbx
	cmpq	%r14, %rbx
	jne	.L1464
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1579:
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r12)
	cmpl	$1, %edx
	je	.L1577
.L1458:
	addq	$48, %r14
	cmpq	%r14, %rbx
	je	.L1578
.L1464:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1455
	call	_ZdaPv@PLT
.L1455:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1456
	call	_ZdaPv@PLT
.L1456:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L1458
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	jne	.L1579
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %edx
	jne	.L1458
.L1577:
	movq	(%r12), %rdx
	movq	%r12, %rdi
	call	*16(%rdx)
	testq	%r15, %r15
	je	.L1462
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1463:
	cmpl	$1, %eax
	jne	.L1458
	movq	(%r12), %rax
	addq	$48, %r14
	movq	%r12, %rdi
	call	*24(%rax)
	cmpq	%r14, %rbx
	jne	.L1464
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	-80(%rbp), %r14
.L1454:
	testq	%r14, %r14
	je	.L1465
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1465:
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %r12
	movq	%rax, -328(%rbp)
	cmpq	%r12, %rax
	je	.L1466
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1467
	movq	(%r14), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1468
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r14)
	movq	%rax, (%r14)
	jne	.L1469
	movq	%r14, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r14), %rbx
	testq	%rbx, %rbx
	je	.L1471
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1472
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rbx)
	cmpl	$1, %edx
	je	.L1580
	.p2align 4,,10
	.p2align 3
.L1471:
	movq	48(%r14), %rbx
	leaq	48(%r14), %r15
	cmpq	%r15, %rbx
	je	.L1478
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	%rbx, %r9
	movq	(%rbx), %rbx
	movq	16(%r9), %rcx
	leaq	16(%r9), %rdi
	movq	%r9, -320(%rbp)
	call	*(%rcx)
	movq	-320(%rbp), %r9
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	cmpq	%r15, %rbx
	jne	.L1477
.L1478:
	leaq	8(%r14), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1467:
	addq	$8, %r12
	cmpq	%r12, -328(%rbp)
	jne	.L1479
	movq	-104(%rbp), %r12
.L1466:
	testq	%r12, %r12
	je	.L1480
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1480:
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1481
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1488
	movq	8(%r12), %r14
	testq	%r14, %r14
	jne	.L1581
	.p2align 4,,10
	.p2align 3
.L1490:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L1487
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1490
.L1581:
	lock subl	$1, 8(%r14)
	jne	.L1490
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r14)
	jne	.L1490
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	(%rdi), %rdx
	movq	%rdi, -320(%rbp)
	call	*16(%rdx)
	movq	-320(%rbp), %rdi
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	cmpl	$1, %edx
	jne	.L1442
	movq	(%rdi), %rdx
	call	*24(%rdx)
	.p2align 4,,10
	.p2align 3
.L1442:
	addq	$16, %r12
	cmpq	%r15, %r12
	je	.L1444
.L1445:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1442
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %edx
	jne	.L1442
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1429:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %edx
	jne	.L1428
.L1575:
	movq	(%rdi), %rdx
	movq	%rdi, -320(%rbp)
	call	*16(%rdx)
	testq	%r15, %r15
	movq	-320(%rbp), %rdi
	je	.L1432
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1433:
	cmpl	$1, %eax
	jne	.L1428
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	-128(%rbp), %r12
.L1481:
	testq	%r12, %r12
	je	.L1492
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1492:
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1493
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	(%r12), %rdi
	addq	$16, %r12
	call	free@PLT
	cmpq	%r12, %rbx
	jne	.L1494
	movq	-152(%rbp), %r12
.L1493:
	testq	%r12, %r12
	je	.L1495
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1495:
	movq	-168(%rbp), %rdi
	call	free@PLT
.L1453:
	movq	-312(%rbp), %rdi
	call	uv_mutex_unlock@PLT
	testq	%r13, %r13
	je	.L1399
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1498
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
.L1499:
	cmpl	$1, %edx
	jne	.L1399
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	call	*16(%rdx)
	testq	%r15, %r15
	je	.L1501
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1502:
	cmpl	$1, %eax
	je	.L1582
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1583
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1419:
	.cfi_restore_state
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1401:
	addl	$1, 8(%r13)
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1469:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1472:
	movl	8(%rbx), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rbx)
	cmpl	$1, %edx
	jne	.L1471
.L1580:
	movq	(%rbx), %rdx
	movq	%rbx, %rdi
	call	*16(%rdx)
	testq	%r15, %r15
	je	.L1475
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L1476:
	cmpl	$1, %eax
	jne	.L1471
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	cmpl	$1, %eax
	jne	.L1485
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1485:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L1487
.L1488:
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L1485
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L1485
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1462:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1405:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %edx
	jne	.L1404
.L1571:
	movq	(%rdi), %rdx
	movq	%rdi, -320(%rbp)
	call	*16(%rdx)
	testq	%r15, %r15
	movq	-320(%rbp), %rdi
	je	.L1408
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1409:
	cmpl	$1, %eax
	jne	.L1404
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1432:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1498:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1570:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1475:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1408:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1501:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1502
.L1583:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7943:
	.size	_ZN4node6worker15MessagePortData11DisentangleEv, .-_ZN4node6worker15MessagePortData11DisentangleEv
	.align 2
	.p2align 4
	.globl	_ZN4node6worker15MessagePortDataD2Ev
	.type	_ZN4node6worker15MessagePortDataD2Ev, @function
_ZN4node6worker15MessagePortDataD2Ev:
.LFB7934:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 72(%rdi)
	movq	%rax, (%rdi)
	jne	.L1597
	movq	%rdi, %rbx
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%rbx), %r12
	testq	%r12, %r12
	je	.L1587
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1588
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1598
	.p2align 4,,10
	.p2align 3
.L1587:
	movq	48(%rbx), %r12
	leaq	48(%rbx), %r14
	cmpq	%r14, %r12
	je	.L1593
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	16(%r13), %rax
	leaq	16(%r13), %rdi
	call	*(%rax)
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	cmpq	%r14, %r12
	jne	.L1594
.L1593:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L1588:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1587
.L1598:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1591
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1592:
	cmpl	$1, %eax
	jne	.L1587
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1597:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1591:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1592
	.cfi_endproc
.LFE7934:
	.size	_ZN4node6worker15MessagePortDataD2Ev, .-_ZN4node6worker15MessagePortDataD2Ev
	.globl	_ZN4node6worker15MessagePortDataD1Ev
	.set	_ZN4node6worker15MessagePortDataD1Ev,_ZN4node6worker15MessagePortDataD2Ev
	.section	.text._ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB9673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movabsq	$1152921504606846975, %rdx
	subq	$72, %rsp
	movq	(%rdi), %rcx
	movq	8(%rdi), %rax
	movq	%rdi, -112(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rax, -104(%rbp)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, -64(%rbp)
	movq	%rcx, -80(%rbp)
	cmpq	%rdx, %rax
	je	.L1639
	movq	%rsi, %r12
	subq	-80(%rbp), %r12
	testq	%rax, %rax
	je	.L1623
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -96(%rbp)
	cmpq	%rsi, %rax
	jbe	.L1640
	movabsq	$9223372036854775800, %rax
	movq	%rax, -96(%rbp)
.L1601:
	movq	-96(%rbp), %rdi
	call	_Znwm@PLT
	movq	%rax, -88(%rbp)
.L1622:
	movq	(%rbx), %rax
	movq	-88(%rbp), %rsi
	movq	$0, (%rbx)
	movq	-80(%rbp), %rbx
	movq	%rax, (%rsi,%r12)
	movq	%rsi, %r12
	cmpq	%rbx, -56(%rbp)
	je	.L1603
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	(%rbx), %rax
	movq	$0, (%rbx)
	movq	%rax, (%r12)
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1604
	movq	(%r15), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rcx
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1605
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r15)
	movq	%rax, (%r15)
	jne	.L1641
	movq	%r15, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r15), %r14
	testq	%r14, %r14
	je	.L1608
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1609
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L1642
	.p2align 4,,10
	.p2align 3
.L1608:
	movq	48(%r15), %r13
	leaq	48(%r15), %rax
	cmpq	%r13, %rax
	je	.L1615
	movq	%r12, -72(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	%r13, %r12
	movq	0(%r13), %r13
	movq	16(%r12), %rax
	leaq	16(%r12), %rdi
	call	*(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	jne	.L1614
	movq	-72(%rbp), %r12
.L1615:
	leaq	8(%r15), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1604:
	addq	$8, %rbx
	addq	$8, %r12
	cmpq	%rbx, -56(%rbp)
	jne	.L1616
.L1603:
	movq	-56(%rbp), %rdi
	movq	-104(%rbp), %rax
	leaq	8(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L1617
	subq	%rdi, %rax
	leaq	-8(%rax), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	addq	$1, %rcx
	testq	%rsi, %rsi
	je	.L1626
	movq	%rcx, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1619:
	movdqu	(%rdi,%rax), %xmm1
	movups	%xmm1, 8(%r12,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1619
	movq	%rcx, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r15
	leaq	(%rbx,%r15), %rdx
	addq	-56(%rbp), %r15
	movq	%r15, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L1620
.L1618:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1620:
	leaq	8(%rbx,%rsi), %rbx
.L1617:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1621
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1621:
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%rbx, %xmm2
	movq	%rax, %xmm0
	addq	-96(%rbp), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%rdx)
	movups	%xmm0, (%rdx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1640:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L1602
	movq	$0, -88(%rbp)
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1609:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L1608
.L1642:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1612
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1613:
	cmpl	$1, %eax
	jne	.L1608
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	$8, -96(%rbp)
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1641:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1612:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1613
.L1626:
	movq	%rbx, %rdx
	jmp	.L1618
.L1602:
	movq	-96(%rbp), %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -96(%rbp)
	jmp	.L1601
.L1639:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9673:
	.size	_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker7Message14AddMessagePortEOSt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS3_EE
	.type	_ZN4node6worker7Message14AddMessagePortEOSt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS3_EE, @function
_ZN4node6worker7Message14AddMessagePortEOSt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS3_EE:
.LFB7852:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movq	80(%rdi), %rsi
	cmpq	88(%rdi), %rsi
	je	.L1644
	movq	(%rdx), %rax
	addq	$8, %rsi
	movq	$0, (%rdx)
	movq	%rax, -8(%rsi)
	movq	%rsi, 80(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1644:
	addq	$72, %rdi
	jmp	_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.cfi_endproc
.LFE7852:
	.size	_ZN4node6worker7Message14AddMessagePortEOSt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS3_EE, .-_ZN4node6worker7Message14AddMessagePortEOSt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS3_EE
	.section	.text._ZN4node6worker7MessageD2Ev,"axG",@progbits,_ZN4node6worker7MessageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node6worker7MessageD2Ev
	.type	_ZN4node6worker7MessageD2Ev, @function
_ZN4node6worker7MessageD2Ev:
.LFB11663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	104(%rdi), %r14
	movq	96(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %r14
	jne	.L1658
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1721:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L1719
.L1652:
	addq	$48, %r13
	cmpq	%r13, %r14
	je	.L1720
.L1658:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1649
	call	_ZdaPv@PLT
.L1649:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1650
	call	_ZdaPv@PLT
.L1650:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1652
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	jne	.L1721
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %eax
	jne	.L1652
.L1719:
	movq	(%r12), %rax
	movq	%rdx, -56(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1656
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1657:
	cmpl	$1, %eax
	jne	.L1652
	movq	(%r12), %rax
	addq	$48, %r13
	movq	%r12, %rdi
	call	*24(%rax)
	cmpq	%r13, %r14
	jne	.L1658
	.p2align 4,,10
	.p2align 3
.L1720:
	movq	96(%rbx), %r13
.L1648:
	testq	%r13, %r13
	je	.L1659
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1659:
	movq	80(%rbx), %r13
	movq	72(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1660
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L1661
	movq	(%r15), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1662
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r15)
	movq	%rax, (%r15)
	jne	.L1722
	movq	%r15, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r15), %r14
	testq	%r14, %r14
	je	.L1665
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1666
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L1723
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	48(%r15), %rax
	leaq	48(%r15), %r14
	cmpq	%r14, %rax
	je	.L1672
	.p2align 4,,10
	.p2align 3
.L1671:
	movq	%rax, %r9
	movq	(%rax), %rax
	movq	16(%r9), %rcx
	leaq	16(%r9), %rdi
	movq	%r9, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	*(%rcx)
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	cmpq	%r14, %rax
	jne	.L1671
.L1672:
	leaq	8(%r15), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1661:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L1673
	movq	72(%rbx), %r12
.L1660:
	testq	%r12, %r12
	je	.L1674
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1674:
	movq	56(%rbx), %r14
	movq	48(%rbx), %r12
	cmpq	%r12, %r14
	je	.L1675
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1682
	movq	8(%r12), %r13
	testq	%r13, %r13
	jne	.L1724
	.p2align 4,,10
	.p2align 3
.L1684:
	addq	$16, %r12
	cmpq	%r12, %r14
	je	.L1681
.L1676:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1684
.L1724:
	lock subl	$1, 8(%r13)
	jne	.L1684
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L1684
	movq	0(%r13), %rax
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r12, %r14
	jne	.L1676
	.p2align 4,,10
	.p2align 3
.L1681:
	movq	48(%rbx), %r12
.L1675:
	testq	%r12, %r12
	je	.L1686
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1686:
	movq	32(%rbx), %r13
	movq	24(%rbx), %r12
	cmpq	%r12, %r13
	je	.L1687
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	(%r12), %rdi
	addq	$16, %r12
	call	free@PLT
	cmpq	%r12, %r13
	jne	.L1688
	movq	24(%rbx), %r12
.L1687:
	testq	%r12, %r12
	je	.L1689
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1689:
	movq	8(%rbx), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L1680:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L1679
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1679:
	addq	$16, %r12
	cmpq	%r12, %r14
	je	.L1681
.L1682:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1679
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1679
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1666:
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %eax
	jne	.L1665
.L1723:
	movq	(%r14), %rax
	movq	%rdx, -56(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1669
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1670:
	cmpl	$1, %eax
	jne	.L1665
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1656:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1669:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1722:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11663:
	.size	_ZN4node6worker7MessageD2Ev, .-_ZN4node6worker7MessageD2Ev
	.weak	_ZN4node6worker7MessageD1Ev
	.set	_ZN4node6worker7MessageD1Ev,_ZN4node6worker7MessageD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker15MessagePortDataD0Ev
	.type	_ZN4node6worker15MessagePortDataD0Ev, @function
_ZN4node6worker15MessagePortDataD0Ev:
.LFB7936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 72(%rdi)
	movq	%rax, (%rdi)
	jne	.L1738
	movq	%rdi, %r12
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r12), %r13
	testq	%r13, %r13
	je	.L1728
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1729
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1739
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	48(%r12), %rbx
	leaq	48(%r12), %r14
	cmpq	%r14, %rbx
	je	.L1734
	.p2align 4,,10
	.p2align 3
.L1735:
	movq	%rbx, %r13
	movq	(%rbx), %rbx
	movq	16(%r13), %rax
	leaq	16(%r13), %rdi
	call	*(%rax)
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	cmpq	%r14, %rbx
	jne	.L1735
.L1734:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1729:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1728
.L1739:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1732
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1733:
	cmpl	$1, %eax
	jne	.L1728
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1738:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1732:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1733
	.cfi_endproc
.LFE7936:
	.size	_ZN4node6worker15MessagePortDataD0Ev, .-_ZN4node6worker15MessagePortDataD0Ev
	.section	.text._ZN4node6worker7MessageD0Ev,"axG",@progbits,_ZN4node6worker7MessageD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node6worker7MessageD0Ev
	.type	_ZN4node6worker7MessageD0Ev, @function
_ZN4node6worker7MessageD0Ev:
.LFB11665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	104(%rdi), %rbx
	movq	96(%rdi), %r14
	movq	%rax, (%rdi)
	cmpq	%r14, %rbx
	jne	.L1751
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1814:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1812
.L1745:
	addq	$48, %r14
	cmpq	%r14, %rbx
	je	.L1813
.L1751:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1742
	call	_ZdaPv@PLT
.L1742:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1743
	call	_ZdaPv@PLT
.L1743:
	movq	8(%r14), %r13
	testq	%r13, %r13
	je	.L1745
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	jne	.L1814
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1745
.L1812:
	movq	0(%r13), %rax
	movq	%rdx, -56(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1749
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1750:
	cmpl	$1, %eax
	jne	.L1745
	movq	0(%r13), %rax
	addq	$48, %r14
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r14, %rbx
	jne	.L1751
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	96(%r12), %r14
.L1741:
	testq	%r14, %r14
	je	.L1752
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1752:
	movq	80(%r12), %rbx
	movq	72(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1753
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L1754
	movq	(%r15), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1755
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r15)
	movq	%rax, (%r15)
	jne	.L1815
	movq	%r15, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r15), %r14
	testq	%r14, %r14
	je	.L1758
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1759
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L1816
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	48(%r15), %rax
	leaq	48(%r15), %r14
	cmpq	%r14, %rax
	je	.L1765
	.p2align 4,,10
	.p2align 3
.L1764:
	movq	%rax, %r9
	movq	(%rax), %rax
	movq	16(%r9), %rcx
	leaq	16(%r9), %rdi
	movq	%r9, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	*(%rcx)
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	cmpq	%r14, %rax
	jne	.L1764
.L1765:
	leaq	8(%r15), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1754:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1766
	movq	72(%r12), %r13
.L1753:
	testq	%r13, %r13
	je	.L1767
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1767:
	movq	56(%r12), %rbx
	movq	48(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1768
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1775
	movq	8(%r13), %r14
	testq	%r14, %r14
	jne	.L1817
	.p2align 4,,10
	.p2align 3
.L1777:
	addq	$16, %r13
	cmpq	%r13, %rbx
	je	.L1774
.L1769:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1777
.L1817:
	lock subl	$1, 8(%r14)
	jne	.L1777
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r14)
	jne	.L1777
	movq	(%r14), %rax
	addq	$16, %r13
	movq	%r14, %rdi
	call	*24(%rax)
	cmpq	%r13, %rbx
	jne	.L1769
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	48(%r12), %r13
.L1768:
	testq	%r13, %r13
	je	.L1779
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1779:
	movq	32(%r12), %rbx
	movq	24(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1780
	.p2align 4,,10
	.p2align 3
.L1781:
	movq	0(%r13), %rdi
	addq	$16, %r13
	call	free@PLT
	cmpq	%r13, %rbx
	jne	.L1781
	movq	24(%r12), %r13
.L1780:
	testq	%r13, %r13
	je	.L1782
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1782:
	movq	8(%r12), %rdi
	call	free@PLT
	addq	$24, %rsp
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	cmpl	$1, %eax
	jne	.L1772
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1772:
	addq	$16, %r13
	cmpq	%r13, %rbx
	je	.L1774
.L1775:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L1772
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L1772
	jmp	.L1773
	.p2align 4,,10
	.p2align 3
.L1755:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1759:
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %eax
	jne	.L1758
.L1816:
	movq	(%r14), %rax
	movq	%rdx, -56(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-56(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1762
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1763:
	cmpl	$1, %eax
	jne	.L1758
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1749:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1762:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1815:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11665:
	.size	_ZN4node6worker7MessageD0Ev, .-_ZN4node6worker7MessageD0Ev
	.section	.rodata.str1.1
.LC43:
	.string	"MessagePort::OnClose()"
	.section	.text.unlikely
	.align 2
.LCOLDB44:
	.text
.LHOTB44:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort7OnCloseEv
	.type	_ZN4node6worker11MessagePort7OnCloseEv, @function
_ZN4node6worker11MessagePort7OnCloseEv:
.LFB7987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1822
.L1819:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1820
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1820:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%rbx), %r13
	movq	%r12, %rdi
	movq	$0, 72(%r13)
	movq	$0, 88(%rbx)
	call	uv_mutex_unlock@PLT
	movq	%r13, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort7OnCloseEv.cold, @function
_ZN4node6worker11MessagePort7OnCloseEv.cold:
.LFSB7987:
.L1822:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	.LC43(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1819
	.cfi_endproc
.LFE7987:
	.text
	.size	_ZN4node6worker11MessagePort7OnCloseEv, .-_ZN4node6worker11MessagePort7OnCloseEv
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort7OnCloseEv.cold, .-_ZN4node6worker11MessagePort7OnCloseEv.cold
.LCOLDE44:
	.text
.LHOTE44:
	.section	.text.unlikely
	.align 2
.LCOLDB45:
	.text
.LHOTB45:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1844
	movq	8(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1832
	cmpw	$1040, %cx
	jne	.L1826
.L1832:
	movq	23(%rdx), %rbx
.L1828:
	testq	%rbx, %rbx
	je	.L1824
	movq	88(%rbx), %rax
	testq	%rax, %rax
	je	.L1824
	movq	16(%rbx), %rcx
	movslq	32(%rbx), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L1842
.L1830:
	movb	$1, 96(%rbx)
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%rbx), %rax
	leaq	48(%rax), %rdx
	cmpq	%rdx, 48(%rax)
	je	.L1831
	movl	72(%rbx), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1831
	leaq	104(%rbx), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L1845
.L1831:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L1824:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1826:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1844:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1845:
	leaq	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB8003:
.L1842:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	.LC39(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	88(%rbx), %rax
	jmp	.L1830
	.cfi_endproc
.LFE8003:
	.text
	.size	_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node6worker11MessagePort5StartERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE45:
	.text
.LHOTE45:
	.section	.rodata._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC46:
	.string	"%d"
	.section	.text.unlikely._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1847
	leaq	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1847:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC31(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1848:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1848
	cmpb	$120, %dl
	jg	.L1849
	cmpb	$99, %dl
	jg	.L1850
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L1851
	cmpb	$88, %dl
	je	.L1852
	jmp	.L1849
.L1850:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1849
	leaq	.L1854(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1854:
	.long	.L1855-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1855-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1857-.L1854
	.long	.L1856-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1855-.L1854
	.long	.L1849-.L1854
	.long	.L1855-.L1854
	.long	.L1849-.L1854
	.long	.L1849-.L1854
	.long	.L1853-.L1854
	.section	.text.unlikely._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1851:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1858
	call	_ZdlPv@PLT
.L1858:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1878
	jmp	.L1860
.L1849:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1883
	call	_ZdlPv@PLT
	jmp	.L1883
.L1855:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC46(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L1880
.L1857:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L1865:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1865
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1880
.L1853:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1880:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1877
	jmp	.L1864
.L1852:
	movl	(%r8), %esi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1868
	call	_ZdlPv@PLT
.L1868:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1864
.L1877:
	call	_ZdlPv@PLT
	jmp	.L1864
.L1856:
	leaq	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1864:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1883:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1860
.L1878:
	call	_ZdlPv@PLT
.L1860:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1846
	call	_ZdlPv@PLT
.L1846:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1872
	call	__stack_chk_fail@PLT
.L1872:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11561:
	.size	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1885
	call	__stack_chk_fail@PLT
.L1885:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11415:
	.size	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_:
.LFB11204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1887
	call	_ZdlPv@PLT
.L1887:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1889
	call	__stack_chk_fail@PLT
.L1889:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11204:
	.size	_ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB9806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	call	*72(%rax)
	movq	%r14, %rsi
	leaq	.LC33(%rip), %rdx
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	-128(%rbp), %r14
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC34(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1892
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJiEEEvP8_IO_FILEPKcDpOT_
.L1892:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1893
	call	_ZdlPv@PLT
.L1893:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1894
	call	_ZdlPv@PLT
.L1894:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1895
	call	_ZdlPv@PLT
.L1895:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1891
	call	_ZdlPv@PLT
.L1891:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1897
	call	__stack_chk_fail@PLT
.L1897:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9806:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"Closing message port, data set = %d"
	.section	.text.unlikely
	.align 2
.LCOLDB48:
	.text
.LHOTB48:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE:
.LFB7968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	16(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	88(%rdi), %rax
	testq	%rax, %rax
	setne	%dl
	movl	%edx, -44(%rbp)
	movslq	32(%rdi), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L1904
.L1900:
	testq	%rax, %rax
	je	.L1901
	leaq	8(%rax), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L1899:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1906
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1901:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	jmp	.L1899
.L1906:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE.cold, @function
_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE.cold:
.LFSB7968:
.L1904:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	-44(%rbp), %rdx
	leaq	.LC47(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	88(%r12), %rax
	jmp	.L1900
	.cfi_endproc
.LFE7968:
	.text
	.size	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE, .-_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE.cold, .-_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE.cold
.LCOLDE48:
	.text
.LHOTE48:
	.section	.rodata.str1.1
.LC49:
	.string	"Created message port"
.LC50:
	.string	"emitMessage"
	.section	.text.unlikely
	.align 2
.LCOLDB51:
	.text
.LHOTB51:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE
	.type	_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE, @function
_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE:
.LFB7964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	104(%rdi), %r9
	movl	$19, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movq	%r9, %rcx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movq	%r15, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -72(%rbp)
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node6worker11MessagePortE(%rip), %rax
	movl	$104, %edi
	movq	%rax, (%r12)
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	8(%rbx), %rdi
	call	uv_mutex_init@PLT
	movq	-72(%rbp), %r9
	testl	%eax, %eax
	jne	.L1909
	leaq	48(%rbx), %rax
	movq	$0, 64(%rbx)
	movl	$56, %edi
	movq	%rax, %xmm0
	movq	%r12, 72(%rbx)
	movq	$0, 80(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdx
	movabsq	$4294967297, %rax
	movq	%rax, 8(%rdx)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	16(%rdx), %rcx
	movq	%rax, (%rdx)
	movq	%rcx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1909
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rcx
	movq	$0, 96(%rbx)
	movq	360(%r13), %rax
	movq	-88(%rbp), %r9
	movq	%rbx, 88(%r12)
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	movb	$0, 96(%r12)
	leaq	_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEENUlP10uv_async_sE_4_FUNESB_(%rip), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%r9, %rsi
	movq	$0, 232(%r12)
	movups	%xmm0, 80(%rbx)
	movq	2360(%rax), %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L1948
	movq	360(%r13), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	144(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1913
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L1928
.L1929:
	movq	%r14, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1913
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC50(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1949
.L1927:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1913
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L1950
	movq	232(%r12), %rdi
	movq	352(%r13), %r13
	testq	%rdi, %rdi
	je	.L1917
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 232(%r12)
.L1917:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	16(%r12), %rdx
	movq	%rax, 232(%r12)
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1943
.L1907:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1951
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1928:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	jne	.L1929
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	88(%r12), %rax
	xorl	%edx, %edx
	movq	16(%r12), %rcx
	testq	%rax, %rax
	setne	%dl
	movl	%edx, -60(%rbp)
	movslq	32(%r12), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L1952
.L1923:
	testq	%rax, %rax
	je	.L1920
	leaq	8(%rax), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L1909:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1948:
	leaq	_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1920:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	%rdx, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1950:
	leaq	_ZZN4node6worker12_GLOBAL__N_122GetEmitMessageFunctionEN2v85LocalINS2_7ContextEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1951:
	call	__stack_chk_fail@PLT
.L1952:
	jmp	.L1922
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE.cold, @function
_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE.cold:
.LFSB7964:
.L1943:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC49(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1907
.L1922:
	leaq	-60(%rbp), %rdx
	leaq	.LC47(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	88(%r12), %rax
	jmp	.L1923
	.cfi_endproc
.LFE7964:
	.text
	.size	_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE, .-_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE.cold, .-_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE.cold
.LCOLDE51:
	.text
.LHOTE51:
	.globl	_ZN4node6worker11MessagePortC1EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE
	.set	_ZN4node6worker11MessagePortC1EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE,_ZN4node6worker11MessagePortC2EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE
	.type	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE, @function
_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE:
.LFB7970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZN2v87Context5EnterEv@PLT
	movq	%r14, %rdi
	call	_ZN4node6worker33GetMessagePortConstructorTemplateEPNS_11EnvironmentE
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1956
	movl	$240, %edi
	movq	%rax, %r15
	call	_Znwm@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN4node6worker11MessagePortC1EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEE
	movl	72(%r12), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1975
	cmpq	$0, (%rbx)
	je	.L1956
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1976
	leaq	8(%rdi), %r14
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%r12), %r15
	movq	%r14, %rdi
	movq	$0, 72(%r15)
	movq	$0, 88(%r12)
	call	uv_mutex_unlock@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movq	(%rbx), %rax
	movq	88(%r12), %rdi
	movq	$0, (%rbx)
	movq	%rax, 88(%r12)
	testq	%rdi, %rdi
	je	.L1958
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	88(%r12), %rax
.L1958:
	leaq	8(%rax), %r14
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%r12), %rax
	movq	%r12, 72(%rax)
	movl	72(%r12), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1959
	leaq	104(%r12), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L1977
.L1959:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
.L1956:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1975:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1977:
	leaq	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1976:
	leaq	_ZZN4node6worker11MessagePort6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7970:
	.size	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE, .-_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE
	.section	.rodata.str1.1
.LC52:
	.string	"ERR_INVALID_ARG_TYPE"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"First argument needs to be a MessagePort instance"
	.section	.rodata.str1.1
.LC54:
	.string	"Invalid context argument"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2021
	cmpw	$1040, %cx
	jne	.L1979
.L2021:
	movq	23(%rdx), %r12
.L1981:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1982
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1983:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1990
	movl	16(%rbx), %edx
	movq	3184(%r12), %rdi
	testl	%edx, %edx
	jle	.L2058
	movq	8(%rbx), %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1990
.L1989:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1996
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L1997:
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2059
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2022
	cmpw	$1040, %cx
	jne	.L1999
.L2022:
	movq	23(%rdx), %r14
.L2001:
	testq	%r14, %r14
	je	.L2060
	cmpl	$1, 16(%rbx)
	jg	.L2003
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2005
.L2068:
	leaq	-64(%rbp), %r15
	movq	%r12, %rdi
	movq	%r13, -64(%rbp)
	movq	%r15, %rsi
	call	_ZN4node10contextify17ContextifyContext30ContextFromContextifiedSandboxEPNS_11EnvironmentERKN2v85LocalINS4_6ObjectEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2005
	movq	88(%r14), %rdx
	testq	%rdx, %rdx
	je	.L2006
	movl	72(%r14), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L2020
	leaq	8(%rdx), %rdi
	movq	%rdi, -80(%rbp)
	call	uv_mutex_lock@PLT
	movq	88(%r14), %rdx
	movq	-80(%rbp), %rdi
	movq	$0, 72(%rdx)
	movq	$0, 88(%r14)
	movq	%rdx, -72(%rbp)
	call	uv_mutex_unlock@PLT
	movq	-72(%rbp), %rdx
.L2006:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2013
	movzbl	11(%r14), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L2013
	movq	0(%r13), %rax
	movq	(%r14), %rsi
	movq	%rdx, -72(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r14
.L2013:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-72(%rbp), %rdx
	movq	8(%r13), %rsi
	movq	%rdx, -64(%rbp)
	testq	%rsi, %rsi
	je	.L2014
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L2014
	movq	0(%r13), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L2014:
	movq	%r12, %rdi
	movq	%r15, %rdx
	call	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE
	movq	-64(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2015
	movq	(%rdi), %rax
	call	*8(%rax)
.L2015:
	testq	%r12, %r12
	je	.L2016
	movq	8(%r12), %rax
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L2017
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L2061
.L2018:
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L2016:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
.L1978:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2062
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2058:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L1989
.L1990:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2063
.L1986:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
.L2057:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2064
.L2009:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2065
.L2010:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2066
.L2011:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2067
.L2012:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L1982:
	movq	8(%rbx), %rdi
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	8(%rbx), %r13
	jmp	.L1997
	.p2align 4,,10
	.p2align 3
.L2003:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L2068
.L2005:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2069
.L2008:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC54(%rip), %rsi
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L1979:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2067:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2066:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2011
	.p2align 4,,10
	.p2align 3
.L2065:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2020:
	xorl	%edx, %edx
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L2059:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2060:
	leaq	_ZZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2063:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L1999:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2061:
	movq	16(%r12), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	jne	.L2018
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2069:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2008
.L2062:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8007:
	.size	_ZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC55:
	.string	"ERR_CONSTRUCT_CALL_REQUIRED"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"Cannot call constructor without `new`"
	.section	.text.unlikely
.LCOLDB57:
	.text
.LHOTB57:
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2095
	cmpw	$1040, %cx
	jne	.L2071
.L2095:
	movq	23(%rdx), %r13
.L2073:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2074
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2113
.L2074:
	movq	8(%rbx), %rax
	leaq	-64(%rbp), %r15
	leaq	8(%rax), %rdi
	call	_ZN2v86Object15CreationContextEv@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	$0, -64(%rbp)
	call	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE
	movq	-64(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L2081
	movq	(%rdi), %rax
	call	*8(%rax)
.L2081:
	testq	%r14, %r14
	je	.L2092
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	$0, -64(%rbp)
	call	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE
	movq	-64(%rbp), %rdi
	movq	%rax, -72(%rbp)
	testq	%rdi, %rdi
	je	.L2083
	movq	(%rdi), %rax
	call	*8(%rax)
.L2083:
	cmpq	$0, -72(%rbp)
	je	.L2114
	movq	-72(%rbp), %rax
	movq	88(%r14), %rdi
	movq	88(%rax), %rsi
	call	_ZN4node6worker15MessagePortData8EntangleEPS1_S2_
	movq	8(%rbx), %rax
	movq	8(%r14), %rcx
	leaq	8(%rax), %r15
	testq	%rcx, %rcx
	je	.L2089
	movzbl	11(%rcx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L2115
.L2089:
	movq	360(%r13), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	1368(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2116
.L2090:
	movq	-72(%rbp), %rax
	movq	8(%rbx), %r15
	movq	8(%rax), %rcx
	addq	$8, %r15
	testq	%rcx, %rcx
	je	.L2091
	movzbl	11(%rcx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L2117
.L2091:
	movq	360(%r13), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	1376(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2118
.L2092:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
.L2070:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2119
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2113:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L2074
	movq	352(%r13), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2120
.L2075:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC56(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2121
.L2076:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2122
.L2077:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2123
.L2078:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2124
.L2079:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2070
	.p2align 4,,10
	.p2align 3
.L2114:
	movq	(%r14), %rax
	leaq	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2085
	movq	88(%r14), %rax
	xorl	%edx, %edx
	movq	16(%r14), %rcx
	testq	%rax, %rax
	setne	%dl
	movl	%edx, -64(%rbp)
	movslq	32(%r14), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L2111
.L2086:
	testq	%rax, %rax
	je	.L2087
	leaq	8(%rax), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2071:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2115:
	movq	16(%r14), %rax
	movq	(%rcx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	-72(%rbp), %rax
	movq	(%rcx), %rsi
	movq	16(%rax), %rax
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2118:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2085:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2092
.L2124:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2079
.L2123:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2078
.L2122:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2077
.L2121:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L2076
.L2120:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2075
	.p2align 4,,10
	.p2align 3
.L2087:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2116:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2090
.L2119:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB8037:
.L2111:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r15, %rdx
	leaq	.LC47(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	88(%r14), %rax
	jmp	.L2086
	.cfi_endproc
.LFE8037:
	.text
	.size	_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node6worker12_GLOBAL__N_1L14MessageChannelERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE57:
	.text
.LHOTE57:
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text.unlikely
	.align 2
.LCOLDB59:
	.text
.LHOTB59:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE
	.type	_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE, @function
_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE:
.LFB7838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -264(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	je	.L2241
	leaq	-128(%rbp), %rax
	movq	352(%rsi), %rsi
	movq	%rdi, %r13
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-256(%rbp), %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	80(%r13), %rdx
	movq	72(%r13), %rbx
	movq	%rdx, %r12
	subq	%rbx, %r12
	movq	%r12, %rax
	sarq	$3, %rax
	testq	%r12, %r12
	js	.L2242
	movq	$0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -224(%rbp)
	testq	%rax, %rax
	je	.L2128
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r12), %rbx
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	movq	%rbx, -208(%rbp)
	call	memset@PLT
	movq	80(%r13), %rdx
	movq	%rax, %rdi
	movq	%rbx, %rax
	movq	72(%r13), %rbx
.L2197:
	movq	%rax, -216(%rbp)
	cmpq	%rbx, %rdx
	je	.L2129
	movq	%r13, -280(%rbp)
	leaq	-96(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$0, -248(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L2151:
	salq	$3, %r12
	movq	-256(%rbp), %rsi
	addq	%r12, %rax
	leaq	(%rdi,%r12), %rbx
	movq	-264(%rbp), %rdi
	movq	(%rax), %rdx
	movq	$0, (%rax)
	movq	%rdx, -96(%rbp)
	movq	-272(%rbp), %rdx
	call	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE
	movq	-96(%rbp), %r15
	movq	%rax, (%rbx)
	testq	%r15, %r15
	je	.L2130
	movq	(%r15), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2131
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r15)
	movq	%rax, (%r15)
	jne	.L2243
	movq	%r15, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r15), %r13
	testq	%r13, %r13
	je	.L2134
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2135
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L2244
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	48(%r15), %r13
	leaq	48(%r15), %rbx
	cmpq	%rbx, %r13
	je	.L2141
	.p2align 4,,10
	.p2align 3
.L2140:
	movq	%r13, %r14
	movq	0(%r13), %r13
	movq	16(%r14), %rax
	leaq	16(%r14), %rdi
	call	*(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	jne	.L2140
.L2141:
	leaq	8(%r15), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2130:
	movq	-224(%rbp), %rdi
	cmpq	$0, (%rdi,%r12)
	je	.L2245
.L2142:
	movq	-280(%rbp), %rcx
	addl	$1, -248(%rbp)
	movl	-248(%rbp), %r12d
	movq	80(%rcx), %rdx
	movq	72(%rcx), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %r12
	jb	.L2151
	movq	-280(%rbp), %r13
	movq	%rax, %rbx
	cmpq	%rdx, %rax
	je	.L2129
	movq	%rax, %r12
	movq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2152
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r14
	jne	.L2155
.L2153:
	movq	%rbx, 80(%r13)
.L2129:
	pxor	%xmm0, %xmm0
	movq	48(%r13), %r14
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	cmpq	%r14, 56(%r13)
	je	.L2156
	movq	-264(%rbp), %r15
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movabsq	$1152921504606846975, %rbx
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2235:
	movq	-184(%rbp), %r14
	cmpq	-176(%rbp), %r14
	je	.L2246
	movq	%rdx, (%r14)
	addq	$8, %r14
	movq	%r14, -184(%rbp)
.L2159:
	movq	56(%r13), %rsi
	movq	48(%r13), %r14
	leal	1(%r12), %eax
	movq	%rax, %r12
	movq	%rsi, %rdx
	subq	%r14, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rax
	jnb	.L2247
.L2169:
	salq	$4, %rax
	movq	-256(%rbp), %rdx
	movq	%r15, %rsi
	movq	(%r14,%rax), %rdi
	call	_ZN4node6worker25SharedArrayBufferMetadata20GetSharedArrayBufferEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L2235
	xorl	%r13d, %r13d
.L2192:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2190
	call	_ZdlPv@PLT
.L2190:
	movq	-224(%rbp), %rdi
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	%r15, %rdi
	call	*%rax
	movq	-224(%rbp), %rdi
	cmpq	$0, (%rdi,%r12)
	jne	.L2142
	.p2align 4,,10
	.p2align 3
.L2245:
	movq	-216(%rbp), %r12
	cmpq	%r12, %rdi
	je	.L2143
	movq	%rdi, %r13
	leaq	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE(%rip), %r14
	jmp	.L2149
	.p2align 4,,10
	.p2align 3
.L2249:
	leaq	8(%rax), %rbx
	movq	%rbx, %rdi
	call	uv_mutex_lock@PLT
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rbx, %rdi
	call	uv_mutex_unlock@PLT
.L2144:
	addq	$8, %r13
	cmpq	%r13, %r12
	je	.L2248
.L2149:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L2144
	movq	(%r15), %rax
	movq	80(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2145
	movq	88(%r15), %rax
	xorl	%edx, %edx
	movq	16(%r15), %rcx
	testq	%rax, %rax
	setne	%dl
	movl	%edx, -96(%rbp)
	movslq	32(%r15), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L2234
.L2146:
	testq	%rax, %rax
	jne	.L2249
	xorl	%esi, %esi
	movq	%r15, %rdi
	addq	$8, %r13
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	cmpq	%r13, %r12
	jne	.L2149
	.p2align 4,,10
	.p2align 3
.L2248:
	movq	-224(%rbp), %rdi
.L2143:
	xorl	%r13d, %r13d
.L2150:
	testq	%rdi, %rdi
	je	.L2191
	call	_ZdlPv@PLT
.L2191:
	movq	-256(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-288(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2250
	addq	$264, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2135:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L2134
.L2244:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2138
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2139:
	cmpl	$1, %eax
	jne	.L2134
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L2152:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L2155
	jmp	.L2153
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	-192(%rbp), %r8
	movq	%r14, %rsi
	subq	%r8, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	je	.L2251
	testq	%rax, %rax
	je	.L2199
	movabsq	$9223372036854775800, %r9
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2252
.L2161:
	movq	%r9, %rdi
	movq	%rsi, -296(%rbp)
	movq	%r8, -280(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%r9, -248(%rbp)
	call	_Znwm@PLT
	movq	-248(%rbp), %r9
	movq	-272(%rbp), %rdx
	movq	-280(%rbp), %r8
	movq	-296(%rbp), %rsi
	leaq	8(%rax), %r10
	addq	%rax, %r9
.L2162:
	movq	%rdx, (%rax,%rsi)
	cmpq	%r8, %r14
	je	.L2163
	leaq	-8(%r14), %rdi
	leaq	15(%rax), %rdx
	subq	%r8, %rdi
	subq	%r8, %rdx
	movq	%rdi, %r10
	shrq	$3, %r10
	cmpq	$30, %rdx
	jbe	.L2202
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %r10
	je	.L2202
	addq	$1, %r10
	xorl	%edx, %edx
	movq	%r10, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2165:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L2165
	movq	%r10, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rsi
	leaq	(%r8,%rsi), %rdx
	addq	%rax, %rsi
	cmpq	%r10, %r11
	je	.L2167
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L2167:
	leaq	16(%rax,%rdi), %r10
.L2163:
	testq	%r8, %r8
	je	.L2168
	movq	%r8, %rdi
	movq	%r10, -280(%rbp)
	movq	%r9, -272(%rbp)
	movq	%rax, -248(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r10
	movq	-272(%rbp), %r9
	movq	-248(%rbp), %rax
.L2168:
	movq	%rax, %xmm0
	movq	%r10, %xmm2
	movq	%r9, -176(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -192(%rbp)
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2243:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2145:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2138:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2139
	.p2align 4,,10
	.p2align 3
.L2252:
	testq	%rdi, %rdi
	jne	.L2253
	movl	$8, %r10d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2175:
	movq	%r14, 56(%r13)
.L2156:
	movq	-264(%rbp), %r15
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_120DeserializerDelegateE(%rip), %rax
	movq	16(%r13), %rcx
	leaq	-96(%rbp), %r8
	movq	%rax, -96(%rbp)
	leaq	-224(%rbp), %rax
	movq	8(%r13), %rdx
	leaq	-232(%rbp), %r12
	movq	%rax, -80(%rbp)
	movq	352(%r15), %rsi
	movq	%r12, %rdi
	leaq	-192(%rbp), %rax
	movq	%rax, -72(%rbp)
	leaq	96(%r13), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v817ValueDeserializerC1EPNS_7IsolateEPKhmPNS0_8DelegateE@PLT
	movq	%r12, -88(%rbp)
	movq	24(%r13), %r14
	cmpq	%r14, 32(%r13)
	je	.L2180
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	2376(%rdi), %rdi
	leaq	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm(%rip), %rsi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2184
	lock addq	%rdx, 16(%rdi)
.L2185:
	movq	24(%r13), %rax
	movq	352(%r15), %rdi
	addq	%rcx, %rax
	xorl	%ecx, %ecx
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	$0, (%rax)
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v817ValueDeserializer19TransferArrayBufferEjNS_5LocalINS_11ArrayBufferEEE@PLT
.L2183:
	movq	32(%r13), %rcx
	movq	24(%r13), %r14
	leal	1(%rbx), %eax
	movq	%rax, %rbx
	movq	%rcx, %rdx
	subq	%r14, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rax
	jnb	.L2254
.L2186:
	movq	360(%r15), %rdi
	salq	$4, %rax
	addq	%rax, %r14
	movq	%rax, %rcx
	cmpb	$0, 2384(%rdi)
	movq	8(%r14), %rdx
	jne	.L2181
	movq	2368(%rdi), %rdi
	movq	%rax, -264(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, -248(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-248(%rbp), %rdx
	movq	-264(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L2255
	movl	%edx, %esi
	movq	%rcx, -264(%rbp)
	call	uv_buf_init@PLT
	movq	-264(%rbp), %rcx
	movq	%rax, %r14
	movq	24(%r13), %rax
	movq	%rdx, -248(%rbp)
	movq	%r14, %rdi
	addq	%rcx, %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	call	memcpy@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-248(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	352(%r15), %rdi
	movq	%rdx, -144(%rbp)
	movq	%r8, %rdx
	movq	%rax, -152(%rbp)
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v817ValueDeserializer19TransferArrayBufferEjNS_5LocalINS_11ArrayBufferEEE@PLT
	movq	-144(%rbp), %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-152(%rbp), %r14
	movq	%r8, -248(%rbp)
	call	uv_buf_init@PLT
	testq	%r14, %r14
	movq	-248(%rbp), %r8
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	je	.L2183
	movq	360(%r15), %rax
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2199:
	movl	$8, %r9d
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2184:
	movq	%rcx, -248(%rbp)
	movq	(%r14), %rsi
	call	*%rax
	movq	-248(%rbp), %rcx
	jmp	.L2185
	.p2align 4,,10
	.p2align 3
.L2247:
	movq	%rsi, %r15
	cmpq	%rsi, %r14
	je	.L2156
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%r14, %rbx
	jne	.L2170
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2178:
	addq	$16, %rbx
	cmpq	%rbx, %r15
	je	.L2175
.L2170:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L2178
	lock subl	$1, 8(%r12)
	jne	.L2178
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r12)
	jne	.L2178
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L2178
.L2174:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	cmpl	$1, %eax
	jne	.L2173
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2173:
	addq	$16, %rbx
	cmpq	%rbx, %r15
	je	.L2175
.L2176:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L2173
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L2173
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L2254:
	movq	%rcx, %r15
	cmpq	%rcx, %r14
	je	.L2180
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	call	free@PLT
	cmpq	%rbx, %r15
	jne	.L2187
	movq	%r14, 32(%r13)
.L2180:
	movq	-256(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v817ValueDeserializer10ReadHeaderENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	jne	.L2256
.L2189:
	movq	%r12, %rdi
	call	_ZN2v817ValueDeserializerD1Ev@PLT
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2241:
	leaq	_ZZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	%r12, -208(%rbp)
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L2197
.L2202:
	movq	%rax, %rsi
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	(%rdx), %rcx
	addq	$8, %rdx
	addq	$8, %rsi
	movq	%rcx, -8(%rsi)
	cmpq	%rdx, %r14
	jne	.L2164
	jmp	.L2167
.L2256:
	movq	-256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v817ValueDeserializer9ReadValueENS_5LocalINS_7ContextEEE@PLT
	movq	-288(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
	jmp	.L2189
.L2255:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2253:
	cmpq	%rbx, %rdi
	cmova	%rbx, %rdi
	movq	%rdi, %r9
	salq	$3, %r9
	jmp	.L2161
.L2250:
	call	__stack_chk_fail@PLT
.L2251:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2242:
	leaq	.LC58(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE.cold, @function
_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE.cold:
.LFSB7838:
.L2234:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-272(%rbp), %rdx
	leaq	.LC47(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	88(%r15), %rax
	jmp	.L2146
	.cfi_endproc
.LFE7838:
	.text
	.size	_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE, .-_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE
	.section	.text.unlikely
	.size	_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE.cold, .-_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE.cold
.LCOLDE59:
	.text
.LHOTE59:
	.section	.rodata.str1.1
.LC60:
	.string	"MessagePort has message"
	.section	.text.unlikely
	.align 2
.LCOLDB61:
	.text
.LHOTB61:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb
	.type	_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb, @function
_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb:
.LFB7971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%rsi, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %rax
	movups	%xmm0, -152(%rbp)
	movq	%rax, -176(%rbp)
	movq	88(%rdi), %rax
	movq	$0, -168(%rbp)
	leaq	8(%rax), %r12
	movups	%xmm0, -136(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -120(%rbp)
	movq	$0, -160(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2410
.L2258:
	movq	88(%rbx), %rdx
	movq	48(%rdx), %r14
	addq	$48, %rdx
	cmpb	$0, 96(%rbx)
	jne	.L2420
	testb	%r13b, %r13b
	jne	.L2262
.L2420:
	cmpq	%rdx, %r14
	je	.L2260
.L2261:
	movq	-168(%rbp), %rdi
	call	free@PLT
	movq	24(%r14), %rdx
	pxor	%xmm0, %xmm0
	movdqu	40(%r14), %xmm1
	movq	-152(%rbp), %r13
	movq	-144(%rbp), %rax
	movq	$0, 24(%r14)
	movq	%rdx, -168(%rbp)
	movq	32(%r14), %rdx
	movq	%rax, -200(%rbp)
	movq	%r13, %r15
	movq	%rdx, -160(%rbp)
	movq	56(%r14), %rdx
	movups	%xmm1, -152(%rbp)
	movq	%rdx, -136(%rbp)
	movq	$0, 56(%r14)
	movups	%xmm0, 40(%r14)
	cmpq	%rax, %r13
	je	.L2267
	.p2align 4,,10
	.p2align 3
.L2264:
	movq	(%r15), %rdi
	addq	$16, %r15
	call	free@PLT
	cmpq	%r15, -200(%rbp)
	jne	.L2264
.L2267:
	testq	%r13, %r13
	je	.L2266
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2266:
	movdqu	64(%r14), %xmm2
	movq	80(%r14), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 80(%r14)
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	movq	%rcx, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	movups	%xmm0, 64(%r14)
	cmpq	%rdx, %r8
	je	.L2281
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%r12, -200(%rbp)
	movq	%r8, %r15
	movq	%r8, %r12
	je	.L2423
	movq	%rbx, -216(%rbp)
	movq	%rdx, %rbx
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2279:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L2421
.L2271:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2279
	lock subl	$1, 8(%r13)
	jne	.L2279
	movq	0(%r13), %rcx
	movq	%r13, %rdi
	call	*16(%rcx)
	lock subl	$1, 12(%r13)
	jne	.L2279
	movq	0(%r13), %rcx
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rcx)
	cmpq	%r12, %rbx
	jne	.L2271
	.p2align 4,,10
	.p2align 3
.L2421:
	movq	-200(%rbp), %r12
	movq	-216(%rbp), %rbx
	movq	%r15, %r8
.L2281:
	testq	%r8, %r8
	je	.L2270
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L2270:
	movq	-104(%rbp), %rax
	movdqu	88(%r14), %xmm3
	pxor	%xmm0, %xmm0
	movq	-96(%rbp), %rsi
	movq	104(%r14), %rdx
	movq	$0, 104(%r14)
	movq	%rax, -216(%rbp)
	movq	%rax, %r13
	movq	%rdx, -88(%rbp)
	movq	%rsi, -200(%rbp)
	movups	%xmm3, -104(%rbp)
	movups	%xmm0, 88(%r14)
	cmpq	%rsi, %rax
	je	.L2297
	movq	%r12, -224(%rbp)
	movq	%r14, -232(%rbp)
	movq	%rbx, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L2282:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L2285
	movq	(%r15), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2286
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r15)
	movq	%rax, (%r15)
	jne	.L2333
	movq	%r15, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r15), %r12
	testq	%r12, %r12
	je	.L2289
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2290
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L2424
	.p2align 4,,10
	.p2align 3
.L2289:
	movq	48(%r15), %r12
	leaq	48(%r15), %rbx
	cmpq	%rbx, %r12
	je	.L2296
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	16(%r14), %rax
	leaq	16(%r14), %rdi
	call	*(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	jne	.L2295
.L2296:
	leaq	8(%r15), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2285:
	addq	$8, %r13
	cmpq	%r13, -200(%rbp)
	jne	.L2282
	movq	-224(%rbp), %r12
	movq	-232(%rbp), %r14
	movq	-240(%rbp), %rbx
.L2297:
	cmpq	$0, -216(%rbp)
	je	.L2284
	movq	-216(%rbp), %rdi
	call	_ZdlPv@PLT
.L2284:
	movq	-80(%rbp), %rax
	movdqu	112(%r14), %xmm4
	pxor	%xmm0, %xmm0
	movq	128(%r14), %rdx
	movq	-72(%rbp), %r13
	movq	$0, 128(%r14)
	movq	%rax, -200(%rbp)
	movq	%rax, %r15
	movq	%rdx, -64(%rbp)
	movaps	%xmm4, -80(%rbp)
	movups	%xmm0, 112(%r14)
	cmpq	%r13, %rax
	je	.L2310
	movq	%rbx, -216(%rbp)
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2427:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L2425
.L2304:
	addq	$48, %r15
	cmpq	%r15, %r13
	je	.L2426
.L2298:
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2301
	call	_ZdaPv@PLT
.L2301:
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2302
	call	_ZdaPv@PLT
.L2302:
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.L2304
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L2427
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %eax
	jne	.L2304
.L2425:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2308
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2309:
	cmpl	$1, %eax
	jne	.L2304
	movq	(%r14), %rax
	addq	$48, %r15
	movq	%r14, %rdi
	call	*24(%rax)
	cmpq	%r15, %r13
	jne	.L2298
	.p2align 4,,10
	.p2align 3
.L2426:
	movq	-216(%rbp), %rbx
.L2310:
	cmpq	$0, -200(%rbp)
	je	.L2300
	movq	-200(%rbp), %rdi
	call	_ZdlPv@PLT
.L2300:
	movq	88(%rbx), %rax
	subq	$1, 64(%rax)
	movq	48(%rax), %r13
	movq	%r13, %rdi
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	16(%r13), %rax
	leaq	16(%r13), %rdi
	call	*(%rax)
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	cmpq	$0, -168(%rbp)
	je	.L2428
	movq	16(%rbx), %rax
	movzbl	1930(%rax), %edx
	testb	%dl, %dl
	jne	.L2429
.L2317:
	movq	$0, -208(%rbp)
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2262:
	cmpq	%rdx, %r14
	je	.L2260
	cmpq	$0, 24(%r14)
	je	.L2261
.L2260:
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rax
	movq	136(%rax), %rax
	movq	%rax, -208(%rbp)
	call	uv_mutex_unlock@PLT
.L2263:
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r14
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %rax
	movq	%rax, -176(%rbp)
	cmpq	%r14, %rbx
	jne	.L2328
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2432:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L2430
.L2322:
	addq	$48, %r14
	cmpq	%r14, %rbx
	je	.L2431
.L2328:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2319
	call	_ZdaPv@PLT
.L2319:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2320
	call	_ZdaPv@PLT
.L2320:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L2322
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	jne	.L2432
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L2322
.L2430:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L2326
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L2327:
	cmpl	$1, %eax
	jne	.L2322
	movq	(%r12), %rax
	addq	$48, %r14
	movq	%r12, %rdi
	call	*24(%rax)
	cmpq	%r14, %rbx
	jne	.L2328
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	-80(%rbp), %r14
.L2318:
	testq	%r14, %r14
	je	.L2329
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2329:
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2330
	movq	%rbx, -200(%rbp)
	.p2align 4,,10
	.p2align 3
.L2343:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2331
	movq	0(%r13), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2332
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r13)
	movq	%rax, 0(%r13)
	jne	.L2333
	movq	%r13, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r13), %r14
	testq	%r14, %r14
	je	.L2335
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2336
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L2433
	.p2align 4,,10
	.p2align 3
.L2335:
	movq	48(%r13), %r14
	leaq	48(%r13), %rbx
	cmpq	%r14, %rbx
	je	.L2342
	.p2align 4,,10
	.p2align 3
.L2341:
	movq	%r14, %r15
	movq	(%r14), %r14
	movq	16(%r15), %rdx
	leaq	16(%r15), %rdi
	call	*(%rdx)
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	%r14, %rbx
	jne	.L2341
.L2342:
	leaq	8(%r13), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2331:
	addq	$8, %r12
	cmpq	%r12, -200(%rbp)
	jne	.L2343
	movq	-104(%rbp), %r12
.L2330:
	testq	%r12, %r12
	je	.L2344
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2344:
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2345
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2352
	movq	8(%r12), %r13
	testq	%r13, %r13
	jne	.L2434
	.p2align 4,,10
	.p2align 3
.L2354:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L2351
.L2346:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2354
.L2434:
	lock subl	$1, 8(%r13)
	jne	.L2354
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L2354
	movq	0(%r13), %rax
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r12, %rbx
	jne	.L2346
	.p2align 4,,10
	.p2align 3
.L2351:
	movq	-128(%rbp), %r12
.L2345:
	testq	%r12, %r12
	je	.L2356
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2356:
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2357
	.p2align 4,,10
	.p2align 3
.L2358:
	movq	(%r12), %rdi
	addq	$16, %r12
	call	free@PLT
	cmpq	%r12, %rbx
	jne	.L2358
	movq	-152(%rbp), %r12
.L2357:
	testq	%r12, %r12
	je	.L2359
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2359:
	movq	-168(%rbp), %rdi
	call	free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2435
	movq	-208(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2332:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2336:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L2335
.L2433:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L2339
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2340:
	cmpl	$1, %eax
	jne	.L2335
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2350:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L2349
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2349:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L2351
.L2352:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2349
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L2349
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2326:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2333:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2290:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %eax
	jne	.L2289
.L2424:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2293
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L2294:
	cmpl	$1, %eax
	jne	.L2289
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2308:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2339:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	(%rbx), %rax
	leaq	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2312
	movq	88(%rbx), %rax
	xorl	%edx, %edx
	movq	16(%rbx), %rcx
	testq	%rax, %rax
	setne	%dl
	movl	%edx, -180(%rbp)
	movslq	32(%rbx), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L2411
.L2313:
	testq	%rax, %rax
	je	.L2314
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
.L2316:
	movq	16(%rbx), %rax
	movq	360(%rax), %rax
	movq	136(%rax), %rax
	movq	%rax, -208(%rbp)
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2293:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	%rbx, -216(%rbp)
	movq	%rdx, %rbx
	jmp	.L2277
	.p2align 4,,10
	.p2align 3
.L2274:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L2421
.L2277:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2274
	movl	8(%r13), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%r13)
	cmpl	$1, %ecx
	jne	.L2274
	movq	0(%r13), %rcx
	movq	%r13, %rdi
	call	*16(%rcx)
	movl	12(%r13), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 12(%r13)
	cmpl	$1, %ecx
	jne	.L2274
	movq	0(%r13), %rcx
	movq	%r13, %rdi
	call	*24(%rcx)
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2429:
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L2317
	movq	16(%rbx), %rsi
	movq	-208(%rbp), %rdx
	leaq	-176(%rbp), %rdi
	call	_ZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEE
	movq	%rax, -208(%rbp)
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2314:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2312:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2316
.L2435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb.cold, @function
_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb.cold:
.LFSB7971:
.L2411:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-180(%rbp), %rdx
	leaq	.LC47(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	88(%rbx), %rax
	jmp	.L2313
.L2410:
	leaq	.LC60(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2258
	.cfi_endproc
.LFE7971:
	.text
	.size	_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb, .-_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb.cold, .-_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb.cold
.LCOLDE61:
	.text
.LHOTE61:
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"Running MessagePort::OnMessage()"
	.align 8
.LC63:
	.string	"MessagePort drains queue because !can_call_into_js()"
	.section	.text.unlikely
	.align 2
.LCOLDB64:
	.text
.LHOTB64:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort9OnMessageEv
	.type	_ZN4node6worker11MessagePort9OnMessageEv, @function
_ZN4node6worker11MessagePort9OnMessageEv:
.LFB7986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	32(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	cmpb	$0, 2208(%rax,%rdx)
	jne	.L2468
.L2437:
	movq	352(%rax), %rsi
	leaq	-112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2438
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L2472
.L2438:
	call	_ZN2v86Object15CreationContextEv@PLT
	movl	$1000, %ebx
	leaq	-120(%rbp), %r14
	movq	%rax, %r12
	movq	88(%r15), %rax
	leaq	8(%rax), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	%r13, %rdi
	leaq	-80(%rbp), %r13
	call	uv_mutex_unlock@PLT
	movq	88(%r15), %rax
	cmpq	$1000, 64(%rax)
	cmovnb	64(%rax), %rbx
	subq	$1, %rbx
	jmp	.L2439
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	16(%r15), %rdx
	movslq	32(%r15), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2469
.L2470:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	cmpq	$0, 88(%r15)
	je	.L2440
	subq	$1, %rbx
	cmpq	$-1, %rbx
	je	.L2473
.L2439:
	movq	16(%r15), %rax
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, -120(%rbp)
	call	_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb
	testq	%rax, %rax
	je	.L2474
	movq	16(%r15), %rdx
	movq	%rax, -120(%rbp)
	movq	360(%rdx), %rcx
	movq	136(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2444
	movq	(%rcx), %rcx
	cmpq	%rcx, (%rax)
	je	.L2443
.L2444:
	movzbl	1930(%rdx), %eax
	testb	%al, %al
	je	.L2445
	movzbl	2664(%rdx), %eax
	testb	%al, %al
	jne	.L2445
	movq	232(%r15), %rsi
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	jne	.L2470
	cmpq	$0, 88(%r15)
	je	.L2443
	movl	72(%r15), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L2443
	leaq	104(%r15), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	je	.L2443
.L2451:
	leaq	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2474:
	movq	$0, -120(%rbp)
.L2443:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2440:
	movq	-136(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2475
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2472:
	.cfi_restore_state
	movq	16(%r15), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r9
	movq	%r9, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2473:
	movl	72(%r15), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L2440
	leaq	104(%r15), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	je	.L2440
	jmp	.L2451
.L2475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker11MessagePort9OnMessageEv.cold, @function
_ZN4node6worker11MessagePort9OnMessageEv.cold:
.LFSB7986:
.L2469:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC63(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2470
.L2468:
	leaq	.LC62(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	16(%r15), %rax
	jmp	.L2437
	.cfi_endproc
.LFE7986:
	.text
	.size	_ZN4node6worker11MessagePort9OnMessageEv, .-_ZN4node6worker11MessagePort9OnMessageEv
	.section	.text.unlikely
	.size	_ZN4node6worker11MessagePort9OnMessageEv.cold, .-_ZN4node6worker11MessagePort9OnMessageEv.cold
.LCOLDE64:
	.text
.LHOTE64:
	.p2align 4
	.type	_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEENUlP10uv_async_sE_4_FUNESB_, @function
_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEENUlP10uv_async_sE_4_FUNESB_:
.LFB7961:
	.cfi_startproc
	endbr64
	subq	$104, %rdi
	jmp	_ZN4node6worker11MessagePort9OnMessageEv
	.cfi_endproc
.LFE7961:
	.size	_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEENUlP10uv_async_sE_4_FUNESB_, .-_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEENUlP10uv_async_sE_4_FUNESB_
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort5DrainERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort5DrainERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort5DrainERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L2478
	movq	(%rdi), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2487
.L2480:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2485
	cmpw	$1040, %cx
	jne	.L2481
.L2485:
	movq	23(%rdx), %rdi
.L2483:
	testq	%rdi, %rdi
	je	.L2477
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node6worker11MessagePort9OnMessageEv
	.p2align 4,,10
	.p2align 3
.L2478:
	.cfi_restore_state
	movq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2480
.L2487:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2477:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2481:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L2483
	.cfi_endproc
.LFE8005:
	.size	_ZN4node6worker11MessagePort5DrainERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker11MessagePort5DrainERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort14ReceiveMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort14ReceiveMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort14ReceiveMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2519
	cmpw	$1040, %cx
	jne	.L2489
.L2519:
	movq	23(%rdx), %r12
.L2491:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L2492
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2493:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2500
	movl	16(%rbx), %edx
	movq	3184(%r12), %rdi
	testl	%edx, %edx
	jle	.L2526
	movq	8(%rbx), %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L2500
.L2499:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L2505
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L2506:
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2527
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2520
	cmpw	$1040, %cx
	jne	.L2508
.L2520:
	movq	23(%rdx), %r12
.L2510:
	testq	%r12, %r12
	je	.L2528
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2517
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L2529
.L2517:
	call	_ZN2v86Object15CreationContextEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN4node6worker11MessagePort14ReceiveMessageEN2v85LocalINS2_7ContextEEEb
	testq	%rax, %rax
	je	.L2488
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L2488:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2526:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L2499
.L2500:
	movq	352(%r12), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2530
.L2496:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2531
.L2501:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2532
.L2502:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2533
.L2503:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2534
.L2504:
	addq	$24, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2492:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	8(%rbx), %r12
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L2489:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2529:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2528:
	movq	(%rbx), %rbx
	movq	32(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2521
	cmpw	$1040, %cx
	je	.L2521
	leaq	32(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2521:
	movq	23(%rdx), %rax
.L2514:
	movq	360(%rax), %rax
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L2535
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2527:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2530:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2531:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2532:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2533:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2534:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2508:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2510
.L2535:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L2488
	.cfi_endproc
.LFE8006:
	.size	_ZN4node6worker11MessagePort14ReceiveMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker11MessagePort14ReceiveMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"Transfer list contains duplicate ArrayBuffer"
	.align 8
.LC66:
	.string	"Transfer list contains source port"
	.align 8
.LC67:
	.string	"MessagePort in transfer list is already detached"
	.align 8
.LC68:
	.string	"Transfer list contains duplicate MessagePort"
	.section	.rodata.str1.1
.LC69:
	.string	"ERR_INVALID_TRANSFER_OBJECT"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"Found invalid object in transferList"
	.section	.text.unlikely
	.align 2
.LCOLDB71:
	.text
.LHOTB71:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE
	.type	_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE, @function
_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE:
.LFB7903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$328, %rsp
	movq	%rdi, -288(%rbp)
	movq	352(%rsi), %rsi
	movq	%rcx, -328(%rbp)
	movq	%r9, -296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpq	$0, 8(%r15)
	jne	.L2677
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SerializerDelegateE(%rip), %rax
	leaq	-264(%rbp), %r15
	movq	%rbx, -128(%rbp)
	movq	352(%rbx), %rsi
	leaq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	movq	-288(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	%r12, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v815ValueSerializerC1EPNS_7IsolateEPNS0_8DelegateE@PLT
	cmpq	$0, 0(%r13)
	movq	%r15, -136(%rbp)
	je	.L2538
	leaq	-208(%rbp), %rcx
	xorl	%eax, %eax
	movq	$0, -344(%rbp)
	movq	%rcx, -336(%rbp)
	leaq	-80(%rbp), %rcx
	movq	$0, -320(%rbp)
	movq	$0, -312(%rbp)
	movl	$0, -280(%rbp)
	movq	%rcx, -352(%rbp)
.L2590:
	movq	16(%r13), %rdx
	movq	(%rdx,%rax,8), %r14
	movq	%r14, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	je	.L2539
	movq	%r14, %rdi
	movq	%r14, -208(%rbp)
	call	_ZNK2v811ArrayBuffer12IsDetachableEv@PLT
	testb	%al, %al
	je	.L2556
	movq	-208(%rbp), %rdi
	call	_ZNK2v811ArrayBuffer10IsExternalEv@PLT
	testb	%al, %al
	jne	.L2556
	movq	360(%rbx), %rax
	cmpb	$0, 2384(%rax)
	je	.L2556
	movq	72(%rax), %rdx
	movq	-208(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v86Object10HasPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	testb	%al, %al
	je	.L2541
	shrw	$8, %ax
	jne	.L2556
	movq	-320(%rbp), %r14
	movq	-336(%rbp), %rdx
	movq	-312(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPN2v85LocalINS2_11ArrayBufferEEESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	%rax, %rdx
	cmpq	%rax, %r14
	je	.L2542
	movq	352(%rbx), %rdi
	leaq	.LC65(%rip), %rsi
	movl	$44, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2582
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	%rsi, -280(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-280(%rbp), %rsi
	jmp	.L2582
.L2542:
	movq	-320(%rbp), %rcx
	movq	%rcx, %r9
	subq	-312(%rbp), %r9
	movq	%r9, %rsi
	sarq	$3, %rsi
	cmpq	%rcx, -344(%rbp)
	je	.L2545
	movq	-208(%rbp), %rax
	addq	$8, %rcx
	movq	%rax, -8(%rcx)
	movq	%rcx, -320(%rbp)
.L2546:
	movq	-208(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v815ValueSerializer19TransferArrayBufferEjNS_5LocalINS_11ArrayBufferEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2556:
	addl	$1, -280(%rbp)
	movl	-280(%rbp), %eax
	cmpq	0(%r13), %rax
	jb	.L2590
	movq	%r15, %rdi
	call	_ZN2v815ValueSerializer11WriteHeaderEv@PLT
	movq	-328(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v815ValueSerializer10WriteValueENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L2541
	movq	-312(%rbp), %r13
	cmpq	%r13, -320(%rbp)
	je	.L2592
	leaq	-208(%rbp), %rax
	movq	%r15, -336(%rbp)
	leaq	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm(%rip), %r14
	movq	%rax, -296(%rbp)
	movq	%r12, -328(%rbp)
	movq	%r13, %r12
	movq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L2598:
	movq	(%r12), %r8
	movq	-296(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r8, -280(%rbp)
	call	_ZN2v811ArrayBuffer11ExternalizeEv@PLT
	movq	-280(%rbp), %r8
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r15
	movq	%r8, %rdi
	call	_ZN2v811ArrayBuffer6DetachEv@PLT
	movq	360(%r13), %rcx
	cmpb	$0, 2384(%rcx)
	je	.L2678
	movq	2376(%rcx), %rdi
	movq	(%rdi), %rcx
	movq	64(%rcx), %rcx
	cmpq	%r14, %rcx
	jne	.L2594
	lock subq	%r15, 16(%rdi)
.L2595:
	movq	-288(%rbp), %rax
	movq	%rbx, -256(%rbp)
	movq	%r15, -248(%rbp)
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	je	.L2596
	movq	%rbx, (%rsi)
	addq	$16, %rsi
	addq	$8, %r12
	movq	%r15, -8(%rsi)
	movq	%rsi, 32(%rax)
	cmpq	%r12, -320(%rbp)
	jne	.L2598
.L2674:
	movq	-328(%rbp), %r12
	movq	-336(%rbp), %r15
.L2592:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	leaq	-208(%rbp), %r14
	movq	%rax, -280(%rbp)
	cmpq	%rax, %rbx
	je	.L2612
	movq	%r12, -296(%rbp)
	movq	%rbx, %r13
	movq	%r15, -320(%rbp)
	.p2align 4,,10
	.p2align 3
.L2613:
	movq	0(%r13), %rbx
	leaq	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE(%rip), %rsi
	movq	(%rbx), %rax
	movq	80(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2602
	movq	88(%rbx), %rax
	xorl	%edx, %edx
	movq	16(%rbx), %rcx
	testq	%rax, %rax
	setne	%dl
	movl	%edx, -208(%rbp)
	movslq	32(%rbx), %rdx
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L2665
.L2603:
	testq	%rax, %rax
	je	.L2604
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
.L2606:
	movq	88(%rbx), %rax
	movq	-112(%rbp), %r12
	testq	%rax, %rax
	je	.L2679
	leaq	8(%rax), %r15
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	88(%rbx), %rax
	movq	%r15, %rdi
	movq	$0, 72(%rax)
	movq	$0, 88(%rbx)
	movq	%rax, -208(%rbp)
	call	uv_mutex_unlock@PLT
	movq	80(%r12), %rsi
	cmpq	88(%r12), %rsi
	je	.L2608
	movq	-208(%rbp), %rax
	addq	$8, %rsi
	movq	$0, -208(%rbp)
	movq	%rax, -8(%rsi)
	movq	%rsi, 80(%r12)
.L2609:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2610
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, -280(%rbp)
	jne	.L2613
.L2675:
	movq	-296(%rbp), %r12
	movq	-320(%rbp), %r15
.L2612:
	movq	%r15, %rdi
	call	_ZN2v815ValueSerializer7ReleaseEv@PLT
	movq	%rax, %rbx
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L2680
	movq	-288(%rbp), %r14
	movq	8(%r14), %rdi
	call	free@PLT
	movq	%rbx, 8(%r14)
	movl	$257, %ebx
	movq	%r13, 16(%r14)
	jmp	.L2557
	.p2align 4,,10
	.p2align 3
.L2539:
	movq	3184(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L2558
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L2559
	testq	%r14, %r14
	je	.L2559
	movq	(%r14), %rcx
	cmpq	%rcx, (%rax)
	je	.L2560
.L2559:
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2681
	movq	(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2629
	cmpw	$1040, %cx
	jne	.L2562
.L2629:
	movq	23(%rdx), %rax
.L2564:
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L2565
	cmpq	$0, 88(%rax)
	je	.L2565
	movl	72(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L2565
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movq	%rsi, %rcx
	subq	%rdx, %rcx
	movq	%rcx, %rdi
	sarq	$5, %rcx
	sarq	$3, %rdi
	testq	%rcx, %rcx
	jle	.L2569
	salq	$5, %rcx
	addq	%rdx, %rcx
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2686:
	cmpq	8(%rdx), %rax
	je	.L2682
	cmpq	16(%rdx), %rax
	je	.L2683
	cmpq	24(%rdx), %rax
	je	.L2684
	addq	$32, %rdx
	cmpq	%rdx, %rcx
	je	.L2685
.L2574:
	cmpq	(%rdx), %rax
	jne	.L2686
.L2570:
	cmpq	%rsi, %rdx
	je	.L2579
	movq	352(%rbx), %rdi
	leaq	.LC68(%rip), %rsi
	movl	$44, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2582
	jmp	.L2676
	.p2align 4,,10
	.p2align 3
.L2565:
	movq	352(%rbx), %rdi
	leaq	.LC67(%rip), %rsi
	movl	$48, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2676
.L2582:
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZN4node6worker12_GLOBAL__N_123ThrowDataCloneExceptionEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEE
	movb	$0, %bh
.L2557:
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L2614
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2614:
	movq	%r15, %rdi
	call	_ZN2v815ValueSerializerD1Ev@PLT
	movq	-80(%rbp), %rdi
	leaq	16+_ZTVN4node6worker12_GLOBAL__N_118SerializerDelegateE(%rip), %rax
	movq	%rax, -144(%rbp)
	testq	%rdi, %rdi
	je	.L2615
	call	_ZdlPv@PLT
.L2615:
	movq	-96(%rbp), %r14
	movq	-104(%rbp), %r13
	cmpq	%r13, %r14
	je	.L2616
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2617
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L2620
.L2618:
	movq	-104(%rbp), %r13
.L2616:
	testq	%r13, %r13
	je	.L2621
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2621:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-304(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2687
	addq	$328, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2617:
	.cfi_restore_state
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L2620
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2562:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2564
	.p2align 4,,10
	.p2align 3
.L2677:
	leaq	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2681:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	sarq	$3, %rdi
.L2569:
	cmpq	$2, %rdi
	je	.L2575
	cmpq	$3, %rdi
	je	.L2576
	cmpq	$1, %rdi
	je	.L2577
.L2579:
	cmpq	-64(%rbp), %rsi
	je	.L2583
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, -72(%rbp)
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2682:
	addq	$8, %rdx
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2683:
	addq	$16, %rdx
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2684:
	addq	$24, %rdx
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2576:
	cmpq	(%rdx), %rax
	je	.L2570
	addq	$8, %rdx
.L2575:
	cmpq	(%rdx), %rax
	je	.L2570
	addq	$8, %rdx
.L2577:
	cmpq	(%rdx), %rax
	jne	.L2579
	jmp	.L2570
.L2583:
	movq	-336(%rbp), %rdx
	movq	-352(%rbp), %rdi
	call	_ZNSt6vectorIPN4node6worker11MessagePortESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2594:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	*%rcx
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2538:
	movq	%r15, %rdi
	call	_ZN2v815ValueSerializer11WriteHeaderEv@PLT
	movq	-328(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v815ValueSerializer10WriteValueENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L2628
	xorl	%ebx, %ebx
	movb	$0, %bh
	jmp	.L2614
.L2596:
	leaq	24(%rax), %rdi
	leaq	-256(%rbp), %rdx
	addq	$8, %r12
	call	_ZNSt6vectorIN4node14MallocedBufferIcEESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-256(%rbp), %rdi
	call	free@PLT
	cmpq	%r12, -320(%rbp)
	jne	.L2598
	jmp	.L2674
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	352(%rbx), %r13
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC69(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L2688
.L2585:
	movq	%r13, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC70(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2689
.L2586:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2690
.L2587:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2691
.L2588:
	movq	%r13, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-280(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2692
.L2589:
	movq	%r14, %rsi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movb	$0, %bh
	jmp	.L2557
.L2560:
	movq	352(%rbx), %rdi
	leaq	.LC66(%rip), %rsi
	movl	$34, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L2582
	jmp	.L2676
.L2678:
	leaq	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2610:
	addq	$8, %r13
	cmpq	%r13, -280(%rbp)
	jne	.L2613
	jmp	.L2675
.L2604:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE@PLT
	jmp	.L2606
.L2602:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2606
.L2608:
	leaq	72(%r12), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorISt10unique_ptrIN4node6worker15MessagePortDataESt14default_deleteIS3_EESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L2609
.L2541:
	xorl	%ebx, %ebx
	movb	$0, %bh
	jmp	.L2557
.L2679:
	leaq	_ZZN4node6worker11MessagePort6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2545:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	je	.L2693
	testq	%rsi, %rsi
	je	.L2623
	movabsq	$1152921504606846975, %rax
	leaq	(%rsi,%rsi), %r8
	cmpq	%r8, %rsi
	jbe	.L2694
.L2548:
	leaq	0(,%rax,8), %r8
	movq	%rsi, -368(%rbp)
	movq	%r8, %rdi
	movq	%rdx, -360(%rbp)
	movq	%r9, -344(%rbp)
	movq	%r8, -320(%rbp)
	call	_Znwm@PLT
	movq	-320(%rbp), %r8
	movq	-344(%rbp), %r9
	movq	-360(%rbp), %rdx
	movq	-368(%rbp), %rsi
	movq	%rax, %rcx
.L2549:
	movq	-208(%rbp), %rax
	movq	-312(%rbp), %r10
	movq	%rax, (%rcx,%r9)
	cmpq	%r10, %rdx
	je	.L2626
	leaq	-8(%rdx), %rdi
	leaq	15(%rcx), %rax
	subq	%r10, %rdi
	subq	%r10, %rax
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L2627
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L2627
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %rdx
	shrq	%rdx
	salq	$4, %rdx
.L2552:
	movdqu	(%r10,%rax), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2552
	movq	%r9, %r10
	movq	-312(%rbp), %rax
	andq	$-2, %r10
	leaq	0(,%r10,8), %rdx
	addq	%rdx, %rax
	addq	%rcx, %rdx
	cmpq	%r10, %r9
	je	.L2554
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2554:
	leaq	8(%rcx,%rdi), %rax
.L2550:
	movq	-312(%rbp), %rdi
	addq	$8, %rax
	movq	%rax, -320(%rbp)
	testq	%rdi, %rdi
	je	.L2555
	movq	%rsi, -360(%rbp)
	movq	%rcx, -344(%rbp)
	movq	%r8, -312(%rbp)
	call	_ZdlPv@PLT
	movq	-360(%rbp), %rsi
	movq	-344(%rbp), %rcx
	movq	-312(%rbp), %r8
.L2555:
	leaq	(%rcx,%r8), %rax
	movq	%rcx, -312(%rbp)
	movq	%rax, -344(%rbp)
	jmp	.L2546
.L2692:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2589
.L2691:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2588
.L2690:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2587
.L2689:
	movq	%rax, -288(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-288(%rbp), %rdi
	jmp	.L2586
.L2688:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2585
.L2694:
	xorl	%ecx, %ecx
	testq	%r8, %r8
	je	.L2549
	cmpq	%rax, %r8
	cmovbe	%r8, %rax
	jmp	.L2548
.L2628:
	movq	$0, -312(%rbp)
	jmp	.L2592
.L2623:
	movl	$1, %eax
	jmp	.L2548
.L2687:
	call	__stack_chk_fail@PLT
.L2680:
	leaq	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2627:
	movq	-312(%rbp), %rax
	movq	%rcx, %r9
.L2551:
	movq	(%rax), %r10
	addq	$8, %rax
	addq	$8, %r9
	movq	%r10, -8(%r9)
	cmpq	%rdx, %rax
	jne	.L2551
	jmp	.L2554
.L2626:
	movq	%rcx, %rax
	jmp	.L2550
.L2693:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE.cold, @function
_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE.cold:
.LFSB7903:
.L2665:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rdx
	leaq	.LC47(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	88(%rbx), %rax
	jmp	.L2603
	.cfi_endproc
.LFE7903:
	.text
	.size	_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE, .-_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE
	.section	.text.unlikely
	.size	_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE.cold, .-_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE.cold
.LCOLDE71:
	.text
.LHOTE71:
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"The target port was posted to itself, and the communication channel was lost"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort11PostMessageEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEERKNS_16MaybeStackBufferIS7_Lm8EEE
	.type	_ZN4node6worker11MessagePort11PostMessageEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEERKNS_16MaybeStackBufferIS7_Lm8EEE, @function
_ZN4node6worker11MessagePort11PostMessageEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEERKNS_16MaybeStackBufferIS7_Lm8EEE:
.LFB7989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L2696
	movzbl	11(%rbx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L2783
.L2696:
	movq	%rbx, %rdi
	movq	%r8, -192(%rbp)
	leaq	-176(%rbp), %r15
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %r12
	movq	%r10, -184(%rbp)
	call	_ZN2v86Object15CreationContextEv@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %r9
	movq	%r13, %rsi
	movq	-184(%rbp), %r10
	movq	-192(%rbp), %r8
	movq	%rax, %rdx
	movq	%r15, %rdi
	movq	%r12, -176(%rbp)
	movq	%r10, %rcx
	movups	%xmm0, -152(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE
	movl	%eax, %ebx
	movq	88(%r14), %rax
	testq	%rax, %rax
	je	.L2784
	testb	%bl, %bl
	jne	.L2699
	xorl	%eax, %eax
	movb	$0, %ah
	movw	%ax, -194(%rbp)
.L2698:
	movq	-72(%rbp), %r14
	movq	-80(%rbp), %r13
	movq	%r12, -176(%rbp)
	cmpq	%r13, %r14
	jne	.L2715
	jmp	.L2705
	.p2align 4,,10
	.p2align 3
.L2787:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L2785
.L2709:
	addq	$48, %r13
	cmpq	%r13, %r14
	je	.L2786
.L2715:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2706
	call	_ZdaPv@PLT
.L2706:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2707
	call	_ZdaPv@PLT
.L2707:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2709
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L2787
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %eax
	jne	.L2709
.L2785:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2713
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L2714:
	cmpl	$1, %eax
	jne	.L2709
	movq	(%r12), %rax
	addq	$48, %r13
	movq	%r12, %rdi
	call	*24(%rax)
	cmpq	%r13, %r14
	jne	.L2715
	.p2align 4,,10
	.p2align 3
.L2786:
	movq	-80(%rbp), %r13
.L2705:
	testq	%r13, %r13
	je	.L2716
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2716:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	cmpq	%r12, %r13
	je	.L2717
	.p2align 4,,10
	.p2align 3
.L2730:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2718
	movq	(%r15), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2719
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r15)
	movq	%rax, (%r15)
	jne	.L2788
	movq	%r15, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r15), %r14
	testq	%r14, %r14
	je	.L2722
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2723
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L2789
	.p2align 4,,10
	.p2align 3
.L2722:
	movq	48(%r15), %r14
	leaq	48(%r15), %rdx
	cmpq	%r14, %rdx
	je	.L2729
	.p2align 4,,10
	.p2align 3
.L2728:
	movq	%r14, %r9
	movq	%rdx, -192(%rbp)
	movq	(%r14), %r14
	movq	16(%r9), %rcx
	leaq	16(%r9), %rdi
	movq	%r9, -184(%rbp)
	call	*(%rcx)
	movq	-184(%rbp), %r9
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	movq	-192(%rbp), %rdx
	cmpq	%r14, %rdx
	jne	.L2728
.L2729:
	leaq	8(%r15), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2718:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L2730
	movq	-104(%rbp), %r12
.L2717:
	testq	%r12, %r12
	je	.L2731
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2731:
	movq	-120(%rbp), %r14
	movq	-128(%rbp), %r12
	cmpq	%r12, %r14
	je	.L2732
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L2733
	jmp	.L2739
	.p2align 4,,10
	.p2align 3
.L2741:
	addq	$16, %r12
	cmpq	%r12, %r14
	je	.L2738
.L2733:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2741
	lock subl	$1, 8(%r13)
	jne	.L2741
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L2741
	movq	0(%r13), %rax
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r12, %r14
	jne	.L2733
	.p2align 4,,10
	.p2align 3
.L2738:
	movq	-128(%rbp), %r12
.L2732:
	testq	%r12, %r12
	je	.L2743
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2743:
	movq	-144(%rbp), %r13
	movq	-152(%rbp), %r12
	cmpq	%r12, %r13
	je	.L2744
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	(%r12), %rdi
	addq	$16, %r12
	call	free@PLT
	cmpq	%r12, %r13
	jne	.L2745
	movq	-152(%rbp), %r12
.L2744:
	testq	%r12, %r12
	je	.L2746
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2746:
	movq	-168(%rbp), %rdi
	call	free@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2790
	movzwl	-194(%rbp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2699:
	.cfi_restore_state
	movq	80(%rax), %r8
	movq	%r8, %rdi
	movq	%r8, -184(%rbp)
	call	uv_mutex_lock@PLT
	movq	88(%r14), %rax
	movq	-184(%rbp), %r8
	movq	96(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2700
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	cmpq	%rdx, %rax
	jne	.L2703
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2702:
	addq	$8, %rax
	cmpq	%rax, %rdx
	je	.L2701
.L2703:
	cmpq	(%rax), %rdi
	jne	.L2702
	leaq	.LC72(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	%r8, -184(%rbp)
	call	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz@PLT
	movq	-184(%rbp), %r8
.L2700:
	movl	$257, %eax
	movw	%ax, -194(%rbp)
.L2704:
	movq	%r8, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2737:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L2736
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2736:
	addq	$16, %r12
	cmpq	%r12, %r14
	je	.L2738
.L2739:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2736
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L2736
	jmp	.L2737
	.p2align 4,,10
	.p2align 3
.L2719:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2723:
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %eax
	jne	.L2722
.L2789:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2726
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2727:
	cmpl	$1, %eax
	jne	.L2722
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2713:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L2788:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2784:
	movw	%bx, -194(%rbp)
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2783:
	movq	16(%rdi), %rax
	movq	(%rbx), %rsi
	movq	%rcx, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %r10
	movq	%rax, %rbx
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2726:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2727
	.p2align 4,,10
	.p2align 3
.L2701:
	movq	%r15, %rsi
	movq	%r8, -184(%rbp)
	call	_ZN4node6worker15MessagePortData18AddToIncomingQueueEONS0_7MessageE
	movl	$257, %edx
	movq	-184(%rbp), %r8
	movw	%dx, -194(%rbp)
	jmp	.L2704
.L2790:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7989:
	.size	_ZN4node6worker11MessagePort11PostMessageEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEERKNS_16MaybeStackBufferIS7_Lm8EEE, .-_ZN4node6worker11MessagePort11PostMessageEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEERKNS_16MaybeStackBufferIS7_Lm8EEE
	.section	.rodata.str1.1
.LC73:
	.string	"ERR_MISSING_ARGS"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"Not enough arguments to MessagePort.postMessage"
	.align 8
.LC76:
	.string	"Optional transferList argument must be an iterable"
	.align 8
.LC77:
	.string	"Optional options.transfer argument must be an iterable"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker11MessagePort11PostMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker11MessagePort11PostMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker11MessagePort11PostMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2889
	cmpw	$1040, %cx
	jne	.L2792
.L2889:
	movq	23(%rdx), %r15
.L2794:
	movq	8(%rbx), %rax
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object15CreationContextEv@PLT
	movq	%rax, %r14
	movl	16(%rbx), %eax
	testl	%eax, %eax
	je	.L2957
	cmpl	$1, %eax
	jle	.L2958
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %rdi
.L2803:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L2804
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L2959
.L2804:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2806
	movl	16(%rbx), %eax
.L2805:
	movdqa	.LC75(%rip), %xmm0
	leaq	-120(%rbp), %r13
	movq	%r13, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movq	$0, -120(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	cmpl	$1, %eax
	jg	.L2960
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2814:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2815
	cmpl	$1, 16(%rbx)
	jg	.L2816
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	addq	$88, %rcx
.L2817:
	leaq	-144(%rbp), %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r8, %rdx
	movq	%r8, -280(%rbp)
	call	_ZN4node6workerL12ReadIterableEPNS_11EnvironmentEN2v85LocalINS3_7ContextEEERNS_16MaybeStackBufferINS4_INS3_5ValueEEELm8EEES9_
	testb	%al, %al
	je	.L2824
	shrw	$8, %ax
	jne	.L2815
	cmpl	$1, 16(%rbx)
	movq	-280(%rbp), %r8
	jg	.L2819
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2820:
	movq	360(%r15), %rax
	movq	%r14, %rsi
	movq	%r8, -280(%rbp)
	movq	1752(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-280(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L2824
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2823
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2961
.L2823:
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node6workerL12ReadIterableEPNS_11EnvironmentEN2v85LocalINS3_7ContextEEERNS_16MaybeStackBufferINS4_INS3_5ValueEEELm8EEES9_
	testb	%al, %al
	je	.L2824
	shrw	$8, %ax
	je	.L2962
	.p2align 4,,10
	.p2align 3
.L2815:
	movq	8(%rbx), %rdx
	leaq	8(%rdx), %rdi
	movq	%rdx, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-280(%rbp), %rdi
	movq	-288(%rbp), %rdx
	testl	%eax, %eax
	jle	.L2963
	movq	8(%rdx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2890
	cmpw	$1040, %cx
	jne	.L2831
.L2890:
	movq	23(%rdx), %rdi
.L2833:
	movl	16(%rbx), %eax
	testq	%rdi, %rdi
	je	.L2964
	testl	%eax, %eax
	jle	.L2965
	movq	8(%rbx), %rdx
.L2880:
	leaq	-144(%rbp), %rcx
	movq	%r15, %rsi
	call	_ZN4node6worker11MessagePort11PostMessageEPNS_11EnvironmentEN2v85LocalINS4_5ValueEEERKNS_16MaybeStackBufferIS7_Lm8EEE
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2791
	cmpq	%r13, %rdi
	je	.L2791
.L2947:
	call	free@PLT
.L2791:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2966
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2958:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L2803
	.p2align 4,,10
	.p2align 3
.L2960:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2814
.L2962:
	movq	352(%r15), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2967
.L2825:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC77(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2968
.L2826:
	movq	%r14, %rdi
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2969
.L2827:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2970
.L2828:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2971
.L2829:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L2824:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2791
	testq	%rdi, %rdi
	jne	.L2947
	jmp	.L2791
	.p2align 4,,10
	.p2align 3
.L2816:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rcx
	jmp	.L2817
	.p2align 4,,10
	.p2align 3
.L2957:
	movq	352(%r15), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2972
.L2796:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC74(%rip), %rsi
.L2956:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2973
.L2810:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2974
.L2811:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2975
.L2812:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2976
.L2813:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L2791
	.p2align 4,,10
	.p2align 3
.L2959:
	movslq	43(%rdx), %rdx
	subl	$3, %edx
	andl	$-3, %edx
	je	.L2805
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L2965:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L2880
	.p2align 4,,10
	.p2align 3
.L2792:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L2794
	.p2align 4,,10
	.p2align 3
.L2806:
	movq	352(%r15), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2977
.L2809:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC76(%rip), %rsi
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2819:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2820
	.p2align 4,,10
	.p2align 3
.L2964:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %r10
	movq	$0, -264(%rbp)
	movq	%r10, -272(%rbp)
	movq	$0, -256(%rbp)
	movups	%xmm0, -248(%rbp)
	movups	%xmm0, -232(%rbp)
	movups	%xmm0, -216(%rbp)
	movups	%xmm0, -200(%rbp)
	movups	%xmm0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	testl	%eax, %eax
	jle	.L2978
	movq	8(%rbx), %rcx
.L2836:
	movq	%r12, %r9
	leaq	-272(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	leaq	-144(%rbp), %r8
	call	_ZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEE
	movq	-168(%rbp), %rbx
	movq	-176(%rbp), %r12
	leaq	16+_ZTVN4node6worker7MessageE(%rip), %r10
	movq	%r10, -272(%rbp)
	cmpq	%r12, %rbx
	jne	.L2847
	jmp	.L2837
	.p2align 4,,10
	.p2align 3
.L2981:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L2979
	.p2align 4,,10
	.p2align 3
.L2841:
	addq	$48, %r12
	cmpq	%r12, %rbx
	je	.L2980
.L2847:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2838
	call	_ZdaPv@PLT
.L2838:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2839
	call	_ZdaPv@PLT
.L2839:
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L2841
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	jne	.L2981
	movl	8(%r14), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %eax
	jne	.L2841
.L2979:
	movq	(%r14), %rax
	movq	%rdx, -280(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-280(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2845
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2846:
	cmpl	$1, %eax
	jne	.L2841
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2841
.L2831:
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L2833
.L2978:
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	addq	$88, %rcx
	jmp	.L2836
.L2961:
	cmpl	$5, 43(%rax)
	je	.L2815
	jmp	.L2823
.L2972:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2980:
	movq	-176(%rbp), %r12
.L2837:
	testq	%r12, %r12
	je	.L2848
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2848:
	movq	-192(%rbp), %rax
	movq	-200(%rbp), %rsi
	movq	%rax, -288(%rbp)
	movq	%rsi, -280(%rbp)
	cmpq	%rsi, %rax
	je	.L2849
	.p2align 4,,10
	.p2align 3
.L2862:
	movq	-280(%rbp), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L2850
	movq	(%r12), %rax
	leaq	_ZN4node6worker15MessagePortDataD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2851
	leaq	16+_ZTVN4node6worker15MessagePortDataE(%rip), %rax
	cmpq	$0, 72(%r12)
	movq	%rax, (%r12)
	jne	.L2982
	movq	%r12, %rdi
	call	_ZN4node6worker15MessagePortData11DisentangleEv
	movq	88(%r12), %r15
	testq	%r15, %r15
	je	.L2854
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2855
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L2856:
	cmpl	$1, %eax
	je	.L2983
.L2854:
	movq	48(%r12), %r15
	leaq	48(%r12), %rbx
	cmpq	%r15, %rbx
	je	.L2861
	.p2align 4,,10
	.p2align 3
.L2860:
	movq	%r15, %r14
	movq	(%r15), %r15
	movq	16(%r14), %rdx
	leaq	16(%r14), %rdi
	call	*(%rdx)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	cmpq	%r15, %rbx
	jne	.L2860
.L2861:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2850:
	addq	$8, -280(%rbp)
	movq	-280(%rbp), %rax
	cmpq	%rax, -288(%rbp)
	jne	.L2862
	movq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)
.L2849:
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L2863
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2863:
	movq	-216(%rbp), %rbx
	movq	-224(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2864
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L2865
	jmp	.L2871
	.p2align 4,,10
	.p2align 3
.L2873:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L2870
.L2865:
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L2873
	lock subl	$1, 8(%r14)
	jne	.L2873
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r14)
	jne	.L2873
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2873
.L2869:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	cmpl	$1, %eax
	jne	.L2868
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2868:
	addq	$16, %r12
	cmpq	%r12, %rbx
	je	.L2870
.L2871:
	movq	8(%r12), %r14
	testq	%r14, %r14
	je	.L2868
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L2868
	jmp	.L2869
.L2851:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2850
.L2855:
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L2856
.L2870:
	movq	-224(%rbp), %r12
.L2864:
	testq	%r12, %r12
	je	.L2875
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2875:
	movq	-240(%rbp), %rbx
	movq	-248(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2876
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	(%r12), %rdi
	addq	$16, %r12
	call	free@PLT
	cmpq	%r12, %rbx
	jne	.L2877
	movq	-248(%rbp), %r12
.L2876:
	testq	%r12, %r12
	je	.L2878
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2878:
	movq	-264(%rbp), %rdi
	call	free@PLT
	jmp	.L2824
.L2845:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2846
.L2963:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2983:
	movq	(%r15), %rax
	movq	%rdx, -296(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-296(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2858
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L2859:
	cmpl	$1, %eax
	jne	.L2854
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L2854
.L2973:
	movq	%rax, -280(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-280(%rbp), %rdi
	jmp	.L2810
.L2974:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2811
.L2975:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2812
.L2976:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2813
.L2982:
	leaq	_ZZN4node6worker15MessagePortDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2858:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L2859
.L2977:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2809
.L2971:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2829
.L2970:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2828
.L2969:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2827
.L2968:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2826
.L2967:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2825
.L2966:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8000:
	.size	_ZN4node6worker11MessagePort11PostMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker11MessagePort11PostMessageERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node6worker12_GLOBAL__N_120DeserializerDelegateE, @object
	.size	_ZTVN4node6worker12_GLOBAL__N_120DeserializerDelegateE, 56
_ZTVN4node6worker12_GLOBAL__N_120DeserializerDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD1Ev
	.quad	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegateD0Ev
	.quad	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateE
	.quad	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEj
	.quad	_ZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEj
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node6worker12_GLOBAL__N_118SerializerDelegateE, @object
	.size	_ZTVN4node6worker12_GLOBAL__N_118SerializerDelegateE, 80
_ZTVN4node6worker12_GLOBAL__N_118SerializerDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD1Ev
	.quad	_ZN4node6worker12_GLOBAL__N_118SerializerDelegateD0Ev
	.quad	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate19ThrowDataCloneErrorEN2v85LocalINS3_6StringEEE
	.quad	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate15WriteHostObjectEPN2v87IsolateENS3_5LocalINS3_6ObjectEEE
	.quad	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate22GetSharedArrayBufferIdEPN2v87IsolateENS3_5LocalINS3_17SharedArrayBufferEEE
	.quad	_ZN4node6worker12_GLOBAL__N_118SerializerDelegate23GetWasmModuleTransferIdEPN2v87IsolateENS3_5LocalINS3_16WasmModuleObjectEEE
	.quad	_ZN2v815ValueSerializer8Delegate22ReallocateBufferMemoryEPvmPm
	.quad	_ZN2v815ValueSerializer8Delegate16FreeBufferMemoryEPv
	.weak	_ZTVN4node6worker7MessageE
	.section	.data.rel.ro.local._ZTVN4node6worker7MessageE,"awG",@progbits,_ZTVN4node6worker7MessageE,comdat
	.align 8
	.type	_ZTVN4node6worker7MessageE, @object
	.size	_ZTVN4node6worker7MessageE, 72
_ZTVN4node6worker7MessageE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker7MessageD1Ev
	.quad	_ZN4node6worker7MessageD0Ev
	.quad	_ZNK4node6worker7Message10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6worker7Message14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6worker7Message8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node6worker15MessagePortDataE
	.section	.data.rel.ro.local._ZTVN4node6worker15MessagePortDataE,"awG",@progbits,_ZTVN4node6worker15MessagePortDataE,comdat
	.align 8
	.type	_ZTVN4node6worker15MessagePortDataE, @object
	.size	_ZTVN4node6worker15MessagePortDataE, 72
_ZTVN4node6worker15MessagePortDataE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker15MessagePortDataD1Ev
	.quad	_ZN4node6worker15MessagePortDataD0Ev
	.quad	_ZNK4node6worker15MessagePortData10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6worker15MessagePortData14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6worker15MessagePortData8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node6worker11MessagePortE
	.section	.data.rel.ro._ZTVN4node6worker11MessagePortE,"awG",@progbits,_ZTVN4node6worker11MessagePortE,comdat
	.align 8
	.type	_ZTVN4node6worker11MessagePortE, @object
	.size	_ZTVN4node6worker11MessagePortE, 112
_ZTVN4node6worker11MessagePortE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker11MessagePortD1Ev
	.quad	_ZN4node6worker11MessagePortD0Ev
	.quad	_ZNK4node6worker11MessagePort10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6worker11MessagePort14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6worker11MessagePort8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node6worker11MessagePort5CloseEN2v85LocalINS2_5ValueEEE
	.quad	_ZN4node6worker11MessagePort7OnCloseEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9MutexBaseINS0_16LibuvMutexTraitsEEESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC78:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC80:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = int; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.weak	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC81:
	.string	"../src/debug_utils-inl.h:76"
.LC82:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC81
	.quad	.LC82
	.quad	.LC80
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC83:
	.string	"../src/debug_utils-inl.h:113"
.LC84:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC81
	.quad	.LC82
	.quad	.LC85
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC86:
	.string	"../src/util-inl.h:374"
.LC87:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC89:
	.string	"../src/util-inl.h:325"
.LC90:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args
	.section	.rodata.str1.1
.LC92:
	.string	"../src/util.h:352"
.LC93:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 8; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args:
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.1
.LC95:
	.string	"../src/util.h:376"
.LC96:
	.string	"!IsInvalidated()"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 8; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EE25AllocateSufficientStorageEmE4args:
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC98:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC100:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.section	.rodata.str1.1
.LC101:
	.string	"../src/node_messaging.cc"
.LC102:
	.string	"messaging"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC101
	.quad	0
	.quad	_ZN4node6worker12_GLOBAL__N_1L13InitMessagingEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC102
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC103:
	.string	"../src/node_messaging.cc:923"
.LC104:
	.string	"(port) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"static void node::worker::MessagePort::MoveToContext(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6worker11MessagePort13MoveToContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.section	.rodata.str1.1
.LC106:
	.string	"../src/node_messaging.cc:880"
.LC107:
	.string	"args[0]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"static void node::worker::MessagePort::Stop(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6worker11MessagePort4StopERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.section	.rodata.str1.1
.LC109:
	.string	"../src/node_messaging.cc:699"
.LC110:
	.string	"data_"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"std::unique_ptr<node::worker::MessagePortData> node::worker::MessagePort::Detach()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker11MessagePort6DetachEvE4args, @object
	.size	_ZZN4node6worker11MessagePort6DetachEvE4args, 24
_ZZN4node6worker11MessagePort6DetachEvE4args:
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.section	.rodata.str1.1
.LC112:
	.string	"../src/node_messaging.cc:545"
	.section	.rodata.str1.8
	.align 8
.LC113:
	.string	"(uv_async_send(&async_)) == (0)"
	.align 8
.LC114:
	.string	"void node::worker::MessagePort::TriggerAsync()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args, @object
	.size	_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args, 24
_ZZN4node6worker11MessagePort12TriggerAsyncEvE4args:
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.section	.rodata.str1.1
.LC115:
	.string	"../src/node_messaging.cc:513"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"(uv_async_init(env->event_loop(), &async_, onmessage)) == (0)"
	.align 8
.LC117:
	.string	"node::worker::MessagePort::MessagePort(node::Environment*, v8::Local<v8::Context>, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEE4args, @object
	.size	_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEE4args, 24
_ZZN4node6worker11MessagePortC4EPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_6ObjectEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.section	.rodata.str1.1
.LC118:
	.string	"../src/node_messaging.cc:467"
.LC119:
	.string	"(b->sibling_) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"static void node::worker::MessagePortData::Entangle(node::worker::MessagePortData*, node::worker::MessagePortData*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args_0, @object
	.size	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args_0, 24
_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.section	.rodata.str1.1
.LC121:
	.string	"../src/node_messaging.cc:466"
.LC122:
	.string	"(a->sibling_) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args, @object
	.size	_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args, 24
_ZZN4node6worker15MessagePortData8EntangleEPS1_S2_E4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC120
	.section	.rodata.str1.1
.LC123:
	.string	"../src/node_messaging.cc:445"
.LC124:
	.string	"(owner_) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"virtual node::worker::MessagePortData::~MessagePortData()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker15MessagePortDataD4EvE4args, @object
	.size	_ZZN4node6worker15MessagePortDataD4EvE4args, 24
_ZZN4node6worker15MessagePortDataD4EvE4args:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.section	.rodata.str1.1
.LC126:
	.string	"../src/node_messaging.cc:429"
.LC127:
	.string	"(data.first) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"v8::Maybe<bool> node::worker::Message::Serialize(node::Environment*, v8::Local<v8::Context>, v8::Local<v8::Value>, const TransferList&, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_1, @object
	.size	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_1, 24
_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_1:
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.section	.rodata.str1.1
.LC129:
	.string	"../src/node_messaging.cc:417"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"env->isolate_data()->uses_node_allocator()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_0, @object
	.size	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_0, 24
_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args_0:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC128
	.section	.rodata.str1.1
.LC131:
	.string	"../src/node_messaging.cc:326"
.LC132:
	.string	"main_message_buf_.is_empty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args, @object
	.size	_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args, 24
_ZZN4node6worker7Message9SerializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEENS5_INS4_5ValueEEERKNS_16MaybeStackBufferIS9_Lm8EEENS5_INS4_6ObjectEEEE4args:
	.quad	.LC131
	.quad	.LC132
	.quad	.LC128
	.section	.rodata.str1.1
.LC133:
	.string	"../src/node_messaging.cc:213"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"domexception_ctor_val->IsFunction()"
	.align 8
.LC135:
	.string	"v8::MaybeLocal<v8::Function> node::worker::{anonymous}::GetDOMException(v8::Local<v8::Context>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEEE4args, @object
	.size	_ZZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEEE4args, 24
_ZZN4node6worker12_GLOBAL__N_115GetDOMExceptionEN2v85LocalINS2_7ContextEEEE4args:
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.section	.rodata.str1.1
.LC136:
	.string	"../src/node_messaging.cc:199"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"emit_message_val->IsFunction()"
	.align 8
.LC138:
	.string	"v8::MaybeLocal<v8::Function> node::worker::{anonymous}::GetEmitMessageFunction(v8::Local<v8::Context>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker12_GLOBAL__N_122GetEmitMessageFunctionEN2v85LocalINS2_7ContextEEEE4args, @object
	.size	_ZZN4node6worker12_GLOBAL__N_122GetEmitMessageFunctionEN2v85LocalINS2_7ContextEEEE4args, 24
_ZZN4node6worker12_GLOBAL__N_122GetEmitMessageFunctionEN2v85LocalINS2_7ContextEEEE4args:
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.section	.rodata.str1.1
.LC139:
	.string	"../src/node_messaging.cc:99"
.LC140:
	.string	"!IsCloseMessage()"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"v8::MaybeLocal<v8::Value> node::worker::Message::Deserialize(node::Environment*, v8::Local<v8::Context>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEEE4args, @object
	.size	_ZZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEEE4args, 24
_ZZN4node6worker7Message11DeserializeEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEEE4args:
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.section	.rodata.str1.1
.LC142:
	.string	"../src/node_messaging.cc:82"
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"(transfer_id) <= (wasm_modules_.size())"
	.align 8
.LC144:
	.string	"virtual v8::MaybeLocal<v8::WasmModuleObject> node::worker::{anonymous}::DeserializerDelegate::GetWasmModuleFromId(v8::Isolate*, uint32_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEjE4args, @object
	.size	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEjE4args, 24
_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate19GetWasmModuleFromIdEPN2v87IsolateEjE4args:
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.section	.rodata.str1.1
.LC145:
	.string	"../src/node_messaging.cc:76"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"(clone_id) <= (shared_array_buffers_.size())"
	.align 8
.LC147:
	.string	"virtual v8::MaybeLocal<v8::SharedArrayBuffer> node::worker::{anonymous}::DeserializerDelegate::GetSharedArrayBufferFromId(v8::Isolate*, uint32_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEjE4args, @object
	.size	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEjE4args, 24
_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate26GetSharedArrayBufferFromIdEPN2v87IsolateEjE4args:
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.section	.rodata.str1.1
.LC148:
	.string	"../src/node_messaging.cc:70"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"(id) <= (message_ports_.size())"
	.align 8
.LC150:
	.string	"virtual v8::MaybeLocal<v8::Object> node::worker::{anonymous}::DeserializerDelegate::ReadHostObject(v8::Isolate*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateEE4args, @object
	.size	_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateEE4args, 24
_ZZN4node6worker12_GLOBAL__N_120DeserializerDelegate14ReadHostObjectEPN2v87IsolateEE4args:
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0
	.section	.rodata.str1.8
	.align 8
.LC151:
	.string	"../src/memory_tracker-inl.h:269"
	.section	.rodata.str1.1
.LC152:
	.string	"(n->size_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"void node::MemoryTracker::Track(const node::MemoryRetainer*, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0:
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"../src/memory_tracker-inl.h:268"
	.section	.rodata.str1.1
.LC155:
	.string	"(CurrentNode()) == (n)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args:
	.quad	.LC154
	.quad	.LC155
	.quad	.LC153
	.weak	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args
	.section	.rodata.str1.8
	.align 8
.LC156:
	.string	"../src/memory_tracker-inl.h:27"
	.section	.rodata.str1.1
.LC157:
	.string	"(retainer_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC158:
	.string	"node::MemoryRetainerNode::MemoryRetainerNode(node::MemoryTracker*, const node::MemoryRetainer*)"
	.section	.data.rel.ro.local._ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,"awG",@progbits,_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,comdat
	.align 16
	.type	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, @gnu_unique_object
	.size	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, 24
_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC159:
	.string	"../src/debug_utils-inl.h:67"
.LC160:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC162:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC164:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.weak	_ZZN4node11Environment8AllocateEmE4args
	.section	.rodata.str1.1
.LC165:
	.string	"../src/env-inl.h:889"
.LC166:
	.string	"(ret) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"char* node::Environment::Allocate(size_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8AllocateEmE4args,"awG",@progbits,_ZZN4node11Environment8AllocateEmE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8AllocateEmE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8AllocateEmE4args, 24
_ZZN4node11Environment8AllocateEmE4args:
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC75:
	.quad	0
	.quad	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
