	.file	"node_native_module.cc"
	.text
	.section	.text._ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.type	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, @function
_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv:
.LFB2841:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2841:
	.size	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, .-_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.section	.text._ZN2v86String26ExternalStringResourceBase7DisposeEv,"axG",@progbits,_ZN2v86String26ExternalStringResourceBase7DisposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.type	_ZN2v86String26ExternalStringResourceBase7DisposeEv, @function
_ZN2v86String26ExternalStringResourceBase7DisposeEv:
.LFB2842:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE2842:
	.size	_ZN2v86String26ExternalStringResourceBase7DisposeEv, .-_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase4LockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase4LockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.type	_ZNK2v86String26ExternalStringResourceBase4LockEv, @function
_ZNK2v86String26ExternalStringResourceBase4LockEv:
.LFB2843:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2843:
	.size	_ZNK2v86String26ExternalStringResourceBase4LockEv, .-_ZNK2v86String26ExternalStringResourceBase4LockEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase6UnlockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase6UnlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.type	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, @function
_ZNK2v86String26ExternalStringResourceBase6UnlockEv:
.LFB2844:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2844:
	.size	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, .-_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.section	.text._ZNK4node32NonOwningExternalOneByteResource4dataEv,"axG",@progbits,_ZNK4node32NonOwningExternalOneByteResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node32NonOwningExternalOneByteResource4dataEv
	.type	_ZNK4node32NonOwningExternalOneByteResource4dataEv, @function
_ZNK4node32NonOwningExternalOneByteResource4dataEv:
.LFB4055:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE4055:
	.size	_ZNK4node32NonOwningExternalOneByteResource4dataEv, .-_ZNK4node32NonOwningExternalOneByteResource4dataEv
	.section	.text._ZNK4node32NonOwningExternalOneByteResource6lengthEv,"axG",@progbits,_ZNK4node32NonOwningExternalOneByteResource6lengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node32NonOwningExternalOneByteResource6lengthEv
	.type	_ZNK4node32NonOwningExternalOneByteResource6lengthEv, @function
_ZNK4node32NonOwningExternalOneByteResource6lengthEv:
.LFB4056:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE4056:
	.size	_ZNK4node32NonOwningExternalOneByteResource6lengthEv, .-_ZNK4node32NonOwningExternalOneByteResource6lengthEv
	.section	.text._ZNK4node32NonOwningExternalTwoByteResource4dataEv,"axG",@progbits,_ZNK4node32NonOwningExternalTwoByteResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node32NonOwningExternalTwoByteResource4dataEv
	.type	_ZNK4node32NonOwningExternalTwoByteResource4dataEv, @function
_ZNK4node32NonOwningExternalTwoByteResource4dataEv:
.LFB4067:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE4067:
	.size	_ZNK4node32NonOwningExternalTwoByteResource4dataEv, .-_ZNK4node32NonOwningExternalTwoByteResource4dataEv
	.section	.text._ZNK4node32NonOwningExternalTwoByteResource6lengthEv,"axG",@progbits,_ZNK4node32NonOwningExternalTwoByteResource6lengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node32NonOwningExternalTwoByteResource6lengthEv
	.type	_ZNK4node32NonOwningExternalTwoByteResource6lengthEv, @function
_ZNK4node32NonOwningExternalTwoByteResource6lengthEv:
.LFB4068:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE4068:
	.size	_ZNK4node32NonOwningExternalTwoByteResource6lengthEv, .-_ZNK4node32NonOwningExternalTwoByteResource6lengthEv
	.section	.text._ZN4node32NonOwningExternalTwoByteResourceD2Ev,"axG",@progbits,_ZN4node32NonOwningExternalTwoByteResourceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node32NonOwningExternalTwoByteResourceD2Ev
	.type	_ZN4node32NonOwningExternalTwoByteResourceD2Ev, @function
_ZN4node32NonOwningExternalTwoByteResourceD2Ev:
.LFB5879:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5879:
	.size	_ZN4node32NonOwningExternalTwoByteResourceD2Ev, .-_ZN4node32NonOwningExternalTwoByteResourceD2Ev
	.weak	_ZN4node32NonOwningExternalTwoByteResourceD1Ev
	.set	_ZN4node32NonOwningExternalTwoByteResourceD1Ev,_ZN4node32NonOwningExternalTwoByteResourceD2Ev
	.section	.text._ZN4node32NonOwningExternalOneByteResourceD2Ev,"axG",@progbits,_ZN4node32NonOwningExternalOneByteResourceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node32NonOwningExternalOneByteResourceD2Ev
	.type	_ZN4node32NonOwningExternalOneByteResourceD2Ev, @function
_ZN4node32NonOwningExternalOneByteResourceD2Ev:
.LFB5883:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5883:
	.size	_ZN4node32NonOwningExternalOneByteResourceD2Ev, .-_ZN4node32NonOwningExternalOneByteResourceD2Ev
	.weak	_ZN4node32NonOwningExternalOneByteResourceD1Ev
	.set	_ZN4node32NonOwningExternalOneByteResourceD1Ev,_ZN4node32NonOwningExternalOneByteResourceD2Ev
	.section	.text._ZN4node32NonOwningExternalOneByteResourceD0Ev,"axG",@progbits,_ZN4node32NonOwningExternalOneByteResourceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node32NonOwningExternalOneByteResourceD0Ev
	.type	_ZN4node32NonOwningExternalOneByteResourceD0Ev, @function
_ZN4node32NonOwningExternalOneByteResourceD0Ev:
.LFB5885:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5885:
	.size	_ZN4node32NonOwningExternalOneByteResourceD0Ev, .-_ZN4node32NonOwningExternalOneByteResourceD0Ev
	.section	.text._ZN4node32NonOwningExternalTwoByteResourceD0Ev,"axG",@progbits,_ZN4node32NonOwningExternalTwoByteResourceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node32NonOwningExternalTwoByteResourceD0Ev
	.type	_ZN4node32NonOwningExternalTwoByteResourceD0Ev, @function
_ZN4node32NonOwningExternalTwoByteResourceD0Ev:
.LFB5881:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5881:
	.size	_ZN4node32NonOwningExternalTwoByteResourceD0Ev, .-_ZN4node32NonOwningExternalTwoByteResourceD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoaderC2Ev
	.type	_ZN4node13native_module18NativeModuleLoaderC2Ev, @function
_ZN4node13native_module18NativeModuleLoaderC2Ev:
.LFB4416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%r12, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movb	$0, (%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 40(%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 80(%rdi)
	movq	%rax, 88(%rdi)
	leaq	112(%rdi), %rax
	movq	%rax, 128(%rdi)
	movq	%rax, 136(%rdi)
	leaq	200(%rdi), %rax
	addq	$208, %rdi
	movl	$0, -192(%rdi)
	movq	$0, -184(%rdi)
	movq	$0, -160(%rdi)
	movl	$0, -144(%rdi)
	movq	$0, -136(%rdi)
	movq	$0, -112(%rdi)
	movl	$0, -96(%rdi)
	movq	$0, -88(%rdi)
	movq	$0, -64(%rdi)
	movq	%rax, -56(%rdi)
	movq	$1, -48(%rdi)
	movq	$0, -40(%rdi)
	movq	$0, -32(%rdi)
	movl	$0x3f800000, -24(%rdi)
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	call	_ZN4node13native_module18NativeModuleLoader9GetConfigEv@PLT
	leaq	232(%r12), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L19
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node13native_module18NativeModuleLoader20LoadJavaScriptSourceEv@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4416:
	.size	_ZN4node13native_module18NativeModuleLoaderC2Ev, .-_ZN4node13native_module18NativeModuleLoaderC2Ev
	.globl	_ZN4node13native_module18NativeModuleLoaderC1Ev
	.set	_ZN4node13native_module18NativeModuleLoaderC1Ev,_ZN4node13native_module18NativeModuleLoaderC2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv
	.type	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv, @function
_ZN4node13native_module18NativeModuleLoader11GetInstanceEv:
.LFB4418:
	.cfi_startproc
	endbr64
	leaq	_ZN4node13native_module18NativeModuleLoader9instance_E(%rip), %rax
	ret
	.cfi_endproc
.LFE4418:
	.size	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv, .-_ZN4node13native_module18NativeModuleLoader11GetInstanceEv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader6ExistsEPKc
	.type	_ZN4node13native_module18NativeModuleLoader6ExistsEPKc, @function
_ZN4node13native_module18NativeModuleLoader6ExistsEPKc:
.LFB4419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rbx, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%rsi, %rsi
	je	.L52
	movq	%rdi, %r14
	leaq	112(%rdi), %r12
	movq	%rsi, %rdi
	movq	%rsi, %r15
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L53
	cmpq	$1, %rax
	jne	.L26
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%rbx, %rdx
.L27:
	movq	%rax, -88(%rbp)
	xorl	%r13d, %r13d
	movb	$0, (%rdx,%rax)
	movq	120(%r14), %r15
	movq	-96(%rbp), %r14
	testq	%r15, %r15
	je	.L29
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r14
	movq	%r12, %r9
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L35:
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.L31
.L30:
	movq	40(%r15), %r13
	movq	%rcx, %rdx
	cmpq	%rcx, %r13
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L32
	movq	32(%r15), %rdi
	movq	%r14, %rsi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
	testl	%eax, %eax
	jne	.L33
.L32:
	movq	%r13, %rax
	movl	$2147483648, %esi
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jge	.L34
	movabsq	$-2147483649, %rsi
	cmpq	%rsi, %rax
	jle	.L35
.L33:
	testl	%eax, %eax
	js	.L35
.L34:
	movq	%r15, %r9
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L30
.L31:
	xorl	%r13d, %r13d
	cmpq	%r9, %r12
	je	.L29
	movq	40(%r9), %r12
	cmpq	%r12, %rcx
	movq	%r12, %rdx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L37
	movq	32(%r9), %rsi
	movq	%r14, %rdi
	movq	%rcx, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L38
.L37:
	subq	%r12, %rcx
	movl	$1, %r13d
	cmpq	$2147483647, %rcx
	jg	.L29
	cmpq	$-2147483648, %rcx
	jl	.L44
	movl	%ecx, %r13d
.L38:
	notl	%r13d
	shrl	$31, %r13d
.L29:
	cmpq	%rbx, %r14
	je	.L22
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L22:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L55
	movq	%rbx, %rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L25:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L44:
	xorl	%r13d, %r13d
	jmp	.L29
.L54:
	call	__stack_chk_fail@PLT
.L55:
	movq	%rbx, %rdi
	jmp	.L25
	.cfi_endproc
.LFE4419:
	.size	_ZN4node13native_module18NativeModuleLoader6ExistsEPKc, .-_ZN4node13native_module18NativeModuleLoader6ExistsEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader15GetSourceObjectEN2v85LocalINS2_7ContextEEE
	.type	_ZN4node13native_module18NativeModuleLoader15GetSourceObjectEN2v85LocalINS2_7ContextEEE, @function
_ZN4node13native_module18NativeModuleLoader15GetSourceObjectEN2v85LocalINS2_7ContextEEE:
.LFB4420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	128(%r12), %r15
	movq	%rax, %r14
	leaq	112(%r12), %rax
	movq	%rax, -64(%rbp)
	cmpq	%r15, %rax
	je	.L68
	.p2align 4,,10
	.p2align 3
.L67:
	movq	32(%r15), %rsi
	movl	40(%r15), %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L72
.L58:
	movq	64(%r15), %rcx
	testq	%rcx, %rcx
	movq	%rcx, -56(%rbp)
	je	.L59
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	leaq	16+_ZTVN4node32NonOwningExternalOneByteResourceE(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	80(%r15), %rax
	movq	%rcx, 8(%rsi)
	movq	%rdx, (%rsi)
	movq	%rax, 16(%rsi)
	call	_ZN2v86String18NewExternalOneByteEPNS_7IsolateEPNS0_29ExternalOneByteStringResourceE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L71
.L61:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L73
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -64(%rbp)
	jne	.L67
.L68:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	72(%r15), %rcx
	testq	%rcx, %rcx
	je	.L74
	movl	$24, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	leaq	16+_ZTVN4node32NonOwningExternalTwoByteResourceE(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	80(%r15), %rax
	movq	%rcx, 8(%rsi)
	movq	%rdx, (%rsi)
	movq	%rax, 16(%rsi)
	call	_ZN2v86String18NewExternalTwoByteEPNS_7IsolateEPNS0_22ExternalStringResourceE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L61
.L71:
	movq	%rcx, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L72:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L73:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	-64(%rbp), %rax
	jne	.L67
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	_ZZNK4node10UnionBytes14two_bytes_dataEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE4420:
	.size	_ZN4node13native_module18NativeModuleLoader15GetSourceObjectEN2v85LocalINS2_7ContextEEE, .-_ZN4node13native_module18NativeModuleLoader15GetSourceObjectEN2v85LocalINS2_7ContextEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader15GetConfigStringEPN2v87IsolateE
	.type	_ZN4node13native_module18NativeModuleLoader15GetConfigStringEPN2v87IsolateE, @function
_ZN4node13native_module18NativeModuleLoader15GetConfigStringEPN2v87IsolateE:
.LFB4421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	208(%rdi), %r13
	testq	%r13, %r13
	je	.L76
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node32NonOwningExternalOneByteResourceE(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	224(%rbx), %rax
	movq	%rdx, (%rsi)
	movq	%r13, 8(%rsi)
	movq	%rax, 16(%rsi)
	call	_ZN2v86String18NewExternalOneByteEPNS_7IsolateEPNS0_29ExternalOneByteStringResourceE@PLT
	testq	%rax, %rax
	je	.L82
.L78:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	216(%rdi), %r13
	testq	%r13, %r13
	je	.L83
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node32NonOwningExternalTwoByteResourceE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	224(%rbx), %rax
	movq	%rcx, (%rsi)
	movq	%r13, 8(%rsi)
	movq	%rax, 16(%rsi)
	call	_ZN2v86String18NewExternalTwoByteEPNS_7IsolateEPNS0_22ExternalStringResourceE@PLT
	testq	%rax, %rax
	jne	.L78
.L82:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	_ZZNK4node10UnionBytes14two_bytes_dataEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE4421:
	.size	_ZN4node13native_module18NativeModuleLoader15GetConfigStringEPN2v87IsolateE, .-_ZN4node13native_module18NativeModuleLoader15GetConfigStringEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader10code_cacheB5cxx11Ev
	.type	_ZN4node13native_module18NativeModuleLoader10code_cacheB5cxx11Ev, @function
_ZN4node13native_module18NativeModuleLoader10code_cacheB5cxx11Ev:
.LFB4447:
	.cfi_startproc
	endbr64
	leaq	152(%rdi), %rax
	ret
	.cfi_endproc
.LFE4447:
	.size	_ZN4node13native_module18NativeModuleLoader10code_cacheB5cxx11Ev, .-_ZN4node13native_module18NativeModuleLoader10code_cacheB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node13native_module18NativeModuleLoader12GetCodeCacheEPKc
	.type	_ZNK4node13native_module18NativeModuleLoader12GetCodeCacheEPKc, @function
_ZNK4node13native_module18NativeModuleLoader12GetCodeCacheEPKc:
.LFB4448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	232(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	-96(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%r15, %r15
	je	.L120
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L121
	cmpq	$1, %rax
	jne	.L89
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L90:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-88(%rbp), %rsi
	movl	$3339675911, %edx
	movq	-96(%rbp), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	160(%rbx), %r12
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%r12
	movq	152(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L91
	movq	(%rax), %rbx
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdi
	movq	48(%rbx), %rcx
	testq	%r9, %r9
	je	.L92
.L96:
	cmpq	%rcx, %r15
	je	.L122
.L93:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L95
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r12
	cmpq	%rdx, %r8
	je	.L96
.L95:
	cmpq	%r13, %rdi
	je	.L98
	call	_ZdlPv@PLT
	xorl	%r12d, %r12d
.L99:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	cmpq	%rcx, %r15
	je	.L124
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L95
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r12
	cmpq	%rdx, %r8
	jne	.L95
	cmpq	%rcx, %r15
	jne	.L97
.L124:
	cmpq	$0, 16(%rbx)
	jne	.L97
	cmpq	%r13, %rdi
	jne	.L125
.L100:
	movq	40(%rbx), %r12
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L89:
	testq	%rax, %rax
	jne	.L126
	movq	%r13, %rdx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L122:
	cmpq	16(%rbx), %r9
	jne	.L93
	movq	8(%rbx), %rsi
	movq	%r9, %rdx
	movq	%r8, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %rdi
	movq	-128(%rbp), %r9
	testl	%eax, %eax
	movq	-136(%rbp), %r8
	jne	.L93
	cmpq	%r13, %rdi
	je	.L100
	.p2align 4,,10
	.p2align 3
.L125:
	call	_ZdlPv@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L88:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%r12d, %r12d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-96(%rbp), %rdi
	jmp	.L95
.L123:
	call	__stack_chk_fail@PLT
.L126:
	movq	%r13, %rdi
	jmp	.L88
	.cfi_endproc
.LFE4448:
	.size	_ZNK4node13native_module18NativeModuleLoader12GetCodeCacheEPKc, .-_ZNK4node13native_module18NativeModuleLoader12GetCodeCacheEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKc
	.type	_ZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKc, @function
_ZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKc:
.LFB4450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -96(%rbp)
	testq	%rdx, %rdx
	je	.L166
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L167
	cmpq	$1, %rax
	jne	.L131
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L132:
	movq	%rax, -88(%rbp)
	leaq	112(%rbx), %r11
	movb	$0, (%rdx,%rax)
	movq	120(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L133
	movq	-88(%rbp), %r12
	movq	-96(%rbp), %r14
	movq	%r11, %r15
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L139:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L135
.L134:
	movq	40(%rbx), %r9
	movq	%r12, %rdx
	cmpq	%r12, %r9
	cmovbe	%r9, %rdx
	testq	%rdx, %rdx
	je	.L136
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %r9
	testl	%eax, %eax
	jne	.L137
.L136:
	movq	%r9, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L138
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L139
.L137:
	testl	%eax, %eax
	js	.L139
.L138:
	movq	%rbx, %r15
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L134
.L135:
	cmpq	%r15, %r11
	je	.L141
	movq	40(%r15), %rbx
	cmpq	%rbx, %r12
	movq	%rbx, %rdx
	cmovbe	%r12, %rdx
	testq	%rdx, %rdx
	je	.L142
	movq	32(%r15), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L143
.L142:
	movq	%r12, %rcx
	subq	%rbx, %rcx
	cmpq	$2147483647, %rcx
	jg	.L144
	cmpq	$-2147483648, %rcx
	jl	.L141
	movl	%ecx, %eax
.L143:
	testl	%eax, %eax
	js	.L141
.L144:
	cmpq	%r13, %r14
	jne	.L168
.L151:
	movq	64(%r15), %rbx
	testq	%rbx, %rbx
	je	.L145
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node32NonOwningExternalOneByteResourceE(%rip), %rcx
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	movq	80(%r15), %rax
	movq	%rcx, (%rsi)
	movq	%rbx, 8(%rsi)
	movq	%rax, 16(%rsi)
	call	_ZN2v86String18NewExternalOneByteEPNS_7IsolateEPNS0_29ExternalOneByteStringResourceE@PLT
	testq	%rax, %rax
	je	.L165
.L147:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L169
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L170
	movq	%r13, %rdx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-96(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L141:
	cmpq	%r13, %r14
	jne	.L171
.L150:
	leaq	_ZZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movq	72(%r15), %rbx
	testq	%rbx, %rbx
	je	.L172
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node32NonOwningExternalTwoByteResourceE(%rip), %rcx
	movq	-136(%rbp), %rdi
	movq	%rax, %rsi
	movq	80(%r15), %rax
	movq	%rcx, (%rsi)
	movq	%rbx, 8(%rsi)
	movq	%rax, 16(%rsi)
	call	_ZN2v86String18NewExternalTwoByteEPNS_7IsolateEPNS0_22ExternalStringResourceE@PLT
	testq	%rax, %rax
	jne	.L147
.L165:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	-96(%rbp), %r15
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L130:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	_ZZNK4node10UnionBytes14two_bytes_dataEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L151
.L169:
	call	__stack_chk_fail@PLT
.L171:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L150
.L170:
	movq	%r13, %rdi
	jmp	.L130
	.cfi_endproc
.LFE4450:
	.size	_ZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKc, .-_ZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKc
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB5150:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L188
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L177:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L175
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L173
.L176:
	movq	%rbx, %r12
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L176
.L173:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE5150:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E:
.LFB5158:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L195:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L193
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L191
.L194:
	movq	%rbx, %r12
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L194
.L191:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE5158:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.section	.text._ZN4node13native_module18NativeModuleLoaderD2Ev,"axG",@progbits,_ZN4node13native_module18NativeModuleLoaderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13native_module18NativeModuleLoaderD2Ev
	.type	_ZN4node13native_module18NativeModuleLoaderD2Ev, @function
_ZN4node13native_module18NativeModuleLoaderD2Ev:
.LFB5888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$232, %rdi
	call	uv_mutex_destroy@PLT
	movq	168(%rbx), %r12
	testq	%r12, %r12
	jne	.L214
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L268:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L210
.L213:
	movq	%r13, %r12
.L214:
	movq	40(%r12), %r14
	movq	(%r12), %r13
	testq	%r14, %r14
	je	.L211
	movq	%r14, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L211:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L268
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L213
.L210:
	movq	160(%rbx), %rax
	movq	152(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	152(%rbx), %rdi
	leaq	200(%rbx), %rax
	movq	$0, 176(%rbx)
	movq	$0, 168(%rbx)
	cmpq	%rax, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	120(%rbx), %r12
	leaq	104(%rbx), %r14
	testq	%r12, %r12
	je	.L216
.L219:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N4node10UnionBytesEESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L217
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L216
.L218:
	movq	%r13, %r12
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L218
.L216:
	movq	72(%rbx), %r12
	leaq	56(%rbx), %r14
	testq	%r12, %r12
	je	.L220
.L223:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L221
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L220
.L222:
	movq	%r13, %r12
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L222
.L220:
	movq	24(%rbx), %r12
	leaq	8(%rbx), %r13
	testq	%r12, %r12
	je	.L209
.L227:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L225
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L209
.L226:
	movq	%rbx, %r12
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L226
.L209:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5888:
	.size	_ZN4node13native_module18NativeModuleLoaderD2Ev, .-_ZN4node13native_module18NativeModuleLoaderD2Ev
	.weak	_ZN4node13native_module18NativeModuleLoaderD1Ev
	.set	_ZN4node13native_module18NativeModuleLoaderD1Ev,_ZN4node13native_module18NativeModuleLoaderD2Ev
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB5191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L311
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L291
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L312
	movabsq	$9223372036854775776, %rcx
.L271:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L289:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L273
	testq	%r10, %r10
	je	.L313
.L273:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L314
	cmpq	$1, %r9
	jne	.L276
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L277:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L293
.L318:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L309:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L315
.L282:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L279
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L312:
	testq	%rcx, %rcx
	jne	.L272
	xorl	%r13d, %r13d
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L278:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L283
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L316
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L287
.L285:
	subq	%rbx, %r12
	addq	%r12, %r8
.L283:
	testq	%r14, %r14
	je	.L288
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L288:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L317
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L287
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L291:
	movl	$32, %ecx
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L276:
	testq	%r9, %r9
	jne	.L275
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L318
.L293:
	movq	%r13, %r8
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %rdi
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L275:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L277
.L313:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L272:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L271
.L317:
	call	__stack_chk_fail@PLT
.L311:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5191:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"vector::reserve"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader12GetModuleIdsB5cxx11Ev
	.type	_ZN4node13native_module18NativeModuleLoader12GetModuleIdsB5cxx11Ev, @function
_ZN4node13native_module18NativeModuleLoader12GetModuleIdsB5cxx11Ev:
.LFB4422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	144(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movabsq	$288230376151711743, %rax
	movups	%xmm0, (%rdi)
	cmpq	%rax, %r12
	ja	.L355
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	testq	%r12, %r12
	jne	.L356
.L321:
	movq	128(%r14), %r12
	addq	$112, %r14
	cmpq	%r14, %r12
	je	.L319
	leaq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L359:
	leaq	16(%rbx), %rdi
	movq	40(%r12), %r13
	movq	%rdi, (%rbx)
	movq	32(%r12), %r9
	movq	%r9, %rax
	addq	%r13, %rax
	je	.L330
	testq	%r9, %r9
	je	.L357
.L330:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L358
	cmpq	$1, %r13
	jne	.L333
	movzbl	(%r9), %eax
	movb	%al, 16(%rbx)
.L334:
	movq	%r13, 8(%rbx)
	movb	$0, (%rdi,%r13)
	movq	%r12, %rdi
	addq	$32, 8(%r15)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L319
.L360:
	movq	8(%r15), %rbx
	movq	16(%r15), %rsi
.L336:
	cmpq	%rbx, %rsi
	jne	.L359
	leaq	32(%r12), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L360
.L319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L334
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L358:
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r9, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r9
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
.L332:
	movq	%r13, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	(%rbx), %rdi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L356:
	salq	$5, %r12
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	(%r15), %r9
	movq	8(%r15), %rdi
	movq	%rax, %rbx
	movq	%r9, %rax
	cmpq	%r9, %rdi
	je	.L322
	movq	%rbx, %rdx
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L354:
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %rdi
	je	.L322
.L326:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	jne	.L323
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L322:
	testq	%r9, %r9
	je	.L327
	movq	%r9, %rdi
	call	_ZdlPv@PLT
.L327:
	movq	%rbx, %xmm0
	leaq	(%rbx,%r12), %rsi
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, 16(%r15)
	movups	%xmm0, (%r15)
	jmp	.L321
.L357:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L355:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4422:
	.size	_ZN4node13native_module18NativeModuleLoader12GetModuleIdsB5cxx11Ev, .-_ZN4node13native_module18NativeModuleLoader12GetModuleIdsB5cxx11Ev
	.section	.text._ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_,"axG",@progbits,_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_
	.type	_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_, @function
_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_:
.LFB5235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%r12, -56(%rbp)
	testq	%r13, %r13
	je	.L363
	movq	8(%rsi), %r15
	movq	(%rsi), %r14
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L369:
	movq	24(%r13), %r13
	testq	%r13, %r13
	je	.L365
.L364:
	movq	40(%r13), %rbx
	movq	%r15, %rdx
	cmpq	%r15, %rbx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L366
	movq	32(%r13), %rdi
	movq	%r14, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L367
.L366:
	subq	%r15, %rbx
	movl	$2147483648, %eax
	cmpq	%rax, %rbx
	jge	.L368
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rbx
	jle	.L369
	movl	%ebx, %eax
.L367:
	testl	%eax, %eax
	js	.L369
.L368:
	movq	%r13, %r12
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.L364
.L365:
	cmpq	%r12, -56(%rbp)
	je	.L363
	movq	40(%r12), %rbx
	cmpq	%rbx, %r15
	movq	%rbx, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L371
	movq	32(%r12), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L372
.L371:
	movq	%r15, %r8
	subq	%rbx, %r8
	cmpq	$2147483647, %r8
	jg	.L363
	cmpq	$-2147483648, %r8
	jl	.L374
	movl	%r8d, %eax
.L372:
	testl	%eax, %eax
	cmovs	-56(%rbp), %r12
.L363:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L374:
	.cfi_restore_state
	movq	-56(%rbp), %r12
	jmp	.L363
	.cfi_endproc
.LFE5235:
	.size	_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_, .-_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_:
.LFB5411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L408
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L390:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L386
.L409:
	movq	%rax, %r15
.L385:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L387
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L388
.L387:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L389
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L390
.L388:
	testl	%eax, %eax
	js	.L390
.L389:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L409
.L386:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L384
.L392:
	testq	%rdx, %rdx
	je	.L395
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L396
.L395:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L397
	cmpq	$-2147483648, %rcx
	jl	.L398
	movl	%ecx, %eax
.L396:
	testl	%eax, %eax
	js	.L398
.L397:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L384:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L410
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L410:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5411:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_:
.LFB5214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	0(%r13), %r15
	movq	8(%r13), %r13
	leaq	48(%rax), %r14
	movq	%rax, %r12
	leaq	32(%rax), %r8
	movq	%r14, 32(%rax)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L412
	testq	%r15, %r15
	je	.L439
.L412:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L440
	cmpq	$1, %r13
	jne	.L415
	movzbl	(%r15), %eax
	movb	%al, 48(%r12)
	movq	%r14, %rax
.L416:
	movq	%r13, 40(%r12)
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movb	$0, (%rax,%r13)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rax, %r15
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L417
	leaq	8(%rbx), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L441
.L418:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	movq	%r12, %rax
	movl	$1, %edx
.L421:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L442
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L443
	movq	%r14, %rax
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L414:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%r12), %rax
	movq	-72(%rbp), %r8
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L417:
	movq	32(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %rax
	xorl	%edx, %edx
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L441:
	cmpq	%rcx, %rdx
	je	.L418
	movq	40(%r12), %r14
	movq	40(%rdx), %r15
	cmpq	%r15, %r14
	movq	%r15, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L419
	movq	32(%r12), %rdi
	movq	32(%r13), %rsi
	movq	%rcx, -72(%rbp)
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L420
.L419:
	subq	%r15, %r14
	xorl	%edi, %edi
	cmpq	$2147483647, %r14
	jg	.L418
	cmpq	$-2147483648, %r14
	jl	.L428
	movl	%r14d, %edi
.L420:
	shrl	$31, %edi
	jmp	.L418
.L439:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$1, %edi
	jmp	.L418
.L442:
	call	__stack_chk_fail@PLT
.L443:
	movq	%r14, %rdi
	jmp	.L414
	.cfi_endproc
.LFE5214:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE:
.LFB5475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	48(%r12), %rax
	movq	%rdi, %rbx
	movq	8(%rdi), %rsi
	movq	(%rbx), %r8
	divq	%rsi
	leaq	(%r8,%rdx,8), %r9
	movq	%rdx, %rdi
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L445
	movq	(%r12), %r13
	cmpq	%rcx, %rax
	je	.L461
	testq	%r13, %r13
	je	.L448
	movq	48(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L448
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r13
.L448:
	movq	40(%r12), %r14
	movq	%r13, (%rcx)
	testq	%r14, %r14
	je	.L450
	movq	%r14, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L450:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L452
	movq	48(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L448
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r9), %rax
.L447:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L462
.L449:
	movq	$0, (%r9)
	movq	(%r12), %r13
	jmp	.L448
.L452:
	movq	%rcx, %rax
	jmp	.L447
.L462:
	movq	%r13, 16(%rbx)
	jmp	.L449
	.cfi_endproc
.LFE5475:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB5492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$56, %edi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%r14), %r14
	movq	%rax, %r12
	movq	$0, (%rax)
	addq	$8, %rax
	leaq	24(%r12), %r15
	movq	%rax, -72(%rbp)
	movq	%r15, 8(%r12)
	testq	%r14, %r14
	je	.L516
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L517
	cmpq	$1, %rax
	jne	.L467
	movzbl	(%r14), %edx
	movb	%dl, 24(%r12)
	movq	%r15, %rdx
.L468:
	movq	%rax, 16(%r12)
	movb	$0, (%rdx,%rax)
	movq	0(%r13), %rax
	movl	$3339675911, %edx
	movq	$0, 0(%r13)
	movq	16(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rax, 40(%r12)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r9
	xorl	%edx, %edx
	movq	%rax, %r14
	divq	%r9
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %r10
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L469
	movq	(%rax), %r13
	movq	48(%r13), %rcx
.L472:
	cmpq	%rcx, %r14
	je	.L518
.L470:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L469
	movq	48(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r9
	cmpq	%rdx, %r10
	je	.L472
.L469:
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	movq	%r9, %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L475
	movq	(%rbx), %r15
.L476:
	movq	%r14, 48(%r12)
	movq	-72(%rbp), %r14
	addq	%r15, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L485
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%r14), %rax
	movq	%r12, (%rax)
.L486:
	addq	$1, 24(%rbx)
	movq	%r12, %rax
	movl	$1, %edx
.L474:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L519
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L520
	movq	%r15, %rdx
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L518:
	movq	16(%r12), %rdx
	cmpq	16(%r13), %rdx
	jne	.L470
	movq	%r10, -88(%rbp)
	movq	8(%r12), %rdi
	movq	%r9, -80(%rbp)
	testq	%rdx, %rdx
	je	.L471
	movq	8(%r13), %rsi
	movq	%rdi, -96(%rbp)
	call	memcmp@PLT
	movq	-96(%rbp), %rdi
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	movq	-88(%rbp), %r10
	jne	.L470
.L471:
	movq	40(%r12), %r14
	testq	%r14, %r14
	je	.L489
	movq	%r14, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movq	%r14, %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	8(%r12), %rdi
.L489:
	cmpq	%rdi, %r15
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rax
	xorl	%edx, %edx
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L517:
	leaq	8(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%r12)
.L466:
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %rax
	movq	8(%r12), %rdx
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L516:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L475:
	cmpq	$1, %rdx
	je	.L521
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L522
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r10
.L478:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L480
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L482:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%r11), %rax
	movq	%rcx, (%rax)
.L483:
	testq	%rsi, %rsi
	je	.L480
.L481:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r13
	leaq	(%r15,%rdx,8), %r11
	movq	(%r11), %rax
	testq	%rax, %rax
	jne	.L482
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%r11)
	cmpq	$0, (%rcx)
	je	.L492
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L481
	.p2align 4,,10
	.p2align 3
.L480:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%rbx)
	divq	%r13
	movq	%r15, (%rbx)
	leaq	0(,%rdx,8), %rax
	movq	%rax, -72(%rbp)
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L485:
	movq	16(%rbx), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L487
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r12, (%r15,%rdx,8)
.L487:
	leaq	16(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%rdx, %rdi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L521:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r10
	jmp	.L478
.L519:
	call	__stack_chk_fail@PLT
.L522:
	call	_ZSt17__throw_bad_allocv@PLT
.L520:
	movq	%r15, %rdi
	jmp	.L466
	.cfi_endproc
.LFE5492:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultE
	.type	_ZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultE, @function
_ZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultE:
.LFB4451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -248(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%r8, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-248(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKc
	movq	%rax, -264(%rbp)
	testq	%rax, %rax
	je	.L600
	movq	-248(%rbp), %rcx
	leaq	-80(%rbp), %rax
	movb	$115, -78(%rbp)
	leaq	-96(%rbp), %r15
	movq	%rax, -256(%rbp)
	movq	%rcx, %rdi
	movq	%rax, -96(%rbp)
	movl	$27182, %eax
	movq	%rcx, -288(%rbp)
	movw	%ax, -80(%rbp)
	movq	$3, -88(%rbp)
	movb	$0, -77(%rbp)
	call	strlen@PLT
	movq	-288(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	-112(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	leaq	16(%rax), %rdx
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L601
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L526:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	movq	$0, 8(%rax)
	cmpq	-256(%rbp), %rdi
	je	.L527
	call	_ZdlPv@PLT
.L527:
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L602
.L528:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movl	$0, -288(%rbp)
	movq	%rax, -328(%rbp)
	cmpq	$-112, %rbx
	je	.L529
	leaq	112(%rbx), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	%al, %eax
	movl	%eax, -288(%rbp)
.L529:
	leaq	232(%r12), %rbx
	movq	%rbx, %rdi
	call	uv_mutex_lock@PLT
	movq	-256(%rbp), %rax
	movq	-248(%rbp), %r8
	movq	%rax, -96(%rbp)
	testq	%r8, %r8
	jne	.L603
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L603:
	movq	%r8, %rdi
	movq	%r8, -296(%rbp)
	call	strlen@PLT
	movq	-296(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -192(%rbp)
	movq	%rax, %r9
	ja	.L604
	cmpq	$1, %rax
	jne	.L531
	movzbl	(%r8), %edx
	leaq	-192(%rbp), %rsi
	movq	%rsi, -304(%rbp)
	movb	%dl, -80(%rbp)
	movq	-256(%rbp), %rdx
.L532:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-88(%rbp), %rsi
	movl	$3339675911, %edx
	leaq	152(%r12), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -296(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	160(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rcx
	movq	152(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L533
	movq	(%rax), %r12
	movq	-88(%rbp), %r15
	movq	-96(%rbp), %rdi
	movq	48(%r12), %rsi
	testq	%r15, %r15
	je	.L534
.L538:
	cmpq	%rsi, %r8
	je	.L605
.L535:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L537
	movq	48(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	je	.L538
.L537:
	cmpq	-256(%rbp), %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	uv_mutex_unlock@PLT
	movl	$2, %eax
.L543:
	movq	-272(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movq	-264(%rbp), %xmm0
	movl	-288(%rbp), %esi
	movq	%r15, -136(%rbp)
	movq	(%rdx), %rcx
	movq	8(%rdx), %rdx
	pushq	$0
	movhps	-312(%rbp), %xmm0
	pushq	%rax
	movaps	%xmm0, -192(%rbp)
	subq	%rcx, %rdx
	movq	-320(%rbp), %xmm0
	movl	%esi, -160(%rbp)
	movq	-304(%rbp), %rsi
	sarq	$3, %rdx
	movhps	-328(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -152(%rbp)
	call	_ZN2v814ScriptCompiler24CompileFunctionInContextENS_5LocalINS_7ContextEEEPNS0_6SourceEmPNS1_INS_6StringEEEmPNS1_INS_6ObjectEEENS0_14CompileOptionsENS0_13NoCacheReasonE@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L606
	movl	$1, %eax
	testq	%r15, %r15
	je	.L551
	movq	-136(%rbp), %rax
	movzbl	12(%rax), %eax
.L551:
	movq	-280(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, (%rsi)
	call	_ZN2v814ScriptCompiler26CreateCodeCacheForFunctionENS_5LocalINS_8FunctionEEE@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L607
	movq	%rbx, %rdi
	call	uv_mutex_lock@PLT
	movq	-296(%rbp), %rdi
	leaq	-248(%rbp), %rsi
	leaq	-232(%rbp), %rdx
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJRPKcSE_EEES6_INSH_14_Node_iteratorISF_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	%rbx, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-232(%rbp), %r12
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L550
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L550:
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L548
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L548:
	movq	-128(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L557
	call	_ZdlPv@PLT
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L600:
	xorl	%r13d, %r13d
.L557:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L608
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	cmpq	%rsi, %r8
	je	.L609
	.p2align 4,,10
	.p2align 3
.L539:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L537
	movq	48(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L537
	cmpq	%rsi, %r8
	jne	.L539
.L609:
	cmpq	$0, 16(%r12)
	jne	.L539
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L531:
	testq	%r9, %r9
	jne	.L610
	leaq	-192(%rbp), %rsi
	movq	-256(%rbp), %rdx
	movq	%rsi, -304(%rbp)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L601:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L605:
	cmpq	16(%r12), %r15
	jne	.L535
	movq	8(%r12), %rsi
	movq	%r15, %rdx
	movq	%r9, -368(%rbp)
	movq	%rcx, -360(%rbp)
	movq	%r8, -352(%rbp)
	movq	%rdi, -344(%rbp)
	call	memcmp@PLT
	movq	-344(%rbp), %rdi
	movq	-352(%rbp), %r8
	testl	%eax, %eax
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %r9
	jne	.L535
.L536:
	cmpq	-256(%rbp), %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	40(%r12), %r15
	movq	-296(%rbp), %rdi
	movq	%r12, %rsi
	movq	$0, 40(%r12)
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St10unique_ptrIN2v814ScriptCompiler10CachedDataESt14default_deleteISB_EEESaISF_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSH_18_Mod_range_hashingENSH_20_Default_ranged_hashENSH_20_Prime_rehash_policyENSH_17_Hashtable_traitsILb1ELb0ELb1EEEE5eraseENSH_20_Node_const_iteratorISF_Lb0ELb1EEE
	movq	%rbx, %rdi
	call	uv_mutex_unlock@PLT
	xorl	%eax, %eax
	testq	%r15, %r15
	sete	%al
	addl	$1, %eax
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L604:
	leaq	-192(%rbp), %rax
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r9, -344(%rbp)
	movq	%rax, %rsi
	movq	%r8, -296(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-296(%rbp), %r8
	movq	-344(%rbp), %r9
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-192(%rbp), %rax
	movq	%rax, -80(%rbp)
.L530:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-192(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L606:
	xorl	%r13d, %r13d
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L602:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L533:
	movq	-96(%rbp), %rdi
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L607:
	leaq	_ZZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L608:
	call	__stack_chk_fail@PLT
.L610:
	leaq	-192(%rbp), %rax
	movq	-256(%rbp), %rdi
	movq	%rax, -304(%rbp)
	jmp	.L530
	.cfi_endproc
.LFE4451:
	.size	_ZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultE, .-_ZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultE
	.section	.rodata.str1.1
.LC4:
	.string	"exports"
.LC5:
	.string	"require"
.LC6:
	.string	"module"
.LC7:
	.string	"process"
.LC8:
	.string	"internalBinding"
.LC9:
	.string	"primordials"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader15CompileAsModuleEN2v85LocalINS2_7ContextEEEPKcPNS1_6ResultE
	.type	_ZN4node13native_module18NativeModuleLoader15CompileAsModuleEN2v85LocalINS2_7ContextEEEPKcPNS1_6ResultE, @function
_ZN4node13native_module18NativeModuleLoader15CompileAsModuleEN2v85LocalINS2_7ContextEEEPKcPNS1_6ResultE:
.LFB4449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L624
.L612:
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L625
.L613:
	xorl	%edx, %edx
	movl	$6, %ecx
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L626
.L614:
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L627
.L615:
	xorl	%edx, %edx
	movl	$15, %ecx
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L628
.L616:
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L629
.L617:
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	$0, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%rbx, %r8
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	48(%rax), %rdx
	movq	%r13, %rsi
	movq	%rdx, -128(%rbp)
	leaq	-144(%rbp), %rcx
	movq	%rdx, -136(%rbp)
	movq	%r15, %rdx
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -144(%rbp)
	call	_ZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultE
	movq	-144(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L618
	call	_ZdlPv@PLT
.L618:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L630
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L626:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L628:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L617
.L630:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4449:
	.size	_ZN4node13native_module18NativeModuleLoader15CompileAsModuleEN2v85LocalINS2_7ContextEEEPKcPNS1_6ResultE, .-_ZN4node13native_module18NativeModuleLoader15CompileAsModuleEN2v85LocalINS2_7ContextEEEPKcPNS1_6ResultE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_:
.LFB5567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L675
	movq	8(%rdx), %r14
	movq	40(%rsi), %r15
	movq	%rsi, %rbx
	movq	32(%rsi), %r9
	movq	0(%r13), %r8
	cmpq	%r15, %r14
	movq	%r15, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L638
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	movq	-72(%rbp), %rdx
	jne	.L676
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$2147483647, %rax
	jg	.L657
.L658:
	cmpq	$-2147483648, %rax
	jl	.L641
	testl	%eax, %eax
	js	.L641
	testq	%rdx, %rdx
	je	.L648
.L657:
	movq	%r8, %rsi
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L649
.L648:
	subq	%r14, %r15
	cmpq	$2147483647, %r15
	jg	.L650
	cmpq	$-2147483648, %r15
	jl	.L651
	movl	%r15d, %eax
.L649:
	testl	%eax, %eax
	js	.L651
.L650:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L667:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$2147483647, %rax
	jle	.L658
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L676:
	jns	.L657
.L641:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%r12)
	je	.L667
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %rcx
	movq	%rax, %r15
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L644
	movq	-56(%rbp), %r8
	movq	32(%rax), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, %rsi
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	jne	.L645
.L644:
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L633
	cmpq	$-2147483648, %rcx
	jl	.L646
	movl	%ecx, %eax
.L645:
	testl	%eax, %eax
	jns	.L633
.L646:
	cmpq	$0, 24(%r15)
	movl	$0, %eax
	cmovne	%rbx, %rax
	cmove	%r15, %rbx
	addq	$40, %rsp
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L675:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L633
	movq	32(%rdi), %rbx
	movq	8(%rdx), %r15
	movq	40(%rbx), %r14
	movq	%r15, %rdx
	cmpq	%r15, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L634
	movq	32(%rbx), %rdi
	movq	0(%r13), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L635
.L634:
	subq	%r15, %r14
	cmpq	$2147483647, %r14
	jg	.L633
	cmpq	$-2147483648, %r14
	jl	.L674
	movl	%r14d, %eax
.L635:
	testl	%eax, %eax
	jns	.L633
.L674:
	addq	$40, %rsp
	movq	%rbx, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L633:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	cmpq	%rbx, 32(%r12)
	je	.L674
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %rcx
	movq	%rax, %r15
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L653
	movq	-56(%rbp), %r8
	movq	32(%rax), %rsi
	movq	%rcx, -64(%rbp)
	movq	%r8, %rdi
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	jne	.L654
.L653:
	subq	%rcx, %r14
	cmpq	$2147483647, %r14
	jg	.L633
	cmpq	$-2147483648, %r14
	jl	.L655
	movl	%r14d, %eax
.L654:
	testl	%eax, %eax
	jns	.L633
.L655:
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	cmovne	%r15, %rbx
	cmovne	%r15, %rax
	movq	%rbx, %rdx
	jmp	.L667
	.cfi_endproc
.LFE5567:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0, @function
_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0:
.LFB6055:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-400(%rbp), %r14
	pushq	%r13
	movq	%r14, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-352(%rbp), %rbx
	subq	$440, %rsp
	movq	%rdi, -440(%rbp)
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-336(%rbp), %rax
	movq	$19, -400(%rbp)
	movq	%rax, -352(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-400(%rbp), %rdx
	movq	%r14, %rsi
	movdqa	.LC10(%rip), %xmm0
	movl	$28769, %ecx
	movq	%rax, -352(%rbp)
	leaq	-320(%rbp), %rdi
	movq	%rdx, -336(%rbp)
	movups	%xmm0, (%rax)
	movq	-352(%rbp), %rdx
	movw	%cx, 16(%rax)
	movb	$47, 18(%rax)
	movq	-400(%rbp), %rax
	movq	%rax, -344(%rbp)
	movb	$0, (%rdx,%rax)
	xorl	%edx, %edx
	leaq	-304(%rbp), %rax
	movq	%rax, -320(%rbp)
	movq	$21, -400(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movl	$12142, %edi
	movl	$12147, %esi
	movq	-400(%rbp), %rdx
	movdqa	.LC11(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movq	%rdx, -304(%rbp)
	movl	$1954047348, 16(%rax)
	movb	$47, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-400(%rbp), %rax
	movq	-320(%rbp), %rdx
	movq	%rax, -312(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-272(%rbp), %rax
	leaq	-240(%rbp), %rdx
	movq	%rax, -288(%rbp)
	movabsq	$7809644666444607081, %rax
	movw	%di, -228(%rbp)
	movl	$128, %edi
	movq	%rax, -272(%rbp)
	movl	$1885692975, -264(%rbp)
	movw	%si, -260(%rbp)
	movq	$14, -280(%rbp)
	movb	$0, -258(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rax, -240(%rbp)
	movl	$1767992623, -232(%rbp)
	movq	$14, -248(%rbp)
	movb	$0, -226(%rbp)
	call	_Znwm@PLT
	movq	-352(%rbp), %r15
	movq	-344(%rbp), %r12
	leaq	16(%rax), %rdi
	movq	%rax, %r13
	movq	%rdi, (%rax)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L761
	testq	%r15, %r15
	je	.L678
.L761:
	movq	%r12, -400(%rbp)
	cmpq	$15, %r12
	ja	.L832
	cmpq	$1, %r12
	jne	.L833
	movzbl	(%r15), %eax
	movb	%al, 16(%r13)
.L682:
	movq	%r12, 8(%r13)
	leaq	32(%r13), %r8
	movb	$0, (%rdi,%r12)
	movq	-320(%rbp), %r15
	leaq	48(%r13), %rdi
	movq	-312(%rbp), %r12
	movq	%rdi, 32(%r13)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L762
	testq	%r15, %r15
	je	.L678
.L762:
	movq	%r12, -400(%rbp)
	cmpq	$15, %r12
	ja	.L834
	cmpq	$1, %r12
	je	.L689
	testq	%r12, %r12
	jne	.L690
.L688:
	movq	%r12, 40(%r13)
	leaq	64(%r13), %r8
	movb	$0, (%rdi,%r12)
	movq	-288(%rbp), %r15
	leaq	80(%r13), %rdi
	movq	-280(%rbp), %r12
	movq	%rdi, 64(%r13)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L763
	testq	%r15, %r15
	je	.L678
.L763:
	movq	%r12, -400(%rbp)
	cmpq	$15, %r12
	ja	.L835
	cmpq	$1, %r12
	je	.L695
	testq	%r12, %r12
	jne	.L696
.L694:
	movq	%r12, 72(%r13)
	leaq	96(%r13), %r8
	movb	$0, (%rdi,%r12)
	movq	-256(%rbp), %r15
	leaq	112(%r13), %rdi
	movq	-248(%rbp), %r12
	movq	%rdi, 96(%r13)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L697
	testq	%r15, %r15
	je	.L678
.L697:
	movq	%r12, -400(%rbp)
	cmpq	$15, %r12
	ja	.L836
	cmpq	$1, %r12
	jne	.L700
	movzbl	(%r15), %eax
	movb	%al, 112(%r13)
.L701:
	leaq	128(%r13), %rax
	movq	%r12, 104(%r13)
	movq	%rax, -448(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -456(%rbp)
	movb	$0, (%rdi,%r12)
	movq	%rax, %r12
.L705:
	subq	$32, %r12
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L702
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	jne	.L705
.L703:
	leaq	-208(%rbp), %rax
	leaq	-160(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rax, -224(%rbp)
	movl	$31091, %eax
	leaq	-392(%rbp), %r12
	movw	%ax, -208(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	leaq	-144(%rbp), %rax
	movb	$115, -206(%rbp)
	movq	$3, -216(%rbp)
	movb	$0, -205(%rbp)
	movl	$1769169271, -176(%rbp)
	movq	$4, -184(%rbp)
	movb	$0, -172(%rbp)
	movq	%rax, -160(%rbp)
	movq	$21, -400(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-400(%rbp), %rdx
	leaq	-128(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC12(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movups	%xmm0, (%rax)
	movl	$1852400750, 16(%rax)
	movb	$103, 20(%rax)
	movq	-400(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-112(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -128(%rbp)
	movq	$25, -400(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-400(%rbp), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	movdqa	.LC13(%rip), %xmm0
	movq	%rax, -128(%rbp)
	movabsq	$7811887700347154527, %rcx
	movq	%rdx, -112(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movb	$108, 24(%rax)
	movq	-400(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-80(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -96(%rbp)
	movq	$26, -400(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-400(%rbp), %rdx
	movdqa	.LC13(%rip), %xmm0
	movabsq	$8319104414429376607, %rcx
	movq	%rax, -96(%rbp)
	movq	-456(%rbp), %r15
	movq	%rdx, -80(%rbp)
	movl	$29295, %edx
	movq	%rcx, 16(%rax)
	movw	%dx, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-400(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%r13, -480(%rbp)
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-408(%rbp), %rax
	movl	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%r12, -376(%rbp)
	movq	%r12, -368(%rbp)
	movq	$0, -360(%rbp)
	movq	%rax, -472(%rbp)
	movq	%r14, -424(%rbp)
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L711:
	cmpq	$1, %r11
	jne	.L713
	movzbl	(%r8), %eax
	movb	%al, 48(%r13)
.L714:
	movq	%r11, 40(%r13)
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movb	$0, (%rdi,%r11)
	movzbl	%bl, %edi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -360(%rbp)
.L706:
	addq	$32, %r15
	leaq	-64(%rbp), %rax
	cmpq	%rax, %r15
	je	.L837
.L715:
	movq	-424(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L706
	testq	%rax, %rax
	setne	%bl
	cmpq	%r12, %rdx
	sete	%dil
	orb	%dil, %bl
	je	.L838
.L707:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	(%r15), %r8
	movq	8(%r15), %r11
	leaq	48(%rax), %rdi
	movq	%rax, %r13
	movq	%rdi, 32(%rax)
	movq	%r8, %rax
	addq	%r11, %rax
	je	.L764
	testq	%r8, %r8
	je	.L678
.L764:
	movq	%r11, -408(%rbp)
	cmpq	$15, %r11
	jbe	.L711
	movq	-472(%rbp), %rsi
	leaq	32(%r13), %rdi
	xorl	%edx, %edx
	movq	%r11, -464(%rbp)
	movq	%r8, -432(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-432(%rbp), %r8
	movq	-464(%rbp), %r11
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	movq	-408(%rbp), %rax
	movq	%rax, 48(%r13)
.L712:
	movq	%r11, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-408(%rbp), %r11
	movq	32(%r13), %rdi
	jmp	.L714
.L833:
	testq	%r12, %r12
	je	.L682
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L702:
	cmpq	%rbx, %r12
	jne	.L705
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L713:
	testq	%r11, %r11
	je	.L714
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L837:
	movq	-440(%rbp), %rcx
	movq	-480(%rbp), %r13
	movq	%rax, %r15
	movq	72(%rcx), %r12
	leaq	56(%rcx), %rbx
	testq	%r12, %r12
	je	.L720
.L716:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L719
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L830
.L721:
	movq	%r14, %r12
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L721
.L830:
	movq	%r15, %rax
.L720:
	movq	-440(%rbp), %rcx
	movq	-384(%rbp), %rdx
	leaq	64(%rcx), %rsi
	movq	$0, 72(%rcx)
	movq	%rsi, -432(%rbp)
	movq	%rsi, 80(%rcx)
	movq	%rsi, 88(%rcx)
	movq	$0, 96(%rcx)
	testq	%rdx, %rdx
	je	.L718
	movq	%rcx, %rsi
	movl	-392(%rbp), %ecx
	movq	%rdx, 72(%rsi)
	movl	%ecx, 64(%rsi)
	movq	-376(%rbp), %rcx
	movq	%rcx, 80(%rsi)
	movq	-368(%rbp), %rcx
	movq	%rcx, 88(%rsi)
	movq	-432(%rbp), %rcx
	movq	%rcx, 8(%rdx)
	movq	-360(%rbp), %rdx
	movq	%rdx, 96(%rsi)
.L718:
	movq	%rax, %r12
.L756:
	subq	$32, %r12
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L722
	call	_ZdlPv@PLT
	cmpq	-456(%rbp), %r12
	jne	.L756
.L723:
	movq	-440(%rbp), %rax
	movq	128(%rax), %r12
	addq	$112, %rax
	movq	%rax, -456(%rbp)
	cmpq	%rax, %r12
	je	.L752
	cmpq	%r13, -448(%rbp)
	je	.L728
	movq	%rax, %r14
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L730:
	movq	40(%r13), %rcx
	cmpq	40(%r12), %rcx
	ja	.L732
	movq	32(%r13), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	testq	%rax, %rax
	je	.L839
.L732:
	movq	72(%r13), %rcx
	cmpq	40(%r12), %rcx
	ja	.L734
	movq	64(%r13), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	testq	%rax, %rax
	je	.L840
.L734:
	movq	104(%r13), %rcx
	cmpq	40(%r12), %rcx
	ja	.L736
	movq	96(%r13), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	testq	%rax, %rax
	je	.L841
.L736:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	je	.L842
.L738:
	movq	8(%r13), %rcx
	leaq	32(%r12), %r15
	cmpq	40(%r12), %rcx
	ja	.L730
	movq	0(%r13), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEPKcmm@PLT
	testq	%rax, %rax
	jne	.L730
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L722:
	cmpq	-456(%rbp), %r12
	jne	.L756
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L840:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L841:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %r14
	jne	.L738
	.p2align 4,,10
	.p2align 3
.L842:
	movq	-440(%rbp), %rax
	movq	128(%rax), %rax
	movq	%rax, -424(%rbp)
	cmpq	%rax, -456(%rbp)
	je	.L843
.L757:
	movq	-440(%rbp), %rax
	movq	%r13, -472(%rbp)
	addq	$8, %rax
	movq	%rax, -464(%rbp)
	.p2align 4,,10
	.p2align 3
.L751:
	movq	-440(%rbp), %rax
	movq	72(%rax), %r14
	testq	%r14, %r14
	je	.L740
	movq	-424(%rbp), %rax
	movq	-432(%rbp), %rbx
	movq	40(%rax), %r15
	movq	32(%rax), %r13
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L746:
	movq	24(%r14), %r14
	testq	%r14, %r14
	je	.L742
.L741:
	movq	40(%r14), %r12
	movq	%r15, %rdx
	cmpq	%r15, %r12
	cmovbe	%r12, %rdx
	testq	%rdx, %rdx
	je	.L743
	movq	32(%r14), %rdi
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L744
.L743:
	subq	%r15, %r12
	movl	$2147483648, %eax
	cmpq	%rax, %r12
	jge	.L745
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r12
	jle	.L746
	movl	%r12d, %eax
.L744:
	testl	%eax, %eax
	js	.L746
.L745:
	movq	%r14, %rbx
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L741
.L742:
	cmpq	%rbx, -432(%rbp)
	je	.L740
	movq	40(%rbx), %r14
	cmpq	%r14, %r15
	movq	%r14, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L748
	movq	32(%rbx), %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L749
.L748:
	movq	%r15, %r9
	movl	$2147483648, %eax
	subq	%r14, %r9
	cmpq	%rax, %r9
	jge	.L750
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r9
	jle	.L740
	movl	%r9d, %eax
.L749:
	testl	%eax, %eax
	jns	.L750
.L740:
	movq	-424(%rbp), %rax
	movq	-464(%rbp), %rdi
	leaq	32(%rax), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
.L750:
	movq	-424(%rbp), %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, -424(%rbp)
	cmpq	%rax, -456(%rbp)
	jne	.L751
	movq	-472(%rbp), %r13
.L752:
	movq	-440(%rbp), %rax
	movb	$1, (%rax)
	cmpq	%r13, -448(%rbp)
	je	.L726
.L727:
	movq	%r13, %rbx
	.p2align 4,,10
	.p2align 3
.L755:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L753
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	-448(%rbp), %rbx
	jne	.L755
.L726:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L844
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJRKS5_EEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L753:
	addq	$32, %rbx
	cmpq	%rbx, -448(%rbp)
	jne	.L755
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L838:
	movq	8(%r15), %rcx
	movq	40(%rdx), %r13
	cmpq	%r13, %rcx
	movq	%r13, %rdx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L708
	movq	32(%r14), %rsi
	movq	(%r15), %rdi
	movq	%rcx, -432(%rbp)
	call	memcmp@PLT
	movq	-432(%rbp), %rcx
	testl	%eax, %eax
	jne	.L709
.L708:
	subq	%r13, %rcx
	movl	$2147483648, %eax
	cmpq	%rax, %rcx
	jge	.L707
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L760
	movl	%ecx, %eax
.L709:
	shrl	$31, %eax
	movl	%eax, %ebx
	jmp	.L707
.L700:
	testq	%r12, %r12
	je	.L701
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L695:
	movzbl	(%r15), %eax
	movb	%al, 80(%r13)
	jmp	.L694
.L689:
	movzbl	(%r15), %eax
	movb	%al, 48(%r13)
	jmp	.L688
.L834:
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	movq	-400(%rbp), %rax
	movq	%rax, 48(%r13)
.L690:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-400(%rbp), %r12
	movq	32(%r13), %rdi
	jmp	.L688
.L832:
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-400(%rbp), %rax
	movq	%rax, 16(%r13)
.L684:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-400(%rbp), %r12
	movq	0(%r13), %rdi
	jmp	.L682
.L836:
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 96(%r13)
	movq	%rax, %rdi
	movq	-400(%rbp), %rax
	movq	%rax, 112(%r13)
.L699:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-400(%rbp), %r12
	movq	96(%r13), %rdi
	jmp	.L701
.L835:
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 64(%r13)
	movq	%rax, %rdi
	movq	-400(%rbp), %rax
	movq	%rax, 80(%r13)
.L696:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-400(%rbp), %r12
	movq	64(%r13), %rdi
	jmp	.L694
.L728:
	movq	%r12, -424(%rbp)
	jmp	.L757
.L760:
	movl	$1, %ebx
	jmp	.L707
.L843:
	movq	-440(%rbp), %rax
	movb	$1, (%rax)
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L678:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L844:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6055:
	.size	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0, .-_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv
	.type	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv, @function
_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv:
.LFB4438:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	je	.L847
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	jmp	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0
	.cfi_endproc
.LFE4438:
	.size	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv, .-_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader19GetCannotBeRequiredB5cxx11Ev
	.type	_ZN4node13native_module18NativeModuleLoader19GetCannotBeRequiredB5cxx11Ev, @function
_ZN4node13native_module18NativeModuleLoader19GetCannotBeRequiredB5cxx11Ev:
.LFB4443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, (%rdi)
	jne	.L849
	call	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0
.L849:
	addq	$8, %rsp
	leaq	56(%rbx), %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4443:
	.size	_ZN4node13native_module18NativeModuleLoader19GetCannotBeRequiredB5cxx11Ev, .-_ZN4node13native_module18NativeModuleLoader19GetCannotBeRequiredB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader16CannotBeRequiredEPKc
	.type	_ZN4node13native_module18NativeModuleLoader16CannotBeRequiredEPKc, @function
_ZN4node13native_module18NativeModuleLoader16CannotBeRequiredEPKc:
.LFB4446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	jne	.L852
	call	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0
.L852:
	leaq	-80(%rbp), %r12
	leaq	-96(%rbp), %r13
	movq	%r12, -96(%rbp)
	testq	%r14, %r14
	jne	.L866
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L867
	cmpq	$1, %rax
	jne	.L854
	movzbl	(%r14), %edx
	movb	%dl, -80(%rbp)
	movq	%r12, %rdx
.L855:
	movq	%rax, -88(%rbp)
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	addq	$64, %rbx
	movb	$0, (%rdx,%rax)
	call	_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_
	movq	-96(%rbp), %rdi
	cmpq	%rax, %rbx
	setne	%r13b
	cmpq	%r12, %rdi
	je	.L851
	call	_ZdlPv@PLT
.L851:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L868
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L869
	movq	%r12, %rdx
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L867:
	movq	%r13, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L853:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L855
.L868:
	call	__stack_chk_fail@PLT
.L869:
	movq	%r12, %rdi
	jmp	.L853
	.cfi_endproc
.LFE4446:
	.size	_ZN4node13native_module18NativeModuleLoader16CannotBeRequiredEPKc, .-_ZN4node13native_module18NativeModuleLoader16CannotBeRequiredEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader16GetCanBeRequiredB5cxx11Ev
	.type	_ZN4node13native_module18NativeModuleLoader16GetCanBeRequiredB5cxx11Ev, @function
_ZN4node13native_module18NativeModuleLoader16GetCanBeRequiredB5cxx11Ev:
.LFB4444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, (%rdi)
	jne	.L871
	call	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0
.L871:
	addq	$8, %rsp
	leaq	8(%rbx), %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4444:
	.size	_ZN4node13native_module18NativeModuleLoader16GetCanBeRequiredB5cxx11Ev, .-_ZN4node13native_module18NativeModuleLoader16GetCanBeRequiredB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module18NativeModuleLoader13CanBeRequiredEPKc
	.type	_ZN4node13native_module18NativeModuleLoader13CanBeRequiredEPKc, @function
_ZN4node13native_module18NativeModuleLoader13CanBeRequiredEPKc:
.LFB4445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	jne	.L874
	call	_ZN4node13native_module18NativeModuleLoader26InitializeModuleCategoriesEv.part.0
.L874:
	leaq	-80(%rbp), %r12
	leaq	-96(%rbp), %r13
	movq	%r12, -96(%rbp)
	testq	%r14, %r14
	jne	.L888
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L888:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L889
	cmpq	$1, %rax
	jne	.L876
	movzbl	(%r14), %edx
	movb	%dl, -80(%rbp)
	movq	%r12, %rdx
.L877:
	movq	%rax, -88(%rbp)
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	addq	$16, %rbx
	movb	$0, (%rdx,%rax)
	call	_ZNKSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE4findERKS5_
	movq	-96(%rbp), %rdi
	cmpq	%rax, %rbx
	setne	%r13b
	cmpq	%r12, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L890
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L891
	movq	%r12, %rdx
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L889:
	movq	%r13, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L875:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L877
.L890:
	call	__stack_chk_fail@PLT
.L891:
	movq	%r12, %rdi
	jmp	.L875
	.cfi_endproc
.LFE4445:
	.size	_ZN4node13native_module18NativeModuleLoader13CanBeRequiredEPKc, .-_ZN4node13native_module18NativeModuleLoader13CanBeRequiredEPKc
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node13native_module18NativeModuleLoader9instance_E, @function
_GLOBAL__sub_I__ZN4node13native_module18NativeModuleLoader9instance_E:
.LFB5890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node13native_module18NativeModuleLoader9instance_E(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node13native_module18NativeModuleLoaderC1Ev
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node13native_module18NativeModuleLoader9instance_E(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZN4node13native_module18NativeModuleLoaderD1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5890:
	.size	_GLOBAL__sub_I__ZN4node13native_module18NativeModuleLoader9instance_E, .-_GLOBAL__sub_I__ZN4node13native_module18NativeModuleLoader9instance_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node13native_module18NativeModuleLoader9instance_E
	.weak	_ZTVN4node32NonOwningExternalOneByteResourceE
	.section	.data.rel.ro.local._ZTVN4node32NonOwningExternalOneByteResourceE,"awG",@progbits,_ZTVN4node32NonOwningExternalOneByteResourceE,comdat
	.align 8
	.type	_ZTVN4node32NonOwningExternalOneByteResourceE, @object
	.size	_ZTVN4node32NonOwningExternalOneByteResourceE, 80
_ZTVN4node32NonOwningExternalOneByteResourceE:
	.quad	0
	.quad	0
	.quad	_ZN4node32NonOwningExternalOneByteResourceD1Ev
	.quad	_ZN4node32NonOwningExternalOneByteResourceD0Ev
	.quad	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.quad	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.quad	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.quad	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.quad	_ZNK4node32NonOwningExternalOneByteResource4dataEv
	.quad	_ZNK4node32NonOwningExternalOneByteResource6lengthEv
	.weak	_ZTVN4node32NonOwningExternalTwoByteResourceE
	.section	.data.rel.ro.local._ZTVN4node32NonOwningExternalTwoByteResourceE,"awG",@progbits,_ZTVN4node32NonOwningExternalTwoByteResourceE,comdat
	.align 8
	.type	_ZTVN4node32NonOwningExternalTwoByteResourceE, @object
	.size	_ZTVN4node32NonOwningExternalTwoByteResourceE, 80
_ZTVN4node32NonOwningExternalTwoByteResourceE:
	.quad	0
	.quad	0
	.quad	_ZN4node32NonOwningExternalTwoByteResourceD1Ev
	.quad	_ZN4node32NonOwningExternalTwoByteResourceD0Ev
	.quad	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.quad	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.quad	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.quad	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.quad	_ZNK4node32NonOwningExternalTwoByteResource4dataEv
	.quad	_ZNK4node32NonOwningExternalTwoByteResource6lengthEv
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC14:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC16:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"../src/node_native_module.cc:306"
	.section	.rodata.str1.1
.LC18:
	.string	"(new_cached_data) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"v8::MaybeLocal<v8::Function> node::native_module::NativeModuleLoader::LookupAndCompile(v8::Local<v8::Context>, const char*, std::vector<v8::Local<v8::String> >*, node::native_module::NativeModuleLoader::Result*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultEE4args, @object
	.size	_ZZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultEE4args, 24
_ZZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultEE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"../src/node_native_module.cc:227"
	.align 8
.LC21:
	.string	"(source_it) != (source_.end())"
	.align 8
.LC22:
	.string	"v8::MaybeLocal<v8::String> node::native_module::NativeModuleLoader::LoadBuiltinModuleSource(v8::Isolate*, const char*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKcE4args, @object
	.size	_ZZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKcE4args, 24
_ZZN4node13native_module18NativeModuleLoader23LoadBuiltinModuleSourceEPN2v87IsolateEPKcE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.globl	_ZN4node13native_module18NativeModuleLoader9instance_E
	.bss
	.align 32
	.type	_ZN4node13native_module18NativeModuleLoader9instance_E, @object
	.size	_ZN4node13native_module18NativeModuleLoader9instance_E, 272
_ZN4node13native_module18NativeModuleLoader9instance_E:
	.zero	272
	.weak	_ZZNK4node10UnionBytes14two_bytes_dataEvE4args
	.section	.rodata.str1.1
.LC23:
	.string	"../src/node_union_bytes.h:73"
.LC24:
	.string	"(two_bytes_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"const uint16_t* node::UnionBytes::two_bytes_data() const"
	.section	.data.rel.ro.local._ZZNK4node10UnionBytes14two_bytes_dataEvE4args,"awG",@progbits,_ZZNK4node10UnionBytes14two_bytes_dataEvE4args,comdat
	.align 16
	.type	_ZZNK4node10UnionBytes14two_bytes_dataEvE4args, @gnu_unique_object
	.size	_ZZNK4node10UnionBytes14two_bytes_dataEvE4args, 24
_ZZNK4node10UnionBytes14two_bytes_dataEvE4args:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.quad	7809644666444607081
	.quad	8247343761545191983
	.align 16
.LC11:
	.quad	7809644666444607081
	.quad	7957688328179249199
	.align 16
.LC12:
	.quad	7809644666444607081
	.quad	7593684098898818095
	.align 16
.LC13:
	.quad	7809644666444607081
	.quad	7381244141242971695
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
