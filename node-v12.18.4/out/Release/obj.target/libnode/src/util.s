	.file	"util.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB5097:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE5097:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.text
	.p2align 4
	.type	_ZN4nodeL14MakeUtf8StringIcEEvPN2v87IsolateENS1_5LocalINS1_5ValueEEEPNS_16MaybeStackBufferIT_Lm1024EEE, @function
_ZN4nodeL14MakeUtf8StringIcEEvPN2v87IsolateENS1_5LocalINS1_5ValueEEEPNS_16MaybeStackBufferIT_Lm1024EEE:
.LFB8539:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L3
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movl	%eax, %r15d
	testb	%al, %al
	je	.L3
	movq	16(%rbx), %r10
	testq	%r10, %r10
	je	.L31
	leaq	1(%rdx), %r14
	cmpq	8(%rbx), %r14
	jbe	.L7
	leaq	24(%rbx), %rcx
	movl	$0, %eax
	movl	$0, %r8d
	cmpq	%rcx, %r10
	movq	%rcx, -64(%rbp)
	cmove	%eax, %r15d
	cmovne	%r10, %r8
	testq	%r14, %r14
	je	.L32
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%r8, -56(%rbp)
	call	realloc@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L33
.L10:
	movq	%r10, 16(%rbx)
	movq	%r14, 8(%rbx)
	testb	%r15b, %r15b
	jne	.L7
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L34
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r14, (%rbx)
	movl	%r14d, %ecx
	movq	%r10, %rdx
	movl	$10, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	8(%rbx), %rdx
	cltq
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	ja	.L35
	cmpq	%rdx, %rax
	ja	.L36
	movq	16(%rbx), %rdx
	movq	%rax, (%rbx)
	movb	$0, (%rdx,%rax)
.L3:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	-64(%rbp), %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	16(%rbx), %r10
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r8, %rdi
	call	free@PLT
	xorl	%r10d, %r10d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	cmpb	$0, _ZN4node11per_process14v8_initializedE(%rip)
	jne	.L37
.L12:
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	realloc@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	jne	.L10
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L37:
	call	_ZN2v87Isolate10GetCurrentEv@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L12
	call	_ZN2v87Isolate21LowMemoryNotificationEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L12
	.cfi_endproc
.LFE8539:
	.size	_ZN4nodeL14MakeUtf8StringIcEEvPN2v87IsolateENS1_5LocalINS1_5ValueEEEPNS_16MaybeStackBufferIT_Lm1024EEE, .-_ZN4nodeL14MakeUtf8StringIcEEvPN2v87IsolateENS1_5LocalINS1_5ValueEEEPNS_16MaybeStackBufferIT_Lm1024EEE
	.section	.rodata._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB9998:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L39
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L40
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L40:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L41
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L42
.L41:
	testq	%rbx, %rbx
	je	.L42
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L42:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L43
	call	__stack_chk_fail@PLT
.L39:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L43:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9998:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata._ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0.str1.1,"aMS",@progbits,1
.LC1:
	.string	"0123456789abcdef"
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB10006:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC1(%rip), %rax
.L50:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L50
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L51
	call	__stack_chk_fail@PLT
.L51:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10006:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L54
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L56
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L58
.L54:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L58
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4103:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB6032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L61
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L90
	cmpq	$1, %rax
	jne	.L64
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L65:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L60:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L65
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L63:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L61:
	cmpb	$37, 1(%rax)
	jne	.L92
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L93
	cmpq	$1, %r13
	jne	.L70
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L71:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L73
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L94
.L73:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L75:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L95
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L77:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L60
	call	_ZdlPv@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L70:
	testq	%r13, %r13
	jne	.L96
	movq	%rbx, %rax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L69:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L95:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L75
.L92:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L91:
	call	__stack_chk_fail@PLT
.L96:
	movq	%rbx, %rdi
	jmp	.L69
	.cfi_endproc
.LFE6032:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9Utf8ValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.type	_ZN4node9Utf8ValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE, @function
_ZN4node9Utf8ValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE:
.LFB7808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqa	.LC2(%rip), %xmm0
	movq	%r14, 16(%rdi)
	movb	$0, 24(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rdx, %rdx
	je	.L97
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L97
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movl	%eax, %ecx
	testb	%al, %al
	je	.L97
	movq	16(%rbx), %r10
	testq	%r10, %r10
	je	.L129
	leaq	1(%rdx), %r15
	cmpq	8(%rbx), %r15
	jbe	.L102
	cmpq	%r10, %r14
	movl	$0, %eax
	movl	$0, %r8d
	cmove	%eax, %ecx
	cmovne	%r10, %r8
	testq	%r15, %r15
	je	.L130
	movq	%r8, %rdi
	movq	%r15, %rsi
	movb	%cl, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	realloc@PLT
	movq	-56(%rbp), %r8
	movzbl	-64(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L131
.L105:
	movq	%r10, 16(%rbx)
	movq	%r15, 8(%rbx)
	testb	%cl, %cl
	je	.L132
.L102:
	movq	%r15, (%rbx)
	movl	%r15d, %ecx
	movq	%r10, %rdx
	movl	$10, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	8(%rbx), %rdx
	cltq
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	ja	.L133
	cmpq	%rdx, %rax
	ja	.L134
	movq	16(%rbx), %rdx
	movq	%rax, (%rbx)
	movb	$0, (%rdx,%rax)
.L97:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L102
	movq	%r10, %rdi
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	16(%rbx), %r10
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r8, %rdi
	movb	%cl, -56(%rbp)
	call	free@PLT
	movzbl	-56(%rbp), %ecx
	xorl	%r10d, %r10d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L131:
	cmpb	$0, _ZN4node11per_process14v8_initializedE(%rip)
	jne	.L135
.L107:
	movq	%r15, %rsi
	movq	%r8, %rdi
	movb	%cl, -56(%rbp)
	call	realloc@PLT
	movzbl	-56(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r10
	jne	.L105
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L135:
	movq	%r8, -64(%rbp)
	movb	%cl, -56(%rbp)
	call	_ZN2v87Isolate10GetCurrentEv@PLT
	movzbl	-56(%rbp), %ecx
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L107
	call	_ZN2v87Isolate21LowMemoryNotificationEv@PLT
	movq	-64(%rbp), %r8
	movzbl	-56(%rbp), %ecx
	jmp	.L107
	.cfi_endproc
.LFE7808:
	.size	_ZN4node9Utf8ValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE, .-_ZN4node9Utf8ValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.globl	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.set	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE,_ZN4node9Utf8ValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node12TwoByteValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.type	_ZN4node12TwoByteValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE, @function
_ZN4node12TwoByteValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE:
.LFB7811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movdqa	.LC2(%rip), %xmm0
	movq	%r14, 16(%rdi)
	movw	%r8w, 24(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rdx, %rdx
	je	.L136
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L136
	movq	%rax, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	movq	16(%rbx), %r10
	leal	1(%rax), %r8d
	movslq	%r8d, %r15
	testq	%r10, %r10
	je	.L175
	cmpq	8(%rbx), %r15
	jbe	.L140
	cmpq	%r10, %r14
	movl	$0, %r9d
	leaq	(%r15,%r15), %rsi
	cmovne	%r10, %r9
	setne	-52(%rbp)
	testq	%r15, %r15
	js	.L176
	testq	%rsi, %rsi
	je	.L177
	movq	%r9, %rdi
	movl	%r8d, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	realloc@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rsi
	testq	%rax, %rax
	movl	-80(%rbp), %r8d
	movq	%rax, %r10
	je	.L178
.L145:
	cmpb	$0, -52(%rbp)
	movq	%r10, 16(%rbx)
	movq	%r15, 8(%rbx)
	jne	.L140
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L179
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r15, (%rbx)
	xorl	%ecx, %ecx
	movq	%r10, %rdx
	movl	$2, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	movq	8(%rbx), %rdx
	cltq
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	ja	.L180
	cmpq	%rdx, %rax
	ja	.L181
	movq	16(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, (%rbx)
	movw	%cx, (%rdx,%rax,2)
.L136:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	%r10, %rdi
	addq	%rdx, %rdx
	movq	%r14, %rsi
	movl	%r8d, -52(%rbp)
	call	memcpy@PLT
	movq	16(%rbx), %r10
	movl	-52(%rbp), %r8d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L181:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r9, %rdi
	movl	%r8d, -64(%rbp)
	call	free@PLT
	movl	-64(%rbp), %r8d
	movl	$1, %eax
	xorl	%r10d, %r10d
.L144:
	testq	%r15, %r15
	je	.L145
	testb	%al, %al
	je	.L145
	leaq	_ZZN4node7ReallocItEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L178:
	cmpb	$0, _ZN4node11per_process14v8_initializedE(%rip)
	jne	.L182
.L147:
	movq	%r9, %rdi
	movl	%r8d, -64(%rbp)
	call	realloc@PLT
	movl	-64(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r10
	sete	%al
	jmp	.L144
.L182:
	movq	%rsi, -80(%rbp)
	movq	%r9, -72(%rbp)
	movl	%r8d, -64(%rbp)
	call	_ZN2v87Isolate10GetCurrentEv@PLT
	movl	-64(%rbp), %r8d
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	-80(%rbp), %rsi
	movq	%rax, %rdi
	je	.L147
	call	_ZN2v87Isolate21LowMemoryNotificationEv@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %r9
	movl	-64(%rbp), %r8d
	jmp	.L147
	.cfi_endproc
.LFE7811:
	.size	_ZN4node12TwoByteValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE, .-_ZN4node12TwoByteValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.globl	_ZN4node12TwoByteValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.set	_ZN4node12TwoByteValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE,_ZN4node12TwoByteValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11BufferValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.type	_ZN4node11BufferValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE, @function
_ZN4node11BufferValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE:
.LFB7814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movdqa	.LC2(%rip), %xmm0
	movq	%r14, 16(%rdi)
	movb	$0, 24(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rdx, %rdx
	je	.L228
	movq	(%rdx), %rax
	movq	%rdx, %r13
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L229
.L186:
	movq	%r13, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	movl	%eax, %ebx
	testb	%al, %al
	je	.L187
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	16(%r12), %r15
	movq	%rax, %rcx
	leaq	1(%rax), %r8
	testq	%r15, %r15
	je	.L230
	cmpq	8(%r12), %r8
	jbe	.L189
	cmpq	%r15, %r14
	je	.L190
	testq	%r8, %r8
	je	.L231
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	realloc@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	je	.L203
	movq	%rax, 16(%r12)
	movq	%rax, %r15
	movq	%r8, 8(%r12)
.L189:
	movq	%r8, (%r12)
	movq	%rcx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	8(%r12), %rax
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	cmpq	%rax, %r8
	ja	.L232
.L192:
	cmpq	%rax, %rcx
	ja	.L233
	movq	16(%r12), %rax
	movq	%rcx, (%r12)
	movb	$0, (%rax,%rcx)
.L183:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L186
	addq	$24, %rsp
	movq	%rsi, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4nodeL14MakeUtf8StringIcEEvPN2v87IsolateENS1_5LocalINS1_5ValueEEEPNS_16MaybeStackBufferIT_Lm1024EEE
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	16(%r12), %rax
	cmpq	%rax, %r14
	je	.L201
	testq	%rax, %rax
	je	.L201
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE10InvalidateEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L201:
	movq	$0, 16(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	testq	%r8, %r8
	je	.L234
	movq	%r8, %rdi
	movq	%r8, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	malloc@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L235
	movq	%r15, 16(%r12)
	movq	%r8, 8(%r12)
.L202:
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L189
.L194:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	movq	16(%r12), %r15
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	free@PLT
	movq	$0, (%r12)
	movq	$0, 16(%r12)
	movq	$0, 8(%r12)
.L227:
	movq	$-1, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	8(%r12), %rax
	movq	-56(%rbp), %rcx
	jmp	.L192
.L235:
	xorl	%ebx, %ebx
.L203:
	cmpb	$0, _ZN4node11per_process14v8_initializedE(%rip)
	jne	.L236
.L197:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	realloc@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L199
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L199:
	movq	%rax, 16(%r12)
	movq	%r8, 8(%r12)
	testb	%bl, %bl
	je	.L202
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L234:
	movq	$0, 16(%r12)
	movq	(%r12), %rdx
	movq	$0, 8(%r12)
	testq	%rdx, %rdx
	jne	.L237
	movq	%rax, -56(%rbp)
	jmp	.L227
.L236:
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v87Isolate10GetCurrentEv@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L197
	call	_ZN2v87Isolate21LowMemoryNotificationEv@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	jmp	.L197
.L237:
	xorl	%r15d, %r15d
	jmp	.L194
	.cfi_endproc
.LFE7814:
	.size	_ZN4node11BufferValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE, .-_ZN4node11BufferValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.globl	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.set	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE,_ZN4node11BufferValueC2EPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node21LowMemoryNotificationEv
	.type	_ZN4node21LowMemoryNotificationEv, @function
_ZN4node21LowMemoryNotificationEv:
.LFB7816:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN4node11per_process14v8_initializedE(%rip)
	jne	.L249
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v87Isolate10GetCurrentEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L238
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate21LowMemoryNotificationEv@PLT
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7816:
	.size	_ZN4node21LowMemoryNotificationEv, .-_ZN4node21LowMemoryNotificationEv
	.p2align 4
	.globl	_ZN4node15GetProcessTitleB5cxx11EPKc
	.type	_ZN4node15GetProcessTitleB5cxx11EPKc, @function
_ZN4node15GetProcessTitleB5cxx11EPKc:
.LFB7817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$16, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L269:
	cmpl	$-105, %eax
	jne	.L268
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	(%rax,%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
.L260:
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	uv_get_process_title@PLT
	testl	%eax, %eax
	jne	.L269
	movq	-96(%rbp), %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L270
	movq	%rax, 0(%r13)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%r13)
.L262:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%r13)
.L250:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	leaq	16(%r13), %r15
	movq	%r15, 0(%r13)
	testq	%r14, %r14
	je	.L272
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L273
	cmpq	$1, %rax
	jne	.L256
	movzbl	(%r14), %edx
	movb	%dl, 16(%r13)
.L257:
	movq	%rax, 8(%r13)
	movb	$0, (%r15,%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L250
	call	_ZdlPv@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L256:
	testq	%rax, %rax
	je	.L257
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L270:
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 16(%r13)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %r15
	movq	-104(%rbp), %rax
	movq	%rax, 16(%r13)
.L255:
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	0(%r13), %r15
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7817:
	.size	_ZN4node15GetProcessTitleB5cxx11EPKc, .-_ZN4node15GetProcessTitleB5cxx11EPKc
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Cannot create a string longer than 0x%x characters"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ERR_STRING_TOO_LONG"
.LC5:
	.string	"code"
	.text
	.p2align 4
	.globl	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE
	.type	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE, @function
_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE:
.LFB7827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %ecx
	movl	$1, %edx
	movl	$128, %esi
	movl	$1073741799, %r9d
	leaq	.LC3(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L282
.L275:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L283
.L276:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L284
.L277:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L285
.L278:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L286
.L279:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L287
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %rdi
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L284:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L285:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L286:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L279
.L287:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7827:
	.size	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE, .-_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE
	.p2align 4
	.globl	_ZN4node28GetCurrentTimeInMicrosecondsEv
	.type	_ZN4node28GetCurrentTimeInMicrosecondsEv, @function
_ZN4node28GetCurrentTimeInMicrosecondsEv:
.LFB7828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	uv_gettimeofday@PLT
	testl	%eax, %eax
	jne	.L292
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-32(%rbp), %xmm0
	mulsd	.LC6(%rip), %xmm0
	cvtsi2sdl	-24(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	jne	.L293
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	leaq	_ZZN4node28GetCurrentTimeInMicrosecondsEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L293:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7828:
	.size	_ZN4node28GetCurrentTimeInMicrosecondsEv, .-_ZN4node28GetCurrentTimeInMicrosecondsEv
	.p2align 4
	.globl	_ZN4node13WriteFileSyncEPKc8uv_buf_t
	.type	_ZN4node13WriteFileSyncEPKc8uv_buf_t, @function
_ZN4node13WriteFileSyncEPKc8uv_buf_t:
.LFB7829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movl	$384, %r8d
	movl	$577, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-480(%rbp), %r13
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -40
	movq	%rsi, -496(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -488(%rbp)
	movq	%rdi, %rdx
	xorl	%edi, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uv_fs_open@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	uv_fs_req_cleanup@PLT
	testl	%r12d, %r12d
	js	.L294
	subq	$8, %rsp
	movl	%r12d, %edx
	leaq	-496(%rbp), %rcx
	xorl	%edi, %edi
	pushq	$0
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%r13, %rsi
	call	uv_fs_write@PLT
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	uv_fs_req_cleanup@PLT
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	js	.L297
	movl	%r12d, %edx
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	call	uv_fs_close@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	uv_fs_req_cleanup@PLT
.L294:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	leaq	-24(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movl	%ebx, %r12d
	jmp	.L294
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7829:
	.size	_ZN4node13WriteFileSyncEPKc8uv_buf_t, .-_ZN4node13WriteFileSyncEPKc8uv_buf_t
	.p2align 4
	.globl	_ZN4node13WriteFileSyncEPN2v87IsolateEPKcNS0_5LocalINS0_6StringEEE
	.type	_ZN4node13WriteFileSyncEPN2v87IsolateEPKcNS0_5LocalINS0_6StringEEE, @function
_ZN4node13WriteFileSyncEPN2v87IsolateEPKcNS0_5LocalINS0_6StringEEE:
.LFB7830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-1064(%rbp), %rbx
	subq	$1520, %rsp
	movdqa	.LC2(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -1072(%rbp)
	movb	$0, -1064(%rbp)
	movaps	%xmm0, -1088(%rbp)
	testq	%rdx, %rdx
	je	.L305
	leaq	-1088(%rbp), %rdx
	call	_ZN4nodeL14MakeUtf8StringIcEEvPN2v87IsolateENS1_5LocalINS1_5ValueEEEPNS_16MaybeStackBufferIT_Lm1024EEE
	movq	-1072(%rbp), %rdi
	movl	-1088(%rbp), %esi
.L301:
	call	uv_buf_init@PLT
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movl	$384, %r8d
	leaq	-1536(%rbp), %r13
	movq	%rdx, -1544(%rbp)
	movl	$577, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, -1552(%rbp)
	call	uv_fs_open@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	uv_fs_req_cleanup@PLT
	testl	%r12d, %r12d
	js	.L302
	subq	$8, %rsp
	movl	%r12d, %edx
	leaq	-1552(%rbp), %rcx
	xorl	%edi, %edi
	pushq	$0
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%r13, %rsi
	call	uv_fs_write@PLT
	movq	%r13, %rdi
	movl	%eax, %r14d
	call	uv_fs_req_cleanup@PLT
	popq	%rax
	popq	%rdx
	testl	%r14d, %r14d
	js	.L306
	movl	%r12d, %edx
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	call	uv_fs_close@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	uv_fs_req_cleanup@PLT
.L302:
	movq	-1072(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L303
	testq	%rdi, %rdi
	je	.L303
	call	free@PLT
.L303:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movl	%r14d, %r12d
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L305:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	jmp	.L301
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7830:
	.size	_ZN4node13WriteFileSyncEPN2v87IsolateEPKcNS0_5LocalINS0_6StringEEE, .-_ZN4node13WriteFileSyncEPN2v87IsolateEPKcNS0_5LocalINS0_6StringEEE
	.align 2
	.p2align 4
	.globl	_ZN4node18DiagnosticFilename9LocalTimeEP2tm
	.type	_ZN4node18DiagnosticFilename9LocalTimeEP2tm, @function
_ZN4node18DiagnosticFilename9LocalTimeEP2tm:
.LFB7834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	gettimeofday@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	localtime_r@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L318
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L318:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7834:
	.size	_ZN4node18DiagnosticFilename9LocalTimeEP2tm, .-_ZN4node18DiagnosticFilename9LocalTimeEP2tm
	.section	.rodata.str1.1
.LC7:
	.string	"."
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_
	.type	_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_, @function
_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_:
.LFB7835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-368(%rbp), %rbx
	subq	$520, %rsp
	movq	%rdx, -536(%rbp)
	movq	.LC8(%rip), %xmm1
	movq	%rsi, -544(%rbp)
	movhps	.LC9(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -528(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r12, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-528(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -552(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	-512(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rdi, -528(%rbp)
	call	gettimeofday@PLT
	movq	-528(%rbp), %rdi
	leaq	-496(%rbp), %rsi
	call	localtime_r@PLT
	movq	-536(%rbp), %r8
	testq	%r8, %r8
	je	.L371
	movq	%r8, %rdi
	movq	%r8, -528(%rbp)
	call	strlen@PLT
	movq	-528(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L321:
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	cmpb	$0, 225(%rdx)
	je	.L372
.L322:
	movb	$48, 224(%rdx)
	movq	-24(%rax), %rax
	movq	%r12, %rdi
	movq	$4, -416(%rbp,%rax)
	movl	-476(%rbp), %eax
	leal	1900(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	cmpb	$0, 225(%rdx)
	je	.L373
.L326:
	movb	$48, 224(%rdx)
	movq	-24(%rax), %rax
	movq	%r12, %rdi
	movq	$2, -416(%rbp,%rax)
	movl	-480(%rbp), %eax
	leal	1(%rax), %esi
	call	_ZNSolsEi@PLT
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	cmpb	$0, 225(%rdx)
	je	.L374
.L330:
	movb	$48, 224(%rdx)
	movl	-484(%rbp), %esi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$2, -416(%rbp,%rax)
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	cmpb	$0, 225(%rdx)
	je	.L375
.L333:
	movb	$48, 224(%rdx)
	movl	-488(%rbp), %esi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$2, -416(%rbp,%rax)
	call	_ZNSolsEi@PLT
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	cmpb	$0, 225(%rdx)
	je	.L376
.L336:
	movb	$48, 224(%rdx)
	movl	-492(%rbp), %esi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$2, -416(%rbp,%rax)
	call	_ZNSolsEi@PLT
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	cmpb	$0, 225(%rdx)
	je	.L377
.L339:
	movb	$48, 224(%rdx)
	movl	-496(%rbp), %esi
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	$2, -416(%rbp,%rax)
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	call	uv_os_getpid@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdx
	addq	%r12, %rdx
	cmpb	$0, 225(%rdx)
	je	.L378
.L342:
	movb	$48, 224(%rdx)
	movq	-24(%rax), %rax
	movl	$1, %esi
	movq	$3, -416(%rbp,%rax)
	lock xaddl	%esi, _ZL3seq(%rip)
	addl	$1, %esi
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r14, %r14
	je	.L379
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L346:
	leaq	16(%r13), %rax
	movq	$0, 8(%r13)
	movq	%rax, 0(%r13)
	movq	-384(%rbp), %rax
	movb	$0, 16(%r13)
	testq	%rax, %rax
	je	.L347
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L380
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L349:
	movq	.LC8(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC10(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-552(%rbp), %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r15, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L381
	addq	$520, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L373:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	cmpb	$0, 56(%rdi)
	je	.L382
.L328:
	movb	$1, 225(%rdx)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L372:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	cmpb	$0, 56(%rdi)
	je	.L383
.L324:
	movb	$1, 225(%rdx)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L374:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	cmpb	$0, 56(%rdi)
	je	.L384
.L331:
	movb	$1, 225(%rdx)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L375:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	cmpb	$0, 56(%rdi)
	je	.L385
.L334:
	movb	$1, 225(%rdx)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L376:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	cmpb	$0, 56(%rdi)
	je	.L386
.L337:
	movb	$1, 225(%rdx)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L377:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	cmpb	$0, 56(%rdi)
	je	.L387
.L340:
	movb	$1, 225(%rdx)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L378:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L327
	cmpb	$0, 56(%rdi)
	je	.L388
.L343:
	movb	$1, 225(%rdx)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L371:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L386:
	movq	%rdx, -536(%rbp)
	movq	%rdi, -528(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-528(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-536(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L338
	movb	$1, 225(%rdx)
	movq	-432(%rbp), %rax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%rdx, -536(%rbp)
	movq	%rdi, -528(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-528(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-536(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L335
	movb	$1, 225(%rdx)
	movq	-432(%rbp), %rax
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L384:
	movq	%rdx, -536(%rbp)
	movq	%rdi, -528(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-528(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-536(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L332
	movb	$1, 225(%rdx)
	movq	-432(%rbp), %rax
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%rdx, -536(%rbp)
	movq	%rdi, -528(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-528(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-536(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L325
	movb	$1, 225(%rdx)
	movq	-432(%rbp), %rax
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%rdx, -536(%rbp)
	movq	%rdi, -528(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-528(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-536(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L329
	movb	$1, 225(%rdx)
	movq	-432(%rbp), %rax
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%rdx, -536(%rbp)
	movq	%rdi, -528(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-528(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-536(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L344
	movb	$1, 225(%rdx)
	movq	-432(%rbp), %rax
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%rdx, -536(%rbp)
	movq	%rdi, -528(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-528(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-536(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L341
	movb	$1, 225(%rdx)
	movq	-432(%rbp), %rax
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L338:
	movq	%rdx, -528(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-528(%rbp), %rdx
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%rdx, -528(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-528(%rbp), %rdx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%rdx, -528(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-528(%rbp), %rdx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%rdx, -528(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-528(%rbp), %rdx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%rdx, -528(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-528(%rbp), %rdx
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%rdx, -528(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-528(%rbp), %rdx
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%rdx, -528(%rbp)
	movl	$32, %esi
	call	*%rax
	movq	-432(%rbp), %rax
	movq	-528(%rbp), %rdx
	jmp	.L334
.L327:
	call	_ZSt16__throw_bad_castv@PLT
.L381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7835:
	.size	_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_, .-_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB7909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L390
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L390:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L392
.L406:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L406
.L392:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L407
.L393:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r14
	movl	$32, -240(%rbp)
	movq	%rax, -232(%rbp)
	andq	$-16, %r14
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$48, -236(%rbp)
	call	*%r10
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L408
	cmpq	$1, %r13
	jne	.L396
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L397:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L397
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L395:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L407:
	orq	$0, -8(%rsp,%rcx)
	jmp	.L393
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7909:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L417
	movq	16(%rdi), %r10
.L411:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L412
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L418
	movq	16(%r9), %r10
.L413:
	cmpq	%r10, %rax
	jbe	.L420
.L412:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L414:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L421
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L416:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L421:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$15, %r10d
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$15, %r10d
	jmp	.L413
	.cfi_endproc
.LFE8231:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC11:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L449
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L440
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L450
.L424:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L439:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L451
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L427:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L428
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L429:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L448:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L452
.L432:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L429
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L428:
	cmpq	%r12, %rbx
	je	.L433
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L453
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L437
.L435:
	subq	%rbx, %r12
	addq	%r12, %r8
.L433:
	testq	%r15, %r15
	je	.L438
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L438:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L437
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L450:
	testq	%r8, %r8
	jne	.L425
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L440:
	movl	$32, %esi
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L451:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L427
.L425:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L424
.L449:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8991:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.text
	.p2align 4
	.globl	_ZN4node11SplitStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEc
	.type	_ZN4node11SplitStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEc, @function
_ZN4node11SplitStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEc:
.LFB7819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 8(%rsi)
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	jne	.L482
.L454:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L483
	addq	$488, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	leaq	-328(%rbp), %rax
	movq	%rsi, %r13
	movl	%edx, %ebx
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	leaq	-448(%rbp), %r15
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, -328(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%cx, -104(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -112(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	8+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -424(%rbp)
	movq	%rax, -448(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -432(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	8(%r13), %r14
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	0(%r13), %r13
	movq	%rax, -432(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	%rax, -360(%rbp)
	movq	%r13, %rax
	movl	$0, -368(%rbp)
	addq	%r14, %rax
	je	.L456
	testq	%r13, %r13
	je	.L484
.L456:
	movq	%r14, -488(%rbp)
	cmpq	$15, %r14
	ja	.L485
	cmpq	$1, %r14
	jne	.L459
	movzbl	0(%r13), %eax
	movb	%al, -344(%rbp)
	movq	-512(%rbp), %rax
.L460:
	movq	%r14, -352(%rbp)
	leaq	-432(%rbp), %r13
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movb	$0, (%rax,%r14)
	movq	-360(%rbp), %rsi
	movq	%r13, %rdi
	movl	$8, -368(%rbp)
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	-504(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	-296(%rbp), %edx
	testl	%edx, %edx
	jne	.L461
	movsbl	%bl, %ebx
	leaq	-480(%rbp), %r14
	leaq	-464(%rbp), %r13
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L486:
	movq	-480(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L464
.L481:
	call	_ZdlPv@PLT
.L464:
	movl	-296(%rbp), %eax
	testl	%eax, %eax
	jne	.L461
.L465:
	movl	%ebx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r13, -480(%rbp)
	movq	$0, -472(%rbp)
	movb	$0, -464(%rbp)
	call	_ZSt7getlineIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EES4_@PLT
	cmpq	$0, -472(%rbp)
	je	.L486
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L466
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-480(%rbp), %rax
	cmpq	%r13, %rax
	je	.L487
	movq	%rax, (%rsi)
	movq	-464(%rbp), %rax
	movq	%rax, 16(%rsi)
.L468:
	movq	-472(%rbp), %rax
	movq	%rax, 8(%rsi)
	movl	-296(%rbp), %eax
	addq	$32, 8(%r12)
	testl	%eax, %eax
	je	.L465
.L461:
	leaq	24+_ZTVNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-360(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	cmpq	-512(%rbp), %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	-520(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -432(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-504(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L459:
	testq	%r14, %r14
	jne	.L488
	movq	-512(%rbp), %rax
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-480(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L481
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L487:
	movdqa	-464(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	-360(%rbp), %rdi
	leaq	-488(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -360(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -344(%rbp)
.L458:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %r14
	movq	-360(%rbp), %rax
	jmp	.L460
.L483:
	call	__stack_chk_fail@PLT
.L488:
	movq	-512(%rbp), %rdi
	jmp	.L458
.L484:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7819:
	.size	_ZN4node11SplitStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEc, .-_ZN4node11SplitStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEc
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB9253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L490
	testq	%r14, %r14
	je	.L506
.L490:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L507
	cmpq	$1, %r13
	jne	.L493
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L494:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L508
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L494
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L507:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L492:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L494
.L506:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L508:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9253:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,"axG",@progbits,_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_,comdat
	.p2align 4
	.weak	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.type	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, @function
_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_:
.LFB9257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	subq	$64, %rsp
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -80(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L510
	testq	%r14, %r14
	je	.L515
.L510:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L538
	cmpq	$1, %r13
	jne	.L513
	movzbl	(%r14), %eax
	movb	%al, -64(%rbp)
	movq	%rbx, %rax
.L514:
	movq	%r13, -72(%rbp)
	leaq	16(%r12), %rdi
	movb	$0, (%rax,%r13)
	movq	-80(%rbp), %r14
	movq	-72(%rbp), %r13
	movq	%rdi, (%r12)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L524
	testq	%r14, %r14
	je	.L515
.L524:
	movq	%r13, -88(%rbp)
	cmpq	$15, %r13
	ja	.L539
	cmpq	$1, %r13
	jne	.L519
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L520:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L540
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L541
	movq	%rbx, %rax
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L519:
	testq	%r13, %r13
	je	.L520
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	-80(%rbp), %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -80(%rbp)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
.L512:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	-80(%rbp), %rax
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%r12, %rdi
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-88(%rbp), %rax
	movq	%rax, 16(%r12)
.L518:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L520
.L515:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L540:
	call	__stack_chk_fail@PLT
.L541:
	movq	%rbx, %rdi
	jmp	.L512
	.cfi_endproc
.LFE9257:
	.size	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_, .-_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB9260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L546
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L544:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L544
	.cfi_endproc
.LFE9260:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB9289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L548
	testq	%rsi, %rsi
	je	.L564
.L548:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L565
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L551
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L552:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L566
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L552
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L565:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L550:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L552
.L564:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L566:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9289:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.rodata._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC12:
	.string	"lz"
.LC13:
	.string	"%d"
	.section	.text.unlikely._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB9259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L568
	leaq	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L568:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC12(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L569:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L569
	cmpb	$120, %dl
	jg	.L570
	cmpb	$99, %dl
	jg	.L571
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L572
	cmpb	$88, %dl
	je	.L573
	jmp	.L570
.L571:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L570
	leaq	.L575(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L575:
	.long	.L576-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L576-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L578-.L575
	.long	.L577-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L576-.L575
	.long	.L570-.L575
	.long	.L576-.L575
	.long	.L570-.L575
	.long	.L570-.L575
	.long	.L574-.L575
	.section	.text.unlikely._ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L572:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L579
	call	_ZdlPv@PLT
.L579:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L599
	jmp	.L581
.L570:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L604
	call	_ZdlPv@PLT
	jmp	.L604
.L576:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L601
.L578:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L586:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L586
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L601
.L574:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L601:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L598
	jmp	.L585
.L573:
	movl	(%r8), %esi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L589
	call	_ZdlPv@PLT
.L589:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L585
.L598:
	call	_ZdlPv@PLT
	jmp	.L585
.L577:
	leaq	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L585:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L604:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L581
.L599:
	call	_ZdlPv@PLT
.L581:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L593
	call	__stack_chk_fail@PLT
.L593:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9259:
	.size	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_, @function
_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_:
.LFB8971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-216(%rbp), %r8
	testq	%rax, %rax
	jne	.L606
	leaq	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L606:
	movq	%rax, %r9
	leaq	-192(%rbp), %r13
	leaq	-176(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -232(%rbp)
	leaq	.LC12(%rip), %rbx
	movq	%r9, -216(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %r9
.L607:
	movq	%r9, %rax
	movq	%r9, -216(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -248(%rbp)
	movq	%r9, -240(%rbp)
	movb	%sil, -232(%rbp)
	call	strchr@PLT
	movb	-232(%rbp), %dl
	movq	-240(%rbp), %r9
	testq	%rax, %rax
	movq	-248(%rbp), %r8
	jne	.L607
	cmpb	$120, %dl
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	jg	.L608
	cmpb	$99, %dl
	jg	.L609
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-128(%rbp), %r10
	movq	%rax, -232(%rbp)
	je	.L610
	cmpb	$88, %dl
	je	.L611
	jmp	.L608
.L609:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L608
	leaq	.L613(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L613:
	.long	.L614-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L614-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L616-.L613
	.long	.L615-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L614-.L613
	.long	.L608-.L613
	.long	.L614-.L613
	.long	.L608-.L613
	.long	.L608-.L613
	.long	.L612-.L613
	.section	.text.unlikely._ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_,comdat
.L610:
	movq	-216(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -240(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_
	movq	-240(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -216(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-232(%rbp), %rdi
	jne	.L676
	jmp	.L674
.L608:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_
	leaq	-128(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L674
.L676:
	call	_ZdlPv@PLT
	jmp	.L674
.L614:
	movq	(%r8), %r9
	leaq	-144(%rbp), %rax
	movq	8(%r8), %rcx
	movq	%rax, -232(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r9, %rax
	addq	%rcx, %rax
	je	.L622
	testq	%r9, %r9
	jne	.L622
.L628:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L622:
	movq	%rcx, -200(%rbp)
	cmpq	$15, %rcx
	jbe	.L623
	leaq	-200(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rcx, -248(%rbp)
	movq	%r9, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %r9
	movq	%rax, -160(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -144(%rbp)
.L623:
	movq	-160(%rbp), %rax
	cmpq	$1, %rcx
	jne	.L624
	movb	(%r9), %dl
	movb	%dl, (%rax)
	jmp	.L625
.L624:
	testq	%rcx, %rcx
	je	.L625
	movq	%rax, %rdi
	movq	%r9, %rsi
	rep movsb
.L625:
	movq	-200(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-160(%rbp), %rdi
	cmpq	-232(%rbp), %rdi
	jne	.L671
	jmp	.L627
.L616:
	movq	(%r8), %r9
	movq	8(%r8), %rcx
	movq	%rbx, -96(%rbp)
	movq	%r9, %rax
	addq	%rcx, %rax
	je	.L645
	testq	%r9, %r9
	je	.L628
.L645:
	movq	%rcx, -200(%rbp)
	cmpq	$15, %rcx
	jbe	.L630
	leaq	-200(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rcx, -240(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-240(%rbp), %rcx
	movq	-232(%rbp), %r9
	movq	%rax, -96(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -80(%rbp)
.L630:
	movq	-96(%rbp), %rax
	cmpq	$1, %rcx
	jne	.L631
	movb	(%r9), %dl
	movb	%dl, (%rax)
	jmp	.L632
.L631:
	testq	%rcx, %rcx
	je	.L632
	movq	%rax, %rdi
	movq	%r9, %rsi
	rep movsb
.L632:
	movq	-200(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-96(%rbp), %r8
	leaq	-112(%rbp), %rax
	movq	-88(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r8, %rax
	addq	%rcx, %rax
	je	.L646
	testq	%r8, %r8
	je	.L628
.L646:
	movq	%rcx, -200(%rbp)
	cmpq	$15, %rcx
	jbe	.L634
	leaq	-200(%rbp), %rsi
	leaq	-128(%rbp), %rdi
	xorl	%edx, %edx
	movq	%rcx, -248(%rbp)
	movq	%r8, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-248(%rbp), %rcx
	movq	-240(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -112(%rbp)
.L634:
	movq	-128(%rbp), %rax
	cmpq	$1, %rcx
	jne	.L635
	movb	(%r8), %dl
	movb	%dl, (%rax)
	jmp	.L636
.L635:
	testq	%rcx, %rcx
	je	.L636
	movq	%rax, %rdi
	movq	%r8, %rsi
	rep movsb
.L636:
	movq	-200(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-128(%rbp), %rdi
	cmpq	-232(%rbp), %rdi
	jne	.L671
	jmp	.L627
.L612:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L671
	jmp	.L627
.L611:
	movq	%r8, %rsi
	movq	%r10, %rdi
	movq	%r10, -240(%rbp)
	call	_ZN4node12ToBaseStringILj4ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEES6_RKT0_
	movq	-240(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	-128(%rbp), %rdi
	cmpq	-232(%rbp), %rdi
	je	.L627
.L671:
	call	_ZdlPv@PLT
	jmp	.L627
.L615:
	leaq	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L627:
	movq	-216(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L674:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	-192(%rbp), %rdi
	cmpq	-224(%rbp), %rdi
	je	.L605
	call	_ZdlPv@PLT
.L605:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L644
	call	__stack_chk_fail@PLT
.L644:
	addq	$216, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8971:
	.size	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_, .-_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_
	.type	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_, @function
_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_:
.LFB8556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L678
	call	__stack_chk_fail@PLT
.L678:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8556:
	.size	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_, .-_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_
	.section	.rodata.str1.1
.LC14:
	.string	"Node.js"
.LC15:
	.string	"%s[%d]"
	.text
	.p2align 4
	.globl	_ZN4node27GetHumanReadableProcessNameB5cxx11Ev
	.type	_ZN4node27GetHumanReadableProcessNameB5cxx11Ev, @function
_ZN4node27GetHumanReadableProcessNameB5cxx11Ev:
.LFB7818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_os_getpid@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, -68(%rbp)
	call	_ZN4node15GetProcessTitleB5cxx11EPKc
	movq	%r12, %rdi
	leaq	-68(%rbp), %rcx
	movq	%r13, %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN4node7SPrintFIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiEEES6_PKcDpOT_
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L680
	call	_ZdlPv@PLT
.L680:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L684
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L684:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7818:
	.size	_ZN4node27GetHumanReadableProcessNameB5cxx11Ev, .-_ZN4node27GetHumanReadableProcessNameB5cxx11Ev
	.weak	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC16:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC18:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = int; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.weak	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/debug_utils-inl.h:76"
.LC20:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC18
	.weak	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = std::__cxx11::basic_string<char>; Args = {int}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args_0:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC21
	.weak	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEJiEEES6_PKcOT_DpOT0_E4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.weak	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC22:
	.string	"../src/util.h:391"
.LC23:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EE9SetLengthEmE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.weak	_ZZN4node7ReallocItEEPT_S2_mE4args
	.section	.rodata.str1.1
.LC25:
	.string	"../src/util-inl.h:374"
.LC26:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"T* node::Realloc(T*, size_t) [with T = short unsigned int; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocItEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocItEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocItEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocItEEPT_S2_mE4args, 24
_ZZN4node7ReallocItEEPT_S2_mE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.weak	_ZZN4node7ReallocIcEEPT_S2_mE4args
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"T* node::Realloc(T*, size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIcEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIcEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIcEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIcEEPT_S2_mE4args, 24
_ZZN4node7ReallocIcEEPT_S2_mE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC28
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC29:
	.string	"../src/util-inl.h:325"
.LC30:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args
	.section	.rodata.str1.1
.LC32:
	.string	"../src/util.h:397"
.LC33:
	.string	"(length + 1) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLengthAndZeroTerminate(size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE25SetLengthAndZeroTerminateEmE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE10InvalidateEvE4args
	.section	.rodata.str1.1
.LC35:
	.string	"../src/util.h:409"
.LC36:
	.string	"!IsAllocated()"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::Invalidate() [with T = char; long unsigned int kStackStorageSize = 1024]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE10InvalidateEvE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE10InvalidateEvE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE10InvalidateEvE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE10InvalidateEvE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE10InvalidateEvE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node16MaybeStackBufferItLm1024EE25SetLengthAndZeroTerminateEmE4args
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLengthAndZeroTerminate(size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EE25SetLengthAndZeroTerminateEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EE25SetLengthAndZeroTerminateEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EE25SetLengthAndZeroTerminateEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EE25SetLengthAndZeroTerminateEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EE25SetLengthAndZeroTerminateEmE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC38
	.weak	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.1
.LC39:
	.string	"../src/util.h:376"
.LC40:
	.string	"!IsInvalidated()"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EE25AllocateSufficientStorageEmE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE9SetLengthEmE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC42
	.weak	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = char; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm1024EE25AllocateSufficientStorageEmE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/util.cc:185"
.LC45:
	.string	"(0) == (uv_gettimeofday(&tv))"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"double node::GetCurrentTimeInMicroseconds()"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node28GetCurrentTimeInMicrosecondsEvE4args, @object
	.size	_ZZN4node28GetCurrentTimeInMicrosecondsEvE4args, 24
_ZZN4node28GetCurrentTimeInMicrosecondsEvE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.local	_ZL3seq
	.comm	_ZL3seq,4,4
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/debug_utils-inl.h:67"
.LC48:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	0
	.quad	1024
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	0
	.long	1093567616
	.section	.data.rel.ro,"aw"
	.align 8
.LC8:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC9:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC10:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
