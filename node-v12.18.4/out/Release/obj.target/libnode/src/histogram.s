	.file	"histogram.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4286:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4288:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN4node13HistogramBase10TraceDeltaEl,"axG",@progbits,_ZN4node13HistogramBase10TraceDeltaEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13HistogramBase10TraceDeltaEl
	.type	_ZN4node13HistogramBase10TraceDeltaEl, @function
_ZN4node13HistogramBase10TraceDeltaEl:
.LFB4770:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4770:
	.size	_ZN4node13HistogramBase10TraceDeltaEl, .-_ZN4node13HistogramBase10TraceDeltaEl
	.section	.text._ZN4node13HistogramBase12TraceExceedsEl,"axG",@progbits,_ZN4node13HistogramBase12TraceExceedsEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13HistogramBase12TraceExceedsEl
	.type	_ZN4node13HistogramBase12TraceExceedsEl, @function
_ZN4node13HistogramBase12TraceExceedsEl:
.LFB4771:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4771:
	.size	_ZN4node13HistogramBase12TraceExceedsEl, .-_ZN4node13HistogramBase12TraceExceedsEl
	.section	.text._ZNK4node13HistogramBase8SelfSizeEv,"axG",@progbits,_ZNK4node13HistogramBase8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13HistogramBase8SelfSizeEv
	.type	_ZNK4node13HistogramBase8SelfSizeEv, @function
_ZNK4node13HistogramBase8SelfSizeEv:
.LFB4774:
	.cfi_startproc
	endbr64
	movl	$64, %eax
	ret
	.cfi_endproc
.LFE4774:
	.size	_ZNK4node13HistogramBase8SelfSizeEv, .-_ZNK4node13HistogramBase8SelfSizeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7546:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7546:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7547:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7547:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7548:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7548:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7550:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L11
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L11:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7550:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN4node9HistogramD2Ev,"axG",@progbits,_ZN4node9HistogramD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9HistogramD2Ev
	.type	_ZN4node9HistogramD2Ev, @function
_ZN4node9HistogramD2Ev:
.LFB7587:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L12
	jmp	hdr_close@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	ret
	.cfi_endproc
.LFE7587:
	.size	_ZN4node9HistogramD2Ev, .-_ZN4node9HistogramD2Ev
	.weak	_ZN4node9HistogramD1Ev
	.set	_ZN4node9HistogramD1Ev,_ZN4node9HistogramD2Ev
	.section	.text._ZN4node9HistogramD0Ev,"axG",@progbits,_ZN4node9HistogramD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9HistogramD0Ev
	.type	_ZN4node9HistogramD0Ev, @function
_ZN4node9HistogramD0Ev:
.LFB7589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	hdr_close@PLT
.L15:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7589:
	.size	_ZN4node9HistogramD0Ev, .-_ZN4node9HistogramD0Ev
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB9809:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L20
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	ret
	.cfi_endproc
.LFE9809:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB9811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9811:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L44
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L39
	cmpw	$1040, %cx
	jne	.L27
.L39:
	movq	23(%rdx), %r12
.L29:
	testq	%r12, %r12
	je	.L25
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L45
	movq	8(%rbx), %rdi
.L32:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L46
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L34
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L35:
	call	_ZNK2v86Number5ValueEv@PLT
	comisd	.LC1(%rip), %xmm0
	jbe	.L47
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L48
	movq	40(%r12), %rdi
	movq	(%rbx), %rbx
	call	hdr_value_at_percentile@PLT
	pxor	%xmm0, %xmm0
	movq	8(%rbx), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L49
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L25:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L34:
	movq	8(%rbx), %rdi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	_ZZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	_ZZN4node9Histogram10PercentileEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	_ZZN4node9Histogram10PercentileEdE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L25
	.cfi_endproc
.LFE7598:
	.size	_ZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"histogram"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node13HistogramBase10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node13HistogramBase10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node13HistogramBase10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	hdr_get_memory_size@PLT
	testq	%rax, %rax
	je	.L50
	movl	$72, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	movl	$9, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC3(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 64(%r12)
	leaq	-48(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	%r12, -48(%rbp)
	call	*8(%rax)
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rax
	call	*8(%rax)
.L53:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L50
	cmpq	72(%rbx), %rax
	je	.L64
.L55:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L50
	movq	8(%rbx), %rdi
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L50:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L55
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7592:
	.size	_ZNK4node13HistogramBase10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node13HistogramBase10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev:
.LFB4773:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1935753837, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7021788497416776008, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$101, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE4773:
	.size	_ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev, .-_ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase7DoResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase7DoResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase7DoResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L78
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L73
	cmpw	$1040, %cx
	jne	.L69
.L73:
	movq	23(%rdx), %rbx
.L71:
	testq	%rbx, %rbx
	je	.L67
	movq	40(%rbx), %rdi
	call	hdr_reset@PLT
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
.L67:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7601:
	.size	_ZN4node13HistogramBase7DoResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase7DoResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase10GetExceedsERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase10GetExceedsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase10GetExceedsERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L91
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L86
	cmpw	$1040, %cx
	jne	.L81
.L86:
	movq	23(%rdx), %rax
.L83:
	testq	%rax, %rax
	je	.L79
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rbx
	cvtsi2sdq	48(%rax), %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L92
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L79:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L79
	.cfi_endproc
.LFE7596:
	.size	_ZN4node13HistogramBase10GetExceedsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase10GetExceedsERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase9GetStddevERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase9GetStddevERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase9GetStddevERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L105
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L100
	cmpw	$1040, %cx
	jne	.L95
.L100:
	movq	23(%rdx), %rax
.L97:
	testq	%rax, %rax
	je	.L93
	movq	(%rbx), %rbx
	movq	40(%rax), %rdi
	call	hdr_stddev@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L106
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L93:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L93
	.cfi_endproc
.LFE7597:
	.size	_ZN4node13HistogramBase9GetStddevERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase9GetStddevERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase7GetMeanERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase7GetMeanERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase7GetMeanERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L119
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L114
	cmpw	$1040, %cx
	jne	.L109
.L114:
	movq	23(%rdx), %rax
.L111:
	testq	%rax, %rax
	je	.L107
	movq	(%rbx), %rbx
	movq	40(%rax), %rdi
	call	hdr_mean@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L120
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L107:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L107
	.cfi_endproc
.LFE7595:
	.size	_ZN4node13HistogramBase7GetMeanERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase7GetMeanERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase6GetMaxERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase6GetMaxERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase6GetMaxERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L133
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L128
	cmpw	$1040, %cx
	jne	.L123
.L128:
	movq	23(%rdx), %rax
.L125:
	testq	%rax, %rax
	je	.L121
	movq	40(%rax), %rdi
	call	hdr_max@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rbx
	cvtsi2sdq	%rax, %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L134
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L121:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L121
	.cfi_endproc
.LFE7594:
	.size	_ZN4node13HistogramBase6GetMaxERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase6GetMaxERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase6GetMinERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase6GetMinERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase6GetMinERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L147
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L142
	cmpw	$1040, %cx
	jne	.L137
.L142:
	movq	23(%rdx), %rax
.L139:
	testq	%rax, %rax
	je	.L135
	movq	40(%rax), %rdi
	call	hdr_min@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rbx
	cvtsi2sdq	%rax, %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L148
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L135:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L148:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L135
	.cfi_endproc
.LFE7593:
	.size	_ZN4node13HistogramBase6GetMinERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase6GetMinERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$144, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L166
	cmpw	$1040, %cx
	jne	.L150
.L166:
	movq	23(%rdx), %r13
.L152:
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L175
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L167
	cmpw	$1040, %cx
	jne	.L154
.L167:
	movq	23(%rdx), %r14
.L156:
	testq	%r14, %r14
	je	.L149
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L176
	movq	8(%rbx), %rdi
.L159:
	call	_ZNK2v85Value5IsMapEv@PLT
	testb	%al, %al
	je	.L177
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L161
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L162:
	leaq	-160(%rbp), %rbx
	movq	40(%r14), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	hdr_iter_percentile_init@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L178:
	pxor	%xmm0, %xmm0
	movsd	-64(%rbp), %xmm1
	movq	352(%r13), %rdi
	cvtsi2sdq	-128(%rbp), %xmm0
	movsd	%xmm1, -168(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movsd	-168(%rbp), %xmm1
	movq	352(%r13), %rdi
	movq	%rax, %r14
	movapd	%xmm1, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	3280(%r13), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v83Map3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
.L164:
	movq	%rbx, %rdi
	call	hdr_iter_next@PLT
	testb	%al, %al
	jne	.L178
.L149:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L161:
	movq	8(%rbx), %r12
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	_ZZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7599:
	.size	_ZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L181
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L181
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L181
	movq	%r9, %rdi
.L184:
	cmpq	%rsi, %rbx
	jne	.L182
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L182
	cmpq	16(%rdi), %rbx
	jne	.L182
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L216
	testq	%rsi, %rsi
	je	.L186
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L186
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L186:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L181:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L217
.L189:
	cmpq	$0, 8(%rbx)
	je	.L180
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L193
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L218
.L193:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L180:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L219
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L198
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L186
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L185:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L220
.L187:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L218:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r10, %rax
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L217:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L221
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L189
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%rsi, 2600(%r12)
	jmp	.L187
.L221:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L219:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7092:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN4node13HistogramBaseD2Ev,"axG",@progbits,_ZN4node13HistogramBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13HistogramBaseD2Ev
	.type	_ZN4node13HistogramBaseD2Ev, @function
_ZN4node13HistogramBaseD2Ev:
.LFB9805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13HistogramBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L223
	call	hdr_close@PLT
.L223:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE9805:
	.size	_ZN4node13HistogramBaseD2Ev, .-_ZN4node13HistogramBaseD2Ev
	.weak	_ZN4node13HistogramBaseD1Ev
	.set	_ZN4node13HistogramBaseD1Ev,_ZN4node13HistogramBaseD2Ev
	.section	.text._ZN4node13HistogramBaseD0Ev,"axG",@progbits,_ZN4node13HistogramBaseD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N4node13HistogramBaseD0Ev
	.type	_ZThn32_N4node13HistogramBaseD0Ev, @function
_ZThn32_N4node13HistogramBaseD0Ev:
.LFB9920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13HistogramBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L229
	call	hdr_close@PLT
.L229:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9920:
	.size	_ZThn32_N4node13HistogramBaseD0Ev, .-_ZThn32_N4node13HistogramBaseD0Ev
	.align 2
	.p2align 4
	.weak	_ZN4node13HistogramBaseD0Ev
	.type	_ZN4node13HistogramBaseD0Ev, @function
_ZN4node13HistogramBaseD0Ev:
.LFB9807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13HistogramBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	hdr_close@PLT
.L235:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9807:
	.size	_ZN4node13HistogramBaseD0Ev, .-_ZN4node13HistogramBaseD0Ev
	.section	.text._ZN4node13HistogramBaseD2Ev,"axG",@progbits,_ZN4node13HistogramBaseD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N4node13HistogramBaseD1Ev
	.type	_ZThn32_N4node13HistogramBaseD1Ev, @function
_ZThn32_N4node13HistogramBaseD1Ev:
.LFB9921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13HistogramBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	hdr_close@PLT
.L241:
	addq	$8, %rsp
	leaq	-32(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE9921:
	.size	_ZThn32_N4node13HistogramBaseD1Ev, .-_ZThn32_N4node13HistogramBaseD1Ev
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node13HistogramBaseD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L247
	leaq	16+_ZTVN4node13HistogramBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, 32(%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	hdr_close@PLT
.L248:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7106:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L254
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L254:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L269
.L255:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L256
	movq	8(%rax), %rax
	leaq	_ZN4node13HistogramBaseD0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L257
	leaq	16+_ZTVN4node13HistogramBaseE(%rip), %rax
	movq	40(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, 32(%r12)
	testq	%rdi, %rdi
	je	.L258
	call	hdr_close@PLT
.L258:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L255
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7104:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7094:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9HistogramC2Elli
	.type	_ZN4node9HistogramC2Elli, @function
_ZN4node9HistogramC2Elli:
.LFB7583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movl	%ecx, %edx
	leaq	-32(%rbp), %rcx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	$0, 8(%rbx)
	movq	%rax, (%rbx)
	call	hdr_init@PLT
	testl	%eax, %eax
	jne	.L280
	movq	8(%rbx), %rdi
	movq	-32(%rbp), %rax
	movq	%rax, 8(%rbx)
	testq	%rdi, %rdi
	je	.L272
	call	hdr_close@PLT
.L272:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	leaq	_ZZN4node9HistogramC4ElliE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7583:
	.size	_ZN4node9HistogramC2Elli, .-_ZN4node9HistogramC2Elli
	.globl	_ZN4node9HistogramC1Elli
	.set	_ZN4node9HistogramC1Elli,_ZN4node9HistogramC2Elli
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBaseC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli
	.type	_ZN4node13HistogramBaseC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli, @function
_ZN4node13HistogramBaseC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli:
.LFB7590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L283
	movq	%rdx, %rsi
	movq	%rdx, %r13
	movq	%r8, %r14
	movl	%r9d, %r15d
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L332
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -80(%rbp)
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	2592(%rbx), %rsi
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r8
	testq	%r9, %r9
	je	.L285
	movq	(%r9), %rax
	movq	32(%rax), %rcx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L285
	movq	32(%rdi), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L285
	movq	%rdi, %rax
.L288:
	cmpq	%rcx, %r12
	jne	.L286
	cmpq	%r11, 8(%rax)
	jne	.L286
	cmpq	16(%rax), %r12
	jne	.L286
	cmpq	$0, (%r9)
	je	.L285
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L285:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	movq	%r8, -80(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r9
	testb	%al, %al
	jne	.L289
	movq	2584(%rbx), %r10
	movq	-80(%rbp), %r8
	movq	%r12, 32(%r13)
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L299
.L337:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r8), %rax
	movq	%r13, (%rax)
.L300:
	movq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	addq	$1, 2608(%rbx)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	addq	$1, 2656(%rbx)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	call	hdr_init@PLT
	testl	%eax, %eax
	jne	.L333
	movq	40(%r12), %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 40(%r12)
	testq	%rdi, %rdi
	je	.L303
	call	hdr_close@PLT
.L303:
	movq	$0, 48(%r12)
	leaq	16+_ZTVN4node13HistogramBaseE(%rip), %rax
	movq	%rax, (%r12)
	addq	$104, %rax
	movq	%rax, 32(%r12)
	movq	24(%r12), %rax
	movq	$0, 56(%r12)
	testq	%rax, %rax
	je	.L306
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L282
.L306:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L282:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L335
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L336
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -88(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %r10
	leaq	2632(%rbx), %rax
	movq	%rax, -80(%rbp)
.L292:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L294
	xorl	%r8d, %r8d
	leaq	2600(%rbx), %r11
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L296:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L297:
	testq	%rsi, %rsi
	je	.L294
.L295:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r9
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L296
	movq	2600(%rbx), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L309
	movq	%rcx, (%r10,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L295
	.p2align 4,,10
	.p2align 3
.L294:
	movq	2584(%rbx), %rdi
	cmpq	-80(%rbp), %rdi
	je	.L298
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %r10
.L298:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r13)
	divq	%r9
	movq	%r9, 2592(%rbx)
	movq	%r10, 2584(%rbx)
	leaq	0(,%rdx,8), %r8
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L337
.L299:
	movq	2600(%rbx), %rax
	movq	%r13, 2600(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L301
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r10,%rdx,8)
.L301:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r8)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rdx, %r8
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L283:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	_ZZN4node9HistogramC4ElliE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	2632(%rbx), %r10
	movq	$0, 2632(%rbx)
	movq	%r10, -80(%rbp)
	jmp	.L292
.L334:
	call	__stack_chk_fail@PLT
.L336:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7590:
	.size	_ZN4node13HistogramBaseC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli, .-_ZN4node13HistogramBaseC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli
	.globl	_ZN4node13HistogramBaseC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli
	.set	_ZN4node13HistogramBaseC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli,_ZN4node13HistogramBaseC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli
	.section	.rodata.str1.1
.LC4:
	.string	"Histogram"
.LC5:
	.string	"exceeds"
.LC6:
	.string	"min"
.LC7:
	.string	"max"
.LC8:
	.string	"mean"
.LC9:
	.string	"stddev"
.LC10:
	.string	"percentile"
.LC11:
	.string	"percentiles"
.LC12:
	.string	"reset"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase10InitializeEPNS_11EnvironmentE
	.type	_ZN4node13HistogramBase10InitializeEPNS_11EnvironmentE, @function
_ZN4node13HistogramBase10InitializeEPNS_11EnvironmentE:
.LFB7603:
	.cfi_startproc
	endbr64
	cmpq	$0, 3136(%rdi)
	je	.L362
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	352(%rdi), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	xorl	%edx, %edx
	movl	$9, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L363
.L341:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase10GetExceedsERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L364
.L342:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase6GetMinERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L365
.L343:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase6GetMaxERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L366
.L344:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase7GetMeanERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r13
	popq	%rax
	testq	%r13, %r13
	je	.L367
.L345:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase9GetStddevERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L368
.L346:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L369
.L347:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L370
.L348:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node13HistogramBase7DoResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L371
.L349:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3136(%rbx), %rdi
	movq	352(%rbx), %r12
	testq	%rdi, %rdi
	je	.L350
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3136(%rbx)
.L350:
	testq	%r14, %r14
	je	.L338
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3136(%rbx)
.L338:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L364:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L366:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L367:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L368:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L369:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L370:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L371:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L349
	.cfi_endproc
.LFE7603:
	.size	_ZN4node13HistogramBase10InitializeEPNS_11EnvironmentE, .-_ZN4node13HistogramBase10InitializeEPNS_11EnvironmentE
	.section	.text._ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC2EPS1_:
.LFB9121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L389
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L390
.L375:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L379
.L372:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L380
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L376:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L375
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L381
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L377:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L379:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L372
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L381:
	xorl	%edx, %edx
	jmp	.L377
	.cfi_endproc
.LFE9121:
	.size	_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC2EPS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13HistogramBase3NewEPNS_11EnvironmentElli
	.type	_ZN4node13HistogramBase3NewEPNS_11EnvironmentElli, @function
_ZN4node13HistogramBase3NewEPNS_11EnvironmentElli:
.LFB7602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rcx, %rdx
	jg	.L405
	movl	%r8d, %r15d
	testl	%r8d, %r8d
	jle	.L406
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	3136(%rsi), %rdi
	movq	%rdx, %r14
	movq	3280(%rsi), %rsi
	movq	%rcx, %rbx
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L407
	movl	$64, %edi
	movq	%rax, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	%rbx, %r8
	movl	%r15d, %r9d
	movq	%rax, %rdi
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, -56(%rbp)
	call	_ZN4node13HistogramBaseC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEElli
	movq	-56(%rbp), %r10
	movq	%r13, %rdi
	movq	%r10, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_13HistogramBaseELb0EEC1EPS1_
	movq	0(%r13), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L398
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L399
	movb	$1, 9(%rax)
.L391:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movq	$0, 0(%r13)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L398:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L401
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L396:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
.L399:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	leaq	_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L401:
	xorl	%edx, %edx
	jmp	.L396
	.cfi_endproc
.LFE7602:
	.size	_ZN4node13HistogramBase3NewEPNS_11EnvironmentElli, .-_ZN4node13HistogramBase3NewEPNS_11EnvironmentElli
	.weak	_ZTVN4node9HistogramE
	.section	.data.rel.ro.local._ZTVN4node9HistogramE,"awG",@progbits,_ZTVN4node9HistogramE,comdat
	.align 8
	.type	_ZTVN4node9HistogramE, @object
	.size	_ZTVN4node9HistogramE, 32
_ZTVN4node9HistogramE:
	.quad	0
	.quad	0
	.quad	_ZN4node9HistogramD1Ev
	.quad	_ZN4node9HistogramD0Ev
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node13HistogramBaseE
	.section	.data.rel.ro._ZTVN4node13HistogramBaseE,"awG",@progbits,_ZTVN4node13HistogramBaseE,comdat
	.align 8
	.type	_ZTVN4node13HistogramBaseE, @object
	.size	_ZTVN4node13HistogramBaseE, 136
_ZTVN4node13HistogramBaseE:
	.quad	0
	.quad	0
	.quad	_ZN4node13HistogramBaseD1Ev
	.quad	_ZN4node13HistogramBaseD0Ev
	.quad	_ZNK4node13HistogramBase10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node13HistogramBase14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node13HistogramBase8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZN4node13HistogramBase10TraceDeltaEl
	.quad	_ZN4node13HistogramBase12TraceExceedsEl
	.quad	-32
	.quad	0
	.quad	_ZThn32_N4node13HistogramBaseD1Ev
	.quad	_ZThn32_N4node13HistogramBaseD0Ev
	.section	.rodata.str1.1
.LC13:
	.string	"../src/histogram.cc:106"
.LC14:
	.string	"(figures) > (0)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"static node::BaseObjectPtr<node::HistogramBase> node::HistogramBase::New(node::Environment*, int64_t, int64_t, int)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args_0, @object
	.size	_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args_0, 24
_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args_0:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/histogram.cc:105"
.LC17:
	.string	"(lowest) <= (highest)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args, @object
	.size	_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args, 24
_ZZN4node13HistogramBase3NewEPNS_11EnvironmentElliE4args:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC15
	.section	.rodata.str1.1
.LC18:
	.string	"../src/histogram.cc:84"
.LC19:
	.string	"args[0]->IsMap()"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"static void node::HistogramBase::GetPercentiles(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node13HistogramBase14GetPercentilesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"../src/histogram.cc:74"
.LC22:
	.string	"args[0]->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"static void node::HistogramBase::GetPercentile(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node13HistogramBase13GetPercentileERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"../src/histogram.cc:18"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"(0) == (hdr_init(lowest, highest, figures, &histogram))"
	.align 8
.LC26:
	.string	"node::Histogram::Histogram(int64_t, int64_t, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9HistogramC4ElliE4args, @object
	.size	_ZZN4node9HistogramC4ElliE4args, 24
_ZZN4node9HistogramC4ElliE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.weak	_ZZN4node9Histogram10PercentileEdE4args_0
	.section	.rodata.str1.1
.LC27:
	.string	"../src/histogram-inl.h:38"
.LC28:
	.string	"(percentile) <= (100)"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"double node::Histogram::Percentile(double)"
	.section	.data.rel.ro.local._ZZN4node9Histogram10PercentileEdE4args_0,"awG",@progbits,_ZZN4node9Histogram10PercentileEdE4args_0,comdat
	.align 16
	.type	_ZZN4node9Histogram10PercentileEdE4args_0, @gnu_unique_object
	.size	_ZZN4node9Histogram10PercentileEdE4args_0, 24
_ZZN4node9Histogram10PercentileEdE4args_0:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.weak	_ZZN4node9Histogram10PercentileEdE4args
	.section	.rodata.str1.1
.LC30:
	.string	"../src/histogram-inl.h:37"
.LC31:
	.string	"(percentile) > (0)"
	.section	.data.rel.ro.local._ZZN4node9Histogram10PercentileEdE4args,"awG",@progbits,_ZZN4node9Histogram10PercentileEdE4args,comdat
	.align 16
	.type	_ZZN4node9Histogram10PercentileEdE4args, @gnu_unique_object
	.size	_ZZN4node9Histogram10PercentileEdE4args, 24
_ZZN4node9Histogram10PercentileEdE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC29
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC32:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC34:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC35:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC37:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC38:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC40:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC41:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC43:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC44:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC46:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/base_object-inl.h:44"
.LC48:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC46
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC49:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC51:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.align 8
.LC2:
	.long	0
	.long	1079574528
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
